# Ruparupa API Release Notes

**Version 2.0.20180201**
    HF : Redeem voucher can not applied more than one
    *HF set store_code to empty when stock delivery not available
    HF : Count total giftcard used based on item
    HF : Fix bogo promotion, if stock not available make customer cannot buy it
    HF : Wrong remove promotion when recheck voucher code applied
    HF : Stock Buffer - 2, discount more than 70%
    Implement pickup voucher, journal, SOB2C to old server gideon
    HF : Update Stock Manual, untuk sku HCIOLY12
    HF : Counting global stock qty do not include minus stock
    HF : If we don't have gift-card information we make gift card amount to 0
    HF: Remove gift card from item
    HF : checking buyer bu, must have status buyer : saleable
    [hotfix] remove ready for pickup by 3pl validation (check awb)
    HF : Supplier Email notification order
    HF : Special price > normal price
    HF : set 0 all minus stock
    HF : add block sisco
    HF : check item preorder
    HF : validate multiple supplier review notification email
    HF : Update Attribute SAP
    HF : giftcard Journal manual post
    HF : set is in stock voucher ace, informa, toys
    [hotfix] sorting store dashboard by pin_pos

**Version 2.0.2018016**
    HF : Wrong JSON format when merging cart rules
    HF : Add specific error when creating an order
    HF : Recalculate grand total exclude shipping when delete an item
    FIX RR2-717 - OMS - Store Dashboard 2.1 - Pending Pickup Voucher
    FIX RR2-687 - OMS - Store Dashboard 2.1 - new
    Add update sales order payment endpoint
    HF : Failed to cancel payment due to stock not available

**Version 2.0.20180109.01**
    HF : Wrong JSON format when merging cart rules
    HF : Add specific error when creating an order
    HF : Recalculate grand total exclude shipping when delete an item
    OMS Dashboard 2.1
    Add update sales order payment endpoint

**Version 2.0.20180109**
    FIX RR2-719  - [API] Hold the order and notify to slack if in an order exist certain SKU
    RR2-720  - Change Store Code - Promo End Year RR
    *fix RR2-720  change store code
    HF : Bug when sending email for redeem point
    HF : Add log when redeem point result from pos
    HF : Category List Sellercenter, only having article hierarchy
   	HF : Gideon Product List, Take from product source 428
   	HF : Category List Sellercenter, only having article hierarchy
   	HF : GTIN and getProductBaseOnSku
   	HF : Return voucher ID when generate voucher failed if voucher ID is return from POS server
   	[hotfix] regenerate pickup voucher
   	HF : SKU no buffer base on start date and end date

**Version 2.0.20171223**
    *fix RR2-619 Gtin
    FIX RR2-671 - OMS - Product - Product Stock  Detail -  Sort Stock by qty
    FIX RR2-281 - OMS - Order - column Voucher
    *fix RR2-367 Stock PDP
    search by sku upplier review
    Fixed minor undefined index
    Fix RR2-580 - ( API ) Invoice Created - rule cashback input into sales_order.cart_rules
    fixed supplier alias
    RR2-477 Email MP Add Product Desc
    FIX RR2-594 - Postmark Tag
    RR2-662:lazada-order-process-change-on-item-price
    FIX RR2-474 - ( Batchprocessor ) Reminder DC Pending

**Version 2.0.20171223.hf**
    HF : MP order seller notification email nyasar && average review decimals
    HF : MP order seller notification email nyasar
    average review decimals
    HF : validate multiple supplier review notification email
    HF : unset promo free shipping 5 kota besar
    Modifying Invoices in Sales Order - For Order Detail Frontend - Fixing Delivery Method
    HF : Fix unstable delivery button when stock is empty
    HF : When to stop rule procession while applying gift card
    [hotfix] ownfleet only from DC
    hf: Typo store_Code to store_code
    [hotfix] check awb only change shipment status if shipment_status <> canceled
    HF : sku HCIOLY12, only include store code free shipping 5 kota
    HF: Typo Related Product
    HF : import posprice | move to batch pricezone getting 1 row that have special from date nearest with now
    HF : Modifying Related Product - [only is in stock = 1, status = 10, dynamic content] - Miss
    HF : Fix Cart rules do not stack
    HF : import posprice | move to batch pricezone getting 1 row that have special from date nearest with now
    HF : Get Active price when giving discount to free item
    HF : Fix implementation of marketing promo selling price item x
    HF : Fix prevent return string null when customer login
    HF : wrong delete item in shopping cart
    HF : Handle store_fulfillment item change
    *HF RR2-487 revert stock
    Set default shipping method to delivery
    Remove destruction on unit test, it cause error
    HF : DC Medan & DC Makassar for freeshipping
    *HF b2b set default return
    *HF b2b set default company address id
    HF : Remove default shipping method to delivery if it's a store fulfilment
    HF : Manual update stock
    HF : bugs discount type , fix_multiple
    🔥 [hotfix] string “null” in cart rules
    🔥 [hotfix] continue forloop instead of return fun
    Deleting code for ACE Promo 30 Nov 2017 - 3 Dec 2017
    HF - Import Price POS, override SAP
    HF - Fix Bugs, temporary seo product category
    HF : Fix when rounding prorated gift card amount
    HF : Change unit testing
    HF : PHP message: PHP Notice:  Undefined index: errors
    HF : PHP message: PHP Notice:  Undefined variable: currentDate
    HF : Undefined variable: data
    Fix RR2-665 - (API) Promo Cash back Voucher Can see per item
    Preparing for ACE Promo 30 Nov 2017 - 3 Dec 2017 (DON'T PULL PRODUCTION, JUST MERGE)
    [hotfix] add sender email in pembayaran template
    *fix gift idea
    *HF b2b notice
    HF - Not Found supplier_id
    [hotfix] Undefined index: kecamatan

**Version 2.0.20171129.2**
    Preparing for ACE Promo 30 Nov 2017 - 3 Dec 2017
    HF : check item preorder
    HF - Edit Column salesrule.discount_type
    HF - Add new type voucher cashback on salerule

**Version 2.0.20171129.1**
    Deploy B2B

**Version 2.0.20171129**
    HF : Count gift card amount not multiply to qty ordered
    HF : Return false for preorder item
    🔥[hotfix] reset password token age
    HF : Add flag encrypted address_id when get address list
    HF : Undefined index best_seller due to phalcon not return field of null value
    [hotfix] reminder sap so null exclude marketplace
    HF : Undefined ID-JK when search for default shipping cost
    HF : Undefined index Undefined index: value
    FIX RR2-631 & RR2-632 - template email shipment processing and complete
    FIX RR2-630 - Template email Pembayaran sukses
    HF : Add flag to decrypt customer_address_id
    Modify get invoice list for oms
    FIX RR2-630 - Template email Pembayaran sukses [add note text]
    FIX RR2-630 - Template email Pembayaran sukses [remove city name prefix]
    FIX RR2-664 - Dont generate awb if receipment address is HO
    HF : Wrong when counting gift card amount if applied more than one GC
    FIX RR2-630 - Template email Pembayaran sukses [set default value for send_from and add env var]
    adding actual fcm api call

**Version 2.0.20171123.1**
    OMS v2
        FIX RR2-551  - OMS - DC Dashboard - Status : Pending SO SAP
        FIX RR2-551  - OMS - DC Dashboard - Status : Pending SO SAP [endpoint batchprocessor pending so sap]
        RR2-550 : OMS - DC Dashboard - Status: All
        RR2-553 : OMS - DC Dashboard - Status : New / Pending DO
    HF : Shipping discount must not more than shipping amount
    HF : Fix handling fee count twice
    HF : Remove hardcoded max_redemption for payment promotion
    *fix batch is in stock category
    HF : PHP Warning handling fee adjust
    HF : Warning empty foreach when checking marketing rule
    HF : Fatal error when try to delete all item in cart with gift card applied
    HF : Make sure we count handling fee adjust only if value of handling fee more than zero
    HF : Remove shipping_discount_amount as required parameter
    HF : shipping discount amount not distribute to item
    HF : Group Checking for cash back promotion
    *HF bank reminder transfer ENV to getenv
    (Batchprocessor) DC Medan dan DC Makassar
    CategoryBulk Upload and Event Switching
    HF : origin shipping is populate for pickup item
    Fix RR2-592 - ( Batchprocessor ) Update qty_bk after import inventory
    HF : Remove ACE3DAYS Promo Logo
    HF : Change setFromArray to setSku Switching Events
    HF : Switching Event to MAX(id)
    HF : Update DOC Events instead of All Variant details
    *HF mapping category qty can use SKU
    *HF revert stock notice
    HF : Updating category stock mapping after reset/revert stock for event
    *HF notice undefined pickup id if store DC
    HF : Generate Special Price [Batch]
    fixed bugs when filter supplier id using mp for all data marketplace product variant count
    HF : Undefined index: recordsTotal in /var/ruparupa/api.ruparupa.srv/app/controllers/InvoiceController.php on line 39
    HF : Generate Special Price [Batch] Modifying Query
    HF : Delete Expired Special Price
    HF : Bug Fixing Special Price
    [hotfix] sql for reminder pending order store
    updating Nsq publisher library
    *HF remove cache elastic timeout
    *HF when revert stock update cache elastic using queue
    HF : fixed bugs add supplier alias in order lazada
    HF : Remove hardcoded data
    HF : Refactor code to speed up getting product category information
    Validate only status = 10 product in inspiration list
    HF - override stock juanda medan and pettarani makassar
    fix payload awb recipient
    fix bug cannot revert status to pending
    *HF when revert stock update cache elastic using queue
    HF - override stock juanda medan and pettarani makassar, fix bugs
    HF : Free item do not get free shipping promo
    HF - Stock SemiManual execute 8 - 21 nov
    HF : Exclude ownfleet from counting shipping rate
    HF : Not recalculated shipping rate when it's method is ownfleet
    HF : float not empty by php
    HF - Update Min Max , add field update min max date
    HF Fix RR2-641 - ( Batchprocessor ) Routine Process - Switch Event
    [hotfix] fix bug item list in shipment email
    *HF emarsys need categoryid in category_tree
    [hotfix] fix bug search product review by customer id
    🔥 [hotfix] highest price, handling null
    HF : Customer cannot checkout due to different grand total and sum of total_row of items.
    HF : Add shipping_discount_amount as fields can empty
    🔥 [hotfix] cart updated_at date format
    HF : Remove handling fee adjust amount from free item
    HF : Force handling fee adjust to 0 if free item
    HF - Update Qty Backup on Event
    HF - Update Stock Manual
    HF - Switch Event Next, qty_bk = qty
    Switch Event Temporary for Flash Sale
    HF : Prorate gift card amount, after prorate we need to update global gift card amount
    HF RR2-649 ( API ) Product - Import


**Version 2.0.20171123**
    Fix RR2-274 - ( API ) Category - Manage Best Seller
    Pre Order Product
    Minimize CategoryTree for Frontend

**Version 2.0.20171107.1**
    Add data for table shipping_courier_credential
    *fix Notification Long Lat
    Fix counting handling fee for pickup item
    Fix cash back amount must be deduct first with gift card amount

**Version 2.0.20171107**
    Add comments response supplier review notification
    Fix get Origin of shipping only for delivery item
    *fix is in stock/category (Promo 11)
    Fix RR2-572 - Modify courier credential source
    Fix cash back amount must be deduct first with gift card amount
    Fix counting handling fee for pickup item
    Add data for table shipping_courier_credential (sql script)
    *fix Notification Long Lat

**Version 2.0.20171031.1**
    HF : Typo limit inspirations (from 3 -> 5)
    HF : Added is_in_stock in best seller in category
    HF : Store inspiration_list in Redis
    [hotfix]fix bug not send email to cust when storefulfillment has been processing
    HF : ACE3DAYS Product's Label
    HF - Import Inventory buffer stock,exclude MP
    HF : Can delivery status item

**Version 2.0.20171031**
    Added splash_page url_key in supplier and brand (if exists)
    Fix RR2-484 - API - Marketing - Implement max_redemption in cashback
    Fix RR2-530 - API - Cashback lock to customer
    * fix bug missing courierid
    Fix RR2-324 - (API) - Implement new operator contains
    Fix RR2-534 : BATCH - Marketing : Put end promotion date time in json
    Fix RR2-533 - API - Check date and time promotion
    Fixed minor bugs product review and supplier review
    Fixed minor bugs product review and supplier review convert all status INTEGER
    FIX RR2-465 - [OMS] Shipment tracking dashboard for DC & Store
    *fix RR2-434 & RR2 436
    fix RR2-543 - Finance Report - Sheet "Customer Order Report"
    RR2-154 EMail Rating to MP Seller
    Fix RR2-533 - API - Check date and time promotion
    HF : Free shipping must see item per item
    FIX RR2-552 - Add new SAP Express Sub Account for Tangerang Origin
    FIX RR2-327 - Modify Wishlist get, add and delete data

**Version 2.0.20171024.1**
    Hot Fix CR - Exclude free item in calculating credit memo value
    [hotfix] OMS fix sql sort by total qty
    Synonym
    HF : Free item not get free shipping promotion
    Fix RR2-484 - API - Marketing - Implement max_redemption in cashback
    *HF zvo4 item price can't be higher than item unit price
    [hotfix] OMS fix manage stock list performance
    Fix RR2-498 - ( Batchprocessor ) Import Price POS - the heighest special price
    HF - Fix RR2-570 - ( API ) Import Inventory - Split Product WB
    *fix RR2-539 (FRONTEND) synonym two or more words
    HF - Heighest Price POS,revision 1

**Version 2.0.20171024**
    Fix RR2-471 - ( API/Marketing ) Salesrule - Maximum Amount
    Fix RR2-263 - Product Atribut - Instalasi - Bulk Upload
    *fix RR2-386 Member ID
    FIX RR2-438 - OMS - Product  - Sort Stock by qty
    RR2-491 ( Gideon ) Product Review - Sync with new Json Format
    add messeges empty data product review

**Version 2.0.20171017.2**
    🔥 [hotfix] pickup information
    Fix RR2-494 - ( Batchprocessor ) Import Product

**Version 2.0.20171017.1**
    fix RR2-230 - ( Batchprocessor ) Routine - Finance Report
    fix RR2-314 - Create Excel Report for finance

**Version 2.0.20171017**
    Fix RR2-443 - ( API ) productStock - MinMax
    fix RR2-163 - Search Synonym product in Front End
    * fix RR2-452 (API) update product is_in_stock status
    Salesrule Voucher List Status (translate to Indonesian)
    Fix RR2-470 - Status Voucher
    * fix RR2-454  (API) Call elastic to set is_in_stock when run updatecomparepir

**Version 2.0.20171011.2**
    HF : When Special Price Active but > Price, removed from best deal
    [HF] Fix error in get data remarketing
    HF : fixed bugs add product special Character
    HF : fixed bugs add product special Character when insert variant
    HF customer monitoring hold to approve
    HF : Cash back check add validation max usage customer
    HF : Fix Marketing Cash back, items in attribute condition mean sum of eligible item
    HF : RR2-428 : (Marketplace) tidak bisa diupdate stoknya
    HF : RR2-211 (API) Supplier Review - Save
    HF - Buffer Iventory Stock Store
    HF: Ace Promotion Spec & Icon 13 - 17 Oct 2017
    HF : Remove supplie from cache when insert or update new supplier
    HF: Prevent Category Detail with ID = '0' to be processed
    HF: Related Product if empty then empty (but will reproduce for CMS only)
    HF : RR2-469 (API) Special Character change to html entities
    HF - RR2-466 ( API ) Product Stock - Exclude Batch DC Zero

**Version 2.0.20171011.1**
    Fix RR2-421 - ( API ) Populate Sisco Products in Category
    add additional product attributes
    Fix RR2-128 - DIskon Subtotal Item X (Fix)
    FIX RR2-374 - ( Batchprocessor ) Reminder STOPS Pending
    fix RR2-381 - ( API ) Customer monitoring subtotal and giftcard marketing status hold
    FIX RR2-431 - Implement Login By Sosmed
    FIX RR2-150 - OMS - Customer  - Wishlist
    FIX RR2-160 - OMS - Customer - Account Profile - Voucher

**Version 2.0.20170926.1**

    HF : Max redemption error when try to reapply gift card
    Fix RR2-354 - (Marketing) Implement can combine
    HF : return count only status MP order list
    [HF] fixed login by facebook
    HF : Wrong when coung discount percentage but not using voucher
    [HF] fix bug move giftcard infromation
    [HF] add voucher_amount_used column in migrate giftcard info
    HF : currentDate Fix
    HF : Total giftcard amount per item can not more than it's row total
    Fix RR2-289 - (Marketing) Uses per Customer Checking
    Fix RR2-418 - ( API ) Product Update Batch
    Fix RR2-415 - ( API ) Category - import
    HF : Wrong checking condition when prorate gift card
    HF : condition "Is one of" is not working
    HF : Fix apply action voucher using voucher value affected to item is not working
    HF : Change how we recording gift card amount information in global and in item
    [HF] fix reserved stock import inventory
    HF : Wrong counting discount on item when using gift card
    HF : Condition check
    HF : Promotion condition check
    HF : Reset handling fee when recalculate shipping amount
    HF : rule_id checking now not just in cart_rules but also in gift_card applied
    Fix RR2-421 - ( API ) Generate Special Price - Import Best Deals
    Fix RR2-421 - ( API ) Populate Sisco Products in Category
    HF : Fix checking condition check in array
    HF : Fix using gift card can not combine with other item
    HF : Return validation error when apply giftcard
    🔥 [hotfix] cashback voucher expiry
    🔥 [hotfix] gift card information
    Fix RR2-430 - ( Gideon/API ) Product Detail - special price | best deals
    HF : Wrong Calculating global gift card amount in invoice & fix undefined data
    Fix updateBatch Gideon : Now attribute_set_id will be updated (related to product group)
    HF : Error getOrderNo Lazada Update Status
    RR2-390 : ( Batchprocessor ) SKU Marketplace Migration
    fixed bugs change sku
    HF : RR2-390 : ( Batchprocessor ) SKU Marketplace Migration
    Improve updateBatch Gideon


**Version 2.0.20170926**

    Edit Payload wishlist
    RR2-207 Detail Product
    RR2-205 fixed bugs (Product Review - Save)
    RR2-205 fixed bugs (Product Review - average)
    fix RR2-325 - (API) - Generate Voucher must see status
    RR2-333 (SELLERCENTER) Fix MP Report
    RR2-306 ( API ) Special Price - Button Generate Special Price
    Fix RR2-353 - ( API ) Route Detail Check If Product's status is active
    [wishlist] Get data (by email, by product_name, limit & offset)
    RR2-211 - (API) Supplier Review add pagination

**Version 2.0.20170918.2**

    Mockup wishlis
    Fix RR2-275 - buy x get item with n off
    HF | RR2-298  Add Column update batch product
    [HF] Payload wishlist
    HF | Reserved Pending Shipment, add invoice status = ready for  pickup
    HF | Add column Product Update Batch
    🔥 [hotfix] product feed query

**Version 2.0.20170918.1**

    Fix RR2-275 - buy x get item with n off

**Version 2.0.20170918**

    Fix RR2-345 - ( API ) Delete Bulk Products in Category
    FIX RR2-255 - ( API ) Gift Cards - Insert into salesrule_order
    FIX RR2-253 - ( Database ) Create Table salesrule_order
    FIX RR2-307 - ( API ) Clear All data expired - store zero , dc zero
    FIX RR2-315 - Improvement Remarketing Data Feed
    FIX RR2-209 - (API) Send Reminder Review Email
    FIX RR2-176 - [sosmed] set default value for requester params
    FIX RR2-176 - [sosmed] add requester parameter

**Version 2.0.20170912.1**

    HF | RR2-298 Add Column update batch produc
    Hot fix : When create credit memo,we make customer id to null
    HOT FIX : If customer get free shipping, set discount shipping = shipping amount
    HOT FIX : Generate new token if token is null
    HOT FIX : Wrong counting percentage discount voucher with max_redemption
    HF | List Gideon
    HF: RR2-288 - Re Fixed bugs images empty When MP status approves
    HF | RR2-246
    HOT FIX - Remove grand_total_exclude_shipping from mandatory fields
    🔥[hotfix] remarketing 💵
    Fix RR2-284 - ( API ) Reserved SKU | HF
    HOT FIX : Voucher using max_redemption not working
    HOT FIX : Redeem point
    Fix RR2-289 - (Marketing) Uses per Customer Checking (error not send to front end)
    HOT FIX : Checking marketing promotion after apply giftcart
    Fix RR2-289 - (Marketing) Uses per Customer Checking
    Add new parameter grand_total_exclude_shipping


**Version 2.0.20170912**

    fix RR2-202 - (API) Product - Product Review (save / update)
    fix RR2-204 - (API) Product - Product Review (detail
    fix RR2-202 - (API) Product - Product Review (save / update)
    fix RR2-203 - (API) Product - Product Review (list)
    fix RR2-215 - (API) Send Email To Seller
    Fix RR2-242 - ( API ) - Route Details - Remove validation for section product
    fix RR2-211 - (API) Supplier Review - Save
    Fix RR2-189 - (API) Shopping Cart - Checking Multi Variant impact
    fix RR2-213 - (API) Supplier Review - List
    Fix RR2-199 - (API) Product Detail Response - Fixing Events
    Fix RR2-199 - (API) Product Detail Response - Fixing Events & Video Detail
    Fix RR2-262 - ( API ) - Update Product is_in_stock for Batch
    Fix RR2-262 - ( API ) - Update Product & Variant is_in_stock for Batch & Other
    FIX RR2-176 - (API) Customer - Create End Point Register Using Sosmed [google]
    FIX RR2-176 - (API) Customer - Create End Point Register Using Sosmed [facebook]
    Fix RR2-279 - ( API ) Salesrule - add
    new endpoint to get remaining voucher usage
    Fix RR2-108 - ( API ) CMS Block - PlaceHolder
    Fix RR2-284 - ( API ) Reserved SKU
    Hot fix : RR2-285 - Shipping address null
    HOT FIX : Update registration voucher to 14 days after get
    RR2-282 & remove max_redemption in fixed amount
    Fix RR2-282 - Credit Memo wrong when counting discount
    HOT FIX : Revert max_redemption in fix amount discount
    Fix RR2-279 - ( API ) Salesrule - add
    fix RR2-258 - MP Report - settlement and summary
    fix RR2-267 - ( API ) Product List - Images | HF
    Fix RR2-251 - (API) Make handling fee to 0 if handling fee is less than 0

**Version 2.0.2017.09.05.0**

    Seperate process to save payment information
    Fix bug with counting shipping cost
    (Shipping null) Remake Code
    Fix Inbalance Journal
    Reminder shopping marathon
    Import inventory use array chunk in buffer stock function
    Fix remark ussing carrier code not carrier name
    customer alert channel use .env
    Fix bug with same stock can not go through
    Fix bug with customer can redeem voucher more than voucher amount
    Imlement cashback promot checked from shopping cart
    Fix create cashback max_redemption set to 0
    Fix RR2-242 - ( API ) - Route Details - Flushing Redis when updating url_key
    Fix RR2-224 - Implement Cancel Payment CC
    HOT FIX : add logic to handle if qty / weight is 0 and if customer phone is empty

**Version 2.0.2017.08.30.0**

    Remarketing Criteo
    Fix Lazada order always go to DC
    Fix bug Compare Product
    Fix bug update product 2 Category
    Implement Customer Alert

**Version 2.0.2017.08.24.1**

	hotfix : dont update order and invoice status for refund status when shipping status change

**Version 2.0.2017.08.24**

	Reminder Email
	Validation member number
	Redirect falsh sale to next flash sale if event is active
