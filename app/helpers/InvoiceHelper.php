<?php

namespace helpers;


class InvoiceHelper
{
    public static function isOrderAutoReceived($salesOrder, $invoice)
    {
        if ($invoice->getDeliveryMethod() != "pickup" || $salesOrder->getStoreCodeNewRetail() == "" || strlen($salesOrder->order_no) < 4 || $invoice->getStoreCode() != $salesOrder->getStoreCodeNewRetail()) {
            return false;
        }

        $orderPrefix = substr($salesOrder->order_no, 0, 4);

        $orderODIRegex = "/^ODI\d+$/";
        if (preg_match($orderODIRegex, $salesOrder->order_no)) {
            $orderPrefix = substr($salesOrder->order_no, 0, 3);
        }

        $storeLib = new \Library\Store();
        $storeList = $storeLib->getAllStore(["store_code" => $invoice->getStoreCode()]);
        if (!isset($storeList['list'])) {
            return false;
        }

        if (count($storeList['list']) <= 0) {
            return false;
        }

        $store = $storeList['list'][0];
        $listOrderAutoReceived = explode(",", $store['order_auto_received']);
        $isEligibleAutoReceived = in_array($orderPrefix, $listOrderAutoReceived);
        if (!$isEligibleAutoReceived) {
            return false;
        }

        $invoiceItem = $invoice->getInvoiceItems();
        if (count($invoiceItem) <= 0) {
            $siiModel = new \Models\SalesInvoiceItem();
            $siiCondition = array("conditions" => "invoice_id = " . $invoice->invoice_id);
            $invoiceItem = $siiModel->find($siiCondition);
        }

        $isStatusFulfillmentEligible = 0;
        $isItemShopInStore = 0;
        foreach ($invoiceItem as $item) {
            if ($item->status_fulfillment == "complete" || $item->status_fulfillment == "") {
                $isStatusFulfillmentEligible++;
            }
            $salesOrderItemModel = new \Models\SalesOrderItem;
            $salesOrderItemCondition = array("conditions" => "sales_order_item_id = " . $item->sales_order_item_id);
            $salesOrderItemResult = $salesOrderItemModel->findFirst($salesOrderItemCondition)->toArray();
            if ($salesOrderItemResult['is_product_scanned'] == 10 && $salesOrderItemResult['is_within_radius'] == 10) {
                $isItemShopInStore++;
            }
        }
        return $isStatusFulfillmentEligible == $isItemShopInStore;
    }

}