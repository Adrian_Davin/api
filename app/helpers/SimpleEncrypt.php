<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/30/2017
 * Time: 3:39 PM
 *
 * Simple encryption to passing variable to URL
 */

namespace Helpers;


class SimpleEncrypt
{
    public static function encrypt($text = "")
    {
        $baseEncode = base64_encode($text);
        $encoded = bin2hex($baseEncode);

        return $encoded;
    }

    public static function decrypt($text = "")
    {
        $strProperty = strlen($text)%2 == 0 ? "even" : "odd";

        if($strProperty == "odd") {
            $text = $text."1";
        }

        $baseEncoded = hex2bin($text);
        $origin_string = base64_decode($baseEncoded);

        return $origin_string;
    }

}