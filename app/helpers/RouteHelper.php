<?php
/**
 * Created by PhpStorm.
 * User: Aaron Putra
 * Date: 16/05/2017
 * Time: 14.40
 */

namespace Helpers;

use Models\MasterUrlKey;
use Phalcon\Db;

class RouteHelper
{
    public static function getRouteData($ref_id, $section, $access_type = "", $companyCode = 'ODI'){
        $master_url_model = new MasterUrlKey();
        $parameter = [
            "conditions" => "reference_id ='" . $ref_id ."' AND section ='".$section."' AND company_code like '%".$companyCode."%'",
            "order" => "created_at desc",
        ];

        $urlKey = $master_url_model::findFirst($parameter);

        if($urlKey) {
            $urlData = array(
                'url_key' => $urlKey->getUrlKey().".html",
                'additional_header' => $urlKey->getAdditionalHeader(),
                'additional_header_mobile' => $urlKey->getAdditionalHeaderMobile(),
                'inline_script' => $urlKey->getInlineScript(),
                'inline_script_mobile' => $urlKey->getInlineScriptMobile(),
                'document_title' => $urlKey->getDocumentTitle(),
                'company_code' => $urlKey->getCompanyCode()
            );
            return $urlData;
        }
        else {
            return array();
        }

    }

    /**
     * @param int $ref_id
     * @param string $section
     * @param string $url_key
     * @return array
     */
    public static function checkUrlKey($ref_id = 0, $section = "", $url_key = "", $companyCode = 'ODI', $oldUrlKey = ""){
        /**
         * url_key_status :
         *  if '0' => url exist on other section [FALSE]
         *  if '1' => url not exist on other section, but exist in current section [TRUE/Need Update Url]
         *  if '2' => url not exist on both section / new [TRUE]
         */
        $master_url_model = new MasterUrlKey();
        $urlKey = array();

        // Always fresh - search in database using RAW Query - to prevent from caching
        if($section == "promo_page"){ //use oldUrlKey to search if section is promo_page (for TAHU use only)
            $sql  = "SELECT url_key, section, reference_id FROM master_url_key WHERE url_key = '".$oldUrlKey."' AND company_code = '".$companyCode."' LIMIT 1";
        }else{
            $sql  = "SELECT url_key, section, reference_id FROM master_url_key WHERE url_key = '".$url_key."' AND company_code = '".$companyCode."' LIMIT 1";
        }
        
        
        $master_url_model->useWriteConnection();
        $result = $master_url_model->getDi()->getShared($master_url_model->getConnection())->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $urlKey = $result->fetchAll();

        if (!empty($urlKey)) {
            if($urlKey[0]['section'] == $section && $urlKey[0]['reference_id'] == $ref_id){
                return array("url_key_status" => '1');
            }
            else{
                return array("url_key_status" => '0');
            }
        } else {
            return array("url_key_status" => '2');
        }
    }

    public static function buildFormatUrlKey($data = array()) {
        if (empty($data)) {
            return false;
        }

        // build structure for update / save url key
        $urlData = array(
            'url_key' => $data['url_key'],
            'section' => $data['section'],
            'inline_script' => $data['inline_script'],
            'additional_header' => $data['additional_header'],
            'inline_script_mobile' => $data['inline_script_mobile'],
            'additional_header_mobile' => $data['additional_header_mobile'],
            'identifier' => isset($data['identifier']) ? $data['identifier'] : 0 ,
            'document_title' => "Jual ".$data['name']." | Ruparupa",
            'reference_id' => $data['reference_id'],
            'company_code' => isset($data['company_code']) ? $data['company_code'] : 'ODI',
            'created_at' => date('Y-m-d H:i:s')
        );

        return $urlData;
    }
}