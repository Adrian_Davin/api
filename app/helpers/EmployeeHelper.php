<?php

/**
 * Created for ruparupa by.
 * User: Reza Fiansyah Putra
 * Date: 18/11/2021
 * Time: 11:11 AM
 */

namespace Helpers;

class EmployeeHelper
{
    public static function getSAPCompanyIDByBusinessUnitCode($businessUnitCode)
    {
        $sapCompanyID = "";

        switch (strtoupper($businessUnitCode)) {
            case "AHI":
                $sapCompanyID = "R100";
                break;
            case "HCI":
                $sapCompanyID = "R110";
                break;
            case "TGI":
                $sapCompanyID = "R120";
                break;
            case "ODI":
                $sapCompanyID = "R130";
                break;
            default:
                $sapCompanyID = "";
        }

        return $sapCompanyID;
    }
}
