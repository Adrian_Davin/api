<?php

namespace helpers;

class ConversionHelper
{
    /**
     * Generate XML from array.
     * This function will not use int as key and will use parent key if posible or use default key item+$key
     * @param $xml_data
     * @param $data
     * @param string $parent_key
     */
    public static function arrayToXml( &$xml_data, $data, $parent_key = "" )
    {
        foreach ($data as $key => $value) {
            $this_key = $key;
            if (is_array($value)) {
                if (is_numeric($key) && empty($parent_key)) {
                    $key = "item".$key; //dealing with <0/>..<n/> issues
                } else if (!empty($parent_key)) {
                    $key = $parent_key;
                }

                if(!is_numeric(key($value))) {
                    $subnode = $xml_data->addChild($key);
                } else {
                    $subnode = $xml_data;
                }

                self::arrayToXml($subnode, $value, $this_key);

            } else {
                $xml_data->addChild("$key", htmlspecialchars("$value"));
            }

        }
    }
}


