<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/21/2016
 * Time: 3:55 PM
 */

namespace Helpers;


use function GuzzleHttp\Psr7\str;
use Library\Nsq;
use Models\Product;
use Models\Store;
use Models\SupplierAddress;
use Models\PickupPoint;
use Models\Supplier;
use Library\APIWrapper;

class ProductHelper
{
    public static function completeStoreInformation($productStock = array())
    {
        $storeModel = new Store();
        $supplierAddressModel = new SupplierAddress();
        $supplierModel = new Supplier();
        $pickupPointModel = new PickupPoint();
        $storeWithStockData = [];
        if(count($productStock) > 0) {
            $tempStoreCode = array();
            foreach($productStock as $stock) {
                $tempStoreCode[] = $stock['store_code'];
                $tempQtyStoreCode[$stock['store_code']] = $stock['qty'];
            }
            // echo 'before findx '. date("l jS \of F Y h:i:s A");
            $storeDatas = $storeModel->findX(
                   array( 
                       'conditions' => "store_code IN ('".implode("','",$tempStoreCode)."') AND status = 10 "
                       )
                );
            // echo 'after findx '. date("l jS \of F Y h:i:s A");
            foreach($storeDatas as $storeData) {                
                if($storeData) {
                    $supplierAlias = $storeData->Supplier->getSupplierAlias();
                    $pickup_location = !empty($storeData->PickupPoint) ? $storeData->PickupPoint->pickup_code : $storeData->getStoreCode();
                    $stockData['store_code'] = $storeData->getStoreCode();
                    $stockData['name'] = $storeData->getName();
                    $stockData['qty'] = $tempQtyStoreCode[$stockData['store_code']];
                    if($storeData->PickupPoint && $storeData->PickupPoint->MasterCity) {
                        $stockData['city'] = $storeData->PickupPoint->MasterCity->toArray();
                    } else {
                        $stockData['city'] = array();
                    }

                    $stockData['zone_id'] = $storeData->getZoneId();

                    if (!empty($storeData->PickupPoint)) {
                        $stockData['origin_shipping_region'] = $storeData->PickupPoint->getProvinceCode();
                        $stockData['origin_shipping_district'] = $storeData->PickupPoint->getKecamatanCode();
                    } else if($storeData->SupplierAddress) {
                        $supplierAddress = $storeData->SupplierAddress;
                        $stockData['origin_shipping_region'] = $supplierAddress->getProvinceCode();
                        $stockData['origin_shipping_district'] = $supplierAddress->getKecamatanCode();
                    }  else {
                        $stockData['origin_shipping_region'] = "ID-JK"; // Assume it's send from jakarta
                        $stockData['origin_shipping_district'] = getenv('DEFAULT_ORIGIN_SHIPPING_DISTRICT');
                    }

                    if(empty($stockData['origin_shipping_district'])){
                        $stockData['origin_shipping_region'] = "ID-JK"; // Assume it's send from jakarta
                        $stockData['origin_shipping_district'] = getenv('DEFAULT_ORIGIN_SHIPPING_DISTRICT');
                    }

                    $stockData['supplier_alias'] = $supplierAlias;
                    $stockData['fulfillment_center'] = $storeData->getFulfillment_center();
                    $stockData['pickup_center'] = $storeData->getPickup_center();
                    $stockData['pickup_data'] = [];
                    if(!empty($storeData->PickupPoint)) {
                        $pickup_data = $storeData->PickupPoint->toArray();
                        $stockData['pickup_data'] = [
                            "pickup_name" => $pickup_data['pickup_name'],
                            "address_line_1" => empty($pickup_data['address_line_1']) ? '' : $pickup_data['address_line_1'],
                            "address_line_2" => empty($pickup_data['address_line_2']) ? '' : $pickup_data['address_line_2'],
                            "is_express_courier" => empty($pickup_data['is_express_courier']) ? '' : $pickup_data['is_express_courier'],
                            "geolocation" => empty($pickup_data['geolocation']) ? '' : $pickup_data['geolocation']
                        ];
                    }

                    $storeWithStockData[$pickup_location][] = $stockData;
                } else {
                    /**
                     * @todo: @aaronpoetra
                     *
                     **/
                }
            }

            // Just a litte trick to make DC to top of the list
            ksort($storeWithStockData);
        }

        return $storeWithStockData;
    }

    /**
     * @param array $productStock
     * @return array
     */
    public static function getStoreInformation($storeList = array())
    {
        $storeModel = new \Models\Store();
        $storeWithStockData = [];

        foreach($storeList as $store_code) {
            $storeData = $storeModel::findFirst(["store_code = '".$store_code."'"]);
            if($storeData) {
                $stockFound = true;
                $supplierAlias = $storeData->Supplier->getSupplierAlias();
                $pickup_location = !empty($storeData->PickupPoint) ? $storeData->PickupPoint->pickup_code : $storeData->getStoreCode();
                $finalStoreData = ['store_code' => $store_code];

                if($storeData->PickupPoint && $storeData->PickupPoint->MasterCity) {
                    $finalStoreData['city'] = $storeData->PickupPoint->MasterCity->toArray();
                } else {
                    $finalStoreData['city'] = array();
                }

                $finalStoreData['zone_id'] = $storeData->getZoneId();

                if (!empty($storeData->PickupPoint)) {
                    $finalStoreData['origin_shipping_region'] = $storeData->PickupPoint->getProvinceCode();
                    $finalStoreData['origin_shipping_district'] = $storeData->PickupPoint->getKecamatanCode();
                } else if($storeData->SupplierAddress) {
                    $supplierAddress = $storeData->SupplierAddress;
                    $finalStoreData['origin_shipping_region'] = $supplierAddress->getProvinceCode();
                    $finalStoreData['origin_shipping_district'] = $supplierAddress->getKecamatanCode();
                }  else {
                    $finalStoreData['origin_shipping_region'] = "ID-JK"; // Assume it's send from jakarta
                    $finalStoreData['origin_shipping_district'] = getenv('DEFAULT_SHIPPING_KECAMATAN_CODE'); // Assume it's send from jakarta
                }

                $finalStoreData['supplier_alias'] = $supplierAlias;
                $finalStoreData['pickup_data'] = [];
                if(!empty($storeData->PickupPoint)) {
                    $pickup_data = $storeData->PickupPoint->toArray();
                    $finalStoreData['pickup_data'] = [
                        "pickup_name" => $pickup_data['pickup_name'],
                        "address_line_1" => empty($pickup_data['address_line_1']) ? '' : $pickup_data['address_line_1'],
                        "address_line_2" => empty($pickup_data['address_line_2']) ? '' : $pickup_data['address_line_2'],
                    ];
                }

                $storeWithStockData[$pickup_location][] = $finalStoreData;
            }
        }

        // Just a litte trick to make DC to top of the list
        ksort($storeWithStockData);


        return $storeWithStockData;
    }

    /**
     * check stock for this item in selected store
     *
     * @param string $sku
     * @param string $store_code
     * @param int $qty_ordered
     * @return bool
     */
    public static function checkStockItem($sku = "", $store_code = "", $qty_ordered = 0, $company_code = "ODI")
    {
        if(empty($sku) || empty($store_code)) {
            return array(false , 0);
        }

        // Load shopping card data
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
        $apiWrapper->setEndPoint("legacy/stock/check_available/".$sku."?store_code=".$store_code."&company_code=".strtolower($company_code));

        if($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
        } else {
            return array(false, 0);
        }

        if (!empty($apiWrapper->getError())) {
            return array(false, 0);
        }

        // maybe cart is found, but data is empty
        $productStockData = $apiWrapper->getData();

        if (!empty($productStockData['max_qty']) && $productStockData['max_qty'] > 0 && $productStockData['max_qty'] >= $qty_ordered) {
            return array(true, $productStockData['max_qty']);
        } else {
            return array(false, 0);
        }
    }


    /**
     * check stock for this item in selected store
     *
     * @param string $sku
     * @return bool
     */
    public static function filterLastIncrement($sku = "")
    {
        $lastID = ltrim(substr($sku, 5),"0");
        $nextID = $lastID + 1;
        return $nextID;
    }

    /**
     * check status content, status image, status buyer, status mp
     *
     * @param string $params
     * @return array
     */
    public static function checkStatusGlobal($params = array())
    {
        $result = array();
        /**
         * ToDo : Marketplace
         */
        if($params['product_source'] == 857 ){
            if($params['status_mp'] == 'approved' AND $params['status_buyer_mp'] == 'approved') {
                if ($params['update_global_status'] == "inactive") {
                    $result['status'] = 0;
                } else if ($params['update_global_status'] == "active") {
                    $result['status'] = 10;
                }else{
                    $result['status'] = 10;
                }
            }elseif($params['status_mp'] == 'delete'){
                $result['status'] = -1;
            }else{
                $result['status'] = 0;
            }
        }else{
            if( $params['status_content'] === 'complete' &&
                $params['status_image'] === 'complete' &&
                $params['status_buyer'] === 'saleable'
            ){
                $result['status'] = 10;
            }elseif($params['status_image'] === 'complete' &&
                $params['status_content'] === 'pending check buyer bu'
            ){
                $result['status_content'] = 'ready check buyer bu';
            }else{
                $result['status'] = 0;
            }

        }

        return $result;
    }

    /**
     * Check Format Date
     *
     * @param string $date
     * @return boolean
     */
    public static function checkIsAValidDate($myDateString){
        return (bool)strtotime($myDateString);
    }

    public static function updateProductToElastic($params = array(), $updateProduct2Category = false){

        if($updateProduct2Category){
            $sku = array();
            //we need to re-format
            foreach ($params as $key => $val){
                $sku[$key]['sku'] = $val['sku'];
            }

            $params = array_map("unserialize", array_unique(array_map("serialize", $sku)));;
        }

        $nsq = new Nsq();
        foreach ($params as $product) {
            $message = [
                "data" => ["sku" => isset($product['sku'])?str_replace("'","",$product['sku']):str_replace("'","",$product)],
                "message" => "createCache"
            ];
            $nsq->publishCluster('product', $message);
        }
    }

    // public static function compareProducts($old_products = "", $new_products = "", $merging = FALSE)
    // {
    //     $new = $new_products;
    //     $old = $old_products;
    //     $sku = array();
    //     $arr_new = array();
    //     $arr_old = array();

    //     if(empty($new) && empty($old)){
    //         return false;
    //     }

    //     if(!empty($new) && !empty($old)){
    //         $arr_new = explode(',', $new);
    //         $arr_old = explode(',', $old);

    //         if($merging){
    //             $sku = array_unique(array_merge($arr_new,$arr_old));
    //         }
    //         else{
    //             $arr_diff_new = array_diff($arr_new, $arr_old);
    //             $arr_diff_old = array_diff($arr_old,$arr_new);

    //             $arr_merge = array_merge($arr_diff_new,$arr_diff_old);
    //             $sku = $arr_merge;
    //         }
    //     }
    //     else{
    //         if(!empty($new)){
    //             $arr_new = explode(',', $new);
    //             $sku = $arr_new;
    //         }
    //         else if (!empty($old)){
    //             $arr_old = explode(',', $old);
    //             $sku = $arr_old;
    //         }
    //     }

    //     // update to elastic
    //     if(!empty($sku)){
    //         foreach ($sku as $key => $val){
    //             $nsq = new Nsq();
    //             $message = [
    //                 "data" => ["sku" => $val],
    //                 "message" => "createCache"
    //             ];
    //             $nsq->publishCluster('product', $message);
    //         }
    //     }
    //     else{
    //         foreach ($arr_old as $key => $val){
    //             $nsq = new Nsq();
    //             $message = [
    //                 "data" => ["sku" => $val],
    //                 "message" => "createCache"
    //             ];
    //             $nsq->publishCluster('product', $message);
    //         }
    //     }

    //     return true;

    // }

    public static function compareProductsEvent($eventModel, $companyCode = 'ODI', $lock_qty = '10')
    {
        $new = "";
        $old = "";
        $sku = array();
        $arr_new = array();
        $arr_old = array();
        $eventStatus = $eventModel->getStatus();

        if ($lock_qty == '0') {
            // no locking qty - means, no updating event to elastic
            // we have to clean things up, remove event from product that corresponding to this ID
            // TODO
            $overrideLockStatus = array('active' => false, 'event_id' => $eventModel->getEventId(), 'force_delete' => true, 'product_list' => $eventModel->getProducts());
            self::updateEventsToElastic($overrideLockStatus, $companyCode);
            return true;
        }

        if (!empty($eventModel)) {
            $old_p['event_id'] = $eventModel->getEventId();
            $old_p['title'] = $eventModel->getTitle();
            $old_p['start_date'] = date('Y-m-d H:i:s', strtotime($eventModel->getStartDate()));
            $old_p['end_date'] = date('Y-m-d H:i:s', strtotime($eventModel->getEndDate()));
            $old_p['url_key'] = $eventModel->getUrlKey();
            $old_p['small_banner'] = $eventModel->getSmallBanner();
            $old_p['is_active_small_banner'] = $eventModel->getIsActiveSmallBanner();
            if(!empty($eventModel->getOldProducts())) {
                $old_p['products'] = $eventModel->getOldProducts();
                $new_p['products'] = $eventModel->getProducts();
                $new = $new_p['products'];
            } else {
                $old_p['products'] = $eventModel->getProducts();
            }
            $old_p['status'] = $eventStatus;
            if ($old_p['status']=='') {
                $old_p['status'] = 10;
            }
            $old = $old_p['products'];
        }

        if(empty($new) && empty($old)){
            return false;
        }

        if(!empty($new) && !empty($old)){
            // update event
            $arr_new = explode(',', $new);
            $arr_old = explode(',', $old);
            $sku = array_intersect($arr_old,$arr_new);
            $old_p['action'] = 'update';
        } else if(!empty($old)) {
            // add event
            $arr_old = explode(',', $old);
            $sku = $arr_old;
            $old_p['action'] = 'add';
        }

        // check event within range start date and end date
        $nowDate = date('Y-m-d H:i:s');
        $nowDate = date('Y-m-d H:i:s', strtotime($nowDate));

        $startDate = $old_p['start_date'];
        $endDate = $old_p['end_date'];

        if (($nowDate >= $startDate) && ($nowDate <= $endDate))
        {
            if ($old_p['status'] == 10) {
                $old_p['active'] = true;
            }
            else {
                $old_p['active'] = false;
            }
        } else {
            $old_p['active'] = false;
        }

        // update to elastic
        if(!empty($sku)){
            foreach ($sku as $key => $val){
                $id = self::getIdFromElastic(['sku' => $val]);
                if (!empty($id)) {
                    $old_p['id'] = $id;
                    $old_p['sku'] = $val;
                    self::updateEventsToElastic($old_p, $companyCode);
                }
            }
        }

        if($old_p['action'] == 'update') {
            // add events to elastic
            $arr_diff_new = array_diff($arr_new,$arr_old);
            if(!empty($arr_diff_new)){
                foreach ($arr_diff_new as $key => $val){
                    $id = self::getIdFromElastic(['sku' => $val]);
                    if (!empty($id)) {
                        $old_p['id'] = $id;
                        $old_p['action'] = 'add';
                        $old_p['sku'] = $val;
                        self::updateEventsToElastic($old_p, $companyCode);
                    }
                }
            }

            // remove events in elastic
            $arr_diff_old = array_diff($arr_old,$arr_new);
            if(!empty($arr_diff_old)){
                foreach ($arr_diff_old as $key => $val){
                    $id = self::getIdFromElastic(['sku' => $val]);
                    if (!empty($id)) {
                        $old_p['id'] = $id;
                        $old_p['active'] = false;
                        $old_p['sku'] = $val;
                        self::updateEventsToElastic($old_p, $companyCode);
                    }
                }
            }
        }

        return true;

    }

    // retrieve ids from elastic
    public static function getIdFromElastic($params) {
        $sku = '';
        if (isset($params['sku'])) {
            $sku = $params['sku'];
        }

        $body = '
        {
            "query": {
                "bool": {
                    "must": [{
                        "nested": {
                            "path": "variants",
                            "query": {
                                "terms": {
                                    "variants.sku.faceted": ["'. $sku . '"]
                                }
                            }
                        }
                    }]
                }
            }
        }';

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/_search';
        if(!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }
        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);

        $id = '';
        foreach ($result['hits']['hits'] as $id => $value) {
            $id = $value['_id'];
        }

        return $id;
    }

    // retrieve ids from elastic
    public static function getSupplierFromElastic($params) {
        $sku = '';
        if (isset($params['sku'])) {
            $sku = $params['sku'];
        }

        $body = '
        {
            "query": {
                "bool": {
                    "must": [{
                        "nested": {
                            "path": "variants",
                            "query": {
                                "terms": {
                                    "variants.sku.faceted": ["'. $sku . '"]
                                }
                            }
                        }
                    }]
                }
            }
        }';

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/_search';
        if(!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }
        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);

        $valueField = '';
        foreach ($result['hits']['hits'] as $id => $value) {
            $valueField = $value['_source']['supplier'];
        }

        return $valueField;
    }

    public static function updateEventsToElastic($params, $companyCode = 'ODI') {
        $productPriceModel = new \Models\ProductPrice();
        // query to update special price
        $sql = "UPDATE product_price SET modified_description = '??PLACEHOLDER' WHERE sku = '".$params['sku']."'";
        if (isset($params['active']) && $params['active']) {
            // add here to mutate modified_description:
            // "Barang ini termasuk dalam Flash Sale pada $params['start_date'] - $params['end_date']"
            if(isset($params['action']) && $params['action'] == 'add') {
                //add event
                $body = '{
                      "script": {
                        "lang": "painless",
                        "source": "int flag = 0; for (int index = 0; index < ctx._source.events.length; index++) { if(ctx._source.events[index].event_id == '.intVal($params['event_id']).') { flag = 1 } } if(flag == 0) { ctx._source.events.add(params.events) }",
                        "params": {
                            "events" : {
                              "event_id" : ' .intVal($params['event_id']). ',
                              "start_date" : "' . $params['start_date'] . '",
                              "end_date" : "' . $params['end_date'] . '",
                              "title" : "' . $params['title'] . '",
                              "url_key" : "' . $params['url_key'] . '",
                              "company_code" : "'. $companyCode .'",
                              "small_banner": "'.$params['small_banner'].'",
                              "is_active_small_banner": '.($params['is_active_small_banner'] ? 'true' : 'false').'
                            }
                          }
                      }
                    }';
            } else {
                // update event
                // The update script doesn't replace the existing, instead removes and insert it back
                // to make sure the event is on the last position.
                $body = '
                {
                  "script": {
                    "lang": "painless",
                    "source": "def eventList = ctx._source.events; eventList.removeIf(e -> e.event_id == ' . intVal($params['event_id']) . '); eventList.add(params.events);",
                    "params": {
                        "events" : {
                            "event_id" : '.intVal($params['event_id']).',
                            "start_date" : "' . $params['start_date'] . '",
                            "end_date" : "' . $params['end_date'] . '",
                            "title" : "' . $params['title'] . '",
                            "url_key" : "' . $params['url_key'] . '",
                            "company_code" : "'. $companyCode .'",
                            "small_banner": "'.$params['small_banner'].'",
                            "is_active_small_banner": '.($params['is_active_small_banner'] ? 'true' : 'false').'
                        }
                    }
                  }
                }';
            }

            // edit modified_description
            $sql = str_replace("??PLACEHOLDER", "Barang ini termasuk dalam ".$params['title']." pada ".$params['start_date']." s/d ".$params['end_date']."", $sql);
            $productPriceModel->useWriteConnection();
            $productPriceModel->getDi()->getShared($productPriceModel->getConnection())->query($sql);            
        } else {
            if (isset($params['force_delete']) && $params['force_delete']) {
                $productsData = '["'.str_replace(',','","',$params['product_list']).'"]';
                $body = '{
                    "query": {
                        "nested": {
                          "path": "variants",
                          "query": {
                            "terms": {
                              "variants.sku.faceted": '.$productsData.'
                            }
                          }
                        }
                      },
                      "script": {
                        "lang": "painless",
                        "source": "for (int index = 0; index < ctx._source.events.length; index++) { if(ctx._source.events[index].event_id == '.intVal($params['event_id']).') { ctx._source.events.remove(index) } }"
                      }
                }';
            } else {
                // event not active remove event
                $body = '
                {
                "script": {
                    "lang": "painless",
                    "source": "for (int index = 0; index < ctx._source.events.length; index++) { if(ctx._source.events[index].event_id == '.intVal($params['event_id']).') { ctx._source.events.remove(index) } }"
                }
                }';
            }

            // edit modified_description
            $sql = str_replace("??PLACEHOLDER", "", $sql);
            $productPriceModel->useWriteConnection();
            $productPriceModel->getDi()->getShared($productPriceModel->getConnection())->query($sql);            
        }

        if (isset($params['force_delete']) && $params['force_delete']) {
            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/_update_by_query?refresh=true';
        } else {
            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/' . $params['id'] .
            '/_update?refresh=true';
        }

        if(!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }
        $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);
    }

    public static function buildStructure($product_cart_data = array(), $sku = ''){
        // mapping manual
        $product_details = array();
        $store_code_new_retail = isset($product_cart_data['store_code_new_retail']) ? $product_cart_data['store_code_new_retail'] : '';
        $product_details['name'] = $product_cart_data['name'];
        $product_details['is_extended'] = isset($product_cart_data['is_extended']) ? $product_cart_data['is_extended'] : 10;
        $product_details['status_retail'] = isset($product_cart_data['status_retail']) ? $product_cart_data['status_retail'] : 10;
        $product_details['url_key'] = $product_cart_data['url_key'];
        $product_details['is_in_stock'] = $product_cart_data['is_in_stock'];
        $product_details['id'] = (isset($product_cart_data['_id'])) ? (int)$product_cart_data['_id'] : 0;
        $product_details['supplier'] = (isset($product_cart_data['supplier'])) ? $product_cart_data['supplier'] : [];
        $product_details['brand_name'] = (isset($product_cart_data['brand'])) ? ((isset($product_cart_data['brand']['name'])) ? $product_cart_data['brand']['name'] : []) : [];
        $product_details['minimum_order'] = (isset($product_cart_data['minimum_order'])) ? $product_cart_data['minimum_order'] : 1;
        $product_details['is_apply_multiple'] = (isset($product_cart_data['is_apply_multiple'])) ? $product_cart_data['is_apply_multiple'] : 0;
        
        $product_details['variants'] = array();
        $product_details['categories_vendor'] = $product_cart_data['categories_vendor'];
        $dafaultFound = false;
        foreach ($product_cart_data['variants'] as $variant) {
            if ($variant['is_default'] == '1' || (!empty($sku) && $sku == $variant['sku'])) {
                if (!empty($sku) && $sku != $variant['sku']) {
                    continue;
                }

                $dafaultFound = true;
                $product_details['variants'][0]['sku'] = $variant['sku'];
                $product_details['variants'][0]['status_retail'] = isset($variant['status_retail']) ? $variant['status_retail'] : 10;
                $product_details['variants'][0]['is_in_stock'] = $variant['is_in_stock'];
                if (isset($variant['images'][0]['image_url'])) {
                    $product_details['variants'][0]['images'][0]['image_url'] = $variant['images'][0]['image_url'];
                } else {
                    $product_details['variants'][0]['images'][0]['image_url'] = !empty($store_code_new_retail) ? '/v1583812065/2.1/ace-retail-online/ace-logo-2.png' : '/v1525246613/2.1/svg/no-image.svg';
                }
                $product_details['variants'][0]['prices'][0]['price'] = $variant['prices'][0]['price'];
                $product_details['variants'][0]['prices'][0]['special_price'] = $variant['prices'][0]['special_price'];
                $product_details['variants'][0]['prices'][0]['special_from_date'] = $variant['prices'][0]['special_from_date'];
                $product_details['variants'][0]['prices'][0]['special_to_date'] = $variant['prices'][0]['special_to_date'];
                $product_details['variants'][0]['label'] = $variant['label'];

                break;
            }
        }

        if (!$dafaultFound) {
            $product_details['variants'][0]['sku'] = $product_cart_data['variants'][0]['sku'];
            $product_details['variants'][0]['is_in_stock'] = $product_cart_data['variants'][0]['is_in_stock'];
            $product_details['variants'][0]['status_retail'] = isset($product_cart_data['variants'][0]['status_retail']) ? $product_cart_data['variants'][0]['status_retail'] : 10;
            if (isset($product_cart_data['variants'][0]['images'][0]['image_url'])) {
                $product_details['variants'][0]['images'][0]['image_url'] = $product_cart_data['variants'][0]['images'][0]['image_url'];
            } else {
                $product_details['variants'][0]['images'][0]['image_url'] = !empty($store_code_new_retail) ? '/v1583812065/2.1/ace-retail-online/ace-logo-2.png' : '/v1525246613/2.1/svg/no-image.svg';
            }
            $product_details['variants'][0]['prices'][0]['price'] = $product_cart_data['variants'][0]['prices'][0]['price'];
            $product_details['variants'][0]['prices'][0]['special_price'] = $product_cart_data['variants'][0]['prices'][0]['special_price'];
            $product_details['variants'][0]['prices'][0]['special_from_date'] = $product_cart_data['variants'][0]['prices'][0]['special_from_date'];
            $product_details['variants'][0]['prices'][0]['special_to_date'] = $product_cart_data['variants'][0]['prices'][0]['special_to_date'];
            $product_details['variants'][0]['label'] = $product_cart_data['variants'][0]['label'];
        }

        return $product_details;
    }

    /**
     * @param string $sku
     * @return string
     * @throws \Library\HTTPException
     */
    public static function getSalesUom($sku = "")
    {
        $elasticLib = new \Library\Elastic();
        $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX');
        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE');

        // Get all data with max data is 50 and random all product to show
        $query = [
            "_source" => array("attributes"),
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "nested" => [
                                "path" => "variants",
                                "query" => [
                                    "term" => [
                                        "variants.sku.faceted" => $sku
                                    ]
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "status" => 10
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $elasticLib->body = $query;

        \Helpers\LogHelper::log('debug_625', '/app/helpers/ProductHelper.php line 786: '.$sku.'', 'debug');
        $elasticLib->search();
        \Helpers\LogHelper::log('debug_625', '/app/helpers/ProductHelper.php line 788: '.$sku.'', 'debug');

        $productData = array();
        if($elasticLib->getTotalData() > 0) {
            $productData = $elasticLib->getFormatedResult();
        }

        \Helpers\LogHelper::log('debug_625', '/app/helpers/ProductHelper.php line 793: '.$sku.'', 'debug');

        $sales_uom = "";
        if (!empty($productData)) {
            foreach ($productData['attributes'] as $attribute) {
                if($attribute['attribute_name'] == 'sales_uom') {
                    $sales_uom  = $attribute['attribute_value'];
                    break;
                }
            }

        }

        return $sales_uom;
    }

    public static function updateProductCustom($params, $table){
        $productModel = new Product();
        $productModel->useWriteConnection();
        $set = array();
        $condition = array();
        foreach($params as $key => $param){
            if($key=="product_variant_id"){
                $condition[] = " ".$key." = '".$param."'";
            }else {
                $set[] = " ".$key . " = '".$param."'";
            }
        }

        if(count($condition)>0) {
            $conditionString = implode(" AND ", $condition);
            $setString = implode(",", $set);
            $sql = "UPDATE " . $table . " SET " . $setString . " WHERE " . $conditionString;
            $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
            return $result->numRows();
        }else{
            return false;
        }

    }

    public static function getActiveProductPrice($price_info = [])
    {
        $date = date('Y-m-d H:i:s');

        $activePrice = $price_info['price'];
        if(isset($price_info['special_from_date']) && isset($price_info['special_to_date'])) {
            if ($price_info['special_from_date'] <= $date && $price_info['special_to_date'] >= $date) {
                if($price_info['special_price'] >= $price_info['price']){
                    $activePrice = $price_info['price'];
                }
                else{
                    $activePrice = $price_info['special_price'];
                }
            }
        }

        return $activePrice;
    }

    public static function updateIsInStockVariant($params)
    {
        $affectedRow = 0;
        $body = '{

                    "query": {
                        "bool": {
                        "filter": {
                            "nested": {
                            "path": "variants",
                            "query": {
                                "terms": {
                                "variants.sku.faceted": [' . $params['sku'] . ']
                                }
                            }
                            }
                        }
                        }
                    },
                    "script": {
                        "source": "int flag = -1;for (int index = 0; index < ctx._source.variants.length; index++) {ctx._source.variants[flag].is_in_stock.' . $params['company_code'] . ' = (params.is_in_stock);}",
                        "lang": "painless",
                        "params": {
                            "is_in_stock": ' . $params['is_in_stock'] . '                        
                        }
                    }
            }';

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/_update_by_query?refresh=true';
        if (!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }

        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);
        $affectedRow = $result['_shards']['successful'];
        
        return $affectedRow;
    }

    public static function updateIsInStockProduct($params)
    {
        $affectedRow = 0;
        $body = '{
                    "query": {
                        "bool": {
                        "filter": {
                            "nested": {
                            "path": "variants",
                            "query": {
                                "terms": {
                                "variants.sku.faceted": [' . $params['sku'] . ']
                                }
                            }
                            }
                        }
                        }
                    },
                    "script": {
                        "lang": "painless",
                        "inline": "ctx._source.is_in_stock.' . $params['company_code'] . ' = params.is_in_stock",
                        "params": {
                            "is_in_stock": ' . $params['is_in_stock'] . '
                        }
                        }
            }';

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/_update_by_query?refresh=true';
        if (!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }

        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);
        $affectedRow = $result['_shards']['successful'];
    

        return $affectedRow;
    }

    public static function updateIsInStock($params)
    {
        $id = self::getIdFromElastic(['sku' => $params['sku']]);
        $affectedRow = 0;
        if (!empty($id)) {
            $body = '{
                      "script": {
                        "lang": "painless",
                        "inline": "int flag = 0;int global_is_in_stock = 0;for (int index = 0; index < ctx._source.variants.length; index++) { if(ctx._source.variants[index].sku == (params.sku)) { flag = index } if(ctx._source.variants[index].is_in_stock == 1 && ctx._source.variants[index].sku != (params.sku)) { global_is_in_stock = 1} } ctx._source.variants[flag].is_in_stock = (params.is_in_stock); if((params.is_in_stock) == 1) { global_is_in_stock = 1 } ctx._source.is_in_stock = global_is_in_stock",
                        "params": {
                            "sku" : "' . $params['sku'] . '",
                            "is_in_stock" : ' . $params['is_in_stock'] . '
                        }
                      }
                }';

            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                '/' . $id .
                '/_update';
            if (!empty(getenv('HTTP_PROXY'))) {
                $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
            } else {
                $client = new \GuzzleHttp\Client();
            }

            $response = $client->post($elasticUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);

            $result = \GuzzleHttp\json_decode($response->getBody(), true);
            $affectedRow = $result['_shards']['successful'];
        }

        return $affectedRow;
    }

    public static function updateCategoryIsInStock($params)
    {
        $id = self::getIdFromElastic(['sku' => $params['sku']]);
        $affectedRow = 0;
        if (!empty($id)) {
            $body = '{
                      "script": {
                        "lang": "painless",
                        "inline": "int flag = -1;for (int index = 0; index < ctx._source.categories.length; index++) {if(ctx._source.categories[index].category_id == (params.category_id)) { flag = index } } ctx._source.categories[flag].'.$params['field'].' = (params.'.$params['field'].');",
                        "params": {
                            "category_id" : ' . $params['category_id'] . ',
                            "'.$params['field'].'" : ' . $params['value'] . '
                        }
                      }
                }';

            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                '/' . $id .
                '/_update';

            if (!empty(getenv('HTTP_PROXY'))) {
                $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
            } else {
                $client = new \GuzzleHttp\Client();
            }

            $response = $client->post($elasticUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);

            $result = \GuzzleHttp\json_decode($response->getBody(), true);
            $affectedRow = $result['_shards']['successful'];
        }

        return $affectedRow;
    }

    public static function getFlatProductData($sku = "", $load_category = false, $load_event = false)
    {
        $elasticLib = new \Library\Elastic();
        $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX');
        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE');

        // Get all data with max data is 50 and random all product to show
        $query = [
            'from' => 0,
            'size' => 50,
            "query" => [
                "bool" => [
                    "must" => [
                      [
                        "term" => [
                            "status" => 10
                        ]
                      ],
                      [
                        "nested" => [
                            "path" => "variants",
                            "query" => [
                                "match" => [
                                    "variants.sku" => $sku
                                ]
                            ]
                        ]
                      ]
                  ]
                ]
            ]
        ];

        $elasticLib->body = $query;

        $elasticLib->search();

        $productData = array();
        if($elasticLib->getTotalData() > 0) {
            $productData = $elasticLib->getFormatedResult();
        }

        $flatProductData = [];
        if (!empty($productData)) {
            if (isset($productData[0])) {
                $productData = $productData[0];
            }

            $flatProductData['sku'] = $sku;
            $flatProductData['name'] = $productData['name'];
            $flatProductData['shared_commision'] = $productData['shared_commision'];
            $flatProductData['packaging_uom'] = $productData['packaging_uom'];
            $flatProductData['packaging_height'] = $productData['packaging_height'];
            $flatProductData['packaging_length'] = $productData['packaging_length'];
            $flatProductData['packaging_width'] = $productData['packaging_width'];
            $flatProductData['weight'] = (float) $productData['weight'];
            $flatProductData['supplier_alias'] = $productData['supplier']['supplier_alias'];
            $flatProductData['qty_ordered'] = 1; // for event details, static only
            $flatProductData['url_key'] = $productData['url_key']; // Added url_key
            $flatProductData['brand'] = $productData['brand']['name'];            

            foreach ($productData['attributes'] as $attribute) {
                if($attribute['attribute_name'] == 'product_source' && !empty($attribute['attribute_value'])) {
                    $flatProductData['product_source'] = $attribute['attribute_option']['option_value'];
                    break;
                }
            }

            foreach ($productData['variants'] as $variant) {
                if ($variant['sku'] == $sku) {
                    $flatProductData['is_in_stock'] = $variant['is_in_stock'];
                    $flatProductData['label'] = (empty($variant['label']))?'': $variant['label'];
                    $flatProductData['ownfleet_template_id'] = !empty($variant['ownfleet_template_id'])? $variant['ownfleet_template_id'] : 0;

                    if(!empty($variant['images'])) {
                        foreach ($variant['images'] as $image) {
                            if ($image['angle'] == 1) {
                                $flatProductData['primary_image_url'] = $image['image_url'];
                                break;
                            }
                        }
                    } else {
                        $flatProductData['primary_image_url'] = "";
                    }

                    $flatProductData['price'] = (int) $variant['prices'][0]['price'];

                    if(!empty($variant['prices'][0]['special_price'])){
                        $flatProductData['special_price'] = (int) $variant['prices'][0]['special_price'];
                        $flatProductData['special_from_date'] = $variant['prices'][0]['special_from_date'];
                        $flatProductData['special_to_date'] = $variant['prices'][0]['special_to_date'];
                    } else{
                        $flatProductData['special_price'] = 0;
                        $flatProductData['special_from_date'] = "";
                        $flatProductData['special_to_date'] = "";
                    }

                    $attributes = [];
                    if (!empty($variant['attributes'])) {

                        foreach ($variant['attributes'] as $attribute) {
                            $attribute = [
                                "attribute_id" => $attribute['attribute_id'],
                                "attribute_label" => $attribute['attribute_label'],
                                "attribute_name" => $attribute['attribute_name'],
                                "attribute_value" => !empty($attribute['attribute_value']) ? (!empty($attribute['attribute_option']['option_value']) ? $attribute['attribute_option']['option_value'] : '' ): '',
                                "extra_information" => !empty($attribute['attribute_option']['extra_information']) ? $attribute['attribute_option']['extra_information'] : ''
                            ];
                            $attributes[] = $attribute;
                        }

                    }
                    $flatProductData['attributes'] = $attributes;

                    $flatProductData['express_courier'] = array();
                    if (isset($variant['can_gosend'])) {
                        if (count($variant['can_gosend']) > 0) {
                            $gosendService = array();
                            foreach($variant['can_gosend'] as $rowGosend) {
                                $gosendService[] = $rowGosend['delivery_method'];
                            }

                            $gosendServiceText = '';
                            if (count($gosendService) > 0) {
                                if (count($gosendService) > 1) {
                                    $gosendServiceText = 'all_service';
                                } else {
                                    $gosendServiceText = $gosendService[0];
                                }
                            }

                            $flatProductData['express_courier']['can_gosend'] = $gosendServiceText;
                        }
                    }
                }
            }

            if($load_category) {
                $categoriesHCI = !empty($productData['categories_HCI']) && isset($productData['categories_HCI']) ? $productData['categories_HCI'] : [];
                $categoriesAHI = !empty($productData['categories_AHI']) && isset($productData['categories_AHI']) ? $productData['categories_AHI'] : [];
                $flatProductData['category'] = array_merge($productData['categories'], $categoriesHCI, $categoriesAHI);
            }

            if($load_event) {
                $flatProductData['event'] = $productData['events'];
            }
        }

        return $flatProductData;
    }

    public static function getFlatProductDetail($sku = "", $load_category = false, $load_event = false)
    {
        $elasticLib = new \Library\Elastic();
        $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX');
        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE');

        // Get all data with max data is 50 and random all product to show
        $query = [
            'from' => 0,
            'size' => 50,
            "query" => [
                "bool" => [
                    "must" => [
                      [
                        "term" => [
                            "status" => 10
                        ]
                      ],
                      [
                        "nested" => [
                            "path" => "variants",
                            "query" => [
                                "match" => [
                                    "variants.sku" => $sku
                                ]
                            ]
                        ]
                      ]
                  ]
                ]
            ]
        ];

        $elasticLib->body = $query;

        $elasticLib->search();

        $productData = array();
        if($elasticLib->getTotalData() > 0) {
            $productData = $elasticLib->getFormatedResult();
        }

        $flatProductData = [];
        if (!empty($productData)) {
            if (isset($productData[0])) {
                $productData = $productData[0];
            }

            $flatProductData['sku'] = $sku;
            $flatProductData['name'] = $productData['name'];
            $flatProductData['shared_commision'] = $productData['shared_commision'];
            $flatProductData['packaging_uom'] = $productData['packaging_uom'];
            $flatProductData['packaging_height'] = $productData['packaging_height'];
            $flatProductData['packaging_length'] = $productData['packaging_length'];
            $flatProductData['packaging_width'] = $productData['packaging_width'];
            $flatProductData['weight'] = (float) $productData['weight'];
            $flatProductData['supplier_alias'] = $productData['supplier']['supplier_alias'];
            $flatProductData['qty_ordered'] = 1; // for event details, static only
            $flatProductData['url_key'] = $productData['url_key']; // Added url_key
            $flatProductData['warranty'] = $productData['warranty']; // Added url_key
            $flatProductData['brand'] = $productData['brand']['name'];

            foreach ($productData['attributes'] as $attribute) {
                if($attribute['attribute_name'] == 'product_source' && !empty($attribute['attribute_value'])) {
                    $flatProductData['product_source'] = $attribute['attribute_option']['option_value'];
                    break;
                }
            }

            foreach ($productData['variants'] as $variant) {
                if ($variant['sku'] == $sku) {
                    $flatProductData['is_in_stock'] = $variant['is_in_stock'];
                    $flatProductData['label'] = (empty($variant['label']))?'': $variant['label'];
                    $flatProductData['ownfleet_template_id'] = !empty($variant['ownfleet_template_id'])? $variant['ownfleet_template_id'] : 0;

                    if(!empty($variant['images'])) {
                        foreach ($variant['images'] as $image) {
                            if ($image['angle'] == 1) {
                                $flatProductData['primary_image_url'] = $image['image_url'];
                                break;
                            }
                        }
                    } else {
                        $flatProductData['primary_image_url'] = "";
                    }

                    $flatProductData['price'] = (int) $variant['prices'][0]['price'];

                    if(!empty($variant['prices'][0]['special_price'])){
                        $flatProductData['special_price'] = (int) $variant['prices'][0]['special_price'];
                        $flatProductData['special_from_date'] = $variant['prices'][0]['special_from_date'];
                        $flatProductData['special_to_date'] = $variant['prices'][0]['special_to_date'];
                    }
                    else{
                        $flatProductData['special_price'] = 0;
                        $flatProductData['special_from_date'] = "";
                        $flatProductData['special_to_date'] = "";
                    }

                    $attributes = [];
                    if (!empty($variant['attributes'])) {

                        foreach ($variant['attributes'] as $attribute) {
                            $attribute = [
                                "attribute_id" => $attribute['attribute_id'],
                                "attribute_label" => $attribute['attribute_label'],
                                "attribute_name" => $attribute['attribute_name'],
                                "attribute_value" => !empty($attribute['attribute_value']) ? (!empty($attribute['attribute_option']['option_value']) ? $attribute['attribute_option']['option_value'] : '' ): '',
                                "extra_information" => !empty($attribute['attribute_option']['extra_information']) ? $attribute['attribute_option']['extra_information'] : ''
                            ];
                            $attributes[] = $attribute;
                        }

                    }
                    $flatProductData['attributes'] = $attributes;

                    if(!empty($variant['attribute_sap'])) {
                        $attributeSap['ah_sisco'] = $variant['attribute_sap']['ah_sisco'];

                        $flatProductData['attribute_sap'] = $attributeSap;
                    }
                }
            }

            if($load_category) {
                $categoriesHCI = !empty($productData['categories_HCI']) && isset($productData['categories_HCI']) ? $productData['categories_HCI'] : [];
                $categoriesAHI = !empty($productData['categories_AHI']) && isset($productData['categories_AHI']) ? $productData['categories_AHI'] : [];
                $flatProductData['category'] = array_merge($productData['categories'], $categoriesHCI, $categoriesAHI);
            }

            if($load_event) {
                $flatProductData['event'] = $productData['events'];
            }
        }

        return $flatProductData;
    }

    public static function updateProductLabel($sku = "", $labelParams = "")
    {
        if (empty($labelParams)) {
            return 0;
        }

        $affectedRow = 0;
        $body = '{
                    "query": {
                        "nested": {
                            "path": "variants",
                            "query": {
                                "terms": {
                                    "variants.sku.faceted": [
                                        "'.$sku.'"
                                    ]
                                }
                            }
                        }
                    },
                    "script": {
                        "lang": "painless",
                        "source": "for(int a= 0; a < ctx._source.variants.length; a++){ctx._source.variants[a].label = (params.label)}",
                        "params": {
                            "label": {
                                '.$labelParams.'
                            }
                        }
                    }
            }';

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/_update_by_query?refresh=true';
        if (!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }

        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);
        $affectedRow = $result['_shards']['successful'];
        
        return $affectedRow;
    }

    public static function getVariantsFromElastic($sku) {
        $body = '
        {
            "query": {
                "bool": {
                    "must": [{
                        "nested": {
                            "path": "variants",
                            "query": {
                                "terms": {
                                    "variants.sku.faceted": ["'. $sku . '"]
                                }
                            }
                        }
                    }]
                }
            }
        }';

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/_search';
        if(!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }
        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);

        $valueField = '';
        foreach ($result['hits']['hits'] as $id => $value) {
            $valueField = $value['_source']['variants'];
        }

        return $valueField;
    }

    public static function getAttributeFromElastic($sku) {
        $body = '
        {
            "_source":["attributes"],
            "query": {
                "bool": {
                    "must": [{
                        "nested": {
                            "path": "variants",
                            "query": {
                                "terms": {
                                    "variants.sku.faceted": ["'. $sku . '"]
                                }
                            }
                        }
                    }]
                }
            }
        }';

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/_search';
        if(!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }
        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);

        $valueField = '';
        foreach ($result['hits']['hits'] as $id => $value) {
            $valueField = $value['_source']['attributes'];
        }

        return $valueField;
    }

    public static function getProductDepartment($sku = "") {
        $elasticLib = new \Library\Elastic();
        $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX');
        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE');
        // Get all data with max data is 50 and random all product to show
        $query = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "nested" => [
                                "path" => "variants",
                                "query" => [
                                    "match" => [
                                        "variants.sku" => $sku
                                    ]
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "status" => 10
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $elasticLib->body = $query;
        $elasticLib->search();
        $productData = array();
        if($elasticLib->getTotalData() > 0) {
            $productData = $elasticLib->getFormatedResult();
        }
        $departmentData = array();
        if (!empty($productData)) {
            foreach ($productData["variants"] as $prodVar) {
                if ($prodVar["sku"] == $sku) {
                    if ($productData["is_extended"] == 10) {
                        $departmentData["dept"] = $prodVar["attribute_sap"]["department_sisco"];
                        $departmentData["dept_name"] = $prodVar["attribute_sap"]["department_sisco_desc"];
                    } else {
                        $departmentData["dept"] = $prodVar["attribute_sap"]["product_group_retail"];
                        $departmentData["dept_name"] = "";
                    }
                    break;
                }
            }
        }
        return $departmentData;
    }

    public static function isApplyMdrOnCust ($sku = "") {
        $itemDetail = self::getFlatProductDetail($sku);
        if (!empty($itemDetail['attribute_sap']) && !empty($itemDetail['attribute_sap']['ah_sisco'])) {
            // If is tv sku, apply mdr
            if (strpos($itemDetail['attribute_sap']['ah_sisco'], getenv('AH_SISCO_TV_PREFIX')) !== false) {
                return true;
            }
        }
        return false;
    }
}