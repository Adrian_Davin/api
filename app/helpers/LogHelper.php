<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/17/2017
 * Time: 11:11 AM
 */

namespace Helpers;

use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;

class LogHelper
{
     public static function log($filename = "error", $message = "", $error_type = "")
    {
        $logger = new Logger("");
        // $stream = new StreamHandler(VAR_PATH."/logs/".$filename.".log", Logger::DEBUG);
        $stream = new StreamHandler($_ENV['LOG_PATH'].$filename.".log", Logger::DEBUG);
        $dateFormat = "Y-m-d H:i:s";
        $output = "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n";
        $formatter = new \Monolog\Formatter\LineFormatter($output, $dateFormat, true,true);
        $stream->setFormatter($formatter);
        $logger->pushHandler($stream);

        //$logger = new FileAdapter(VAR_PATH."/logs/".$filename.".log");
        switch ($error_type) {
            case "info" : $logger->info($message);break;
            case "debug" : $logger->debug($message);break;
            case "notice" : $logger->notice($message);break;
            case "warning" : $logger->warning($message);break;
            case "critical" : $logger->critical($message);break;
            case "alert" : $logger->alert($message);break;
            case "emergency" : $logger->emergency($message);break;
            case "error" :
            default : $logger->error($message);break;
        }

    }

    public static function trapLog($filename = "error", $message = "", $error_type = "") {
        if (getenv('ENABLE_TRAP_LOG') == "ON") {
            self::log($filename, $message, $error_type);
        }
    }
}