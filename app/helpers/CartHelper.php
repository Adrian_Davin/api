<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/27/2016
 * Time: 10:25 AM
 */

namespace Helpers;

use \Library\Marketing as MarketingLib;

class CartHelper
{
    /**
     * count weight and volumetric weight of item(s)
     * updated at 2018-08-07 by hendy@ruparupa.com and monica@ruparupa.com
     * to change calculation order
     * valid from cut off date 2018-09-01 00:00:00
     * (1) compare weight vs volumetric per individual line item
     * (2) aggregate total max between those two
     * @param $items
     * @return array
     */
    public static function countWeight($items)
    {
        $totalShippingWeight = 0;
        $totalActualWeight = 0;
        $multipliers = [
            'mm'    => 0.1,
            'cm'    => 1,
            'm'     => 100
        ];

        foreach ($items as $item) {
            $uom = strtolower($item['packaging_uom']);
            $multiplyFactor = isset($multipliers[$uom]) ? $multipliers[$uom] : 1;

            $volume = ($item['packaging_height'] * $multiplyFactor) *
                ($item['packaging_length'] * $multiplyFactor) *
                ($item['packaging_width'] * $multiplyFactor);

            $volumetricWeight = ($volume / 6000);

            // (1) compare weight vs volumetric per individual line item
            $shippingWeight =
                max($volumetricWeight, $item['weight']) > 0
                ? max($volumetricWeight, $item['weight'])
                : 1;

            // (2) aggregate total max between those two
            $totalShippingWeight += $shippingWeight * $item['qty_ordered'];

            $totalActualWeight += $item['weight'] * $item['qty_ordered'];
        }

        $weight = [
            "original" => intval(ceil($totalActualWeight)),
            "shipping" => intval(ceil($totalShippingWeight)),
            "original_no_ceil" => $totalActualWeight, // needed for distribute shipping_amount
            "shipping_no_ceil" => $totalShippingWeight
        ];

        return $weight;
    }

    public static function countShippingCost($weight = 0, $shipping_price = 0)
    {
        // Count shipping cost
        $x3 = $weight * $shipping_price;

        return $x3;
    }

    public static function countDiscountAmountUsed($amount = 0, $origin_discount_amount = 0)
    {
        $amountAfterDiscount = $amount - $origin_discount_amount;
        if ($amountAfterDiscount > 0) {
            $discountAmountUsed = $origin_discount_amount;
        } else {
            $discountAmountUsed = $amount;
        }

        return $discountAmountUsed;
    }

    public static function checkRuleIsExist($rule_list = array(), $rule = array())
    {
        foreach ($rule_list as $thisRule) {
            if ($thisRule['rule_id'] == $rule['rule_id']) {
                return true;
            }
        }

        return false;
    }

    /**
     * Remove certain attribute from condition
     *
     * @param array $conditions
     * @param string $attribute
     * @return array
     */
    public static function removeAtributeCondition($conditions = [], $attribute = 'items')
    {

        foreach ($conditions as $keyConditon => $valCondition) {
            $discountConditionAttribute = explode(".", $valCondition['attribute']);

            if ($discountConditionAttribute[0] == $attribute) {
                unset($discountConditionAttribute[0]);
            }

            $newAttribute = implode(".", $discountConditionAttribute);

            $valCondition['attribute'] = $newAttribute;
            $conditions[$keyConditon] = $valCondition;

            if (isset($valCondition['conditions'])) {
                $conditions[$keyConditon]['conditions'] = self::removeAtributeCondition($valCondition['conditions'], $attribute);
            }
        }

        return $conditions;
    }

    public static function keepAttributeConditions($conditions = [], $attribute = 'items')
    {

        foreach ($conditions as $keyConditon => $valCondition) {
            $discountConditionAttribute = explode(".", $valCondition['attribute']);

            if ($discountConditionAttribute[0] != $attribute) {
                unset($conditions[$keyConditon]);
            }

            if (isset($valCondition['conditions'])) {
                $childConditon = self::keepAttributeConditions($valCondition['conditions'], $attribute);
                if (isset($conditions[$keyConditon])) {
                    $conditions[$keyConditon]['conditions'] = $childConditon;
                } else {
                    $conditions = array_merge($childConditon, $conditions);
                }
            }
        }


        return $conditions;
    }

    public static function removeConditionBasedOnAttribute($conditions = [], $attribute = 'items')
    {
        foreach ($conditions as $keyConditon => $valCondition) {
            $discountConditionAttribute = explode(".", $valCondition['attribute']);

            if ($discountConditionAttribute[0] == $attribute) {
                unset($conditions[$keyConditon]);

                /**
                 * If this is first key, remove aggregator for next condition
                 */
                if ($keyConditon == 0) {
                    $i = 0;
                    foreach ($conditions as $thisKey => $thisVal) {
                        if ($thisKey == $keyConditon) {
                            continue;
                        }

                        if ($i == 1) {
                            break;
                        }

                        unset($conditions[$thisKey]['aggregator']);
                        $i++;
                    }
                }
            }

            if (isset($valCondition['conditions'])) {
                $childConditon = self::removeConditionBasedOnAttribute($valCondition['conditions'], $attribute);
                if (isset($conditions[$keyConditon])) {
                    $conditions[$keyConditon]['conditions'] = $childConditon;
                } else {
                    $conditions = $childConditon;
                }
            }
        }

        return $conditions;
    }

    public static function isGosend($carrierID)
    {
        $gosendGroup = array(getenv('GOSEND_INSTANT_CARRIER_ID'), getenv('GOSEND_SAMEDAY_CARRIER_ID'));
        if (in_array($carrierID, $gosendGroup)) {
            return true;
        }

        return false;
    }

    public static function maxWeightGosend($carrierID, $qtyOrder)
    {
        $maxWeight = 5.25;
        if ($carrierID == getenv('GOSEND_INSTANT_CARRIER_ID')) {
            switch ($qtyOrder) {
                case 1:
                    $maxWeight = 20;
                    break;
                case 2:
                    $maxWeight = 17.5;
                    break;
                default:
                    $maxWeight = 15;
            }
        } elseif ($carrierID == getenv('GOSEND_SAMEDAY_CARRIER_ID')) {
            switch ($qtyOrder) {
                case 1:
                    $maxWeight = 5;
                    break;
                case 2:
                    $maxWeight = 4.3;
                    break;
                default:
                    $maxWeight = 3.75;
            }
        }

        return $maxWeight;
    }

    public static function maxVolumeGosend($carrierID, $qtyOrder)
    {
        $maxVolume = 11475;
        if ($carrierID == getenv('GOSEND_INSTANT_CARRIER_ID')) {
            switch ($qtyOrder) {
                case 1:
                    $maxVolume = 175000;
                    break;
                case 2:
                    $maxVolume = 117236;
                    break;
                default:
                    $maxVolume = 73828;
            }
        } elseif ($carrierID == getenv('GOSEND_SAMEDAY_CARRIER_ID')) {
            switch ($qtyOrder) {
                case 1:
                    $maxVolume = 27200;
                    break;
                case 2:
                    $maxVolume = 18222;
                    break;
                default:
                    $maxVolume = 11475;
            }
        }

        return $maxVolume;
    }

    // quick fix for prorate in level item
    public static function prorateLevelItems($items = [], $item = [], $giftcards = '', $grandTotal)
    {
        $prorate = 0;
        $giftcard = json_decode($giftcards, true);
        $giftcardItem = json_decode($item['gift_cards'], true);
        $eligible = false;
        foreach ($giftcard as $voucherApplied) {
            foreach ($giftcardItem as $voucherEligible) {
                if ($voucherApplied['voucher_code'] == $voucherEligible['voucher_code']) {
                    $eligible = true;
                }
            }
        }
        if (!$eligible) {
            return $prorate;
        }
        foreach ($giftcard as $row) {
            $grandRowTotal = 0;

            // for refund only
            $marketingLib = new MarketingLib();
            $voucherDetail = $marketingLib->getVoucherDetail($row['voucher_code'], 'ODI');
            $ruleID = $voucherDetail['rules']['rule_id'];
            $refundVoucher = explode(',', getenv('REFUND_RULE_ID'));
            $isRefund = false;
            if (in_array($ruleID, $refundVoucher)) {
                $isRefund = true;
            }

            if ($row['voucher_affected'] == 'item' || $row['voucher_affected'] == 'global') {
                $doProrate = false;
                foreach ($items as $itemx) {
                    $giftcardx = json_decode($itemx['gift_cards'], true);
                    if (!empty($giftcardx)) {
                        foreach ($giftcardx as $rowx) {
                            if ($row['voucher_code'] == $rowx['voucher_code']) {
                                if ($isRefund) {
                                    $grandRowTotal += $itemx['row_total'] + $itemx['shipping_amount'] - $itemx['shipping_discount_amount'];
                                } else {
                                    $grandRowTotal += $itemx['row_total'];
                                }
                                $doProrate = true;
                                break;
                            }
                        }
                    }
                }
                if ($row['voucher_amount_used'] > $grandTotal && $isRefund) {
                    $row['voucher_amount_used'] = $grandTotal;
                }
                if ($doProrate) {
                    if ($isRefund) {
                        $prorate += round((($item['row_total'] + $item['shipping_amount'] - $item['shipping_discount_amount']) / $grandRowTotal) * $row['voucher_amount_used'] / $item['qty_ordered']);
                    } else {
                        $prorate += round(($item['row_total'] / $grandRowTotal) * $row['voucher_amount_used'] / $item['qty_ordered']);
                    }
                }
            }
        }
        return $prorate;
    }

    // quick fix for prorate in level item
    public static function prorateLevelItemsDiscount($items = [], $item = [], $cartrules = '', $grandTotal)
    {
        $prorate = 0;
        $cartrule = json_decode($cartrules, true);
        if (trim($item['cart_rules']) == "") {
            $item['cart_rules'] = json_encode(array());
        }
        $cartruleItem = json_decode($item['cart_rules'], true);
        $eligible = false;
        foreach ($cartrule as $ruleApplied) {
            // if the action marketing is bogo, skip the prorate
            if ($ruleApplied['action']['applied_action'] == "get_item_discount") {
                continue;
            }

            // if the action didnt cut anything skip the prorate
            if ($ruleApplied['action']['discount_amount'] == "0.00" || $ruleApplied['action']['discount_amount'] < 1) {
                continue;
            }

            foreach ($cartruleItem as $ruleEligible) {
                if ($ruleApplied['rule_id'] == $ruleEligible['rule_id']) {
                    $eligible = true;
                }
            }
        }
        if (!$eligible) {
            return $prorate;
        }
        foreach ($cartruleItem as $row) {
            // if the action marketing is bogo, skip the prorate
            if ($row['action']['applied_action'] == "get_item_discount") {
                continue;
            }

            // if the action didnt cut anything skip the prorate
            if ($row['action']['discount_amount'] == "0.00" || $row['action']['discount_amount'] < 1) {
                continue;
            }

            $grandRowTotal = 0;
            //if($row['voucher_affected']=='item' || $row['voucher_affected']=='global'){ 
            $doProrate = false;
            foreach ($items as $itemx) {
                if (trim($itemx['cart_rules']) == "") {
                    $itemx['cart_rules'] = json_encode(array());
                }
                $cartrulex = json_decode($itemx['cart_rules'], true);
                if (!empty($cartrulex)) {
                    foreach ($cartrulex as $rowx) {
                        if ($row['rule_id'] == $rowx['rule_id']) {
                            $grandRowTotal += $itemx['row_total'] + ($itemx['discount_amount'] * $itemx['qty_ordered']);
                            $doProrate = true;
                            break;
                        }
                    }
                }
            }

            if ($row['action']['discount_type'] == 'percent') {
                $row['action']['discount_amount'] = ($grandRowTotal * $row['action']['discount_amount']) / 100;
                if ($row['action']['discount_amount'] > $row['action']['max_redemption']) {
                    $row['action']['discount_amount'] = $row['action']['max_redemption'];
                }
            }
            if ($doProrate) {
                $rowTotalWithoutDiscount = $item['row_total'] + ($item['discount_amount'] * $item['qty_ordered']);
                $prorate += round(($rowTotalWithoutDiscount / $grandRowTotal) * $row['action']['discount_amount'] / $item['qty_ordered']);
            }
            //}
        }
        return $prorate;
    }

    public static function countTotalPackagingWeight($items){
        $totalShippingWeight = 0;
        foreach($items as $item) {
            $totalShippingWeight += $item['packaging_weight'];
        }

        $weight = [
            "shipping" => ceil($totalShippingWeight),
        ];

        return $weight;
    }

}
