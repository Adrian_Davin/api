<?php
/**
 * Helper for you parsing anything
 */

namespace Helpers;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

class ParserHelper extends \Phalcon\DI\Injectable
{
    /**
     * Parse query string from any HTTP request method to one param array
     * @var $request object
     * @return array
     * @throws \Library\HTTPException
     */
    public static function parseRequest($request)
    {
        $httpMetthod = $request->getMethod();

        // populate get param
        $getParam = $request->get();
        $requestInfo = $httpMetthod .' '. $getParam["_url"];
        $requestInfoTemp = $httpMetthod .' '. $getParam["_url"];
        if (isset($_SERVER['HTTP_REFERER'])) {
            $requestInfo .= ' '. $_SERVER['HTTP_REFERER'];
        }
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $requestInfo .= ' '. $_SERVER['REMOTE_ADDR'];
        }
        unset($getParam["_url"]);

        $otherParams = array();
        // If get or put, we need to call param from URI
        if(strtoupper($httpMetthod) != 'GET' || (strtoupper($httpMetthod) != 'PUT'))  {
            $getParam = array_merge($getParam, $request->getDi()->getRouter()->getParams());
        }

        // Remove .html from params
        $getParam = self::_cleanPrefix($getParam);

        // Other than get, we need to get parameter from body
        if(strtoupper($httpMetthod) != 'GET') {
            $jsonRaw = $request->getRawBody();
            $raw_data = null;
            if(!empty($jsonRaw)) {
                if(!self::isJSON($jsonRaw)) {
                    throw new \Library\HTTPException(
                        "Wrong JSON format",
                        400,
                        array(
                            'msgTitle' => 'JSON Request not valid',
                            'dev' => "JSON Request not valid, json data : " . $jsonRaw
                        )
                    );
                } else {
                    $raw_data = $request->getJsonRawBody (true);
                }
            }

            // If data is empty set empty array
            if($raw_data == null) {
                $otherParams = array();
            } else if (!empty($raw_data['data'])) {
                $otherParams = $raw_data['data'];
            }
        }

        // Now we get param from header
        $headerParams = array();
        $logParamsFromApp = 0;

        foreach($request->getHeaders() as $keyHeader => $valHeader) {
            preg_match("/^rr-(.+)/", strtolower($keyHeader), $result);
            if(!empty($result)) {
                $keyArray =  str_replace("-","_",$result[1]);
                $headerParams[$keyArray] = $valHeader;
            } else {
                // log if there are app-id or user-email header
                if (strtolower($keyHeader) == "app-id" || strtolower($keyHeader) == "user-email") {
                    $logParamsFromApp = 1;
                    $headerParams[strtolower($keyHeader)] = $valHeader;
                }
                if (strtolower($keyHeader) == "x-app-version") {
                    $headerParams[strtolower($keyHeader)] = $valHeader;
                }
            }
        }

        // merge anything
        $allParams = array_merge($getParam, $otherParams, $headerParams);
        $allParamsTemp = $allParams;

        // interupt when batch api found (prevent the creation of large size file log)
        if (isset($allParams[0]['batch'])) {
            $allParams = array('batch' => $allParams[0]['batch']);
        }

        /**
         * Remove password from fields log
         */
        if(isset($allParams['password'])) {
            $allParams['password'] = null;
        }

        if(isset($allParams['"confirm_password"'])) {
            $allParams['"confirm_password"'] = null;
        }

        if (getenv('ENVIRONMENT') != 'production' || $logParamsFromApp > 0) {
            // we need to log for
            $logger = new Logger("Request Params ");
            $logger->pushHandler(new StreamHandler($_ENV['LOG_PATH']."requestParams.log", Logger::INFO ));
            $logdata = json_encode($allParams);
            $filter_pattern = '/\"password\":\"(.*)\"}/';
            preg_match($filter_pattern, $logdata, $filter);
            if (isset($filter[1])) {
                $logdata = str_replace($filter[1],'...',$logdata);
            }
            $logger->addInfo("$requestInfo $logdata");
        }

        $search = 'PUT\ \/shopping\/cart\/';
        if(preg_match("/{$search}/i", $requestInfoTemp)) {
            $logger = new Logger("Request Params ");
            $logger->pushHandler(new StreamHandler($_ENV['LOG_PATH']."requestParamsShoppingCart.log", Logger::INFO ));
            $logdata = json_encode($allParams);
            $logger->addInfo("$requestInfoTemp $logdata");
        }

        // remove app-id and user-email so it can't be used in controllers
        unset($allParamsTemp['app-id']);
        unset($allParamsTemp['user-email']);
        return $allParamsTemp;
    }

    private static function _cleanPrefix($params = array())
    {
        if(empty($params)) {
            return $params;
        }

        $clean_result = array();
        foreach ($params as $key => $val) {
            if(is_scalar($val)) {
                $clean_result[$key] = preg_replace('/\.[html]+$/', '', $val);
            } else {
                $clean_result[$key] = self::_cleanPrefix($val);
            }
        }

        return $clean_result;
    }

    public static function isJSON($string){
       return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    /**
     * Generate XML from array.
     * This function will not use int as key and will use parent key if posible or use default key item+$key
     * @param $xml_data
     * @param $data
     * @param string $parent_key
     */
    public static function arrayToXml( &$xml_data, $data, $parent_key = "" )
    {
        foreach ($data as $key => $value) {
            $this_key = $key;
            if (is_array($value)) {
                if (is_numeric($key) && empty($parent_key)) {
                    $key = "item".$key; //dealing with <0/>..<n/> issues
                } else if (!empty($parent_key)) {
                    $key = $parent_key;
                }

                if(!is_numeric(key($value))) {
                    $subnode = $xml_data->addChild($key);
                } else {
                    $subnode = $xml_data;
                }

                self::arrayToXml($subnode, $value, $this_key);

            } else {
                $xml_data->addChild("$key", htmlspecialchars("$value"));
            }

        }
    }

    /**
     * Parse HTML tags and their attributes to array
     * @param string $string_to_parse string containing html tags to be parsed
     * @param array $allowed_tags allowed tags to be parsed
     */
    public static function parseHTMLTagsToArray ($string_to_parse, $allowed_tags=array('meta','link','script', 'head')) {
        $result = array();
        $additional_header = array();

        preg_match_all('/<([\w]*).*>/', $string_to_parse, $result);

        foreach ($result[1] as $index => $tag) {
            if (in_array($tag,$allowed_tags)) {
                // get attributes
                preg_match_all('/([\w]*)=\"([^"]*)\"/', $result[0][$index], $attributes_extract);

                $attributes = array();

                foreach ($attributes_extract[0] as $index => $attribute) {
                    $attributes[] = array($attributes_extract[1][$index] => $attributes_extract[2][$index]);
                }

                $additional_header[] = array(
                    'tag' => $tag,
                    'attributes' => $attributes
                );

            }

        }

        return count($additional_header) ? $additional_header : null;

    }
}