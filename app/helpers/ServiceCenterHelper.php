<?php

/**
 * Created for ruparupa by.
 * User: Reza Fiansyah Putra
 * Date: 09/07/2021
 * Time: 13:45 PM
 */

namespace Helpers;

use Models\CustomerAceOrderWarranty;
use Models\SalesInvoice;
use Models\SalesInvoiceItem;

class ServiceCenterHelper
{
    public static function multiplyWarrantyAvailabilitySKUDataPerQuantity($dataToMultiply, $multiplication)
    {
        $i = 0;
        $results = [];
        while ($i < $multiplication) {
            array_push($results, $dataToMultiply);
            $i++;
        }

        return $results;
    }

    public static function generateWarrantyCodeODI()
    {
        $nanotime = exec('date +%s%N');
        return "ODIAW" . substr(number_format($nanotime * rand(), 0, '', ''), 0, 9);
    }
}
