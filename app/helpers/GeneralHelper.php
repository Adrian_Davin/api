<?php

namespace helpers;

class GeneralHelper
{
    /**
     * @param $needle
     * @param $haystack
     * @param bool $strict
     * @param bool $returnKey
     * @return bool
     */
    public static function in_array_r($needle, $haystack, $strict = false, $returnKey = false)
    {
        foreach ($haystack as $key => $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && GeneralHelper::in_array_r($needle, $item, $strict))) {
                if ($returnKey == true) {
                    return array('key' => $key, 'item' => $item);
                } else {
                    return $item;
                }
            }
        }
        return false;
    }

    public static function array_unique_by_key($key = "", $haystack = array())
    {
        $temp_array = array();
        if (empty($key) || empty($haystack)) {
            return false;
        } else {
            if (is_array($haystack)) {
                foreach ($haystack as &$item) {
                    if (!isset($temp_array[$item[$key]]))
                        $temp_array[$item[$key]] = &$item;
                }
            }
            return array_values($temp_array);
        }
    }

    public static function array_sort_by_created_at($arrayToSort = [], $order = "ASC")
    {
        if ($order == "ASC") {
            usort($arrayToSort, function ($elementX, $elementY) {
                $datetime1 = strtotime($elementX['created_at']);
                $datetime2 = strtotime($elementY['created_at']);

                return $datetime1 >= $datetime2;
            });
        } else if ($order == "DESC") {
            usort($arrayToSort, function ($elementX, $elementY) {
                $datetime1 = strtotime($elementX['created_at']);
                $datetime2 = strtotime($elementY['created_at']);

                return $datetime1 <= $datetime2;
            });
        }

        return $arrayToSort;
    }

    public static function checkActiveBetweenDate($date_from = '', $date_to = '')
    {
        $currentTime = strtotime(date('Y-m-d H:i:s'));

        // if this is not the time to get this promotion skip it
        if (strtotime($date_from) < $currentTime && $currentTime > strtotime($date_to)) {
            return false;
        }

        return true;
    }

    public static function iscurrentTimeActive($date_from = '', $date_to = '')
    {
        $currentTime = strtotime(date('Y-m-d H:i:s'));
        if ($currentTime > strtotime($date_from) && $currentTime < strtotime($date_to)) {
            return true;
        }

        return false;
    }

    public static function getTemplateId($templateCode = '', $companyCode = '')
    {
        $templateId = '';
        if (!empty($templateCode)) {
            $masterEmailTemplateModel = new \Models\MasterEmailTemplate();
            $templateData = $masterEmailTemplateModel::findFirst(["template_code = '" . $templateCode . "' AND company_code = '" . $companyCode . "'"]);
            if ($templateData) {
                return $templateData->getTemplateId();
            }
        }

        return $templateId;
    }

    /**
     * @param number $length (length number to generate)
     * @return string $randomString (generated number)
     */
    public static function generateRandomNumber($length = 6)
    {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    /**
     * @param string $phone (phone number with 08 format)
     * @return string $formattedNumber (converted to +628)
     */
    public static function formatPhoneNumber($phone)
    {
        if (substr($phone, 0, 2) == "08") {
            return substr_replace($phone, "+628", 0, 2);
        }

        return false;
    }

    /**
     * @param string $phone (phone number with +628)
     * @return string $formattedNumber (converted to 08)
     */
    public static function formatNormalPhoneNumber($phone)
    {
        if (substr($phone, 0, 4) == "+628") {
            return substr_replace($phone, "08", 0, 4);
        }

        return false;
    }

    /**
     * @param string $phone (phone number)
     * @return string $formattedNumber (converted to +628)
     */
    public static function formatPhoneNumberToValidNumberForWhatsapp($phone)
    {
        $formattedWhatsappPhoneNumber = "";

        if (substr($phone, 0, 2) == "08") {
            $formattedWhatsappPhoneNumber = GeneralHelper::formatPhoneNumber($phone);
        } else if (substr($phone, 0, 3) == "628" || substr($phone, 0, 4) == "+628") {
            $formattedWhatsappPhoneNumber = $phone;
        }

        return $formattedWhatsappPhoneNumber;
    }

    public static function formatNumberToRupiah($number, $decimalPoint)
    {
        $rupiah = "Rp " . number_format($number, $decimalPoint, ',', '.');

        return $rupiah;
    }

    public static function from_indonesian_date($date)
    {
        $expectedDateFormat = ""; //yyyy-mm-dd

        if (!empty($date)) {
            $month = array(
                'Januari' => '01',
                'Februari' => '02',
                'Maret' => '03',
                'April' => '04',
                'Mei' => '05',
                'Juni' => '06',
                'Juli' => '07',
                'Agustus' => '08',
                'September' => '09',
                'Oktober' => '10',
                'November' => '11',
                'Desember' => '12'
            );

            $divide = explode(' ', $date);
            if (!empty($month[$divide[1]])) {
                $expectedDateFormat = $divide[2] . '-' . $month[$divide[1]] . '-' . $divide[0];
            }
        }

        return $expectedDateFormat;
    }

    public static function isStoreCodeSelma($storeCode) {
        $isSelma = false;

        if (isset($storeCode) && !empty($storeCode)) {
            if ($storeCode == "H203" || substr($storeCode, 0, 2) == "H7" || substr($storeCode, 0, 2) == "J7") {
                $isSelma = true;
            }           
        }

        return $isSelma;
    }

    public static function censorEmail($email) {
        // Censored email is returned if success, otherwise it'll return empty string
        $censoredEmail = "";
        if (empty($email)) {
            return $censoredEmail;
        }

        $emailParts = explode("@", $email);
        if (count($emailParts) < 2) {
            return $censoredEmail;
        }

        /**
         * Censor process
         * Requirement:
         * - Censor all char on email except first 3 char dan first 2 char after '@'
         * - Parts of email name testing@ruparupa.com
         *  - testing is called emailName
         *  - ruparupa is called domainName
         * - if emailName or domainName length is 1, we don't need to censor it
         * - if emailName length is 2 - 3, we only censor 1 char
         * - if domainName length is 2, we only censor 1 char
         * Example:
         * Before => testing@ruparupa.com
         * After => tesxxxx@ruxxxxxx.com
         */

        // Example testing@ruparupa.com

        // Process name part (testing)
        $emailName = $emailParts[0];
        if (strlen($emailName) > 1) {
            // Decide num of censored chars & build uncensored part of email name
            $numCensoredNameChars = strlen($emailName) > 3 ? strlen($emailName) - 3 : 1;
            $uncensoredEmailName = join("", array_slice(str_split($emailName), 0, strlen($emailName) - $numCensoredNameChars));
            // Append uncensored email name & censored chars
            $emailName = sprintf("%s%s", $uncensoredEmailName, str_repeat("x", $numCensoredNameChars));
        }

        // Process domain part (ruparupa.com)
        $fullDomain = $emailParts[1];
        $domainParts = explode(".", $fullDomain);
        // Take the domain name
        $domainName = $domainParts[0];
        if (strlen($domainName) > 1) {
            // Remove domain name from domainParts
            $domainExt = implode(".", array_slice($domainParts, 1));
            // Decide num of censored chars & build uncensored part of domain name
            $numCensoredDomainNameChars = strlen($domainName) > 2 ? strlen($domainName) - 2 : 1;
            $uncensoredDomainName = join("", array_slice(str_split($domainName), 0, strlen($domainName) - $numCensoredDomainNameChars));
            // Append uncensored domain name, censored chars & its extension (com / co.id / etc)
            $fullDomain = sprintf("%s%s.%s", $uncensoredDomainName, str_repeat("x", $numCensoredDomainNameChars), $domainExt);
        }
        $censoredEmail = sprintf("%s@%s", $emailName, $fullDomain);
        return $censoredEmail;
    }

    public static function censorPhone($phone) {
        // Censored email is returned if success, otherwise it'll return empty string
        $censoredPhone = "";
        if (empty($phone)) {
            return $censoredPhone;
        }

        /**
         * Censor process
         * Requirement:
         * - Censor all digits on email except first 2 digit dan last 2 digit
         * Example:
         * Before => 087885244540
         * After => 08xxxxxxxx40
         */
        
        $numCensoredDigits = strlen($phone) - 4;
        $separatedPhone = str_split($phone);
        $firstTwoDigits = join("", array_slice($separatedPhone, 0, 2));
        $lastTwoDigits = join("", array_slice($separatedPhone, strlen($phone) - 2, 2));
        $censoredPhone = sprintf("%s%s%s", $firstTwoDigits, str_repeat("x", $numCensoredDigits), $lastTwoDigits);
        return $censoredPhone;
    }

    public static function prepareDynamicBindStatement($query, $params) {
        foreach ($params as $key => $value) {
            $tempQuery = $query;
            if (is_array($value)) {
                $query = str_replace("{$key} IN ?", "{$key} IN ({{$key}:array})", $query);
            } else {
                $query = str_replace("{$key} = ?", "{$key} = :{$key}:", $query);
            }
            if ($query == $tempQuery){
                return null;
            }
        }
        return $query;
    }

    public static function getMemberOrder(string $supplierAlias, string $brandStoreName): string {
        $memberOrder = $supplierAlias;

        switch (strtolower($brandStoreName)) {
            case "selma":
                $memberOrder = "SLM";
                break;
            case "ace hardware":
            case "azko":
                $memberOrder = "AHI";
                break;
            case "toys kingdom":
                $memberOrder = "TGI";
                break;
            case "informa":
            case "informa electronic":
            case "informa sleep":
                $memberOrder = "HCI";
                break;
        }

        return $memberOrder;
    }
    
}
