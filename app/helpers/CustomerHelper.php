<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/21/2016
 * Time: 3:55 PM
 */

namespace Helpers;


class CustomerHelper
{
    public function _validationRequired($value = FALSE){
        if(!$value)
            return FALSE;
        if(empty(trim($value)))
            return FALSE;

        return TRUE;
    }

    public function _validationMaxLength($value = FALSE, $param = FALSE){
        $value = trim($value);
        if(!$value)
            return FALSE;
        if(strlen($value) > $param)
            return FALSE;

        return TRUE;
    }

    public function _validationMinLength($value = FALSE, $param = FALSE){
        $value = trim($value);
        if(!$value)
            return FALSE;
        if(strlen($value) < $param)
            return FALSE;

        return TRUE;
    }

    public function _validationMatchPassword($password = FALSE, $retype_password = FALSE){
        if($password !=$retype_password)
            return FALSE;

        return TRUE;
    }

    /**
     * Is Numeric
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    public function _validationIsNumeric($str)
    {
        return ( ! is_numeric($str)) ? FALSE : TRUE;
    }

    public function _validationMatch($str1,$str2)
    {
        return ($str1 !== $str2) ? FALSE : TRUE;
    }

    /**
     * Valid Email
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    public function _validEmail($str)
    {
        return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
    }

    public function pass_exist($str){
        $session = $this->session->get('auth-identity');
        $id_user = $session['id'];

        return $this->profileModel->checkPass($id_user, $str);
    }

    public function linkingCustomerUsingNsq($email, $phone, $company_code) {
        // In here we need to pass the value of email, phone and company_code to queue-v2 for linking customer
        $nsq = new \Library\Nsq();
        $message = [
            "data" => [
                "email" => $email,
                "phone" => $phone,
                "company_code" => $company_code,
            ],
            "message" => "linkCustomerBetweenCompany"
        ];
        $nsq->publishCluster('customer', $message);
    }

}