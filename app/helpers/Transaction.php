<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/9/2017
 * Time: 4:05 PM
 */

namespace Helpers;


class Transaction
{

    /**
     * @param float $sub_total this is subtotal from item without discount
     * @param float $discount_amount
     * @param float $shipping_amount
     * @param float $shipping_discount_amount
     * @param float $gift_cards_amount
     * @return float
     *
     */
    public static function countTotal($sub_total = 0.00, $discount_amount = 0.00, $shipping_amount = 0.00, $shipping_discount_amount = 0.00, $gift_cards_amount = 0.00, $mdrCustomer = 0.00 )
    {
        return ($sub_total - $discount_amount) + ($shipping_amount - $shipping_discount_amount) - $gift_cards_amount + $mdrCustomer;
    }

    /**
     * @param float $selling_price
     * @param float $handling_fee
     * @param float $discount_amount
     * @return float
     */
    public static function countSubTotalItem($selling_price = 0.00, $handling_fee = 0.00, $discount_amount =  0.00)
    {
        return ($selling_price + $handling_fee ) - $discount_amount;
    }

    /**
     * @param float $sub_total
     * @param int $qty_ordered
     * @return float
     */
    public static function countTotalItem($sub_total = 0.00, $qty_ordered = 1)
    {
        return $sub_total * $qty_ordered;
    }

    /**
     * @param float $handling_fee
     * @param int $qty_ordered
     * @return float
     */
    public static function countHandlingFee($handling_fee = 0.00, $qty_ordered = 1)
    {
        return $handling_fee * $qty_ordered;
    }

    public static function generateOrderNo($company_code = 'ODI')
    {
        // add suffix '20' for 2.0 identifier
        return $company_code.substr(number_format(time() * rand(),0,'',''),0,9).'21';
    }

    public static function generatePreOrderNo($company_code = 'ODI')
    {
        // add suffix '20' for 2.0 identifier
        return "PRE".$company_code.substr(number_format(time() * rand(),0,'',''),0,9).'21';
    }

    public static function generateLazadaOrderNo($order_id)
    {
        return "LZDODI".$order_id;
    }

    public static function generateInvoiceNo()
    {
        $nanotime = exec('date +%s%N');
        return "INV".substr(number_format($nanotime * rand(1, getrandmax()),0,'',''),0,9);
    }

    public static function generateInvoicePreOrderNo (){
        $nanotime = exec('date +%s%N');
        return "PREINV".substr(number_format($nanotime * rand(1, getrandmax()),0,'',''),0,9);
    }

    public static function generateCreditMemoNo()
    {
        return "CR".substr(number_format(time() * rand(),0,'',''),0,9);
    }
}