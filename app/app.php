<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

/**
 * Handle default route, redirect it to ruparupa home page
 */
$app->get('/', function () use ($app) {
    $app->response->redirect("https://www.ruparupa.com");
    $app->response->sendHeaders();
});

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $rupaResponse = new \Library\Response();
    $rupaResponse->setContentType("application/json");
    $rupaResponse->setStatusCode(404, "Not Found")->sendHeaders();
    $rupaResponse->setContent(array("errors" => array("code" => "404", "title" => "API endpoint not found", 'message' => "API endpoint not found, please check API documentation again")));
    $rupaResponse->send();
});
