<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SpecialPriceController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/specialprice");

// Setting route for API
$collectionObj->get("/","specialPriceList");
$collectionObj->get("/active","specialPriceActiveList");

$collectionObj->post("/generate","generateSpecialPrice");
$collectionObj->post("/instant_generate","generateSpecialPriceInstant");
$collectionObj->post("/","createSpecialPrice");
$collectionObj->post("/zone","createPriceZoneGideon");

$collectionObj->delete("/","deleteExpiredPrice");

return $collectionObj;