<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SalesLogController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/sales/log");

$collectionObj->post("/","saveSalesLog");
$collectionObj->put("/{sales_log_id}","saveSalesLog");
$collectionObj->get("/","getSalesLogList");
$collectionObj->get("/{sales_log_id}","getSalesLogData");

return $collectionObj;