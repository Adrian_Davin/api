<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 8/22/2017
 * Time: 2:58 PM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SeoAutoLinkController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/seo/autoLink");

// Setting route for API
$collectionObj->get("/","getSeoAutoLink");
$collectionObj->get("/{seo_link}","getSeoAutoLink");
$collectionObj->post("/","insertSeoAutoLink");
$collectionObj->put("/{seo_auto_link_id}","updateSeoAutoLink");

$collectionObj->get("/customList","customList");
$collectionObj->get("/customAlphabet","customAlphabet");

return $collectionObj;