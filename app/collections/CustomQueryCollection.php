<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CustomQueryController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/custom/query");

/*
 * This end point will handle for get all data from elastic such as :
 * - product detail
 * - product list (show category)
 * - product search
 */
$collectionObj->post("/marketing/rule/status","marketingRuleStatus");
$collectionObj->post("/after/batch/stock","customInventoryAfterBatchStock");

// Setting route for API

return $collectionObj;