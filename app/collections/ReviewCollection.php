<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ReviewController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/review");

/*
 * This end point will handle for get all data from mongodb such as :
 * - supplier review
 * - product review
 */

//route for supplier review
$collectionObj->post("/supplier","saveSupplierReview");
$collectionObj->get("/supplier","supplierReviewList");
$collectionObj->get("/supplier/average","supplierReviewAvg");
$collectionObj->get("/supplier/notification","supplierReviewNotification");
$collectionObj->get("/customer/notification","customerReviewNotification");

//route for supplier review
$collectionObj->post("/product","saveProductReview");
$collectionObj->get("/product","productReviewList");
$collectionObj->get("/product/average","productReviewAverage");

// Setting route for API

return $collectionObj;