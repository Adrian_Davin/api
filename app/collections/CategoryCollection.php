<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CategoryController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/categories");

// upload bulk category - AHI
$collectionObj->get("/all","getAllCategories");
$collectionObj->get("/pairing","getPairingProductCategory");

// Setting route for API
$collectionObj->get("/","categoryList");
$collectionObj->get("/tree","categoryTree");
$collectionObj->delete("/tree","categoryTreeDelete");
$collectionObj->put("/tree","generateCategoryTree");

$collectionObj->put("/{category_id:[0-9]+}","updateData");
$collectionObj->get("/{category_id:[0-9]+}","categoryDetail");
$collectionObj->get("/path/{article_hierarchy}","categoryPath");
$collectionObj->post("/","updateData");
$collectionObj->post("/productToCategory","productToCategory");

$collectionObj->get("/generateUrlPath", "generateUrlPath");
$collectionObj->post("/import","importCategory");

$collectionObj->put("/generatedUrl", "getGeneratedURl");
$collectionObj->post("/batch","uploadBatchCategory");
$collectionObj->put("/update_batch", "updateBatchCategory");

$collectionObj->get("/autoHeroProduct","autoHeroProduct");

// get inspirations when searching at frontend
$collectionObj->get("/customCategoryDetail","customCategoryDetail");
return $collectionObj;