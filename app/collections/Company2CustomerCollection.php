<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 5:49 PM
 */
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\Company2CustomerController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/company/customer");

// Setting route for API
$collectionObj->get("/","getcustomerData");
$collectionObj->post("/","saveCustomer");
$collectionObj->put("/{customer_id}","saveCustomer");
$collectionObj->delete("/{customer_id}","deleteCustomer");

return $collectionObj;