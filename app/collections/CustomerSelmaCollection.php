<?php

use Phalcon\Mvc\Micro\Collection;

$handler = "\Controllers\SelmaController\CustomerSelmaController";

$collection = new Collection();
$collection->setHandler($handler, true);
$collection->setPrefix("/selma/customer");

$collection->get("/{customer_id}", "getById");
$collection->put("/{customer_id}", "update");

$collection->post("/register", "register");
$collection->post("/login", "login");
$collection->post("/logout", "logout");

$collection->post("/password/forgot", "forgotPassword");
$collection->post("/password/change", "changePassword");

$collection->get("/voucher", "getUserVoucher");
$collection->get("/notification", "getListNotif");
$collection->get("/tiering/{customer_id}", "getTiering");
$collection->get("/receipt_list", "getReceiptList");
$collection->get("/receipt", "getDigitalReceipt");
$collection->post("/pay_with_point", "payWithPoint");
$collection->get("/point", "getPointHistory");
$collection->get("/check_email/{customer_id}", "getCheckEmail");
$collection->post("/connect_member", "postConnectMember");

$collection->get("/member/{customer_id}", "getMemberById");

$collection->get("/transaction", "getTransactionHist");
$collection->post("/transaction/rate", "postSatisfactionRate");
$collection->get("/transaction/delivery/list", "getTransactionDeliveryList");
$collection->get("/transaction/delivery/detail", "getTransactionDeliveryDetail");
$collection->get("/transaction/installation/list", "getTransactionInstallationList");
$collection->get("/transaction/installation/detail", "getTransactionInstallationDetail");
$collection->get("/transaction/installation/route", "getInstallationRoute");

$collection->post("/email/verify", "verifyEmail");
$collection->post("/email/change/send_otp", "sendChangeEmailOTP");
$collection->post("/email/change/submit_otp", "submitChangeEmailOTP");

$collection->post("/phone/verify/send_otp", "verifyPhone");
$collection->post("/phone/verify/submit_otp", "verifyPhoneSubmitOTP");
$collection->post("/phone/change/send_otp", "sendChangePhoneOTP");
$collection->post("/phone/change/submit_otp", "submitChangePhoneOTP");
$collection->post("/phone/update", "submitMandatoryUpdatePhone");

$collection->get("/redeem_voucher", "getRedeemVoucherList");
// $collection->post("/redeem_voucher/send_otp", "redeemVoucherSendOTP"); //Comment unused feature
// $collection->post("/redeem_voucher/submit_otp", "redeemVoucherSubmitOTP"); //Comment unused feature

$collection->post("/pwp/send_otp", "postPwpSendOTP");
$collection->post("/pwp/submit_otp", "postPwpSubmitOTP");

return $collection;
