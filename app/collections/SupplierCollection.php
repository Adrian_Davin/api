<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SupplierController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/supplier");

/*
 * This end point will handle for get all data from elastic such as :
 * - product detail
 * - product list (show category)
 * - product search
 */

//route for supplier profile
$collectionObj->post("","saveSupplier");
$collectionObj->put("/{supplier_id:[0-9]+}","saveSupplier");

$collectionObj->get("/list","listSupplier");
$collectionObj->get("/detail/{supplier_id}","detailSupplier");

//route for supplier user
$collectionObj->get("/login","login");
$collectionObj->put("/logout/{supplier_user_id}","logout");
$collectionObj->get("/user/{supplier_user_id:[0-9]+}","getSupplierUserData");

//route for report
$collectionObj->get("/report/settlement/{supplier_id}","settlementReport");
$collectionObj->get("/report/summary","summaryReport");

// route for supplier role
$collectionObj->get("/role","getSupplierRoleList");
$collectionObj->get("/role/{role_id:[0-9]+}","getSupplierRole");
$collectionObj->post("/role","saveSupplierRole");

// Setting route for API

return $collectionObj;