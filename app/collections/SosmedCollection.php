<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 05/09/2017
 * Time: 2:47 PM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SosmedController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/sosmed");

$collectionObj->get("/login_url","loginUrl");
$collectionObj->get("/authorize_login","authorizeLogin");

return $collectionObj;