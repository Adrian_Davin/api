<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ShoppingCartController",true); // load by namespace and using lazy load

// Set a common prefix for all routes
$collectionObj->setPrefix("/shopping/cart"); // dipake buat di URL mau apa

/**
 * @api {post}
 * @apiDescription
 * Save new cart
 *
 */
$collectionObj->post("/","saveCartData");

/**
 * @apiDescription
 * Add / update qty product to shopping cart, Update other information about
 * Shopping Cart (billing, shipping, user_ip, etc)
 * @apiExample
 *
 *	{
 *        "data": {
 *            "items" : [
 *				{
 *					"sku": "302753",
 *					"qty_ordered": "1"
 *				}
 *			]
 *        }
 *	}
 */
$collectionObj->put("/{cart_id:[a-zA-Z0-9]+}","saveCartData");

/**
 * @apiDescription
 * Get shoppingcart information
 */
$collectionObj->get("/{cart_id:[a-zA-Z0-9]+}","getCartData");

$collectionObj->post("/reorder/{order_no}","saveCartFromOrder");
$collectionObj->post("/payment-check","checkPayment");
$collectionObj->get("/promotion/cash_back/{cart_id}","checkCashBackPromotion");
$collectionObj->get("/promotion/voucher/{cart_id}","checkVoucherPromotion");

/**
 * @apiDescription
 * Update payment information
 * @apiExample
 *
 *	{
 *    "payment": {
 *       "method": "vpayment",
 *       "type": "credit_card",
 *       "cc_number" : "541322123"
 *    }
 *	}
 */
$collectionObj->put("/payment/{cart_id:[a-zA-Z0-9]+}","savePaymentData");
$collectionObj->put("/po/{cart_id:[a-zA-Z0-9]+}","savePoNumberData");

/**
 * @apiDescription
 * Remove item from Shopping Cart
 */
$collectionObj->delete("/item/{cart_id:.+}","deleteCartItems");
$collectionObj->get("/item/stock/{cart_id:.+}","checkStockItems");
$collectionObj->put("/voucher/redeem/{cart_id:[a-zA-Z0-9]+}","redeemVoucher");
$collectionObj->delete("/voucher/redeem/{cart_id:[a-zA-Z0-9]+}","deleteRedeemVoucher");

/**
 * @apiDescription
 * Count shipping and handling for all item in cart
 *
 */
$collectionObj->put("/count_shipping_cost/{cart_id:[a-zA-Z0-9]+}","countShippingCost");

/**
 * @apiDescription
 * Validate current shopping cart
 */
$collectionObj->get("/validate/{cart_id:.+}","cartValidation");
$collectionObj->get("/shipping/assignation/{cart_id:.+}","shippingAssignation");

return $collectionObj;