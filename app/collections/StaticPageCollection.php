<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\StaticPageController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/staticpage");

// Setting route for API
$collectionObj->get("/","staticPageList");
$collectionObj->get("/{identifier}","staticPageDetail");
$collectionObj->post("/","createStaticPage");
$collectionObj->put("/{static_page_id}", "updateStaticPage");

return $collectionObj;