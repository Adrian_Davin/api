<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\EventController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/events");

// Setting route for API
$collectionObj->get("/","eventList");
$collectionObj->get("/{identifier}","eventDetail");
$collectionObj->post("/","createEvent");
$collectionObj->put("/{event_id}","updateEvent");
//$collectionObj->put("/switch/next","switchingNextEvents");
//$collectionObj->put("/switch/prev","switchingPrevEvents");
$collectionObj->put("/switch","switchingEvents");

return $collectionObj;