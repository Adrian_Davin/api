<?php

use Phalcon\Mvc\Micro\Collection;

$handler = "\Controllers\AceController\ServiceCenterAceController";

$collection = new Collection();
$collection->setHandler($handler, true);
$collection->setPrefix("/ace/service-center");

$collection->get("/warranty-availability/{order_no}", "warrantyAvailabilityCheckup");
$collection->get("/warranty-availability/{receipt_id}/offline", "warrantyAvailabilityCheckupOffline");
$collection->get("/warranty-details/{ace_warranty_id}", "warrantyDetails");
$collection->get("/warranty-details/{ace_warranty_id}/offline", "warrantyDetailsOffline");
$collection->get("/warranty-services/{customer_id}", "warrantyServicesByCustomerId");
$collection->get("/warranty-services/{customer_id}/offline", "warrantyServicesByCustomerIdOfflineTransaction");
$collection->get("/warranty-services/{ace_service_number}/details", "warrantyServiceDetailByAceServiceNumber");

$collection->post("/warranty-activation", "warrantyActivation");
$collection->post("/warranty-activation/offline", "warrantyActivationOffline");
$collection->post("/warranty-services/service-charge-confirmation", "warrantyServiceChargeConfirmation");

/** Webhook */
$collection->post("/webhook/service-charge", "webhookServiceCharge");
$collection->post("/webhook/status", "webhookStatus");
$collection->post("/webhook/activate-warranty", "webhookActivateWarranty");

return $collection;
