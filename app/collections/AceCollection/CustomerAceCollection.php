<?php

use Phalcon\Mvc\Micro\Collection;

$handler = "\Controllers\AceController\CustomerAceController";

$collection = new Collection();
$collection->setHandler($handler, true);
$collection->setPrefix("/ace/customer");

$collection->get("/{id}", "getById");
$collection->post("/{id}", "update");
$collection->post("/login", "login");
$collection->post("/logout", "logout");
$collection->post("/register", "register");
$collection->post("/register-membership", "registerMembership");
$collection->post("/generate_otp", "generateOtp");
$collection->post("/validate_otp", "validateOtp");
$collection->post("/check_email_available", "checkEmailAvailable");
$collection->post("/check_phone_available", "checkPhoneAvailable");
$collection->post("/passkey/{id}", "changePasskey");
$collection->post("/phone/{id}", "changePhone");
$collection->post("/primary-address/{id}", "changeAddress");
$collection->post("/forget_passkey", "forgetPasskey");
$collection->get("/point_history", "getPointHistory");
$collection->get("/transaction_history", "getTransactionHistory");
$collection->get("/transaction_history_online", "getTransactionOnlineHistory");
$collection->get("/transaction_history", "getTransactionHistory");
$collection->get("/total_transaction/{customer_id}", "getTotalAmountTransactionByCustomerId");
$collection->get("/address/{customer_id}", "getAddress");
$collection->post("/address", "createAddress");
$collection->post("/address/{customer_id}", "updateAddress");
$collection->post("/address/delete/{customer_id}", "deleteAddress");
$collection->post("/update-webhook/{ace_customer_id}", "updateWebhook");
$collection->post("/verify_email", "verifyEmailAce");

$collection->post("/email", "createEmail");
$collection->put("/email/{id}", "changeEmail");
return $collection;
