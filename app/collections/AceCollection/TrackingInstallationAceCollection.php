<?php

use Phalcon\Mvc\Micro\Collection;

$handler = "\Controllers\AceController\TrackingInstallationAceController";

$collection = new Collection();
$collection->setHandler($handler, true);
$collection->setPrefix("/ace/tracking-installation");

$collection->get("/{receipt_no}/list/{customer_id}", "trackingInstallationList");
$collection->get("/detail/{transaction_no}/{job_id}/customer/{customer_id}", "trackingInstallationDetail");
$collection->get("/installer-route/{transaction_no}/{job_id}/customer/{customer_id}", "trackingInstallationInstallerRoutes");

return $collection;
