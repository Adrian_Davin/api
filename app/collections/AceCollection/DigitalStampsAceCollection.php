<?php

use Phalcon\Mvc\Micro\Collection;

$handler = "\Controllers\AceController\DigitalStampsAceController";

$collection = new Collection();
$collection->setHandler($handler, true);
$collection->setPrefix("/ace/digital-stamps");

/**
 * Redeem customer point from point
 * to voucher
 */
$collection->get("/{customer_id}", "refreshStamps");

return $collection;