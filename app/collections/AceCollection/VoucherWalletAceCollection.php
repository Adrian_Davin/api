<?php

use Phalcon\Mvc\Micro\Collection;

$handler = "\Controllers\AceController\VoucherWalletAceController";

$collection = new Collection();
$collection->setHandler($handler, true);
$collection->setPrefix("/ace/voucher-wallet");

/**
 * Redeem customer point from point
 * to voucher only
 */
$collection->post("/voucher-redeem", "redeemPointToVoucher");

return $collection;
