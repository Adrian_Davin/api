<?php

use Phalcon\Mvc\Micro\Collection;

$handler = "\Controllers\AceController\DiginexAceController";

$collection = new Collection();
$collection->setHandler($handler, true);
$collection->setPrefix("/ace/diginex");

$collection->get("/customer-redeem/{ace_customer_member_no}", "checkCustomerRedeemKioskScan");

$collection->get("/sales-assistant/{ticket_number}", "checkCustomerRequestSalesAssistant");
/**
 * Claim voucher point from scan
 * QR Code in store and customer
 * eligible to claim voucher
 */
$collection->post("/claim-voucher", "claimDiginexVoucher");

/**
 * Endpoint to save customer checkin
 * data from kiosk
 */
$collection->post("/scan-qr/kiosk", "scanQRKiosk");

/**
 * Endpoint to save customer checkin
 * data from ace (mobile apps)
 */
$collection->post("/scan-qr/mobile", "scanQRMobile");

/**
 * Endpoint for sales assistant webhook
 */
$collection->post("/sales-assistant/webhook", "saveSalesAssistant");

/**
 * Endpoint to request sales assistant
 * and send the request to HO API
 */
$collection->post("/sales-assistant/request", "salesAssistantTicketRequest");

/**
 * Endpoint to close sales assistant ticket created
 * When customer request but sales assistant does not
 * Take the sales assistant request
 */
$collection->post("/sales-assistant/request-close", "salesAssistantTicketClose");

return $collection;
