<?php

use Phalcon\Mvc\Micro\Collection;

$handler = "\Controllers\AceController\PayWithPointAceController";

$collection = new Collection();
$collection->setHandler($handler, true);
$collection->setPrefix("/ace/pwp");

/**
 * Redeem customer point from point
 * to voucher and apply to cart
 */
$collection->post("/redeem", "redeemPoint");

return $collection;
