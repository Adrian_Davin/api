<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\MarketingAdminRoleController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/user/marketing/role");

// Setting route for API
$collectionObj->get("/","getRoleList");
$collectionObj->get("/{role_id:[0-9]+}","getRoleAttribute");
$collectionObj->put("/{role_id:[0-9]+}","saveAdminRole");
$collectionObj->post("/","saveAdminRole");

return $collectionObj;