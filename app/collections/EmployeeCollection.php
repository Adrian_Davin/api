<?php


$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\EmployeeController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/employee");

// Setting route for API

$collectionObj->get("/detail/{nip}","getEmployeeDataByNip");

$collectionObj->post("/login","loginNip");
$collectionObj->post("/stats","getEmployeeStats");
$collectionObj->post("/check","checkEmployee");
$collectionObj->post("/otp/generate","generateEmployeeOtp");
$collectionObj->post("/otp/validate","validateEmployeeOtp");

$collectionObj->put("/","upsertEmployee");
$collectionObj->put("/department","upsertEmployeeDepartment");


return $collectionObj;