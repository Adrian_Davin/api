<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CustomerController", true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/customers");

// Setting route for API
$collectionObj->post("/login", "login");
$collectionObj->post("/register", "registerCustomer");

$collectionObj->post("/logout", "logout");
$collectionObj->post("/", "saveCustomer");
$collectionObj->post("/check", "checkCustomer");
$collectionObj->post("/token/generate", "generateToken"); // currently not using this
$collectionObj->post("/token/otp/generate", "generateTokenOtp"); // forgot password generate token only for otp purposes
$collectionObj->post("/company","saveCustomerCompany");


$collectionObj->get("/","getCustomerData");
$collectionObj->get("/company/{customer_id:[0-9]+}","getCustomerCompany");
$collectionObj->get("/customListCustomer","getCustomerDataCustom");
$collectionObj->get("/{customer_id:[0-9]+}","getCustomerDetail");
$collectionObj->get("/token/check/{token:.+}","checkToken");

$collectionObj->put("/{customer_id:[0-9]+}", "saveCustomer");
$collectionObj->put("/reset_password", "resetPassword");
// $collectionObj->put("/change_password/{customer_id:[0-9]+}", "changePassword");
$collectionObj->get("/voucher/{customer_id:[0-9]+}", "getVoucherList");
$collectionObj->put("/company","updateCustomerCompany");

// Wishlist
$collectionObj->get("/wishlist", "getCustomerWishlist");
$collectionObj->post("/wishlist", "addCustomerWishlist");
$collectionObj->delete("/wishlist", "deleteCustomerWishlist");

// for oms
$collectionObj->get("/wishlist_list", "getWishlistList"); // not using anymore
$collectionObj->get("/abuser_list", "getAbuserList");

// Facebook Leads
$collectionObj->post("/facebookleads", "saveCustomerFacebookleads");

// Informa Member Chances - Payment Thankyou Page
$collectionObj->post("/member_chances", "saveCustomerMemberChances");

// Verify Email
$collectionObj->put("/verify/email", "customerVerifyEmail");

// Store Mode 
$collectionObj->post("/storemode/login", "loginStoreMode"); 
$collectionObj->get("/storemode","getStoreModeCustomer");

// Linking Customer between Company (Reporting purposes)
$collectionObj->put("/linking-customer", "linkingCustomerBetweenCompany");

return $collectionObj;
