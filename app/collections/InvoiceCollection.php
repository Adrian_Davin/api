<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\InvoiceController", true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/invoice");

// Setting route for API
$collectionObj->post("/", "createInvoice");
$collectionObj->post("/promotion/check/{invoice_id}", "invoicePromoCheck");

$collectionObj->put("/{invoice_id}", "updateInvoice");
$collectionObj->put("/pickupVoucher/{invoice_no}", "updatePickupVoucher");
$collectionObj->put("/regenerate/pickupVoucher/{invoice_no}", "regeneratePickupVoucher");
$collectionObj->put("/so_number/{invoice_no}", "updateSoSapNumber");
$collectionObj->put("/_sapSoNumber/batch", "updateBatchSoNumber");
$collectionObj->put("/voucher/update", "updatePosVoucher");
$collectionObj->put("/productDigital", "updateProductDigital");
$collectionObj->get("/", "getInvoices");
$collectionObj->get("/_invoiceListCustom", "getInvoicesCustom");
$collectionObj->get("/_invoiceTotalDc", "getInvoicesTotalDc");
$collectionObj->get("/invoiceLabel/{invoice_no:[a-zA-Z0-9]+}", "getDetailInvoiceLabel");
$collectionObj->get("/{invoice_no:[a-zA-Z0-9]+}", "getInvoiceDetail");
$collectionObj->get("/supplier/{supplier_alias}", "getInvoices");
$collectionObj->get("/check_empty_so_sap", "checkEmptySoSap");
$collectionObj->get("/voucher/{voucher_id}", "getListVoucher");
$collectionObj->get("/receipt/{voucher_id}", "getReceiptId");
$collectionObj->get("/receiptLog/{voucher_id}", "getReceiptLog");
$collectionObj->put("/chatime/{invoice_no}", "updateReceiptId");
$collectionObj->post("/membershipSlackNotification","membershipSlackNotification");
$collectionObj->put("/updateStatus", "updateInvoiceStatus");
$collectionObj->put("/stopSla", "stopInvoiceSLA");

// invoice item
$collectionObj->get("/item", "getInvoiceItem");
$collectionObj->put("/item/{invoice_item_id}", "updateInvoiceItem");

$collectionObj->post("/cashback/abuser/release","releaseCashbackAbuser");
$collectionObj->post("/cashback/abuser/unrelease","unreleaseCashbackAbuser");

return $collectionObj;
