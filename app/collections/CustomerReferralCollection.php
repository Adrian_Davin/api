<?php

/**
 * Created by PhpStorm.
 * User: kevin.aldiansyah
 * Date: 11/04/2021
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CustomerReferralController", true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/referral");

// Setting route for API

// Referral Program
$collectionObj->get("/reward","getCustomerReferralRewards");
$collectionObj->post("/link","upsertCustomerReferralLink");
$collectionObj->get("/link","getCustomerReferralLink");
$collectionObj->get("/code","getCustomerReferralCode");

return $collectionObj;