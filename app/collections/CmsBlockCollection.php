<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CmsBlockController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/cms_block");

// Setting route for API
$collectionObj->get("/","cmsBlockList");
$collectionObj->get("/{cms_block}","cmsBlockDetail");
$collectionObj->get("/detail","cmsBlockDetail");
$collectionObj->post("/","createCmsBlock");
$collectionObj->put("/{cms_block_id}", "updateCmsBlock");

return $collectionObj;