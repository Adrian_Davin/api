<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 8/22/2017
 * Time: 2:58 PM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CustomerAlertController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/customers/alert");

// Setting route for API
$collectionObj->get("/listAlert","getAlertList");
$collectionObj->post("/","saveCustomerAlert");
$collectionObj->delete("/{customer_alert_id}","deleteCustomerAlert");

return $collectionObj;