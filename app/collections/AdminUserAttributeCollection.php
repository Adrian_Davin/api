<?php
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\AdminUserAttributeController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/user/attribute");

// Setting route for API
$collectionObj->post("/","saveAttribute");

return $collectionObj;