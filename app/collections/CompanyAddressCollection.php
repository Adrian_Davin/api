<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 5:49 PM
 */
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CompanyAddressController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/company/address");

// Setting route for API
$collectionObj->get("/","getAddressData");
$collectionObj->post("/","saveAddress");
$collectionObj->put("/{address_id}","saveAddress");
$collectionObj->delete("/{address_id}","deleteAddress");

return $collectionObj;