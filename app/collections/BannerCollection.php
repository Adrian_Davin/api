<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\BannerController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/bannercampaigns");

// Setting route for API
$collectionObj->get("/{campaign_name}","getBannerList");

return $collectionObj;