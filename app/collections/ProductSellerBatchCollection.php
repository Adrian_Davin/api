<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ProductSellerBatchController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/product-seller-batch");

// Setting route for API
$collectionObj->get("/","getListBatch");
$collectionObj->post("/","saveBatch");

return $collectionObj;