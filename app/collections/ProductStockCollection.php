<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ProductStockController",true);

// Set a common prefix
$collectionObj->setPrefix("/product/stock");

// Setting Product Stock for API
$collectionObj->get("/","getProductStock");
$collectionObj->put("/","putListProductStock");
$collectionObj->put("/variant/{sku}","putDetailProductStock");
$collectionObj->post("/","createProductStock");

return $collectionObj;