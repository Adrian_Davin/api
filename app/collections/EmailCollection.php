<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 8/16/2017
 * Time: 1:18 PM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\EmailController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/email");

// Setting route for API
$collectionObj->get("/sendMail","sendEmail");
$collectionObj->get("/get_email_template_id","getEmailTemplateID");
$collectionObj->post("/sendCashbackEmail","sendCashbackEmail");
$collectionObj->post("/saveDataSellerNotif", "saveDataSellerNotif");

return $collectionObj;