<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\InspirationController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/inspirations");

// Setting route for API
$collectionObj->get("/new","newInspirationList");
$collectionObj->get("/","inspirationList");

// INSPIRATION PROMOTIONS
$collectionObj->get("/promotions", "listPromotionInspirations");
// $collectionObj->get("/{inspiration_id}","inspirationDetail");
$collectionObj->post("/","createInspiration");
$collectionObj->put("/{inspiration_id}","updateInspiration");

return $collectionObj;