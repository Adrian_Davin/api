<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CustomerAddressController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/customers/address");

// Setting route for API
$collectionObj->get("/","getAddressData");
$collectionObj->get("/{address_id}","getAddressDetail");
$collectionObj->put("/{address_id}","saveAddress");
$collectionObj->post("/","saveAddress");
$collectionObj->get("/list/{customer_id}","getAddressDetail");
$collectionObj->delete("/{address_id}","deleteAddress");

return $collectionObj;