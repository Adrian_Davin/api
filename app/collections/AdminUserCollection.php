<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 14/3/2017
 * Time: 3:22 AM
 */
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\AdminUserController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/user");

// Setting route for API
$collectionObj->put("/","login");
$collectionObj->put("/logout/{admin_user_id}","logout");
$collectionObj->post("/","saveAdminUser");
$collectionObj->post("/customize","saveAdminUserCustomize");
$collectionObj->put("/{admin_user_id}","saveAdminUser");

$collectionObj->get("/","getAdminUserList");
$collectionObj->get("/_adminUserListCustom","getAdminUserListCustom");
$collectionObj->get("/customize","getAdminUserCustomize");

$collectionObj->get("/{admin_user_id:[0-9]+}","getAdminUserData");
$collectionObj->get("/apps","getListApps");

return $collectionObj;