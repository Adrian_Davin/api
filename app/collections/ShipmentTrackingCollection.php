<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ShipmentTrackingController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/shipment/tracking");

$collectionObj->post("/","saveShipmentTracking");
$collectionObj->put("/{shipment_tracking_id}","saveShipmentTracking");
$collectionObj->get("/","getShipmentTrackingList");
$collectionObj->get("/{shipment_tracking_id:[0-9]+}","getShipmentTrackingData");
$collectionObj->get("/get_awb_tracking","getAwbTracking");

return $collectionObj;