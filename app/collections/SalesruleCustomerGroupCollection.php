<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 13/06/2017
 * Time: 10:12 PM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SalesruleCustomerGroupController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/salesrule/customer_group");

// Setting route for API
$collectionObj->get("/","listSalesruleCustomerGroup");
$collectionObj->get("/concatenated","listSalesruleCustomerGroupConcatenated");

return $collectionObj;