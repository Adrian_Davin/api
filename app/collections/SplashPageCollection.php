<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SplashPageController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/splash_page");

// Setting route for API
$collectionObj->get("/","splashPageList");
$collectionObj->get("/{splash_page_id}","splashPageDetail");
$collectionObj->post("/","createSplashPage");
$collectionObj->put("/{splash_page_id}", "updateSplashPage");

return $collectionObj;