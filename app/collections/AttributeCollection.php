<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\AttributeController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/attribute");

$collectionObj->post("/listoptionvalue","listOptionValue");
$collectionObj->get("/listoptionvalue","listOptionValue");
$collectionObj->get("/listoptionvalue/{attribute_id}","listOptionValue");
$collectionObj->get("/listvariantoption","listVariantOption");
$collectionObj->get("/listunitoptions/{attribute_unit_id}","listUnitOptions");
$collectionObj->put("/optionvalue","updateOptionValue");
$collectionObj->put("/variantoption","updateVariantOption");
$collectionObj->put("/unitoptions","updateUnitOptions");

// Setting route for API

return $collectionObj;