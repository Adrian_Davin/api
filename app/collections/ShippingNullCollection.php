<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ShippingNullController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/shippingNull");

$collectionObj->put("/updateStock","updateStock");

return $collectionObj;