<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\DepartmentBuController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/departmentBu");

/*
 * This end point will handle for get all data from elastic such as :
 * - product detail
 * - product list (show category)
 * - product search
 */
$collectionObj->get("/list","listDepartment");

$collectionObj->get("/detail/{id_department_bu}","detailDepartment");

$collectionObj->post("/save","createDepartmentBU");

$collectionObj->put("/update/{id_department_bu}","updateDepartmentBU");

// Setting route for API

return $collectionObj;