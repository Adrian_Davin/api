<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ReportsController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/reports");

/*
 * This end point will handle for get all data from elastic such as :
 *
 */

$collectionObj->put("/marketplace","mpReports");
$collectionObj->get("/finance","financeReports");

// Setting route for API

return $collectionObj;