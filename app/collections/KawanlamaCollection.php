<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\KawanlamaController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/klsys");

$collectionObj->post("/pickup/voucher","pickupVoucher");

$collectionObj->post("/pickup/journal","pickupJournal");
$collectionObj->post("/sales/journal","salesJournal");
$collectionObj->post("/sales_b2b_top/journal","salesB2BTopJournal");
$collectionObj->post("/incoming_b2b_top/journal","incomingB2BTopJournal");
$collectionObj->post("/incoming/journal","incomingJournal");
$collectionObj->post("/mdr/journal","mdrJournal");
$collectionObj->post("/giftcard/journal","giftCardJournal");
$collectionObj->post("/pickup_reverse/journal","pickupReverseJournal");
$collectionObj->post("/incoming_reverse/journal","incomingReverseJournal");
$collectionObj->post("/mdr_reverse/journal","mdrReverseJournal");
$collectionObj->post("/giftcard_reverse/journal","giftCardReverseJournal");

$collectionObj->post("/mdr","createMdr");

$collectionObj->post("/sosap","createSoSAP");
$collectionObj->post("/socisap","createSoCISAP");
$collectionObj->post("/refund/journal","createJournalRefund");
$collectionObj->post("/status-member","checkStatusMember");
$collectionObj->post("/point-member","checkPointMember");
$collectionObj->post("/ace-member-data","checkAceMemberData");
$collectionObj->post("/member-data","checkMemberData");
$collectionObj->post("/login-store","loginStore");

$collectionObj->post("/soregisterMember","createSoRegisterMember");

return $collectionObj;