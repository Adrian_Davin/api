<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\BrandController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/brand");

/*
 * This end point will handle for get all data from elastic such as :
 * - product detail
 * - product list (show category)
 * - product search
 */
$collectionObj->get("/listBrand","listBrand");
$collectionObj->get("/detail/{brand_id}","detailBrand");
$collectionObj->get("/brandUrlKey/{brand_id}","getUrlKey");
$collectionObj->get("/brandId/{category_id}","getBrandId");

$collectionObj->post("/","createBrand");
$collectionObj->put("/{brand_id}", "updateBrand");
$collectionObj->delete("/{brand_id}","deleteBrand");


// Setting route for API

return $collectionObj;