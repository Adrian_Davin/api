<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\PageRedirectController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/pageredirect");

// Setting route for API
$collectionObj->get("/","pageRedirectList");
$collectionObj->get("/{identifier}","pageRedirectDetail"); // if number reference to page_redirect_id, if string reference to keyword
$collectionObj->post("/","createPageRedirect");
$collectionObj->put("/{page_redirect_id}", "updatePageRedirect");

return $collectionObj;