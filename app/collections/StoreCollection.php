<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 14/3/2017
 * Time: 3:22 AM
 */
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\StoreController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/store");

// Setting route for API
$collectionObj->get("/","getStoreList");
$collectionObj->get("/supplier/{supplier_id}","getStoreList");
$collectionObj->get("/{store_id}","getStoreData");
$collectionObj->post("/nearest","getNearestStore");
$collectionObj->put("/{store_id}","saveStore");
$collectionObj->post("/","saveStore");
return $collectionObj;