<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\RouteController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/route");

// Setting route for API
$collectionObj->get("/","getAllRouting");
$collectionObj->get("/{url_key:.+}","getRouting");
$collectionObj->get("/identifier/{identifier}","checkRoutingByIdentifier");

$collectionObj->post("/","saveRouting");
$collectionObj->put("/","updateRouting");
$collectionObj->delete("/","hardDelete");
$collectionObj->put("/change","changeRoutingParent");

return $collectionObj;