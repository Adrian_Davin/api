<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();


// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ProductLabelsController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/labels");

// Setting route for API
$collectionObj->get("/","labelList");
$collectionObj->get("/url_cloudinary/{company_code}","getURLCloudinaryList");
$collectionObj->get("/{product_label_id:[0-9]+}","labelDetail");
$collectionObj->get("/expired","labelListExpired");
$collectionObj->get("/search/{name}","labelDetailByName");
$collectionObj->get("/ace_promo","getAcePromo");
$collectionObj->get("/ace_promo_exclude","getAcePromoExclude");

$collectionObj->post("/","createLabel");
$collectionObj->post("/ace_promo","insertAcePromo");
$collectionObj->put("/{product_label_id}", "updateLabel");
$collectionObj->put("/generate/{product_label_id}", "generateLabel");
$collectionObj->put("/remove/{product_label_id}", "removeLabel");
$collectionObj->put("/expired", "updateExpiredLabel");


return $collectionObj;