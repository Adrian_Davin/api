<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 14/3/2017
 * Time: 3:22 AM
 */
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\PickupPointController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/pickuppoint");

// Setting route for API
$collectionObj->get("/","getPickupPointList");
$collectionObj->get("/{pickup_id}","getPickupPointData");
return $collectionObj;