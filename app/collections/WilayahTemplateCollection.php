<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\WilayahTemplateController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/region");

// Setting route for API
$collectionObj->get("/","regionList"); //
$collectionObj->get("/supplier/{supplier_id}","regionList"); //
$collectionObj->get("/{wilayah_template_id}","getRegionDetail");//

$collectionObj->post("/","createRegion");//
$collectionObj->post("/upload","uploadSku");

$collectionObj->put("/{wilayah_template_id}", "updateRegion"); //

return $collectionObj;