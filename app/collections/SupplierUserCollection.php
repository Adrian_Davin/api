<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SupplierUserController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/supplier/user");

/*
 * This end point will handle for get all data from elastic such as :
 * - product detail
 * - product list (show category)
 * - product search
 */

//route for supplier user
$collectionObj->post("","saveUser");
$collectionObj->get("/list","listUser");
$collectionObj->get("/detail/{supplier_user_id}","detailUser");
$collectionObj->get("/login","login");

// Setting route for API

return $collectionObj;