<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SkuController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/sku");

// Setting route for API
$collectionObj->post("/request_attempt","saveSkuRequestAttempt");

return $collectionObj;