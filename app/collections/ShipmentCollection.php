<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ShipmentController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/shipment");

$collectionObj->get("/","getShipmentList");
$collectionObj->get("/{shipment_id}","getShipmentData");
$collectionObj->get("/get_shipping_null","getShippingNullData");
$collectionObj->get("/get_pending_order","getPendingOrder");
$collectionObj->get("/get_origin_by_kecamatan_id","getOriginByKecamatanID");
$collectionObj->get("/get_shipment_batch","getShipmentBatch");

$collectionObj->put("/{shipment_id}","updateShipmentData");
$collectionObj->put("/rollback/{shipment_id}","rollBackShipment");
$collectionObj->put("/batch/checkawb","checkAwbBatch");
$collectionObj->put("/ninjavan/webhooks","ninjaWebhooks");
$collectionObj->put("/update_shipping_null","updateShippingNull");
$collectionObj->put("/send_email_shipping_null","sendEmailShippingNull");
$collectionObj->put("/send_email_reminder_pending_order","sendEmailReminderPendingOrder");
$collectionObj->put("/process_shipment_batch","processShipmentBatch");
$collectionObj->put("/update_status_batch","updateStatusBatch");

return $collectionObj;