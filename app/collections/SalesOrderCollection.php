<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SalesOrderController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/sales/order");

// Setting route for API
$collectionObj->get("/","getOrderList");
$collectionObj->get("/_orderItem","getOrderItem");
$collectionObj->get("/summary","getOrderSummary");
$collectionObj->get("/summaryOnly","getOrderSummaryOnly");
$collectionObj->get("/_orderListCustom","getOrderListCustom");
$collectionObj->get("/_orderListExtOrderNo","getOrderListBaseOnExternalOrderNo");
$collectionObj->get("/orderListPayment","getOrderListPayment");
$collectionObj->put("/payment/{payment_id:[0-9]+$}","updateOrderPayment");
$collectionObj->get("/orderListDiscountAmount","getOrderListDiscountAmount");

$collectionObj->get("/{order_no:(ODIB[0-9]+|AHI[0-9]+|HCI[0-9]+|ODI[0-9]+|ODIS[0-9]+|ODIT[0-9]+|ODIK[0-9]+|LZDODI[0-9]+|ODIR[0-9]+|ODIA[0-9]+|PREODI[0-9]+|ODIH[0-9]+|ODII[0-9]+|ODIL[0-9]+|ODIM[0-9]+|ODIN[0-9]+|ODIC[0-9]+)}","getOrderDetail");
$collectionObj->get("/shipment/{order_no:(ODIB[0-9]+|AHI[0-9]+|HCI[0-9]+|ODI[0-9]+|ODIS[0-9]+|ODIT[0-9]+|ODIK[0-9]+|LZDODI[0-9]+|ODIR[0-9]+|ODIA[0-9]+|PREODI[0-9]+|ODIH[0-9]+|ODII[0-9]+|ODIL[0-9]+|ODIM[0-9]+|ODIN[0-9]+|ODIC[0-9]+)}","getOrderShipmentDetail");
$collectionObj->post("/","saveOrder");
$collectionObj->post("/cache","createOrderCache");
$collectionObj->put("/{sales_order_id:[0-9]+$}","updateOrder");

$collectionObj->post("/refund/items","prepareCreditMemo");
$collectionObj->put("/refund/items/{credit_memo_id}","prepareCreditMemo");
$collectionObj->post("/refund","saveCreditMemo");
$collectionObj->get("/refund/{order_no:.+}","getCreditMemo");
$collectionObj->get("/refund/list","getCreditMemoListCustom");
$collectionObj->get("/refund/mongo/{credit_memo_id}","getCreditMemoMongo");

$collectionObj->put("/lazada/updateStatus","lazadaUpdateOrderStatus");
$collectionObj->put("/detail/orderAddress","updateOrderAddress");

/**
 * Marketplace Reports
 */
$collectionObj->get("/mp/settlement","getDetailMpSettlement");
$collectionObj->get("/mp/totalSettlement","getTotalMpSettlement");
$collectionObj->get("/mp/rekap","getMpRekap");

// insert giftcard from sales_order to salesrule_order
$collectionObj->get("/giftcard","getDataGiftCards");
$collectionObj->post("/giftcard","insertDataGiftCards");

// remove history voucher
$collectionObj->post("/removeHistoryVoucher", "removeHistoryVoucher");

return $collectionObj;

