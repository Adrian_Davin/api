<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ProductVariantController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/productVariant");

/*
 * This end point will handle for get all data from elastic such as :
 * - product detail
 * - product list (show category)
 * - product search
 */

// $collectionObj->get("/listProduct/custom", "listProductCustom");
// $collectionObj->get("/listQualityProduct","listQualityProduct");
// $collectionObj->get("/listVariantId","listVariantId");

// Setting route for API
$collectionObj->put("/updateProductImages","updateProductImages");

//$collectionObj->put("/changesupplier","changeProductSupplier");
$collectionObj->put("/changesupplierImage","changeProductSupplierImage");

$collectionObj->post("/importTempPriceZoneBatch","importTempPriceZoneBatch");
$collectionObj->put("/updateTempPriceZoneBatch/{method}","updateTempPriceZoneBatch");
$collectionObj->put("/updateProductPriceBatch","updateProductPriceBatch");
$collectionObj->put("/updatePriceBatch","updatePriceBatch");
$collectionObj->put("/updatePriceZoneBatch","updatePriceZoneBatch");
$collectionObj->get("/queryTempPricezone","queryTempPricezone");

$collectionObj->get("/commentHistory","getCommentHistory");
$collectionObj->post("/insertCommentHistory","insertCommentHistory");

$collectionObj->post("/saveProductStock","saveProductStock");
$collectionObj->post("/saveProductPrice","saveProductPrice");

$collectionObj->post("/saveProductCommission","saveProductCommission");
$collectionObj->post("/saveProductCommissionBatch","saveProductCommissionBatch");
$collectionObj->get("/productCommission/{sku}","getProductCommission");

$collectionObj->post("/saveProductNetSales","saveProductNetSales");
$collectionObj->post("/saveProductNetSalesBatch","saveProductNetSalesBatch");
$collectionObj->get("/productNetSales/{sku}","getProductNetSales");


$collectionObj->get("/getProductCommissionNetSalesFile","getProductCommissionNetSalesFile");

$collectionObj->get("/generateSku","generateSku");

$collectionObj->get("/totalQty","totalQty");

/**
 * Import Min Max
 */
$collectionObj->put("/updateMinMaxStock","updateMinMaxStock");

return $collectionObj;