<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 1:54 PM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\CompanyController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/company");

//route for company
$collectionObj->get("/list","getlistCompany");
$collectionObj->get("/companyWithAddress","getCompanyWithAddress");
$collectionObj->post("","saveCompany");
$collectionObj->put("/{company_id:[0-9]+}","saveCompany");
$collectionObj->delete("/{company_id}","deleteCompany");

// Setting route for API

return $collectionObj;