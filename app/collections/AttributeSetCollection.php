<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\AttributeSetController",true);

$collectionObj->setPrefix("/attributeSet");

$collectionObj->get("/list","listAttributeSet");

$collectionObj->get("/detail/{article_hierarchy}","detailAttributeSet");

$collectionObj->get("/2attribute","list2Attribute");
$collectionObj->put("/2attribute","update2Attributes");

$collectionObj->put("/","updateAttributeSet");
$collectionObj->post("/","createAttributeSet");

// Setting route for API

return $collectionObj;