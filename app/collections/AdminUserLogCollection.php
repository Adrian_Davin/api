<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\AdminUserLogController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/user/log");

// Setting route for API
$collectionObj->get("/","userLogList");
$collectionObj->get("/{id_user_log}","userLogDetails");
$collectionObj->post("/","createUserLog");

return $collectionObj;