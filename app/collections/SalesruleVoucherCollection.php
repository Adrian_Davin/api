<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 18/02/2017
 * Time: 4:12 PM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SalesruleVoucherController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/salesrule/voucher");

// Setting route for API
$collectionObj->get("/","getSalesVoucher");
$collectionObj->get("/{rule_id:[0-9]+}","listVoucher");
$collectionObj->get("/search","listVoucherSearch");
$collectionObj->get("/detail","detailVoucher");
$collectionObj->get("/total","totalVoucher");
$collectionObj->get("/usage","getVoucherUsage");
$collectionObj->get("/voucher_dashboard","totalVoucher");
$collectionObj->post("/","saveSalesruleVoucher");
$collectionObj->put("/{voucher_id}","saveSalesruleVoucher");
$collectionObj->put("/update/status","updateStatusVoucher");
$collectionObj->delete("/{voucher_id}","deleteSalesruleVoucher");

$collectionObj->post("/generate","generateVoucher");

/**
 * get remaining limit use of particular voucher_code
 */
$collectionObj->get("/remaining/{voucher_code:[a-zA-Z0-9]+}","getRemainingVoucher");

return $collectionObj;