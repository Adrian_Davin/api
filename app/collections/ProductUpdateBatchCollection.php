<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ProductUpdateBatchController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/productupdatebatch");

// Setting route for API
$collectionObj->get("/","updateBatchList");

$collectionObj->post("/","saveUpdateBatch");

$collectionObj->put("/","processUpdateBatch");

return $collectionObj;