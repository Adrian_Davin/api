<?php
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\VendorController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/vendor");

$collectionObj->put("/cancel_order","vendorCancelOrder");

// for oms, dont disturb
$collectionObj->get("/shopee/return","getReturnShopeeData");

$collectionObj->put("/shopee/return","updateReturnShopee");

$collectionObj->get("/shopee/order","getOrderShopee");

$collectionObj->get("/shopee/generate_escrow","getDataEscrow");

$collectionObj->get("/tokopedia/order","getOrderTokopedia");

$collectionObj->get("/tokopedia/generate_escrow","getDataEscrowTokopedia");

return $collectionObj;