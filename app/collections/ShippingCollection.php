<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ShippingController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/shipping");

// Setting route for API
$collectionObj->post("/generateawb","generateAwb");
$collectionObj->get("/carrier","getCarrierList");
$collectionObj->get("/getall","getAllCarrier");

// Deal with courier credential in redis
$collectionObj->post("/couriercredential","createCourierCredentialCache");
$collectionObj->delete("/couriercredential/{courier_account}","deleteCourierCredentialCache");

return $collectionObj;