<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\MarketingTukarvoucherController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/marketing_tukarvoucher");

// Setting route for API
$collectionObj->post("/","saveTukarvoucher");

return $collectionObj;