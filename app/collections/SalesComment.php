<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SalesCommentController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/salescomment");

// Setting route for API
$collectionObj->post("/","saveComment");
$collectionObj->get("/","getCommentList");
$collectionObj->get("/getcommentchat","getCommentChat");

return $collectionObj;