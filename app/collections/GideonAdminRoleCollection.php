<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\GideonAdminRoleController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/user/gideon/role");

// Setting route for API
$collectionObj->get("/","getGideonRoleList");
$collectionObj->get("/{role_id:[0-9]+}","getGideonRoleDetail");
$collectionObj->put("/{role_id:[0-9]+}","saveGideonAdminRole");
$collectionObj->post("/","saveGideonAdminRole");

return $collectionObj;