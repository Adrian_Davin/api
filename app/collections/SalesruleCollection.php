<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SalesruleController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/salesrule");

// Setting route for API
$collectionObj->post("/","saveSalesrule");
$collectionObj->post("/copyrule/{rule_id}","copySalesrule");
$collectionObj->put("/","saveSalesrule");

$collectionObj->get("/","listSalesrule");
$collectionObj->get("/detail/{rule_id}","detailSalesrule");
$collectionObj->get("/listAction","listAction");
$collectionObj->get("/action/{salesrule_action_id}","detailSalesruleAction");
$collectionObj->get("/action","listSalesruleAction");

$collectionObj->get("/pool","getSalesruleRegistrationPoolList");
$collectionObj->get("/pool/{salesrule_registration_pool_id}","getSalesruleRegistrationPoolDetail");
$collectionObj->post("/pool","saveSalesruleRegistrationPool");
$collectionObj->put("/pool","saveSalesruleRegistrationPool");

return $collectionObj;