<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\NegativeKeywordsController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/negativekeywords");

// Setting route for API
$collectionObj->post("/auth","authJWT");
$collectionObj->get("/listKeywords","listNegativeKeywords");
$collectionObj->post("/insert","createNegativeKeyword");
$collectionObj->post("/upload","uploadCsvFile");
$collectionObj->put("/{keyword_id}","updateNegativeKeyword");
$collectionObj->get("/search", "searchKeyword");
$collectionObj->delete("/{keyword_id}","deleteKeyword");

return $collectionObj;