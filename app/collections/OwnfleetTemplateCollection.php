<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\OwnfleetTemplateController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/ownfleet");

// Setting route for API
$collectionObj->get("/","templateList");
$collectionObj->get("/supplier/{supplier_id}","templateList");
$collectionObj->get("/{ownfleet_template_id}","getTemplateDetail");

$collectionObj->post("/","createTemplate");
$collectionObj->post("/upload","uploadSku");

$collectionObj->put("/{ownfleet_template_id}", "updateTemplate");

return $collectionObj;