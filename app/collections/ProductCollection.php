<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\ProductController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/products");

// Setting route for API
// $collectionObj->put("/price","updateProductPrice");

$collectionObj->put("/productChangeLog","productChangeLog");
$collectionObj->get("/productChangeLogDetail/{product_id}","productChangeLogDetail");

$collectionObj->get("/can_delivery","getProdctStockInLocation");

// This endpoint is already unmaintain, moved to go-stock
$collectionObj->get("/getMaxStock","getMaximumStock");

$collectionObj->get("/price/{sku}","getProductPriceBySKU");
$collectionObj->get("/variant/stock/{sku}","getProductVariantStock");
$collectionObj->put("/variant/stock/{sku}","saveProductVariantStock");
$collectionObj->put("/variant/stock/batch","updateBatchProductStock");
$collectionObj->put("/variant/stock/batchpromo1111","updateBatchProductStockPromo1111");
$collectionObj->put("/variant/stock/batchMultipleStore","updateBatchProductStockMultipleStore");
$collectionObj->put("/variant/stock/batch_by_stock_id","updateBatchProductStockByStockId");
$collectionObj->get("/variant/stock/getSkuDcBatch","getSkuDcBatch");
$collectionObj->get("/variant/stock/get_sku_store_batch","getSkuStoreBatch");
$collectionObj->put("/variant/stock/bufferByPriceAndQty","bufferByPriceAndQty");
$collectionObj->put("/variant/stock/reservedStockPendingPayment","reservedStockPendingPayment");
$collectionObj->put("/variant/stock/reservedStockPendingShipment","reservedStockPendingShipment");
$collectionObj->get("/variant/stock/getCountIsInStockTrue","getCountIsInStockTrue");
$collectionObj->get("/variant/stock/getIsInStockTrue","getIsInStockTrue");
$collectionObj->get("/variant/stock/getCountIsInStockFalse","getCountIsInStockFalse");
$collectionObj->get("/variant/stock/getIsInStockFalse","getIsInStockFalse");
$collectionObj->put("/variant/stock/isInStock","isInStock");
$collectionObj->put("/variant/stock/all_dc_stock_zero","allDcStockZero"); //Need for 1st step in calculate DC qty
$collectionObj->put("/variant/stock/dcZero","updateStockDcZero"); //by SKU
$collectionObj->put("/variant/stock/all_store_zero","allStoreStockZero"); //by SKU
$collectionObj->get("/variant/stock/getAllCategoryStockMapping","getAllCategoryStockMapping"); // get all is in stock in category stock mapping
$collectionObj->put("/variant/stock/categoryStockMapping","categoryStockMapping"); // mapping qty at table category stock mapping
$collectionObj->put("/variant/stock/updateQtyBk","updateQtyBk");
$collectionObj->put("/variant/stock/updateStockManual","updateStockManual");

// Save product variant to elastic
$collectionObj->get("/cache/{sku}","getFromCache");
$collectionObj->delete("/cache/{sku}","deleteFromCache");

// Last seen product
$collectionObj->post("/last/seen","saveLastSeenProduct");
$collectionObj->put("/last/seen/{last_seen_id}","saveLastSeenProduct");
$collectionObj->get("/last/seen/{last_seen_id}","getLastSeenProduct");

// import Product
$collectionObj->get("/import/attribute","getImportAttribute");
$collectionObj->get("/import/attributeEs","getImportAttributeES");

// Recommendation
$collectionObj->post("/recommendation","createRecommendation");
$collectionObj->put("/recommendation/{recommended_id}","updateRecommendation");
$collectionObj->get("/recommendation/{search_key}","recommendationDetail");
$collectionObj->get("/recommendation","recommendationList");

// Synonym
$collectionObj->post("/synonym","createSynonym");
$collectionObj->put("/synonym/{synonym_id}","updateSynonym");
$collectionObj->get("/synonym/{synonym_id}","synonymDetail");
$collectionObj->get("/synonym","synonymList");

//$collectionObj->get("/categoryBreakDown/{product_id}","categoryBreakDown");

// Remarketing
$collectionObj->get("/remarketing","getDataRemarketing");

// Populate AHI,TGI,HCI Products
$collectionObj->put("/populate_sisco","populateSiscoProducts");

// Revert Stock
$collectionObj->put("/revert_stocks","revertStocks");

// Change Store Code
$collectionObj->put("/change_store_code","changeStoreCode");

// Update Is In Stock
/**
 * Sudah tidak terpakai
 */
$collectionObj->put("/stock/{sku}","changeStockStatus");

// Update Category Is In Stock
$collectionObj->put("/stock/category/{sku}","changeCategoryStockStatus");

// Product Price Zone 
$collectionObj->get("/handling_fee_adjust", "getProductHandlingFeeAdjust");

return $collectionObj;