<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/12/2016
 * Time: 9:33 AM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\TtpuController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/ttpu");

/*
 * This end point will handle for get all data from elastic such as :
 * - product detail
 * - product list (show category)
 * - product search
 */
$collectionObj->post("/create","createTtpu");
$collectionObj->put("/update/status","updateStatus");
$collectionObj->get("/list","listTtpu");
$collectionObj->get("/detail/{ttpu_no:[a-zA-Z0-9]+}","detailTtpu");

// Setting route for API

return $collectionObj;