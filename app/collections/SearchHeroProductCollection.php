<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 8/22/2017
 * Time: 2:58 PM
 */

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SearchHeroProductController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/search/heroproduct");

// Setting route for API
$collectionObj->get("/{search_hero_product_id}","getListHeroProduct");
$collectionObj->get("/listHeroProduct","getListHeroProduct");
$collectionObj->post("/","saveHeroProduct");
$collectionObj->put("/{search_hero_product_id}","saveHeroProduct");
$collectionObj->delete("/{search_hero_product_id}","deleteHeroProduct");

return $collectionObj;