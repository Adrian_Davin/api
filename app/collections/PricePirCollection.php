<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\PricePirController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/pricepir");

$collectionObj->put("/","getChunkedDataDetails");
$collectionObj->post("/","insertPricePir");
$collectionObj->put("/compare","comparePir");
$collectionObj->put("/updateIsInStock","updateIsInStockAfterCompare");
$collectionObj->get("/getCountProductPrice","getCountProductPrice");
$collectionObj->get("/getProductPrice","getProductPrice");

return $collectionObj;