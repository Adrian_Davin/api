<?php
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\MasterController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/master");

// Setting route for API
$collectionObj->get("/country","getCountryList");
$collectionObj->get("/country/{country_id}","getCountryData");
$collectionObj->get("/province","getProvinceList");
$collectionObj->get("/province/{province_id}","getProvinceData");
$collectionObj->get("/city","getCityList");
$collectionObj->get("/city/{city_id}","getCityData");
$collectionObj->get("/kecamatan","getKecamatanList");
$collectionObj->get("/kecamatan/{kecamatan_id}","getKecamatanData");
$collectionObj->get("/kecamatan-vendor","getKecamatanVendor");
$collectionObj->get("/kelurahan","getKelurahanList");
$collectionObj->get("/kelurahan/{kelurahan_id}","getKelurahanData");
$collectionObj->get("/kelurahan-vendor","getKelurahanVendor");
$collectionObj->get("/rejectReason","getRejectReasonList");
$collectionObj->get("/master_brand_store", "getMasterBrandStoreList");
$collectionObj->post("/ccbank","createBinListCache");
$collectionObj->delete("/ccbank/{bin}","deleteBinCache");

//mapping for lazada
$collectionObj->get("/coloursMapping","getColoursMapping");
$collectionObj->get("/brandMapping","getBrandMapping");

//healthcheck
$collectionObj->get("/healthcheck","healthCheck");

return $collectionObj;