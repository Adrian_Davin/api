<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\BankController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/bank");

// Setting route for API
$collectionObj->get("/installment","getBankInstallmentList");

return $collectionObj;