<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\NotificationsController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/notifications");

// Setting route for API
$collectionObj->post("/location","checkLocation");

// Slack Notification
$collectionObj->put("/slack","slackNotification");


return $collectionObj;