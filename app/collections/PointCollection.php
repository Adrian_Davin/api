<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\PointController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/point");

// Setting route for API
$collectionObj->get("/","pointCheck");
$collectionObj->post("/","redeemPoint");

return $collectionObj;