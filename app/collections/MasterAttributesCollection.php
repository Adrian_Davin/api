<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\MasterAttributesController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/master/attributes");

// Setting route for API
$collectionObj->get("/","getListMasterAttributes");
$collectionObj->get("/set/{attribute_set_id:[0-9]+}","getListMasterAttributesOnSet");
$collectionObj->get("/variant/set/{attribute_variant_set_id:[0-9]+}","getListMasterAttributesOnVariantSet");
$collectionObj->get("/variant/option/{option_id:[0-9]+}","getVariantOption");
$collectionObj->post("/","saveMasterAttributes"); // add using batchprocessor`
$collectionObj->post("/add","addMasterAttributes");
$collectionObj->post("/update","updateMasterAttributes");

return $collectionObj;