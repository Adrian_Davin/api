<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SettingDataController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/settingdata");

// Setting route for API
$collectionObj->get("/","settingDetail");
$collectionObj->put("/{setting_data_id}","updateSettingData");

return $collectionObj;