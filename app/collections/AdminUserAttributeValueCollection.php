<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 14/3/2017
 * Time: 3:22 AM
 */
$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\AdminUserAttributeValueController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/user/attribute/value");

// Setting route for API
$collectionObj->post("/","saveAdminUserAttributeValue");
$collectionObj->put("/{admin_user_attribute_value_id}","saveAdminUserAttributeValue");
$collectionObj->get("/","userAttributeValueList");

return $collectionObj;