<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\PromoLocalController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/promo/local");

// Setting route for API
$collectionObj->get("/","getPromoLocal");
$collectionObj->post("/","importPromoLocal");

return $collectionObj;