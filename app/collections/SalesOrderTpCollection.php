<?php

$collectionObj = new \Phalcon\Mvc\Micro\Collection();

// Set the main handler. ie. a controller instance
$collectionObj->setHandler("\Controllers\SalesOrderTpController",true);

// Set a common prefix for all routes
$collectionObj->setPrefix("/salesOrderTp");

$collectionObj->post("/saveStockToMysql","saveStockToMysql");
$collectionObj->put("/createRevertTp","createRevertTp");
$collectionObj->put("/checkExistTp","checkExistTp");
$collectionObj->put("/recreateStockTp","recreateStockTp");
$collectionObj->get("/getSalesOrderTpDetail/{order_no:[a-zA-Z0-9]+}","getSalesOrderTpDetail");
$collectionObj->get("/checkOrderStatus/{order_no:[a-zA-Z0-9]+}","checkOrderStatus");
$collectionObj->get("/checkInvoiceStatus/{order_no:[a-zA-Z0-9]+}","checkInvoiceStatus");
$collectionObj->put("/createSlackNotification","createSlackNotification");
$collectionObj->get("/getErrorTpData","getErrorTpData");
$collectionObj->get("/countErrorTp","countErrorTp");


return $collectionObj;