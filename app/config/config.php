<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

return new \Phalcon\Config([
    'dbMaster' => [
        'adapter'    => 'Mysql',
        'host'       => $_ENV['DB_HOST_MASTER'],
        'username'   => $_ENV['DB_USER_MASTER'],
        'password'   => $_ENV['DB_PASS_MASTER'],
        'dbname'     => $_ENV['DB_NAME_MASTER'],
        'charset'    => 'utf8',
    ],

    'dbReader' => [
        'adapter'    => 'Mysql',
        'host'       => $_ENV['DB_HOST_READER'],
        'username'   => $_ENV['DB_USER_READER'],
        'password'   => $_ENV['DB_PASS_READER'],
        'dbname'     => $_ENV['DB_NAME_READER'],
        'charset'    => 'utf8',
    ],

    'dbSlave' => [
        'adapter'    => 'Mysql',
        'host'       => $_ENV['DB_HOST_SLAVE'],
        'username'   => $_ENV['DB_USER_SLAVE'],
        'password'   => $_ENV['DB_PASS_SLAVE'],
        'dbname'     => $_ENV['DB_NAME_SLAVE'],
        'charset'    => 'utf8',
    ],

        'dbRevive' => [
    'adapter'    => 'Mysql',
    'host'       => $_ENV['DB_HOST_REVIVE'],
    'username'   => $_ENV['DB_USER_REVIVE'],
    'password'   => $_ENV['DB_PASS_REVIVE'],
    'dbname'     => $_ENV['DB_NAME_REVIVE'],
    'charset'    => 'utf8',
]
]);
