<?php

use Phalcon\Mvc\View\Simple as View;
use Phalcon\Mvc\Url as UrlResolver;

/**
 * Shared loader service
 */
$di->setShared('loader', function () {

    /**
     * Include Autoloader
     */
    return include __DIR__ . '/loader.php';
});

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include __DIR__ . "/config.php";
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('dbMaster', function () {
    $config = $this->getConfig();

    $dbConfig = $config->dbMaster->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

$di->setShared('dbReader', function () {
    $config = $this->getConfig();

    $dbConfig = $config->dbReader->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

$di->setShared('dbSlave', function () {
    $config = $this->getConfig();

    $dbConfig = $config->dbSlave->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

$di->setShared('dbRevive', function () {
    $config = $this->getConfig();

    $dbConfig = $config->dbRevive->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

// Set the models cache service
$di->set(
    "modelsCache",
    function () {
        // Cache data for one day by default
        $frontCache = new Phalcon\Cache\Frontend\Data(
            [
                "lifetime" => 86400,
            ]
        );

        // Memcached connection settings
        $cache = new Phalcon\Cache\Backend\Redis(
            $frontCache,
            [
                'host' => $_ENV['MODELCACHE_REDIS_HOST'],
                'port' => $_ENV['MODELCACHE_REDIS_PORT'],
                'persistent' => false,
                'index' => $_ENV['MODELCACHE_REDIS_DB'],
            ]
        );

        return $cache;
    }
);
