<?php

include __DIR__ . '/../../vendor/autoload.php';

/**
 * Load our config
 */

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../../');
$dotenv->load();


/**

 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

// Register namespaces
$loader->registerNamespaces(
    array(
        'Controllers' => APP_PATH . "/controllers",
        'Models' => APP_PATH . "/models",
        'Library' => APP_PATH . "/library",
        'Helpers' => APP_PATH . "/helpers",
    )
)->register();

return $loader;