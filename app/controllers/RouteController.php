<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 13/1/2017
 * Time: 2:23 PM
 */

namespace Controllers;

/**
 * Class ControllerNameController
 * @package Controllers
 */
class RouteController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getAllRouting()
    {
        $routerLib = new \Library\Route();
        $allRoute = $routerLib->getAllRoute($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($routerLib->getErrorCode())) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Route not found");
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allRoute;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getRouting()
    {
        $routerLib = new \Library\Route();
        $allRoute = $routerLib->getRouteDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(empty($allRoute)) {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
        } else {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        }

        if(!empty($routerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $routerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($routerLib->getErrorCode()),
                "messages" => $routerLib->getErrorMessages()
            );
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $allRoute;
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function checkRoutingByIdentifier(){
        $routerLib = new \Library\Route();
        $route = $routerLib->checkIdentifier($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!$route) {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
        } else {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        }

        if(!empty($routerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $routerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($routerLib->getErrorCode()),
                "messages" => $routerLib->getErrorMessages()
            );
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $route;
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveRouting()
    {
        $routerLib = new \Library\Route();
        $result = $routerLib->saveData($this->params);

        $rupaResponse = new \Library\Response();

        if(!empty($routerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $routerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($routerLib->getErrorCode()),
                "messages" => $routerLib->getErrorMessages());
            $responseContent['data'] = array();
            $responseContent['messages'] = array('Save Failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("Success");
        }

        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode($routerLib->getStatusCode(), $routerLib->getStatusCodeMessage())->sendHeaders();
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function hardDelete()
    {
        $routerLib = new \Library\Route();
        $result = $routerLib->hardDeleteData($this->params);

        $rupaResponse = new \Library\Response();

        if(!empty($routerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $routerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($routerLib->getErrorCode()),
                "messages" => $routerLib->getErrorMessages());
            $responseContent['data'] = array();
            $responseContent['messages'] = array('Delete Failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("Success");
        }

        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode($routerLib->getStatusCode(), $routerLib->getStatusCodeMessage())->sendHeaders();
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateRouting(){
        $routerLib = new \Library\Route();
        $result = $routerLib->updateData($this->params);

        $rupaResponse = new \Library\Response();

        if(!empty($routerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $routerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($routerLib->getErrorCode()),
                "messages" => $routerLib->getErrorMessages());
            $responseContent['data'] = array();
            $responseContent['messages'] = array('Update Failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("Success");
        }

        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode($routerLib->getStatusCode(), $routerLib->getStatusCodeMessage())->sendHeaders();
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function changeRoutingParent(){
        $routerLib = new \Library\Route();
        $result = $routerLib->changeParent($this->params);

        $rupaResponse = new \Library\Response();

        if(!empty($routerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $routerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($routerLib->getErrorCode()),
                "messages" => $routerLib->getErrorMessages());
            $responseContent['data'] = array();
            $responseContent['messages'] = array('Update Failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("Success");
        }

        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode($routerLib->getStatusCode(), $routerLib->getStatusCodeMessage())->sendHeaders();
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}