<?php
namespace Controllers;

use Models\PromoLocal;

/**
 * Class PromoLocalController
 * @package Controllers
 */
class PromoLocalController extends \Controllers\BaseController
{
    /**
     * @var $promoLocal \Models\PromoLocal
     */
    protected $promoLocal;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->promoLocal = new \Models\PromoLocal();
    }

    public function getPromoLocal()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $result = $this->promoLocal->find();

        if ($result) {
            $response['data'] = $result->toArray();
            $response['messages'] = "success";
        } else {
            $response['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get sku list failed, please contact ruparupa tech support"
            );
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function importPromoLocal(){
        $promoLocalModel = new PromoLocal();
        $result = $promoLocalModel->importProduct($this->params);

        $responseContent = array();
        $rupaResponse = new Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($promoLocalModel->getErrorCode())){
            $responseContent['errors'] = array(
                "code" => $promoLocalModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($promoLocalModel->getErrorCode()),
                "messages" => $promoLocalModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        }
        else{
            $responseContent['errors'] = array();
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = array();
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}