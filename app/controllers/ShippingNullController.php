<?php

namespace Controllers;


/**
 * Class ShippingNullController
 * @package Controllers
 * @RoutePrefix("/shippingNull")
 *
 */
class ShippingNullController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }


    public function updateStock(){
        // echo "masuk\n";

        // insert to mysql
        $affectedRows = 0;
        if(sizeof($this->params['sku']) > 1){
            //store_code = DC
            $values = "";
            $sql = "insert into product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) values "; 
            $valuesArray = array();   
            foreach( $this->params['sku'] as $val){
                $valueString = "('".$val."','DC',0,'odi','".date('Y-m-d 00:00:00')."','".date('Y-m-d 00:00:00',strtotime('+1 day'))."')";
                array_push($valuesArray, $valueString);
            }  
            $values = implode(',',$valuesArray);
            $sql = $sql . $values;
            $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();

            //store_code = 1000DC
            $values = "";
            $sql = "insert into product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) values "; 
            $valuesArray = array();   
            foreach( $this->params['sku'] as $val){
                $valueString = "('".$val."','1000DC',0,'odi','".date('Y-m-d 00:00:00')."','".date('Y-m-d 00:00:00',strtotime('+1 day'))."')";
                array_push($valuesArray, $valueString);
            }  
            $values = implode(',',$valuesArray);
            $sql = $sql . $values;
            $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();

             //store_code = ALLSTOREFULFILLMENT
            $valuesAllStoreFulfillment = "";
            $sql = "insert into product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) values "; 
            $valuesArray = array();   
            foreach( $this->params['sku'] as $val){
                $valueString = "('".$val."','ALLSTOREFULFILLMENT',0,'odi','".date('Y-m-d 00:00:00')."','".date('Y-m-d 00:00:00',strtotime('+1 day'))."')";
                array_push($valuesArray, $valueString);
            }  
            $valuesAllStoreFulfillment = implode(',',$valuesArray);
            $sql = $sql . $valuesAllStoreFulfillment;
            $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();
       
           
        }else{
            $skuCompare = $this->params['sku'];
            //store_code = DC
            $sql = "insert into product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) values ";
            $valueString = "('".$skuCompare."','DC',0,'odi','".date('Y-m-d 00:00:00')."','".date('Y-m-d 00:00:00',strtotime('+1 day'))."')";
            $sql = $sql . $valueString;
            $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();

            //store_code = 1000DC
            $sql = "insert into product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) values ";
            $valueString = "('".$skuCompare."','1000DC',0,'odi','".date('Y-m-d 00:00:00')."','".date('Y-m-d 00:00:00',strtotime('+1 day'))."')";
            $sql = $sql . $valueString;
            $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();
            
            //store_code = ALLSTOREFULFILLMENT
            $sql = "insert into product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) values ";
            $valueString = "('".$skuCompare."','ALLSTOREFULFILLMENT',0,'odi','".date('Y-m-d 00:00:00')."','".date('Y-m-d 00:00:00',strtotime('+1 day'))."')";
            $sql = $sql . $valueString;
            $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();
        }

        $final = array(
            'updated_rows' => $affectedRows,
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success", $affectedRows);
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

        return $final;
    }
}