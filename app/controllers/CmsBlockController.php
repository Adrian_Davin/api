<?php


namespace Controllers;

class CmsBlockController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function cmsBlockList(){
        $cmsBlockLib = new \Library\CmsBlock();
        $cmsBlockList = $cmsBlockLib->getAllCmsBlock($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($cmsBlockLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $cmsBlockLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($cmsBlockLib->getErrorCode()),
                "messages" => $cmsBlockLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create page");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $cmsBlockList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function cmsBlockDetail(){
        $cmsBlockLib = new \Library\CmsBlock();
        $cmsBlockDetail = $cmsBlockLib->getCmsBlockDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($cmsBlockLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $cmsBlockLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($cmsBlockLib->getErrorCode()),
                "messages" => $cmsBlockLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create page");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $cmsBlockDetail;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createCmsBlock(){
        $cmsBlockModel = new \Models\CmsBlock();

        $cmsBlockModel->setFromArray($this->params);
        $result = $cmsBlockModel->saveData('create_cms_block');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if($result == false) {
            $responseContent['errors'] = array(
                "code" => "RR303",
                "title" => $rupaResponse->getResponseDescription("RR303"),
                "messages" => "Creating cms failed. Please try again."
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $result;
        }

        $responseContent['messages'] = array("success");
        $responseContent['data'] = "";

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateCmsBlock(){
        $cmsBlockModel = new \Models\CmsBlock();

        $cmsBlockModel->setFromArray($this->params);
        $result = $cmsBlockModel->saveData('update_cms_block');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if($result == false) {
            $responseContent['errors'] = array(
                "code" => "RR303",
                "title" => $rupaResponse->getResponseDescription("RR303"),
                "messages" => "Updating failed. Please try again."
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $result;
        }

        $responseContent['messages'] = array("success");
        $responseContent['data'] = "";

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

}