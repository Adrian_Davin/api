<?php


namespace Controllers;

/**
 * Class SalesruleCouponController
 * @package Controllers
 * @RoutePrefix("/salesrule/coupon")
 *
 */
class SalesruleVoucherController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function saveSalesruleVoucher()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $salesruleVoucher = new \Models\SalesruleVoucher();
        $salesruleVoucher->setFromArray($this->params);
        $result = $salesruleVoucher->find(" voucher_code = '".$salesruleVoucher->getVoucherCode()."' and voucher_id <> ".$salesruleVoucher->getVoucherId());
        if(count($result->toArray()) > 0){
            $response['errors'] = array("code" => "RR102",
                "title" => $rupaResponse->getResponseDescription("RR102"),
                "messages" => "Duplicate Voucher Code");
            $response['messages'] = array("failed");
        }else{
            $salesruleVoucher->saveData("salesrule_voucher");
            if(!empty($salesruleVoucher->getErrorCode())) {
                $response['errors'] = array(
                    "code" => $salesruleVoucher->getErrorCode() ,
                    "title" => $rupaResponse->getResponseDescription($salesruleVoucher->getErrorCode()),
                    "message" => $salesruleVoucher->getErrorMessages());

                $response['messages'] = array('failed');
            } else {
                $response['messages'] = array("success");
            }
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function deleteSalesruleVoucher()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $salesruleVoucher = new \Models\SalesruleVoucher();
        $result = $salesruleVoucher->deleteSalesruleCoupon($this->params);
        if (isset($result['data'])) $response['data'] = $result['data'];
        if (isset($result['messages'])) $response['messages'] = $result['messages'];
        if (isset($result['error'])) $response['error'] = $result['error'];
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateStatusVoucher()
    {
        $response = array(
            'data' => '',
            'error' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $salesruleVoucher = new \Models\SalesruleVoucher();
        $result = $salesruleVoucher->updateStatusVoucher($this->params);
        if (isset($result['data'])) $response['data'] = $result['data'];
        if (isset($result['messages'])) $response['messages'] = $result['messages'];
        if (isset($result['errors'])) $response['errors'] = $result['errors'];
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function generateVoucher()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if (isset($this->params['confirm_check']) && $this->params['confirm_check'] === 'true') {
            $salesruleVoucher = new \Models\SalesruleVoucher();
            $return = $salesruleVoucher->checkVoucherGenerateQuantity($this->params['rule_id']);

            if (!$return['return']) {
                $response['errors'] = array("code" => "RR302",
                "title" => $this->rupaResponse->getResponseDescription("RR302"),
                "messages" => "Data not found");
                $response['messages'] = array("failed");
            }
            else {
                if ($return['check']) {
                    $response['data'] = array("true");
                    $response['messages'] = array("check success, voucher exist");

                    $rupaResponse->setContent($response);
                    $rupaResponse->send();
                    $response['errors'] = array();
                    return;
                }        
            }
            
        }

        $marketingLib = new \Library\Marketing();
        $marketingLib->generateVoucher($this->params);
        $marketingLib->validateGeneratedVoucher($this->params);
        $marketingLib->saveGeneratedVoucher($this->params);

        if(!empty($marketingLib->getErrorCode())) {
            $response['errors'] = array("code" => $marketingLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($marketingLib->getErrorCode()),
                "messages" => $marketingLib->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $marketingLib->getVoucherList();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function listVoucher()
    {
        $fixResult = array();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $param = $this->params;

        $salesruleVoucher = new \Models\SalesruleVoucher();

        $additionalCon = '';
        if(isset($this->params['voucher_code'])){
            $additionalCon = " and voucher_code like '%".$param['voucher_code']."%' ";
        }
        $vouchers = $salesruleVoucher->find(
                array(
                    "conditions" =>"rule_id = ".$param['rule_id']." ".$additionalCon." and status > -1 ",
                    "columns" => "voucher_code",
                    "limit" => $param['limit'],
                    "offset" => $param['offset']
                )
        );

        $vouchersCount = $salesruleVoucher->count(
            array(
                "conditions" =>"rule_id = ".$param['rule_id']." ".$additionalCon." and status > -1 ",
                "columns" => "voucher_code"
            )
        );

        $result = array();
        if($vouchers){

            $salesruleLib = new \Library\SalesRule();
            foreach($vouchers->toArray() as $row){
                $result[] = $salesruleLib->getVoucherDetail($row);
            }
        }

        $fixResult['recordsTotal'] = $vouchersCount;
        $fixResult['list'] = $result;

        $response['errors'] = array();
        $response['data'] = $fixResult;
        $response['messages'] = array("success");

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function listVoucherSearch()
    {
        $fixResult = array();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $param = $this->params;

        $salesruleVoucher = new \Models\SalesruleVoucher();

        $additionalCon = '';
        if(isset($this->params['voucher_code'])){
            $additionalCon = "voucher_code like '%".$param['voucher_code']."%' ";
        }

        if(isset($this->params['email'])){
            $customerModel = new \Models\Customer();
            $paramEmail['email'] = $this->params['email'];
            $hasil = $customerModel->getCustomerIdByEmail($paramEmail);
            $custIdArray = array();
            if (count($hasil) > 0) {
                foreach($hasil as $row) {
                    $custIdArray[] = $row['customer_id'];
                }
            }

            if (count($custIdArray) > 0) {
                $in_string = implode(",", $custIdArray);
                $additionalCon = "customer_id in (".$in_string.") ";
            }
        }

        $vouchers = $salesruleVoucher->find(
            array(
                "conditions" => $additionalCon . " and status > -1 ",
                "columns" => "voucher_code",
                "limit" => $param['limit'],
                "offset" => $param['offset']
            )
        );

        $vouchersCount = $salesruleVoucher->count(
            array(
                "conditions" => $additionalCon . " and status > -1 ",
                "columns" => "voucher_code"
            )
        );

        $result = array();
        if($vouchers){
            $salesruleLib = new \Library\SalesRule();
            foreach($vouchers->toArray() as $row){
                $result[] = $salesruleLib->getVoucherDetail($row);
            }
        }

        $fixResult['recordsTotal'] = $vouchersCount;
        $fixResult['list'] = $result;

        $response['errors'] = array();
        $response['data'] = $fixResult;
        $response['messages'] = array("success");

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function detailVoucher()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;

        $salesruleLib = new \Library\SalesRule();
        $result = $salesruleLib->getVoucherDetail($params);

        if($result){

            $response['errors'] = array();
            $response['data'] = $result;
            $response['messages'] = array("success");
        }else{
            $response['errors'] = array("code" => $salesruleLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesruleLib->getErrorCode()),
                "messages" => $salesruleLib->getErrorMessages());
            $response['messages'] = array("failed");

        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function totalVoucher()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;

        $salesruleLib = new \Library\SalesRule();
        $result = $salesruleLib->getVoucherTotal($params);

        if($result){

            $response['errors'] = array();
            $response['data'] = $result;
            $response['messages'] = array("success");
        }else{
            $response['errors'] = array("code" => $salesruleLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesruleLib->getErrorCode()),
                "messages" => $salesruleLib->getErrorMessages());
            $response['messages'] = array("failed");

        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getVoucherUsage() {
        $fixResult = array();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $salesruleVoucher = new \Models\SalesruleVoucher();

        $voucherCode = $this->params["voucher_code"];
        $voucherLimit = $salesruleVoucher->getVoucherUsageAndLimit($voucherCode);

        $fixResult['recordsTotal'] = $voucherLimit ? 1 : 0;
        $fixResult['list'] = array($voucherLimit);

        $response['errors'] = array();
        $response['data'] = $fixResult;
        $response['messages'] = array("success");

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getSalesVoucher()
    {
        $salesRuleLib = new \Library\SalesRule();
        $allSalesVoucher = $salesRuleLib->getAllSalesVoucher($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($allSalesVoucher['list'])) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Sales voucher not found");
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allSalesVoucher['list'];
            $response['messages'] = array("result" => "success","recordsTotal" => $allSalesVoucher['recordsTotal']);
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getRemainingVoucher()
    {
        $salesRuleLib = new \Library\SalesRule();
        $allSalesVoucher = $salesRuleLib->getAllSalesVoucher(['voucher_code' => $this->params['voucher_code']]);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($allSalesVoucher['list']) || count($allSalesVoucher['list']) > 1) {
            $response['errors'] = array("code" => "RR302",
                                        "title" => $rupaResponse->getResponseDescription("RR302"),
                                        "messages" => "Sales voucher not found");
            $response['messages'] = array("result" => "failed");
            $rupaResponse->setHeaderStatus('404');
        } else {
            $remaining = $allSalesVoucher['list'][0]['limit_used'] - $allSalesVoucher['list'][0]['times_used'];
            $response['errors'] = array();
            $response['data'] = ['remaining' => $remaining > 0 ? $remaining : 0];
            $response['messages'] = array("result" => "success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();

    }
}