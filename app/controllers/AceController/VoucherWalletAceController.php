<?php

namespace Controllers\AceController;

use Controllers\BaseController;
use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Library\Response;
use Library\KawanLamaSystem;
use Library\Marketing;
use Library\AceHardware\PayWithPoint;

class VoucherWalletAceController extends BaseController
{
    protected $kawanLamaSystemLib;
    protected $marketingLib;
    protected $payWithPointLib;
    protected $rrResponse;

    public function onConstruct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();
        $this->marketingLib = new Marketing();
        $this->payWithPointLib = new PayWithPoint();

        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "OK")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"])
    {
        if ($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = [];
        $response["errors"] = [
            "code" => $errorCode,
            "title" => $this->rrResponse->getResponseDescription($errorCode),
            "messages" => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function redeemPointToVoucher()
    {
        $requiredParamFields = ["customer_id", "otp_number", "point_redeem", "is_special_bundling"];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields, false);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        if ((int)$this->params['point_redeem'] < 0) {
            return $this->sendErrorResponse(
                400,
                'Point yang ditukarkan tidak boleh kurang dari 0'
            );
        }

        try {
            $customerDetail = $this->kawanLamaSystemLib->getCustomerDetail("customer_id = " . $this->params['customer_id']);
            if (!$customerDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $this->params['customer_id']);
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }
            if (substr($customerAceDetail['ace_customer_member_no'], 0, 3) === 'TAM') {
                return $this->sendErrorResponse(
                    400,
                    'Customer dengan member TAM tidak dapat menukarkan poin ke voucher'
                );
            }

            $customerAceHO = $this->kawanLamaSystemLib->getCustomerDataAce($customerAceDetail);
            if (!$customerAceHO) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $customerPoints = 0;
            $customerPoints = (int)substr($customerAceHO[0]['Jml_Point'], 0, strpos($customerAceHO[0]['Jml_Point'], ' '));
            if ((int) $this->params['point_redeem'] > $customerPoints) {
                return $this->sendErrorResponse(
                    400,
                    'Point yang ditukarkan tidak boleh lebih dari poin yang dimiliki'
                );
            }

            $now = new \DateTime("now");
            $expDate = new \DateTime($customerAceHO[0]['Exp_Date']);
            if ($now > $expDate) {
                return $this->sendErrorResponse(
                    400,
                    'Masa berlaku membership anda sudah berakhir'
                );
            }

            $isValidOTP = $this->kawanLamaSystemLib->validateOtpAce([
                'action' => 'redeem_point',
                'customer_id' => $this->params['customer_id'],
                'phone' => GeneralHelper::formatPhoneNumber($customerDetail['phone']),
                'otp_number' => $this->params['otp_number']
            ]);
            if (!$isValidOTP) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $arraySpecialBundlingValidDate = explode(',', getenv('REDEEM_VOUCHER_SPECIAL_BUNDLING_VALID_DATE'));
            if (
                $this->params['is_special_bundling'] &&
                !in_array(date('Y-m-d'), $arraySpecialBundlingValidDate)
            ) {
                return $this->sendErrorResponse(
                    400,
                    'Periode special bundling tidak dapat dilakukan pada hari ini!'
                );
            }

            $voucherGeneratorParams = array();
            if (
                $this->params['is_special_bundling'] &&
                (int)$this->params['point_redeem'] == 100
            ) {
                $voucherGeneratorParams = [
                    'rule_id' => getenv('REDEEM_VOUCHER_100_RULE_ID'),
                    'customer_id' => $this->params['customer_id'],
                    'company_code' => 'ODI',
                    'type' => 5, // Type = redeem
                    'expiration_date' => getenv('REDEEM_VOUCHER_SPECIAL_BUNDLING_EXPIRED'),
                    'qty' => 1,
                    'voucher_amount' => 300000, // 100 * 3000
                    'length' => 7,
                    'format' => 'alphanum',
                    'prefix' => getenv('POINT_REDEEM_PREFIX_HO'),
                    'suffix' => 'S'
                ];
            } else if (
                $this->params['is_special_bundling'] &&
                (int)$this->params['point_redeem'] == 35
            ) {
                $voucherGeneratorParams = [
                    'rule_id' => getenv('REDEEM_VOUCHER_35_RULE_ID'),
                    'customer_id' => $this->params['customer_id'],
                    'company_code' => 'ODI',
                    'type' => 5, // Type = redeem
                    'expiration_date' => getenv('REDEEM_VOUCHER_SPECIAL_BUNDLING_EXPIRED'),
                    'qty' => 1,
                    'voucher_amount' => 100000, // 100 * 3000
                    'length' => 7,
                    'format' => 'alphanum',
                    'prefix' => getenv('POINT_REDEEM_PREFIX_HO'),
                    'suffix' => 'S'
                ];
            } else {
                $voucherGeneratorParams = [
                    'rule_id' => getenv('REDEEM_VOUCHER_RULE_ID'),
                    'customer_id' => $this->params['customer_id'],
                    'company_code' => 'ODI',
                    'type' => 5, // Type = redeem
                    'expiration_date' => date("Y-m-d 23:59:59", strtotime("+6 month", time())),
                    'qty' => 1,
                    'voucher_amount' => (int)$this->params['point_redeem'] * (int)getenv('POINT_VOUCHER_WORTH_AHI'), // n * 2500
                    'length' => 7,
                    'format' => 'alphanum',
                    'prefix' => getenv('POINT_REDEEM_PREFIX_HO'),
                ];
            }
            $this->marketingLib->generateVoucher($voucherGeneratorParams);
            $this->marketingLib->validateGeneratedVoucher($voucherGeneratorParams);


            $voucherGeneratorParams['voucher_code'] = $this->marketingLib->getVoucherList()[0];

            $salesRuleVoucher = $this->payWithPointLib->saveAndProcessPointRedeem(
                $this->params['customer_id'],
                $this->params['point_redeem'],
                $voucherGeneratorParams,
                $customerAceDetail
            );
            if (!$salesRuleVoucher) {
                return $this->sendErrorResponse(
                    $this->payWithPointLib->getErrorCode(),
                    $this->payWithPointLib->getErrorMessages()
                );
            }

            // Posting Journal 3601
            $this->kawanLamaSystemLib->postingJournal($salesRuleVoucher);
        } catch (\Exception $ex) {
            LogHelper::log("redeem_point_voucher_wallet", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,CUSTOMER ID: " . $this->params['customer_id']);
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }
}
