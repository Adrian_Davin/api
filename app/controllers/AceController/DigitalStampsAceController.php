<?php

namespace Controllers\AceController;

use Controllers\BaseController;
use Exception;
use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Library\Response;
use Library\KawanLamaSystem;
use Library\APIWrapper;

class DigitalStampsAceController extends BaseController
{
    protected $kawanLamaSystemLib;
    protected $rrResponse;

    public function onConstruct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();

        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "OK")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"])
    {
        if ($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = [];
        $response["errors"] = [
            "code" => $errorCode,
            "title" => $this->rrResponse->getResponseDescription($errorCode),
            "messages" => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function refreshStamps($customer_id)
    {
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $customer_id);
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $dateFrom = getenv('DIGITAL_STAMP_START_DATE');
            $dateUntil = date("Y-m-d");
            $transactionHistoryAce = $this->kawanLamaSystemLib->getTransactionHistoryAce(
                'AHI',
                '1',
                '999',
                $dateFrom,
                $dateUntil,
                "offline",
                $customerAceDetail["customer_token"],
                $customerAceDetail["ace_customer_id"]
            );

            if (!$transactionHistoryAce)
                return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

            $marketingStampRequest = [
                'ace_customer_id' => $customerAceDetail['ace_customer_id'],
                'receives' => []
            ];
            if (is_array($transactionHistoryAce["rows"]) && count($transactionHistoryAce["rows"]) > 0) {
                $receives = [];
                for ($i = 0; $i < count($transactionHistoryAce["rows"]); $i++) {
                    array_push($receives, [
                        'receive_no' => $transactionHistoryAce["rows"][$i]['receive_no'],
                        'company_name' => $transactionHistoryAce["rows"][$i]['company_name'],
                        'trans_date' => $transactionHistoryAce["rows"][$i]['trans_date'],
                        'trans_type' => $transactionHistoryAce["rows"][$i]['trans_type'],
                        'amount' => $transactionHistoryAce["rows"][$i]['amount'],
                        'jml_point' => $transactionHistoryAce["rows"][$i]['jml_point'],
                    ]);
                }
                $marketingStampRequest['receives'] = $receives;
            }

            // Directly assign voucher to existing cart
            $apiWrapper = new APIWrapper(getenv('CART_API'));
            $apiWrapper->setEndPoint("/marketing/stamp/offline");
            $apiWrapper->setParam($marketingStampRequest);
            if ($apiWrapper->sendRequest("post")) {
                $apiWrapper->formatResponse();
                LogHelper::log('refreshStamps', '[SUCCESS] ' . json_encode($apiWrapper->getData()));
            } else {
                LogHelper::log('refreshStamps', '[ERROR] when calling /marketing/stamp/offline API ' . json_encode($apiWrapper->getError()));
                throw new Exception($apiWrapper->getError());
            }
        } catch (\Exception $ex) {
            LogHelper::log("refreshStamps", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,CUSTOMER ID: " . $customer_id);
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);

        return $this->rrResponse->send();
    }
}
