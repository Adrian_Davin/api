<?php

namespace Controllers\AceController;

use Controllers\BaseController;
use Helpers\LogHelper;
use Library\Response;
use Library\KawanLamaSystem;
use Library\AceHardware\Diginex;
use Library\Mongodb;

class DiginexAceController extends BaseController
{
    protected $kawanLamaSystemLib;
    protected $diginexLib;
    protected $mongoDbLib;
    protected $rrResponse;

    public function onConstruct()
    {
        $this->diginexLib = new Diginex();
        $this->kawanLamaSystemLib = new KawanLamaSystem();
        $this->mongoDbLib = new Mongodb();

        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "OK")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"])
    {
        if ($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = [];
        $response["errors"] = [
            "code"      => $errorCode,
            "title"     => $this->rrResponse->getResponseDescription($errorCode),
            "messages"  => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function scanQRKiosk()
    {
        $dataToReturn = array();

        $requiredParamFields = ["ace_customer_member_no", "store_code", "type"];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        $storeModel = new \Models\Store();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail(
                "ace_customer_member_no = '" . $this->params['ace_customer_member_no'] . "'"
            );
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $store = $storeModel::findFirst(["store_code = '" . $this->params['store_code'] . "'"]);
            if (empty($store)) {
                return $this->sendErrorResponse(
                    400,
                    'Store with code ' . $this->params['store_code'] . 'not found in our database'
                );
            } else {
                $store = $store->toArray();
            }

            $isClaimVoucherValid = $this->diginexLib->validateDiginexVoucherUsageCountPerMonth(
                $customerAceDetail['customer_id']
            );

            $mongoCheckinParameters = array(
                'ace_customer_id'           => $customerAceDetail['ace_customer_id'],
                'ace_customer_member_no'    => $customerAceDetail['ace_customer_member_no'],
                'store_code'                => $store['store_code'],
                'store_name'                => $store['name'],
                'store_zone_id'             => $store['zone_id'],
                'check_in_time'             => date('Y-m-d H:i:s'),
                'check_in_source'           => "kiosk",
                'scan_kiosk_type'           => $this->params['type'],
                'is_allow_claim_voucher'    => $isClaimVoucherValid,
                'is_process'                => false
            );
            $isCustomerCheckinWritten = $this->diginexLib->writeDiginexCheckInCustomerToMongo(
                $mongoCheckinParameters
            );
            if (!$isCustomerCheckinWritten) {
                return $this->sendErrorResponse(500, "Something wrong happened when inserting checkin data");
            } else {
                $dataToReturn = $mongoCheckinParameters;
            }

            if ($this->params['type'] === 'claim' && $isClaimVoucherValid) {
                $salesRuleVoucher = $this->diginexLib->claimDiginexVoucher(
                    $customerAceDetail['customer_id'],
                    $store['store_code'],
                    $store['name']
                );
                if (!$salesRuleVoucher) {
                    return $this->sendErrorResponse(
                        $this->diginexLib->getErrorCode(),
                        $this->diginexLib->getErrorMessages()
                    );
                }

                // Posting Journal 3601
                $this->kawanLamaSystemLib->postingJournal($salesRuleVoucher);
            }
        } catch (\Exception $ex) {
            LogHelper::log("diginex_scan_qr_kiosk", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,ACE MEMBER NUMBER: " . $this->params['ace_customer_member_no']);

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $dataToReturn;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function scanQRMobile()
    {
        $dataToReturn = array();

        $requiredParamFields = ["ace_customer_member_no", "store_code"];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        $storeModel = new \Models\Store();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail(
                "ace_customer_member_no = '" . $this->params['ace_customer_member_no'] . "'"
            );
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $store = $storeModel::findFirst(["store_code = '" . $this->params['store_code'] . "'"]);
            if (empty($store)) {
                return $this->sendErrorResponse(
                    400,
                    'Store with code ' . $this->params['store_code'] . 'not found in our database'
                );
            } else {
                $store = $store->toArray();
            }

            $isClaimVoucherValid = $this->diginexLib->validateDiginexVoucherUsageCountPerMonth(
                $customerAceDetail['customer_id']
            );

            $mongoCheckinParameters = array(
                'ace_customer_id'           => $customerAceDetail['ace_customer_id'],
                'ace_customer_member_no'    => $customerAceDetail['ace_customer_member_no'],
                'store_code'                => $store['store_code'],
                'store_name'                => $store['name'],
                'store_zone_id'             => $store['zone_id'],
                'check_in_time'             => date('Y-m-d H:i:s'),
                'check_in_source'           => "ace (mobile apps)",
                'scan_kiosk_type'           => null,
                'is_allow_claim_voucher'    => $isClaimVoucherValid,
                'is_process'                => null
            );
            $isCustomerCheckinWritten = $this->diginexLib->writeDiginexCheckInCustomerToMongo(
                $mongoCheckinParameters
            );
            if (!$isCustomerCheckinWritten) {
                return $this->sendErrorResponse(500, "Something wrong happened when inserting checkin data");
            } else {
                $dataToReturn = $mongoCheckinParameters;
            }
        } catch (\Exception $ex) {
            LogHelper::log("diginex_scan_qr_mobile", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,ACE MEMBER NUMBER: " . $this->params['ace_customer_member_no']);

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $dataToReturn;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function claimDiginexVoucher()
    {
        $dataToReturn = array();

        $requiredParamFields = ["customer_id", "store_code"];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        $storeModel = new \Models\Store();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $this->params['customer_id']);
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $store = $storeModel::findFirst(["store_code = '" . $this->params['store_code'] . "'"]);
            if (empty($store)) {
                return $this->sendErrorResponse(
                    400,
                    'Store with code ' . $this->params['store_code'] . 'not found in our database'
                );
            } else {
                $store = $store->toArray();
            }

            $isClaimVoucherValid = $this->diginexLib->validateDiginexVoucherUsageCountPerMonth(
                $this->params['customer_id']
            );
            if (!$isClaimVoucherValid) {
                return $this->sendErrorResponse(
                    $this->diginexLib->getErrorCode(),
                    $this->diginexLib->getErrorMessages()
                );
            }

            $salesRuleVoucher = $this->diginexLib->claimDiginexVoucher(
                $this->params['customer_id'],
                $store['store_code'],
                $store['name']
            );
            if (!$salesRuleVoucher) {
                return $this->sendErrorResponse(
                    $this->diginexLib->getErrorCode(),
                    $this->diginexLib->getErrorMessages()
                );
            }

            // Posting Journal 3601
            $this->kawanLamaSystemLib->postingJournal($salesRuleVoucher);

            $salesRuleVoucher = $salesRuleVoucher->toArray();

            $dataToReturn = array(
                'ace_customer_id'           => $customerAceDetail['ace_customer_id'],
                'ace_customer_member_no'    => $customerAceDetail['ace_customer_member_no'],
                'store_code'                => $store['store_code'],
                'store_name'                => $store['name'],
                'voucher_code'              => $salesRuleVoucher['voucher_code'],
                'voucher_amount'            => $salesRuleVoucher['voucher_amount'],
                'expiration_date'           => $salesRuleVoucher['expiration_date']
            );
        } catch (\Exception $ex) {
            LogHelper::log("diginex_claim_voucher", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,CUSTOMER ID: " . $this->params['customer_id']);

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $dataToReturn;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function checkCustomerRedeemKioskScan($ace_customer_member_no)
    {
        $dataToReturn = array();

        try {
            $dataToReturn = $this->diginexLib->getMongoDBDataForCheckCustomerRedeemKioskScan(
                $ace_customer_member_no
            );
        } catch (\Exception $ex) {
            LogHelper::log("diginex_scan_qr_kiosk", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,ACE MEMBER NUMBER: " . $this->params['ace_customer_member_no']);

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $dataToReturn;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function checkCustomerRequestSalesAssistant($ticket_number)
    {
        $dataToReturn = array();

        try {
            $dataToReturn = $this->diginexLib->getMongoDBDataForCustomerSalesAssistantRequest(
                $ticket_number
            );
        } catch (\Exception $ex) {
            LogHelper::log("diginex_sales_assistant_request_check", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,DIGINEX TICKET NUMBER: " . $ticket_number);

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $dataToReturn;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function saveSalesAssistant()
    {
        $requiredParamFields = [
            "ace_customer_member_no",
            "store_code",
            "sales_assistant_ticket_no",
            "sales_assistant_employee_id",
            "sales_assistant_employee_name",
            "sales_assistant_ticket_status"
        ];

        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        $storeModel = new \Models\Store();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail(
                "ace_customer_member_no = '" . $this->params['ace_customer_member_no'] . "'"
            );
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $store = $storeModel::findFirst(["store_code = '" . $this->params['store_code'] . "'"]);
            if (empty($store)) {
                return $this->sendErrorResponse(
                    400,
                    'Store with code ' . $this->params['store_code'] . 'not found in our database'
                );
            } else {
                $store = $store->toArray();
            }

            $isCustomerSalesAssistantRequestWritten = $this->diginexLib->writeDiginexCustomerSalesAssistantToMongo(
                array(
                    'ace_customer_id'           => $customerAceDetail['ace_customer_id'],
                    'ace_customer_member_no'    => $customerAceDetail['ace_customer_member_no'],
                    'store_code'                => $store['store_code'],
                    'store_name'                => $store['name'],
                    'store_aisle_name'          => null,
                    'ticket_number'             => $this->params['sales_assistant_ticket_no'],
                    'sales_assistant_id'        => $this->params['sales_assistant_employee_id'],
                    'sales_assistant_name'      => $this->params['sales_assistant_employee_name'],
                    'status'                    => $this->params['sales_assistant_ticket_status'],
                    'is_need_closing'           => false,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s')
                )
            );
            if (!$isCustomerSalesAssistantRequestWritten) {
                return $this->sendErrorResponse(
                    500,
                    "Something wrong happened when inserting sales assistant request data"
                );
            }

            $isSalesAssistantSaved = $this->diginexLib->saveSalesAssistant([
                'customer_id'                       => $customerAceDetail['customer_id'],
                'employee_referral_nip'             => $this->params['sales_assistant_employee_id'],
                'employee_referral_name'            => $this->params['sales_assistant_employee_name'],
                'employee_referral_site_code_sap'   => $this->params['store_code'],
                'type'                              => 'sales_assistant',
                'sales_assistant_ticket_no'         => $this->params['sales_assistant_ticket_no'],
                'status'                            => $this->params['sales_assistant_ticket_status']
            ]);

            if (!$isSalesAssistantSaved) {
                return $this->sendErrorResponse(
                    $this->diginexLib->getErrorCode(),
                    $this->diginexLib->getErrorMessages()
                );
            }
        } catch (\Exception $ex) {
            LogHelper::log(
                "diginex_save_assistant",
                "EXCEPTION OCCURED: " . $ex->getMessage() .
                    "\n , Sales Assistant Ticket No : " . $this->params['sales_assistant_ticket_no'] . "\n "
            );
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function salesAssistantTicketRequest()
    {
        $dataToReturn = array();

        $requiredParamFields = ["customer_id", "store_code", "store_aisle_name"];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        $aceWebAPI = new \Library\AceWebAPI();
        $storeModel = new \Models\Store();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail(
                "customer_id = '" . $this->params['customer_id'] . "'"
            );
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $store = $storeModel::findFirst(["store_code = '" . $this->params['store_code'] . "'"]);
            if (empty($store)) {
                return $this->sendErrorResponse(
                    400,
                    'Store with code ' . $this->params['store_code'] . 'not found in our database'
                );
            } else {
                $store = $store->toArray();
            }

            $createSalesAssistantTicket = $aceWebAPI->createTicketForRequestingSalesAssistant(
                array(
                    'member_id' =>  $customerAceDetail['ace_customer_member_no'],
                    'location'  =>  $this->params['store_aisle_name'],
                    'admin_id'  =>  0,
                    'site_code' =>  $this->params['store_code'],
                    'kiosk_id'  =>  0
                )
            );
            if (!$createSalesAssistantTicket) {
                LogHelper::log(
                    "diginex_request_sales_assistant",
                    "MESSAGE: " . $aceWebAPI->getErrorMessages() . " ,ACE CUSTOMER MEMBER NUMBER: " . $customerAceDetail['ace_customer_member_no']
                );

                return $this->sendErrorResponse(
                    $aceWebAPI->getErrorCode(),
                    $aceWebAPI->getErrorMessages()
                );
            }

            $dispatchSalesAssistantTicket = $aceWebAPI->dispatchTicketSalesAssistantRequest(
                $createSalesAssistantTicket['ticketNo'],
                $createSalesAssistantTicket['request_assistance_id'],
                $this->params['store_code'],
                $this->params['store_aisle_name']
            );
            if (!$dispatchSalesAssistantTicket) {
                LogHelper::log(
                    "diginex_request_sales_assistant",
                    "DISPATCH TO SALES ASSISTANT FAILED, MESSAGE: " .
                        $aceWebAPI->getErrorMessages() . " ,ACE CUSTOMER MEMBER NUMBER: " .
                        $customerAceDetail['ace_customer_member_no']
                );

                return $this->sendErrorResponse(
                    $aceWebAPI->getErrorCode(),
                    $aceWebAPI->getErrorMessages()
                );
            }

            $mongoSalesAssistantParameters = array(
                'ace_customer_id'           => $customerAceDetail['ace_customer_id'],
                'ace_customer_member_no'    => $customerAceDetail['ace_customer_member_no'],
                'store_code'                => $store['store_code'],
                'store_name'                => $store['name'],
                'store_aisle_name'          => $this->params['store_aisle_name'],
                'ticket_number'             => $createSalesAssistantTicket['ticketNo'],
                'request_assistance_id'     => $createSalesAssistantTicket['request_assistance_id'],
                'sales_assistant_id'        => '',
                'sales_assistant_name'      => '',
                'status'                    => 'menunggu',
                'is_need_closing'           => false,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s')
            );
            $isCustomerSalesAssistantRequestWritten = $this->diginexLib->writeDiginexCustomerSalesAssistantToMongo(
                $mongoSalesAssistantParameters
            );
            if (!$isCustomerSalesAssistantRequestWritten) {
                return $this->sendErrorResponse(
                    500,
                    "Something wrong happened when inserting sales assistant request data"
                );
            }

            $dataToReturn = array(
                'ace_customer_id'           => $customerAceDetail['ace_customer_id'],
                'ace_customer_member_no'    => $customerAceDetail['ace_customer_member_no'],
                'store_code'                => $store['store_code'],
                'store_name'                => $store['name'],
                'store_aisle_name'          => $this->params['store_aisle_name'],
                'ticket_number'             => $mongoSalesAssistantParameters['ticket_number'],
                'is_need_closing'           => $mongoSalesAssistantParameters['is_need_closing'],
                'request_timestamp'         => $mongoSalesAssistantParameters['created_at']
            );
        } catch (\Exception $ex) {
            LogHelper::log(
                "diginex_request_sales_assistant",
                "EXCEPTION OCCURED: " . $ex->getMessage() .
                    " ,CUSTOMER ID: " . $this->params['customer_id']
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $dataToReturn;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    /**
     * A function to be executed when the ticket requested by customer
     * not get any response from sales assistant when requesting the sales assistant
     * from button trigger in mobile apps
     */
    public function salesAssistantTicketClose()
    {
        $dataToReturn = array();

        $requiredParamFields = ["customer_id", "ticket_number"];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        $aceWebAPI = new \Library\AceWebAPI();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail(
                "customer_id = '" . $this->params['customer_id'] . "'"
            );
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $closeSalesAssistantTicket = $aceWebAPI->closeTicketSalesAssistantRequest($this->params['ticket_number']);
            if (!$closeSalesAssistantTicket) {
                LogHelper::log(
                    "diginex_close_sales_assistant_ticket",
                    "MESSAGE: " . $aceWebAPI->getErrorMessages() . " ,ACE CUSTOMER MEMBER NUMBER: " . $customerAceDetail['ace_customer_member_no']
                );

                return $this->sendErrorResponse(
                    $aceWebAPI->getErrorCode(),
                    $aceWebAPI->getErrorMessages()
                );
            }

            $isCustomerSalesAssistantRequestWritten = $this->diginexLib->writeDiginexCustomerSalesAssistantToMongo(
                array(
                    'ace_customer_member_no'    => $customerAceDetail['ace_customer_member_no'],
                    'ticket_number'             => $this->params['ticket_number'],
                    'status'                    => 'batal',
                    'is_need_closing'           => true,
                    'updated_at'                => date('Y-m-d H:i:s')
                )
            );
            if (!$isCustomerSalesAssistantRequestWritten) {
                return $this->sendErrorResponse(
                    500,
                    "Something wrong happened when inserting sales assistant request data"
                );
            }
        } catch (\Exception $ex) {
            LogHelper::log(
                "diginex_close_sales_assistant_ticket",
                "EXCEPTION OCCURED: " . $ex->getMessage() .
                    " ,CUSTOMER ID: " . $this->params['customer_id']
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $dataToReturn;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }
}
