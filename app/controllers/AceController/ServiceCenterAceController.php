<?php

namespace Controllers\AceController;

use Controllers\BaseController;
use Exception;
use Helpers\LogHelper;
use Helpers\ServiceCenterHelper;
use Library\Response;
use Library\KawanLamaSystem;
use Library\AceHardware\ServiceCenter;

class ServiceCenterAceController extends BaseController
{
    protected $kawanLamaSystemLib;
    protected $serviceCenterLib;
    protected $rrResponse;

    public function onConstruct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();
        $this->serviceCenterLib = new ServiceCenter();

        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "OK")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"])
    {
        if ($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = [];
        $response["errors"] = [
            "code" => $errorCode,
            "title" => $this->rrResponse->getResponseDescription($errorCode),
            "messages" => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function warrantyAvailabilityCheckup($order_no)
    {
        $result = null;
        try {
            $result = $this->serviceCenterLib->serviceCenterWarrantyCheckupByOrderNo($order_no);
            if (!$result)
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_availability_checkup",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,ACE ORDER NO: $order_no"
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $result;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyAvailabilityCheckupOffline($receipt_id)
    {
        $result = null;
        try {
            $result = $this->serviceCenterLib->serviceCenterWarrantyCheckupByReceiptId($receipt_id);
            if (!$result)
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_availability_checkup_offline",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,ACE RECEIPT ID: $receipt_id"
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $result;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyDetails($ace_warranty_id)
    {
        $result = null;
        try {
            $result = $this->serviceCenterLib->serviceCenterWarrantyDetailsCheckPerWarrantyID($ace_warranty_id);
            if (!$result)
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_availability_details",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,ACE ORDER NO: $ace_warranty_id"
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $result;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyDetailsOffline($ace_warranty_id)
    {
        $result = null;
        try {
            $result = $this->serviceCenterLib->serviceCenterWarrantyDetailsCheckPerWarrantyIDOfflineTransaction($ace_warranty_id);
            if (!$result)
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_availability_details_offline",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,ACE ORDER NO: $ace_warranty_id"
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $result;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyActivation()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "sales_invoice_item_id"];
        $aceCustomerMemberNo = "";

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $customerModel = new \Models\Customer();
        $customerDataObject = $customerModel->findFirst("customer_id = '" . $params['customer_id'] . "'");
        $customerData = !empty($customerDataObject) ? $customerDataObject->toArray() : "";
        if (!$customerData) {
            return $this->sendErrorResponse(400, 'customer tidak ditemukan!');
        }

        $serialNumber = isset($params['serial_number']) ? $params['serial_number'] : null;
        $warrantyNumber = isset($params['warranty_number']) ? $params['warranty_number'] : null;

        if (empty($serialNumber) && empty($warrantyNumber)) {
            return $this->sendErrorResponse(400, 'Nomor Serial atau Nomor Kartu Garansi wajib diisi');
        }

        try {
            // Ace membership checking
            if ($customerData['company_code'] == 'ODI') {
                $customer2groupModel = new \Models\Customer2Group();
                $customer2groupObject = $customer2groupModel->findFirst("customer_id = '" . $params['customer_id'] . "' AND group_id = 4");
                $customer2groupData = !empty($customer2groupObject) ? $customer2groupObject->toArray() : "";
                if (!$customer2groupData) {
                    return $this->sendErrorResponse(400, 'data membership ACE Hardware belum dihubungkan dengan akun ini.');
                }

                $aceCustomerMemberNo = $customer2groupData['member_number'];
            } else if ($customerData['company_code'] == 'AHI') {
                $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['customer_id']);
                if (!$customerAceDetail)
                    return $this->sendErrorResponse(
                        $this->kawanLamaSystemLib->getErrorCode(),
                        $this->kawanLamaSystemLib->getErrorMessages()
                    );

                $aceCustomerMemberNo = $customerAceDetail['ace_customer_member_no'];
            }

            $warrantyActivationValidity = $this->serviceCenterLib->validateWarrantyActivation(
                $serialNumber,
                $warrantyNumber,
                $params['sales_invoice_item_id']
            );
            if (!$warrantyActivationValidity) {
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
            }

            $aceWarrantyId = $this->serviceCenterLib->serviceCenterActivateWarranty(
                $params['customer_id'],
                $params['sales_invoice_item_id'],
                null,
                array(
                    'AceMemberNo' => $aceCustomerMemberNo,
                    'SKU' => $warrantyActivationValidity['salesInvoiceItemSKU'],
                    'WarrantyCode' => ServiceCenterHelper::generateWarrantyCodeODI(),
                    'ReceiptNumber' => $warrantyActivationValidity['salesInvoiceReceiptId'],
                    'WarrantyNumber' => !empty($params['warranty_number']) ? $params['warranty_number'] : "",
                    'SerialNumber' => !empty($params['serial_number']) ? $params['serial_number'] : "",
                    'ReceiptDate' => date($warrantyActivationValidity['salesInvoiceCreatedAt'])
                )
            );
            if (!$aceWarrantyId) {
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
            }

            $result = $this->serviceCenterLib->serviceCenterWarrantyDetailsCheckPerWarrantyID($aceWarrantyId);
            if (!$result)
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_activation",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,CUSTOMER ID: " . $params['customer_id']
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyActivationOffline()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "receipt_id", "sku"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $customerModel = new \Models\Customer();
        $customerDataObject = $customerModel->findFirst("customer_id = '" . $params['customer_id'] . "'");
        $customerData = !empty($customerDataObject) ? $customerDataObject->toArray() : "";
        if (!$customerData) {
            return $this->sendErrorResponse(400, 'customer tidak ditemukan!');
        }

        $serialNumber = isset($params['serial_number']) ? $params['serial_number'] : null;
        $warrantyNumber = isset($params['warranty_number']) ? $params['warranty_number'] : null;

        if (empty($serialNumber) && empty($warrantyNumber)) {
            return $this->sendErrorResponse(400, 'Nomor Serial atau Nomor Kartu Garansi wajib diisi');
        }

        try {
            // Ace membership checking
            if ($customerData['company_code'] == 'ODI') {
                $customer2groupModel = new \Models\Customer2Group();
                $customer2groupObject = $customer2groupModel->findFirst("customer_id = '" . $params['customer_id'] . "' AND group_id = 4");
                $customer2groupData = !empty($customer2groupObject) ? $customer2groupObject->toArray() : "";
                if (!$customer2groupData) {
                    return $this->sendErrorResponse(400, 'data membership ACE Hardware belum dihubungkan dengan akun ini.');
                }

                $aceCustomerMemberNo = $customer2groupData['member_number'];
            } else if ($customerData['company_code'] == 'AHI') {
                $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['customer_id']);
                if (!$customerAceDetail)
                    return $this->sendErrorResponse(
                        $this->kawanLamaSystemLib->getErrorCode(),
                        $this->kawanLamaSystemLib->getErrorMessages()
                    );

                $aceCustomerMemberNo = $customerAceDetail['ace_customer_member_no'];
            }

            $warrantyActivationValidity = $this->serviceCenterLib->validateWarrantyActivationOfflineTransaction(
                $serialNumber,
                $warrantyNumber,
                $params['receipt_id'],
                $params['sku']
            );
            if (!$warrantyActivationValidity) {
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
            }

            $aceWarrantyId = $this->serviceCenterLib->serviceCenterActivateWarranty(
                $params['customer_id'],
                null,
                $params['receipt_id'] . '-' . $params['sku'],
                array(
                    'AceMemberNo' => $aceCustomerMemberNo,
                    'SKU' => $warrantyActivationValidity['sku'],
                    'WarrantyCode' => ServiceCenterHelper::generateWarrantyCodeODI(),
                    'ReceiptNumber' => $warrantyActivationValidity['receiptId'],
                    'WarrantyNumber' => !empty($params['warranty_number']) ? $params['warranty_number'] : "",
                    'SerialNumber' => !empty($params['serial_number']) ? $params['serial_number'] : "",
                    'ReceiptDate' => date($warrantyActivationValidity['orderDate'])
                )
            );
            if (!$aceWarrantyId) {
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
            }

            $result = $this->serviceCenterLib->serviceCenterWarrantyDetailsCheckPerWarrantyIDOfflineTransaction($aceWarrantyId);
            if (!$result)
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_activation_offline",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,CUSTOMER ID: " . $params['customer_id']
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyServicesByCustomerId($customer_id)
    {
        $orderedResults = [];

        try {
            $orderedResults = $this->serviceCenterLib->getServiceWarrantyDataPerCustomerWithOfflineFlag($customer_id);
            if (!$orderedResults && $this->serviceCenterLib->getErrorCode()) {
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
            }
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_services_by_customer_id",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,ACE Customer ID: $customer_id"
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $orderedResults;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyServicesByCustomerIdOfflineTransaction($customer_id)
    {
        $orderedResults = [];

        try {
            $orderedResults = $this->serviceCenterLib->getServiceWarrantyDataPerCustomerWithOfflineFlag($customer_id, true);
            if (!$orderedResults && $this->serviceCenterLib->getErrorCode()) {
                return $this->sendErrorResponse(
                    $this->serviceCenterLib->getErrorCode(),
                    $this->serviceCenterLib->getErrorMessages()
                );
            }
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_services_by_customer_id_offline",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,ACE Customer ID: $customer_id"
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $orderedResults;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyServiceDetailByAceServiceNumber($ace_service_number)
    {
        $results = [];
        $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
        $customerAceWarrantyServiceModel = new \Models\CustomerAceWarrantyService();
        $productModel = new \Models\Product();
        $salesInvoiceItemModel = new \Models\SalesInvoiceItem();

        try {
            $customerAceWarrantyService = $customerAceWarrantyServiceModel->findFirst("ace_service_number = '$ace_service_number'")->toArray();
            if (!$customerAceWarrantyService) {
                return $this->sendErrorResponse(
                    400,
                    "Data barang dengan nomor servis $ace_service_number tidak ditemukan"
                );
            }
            $customerAceOrderWarranty = $customerAceOrderWarrantyModel->findFirst(
                "customer_ace_order_warranty_id = '" . $customerAceWarrantyService['customer_ace_order_warranty_id'] . "'"
            )->toArray();
            if (!$customerAceOrderWarranty) {
                return $this->sendErrorResponse(
                    400,
                    "Data barang yang didaftarkan sebagai garansi tidak ditemukan"
                );
            }

            if (isset($customerAceOrderWarranty['sales_invoice_item_id']) && !empty($customerAceOrderWarranty['sales_invoice_item_id'])) {
                $salesInvoiceItem = $salesInvoiceItemModel
                    ->findFirst("invoice_item_id = '" . $customerAceOrderWarranty['sales_invoice_item_id'] . "'")
                    ->toArray();
                if (!$salesInvoiceItem) {
                    return $this->sendErrorResponse(
                        400,
                        "Data SKU barang tidak ditemukan"
                    );
                }

                $elasticProduct = $productModel->searchProductFromElastic($salesInvoiceItem['sku']);

                $results = array(
                    'ace_service_number' => $customerAceWarrantyService['ace_service_number'],
                    'ace_warranty_id' => $customerAceOrderWarranty['ace_warranty_id'],
                    'service_status' => $customerAceWarrantyService['status'],
                    'service_additional_charge' => isset($customerAceWarrantyService['service_warranty_charge']) ?
                        $customerAceWarrantyService['service_warranty_charge'] :
                        null,
                    'service_date' => $customerAceWarrantyService['service_date'],
                    'service_due_date' => $customerAceWarrantyService['service_expected_finished_date'],
                    'service_warranty_items' => !empty($customerAceWarrantyService['service_warranty_items']) ?
                        $customerAceWarrantyService['service_warranty_items'] :
                        [],
                    'sku_number'            => $elasticProduct['variants'][0]['sku'],
                    'sku_name'              => $elasticProduct['name'],
                    'sku_image'             => $elasticProduct['variants'][0]['images'][0]['image_url'],
                );
            } else {
                $results = array(
                    'ace_service_number' => $customerAceWarrantyService['ace_service_number'],
                    'ace_warranty_id' => $customerAceOrderWarranty['ace_warranty_id'],
                    'service_status' => $customerAceWarrantyService['status'],
                    'service_additional_charge' => isset($customerAceWarrantyService['service_warranty_charge']) ?
                        $customerAceWarrantyService['service_warranty_charge'] :
                        null,
                    'service_date' => $customerAceWarrantyService['service_date'],
                    'service_due_date' => $customerAceWarrantyService['service_expected_finished_date'],
                    'service_warranty_items' => !empty($customerAceWarrantyService['service_warranty_items']) ?
                        $customerAceWarrantyService['service_warranty_items'] :
                        []
                );
            }
        } catch (Exception $ex) {
            LogHelper::log(
                "warranty_service_detail_by_ace_service_number",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,ACE Customer ID: $ace_service_number"
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = $results;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function warrantyServiceChargeConfirmation()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["ace_service_number", "is_customer_approve"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $aceWebAPI = new \Library\AceWebAPI();
        $customerAceWarrantyServiceModel = new \Models\CustomerAceWarrantyService();
        $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();

        try {
            $customerAceWarrantyService = $customerAceWarrantyServiceModel->findFirst("ace_service_number = '" . $params['ace_service_number'] . "'");
            if (!$customerAceWarrantyService) {
                return $this->sendErrorResponse(
                    400,
                    "Data barang dengan nomor servis " . $params['ace_service_number'] . " tidak ditemukan"
                );
            }

            $customerAceOrderWarranty = $customerAceOrderWarrantyModel->findFirst(
                "customer_ace_order_warranty_id = '" . $customerAceWarrantyService->getCustomerAceOrderWarrantyId() . "'"
            )->toArray();
            if (!$customerAceOrderWarranty) {
                return $this->sendErrorResponse(
                    400,
                    "Data garansi barang tidak dapat ditemukan"
                );
            }

            $customer = $this->kawanLamaSystemLib->getCustomerDetail("customer_id = " . $customerAceOrderWarranty['customer_id']);

            if ($customerAceWarrantyService->getStatus() != 9) {
                return $this->sendErrorResponse(
                    400,
                    "Status customer tidak sesuai untuk melakukan konfirmasi"
                );
            }

            $response = $aceWebAPI->serviceWarrantyFeeConfirmation(
                array(
                    'ServiceNumber' => $params['ace_service_number'],
                    'IsCustomerApprove' => $params['is_customer_approve'],
                    'CancelationNotes' => isset($params['cancelation_notes']) ?
                        $params['cancelation_notes'] :
                        ""
                )
            );
            if (!$response) {
                return $this->sendErrorResponse(
                    400,
                    "Terjadi kesalahan saat melakukan perubahan data konfirmasi"
                );
            }

            $customerAceWarrantyService->setStatus($params['is_customer_approve']);

            $result = $customerAceWarrantyService->update();
            if (false === $result) {
                return $this->sendErrorResponse(
                    400,
                    implode('|', $customerAceWarrantyServiceModel->getMessages())
                );
            }

            if ($params['is_customer_approve'] == 10) {
                $this->serviceCenterLib->sendEmailPaymentDisapproval(
                    $customer['email'],
                    isset($customer['first_name']) ? $customer['first_name'] : "",
                    isset($customer['last_name']) ? $customer['last_name'] : ""
                );

                $this->serviceCenterLib->sendEmailFinishedNotification(
                    $customer['email'],
                    isset($customer['last_name']) ? $customer['last_name'] : "",
                    isset($customer['first_name']) ? $customer['first_name'] : ""
                );
            } else if ($params['is_customer_approve'] == 11) {
                $totalCharge = (int)$customerAceWarrantyService->getServiceWarrantyCharge();
                $serviceWarrantyItems = json_decode($customerAceWarrantyService->getServiceWarrantyItems());
                for ($i = 0; $i < count($serviceWarrantyItems); $i++) {
                    $totalCharge += (int)$serviceWarrantyItems[$i]->service_price;
                }
                $this->serviceCenterLib->sendEmailPaymentAgreement(
                    $customer['email'],
                    isset($customer['first_name']) ? $customer['first_name'] : "",
                    isset($customer['last_name']) ? $customer['last_name'] : "",
                    $totalCharge
                );
            }
        } catch (Exception $ex) {
            LogHelper::log(
                "warrantyServiceChargeConfirmation",
                "EXCEPTION OCCURED: " . $ex->getMessage() . " ,ACE Customer ID: " . $params['customer_id']
            );

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function webhookServiceCharge()
    {
        $requiredParamFields = [
            "ace_service_number",
            "ace_warranty_id",
            "service_additional_charge",
            "service_date",
            "service_warranty_items",
            "service_due_date"
        ];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        try {
            $customerAceWarrantyServiceModel = new \Models\CustomerAceWarrantyService();
            $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
            $customerAceOrderWarrantyNonMembershipModel = new \Models\CustomerAceOrderWarrantyNonMembership();
            $customerAceWarrantyServiceNonMembershipModel = new \Models\CustomerAceWarrantyServiceNonMembership();

            $customerServiceWarranty = $customerAceWarrantyServiceModel->findFirst([
                'conditions' => 'ace_service_number =' . "'" . $this->params['ace_service_number'] . "'"
            ]);
            if (!$customerServiceWarranty) {
                $customerOrderWarrantyNonMembership = $customerAceOrderWarrantyNonMembershipModel->findFirst([
                    'conditions' => 'ace_warranty_id =' .
                        "'" . $this->params['ace_warranty_id'] . "'" .
                        'and status = 0'
                ])->toArray();

                $customerServiceWarrantyNonMembership = $customerAceWarrantyServiceNonMembershipModel->findFirst([
                    'conditions' => 'ace_service_number =' . "'" . $this->params['ace_service_number'] . "'"
                ]);

                if (empty($customerOrderWarrantyNonMembership) || empty($customerServiceWarrantyNonMembership)) {
                    LogHelper::log("webhook_service_charge", "ace_service_number not found in membership or non membership table" .
                        " ,ACE SERVICE WARRANTY ID: " . $this->params['ace_warranty_id'], "info");

                    return $this->sendErrorResponse(
                        400,
                        'ace_service_number not found in database!'
                    );
                }

                $customerServiceWarrantyNonMembership->setServiceWarrantyCharge($this->params['service_additional_charge']);
                $customerServiceWarrantyNonMembership->setServiceWarrantyItems(json_encode($this->params['service_warranty_items']));
                $customerServiceWarrantyNonMembership->setServiceDate($this->params['service_date']);
                $customerServiceWarrantyNonMembership->setServiceExpectedFinishedDate($this->params['service_due_date']);

                $result = $customerServiceWarrantyNonMembership->update();
                if (false === $result) {
                    throw new Exception(
                        implode('|', $customerAceWarrantyServiceNonMembershipModel->getMessages())
                    );
                }
            } else {
                $customerAceOrderWarranty = $customerAceOrderWarrantyModel->findFirst(
                    "customer_ace_order_warranty_id = '" . $customerServiceWarranty->getCustomerAceOrderWarrantyId() . "'"
                )->toArray();
                if (!$customerAceOrderWarranty) {
                    return $this->sendErrorResponse(
                        400,
                        "Data garansi barang tidak dapat ditemukan"
                    );
                }

                $customer = $this->kawanLamaSystemLib->getCustomerDetail("customer_id = " . $customerAceOrderWarranty['customer_id']);

                $customerServiceWarranty->setServiceWarrantyCharge($this->params['service_additional_charge']);
                $customerServiceWarranty->setServiceWarrantyItems(json_encode($this->params['service_warranty_items']));
                $customerServiceWarranty->setServiceDate($this->params['service_date']);
                $customerServiceWarranty->setServiceExpectedFinishedDate($this->params['service_due_date']);

                $result = $customerServiceWarranty->update();
                if (false === $result) {
                    throw new Exception(
                        implode('|', $customerAceWarrantyServiceModel->getMessages())
                    );
                }

                $totalCharge = 0;
                for ($i = 0; $i < count($this->params['service_warranty_items']); $i++) {
                    $totalCharge += (int)$this->params['service_warranty_items'][$i]['service_price'];
                }

                #region send email confirmation
                if ($totalCharge >= (int)getenv('SERVICE_CENTER_MINIMUM_SERVICE_CHARGE')) {
                    $isOffline = !empty($customerAceOrderWarranty['receipt_id_and_sku']) ? true : false;
                    $this->serviceCenterLib->sendEmailPaymentConfirmation(
                        $customer['email'],
                        isset($customer['first_name']) ? $customer['first_name'] : "",
                        isset($customer['last_name']) ? $customer['last_name'] : "",
                        $customerServiceWarranty->getAceServiceNumber(),
                        $isOffline
                    );
                }
                #endregion
            }
        } catch (Exception $ex) {
            LogHelper::log("webhook_service_charge", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,ACE SERVICE WARRANTY ID: " . $this->params['ace_warranty_id']);

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function webhookStatus()
    {
        $requiredParamFields = [
            "ace_service_number",
            "ace_warranty_id",
            "service_status",
            "service_date",
            "service_due_date"
        ];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        try {
            $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
            $customerAceWarrantyServiceModel = new \Models\CustomerAceWarrantyService();
            $customerAceOrderWarrantyNonMembershipModel = new \Models\CustomerAceOrderWarrantyNonMembership();
            $customerAceWarrantyServiceNonMembershipModel = new \Models\CustomerAceWarrantyServiceNonMembership();

            $customerOrderWarranty = $customerAceOrderWarrantyModel->findFirst([
                'conditions' => 'ace_warranty_id =' . "'" . $this->params['ace_warranty_id'] . "'"
            ]);

            // First thing first, check in non membership table
            if (!$customerOrderWarranty) {
                $customerOrderWarrantyNonMembership = $customerAceOrderWarrantyNonMembershipModel->findFirst([
                    'conditions' => 'ace_warranty_id =' .
                        "'" . $this->params['ace_warranty_id'] . "'" .
                        'and status = 0'
                ]);

                if (!$customerOrderWarrantyNonMembership) {
                    return $this->sendErrorResponse(
                        400,
                        'ace_warranty_id not found!'
                    );
                } else {
                    $customerOrderWarrantyNonMembership = $customerOrderWarrantyNonMembership->toArray();
                }

                $customerServiceWarrantyNonMembership = $customerAceWarrantyServiceNonMembershipModel->findFirst([
                    'conditions' => 'ace_service_number =' . "'" . $this->params['ace_service_number'] . "'"
                ]);

                if (!$customerServiceWarrantyNonMembership) {
                    $customerAceWarrantyServiceNonMembershipModel->assign([
                        'customer_ace_order_warranty_non_membership_id' => $customerOrderWarrantyNonMembership['customer_ace_order_warranty_non_membership_id'],
                        'ace_service_number' => $this->params['ace_service_number'],
                        'status' => $this->params['service_status'],
                        'service_date' => $this->params['service_date'],
                        'service_expected_finished_date' => isset($this->params['service_due_date']) ? $this->params['service_due_date'] : null,
                        'item_deliver_to_store_date' => $this->params['item_deliver_to_store_date'] ?? null,
                        'item_deliver_to_store_location' => $this->params['item_deliver_to_store_location'] ?? null
                    ]);

                    if (false === $customerAceWarrantyServiceNonMembershipModel->save()) {
                        return $this->sendErrorResponse(
                            400,
                            implode('|', $customerAceWarrantyServiceNonMembershipModel->getMessages())
                        );
                    }
                } else {
                    $customerServiceWarrantyNonMembership->setStatus($this->params['service_status']);
                    $customerServiceWarrantyNonMembership->setServiceExpectedFinishedDate($this->params['service_due_date']);

                    $result = $customerServiceWarrantyNonMembership->update();
                    if (false === $result) {
                        return $this->sendErrorResponse(
                            400,
                            implode('|', $customerServiceWarrantyNonMembership->getMessages())
                        );
                    }
                }
            } else {
                $customerOrderWarranty = $customerOrderWarranty->toArray();

                $customerServiceWarranty = $customerAceWarrantyServiceModel->findFirst([
                    'conditions' => 'ace_service_number =' . "'" . $this->params['ace_service_number'] . "'"
                ]);

                $customer = $this->kawanLamaSystemLib->getCustomerDetail("customer_id = " . $customerOrderWarranty['customer_id']);

                if (!$customerServiceWarranty) {
                    $customerAceWarrantyServiceModel->assign([
                        'customer_ace_order_warranty_id' => $customerOrderWarranty['customer_ace_order_warranty_id'],
                        'ace_service_number' => $this->params['ace_service_number'],
                        'status' => $this->params['service_status'],
                        'service_date' => $this->params['service_date'],
                        'service_expected_finished_date' => $this->params['service_due_date'],
                        'item_deliver_to_store_date' => $this->params['item_deliver_to_store_date'] ?? null,
                        'item_deliver_to_store_location' => $this->params['item_deliver_to_store_location'] ?? null
                    ]);

                    if (false === $customerAceWarrantyServiceModel->save()) {
                        return $this->sendErrorResponse(
                            400,
                            implode('|', $customerAceWarrantyServiceModel->getMessages())
                        );
                    }

                    if ($this->params['service_status'] == "2") {
                        $isOffline = !empty($customerOrderWarranty['receipt_id_and_sku']) ? true : false;
                        #region send email confirmation
                        $this->serviceCenterLib->sendEmailItemReceivedByStore(
                            $customer['email'],
                            isset($customer['first_name']) ? $customer['first_name'] : "",
                            isset($customer['last_name']) ? $customer['last_name'] : "",
                            $this->params['ace_service_number'],
                            $isOffline
                        );
                        #endregion
                    }
                } else {
                    $customerServiceWarranty->setStatus($this->params['service_status']);
                    $customerServiceWarranty->setServiceExpectedFinishedDate($this->params['service_due_date']);

                    $result = $customerServiceWarranty->update();
                    if (false === $result) {
                        return $this->sendErrorResponse(
                            400,
                            implode('|', $customerServiceWarranty->getMessages())
                        );
                    }

                    #region send email confirmation
                    if (
                        $this->params['service_status'] == "23" ||
                        $this->params['service_status'] == "25"
                    ) {
                        $this->serviceCenterLib->sendEmailReadyToPickup(
                            $customer['email'],
                            isset($customer['first_name']) ? $customer['first_name'] : "",
                            isset($customer['last_name']) ? $customer['last_name'] : ""
                        );
                    } else if (
                        $this->params['service_status'] == "21" ||
                        $this->params['service_status'] == "30"
                    ) {
                        $this->serviceCenterLib->sendEmailFinishedNotification(
                            $customer['email'],
                            isset($customer['first_name']) ? $customer['first_name'] : "",
                            isset($customer['last_name']) ? $customer['last_name'] : ""
                        );
                    }
                    #endregion
                }
            }
        } catch (Exception $ex) {
            LogHelper::log("webhook_status", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,ACE SERVICE WARRANTY ID: " . $this->params['ace_warranty_id']);

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function webhookActivateWarranty()
    {
        $params = $this->params;
        $requiredParamFields = [
            "ace_customer_id",
            "ace_warranty_id",
            "receipt_id",
            "sku",
            "new_item_replacement_warranty_date",
            "spare_part_warranty_date",
            "service_warranty_date",
            "spare_parts_detail_warranty"
        ];
        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        $serialNumber = isset($params['serial_number']) ? $params['serial_number'] : null;
        $warrantyNumber = isset($params['warranty_number']) ? $params['warranty_number'] : null;

        if (empty($serialNumber) && empty($warrantyNumber)) {
            return $this->sendErrorResponse(400, 'Nomor Serial atau Nomor Kartu Garansi wajib diisi');
        }

        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("ace_customer_id = " . $params['ace_customer_id']);

            // Should be saved to non membership table
            if (!$customerAceDetail) {
                $saveData = $this->serviceCenterLib->saveDataToCustomerAceOrderWarrantyNonMembership([
                    'ace_customer_id' => $params['ace_customer_id'],
                    'receipt_id_and_sku' => $params['receipt_id'] . '-' . $params['sku'],
                    'warranty_code' => ServiceCenterHelper::generateWarrantyCodeODI(),
                    'ace_warranty_id' => $params['ace_warranty_id'],
                    'serial_number' => $serialNumber,
                    'warranty_number' => $warrantyNumber,
                    'new_item_replacement_warranty_date' => $params['new_item_replacement_warranty_date'],
                    'spare_part_warranty_date' => $params['spare_part_warranty_date'],
                    'service_warranty_date' => $params['service_warranty_date'],
                    'spare_parts_detail_warranty' => json_encode($params['spare_parts_detail_warranty']),
                    'status' => 0
                ]);

                if (!$saveData) {
                    return $this->sendErrorResponse(
                        $this->serviceCenterLib->getErrorCode(),
                        $this->serviceCenterLib->getErrorMessages()
                    );
                }
            } else {
                $warrantyActivationValidity = $this->serviceCenterLib->validateWarrantyActivationOfflineTransaction(
                    $serialNumber,
                    $warrantyNumber,
                    $params['receipt_id'],
                    $params['sku']
                );
                if (!$warrantyActivationValidity) {
                    return $this->sendErrorResponse(
                        $this->serviceCenterLib->getErrorCode(),
                        $this->serviceCenterLib->getErrorMessages()
                    );
                }

                $saveData = $this->serviceCenterLib->saveDataToCustomerAceOrderWarranty([
                    'customer_id' => $customerAceDetail['customer_id'],
                    'sales_invoice_item_id' => null,
                    'receipt_id_and_sku' => $params['receipt_id'] . '-' . $params['sku'],
                    'warranty_code' => ServiceCenterHelper::generateWarrantyCodeODI(),
                    'ace_warranty_id' => $params['ace_warranty_id'],
                    'serial_number' => $serialNumber,
                    'warranty_number' => $warrantyNumber,
                    'new_item_replacement_warranty_date' => $params['new_item_replacement_warranty_date'],
                    'spare_part_warranty_date' => $params['spare_part_warranty_date'],
                    'service_warranty_date' => $params['service_warranty_date'],
                    'spare_parts_detail_warranty' => json_encode($params['spare_parts_detail_warranty']),
                ]);

                if (!$saveData) {
                    return $this->sendErrorResponse(
                        $this->serviceCenterLib->getErrorCode(),
                        $this->serviceCenterLib->getErrorMessages()
                    );
                }
            }
        } catch (Exception $ex) {
            LogHelper::log("webhook_activate_warranty", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,ACE SERVICE WARRANTY ID: " . $this->params['ace_warranty_id']);

            return $this->sendErrorResponse(500, $ex->getMessage());
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }
}
