<?php

namespace Controllers\AceController;

use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Library\KawanLamaSystem;
use Library\AceHardware\ServiceCenter;
use Library\Response;
use Controllers\BaseController;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class CustomerAceController extends BaseController
{
    protected $kawanLamaSystemLib;
    protected $serviceCenterLib;
    protected $rrResponse;

    public function onConstruct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();
        $this->serviceCenterLib = new ServiceCenter();
        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "Ok")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"])
    {
        if ($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = [];
        $response["errors"] = [
            "code" => $errorCode,
            "title" => $this->rrResponse->getResponseDescription($errorCode),
            "messages" => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    private function historyTransactionValidation($params)
    {
        $requiredParamField = ["page", "size", "bu_code", "customer_id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if ($params["bu_code"] != "AHI" && $params["bu_code"] != "HCI")
            return $this->sendErrorResponse(400, ["Bu Code must be AHI or HCI"]);

        return true;
    }

    public function login()
    {
        if (isset($this->params['phone'])) {
            $isCustomerDeleted = $this->kawanLamaSystemLib->getCustomerDetail("phone = '" . $this->params["phone"] . "' AND company_code = 'AHI' AND status = 1");
            if ($isCustomerDeleted) {
                return $this->sendErrorResponse(400, "Akun sudah dihapus dan tidak dapat digunakan kembali.");
            }
        } else {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("ace_customer_member_no = '" . $this->params["member_no"] . "'");
            if ($customerAceDetail) {
                $isCustomerDeleted = $this->kawanLamaSystemLib->getCustomerDetail("customer_id = '" . $customerAceDetail['customer_id'] . "' AND company_code = 'AHI' AND status = 1");
                if ($isCustomerDeleted) return $this->sendErrorResponse(400, "Akun sudah dihapus dan tidak dapat digunakan kembali.");
            }
        }

        $loginResult = $this->kawanLamaSystemLib->customerLoginAce($this->params);
        if (!$loginResult) return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $loginResult;
        $response['messages'] = ["success"];

        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function logout()
    {
        $logoutResult = $this->kawanLamaSystemLib->customerLogoutAce($this->params);
        if (!$logoutResult)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $logoutResult;
        $response['messages'] = ["success"];

        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function register()
    {
        $isCustomerDeleted = $this->kawanLamaSystemLib->getCustomerDetail("phone = '" . $this->params["phone"] . "' AND company_code = 'AHI' AND status = 1");
        if ($isCustomerDeleted) {
            return $this->sendErrorResponse(400, "Akun sudah dihapus dan tidak dapat digunakan kembali.");
        }

        $registerResult = $this->kawanLamaSystemLib->registerCustomerAce(true, $this->params);
        if (!$registerResult)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $loginResult = $this->kawanLamaSystemLib->customerLoginAce($registerResult);
        if (!$loginResult)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $loginResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function registerMembership()
    {
        $registerResult = $this->kawanLamaSystemLib->registerCustomerMemberAce($this->params);
        if (!$registerResult)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $registerResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function generateOtp()
    {
        $isOTPSent = $this->kawanLamaSystemLib->generateOtpAce($this->params);
        if (!$isOTPSent)
            return $this->sendErrorResponse(
                $this->kawanLamaSystemLib->getErrorCode(),
                $this->kawanLamaSystemLib->getErrorMessages()
            );

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function validateOtp()
    {
        $validateOtpAce = $this->kawanLamaSystemLib->validateOtpAce($this->params);
        if (!$validateOtpAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getById()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamFields = ["id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamFields);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['id']);
        if (!$customerAceDetail)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $isAllowUpgradeMember = substr($customerAceDetail['ace_customer_member_no'], 0, 2) == "AR" ? false
            : $this->kawanLamaSystemLib->checkCustomerWithUpdateAceMemberSKU($params['id']);

        $customerHO = $this->kawanLamaSystemLib->getCustomerDataAce($customerAceDetail);
        if (!$customerHO)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $customerHO[0] += ["Is_Allow_Upgrade_Member" => $isAllowUpgradeMember];

        #region update customer ace details and customer 2 group
        $now = new \DateTime("now");
        $expDate = new \DateTime($customerHO[0]["Exp_Date"]);
        if (
            array_key_exists('expired_date', $customerAceDetail) ||
            ($customerAceDetail['expired_date'] != $expDate &&
                $now <= $expDate)
        ) {
            $customerAceDetailModel = new \Models\CustomerAceDetails;
            $customer2GroupModel = new \Models\Customer2Group;

            $manager = new TxManager();
            $manager->setDbService($customerAceDetailModel->getConnection());
            $manager->setDbService($customer2GroupModel->getConnection());
            $transaction = $manager->get();

            try {
                $sql = "UPDATE customer_ace_details " .
                    "SET ace_customer_member_no = '" . $customerHO[0]["Card_Id"] . "'," .
                    "expired_date ='" . $expDate->format('Y-m-d 23:59:59') . "', " .
                    "status = 1 " .
                    "WHERE customer_id =" . $customerAceDetail['customer_id'];

                $customerAceDetailModel->useWriteConnection();
                $customerAceDetailModel->getDi()->getShared($customerAceDetailModel->getConnection())->query($sql);
                $customerAceDetailModel->setTransaction($transaction);

                $sql2 = "UPDATE customer_2_group SET expiration_date ='" . $expDate->format('Y-m-d 23:59:59') . "', " .
                    "is_verified = 10, update_timestamp ='" . $now->format('Y-m-d 23:59:59') . "', member_number = '" . $customerHO[0]["Card_Id"] . "' " .
                    "WHERE customer_id ='" . $customerAceDetail['customer_id'] . "' " .
                    "AND group_id = 4";

                $customer2GroupModel->useWriteConnection();
                $customer2GroupModel->getDi()->getShared($customer2GroupModel->getConnection())->query($sql2);
                $customer2GroupModel->setTransaction($transaction);

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollback();
                LogHelper::log("getById", "EXCEPTION OCCURED WHEN UPDATING DATA: " . $e->getMessage() .
                    " ,CUSTOMER ID: " . $customerAceDetail['customer_id']);
            }
        }
        #endregion
        $response["data"] = $this->kawanLamaSystemLib->mappingCustomerDataAce($customerHO[0]);
        $response["messages"] = ["success"];

        $this->rrResponse->setContentType("application/json");

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function update()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['id']);
        if (!$customerAceDetail)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $customer = $this->kawanLamaSystemLib->getCustomerDataAce($customerAceDetail);
        if (!$customer)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $updatedData = $this->kawanLamaSystemLib->updateCustomerData($customer[0], $params);
        $updateCustomerAce = $this->kawanLamaSystemLib->updateCustomerAce($updatedData, $customerAceDetail["customer_token"], $params['id']);
        if (!$updateCustomerAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['data'] = [];
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function changePasskey()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["id", "old_password", "new_password"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if (strlen($params["new_password"]) != 6) {
            return $this->sendErrorResponse(400, "Panjang Password baru harus 6 digit");
        }

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['id']);
        if (!$customerAceDetail)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $result = $this->kawanLamaSystemLib->changePassKey(
            $params["id"],
            $params["old_password"],
            $params["new_password"],
            $customerAceDetail["customer_token"],
            $customerAceDetail["ace_customer_id"]
        );
        if (!$result)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getPointHistory()
    {
        $dataToReturn = array();
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["page", "size", "bu_code", "customer_id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if ($params["bu_code"] != "AHI" && $params["bu_code"] != "HCI")
            return $this->sendErrorResponse(400, ["Bu Code must be AHI or HCI"]);

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['customer_id']);
        if (!$customerAceDetail)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $dateFrom = "1900-01-01";
        $dateUntil = date("Y-m-d");

        $pointHistoryAce = $this->kawanLamaSystemLib->getPointHistoryAce(
            $params["bu_code"],
            $params["page"],
            $params["size"],
            $dateFrom,
            $dateUntil,
            $customerAceDetail["customer_token"],
            $customerAceDetail["ace_customer_id"]
        );
        if (!$pointHistoryAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $dataToReturn = array(
            'rows'          => $pointHistoryAce == 1 ? [] : $pointHistoryAce["rows"],
            'point_expired' => $pointHistoryAce == 1 ? 0 : $pointHistoryAce["point_expired"]
        );

        $response['data'] = $dataToReturn;
        $response['count'] = (int)$pointHistoryAce["count"];
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);

        return $this->rrResponse->send();
    }

    public function getTransactionHistory()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];

        $validate = $this->historyTransactionValidation($params);

        if ($validate) {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['customer_id']);
            if (!$customerAceDetail)
                return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

            $isWarrantyDisplace = $this->serviceCenterLib->displaceNonMembershipDataToMembershipTable(
                $params['customer_id'],
                $customerAceDetail["ace_customer_id"]
            );
            if (!$isWarrantyDisplace) {
                return $this->sendErrorResponse($this->serviceCenterLib->getErrorCode(), $this->serviceCenterLib->getErrorMessages());
            }

            $dateFrom = "1900-01-01";
            $dateUntil = date("Y-m-d");
            $transactionHistoryAce = $this->kawanLamaSystemLib->getTransactionHistoryAce(
                $params["bu_code"],
                $params["page"],
                $params["size"],
                $dateFrom,
                $dateUntil,
                "offline",
                $customerAceDetail["customer_token"],
                $customerAceDetail["ace_customer_id"]
            );

            if (!$transactionHistoryAce)
                return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

            $filteredOfflineTransactionRows = [];
            $transactionTotalDataCount = 0;
            if (is_array($transactionHistoryAce["rows"]) && count($transactionHistoryAce["rows"]) > 0) {
                for ($i = 0; $i < count($transactionHistoryAce["rows"]); $i++) {
                    // Get Only Offline Type Transaction And Not Reversal
                    if (
                        $transactionHistoryAce["rows"][$i]["trans_type"] === "Offline" &&
                        (int)$transactionHistoryAce["rows"][$i]["amount"] > 0
                    )
                        array_push($filteredOfflineTransactionRows, $transactionHistoryAce["rows"][$i]);
                }
                $transactionTotalDataCount = (int)$transactionHistoryAce["count"];
            }

            $response['data'] = $filteredOfflineTransactionRows;
            $response['count'] = $transactionTotalDataCount;
            $response['messages'] = ["sukses"];
            $this->rrResponse->setContent($response);

            return $this->rrResponse->send();
        }

        return;
    }

    public function getTransactionOnlineHistory()
    {
        $params = $this->params;
        $response['errors'] = [];
        $response['data'] = [];

        try {
            $validate = $this->historyTransactionValidation($params);

            if ($validate) {
                $dateFrom = (isset($this->params['date_from']))
                    ? $this->params['date_from'] : '1900-01-01';
                $dateUntil = (isset($this->params['date_to']))
                    ? $this->params['date_to'] : date('Y-m-d');
                $transactionHistoryAce = $this->kawanLamaSystemLib->getOnlineTransactionHistoryAce(
                    $params['page'],
                    $params['size'],
                    $dateFrom,
                    $dateUntil,
                    $params['customer_id']
                );
                if (!$transactionHistoryAce)
                    return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

                $response['data'] = $transactionHistoryAce['data'] == 1 ? [] : $transactionHistoryAce['data'];
                $response['count'] = (int)$transactionHistoryAce['count'];
                $response['messages'] = ['success'];
                $this->rrResponse->setContent($response);
            }
        } catch (\Exception $e) {
            LogHelper::log("get_online_transaction_history_ace", "EXCEPTION MESSAGE: " . $e->getMessage() . " ,CUSTOMER ID: " . $params['customer_id']);
        }

        return $this->rrResponse->send();
    }

    public function getTotalAmountTransactionByCustomerId()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamFields = ["customer_id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamFields);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['customer_id']);
        if (!$customerAceDetail)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $dateFrom = "1970-01-01";
        $dateUntil = date("Y-m-d");

        $totalTransactionAmount = 0;

        // Get all transactions
        $transactionHistoryAce = $this->kawanLamaSystemLib->getTransactionHistoryAce(
            "AHI",
            "1",
            "999",
            $dateFrom,
            $dateUntil,
            "offline",
            $customerAceDetail["customer_token"],
            $customerAceDetail["ace_customer_id"]
        );

        if (!$transactionHistoryAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        if (is_array($transactionHistoryAce["rows"]) && count($transactionHistoryAce["rows"]) > 0) {
            for ($i = 0; $i < count($transactionHistoryAce["rows"]); $i++) {
                $totalTransactionAmount += (int)$transactionHistoryAce["rows"][$i]['amount'];
            }
        }

        $transactionHistoryAce = $this->kawanLamaSystemLib->getTransactionHistoryAce(
            "AHI",
            "1",
            "999",
            $dateFrom,
            $dateUntil,
            "online",
            $customerAceDetail["customer_token"],
            $customerAceDetail["ace_customer_id"]
        );

        if (!$transactionHistoryAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        if (is_array($transactionHistoryAce["rows"]) && count($transactionHistoryAce["rows"]) > 0) {
            for ($i = 0; $i < count($transactionHistoryAce["rows"]); $i++) {
                $totalTransactionAmount += (int)$transactionHistoryAce["rows"][$i]['amount'];
            }
        }

        $response['data'] = $totalTransactionAmount;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);

        return $this->rrResponse->send();
    }

    public function checkEmailAvailable()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["email", "company_id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if ($params["company_id"] != "AHI" && $params["company_id"] != "HCI")
            return $this->sendErrorResponse(400, ["Company Id harus AHI atau HCI"]);

        $companyType = $params["company_id"] == "AHI" ? "A" : "I";

        $isEmailAvailableAce = $this->kawanLamaSystemLib->checkEmailAvailableAce($params['email'], $companyType);
        if (!$isEmailAvailableAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['data'] = $isEmailAvailableAce;
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function checkPhoneAvailable()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["phone", "company_id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if ($params["company_id"] != "AHI" && $params["company_id"] != "HCI")
            return $this->sendErrorResponse(400, ["Company ID harus AHI atau HCI"]);

        $companyType = $params["company_id"] == "AHI" ? "A" : "I";

        $isPhoneAvailableAce = $this->kawanLamaSystemLib->checkPhoneAvailableAce($params['phone'], $companyType);
        if (!$isPhoneAvailableAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['data'] = $isPhoneAvailableAce;
        $response['messages'] = ["success"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function createEmail()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "email"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if (!filter_var($params["email"], FILTER_VALIDATE_EMAIL))
            return $this->sendErrorResponse(400, "E-mail format tidak valid");

        $checkUniquePrimaryData = $this->kawanLamaSystemLib->checkUniquePrimaryData("email", $params['customer_id'], $params['email']);
        if (!$checkUniquePrimaryData)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['customer_id']);
        if (!$customerAceDetail)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $createEmailAce = $this->kawanLamaSystemLib->createCustomerEmail(
            $params['customer_id'],
            $customerAceDetail["ace_customer_id"],
            $params['email']
        );
        if (!$createEmailAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['data'] = $createEmailAce;
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function changeEmail($id)
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["id", "old_email", "new_email", "otp_number"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if (!filter_var($params["new_email"], FILTER_VALIDATE_EMAIL))
            return $this->sendErrorResponse(400, "E-mail format tidak valid");

        $checkUniquePrimaryData = $this->kawanLamaSystemLib->checkUniquePrimaryData("email", $id, $params['new_email']);
        if (!$checkUniquePrimaryData)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $id);
        if (!$customerAceDetail)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $isEmailVerified = array_key_exists("is_email_verified", $customerAceDetail) ? $customerAceDetail["is_email_verified"] : "0";
        $changeEmailAce = $this->kawanLamaSystemLib->changeCustomerEmail(
            $id,
            $customerAceDetail["ace_customer_id"],
            $params['new_email'],
            $params['old_email'],
            $params['otp_number'],
            $isEmailVerified
        );
        if (!$changeEmailAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['data'] = $changeEmailAce;
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function changePhone()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["id", "old_phone", "new_phone", "otp_number"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $checkUniquePrimaryData = $this->kawanLamaSystemLib->checkUniquePrimaryData("phone", $params['id'], $params['new_phone']);
        if (!$checkUniquePrimaryData)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['id']);
        if (!$customerAceDetail)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $changePhoneAce = $this->kawanLamaSystemLib->changeCustomerPhone(
            $params['id'],
            $customerAceDetail["ace_customer_id"],
            $params['new_phone'],
            $params['old_phone'],
            $params['otp_number'],
            "0"
        );
        if (!$changePhoneAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['data'] = $changePhoneAce;
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function changeAddress()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["id", "address_id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $customerAddress = $this->kawanLamaSystemLib->changePrimaryCustomerAddress($params["address_id"], $params['id']);
        if (!$customerAddress)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['data'] = $customerAddress;
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function forgetPasskey()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["company_id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if ($params["company_id"] != "AHI" && $params["company_id"] != "HCI")
            return $this->sendErrorResponse(400, ["Company Id must be AHI or HCI"]);

        $hasEmail = array_key_exists("email", $params) && $params["email"] != null;
        $hasPhone = array_key_exists("phone", $params) && $params["phone"] != null;

        if (!$hasEmail && !$hasPhone)
            return $this->sendErrorResponse(400, ["Diperlukan e-mail atau nomor handphone"]);

        $email = $hasEmail ? $params["email"] : false;
        $phone = $hasPhone ? $params["phone"] : false;

        $forgetPasskeyAce = $this->kawanLamaSystemLib->forgetPasskeyAce($email, $phone, $params["company_id"]);
        if (!$forgetPasskeyAce)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $sendPasskeyToCustomer = $this->kawanLamaSystemLib->sendPasskeyToCustomer(
            $email,
            $phone,
            $forgetPasskeyAce[0]["passkey"],
            $forgetPasskeyAce[1]["card_id"]
        );
        if (!$sendPasskeyToCustomer)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $response['data'] = [];
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getAddress()
    {
        $params = $this->params;
        $requiredParamField = ["customer_id"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $address = $this->kawanLamaSystemLib->getCustomerAddressAce($params["customer_id"]);
        if ($this->kawanLamaSystemLib->getErrorCode())
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $responseContent['errors'] = [];
        $responseContent['data'] = $address;
        $responseContent['messages'] = ["sukses"];

        $this->rrResponse->setContent($responseContent);
        return $this->rrResponse->send();
    }

    public function createAddress()
    {
        $params = $this->params;
        $requiredParamField = ["customer_id", "first_name", "full_address", "phone", "post_code", "province", "city", "kecamatan", "address_name"];

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $address = $this->kawanLamaSystemLib->createCustomerAddressAce($params);
        if (!$address)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $responseContent['errors'] = [];
        $responseContent['data'] = [];
        $responseContent['messages'] = ["sukses"];

        $this->rrResponse->setContent($responseContent);
        return $this->rrResponse->send();
    }

    public function updateAddress()
    {
        $params = $this->params;
        $requiredParamField = ["address_id", "customer_id", "first_name", "full_address", "phone", "post_code", "province", "city", "kecamatan", "address_name"];

        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $address = $this->kawanLamaSystemLib->updateCustomerAddressAce($params["address_id"], $params);

        if (!$address)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $responseContent['errors'] = [];
        $responseContent['data'] = [];
        $responseContent['messages'] = ["sukses"];

        $this->rrResponse->setContent($responseContent);
        return $this->rrResponse->send();
    }

    public function deleteAddress()
    {
        $requiredParamField = ["address_id"];
        $params = $this->params;

        $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $needDecrypt = isset($filters["decrypt"]) && $filters["decrypt"] == "yes";

        $address = $this->kawanLamaSystemLib->deleteCustomerAddressAce($params["address_id"], $needDecrypt, $params["customer_id"]);

        if (!$address)
            return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

        $responseContent['errors'] = [];
        $responseContent['data'] = [];
        $responseContent['messages'] = ["sukses"];

        $this->rrResponse->setContent($responseContent);
        return $this->rrResponse->send();
    }

    public function updateWebhook()
    {
        $params = $this->params;
        $aceCustomerId = $params["ace_customer_id"];

        if (!array_key_exists("new_email", $params) && !array_key_exists("new_phone_number", $params)) {
            return $this->sendErrorResponse(
                422,
                "Request cannot be processed because parameter key for new_email and parameter new_phone_number is empty"
            );
        }

        $newEmail = array_key_exists("new_email", $params) ? $params["new_email"] : "";
        $newPhone = array_key_exists("new_phone_number", $params) ? $params["new_phone_number"] : "";

        if (!$newEmail && !$newPhone) {
            return $this->sendErrorResponse(
                422,
                "Request cannot be processed because parameter new_email and parameter new_phone_number value is empty"
            );
        }

        $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("ace_customer_id = '" . $aceCustomerId . "'");
        if (!$customerAceDetail) {
            LogHelper::log(
                "update_webhook",
                "No customer data found" .
                    ", ace_customer_id: " . $aceCustomerId
            );
            return false;
        }

        // For covering scenario when user want to nullify by sending
        // email: email@email.com and phone: cust memiliki 2 kartu
        // The '||' logic is for covering when email is empty but phone '-'
        // It is also for nullify
        if (
            (($newEmail == "-" || $newEmail == "email@email.com") && substr($newPhone, 0, 4) != "+628"
            ) ||
            (empty($newEmail) && $newPhone === '-'
            )
        ) {
            $newEmail = "-";
            $newPhone = "-";
        }
        // Normal change scenario 
        else if ($newEmail !== "-" || $newPhone !== "-") {
            if (!empty($newEmail)) {
                if (!filter_var($newEmail, FILTER_VALIDATE_EMAIL)) {
                    LogHelper::log("update_webhook", "Request cannot be processed because parameter new_email is not a valid email");
                    return $this->sendErrorResponse(
                        422,
                        "Request cannot be processed because parameter new_email is not a valid email"
                    );
                }

                $customerDetailTmp = $this->kawanLamaSystemLib->getCustomerDetail(
                    "email = '" . $newEmail . "' AND company_code = 'AHI' AND customer_id != " . $customerAceDetail['customer_id']
                );
                if ($customerDetailTmp) {
                    LogHelper::log("update_webhook", "Request cannot be processed because email $newEmail already exist in the database");
                    return $this->sendErrorResponse(
                        422,
                        "Request cannot be processed because email $newEmail already exist in the database"
                    );
                }
            }

            if (!empty($newPhone)) {
                $newPhone = GeneralHelper::formatNormalPhoneNumber($params["new_phone_number"]);

                $customerDetailTmp = $this->kawanLamaSystemLib->getCustomerDetail("phone = '" . $newPhone . "' AND company_code = 'AHI' AND customer_id != " . $customerAceDetail['customer_id']);
                if ($customerDetailTmp) {
                    LogHelper::log("update_webhook", "Request cannot be processed because phone number $newPhone already exist in the database");
                    return $this->sendErrorResponse(
                        422,
                        "Request cannot be processed because phone number $newPhone already exist in the database"
                    );
                }
            }
        }

        $this->kawanLamaSystemLib->updateWebhook(
            $customerAceDetail['customer_id'],
            $newEmail,
            $newPhone
        );

        $responseContent['errors'] = [];
        $responseContent['data'] = [];
        $responseContent['messages'] = ["sukses"];

        $this->rrResponse->setContent($responseContent);
        return $this->rrResponse->send();
    }

    public function verifyEmailAce()
    {
        $params = $this->params;
        $response['errors'] = [];
        $response['data'] = [];
        $requiredParamField = ["customer_id", "email", "is_verified"];

        try {

            $validate = $this->kawanLamaSystemLib->validateRequired($params, $requiredParamField);
            if (!validate) {
                return $this->sendErrorResponse(400, $validate);
            }

            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $params['customer_id']);
            if (!$customerAceDetail)
                return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

            $isEmailVerified = $this->kawanLamaSystemLib->verifyEmail($customerAceDetail["ace_customer_id"], $params["is_verified"]);
            if (!$isEmailVerified)
                return $this->sendErrorResponse($this->kawanLamaSystemLib->getErrorCode(), $this->kawanLamaSystemLib->getErrorMessages());

            $response['data'] = $isEmailVerified;
            $response['messages'] = ['sukses'];
            $this->rrResponse->setContent($response);
        } catch (\Exception $e) {
            LogHelper::log("verify_email_ace", "EXCEPTION MESSAGE: " . $e->getMessage() . " ,CUSTOMER ID: " . $params['customer_id']);
        }

        return $this->rrResponse->send();
    }
}
