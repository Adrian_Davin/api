<?php

namespace Controllers\AceController;

use Controllers\BaseController;
use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Library\Response;
use Library\KawanLamaSystem;
use Library\Marketing;
use Library\APIWrapper;
use Library\AceHardware\PayWithPoint;

class PayWithPointAceController extends BaseController
{
    protected $kawanLamaSystemLib;
    protected $marketingLib;
    protected $payWithPointLib;
    protected $rrResponse;

    public function onConstruct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();
        $this->marketingLib = new Marketing();
        $this->payWithPointLib = new PayWithPoint();

        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "OK")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"])
    {
        if ($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = [];
        $response["errors"] = [
            "code" => $errorCode,
            "title" => $this->rrResponse->getResponseDescription($errorCode),
            "messages" => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function redeemPoint()
    {
        $requiredParamFields = ["customer_id", "otp_number", "point_redeem", "cart_id"];
        $validate = $this->kawanLamaSystemLib->validateRequired($this->params, $requiredParamFields);
        if ($validate) return $this->sendErrorResponse(400, $validate);

        if ((int)$this->params['point_redeem'] < 0) {
            return $this->sendErrorResponse(
                400,
                'Point yang ditukarkan tidak boleh kurang dari 0'
            );
        }

        try {
            $customerDetail = $this->kawanLamaSystemLib->getCustomerDetail("customer_id = " . $this->params['customer_id']);
            if (!$customerDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $this->params['customer_id']);
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }
            if (substr($customerAceDetail['ace_customer_member_no'], 0, 3) === 'TAM') {
                return $this->sendErrorResponse(
                    400,
                    'Customer dengan member TAM tidak dapat menukarkan poin ke voucher'
                );
            }

            $customerAceHO = $this->kawanLamaSystemLib->getCustomerDataAce($customerAceDetail);
            if (!$customerAceHO) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $customerPoints = 0;
            $customerPoints = (int)substr($customerAceHO[0]['Jml_Point'], 0, strpos($customerAceHO[0]['Jml_Point'], ' '));
            if ((int) $this->params['point_redeem'] > $customerPoints) {
                return $this->sendErrorResponse(
                    400,
                    'Point yang ditukarkan tidak boleh lebih dari poin yang dimiliki'
                );
            }

            $now = new \DateTime("now");
            $expDate = new \DateTime($customerAceHO[0]['Exp_Date']);
            if ($now > $expDate) {
                return $this->sendErrorResponse(
                    400,
                    'Masa berlaku membership anda sudah berakhir'
                );
            }

            $apiWrapper = new APIWrapper(getenv('CART_API'));
            $apiWrapper->setEndPoint("/cart/" . $this->params['cart_id']);
            if ($apiWrapper->sendRequest("get")) {
                $apiWrapper->formatResponse();
            } else {
                LogHelper::log('redeem_point_ace', '[ERROR]: ' . json_encode($apiWrapper->getData()));
                return $this->sendErrorResponse(
                    400,
                    "Gagal untuk mendapatkan customer cart"
                );
            }

            $customerCart = $apiWrapper->getData();
            $voucherAmount = (int)$this->params['point_redeem'] * (int)getenv('POINT_VOUCHER_WORTH_AHI'); // n * 2500
            if ((int)$voucherAmount > (int)$customerCart['subtotal']) {
                return $this->sendErrorResponse(
                    400,
                    "Penukaran point tidak boleh melebihi harga yang harus dibayarkan"
                );
            }

            $isPointRedeemValidAgainstTransaction = $this->payWithPointLib->validatePointRedeemAgainstCartTransaction(
                $this->params['point_redeem'],
                $this->params['cart_id']
            );
            if (!$isPointRedeemValidAgainstTransaction) {
                return $this->sendErrorResponse(
                    $this->payWithPointLib->getErrorCode(),
                    $this->payWithPointLib->getErrorMessages()
                );
            }

            $isValidOTP = $this->kawanLamaSystemLib->validateOtpAce([
                'action' => 'redeem_point',
                'customer_id' => $this->params['customer_id'],
                'phone' => GeneralHelper::formatPhoneNumber($customerDetail['phone']),
                'otp_number' => $this->params['otp_number']
            ]);
            if (!$isValidOTP) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $voucherGeneratorParams = [
                'rule_id' => getenv('REDEEM_SALES_RULE_AHI'),
                'customer_id' => $this->params['customer_id'],
                'company_code' => 'ODI',
                'type' => 5, // Type = redeem
                'expiration_date' => date("Y-m-d 23:59:59", strtotime("+6 month", time())),
                'qty' => 1,
                'voucher_amount' => $voucherAmount,
                'length' => 7,
                'format' => 'alphanum',
                'prefix' => getenv('POINT_REDEEM_PREFIX_HO'),
            ];
            $this->marketingLib->generateVoucher($voucherGeneratorParams);
            $this->marketingLib->validateGeneratedVoucher($voucherGeneratorParams);

            $voucherGeneratorParams['voucher_code'] = $this->marketingLib->getVoucherList()[0];

            $salesRuleVoucher = $this->payWithPointLib->saveAndProcessPointRedeem(
                $this->params['customer_id'],
                $this->params['point_redeem'],
                $voucherGeneratorParams,
                $customerAceDetail
            );
            if (!$salesRuleVoucher) {
                return $this->sendErrorResponse(
                    $this->payWithPointLib->getErrorCode(),
                    $this->payWithPointLib->getErrorMessages()
                );
            }

            // Posting Journal 3601
            $this->kawanLamaSystemLib->postingJournal($salesRuleVoucher);

            // Directly assign voucher to existing cart
            $asignToCart = $this->payWithPointLib->applyVoucherToCart(
                $this->params['cart_id'],
                $voucherGeneratorParams['voucher_code']
            );
            if (!$asignToCart) {
                return $this->sendErrorResponse(
                    $this->payWithPointLib->getErrorCode(),
                    $this->payWithPointLib->getErrorMessages()
                );
            }
        } catch (\Exception $ex) {
            LogHelper::log("redeem_point_ace", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,CUSTOMER ID: " . $this->params['customer_id']);
        }

        $response['errors'] = [];
        $response['data'] = [];
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }
}
