<?php

namespace Controllers\AceController;

use Controllers\BaseController;
use Helpers\LogHelper;
use Library\Response;
use Library\KawanLamaSystem;

class TrackingInstallationAceController extends BaseController
{
    protected $kawanLamaSystemLib;
    protected $rrResponse;

    public function onConstruct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();

        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "OK")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"])
    {
        if ($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = [];
        $response["errors"] = [
            "code" => $errorCode,
            "title" => $this->rrResponse->getResponseDescription($errorCode),
            "messages" => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function trackingInstallationList($receipt_no, $customer_id)
    {
        $returnData = array(
            "transactions" => []
        );

        if (empty($receipt_no) || empty($customer_id)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter tidak lengkap";
            return;
        }

        $aceWebAPI = new \Library\AceWebAPI();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $customer_id);
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $sessionHeader = '-;' . $customerAceDetail['ace_customer_member_no'];
            $apiResponse = $aceWebAPI->trackingInstallationPerReceiptNumber(
                $receipt_no,
                $sessionHeader
            );
            if (!$apiResponse) {
                return $this->sendErrorResponse(
                    400,
                    "Terjadi kesalahan ketika mendapatkan data daftar instalasi"
                );
            }

            foreach ($apiResponse as $val) {
                array_push($returnData['transactions'], [
                    'transaction_no' => $val['notrans'],
                    'job_id' => $val['jobid'],
                    'installation_status' => $val['ins_status'],
                    'is_active' => $val['is_active']
                ]);
            }
        } catch (\Exception $ex) {
            LogHelper::log(
                "tracking_installation_list",
                "EXCEPTION OCCURED: " . $ex->getMessage() .
                    " ,CUSTOMER ID: " . $customer_id . ", RECEIPT NUMBER: " . $receipt_no
            );
        }

        $response['errors'] = [];
        $response['data'] = $returnData;
        $response['messages'] = ["Sukses"];
        $response['count'] = count($returnData['transactions']);
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function trackingInstallationDetail($transaction_no, $job_id, $customer_id)
    {
        $returnData = [];

        if (empty($transaction_no) || empty($job_id) || empty($customer_id)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter tidak lengkap";
            return;
        }

        $aceWebAPI = new \Library\AceWebAPI();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $this->params['customer_id']);
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $sessionHeader = '-;' . $customerAceDetail['ace_customer_member_no'];
            $apiResponse = $aceWebAPI->trackingInstallationDetailPerTransactionNumberAndJob(
                $transaction_no,
                $job_id,
                $sessionHeader
            );
            if (!$apiResponse) {
                return $this->sendErrorResponse(
                    400,
                    "Terjadi kesalahan ketika mendapatkan data detail instalasi"
                );
            }

            $returnData = array(
                'transaction_no' => $apiResponse['notrans'],
                'job_id' => $apiResponse['jobid'],
                'installation_date' => $apiResponse['ins_date'],
                'installation_status' => $apiResponse['ins_status'],
                'installation_start_time' => $apiResponse['ins_starttime'],
                'installation_end_time' => $apiResponse['ins_endtime'],
                'is_rating' => $apiResponse['is_rating'],
                'deliveryDate' => $apiResponse['deliveryDate'],
                'customer_sequence' => $apiResponse['customerSeq'],
                'current_sequence' => $apiResponse['currentSeq'],
                'installer_name' => $apiResponse['installerName'],
                'delivery_eta' => $apiResponse['deliveryETA'],
                'customer_address_long' => $apiResponse['cust_addr_lng'],
                'customer_address_lat' => $apiResponse['cust_addr_lat'],
                'items' => $apiResponse['items']
            );
        } catch (\Exception $ex) {
            LogHelper::log("tracking_installation_detail", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,CUSTOMER ID: " . $customer_id);
        }

        $response['errors'] = [];
        $response['data'] = $returnData;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function trackingInstallationInstallerRoutes($transaction_no, $job_id, $customer_id)
    {
        $returnData = [];

        if (empty($transaction_no) || empty($job_id) || empty($customer_id)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter tidak lengkap";
            return;
        }

        $aceWebAPI = new \Library\AceWebAPI();
        try {
            $customerAceDetail = $this->kawanLamaSystemLib->getCustomerAceDetail("customer_id = " . $this->params['customer_id']);
            if (!$customerAceDetail) {
                return $this->sendErrorResponse(
                    $this->kawanLamaSystemLib->getErrorCode(),
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            $sessionHeader = '-;' . $customerAceDetail['ace_customer_member_no'];
            $apiResponse = $aceWebAPI->trackingInstallationInstallerRoutes(
                $transaction_no,
                $job_id,
                $sessionHeader
            );
            if (!$apiResponse) {
                return $this->sendErrorResponse(
                    400,
                    "Terjadi kesalahan ketika mendapatkan data rute instalasi"
                );
            }

            $returnData = array(
                'installation_status' => $apiResponse['ins_status'],
                'lat' => $apiResponse['lat'],
                'long' => $apiResponse['lon'],
                'date_time' => $apiResponse['datetime']
            );
        } catch (\Exception $ex) {
            LogHelper::log("tracking_installation_installer_routes", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,CUSTOMER ID: " . $customer_id);
        }

        $response['errors'] = [];
        $response['data'] = $returnData;
        $response['messages'] = ["Sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }
}
