<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 13/1/2017
 * Time: 2:23 PM
 */

namespace Controllers;

use Library\MobileAppsNotification;

/**
 * Class ControllerNameController
 * @package Controllers
 */
class NotificationsController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function checkLocation()
    {
        $apps = new MobileAppsNotification();
        $notification['notified'] = $apps->checkLocation($this->params);
        if ($notification['notified']) {
            $notification['fcm_interaction'] = $apps->sendNotification($this->params['token']);
        }
        $notification['token'] = $this->params['token'];
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $rupaResponse->setContent(['data' => $notification]);
        $rupaResponse->send();
    }

    public function slackNotification(){
        $notificationText = $this->params['notif_text'];
        $params = [ 
            "channel" => $this->params['notif_channel'],
            "username" => $this->params['notif_username'],
            "text" => $notificationText,
            "icon_emoji" => $this->params['notif_icon_emoji']
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }
}