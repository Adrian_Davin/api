<?php

namespace Controllers\InformaController;

use Helpers\LogHelper;
use Library\InformaApp;
use Library\Response;
use Controllers\BaseController;

class CustomerInformaController extends BaseController
{
    protected $informaAppLib;
    protected $rrResponse;
    public $maintenanceMessage = "Mohon maaf, sistem dalam proses pemeliharaan. Silahkan hubungi CS Informa untuk informasi lebih lanjut.";
    public $maintenanceDate = "2021-10-01";

    public function onConstruct() {
        $this->informaAppLib = new InformaApp();
        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "Ok")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"]) {
        if($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = $this->informaAppLib->getErrorData();
        $response["errors"] = [
            "code" => $errorCode,
            "title" => $this->rrResponse->getResponseDescription($errorCode),
            "messages" => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function login() {
        $this->informaAppLib->setAppVersion($this->params);

        $loginResult = $this->informaAppLib->customerLogin($this->params);
        if (! $loginResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        
        
        $response['errors'] = [];
        $response['data'] = $loginResult;
        $response['messages'] = ["success"];
        
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function register() {
        $this->informaAppLib->setAppVersion($this->params);

        $registerResult = $this->informaAppLib->registerCustomerInforma(true, $this->params);
        if (! $registerResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $registerResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function forgotPassword() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];

        $this->informaAppLib->setAppVersion($params);

        if (!array_key_exists("company_id", $params)) {
            $params["company_id"] = "HCI";
        }

        if($params["company_id"] != "AHI" && $params["company_id"] != "HCI")
            return $this->sendErrorResponse(400, ["Company Id must be AHI or HCI"]);

        $hasEmail = array_key_exists("email", $params) && $params["email"] != null;

        if(!$hasEmail)
            return $this->sendErrorResponse(400, ["Diperlukan e-mail"]);

        $email = $hasEmail ? $params["email"] : false;

        $forgetPasskeyInforma = $this->informaAppLib->forgetPasskey($email, $params["company_id"]);
        if(! $forgetPasskeyInforma)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $forgetPasskeyInforma;
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getById() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $customerData = $this->informaAppLib->getCustomerData("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerData) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        } else {
            if (isset($customerData["gender"])) {
                $customerDataInforma["sex"] =  $this->informaAppLib->getSexId($customerData["gender"]);
            }
            $customerDataInforma["birth_date"] =  isset($customerData["birth_date"]) ? $customerData["birth_date"] : "";
        }

        if (isset($customerDetail["ktp"])) {
            $customerDataInforma["ktp"] = $customerDetail["ktp"];
        }

        if (isset($customerDetail["marital_status"])) {
            $customerDataInforma["marital_status"] = $customerDetail["marital_status"];
        }


        $memberData = array();
        $isAllowUpgradeMember = true;
        $isPaidMember = false;
        $memberData["P_CUST_ID"] = "";
        if ($customerDataInforma['is_member'] != 0){
            $memberData = $this->informaAppLib->getMemberDataInforma($informaAuth["uid"]);
            if (!$memberData) {
                return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
            }

            $isAllowUpgradeMember = strtoupper(substr($memberData["P_CARD_ID"], 0, 3)) == "TIM";
            $isPaidMember = strtoupper(substr($memberData["P_CARD_ID"], 0, 2)) == "IR";
        }
        
        if ($isAllowUpgradeMember) {
            $isAllowUpgradeMember = $this->informaAppLib->checkCustomerWithUpdateInformaMemberSKU($params["customer_id"]);
        }

        $customerDataInforma["member"] = $memberData;
        $responseData = $this->informaAppLib->mappingCustomerData($customerDataInforma);
        $responseData["customer_id"] = $params["customer_id"];
        $responseData["details"] = $customerDetail;
        $responseData["is_allow_upgrade_member"] = $isAllowUpgradeMember;
        $responseData["is_paid_member"] = $isPaidMember;
        $responseData["is_phone_valid"] = $responseData["phone"] != "";

        // get informaCustomerData & informaMemberData
        $response["data"] = $responseData;
        $response["messages"] = ["success"];

        // data user + data member

        $this->rrResponse->setContentType("application/json");

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
        
    }

    public function getMemberById() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "Tidak bisa diakses oleh non member");
        }
        
        $membershipData = $this->informaAppLib->getMembershipDataInforma($customerDetail["informa_customer_id"]);
        if (!$membershipData) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        // get informaCustomerData & informaMemberData
        $response["data"] = $membershipData;
        $response["messages"] = ["success"];
        
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function changePassword() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "old_password", "new_password", "new_password_confirm"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if($validate)
            return $this->sendErrorResponse(400, $validate);

        if($params["new_password"] != $params["new_password_confirm"]) {
            return $this->sendErrorResponse(400, "Konfirmasi password tidak cocok");
        }

        $customerData = $this->informaAppLib->getCustomerData("customer_id = " . $params['customer_id']);
        if (!$customerData) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $result = $this->informaAppLib->changePassKey(
            $params["customer_id"],
            $customerData["email"],
            $params["old_password"],
            $params["new_password"],
            $customerDetail["informa_uid"],
            $customerDetail["session_id"]
        );
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response["data"] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function update()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "full_name"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if($validate)
            return $this->sendErrorResponse(400, $validate);
       
        if (!isset($params["city"])) {
            $params["city"] = "";
        }

        if (!isset($params["address"])) {
            $params["address"] = "";
        }
            
        $customerData = $this->informaAppLib->getCustomerData("customer_id = '" . $params["customer_id"] . "'");
        if(! $customerData)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;

        if ($isMember) {
            if (!isset($params["ktp"])) {
                return $this->sendErrorResponse(400, "ktp must be set for member");
            }
    
            if (!isset($params["birth_date"])) {
                return $this->sendErrorResponse(400, "birth_date must be set for member");
            }
      
            if (!isset($params["birth_place"])) {
                return $this->sendErrorResponse(400, "birth_place must be set for member");
            }
       
            if (!isset($params["marital_status"])) {
                return $this->sendErrorResponse(400, "marital_status must be set for member");
            }
    
            if (!isset($params["sex"])) {
                return $this->sendErrorResponse(400, "sex must be set for member");
            }    
    
        }

        $result =  $this->informaAppLib->updateCustomerData( $customerData, $customerDetail, $params, $isMember);
        if(! $result)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        

        $response["data"] = [
            "success" => true,
        ];
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getUserVoucher() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);


        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $storeVoucher = $this->informaAppLib->getUserVoucherCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"]);
        if(! $storeVoucher)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());   

        $dealsVoucher = $this->informaAppLib->getCustomerVoucherDeals($params["customer_id"]);
        if ($dealsVoucher) {
            $voucherResult = $this->informaAppLib->appendDealsVoucher($storeVoucher, $dealsVoucher);
        } else {
            $voucherResult = $storeVoucher;
        }

        $response['errors'] = [];
        $response['data'] = $voucherResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getListNotif() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if (!array_key_exists("page", $params)) {
            $params["page"] = 1;
        }
    
        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $listNotifResult = $this->informaAppLib->getListNotifCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["page"]);
        if(! $listNotifResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $listNotifResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getTransactionHist() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);
        
        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);


        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $listTxHistResult = $this->informaAppLib->getTransactionHistCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"]);
        if(! $listTxHistResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $listTxHistResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getTiering() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);
        
        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);


        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $result = $this->informaAppLib->getTieringCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $result;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getDigitalReceipt() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id", "receipt_no"];
        $this->informaAppLib->setAppVersion($params);
        
        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);


        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $receiptInfo = $this->informaAppLib->getDigitalReceiptCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["receipt_no"]);
        if(! $receiptInfo)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $receiptInfo;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function logout() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);
        
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $logoutResult = $this->informaAppLib->logoutCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"]);
        if(! $logoutResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $logoutResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();

    }

    public function getTransactionDeliveryList() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "receipt_no"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $result = $this->informaAppLib->transactionDeliveryListCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["receipt_no"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getTransactionDeliveryDetail() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "notrans", "jobid", "siteid"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $txDetailResult = $this->informaAppLib->transactionDeliveryDetailCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["notrans"], $params["jobid"], $params["siteid"]);
        if(! $txDetailResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $txDetailResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function postSatisfactionRate() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "transaction_no", "rating_delivery", "rating_installation", "notes"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $requestParams = [
            "notrans" => $params["transaction_no"],
            "ratingDlv" => strval($params["rating_delivery"]),
            "ratingIns" => strval($params["rating_installation"]),
            "notes" => $params["notes"],
        ];

        $satisfactionRateResult = $this->informaAppLib->satisfactionRateCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $requestParams);
        if(! $satisfactionRateResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $satisfactionRateResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }


    public function verifyEmail() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;
        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $emailVerifResult = $this->informaAppLib->verifyEmailCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"]);
        if(! $emailVerifResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $emailVerifResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function sendChangePhoneOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "new_phone"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $validatePhone = $this->informaAppLib->isphoneAvailable($params["new_phone"], $params['customer_id']);
        if (!$validatePhone) 
            return $this->sendErrorResponse(400, "Gagal, nomor handphone sudah terdaftar");


        $isMember = $customerDataInforma['is_member'] != 0;

        if ($isMember) {
            $sendChangePhoneOTPResult = $this->informaAppLib->sendChangePhoneOTPCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["new_phone"]);
            if(! $sendChangePhoneOTPResult)
                return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
            $sendChangePhoneOTPResult = [
                'success' => true,
                'message' => $sendChangePhoneOTPResult,
            ];
    
        } else {
            return $this->sendErrorResponse(400, "Jadilah member informa agar dapat mengubah nomor HP");
        }

        $sendChangePhoneOTPResult["is_member"] = $isMember;

        $response['data'] = $sendChangePhoneOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function submitChangePhoneOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "new_phone", "otp"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validatePhone = $this->informaAppLib->isphoneAvailable($params["new_phone"], $params['customer_id']);
        if (!$validatePhone) 
            return $this->sendErrorResponse(400, "Nomor handphone yang anda pilih sudah terdaftar, silahkan hubungin customer service kami");

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;
        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $submitChangePhoneOTPResult = $this->informaAppLib->submitChangePhoneOTPCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["new_phone"], $params["otp"]);
        if(! $submitChangePhoneOTPResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $sendChangePhoneProfileResult = $this->informaAppLib->changeProfilePhoneCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $customerDetail["customer_id"], $params["new_phone"]);

        if(! $sendChangePhoneProfileResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $submitChangePhoneOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function verifyPhone() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;
        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $phoneVerifResult = $this->informaAppLib->verifyPhoneCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"]);
        if(! $phoneVerifResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $phoneVerifResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function sendChangeEmailOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "new_email"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validateEmail = $this->informaAppLib->isEmailAvailable($params["new_email"], $params["customer_id"]);
        if (!$validateEmail) 
            return $this->sendErrorResponse(400, "Gagal, email sudah terdaftar");

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;

        if ($isMember) {
            $sendChangeEmailOTPResult = $this->informaAppLib->sendChangeEmailOTPCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["new_email"]);
            if(! $sendChangeEmailOTPResult)
                return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

            $sendChangeEmailOTPResult = [
                'success' => true,
                'message' => $sendChangeEmailOTPResult["message"],
                'otp' => $sendChangeEmailOTPResult["otp"],
            ];
    
        } else {
            return $this->sendErrorResponse(400, "Jadilah member informa agar dapat mengubah email");
        }

        $sendChangeEmailOTPResult["is_member"] = $isMember;

        $response['data'] = $sendChangeEmailOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function submitChangeEmailOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "new_email", "otp"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validateEmail = $this->informaAppLib->isEmailAvailable($params["new_email"], $params["customer_id"]);
        if (!$validateEmail) 
            return $this->sendErrorResponse(400, "Email yang anda pilih sudah terdaftar, silahkan hubungin customer service kami");
    
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $submitChangeEmailOTPResult = $this->informaAppLib->submitChangeEmailOTPCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["new_email"], $params["otp"]);
        if(! $submitChangeEmailOTPResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $changeEmailProfileResult = $this->informaAppLib->changeProfileEmailCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $customerDetail["customer_id"], $customerDataInforma["email"], $params["new_email"]);
        if(! $changeEmailProfileResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());    

        $sendChangeEmailOTPResult["is_member"] = $isMember;

        $response['data'] = $submitChangeEmailOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function verifyPhoneSubmitOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "otp"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;
        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $verifyPhoneOTPResult = $this->informaAppLib->verifyPhoneSubmitOTPCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["otp"]);
        if(! $verifyPhoneOTPResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $verifyPhoneOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getRedeemVoucherList() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $getRedeemVoucherListResult = $this->informaAppLib->getRedeemVoucherListCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"]);
        if(! $getRedeemVoucherListResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $getRedeemVoucherListResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function redeemVoucherSendOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        
        $validateDate = $this->informaAppLib->isCurrentDate($this->maintenanceDate);
        if ($validateDate) {
            return $this->sendErrorResponse(400, $this->maintenanceMessage);
        }

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }
        
        $result = $this->informaAppLib->redeemVoucherSendOTPCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function redeemVoucherSubmitOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "voucher_code", "otp"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validateDate = $this->informaAppLib->isCurrentDate($this->maintenanceDate);
        if ($validateDate) {
            return $this->sendErrorResponse(400, $this->maintenanceMessage);
        }
    

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->informaAppLib->redeemVoucherSubmitOTPCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["voucher_code"], $params["otp"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getTransactionInstallationList() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "receipt_no"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->informaAppLib->transactionInstallationListCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["receipt_no"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getTransactionInstallationDetail() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "notrans", "jobid"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->informaAppLib->transactionInstallationDetailCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["notrans"], $params["jobid"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function payWithPoint() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "otp"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validateDate = $this->informaAppLib->isCurrentDate($this->maintenanceDate);
        if ($validateDate) {
            return $this->sendErrorResponse(400, $this->maintenanceMessage);
        }

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }
        
        $result = $this->informaAppLib->payWithPointCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $params["otp"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getReceiptList() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->informaAppLib->getReceiptListCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], 1);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getPointHistory() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "page"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }
        $isMember = $customerDataInforma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "Tidak bisa diakses oleh non member");
        }
        
        $result = $this->informaAppLib->getPointHistoryCustomerInforma($customerDetail["informa_customer_id"], $params["page"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getCheckEmail() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);


        $customerData = $this->informaAppLib->getCustomerData("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerData) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        if (!isset($customerData["email"]) || is_null($customerData["email"])) {
            return $this->sendErrorResponse(404, "Email tidak ditemukan");
        }

        // get customer data by customer_id
        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;
        if ($isMember) {
            $memberData = $this->informaAppLib->getMemberDataInforma($informaAuth["uid"]);
            if (!$memberData) {
                return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
            }

            $isPaidMember = substr($memberData["P_CARD_ID"], 0, 2) == "IR";
            if ($isPaidMember) {
                return $this->sendErrorResponse(400, "User is already a paid member");
            }
        }

        $checkEmailResult = $this->informaAppLib->getCheckEmailCustomerInforma($customerData["email"]);
        if(! $checkEmailResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $checkEmailResult["status"] = (int)$checkEmailResult["status"];

        $logMsg = "[CHECK_EMAIL] customer_id: ". $params["customer_id"] ." => Response: ". json_encode($checkEmailResult);
        \Helpers\LogHelper::log("informa/informa_check_email_debug", $logMsg, "info");
   
        $registerResult = [];
        $updateResult = false;
        $cardNumber = "";
        // Daftar member baru
        if ($checkEmailResult["status"] == 0 || $checkEmailResult["status"] == 3 ) {
            $registerResult = $this->informaAppLib->postRegisterMemberCustomerInforma($customerData);
            if(! $registerResult)
                return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
            
            $cardNumber = $registerResult[0]["Card"];
        } else if ($checkEmailResult["status"] == 2) {
            $updateResult = $this->informaAppLib->updateTemporaryMemberCustomerInforma($customerDetail["customer_informa_details_id"], $checkEmailResult);
            if(! $updateResult)
                return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

            $cardNumber = $checkEmailResult["card_id"];
        }

        if ($cardNumber != "") {
            $setCust2Group = $this->informaAppLib->setCustomer2Group($params["customer_id"], $cardNumber, "1/1/1900 12:00:00 AM");
            if(! $setCust2Group)
                return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $result = [
            "check_result" => $checkEmailResult,
            "new_member" => $registerResult,
            "update_result" => $updateResult,
        ];
        
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function postConnectMember() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $customerData = $this->informaAppLib->getCustomerData("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerData) {
            if ($this->informaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $checkEmailResult = $this->informaAppLib->getCheckEmailCustomerInforma($customerData["email"]);
        if(! $checkEmailResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        if ($checkEmailResult["status"] != 1) {
            return $this->sendErrorResponse(400, "Invalid connect member requirement");
        }

        $fullName = $this->informaAppLib->getFullName($customerData["first_name"], isset($customerData["last_name"]) ? $customerData["last_name"] : "");
    
        $result = $this->informaAppLib->postConnectMemberCustomerInforma($params["customer_id"], $informaAuth, $fullName, $customerData["email"], $checkEmailResult["card_id"], $checkEmailResult["cust_id"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $memberData = $this->informaAppLib->getMemberDataInforma($informaAuth["uid"]);
        if (!$memberData) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $setCust2Group = $this->informaAppLib->setCustomer2Group($params["customer_id"], $memberData["P_CARD_ID"], $memberData["P_EXPIRED_DATE"]);
        if(! $setCust2Group)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getInstallationRoute() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "notrans", "jobid"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->informaAppLib->transactionInstallationRouteCustomerInforma($informaAuth, $params["notrans"], $params["jobid"]);
        if(! $result)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function submitMandatoryUpdatePhone() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "phone"];
        $this->informaAppLib->setAppVersion($params);

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validatePhone = $this->informaAppLib->isphoneAvailable($params["phone"], $params['customer_id']);
        if (!$validatePhone) 
            return $this->sendErrorResponse(400, "Gagal, nomor handphone sudah terdaftar");

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $sendChangePhoneProfileResult = $this->informaAppLib->changeProfilePhoneCustomerInforma($customerDetail["informa_uid"], $customerDetail["session_id"], $customerDetail["customer_id"], $params["phone"]);
        if(! $sendChangePhoneProfileResult)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $customerData = $this->informaAppLib->getCustomerData("customer_id = " . $params['customer_id']);
        if (!$customerData) 
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
    
        $customerHelper = new \Helpers\CustomerHelper();
        $customerHelper->linkingCustomerUsingNsq($customerData["email"], $params["phone"], "HCI");

        $response['data'] = $sendChangePhoneProfileResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function postPwpSendOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "point_redeem"];

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $skipCart = isset($params["skip_cart"]) ? $params["skip_cart"] : false;
        if ($skipCart) {
            $params["cart_id"] = "";
        } else {
            $validate = $this->informaAppLib->validateRequired($params, ["cart_id"]);
            if ($validate) 
                return $this->sendErrorResponse(400, $validate);    
        }

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "Anda belum terhubung dengan membership informa");
        }

        $memberData = $this->informaAppLib->getMemberDataInforma($informaAuth["uid"]);
        if (!$memberData) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        if ($memberData["P_HP_STATUS"] != 1) {
            return $this->sendErrorResponse(400, "Harap lakukan verifikasi No HP Anda terlebih dahulu");
        }

        $isPaidMember = substr($memberData["P_CARD_ID"], 0, 2) == "IR";
        if (!$isPaidMember) {
            return $this->sendErrorResponse(400, "Anda belum terhubung dengan informa rewards");
        }

        // DO REQUEST OTP
        $result = $this->informaAppLib->postPwpSendOTPInforma($informaAuth, $params['customer_id'], $memberData["P_NO_HP"], $memberData["P_CARD_ID"], $params['point_redeem'], $params['cart_id'], $skipCart);
        if (!$result) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function postPwpSubmitOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "point_redeem", "otp"];

        $validate = $this->informaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $skipCart = isset($params["skip_cart"]) ? $params["skip_cart"] : false;
        if ($skipCart) {
            $params["cart_id"] = "";
        } else {
            $validate = $this->informaAppLib->validateRequired($params, ["cart_id"]);
            if ($validate) 
                return $this->sendErrorResponse(400, $validate);    
        }

        $customerDetail = $this->informaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $informaAuth = [
            "uid" => $customerDetail["informa_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataInforma= $this->informaAppLib->getCustomerDataInforma($informaAuth);
        if (!$customerDataInforma) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $isMember = $customerDataInforma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "Anda belum terhubung dengan membership informa");
        }

        $memberData = $this->informaAppLib->getMemberDataInforma($informaAuth["uid"]);
        if (!$memberData) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        if ($memberData["P_HP_STATUS"] != 1) {
            return $this->sendErrorResponse(400, "Harap lakukan verifikasi No HP Anda terlebih dahulu");
        }

        $isPaidMember = substr($memberData["P_CARD_ID"], 0, 2) == "IR";
        if (!$isPaidMember) {
            return $this->sendErrorResponse(400, "Anda belum terhubung dengan informa rewards");
        }

        // DO REQUEST OTP
        $result = $this->informaAppLib->postPwpSubmitOTPInforma($informaAuth, $params['customer_id'], $memberData["P_NO_HP"], $memberData["P_CARD_ID"], $memberData["P_CUST_ID"], $params['otp'], $params['point_redeem'], $params['cart_id'], $skipCart);
        if (!$result) {
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());
        }

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

}

