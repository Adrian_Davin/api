<?php

namespace Controllers\SelmaController;

use Helpers\LogHelper;
use Library\SelmaApp;
use Library\Response;
use Controllers\BaseController;

class CustomerSelmaController extends BaseController
{
    protected $selmaAppLib;
    protected $rrResponse;
    public $maintenanceMessage = "Mohon maaf, sistem dalam proses pemeliharaan. Silahkan hubungi CS Selma untuk informasi lebih lanjut.";
    public $maintenanceDate = "2021-10-01";

    public function onConstruct()
    {
        $this->selmaAppLib = new SelmaApp();
        $this->rrResponse = new Response();
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setStatusCode(200, "Ok")->sendHeaders();

        parent::onConstruct();
    }

    private function sendErrorResponse($errorCode, $errorMessages, $messages = ["failed"])
    {
        if ($errorMessages == "Access Deny. Invalid Token") $errorCode = 401;

        $response["data"] = $this->selmaAppLib->getErrorData();
        $response["errors"] = [
            "code" => $errorCode,
            "title" => $this->rrResponse->getResponseDescription($errorCode),
            "messages" => $errorMessages
        ];
        $response["messages"] = $messages;

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }




    public function login()
    {
        $this->selmaAppLib->setAppVersion($this->params);

        $loginResult = $this->selmaAppLib->customerLogin($this->params);
        if (!$loginResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());


        $response['errors'] = [];
        $response['data'] = $loginResult;
        $response['messages'] = ["success"];


        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }


    public function forgotPassword()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];

        $this->selmaAppLib->setAppVersion($params);

        if (!array_key_exists("company_id", $params)) {
            $params["company_id"] = "SLM";
        }

        if ($params["company_id"] != "AHI" && $params["company_id"] != "HCI" && $params['company_id'] != "SLM")
            return $this->sendErrorResponse(400, ["Company Id must be AHI, HCI or SLM"]);

        $hasEmail = array_key_exists("email", $params) && $params["email"] != null;

        if (!$hasEmail)
            return $this->sendErrorResponse(400, ["Diperlukan e-mail"]);

        $email = $hasEmail ? $params["email"] : false;

        $forgetPasskeySelma = $this->selmaAppLib->forgetPasskey($email, $params["company_id"]);
        if (!$forgetPasskeySelma)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $forgetPasskeySelma;
        $response['messages'] = ["sukses"];

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function logout()
    {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404)
                return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $logoutResult = $this->selmaAppLib->logoutCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"]);
        if (!$logoutResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $logoutResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }


    public function register()
    {
        $this->selmaAppLib->setAppVersion($this->params);

        $registerResult = $this->selmaAppLib->registerCustomerSelma(true, $this->params);
        if (!$registerResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $registerResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getById() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }
        
        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $customerData = $this->selmaAppLib->getCustomerData("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerData) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        } else {
            if (isset($customerData["gender"])) {
                $customerDataSelma["sex"] =  $this->selmaAppLib->getSexId($customerData["gender"]);
            }
            $customerDataSelma["birth_date"] =  isset($customerData["birth_date"]) ? $customerData["birth_date"] : "";
        }

        if (isset($customerDetail["ktp"])) {
            $customerDataSelma["ktp"] = $customerDetail["ktp"];
        }

        if (isset($customerDetail["marital_status"])) {
            $customerDataSelma["marital_status"] = $customerDetail["marital_status"];
        }


        $memberData = array();
        $isAllowUpgradeMember = true;
        $isPaidMember = false;
        $memberData["P_CUST_ID"] = "";
        if ($customerDataSelma['is_member'] != 0){
            $memberData = $this->selmaAppLib->getMemberDataSelma($selmaAuth["uid"]);
            if (!$memberData) {
                return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
            }

            $isAllowUpgradeMember = strtoupper(substr($memberData["P_CARD_ID"], 0, 3)) == "TIM";
            $isPaidMember = strtoupper(substr($memberData["P_CARD_ID"], 0, 2)) == "IR";
        }
        
        if ($isAllowUpgradeMember) {
            $isAllowUpgradeMember = $this->selmaAppLib->checkCustomerWithUpdateSelmaMemberSKU($params["customer_id"]);
        }

        $customerDataSelma["member"] = $memberData;
        $responseData = $this->selmaAppLib->mappingCustomerData($customerDataSelma);
        $responseData["customer_id"] = $params["customer_id"];
        $responseData["details"] = $customerDetail;
        $responseData["is_allow_upgrade_member"] = $isAllowUpgradeMember;
        $responseData["is_paid_member"] = $isPaidMember;
        $responseData["is_phone_valid"] = $responseData["phone"] != "";

        // get selmaCustomerData & selmaMemberData
        $response["data"] = $responseData;
        $response["messages"] = ["success"];

        // data user + data member

        $this->rrResponse->setContentType("application/json");

        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
        
    }

    public function update()
    {
        $params = $this->params;

        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "full_name"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if($validate)
            return $this->sendErrorResponse(400, $validate);
       
        if (!isset($params["city"])) {
            $params["city"] = "";
        }

        if (!isset($params["address"])) {
            $params["address"] = "";
        }
            
        $customerData = $this->selmaAppLib->getCustomerData("customer_id = '" . $params["customer_id"] . "'");
        if(! $customerData)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;

        if ($isMember) {
            if (!isset($params["ktp"])) {
                return $this->sendErrorResponse(400, "ktp must be set for member");
            }
    
            if (!isset($params["birth_date"])) {
                return $this->sendErrorResponse(400, "birth_date must be set for member");
            }
      
            if (!isset($params["birth_place"])) {
                return $this->sendErrorResponse(400, "birth_place must be set for member");
            }
       
            if (!isset($params["marital_status"])) {
                return $this->sendErrorResponse(400, "marital_status must be set for member");
            }
    
            if (!isset($params["sex"])) {
                return $this->sendErrorResponse(400, "sex must be set for member");
            }    
    
        }

        $result =  $this->selmaAppLib->updateCustomerData( $customerData, $customerDetail, $params, $isMember);
        if(! $result)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        

        $response["data"] = [
            "success" => true,
        ];
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function changePassword() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "old_password", "new_password", "new_password_confirm"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if($validate)
            return $this->sendErrorResponse(400, $validate);

        if($params["new_password"] != $params["new_password_confirm"]) {
            return $this->sendErrorResponse(400, "Konfirmasi password tidak cocok");
        }

        $customerData = $this->selmaAppLib->getCustomerData("customer_id = " . $params['customer_id']);
        if (!$customerData) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $result = $this->selmaAppLib->changePassKey(
            $params["customer_id"],
            $customerData["email"],
            $params["old_password"],
            $params["new_password"],
            $customerDetail["selma_uid"],
            $customerDetail["session_id"]
        );
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response["data"] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getUserVoucher() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);


        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $storeVoucher = $this->selmaAppLib->getUserVoucherCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"]);
        if(! $storeVoucher)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());   

        $dealsVoucher = $this->selmaAppLib->getCustomerVoucherDeals($params["customer_id"]);
        if ($dealsVoucher) {
            $voucherResult = $this->selmaAppLib->appendDealsVoucher($storeVoucher, $dealsVoucher);
        } else {
            $voucherResult = $storeVoucher;
        }

        $response['errors'] = [];
        $response['data'] = $voucherResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getListNotif() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        if (!array_key_exists("page", $params)) {
            $params["page"] = 1;
        }
    
        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $listNotifResult = $this->selmaAppLib->getListNotifCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["page"]);
        if(! $listNotifResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $listNotifResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getTransactionHist() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);
        
        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);


        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $listTxHistResult = $this->selmaAppLib->getTransactionHistCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"]);
        if(! $listTxHistResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $listTxHistResult;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getTransactionDeliveryList() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "receipt_no"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $result = $this->selmaAppLib->transactionDeliveryListCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["receipt_no"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getTransactionDeliveryDetail() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "notrans", "jobid", "siteid"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $txDetailResult = $this->selmaAppLib->transactionDeliveryDetailCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["notrans"], $params["jobid"], $params["siteid"]);
        if(! $txDetailResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $txDetailResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function postSatisfactionRate() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "transaction_no", "rating_delivery", "rating_installation", "notes"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $requestParams = [
            "notrans" => $params["transaction_no"],
            "ratingDlv" => strval($params["rating_delivery"]),
            "ratingIns" => strval($params["rating_installation"]),
            "notes" => $params["notes"],
        ];

        $satisfactionRateResult = $this->selmaAppLib->satisfactionRateCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $requestParams);
        if(! $satisfactionRateResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $satisfactionRateResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function verifyEmail() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;
        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $emailVerifResult = $this->selmaAppLib->verifyEmailCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"]);
        if(! $emailVerifResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $emailVerifResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function sendChangePhoneOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "new_phone"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $validatePhone = $this->selmaAppLib->isphoneAvailable($params["new_phone"], $params['customer_id']);
        if (!$validatePhone) 
            return $this->sendErrorResponse(400, "Gagal, nomor handphone sudah terdaftar");


        $isMember = $customerDataSelma['is_member'] != 0;

        if ($isMember) {
            $sendChangePhoneOTPResult = $this->selmaAppLib->sendChangePhoneOTPCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["new_phone"]);
            if(! $sendChangePhoneOTPResult)
                return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
            $sendChangePhoneOTPResult = [
                'success' => true,
                'message' => $sendChangePhoneOTPResult,
            ];
    
        } else {
            return $this->sendErrorResponse(400, "Jadilah member selma agar dapat mengubah nomor HP");
        }

        $sendChangePhoneOTPResult["is_member"] = $isMember;

        $response['data'] = $sendChangePhoneOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function submitChangePhoneOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "new_phone", "otp"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validatePhone = $this->selmaAppLib->isphoneAvailable($params["new_phone"], $params['customer_id']);
        if (!$validatePhone) 
            return $this->sendErrorResponse(400, "Nomor handphone yang anda pilih sudah terdaftar, silahkan hubungin customer service kami");

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;
        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $submitChangePhoneOTPResult = $this->selmaAppLib->submitChangePhoneOTPCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["new_phone"], $params["otp"]);
        if(! $submitChangePhoneOTPResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $sendChangePhoneProfileResult = $this->selmaAppLib->changeProfilePhoneCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $customerDetail["customer_id"], $params["new_phone"]);

        if(! $sendChangePhoneProfileResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $submitChangePhoneOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function sendChangeEmailOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "new_email"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validateEmail = $this->selmaAppLib->isEmailAvailable($params["new_email"], $params["customer_id"]);
        if (!$validateEmail) 
            return $this->sendErrorResponse(400, "Gagal, email sudah terdaftar");

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;

        if ($isMember) {
            $sendChangeEmailOTPResult = $this->selmaAppLib->sendChangeEmailOTPCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["new_email"]);
            if(! $sendChangeEmailOTPResult)
                return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

            $sendChangeEmailOTPResult = [
                'success' => true,
                'message' => $sendChangeEmailOTPResult["message"],
                'otp' => $sendChangeEmailOTPResult["otp"],
            ];
    
        } else {
            return $this->sendErrorResponse(400, "Jadilah member selma agar dapat mengubah email");
        }

        $sendChangeEmailOTPResult["is_member"] = $isMember;

        $response['data'] = $sendChangeEmailOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function submitChangeEmailOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "new_email", "otp"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validateEmail = $this->selmaAppLib->isEmailAvailable($params["new_email"], $params["customer_id"]);
        if (!$validateEmail) 
            return $this->sendErrorResponse(400, "Email yang anda pilih sudah terdaftar, silahkan hubungin customer service kami");
    
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $submitChangeEmailOTPResult = $this->selmaAppLib->submitChangeEmailOTPCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["new_email"], $params["otp"]);
        if(! $submitChangeEmailOTPResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $changeEmailProfileResult = $this->selmaAppLib->changeProfileEmailCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $customerDetail["customer_id"], $customerDataSelma["email"], $params["new_email"]);
        if(! $changeEmailProfileResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());    

        $sendChangeEmailOTPResult["is_member"] = $isMember;

        $response['data'] = $submitChangeEmailOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getRedeemVoucherList() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $getRedeemVoucherListResult = $this->selmaAppLib->getRedeemVoucherListCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"]);
        if(! $getRedeemVoucherListResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $getRedeemVoucherListResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function redeemVoucherSendOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        
        $validateDate = $this->selmaAppLib->isCurrentDate($this->maintenanceDate);
        if ($validateDate) {
            return $this->sendErrorResponse(400, $this->maintenanceMessage);
        }

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }
        
        $result = $this->selmaAppLib->redeemVoucherSendOTPCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function redeemVoucherSubmitOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "voucher_code", "otp"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validateDate = $this->selmaAppLib->isCurrentDate($this->maintenanceDate);
        if ($validateDate) {
            return $this->sendErrorResponse(400, $this->maintenanceMessage);
        }
    

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->selmaAppLib->redeemVoucherSubmitOTPCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["voucher_code"], $params["otp"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getTiering() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);
        
        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);


        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $result = $this->selmaAppLib->getTieringCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $result;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getDigitalReceipt() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id", "receipt_no"];
        $this->selmaAppLib->setAppVersion($params);
        
        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);


        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $receiptInfo = $this->selmaAppLib->getDigitalReceiptCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["receipt_no"]);
        if(! $receiptInfo)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['errors'] = [];
        $response['data'] = $receiptInfo;
        $response['messages'] = ["sukses"];
        $this->rrResponse->setContent($response);
        $this->rrResponse->send();
    }

    public function getMemberById() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] =[];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate)
            return $this->sendErrorResponse(400, $validate);

        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }


        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "Tidak bisa diakses oleh non member");
        }
        
        $membershipData = $this->selmaAppLib->getMembershipDataSelma($customerDetail["selma_customer_id"]);
        if (!$membershipData) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        // get selmaCustomerData & selmaMemberData
        $response["data"] = $membershipData;
        $response["messages"] = ["success"];
        
        $this->rrResponse->setContentType("application/json");
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getTransactionInstallationList() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "receipt_no"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->selmaAppLib->transactionInstallationListCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["receipt_no"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getTransactionInstallationDetail() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "notrans", "jobid"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->selmaAppLib->transactionInstallationDetailCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["notrans"], $params["jobid"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function payWithPoint() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "otp"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validateDate = $this->selmaAppLib->isCurrentDate($this->maintenanceDate);
        if ($validateDate) {
            return $this->sendErrorResponse(400, $this->maintenanceMessage);
        }

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }
        
        $result = $this->selmaAppLib->payWithPointCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["otp"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getReceiptList() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->selmaAppLib->getReceiptListCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], 1);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getPointHistory() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "page"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }
        $isMember = $customerDataSelma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "Tidak bisa diakses oleh non member");
        }
        
        $result = $this->selmaAppLib->getPointHistoryCustomerSelma($customerDetail["selma_customer_id"], $params["page"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getInstallationRoute() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "notrans", "jobid"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $result = $this->selmaAppLib->transactionInstallationRouteCustomerSelma($selmaAuth, $params["notrans"], $params["jobid"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function submitMandatoryUpdatePhone() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "phone"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $validatePhone = $this->selmaAppLib->isphoneAvailable($params["phone"], $params['customer_id']);
        if (!$validatePhone) 
            return $this->sendErrorResponse(400, "Gagal, nomor handphone sudah terdaftar");

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $sendChangePhoneProfileResult = $this->selmaAppLib->changeProfilePhoneCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $customerDetail["customer_id"], $params["phone"]);
        if(! $sendChangePhoneProfileResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $customerData = $this->selmaAppLib->getCustomerData("customer_id = " . $params['customer_id']);
        if (!$customerData) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
    
        $customerHelper = new \Helpers\CustomerHelper();
        $customerHelper->linkingCustomerUsingNsq($customerData["email"], $params["phone"], "SLM");

        $response['data'] = $sendChangePhoneProfileResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function getCheckEmail() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerData = $this->selmaAppLib->getCustomerData("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerData) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }
        
        if (!isset($customerData["email"]) || is_null($customerData["email"])) {
            return $this->sendErrorResponse(404, "Email tidak ditemukan");
        }

        // get customer data by customer_id
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;
        if ($isMember) {
            $memberData = $this->selmaAppLib->getMemberDataSelma($selmaAuth["uid"]);
            if (!$memberData) {
                return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
            }

            $isPaidMember = substr($memberData["P_CARD_ID"], 0, 2) == "IR";
            if ($isPaidMember) {
                return $this->sendErrorResponse(400, "User is already a paid member");
            }
        }

        $checkEmailResult = $this->selmaAppLib->getCheckEmailCustomerSelma($customerData["email"]);
        if(! $checkEmailResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $checkEmailResult["status"] = (int)$checkEmailResult["status"];

        $registerResult = [];
        $updateResult = false;
        $cardNumber = "";
        // Daftar member baru
        if ($checkEmailResult["status"] == 0  || $checkEmailResult["status"] == 6 ) {
            $registerResult = $this->selmaAppLib->postRegisterMemberCustomerSelma($customerData);
            if(! $registerResult)
                return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
            
            $cardNumber = $registerResult[0]["Card"];
        } else if ($checkEmailResult["status"] == 4) {
            $updateResult = $this->selmaAppLib->updateTemporaryMemberCustomerSelma($customerDetail["customer_selma_details_id"], $checkEmailResult);
            if(! $updateResult)
                return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

            $cardNumber = $checkEmailResult["card_id"];
        }

        if ($cardNumber != "") {
            $setCust2Group = $this->selmaAppLib->setCustomer2Group($params["customer_id"], $cardNumber, "1/1/1900 12:00:00 AM");
            if(! $setCust2Group)
                return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $result = [
            "check_result" => $checkEmailResult,
            "new_member" => $registerResult,
            "update_result" => $updateResult,
        ];
        
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function postConnectMember() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerDetail) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $customerData = $this->selmaAppLib->getCustomerData("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerData) {
            if ($this->selmaAppLib->getErrorCode() != 404) 
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $checkEmailResult = $this->selmaAppLib->getCheckEmailCustomerSelma($customerData["email"]);
        if(! $checkEmailResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        if ($checkEmailResult["status"] != 5) {
            return $this->sendErrorResponse(400, "Invalid connect member requirement");
        }

        $fullName = $this->selmaAppLib->getFullName($customerData["first_name"], isset($customerData["last_name"]) ? $customerData["last_name"] : "");
    
        $result = $this->selmaAppLib->postConnectMemberCustomerSelma($params["customer_id"], $selmaAuth, $fullName, $customerData["email"], $checkEmailResult["card_id"], $checkEmailResult["cust_id"]);
        if(! $result)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $memberData = $this->selmaAppLib->getMemberDataSelma($selmaAuth["uid"]);
        if (!$memberData) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $setCust2Group = $this->selmaAppLib->setCustomer2Group($params["customer_id"], $memberData["P_CARD_ID"], $memberData["P_EXPIRED_DATE"]);
        if(! $setCust2Group)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
    
        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function verifyPhone() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) {
            return $this->sendErrorResponse(400, $validate);
        }
        
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataSelma = $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;
        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $phoneVerifResult = $this->selmaAppLib->verifyPhoneCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"]);
        if(! $phoneVerifResult)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $response['data'] = $phoneVerifResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function verifyPhoneSubmitOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "otp"];
        $this->selmaAppLib->setAppVersion($params);

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) {
            return $this->sendErrorResponse(400, $validate);
        }

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(!$customerDetail) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];
        
        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;
        if (!$isMember) {
            return $this->sendErrorResponse(400, "User is not a member");
        }

        $verifyPhoneOTPResult = $this->selmaAppLib->verifyPhoneSubmitOTPCustomerSelma($customerDetail["selma_uid"], $customerDetail["session_id"], $params["otp"]);
        if(!$verifyPhoneOTPResult) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $response['data'] = $verifyPhoneOTPResult;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function postPwpSendOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "point_redeem"];

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $skipCart = isset($params["skip_cart"]) ? $params["skip_cart"] : false;
        if ($skipCart) {
            $params["cart_id"] = "";
        } else {
            $validate = $this->selmaAppLib->validateRequired($params, ["cart_id"]);
            if ($validate) 
                return $this->sendErrorResponse(400, $validate);    
        }

        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
        return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "Anda belum terhubung dengan membership selma");
        }

        $memberData = $this->selmaAppLib->getMemberDataSelma($selmaAuth["uid"]);
        if (!$memberData) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        if ($memberData["P_HP_STATUS"] != 1) {
            return $this->sendErrorResponse(400, "Harap lakukan verifikasi No HP Anda terlebih dahulu");
        }

        $isPaidMember = substr($memberData["P_CARD_ID"], 0, 2) == "IR";
        if (!$isPaidMember) {
            return $this->sendErrorResponse(400, "Anda belum terhubung dengan selma rewards");
        }

        // DO REQUEST OTP
        $result = $this->selmaAppLib->postPwpSendOTPSelma($selmaAuth, $params['customer_id'], $memberData["P_NO_HP"], $memberData["P_CARD_ID"], $params['point_redeem'], $params['cart_id'], $skipCart);
        if (!$result) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }

    public function postPwpSubmitOTP() {
        $params = $this->params;
        $response["errors"] = [];
        $response["data"] = [];
        $requiredParamField = ["customer_id", "point_redeem", "otp"];

        $validate = $this->selmaAppLib->validateRequired($params, $requiredParamField);
        if ($validate) 
            return $this->sendErrorResponse(400, $validate);

        $skipCart = isset($params["skip_cart"]) ? $params["skip_cart"] : false;
        if ($skipCart) {
            $params["cart_id"] = "";
        } else {
            $validate = $this->selmaAppLib->validateRequired($params, ["cart_id"]);
            if ($validate) 
                return $this->sendErrorResponse(400, $validate);    
        }
        $customerDetail = $this->selmaAppLib->getCustomerDetail("customer_id = ".$params['customer_id']);
        if(! $customerDetail)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        
        $selmaAuth = [
            "uid" => $customerDetail["selma_uid"],
            "sessionid" => $customerDetail["session_id"],
        ];

        $customerDataSelma= $this->selmaAppLib->getCustomerDataSelma($selmaAuth);
        if (!$customerDataSelma) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $isMember = $customerDataSelma['is_member'] != 0;

        if (!$isMember) {
            return $this->sendErrorResponse(400, "Anda belum terhubung dengan membership selma");
        }

        $memberData = $this->selmaAppLib->getMemberDataSelma($selmaAuth["uid"]);
        if (!$memberData) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        if ($memberData["P_HP_STATUS"] != 1) {
            return $this->sendErrorResponse(400, "Harap lakukan verifikasi No HP Anda terlebih dahulu");
        }

        $isPaidMember = substr($memberData["P_CARD_ID"], 0, 2) == "IR";
        if (!$isPaidMember) {
            return $this->sendErrorResponse(400, "Anda belum terhubung dengan selma rewards");
        }

        // DO REQUEST OTP
        $result = $this->selmaAppLib->postPwpSubmitOTPSelma($selmaAuth, $params['customer_id'], $memberData["P_NO_HP"], $memberData["P_CARD_ID"], $memberData["P_CUST_ID"], $params['otp'], $params['point_redeem'], $params['cart_id'], $skipCart);
        if (!$result) {
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());
        }

        $response['data'] = $result;
        $response['messages'] = ["success"];
        $this->rrResponse->setContent($response);
        return $this->rrResponse->send();
    }
}
