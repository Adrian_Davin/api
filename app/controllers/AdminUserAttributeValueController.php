<?php
namespace Controllers;

/**
 * Class AdminUserAttributeValueController
 * @package Controllers
 */
class AdminUserAttributeValueController extends \Controllers\BaseController
{
    /**
     * @var $adminUser \Models\AdminUser
     */
    protected $adminUser;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->adminUser = new \Models\AdminUser();
    }

    public function saveAdminUserAttributeValue()
    {
        $userAttributeModel = new \Models\AdminUserAttributeValue();
        $userAttributeModel->setFromArray($this->params);
        $userAttributeModel->saveData("user_attribute_value");

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($userAttributeModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $userAttributeModel->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($userAttributeModel->getErrorCode()),
                "messages" => $userAttributeModel->getErrorMessages());
            $responseContent['messages'] = array('save failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function userAttributeValueList()
    {
        /*
         * can use header params: admin_user_id, master_app_id, admin_user_attribute_id
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $adminUserLib = new \Library\AdminUser();
            $allAtt = $adminUserLib->userAttributeValueList($this->params);

            if(!empty($adminUserLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $adminUserLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($adminUserLib->getErrorCode()),
                    "messages" => $adminUserLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $allAtt;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get user attribute list failed, please contact ruparupa tech support" . $e->getMessage()
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}