<?php
namespace Controllers;

use \Firebase\JWT\JWT;
use Models\NegativeKeywordsFileModel;

/**
 * Class NegativeKeywordsController
 * @package Controllers
 * @RoutePrefix("/negativekeywords")
 *
 */
class NegativeKeywordsController extends \Controllers\BaseController
{
    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $negativeKeywordModel;

    protected $keywordLib;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->negativeKeywordModel = new \Models\NegativeKeywordsModel();
        $this->negativeKeywordsFileModel = new \Models\NegativeKeywordsFileModel();
        $this->keywordLib = new \Library\NegativeKeywords();
    }

    public function deleteKeyword()
    {
        $keywordLib = new \Library\NegativeKeywords();
        $deleteStatus = $keywordLib->deleteNegativeKeyword($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$deleteStatus) {
            $responseContent['errors'] = array(
                "code" => $keywordLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($keywordLib->getErrorCode()),
                "messages" => $keywordLib->getErrorMessages());
            $responseContent['messages'] = array('delete failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateNegativeKeyword(){
        $negativeKeywordModel = new \Models\NegativeKeywordsModel();
        $rupaResponse = new \Library\Response();
        $keyword = $this->params['keyword'];
        $duplicate = $this->negativeKeywordModel->findFirst(' keyword = "'.$keyword.'"');
       
        if(!$duplicate){
            $negativeKeywordModel->setFromArray($this->params);
            $negativeKeywordModel->saveData('update_negative_keyword');
            $rupaResponse->setContentType("application/json");
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            if(!empty($negativeKeywordModel->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $negativeKeywordModel->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($negativeKeywordModel->getErrorCode()),
                    "messages" => $negativeKeywordModel->getErrorMessages()
                );
                $responseContent['messages'] = array("Failed to update negativeKeyword");
                $responseContent['data'] = array();
            } 
        }else {
            $responseContent['errors'] = array(
                "code" => "RR102",
                "title" => $rupaResponse->getResponseDescription("RR102"),
                "messages" => "Failed to update keyword because the keyword already exists."
            );
            $responseContent['messages'] = array("Failed to update negativeKeyword");
            $responseContent['data'] = $this->params['keyword'];
        }   
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createNegativeKeyword(){
        $negativeKeywordModel = new \Models\NegativeKeywordsModel();
        $keyword = $this->params['keyword'];
        $duplicate = $this->negativeKeywordModel->findFirst(' keyword = "'.$keyword.'"');
        $rupaResponse = new \Library\Response();
        $responseContent = array();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
       
        if(!$duplicate){
            $negativeKeywordModel->setFromArray($this->params);
            $negativeKeywordModel->saveData('create_negative_keyword');
        }else{
            $responseContent['errors'] = array(
                "code" => "RR102",
                "title" => $rupaResponse->getResponseDescription("RR102"),
                "messages" => "Failed to insert keyword because the keyword already exists."
            );  
            $responseContent['data'] = $keyword;
            $responseContent['messages'] = "Negative keyword already exists.";
        }
        
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function listNegativeKeywords(){
        $keywordLib = new \Library\NegativeKeywords();
        $allKeywords = $this->keywordLib->getAllKeywords($this->params);
        
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($this->keywordLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $keywordLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($keywordLib->getErrorCode()),
                "messages" => $keywordLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Negative keyword not found");
            $responseContent['data'] = array();
        } else {
            $recordsTotal = $allKeywords['recordsTotal'];

            $responseContent['messages'] = array("result" => "success", "recordsTotal" => $recordsTotal);
            $responseContent['data'] = $allKeywords;
        }
        
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function authJWT(){
        $jwt = $this->params['jwt'];
        $key = $_ENV['JWT_SECRET_KEY'];
        $decoded = JWT::decode($jwt, $key, ['HS256']);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['messages'] = array("result" => "success", "recordsTotal" => $recordsTotal);
        $responseContent['data'] = $decoded;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function searchKeyword(){
        $keywordLib = new \Library\NegativeKeywords();
        $matchKeywords = $this->keywordLib->searchKeyword($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($this->keywordLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $keywordLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($keywordLib->getErrorCode()),
                "messages" => $keywordLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Keywords not found");
            $responseContent['data'] = array();
        } else {
            $recordsTotal = $matchKeywords['recordsTotal'];
            $responseContent['messages'] = array("result" => "success", "recordsTotal" => $recordsTotal);
            $responseContent['data'] = $matchKeywords;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }


    public function uploadCsvFile(){
        $negativeKeywordsFileModel = new \Models\NegativeKeywordsFileModel();
        $negativeKeywordsLib = new \Library\NegativeKeywords();
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json"); 
        $responseContent = array();
        
        if(isset($this->params['file'])){
            $b64 = $this->params['file'];
            $pos  = strpos($b64, ';');
            $count = 0;
            if($pos !== false){
                $b64 = base64_decode(preg_replace('#^data:text/\w+;base64,#i', '', $b64));
            }
            $lines = explode("\n", $b64);
            $array = array_map('str_getcsv', $lines);
    
            $save = array(
                "file_name"=> $this->params['file_name'],
                "email"=>$this->params['email']
            );
            
            $negativeKeywordsFileModel->setFromArray($save);
            $negativeKeywordsFileModel->saveData('create_negative_keywords_file');

            $duplicate = array();
            $saved = array();
            $keywordCount = 0;
            $savedCount = 0;
            $duplicateCount = 0;
            
            foreach(array_slice($array,1) as $a){
                if($a[0] != "\n" && $a[0] != '' && $a[0] != ' '){
                    $flag = $negativeKeywordsLib->checkKeywords($a[0]);
                    if($flag){
                        $negativeKeywordsModel = new \Models\NegativeKeywordsModel();
                        $negativeKeywordsModel->setKeyword($a[0]);
                        $negativeKeywordsModel->saveData();
                        $saved[$savedCount] = $a[0];
                        $savedCount++;
                    }else{
                        $duplicate[$duplicateCount] = $a[0];
                        $duplicateCount++;
                    }
                    $keywordCount++;
                }
            }

            if($keywordCount == 0){
                $responseContent['errors'] = array(
                    "code" => "501",
                    "title" => "Forbidden",
                    "messages" => "Failed to save file."
                );  
                $responseContent['data'] = array();
                $responseContent['messages'] = "No negative keyword found.";
            }else{
                if($duplicateCount == $keywordCount){
                    $responseContent['errors'] = array(
                        "code" => "501",
                        "title" => "Forbidden",
                        "messages" => "Failed to save file."
                    );  
                    $responseContent['data'] = array();
                    $responseContent['messages'] = "All negative keyword are already exists on database.";
                }
                else if($duplicateCount == 0){
                    $rupaResponse->setHeaderStatus(200);
                    $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
                    $responseContent['messages'] = $savedCount . " keyword(s) has been saved to the database.";
                    $responseContent['data'] = $saved;
                }
                else{
                    $rupaResponse->setHeaderStatus(200);
                    $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
                    $responseContent['messages'] = $savedCount . " keyword(s) has been saved and found " . $duplicateCount . " keyword(s) duplicated that not saved to the database.";
                    $responseContent['data'] = array(
                        'duplicated_keywords' => $duplicate,
                        'saved_keywords' => $saved
                    );
                }
            }

        }else{
            $responseContent['errors'] = array(
                "code" => $negativeKeywordsLib->getErrorCode(),
                "title" => "Forbidden",
                "messages" => "Failed to save file."
            );  
            $responseContent['messages'] = "No files has been set.";
        }
        
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}