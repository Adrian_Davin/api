<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 13/1/2017
 * Time: 2:23 PM
 */

namespace Controllers;

/**
 * Class ControllerNameController
 * @package Controllers
 */
class CustomerAddressController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getAddressData()
    {
        $customerLib = new \Library\Customer();
        $allAddress = $customerLib->getCustomerAddress($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($allAddress)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Address not found");
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allAddress;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getAddressDetail()
    {
        $customerLib = new \Library\Customer();
        $allAddress = $customerLib->getCustomerAddress($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($allAddress)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Customer not found");
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            if(!empty($this->params['customer_id'])) {
                $response['data'] = $allAddress;
            }else{
                $response['data'] = $allAddress[0];
            }
            $response['messages'] = array("success");
        }

        // clean up new line character
        if (isset($response['data'])) {
        foreach ($response['data'] as $index => $address) {
            if (is_array($address)) {
                foreach ($address as $attr => $value) {
                    if (!is_array($value)) {
                        $response['data'][$index][$attr] = str_replace("\n", " ", $value);
                    }
                }
            } else {
                $response['data'][$index] = str_replace("\n", " ", $address);
            }
        }
        }
        
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function saveAddress()
    {
        $customerLib = new \Library\Customer();
        $result = $customerLib->saveData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($customerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $customerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages());
            $responseContent['messages'] = array('save failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteAddress()
    {

        $customerLib = new \Library\Customer();
        $deleteStatus = $customerLib->deleteCustomerAddress($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$deleteStatus) {
            $responseContent['errors'] = array(
                "code" => $customerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages());
            $responseContent['messages'] = array('delete failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}