<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class BrandController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $brandModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->brandModel = new \Models\MasterBrand();
    }

    public function deleteBrand()
    {
        $brandLib = new \Library\Brand();
        $deleteStatus = $brandLib->deleteBrand($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$deleteStatus) {
            $responseContent['errors'] = array(
                "code" => $brandLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($brandLib->getErrorCode()),
                "messages" => $brandLib->getErrorMessages());
            $responseContent['messages'] = array('delete failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateBrand(){
        $brandModel = new \Models\MasterBrand();

        $brandModel->setFromArray($this->params);
        $brandModel->saveData('update_brand');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($brandModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $brandModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($brandModel->getErrorCode()),
                "messages" => $brandModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to update brand");
            $responseContent['data'] = array();
        } 
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createBrand(){
        $brandModel = new \Models\MasterBrand();
        $brandModel->setFromArray($this->params);
        $brandModel->saveData('create_brand');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($brandModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $brandModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($brandModel->getErrorCode()),
                "messages" => $brandModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create brand");
            $responseContent['data'] = array();
            
        } 

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function listBrand()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $brand = $this->brandModel->find(' status >= 0 ');

        $responseContent['data'] = $brand->toArray();
        $responseContent['errors'] = $this->brandModel->getErrors();
        $responseContent['messages'] = $this->brandModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function detailBrand(){

        $brand_id = $this->params['brand_id'];
       

        $brand = $this->brandModel->findFirst([
            'status > :status: and brand_id = :brand_id:',
            'bind' => [
                'status' => -1,
                'brand_id' => $brand_id
            ]
        ]);
        if($brand){
            $this->rupaResponse->setContentType("application/json");
            $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['data'] = $brand->toArray();
            $responseContent['errors'] = [];
            $responseContent['messages'] = "";
    
        }else{
            $this->rupaResponse->setContentType("application/json");
            $this->rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $responseContent['data'] = [];
            $responseContent['errors'] = $this->supplier->getErrors();
            $responseContent['messages'] = $this->supplier->getMessages();
    
        }
        
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function getUrlKey(){

        $brandLib = new \Library\Brand();
        $brandUrlKey = $brandLib->getUrlKey($this->params);
        if(!empty($brandUrlKey)){
            $this->rupaResponse->setContentType("application/json");
            $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['data'] = $brandUrlKey;
            $responseContent['errors'] = [];
            $responseContent['messages'] = "success";
    
        }else{
            $this->rupaResponse->setContentType("application/json");
            $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['data'] = [];
            $responseContent['errors'] = array(
                "code" => 200,
                "title" => $this->rupaResponse->getResponseDescription(200),
                "messages" => "Url Key not found"
            );
            $responseContent['messages'] = "Failed to get url key";
    
        }
        
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function getBrandId(){

        $categoryMappingModel = new \Models\ProductCategoryMappingBrand();
        $categoryId = $categoryMappingModel->findFirst(
            array(
                'conditions' => 'category_id = "'.$this->params['category_id'].'" ',
                'columns' => 'brand_id'
            )
        );
        
        if($categoryId){
            $this->rupaResponse->setContentType("application/json");
            $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['data'] = $categoryId;
            $responseContent['errors'] = [];
            $responseContent['messages'] = "success";
    
        }else{
            $this->rupaResponse->setContentType("application/json");
            $this->rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $responseContent['data'] = [];
            $responseContent['errors'] = array(
                "code" => 404,
                "title" => $this->rupaResponse->getResponseDescription(404),
                "messages" => "Brand Id not found"
            );
            $responseContent['messages'] = "Failed to get brand Id";
    
        }
        
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

}