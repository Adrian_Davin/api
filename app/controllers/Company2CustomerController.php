<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 5:50 PM
 */
namespace Controllers;

class Company2CustomerController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getCustomerData()
    {
        $companyLib = new \Library\Company();
        $allAddress = $companyLib->getCompanyCustomer($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($allAddress)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Address not found");
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allAddress;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function saveCustomer()
    {
        $companyLib = new \Library\Company();
        $companyLib->saveCustomerData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($companyLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $companyLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($companyLib->getErrorCode()),
                "messages" => $companyLib->getErrorMessages());
            $responseContent['messages'] = array('save failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteCustomer()
    {
        $companyLib = new \Library\Company();
        $deleteStatus = $companyLib->deleteCompanyCustomer($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$deleteStatus) {
            $responseContent['errors'] = array(
                "code" => $companyLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($companyLib->getErrorCode()),
                "messages" => $companyLib->getErrorMessages());
            $responseContent['messages'] = array('delete failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}