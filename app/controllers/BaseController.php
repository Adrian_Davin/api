<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/13/2016
 * Time: 2:07 PM
 */

namespace Controllers;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

class BaseController extends \Phalcon\Mvc\Controller
{
    protected $params;

    protected $response;

    public function onConstruct()
    {

        $this->params = \Helpers\ParserHelper::parseRequest($this->request);
       // $this->response = new \Phalcon\Http\Response();

        return;
    }

}