<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class GideonAdminRoleController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $gideonAdminRoleModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->gideonAdminRoleModel = new \Models\GideonAdminRole();
    }

    public function getGideonRoleList()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $gideonAdminRole = $this->gideonAdminRoleModel->find(' status >= 0 ');

        $responseContent['data'] = $gideonAdminRole->toArray();
        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function getGideonRoleDetail()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $params = $this->params;

        $response = array(
            'data' => '',
            'errors' => array(),
            'messages' => array()
        );

        try{
            $gideonAdminRole = $this->gideonAdminRoleModel->findFirst(' status >= 0 and role_id = '.$params['role_id']);
            if ($gideonAdminRole) $response['data'] = $gideonAdminRole->toArray(array(),true);
        }catch (\Exception $e){
            // do nothing
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function saveGideonAdminRole()
    {
        /**
         * ToDo : set from array ke gideon_admin_role
         */
        $this->gideonAdminRoleModel->setFromArray($this->params);

        /**
         * ToDo : Save SalesRule
         */
        $errors = $this->gideonAdminRoleModel->saveRole();
        $this->rupaResponse->setContentType("application/json");
        if(!$errors) {
            $response['errors'] = array("code" => "RR301",
                "title" => $this->rupaResponse->getResponseDescription("RR301"),
                "messages" => $errors);
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success save your data");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

}