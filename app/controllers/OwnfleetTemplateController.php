<?php


namespace Controllers;


use Library\Response;
use Models\SupplierOwnfleetRate;
use Models\SupplierOwnfleetTemplate;

class OwnfleetTemplateController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function createTemplate(){
        $ownfleetModel = new SupplierOwnfleetTemplate();
        $templateData = array(
            'supplier_id' => $this->params['supplier_id'],
            'name' => $this->params['name'],
            'status' => 10
        );

        unset($this->params['supplier_id']);
        unset($this->params['name']);

        $ownfleetModel->setFromArray($templateData);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($ownfleetModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $ownfleetModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($ownfleetModel->getErrorCode()),
                "messages" => $ownfleetModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create ownfleet template");
            $responseContent['data'] = array();
        } else {
            $ownfleetModel->saveData('create_ownfleet_template');

            $ownfleetRateModel = new SupplierOwnfleetRate();

            //change '#' into ownfleet_template_id
            $this->params['string_value'] = str_replace('#', $ownfleetModel->getOwnfleetTemplateId(), $this->params['string_value']);

            $ownfleetRateModel->setFromArray($this->params);
            $result = $ownfleetRateModel->insertTemplateToRate();

            if($result == "error"){
                $responseContent['messages'] = array("Failed inserting template data to ownfleet_rate");
                $responseContent['data'] = "";
            }
            else {
                $responseContent['messages'] = array("Success inserting template data to ownfleet_rate");
                $responseContent['data'] = "";
            }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function uploadSku(){
        $ownfleetModel = new SupplierOwnfleetTemplate();
        $result = $ownfleetModel->uploadTemplateToProduct($this->params);

        $rupaResponse = new Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$result){
            $responseContent['errors'] = array(
                "code" => $ownfleetModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($ownfleetModel->getErrorCode()),
                "messages" => $ownfleetModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = "";
        }
        else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $result;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function templateList(){
        $ownfleetLib = new \Library\OwnfleetTemplate();
        $templateList = $ownfleetLib->getAllTemplate($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($ownfleetLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $ownfleetLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($ownfleetLib->getErrorCode()),
                "messages" => $ownfleetLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create page");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success creating page");
            $responseContent['data'] = $templateList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();


    }

    public function getTemplateDetail(){
        $ownfleetLib = new \Library\OwnfleetTemplate();

        $templateData = $ownfleetLib->getTemplateDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($ownfleetLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $ownfleetLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($ownfleetLib->getErrorCode()),
                "messages" => $ownfleetLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get static page details");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $templateData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateTemplate(){
        $ownfleetModel = new \Models\SupplierOwnfleetTemplate();
        $id = $this->params['ownfleet_template_id'];

        $ownfleetModel->setFromArray($this->params);
        $ownfleetModel->saveData('update_template');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        $ownfleetRateModel = new SupplierOwnfleetRate();

        if(!empty($this->params['name'])){ // for status
            if(!empty($this->params['string_value'])){ // if updating the file csv
                // clear previous shipping rate
                $is_affected = $ownfleetRateModel->clearShippingRate($id);

                if($is_affected == "success"){
                    unset($this->params['supplier_id']);
                    unset($this->params['ownfleet_template_id']);
                    unset($this->params['name']);

                    //change '#' into ownfleet_template_id
                    $this->params['string_value'] = str_replace('#', $id, $this->params['string_value']);
                    $ownfleetRateModel->setFromArray($this->params);

                    $result = $ownfleetRateModel->insertTemplateToRate();

                    if($result == "error"){
                        $responseContent['messages'] = array("Failed updating template data to ownfleet_rate");
                        $responseContent['data'] = "";
                    }
                    else {
                        $responseContent['messages'] = array("Success updating template data to ownfleet_rate");
                        $responseContent['data'] = "";
                    }
                }
            }
            else {
                $responseContent['messages'] = array("Success updating template data to ownfleet_rate");
                $responseContent['data'] = "";
            }
        }
        else{
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = "";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

}