<?php


namespace Controllers;

use Library\Response;
use Models\ProductVariant;

class PricePirController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getChunkedDataDetails(){
        $pricePirModel = new \Models\ProductPrice();
        $result = $pricePirModel->getChunckedData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($pricePirModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $pricePirModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($pricePirModel->getErrorCode()),
                "messages" => $pricePirModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $result;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function insertPricePir(){
        $pricePirModel = new \Models\ProductPrice();
        $result = $pricePirModel->insertPir($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($pricePirModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $pricePirModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($pricePirModel->getErrorCode()),
                "messages" => $pricePirModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $result;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function comparePir(){
        $pricePirModel = new \Models\ProductPrice();
        $pricePirModel->cleanExpiredProductStockOverrideStatus();
        $result = $pricePirModel->comparePir();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($pricePirModel->getErrorCode())){
            $responseContent['errors'] = array(
                "code" => $pricePirModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($pricePirModel->getErrorCode()),
                "messages" => $pricePirModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        }
        else{
            $responseContent['errors'] = "";
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateIsInStockAfterCompare(){
        $productVariant = new ProductVariant();
        $result = $productVariant->updateIsInStockComparePir();

        $rupaResponse = new Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['errors'] = array();
        $responseContent['messages'] = array("success");
        $responseContent['data'] = $result;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function getCountProductPrice()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productPriceModel = new \Models\ProductPrice();
        $responseModel = $productPriceModel->getCountProductPrice();

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getProductPrice()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productPriceModel = new \Models\ProductPrice();
        $responseModel = $productPriceModel->getProductPrice($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}