<?php
namespace Controllers;

/**
 * Class PickupPointController
 * @package Controllers
 */
class PickupPointController extends \Controllers\BaseController
{
    /**
     * @var $pickupPoint \Models\PickupPoint
     */
    protected $pickupPoint;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->pickupPoint = new \Models\PickupPoint();
    }

    public function getPickupPointList()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $pickupPointLib = new \Library\PickupPoint();
            $allPickupPoint = $pickupPointLib->getAllPickupPoint($this->params);

            if(!empty($pickupPointLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $pickupPointLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($pickupPointLib->getErrorCode()),
                    "messages" => $pickupPointLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $allPickupPoint;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get pickup point failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getPickupPointData()
    {
        /*
         * params: pickup_id
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $pickupPointLib = new \Library\PickupPoint();
            $pickup_id = !empty($this->params['pickup_id']) ? $this->params['pickup_id'] : "";
            $pickupPointData = $pickupPointLib->getPickupPointData($pickup_id);

            if(!empty($pickupPointLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $pickupPointLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($pickupPointLib->getErrorCode()),
                    "messages" => $pickupPointLib->getErrorMessages()
                );
                $responseContent['data'] = array();
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $pickupPointData;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get sales log failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}