<?php


namespace Controllers;

/**
 * Class SupplierUserController
 * @package Controllers
 * @RoutePrefix("/supplier/user")
 *
 */
class SupplierUserController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $supplier;

    protected $supplierUser;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->supplier = new \Models\Supplier();
        $this->supplierUser = new \Models\SupplierUser();
    }

    public function login()
    {
        $supplierLib = new \Library\Supplier();
        $supplierLib->setSupplierFromArray($this->params);
        $loginResult = $supplierLib->supplierLogin();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(empty($loginResult)) {
            $response['errors'] = array(
                "code" => $supplierLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($supplierLib->getErrorCode()),
                "messages" => $supplierLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = $loginResult;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function saveUser(){

        $supplierUserLib = new \Library\SupplierUser();
        $supplierUserLib->setSupplierUserFromArray($this->params);
        $save = $supplierUserLib->saveData();
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($supplierUserLib->getErrorCode())) {
            $errorCode = $supplierUserLib->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR400";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $this->rupaResponse->getResponseDescription($errorCode),
                "messages" => $supplierUserLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to save order");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = array("supplier_user_id" => $supplierUserLib->getSupplierUserId());
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function listUser(){

        $this->supplierUser->setFromArray($this->params);
        $supplierUser = $this->supplierUser->getSupplierUserList();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $supplierUser->toArray();
        $responseContent['errors'] = $this->supplierUser->getErrors();
        $responseContent['messages'] = $this->supplierUser->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function detailUser(){

        $supplierParam = array(
            "conditions" => "supplier_user_id = ".$this->params['supplier_user_id']
        );

        $supplier = $this->supplierUser->findFirst($supplierParam);

        $detail = $supplier->toArray();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $detail;
        $responseContent['errors'] = $this->supplier->getErrors();
        $responseContent['messages'] = $this->supplier->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

}