<?php


namespace Controllers;

/**
 * Class SalesruleCustomerGroupController
 * @package Controllers
 * @RoutePrefix("/salesrule/customer_group")
 *
 */
class SalesruleCustomerGroupController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    /**
     * @var \Models\SalesruleCustomerGroup
     */
    protected $salesruleCustomerGroupModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->salesruleCustomerGroupModel = new \Models\SalesruleCustomerGroup();
    }

    public function listSalesruleCustomerGroup()
    {
        $query = $this->salesruleCustomerGroupModel->query();

        if (isset($this->params['rule_id'])) {
            $query->where("rule_id = {$this->params['rule_id']}");
        }

        $salesruleCustomerGroup = $query->execute();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $salesruleCustomerGroup->toArray();
        $responseContent['errors'] = $this->salesruleCustomerGroupModel->getErrors();
        $responseContent['messages'] = $this->salesruleCustomerGroupModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function listSalesruleCustomerGroupConcatenated()
    {
        $query = $this->salesruleCustomerGroupModel->query();

        $query->columns(['rule_id', 'GROUP_CONCAT(customer_group_id) AS customer_group_id']);
        $query->groupBy('rule_id');

        $salesruleCustomerGroup = $query->execute();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $salesruleCustomerGroup->toArray();
        $responseContent['errors'] = $this->salesruleCustomerGroupModel->getErrors();
        $responseContent['messages'] = $this->salesruleCustomerGroupModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }
}