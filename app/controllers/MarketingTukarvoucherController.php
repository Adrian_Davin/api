<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class MarketingTukarvoucherController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;
    
    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
    }

    public function saveTukarvoucher() 
    { 
        $tukarvoucherLib = new \Library\Tukarvoucher();
        $tukarvoucherLib->setTukarvoucherFromArray($this->params);
        
        $tukarvoucherLib->saveData('marketing_tukarvoucher'); 
        $this->rupaResponse->setContentType("application/json"); 
        if(!empty($tukarvoucherLib->getErrorCode())) { 
            $response['errors'] = array("code" => $tukarvoucherLib->getErrorCode(), 
                "title" => $this->rupaResponse->getResponseDescription($tukarvoucherLib->getErrorCode()), 
                "messages" => $tukarvoucherLib->getErrorMessages()); 
            $response['messages'] = array($tukarvoucherLib->getErrorMessages()); 
        } else { 
            $response['errors'] = array(); 
            $response['data'] = $tukarvoucherLib->getData(); 
            $response['messages'] = array("success save your data"); 
        } 
        
        $this->rupaResponse->setContent($response); 
        $this->rupaResponse->send(); 
    } 



}