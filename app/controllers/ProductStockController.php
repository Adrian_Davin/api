<?php

namespace Controllers;


class ProductStockController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    private function handleResponse($masterAttributes, $attributeList = array()){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($masterAttributes->getErrorMessages())) {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();

            $responseContent['errors'] = array(
                "code" => $masterAttributes->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($masterAttributes->getErrorCode()),
                "messages" => $masterAttributes->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $statusCode = 200;
            if(empty($attributeList)) {
                $statusCode = 404;
            }

            $rupaResponse->setStatusCode($statusCode, "Ok")->sendHeaders();
            $responseContent['errors'] = array();
            $responseContent['data'] = $attributeList;
            $responseContent['messages'] = "success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getProductStock()
    {
        $productStock = new \Library\ProductStock();
        $stockList = $productStock->getListProductStock($this->params);

        $this->handleResponse($productStock, $stockList);
    }

}