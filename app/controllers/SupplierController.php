<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class SupplierController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $departmentBuModel;

    protected $supplierUser;

    /**
     * @var $supplier \Models\Supplier
     */
    protected $supplier;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->departmentBuModel = new \Models\DepartmentBu();
        $this->supplier = new \Models\Supplier();
        $this->supplierUser = new \Models\SupplierUser();
        $this->supplierReport = new \Models\SupplierReport();
    }

    public function saveSupplier()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $this->supplier->setFromArray($this->params);
        $this->supplier->saveSupplierProfile();

        $responseContent = array();

        if(!empty($this->supplier->getErrorCode())) {
            $errorCode = $this->supplier->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR400";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $this->supplier->getResponseDescription($errorCode),
                "messages" => $this->supplier->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to save order");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = array("supplier_id" => $this->supplier->getSupplierId());
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();

    }

    public function listDepartment()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $brand = $this->departmentBuModel->find(' status >= 0 ');

        $responseContent['data'] = $brand->toArray();
        $responseContent['errors'] = $this->departmentBuModel->getErrors();
        $responseContent['messages'] = $this->departmentBuModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function detailDepartment()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $id_department_bu = $this->params['id_department_bu'];

        $department = $this->departmentBuModel->findFirst(' id_department_bu = '.$id_department_bu);

        $responseContent['data'] = $department->toArray();
        $responseContent['errors'] = $this->departmentBuModel->getErrors();
        $responseContent['messages'] = $this->departmentBuModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function login()
    {
        $supplierLib = new \Library\Supplier();
        $supplierLib->setSupplierFromArray($this->params);
        $loginResult = $supplierLib->supplierLogin($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(empty($loginResult)) {
            $response['errors'] = array(
                "code" => $supplierLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($supplierLib->getErrorCode()),
                "messages" => $supplierLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = $loginResult;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function listSupplier(){
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;
        if(!isset($params['supplier_type'])){
            $params['supplier_type'] = "";
        }

        if($params['supplier_type'] == 'marketplace'){
            $supplier = $this->supplier->find(
                array(
                    'conditions' => ' status > -1 and supplier_alias like "MP%"',
                    'order' => 'name'
                )
            );
        }elseif($params['supplier_type'] == 'inter company'){
            $supplier = $this->supplier->find(
                array(
                    'conditions' => ' status > -1 and supplier_alias not like "MP%"',
                    'order' => 'name'
                )
            );
        }else{
            $supplier = $this->supplier->find(
                array(
                    'order' => 'name'
                )
            );
        }

        $responseContent['data'] = $supplier->toArray();
        $responseContent['errors'] = $this->supplier->getErrors();
        $responseContent['messages'] = $this->supplier->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function detailSupplier(){

        $this->supplier->setFromArray($this->params);
        $supplier = $this->supplier->getSupplierDetail();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $supplier;
        $responseContent['errors'] = $this->supplier->getErrors();
        $responseContent['messages'] = $this->supplier->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function settlementReport()
    {
        $this->params['report_type'] = "settlement";
        $this->supplierReport->setFromArray($this->params);
        $report = $this->supplierReport->getReport();
        $detail = $report->toArray();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $detail;
        $responseContent['errors'] = $this->supplier->getErrors();
        $responseContent['messages'] = $this->supplier->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function summaryReport()
    {
        $this->params['report_type'] = "summary";
        $this->supplierReport->setFromArray($this->params);
        $report = $this->supplierReport->getReport();
        $detail = $report->toArray();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $detail;
        $responseContent['errors'] = $this->supplier->getErrors();
        $responseContent['messages'] = $this->supplier->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function logout()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $result = $this->supplierUser->logout($this->params);
            if (isset($result['error'])) {
                $responseContent['errors'] = array(
                    "code" => "R100" ,
                    "title" => "Error Message",
                    "message" => $result['error']
                );
            } else {
                $responseContent['messages'] = array("success");
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Process logout failed"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getSupplierUserData()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $supplierUserModel = new \Models\SupplierUser();
            $supplierUserModel->setFromArray($this->params);
            $supplierUserData = $supplierUserModel->getSupplierUserDetail();

            if(!empty($supplierUserModel->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $supplierUserModel->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($supplierUserModel->getErrorCode()),
                    "messages" => $supplierUserModel->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $supplierUserData;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get supplier user failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getSupplierRoleList()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $supplierRoleLib = new \Library\Supplier();
        $supplierRole = $supplierRoleLib->getSupplierRoleList();

        if(!empty($supplierRoleLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $supplierRoleLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($supplierRoleLib->getErrorCode()),
                "messages" => $supplierRoleLib->getErrorMessages()
            );
            $responseContent['data'] = array();
            $responseContent['messages'] = "failed";
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $supplierRole;
            $responseContent['messages'] = "success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getSupplierRole()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $supplierRoleLib = new \Library\Supplier();
        $supplierRole = $supplierRoleLib->getSupplierRole($this->params);

        if(!empty($supplierRoleLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $supplierRoleLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($supplierRoleLib->getErrorCode()),
                "messages" => $supplierRoleLib->getErrorMessages()
            );
            $responseContent['data'] = array();
            $responseContent['messages'] = "failed";
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $supplierRole;
            $responseContent['messages'] = "success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveSupplierRole()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $supplierRoleLib = new \Library\Supplier();
        $data = $supplierRoleLib->saveSupplierRole($this->params);
        if(!empty($supplierRoleLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $supplierRoleLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($supplierRoleLib->getErrorCode()),
                "messages" => $supplierRoleLib->getErrorMessages()
            );
            $responseContent['data'] = array();
            $responseContent['messages'] = "failed";
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $data;
            $responseContent['messages'] = "success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}