<?php

namespace Controllers;


/**
 * Class ShippingController
 * @package Controllers
 * @RoutePrefix("/shipping")
 *
 */
class ShippingController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }


    public function generateAwb()
    {
        /**
         * params: order_no
         */
        $shippingLib = new \Library\Shipping();
        $shippingLib->setFromArray($this->params);
        $awb = $shippingLib->generateAwb();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($shippingLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $shippingLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($shippingLib->getErrorCode()),
                "messages" => $shippingLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data']['awb'] = $awb;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCarrierList()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $shippingLib = new \Library\Shipping();
            $allCarrier = $shippingLib->getCarrierList($this->params);

            if(!empty($shippingLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $shippingLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($shippingLib->getErrorCode()),
                    "messages" => $shippingLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $allCarrier;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get carrier failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createCourierCredentialCache()
    {
        $courierCredentialModel = new \Models\ShippingCourierCredential();
        $courierCredentialModel->saveToCache();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = [];
        $responseContent['messages'] = [];
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteCourierCredentialCache()
    {
        $courierCredentialModel = new \Models\ShippingCourierCredential();
        $courierCredentialModel->deleteFromCache($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = [];
        $responseContent['messages'] = [];
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getAllCarrier()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $shippingLib = new \Library\Shipping();
            $allCarrier = $shippingLib->getAllCarrier();

            if(!empty($shippingLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $shippingLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($shippingLib->getErrorCode()),
                    "messages" => $shippingLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $allCarrier;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get carrier failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}