<?php


namespace Controllers;


class SalesCommentController extends \Controllers\BaseController
{
    public function saveComment()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $commentModel = new \Models\SalesComment();
        if (!empty($this->params['comment']) && strlen($this->params['comment']) > 1) {
            $commentModel->setFromArray($this->params);
            $commentModel->saveData();

            if(!empty($commentModel->getErrors())) {
                $responseContent['errors'] = array("code" => "RR302", "title" => $rupaResponse->getResponseDescription("RR302"), "messages" => $commentModel->getErrors());
                $responseContent['messages'] = array("failed");
            } else {
                $responseContent['errors'] = array();
                $responseContent['data'] = array();
                $responseContent['messages'] = array("success");
            }
        } else {
            $responseContent['errors'] = array("code" => "RR302", "title" => $rupaResponse->getResponseDescription("RR002"), "messages" => "Comment cant empty");
            $responseContent['messages'] = array("failed");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCommentList()
    {
        $commentModel = new \Models\SalesComment();
        $commentModel->setFromArray($this->params);
        $result = $commentModel->getCommentList();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(empty($result)) {
            $responseContent['errors'] = array("code" => "RR302", "title" => $rupaResponse->getResponseDescription("RR302"), "messages" => $commentModel->getErrors());
            $responseContent['messages'] = array("");
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCommentChat()
    {
        /**
         * params: shipment_status (ex: pending), delivery_method (ex: 'delivery','pickup')
         */
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $commentModel = new \Models\SalesComment();
        $result = $commentModel->getCommentChat($this->params);

        if (isset($result['data'])) {
            $i = 0;
            foreach ($result['data'] as $data) {
                foreach ($data as $keyData => $valueData) {
                    $resultInvoiceItem = \Models\SalesInvoiceItem::find("invoice_id = " . $data['invoice_id']);
                    $sku = '';
                    foreach ($resultInvoiceItem->toArray() as $dataInvoiceItem) {
                        foreach ($dataInvoiceItem as $keyDataInvoiceItem => $valueDataInvoiceItem) {
                            if ($keyDataInvoiceItem == 'invoice_item_id') {
                                $sku .= $dataInvoiceItem['sku'] . ',';
                                break;
                            }
                        }
                    }

                    $result['data'][$i]['sku'] = rtrim($sku,',');
                }

                $i++;
            }

            $response['data'] = $result['data'];
        }

        if (isset($result['messages'])) $response['messages'] = $result['messages'];
        if (isset($result['error'])) $response['error'] = $result['error'];
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

}