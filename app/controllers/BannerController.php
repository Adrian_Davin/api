<?php

namespace Controllers;


class BannerController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getBannerList()
    {
        $bannerModel = new \Models\Banner();
        $bannerList = $bannerModel->getBannerList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($bannerModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $bannerModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($bannerModel->getErrorCode()),
                "messages" => $bannerModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $bannerList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}