<?php

namespace Controllers;

use \Library\Customer as CustomerLib;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/customer")
 *
 */
class CustomerController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function registerCustomer()
    {
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($this->params);
        $customerLib->registerCustomer(true, $this->params);
       
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (!empty($customerLib->getErrorCode())) {
           
            $response['data'] = array();
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array();
        } else {
            // Automatic login and get voucher list for this customer
            // $customerLib = new \Library\Customer();
          
            $loginResult = $customerLib->customerLogin($this->params);
            if (!empty($customerLib->getErrorCode())) {
                $response['data'] = array();
                $response['errors'] = array(
                    "code" => $customerLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                    "messages" => $customerLib->getErrorMessages()
                );
                $response['messages'] = array("failed");
            } else {
                $loginResult['voucher_list'] = $customerLib->getVoucherList();
                $loginResult['is_duplicate_device'] = $customerLib->getIsDuplicateDevice();
                $loginResult['is_referral_not_found'] = $customerLib->getIsReferralNotFound();
                $response['errors'] = array();
                $response['data'] = $loginResult;
                $response['messages'] = array("success");
            }
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function login()
    {
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($this->params);
        $loginResult = $customerLib->customerLogin($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (empty($loginResult)) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = $loginResult;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function logout()
    {
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($this->params);
        $customerLib->customerLogout($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (!empty($customerLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function saveCustomer()
    {
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($this->params);

        if (isset($this->params['status'])) {
            if ($this->params['status'] == '5') {
                $customerLib->saveGuestCustomer($this->params);
            } else {
                $customerLib->registerCustomer(false, $this->params);
            }
        } else {
            $customerLib->registerCustomer(false, $this->params);
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!empty($customerLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array('save failed');
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function resetPassword() {
        $response = [];

        if (empty($this->params)) {
            $response['errors'] = array(
                "code" => 400,
                "title" => "Bad Request",
                "messages" => "Payload tidak boleh kosong"
            );
            $response['messages'] = array('failed');
        } else {
            $customerLib = new \Library\Customer();
            $customerLib->setCustomerFromArray($this->params);

            $result = $customerLib->resetPassword();
    
            $rupaResponse = new \Library\Response();
            $rupaResponse->setContentType("application/json");
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
    
            if (!empty($customerLib->getErrorCode())) {
                $response['errors'] = array(
                    "code" => $customerLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                    "messages" => $customerLib->getErrorMessages()
                );
                $response['messages'] = array('save failed');
            } else {
                $response['errors'] = array();
                $response['data'] = array();
                $response['messages'] = array("success");
            }    
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function customerVerifyEmail()
    {
        // this is for customer verify email at my profile
        // { "customer_id" : 1 }
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($this->params);

        $result = $customerLib->verifyEmail();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!empty($customerLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array('verification failed');
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getCustomerData()
    {
        $customerLib = new \Library\Customer();
        $allCustomer = $customerLib->getAllCustomer($this->params);       
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (empty($allCustomer)) {
            $message = array();
            $code = 404;
            if (!empty($customerLib->getErrorMessages())) {
                $message[] = $customerLib->getErrorMessages();
            }

            if ((int) $customerLib->getErrorCode() > 0) {
                $code = (int) $customerLib->getErrorCode();
            }
            $rupaResponse->setStatusCode($code, $rupaResponse->getResponseDescription($code))->sendHeaders();
            $response['errors'] = array(
                "code" => $code,
                "title" => $rupaResponse->getResponseDescription($code),
                "messages" => $message
            );
        } else {
            $response['errors'] = array();
            $response['data'] = $allCustomer;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getCustomerCompany()
    {
        $customerLib = new \Library\Customer();
        $allCustomer = $customerLib->getCustomerCompany($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($allCustomer)) {
            $rupaResponse->setStatusCode(404, "Not found")->sendHeaders();
            $response['errors'] = array(
                "code" => 404 ,
                "title" => $rupaResponse->getResponseDescription(404),
                "messages" => []
            );
        } else {
            $response['errors'] = array();           
            $response['data'] = $allCustomer;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateCustomerCompany()
    {
        $customerLib = new \Library\Customer();
        $result = $customerLib->updateCustomerCompany($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($customerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $customerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages());
            $responseContent['messages'] = array('save failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveCustomerCompany()
    {
        $customerLib = new \Library\Customer();
        $result = $customerLib->saveCustomerCompany($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($customerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $customerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages());
            $responseContent['messages'] = array('save failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }


    public function getCustomerDataCustom()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $customerLib = new \Library\Customer();
            $allCustomer = $customerLib->getAllCustomerCustom($this->params);

            $rupaResponse = new \Library\Response();
            $rupaResponse->setContentType("application/json");
            if (empty($allCustomer['list'])) {
                $response['errors'] = array(
                    "code" => $customerLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                    "messages" => $customerLib->getErrorMessages()
                );
                $response['messages'] = array("result" => "failed");
                $response['data'] = array();
            } else {
                $response['errors'] = array();
                $response['messages'] = array("result" => "success", "recordsTotal" => $allCustomer['recordsTotal']);
                $response['data'] = $allCustomer['list'];
            }
        } catch (\Exception $e) {
            $response['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get customer list failed, please contact ruparupa tech support"
            );
            $response['messages'] = array("result" => "failed");
            $response['data'] = array();
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getCustomerDetail()
    {
        $customerLib = new \Library\Customer();
        $storeCodeNewRetail = !empty($this->params['store_code_new_retail']) ? $this->params['store_code_new_retail'] : '';
        unset($this->params['store_code_new_retail']);
        $allCustomer = $customerLib->getAllCustomer($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (empty($allCustomer)) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            if ($allCustomer[0]["registered_by"] != "informa (mobile apps)" && $allCustomer[0]["registered_by"] != "ace (mobile apps)") {
                $cartLib = new \Library\Cart();
                $respMergeMinicart = $cartLib->getCustomerMiniCartId($allCustomer[0]['customer_id'], "", "", "ODI", $storeCodeNewRetail);
                $allCustomer[0]['minicart_id'] = !empty($respMergeMinicart['data']['cart_id']) ? $respMergeMinicart['data']['cart_id'] : "";
            }
            $response['errors'] = array();
            $response['data'] = $allCustomer[0];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function generateToken()
    {
        $customerLib = new \Library\Customer();
        $customerLib->generateToken($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($customerLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success, email send to customer");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function generateTokenOtp() // for generating forgot password otp token only
    {
        $customerLib = new \Library\Customer();
        $token = $customerLib->generateTokenOtp($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($customerLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = "failed";
        } else {
            $response['errors'] = array();
            $response['data'] = array("token" => $token);
            $response['messages'] = "success, token has been generated";
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function checkToken()
    {

        $customerLib = new \Library\Customer();
        $customerLib->tokenCheck($this->params['token']);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($customerLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array("Invalid token");
        } else {
            $response['errors'] = array();
            $response['data'] = $customerLib->getCustomer()->getDataArray();
            $response['messages'] = array("Token is valid");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function checkCustomer()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $params = $this->params;
        if (empty($params)) {
            $response['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => "Payload tidak lengkap"
            );
            $response['message'] = "failed";
            $response['data'] = (object)[];
            $rupaResponse->setContent($response);
            return $rupaResponse->send();
        }

        /**
         * Potential Action:
         * login
         * forgot-password
         * social-login
         * register
         * change-phone
         * change-email
         */

        // Validate required params
        $errorMessages = [];

        // Default
        $requiredParamField = ["action"];
        $requiredFieldTranslation = [
            'phone' => 'Nomor telepon',
            'email' => 'Alamat email',
            'password' => 'Kata sandi'
        ];

        // Add more $requiredParamField based on action
        switch ($params['action']) {
            case 'login':
                array_push($requiredParamField, 'password');
                break;
            case 'register':
                array_push($requiredParamField, 'email', 'phone');
                break;
            case 'change-phone':
                array_push($requiredParamField, 'phone');
                break;
            case 'change-email':
                array_push($requiredParamField, 'email');
                break;
            case 'update-phone':
                array_push($requiredParamField, 'phone');
                break;
        }

        foreach ($requiredParamField as $field) {
            if (empty($params[$field])) {
                $translatedField = $field;
                if (!empty($requiredFieldTranslation[$field])) {
                    $translatedField = $requiredFieldTranslation[$field];
                }
                $errorMessages[] = sprintf("%s tidak boleh kosong", ucfirst($translatedField));
            }
        }

        // More validation that can't utilize $requiredParamField
        if (empty($params["phone"]) && empty($params['email'])) {
            $errorMessages[] = "Alamat email dan nomor telepon tidak boleh kosong";
        }

        if (count($errorMessages) > 0) {
            $response['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => $errorMessages[0]
            );
            $response['message'] = "failed";
            $response['data'] = (object)[];
            $rupaResponse->setContent($response);
            return $rupaResponse->send();
        }

        // Prepare & override params
        $params['email'] = !empty($params['email']) ? strtolower($params['email']) : '';
        $params['phone'] = !empty($params['phone']) ? $params['phone'] : '';
        $params['phone'] = strpos($params["phone"], "+62") > -1 ? str_replace("+62", "0", $params['phone']) : $params['phone'];
    
        // Start logic
        $customerLib = new \Library\Customer();
        $result = $customerLib->checkCustomer($params);
        if (!empty($customerLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "message" => $customerLib->getErrorMessages()
            );
            $response['message'] = "failed";
            $response['data'] = (object)[];
        } else {
            $response['errors'] = array();
            $response['data'] = $result;
            $response['message'] = "success";
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getVoucherList()
    {
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($this->params);
        $customerLib->setConditionsFromArray($this->params);
        $voucherList = $customerLib->getVoucherList();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($customerLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array("Error");
        } else {
            $response['errors'] = array();
            $response['data'] = $voucherList;
            $response['messages'] = array("Success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function changePassword()
    {
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($this->params);
        $voucherList = $customerLib->changePassword();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($customerLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array("Error");
        } else {
            $response['errors'] = array();
            $response['data'] = $voucherList;
            $response['messages'] = array("Success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getCustomerWishlist()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $customerLib = new CustomerLib();
        $data = $customerLib->getWishlistData($this->params);
        if (count($data) > 0) {
            $responseContent['data'] = $data;
            $responseContent['messages'] = array("success");
        } else {
            $responseContent['errors'] = array("code" => "RR302", "title" => "Error", "messages" => "No data found");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getWishlistList()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!isset($this->params['limit']) || !isset($this->params['offset'])) {
            $responseContent['errors'] = array("code" => "RR302", "title" => "Error", "messages" => "Limit and Offset needed");
        } else {
            $customerLib = new CustomerLib();
            $data = $customerLib->getWishlistList($this->params);
            if (count($data['data']) > 0) {
                $responseContent['data'] = $data['data'];
                $responseContent['messages'] = array("result" => "success", "recordsTotal" => $data['recordsTotal']);
            } else {
                $responseContent['errors'] = array("code" => "RR302", "title" => "Error", "messages" => "No data found");
            }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function addCustomerWishlist()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $customerLib = new CustomerLib();
        $result = $customerLib->saveWishlist($this->params);
        if (!empty($result['errors'])) {
            $responseContent['errors'] = array("code" => "RR302", "title" => "Error", "messages" => $result['errors']);
        } else {
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteCustomerWishlist()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $customerLib = new CustomerLib();
        $result = $customerLib->deleteWishlist($this->params);
        if (!empty($result['errors'])) {
            $responseContent['errors'] = array("code" => "RR302", "title" => "Error", "messages" => $result['errors']);
        } else {
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveCustomerFacebookleads()
    {
        $customerFbleadsModel = new \Models\CustomerFbleads();
        $customerFbleadsModel->setFromArray($this->params);
        $customerFbleadsModel->saveData("customer_fbleads");

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if (!empty($customerFbleadsModel->getErrorCode())) {
            $rupaResponse->setStatusCode(404, "Not Found")->sendHeaders();

            $responseContent['errors'] = array(
                "code" => $customerFbleadsModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerFbleadsModel->getErrorCode()),
                "messages" => $customerFbleadsModel->getErrorMessages()
            );
        } else {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = "success";
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveCustomerMemberChances()
    {
        $customerLib = new \Library\Customer();
        $res = $customerLib->saveMemberChances($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!empty($customerLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array();
        } else {
            // Automatic login and get voucher list for this customer
            $response['errors'] = array();
            $response['data'] = array(
                'affected_rows' => $res
            );
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getAbuserList()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!isset($this->params['limit']) || !isset($this->params['offset'])) {
            $responseContent['errors'] = array("code" => "RR302", "title" => "Error", "messages" => "Limit and Offset needed");
        } else {
            $customerLib = new CustomerLib();
            $data = $customerLib->getAbuserList($this->params);
            if (count($data['data']) > 0) {
                $responseContent['data'] = $data['data'];
                $responseContent['messages'] = array("result" => "success", "recordsTotal" => $data['recordsTotal']);
            } else {
                $responseContent['errors'] = array("code" => "RR302", "title" => "Error", "messages" => "No data found");
            }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    // Store Mode
    public function loginStoreMode()
    {
        $customerLib = new \Library\Customer();
        $loginResult = $customerLib->loginStoreMode($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (empty($loginResult)) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array("error");
        } else {
            $response['errors'] = array();
            $response['data'] = $loginResult;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    // Store Mode
    public function linkingCustomerBetweenCompany()
    {
        $customerLib = new \Library\Customer();
        $linkingResult = $customerLib->linkingCustomerBetweenCompany($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (empty($linkingResult)) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array("error");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }
    
    public function getStoreModeCustomer(){
        $customerLib = new \Library\Customer();
        $getCustomer = $customerLib->getStoreModeCustomer($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (empty($getCustomer)) {
            $response['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $response['messages'] = array("error");
        } else {
            $response['errors'] = array();
            $response['data'] = $getCustomer;
            $response['messages'] = array("success");
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }
}
