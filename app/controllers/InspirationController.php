<?php


namespace Controllers;

use Library\Nsq;
use Models\Inspiration;
use Phalcon\Db;

class InspirationController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function inspirationList()
    {
        $inspirationLib = new \Library\Inspiration();

        $allInspirations = $inspirationLib->getAllInspiration($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($inspirationLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $inspirationLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($inspirationLib->getErrorCode()),
                "messages" => $inspirationLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Inspirations not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $allInspirations;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function newInspirationList()
    {
        $inspirationLib = new \Library\Inspiration();

        $allInspirations = $inspirationLib->getAllNewInspiration($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($inspirationLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $inspirationLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($inspirationLib->getErrorCode()),
                "messages" => $inspirationLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Inspirations not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $allInspirations;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function listPromotionInspirations()
    {
        $inspirationLib = new \Library\Inspiration();

        $allInspirations = $inspirationLib->getAllInspiration($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($inspirationLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $inspirationLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($inspirationLib->getErrorCode()),
                "messages" => $inspirationLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Inspirations not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $allInspirations;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }    

    public function inspirationDetail()
    {
        $inspirationLib = new \Library\Inspiration();
        $inspirationData = $inspirationLib->getInspirationDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($inspirationLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $inspirationLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($inspirationLib->getErrorCode()),
                "messages" => $inspirationLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Inspiration not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $inspirationData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}