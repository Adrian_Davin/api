<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 8/22/2017
 * Time: 3:04 PM
 */

namespace Controllers;

/**
 * Class ControllerNameController
 * @package Controllers
 */
class CustomerAlertController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getAlertList()
    {
        $customerLib = new \Library\Customer();
        $customerAlertList = $customerLib->getAlertList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($customerLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $customerLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages()
            );
            $responseContent['messages'] = array("result" => "Customer alert not found","recordsTotal" => 0);
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("result" => "success","recordsTotal" => $customerAlertList['recordsTotal']);
            $responseContent['data'] = $customerAlertList['list'];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveCustomerAlert()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $customerAlertModel = new \Models\CustomerAlert();
        // set customer_id
        if(!empty($this->params['email'])) {
            $customerModel = new \Models\Customer();
            $customerResult = $customerModel->findFirst('email = "'.$this->params['email'].'"');
            if ($customerResult) {
                $customerId = $customerResult->toArray()['customer_id'];
                $this->params['customer_id'] = $customerId;
            }
        }
        $customerAlertModel->setFromArray($this->params);

        $error = $customerAlertModel->saveData();

        if(!$error){
            $response['errors'] = array("code" => "RR301",
                "title" => $rupaResponse->getResponseDescription("RR301"),
                "messages" => $error);
            $response['messages'] = array('Save failed');
        }else{
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function deleteCustomerAlert()
    {
        $customerLib = new \Library\Customer();
        $deleteStatus = $customerLib->deleteCustomerAlert($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$deleteStatus) {
            $responseContent['errors'] = array(
                "code" => $customerLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($customerLib->getErrorCode()),
                "messages" => $customerLib->getErrorMessages());
            $responseContent['messages'] = array('delete failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}