<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class DepartmentBuController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $departmentBuModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->departmentBuModel = new \Models\DepartmentBu();
    }

    public function listDepartment()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $masterAppId = !empty($this->params['master_app_id']) ? $this->params['master_app_id'] : "";
        $supplierId = !empty($this->params['supplier_id']) ? $this->params['supplier_id'] : "";
        $status = 0;

        // Get supplier alias by store code
        if (!empty($this->params['store_code'])){
            $storeModel = new \Models\Store();
            $storeQuery = sprintf("store_code = '%s'", $this->params['store_code']);
            $storeData = $storeModel::findFirst($storeQuery);
            if($storeData) {
                $supplierId = $storeData->Supplier->getSupplierId();
            }
        }
        
        if ($supplierId != ""){
            $brand = $this->departmentBuModel->find(
                array(
                    'conditions' => ' status > '.$status.' and supplier_id = '.$supplierId,
                    'order' => 'sister_company, name'
                )
            );
        }else{
            if ($masterAppId == 2){
                $status = -1;
            }

            $seachParams =  array(
                'conditions' => ' status > '.$status,
                'order' => 'sister_company, name'
            );

            $brand = $this->departmentBuModel->find($seachParams);
        }
        
        $responseContent['data'] = $brand->toArray();
        $responseContent['errors'] = $this->departmentBuModel->getErrors();
        $responseContent['messages'] = $this->departmentBuModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function detailDepartment()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $id_department_bu = $this->params['id_department_bu'];

        $department = $this->departmentBuModel->findFirst(' id_department_bu = '.$id_department_bu);

        $responseContent['data'] = $department->toArray();
        $responseContent['errors'] = $this->departmentBuModel->getErrors();
        $responseContent['messages'] = $this->departmentBuModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function createDepartmentBU()
    {
        $departmentBuModel = new \Models\DepartmentBu();
        $now = date("Y-m-d H:i:s");
        $this->params['create_date'] = $now;
        $departmentBuModel->setFromArray($this->params);
        $departmentBUName = $departmentBuModel->findFirst(' supplier_id = '.$this->params['supplier_id'].' and name = '.'"'.$this->params['name'].'"');
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(!$departmentBUName){
            $departmentBuModel->saveData('create_departmentBu');

            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        
            if(!empty($departmentBuModel->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $departmentBuModel->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($departmentBuModel->getErrorCode()),
                    "messages" => $departmentBuModel->getErrorMessages()
                );
                $responseContent['messages'] = array("Failed to save");
                $responseContent['data'] = array();
                
            } 
            $responseContent['messages'] = array("success");
            $rupaResponse->setContent($responseContent);
            $rupaResponse->send();
        }else{
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['errors'] = array(
                "code" => "RR102",
                "title" => "Data already exist",
                "messages" => "Department is duplicate"
            );
            $responseContent['messages'] = array("Failed to save");
            $responseContent['data'] = array();
            $rupaResponse->setContent($responseContent);
            $rupaResponse->send();
        }
    }

    public function updateDepartmentBU(){
        $departmentBuModel = new \Models\DepartmentBu();
        $now = date("Y-m-d H:i:s");
        $this->params['create_date'] = $now;
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $departmentBuModel->setFromArray($this->params);
        $departmentBuModel->saveData('update_departmentBu');
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($departmentBuModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $departmentBuModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($departmentBuModel->getErrorCode()),
                "messages" => $departmentBuModel->getErrorMessages()
            );
            $responseContent['messages'] = array("success");
            $responseContent['data'] = array();
        } 
        $responseContent['messages'] = array("success");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }


}