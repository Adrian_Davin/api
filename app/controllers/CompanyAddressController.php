<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 5:50 PM
 */
namespace Controllers;

class CompanyAddressController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getAddressData()
    {
        $companyLib = new \Library\Company();
        $allAddress = $companyLib->getCompanyAddress($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($allAddress)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Address not found");
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allAddress;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function saveAddress()
    {
        $companyLib = new \Library\Company();
        $companyLib->saveAddressData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($companyLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $companyLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($companyLib->getErrorCode()),
                "messages" => $companyLib->getErrorMessages());
            $responseContent['messages'] = array('save failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteAddress()
    {
        $companyLib = new \Library\Company();
        $deleteStatus = $companyLib->deleteCompanyAddress($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$deleteStatus) {
            $responseContent['errors'] = array(
                "code" => $companyLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($companyLib->getErrorCode()),
                "messages" => $companyLib->getErrorMessages());
            $responseContent['messages'] = array('delete failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}