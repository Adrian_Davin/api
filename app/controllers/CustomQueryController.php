<?php
namespace Controllers;

use \Models\CustomQuery as CustomQuery;

class CustomQueryController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function marketingRuleStatus(){
        $CustomQuery = new \Models\CustomQuery();
        $CustomQuery->marketingRuleStatus();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$CustomQuery) {
            $response['errors'] = array(
                "code" => "RR301" ,
                "title" => "Failed update",
                "messages" => "Update data failed"
            );
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function customInventoryAfterBatchStock(){
        $CustomQuery = new \Models\CustomQuery();
        $CustomQuery->customInventoryAfterBatchStock();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$CustomQuery) {
            $response['errors'] = array(
                "code" => "RR301" ,
                "title" => "Failed update",
                "messages" => "Update data failed"
            );
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

}