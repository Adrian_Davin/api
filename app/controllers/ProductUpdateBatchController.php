<?php


namespace Controllers;

use Models\Event;

class ProductUpdateBatchController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function updateBatchList()
    {

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $updateBatchLib = new \Library\ProductUpdateBatch();
        $updateBatchData = $updateBatchLib->getAllUpdateBatch();

        $responseContent = array();

        if (!empty($updateBatchLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $updateBatchLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($updateBatchLib->getErrorCode()),
                "messages" => $updateBatchLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get special price");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $updateBatchData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function processUpdateBatch()
    {

        $updateBatchLib = new \Library\ProductUpdateBatch();
        $updateBatchResult = $updateBatchLib->processUpdateBatch($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['messages'] = array("Update Batch Product");
        $responseContent['data'] = array('affected_row' => $updateBatchResult);

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveUpdateBatch()
    {

        $updateBatchLib = new \Library\ProductUpdateBatch();
        $updateBatchResult = $updateBatchLib->saveUpdateBatch($this->params);


        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['messages'] = array("Update Batch Product");
        $responseContent['data'] = array('product_update_batch_id' => $updateBatchResult);

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}
