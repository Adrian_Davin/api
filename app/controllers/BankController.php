<?php

namespace Controllers;


class BankController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getBankInstallmentList()
    {
        $payment = new \Library\Payment();
        $installmentList = $payment->getInstallmentList();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($payment->getErrorMessages())) {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();

            $responseContent['errors'] = array(
                "code" => $payment->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($payment->getErrorCode()),
                "messages" => $payment->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $statusCode = 200;
            if(empty($installmentList)) {
                $statusCode = 404;
            }

            $rupaResponse->setStatusCode($statusCode, "Ok")->sendHeaders();
            $responseContent['errors'] = array();
            $responseContent['data'] = $installmentList;
            $responseContent['messages'] = "success";
        }
        
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}