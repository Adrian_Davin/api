<?php

namespace Controllers;


/**
 * Class SalesLogController
 * @package Controllers
 * @RoutePrefix("/sales/log")
 *
 */
class SalesLogController extends \Controllers\BaseController
{

    /**
     * @var \Models\SalesLog
     */
    protected $salesLog;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->salesLog = new \Models\SalesLog();
    }

    public function getSalesLogList()
    {
        /*
         * can use header params: sales_log_id, sales_order_id, invoice_id, admin_user_id, actions, flag, order_by
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $salesLogLib = new \Library\SalesLog();
            $allLogs = $salesLogLib->getAllSalesLog($this->params);

            if(!empty($salesLogLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $salesLogLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($salesLogLib->getErrorCode()),
                    "messages" => $salesLogLib->getErrorMessages()
                );
                $responseContent['data'] = array();
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $allLogs;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get sales log failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getSalesLogData()
    {
        /*
         * params: sales_log_id
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $salesLogLib = new \Library\SalesLog();
            $sales_log_id = !empty($this->params['sales_log_id']) ? $this->params['sales_log_id'] : "";
            $salesLogData = $salesLogLib->getSalesLogData($sales_log_id);

            if(!empty($salesLogLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $salesLogLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($salesLogLib->getErrorCode()),
                    "messages" => $salesLogLib->getErrorMessages()
                );
                $responseContent['data'] = array();
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $salesLogData;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get sales log failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveSalesLog()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $this->salesLog->setFromArray($this->params);
            $this->salesLog->saveData("sales_log");
            if(!empty($this->salesLog->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $this->salesLog->getErrorCode() ,
                    "title" => $rupaResponse->getResponseDescription($this->salesLog->getErrorCode()),
                    "message" => $this->salesLog->getErrorMessages());
            } else {
                $responseContent['messages'] = "success";
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Save sales log failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}