<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class ReportsController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $reportsModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->reportsModel = new \Models\Reports();
    }

    public function mpReports()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;

        $result = $this->reportsModel->findFirst(
            array(
                'conditions' =>
                    'supplier_id = '.$params['supplier_id'].' and 
                    start_periode = "'.$params['start_date'].'" and 
                    end_periode = "'.$params['end_date'].'" and
                    report_type = "'.$params['report_type'].'"
                    '
            )
        );

        if(!empty($result)){

                $data = $result->toArray();
                // update table sc_seller_report
                $data_update = array(
                    'file_name' => $params['file_name'],
                    'supplier_id' => $params['supplier_id'],
                    'report_type' => $params['report_type'],
                    'start_periode' => $params['start_date'],
                    'end_periode' => $params['end_date'],
                    'updated_at' => date('Y-m-d H:i:s')

                );
                $this->reportsModel->update_($data_update,'supplier_report',' report_id = '.$data['report_id']);
        }else{
                // insert table sc_seller_report
                $data_insert = array(
                    'file_name' => $params['file_name'],
                    'supplier_id' => $params['supplier_id'],
                    'report_type' => $params['report_type'],
                    'start_periode' => $params['start_date'],
                    'end_periode' => $params['end_date'],
                    'created_at' => date('Y-m-d H:i:s')

                );
                $this->reportsModel->insert($data_insert,'supplier_report');
        }

        $responseContent['data'] = array('success');
        $responseContent['errors'] = $this->reportsModel->getErrors();
        $responseContent['messages'] = $this->reportsModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function financeReports(){
        $reportsLib = new \Library\Reports();
        $allOrders = $reportsLib->getAllOrder($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($reportsLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $reportsLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($reportsLib->getErrorCode()),
                "messages" => $reportsLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $recordsTotal = $allOrders['recordsTotal'];
            unset($allOrders['recordsTotal']);

            $responseContent['messages'] = array("result" => "success", "recordsTotal" => $recordsTotal);
            $responseContent['data'] = $allOrders;
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }
}