<?php

namespace Controllers;

use \Library\Cart as CartLib;
use \Models\Cart as CartModel;
use \Models\CustomerAddress as CustomerAddress;
use Phalcon\Db;

/**
 * Class ShoppingCartController
 * @package Controllers
 * @RoutePrefix("/shopping/cart")
 */
class ShoppingCartController extends \Controllers\BaseController
{
    /**
     * router for creating and updating shopping cart
     */
    public function saveCartData()
    {
        $cart = new CartLib();
        $cart->setFromParam($this->params);
        if(empty($this->params['source_order']) && empty($cart->getErrorCode())){
            \Helpers\LogHelper::log('debug_625', '/app/controller/ShoppingCartController.php line 25', 'debug');
            $cart->checkMarketingPromotion(); // cart items saved in this process
            \Helpers\LogHelper::log('debug_625', '/app/controller/ShoppingCartController.php line 27', 'debug');
            $cart->checkGiftcartApplied(['exclude_payment']);
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            
            $responseContent['messages'] = array("failed to save shopping cart");
        } else {
            $cart->saveCart();
            $responseContent['messages'] = array("success");
        }

        $responseContent['data'] = array("cart_id" => $cart->getCartId(), "token" => $cart->getToken());
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCartData()
    {
        $cart = new CartLib();
        $cart->setCartId($this->params['cart_id']);

        if (isset($this->params['reserved_order_no'])) {
            $cart->setReservedOrderNo($this->params['reserved_order_no']);
        }

        $resetShippingRate = !empty($this->params['reset_shipping']) ? $this->params['reset_shipping'] : 0;
        $modifyItems = !empty($this->params['modify_items']) ? $this->params['modify_items'] : 0;
        $cartData = $cart->loadCartData($resetShippingRate, $modifyItems);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to save shopping cart");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $cartData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteCartItems()
    {
        $cart = new CartLib();
        $cart->deleteCartItems($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to delete item");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = array("cart_id" => $cart->getCartId());

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function checkStockItems()
    {
        $cart = new CartModel(); 
        $cart->setCartId($this->params['cart_id']);
        $cart->stockCheck();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("Stock not found");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = array("cart_id" => $cart->getCartId());

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }



    public function saveCartFromOrder()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        // get order from cache
        $salesOrderData = \Library\Redis\SalesOrder::getSalesOrder($this->params['order_no']);

        $customerAddressModel = new \Models\CustomerAddress();

        // if not found in cache, load it from db
        if(empty($salesOrderData)) {
            $salesOrder = new \Library\SalesOrder();
            $allSalesOrderData = $salesOrder->getAllOrder(['order_no' => $this->params['order_no']]);

            if(empty($allSalesOrderData)) {
                $rupaResponse->setStatusCode(404, "Not found")->sendHeaders();
            } else {
                $salesOrderData = $allSalesOrderData[0];

                $salesOrderData['reference_order_no'] = $salesOrderData['order_no'];

                unset($salesOrderData['sales_order_id']);
                unset($salesOrderData['device']);
                unset($salesOrderData['remote_ip']);
                unset($salesOrderData['order_no']);
                unset($salesOrderData['cart_id']);
                unset($salesOrderData['payment']);
                unset($salesOrderData['status']);
                $salesOrderData['gift_cards'] = "[]";
                $salesOrderData['gift_cards_amount'] = 0;

                //set new param from new cart
                $salesOrderData['device']    = (!empty($this->params['device']))? $this->params['device'] : "oms";
                $salesOrderData['remote_ip'] = (!empty($this->params['remote_ip']))? $this->params['remote_ip'] : $this->request->getClientAddress();
                
                if (isset($this->params['refund_no'])) {
                    $salesOrderData['refund_no'] = $this->params['refund_no'];
                }

                $salesOrderData['payment']   = (!empty($this->params['payment']))? $this->params['payment'] : "";

                $billingAddress = $customerAddressModel->findFirst("customer_id = ".$salesOrderData['customer']['customer_id']);
                $shippingAddress = $customerAddressModel->findFirst("customer_id = ".$salesOrderData['customer']['customer_id']);

                $salesOrderData['billing_address'] = !empty($billingAddress)? $billingAddress->toArray() : array();
                $salesOrderData['shipping_address']= !empty($shippingAddress)? $shippingAddress->toArray() : array();

                $salesOrderData['billing_address']['sales_order_address_id'] = null;
                $salesOrderData['shipping_address']['sales_order_address_id'] = null;

                /**
                 * Set sales order item id to null
                 */
                foreach ($salesOrderData['items'] as $keyItem => $item) {
                    unset($salesOrderData['items'][$keyItem]['sales_order_item_id']);
                }


                $cart = new CartLib();
                $cart->setFromParam($salesOrderData);
                //if(empty($cart->getErrorCode())) {
                $cart->saveCart();
                //}

                $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            }
        }

        $responseContent = array();

        if(!empty($cart)) {
            if(!empty($cart->getErrorCode())) {
                $errorCode = $cart->getErrorCode();
                if(is_array($errorCode)) {
                    $errorCode = "RR200";
                }
                $responseContent['errors'] = array(
                    "code" => $errorCode,
                    "title" => $rupaResponse->getResponseDescription($errorCode),
                    "messages" => $cart->getErrorMessages()
                );
                $responseContent['messages'] = array("failed to create shopping cart");
            } else {
                $responseContent['messages'] = array("success");
            }
            $responseContent['data'] = $cart->getDataArray();
        } else {
            $responseContent['errors'] = array(
                "code" => 404,
                "title" => $rupaResponse->getResponseDescription("404"),
                "messages" => "Order no not found"
            );
        }


        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function redeemVoucher()
    {
        $cartModel = new CartModel();
        $cartModel->setCartId($this->params['cart_id']);
     \Helpers\LogHelper::log('debug_625', '/app/controller/ShoppingCartController.php line 264', 'debug');
        $cartModel->redeemVoucher($this->params['voucher_code'], false, true, true, isset($this->params['company_code']) ? $this->params['company_code'] : 'ODI');

        if(empty($cartModel->getErrorCode())) {
            $cartData = $cartModel->getDataArray();
            $cartData['do_count_shipping'] = true;
            $cart = new CartLib();
            $cart->setFromParam($cartData);
            \Helpers\LogHelper::log('debug_625', '/app/controller/ShoppingCartController.php line 273', 'debug');
            $cart->checkMarketingPromotion();
            \Helpers\LogHelper::log('debug_625', '/app/controller/ShoppingCartController.php line 275', 'debug');
            $cart->checkGiftcartApplied(['exclude_payment']);
            $cart->saveCart();
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cartModel->getErrorCode())) {
            $errorCode = $cartModel->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cartModel->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to redeem voucher");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = array("cart_id" => $cartModel->getCartId());

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteRedeemVoucher()
    {
        $cartModel = new CartModel();
        $cartModel->setCartId($this->params['cart_id']);
        $cartModel->deleteRedeemVoucher($this->params['voucher_code']);

        if(empty($cartModel->getErrorCode())) {
            $cartData = $cartModel->getDataArray();
            $cartData['do_count_shipping'] = true;
            $cart = new CartLib();
            $cart->setFromParam($cartData);
            \Helpers\LogHelper::log('debug_625', '/app/controller/ShoppingCartController.php line 316', 'debug');
            $cart->checkMarketingPromotion();
            \Helpers\LogHelper::log('debug_625', '/app/controller/ShoppingCartController.php line 318', 'debug');
            $cart->checkGiftcartApplied(['exclude_payment']); //this method is needed because recalculate existing giftcards to cart (tobe reviewed)
            $cart->saveCart();
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cartModel->getErrorCode())) {
            $errorCode = $cartModel->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cartModel->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to redeem voucher");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = array("cart_id" => $cartModel->getCartId());

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function checkPayment()
    {
        $cart = new CartLib();
         \Helpers\LogHelper::log('debug_625', '/app/Controller/ShoppingCartController.php line 344', 'debug');
        $cartResponse = $cart->checkPayment($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to apply payment promotion");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = $cartResponse;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function shippingAssignation()
    {
        $cart = new CartLib();
        $cart->setCartId($this->params['cart_id']);

        $resetShippingRate = !empty($this->params['reset_shipping']) ? $this->params['reset_shipping'] : 0;
        $modifyItems = !empty($this->params['modify_items']) ? $this->params['modify_items'] : 0;
        $companyCode = !empty($this->params['company_code']) ? $this->params['company_code'] : "";
        $cartData = $cart->loadCartData($resetShippingRate, $modifyItems);

        $assignation = $cart->shippingAssignation($cartData, $companyCode);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to apply payment promotion");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = $assignation;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function checkCashBackPromotion()
    {
        $cart = new CartLib();
        \Helpers\LogHelper::log('debug_625', '/app/Controller/ShoppingCartController.php line 415', 'debug');
        $cartResponse = $cart->checkCashBackPromotion($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to apply payment promotion");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = $cartResponse;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function checkVoucherPromotion()
    {
        $cart = new CartLib();
         \Helpers\LogHelper::log('debug_625', '/app/Controllers/ShoppingCartController.php line 446', 'debug');
        $cartResponse = $cart->checkVoucherPromotion($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to apply payment promotion");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = $cartResponse;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function countShippingCost()
    {
        $cart = new CartLib();
        $cart->countShippigAndHandling($this->params);
        if(empty($cart->getErrorCode())) {
            $cart->saveCart();
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to apply payment promotion");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = [];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function savePaymentData()
    {
        $cart = new CartLib();
        $cart->savePayment($this->params);
        if(empty($cart->getErrorCode())) {
            $cart->saveCart();
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to apply payment promotion");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = [];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function savePoNumberData()
    {
        $cart = new CartLib();
        $cart->savePoNumber($this->params);
        if(empty($cart->getErrorCode())) {
            $cart->saveCart();
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorCode())) {
            $errorCode = $cart->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to update po number");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = [];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function cartValidation()
    {
        $cart = new CartLib();
        $cart->cartValidation($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($cart->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "messages" => $cart->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to validate shopping cart");
        } else {
            $responseContent['messages'] = array("Shopping cart is valid");
        }
        
        $responseContent['data'] = [];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}