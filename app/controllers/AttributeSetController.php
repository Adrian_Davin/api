<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class AttributeSetController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $attributeSetModel;
    protected $attributeSet2AttributeModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->attributeSetModel = new \Models\AttributeSet();
        $this->attributeSet2Attribute = new \Models\AttributeSet2Attribute();
    }

    public function listAttributeSet()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $brand = $this->attributeSetModel->find(
                array(
                    'conditions' => ' status >= 0 ',
                    'order' => 'attribute_set_name'
                )
        );

        $responseContent['data'] = $brand->toArray();
        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function detailAttributeSet()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $ah = $this->params['article_hierarchy'];

        $department = $this->attributeSetModel->findFirst(' attribute_set_name = "'.$ah.'" ');

        $responseContent['data'] = $department->toArray();
        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function list2Attribute()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $conditions = ' status >= 0 ';
        if (isset($this->params['attribute_id'])) {
          $conditions .= ' and attribute_id = '.$this->params['attribute_id'];
        }
        $attribute = $this->attributeSet2Attribute->find(array(
          'conditions' => $conditions
        ));

        $responseContent['data'] = $attribute->toArray();
        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function update2Attributes() {
        $attributeSetLib = new \Library\AttributeSet();
        $attributeSetLib->update2Attributes($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($attributeSetLib->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $attributeSetLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($attributeSetLib->getErrorCode()),
                "messages" => $attributeSetLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $responseContent['data'] = '';
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateAttributeSet() {
        $attributeSetLib = new \Library\AttributeSet();
        $attributeSetLib->updateAttributeSet($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($attributeSetLib->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $attributeSetLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($attributeSetLib->getErrorCode()),
                "messages" => $attributeSetLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $responseContent['data'] = '';
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createAttributeSet() {
        $attributeSetLib = new \Library\AttributeSet();
        $attributeSetId = $attributeSetLib->createAttributeSet($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($attributeSetLib->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $attributeSetLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($attributeSetLib->getErrorCode()),
                "messages" => $attributeSetLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array("");
        } else {
            $responseContent['data'] = $attributeSetId;
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}