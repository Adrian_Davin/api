<?php


namespace Controllers;

use Models\Event;

class SpecialPriceController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function specialPriceList()
    {
        $specialPriceLib = new \Library\SpecialPrice();
        $specialPrice = $specialPriceLib->getAllSpecialPrice($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if (!empty($specialPriceLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $specialPriceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($specialPriceLib->getErrorCode()),
                "messages" => $specialPriceLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get special price");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $specialPrice;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function generateSpecialPriceInstant(){
        $productPriceModel = new \Models\ProductPrice();
        $specialPriceModel = new \Models\SpecialPrice();

        $activeSpecialPrice['price_value'] = $specialPriceModel->getAllActiveSpecialPrice();
        $activeSpecialPrice['instant'] = '1';

        $generatedPrice = $productPriceModel->generatePrice($activeSpecialPrice);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if (!empty($productPriceModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $productPriceModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($productPriceModel->getErrorCode()),
                "messages" => $productPriceModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to generate special price");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $generatedPrice;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function specialPriceActiveList()
    {
        $specialPriceModel = new \Models\SpecialPrice();
        $specialPrice = $specialPriceModel->getAllActiveSpecialPrice();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if (!empty($specialPriceModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $specialPriceModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($specialPriceModel->getErrorCode()),
                "messages" => $specialPriceModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get special price");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            if (empty($specialPrice)) {
                $responseContent['data'] = "No active special price found";
            } else {
                $responseContent['data'] = $specialPrice;
            }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function generateSpecialPrice()
    {
        $productPriceModel = new \Models\ProductPrice();
        $result = $productPriceModel->generatePrice($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if (!empty($productPriceModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $productPriceModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($productPriceModel->getErrorCode()),
                "messages" => $productPriceModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get special price");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createSpecialPrice()
    {
        $specialPriceModel = new \Models\SpecialPrice();
        $specialPriceModel->setPriceFromArray($this->params);
        $specialPriceModel->insertPriceToDb();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!empty($specialPriceModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $specialPriceModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($specialPriceModel->getErrorCode()),
                "messages" => $specialPriceModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create special price");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success creating special price");
            $responseContent['data'] = "";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createPriceZoneGideon()
    {
        $priceZoneGideon = new \Models\PriceZoneGideon();
        $priceZoneGideon->setParamsFromArray($this->params);
        $priceZoneGideon->insertPriceZoneToDb();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!empty($priceZoneGideon->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $priceZoneGideon->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($priceZoneGideon->getErrorCode()),
                "messages" => $priceZoneGideon->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create price zone gideon");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success creating price zone gideon");
            $responseContent['data'] = $priceZoneGideon->getData();
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteExpiredPrice()
    {
        $specialPriceModel = new \Models\SpecialPrice();
        $deleted = $specialPriceModel->deleteSpecialPrice();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if (!empty($specialPriceModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $specialPriceModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($specialPriceModel->getErrorCode()),
                "messages" => $specialPriceModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get special price");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $deleted;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}
