<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 24/1/2017
 * Time: 10:55 AM
 */
namespace Controllers;

/**
 * Class AdminUserController
 * @package Controllers
 */
class AdminUserController extends \Controllers\BaseController
{
    /**
     * @var $adminUser \Models\AdminUser
     */
    protected $adminUser;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->adminUser = new \Models\AdminUser();
    }

    public function login()
    {
        /**
         * params: email, password, master_app_id
         */
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $result = $this->adminUser->login($this->params);

        /**
         * ToDo : Get Gideon User Role
         */
        if($this->params['master_app_id'] == 2){
            if (isset($result['data'])){
                foreach($result['data'][0]['attributes'] as $row){
                    if($row['admin_user_attribute_id'] == 2){
                        $role_id = $row['value'];
                        break;
                    }
                }

                $gideonAdminRoleMod = new \Models\GideonAdminRole();
                $roleData = $gideonAdminRoleMod->findFirst(' role_id = '.$role_id);
                $result['data'][0]['gideon_admin_role'] = $roleData->getDataArray();
            }

        }

        if (isset($result['data'])) $response['data'] = $result['data'];
        if (isset($result['messages'])) $response['messages'] = $result['messages'];
        if (isset($result['error'])) $response['error'] = $result['error'];

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function logout()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $result = $this->adminUser->logout($this->params);
            if (isset($result['error'])) {
                $responseContent['errors'] = array(
                    "code" => "R100" ,
                    "title" => "Error Message",
                    "message" => $result['error']
                );
            } else {
                $responseContent['messages'] = array("success");
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Process logout failed"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getAdminUserList()
    {
        // can use header params: admin_user_id, email, firstname, order_by

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $adminUserLib = new \Library\AdminUser();
            $allUsers = $adminUserLib->getAllAdminUsers($this->params);
            $sumUsers = $adminUserLib->getSumAdminUser($this->params);

            if(!empty($adminUserLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $adminUserLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($adminUserLib->getErrorCode()),
                    "messages" => $adminUserLib->getErrorMessages(),
                );
            } else {
                $responseContent['messages'] =
                array("result" => "success", "recordsTotal" => $sumUsers);
                $responseContent['data'] = $allUsers;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get admin user failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    // For admin user dashboard in oms
    public function getAdminUserListCustom()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $adminUserLib = new \Library\AdminUser();
            $allUsers = $adminUserLib->getAllAdminUserCustom($this->params);

            if(empty($allUsers['list']) || !empty($adminUserLib->getErrorCode())) {
                if (!empty($adminUserLib->getErrorCode())) {
                    $responseContent['errors'] = array(
                        "code" => $adminUserLib->getErrorCode(),
                        "title" => $rupaResponse->getResponseDescription($adminUserLib->getErrorCode()),
                        "messages" => $adminUserLib->getErrorMessages()
                    );
                }

                $responseContent['messages'] = array("result" => "data not found","recordsTotal" => 0);
                $responseContent['data'] = array();
            } else {
                $responseContent['errors'] = array();
                $responseContent['messages'] = array("result" => "success","recordsTotal" => $allUsers['recordsTotal']);
                $responseContent['data'] = $allUsers['list'];
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get admin user failed, please contact ruparupa tech support"
            );
            $responseContent['messages'] = array("result" => "failed");
            $responseContent['data'] = array();
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getAdminUserCustomize()
    {
        $adminUserModel = new \Models\AdminUser();
        $params = $this->params;

        $data = $adminUserModel->getDataCustomize($params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['messages'] = array("success");
        $responseContent['errors'] = array();
        $responseContent['data'] = $data;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getAdminUserData()
    {
        /*
         * params: admin_user_id
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $adminUserLib = new \Library\AdminUser();
            $admin_user_id = !empty($this->params['admin_user_id']) ? $this->params['admin_user_id'] : "";
            $master_app_id = !empty($this->params['master_app_id']) ? $this->params['master_app_id'] : "";

            $adminUserData = $adminUserLib->getAdminUserData($admin_user_id, $master_app_id);

            if(!empty($adminUserLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $adminUserLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($adminUserLib->getErrorCode()),
                    "messages" => $adminUserLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $adminUserData;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get admin user failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveAdminUserCustomize()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array(
            'errors' => array(),
            'data' => array(),
            'messages' => array()
        );

        $params = $this->params;
        $masterAppId = $params['master_app_id'];
        $attribute_value = $params['attribute_value'];
        $adminUserId = $params['admin_user_id'];

        /**
         * ToDo : Insert new admin user
         */
        unset($params['master_app_id']);
        unset($params['attribute_value']);
        if(empty($adminUserId)){
            unset($params['admin_user_id']);
        }

        $adminUserModel = new \Models\AdminUser();
        $adminUserModel->setFromArray($params);

        $error = $adminUserModel->saveData('admin_user');

        if(!$error){
            $responseContent['errors'] = array("code" => "RR301",
                "title" => $rupaResponse->getResponseDescription("RR301"),
                "messages" => $error);
            $responseContent['messages'] = array('failed save your data');
        }else{

            $adminUserId = $adminUserModel->getAdminUserId();

            foreach($attribute_value as $row){

                $attributeValueModel = new \Models\AdminUserAttributeValue();

                $row['master_app_id'] = $masterAppId;
                $row['admin_user_id'] = $adminUserId;

                $attributeValueModel->setFromArrayCustomize($row);
                $dataAttributeValue = $attributeValueModel->findFirst(" 
                    admin_user_id = ".$adminUserId." and 
                    master_app_id = ".$masterAppId." and 
                    admin_user_attribute_id = ".$attributeValueModel->getAdminUserAttributeId());

                if($dataAttributeValue){
                    $attributeValueModel->setAdminUserAttributeValueId($dataAttributeValue->toArray()['admin_user_attribute_value_id']);
                }

                $attributeValueModel->saveData('admin_user_attribute_value');
            }

            $responseContent['messages'] = array('success save your data');

        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveAdminUser()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $this->adminUser->setFromArray($this->params);
            $this->adminUser->saveData("admin_user");
            if(!empty($this->adminUser->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $this->adminUser->getErrorCode() ,
                    "title" => $rupaResponse->getResponseDescription($this->adminUser->getErrorCode()),
                    "message" => $this->adminUser->getErrorMessages());
            } else {
                $responseContent['messages'] = "success";
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Save admin user failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }    

    public function getListApps()
    {
        /*
         * can use header params: admin_user_id, email, firstname, order_by
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $adminUserLib = new \Library\AdminUser();
            $allApps = $adminUserLib->getListApps($this->params);

            if(!empty($adminUserLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $adminUserLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($adminUserLib->getErrorCode()),
                    "messages" => $adminUserLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $allApps;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get application list failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}