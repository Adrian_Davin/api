<?php

namespace Controllers;

use Helpers\LogHelper;
/**
 * Class ControllerNameController
 * @package Controllers
 */
class InvoiceController extends \Controllers\BaseController
{

    /**
     * @var $customerAddress \Models\SalesInvoice
     */
    protected $salesInvoice;
    protected $libInvoice;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->salesInvoice = new \Models\SalesInvoice();
    }

    public function getInvoices()
    {
        $invoiceLib = new \Library\Invoice();
        $allInvoice = $invoiceLib->getAllInvoice($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (empty($allInvoice['list']) and  $allInvoice['recordsTotal'] == 0) {
            $response['errors'] = array(
                "code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Invoice not found"
            );
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allInvoice['list'];
            $response['messages'] = array("result" => "success", "recordsTotal" => $allInvoice['recordsTotal']);
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getDetailInvoiceLabel()
    {
        $invoiceLib = new \Library\Invoice();
        $invoice = $invoiceLib->getDetailInvoiceLabel($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($invoiceLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "messages" => $invoiceLib->getErrorMessages()
            );
        } else {
            $responseContent['messages'] = "success";
            $responseContent['data'] = $invoice;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    // For invoice dashboard in oms
    public function getInvoicesCustom()
    {
        $invoiceLib = new \Library\Invoice();
        $allInvoice = $invoiceLib->getAllInvoiceCustom($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (empty($allInvoice['list'])) {
            $response['errors'] = array(
                "code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Invoice not found"
            );
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allInvoice['list'];
            $response['messages'] = array("result" => "success", "recordsTotal" => $allInvoice['recordsTotal']);
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    // For invoice dashboard in oms
    public function getInvoicesTotalDc()
    {
        $invoiceLib = new \Library\Invoice();
        $allInvoice = $invoiceLib->getAllInvoiceTotalDC($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($invoiceLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "messages" => $invoiceLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allInvoice;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getInvoiceDetail()
    {
        $invoiceLib = new \Library\Invoice();
        $invoiceData = $invoiceLib->getInvoiceDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (empty($invoiceData)) {
            $response['errors'] = array(
                "code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Invoice not found"
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $invoiceData;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function createInvoice()
    {
        $invoiceLib = new \Library\Invoice();
        $order_no = !empty($this->params['order_no']) ? $this->params['order_no'] : "";
        \Helpers\LogHelper::log('debug_625', 'Order NO: '. $order_no, 'debug');
        \Helpers\LogHelper::log('debug_625', '/app/Controller/InvoiceController.php line 152', 'debug');
        $invoiceLib->createInvoice($order_no, $this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($invoiceLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "messages" => $invoiceLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function releaseCashbackAbuser()
    {
        $invoiceLib = new \Library\Invoice();        
        $invoiceLib->releaseCashbackAbuser($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($invoiceLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "messages" => $invoiceLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function unreleaseCashbackAbuser()
    {
        $invoiceLib = new \Library\Invoice();        
        $invoiceLib->unreleaseCashbackAbuser($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($invoiceLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "messages" => $invoiceLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateInvoice()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $invoiceLib = new \Library\Invoice();
        // validate params
        $invoiceLib->prepareInvoiceUpdate($this->params);
        if (empty($invoiceLib->getErrorCode())) {
            $invoiceLib->updateData();
            if (empty($invoiceLib->getErrorCode())) {
                $response['messages'] = "success";
            } else {
                $response['errors'] = array(
                    "code" => $invoiceLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                    "message" => $invoiceLib->getErrorMessages()
                );
            }
        } else {
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "message" => $invoiceLib->getErrorMessages()
            );
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updatePickupVoucher()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $invoiceLib = new \Library\Invoice();
        // validate params
        $invoiceLib->prepareInvoiceUpdatePicupVoucher($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (empty($invoiceLib->getErrorCode())) {
            if (empty($invoiceLib->getErrorCode())) {
                $response['messages'] = "success";
            } else {
                $response['errors'] = array(
                    "code" => $invoiceLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                    "message" => $invoiceLib->getErrorMessages()
                );
            }
        } else {
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "message" => $invoiceLib->getErrorMessages()
            );
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function regeneratePickupVoucher()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $invoiceLib = new \Library\Invoice();
        $invoiceLib->processRegeneratePickupVoucher($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (empty($invoiceLib->getErrorCode())) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $response['messages'] = "success";
        } else {
            $rupaResponse->setStatusCode(500, "Ok")->sendHeaders();
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "message" => $invoiceLib->getErrorMessages()
            );
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateSoSapNumber()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $invoiceLib = new \Library\Invoice();
        // validate params
        $invoiceLib->prepareUpdateSoSapNumber($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if (empty($invoiceLib->getErrorCode())) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            if (empty($invoiceLib->getErrorCode())) {
                $response['messages'] = "success";
            } else {
                $response['errors'] = array(
                    "code" => $invoiceLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                    "message" => $invoiceLib->getErrorMessages()
                );
            }
        } else {
            $rupaResponse->setStatusCode(400, "Ok")->sendHeaders();
            $response['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => $invoiceLib->getErrorMessages()
            );
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateBatchSoNumber()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $invoiceModel = new \Models\SalesInvoice();
        $response = $invoiceModel->updateBatchSoNumber($this->params);

        $responseContent['messages'] = $response['messages'];
        $responseContent['errors'] = $response['errors'];
        $responseContent['data'] = $response['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * @deprecated
     * Use {cart_url}/marketing/invoice/{cart_id} instead
     */
    public function invoicePromoCheck()
    {
        $invoiceLib = new \Library\Invoice();
        $invoice_id = !empty($this->params['invoice_id']) ? $this->params['invoice_id'] : "";
         \Helpers\LogHelper::log('debug_625', '/app/controller/InvoiceController.php line 389', 'debug');
        $invoiceLib->checkPromotion($invoice_id);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($invoiceLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "messages" => $invoiceLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function checkEmptySoSap()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $invoiceModel = new \Models\SalesInvoice();
        $response = $invoiceModel->checkEmptySoSap();

        $responseContent['messages'] = $response['messages'];
        $responseContent['errors'] = $response['errors'];
        $responseContent['data'] = $response['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getInvoiceItem()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $invoiceItemModel = new \Models\SalesInvoiceItem();
        $response = $invoiceItemModel->getInvoiceItem($this->params);

        $responseContent['messages'] = $response['messages'];
        $responseContent['errors'] = $response['errors'];
        $responseContent['data'] = $response['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateInvoiceStatus()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $invoice_id = $this->params['invoice_id'];
        $status = $this->params['invoice_status'];
        $admin_id = $this->params['admin_user_id'];

        if (empty($invoice_id) || empty($status) || empty($admin_id)) {
            $rupaResponse->setStatusCode(400, "Ok")->sendHeaders();
            $responseContent['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => "Missing required parameters"
            );
            $rupaResponse->setContent($responseContent);
            $rupaResponse->send();
        }

        try {
            $invoiceModel = new \Models\SalesInvoice();
            $invoiceModel->useReadOnlyConnection();
            $invoiceData = $invoiceModel->findFirst(
                array(
                    'conditions' => ' invoice_id = '.$invoice_id,
                    'columns' => 'invoice_id, invoice_no, invoice_status, sales_order_id'
                )
            )->toArray();

            $query = "SELECT shipment_id FROM sales_shipment WHERE invoice_id = ".$invoice_id;
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);
            $shipment_id = $result->fetchAll()[0]['shipment_id'];

            $query = "UPDATE sales_invoice SET invoice_status = '$status', sla_age = TIMESTAMPDIFF(MINUTE,created_at,NOW()) WHERE invoice_id = $invoice_id";
    
            $invoiceModel->useWriteConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);
    
            $query = "UPDATE sales_shipment SET shipment_status = '$status' WHERE invoice_id = $invoice_id";
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);

            $paramLog['sales_order_id'] = $invoiceData['sales_order_id'];
            $paramLog['invoice_id'] = $invoice_id;
            $paramLog['shipment_id'] = $shipment_id;
            $paramLog['admin_user_id'] = $admin_id;
            $paramLog['supplier_user_id'] = 0;
            $paramLog['actions'] = "update shipment status #{$invoiceData['invoice_no']} to {$status}";
            $paramLog['affected_field'] = "shipment_status";
            $paramLog['value'] = $status;
            $paramLog['log_time'] = date("Y-m-d H:i:s");
            $paramLog['flag'] = "dc_dashboard";
            $salesLogModel = new \Models\SalesLog();
            $salesLogModel->setFromArray($paramLog);
            $salesLogModel->saveData("sales_log");

            $paramLog['actions'] = "update invoice status #{$invoiceData['invoice_no']} to {$status}";
            $paramLog['affected_field'] = "invoice_status";

            $salesLogModel = new \Models\SalesLog();
            $salesLogModel->setFromArray($paramLog);
            $salesLogModel->saveData("sales_log");
        } catch (\Exception $e) {
            $rupaResponse->setStatusCode(400, "Ok")->sendHeaders();
            $responseContent['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => $e->getMessage()
            );

            $rupaResponse->setContent($responseContent);
        }
        $rupaResponse->send();
    }

    public function stopInvoiceSLA()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $invoice_id = $this->params['invoice_id'];

        if (empty($invoice_id)) {
            $rupaResponse->setStatusCode(400, "Ok")->sendHeaders();
            $responseContent['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => "Missing required parameters"
            );
            $rupaResponse->setContent($responseContent);
            $rupaResponse->send();
        }

        try {
            $invoiceModel = new \Models\SalesInvoice();
            $invoiceLib = new \Library\Invoice();
            $invoiceModel->useWriteConnection();

            $query = "UPDATE sales_invoice SET sla_age = TIMESTAMPDIFF(MINUTE,created_at,NOW()) WHERE invoice_id = $invoice_id";
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);

            $query = "SELECT invoice_no AS result FROM sales_invoice WHERE invoice_id = $invoice_id";
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query)->fetchAll();

            if (count($result) > 0) {
                $invoice_no = $result[0]['result'];
    
                $invoiceLib->InsertSLADC($invoice_no, 1, "order");
            }
        } catch (\Exception $e) {
            LogHelper::log("stopInvoiceSLA", "invoice_id: {$invoice_id} error: {$e->getMessage()}", "error");
            $rupaResponse->setStatusCode(400, "Ok")->sendHeaders();
            $responseContent['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => $e->getMessage()
            );

            $rupaResponse->setContent($responseContent);
        }
        $rupaResponse->send();
    }

    public function updateInvoiceItem()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $invoiceItemModel = new \Models\SalesInvoiceItem();
        $invoiceItemModel->setFromArray($this->params);
        $invoiceItemModel->saveData('sales_invoice_item');
        if (!empty($invoiceItemModel->getErrors())) {
            $responseContent['errors'] = array("code" => "RR302", "title" => $rupaResponse->getResponseDescription("RR302"), "messages" => $invoiceItemModel->getErrors());
            $responseContent['messages'] = array("failed");
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        // check is gosend, is all item in shipment already complete & check express_courier_state
        // $salesShipmentModel = new \Models\SalesShipment();
        // $salesShipmentModel->gosendBooking($this->params['invoice_item_id']);

        // check if all item incomplete
        $shopeeLib = new \Library\Shopee();
        $shopeeLib->checkAllIncomplete($this->params);

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateReceiptId()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $invoiceLib = new \Library\Invoice();
        // validate params
        $invoiceLib->updateReceiptId($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (!empty($invoiceLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $invoiceLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                "message" => $invoiceLib->getErrorMessages()
            );
            $response['messages'] = $invoiceLib->getErrorMessages();
        } else {
            $response['messages'] = "success";
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getListVoucher()
    {
        $invoiceLib = new \Library\Invoice();
        $getVoucherList = $invoiceLib->getVoucherList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");


        if (empty($getVoucherList)) {
            $response['errors'] = array(
                "code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "No Voucher Found"
            );
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $getVoucherList;
            $response['messages'] = array("result" => "success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getReceiptId()
    {
        $invoiceLib = new \Library\Invoice();
        $getReceiptId = $invoiceLib->getReceiptId($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if (empty($this->params)) {
            $response['errors'] = array(
                "code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Voucher Not Found"
            );
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $getReceiptId;
            $response['messages'] = array("result" => "success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getReceiptLog()
    {
        $invoiceLib = new \Library\Invoice();
        $getReceiptId = $invoiceLib->getReceiptLog($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if (empty($this->params)) {
            $response['errors'] = array(
                "code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Voucher Not Found"
            );
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $getReceiptId;
            $response['messages'] = array("result" => "success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updatePosVoucher()
    {
        $invoiceLib = new \Library\Invoice();
        $getVoucherList = $invoiceLib->updatePosVoucher($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if (empty($getVoucherList)) {
            $response['errors'] = array(
                "code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Voucher Not Found"
            );
            $response['messages'] = array("result" => "failed");
        } else {
            if ($getVoucherList == "success") {
                $response['errors'] = array();
                $response['data'] = array();
                $response['messages'] = array("result" => "success");
            } else {
                $response['errors'] = array();
                $response['data'] = array();
                $response['messages'] = array("result" => "failed");
            }
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateProductDigital()
    {
        $invoiceLib = new \Library\Invoice();
        if(!empty($this->params['sap_so_number'])){
            $updateInvoiceSapSoNumber = $invoiceLib->updateInvoiceSapSoNumber($this->params);

            $rupaResponse = new \Library\Response();
            $rupaResponse->setContentType("application/json");

            if (empty($updateInvoiceSapSoNumber)) {
                $response['errors'] = array(
                    "code" => $invoiceLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                    "messages" => $invoiceLib->getErrorMessages()
                );
                $response['messages'] = array("result" => "failed");
            } else {
                if ($updateInvoiceSapSoNumber == "success") {
                    $response['errors'] = array();
                    $response['data'] = array();
                    $response['messages'] = array("result" => "success");
                } else {
                    $response['errors'] = array();
                    $response['data'] = array();
                    $response['messages'] = array("result" => "failed");
                }
            }

        } else if (!empty($this->params['card_no'])){
            $updateMemberNumber = $invoiceLib->updateMemberNumber($this->params);

            $rupaResponse = new \Library\Response();
            $rupaResponse->setContentType("application/json");

            if (empty($updateMemberNumber)) {
                $response['errors'] = array(
                    "code" => $invoiceLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($invoiceLib->getErrorCode()),
                    "messages" => $invoiceLib->getErrorMessages()
                );
                $response['messages'] = array("result" => "failed");
            } else {
                if ($updateMemberNumber == "success") {
                    $response['errors'] = array();
                    $response['data'] = array();
                    $response['messages'] = array("result" => "success");
                } else {
                    $response['errors'] = array();
                    $response['data'] = array();
                    $response['messages'] = array("result" => "failed");
                }
            }
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("result" => "failed");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function membershipSlackNotification(){
        $notificationText = "Upgrade membership failed : ".$this->params['invoice_no']." , Error : ".$this->params['response']."";
        $params = [ 
            "channel" => $_ENV['MEMBERSHIP_SLACK_CHANNEL'],
            "username" => $_ENV['MEMBERSHIP_SLACK_USERNAME'],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];

        $nsq->publishCluster('transaction', $message);
    }
}
