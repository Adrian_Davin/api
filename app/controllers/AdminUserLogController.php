<?php


namespace Controllers;

class AdminUserLogController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    /**
     * TODO Maybe Validation
     */
    public function createUserLog(){
        $userLogModel = new \Models\AdminUserLog();

        $userLogModel->setUserLogFromArray($this->params);
        $userLogModel->create();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

            $responseContent['messages'] = array("success");
            $responseContent['data'] = "";

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function userLogList(){
        $userLogLib = new \Library\AdminUserLog();
        $logLists = $userLogLib->getAllUserLog();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($userLogLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $userLogLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($userLogLib->getErrorCode()),
                "messages" => $userLogLib->getErrorMessages()
            );
            $responseContent['messages'] = array("user log not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $logLists;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function userLogDetails(){
        $userLogLib = new \Library\AdminUserLog();
        $logDetails = $userLogLib->getLogDetails($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($userLogLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $userLogLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($userLogLib->getErrorCode()),
                "messages" => $userLogLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to get data");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $logDetails;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}