<?php


namespace Controllers;


/**
 * Class OrderController
 * @package Controllers
 * @RoutePrefix("/salesordertp")
 *
 */

class SalesOrderTpController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function saveStockToMysql(){

        $this->params["message"] = str_replace("'","\'",$this->params["message"]);

        $sql = "SELECT COUNT(odi) as totalOdi, sku FROM sales_order_tp where odi ='".$this->params['odi']."' and sku = '".$this->params['sku']."' AND source = '".$this->params['source']."' group by sku";
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );

        $totalRow = $result->fetchAll()[0]['totalOdi'];
        $date = date("Y-m-d H:i:s");
        if($totalRow > 0){
            $sql = "UPDATE sales_order_tp SET retry_count = retry_count + 1, sap_no = '".$this->params["mblnr"]."', odi = '".$this->params["odi"]."', sku = '".$this->params["sku"]."', store_code = '".$this->params["store_code"]."', qty = ".$this->params["qty"].", status = '".$this->params["status"]."', source = '".$this->params["source"]."', message = '".$this->params["message"]."',updated_at ='".$date."' where odi = '".$this->params["odi"]."' AND sku = '".$this->params["sku"]."' AND source = '".$this->params["source"]."'";
            $this->getDi()->getShared('dbMaster')->execute($sql);
        }else{
            $sql = "insert into sales_order_tp (sap_no, odi, sku, store_code, qty, status, source, message,created_at, updated_at) values ";
            $valueString = "('".$this->params["mblnr"]."','".$this->params["odi"]."','".$this->params["sku"]."','".$this->params["store_code"]."','".$this->params["qty"]."','".$this->params["status"]."','".$this->params["source"]."','".$this->params["message"]."','".$date."','".$date."')";
            $sql = $sql . $valueString;
            $this->getDi()->getShared('dbMaster')->execute($sql);
        }

        $sql = "INSERT INTO sales_order_tp_log
                (order_no, sku, status, message)
                VALUES('{$this->params["odi"]}', '{$this->params["sku"]}', '{$this->params["status"]}', '{$this->params["message"]}');";
        $this->getDi()->getShared('dbMaster')->execute($sql);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createRevertTp(){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");

        if(isset($this->params['qty_ordered'])){
            $apiWrapper = new \Library\APIWrapper(getenv('STOCK_V2_API'));
            $endPoint = 'legacy/stock/'.$this->params['sku'] . $this->params['store_code'];
            $apiWrapper->setEndPoint($endPoint);

            if ($apiWrapper->send("get")) {
                $apiWrapper->formatResponse();
            };
            if (!empty($apiWrapper->getError())) {
                $responseContent['errors'] = array(
                    "code" => "RR302",
                    "title" => $rupaResponse->getResponseDescription("RR302"),
                    "messages" => "Failed to get data from product stock"
                );
                $responseContent['messages'] = array("Failed");
                $responseContent['data'] = array();
            } else {
                $stock1000Detail = $apiWrapper->getData();

                $stock1000Detail['store_code_detail'] = str_replace('DC', '', $stock1000Detail['store_code_detail']);

                $data = array(
                    "sku" => $this->params['sku'],
                    "store_code" => $stock1000Detail['store_code_detail'],
                    "order_no" => $this->params['order_no'],
                    "qty" => $this->params['qty_ordered'],
                    "message" => "createOrderTp",
                    "type" => "revert",
                    "created_date" => date("Y-m-d H:i:s")
                );

                $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
                $valueString = "('".$this->params["order_no"]."','".$this->params["sku"]."','".$stock1000Detail['store_code_detail']."','".$this->params['qty_ordered']."','process','Revert TP','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')";
                $sql = $sql . $valueString;
                $this->getDi()->getShared('dbMaster')->execute($sql);
                // insert into queue
                $nsq = new \Library\Nsq();
                $nsq->publishCluster('transaction1021', $data);
            }
        }else{
            // $salesOrderCheck = new \Models\SalesOrder;
            // $searchResult = $salesOrderCheck::findFirst("order_no = '".$this->params['order_no']."'");
            // $salesOrderCheck->assign($searchResult->toArray([]));
            // $salesOrderId = $salesOrderCheck->getSalesOrderId();
            
            $sql = "
            SELECT 
                scmi.sku, scmi.qty_refunded, soi.store_code
            FROM
                sales_credit_memo scm
                    INNER JOIN
                sales_credit_memo_item scmi ON scm.credit_memo_id = scmi.credit_memo_id
                    INNER JOIN
                sales_order_item soi ON soi.sales_order_item_id = scmi.sales_order_item_id
            WHERE
                scm.order_no = '".$this->params['order_no']."' and 
                scmi.sku = '".$this->params['sku']."'
            ";
            $result = $this->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $revertDatas = $result->fetchAll();

            foreach($revertDatas as $revertData){
                $apiWrapper = new \Library\APIWrapper(getenv('STOCK_V2_API'));
                $endPoint = 'legacy/stock/'.$revertData['sku'] . $revertData['store_code'];
                $apiWrapper->setEndPoint($endPoint);
        
                if ($apiWrapper->send("get")) {
                    $apiWrapper->formatResponse();
                };

                $stock1000Detail = $apiWrapper->getData();

                $stock1000Detail['store_code_detail'] = str_replace('DC', '', $stock1000Detail['store_code_detail']);

                $qty = (float)$revertData['qty_refunded'];
                $data = array();
                $data = array(
                    "sku" => $revertData['sku'],
                    "store_code" => $stock1000Detail['store_code_detail'],
                    "order_no" => $this->params['order_no'],
                    "qty" => $qty,
                    "message" => "createOrderTp",
                    "type" => "revert",
                    "created_date" => date("Y-m-d H:i:s")
                );
                $skuSearch = $revertData['sku'];

                $sql = "SELECT COUNT(odi) as totalOdi, sku FROM sales_order_tp where odi ='".$this->params['order_no']."' and sku = '".$skuSearch."' AND source = 'Revert TP' group by sku";
                $result = $this->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );

                $totalRow = $result->fetchAll()[0]['totalOdi'];
                if($totalRow > 0){
                    $sql = "UPDATE sales_order_tp SET odi = '".$this->params["order_no"]."', sku = '".$skuSearch."', store_code = '".$stock1000Detail['store_code_detail']."', qty = ".$qty.", status = 'process', message = '',updated_at ='".date("Y-m-d H:i:s")."' where odi = '".$this->params["order_no"]."' AND sku = '".$skuSearch."' AND source = 'Revert TP'";
                    $this->getDi()->getShared('dbMaster')->execute($sql);
                }else{
                    $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
                    $valueString = "('".$this->params["order_no"]."','".$this->params["sku"]."','".$stock1000Detail['store_code_detail']."','".$qty."','process','Revert TP','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')";
                    $sql = $sql . $valueString;
                    $this->getDi()->getShared('dbMaster')->execute($sql);
                }
                // insert into queue
                $nsq = new \Library\Nsq();
                $nsq->publishCluster('transaction1021', $data);
            }
            
            
            // $apiWrapper = new \Library\APIWrapper(getenv('STOCK_API'));
            // $endPoint = $revertData[0]['sku'] . "1000DC";
            // $apiWrapper->setEndPoint($endPoint);
    
            // if ($apiWrapper->send("get")) {
            //     $apiWrapper->formatResponse();
            // };
    
            // $stock1000Detail = $apiWrapper->getData();
            // $qty = (float)$revertData[0]['qty_refunded'];
    
            // $data = array(
            //     "sku" => $revertData[0]['sku'],
            //     "store_code" => $stock1000Detail['store_code_detail'],
            //     "order_no" => $this->params['order_no'],
            //     "qty" => $qty,
            //     "message" => "createOrderTp",
            //     "type" => "revert",
            //     "created_date" => date("Y-m-d H:i:s")
            // );
            // $skuSearch = $revertData[0]['sku'];

            // $sql = "SELECT COUNT(odi) as totalOdi, sku FROM sales_order_tp where odi ='".$this->params['order_no']."' and sku = '".$skuSearch."' AND source = 'Revert TP' group by sku";
            // $result = $this->getDi()->getShared('dbMaster')->query($sql);
            // $result->setFetchMode(
            //     \Phalcon\Db::FETCH_ASSOC
            // );
    
            // $totalRow = $result->fetchAll()[0]['totalOdi'];
            // if($totalRow > 0){
            //     $sql = "UPDATE sales_order_tp SET odi = '".$this->params["order_no"]."', sku = '".$skuSearch."', store_code = '".$stock1000Detail['store_code_detail']."', qty = ".$qty.", status = 'process', message = '',updated_at ='".date("Y-m-d H:i:s")."' where odi = '".$this->params["order_no"]."' AND sku = '".$skuSearch."' AND source = 'Revert TP'";
            //     $this->getDi()->getShared('dbMaster')->execute($sql);
            // }else{
            //     $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
            //     $valueString = "('".$this->params["order_no"]."','".$this->params["sku"]."','".$stock1000Detail['store_code_detail']."','".$qty."','process','Revert TP','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')";
            //     $sql = $sql . $valueString;
            //     $this->getDi()->getShared('dbMaster')->execute($sql);
            // }



           

        }

        // insert into queue
        // $nsq = new \Library\Nsq();
        // $nsq->publishCluster('transaction1021', $data);

        $responseContent['messages'] = array('success');

        $responseContent['data'] = $data;


        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }


    public function recreateStockTp()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");

        $orderCheck = $this->params['order_no'];
        $skuCheck = $this->params['sku'];

        $sql = "SELECT odi, sku, store_code, qty FROM sales_order_tp where ODI ='".$orderCheck."' AND source = 'Create TP' AND status='failed' AND sku = '".$skuCheck."'";
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $listeners = $result->fetchAll();

        $sql = "UPDATE sales_order_tp SET status = 'process' where odi = '".$orderCheck."' AND source = 'Create TP' AND status='failed' AND sku = '".$skuCheck."' ";
        $this->getDi()->getShared('dbMaster')->execute($sql);

        foreach($listeners as $listener){
            $qty = (float)$listener['qty'];

            $listener['store_code'] = str_replace('DC', '', $listener['store_code']);

            $payload = array(
                "sku" => $listener['sku'],
                "store_code" => $listener['store_code'],
                "order_no" => $listener['odi'],
                "qty" => $qty,
                "message" => "createOrderTp",
                "type" => "stock",
                "created_date" => date("Y-m-d H:i:s")
            );
            //insert into queue
            $nsq = new \Library\Nsq();
            $nsq->publishCluster('transaction1021', $payload);
        }
        
        $responseContent['data'] = [];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function checkExistTp(){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");

        $orderCheck = $this->params['order_no'];

        if($this->params['sku'] == ""){
            $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status = 'success'";
        }else{
            $sku = $this->params['sku'];
            if($this->params['type'] == "recreate"){
                $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status = 'process' AND sku = '".$sku."' AND source = 'Create TP'";
            }else if($this->params['type'] == 'revert'){
                $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status = 'process' AND sku = '".$sku."' AND source = 'Revert TP'";
            }else if($this->params['type'] == 'revertSuccess'){
                $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status = 'success' AND sku = '".$sku."' AND source = 'Revert TP'";
            }else if($this->params['type'] == 'revertExists'){
                $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status = 'success' AND sku = '".$sku."' AND source = 'Create TP'";
                $result = $this->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $totalRowTP = $result->fetchAll()[0]['totalOdi'];
                // echo $totalRowTP;
                // exit;
                if ($totalRowTP > 0){
                    $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status in ('process','success') AND sku = '".$sku."' AND source = 'Revert TP'";
                }else{
                    //dirty fix : to set so total row get for revert automatic is > 0 
                    $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status in ('process','failed') AND sku = '".$sku."' AND source = 'Create TP'";
                }
            }else if($this->params['type'] == "revertManualExists"){
                $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."'  AND sku = '".$sku."' AND source = 'Revert TP'";
                $result = $this->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $totalRowTP = $result->fetchAll()[0]['totalOdi'];
                if($totalRowTP > 0){
                    $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status in ('process','failed') AND sku = '".$sku."' AND source = 'Revert TP'";
                }else{
                    $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status = 'success' AND sku = '".$sku."' AND source = 'Create TP'";
                }
                // $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status = 'success' AND sku = '".$sku."' AND source = 'Create TP'";
                // $result = $this->getDi()->getShared('dbMaster')->query($sql);
                // $result->setFetchMode(
                //     \Phalcon\Db::FETCH_ASSOC
                // );
                // $totalRowTP = $result->fetchAll()[0]['totalOdi'];
                // if ($totalRowTP > 0){
                //     $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='".$orderCheck."' AND status = 'success' AND sku = '".$sku."' AND source = 'Revert TP'";
                // }
            }

        }

        

        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );

        $totalRow = $result->fetchAll()[0]['totalOdi'];

        $responseContent['data']['affected_row'] = $totalRow;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function getSalesOrderTpDetail(){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");

        $orderCheck = $this->params['order_no'];

        $sql = "SELECT store_code,sap_no,status, updated_at, sku, source, message, retry_count FROM sales_order_tp where ODI ='".$orderCheck."'";
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $totalRow = $result->fetchAll();
        $responseContent['data'] = $totalRow;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
    public function checkOrderStatus(){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");

        $salesOrder = new \Models\SalesOrder;
        $searchResult = $salesOrder::findFirst("order_no = '".$this->params['order_no']."'");
        $salesOrder->assign($searchResult->toArray([]));
        $salesOrderId = $salesOrder->getSalesOrderId();

        $sql = "SELECT status FROM sales_order_payment where sales_order_id ='".$salesOrderId."'";
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $status = $result->fetchAll();
    
        $responseContent['data']['status'] = $status[0]['status'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function checkInvoiceStatus(){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");

        $salesOrder = new \Models\SalesOrder;
        $searchResult = $salesOrder::findFirst("order_no = '".$this->params['order_no']."'");
        $salesOrder->assign($searchResult->toArray([]));
        $salesOrderId = $salesOrder->getSalesOrderId();

        $sql = "
            select a.invoice_status,b.sku
            from (
                select *
                from sales_invoice
                where sales_order_id = '".$salesOrderId."'
            ) as a inner join sales_invoice_item as b
            on a.invoice_id = b.invoice_id
            where b.status_fulfillment = 'refunded'
        ";
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $status = $result->fetchAll();
    
        $responseContent['data'] = $status;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createSlackNotification(){
        $notificationText = "Alert : TP tidak terbentuk untuk order no : ".$this->params['order_no']." , sku : ".$this->params['sku'].", Response Body : ".$this->params['response_body']."";
        $params = [ 
            "channel" => $_ENV['OPS_SLACK_CHANNEL'],
            "username" => $_ENV['OPS_SLACK_USERNAME'],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    public function getErrorTpData(){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");

        $sql = "
        select distinct odi as odi
        from sales_order_tp
        where odi not in(
            select distinct a.odi as odi
            from (
                select a.*,b.order_no, b.sales_order_id
                from (
                    select odi
                    FROM sales_order_tp 
                    where source = 'Create TP' AND status in ('failed', 'process')
                )as a inner join sales_order as b
                on a.odi = b.order_no   
            )as a inner join (
                select *
                from sales_invoice
                where invoice_status = 'full_refund'
            )as b 
            on a.sales_order_id = b.sales_order_id
        ) and source = 'Create TP' 
        AND status in ('failed', 'process')
        order by sales_order_tp.created_at desc
        LIMIT 10
        ";
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $errorDatas = $result->fetchAll();

        $responseContent['data'] = $errorDatas;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function countErrorTp(){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $responseContent['messages'] = array("success");

        $sql = "
        select count(distinct odi) as totalOdi
        from sales_order_tp
        where odi not in(
            select distinct a.odi as odi
            from (
                select a.*,b.order_no, b.sales_order_id
                from (
                    select odi
                    FROM sales_order_tp 
                    where source = 'Create TP' AND status in ('failed', 'process')
                )as a inner join sales_order as b
                on a.odi = b.order_no   
            )as a inner join (
                select *
                from sales_invoice
                where invoice_status = 'full_refund'
            )as b 
            on a.sales_order_id = b.sales_order_id
        ) and source = 'Create TP' 
        AND status in ('failed', 'process')
        ";
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );

        $totalRow = $result->fetchAll()[0]['totalOdi'];

        $responseContent['data'] = $totalRow;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}
