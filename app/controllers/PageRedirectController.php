<?php
/**
 * Created by PhpStorm.
 * User: iwan
 * Date: 24/05/18
 * Time: 10:04
 */

namespace Controllers;


class PageRedirectController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function createPageRedirect(){
        $pageRedirectModel = new \Models\PageRedirect();
        $this->params['status'] = 10;
        $pageRedirectModel->setFromArray($this->params);

        $pageRedirectModel->saveData('create_page_redirect');
        $pageRedirectModel->afterUpdate();
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($pageRedirectModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $pageRedirectModel->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($pageRedirectModel->getErrorCode()),
                "messages" => $pageRedirectModel->getErrorMessages());
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = "Create Page Redirect Success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function pageRedirectList(){
        $pageRedirectLib = new \Library\PageRedirect();
        $pageRedirectList = $pageRedirectLib->getAllPageRedirect($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($pageRedirectLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $pageRedirectLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($pageRedirectLib->getErrorCode()),
                "messages" => $pageRedirectLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to get page redirect list");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $pageRedirectList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();


    }

    public function pageRedirectDetail(){
        $pageRedirectLib = new \Library\PageRedirect();
        $pageRedirectData = $pageRedirectLib->getDetailPageRedirect($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($pageRedirectLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $pageRedirectLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($pageRedirectLib->getErrorCode()),
                "messages" => $pageRedirectLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get page redirect details");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $pageRedirectData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updatePageRedirect(){
        $pageRedirectModel = new \Models\PageRedirect();
        unset($this->params['company_code']);
        $pageRedirectModel->setFromArray($this->params);
        $pageRedirectModel->saveData('update_page_redirect');
        $pageRedirectModel->afterUpdate();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($pageRedirectModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $pageRedirectModel->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($pageRedirectModel->getErrorCode()),
                "messages" => $pageRedirectModel->getErrorMessages());
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = "Update Page Redirect Success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }
}