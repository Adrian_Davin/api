<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/17/2017
 * Time: 9:31 AM
 */

namespace Controllers;

use Helpers\LogHelper;
use Helpers\ProductHelper;
use Models\ProductCategory;

class CategoryController extends \Controllers\BaseController
{
    public function getGeneratedURl()
    {
        $categoryLib = new \Library\Category();
        $categoryList = $categoryLib->getGeneratedURl($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        /*
         * @todo: refactor the whole mechanism of properly passing error responses
         */
        if ($categoryList) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $categoryList;
        }
        else {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $errors = new \stdClass();
            $errors->code = 404;
            $errors->messages = ['category not found'];
            $responseContent['messages'] = [];
            $responseContent['errors'] = $errors;
            $responseContent['data'] = $categoryList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getAllCategories()
    {
        $categoryLib = new \Library\Category();
        $categoryList = $categoryLib->getAllCategories($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        /*
         * @todo: refactor the whole mechanism of properly passing error responses
         */
        if ($categoryList) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $categoryList;
        }
        else {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $errors = new \stdClass();
            $errors->code = 404;
            $errors->messages = ['category not found'];
            $responseContent['messages'] = [];
            $responseContent['errors'] = $errors;
            $responseContent['data'] = $categoryList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getPairingProductCategory()
    {
        $categoryLib = new \Library\Category();
        $categoryList = $categoryLib->getPairingProductCategory($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        /*
         * @todo: refactor the whole mechanism of properly passing error responses
         */
        if ($categoryList) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $categoryList;
        }
        else {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $errors = new \stdClass();
            $errors->code = 404;
            $errors->messages = ['category not found'];
            $responseContent['messages'] = [];
            $responseContent['errors'] = $errors;
            $responseContent['data'] = $categoryList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    // not maintained
    public function categoryList()
    {
        $categoryLib = new \Library\Category();
        $categoryList = $categoryLib->getCategoryList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        /*
         * @todo: refactor the whole mechanism of properly passing error responses
         */
        if ($categoryList) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $categoryList;
        }
        else {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $errors = new \stdClass();
            $errors->code = 404;
            $errors->messages = ['category not found'];
            $responseContent['messages'] = [];
            $responseContent['errors'] = $errors;
            $responseContent['data'] = $categoryList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function categoryTree()
    {
        $categoryLib = new \Library\Category();
        $categoryTree = $categoryLib->getCategoryTree($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['messages'] = array("success");
        $responseContent['errors'] = array();
        $responseContent['data'] = $categoryTree;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function generateCategoryTree(){
        // forced to remake the category tree
        $categoryLib = new \Library\Category();
        // $deleteStatus = $categoryLib->deleteCategoryTree();
        $deleteStatus = true;
        $companyCode = (isset($this->params['company_code']) && !empty($this->params['company_code'])) ? $this->params['company_code'] : 'ODI';
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if($deleteStatus){
            $categoryModel = new ProductCategory();
            $categoryModel->afterUpdate();

            // generate for both, cms and front-end
            // in here, we need to build just for the tree frontend, the cms just clear tree corresponding data, and root data
            $categoryLib->generateCategoryTree("cms", $companyCode, 0, '');
            $categoryLib->generateCategoryTree("", $companyCode, 0, '');
            // trigger new category tree for mobile app (useragent can be ios, android or RuparupaApps)
            $categoryLib->generateCategoryTree("", $companyCode, 0, 'ios');

            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
        }
        else{
            $responseContent['messages'] = array("failed");
            $responseContent['errors'] = array("Error when try generating category tree");
            $responseContent['data'] = array();
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function categoryTreeDelete()
    {
        $categoryLib = new \Library\Category();
        $status = $categoryLib->deleteCategoryTree();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$status) {
            $responseContent['messages'] = array("false");
            $responseContent['errors'] = array("Error when try delete category tree");
            $responseContent['data'] = [];
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = [];
        }


        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function categoryDetail()
    {
        $categoryLib = new \Library\Category();
        $categoryDetail = $categoryLib->getCategoryList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($categoryLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $categoryLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($categoryLib->getErrorCode()),
                "messages" => $categoryLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $categoryDetail;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function customCategoryDetail()
    {
        $categoryLib = new \Library\Category();
        $categoryDetail = $categoryLib->customCategoryDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($categoryLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $categoryLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($categoryLib->getErrorCode()),
                "messages" => $categoryLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $categoryDetail;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function categoryPath()
    {
        $categoryLib = new \Library\Category();
        $categoryPath = $categoryLib->categoryPath($this->params['article_hierarchy']);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['messages'] = array("success");
        $responseContent['errors'] = array();
        $responseContent['data'] = $categoryPath;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    // TODO 1
    public function updateData()
    {
        // start here again - update category
        $categoryMappingModel = new \Models\ProductCategoryMappingBrand();
        $categoryMappingModel->setFromArray($this->params);

        $categoryModel = new \Models\ProductCategory();
        $categoryModel->setFromArray($this->params);
        if ($this->params['is_rule_based'] == 1) {
            $categoryModel->setFilterToJson($this->params);
        }
        $message = $categoryModel->updateData();

        
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($categoryModel->getErrors())) {
            $responseContent['errors'] = array("code" => "RR301", "title" => $rupaResponse->getResponseDescription("RR301"), "messages" => $categoryModel->getErrors());
            $responseContent['messages'] = $message;
        } else {
            $categoryLib = new \Library\Category();
            $categoryLib->deleteCategoryCache();

            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = $message;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function productTocategory(){
        // we have to change here
        $error = array();
        $message = "success";
        $data = array();
        $flag = 0;

        $p2c = new \Models\Product2Category();

        $remove = array();
        $add = array();
        $hero = array();
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent = array();

        if(isset($this->params['delete_children'])) {
            $result = $p2c->removeProductsOnly($this->params);

            $data = array('affected_rows' => $result['affected_rows'], 'details' => $result['response']);
        } else {
            if (empty($this->params['remove_product']['sku'][0]) && empty($this->params['add_product']['sku'][0]) && empty($this->params['hero_products']['sku'][0]) && empty($this->params['old_hero_products']['sku'][0])) {
                $message = array("Nothing to be added/updated");
            } else {
                if (!empty($this->params['remove_product']['sku'][0])) {
                    // we should always remove first
                    $remove = $p2c->removeDataBulk($this->params);
                    if (!empty($remove)) {
                        $data['removed'] = $remove;
                    } else {
                        $error['message'] = "Failed when trying to remove product";
                        $message = "failed";
                        $flag = 1;
                    }
                }

                if ($flag <= 0) {
                    // start with adding product first
                    if (!empty($this->params['add_product']['sku'][0])) {
                        $add = $p2c->saveDataBulk($this->params);
                        if (!empty($add)) {
                            $data['inserted'] = $add;
                        } else {
                            $error['message'] = "Failed when trying to add product";
                            $message = "failed";
                            $flag = 1;
                        }
                    }

                    if ($flag <= 0) {
                        // add and reorder hero products
                        if (!empty($this->params['hero_products']['sku'][0]) || !empty($this->params['old_hero_products']['sku'][0])) {
                            $hero = $p2c->saveHeroProducts($this->params);
                            if (!empty($hero)) {
                                $data['hero_products'] = $hero;
                            } else {
                                $error['message'] = "Failed when trying to add hero products";
                                $message = "failed";
                            }
                        }
                    }
                }
            }
        }

        $responseContent['error'] = $error;
        $responseContent['data'] = $data; // merging result data
        $responseContent['message'] = $message;
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function generateUrlPath(){
        $categoryModel = new \Models\ProductCategory();
        $categoryModel->setFromArray($this->params);

        $result = $categoryModel->generateUrlPath();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if($result['new_url_path'] == "") {
            $responseContent['errors'] = array("code" => "RR302", "title" => $rupaResponse->getResponseDescription("RR302"), 'messages'=>"Please check your parent path!");
            $responseContent['messages'] = array("Failed to generate url path!");
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("success") ;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function importCategory()
    {
        $params = $this->params;

        $dataInsert = array();
        $levelArray = array(
            6 => 2,
            8 => 3,
            10 => 4,
            12 => 5
        );

        $categoryLib = new \Library\Category();
        $categoryModel = new \Models\ProductCategory();


        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['errors'] = array("code" => "RR106", "title" => $rupaResponse->getResponseDescription("RR106"), "messages" => "Data not found");
        $responseContent['messages'] = array("Article Hierarchy is empty!");

        /**
         * Get Parent Information
         */
        if(!empty($params['article_hierarchy'])){
            $urlKey = preg_replace('#[^0-9a-z]+#i', '-', $params['category_name']);
            $urlKey = strtolower($urlKey);

            $ahParent = substr($params['article_hierarchy'],0,strlen($params['article_hierarchy']) - 2);
            if($ahParent === 'R130'){ // If, Parent is a top category

            }else{
                $parentInfo = $categoryModel->findFirst(
                    array(
                        'conditions' => 'article_hierarchy = "'.$ahParent.'" ',
                        'columns' => 'article_hierarchy,category_id, path, url_path, level, category_key'
                    )
                );

                if($parentInfo){ // Parent Exist
                    $parentInfo = $parentInfo->toArray();
                    $catUrlKey = $parentInfo['url_path']."/".$urlKey;
                    $metaTitle = "Jual ".$params['category_name']." | Ruparupa";

                    $urlKeyModel = new \Models\MasterUrlKey();
                    $isUrlKey = $urlKeyModel->findFirst(' url_key = "'.$catUrlKey.'" ');

                    if(!$isUrlKey){ // Url Key Available

                        $dataInsert['article_hierarchy'] = $params['article_hierarchy'];
                        $dataInsert['parent_id'] = $parentInfo['category_id'];
                        $dataInsert['name'] = $params['category_name'];
                        $dataInsert['meta_title'] = $metaTitle;
                        $dataInsert['position'] = 0;
                        $dataInsert['level'] = $levelArray[strlen($params['article_hierarchy'])];
                        $dataInsert['include_in_menu'] = 0;
                        $dataInsert['show_thumbnail'] = 0;
                        $dataInsert['children_count'] = 0;
                        $dataInsert['status'] = 10;
                        $dataInsert['is_anchor'] = 1;
                        $dataInsert['category_key'] = $urlKey;
                        $dataInsert['url_path'] = $catUrlKey;

                        $categoryModel->setFromArray($dataInsert);

                        $categoryModel->saveData();

                        if(!empty($categoryModel->getErrors())) {
                            $responseContent['errors'] = array("code" => "RR301", "title" => $rupaResponse->getResponseDescription("RR301"), "messages" => $categoryModel->getErrors());
                            $responseContent['messages'] = array("Failed to import category!");
                        } else {

                            /**
                             * Update Path Category
                             */
                            $newCatId = $categoryModel->getCategoryId();
                            $newPath = $parentInfo['path']."/".$newCatId;
                            $categoryModel->setPath($newPath);
                            $categoryModel->saveData();

                            /**
                             * Insert into master url key
                             */
                            $urlKeyModel = new \Models\MasterUrlKey();
                            $urlKeyModel->setSection('category');
                            $urlKeyModel->setUrlKey($catUrlKey);
                            $urlKeyModel->setReferenceId($newCatId);
                            $urlKeyModel->setDocumentTitle($metaTitle);

                            $urlKeyModel->saveUrlKey('masterUrlKey');

                            $categoryLib = new \Library\Category();
                            $categoryLib->deleteCategoryCache();

                            $responseContent['errors'] = array();
                            $responseContent['data'] = array();
                            $responseContent['messages'] = array("Success import category!");
                        }
                    }else{
                        $responseContent['errors'] = array("code" => "RR102", "title" => $rupaResponse->getResponseDescription("RR102"), "messages" => "Url Key Already Exist");
                        $responseContent['messages'] = array("Url Key Already Exist!");
                    }
                }
            }
        }


        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function uploadBatchCategory(){
        $categoryLib = new \Library\Category();

        $categoryResult = $categoryLib->insertCategoryBatch($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$categoryResult){
            $responseContent['messages'] = array("failed");
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
        }
        else{
            $categoryLib = new \Library\Category();
            $categoryLib->deleteCategoryCache();

            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $categoryResult;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateBatchCategory(){
        $categoryLib = new \Library\Category();
        $categoryResult = $categoryLib->updateCategoryBatch($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $categoryLib = new \Library\Category();
        $categoryLib->deleteCategoryCache();

        $responseContent['messages'] = array("success");
        $responseContent['errors'] = array();
        $responseContent['data'] = array("category_id" => $this->params['category_id'],
                                        "name" => $this->params['name'],
                                        "rows_affected" => $categoryResult);

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function autoHeroProduct()
    {
        $categoryLib = new \Library\Category();
        $autoHeroProduct = $categoryLib->getAutoHeroProduct();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($categoryLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $categoryLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($categoryLib->getErrorCode()),
                "messages" => $categoryLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $autoHeroProduct;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}