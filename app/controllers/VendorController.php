<?php

namespace Controllers;

/**
 * Class VendorController
 * @package Controllers
 * @RoutePrefix("/vendor")
 *
 */
class VendorController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
    }


    public function vendorCancelOrder()
    {
        $shopeeLib = new \Library\Shopee();
        $shopeeLib->orderCancelAcceptReject($this->params);

        $responseContent = array(
            'errors'    => [],
            'data'      => [],
            'messages'  => array("success")
        );

        if(!empty($shopeeLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $shopeeLib->getErrorCode(),
                "title" => $shopeeLib->getErrorCode(),
                "messages" => $shopeeLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed");
        }

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    // for oms, dont disturb
    public function getReturnShopeeData()
    {
        $shopeeLib = new \Library\Shopee();
        $allReturnList = $shopeeLib->getReturnShopeeData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($allReturnList['list'])) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Return List not found");
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $allReturnList['list'];
            $response['messages'] = array("result" => "success","recordsTotal" => $allReturnList['recordsTotal']);
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateReturnShopee()
    {
        $shopeeLib = new \Library\Shopee();
        $shopeeLib->updateReturnShopee($this->params);

        $responseContent = array(
            'errors'    => [],
            'data'      => [],
            'messages'  => array("success")
        );

        if(!empty($shopeeLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $shopeeLib->getErrorCode(),
                "title" => $shopeeLib->getErrorCode(),
                "messages" => $shopeeLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed");
        }

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }
    
    public function getOrderShopee()
    {
        $shopeeLib = new \Library\Shopee();
        $shopeeOrder =$shopeeLib->getOrderShopee($this->params);
    
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($shopeeOrder)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Shopee Order not found");
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $shopeeOrder;
            $response['messages'] = array("result" => "success");
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getOrderTokopedia()
    {
        $tokopediaLib = new \Library\Tokopedia();
        $tokopediaOrder =$tokopediaLib->getOrderTokopedia($this->params);
    
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($tokopediaOrder)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "tokopedia Order not found");
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $tokopediaOrder;
            $response['messages'] = array("result" => "success");
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getDataEscrow(){
        $shopeeLib = new \Library\Shopee();
        $shopeeEscrow =$shopeeLib->getDataEscrow($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($shopeeEscrow)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Shopee Escrow not found");
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $shopeeEscrow;
            $response['messages'] = array("result" => "success");
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getDataEscrowTokopedia(){
        $tokopediaLib = new \Library\Tokopedia();
        $tokopediaEscrow =$tokopediaLib->getDataEscrowTokopedia($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($tokopediaEscrow)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "tokopedia Escrow not found");
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $tokopediaEscrow;
            $response['messages'] = array("result" => "success");
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }
}