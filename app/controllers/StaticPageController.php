<?php


namespace Controllers;

use Models\Event;

class StaticPageController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function createStaticPage(){
        $error = "";
        $messages = "Create Static Page Success";

        $staticPageModel = new \Models\StaticPage();
        $staticPageModel->setPageFromArray($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $routeHelper = new \Helpers\RouteHelper();
        $message = $routeHelper->checkUrlKey("","static page",$staticPageModel->getUrlKey(), $staticPageModel->getCompanyCode());

        if($message['url_key_status'] == 0){
            //url key exist
            $error = "Url key existed";
            $messages = "Create failed! Url Key existed.";
        }
        else {
            $staticPageModel->saveData('create_static_page');

            $urlModel = new \Models\MasterUrlKey();
            $urlData = array(
                'url_key' => $staticPageModel->getUrlKey(),
                'section' => 'static page',
                'inline_script' => $staticPageModel->getInlineScript(),
                'inline_script_mobile' => $staticPageModel->getInlineScriptMobile(),
                'inline_script_seo' => $staticPageModel->getInlineScriptSeo(),
                'inline_script_seo_mobile' => $staticPageModel->getInlineScriptSeoMobile(),
                'additional_header' => $staticPageModel->getAdditionalHeader(),
                'additional_header_mobile' => $staticPageModel->getAdditionalHeaderMobile(),
                'document_title' => "Jual " . $staticPageModel->getTitle() . " | Ruparupa",
                'reference_id' => $staticPageModel->getStaticPageId(),
                'company_code' => $staticPageModel->getCompanyCode()
            );

            $urlModel->setFromArray($urlData);
            $result = $urlModel->saveUrlKey();
        }

        $responseContent['errors'] = !empty($error) ? array($error) : array();
        $responseContent['messages'] = array($messages);
        $responseContent['data'] = "";

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function staticPageList(){
        $staticPageLib = new \Library\StaticPage();
        $staticPageList = $staticPageLib->getAllStaticPage($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($staticPageLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $staticPageLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($staticPageLib->getErrorCode()),
                "messages" => $staticPageLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create page");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $staticPageList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();


    }

    public function staticPageDetail(){
        $staticPageLib = new \Library\StaticPage();

        $staticPageData = $staticPageLib->getStaticPageDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($staticPageLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $staticPageLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($staticPageLib->getErrorCode()),
                "messages" => $staticPageLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get static page details");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $staticPageData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateStaticPage(){
        $staticPageModel = new \Models\StaticPage();
        $companyCode = (isset($this->params['company_code'])) ? $this->params['company_code'] : 'ODI';
        unset($this->params['company_code']);
        $staticPageModel->setPageFromArray($this->params);
        $staticPageModel->saveData('update_static_page');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(isset($this->params['title'])){
            if($staticPageModel->getOldUrlKey() != $staticPageModel->getUrlKey()){
                $routeHelper = new \Helpers\RouteHelper();
                $message = $routeHelper->checkUrlKey($staticPageModel->getStaticPageId(),"static page",$staticPageModel->getUrlKey(), $companyCode);

                if($message['url_key_status'] == 0){ // url_key existed
                    $error = "Url key existed";
                    $messages = "Update failed! Url Key existed.";
                }
                else if($message['url_key_status'] == 1){ // url_key used to be this url_key
                    $urlModel = new \Models\MasterUrlKey();
                    $urlData = array(
                        'url_key' => $staticPageModel->getUrlKey(),
                        'section' => 'static page',
                        'inline_script' => $staticPageModel->getInlineScript(),
                        'inline_script_mobile' => $staticPageModel->getInlineScriptMobile(),
                        'inline_script_seo' => $staticPageModel->getInlineScriptSeo(),
                        'inline_script_seo_mobile' => $staticPageModel->getInlineScriptSeoMobile(),
                        'additional_header' => $staticPageModel->getAdditionalHeader(),
                        'additional_header_mobile' => $staticPageModel->getAdditionalHeaderMobile(),
                        'document_title' => "Jual " . $staticPageModel->getTitle() . " | Ruparupa",
                        'reference_id' => $staticPageModel->getStaticPageId(),
                        'company_code' => $companyCode,
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    $urlModel->setFromArray($urlData);
                    $result = $urlModel->updateUrlKey();
                }
                else{ // new
                    $urlModel = new \Models\MasterUrlKey();
                    $urlData = array(
                        'url_key' => $staticPageModel->getUrlKey(),
                        'section' => 'static page',
                        'inline_script' => $staticPageModel->getInlineScript(),
                        'inline_script_mobile' => $staticPageModel->getInlineScriptMobile(),
                        'inline_script_seo' => $staticPageModel->getInlineScriptSeo(),
                        'inline_script_seo_mobile' => $staticPageModel->getInlineScriptSeoMobile(),
                        'additional_header' => $staticPageModel->getAdditionalHeader(),
                        'additional_header_mobile' => $staticPageModel->getAdditionalHeaderMobile(),
                        'document_title' => "Jual " . $staticPageModel->getTitle() . " | Ruparupa",
                        'reference_id' => $staticPageModel->getStaticPageId(),
                        'company_code' => $companyCode,
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    $urlModel->setFromArray($urlData);
                    $result = $urlModel->saveUrlKey();
                }
            }
            else{
                $urlModel = new \Models\MasterUrlKey();
                $urlData = array(
                    'url_key' => $staticPageModel->getUrlKey(),
                    'section' => 'static page',
                    'inline_script' => $staticPageModel->getInlineScript(),
                    'inline_script_mobile' => $staticPageModel->getInlineScriptMobile(),
                    'inline_script_seo' => $staticPageModel->getInlineScriptSeo(),
                    'inline_script_seo_mobile' => $staticPageModel->getInlineScriptSeoMobile(),
                    'additional_header' => $staticPageModel->getAdditionalHeader(),
                    'additional_header_mobile' => $staticPageModel->getAdditionalHeaderMobile(),
                    'document_title' => "Jual ".$staticPageModel->getTitle()." | Ruparupa",
                    'reference_id' => $staticPageModel->getStaticPageId(),
                    'company_code' => $companyCode,
                    'created_at' => date('Y-m-d H:i:s')
                );

                $urlModel->setFromArray($urlData);
                $result = $urlModel->updateUrlKey();
            }
        }

        $responseContent['messages'] = array("success");
        $responseContent['data'] = "";

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

}