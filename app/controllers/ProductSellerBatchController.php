<?php


namespace Controllers;

class ProductSellerBatchController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getListBatch()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $sellerBatchLib = new \Library\ProductSellerBatch();
        $data = $sellerBatchLib->getBatchList();

        $responseContent = array();

        if (!empty($sellerBatchLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $sellerBatchLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($sellerBatchLib->getErrorCode()),
                "messages" => $sellerBatchLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $data;
            $responseContent['errors'] = array();
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveBatch()
    {
        $sellerBatchLib = new \Library\ProductSellerBatch();
        $result = $sellerBatchLib->saveBatch($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if (!empty($sellerBatchLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $sellerBatchLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($sellerBatchLib->getErrorCode()),
                "messages" => $sellerBatchLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result;
            $responseContent['errors'] = array();
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}
