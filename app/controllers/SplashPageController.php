<?php


namespace Controllers;

use Helpers\RouteHelper;
use Library\Response;
use Models\MasterUrlKey;
use Models\SplashPage;
use Library\SplashPage as SplashPageLibrary;

class SplashPageController extends BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function createSplashPage(){
        $error = array();
        $messages = array("Create Splash Page Success");

        $splashPageModel = new SplashPage();
        $splashPageModel->setFromArray($this->params);

        $rupaResponse = new Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $message = RouteHelper::checkUrlKey("","splash page",$splashPageModel->getUrlKey());

        if($message['url_key_status'] == 0){
            //url key exist
            $error = array("Url key existed");
            $messages = array("Create failed! Url Key existed.");
        }
        else {
            $saveResult = $splashPageModel->saveData('create_splash_page');

            if($saveResult == false){
                $error = array("Saving data failed. Please contact ruparupa tech team.");
                $messages = array();
            }
            else {
                $urlModel = new MasterUrlKey();
                $urlData = array(
                    'url_key' => $splashPageModel->getUrlKey(),
                    'section' => 'splash page',
                    'inline_script' => $splashPageModel->getInlineScript(),
                    'inline_script_seo' => $splashPageModel->getInlineScriptSeo(),
                    'inline_script_seo_mobile' => $splashPageModel->getInlineScriptSeoMobile(),
                    'additional_header' => $splashPageModel->getAdditionalHeader(),
                    'document_title' => "Jual Produk " . $splashPageModel->getTitle() . " | Ruparupa",
                    'reference_id' => $splashPageModel->getSplashPageId()
                );

                $urlModel->setFromArray($urlData);
                $result = $urlModel->saveUrlKey();

                if ($result == false) {
                    $error = array("Url key error");
                    $messages = array("Saving url_key failed. Please contact ruparupa tech team.");
                }
            }
        }

        $responseContent['errors'] = $error;
        $responseContent['messages'] = $messages;
        $responseContent['data'] = array();

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function splashPageList(){
        $splashPageLib = new SplashPageLibrary();
        $staticPageList = $splashPageLib->getAllSplashPage($this->params);

        $rupaResponse = new Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($splashPageLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $splashPageLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($splashPageLib->getErrorCode()),
                "messages" => $splashPageLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $staticPageList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function splashPageDetail(){
        $splashPageLib = new SplashPageLibrary();

        $splashPageData = $splashPageLib->getSplashPageDetail($this->params);

        $rupaResponse = new Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($splashPageLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $splashPageLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($splashPageLib->getErrorCode()),
                "messages" => $splashPageLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $splashPageData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateSplashPage(){
        $error = array();
        $messages = array("Update Splash Page Success");

        $splashPageModel = new SplashPage();

        $rupaResponse = new Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $splashPageModel->setFromArray($this->params);
        $saveResult = $splashPageModel->saveData('update_splash_page');

        if($saveResult == false){
            $error = array("Update failed");
            $messages = array("Please contact ruparupa tech team.");
        }
        else {

            if (isset($this->params['title'])) {
                if ($splashPageModel->getOldUrlKey() != $splashPageModel->getUrlKey()) {

                    $message = RouteHelper::checkUrlKey($splashPageModel->getSplashPageId(), "splash page",
                        $splashPageModel->getUrlKey());

                    if ($message['url_key_status'] == 0) { // url_key existed
                        $error = array("Url key existed");
                        $messages = array("Update failed! Url Key existed.");
                    } else {
                        if ($message['url_key_status'] == 1) { // url_key used to be this url_key
                            $urlModel = new MasterUrlKey();
                            $urlData = array(
                                'url_key' => $splashPageModel->getUrlKey(),
                                'section' => 'splash page',
                                'inline_script' => $splashPageModel->getInlineScript(),
                                'inline_script_seo' => $splashPageModel->getInlineScriptSeo(),
                                'inline_script_seo_mobile' => $splashPageModel->getInlineScriptSeoMobile(),
                                'additional_header' => $splashPageModel->getAdditionalHeader(),
                                'document_title' => "Jual Produk " . $splashPageModel->getTitle() . " | Ruparupa",
                                'reference_id' => $splashPageModel->getSplashPageId(),
                                'created_at' => date('Y-m-d H:i:s')
                            );

                            $urlModel->setFromArray($urlData);
                            $result = $urlModel->updateUrlKey();

                            if ($result['affected_rows'] <= 0) {
                                $error = array("Url key error");
                                $messages = array("Update failed! Please contact ruparupa tech team");
                            }
                        } else { // new
                            $urlModel = new MasterUrlKey();
                            $urlData = array(
                                'url_key' => $splashPageModel->getUrlKey(),
                                'section' => 'splash page',
                                'inline_script' => $splashPageModel->getInlineScript(),
                                'inline_script_seo' => $splashPageModel->getInlineScriptSeo(),
                                'inline_script_seo_mobile' => $splashPageModel->getInlineScriptSeoMobile(),
                                'additional_header' => $splashPageModel->getAdditionalHeader(),
                                'document_title' => "Jual Produk " . $splashPageModel->getTitle() . " | Ruparupa",
                                'reference_id' => $splashPageModel->getSplashPageId(),
                                'created_at' => date('Y-m-d H:i:s')
                            );

                            $urlModel->setFromArray($urlData);
                            $result = $urlModel->saveUrlKey();

                            if ($result == false) {
                                $error = array("Url key error");
                                $messages = array("Update failed! Please contact ruparupa tech team");
                            }
                        }
                    }
                } else {
                    $urlModel = new MasterUrlKey();
                    $urlData = array(
                        'url_key' => $splashPageModel->getUrlKey(),
                        'section' => 'splash page',
                        'inline_script' => $splashPageModel->getInlineScript(),
                        'inline_script_seo' => $splashPageModel->getInlineScriptSeo(),
                        'inline_script_seo_mobile' => $splashPageModel->getInlineScriptSeoMobile(),
                        'additional_header' => $splashPageModel->getAdditionalHeader(),
                        'document_title' => "Jual Produk " . $splashPageModel->getTitle() . " | Ruparupa",
                        'reference_id' => $splashPageModel->getSplashPageId(),
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    $urlModel->setFromArray($urlData);
                    $result = $urlModel->updateUrlKey();

                    if ($result['affected_rows'] <= 0) {
                        $error = array("Url key error");
                        $messages = array("Update failed! Please contact ruparupa tech team");
                    }
                }
            }
        }

        $responseContent['errors'] = $error;
        $responseContent['messages'] = $messages;
        $responseContent['data'] = array();

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

}