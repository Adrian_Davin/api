<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class ProductVariantController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    /**
     * @var $productVariantModel \Models\ProductVariant
     */
    protected $productVariantModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->productVariantModel = new \Models\ProductVariant();
    }

    /**
     * @todo : Direct query or move to go
     */
    public function listVariantId()
    {
        $this->productVariantModel->setFromArray($this->params);

        $responseContent['data'] = $this->productVariantModel->getProductVarianIdBaseOnCategory();
        $responseContent['errors'] = $this->productVariantModel->getErrors();
        $responseContent['messages'] = $this->productVariantModel->getMessages();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function totalQty()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $result = $this->productVariantModel->totalQty($this->params);
        if (empty($result['errors'])) {
            $responseContent['messages'] = array("result" => "success",);
            $responseContent['data'] = $result['data'];
        } else {
            $responseContent['errors'] = "There have something error in getting total qty";
            $responseContent['messages'] = array("result" => "failed");
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    /**
     * @todo : Move to GO or direct query to elastic
     */
    public function listProductCustom()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $this->productVariantModel->getProductCustomListByJSON($this->params);
        $responseContent['errors'] = $this->productVariantModel->getErrors();
        $responseContent['messages'] = $this->productVariantModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function importTempPriceZoneBatch()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $data = $this->params;

        $arrValues = array();
        foreach($data['values'] as $row){
            $arrSubValues = array();
            foreach($row as $sub){
                $arrSubValues[] = $sub;
            }
            $arrValues[] = "(".implode(',',$arrSubValues).")";
        }

        $data['values'] = implode(',',$arrValues);

        $tempPriceZoneModel = new \Models\TempPricezone();

        $tempPriceZoneModel->importBatch($data);

        $responseContent['data'] = $tempPriceZoneModel->getData();
        $responseContent['errors'] = $tempPriceZoneModel->getErrors();
        $responseContent['messages'] = $tempPriceZoneModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updateTempPriceZoneBatch()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $tempPricezoneModel = new \Models\TempPricezone();
        $tempPricezoneModel->updateBatch($this->params);

        $responseContent['data'] = $tempPricezoneModel->getData();
        $responseContent['errors'] = $tempPricezoneModel->getErrors();
        $responseContent['messages'] = $tempPricezoneModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function saveProductPrice()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $sku = $this->params['sku'];
        $suffix = $this->params['suffix'];

        $productPriceModel = new \Models\ProductPrice();
        $dataFlagPrice = $productPriceModel->findFirst(
            array(
                'conditions' => ' sku = "'.$sku.'" '
            )
        );

        $dataPrice = $dataFlagPrice->toArray();
        unset($dataPrice['price_id']);
        unset($dataPrice['created_at']);
        unset($dataPrice['updated_at']);
        $dataPrice['sku'] = $sku.$suffix;

        $productPriceModel->setFromArray($dataPrice);
        $productPriceModel->saveData();

        $responseContent['data'] = $productPriceModel->getData();
        $responseContent['errors'] = $productPriceModel->getErrors();
        $responseContent['messages'] = $productPriceModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updateProductPriceBatch()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productPriceModel = new \Models\ProductPrice();
        if($this->params['zone_id'] == 1){
            $productPriceModel->updateBatch($this->params);
        }

        $responseContent['data'] = $productPriceModel->getData();
        $responseContent['errors'] = $productPriceModel->getErrors();
        $responseContent['messages'] = $productPriceModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updatePriceBatch()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productPriceModel = new \Models\ProductPrice();
        $productPriceModel->updateBatchPrice($this->params);

        $responseContent['data'] = $productPriceModel->getData();
        $responseContent['errors'] = $productPriceModel->getErrors();
        $responseContent['messages'] = $productPriceModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updatePriceZoneBatch()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $pricezoneModel = new \Models\PriceZone();
        if($this->params['zone_id'] > 1){
            $pricezoneModel->updateBatch($this->params);
        }

        $responseContent['data'] = $pricezoneModel->getData();
        $responseContent['errors'] = $pricezoneModel->getErrors();
        $responseContent['messages'] = $pricezoneModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function queryTempPricezone()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $tempPricezoneModel = new \Models\TempPricezone();
        $result = $tempPricezoneModel->queryBatch($this->params);

        if(!$result) {
            $responseContent['errors'] = array(
                "code" => 'RR302',
                "title" => $this->rupaResponse->getResponseDescription('RR302'),
                "messages" => "Failed get price zone list"
            );
            $responseContent['messages'] = array("Failed get price zone list");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result;
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    /*
     * save product net sales
     */
    public function saveProductNetSales()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $sku = $this->params['sku'];

        $productNetSalesModel = new \Models\ProductNetSales();
        $dataFlagNetSales = $productNetSalesModel->findFirst(
            array(
                'conditions' => ' sku = "'.$sku.'" '
            )
        );

        $dataNetSales = array();
        $dataNetSalesArray = array();
        if(!empty($dataFlagNetSales)){
            $dataNetSalesArray = $dataFlagNetSales->toArray();
            $dataNetSales['net_sales_id'] = $dataNetSalesArray['net_sales_id'];
        }

        $dataNetSales = array_merge($dataNetSalesArray,$this->params);

        unset($dataNetSales['price_id']);
        unset($dataNetSales['created_at']);
        unset($dataNetSales['updated_at']);

        $productNetSalesModel->setFromArray($dataNetSales);
        $productNetSalesModel->saveData();

        $responseContent['data'] = $productNetSalesModel->getData();
        $responseContent['errors'] = $productNetSalesModel->getErrors();
        $responseContent['messages'] = $productNetSalesModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function saveProductNetSalesBatch()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        foreach($this->params['net_sales'] as $netSales) {
            $sku = $netSales['sku'];

            $productNetSalesModel = new \Models\ProductNetSales();
            $dataFlagNetSales = $productNetSalesModel->findFirst(
                array(
                    'conditions' => ' sku = "' . $sku . '" '
                )
            );

            $dataNetSales = array();
            $dataNetSalesArray = array();
            if (!empty($dataFlagNetSales)) {
                $dataNetSalesArray = $dataFlagNetSales->toArray();
                $dataNetSales['net_sales_id'] = $dataNetSalesArray['net_sales_id'];
            }

            $dataNetSales = array_merge($dataNetSalesArray, $netSales);

            unset($dataNetSales['price_id']);
            unset($dataNetSales['created_at']);
            unset($dataNetSales['updated_at']);

            $productNetSalesModel->setFromArray($dataNetSales);
            $productNetSalesModel->saveData();
        }

        $netSalesCommissionFiles = new \Models\ProductCommissionNetSalesFile();
        $netSalesCommissionFiles->setFromArray($this->params['file']);
        $netSalesCommissionFiles->saveData();

        $responseContent['data'] = $productNetSalesModel->getData();
        $responseContent['errors'] = $productNetSalesModel->getErrors();
        $responseContent['messages'] = $productNetSalesModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function getProductNetSales(){
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;
        $netSalesModel = new \Models\ProductNetSales();
        $result = $netSalesModel->find(
            array(
                'conditions' => 'sku in ("'.$params['sku'].'")',
                'order' => 'created_at desc',
                'limit' => 1
            )
        );

        if(!$result) {
            $responseContent['errors'] = array(
                "code" => 'RR302',
                "title" => $this->rupaResponse->getResponseDescription('RR302'),
                "messages" => "Failed get list comment history"
            );
            $responseContent['messages'] = array("Failed get list comment history");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result->toArray();
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }


    /*
     * save product commission
     */
    public function saveProductCommission()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $sku = $this->params['sku'];

        $productCommissionModel = new \Models\ProductCommission();
        $dataFlagCommission = $productCommissionModel->findFirst(
            array(
                'conditions' => ' sku = "'.$sku.'" '
            )
        );

        $dataCommission = array();
        $dataCommissionArray = array();
        if(!empty($dataFlagCommission)){
            $dataCommissionArray = $dataFlagCommission->toArray();
            $dataCommission['commission_id'] = $dataCommissionArray['commission_id'];
        }

        $dataCommission = array_merge($dataCommissionArray,$this->params);

        unset($dataCommission['price_id']);
        unset($dataCommission['created_at']);
        unset($dataCommission['updated_at']);

        $productCommissionModel->setFromArray($dataCommission);
        $productCommissionModel->saveData();

        $responseContent['data'] = $productCommissionModel->getData();
        $responseContent['errors'] = $productCommissionModel->getErrors();
        $responseContent['messages'] = $productCommissionModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function saveProductCommissionBatch()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        foreach($this->params['comissions'] as $commission) {
            $sku = $commission['sku'];

            $productCommissionModel = new \Models\ProductCommission();
            $dataFlagCommission = $productCommissionModel->findFirst(
                array(
                    'conditions' => ' sku = "' . $sku . '" '
                )
            );

            $dataCommission = array();
            $dataCommissionArray = array();
            if (!empty($dataFlagCommission)) {
                $dataCommissionArray = $dataFlagCommission->toArray();
                $dataCommission['commission_id'] = $dataCommissionArray['commission_id'];
            }

            $dataCommission = array_merge($dataCommissionArray, $commission);

            unset($dataCommission['price_id']);
            unset($dataCommission['created_at']);
            unset($dataCommission['updated_at']);

            $productCommissionModel->setFromArray($dataCommission);
            $productCommissionModel->saveData();
        }

        $netSalesCommissionFiles = new \Models\ProductCommissionNetSalesFile();
        $netSalesCommissionFiles->setFromArray($this->params['file']);
        $netSalesCommissionFiles->saveData();

        $responseContent['data'] = $productCommissionModel->getData();
        $responseContent['errors'] = $productCommissionModel->getErrors();
        $responseContent['messages'] = $productCommissionModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function getProductCommission(){
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;
        $commissionModel = new \Models\ProductCommission();
        $result = $commissionModel->find(
            array(
                'conditions' => 'sku in ("'.$params['sku'].'")',
                'order' => 'created_at desc',
                'limit' => 1
            )
        );

        if(!$result) {
            $responseContent['errors'] = array(
                "code" => 'RR302',
                "title" => $this->rupaResponse->getResponseDescription('RR302'),
                "messages" => "Failed get list comment history"
            );
            $responseContent['messages'] = array("Failed get list comment history");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result->toArray();
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function getProductCommissionNetSalesFile()
    {
        $this->params['report_type'] = "settlement";

        $netSalesCommissionFiles = new \Models\ProductCommissionNetSalesFile();
        $netSalesCommissionFiles->setFromArray($this->params);
        $report = $netSalesCommissionFiles->getProductCommissionNetSalesFile();
        $detail = $report->toArray();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $detail;
        $responseContent['errors'] = $netSalesCommissionFiles->getErrors();
        $responseContent['messages'] = $netSalesCommissionFiles->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }


    /**
     * @todo : Move to GO or direct query to elastic
     */
    public function listQualityProduct()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $result =  $this->productVariantModel->listQualityProduct($this->params);

        if(!$result) {
            $responseContent['errors'] = array(
                "code" => 'RR302',
                "title" => $this->rupaResponse->getResponseDescription('RR302'),
                "messages" => "Failed get list quality product"
            );
            $responseContent['messages'] = array("Failed get list quality product");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result;
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function insertCommentHistory()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $commentHistoryModel = new \Models\ProductCommentHistory();
        $commentHistoryModel->setFromArray($this->params);
        $commentHistoryModel->saveData();

        $responseContent['data'] = $commentHistoryModel->getData();
        $responseContent['errors'] = $commentHistoryModel->getErrors();
        $responseContent['messages'] = $commentHistoryModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function getCommentHistory()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;
        $commentHistoryModel = new \Models\ProductCommentHistory();
        $result = $commentHistoryModel->find(
            array(
                'conditions' => 'sku in ("'.$params['sku'].'")',
                'order' => 'created_at desc',
                'limit' => 1
            )
        );

        if(!$result) {
            $responseContent['errors'] = array(
                "code" => 'RR302',
                "title" => $this->rupaResponse->getResponseDescription('RR302'),
                "messages" => "Failed get list comment history"
            );
            $responseContent['messages'] = array("Failed get list comment history");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result->toArray();
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updateProductImages()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productVariantModel = new \Models\ProductVariant();
        $productVariantId = $productVariantModel->findFirst(" sku = '".$this->params['sku']."' ");

        if($productVariantId){

            $productImagesModel = new \Models\ProductImages();
            $productImagesModel->setFromArray($this->params);
            $checkExist = $productImagesModel->findFirst([
                "conditions" => " product_variant_id in ('".$productVariantId->toArray()['product_variant_id']."') and 
                              angle = ".$this->params['angle']." and
                              status > -1
            "
            ]);
            if($checkExist){
                $productImagesModel->setImageId($checkExist->toArray()['image_id']);
            }

            $productImagesModel->setProductVariantId($productVariantId->toArray()['product_variant_id']);
            $productImagesModel->saveData("product_images");

            $responseContent['messages'] = array("success");
            $responseContent['data'] = array('product_variant_id' => $productVariantId->toArray()['product_variant_id'], 'affected_row' => 1);
            $responseContent['errors'] = array();
        }else{
            $responseContent['errors'] = array(
                "code" => 'RR302',
                "title" => $this->rupaResponse->getResponseDescription('RR302'),
                "messages" => "Failed Update Product Images"
            );
            $responseContent['messages'] = array("Failed Update Product Images");
            $responseContent['data'] = array();
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function saveProductStock()
    {
        $responseContent = array(
            'data' => array(),
            'errors' => array(),
            'messages' => array('No Data Imported')
        );

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $param = $this->params;

        if($param['sku']){
            $data['sku'] = $param['sku'];

            $storeCode = array();

            /*
             * Default DC
             */
            $storeCode[] = "'DC'";

            /**
             * Getting Code Store
             */
            $storeModel = new \Models\Store();
            $storeResult = $storeModel->find(' status >= 10 and pickup_id is not null and type = 2 ');
            if($storeResult){
                foreach($storeResult->toArray() as $row){
                    $storeCode[] = "'".$row['store_code']."'";
                }
            }

            /**
             * Get Store Code Already exist
             */
            $storeCodeExist = array();
            $productStockModel = new \Models\ProductStock();
            $productStockResult = $productStockModel->find(" sku = '".$param['sku']."' ");
            if($productStockResult){
                foreach($productStockResult->toArray() as $row){
                    $storeCodeExist[] = "'".$row['store_code']."'";
                }
            }

            $storeCode = array_diff($storeCode,$storeCodeExist);

            if(!empty($storeCode)){
                $storeCode = implode(',',$storeCode);
                $data['store_code'] = $storeCode;
                $productStockModel = new \Models\ProductStock();

                $response = $productStockModel->importStock($data);
            }
        }

        $responseContent['errors'] = $response['errors'];
        $responseContent['messages'] = $response['messages'];

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updateMinMaxStock(){
        $productVariant = new \Library\ProductVariant();
        $result = $productVariant->updateMinMaxStock($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($productVariant->getErrors())){
            $responseContent['data'] = array();
            $responseContent['errors'] = $productVariant->getErrors();
            $responseContent['messages'] = $productVariant->getMessages();
        }
        else{
            $responseContent['data'] = $result;
            $responseContent['errors'] = array();
            $responseContent['messages'] = array("success");
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function generateSku()
    {
        $param = $this->params;
        if($param['master_app_id'] == $_ENV['MASTER_APP_ID_SELLER'] || $param['master_app_id'] == $_ENV['MASTER_APP_ID_GIDEON']){
            $sku = $this->productVariantModel->generateSkuMp($param);
            $reservedModel = new \Models\ProductVariantReservedSku();
            $reservedModel->setSku($sku);
            $reservedModel->setSupplierId($param['supplier_id']);
            $reservedModel->saveData('product_variant_reserved_sku');

            $responseContent['data'] = array('sku' => $sku);
            $responseContent['errors'] = array();
        }else{
            $responseContent['data'] = array();
            $responseContent['errors'] = array(
                "code" => 'RR106',
                "messages" => "Data Not Found");
        }

        $responseContent['messages'] = "";

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    /**
     * @todo : Move to GO
     */
    public function changeProductSupplier(){
        $productVariant = new \Library\ProductVariant();
        $productVariant->changeProductSupplier($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $productVariant->getData();
        $responseContent['errors'] = $productVariant->getErrors();
        $responseContent['messages'] = $productVariant->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function changeProductSupplierImage(){
        $imageModel = new \Models\ProductImages();
        $imageModel->updateImagesChangeSupplier($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $imageModel->getData();
        $responseContent['errors'] = $imageModel->getErrors();
        $responseContent['messages'] = $imageModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

}