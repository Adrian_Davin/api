<?php

namespace Controllers;


class MasterAttributesController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    private function handleResponse($masterAttributes, $attributeList = array()){
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($masterAttributes->getErrorMessages())) {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();

            $responseContent['errors'] = array(
                "code" => $masterAttributes->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($masterAttributes->getErrorCode()),
                "messages" => $masterAttributes->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $statusCode = 200;
            if(empty($attributeList)) {
                $statusCode = 404;
            }

            $rupaResponse->setStatusCode($statusCode, "Ok")->sendHeaders();
            $responseContent['errors'] = array();
            $responseContent['data'] = $attributeList;
            $responseContent['messages'] = "success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getListMasterAttributes()
    {
        $masterAttributes = new \Library\MasterAttributes();
        $attributeList = $masterAttributes->getListMasterAttributes($this->params);

        $this->handleResponse($masterAttributes, $attributeList);
    }

    public function getListMasterAttributesOnSet()
    {
        $masterAttributes = new \Library\MasterAttributes();
        $attributeList = $masterAttributes->getListMasterAttributesOnSet($this->params);

        $this->handleResponse($masterAttributes, $attributeList);
    }

    public function getListMasterAttributesOnVariantSet()
    {
        $masterAttributes = new \Library\MasterAttributes();
        $attributeList = $masterAttributes->getListMasterAttributesOnVariantSet($this->params);

        $this->handleResponse($masterAttributes, $attributeList);
    }

    public function getVariantOption()
    {
        $masterAttributes = new \Library\MasterAttributes();
        $attributeList = $masterAttributes->getVariantOption($this->params);

        $this->handleResponse($masterAttributes, $attributeList);
    }

    public function saveMasterAttributes() {
        $masterAttributesLib = new \Library\MasterAttributes();
        $masterAttributesLib->saveMasterAttributes($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($masterAttributesLib->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $masterAttributesLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($masterAttributesLib->getErrorCode()),
                "messages" => $masterAttributesLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $responseContent['data'] = '';
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function addMasterAttributes() {
        $masterAttributesLib = new \Library\MasterAttributes();
        $masterAttributesLib->addMasterAttributes($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($masterAttributesLib->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $masterAttributesLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($masterAttributesLib->getErrorCode()),
                "messages" => $masterAttributesLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $responseContent['data'] = '';
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateMasterAttributes() {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $masterAttributeModel = new \Models\MasterAttributes();
        $masterAttributeModel->setFromArray($this->params);

        $error = $masterAttributeModel->saveData();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($masterAttributeModel->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $masterAttributesLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($masterAttributesLib->getErrorCode()),
                "messages" => $masterAttributesLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $responseContent['data'] = '';
            $responseContent['errors'] = ''; 
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}