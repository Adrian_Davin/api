<?php


namespace Controllers;

/**
 * Class OrderController
 * @package Controllers
 * @RoutePrefix("/order")
 *
 */
class SalesOrderController extends \Controllers\BaseController
{

    /**
     * @var $sales_credit_memo \Models\SalesCreditMemo
     */
    protected $sales_credit_memo;

    /**
     * @var $salesrule_order \Models\SalesruleOrder
     */
    protected $salesrule_order;

    public function onConstruct()
    {
        parent::onConstruct();

        $this->sales_credit_memo = new \Models\SalesCreditMemo;
        $this->salesrule_order = new \Models\SalesruleOrder;
    }

    public function getOrderList()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $allOrders = $salesOrderLib->getAllOrder($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $recordsTotal = $allOrders['recordsTotal'];
            unset($allOrders['recordsTotal']);

            $responseContent['messages'] = array("result" => "success", "recordsTotal" => $recordsTotal);
            $responseContent['data'] = $allOrders;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    // For Order dashboard in oms
    public function getOrderListCustom()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $allOrders = $salesOrderLib->getAllOrderCustom($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(empty($allOrders['list']) || !empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("result" => "failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['errors'] = array();
            $responseContent['messages'] = array("result" => "success","recordsTotal" => $allOrders['recordsTotal']);
            $responseContent['data'] = $allOrders['list'];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getOrderListDiscountAmount()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $allOrders = $salesOrderLib->getOrderListDiscountAmount($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $recordsTotal = $allOrders['recordsTotal'];
            unset($allOrders['recordsTotal']);

            $responseContent['messages'] = array("result" => "success", "recordsTotal" => $recordsTotal);
            $responseContent['data'] = $allOrders;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveOrder()
    {
        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start trap log all process save order controllers/SalesOrderController.php Cart id: '.$this->params['cart_id'], 'debug');

        $salesOrderLib = new \Library\SalesOrder();

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start overall salesOrderLib->setFromShoppingCart controllers/SalesOrderController.php Cart id: '.$this->params['cart_id'], 'debug');

        $salesOrderLib->setFromShoppingCart($this->params);

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End overall salesOrderLib->setFromShoppingCart controllers/SalesOrderController.php Cart id: '.$this->params['cart_id'], 'debug');

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start overall salesOrderLib->createSalesOrder controllers/SalesOrderController.php Cart id: '.$this->params['cart_id'], 'debug');

        $salesOrderLib->createSalesOrder($this->params);

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End overall salesOrderLib->createSalesOrder controllers/SalesOrderController.php Cart id: '.$this->params['cart_id'], 'debug');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesOrderLib->getErrorCode())) {
            $errorCode = $salesOrderLib->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR400";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to save order");
            $responseContent['data'] = array();
        } else {
            $customerLib = new \Library\Customer();
            $customerLib->checkCustomerAlert($salesOrderLib->getSalesOrder()->getOrderNo(),$salesOrderLib->getSalesOrder()->getSalesOrderId());

            $productLib = new \Library\Product();
            $productLib->checkSku($salesOrderLib->getSalesOrder()->getOrderNo());

            $responseContent['messages'] = array("success");
            $responseContent['data'] = array("order_no" => $salesOrderLib->getSalesOrder()->getOrderNo(), "status" => $salesOrderLib->getSalesOrder()->getStatus());
        }
        
        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End trap log all process save order controllers/SalesOrderController.php Cart id: '.$this->params['cart_id'], 'debug');

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getOrderDetail()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $this->params['order_no'] = !empty($this->params['order_no']) ? $this->params['order_no'] : "";
        $salesOrderData = $salesOrderLib->getSalesOrderDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $responseContent = array();
        if(!empty($salesOrderLib->getErrorCode())) {
            $rupaResponse->setStatusCode($salesOrderLib->getErrorCode(), $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()))->sendHeaders();
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $salesOrderData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getOrderShipmentDetail()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $this->params['order_no'] = !empty($this->params['order_no']) ? $this->params['order_no'] : "";
        $this->params['page_type'] = !empty($this->params['page_type']) ? $this->params['page_type'] : "";
        $salesOrderData = $salesOrderLib->getSalesOrderShipmentDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $responseContent = array();
        if(!empty($salesOrderLib->getErrorCode())) {
            $rupaResponse->setStatusCode($salesOrderLib->getErrorCode(), $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()))->sendHeaders();
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $salesOrderData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getOrderItem()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderData = $salesOrderLib->getOrderItem($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $salesOrderData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createOrderCache()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderLib->createSaleOrderCache($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = array("order_no" => $this->params['order_no']);
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Prepare for credit memo
     */
    public function prepareCreditMemo()
    {

        $creditMemoLib = new \Library\CreditMemo();
        $creditMemoLib->prepareCreditMemo($this->params);
        //if(empty($creditMemoLib->getErrorCode())) {
            $creditMemoLib->saveToCache();
        //}

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($creditMemoLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $creditMemoLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($creditMemoLib->getErrorCode()),
                "messages" => $creditMemoLib->getErrorMessages()
            );
            //$responseContent['messages'] = array("");
            $responseContent['data'] = [
                "credit_memo_id" => $creditMemoLib->getCreditMemoId(),
                "type" => $creditMemoLib->getType(),
                "subtotal" => $creditMemoLib->getSubTotal(),
                "shipping_amount" => $creditMemoLib->getShippingAmount(),
                "customer_penalty" => $creditMemoLib->getCustomerPenalty(),
                "refund_as_gift_card" => $creditMemoLib->getRefundAsGiftcard(),
                "refund_cash" => $creditMemoLib->getRefundCash(),
                "credit_memo_no" => $creditMemoLib->getCreditMemoNo()
            ];
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = [
                "credit_memo_id" => $creditMemoLib->getCreditMemoId(),
                "type" => $creditMemoLib->getType(),
                "subtotal" => $creditMemoLib->getSubTotal(),
                "shipping_amount" => $creditMemoLib->getShippingAmount(),
                "customer_penalty" => $creditMemoLib->getCustomerPenalty(),
                "refund_as_gift_card" => $creditMemoLib->getRefundAsGiftcard(),
                "refund_cash" => $creditMemoLib->getRefundCash(),
                "credit_memo_no" => $creditMemoLib->getCreditMemoNo()
            ];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Save credit memo
     */
    public function saveCreditMemo()
    {
        $creditMemoLib = new \Library\CreditMemo();
        $result = $creditMemoLib->loadFromCache(['credit_memo_id' => $this->params['credit_memo_id']]);
        if(!empty($result)) {
            $count = $creditMemoLib->countCreditMemoForOrderNo($result['order_no']);
            $requestArray = $creditMemoLib->formatRequestArray($result);
            $creditMemoLib->prepareCreditMemo($requestArray);
            $creditMemoLib->setCreatedBy($this->params['created_by']);

            $result['credit_memo_count'] = $count;

            foreach ($result['invoices'] as $invoiceCRItem) {
                $invoice = \Models\SalesInvoice::findFirst("invoice_no = '".$invoiceCRItem['invoice_no']."'");

                $store_code = $invoice->getStoreCode();
                
                if (substr($store_code, 0, 4) == "1000") {
                    $store_code = substr($store_code,4);
                }
                
                // Send to KL System for product not MP
                if (substr($invoice->getSupplierAlias(), 0, 2) != "MP") {
                    // Create SO to all item from DC
                    if ($invoice->getDeliveryMethod() == "delivery" || ($invoice->getDeliveryMethod() == "ownfleet" && substr( $invoiceStoreCode, 0, 2 ) === "DC")) {
                        //Check invoice store code
                        // exclude all invoice with exclude store code
                        if ($store_code == "exclude") {
                            continue;
                        }

                        $companyModel = New \Models\CompanyProfileDC();
                        $companyProfile = $companyModel->getCompanyByStoreCode($store_code);
                        $companyCode =  !empty($companyProfile) ? $companyProfile["company_code"] : "" ;

                        if ($companyCode == 'TGI' && $invoice->checkIfConsignment()) {
                            if ($invoice->SalesOrder->getOrderType() == 'B2B') {
                                $consB2B = new \Library\CONSB2B($invoice, $result);
                                $consB2B->prepareDCParamsCONSB2B();
                                $consB2B->generateDCParamsCONSB2B();
                                $consB2B->createCONSB2B();
                            } else {
                                $consB2C = new \Library\CONSB2C($invoice, $result);
                                $consB2C->prepareDCParamsCONSB2C();
                                $consB2C->generateDCParamsCONSB2C();
                                $consB2C->createCONSB2C();
                            }
                        }
                    }
                }
            }


            if(isset($this->params['reorder'])){
                $creditMemoLib->setReorder($this->params['reorder']);
            }
            
            
            if(empty($creditMemoLib->getErrorCode())) {
                //$creditMemoLib->setFromArray($finalData);
                $creditMemoLib->saveData();
            }
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($creditMemoLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $creditMemoLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($creditMemoLib->getErrorCode()),
                "messages" => $creditMemoLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = [
                "subtotal" => $creditMemoLib->getSubTotal(),
                "shipping_amount" => $creditMemoLib->getShippingAmount(),
                "customer_penalty" => $creditMemoLib->getCustomerPenalty(),
                "refund_as_gift_card" => $creditMemoLib->getRefundAsGiftcard(),
                "refund_cash" => $creditMemoLib->getRefundCash(),
                "voucher_code" => $creditMemoLib->getVoucherCode(),
            ];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCreditMemoMongo(){

        $creditMemoLib = new \Library\CreditMemo();
        $allOrders = $creditMemoLib->loadFromCache($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($creditMemoLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $creditMemoLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($creditMemoLib->getErrorCode()),
                "messages" => $creditMemoLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $allOrders;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCreditMemo(){
        $creditMemoLib = new \Library\CreditMemo();
        $allOrders = $creditMemoLib->getCreditMemo($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($creditMemoLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $creditMemoLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($creditMemoLib->getErrorCode()),
                "messages" => $creditMemoLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $allOrders;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCreditMemoList(){
        $creditMemoLib = new \Library\CreditMemo();
        $allOrders = $creditMemoLib->getCreditMemoList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($creditMemoLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $creditMemoLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($creditMemoLib->getErrorCode()),
                "messages" => $creditMemoLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $allOrders;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCreditMemoListCustom()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $this->params['type'] = !empty($this->params['type'])? $this->params['type'] : "";
        $creditMemoLib = new \Library\CreditMemo();
        $allCreditMemo = $creditMemoLib->getCreditMemoListAll($this->params);

        if (isset($allCreditMemo['list'])) {
            if((empty($allCreditMemo['list']) && $this->params['type']!="count_only") || ($allCreditMemo['recordsTotal']==0 && $this->params['type']=="count_only")) {
                $responseContent['errors'] = "There have something error, in query getCreditMemoList";
                $responseContent['messages'] = array("result" => "failed");
            } else {
                $responseContent['messages'] = array("result" => "success","recordsTotal" => $allCreditMemo['recordsTotal']);
                $responseContent['data'] = $allCreditMemo['list'];
            }
        } else {
            $responseContent['errors'] = "There have something error, in query getCreditMemoList";
            $responseContent['messages'] = array("result" => "failed");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getDetailMpSettlement()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $salesOrderModel = new \Models\SalesOrder();
        $result = $salesOrderModel->queryMpSettlement($this->params);

        if(!$result) {
            $responseContent['errors'] = array(
                "code" => 'RR302',
                "title" => $rupaResponse->getResponseDescription('RR302'),
                "messages" => "Data not Found"
            );
            $responseContent['messages'] = array("Data not Found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getTotalMpSettlement()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $salesOrderModel = new \Models\SalesOrder();
        $result = $salesOrderModel->queryTotalMpSettlement($this->params);

        if(!$result) {
            $responseContent['errors'] = array(
                "code" => 'RR302',
                "title" => $rupaResponse->getResponseDescription('RR302'),
                "messages" => "Data not Found"
            );
            $responseContent['messages'] = array("Data not Found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getOrderListBaseOnExternalOrderNo()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $allOrders = $salesOrderLib->getOrderListBaseOnExternalOrderNo($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(empty($allOrders['list']) || !empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("result" => "failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['errors'] = array();
            $responseContent['messages'] = array("result" => "success");
            $responseContent['data'] = $allOrders['list'];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getOrderSummary()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderSummary = $salesOrderLib->getSalesOrderSummary($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($salesOrderSummary)) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        } else {
            $rupaResponse->setStatusCode(404, "Not Found")->sendHeaders();
        }

        $responseContent = array();

        if(!empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("result" => "Orders not found", "recordsTotal" => 0, "processedTotal" => 0);
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("result" => "success", "recordsTotal" => $salesOrderSummary['count'], "processedTotal" => $salesOrderSummary['processedCount']);
            $responseContent['data'] = $salesOrderSummary['list'];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getOrderSummaryOnly()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderSummary = $salesOrderLib->getSalesOrderSummaryOnly($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($salesOrderSummary)) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        } else {
            $rupaResponse->setStatusCode(404, "Not Found")->sendHeaders();
        }

        $responseContent = array();

        if(!empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("result" => "Orders not found", "recordsTotal" => 0);
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("result" => "success", "recordsTotal" => $salesOrderSummary['count']);
            $responseContent['data'] = $salesOrderSummary['list'];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function lazadaUpdateOrderStatus(){
        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderLib->lazadaUpdateOrderStatus($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesOrderLib->getErrorCode())) {
            $errorCode = $salesOrderLib->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR400";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to update order");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = array("order_no" => $salesOrderLib->getSalesOrder()->getOrderNo(), "status" => $salesOrderLib->getSalesOrder()->getStatus());
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateOrderAddress()
    {
        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderLib->updateOrderAddress($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages());
            $responseContent['messages'] = array('save failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getOrderListPayment(){
        $salesOrderLib = new \Library\SalesOrder();
        $orderListPayment = $salesOrderLib->getOrderListPayment($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($salesOrderLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesOrderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesOrderLib->getErrorCode()),
                "messages" => $salesOrderLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Orders not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("result" => "success");
            $responseContent['data'] = $orderListPayment;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateOrder()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $orderLib = new \Library\SalesOrder();
        // validate params
        $orderLib->prepareOrderUpdate($this->params);
        if (empty($orderLib->getErrorCode())) {
            $orderLib->updateData();
            if(empty($orderLib->getErrorCode())) {
                $response['messages'] = "success";
            } else {
                $response['errors'] = array(
                    "code" => $orderLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($orderLib->getErrorCode()),
                    "message" => $orderLib->getErrorMessages());
            }
        } else {
            $response['errors'] = array(
                "code" => $orderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($orderLib->getErrorCode()),
                "message" => $orderLib->getErrorMessages());
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateOrderPayment()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $orderLib = new \Library\SalesOrder();
        $orderLib->updateDataPayment($this->params);
        if(empty($orderLib->getErrorCode())) {
            $response['messages'] = "success";
        } else {
            $response['errors'] = array(
                "code" => $orderLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($orderLib->getErrorCode()),
                "message" => $orderLib->getErrorMessages());
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    /**
     * Get giftcard from sales_order for migrated to salesrule_order
     */
    public function getDataGiftCards()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $data = $this->salesrule_order->getDataGiftCards();
        if (count($data) > 0) {
            $responseContent['messages'] = "success";
            $responseContent['data'] = $data;
        } else {
            $responseContent['errors'] = "No data found";
            $responseContent['messages'] = array("result" => "failed");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * insert giftcard from sales_order to salesrule_order
     */
    public function insertDataGiftCards()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $orderLib = new \Library\SalesOrder();
        $affectedRows = $orderLib->insertDataGiftCards($this->params);
        if ($affectedRows > 0) {
            $responseContent['messages'] = "success";
            $responseContent['data']['affected_rows'] = $affectedRows;
        } else {
            $responseContent['errors'] = "No data found";
            $responseContent['messages'] = array("result" => "failed");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function removeHistoryVoucher(){
        $params = $this->params;

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $rupaResponseContent = array();

        if(empty($params['sales_order_id']) || empty($params['admin_user_id'])){
            $rupaResponseContent['errors'] = 'Missing sales_order_id or admin_user_id, sales_order_id and admin_user_id are mandatory';
            $rupaResponseContent['messages'] = '';
            $rupaResponse->setContent($rupaResponseContent);
            $rupaResponse->send();
            return;
        }

        $productStockLib = new \Library\ProductStock();
        $salesOrderLib = new \Library\SalesOrder();
        $orderDetail = $salesOrderLib->getSalesOrderDetailByID($params);

        if($orderDetail['status'] != 'new'){
            
            $rupaResponseContent['errors'] = 'Failed, order status must be new';
            $rupaResponseContent['messages'] = '';

            $rupaResponse->setContent($rupaResponseContent);
            $rupaResponse->send();
            return;
        }

        $salesRuleLib = new \Library\SalesRule();
        $sumSalesRule = $salesRuleLib->countBySalesOrderId($params);
        
        if($sumSalesRule['sum'] > 0){
            $voucherCodes = $salesRuleLib->getListVoucherCode($params);

            $salesOrderLib->cancelRedeemStampsVoucherUnify($orderDetail['order_no']);
            if (!empty($salesOrderLib->getErrorMessages())) {
                $rupaResponseContent["messages"] = $salesOrderLib->getErrorMessages();
                $rupaResponseContent["errors"] = ""; 
            }
            $result = $salesRuleLib->removeVoucherHistoryTransaction($params, $voucherCodes);
            if ($result) {
                $salesOrderItems = $salesOrderLib->getSalesOrderItems($params);
                
                if(empty($salesOrderItems['error'])){
                    $salesOrderItemIds = array();

                    foreach ($salesOrderItems as $item){
                        $salesOrderItemIds[] = (int)$item['sales_order_item_id'];
                    }

                    $productStockLib->revertStockOrderV2('payment', $salesOrderItemIds);

                    $rupaResponseContent["messages"] = "Voucher history removed";
                    $rupaResponseContent["errors"] = "";
                }else{
                    $rupaResponseContent["messages"] = "";
                    $rupaResponseContent["errors"] = "Error when updating stock qty"; 
                }
            } else {
                $rupaResponseContent["messages"] = "";
                $rupaResponseContent["errors"] = "Failed Remove voucher history";
            }
            
        }else{
            // this endpoint called by endpoint go-order /sales/order/cancel (endpoint for cancel order triggered by customer)
            // so that even the order dont have voucher we still need to revert the stock (quantity_on_hold_payment value)
            if ($orderDetail['status'] == 'new') {
                //// temporarily disabled to prevent double reverts
                //// for revamp stock
                
                // $salesOrderItems = $salesOrderLib->getSalesOrderItems($params);                
                // if(empty($salesOrderItems['error'])){
                //     // $salesOrderItemIds = array();

                //     // foreach ($salesOrderItems as $item){
                //     //     $salesOrderItemIds[] = (int)$item['sales_order_item_id'];
                //     // }

                //     // $productStockLib->revertStockOrderV2('payment', $salesOrderItemIds);
                // }
            }            

            $rupaResponseContent["messages"] = "Voucher history already remove before, Please DO NOT spam the process";
            $rupaResponseContent["errors"] = "";            
        }

        $rupaResponse->setContent($rupaResponseContent);
        $rupaResponse->send();
        return;
    }

}