<?php


namespace Controllers;

use Models\MasterAroma;
use Models\MasterSize;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class AttributeController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $optionValueModel;

    protected $unitOptionsModel;

    protected $sizeOptions;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->optionValueModel = new \Models\AttributeOptionValue();
        $this->variantOptionsModel = new \Models\AttributeVariantOptions();
        $this->unitOptionsModel = new \Models\AttributeUnitOptions();
        $this->sizeOptions = new MasterSize();
        $this->aromaOptions = new MasterAroma();
    }

    public function listOptionValue()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;

        if (isset($params['show_status'])) {
            $optionValue = $this->optionValueModel->find('attribute_id = '.$params['attribute_id'].' and status > -1 ');
        } else {
            $optionValue = $this->optionValueModel->find('attribute_id = '.$params['attribute_id'].' and status = 10 ');
        }
        
        $responseContent['data'] = $optionValue->toArray();
        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function listVariantOption()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;

        if (isset($params['show_status'])) {
            $optionValue = $this->variantOptionsModel->find('attribute_id = '.$params['attribute_id'].' and status > -1 ');
        } else {
            $optionValue = $this->variantOptionsModel->find('attribute_id = '.$params['attribute_id'].' and status = 10 ');
        }
        
        $responseContent['data'] = $optionValue->toArray();
        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updateOptionValue() {
        $params = $this->params;
        $attributeLib = new \Library\Attribute();
        
        $attributeLib->updateOption($params);
        // clear redis
        $this->optionValueModel->afterUpdate();
    
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($attributeLib->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $attributeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($attributeLib->getErrorCode()),
                "messages" => $attributeLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $responseContent['data'] = '';
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateVariantOption() {        
        $params = $this->params;
        $attributeLib = new \Library\Attribute();

        $attributeLib->updateVariantOption($params);
        // clear redis
        $this->variantOptionsModel->afterUpdate();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($attributeLib->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $attributeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($attributeLib->getErrorCode()),
                "messages" => $attributeLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $responseContent['data'] = '';
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function listUnitOptions()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $params = $this->params;
        $optionValue = $this->unitOptionsModel->find('attribute_unit_id = '.$params['attribute_unit_id'].' and status > -1 ');

        $responseContent['data'] = $optionValue->toArray();
        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updateUnitOptions() {
        $attributeLib = new \Library\Attribute();
        $attributeLib->updateUnitOptions($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($attributeLib->getErrorMessages())) {
            $responseContent['errors'] = array(
                "code" => $attributeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($attributeLib->getErrorCode()),
                "messages" => $attributeLib->getErrorMessages()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        } else {
            $responseContent['data'] = '';
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}