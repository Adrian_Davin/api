<?php
namespace Controllers;

/**
 * Class SkuController
 * @package Controllers
 */
class SkuController extends \Controllers\BaseController
{
    /**
     * @var $sku \Models\Sku
     */
    protected $sku;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->sku = new \Models\Sku();
    }

    public function saveSkuRequestAttempt()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $responseModel = $this->sku->saveSkuRequestAttempt($this->params);
            if(!empty($responseModel['errors'])) {
                $responseContent['errors'] = array(
                    "code" => "R100" ,
                    "title" => "Error Message",
                    "message" => $responseModel['errors']
                );
            } else {
                $responseContent['data'] = array(
                    'affected_rows' => $responseModel['data']['affected_rows']
                );
                $responseContent['messages'] = array("success");
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Save SKU Request Attempt Failed"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }    
}