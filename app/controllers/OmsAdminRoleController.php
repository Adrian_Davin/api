<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 13/1/2017
 * Time: 2:23 PM
 */

namespace Controllers;

/**
 * Class ControllerNameController
 * @package Controllers
 */
class OmsAdminRoleController extends \Controllers\BaseController
{
    /**
     * @var $marketingAdminRole \Models\OmsAdminRole
     */
    protected $omsAdminRole;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->omsAdminRole = new \Models\OmsAdminRole();
    }

    public function getRoleAttribute()
    {
        /**
         * params: role_id
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $result = $this->omsAdminRole->getRoleAttribute($this->params);
        if (isset($result['data'])) $response['data'] = $result['data'];
        if (isset($result['messages'])) $response['messages'] = $result['messages'];
        if (isset($result['error'])) $response['error'] = $result['error'];
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getRoleList()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $result = $this->omsAdminRole->find()->toArray();
        if (count($result) > 0) {
            $response['data'] = $result;
            $response['messages'] = "success";
        } else {
            $response['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get role list failed, please contact ruparupa tech support"
            );
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function saveAdminRole()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            // role_name check
            if (isset($this->params['role_name'])) {
                if (isset($this->params['role_id'])) {
                    $checkRoleName = $this->omsAdminRole->find("role_name = '" . $this->params['role_name'] . "' and role_id <> " . $this->params['role_id'])->toArray();
                } else {
                    $checkRoleName = $this->omsAdminRole->find("role_name = '" . $this->params['role_name'] . "'")->toArray();
                }

                if (count($checkRoleName) > 0) {
                    $responseContent['errors'] = array(
                        "code" => "R100",
                        "title" => "Error Message",
                        "messages" => "Role Name already in use, choose another one."
                    );
                } else {
                    $this->omsAdminRole->setFromArray($this->params);
                    $this->omsAdminRole->saveData("oms_admin_role");
                    if(!empty($this->omsAdminRole->getErrorCode())) {
                        $responseContent['errors'] = array(
                            "code" => $this->omsAdminRole->getErrorCode(),
                            "title" => $rupaResponse->getResponseDescription($this->omsAdminRole->getErrorCode()),
                            "message" => $this->omsAdminRole->getErrorMessages());
                    } else {
                        $responseContent['messages'] = "success";
                        $responseContent['data'] = array(
                            "role_id" => $this->omsAdminRole->getRoleId()
                        );
                    }
                }
            } else {
                $this->omsAdminRole->setFromArray($this->params);
                $this->omsAdminRole->saveData("oms_admin_role");
                if(!empty($this->omsAdminRole->getErrorCode())) {
                    $responseContent['errors'] = array(
                        "code" => $this->omsAdminRole->getErrorCode() ,
                        "title" => $rupaResponse->getResponseDescription($this->omsAdminRole->getErrorCode()),
                        "message" => $this->omsAdminRole->getErrorMessages());
                } else {
                    $responseContent['messages'] = "success";
                }
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Save oms admin role failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}