<?php


namespace Controllers;

/**
 * Class SalesruleController
 * @package Controllers
 * @RoutePrefix("/salesrule")
 *
 */
class SalesruleController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    /**
     * @var \Models\Salesrule
     */
    protected $salesruleModel;

    /**
     * @var \Models\SalesruleRegistrationPool
     */
    protected $salesruleRegistrationPoolModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->salesruleModel = new \Models\Salesrule();
        $this->salesruleActionModel = new \Models\SalesruleAction();
        $this->salesruleRegistrationPoolModel = new \Models\SalesruleRegistrationPool();
    }

    public function listSalesrule()
    {

        if (isset($this->params['company_code'])) {
            if ($this->params['company_code'] == "ODI") {
                unset($this->params['company_code']);
            }
        }

        $query = $this->salesruleModel->query();

        //$query->where("status >= 0");

        if (isset($this->params['date_active'])) {
            $query->andWhere("(to_date >= '". $this->params['date_active'] ." 00:00:00' AND from_date <= '". $this->params['date_active'] ." 23:59:59')");

            unset($this->params['date_active']);
        }

        if (isset($this->params['date_not_active'])) {
            $query->andWhere("DATE(to_date) < '". $this->params['date_not_active'] ."'");
            unset($this->params['date_not_active']);
        }

        if (isset($this->params['status'])) {
            $query->andWhere("status = " . $this->params['status']);

            unset($this->params['status']);
        }

        if (isset($this->params['company_code'])) {
            $query->andWhere("company_code like '%" . $this->params['company_code'] . "%'");

            unset($this->params['company_code']);
        }

        foreach($this->params as $key => $val)
        {
            $query->andWhere($key ." = '" . $val ."'");    
        }

        $query->orderBy("priority ASC");

        $salesrule = $query->execute();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $salesrule->toArray();
        $responseContent['errors'] = $this->salesruleModel->getErrors();
        $responseContent['messages'] = $this->salesruleModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function listAction()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $salesrule = $this->salesruleActionModel->find(array(' status > 0 ', 'order' => 'order_by'));

        $responseContent['data'] = $salesrule->toArray();
        $responseContent['errors'] = $this->salesruleActionModel->getErrors();
        $responseContent['messages'] = $this->salesruleActionModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function detailSalesrule()
    {
        /**
         * params: rule_id
         */

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $this->salesruleModel->setFromArray($this->params);

        $salesrule = $this->salesruleModel->findFirst(' rule_id = ' . $this->salesruleModel->getRuleId() . ' and status >= 0 ');
        $responseContent['data'] = $salesrule->toArray(array(),true);

        $salesruleGroup = \Models\SalesruleCustomerGroup::find("rule_id = " . $this->salesruleModel->getRuleId());
        $responseContent['data']['group'] = $salesruleGroup->toArray();

        $salesruleAction = \Models\SalesruleAction::findFirst("salesrule_action_id = " . $responseContent['data']['salesrule_action_id']);
        $responseContent['data']['salesrule_action'] = $salesruleAction->toArray();

        unset($responseContent['data']['salesrule_action_id']);

        $responseContent['errors'] = $this->salesruleModel->getErrors();
        $responseContent['messages'] = $this->salesruleModel->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function detailSalesruleAction()
    {
        /**
         * params: salesrule_action_id
         */

        $responseContent = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try{
            $salesruleAction = $this->salesruleActionModel->findFirst('salesrule_action_id = ' . $this->params['salesrule_action_id']);
            if ($salesruleAction) $responseContent['data'] = $salesruleAction->toArray(array(),true);
        }catch (\Exception $e){
            // do nothing
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function listSalesruleAction()
    {
        /**
         * params: salesrule_action_id
         */

        $responseContent = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try{
            $salesruleAction = $this->salesruleActionModel->find();
            if ($salesruleAction) $responseContent['data'] = $salesruleAction->toArray();
        }catch (\Exception $e){
            // do nothing
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveSalesrule()
    {
        $this->salesruleModel->setFromArray($this->params);
        $this->rupaResponse->setContentType("application/json");

        if ($this->params['rule_type'] == 'voucher' && $this->params['publish_description'] !== '' && isset($this->params['confirm_check']) && $this->params['confirm_check'] === 'true') {
            $return = $this->salesruleModel->checkVoucherQuantity();

            if (!$return['return']) {
                $response['errors'] = array("code" => "RR302",
                "title" => $this->rupaResponse->getResponseDescription("RR302"),
                "messages" => "Data not found");
                $response['messages'] = array("failed");
            }
            else {
                if ($return['check']) {
                    $response['data'] = array("true");
                    $response['messages'] = array("check success, voucher qty more than 1");
                    $response['errors'] = array();

                    $this->rupaResponse->setContent($response);
                    $this->rupaResponse->send();
                    return;
                }
            }
        }
       
        $return = $this->salesruleModel->saveSalesRule('sales_rule');

        if(!$return['return']) {
            $response['errors'] = array("code" => "RR301",
                "title" => $this->rupaResponse->getResponseDescription("RR301"),
                "messages" => "Save data failed");
            $response['messages'] = array("failed");
        } else {
            $flagLabelID = $flagLabelActionID = false;
            if (isset($this->params['label_id']) && ((!empty($this->params['label_id']) && $this->params['label_id'] > 0))) {            
                $flagLabelID = true;
            }

            if (isset($this->params['label_action_id']) && ((!empty($this->params['label_action_id']) && $this->params['label_action_id'] > 0))) {            
                $flagLabelActionID = true;
            }

            if ($flagLabelID || $flagLabelActionID) {
                $productLabelLib = new \Library\ProductLabels();
                $productLabelLib->syncWithMarketing($flagLabelID, $flagLabelActionID, $this->params);
            }

            $response['errors'] = array();
            $response['data'] = $return;
            $response['messages'] = array("success save your data");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function copySalesrule(){
        $ruleid = $this->params['rule_id'];
        $this->salesruleModel->setRuleId($ruleid);
        $response = $this->salesruleModel->copySalesRule();

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function saveSalesruleRegistrationPool()
    {

        try{
            $this->salesruleRegistrationPoolModel->setFromArray($this->params);
            $result = $this->salesruleRegistrationPoolModel->saveSalesruleRegistrationPool('salesrule_registration_pool');

            $this->rupaResponse->setContentType("application/json");
            if(!$result['return']) {
                $response['errors'] = array("code" => "RR301",
                    "title" => $this->rupaResponse->getResponseDescription("RR301"),
                    "messages" => "Save data failed");
                $response['messages'] = array("failed");
            } else {
                $response['errors'] = array();
                $response['data'] = $result;
                $response['messages'] = array("success save your data");
            }

            $this->rupaResponse->setContent($response);
            $this->rupaResponse->send();
        }catch (\Exception $e){
            $response['errors'] = array("error save salesrule registration pool ". print_r($e,1)) ;
            $response['data'] = array();
            $response['messages'] = array("Failed save your data");
            $this->rupaResponse->setContent($response);
            $this->rupaResponse->send();
        }
    }

    public function getSalesruleRegistrationPoolDetail()
    {
        /**
         * params: salesrule_registration_pool_id
         */
        try{
            $this->rupaResponse->setContentType("application/json");
            $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $this->salesruleRegistrationPoolModel->setFromArray($this->params);

            $salesrulePool = $this->salesruleRegistrationPoolModel->findFirst(' salesrule_registration_pool_id = ' . $this->salesruleRegistrationPoolModel->getSalesrule_registration_pool_id() . ' and status >= 0 ');

            if(empty($salesrulePool)){
                $response['errors'] = array("Salesrule Registration Pool Not Found");
                $response['data'] = array();
                $response['messages'] = array("Failed getting your data");

                $this->rupaResponse->setStatusCode(400, "Bad Request")->sendHeaders();
                $this->rupaResponse->setContent($response);
                $this->rupaResponse->send();
                return;
            }

            $responseContent['data'] = $salesrulePool->toArray(array(),true);
            $responseContent['errors'] = $this->salesruleRegistrationPoolModel->getErrors();
            $responseContent['messages'] = $this->salesruleRegistrationPoolModel->getMessages();

            $this->rupaResponse->setContent($responseContent);
            $this->rupaResponse->send();
        }catch (\Exception $e){
            $response['errors'] = array("error get salesrule registration pool detail ".print_r($e,1));
            $response['data'] = array();
            $response['messages'] = array("Failed getting your data");
            $this->rupaResponse->setContent($response);
            $this->rupaResponse->send();
        }


        
    }

    public function getSalesruleRegistrationPoolList()
    {
        /**
         * params: pooling_voucher_id
         */

        try{
            $this->rupaResponse->setContentType("application/json");
            $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $this->salesruleRegistrationPoolModel->setFromArray($this->params);
    
            $salesrulePool = $this->salesruleRegistrationPoolModel->find(' status >= 0 ');

            if(empty($salesrulePool)){
                $response['errors'] = array("No Salesrule Registration Pool Found");
                $response['data'] = array();
                $response['messages'] = array("Failed getting your data");

                $this->rupaResponse->setStatusCode(400, "Bad Request")->sendHeaders();
                $this->rupaResponse->setContent($response);
                $this->rupaResponse->send();
                return;
            }

            $responseContent['data'] = $salesrulePool;
            $responseContent['errors'] = $this->salesruleRegistrationPoolModel->getErrors();
            $responseContent['messages'] = $this->salesruleRegistrationPoolModel->getMessages();
    
            $this->rupaResponse->setContent($responseContent);
            $this->rupaResponse->send();
        }catch (\Exception $e){
            $response['errors'] = array("error get salesrule registration pool detail ". print_r($e,1));
            $response['data'] = array();
            $response['messages'] = array("Failed getting your data");
            $this->rupaResponse->setContent($response);
            $this->rupaResponse->send();
        }
    }
}