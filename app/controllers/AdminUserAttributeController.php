<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 13/1/2017
 * Time: 2:23 PM
 */

namespace Controllers;

/**
 * Class ControllerNameController
 * @package Controllers
 */
class AdminUserAttributeController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function saveAttribute()
    {
        $userAttributeModel = new \Models\AdminUserAttribute();
        $userAttributeModel->setFromArray($this->params);
        $userAttributeModel->saveData("user_attribute");

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($userAttributeModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $userAttributeModel->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($userAttributeModel->getErrorCode()),
                "messages" => $userAttributeModel->getErrorMessages());
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = "success";
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}