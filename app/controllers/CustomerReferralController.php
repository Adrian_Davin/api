<?php

namespace Controllers;

use \Library\CustomerReferral as CustomerReferralLib;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/customerreferral")
 *
 */
class CustomerReferralController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

       // Get Referral Reward 
       public function getCustomerReferralRewards()
       {
           $customerReferralLib = new \Library\CustomerReferral();
           $allCustomerReward = $customerReferralLib->getReferralRewardData($this->params);
   
           $rupaResponse = new \Library\Response();
           $rupaResponse->setContentType("application/json");
           if (empty($allCustomerReward)) {
               $response['errors'] = array(
                   "code" => $customerReferralLib->getErrorCode(),
                   "title" => $rupaResponse->getResponseDescription($customerReferralLib->getErrorCode()),
                   "messages" => $customerReferralLib->getErrorMessages()
               );
               $response['messages'] = array("failed");
           } else {
               $response['errors'] = array();
               $response['data'] = $allCustomerReward;
               $response['messages'] = array("success");
           }
   
           $rupaResponse->setContent($response);
           $rupaResponse->send();
       }
   
       // Get Customer Referral Link
       public function getCustomerReferralLink()
       {
           $customerReferralLib = new \Library\CustomerReferral();
           $CustomerReferralLink = $customerReferralLib->getCustomerReferralLink($this->params);
          
           $rupaResponse = new \Library\Response();
           $rupaResponse->setContentType("application/json");
           if (empty($CustomerReferralLink)) {
               $response['errors'] = array(
                   "code" => $customerReferralLib->getErrorCode(),
                   "title" => $rupaResponse->getResponseDescription($customerReferralLib->getErrorCode()),
                   "messages" => $customerReferralLib->getErrorMessages()
               );
               $response['messages'] = array("failed");
           } else {
               $response['errors'] = array();
               $response['data'] = $CustomerReferralLink;
               $response['messages'] = array("success");
           }
   
           $rupaResponse->setContent($response);
           $rupaResponse->send();
       }
   
       // Upsert Customer Referral Link
       public function upsertCustomerReferralLink()
       {
           $customerReferralLib = new \Library\CustomerReferral();
           $result = $customerReferralLib->upsertCustomerReferralLink($this->params);
   
           $rupaResponse = new \Library\Response();
           $rupaResponse->setContentType("application/json");
           $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
   
           if(!empty($customerReferralLib->getErrorCode())) {
               $responseContent['errors'] = array(
                   "code" => $customerReferralLib->getErrorCode() ,
                   "title" => $rupaResponse->getResponseDescription($customerReferralLib->getErrorCode()),
                   "messages" => $customerReferralLib->getErrorMessages());
               $responseContent['messages'] = array('save failed');
           } else {
               $responseContent['errors'] = array();
               $responseContent['data'] = $result;
               $responseContent['messages'] = array("success");
           }
   
           $rupaResponse->setContent($responseContent);
           $rupaResponse->send();
       }

       public function getCustomerReferralCode() {
           $customerReferralLib = new \Library\CustomerReferral();
           $customerReferralCode = $customerReferralLib->getCustomerReferralCode($this->params);
          
           $rupaResponse = new \Library\Response();
           $rupaResponse->setContentType("application/json");
           if (!$customerReferralCode || empty($customerReferralCode)) {
               $response['errors'] = array(
                   "code" => $customerReferralLib->getErrorCode(),
                   "title" => $rupaResponse->getResponseDescription($customerReferralLib->getErrorCode()),
                   "messages" => $customerReferralLib->getErrorMessages()
               );
               $response['messages'] = array("failed");
           } else {
               $response['errors'] = array();
               $response['data'] = [
                   "referral_code" => $customerReferralCode,
               ];
               $response['messages'] = array("success");
           }
   
           $rupaResponse->setContent($response);
           $rupaResponse->send();
       }
}