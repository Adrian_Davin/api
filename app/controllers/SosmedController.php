<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 05/09/2017
 * Time: 2:47 PM
 */
namespace Controllers;

/**
 * Class SosmedController
 * @package Controllers
 */
class SosmedController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function loginUrl()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        try {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $sosmedLib = new \Library\Sosmed();
            $data['login_url'] = $sosmedLib->loginUrl($this->params);
            $responseContent['data'] = $data;
            $responseContent['messages'] = "success";
        } catch (\Exception $e) {
            $rupaResponse->setStatusCode(500);
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get " .$this->params['type']." url params failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function authorizeLogin()
    {
        // because of OTP, this endpoint just return the value, not automatically login or register. Let otp flow do those things.
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        try {
            $sosmedLib = new \Library\Sosmed();
            $sosmedLib->authorizeLogin($this->params);
            if (!empty($sosmedLib->getErrorCode())) {
                $rupaResponse->setStatusCode(500, $sosmedLib->getErrorMessages())->sendHeaders();

                $responseContent['errors'] = array(
                    "code" => $sosmedLib->getErrorCode() ,
                    "title" => $rupaResponse->getResponseDescription($sosmedLib->getErrorCode()),
                    "messages" => $sosmedLib->getErrorMessages()
                );
            } else {
                $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
                $responseContent['errors'] = array();
                $responseContent['data'] = $sosmedLib->getUserParams();
                $responseContent['messages'] = array("success");
            }
        } catch (\Exception $e) {
            $rupaResponse->setStatusCode(500, "Login Failed")->sendHeaders();
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Login Failed"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}