<?php

namespace Controllers;


/**
 * Class ShipmentTrackingController
 * @package Controllers
 * @RoutePrefix("/shipment/tracking")
 *
 */
class ShipmentTrackingController extends \Controllers\BaseController
{

    /**
     * @var \Models\SalesShipmentTracking
     */
    protected $shipmentTracking;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->shipmentTracking = new \Models\SalesShipmentTracking();
    }

    public function getShipmentTrackingList()
    {
        /*
         * can use params: shipment_tracking_id, shipment_id
         */

        $salesShipmentLib = new \Library\SalesShipment();
        $allShipments = $salesShipmentLib->getAllShipmentTracking($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesShipmentLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesShipmentLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                "messages" => $salesShipmentLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Shipment tracking not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $allShipments;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getShipmentTrackingData()
    {
        /*
         * params: shipment_tracking_id
         */

        $salesShipmentLib = new \Library\SalesShipment();
        $shipment_tracking_id = !empty($this->params['shipment_tracking_id']) ? $this->params['shipment_tracking_id'] : "";
        $salesShipmentTrackingData = $salesShipmentLib->getSalesShipmentTrackingData($shipment_tracking_id);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesShipmentLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesShipmentLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                "messages" => $salesShipmentLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Shipment tracking not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $salesShipmentTrackingData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveShipmentTracking()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $this->shipmentTracking->setFromArray($this->params);
        $this->shipmentTracking->saveData("shipment_tracking");
        if(!empty($this->shipmentTracking->getErrorCode())) {
            $response['errors'] = array(
                "code" => $this->shipmentTracking->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($this->shipmentTracking->getErrorCode()),
                "message" => $this->shipmentTracking->getErrorMessages());
        } else {
            $response['messages'] = "success";
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getAwbTracking()
    {
        /*
         * can use params: invoice_no, awb_no
         */

        $salesShipmentTrackingModel = new \Models\SalesShipmentTracking();
        $response = $salesShipmentTrackingModel->getAwbTracking($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(count($response['errors']) > 0) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" =>  $response['errors']
            );
            $responseContent['messages'] = array("Shipment tracking not found");
            $responseContent['data'] = array();
        } else {
            if (count($response['data']) > 0) {
                $responseContent['messages'] = array("success");
                $responseContent['data'] =  $response['data'];
            } else {
                $responseContent['messages'] = array("Shipment tracking not found");
                $responseContent['data'] = $response['data'];
            }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}