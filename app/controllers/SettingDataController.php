<?php


namespace Controllers;

class SettingDataController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function settingDetail(){
        $settingModel = new \Models\SettingData();
        $result = $settingModel->getSettingDetail();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($settingModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $settingModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($settingModel->getErrorCode()),
                "messages" => $settingModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Setting data not found");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $result;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateSettingData(){
        $settingModel = new \Models\SettingData();
        $settingModel->setSettingFormArray($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($settingModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $settingModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($settingModel->getErrorCode()),
                "messages" => $settingModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to update setting data");
            $responseContent['data'] = array();
        } else {
            $settingModel->saveData();
            $responseContent['messages'] = array("success");
            $responseContent['data'] = "";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}