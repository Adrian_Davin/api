<?php


namespace Controllers;

use Helpers\ProductHelper;
use Library\Response;
use Models\Product;
use Models\ProductStock;
use Models\ProductSynonym;
use Models\ProductVariant;
use Models\ProductChangeLog;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class ProductController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
    }

    /**
     * @todo: Move to GO
     */
    public function updateProductPrice()
    {
        $dataList = ProductHelper::updateProductCustom($this->params,"product_price");

        $this->rupaResponse->setContentType("application/json");

        if(empty($dataList)) {
            $response['errors'] = array("code" => "RR302",
                "title" => $this->rupaResponse->getResponseDescription("RR302"),
                "messages" => "Data not found");
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $dataList;
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function searchProduct()
    {
        $productLib = new \Library\Product();
        $result = $productLib->search($this->params['type'], $this->params['term']);

        $this->rupaResponse->setContentType("application/json");
        if(!empty($productLib->getErrors())) {
            $response['errors'] = array("code" => "RR302",
                "title" => $this->rupaResponse->getResponseDescription("RR302"),
                "messages" => $productLib->getErrors());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $result;
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    /**
     * Recommendation starts here
     */
    public function recommendationDetail(){
        $productRecLib = new \Library\ProductRecommendation();
        $productRec = $productRecLib->getProductRecommendationDetail($this->params);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($productRecLib->getErrorCode())) {
            $response['errors'] = array("code" => $productRecLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productRecLib->getErrorCode()),
                "messages" => $productRecLib->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $productRec;
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function recommendationList(){
        $productRecLib = new \Library\ProductRecommendation();
        $productRec = $productRecLib->getProductRecommendationList($this->params);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($productRecLib->getErrorCode())) {
            $response['errors'] = array("code" => $productRecLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productRecLib->getErrorCode()),
                "messages" => $productRecLib->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $productRec;
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function categoryBreakDown(){

        $param = $this->params;

        $productLib = new \Library\Product();
        $productData = $productLib->getProduct2CatBreakDown($param);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($productLib->getErrorCode())) {
            $response['errors'] = array("code" => $productLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productLib->getErrorCode()),
                "messages" => $productLib->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $productData;
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function createRecommendation(){
        $productRecModel = new \Models\ProductRecommendation();
        $productRecModel->setFromArray($this->params);
        $result = $productRecModel->saveData();

        $this->rupaResponse->setContentType("application/json");

        if($result){
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }
        else{
            $response['errors'] = array("code" => $productRecModel->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productRecModel->getErrorCode()),
                "messages" => $productRecModel->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();

    }

    public function updateRecommendation(){
        $productRecModel = new \Models\ProductRecommendation();
        $productRecModel->setFromArray($this->params);
        $result = $productRecModel->saveData();

        $this->rupaResponse->setContentType("application/json");

        if($result){
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }
        else{
            $response['errors'] = array("code" => $productRecModel->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productRecModel->getErrorCode()),
                "messages" => $productRecModel->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();

    }

    /**
     * Recommendation ends here
     */

    public function getProductVariantStock()
    {
        $productStockModel = new \Models\ProductStock();
        //$productStockModel->setFromArray($this->params);
        $productStockModel->setSku($this->params['sku']);
        $this->params['company_code'] = !empty($this->params['company_code'])? $this->params['company_code'] : "ODI";
        $this->params['store_code'] = !empty($this->params['store_code'])? $this->params['store_code'] : null;
        $this->params['on_hand_only'] = !empty($this->params['on_hand_only'])? $this->params['on_hand_only'] : null;
        $this->params['device'] = !empty($this->params['device'])? $this->params['device'] : "";
        $this->params['business_unit'] = !empty($this->params['business_unit']) ? $this->params['business_unit'] : "";
        $this->params['is_product_scanned'] = !empty($this->params['is_product_scanned'])? $this->params['is_product_scanned'] : null;
        $this->params['source'] = !empty($this->params['source'])? $this->params['source'] : null;

        $result = $productStockModel->getProductStock($this->params['company_code'], $this->params['store_code'], $this->params['on_hand_only'], $this->params['device'], $this->params['business_unit'], $this->params['is_product_scanned'], $this->params['source']);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(!empty($productStockModel->getError())) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        } else if(empty($result)) {
            $rupaResponse->setStatusCode(404, "Not Found")->sendHeaders();
        }


        if(empty($result)) {
            $responseContent['errors'] = array("code" => "RR302", "title" => $rupaResponse->getResponseDescription("RR302"), "messages" => $productStockModel->getError());
            $responseContent['messages'] = array("");
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = $result;
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    /**
     * Function to update product stock (Batch)
     * @$this->params MUST contains : store_code, sku, qty, batch (as description about the batch and will filter log in requestparams.log)
     */
    public function updateBatchProductStock()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateBatchProductStock($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateBatchProductStockPromo1111()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateBatchProductStockPromo1111($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update product stock Batch For multiple store and sku
     * @$this->params MUST contains : store_code, sku, qty, batch (as description about the batch and will filter log in requestparams.log)
     */
    public function updateBatchProductStockMultipleStore()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateBatchProductStockMultipleStore($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update product stock (Batch by stock id)
     * @$this->params MUST contains : stock_id, qty, batch (as description about the batch and will filter log in requestparams.log)
     */
    public function updateBatchProductStockByStockId()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateBatchProductStockByStockId($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update dc stock to zero based on params sku
     * @$this->params MUST contains : sku
     */
    public function updateStockDcZero()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateStockDcZero($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Get sku list from table batch_dc_zero
     * No params needed
     */
    public function getSkuDcBatch()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->getSkuDcBatch($this->params);

        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getProductPriceBySKU()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductPrice();
        $responseModel = $productStockModel->getProductPriceBySKU($this->params);

        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Get sku list from table batch_store_zero
     * No params needed
     */
    public function getSkuStoreBatch()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->getSkuStoreBatch($this->params);

        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update all store stock to zero based on params sku
     * @$this->params MUST contains : sku
     */
    public function allStoreStockZero()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->allStoreStockZero($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update stock to 0 by condition of the price and qty
     * @$this->params MUST contains : price_operator (<, >=), price, qty_operator (<), qty
     */
    public function bufferByPriceAndQty()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->bufferByPriceAndQty($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function that reserved current stock for the order that the payment is pending and still active to be paid
     * No params needed
     */
    public function reservedStockPendingPayment()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateBatchReservedStock($productStockModel->getCurrentPendingPaymentQty());

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCountIsInStockTrue()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productVariantModel = new \Models\ProductVariant();
        $responseModel = $productVariantModel->getCountIsInStockTrue();

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getIsInStockTrue()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productVariantModel = new \Models\ProductVariant();
        $responseModel = $productVariantModel->getIsInStockTrue($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCountIsInStockFalse()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productVariantModel = new \Models\ProductVariant();
        $responseModel = $productVariantModel->getCountIsInStockFalse();

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getIsInStockFalse()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productVariantModel = new \Models\ProductVariant();
        $responseModel = $productVariantModel->getIsInStockFalse($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update sku is in stock to 0 / 1
     * No params needed
     */
    public function isInStock()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productVariantModel = new \Models\ProductVariant();
        $responseModel = $productVariantModel->isInStock($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update all DC Stock to Zero
     * No params needed
     */
    public function allDcStockZero()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productVariantModel = new \Models\ProductVariant();
        $responseModel = $productVariantModel->allDcStockZero();

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function that reserved current stock for the order that the shipment is pending
     * No params needed
     */
    public function reservedStockPendingShipment()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateBatchReservedStock($productStockModel->getCurrentPendingShipmentQty());

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     *  By : Aaron Putra
     *  Note Stock Go : Sudah tidak terpakai lagi, karena sudah digantikan update stock GO
     */
    public function saveProductVariantStock()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productLib = new \Library\Product();
        $productLib->updateProductStock($this->params);

        if(!empty($productLib->getErrors())) {
            $responseContent['errors'] = array(
                "code" => "RR002",
                "title" => $rupaResponse->getResponseDescription("RR202"),
                "messages" => $productLib->getErrors()
            );
            $responseContent['data'] = array();
            $responseContent['messages'] = array();
        }
        else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("Update stock success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /** End Here */

    public function deleteFromCache()
    {

        $productModel = new \Models\Product();
        $productModel->deleteFromCache($this->params['sku']);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($productModel->getErrorCode())) {
            $errorCode = $productModel->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $productModel->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to delete product cahce");
        } else {
            $responseContent['messages'] = array("success");
        }
        $responseContent['data'] = array();

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getFromCache()
    {
        $productModel = new \Models\Product();
        $productModel->setFromArray($this->params);
        $query = [
            'from' => 0,
            'size' => 50,
            "query" => [
                "bool" => [
                    "must" => [
                        "nested" => [
                            "path" => "variants",
                            "query" => [
                                "match" => [
                                    "variants.sku" => $this->params['sku']
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $result = $productModel->searchProduct("",$query);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($productModel->getErrorCode())) {
            $errorCode = $productModel->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $productModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Product not found");
        } else {
            $responseContent['messages'] = array("success");
        }

        if (!empty($result) && isset($result[0])) {
            $result = $result[0];
        }

        $responseContent['data'] = $result;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveLastSeenProduct()
    {
        $lastSeenLib = new \Library\LastSeen();
        $lastSeenLib->setLastSeen($this->params);

        if(empty($lastSeenLib->getErrorCode())) {
            $lastSeenLib->saveLastSeen();
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        if(!empty($lastSeenLib->getErrorCode())) {
            $errorCode = $lastSeenLib->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR100";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $lastSeenLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to save last seen product");
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $lastSeenLib->getDataArray();
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getLastSeenProduct()
    {
        $lastSeenLib = new \Library\LastSeen();
        $lastSeenLib->setLastSeen($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($lastSeenLib->getErrorCode())) {
            $errorCode = $lastSeenLib->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR100";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $lastSeenLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to save shopping cart");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $lastSeenLib->getDataArray();
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function getImportAttribute()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $errors = array();

        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $params = $this->params;

        /**
         * Get Attribute Set Id
         */
        $attributeSetModel = new \Models\AttributeSet();
        $dataAttributeSet = $attributeSetModel->findFirst(
            array(
                'conditions' => ' attribute_set_name = "'.substr($params['product_group'],0,10).'"'
            )
        );
        if(!$dataAttributeSet){
            $responseContent['data']['attribute_set_id'] = null;
            $errors['title'][] = array('attribute_set_id' => $rupaResponse->getResponseDescription('RR302'));
        }else{
            $responseContent['data']['attribute_set_id'] = $dataAttributeSet->toArray()['attribute_set_id'];
        }

        /**
         * ToDo : Get Supplier Id
         */
        $supplier_mapping = array(
            'A001' => '59',
            'H001' => '60',
            'H014' => '60',
            'T001' => '61',
            'B001' => '62',
            'K001' => '63',
            'S001' => '64',
            'L001' => '65',
            'O001' => '66'
        );
        if(isset($supplier_mapping[$params['sisco_code']])){
            $responseContent['data']['supplier']['supplier_id'] = $supplier_mapping[$params['sisco_code']];
        }else{
            $responseContent['data']['supplier']['supplier_id'] = null;
            $errors['title'][] = array('supplier_id' => $rupaResponse->getResponseDescription('RR302'));
        }

        /**
         * ToDo : Get Brand Id
         */
        $brandName = strtoupper(trim($params['brand']));

        $brandModel = new \Models\MasterBrand();
        $dataBrand = $brandModel->findFirst(
            array(
                'conditions' => ' UPPER(trim(name_old)) = "'.$brandName.'" '
            )
        );

        if(!$dataBrand){

            if(!empty($params['brand'])){

                /**
                 * Save Brand IF No Exist
                 */
                $brandModel->setName($brandName);
                $brandModel->setNameOld($brandName);
                $brandModel->saveData();

                if($brandModel->getBrandId()){
                    $responseContent['data']['brand']['brand_id'] = $brandModel->getBrandId();
                }else{

                    /**
                     * Failed Save Brand
                     */
                    $responseContent['data']['brand']['brand_id'] = null;
                    $errors['title'][] = array('brand_id' => $rupaResponse->getResponseDescription('RR301'));
                }

            }else{
                $responseContent['data']['brand']['brand_id'] = null;
                $errors['title'][] = array('brand_id' => $rupaResponse->getResponseDescription('RR302'));
            }
        }else{
            $statusBrand = $dataBrand->toArray()['status'];
            if($statusBrand > 0){
                $responseContent['data']['brand']['brand_id'] = $dataBrand->toArray()['brand_id'];
            }else{
                $responseContent['data']['brand']['brand_id'] = null;
                $errors['title'][] = array('brand_id' => $rupaResponse->getResponseDescription('RR105'));
            }

        }

        /**
         * Get Department Bu Id
         */
        $responseContent['data']['variant'][$params['sku']]['attribute_sap'] = array(
            'sku' => $params['sku']
        );

        /**
         * Get Attribute Sap
         */
        $attributeSapModel = new \Models\AttributeSap();
        $dataAttributeSap = $attributeSapModel->findFirst(
            array(
                'conditions' => 'sku = "'.$params['sku'].'"'
            )
        );

        if($dataAttributeSap){
            $responseContent['data']['variant'][$params['sku']]['attribute_sap']['attribute_sap_id'] = $dataAttributeSap->toArray()['attribute_sap_id'];
        }

        if(!empty($responseContent['data']['supplier']['supplier_id'])){
            $id_dept_bu = null;

            if($responseContent['data']['supplier']['supplier_id'] == 63){ // Krisbow
                $id_dept_bu = 49; // Default Dept Bu : Kriwsbow
            }/*elseif($responseContent['data']['supplier']['supplier_id'] == 64){
                $id_dept_bu = 109; // Default Dept Bu : Zwilling
            }*/else{
                $deptName = strtoupper(trim($params['department_bu']));
                $deptModel = new \Models\DepartmentBu();
                $dataDepartment = $deptModel->findFirst(
                    array(
                        'conditions' => ' supplier_id = '.$responseContent['data']['supplier']['supplier_id'].' and upper(trim(name)) = "'.$deptName.'"'
                    )
                );
                if(!$dataDepartment){
                    $id_dept_bu = null;
                    $errors['title'][] = array('department_bu_id' => $rupaResponse->getResponseDescription('RR302'));
                }else{
                    if($dataDepartment->toArray()['status'] > 0){
                        $id_dept_bu = $dataDepartment->toArray()['id_department_bu'];
                    }else{
                        $id_dept_bu = null;
                        $errors['title'][] = array('department_bu_id' => $rupaResponse->getResponseDescription('RR105'));
                    }
                }
            }
            $responseContent['data']['variant'][$params['sku']]['attribute_sap']['id_department_bu'] = $id_dept_bu;
            $responseContent['data']['variant'][$params['sku']]['attribute_sap']['vendor_source_list'] = $params['sisco_code'];
            $responseContent['data']['variant'][$params['sku']]['attribute_sap']['department_sisco_desc'] = $deptName;
        }

        /**
         * Get Categories
         */
        $categoryLib = new \Library\Category();
        $responseContent['data']['category'] = $categoryLib->categoryPath($params['product_group']);

        /**
         * ToDo : Get Warranty
         */
        $responseContent['data']['warranty_contributor'] = 'ruparupa';
        if(empty($params['warranty']) || $params['warranty'] == 40 ){
            $responseContent['data']['warranty_code'] = 'WUH000-WPH000-SVH000';
            $responseContent['data']['warranty_unit'] = 'No period';
            $responseContent['data']['warranty_part'] = 'No period';
            $responseContent['data']['warranty_services'] = 'No period';

        }else{

            $masterWarrantyModel = new \Models\MasterWarranty();
            $masterWarrantyData = $masterWarrantyModel->findFirst([
                'conditions' => ' srvc_agmt_code = '.$params['warranty']
            ]);

            if($masterWarrantyData){
                $warrantyData = $masterWarrantyData->toArray();
                $responseContent['data']['warranty_code'] = $warrantyData['srvc_agmt_no_formula'];
                $responseContent['data']['warranty_unit'] = $warrantyData['warranty_unit'];
                $responseContent['data']['warranty_part'] = $warrantyData['warranty_part'];
                $responseContent['data']['warranty_services'] = $warrantyData['warranty_service'];
            }

        }

        $responseContent['errors'] = $errors;
        $responseContent['messages'] = array();

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function getImportAttributeEs()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();
        $errors = array();

        $responseContent['errors'] = array();
        $responseContent['messages'] = array();

        $params = $this->params;

        /**
         * ToDo : Get Attribute Set Id
         */

        $productCategoryModel = new \Models\ProductCategory();
        $dataAttributeSet = $productCategoryModel->findFirst(
            array(
                'conditions' => ' article_hierarchy = "'.substr($params['product_group'],0,10).'"'
            )
        );
        if(!$dataAttributeSet){
            $responseContent['data']['attribute_set_id'] = 0;
            $responseContent['data']['attribute_variant_set_id'] = 0;
            $errors['title'][] = array('attribute_set_id' => $rupaResponse->getResponseDescription('RR302'));
        }else{
            $attributeSetID = (isset($dataAttributeSet->toArray()['attribute_set_id'])) ? $dataAttributeSet->toArray()['attribute_set_id'] : 0;
            $attributeVariantSetID = (isset($dataAttributeSet->toArray()['attribute_variant_set_id'])) ? $dataAttributeSet->toArray()['attribute_variant_set_id'] : 0;
            $responseContent['data']['attribute_set_id'] = (int)$attributeSetID;
            $responseContent['data']['attribute_variant_set_id'] = (int)$attributeVariantSetID;
        }

        /**
         * ToDo : Get Supplier Id
         */
        $supplier_mapping = array(
            'A001' => '59',
            'H001' => '60',
            'H014' => '60',
            'T001' => '61',
            'B001' => '62',
            'K001' => '63',
            'S001' => '64',
            'L001' => '65',
            'V001' => '161',
            'O001' => '66'
        );

        if(isset($params['sisco_code'])){
            if(isset($supplier_mapping[$params['sisco_code']])){
                $supplierModel = new \Models\Supplier();
                $supplier = $supplierModel->findFirst(array(
                    'columns' => 'supplier_id, supplier_code, supplier_alias, dikirim_oleh, status, logo, name, mp_type, mp_flag_shipment',
                    'conditions' => ' supplier_id = '.$supplier_mapping[$params['sisco_code']]
                ));

                $responseContent['data']['supplier']['supplier_id'] = (int)$supplier->toArray()['supplier_id'];
                $responseContent['data']['supplier']['supplier_code'] = $supplier->toArray()['supplier_code'];
                $responseContent['data']['supplier']['supplier_alias'] = $supplier->toArray()['supplier_alias'];
                $responseContent['data']['supplier']['dikirim_oleh'] = $supplier->toArray()['dikirim_oleh'];
                $responseContent['data']['supplier']['status'] = (int)$supplier->toArray()['status'];
                $responseContent['data']['supplier']['logo'] = $supplier->toArray()['logo'];
                $responseContent['data']['supplier']['name'] = $supplier->toArray()['name'];
                $responseContent['data']['supplier']['mp_type'] = (empty($supplier->toArray()['mp_type'])? "":($supplier->toArray()['mp_type'] == "null")? "":$supplier->toArray()['mp_type']);
                $responseContent['data']['supplier']['mp_flag_shipment'] = $supplier->toArray()['mp_flag_shipment'];

            }else{
                $responseContent['data']['supplier']['supplier_id'] = 0;
                $errors['title'][] = array('supplier_id' => $rupaResponse->getResponseDescription('RR302'));
            }
        }else{
            $responseContent['data']['supplier']['supplier_id'] = 0;
            $errors['title'][] = array('supplier_id' => $rupaResponse->getResponseDescription('RR302'));
        }


        /**
         * Get Detail Brand
         */

        $brandIdSap = strtoupper(trim($params['brand_id_sap']));

        $brandModel = new \Models\MasterBrand();

        $dataBrand = $brandModel->findFirst(
            array(
                'conditions' => ' UPPER(trim(extra_information)) = "'.$brandIdSap.'" '
            )
        );
        if($dataBrand){
            $responseContent['data']['brand']['brand_id'] = (int)$dataBrand->toArray()['brand_id'];
        }else{

            $brandName = strtoupper(trim($params['brand']));
            $dataBrand = $brandModel->findFirst(
                array(
                    'conditions' => ' UPPER(trim(name_old)) = "'.$brandName.'" '
                )
            );

            if(!$dataBrand){
                /**
                 * Save Brand IF No Exist
                 */

                $brandModel->setName($brandName);
                $brandModel->setNameOld($brandName);
                $brandModel->setExtraInformation($brandIdSap);
                $brandModel->setStatus(10);
                $brandModel->saveData();

                if($brandModel->getBrandId()){
                    $responseContent['data']['brand']['brand_id'] = (int)$brandModel->getBrandId();
                }else{

                    $responseContent['data']['brand']['brand_id'] = 0;
                    $errors['title'][] = array('brand_id' => $rupaResponse->getResponseDescription('RR301'));
                }

            }else{

                // update brand id sap
                $arr = $dataBrand->toArray();
                $arr['extra_information'] = $brandIdSap;
                $brandModel->setFromArray($arr);
                $brandModel->saveData('master_brand');

                $responseContent['data']['brand']['brand_id'] = (int)$brandModel->getBrandId();
            }
        }

        /**
         * Product Attributes
         */
        $attributes = array();
        if($responseContent['data']['attribute_set_id'] != 0){
            $masterAttributeLib = new \Library\MasterAttributes();
            $set2attribute = $masterAttributeLib->getListMasterAttributesOnSet(array('attribute_set_id' => $responseContent['data']['attribute_set_id']));
            $indexAttr = 0;
            foreach($set2attribute as $rowSet2Attr){
                $attributes[$indexAttr]['attribute_id'] = (int)$rowSet2Attr['attribute_id'];
                $attributes[$indexAttr]['attribute_value'] = "";
                $attributes[$indexAttr]['attribute_unit_value'] = "";
                $indexAttr ++;
            }
        }
        $responseContent['data']['attributes'] = $attributes;

        /**
         * ToDo : Get Department Bu Id
         */
        $responseContent['data']['variants'][0]['attribute_sap'] = array(
            'sku' => $params['sku']
        );

        /**
         * Attributes Variant
         */
        $attributesVariant = array();
        if($responseContent['data']['attribute_variant_set_id'] != 0){
            $masterAttributeLib = new \Library\MasterAttributes();
            $set2attribute = $masterAttributeLib->getListMasterAttributesOnVariantSet(array('attribute_variant_set_id' => $responseContent['data']['attribute_variant_set_id']));
            $indexAttr = 0;
            foreach($set2attribute as $rowSet2Attr){
                $attributesVariant[$indexAttr]['attribute_id'] = (int)$rowSet2Attr['attribute_id'];
                $attributesVariant[$indexAttr]['attribute_value'] = "";
                $indexAttr ++;
            }
        }
        $responseContent['data']['variants'][0]['attributes'] = $attributesVariant;

        if(!empty($responseContent['data']['supplier']['supplier_id'])){
            $id_dept_bu = 0;
            $name_dept_bu = $deptName = '';

            if($responseContent['data']['supplier']['supplier_id'] == 63){ // Krisbow
                $id_dept_bu = 49; // Default Dept Bu : Kriwsbow
                $name_dept_bu = 'KRISBOW';
            }elseif($responseContent['data']['supplier']['supplier_id'] == 161 || $responseContent['data']['supplier']['supplier_id'] == 65){ // KLV
                $deptName = strtoupper(trim($params['brand']));
                $deptModel = new \Models\DepartmentBu();
                $dataDepartment = $deptModel->findFirst(
                    array(
                        'conditions' => ' supplier_id = '.$responseContent['data']['supplier']['supplier_id'].' and upper(trim(name)) = "'.$deptName.'"'
                    )
                );
                if(!$dataDepartment){
                    $id_dept_bu = 0;
                    $errors['title'][] = array('department_bu_id' => $rupaResponse->getResponseDescription('RR302'));
                }else{
                    if($dataDepartment->toArray()['status'] > 0){
                        $id_dept_bu = $dataDepartment->toArray()['id_department_bu'];
                        $name_dept_bu = $dataDepartment->toArray()['name'];
                    }else{
                        $id_dept_bu = 0;
                        $errors['title'][] = array('department_bu_id' => $rupaResponse->getResponseDescription('RR105'));
                    }
                }
            }else{
                $deptName = strtoupper(trim($params['department_bu']));
                $deptModel = new \Models\DepartmentBu();
                $dataDepartment = $deptModel->findFirst(
                    array(
                        'conditions' => ' supplier_id = '.$responseContent['data']['supplier']['supplier_id'].' and upper(trim(name)) = "'.$deptName.'"'
                    )
                );
                if(!$dataDepartment){
                    $id_dept_bu = 0;
                    $errors['title'][] = array('department_bu_id' => $rupaResponse->getResponseDescription('RR302'));
                }else{
                    if($dataDepartment->toArray()['status'] > 0){
                        $id_dept_bu = $dataDepartment->toArray()['id_department_bu'];
                        $name_dept_bu = $dataDepartment->toArray()['name'];
                    }else{
                        $id_dept_bu = 0;
                        $errors['title'][] = array('department_bu_id' => $rupaResponse->getResponseDescription('RR105'));
                    }
                }
            }
            $responseContent['data']['variants'][0]['attribute_sap']['department_bu'] = (int)$id_dept_bu;
            $responseContent['data']['variants'][0]['attribute_sap']['department_name'] = $name_dept_bu;
            $responseContent['data']['variants'][0]['attribute_sap']['vendor_source_list'] = $params['sisco_code'];
            $responseContent['data']['variants'][0]['attribute_sap']['department_sisco_desc'] = $deptName;
        }


        /**
         * ToDo : Get Categories
         */
        $productGroup = $productCategoryModel->findFirst(
            array(
                'conditions' => ' article_hierarchy = "'.$params['product_group'].'"'
            )
        );
        if($productGroup){
            $categoryPath = explode('/',$productGroup->toArray()['path']);
            $indexCat = 1;
            foreach($categoryPath as $rowCat){
                $arrCat = array(
                    'category_id' => (int)$rowCat,
                    'breadcrumb_position' => $indexCat,
                    'priority' => 10000,
                    'is_in_stock' => 1
                );
                if(isset($params['update'])){
                    if($params['update'] == TRUE){
                        $categoryData = $productCategoryModel->findFirst(
                            array(
                                'conditions' => ' category_id = '.(int)$rowCat
                            )
                        );
                        $arrCat['name'] = $categoryData->toArray()['name'];
                        
                        // get url key
                        $masterUrlKeyModel = new \Models\MasterUrlKey();
                        $masterUrlKeyData = $masterUrlKeyModel->findFirst(
                            array(
                                'conditions' => 'reference_id = "'.$rowCat.'" and section = "category" and status = 10',
                                'order' => 'created_at DESC'
                            )
                        );

                        if($masterUrlKeyData){
                            $arrCat['url_key'] = $masterUrlKeyData->toArray()['url_key'].'.html';
                        }else{
                            $arrCat['url_key'] = "";
                        }
                    }
                }
                $responseContent['data']['categories'][] = $arrCat;
                $indexCat ++;
            }
        }

        /**
         * ToDo : Get Categories AHI
         */
        $productGroup = $productCategoryModel->findFirst(
            array(
                'conditions' => ' article_hierarchy = "'.$params['product_group_bu'].'"'
            )
        );
        if($productGroup){
            $categoryPath = explode('/',$productGroup->toArray()['path']);
            $indexCat = 1;
            foreach($categoryPath as $rowCat){
                $arrCat = array(
                    'category_id' => (int)$rowCat,
                    'breadcrumb_position' => $indexCat,
                    'priority' => 10000,
                    'is_in_stock' => 1
                );
                if(isset($params['update'])){
                    if($params['update'] == TRUE){
                        $categoryData = $productCategoryModel->findFirst(
                            array(
                                'conditions' => ' category_id = '.(int)$rowCat
                            )
                        );
                        $arrCat['name'] = $categoryData->toArray()['name'];
                        
                        // get url key
                        $masterUrlKeyModel = new \Models\MasterUrlKey();
                        $masterUrlKeyData = $masterUrlKeyModel->findFirst(
                            array(
                                'conditions' => 'reference_id = "'.$rowCat.'" and section = "category" and status = 10',
                                'order' => 'created_at DESC'
                            )
                        );

                        if($masterUrlKeyData){
                            $arrCat['url_key'] = $masterUrlKeyData->toArray()['url_key'].'.html';
                        }else{
                            $arrCat['url_key'] = "";
                        }
                    }
                }

                $responseContent['data']['categories_AHI'][] = $arrCat;
                $indexCat ++;
            }
        }

        /**
         * ToDo : Get Warranty
         */
        $responseContent['data']['warranty']['warranty_contributor'] = 'ruparupa';
        if(empty($params['warranty']) || $params['warranty'] == 40 ){
            $responseContent['data']['warranty']['warranty_code'] = 'WUH000-WPH000-SVH000';
            $responseContent['data']['warranty']['warranty_unit'] = 'No period';
            $responseContent['data']['warranty']['warranty_part'] = 'No period';
            $responseContent['data']['warranty']['warranty_services'] = 'No period';

        }else{
            $masterWarrantyModel = new \Models\MasterWarranty();

            $masterWarrantyData = $masterWarrantyModel->findFirst([
                'conditions' => ' srvc_agmt_code = "'.$params['warranty'].'"'
            ]);

            if(!$masterWarrantyData){
                $responseContent['data']['warranty']['warranty_code'] = 'WUH000-WPH000-SVH000';
                $responseContent['data']['warranty']['warranty_unit'] = 'No period';
                $responseContent['data']['warranty']['warranty_part'] = 'No period';
                $responseContent['data']['warranty']['warranty_services'] = 'No period';    
            }else{
                $warrantyData = $masterWarrantyData->toArray();
                $responseContent['data']['warranty']['warranty_code'] = $warrantyData['srvc_agmt_no_formula'];
                $responseContent['data']['warranty']['warranty_unit'] = $warrantyData['warranty_unit'];
                $responseContent['data']['warranty']['warranty_part'] = $warrantyData['warranty_part'];
                $responseContent['data']['warranty']['warranty_services'] = $warrantyData['warranty_service'];
            }
        }

        $responseContent['errors'] = $errors;
        $responseContent['messages'] = array();

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function getProdctStockInLocation()
    {

        $sku = isset($this->params['sku']) ? $this->params['sku'] : "";
        $kecamatan_code = isset($this->params['kecamatan_code'])? $this->params['kecamatan_code'] : "";
        $qty_ordered = isset($this->params['qty_ordered'])? $this->params['qty_ordered'] : 1;
        $company_code = isset($this->params['company_code'])? $this->params['company_code'] : "ODI";

        $productLib = new \Library\Product();
        $isCanDelivery = $productLib->isProductCanDeliveryToLocation($sku, $kecamatan_code, $qty_ordered, $company_code);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!$isCanDelivery['can_delivery']) {
            $rupaResponse->setStatusCode(404, $rupaResponse->getResponseDescription("404"))->sendHeaders();

            $responseContent['errors'] = array(
                "code" => "404",
                "title" => $rupaResponse->getResponseDescription("404"),
                "messages" => "Stock not found"
            );
            $responseContent['messages'] = array("failed");
            $responseContent['data'] = array();
        } else {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['errors'] = array();
            $responseContent['data'] = $isCanDelivery;
            $responseContent['messages'] = "success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    // This function is already unmaintain, moved to go-stock
    public function getMaximumStock()
    {
        $sku = isset($this->params['sku']) ? $this->params['sku'] : "";
        $kecamatan_code = isset($this->params['kecamatan_code'])? $this->params['kecamatan_code'] : getenv('DEFAULT_SHIPPING_KECAMATAN_CODE');
        $company_code = isset($this->params['company_code'])? $this->params['company_code'] : "ODI";
        $store_code = isset($this->params['store_code'])? $this->params['store_code'] : "";
        $on_hand_only = isset($this->params['on_hand_only'])? $this->params['on_hand_only'] : 0;
        $device = isset($this->params['device'])? $this->params['device'] : '';
        $business_unit = isset($this->params['business_unit'])? $this->params['business_unit'] : '';
        $is_product_scanned = isset($this->params['is_product_scanned'])? $this->params['is_product_scanned'] : 0;
        $source = isset($this->params['source'])? $this->params['source'] : "";

        $productLib = new \Library\Product();
        $maximumStock = $productLib->getMaximumStock($sku, $kecamatan_code,$company_code, $store_code, $on_hand_only, $device, $business_unit, $is_product_scanned, $source);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if(!empty($productLib->getErrorCode())) {
            $rupaResponse->setStatusCode(404, $rupaResponse->getResponseDescription("404"))->sendHeaders();

            $responseContent['errors'] = array(
                "code" => "404",
                "title" => $rupaResponse->getResponseDescription("404"),
                "messages" => "Max stock not found"
            );
            $responseContent['messages'] = array("failed");
            $responseContent['data'] = array();
        } else {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
            $responseContent['errors'] = array();
            $responseContent['data'] = ["max_stock" => $maximumStock];
            $responseContent['messages'] = "success";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * This function used for generate remarketing file
     */
    public function getDataRemarketing()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $productModel = new \Models\Product();
            $response = $productModel->getDataRemarketing();
            if(isset($response['error'])) {
                $responseContent['errors'] = $response['error'];
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $response['data'];
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => $e->getMessage()
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Synonym
     */

    public function createSynonym(){
        $productSynModel = new ProductSynonym();
        $productSynModel->setFromArray($this->params);
        $result = $productSynModel->saveData('synonym');

        $this->rupaResponse->setContentType("application/json");

        if($result){
            $productSynLib = new \Library\ProductSynonym();
            $productSynLib->saveSynonymsFile();

            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }
        else{
            $response['errors'] = array("code" => $productSynModel->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productSynModel->getErrorCode()),
                "messages" => $productSynModel->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();

    }
    public function synonymDetail(){
        $productSynLib = new \Library\ProductSynonym();
        $productSyn = $productSynLib->getProductSynonymDetail($this->params);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($productSynLib->getErrorCode())) {
            $response['errors'] = array("code" => $productSynLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productSynLib->getErrorCode()),
                "messages" => $productSynLib->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $productSyn;
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function synonymList(){
        $productSynLib = new \Library\ProductSynonym();
        $productSynonym = $productSynLib->getProductSynonymList($this->params);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($productSynLib->getErrorCode())) {
            $response['errors'] = array("code" => $productSynLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productSynLib->getErrorCode()),
                "messages" => $productSynLib->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $productSynonym;
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function updateSynonym(){
        $productSynModel = new \Models\ProductSynonym();
        $productSynModel->setFromArray($this->params);
        $result = $productSynModel->saveData();

        $this->rupaResponse->setContentType("application/json");

        if($result){
            $productSynLib = new \Library\ProductSynonym();
            $productSynLib->saveSynonymsFile();

            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }
        else{
            $response['errors'] = array("code" => $productSynModel->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productSynModel->getErrorCode()),
                "messages" => $productSynModel->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();

    }

    /**
     * Synonym ends here
     */

    public function populateSiscoProducts(){
        $productLib = new \Library\Product();
        $result = $productLib->populateProducts();

        $this->rupaResponse->setContentType("application/json");

        if($result){
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = $result;
        }
        else{
            $response['errors'] = array("code" => $productLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productLib->getErrorCode()),
                "messages" => $productLib->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    //revert stock
    public function revertStocks(){
        $productLib = new \Library\Product();
        $productLib->revertStock($this->params);

        $this->rupaResponse->setContentType("application/json");

        if(empty($productLib->getErrorCode())) {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }
        else{
            $response['errors'] = array("code" => $productLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productLib->getErrorCode()),
                "messages" => $productLib->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function getAllCategoryStockMapping(){
        $productLib = new \Library\Product();
        $arrIsInStock = $productLib->getAllCategoryStockMapping();
        $this->rupaResponse->setContentType("application/json");

        if(empty($productLib->getErrorCode())){
            $response['errors'] = array();
            $response['data'] = $arrIsInStock;
            $response['messages'] = array("Success");
        }
        else{
            $response['errors'] = array("code" => $productLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productLib->getErrorCode()),
                "messages" => $productLib->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function categoryStockMapping(){
        $productLib = new \Library\Product();
        $productLib->categoryStockMapping($this->params);

        $this->rupaResponse->setContentType("application/json");

        if(empty($productLib->getErrorCode())){
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("Success");
        }
        else{
            $response['errors'] = array("code" => $productLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productLib->getErrorCode()),
                "messages" => $productLib->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function updateQtyBk()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateQtyBk($this->params);

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateStockManual()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $productStockModel = new \Models\ProductStock();
        $responseModel = $productStockModel->updateStockManual();

        $responseContent['messages'] = $responseModel['messages'];
        $responseContent['errors'] = $responseModel['errors'];
        $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function changeStoreCode(){
        $stockModel = new ProductStock();
        $stockModel->changStoreCode($this->params);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($stockModel->getErrorCode())) {
            $response['errors'] = array("code" => $stockModel->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($stockModel->getErrorCode()),
                "messages" => $stockModel->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function changeStockStatus()
    {
        $sku = !empty($this->params['sku']) ? $this->params['sku'] : '';
        $product = new \Library\Product();
        $product->checkIsInStock($sku);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($product->getErrorCode())) {
            $response['errors'] = array("code" => $product->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($product->getErrorCode()),
                "messages" => $product->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function changeCategoryStockStatus()
    {
        $product = new \Library\Product();
        $product->checkCategoryIsInStock($this->params);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($product->getErrorCode())) {
            $response['errors'] = array("code" => $product->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($product->getErrorCode()),
                "messages" => $product->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function productChangeLog(){
        $productChangeLog = new ProductChangeLog();
        $productChangeLog->setFromArray($this->params);
        $result = $productChangeLog->saveData('product_change_log');

        $this->rupaResponse->setContentType("application/json");

        if(empty($productChangeLog->getErrors())){
            $productSynLib = new \Library\ProductSynonym();
            $productSynLib->saveSynonymsFile();

            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }
        else{
            $response['errors'] = array("code" => $productChangeLog->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productChangeLog->getErrorCode()),
                "messages" => $productChangeLog->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function productChangeLogDetail(){
        $productChangeLog = new ProductChangeLog();
        $productChangeLog->setFromArray($this->params);
        $result = $productChangeLog->productChangeLogDetail('product_change_log');

        $this->rupaResponse->setContentType("application/json");

        if(empty($productChangeLog->getErrors())){
            $productSynLib = new \Library\ProductSynonym();
            $productSynLib->saveSynonymsFile();

            $response['errors'] = array();
            $response['data'] = $productChangeLog->getData();
            $response['messages'] = array("success");
        }
        else{
            $response['errors'] = array("code" => $productChangeLog->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productChangeLog->getErrorCode()),
                "messages" => $productChangeLog->getErrorMessages());
            $response['messages'] = array("failed");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

    public function getProductHandlingFeeAdjust(){
        $productLib = new \Library\Product(); 
        
        $result = $productLib->getProductHandlingFeeAdjust($this->request);

        $this->rupaResponse->setContentType("application/json");

        if(!empty($productLib->getErrorCode())) {
            $response['errors'] = array("code" => $productLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($productLib->getErrorCode()),
                "messages" => $productLib->getErrorMessages());
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $result;
            $response['messages'] = array("success");
        }

        $this->rupaResponse->setContent($response);
        $this->rupaResponse->send();
    }

}