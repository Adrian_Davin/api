<?php
namespace Controllers;

/**
 * Class KawanlamaController
 * @package Controllers
 */
class KawanlamaController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function pickupVoucher()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createPickupVoucher();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());

            if (!empty($kawanLamaSystemLib->voucherId)) {
                $response['messages'][] = $kawanLamaSystemLib->voucherId;
            } else {
                $response['messages'] = [];
            }

        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function pickupJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('pickup');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function salesJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('sales');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function salesB2BTopJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('sales_b2b_top');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function pickupReverseJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('pickupReverse');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = "success";
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function incomingJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('incoming');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function incomingB2BTopJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('incoming_b2b_top');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function incomingReverseJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('incomingReverse');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = "success";
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function mdrJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('mdr');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function mdrReverseJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal('mdrReverse');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = "success";
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function createMdr()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        
        $salesOrderModel = new \Models\SalesOrder();
        $salesOrder = $salesOrderModel->findFirst("order_no = '".$this->params['order_no']."'");
        if($salesOrder){
            $salesOrderArray = $salesOrder->toArray();
            $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
            $salesOrderItemMdr = $salesOrderItemMdrModel->findFirst("sales_order_id = ".$salesOrderArray['sales_order_id']);
            if(!$salesOrderItemMdr){
                $shoppingCart = new \Models\Cart();
                $shoppingCart->setCartId($salesOrderArray['cart_id']);
                $cartData = $shoppingCart->loadCartData(true);
                if(count($cartData) > 0){
                    $salesOrderModel = new \Models\SalesOrder();
                    $cartData['cart_id'] = $salesOrderArray['cart_id'];
                    $cartData['check_status_cart'] = false;
                    $salesOrderModel->setFromShoppingCart($cartData);
                    $salesOrderModel->setCartRaw($cartData);
                    $salesOrderData = $salesOrderModel;
        
                    // Set ID
                    $salesOrderItemModel = new \Models\SalesOrderItem();
                    $salesOrderItem = $salesOrderItemModel->find("sales_order_id = ".$salesOrderArray['sales_order_id']);
                    if($salesOrderItem){
                        $salesOrderItemArray = $salesOrderItem->toArray();
                        foreach($salesOrderData->items as $rowItem){
                            $rowItem->sales_order_id = $salesOrderArray['sales_order_id'];
                            foreach($salesOrderItemArray as $dataItem){
                                if(($dataItem['sku'] == $rowItem->sku) && ($dataItem['is_free_item'] == $rowItem->is_free_item)){
                                    $rowItem->sales_order_item_id = $dataItem['sales_order_item_id'];
                                }
                            }
                        }
        
                        // Calculate MDR
                        $mdrModel = new \Library\Finance($salesOrderData);
                        $mdrItems = $mdrModel->calculateMDR();
                        if (count($mdrItems) > 0) {
                            foreach ($mdrItems as $row) {
                                $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
                                $salesOrderItemMdrModel->setFromArray($row);
                                $salesOrderItemMdrModel->save('sales_order_item_mdr');
                            }
                        }else{
                            $error = [
                                "errorCode" => "RR106",
                                "errorMessages" => ["Failed Create MDR : ".$this->params['order_no']]
                            ];
                            $kawanLamaSystemLib->setFromArray($error);    
                        }
                    }else{
                        $error = [
                            "errorCode" => "RR106",
                            "errorMessages" => ["Sales Order Item Not Found base on sales oder id : ".$salesOrderArray['sales_order_id']]
                        ];
                        $kawanLamaSystemLib->setFromArray($error);    
                    }
                    
                }else{
                    $error = [
                        "errorCode" => "RR106",
                        "errorMessages" => ["Cart ID : ".$salesOrderArray['cart_id']." not Found"]
                    ];
                    $kawanLamaSystemLib->setFromArray($error);
                }
            }else{
                $error = [
                    "errorCode" => "RR102",
                    "errorMessages" => ["Data MDR for Order No : ".$salesOrderArray['order_no']." already exists"]
                ];
                $kawanLamaSystemLib->setFromArray($error);
            }
            
        }else{
            $error = [
                "errorCode" => "RR106",
                "errorMessages" => ["Order No : ".$this->params['order_no']." not Found"]
            ];
            $kawanLamaSystemLib->setFromArray($error);
        }
        
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $rupaResponse->setStatusCode(500, "Not Found")->sendHeaders();
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function giftCardJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal("reclass");

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function giftCardReverseJournal()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal("reclassReverse");

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = "success";
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function createSoSAP()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);

        //cek if order type b2b or b2c
        $invoice = new \Models\SalesInvoice();
        $result = $invoice->findFirst("invoice_no = '". $this->params['invoice_no'] ."'");
        if($result->SalesOrder->getOrderType()=='B2B') {
            $kawanLamaSystemLib->createSoB2B();
            // $kawanLamaSystemLib->createSoCIB2B();
        }else{
            $kawanLamaSystemLib->createSoB2C();
            // $kawanLamaSystemLib->createSoCIB2C();
        }
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function createSoCISAP()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);

        //cek if order type b2b or b2c
        $invoice = new \Models\SalesInvoice();
        $result = $invoice->findFirst("invoice_no = '". $this->params['invoice_no'] ."'");
        
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $storeCode = $result->getStoreCode();
        if (substr($storeCode, 0, 4) == "1000") {
            $response['errors'] = array(
                "code" => "RR100" ,
                "title" => "Error Message",
                "messages" => "Store Code with prefix 1000 not accepted for create SO SAP");

            $response['messages'] = array();
        } else {
            if($result->SalesOrder->getOrderType()=='B2B' || $result->SalesOrder->getOrderType() == 'b2b_informa' || $result->SalesOrder->getOrderType() == "b2b_ace") {
                $kawanLamaSystemLib->createSoCIB2B();
            }else{
                $kawanLamaSystemLib->createSoCIB2C();
            }
            
            if(!empty($kawanLamaSystemLib->getErrorCode())) {
                $response['errors'] = array(
                    "code" => $kawanLamaSystemLib->getErrorCode() ,
                    "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                    "messages" => $kawanLamaSystemLib->getErrorMessages());
                $response['messages'] = array();
            } else {
                $response['errors'] = array();
                $response['data'] = [];
                $response['messages'] = array("success");
            }
        }        

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function createSoRegisterMember()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);

        //cek if order type b2b or b2c
        $invoice = new \Models\SalesInvoice();
        $result = $invoice->findFirst("invoice_no = '". $this->params['invoice_no'] ."'");
        if($result){
            $storeCode = $result->getStoreCode();
            if($storeCode == 'A300' || $storeCode == 'H300' || $storeCode == 'H203') {
                $kawanLamaSystemLib->createSoRegisterMember();
            }else{
                $kawanLamaSystemLib->setErrorCode("400");
                $kawanLamaSystemLib->setErrorMessages("Store Code not eligible for create SO SAP Register Member. It should be A300, H300, or H203");
            }
        }else{
            $kawanLamaSystemLib->setErrorCode("404");
            $kawanLamaSystemLib->setErrorMessages("Invoice Tidak Ditemukan");
        }
       
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function createJournalRefund()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $kawanLamaSystemLib->setFromArray($this->params);
        $kawanLamaSystemLib->createJournal("refund");

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if(!empty($kawanLamaSystemLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" => $kawanLamaSystemLib->getErrorMessages());
            $response['messages'] = array();
        } else {
            $response['errors'] = array();
            $response['data'] = [];
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function checkStatusMember()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $KLRequest = $kawanLamaSystemLib->checkStatusMember($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            $response['errors'] = array();
            $response['data'] = json_decode($KLRequest);
            $response['messages'] = array("success");

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function checkPointMember()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $pointResp = $kawanLamaSystemLib->checkPointMember($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $response['errors'] = array();
        $response['data'] = $pointResp;
        $response['messages'] = array("success");

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function checkMemberData()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $checkResult = $kawanLamaSystemLib->checkMemberData($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if (empty($checkResult)) {
            $rupaResponse->setHeaderStatus($kawanLamaSystemLib->getErrorCode());
            $response['errors'] = array(
                "code" => $kawanLamaSystemLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($kawanLamaSystemLib->getErrorCode()),
                "messages" =>  $kawanLamaSystemLib->getErrorMessages()
            );
            $response['messages'] = "failed";
        } else {
            $rupaResponse->setHeaderStatus(200);
            $response['errors'] = array();
            $response['data'] = $checkResult;
            $response['messages'] = "success";
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }
        
    public function checkAceMemberData()
    {
        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
        $KLRequest = $kawanLamaSystemLib->checkAceMemberData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (empty($KLRequest)) {
            $response['errors'] = array(
                "code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Member Not Found"
            );
            $response['messages'] = array("result" => "failed");
        } else {
            $MemberData = json_decode($KLRequest, true);
            if(empty($MemberData['rows']) && $MemberData['is_ok'] == 'false'){
                $response['errors'] = array();
                $response['data'] = $MemberData['rows'];
                $response['messages'] = array("result" => "Member Not Found");
            } else {
                $response['errors'] = array();
                $response['data'] = $MemberData['rows'];
                $response['messages'] = array("result" => "success");
            }
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function loginStore() {
        $kLib = new \Library\KawanLamaSystem();
        $loginResult = $kLib->loginStore($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (empty($loginResult)) {
            $response['errors'] = array(
                "code" => $kLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($kLib->getErrorCode()),
                "messages" => $kLib->getErrorMessages()
            );
            $response['messages'] = array("error");
        } else {
            $response['errors'] = array();
            $response['data'] = $loginResult;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

}