<?php


namespace Controllers;

use Helpers\ProductHelper;
use Library\Nsq;
use Models\Event;
use Models\ProductLabels;

class ProductLabelsController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();

    }

    public function createLabel(){
        $labelModel = new \Models\ProductLabels();
        $labelModel->setLabelFromArray($this->params);
        $labelModel->saveData('create_label');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelModel->getErrorCode()),
                "messages" => $labelModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create label");
            $responseContent['data'] = array();
        } else {

            $data = array(
                'product_label_id' => $labelModel->getProductLabelId(),
                'priority' => $labelModel->getProductLabelId()
            );
            
            $updatePriority = new \Models\ProductLabels();
            $updatePriority->setLabelFromArray($data);
            $updatePriority->saveData('update_label_priority');

            if(!empty($updatePriority->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $updatePriority->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($updatePriority->getErrorCode()),
                    "messages" => $updatePriority->getErrorMessages()
                );
                $responseContent['messages'] = array("Failed to update label priority");
                $responseContent['data'] = array();
            } else {
                $responseContent['errors'] = array();
                $responseContent['messages'] = array("Success updating label priority");
                $responseContent['data'] = "";
            }

            // $products = $labelModel->getSku();
            // $update = ProductHelper::compareProducts($products);

            // if($update){
            //     $responseContent['messages'] = array("Success creating label");
            //     $responseContent['data'] = "";
            // }
            // else{
            //     $responseContent['messages'] = array("Failed creating label");
            //     $responseContent['data'] = "";
            // }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function labelList(){
        $labelLib = new \Library\ProductLabels();
        $labelLib->setParams($this->params);
        $labelList = $labelLib->getAllLabelList();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to load labels");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $labelList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getURLCloudinaryList(){
        $labelLib = new \Library\ProductLabels();
        $labelLib->setParams($this->params);
        $labelList = $labelLib->getURLCloudinaryList();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to load url cloudinary");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $labelList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function labelListExpired(){

        $labelLib = new \Library\ProductLabels();
        $labelList = $labelLib->getAllLabelListExpired();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to load labels");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $labelList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();


    }

    public function getAcePromoExclude(){
        $labelLib = new \Library\ProductLabels();
        $labelList = $labelLib->getAcePromoExclude();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to load labels");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $labelList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();


    }

    public function getAcePromo(){
        $labelLib = new \Library\ProductLabels();
        $labelList = $labelLib->getAcePromo();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to load labels");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success");
            $responseContent['data'] = $labelList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();


    }

    public function insertAcePromo(){
        $labelLib = new \Library\ProductLabels();

        $result = $labelLib->saveAcePromo($this->params[0]['sku']);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to Insert Ace Promo");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success Insert Ace Promo");
            $responseContent['data'] = "";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function labelDetail(){
        $labelLib = new \Library\ProductLabels();

        $labelData = $labelLib->getLabelDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get label details");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $labelData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function labelDetailByName(){
        $labelLib = new \Library\ProductLabels();

        $labelData = $labelLib->getLabelDetailByName($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get label details");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $labelData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateLabel(){
        $labelModel = new \Models\ProductLabels();

        $labelModel->setLabelFromArray($this->params);
        $labelModel->saveData('update_label');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelModel->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelModel->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelModel->getErrorCode()),
                "messages" => $labelModel->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to update label");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success updating label");
            $responseContent['data'] = array();

        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function generateLabel(){
        $labelLib = new \Library\ProductLabels();

        $result = $labelLib->updateLabelToVariant($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to update label");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success updating label");
            $responseContent['data'] = "";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function removeLabel(){
        $labelLib = new \Library\ProductLabels();

        $result = $labelLib->removeLabelFromVariant($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to update label");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success updating label");
            $responseContent['data'] = "";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function updateExpiredLabel(){
        $labelLib = new \Library\ProductLabels();

        $result = $labelLib->updateExpiredLabel();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($labelLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $labelLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($labelLib->getErrorCode()),
                "messages" => $labelLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to update label");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success updating label");
            $responseContent['data'] = "";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

}