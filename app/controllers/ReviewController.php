<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class ReviewController extends \Controllers\BaseController
{

    protected $supplierReview;
    protected $rupaResponse;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->supplierReview = new \Models\SupplierReview();
        $this->productReview = new \Models\ProductReview();
        $this->rupaResponse = new \Library\Response();
    }

    public function supplierReviewList(){
        $customeParams['limit'] = $this->params['limit'];
        $customeParams['offset'] = $this->params['offset'];
        unset($this->params['limit']);
        unset($this->params['offset']);
        $this->supplierReview->setFromArray($this->params);
        $supplierReview = $this->supplierReview->reviewList($customeParams);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $supplierReview['data'];
        $responseContent['errors'] = $this->supplierReview->getErrors();
        $responseContent['messages'] = array("result" => "success","recordsTotal" => $supplierReview['recordsTotal']);

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function saveSupplierReview(){
        $supplierReview = $this->supplierReview->saveReview($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $supplierReview;
        $responseContent['errors'] = $this->supplierReview->getErrors();
        $responseContent['messages'] = $this->supplierReview->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function productReviewList(){

        $this->productReview->setFromArray($this->params);
        $productReview = $this->productReview->reviewList();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        if (isset($productReview['list']) and empty($this->productReview->getErrors())) {
            $responseContent['messages'] = array("result" => "success","recordsTotal" => $productReview['recordsTotal']);
            $responseContent['data'] = $productReview['list'];
        } else {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $responseContent['errors'] = array(
                "code" => $this->productReview->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($this->productReview->getErrorCode()),
                "messages" => $this->productReview->getErrors()
            );
            $responseContent['messages'] = array("");
            $responseContent['data'] = array();
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function productReviewAverage(){

        $this->productReview->setFromArray($this->params);
        $productReview = $this->productReview->average();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (isset($productReview['list'])) {
            if(empty($productReview['list'])) {
                $responseContent['errors'] = "There have something error, in query getListProductCustom";
                $responseContent['messages'] = array("result" => "failed");
            } else {
                $responseContent['messages'] = array("result" => "success","recordsTotal" => $productReview['recordsTotal']);
                $responseContent['data'] = $productReview['list'];
            }
        } else {
            $responseContent['data'] = array();
            $responseContent['errors'] = $this->productVariantModel->getErrors();
            $responseContent['messages'] = $this->productVariantModel->getMessages();
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }



    public function saveProductReview(){
        $productReview = $this->productReview->saveReview($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $productReview;
        $responseContent['errors'] = $this->productReview->getErrors();
        $responseContent['messages'] = $this->productReview->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function supplierReviewAvg(){
        $this->supplierReview->setFromArray($this->params);
        $supplierReview = $this->supplierReview->supplierReviewAvg($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $supplierReview;
        $responseContent['errors'] = $this->supplierReview->getErrors();
        $responseContent['messages'] = $this->supplierReview->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function supplierReviewNotification()
    {
        $this->supplierReview->setFromArray($this->params);
        $supplierReview = $this->supplierReview->supplierNotification();

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $supplierReview;
        $responseContent['errors'] = $this->supplierReview->getErrors();
        $responseContent['messages'] = $this->supplierReview->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function customerReviewNotification()
    {
        $supplierReview = $this->supplierReview->customerNotification($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent['data'] = $supplierReview;
        $responseContent['errors'] = $this->supplierReview->getErrors();
        $responseContent['messages'] = $this->supplierReview->getMessages();

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

}