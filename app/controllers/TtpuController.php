<?php


namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class TtpuController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    protected $ttpuModel;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->ttpuModel = new \Models\Ttpu();
    }

    public function createTtpu()
    {

        $ttpuLib = new \Library\TtpuLib();
        $saveTtpu = $ttpuLib->saveTtpu($this->params);

        if(!empty($ttpuLib->getErrorCode()) || empty($saveTtpu)) {
            $errorCode = $ttpuLib->getErrorCode();
            if (is_array($errorCode)) {
                $errorCode = "RR400";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => "RR400",
                "messages" => $ttpuLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to save order");
            $responseContent['data'] = array();
        }else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = array($saveTtpu);
        }

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function updateStatus()
    {

        $ttpuLib = new \Library\TtpuLib();
        $saveTtpu = $ttpuLib->updateStatus($this->params);

        if(!empty($ttpuLib->getErrorCode()) || empty($saveTtpu)) {
            $errorCode = $ttpuLib->getErrorCode();
            if (is_array($errorCode)) {
                $errorCode = "RR400";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => "RR400",
                "messages" => $ttpuLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to save order");
            $responseContent['data'] = array();
        }else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = array($saveTtpu);
        }

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function listTtpu()
    {
        $ttpu = new \Library\TtpuLib();
        $data = $ttpu->ttpuList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($data['list'])) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Invoice not found");
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $data['list'];
            $response['messages'] = array("result" => "success","recordsTotal" => $data['recordsTotal']);
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function detailTtpu()
    {
        $ttpu = new \Library\TtpuLib();
        $data = $ttpu->detailTtpu($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if(empty($data['list'])) {
            $response['errors'] = array("code" => "RR302",
                "title" => $rupaResponse->getResponseDescription("RR302"),
                "messages" => "Invoice not found");
            $response['messages'] = array("result" => "failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $data['list'];
            $response['messages'] = array("result" => "success","recordsTotal" => $data['recordsTotal']);
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }




}