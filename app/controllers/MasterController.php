<?php
namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/customer")
 *
 */
class MasterController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function healthCheck()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = [];
        $responseContent['messages'] = "OK";
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCountryList()
    {
        $masterKecamatan = new \Models\MasterCountry();
        $result = $masterKecamatan->getDataList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCountryData()
    {
        $masterKecamatan = new \Models\MasterCountry();
        $result = $masterKecamatan->getData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getProvinceList()
    {
        $masterProvince = new \Models\MasterProvince();
        $result = $masterProvince->getDataList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getProvinceData()
    {
        $masterProvince = new \Models\MasterProvince();
        $result = $masterProvince->getData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCityList()
    {
        $masterCity = new \Models\MasterCity();
        $result = $masterCity->getDataList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getCityData()
    {
        $masterCity = new \Models\MasterCity();
        $result = $masterCity->getData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getKecamatanList()
    {
        $masterKecamatan = new \Models\MasterKecamatan();
        $result = $masterKecamatan->getDataList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getKecamatanData()
    {
        $masterKecamatan = new \Models\MasterKecamatan();
        $result = $masterKecamatan->getData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getKecamatanVendor()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        // Validate params
        $errorMessages = [];
        $requiredParamField = ["kecamatan_id", "vendor_name"];
        if (!empty($this->params['all']) && $this->params['all'] == 10) {
            $requiredParamField = [];
        }
        if (!empty($requiredParamField)) {
            foreach ($requiredParamField as $field) {
                if (empty($this->params[$field])) {
                    $errorMessages[] = sprintf("%s tidak boleh kosong", $field);
                }
            }
        }

        if (!empty($errorMessages)) {
            $responseContent['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => $errorMessages[0]
            );
            $responseContent['data'] = (object)[];
            $responseContent['messages'] = "failed";
            $rupaResponse->setContent($responseContent);
            $rupaResponse->setHeaderStatus(400);
            $rupaResponse->send();
            return;
        }
        
        $masterKecamatanVendor = new \Models\MasterKecamatanVendor();
        $isGetAll = !empty($this->params['all']) && $this->params['all'] == 10;
        $result = $isGetAll ? $masterKecamatanVendor->getDataList($this->params) : $masterKecamatanVendor->getData($this->params);

        $responseContent['errors'] = (object)[];
        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("failed") : array("success");
        $rupaResponse->setHeaderStatus(200);
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getKelurahanList()
    {
        $masterKelurahan = new \Models\MasterKelurahan();
        $result = $masterKelurahan->getDataList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getKelurahanData()
    {
        $masterKelurahan = new \Models\MasterKelurahan();
        $result = $masterKelurahan->getData($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getKelurahanVendor()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");

        // Validate params
        $errorMessages = [];
        $requiredParamField = ["kelurahan_id", "vendor_name"];
        if (!empty($this->params['all']) && $this->params['all'] == 10) {
            $requiredParamField = [];
        }
        if (!empty($requiredParamField)) {
            foreach ($requiredParamField as $field) {
                if (empty($this->params[$field])) {
                    $errorMessages[] = sprintf("%s tidak boleh kosong", $field);
                }
            }
        }

        if (!empty($errorMessages)) {
            $responseContent['errors'] = array(
                "code" => 400,
                "title" => $rupaResponse->getResponseDescription(400),
                "message" => $errorMessages[0]
            );
            $responseContent['data'] = (object)[];
            $responseContent['messages'] = "failed";
            $rupaResponse->setContent($responseContent);
            $rupaResponse->setHeaderStatus(400);
            $rupaResponse->send();
            return;
        }
        
        $masterKelurahanVendor = new \Models\MasterKelurahanVendor();
        $isGetAll = !empty($this->params['all']) && $this->params['all'] == 10;
        $result = $isGetAll ? $masterKelurahanVendor->getDataList($this->params) : $masterKelurahanVendor->getData($this->params);

        $responseContent['errors'] = (object)[];
        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("failed") : array("success");
        $rupaResponse->setHeaderStatus(200);
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getRejectReasonList()
    {
        $rejectReasonModel = new \Models\MasterRejectReason();
        if (isset($this->params['type'])) {
            $result = $rejectReasonModel->find(
                array(
                        'conditions' => "status > 0 and type = '" . $this->params['type'] . "'"
                        )
            )->toArray();
        } else {
            $result = $rejectReasonModel->find(
                array(
                        'conditions' => 'status > 0'
                        )
            )->toArray();
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function createBinListCache()
    {
        $masterCcBank = new \Models\MasterCcBank();
        $result = $masterCcBank->saveToCache();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = [];
        $responseContent['messages'] = [];
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function deleteBinCache()
    {
        $masterCcBank = new \Models\MasterCcBank();
        $result = $masterCcBank->deleteFromCache($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = [];
        $responseContent['messages'] = [];
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getMasterBrandStoreList()
    {
        $masterBrandStore = new \Models\MasterBrandStore();
        $result = $masterBrandStore->find(array(
            'conditions' => 'status > 0'
        ))->toArray();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getColoursMapping()
    {
        $coloursMapping = new \Models\ColoursMapping();
        $result = $coloursMapping->find($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getBrandMapping()
    {
        $brandMapping = new \Models\BrandMapping();
        $result = $brandMapping->find($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['data'] = $result;
        $responseContent['messages'] = (count($result) == 0) ? array("Data not found") : array("Data found");
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}