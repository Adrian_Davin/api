<?php


namespace Controllers;


class WilayahTemplateController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    // nanti

    public function createRegion(){
        $regionModel = new \Models\ScWilayahTemplate();
        $templateData = array(
            'supplier_id' => $this->params['supplier_id'],
            'name' => $this->params['name'],
            'status' => 10
        );

        unset($this->params['supplier_id']);
        unset($this->params['name']);

        $regionModel->setFromArray($templateData);

        $regionModel->saveData('create_region_template');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();


        $regionMappingModel = new \Models\ScWilayahMapping();

        //change '#' into ownfleet_template_id
        $this->params['string_value'] = str_replace('#', $regionModel->getWilayahTemplateId(), $this->params['string_value']);
        $regionMappingModel->setFromArray($this->params);
        $result = $regionMappingModel->insertTemplateToRegion();

        if($result == "error"){
            $responseContent['messages'] = array("Failed inserting template data to ownfleet_rate");
            $responseContent['data'] = "";
        }
        else {
            $responseContent['messages'] = array("Success inserting template data to ownfleet_rate");
            $responseContent['data'] = "";
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    } //

    public function uploadSku(){
        $regionModel = new \Models\ScWilayahTemplate();
        $result[] = $regionModel->uploadRegionToProduct($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent['messages'] = array("Success inserting template data to wilayah_template");
        $responseContent['data'] = $result;

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    } //

    public function regionList(){
        $regionLib = new \Library\WilayahTemplate();
        $regionList = $regionLib->getAllRegion($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($regionLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $regionLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($regionLib->getErrorCode()),
                "messages" => $regionLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed to create region template");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("Success creating region template");
            $responseContent['data'] = $regionList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();


    } //

    public function getRegionDetail(){
        $wilayahLib = new \Library\WilayahTemplate();

        $templateData = $wilayahLib->getRegionDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($wilayahLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $wilayahLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($wilayahLib->getErrorCode()),
                "messages" => $wilayahLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get region template details");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $templateData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    } //

    public function updateRegion(){
        $regionModel = new \Models\ScWilayahTemplate();
        $id = $this->params['wilayah_template_id'];

        $regionModel->setFromArray($this->params);
        $regionModel->saveData('update_region_template');

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        $regionMappingModel = new \Models\ScWilayahMapping();

        if(!empty($this->params['name'])){
            if(!empty($this->params['string_value'])){
                // clear previous shipping rate
                $is_affected = $regionMappingModel->clearRegionMapping($id);

                if($is_affected == "success"){
                    unset($this->params['supplier_id']);
                    unset($this->params['wilayah_template_id']);
                    unset($this->params['name']);

                    //change '#' into ownfleet_template_id
                    $this->params['string_value'] = str_replace('#', $id, $this->params['string_value']);
                    $regionMappingModel->setFromArray($this->params);

                    $result = $regionMappingModel->insertTemplateToRegion();

                    if($result == "error"){
                        $responseContent['messages'] = array("Failed inserting template data to wilayah_mapping");
                        $responseContent['data'] = "";
                    }
                    else {
                        $responseContent['messages'] = array("Success inserting template data to wilayah_mapping");
                        $responseContent['data'] = "";
                    }
                }
            }
            else {
                $responseContent['messages'] = array("Success inserting template data to wilayah_mapping");
                $responseContent['data'] = "";
            }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

}