<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 8/22/2017
 * Time: 3:04 PM
 */

namespace Controllers;

/**
 * Class ControllerNameController
 * @package Controllers
 */
class SeoAutoLinkController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getSeoAutoLink()
    {
        $seoAutoLinkLib = new \Library\SeoAutoLink();
        $heroProductList = $seoAutoLinkLib->getSeoAutoLink($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($seoAutoLinkLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $seoAutoLinkLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($seoAutoLinkLib->getErrorCode()),
                "messages" => $seoAutoLinkLib->getErrorMessages()
            );
            $responseContent['messages'] = array("result" => "Seo Link not found","recordsTotal" => 0);
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("result" => "success","recordsTotal" => $heroProductList['recordsTotal']);
            if (isset($this->params['seo_auto_link_id']) || isset($this->params['seo_link'])) {
                $responseContent['data'] = $heroProductList['list'][0];
            } else {
                $responseContent['data'] = $heroProductList['list'];
            }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function insertSeoAutoLink()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        // if insert we need to check if seo_link already in database or not
        if(!empty($this->params['seo_link'])){
            $seoAutoLinkModel = new \Models\SeoAutoLink();
            $seoAutoLink = $seoAutoLinkModel->findFirst('seo_link = "'.$this->params['seo_link'].'" and company_code="'.$this->params['company_code'].'"');
            if(!$seoAutoLink){
                $seoAutoLinkModel = new \Models\SeoAutoLink();
                $seoAutoLinkModel->setFromArray($this->params);

                $error = $seoAutoLinkModel->saveData();

                if(!$error){
                    $response['errors'] = array("code" => "RR301",
                        "title" => $rupaResponse->getResponseDescription("RR301"),
                        "messages" => $error);
                    $response['messages'] = array('Save failed');
                }else{
                    $response['errors'] = array();
                    $response['data'] = array();
                    $response['messages'] = array("success");
                }
            } else {
                $response['errors'] = array("code" => "RR301",
                "title" => $rupaResponse->getResponseDescription("RR301"),
                "messages" => "Seo Link already in database");
                $response['messages'] = array('Save failed');
            }
        } else {
            $response['errors'] = array("code" => "RR301",
            "title" => $rupaResponse->getResponseDescription("RR301"),
            "messages" => "Seo link is needed.");
            $response['messages'] = array('Save failed');
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function updateSeoAutoLink()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $seoAutoLinkModel = new \Models\SeoAutoLink();
        $seoAutoLinkModel->setFromArray($this->params);

        $error = $seoAutoLinkModel->saveData();

        if(!$error){
            $response['errors'] = array("code" => "RR301",
                "title" => $rupaResponse->getResponseDescription("RR301"),
                "messages" => $error);
            $response['messages'] = array('Save failed');
        }else{
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function customList()
    {
        $seoAutoLinkLib = new \Library\SeoAutoLink();
        $seoAutoLinkList = $seoAutoLinkLib->customList($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        /*
         * @todo: refactor the whole mechanism of properly passing error responses
         */
        if ($seoAutoLinkList) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $seoAutoLinkList;
        }
        else {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $errors = new \stdClass();
            $errors->code = 404;
            $errors->messages = ['data not found'];
            $responseContent['messages'] = [];
            $responseContent['errors'] = $errors;
            $responseContent['data'] = $seoAutoLinkList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function customAlphabet()
    {
        $seoAutoLinkLib = new \Library\SeoAutoLink();
        $seoAutoLinkList = $seoAutoLinkLib->customAlphabet($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        /*
         * @todo: refactor the whole mechanism of properly passing error responses
         */
        if ($seoAutoLinkList) {
            $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

            $responseContent['messages'] = array("success");
            $responseContent['errors'] = array();
            $responseContent['data'] = $seoAutoLinkList;
        }
        else {
            $rupaResponse->setStatusCode(404, "Ok")->sendHeaders();
            $errors = new \stdClass();
            $errors->code = 404;
            $errors->messages = ['data not found'];
            $responseContent['messages'] = [];
            $responseContent['errors'] = $errors;
            $responseContent['data'] = $seoAutoLinkList;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}