<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 1:55 PM
 */

namespace Controllers;

class CompanyController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    /**
     * @var $company \Models\Company
     */
    protected $company;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->company = new \Models\Company();
    }

    public function getlistCompany()
    {
        $companyLib = new \Library\Company();
        $companyList = $companyLib->getCompanyList($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!empty($companyLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $companyLib->getErrorCode(),
                "title" => $this->rupaResponse->getResponseDescription($companyLib->getErrorCode()),
                "messages" => $companyLib->getErrorMessages()
            );
            $responseContent['messages'] = array("result" => "Company not found", "recordsTotal" => 0);
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("result" => "success", "recordsTotal" => $companyList['recordsTotal']);
            $responseContent['data'] = $companyList['list'];
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function getCompanyWithAddress()
    {
        $companyLib = new \Library\Company();
        $company = $companyLib->getCompanyWithAddress($this->params);

        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        if (empty($company)) {
            $responseContent['data'] = $company;
            $responseContent['errors'] = '404';
            $responseContent['messages'] = 'Company not found';
        } else {
            $responseContent['data'] = $company;
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';
        }
        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();
    }

    public function saveCompany()
    {
        $this->rupaResponse->setContentType("application/json");
        $this->rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $companyLib = new \Library\Company();
        $companyLib->cekName($this->params);

        $responseContent = array();

        if (!empty($companyLib->getErrorCode())) {
            $errorCode = $companyLib->getErrorCode();
            if (is_array($errorCode)) {
                $errorCode = "RR400";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $this->rupaResponse->getResponseDescription($errorCode),
                "messages" => $companyLib->getErrorMessages()
            );
            $responseContent['messages'] = array("company already exists");
            $responseContent['data'] = array();
        } else {
            $this->company->setFromArray($this->params);
            $this->company->saveData();

            if (!empty($this->company->getErrorCode())) {
                $errorCode = $this->company->getErrorCode();
                if (is_array($errorCode)) {
                    $errorCode = "RR400";
                }
                $responseContent['errors'] = array(
                    "code" => $errorCode,
                    "title" => $this->rupaResponse->getResponseDescription($errorCode),
                    "messages" => $this->company->getErrorMessages()
                );
                $responseContent['messages'] = array("failed to save company");
                $responseContent['data'] = array();
            } else {
                $responseContent['messages'] = array("success");
                $responseContent['data'] = array("company_id" => $this->company->getCompanyId());
            }
        }

        $this->rupaResponse->setContent($responseContent);
        $this->rupaResponse->send();

    }

    public function deleteCompany()
    {
        $companyLib = new \Library\Company();
        $deleteStatus = $companyLib->deleteCompany($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!$deleteStatus) {
            $responseContent['errors'] = array(
                "code" => $companyLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($companyLib->getErrorCode()),
                "messages" => $companyLib->getErrorMessages());
            $responseContent['messages'] = array('delete failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}