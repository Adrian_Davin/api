<?php
namespace Controllers;


class PointController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function pointCheck()
    {
        $pointLib = new \Library\Point();
        $pointLib->setFromArray($this->params);
        $pointLib->pointCheck();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($pointLib->getErrorCode())) {
            $errorCode = $pointLib->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $pointLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = ['point' => $pointLib->getPoint()];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function redeemPoint()
    {
        $pointLib = new \Library\Point();
        $pointLib->setFromArray($this->params);
        $pointLib->redeemPoint();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($pointLib->getErrorCode())) {
            $errorCode = $pointLib->getErrorCode();
            if(is_array($errorCode)) {
                $errorCode = "RR200";
            }
            $responseContent['errors'] = array(
                "code" => $errorCode,
                "title" => $rupaResponse->getResponseDescription($errorCode),
                "messages" => $pointLib->getErrorMessages()
            );
            $responseContent['messages'] = array("failed to redeem your point");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = ['voucher_code' => $pointLib->getVoucherCode()];
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}