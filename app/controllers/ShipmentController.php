<?php

namespace Controllers;


/**
 * Class ShipmentController
 * @package Controllers
 * @RoutePrefix("/shipment")
 *
 */
class ShipmentController extends \Controllers\BaseController
{

    /**
     * @var \Library\Response
     */
    protected $rupaResponse;

    /**
     * @var \Models\SalesShipment
     */
    protected $salesShipment;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->rupaResponse = new \Library\Response();
        $this->salesShipment = new \Models\SalesShipment();
    }

    public function getShipmentList()
    {
        /*
         * can use params: shipment_id, invoice_id
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $salesShipmentLib = new \Library\SalesShipment();
            $allShipments = $salesShipmentLib->getAllShipment($this->params);

            if(!empty($salesShipmentLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $salesShipmentLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                    "messages" => $salesShipmentLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $allShipments;
            }
        }catch (\Exception $e){
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get shipment failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getShipmentData()
    {
        /*
         * params: shipment_id
         */

        $salesShipmentLib = new \Library\SalesShipment();
        $shipment_id = !empty($this->params['shipment_id']) ? $this->params['shipment_id'] : "";
        $salesShipmentData = $salesShipmentLib->getSalesShipmentData($shipment_id);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesShipmentLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesShipmentLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                "messages" => $salesShipmentLib->getErrorMessages()
            );
        } else {
            $responseContent['messages'] = "success";
            $responseContent['data'] = $salesShipmentData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getShipmentBatch()
    {
        $salesShipmentLib = new \Library\SalesShipment();
        $salesShipmentData = $salesShipmentLib->getSalesShipmentBatch();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesShipmentLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesShipmentLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                "messages" => $salesShipmentLib->getErrorMessages()
            );
        } else {
            $responseContent['messages'] = "success";
            $responseContent['data'] = $salesShipmentData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function processShipmentBatch()
    {
        $salesShipmentLib = new \Library\SalesShipment();
        $salesShipmentLib->processSalesShipmentBatch();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($salesShipmentLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $salesShipmentLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                "messages" => $salesShipmentLib->getErrorMessages()
            );
        } else {
            $responseContent['messages'] = "success";
            $responseContent['data'] = array();
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateShipmentData()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $salesShipmentLib = new \Library\SalesShipment();
        // validate params
        $salesShipmentLib->prepareSalesShipment($this->params);
        if (empty($salesShipmentLib->getErrorCode())) {
            $salesShipmentLib->updateData($this->params);
            if(empty($salesShipmentLib->getErrorCode())) {
                $response['messages'] = "success";
            } else {
                $response['errors'] = array(
                    "code" => $salesShipmentLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                    "message" => $salesShipmentLib->getErrorMessages());
            }
        } else {
            $response['errors'] = array(
                "code" => $salesShipmentLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                "message" => $salesShipmentLib->getErrorMessages());
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function rollBackShipment()
    {
        $response = array(
            'data' => '',
            'errors' => '',
            'messages' => ''
        );

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $salesShipmentLib = new \Library\SalesShipment();
        // validate params
        if (empty($salesShipmentLib->getErrorCode())) {
            $salesShipmentLib->roleBackShipmentStatus($this->params);
            if(empty($salesShipmentLib->getErrorCode())) {
                $response['messages'] = "success";
            } else {
                $response['errors'] = array(
                    "code" => $salesShipmentLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                    "message" => $salesShipmentLib->getErrorMessages());
            }
        } else {
            $response['errors'] = array(
                "code" => $salesShipmentLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($salesShipmentLib->getErrorCode()),
                "message" => $salesShipmentLib->getErrorMessages());
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    /**
 * This function is used by batchprocessor to check awb
 */
    public function checkAwbBatch()
    {
        /*
         * MUST contain this params: shipment_id, carrier_id, track_number
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent = array();
        try {
            $salesShipmentLib = new \Library\SalesShipment();
            $responseContent['messages'] = $salesShipmentLib->checkAwbBatch($this->params);
        }catch (\Exception $e){
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Check AWB batch failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * This function is used by oms ninjavan webhooks
     */
    public function ninjaWebhooks()
    {
        /*
         * MUST contain this params: tracking_id, status, comments & pod_name (can empty), timestamp
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        $responseContent = array();
        try {
            $salesShipmentLib = new \Library\SalesShipment();
            $responseContent['messages'] = $salesShipmentLib->ninjaWebhooks($this->params);;
        }catch (\Exception $e){
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Update AWB tracking failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to get Shipping Null product
     * No params needed
     */
    public function getShippingNullData()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseModel = $this->salesShipment->getShippingNullData();

        if (isset($responseModel['messages'])) $responseContent['messages'] = $responseModel['messages'];
        if (isset($responseModel['errors'])) $responseContent['errors'] = $responseModel['errors'];
        if (isset($responseModel['data'])) $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update Shipping Null
     * No params needed
     */
    public function updateShippingNull()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseModel = $this->salesShipment->updateShippingNull($this->params[0]);
        if (isset($responseModel['messages'])) $responseContent['messages'] = $responseModel['messages'];
        if (isset($responseModel['errors'])) $responseContent['errors'] = $responseModel['errors'];
        if (isset($responseModel['data'])) $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to update Shipping Null
     * params needed: to, cc, subject, link_download
     */
    public function sendEmailShippingNull()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseModel = $this->salesShipment->sendShippingNullEmail($this->params);
        if (isset($responseModel['messages'])) $responseContent['messages'] = $responseModel['messages'];
        if (isset($responseModel['errors'])) $responseContent['errors'] = $responseModel['errors'];
        if (isset($responseModel['data'])) $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to get pending order, required: params['rr-type'] = store/dc
     * No params needed
     */
    public function getPendingOrder()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseModel = $this->salesShipment->getPendingOrder($this->params);

        if (isset($responseModel['messages'])) $responseContent['messages'] = $responseModel['messages'];
        if (isset($responseModel['errors'])) $responseContent['errors'] = $responseModel['errors'];
        if (isset($responseModel['data'])) $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to send reminder email to pic store / dc
     * params needed: type, to, cc, data_email
     */
    public function sendEmailReminderPendingOrder()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseModel = $this->salesShipment->sendEmailReminderPendingOrder($this->params);
        if (isset($responseModel['messages'])) $responseContent['messages'] = $responseModel['messages'];
        if (isset($responseModel['errors'])) $responseContent['errors'] = $responseModel['errors'];
        if (isset($responseModel['data'])) $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    /**
     * Function to get origin_pickup
     * params needed: kecamatan_id, sku, qty_ordered, carrier_id
     */
    public function getOriginByKecamatanID()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseModel = $this->salesShipment->getOriginByKecamatanID($this->params);
        if (isset($responseModel['messages'])) $responseContent['messages'] = $responseModel['messages'];
        if (isset($responseModel['errors'])) $responseContent['errors'] = $responseModel['errors'];
        if (isset($responseModel['data'])) $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    // Function to update shipment status batch
    public function updateStatusBatch()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseModel = $this->salesShipment->updateStatusBatch($this->params);
        if (isset($responseModel['messages'])) $responseContent['messages'] = $responseModel['messages'];
        if (isset($responseModel['errors'])) $responseContent['errors'] = $responseModel['errors'];
        if (isset($responseModel['data'])) $responseContent['data'] = $responseModel['data'];

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

}