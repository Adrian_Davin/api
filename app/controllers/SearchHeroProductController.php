<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 8/22/2017
 * Time: 3:04 PM
 */

namespace Controllers;

/**
 * Class ControllerNameController
 * @package Controllers
 */
class SearchHeroProductController extends \Controllers\BaseController
{

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function getListHeroProduct()
    {
        $searchHeroProductLib = new \Library\SearchHeroProduct();
        $heroProductList = $searchHeroProductLib->getListHeroProduct($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($searchHeroProductLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $searchHeroProductLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($searchHeroProductLib->getErrorCode()),
                "messages" => $searchHeroProductLib->getErrorMessages()
            );
            $responseContent['messages'] = array("result" => "Hero Product not found","recordsTotal" => 0);
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("result" => "success","recordsTotal" => $heroProductList['recordsTotal']);
            if (isset($this->params['search_hero_product_id']) || isset($this->params['search_keyword'])) {
                $responseContent['data'] = $heroProductList['list'][0];
            } else {
                $responseContent['data'] = $heroProductList['list'];
            }
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveHeroProduct()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $searchHeroProductModel = new \Models\SearchHeroProduct();
        $searchHeroProductModel->setFromArray($this->params);

        $error = $searchHeroProductModel->saveData();

        if(!$error){
            $response['errors'] = array("code" => "RR301",
                "title" => $rupaResponse->getResponseDescription("RR301"),
                "messages" => $error);
            $response['messages'] = array('Save failed');
        }else{
            $response['errors'] = array();
            $response['data'] = array();
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function deleteHeroProduct()
    {
        $searchHeroProductLib = new \Library\SearchHeroProduct();
        $deleteStatus = $searchHeroProductLib->deleteHeroProduct($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!$deleteStatus) {
            $responseContent['errors'] = array(
                "code" => $searchHeroProductLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($searchHeroProductLib->getErrorCode()),
                "messages" => $searchHeroProductLib->getErrorMessages());
            $responseContent['messages'] = array('delete failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
            $responseContent['messages'] = array("success");
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}