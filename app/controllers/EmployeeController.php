<?php
namespace Controllers;

/**
 * Class EmployeeController
 * @package Controllers
 */
class EmployeeController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }


    public function loginNip() {
        $employeeLib = new \Library\Employee();
        $loginResult = $employeeLib->loginNip($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (empty($loginResult)) {
            $response['errors'] = array(
                "code" => $employeeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($employeeLib->getErrorCode()),
                "messages" => $employeeLib->getErrorMessages()
            );
            $response['messages'] = array("error");
        } else {
            $response['errors'] = array();
            $response['data'] = $loginResult;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getEmployeeStats() {
        $employeeLib = new \Library\Employee();
        $employeeResult = $employeeLib->getEmployeeStats($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (empty($employeeResult)) {
            $response['errors'] = array(
                "code" => $employeeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($employeeLib->getErrorCode()),
                "messages" => $employeeLib->getErrorMessages()
            );
            $response['messages'] = array("error");
        } else {
            $response['errors'] = array();
            $response['data'] = $employeeResult;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function getEmployeeDataByNip() {
        $employeeLib = new \Library\Employee();
        $result = $employeeLib->getEmployeeDataByNip($this->params);
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (empty($result)) {
            $response['errors'] = array(
                "code" => $employeeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($employeeLib->getErrorCode()),
                "message" => $employeeLib->getErrorMessages()
            );
            $response['messages'] = array("error");
        } else {
            $response['errors'] = array();
            $response['data'] = $result;
            $response['messages'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function checkEmployee() {
        $employeeLib = new \Library\Employee();
        $result = $employeeLib->checkEmployee($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($employeeLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $employeeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($employeeLib->getErrorCode()),
                "message" => $employeeLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $result;
            $response['message'] = array("success");
        }

        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function generateEmployeeOtp() {
        $employeeLib = new \Library\Employee();
        $result = $employeeLib->generateEmployeeOtp($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        if (!empty($employeeLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $employeeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($employeeLib->getErrorCode()),
                "messages" => $employeeLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] = $result;
            $response['messages'] = array("success");
        }
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function validateEmployeeOtp()
    {

        $employeeLib = new \Library\Employee();
        $result = $employeeLib->validateEmployeeOtp($this->params);
        
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        
        if (!empty($employeeLib->getErrorMessages())) {
            $response['errors'] = array(
                "code" => $employeeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($employeeLib->getErrorCode()),
                "messages" => $employeeLib->getErrorMessages()
            );
            $response['messages'] = array("failed");
        } else {
            $response['errors'] = array();
            $response['data'] =  $result;
            $response['messages'] = array("success");
        }


        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }

    public function upsertEmployee() {
        
        $employeeLib = new \Library\Employee();
        $result = $employeeLib->upsertEmployee($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!empty($employeeLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $employeeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($employeeLib->getErrorCode()),
                "messages" => $employeeLib->getErrorMessages()
            );
            $response['messages'] = array("failed"); 
        } else{
            $response['errors'] = array();
            $response['data'] = $result;
            $response['messages'] = array("success");
        }
 
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }
    
    public function upsertEmployeeDepartment() {
        
        $employeeLib = new \Library\Employee();
        $result = $employeeLib->upsertEmployeeDepartment($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if (!empty($employeeLib->getErrorCode())) {
            $response['errors'] = array(
                "code" => $employeeLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($employeeLib->getErrorCode()),
                "messages" => $employeeLib->getErrorMessages()
            );
            $response['messages'] = array("failed"); 
        } else{
            $response['errors'] = array();
            $response['data'] = $result;
            $response['messages'] = array("success");
        }
 
        $rupaResponse->setContent($response);
        $rupaResponse->send();
    }
}