<?php


namespace Controllers;

use Models\Event;

class EventController extends \Controllers\BaseController
{
    public function onConstruct()
    {
        parent::onConstruct();
    }

    public function eventList() // Clear
    {
        $eventLib = new \Library\Event();
        $eventData = $eventLib->getAllEvent($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($eventLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $eventLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($eventLib->getErrorCode()),
                "messages" => $eventLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get events");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $eventData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function eventDetail()
    {
        $eventLib = new \Library\Event();

        $eventData = $eventLib->getEventDetail($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if(!empty($eventLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $eventLib->getErrorCode(),
                "title" => $rupaResponse->getResponseDescription($eventLib->getErrorCode()),
                "messages" => $eventLib->getErrorMessages()
            );
            $responseContent['messages'] = array("Failed get event details");
            $responseContent['data'] = array();
        } else {
            $responseContent['messages'] = array("success");
            $responseContent['data'] = $eventData;
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();

    }

    public function createEvent(){
        $error = "";
        $messages = "Create Event Success";

        $paramsEvent = $this->params;
        if (isset($paramsEvent['products']) && is_array($paramsEvent['products'])) {
            $products = $paramsEvent['products'];
            $storeCode = $paramsEvent['store_code'];
            unset($paramsEvent['products']);
            unset($paramsEvent['store_code']);

            $paramsEvent['products'] = "";
            foreach($products as $val) {
                $sku = trim($val," ");
                $paramsEvent['products'] .= $sku . ",";
            }

            $paramsEvent['products'] = rtrim($paramsEvent['products'], ",");
        }

        $eventModel = new \Models\Event();
        $eventModel->setEventFromArray($paramsEvent);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $routeHelper = new \Helpers\RouteHelper();
        $message = $routeHelper->checkUrlKey("","event",$eventModel->getUrlKey(), $this->params['company_code']);
        if($message['url_key_status'] == 0){
            //url key exist
            $error = "Url key existed";
            $messages = "Create failed! Url Key existed.";
        }
        else{
            $eventModel->saveData("create_event");

            if (isset($products)) {
                $eventID = $eventModel->getEventId();
                $totalIndex = count($products);
    
                // insert
                for($i=0;$i<$totalIndex;$i++) {
                    $eventModel->insertEventProducts($products[$i], $storeCode[$i], $eventID);
                }
    
                $companyCode = strtolower($eventModel->getCompanyCode());;
                $startDate = $paramsEvent['start_date'];
                $startDate = strtotime ( '-15 minute' , strtotime ($startDate) ) ;
                $startDate = date ('Y-m-d H:i:s' , $startDate);
                $endDate = $paramsEvent['end_date'];
    
                $startDateActive = $endDate;
                $endDateActive = $startDateActive;
                $endDateActive = strtotime ( '+15 minute' , strtotime ($endDateActive) ) ;
                $endDateActive = date ('Y-m-d H:i:s' , $endDateActive);
                for($i=0;$i<$totalIndex;$i++) {
                    $storeCodeArr = explode(",", $storeCode[$i]);
    
                    $resultCheck = $eventModel->checkOverrideStatus($products[$i], $companyCode, $startDate, $endDate);
                    if ($resultCheck) {
                        $resultDelete = $eventModel->deleteOverrideStatus($products[$i], $companyCode, $startDate, $endDate);
                    }
    
                    $resultCheck = $eventModel->checkOverrideStatus($products[$i], $companyCode, $startDateActive, $endDateActive);
                    if ($resultCheck) {
                        $resultDelete = $eventModel->deleteOverrideStatus($products[$i], $companyCode, $startDateActive, $endDateActive);
                    }
    
                    for($j=0;$j<count($storeCodeArr);$j++) {
                        $statusStock = 0;
                        $resultInsert = $eventModel->insertOverrideStatus($products[$i],$storeCodeArr[$j], $statusStock, $companyCode, $startDate, $endDate);
    
                        $statusStock = 10;
                        $resultInsert = $eventModel->insertOverrideStatus($products[$i],$storeCodeArr[$j], $statusStock, $companyCode, $startDateActive, $endDateActive);
                    }
                }
            }

            $productHelper = new \Helpers\ProductHelper();
            $productHelper->compareProductsEvent($eventModel, $eventModel->getCompanyCode(), $eventModel->getLockQty());

            $urlModel = new \Models\MasterUrlKey();
            $urlData = array(
                'url_key' => $eventModel->getUrlKey(),
                'section' => 'event',
                'inline_script' => $eventModel->getInlineScript(),
                'additional_header' => $eventModel->getAdditionalHeader(),
                'document_title' => "Jual ".$eventModel->getTitle()." | Ruparupa",
                'reference_id' => $eventModel->getEventId(),
                'company_code' => $eventModel->getCompanyCode()
            );

            $urlModel->setFromArray($urlData);
            $result = $urlModel->saveUrlKey();
        }

        $responseContent['errors'] = array($error);
        $responseContent['messages'] = array($messages);
        $responseContent['data'] = "";

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function updateEvent(){
        $error = "";
        $messages = "Updating Success";

        $paramsEvent = $this->params;
        if (isset($paramsEvent['products']) && is_array($paramsEvent['products'])) {
            $products = $paramsEvent['products'];
            $storeCode = $paramsEvent['store_code'];
            unset($paramsEvent['products']);
            unset($paramsEvent['store_code']);

            $paramsEvent['products'] = "";
            foreach($products as $val) {
                $sku = trim($val," ");
                $paramsEvent['products'] .= $sku . ",";
            }

            $paramsEvent['products'] = trim($paramsEvent['products'], ",");
        }

        $eventModel = new \Models\Event();
        $eventModel->setEventFromArray($paramsEvent);
        $eventModel->saveData("update_event");

        if (isset($products)) {
            $eventID = $paramsEvent['event_id'];
            $totalIndex = count($products);

            // delete insert
            $deleteResult = $eventModel->deleteEventProducts($eventID);
            if ($deleteResult) {
                for($i=0;$i<$totalIndex;$i++) {
                    $eventModel->insertEventProducts($products[$i], $storeCode[$i], $eventID);
                }
            }

            $companyCode = strtolower($eventModel->getCompanyCode());
            $startDate = $paramsEvent['start_date'];
            $startDate = strtotime ( '-15 minute' , strtotime ($startDate) ) ;
            $startDate = date ('Y-m-d H:i:s' , $startDate);
            $endDate = $paramsEvent['end_date'];

            $startDateActive = $endDate;
            $endDateActive = $startDateActive;
            $endDateActive = strtotime ( '+15 minute' , strtotime ($endDateActive) ) ;
            $endDateActive = date ('Y-m-d H:i:s' , $endDateActive);
            for($i=0;$i<$totalIndex;$i++) {
                $storeCodeArr = explode(",", $storeCode[$i]);

                $resultCheck = $eventModel->checkOverrideStatus($products[$i], $companyCode, $startDate, $endDate);
                if ($resultCheck) {
                    $resultDelete = $eventModel->deleteOverrideStatus($products[$i], $companyCode, $startDate, $endDate);
                }

                $resultCheck = $eventModel->checkOverrideStatus($products[$i], $companyCode, $startDateActive, $endDateActive);
                if ($resultCheck) {
                    $eventModel->deleteOverrideStatus($products[$i], $companyCode, $startDateActive, $endDateActive);
                }

                for($j=0;$j<count($storeCodeArr);$j++) {
                    $statusStock = 0;
                    $eventModel->insertOverrideStatus($products[$i],$storeCodeArr[$j], $statusStock, $companyCode, $startDate, $endDate);

                    $statusStock = 10;
                    $eventModel->insertOverrideStatus($products[$i],$storeCodeArr[$j], $statusStock, $companyCode, $startDateActive, $endDateActive);
                }
            }
        }

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        if (isset($this->params['title'])){
            if($eventModel->getOldUrlKey() != $eventModel->getUrlKey()){
                // check first if the url_key used to be event's url_key
                $routeHelper = new \Helpers\RouteHelper();
                $message = $routeHelper->checkUrlKey($eventModel->getEventId(),"event",$eventModel->getUrlKey(), $this->params['company_code']);

                if($message['url_key_status'] == 0){ // url_key existed
                    $error = "Url key existed";
                    $messages = "Update failed! Url Key existed.";
                } else {
                    $productHelper = new \Helpers\ProductHelper();
                    $productHelper->compareProductsEvent($eventModel, $this->params['company_code'], $eventModel->getLockQty());

                    $urlModel = new \Models\MasterUrlKey();
                    $urlData = array(
                        'url_key' => $eventModel->getUrlKey(),
                        'section' => 'event',
                        'inline_script' => $eventModel->getInlineScript(),
                        'additional_header' => $eventModel->getAdditionalHeader(),
                        'document_title' => "Jual " . $eventModel->getTitle() . " | Ruparupa",
                        'reference_id' => $eventModel->getEventId(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'company_code' => $eventModel->getCompanyCode()
                    );

                    if ($message['url_key_status'] == 1) {
                        // url_key used to be this url_key
                        $urlModel->setFromArray($urlData);
                        $result = $urlModel->updateUrlKey();
                    } else {
                        // completely new
                        $urlModel->setFromArray($urlData);
                        $result = $urlModel->saveUrlKey();
                    }
                }
            } else {
                $productHelper = new \Helpers\ProductHelper();
                $productHelper->compareProductsEvent($eventModel, $this->params['company_code'], $eventModel->getLockQty());
                $urlModel = new \Models\MasterUrlKey();

                $urlData = array(
                    'url_key' => $eventModel->getUrlKey(),
                    'section' => 'event',
                    'inline_script' => $eventModel->getInlineScript(),
                    'additional_header' => $eventModel->getAdditionalHeader(),
                    'document_title' => "Jual ".$eventModel->getTitle()." | Ruparupa",
                    'reference_id' => $eventModel->getEventId(),
                    'created_at' => date('Y-m-d H:i:s'),
                    'company_code' => $eventModel->getCompanyCode()
                );

                $urlModel->setFromArray($urlData);
                $result = $urlModel->updateUrlKey();
            }
        } else {
            // only update status, not need to update url key
            $event_id = $this->params['event_id'];
            $lockQty = (isset($this->params['lock_qty'])) ? $this->params['lock_qty'] : "10";

            $eventModel = new \Models\Event();
            $result = $eventModel->findFirst(
                [
                    "conditions" => "event_id ='".$event_id."'"
                ]
            );

            $eventModel->setEventFromArray($result->toArray());

            $masterUrlKeyModel = new \Models\MasterUrlKey();
            $resultMasterUrlKey = $masterUrlKeyModel->findFirst(
                [
                    "conditions" => "reference_id = '".$event_id."' AND section = 'event' AND status = 10 AND company_code = '".$eventModel->getCompanyCode()."'",
                    "order" => "created_at DESC",
                    "limit" => 1
                ]
            );
            if($resultMasterUrlKey) {
                $eventModel->setUrlKey($resultMasterUrlKey->toArray()['url_key']);
            }
            $productHelper = new \Helpers\ProductHelper();
            $productHelper->compareProductsEvent($eventModel, $eventModel->getCompanyCode(), $lockQty);
        }

        $responseContent['errors'] = array($error);
        $responseContent['messages'] = array($messages);
        $responseContent['data'] = "";

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function switchingNextEvents(){
        $eventLib = new \Library\Event();
        $eventData = $eventLib->handleNextEvent();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        $responseContent['errors'] = array();
        $responseContent['messages'] = array("success");
        $responseContent['data'] = array();

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function switchingPrevEvents(){
        $eventLib = new \Library\Event();
        $eventData = $eventLib->handlePreviousEvent();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        $responseContent['errors'] = array();
        $responseContent['messages'] = array("success");
        $responseContent['data'] = array();

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function switchingEvents(){
        $eventLib = new \Library\Event();
        $eventData = $eventLib->switchEventNormal();

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        $responseContent = array();

        $responseContent['errors'] = array();
        $responseContent['messages'] = array("success");
        $responseContent['data'] = array();

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}