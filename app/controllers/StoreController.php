<?php
namespace Controllers;

/**
 * Class StoreController
 * @package Controllers
 */
class StoreController extends \Controllers\BaseController
{
    /**
     * @var $pickupPoint \Models\Store
     */
    protected $store;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->store = new \Models\Store();
    }

    public function getStoreList()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $storeLib = new \Library\Store();
            $allStore = $storeLib->getAllStore($this->params);

            if(!empty($storeLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $storeLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($storeLib->getErrorCode()),
                    "messages" => $storeLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = array("result" => "success","recordsTotal" => $allStore['recordsTotal']);
                $responseContent['data'] = $allStore['list'];
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get store list failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getNearestStore() {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $store = new \Models\Store();
            $listOfNearestStore = $store->getNearestStore($this->params);

            $responseContent['data'] = $listOfNearestStore;
            $responseContent['errors'] = '';
            $responseContent['messages'] = 'success';

        } catch(\Exception $e) {
            $responseContent['data'] = [];
            $responseContent['errors'] = true;
            $responseContent['messages'] = 'failed';
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getStoreData()
    {
        /*
         * params: store_id
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $storeLib = new \Library\Store();
            $store_id = !empty($this->params['store_id']) ? $this->params['store_id'] : "";
            $storeData = $storeLib->getStoreData($store_id);

            if(!empty($storeLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $storeLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($storeLib->getErrorCode()),
                    "messages" => $storeLib->getErrorMessages()
                );
                $responseContent['data'] = array();
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $storeData;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get store failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveStore()
    {
        $companyLib = new \Library\Store();
        $companyLib->saveStore($this->params);

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();

        if(!empty($companyLib->getErrorCode())) {
            $responseContent['errors'] = array(
                "code" => $companyLib->getErrorCode() ,
                "title" => $rupaResponse->getResponseDescription($companyLib->getErrorCode()),
                "messages" => $companyLib->getErrorMessages());
            $responseContent['messages'] = array('save failed');
        } else {
            $responseContent['errors'] = array();
            $responseContent['data'] = array();
                 
            $responseContent['messages'] = array("success");
        }
        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }
}