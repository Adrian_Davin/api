<?php

namespace Controllers;

/**
 * Class ProductController
 * @package Controllers
 * @RoutePrefix("/product")
 *
 */
class EmailController extends \Controllers\BaseController
{

    public function onConstruct(){
        parent::onConstruct();
    }

    public function sendEmail()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $storeLib = new \Library\Email();
            $storeLib->sendEmail($this->params);

            if(!empty($storeLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $storeLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($storeLib->getErrorCode()),
                    "messages" => $storeLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = array("status" => "Success");
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Send mail failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function getEmailTemplateID()
    {
        /*
         * can use header params: template_code, company_code
         */

        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $emailLib = new \Library\Email();
            $allTemplates = $emailLib->getEmailTemplateID($this->params);

            if(!empty($emailLib->getErrorCode())) {
                $responseContent['errors'] = array(
                    "code" => $emailLib->getErrorCode(),
                    "title" => $rupaResponse->getResponseDescription($emailLib->getErrorCode()),
                    "messages" => $emailLib->getErrorMessages()
                );
            } else {
                $responseContent['messages'] = "success";
                $responseContent['data'] = $allTemplates;
            }
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Get template id failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function sendCashbackEmail()
    {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        try {
            $rule_id = isset($this->params['rule_id']) ? $this->params['rule_id'] : 0;


            if (empty($rule_id)) {
                $responseContent['errors'] = array(
                    "code" => "404",
                    "title" => "Validation Error",
                    "messages" => "Rule ID is required"
                );
            } else {
                $voucherData = \Models\SalesruleVoucher::find('rule_id = '.$rule_id);

                if ($voucherData->count() > 0) {
                    $voucherToSend = [];
                    $customerId = 0;
                    foreach ($voucherData as $voucher) {
                        if ($voucher->Customer) {
                            if (empty($customerId)) {
                                $customerId = $voucher->getCustomerId();
                            } else if ($customerId != $voucher->getCustomerId()) {
                                $customerId = $voucher->getCustomerId();
                                $customerVoucher = [];
                            }


                            $customerVoucher[$voucher->getCustomerId()][] = $voucher->getVoucherCode();

                            $voucherToSend[$voucher->getCustomerId()] = [
                                "customer_firstname" => $voucher->Customer->getFirstName(),
                                "customer_lastname" => $voucher->Customer->getLastName(),
                                "customer_email" => $voucher->Customer->getEmail(),
                                "voucher_amount" => $voucher->getVoucherAmount(),
                                "voucher_list" => $customerVoucher[$voucher->getCustomerId()],
                                "voucher_expired" => $voucher->getExpirationDate()
                            ];
                        }

                    }

                    if (!empty($voucherToSend)) {
                        foreach ($voucherToSend as $voucher) {
                            $email = new \Library\Email();
                            $email->setEmailSender();
                            $email->setName($voucher['customer_lastname'], $voucher['customer_lastname']);
                            $email->setEmailReceiver($voucher['customer_email']);
                            $email->setEmailTag("cust_voucher_cashback");

                            $email->setTemplateId($this->params['template_id']);

                            $voucherList = array_values($voucher['voucher_list']);
                            if (!empty($voucherList) && count($voucherList) > 1) {
                                $newList = [];
                                foreach ($voucherList as $myVoucher) {
                                    $newList[] = ['voucher_code' => $myVoucher];
                                }
                                $email->setVoucherList($newList);
                            } else {
                                $email->setVoucherCode($voucherList[0]);
                            }

                            $email->setVoucherValue(number_format($voucher['voucher_amount'], 0, ',', '.'));
                            $email->setVoucherExpired(date('j F Y H:i:s', strtotime($voucher['voucher_expired'])));
                            $email->send();
                        }
                    }
                }
            }

            $responseContent['messages'] = "success";
            $responseContent['data'] = array("status" => "Success");
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Send mail failed, please contact ruparupa tech support"
            );
        }

        $rupaResponse->setContent($responseContent);
        $rupaResponse->send();
    }

    public function saveDataSellerNotif() {
        $rupaResponse = new \Library\Response();
        $rupaResponse->setContentType("application/json");
        $rupaResponse->setStatusCode(200, "Ok")->sendHeaders();
        
        try {
            $sku = isset($this->params['sku']) ? $this->params['sku'] : 0;
            $isBuyerApprove = isset($this->params['is_buyer_approved']) ? $this->params['is_buyer_approved'] ? 1:0 : 0;
            $isContentApprove = isset($this->params['is_content_approved']) ? $this->params['is_content_approved'] ? 1:0 : 0;
            $isReject = isset($this->params['is_rejected']) ? $this->params['is_rejected'] ? 1:0 : 0;
            $createdAt = isset($this->params['created_at']) ? $this->params['created_at'] : 0;
            $supplierID = isset($this->params['supplier_id']) ? $this->params['supplier_id'] : 0;
            $productName = isset($this->params['product_name']) ? $this->params['product_name'] : 0;
            
        
            if (empty($sku)) {
                $responseContent['errors'] = array(
                    "code" => "404",
                    "title" => "Validation Error",
                    "messages" => "SKU is required"
                );
            } 
             else {
                $sellerData = \Models\SellerNotif::findFirst('sku = "'.$sku.'"');
              
                if (empty($sellerData)) {
                    
                    $data = [
                        'sku' => $sku,
                        'is_buyer_approved' => $isBuyerApprove,
                        'is_content_approved' => $isContentApprove,
                        'is_rejected' => $isReject,
                        'created_at' => $createdAt,
                        'supplier_id'   => $supplierID,
                        'product_name' => $productName
                    ];
                    $sellerNotif = new \Models\SellerNotif;
                    $sellerNotif->assign($data);
                    $sellerNotif->saveData("save_seller_notif");
                } else {
                    $sellerUpdate = new \Models\SellerNotif;
                    $sellerUpdate->updateDataSellerNotif($sku, $isBuyerApprove, $isContentApprove, $isReject, $createdAt, $productName);
                }
            }
            $responseContent['messages'] = "success";
            $responseContent['data'] = array("status" => "Success");
        } catch (\Exception $e) {
            $responseContent['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Failed to save data seller, please contact ruparupa tech support"
            );
        }
    }

    
}