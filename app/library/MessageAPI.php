<?php

namespace Library;
use \GuzzleHttp\Client as GuzzleClient;
use Helpers\LogHelper;

class MessageAPI {

    protected $messages;
    protected $app_sender;

    // Getter
    public function getMessages()
    {
        return $this->messages;
    }

    public function getAppSender()
    {
        return $this->app_sender;
    }

    // Setter
    public function setMessages($messages)
    {
        return $this->messages = $messages;
    }

    public function setAppSender($app_sender)
    {
        return $this->app_sender = $app_sender;
    }

    // Methods
    public function sendSingleGeneralMessage() {
        try {
            // Check if messages only contain > 1 message we'll immediately return false, 
            // because this function only support sending single message
            // If you want to send message in bulk, create new function using the same API
            if (count($this->messages) > 1) {
                return false;
            }

            $payload = [
                "messages" => $this->messages,
                "app_sender" => $this->app_sender
            ];
            $client = new GuzzleClient();
            $curlParam = [
                'timeout' => '30',
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => sprintf("Bearer %s", getenv('MESSAGE_API_TOKEN'))
                ],
                'body' => json_encode($payload)
            ];

            $response = $client->request('POST', sprintf("%s%s", getenv('MESSAGE_API_URL'), "general/send"), $curlParam);
            if ($response->getStatusCode() != 200) {
                return false;
            }

            $rawResponseBody = $response->getBody();
            LogHelper::log("message_api", sprintf("Send general message, payload: %s, response: %s", json_encode($payload), $rawResponseBody), "info");
            $parsedResponseBody = json_decode($rawResponseBody, true);
            if (empty($parsedResponseBody["data"])) {
                return false;
            }

            return $parsedResponseBody["data"];
        } catch (\Exception $e) {
            $errMessage = sprintf("Failed send general message, payload: %s, error: %s", json_encode($payload), $e->getMessage());
            LogHelper::log("message_api", $errMessage);
            return false;
        }
    }
}
