<?php

namespace Library;

class Nsq
{
    public $nsq_obj;

    public function __construct()
    {
        $nsqHost = $_ENV['NSQ_HOST'].":".$_ENV['NSQ_PORT'];
        $hosts = array($nsqHost);
        $replicas = 1;

        $this->nsq_obj = new \nsqphp\nsqphp;
        $this->nsq_obj->publishTo($hosts, $replicas);

    }

    // DUE TO PHP API HTTP PROTOCOL MIGRATION IN NSQ, THIS FUNCTION WILL NOT BE USED ANYMORE
    // KINDLY USE publishCluster Function INSTEAD OF THIS FUNCTION. DO NOT UNCOMMENT THIS FUNCTION
    // public function publish($topic = '', $data = array())
    // {
    //     if ($topic == 'product') {
    //         if (isset($data["data"]["sku"])) {
    //             $data["data"]["sku"] = (string) $data["data"]["sku"];
    //             $callstack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2);
    //             $data["data"]["info"] = $callstack[1]['class']."::".$callstack[1]['function'].'()';
    //         }
    //     }
    //     $runId = md5(microtime(TRUE));
    //     // Adding Special prefix for runID to note if the listener is from APICORE or not
    //     if ($topic == 'transaction1021') {
    //         $runId = "APICORE".$runId;
    //     }

    //     $logger = ["topic" => $topic, "data" => $data];
    //     \Helpers\LogHelper::log("nsq", json_encode($logger), "debug");

    //     try {
    //         $this->nsq_obj->publish($topic, new \nsqphp\Message\Message(json_encode(array('data' => $data, 'run' => $runId))));
    //     } catch (\Exception $e) {
    //         \Helpers\LogHelper::log("nsq", $e->getMessage());
    //         return false;
    //     }

    //     return true;
    // }

    public function publishCluster($topic = '', $data = array())
    {
        if ($topic == 'product') {
            if (isset($data["data"]["sku"])) {
                $data["data"]["sku"] = (string) $data["data"]["sku"];
                $callstack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2);
                $data["data"]["info"] = $callstack[1]['class']."::".$callstack[1]['function'].'()';
            }
        }
        $runId = md5(microtime(TRUE));
        $logger = ["topic" => $topic, "data" => $data];
        \Helpers\LogHelper::log("nsq_http", json_encode($logger), "debug");

        try {
            $body = json_encode(array('data' => $data, 'run' => $runId));

            $nsqUrl = $_ENV['NSQ_HTTP_HOST']."/pub?topic=".$topic;
            if(!empty(getenv('HTTP_PROXY'))) {
                $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
            } else {
                $client = new \GuzzleHttp\Client();
            }
            $response = $client->post($nsqUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("nsq_http", $e->getMessage());
            return false;
        }

        return true;
    }
}