<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/15/2017
 * Time: 3:00 PM
 * User: roesmien_ecomm
 * Date: 5/16/2017
 * Time: 9:33 AM
 */

namespace Library;
use Helpers\RouteHelper;
use Phalcon\Db as Database;
use Phalcon\Mvc\Model;

class Attribute
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function updateOption($params = array())
    {
        if(empty($params['attribute_id'])) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Attribute id not found";
            return false;
        }

        if(!empty(rtrim($params['attribute_option_value'], '|'))) {
            // rtrim pipe
            $exAttributeOptionValue = explode('|', rtrim($params['attribute_option_value'], '|'));

            foreach ($exAttributeOptionValue as $ex) {
                // validate are value already exists in attribute option value
                $attributeOptionValueModel = new \Models\AttributeOptionValue();
                $checkResult = $attributeOptionValueModel->findFirst("attribute_id = ". $params['attribute_id'] ." and value = '". trim($ex) ."'");
                if (empty($checkResult)) {
                    $dataAttributeOptionValue = [
                        'attribute_id' => $params['attribute_id'],
                        'value' => trim($ex),
                        'status' => $params['status']
                    ];
                    $attributeOptionValueModel = new \Models\AttributeOptionValue();
                    $attributeOptionValueModel->assign($dataAttributeOptionValue);
                    $saveAttributeOptionValueModel = $attributeOptionValueModel->saveData("attribute_option_value");
        
                    if(!$saveAttributeOptionValueModel) {
                        $this->errorCode = "RR302";
                        $this->errorMessages = "Failed to insert attribute option value";
                        return false;
                    }
                }
            }
        }

        if(!empty($params['update'])) {
            foreach($params['update'] as $update ) {
                if(!empty($update['value'])) {
                    // validate are value already exists in attribute option value
                    $attributeOptionValueModel = new \Models\AttributeOptionValue();
                    $checkResult = $attributeOptionValueModel->findFirst("attribute_id = ". $params['attribute_id'] ." and option_id = ".$update['option_id']);
                    if (!empty($checkResult)) {
                        $attributeOptionValueModel = new \Models\AttributeOptionValue();
                        $duplicateValue = $attributeOptionValueModel->findFirst("attribute_id = ". $params['attribute_id'] ." and option_id != ".$update['option_id'] ." and value = '". $update['value'] ."'");

                        if (!empty($duplicateValue)) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Option value already exist in current attribute";
                            return false;
                        }

                        $array = [
                            'option_id' => $update['option_id'],
                            'value' => $update['value'],
                            'status' => $update['status']
                        ];

                        $attributeOptionValueModel = new \Models\AttributeOptionValue();
                        $attributeOptionValueModel->setFromArray($array);
                        $saveAttributeOptionValueModel = $attributeOptionValueModel->saveData("attribute_option_value");

                        if(!$saveAttributeOptionValueModel) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Failed to update attribute option value";
                            return false;
                        }
                    }
                }
            }
        }
    }

    public function updateUnitOptions($params = array())
    {
        if(empty($params['attribute_unit_id'])) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Attribute unit id not found";
            return false;
        }

        if(!empty(rtrim($params['attribute_option_value'], '|'))) {
            $exAttributeUnitValue = explode('|', rtrim($params['attribute_option_value'], '|'));
            foreach ($exAttributeUnitValue as $ex) {
                // validate are value already exists in attribute option value
                $attributeUnitOptionsModel = new \Models\AttributeUnitOptions();
                $checkResult = $attributeUnitOptionsModel->findFirst("attribute_unit_id = ". $params['attribute_unit_id'] ." and unit_option_value = '". trim($ex) ."'");
                if (empty($checkResult)) {
                    $dataAttributeUnitOptionsValue = [
                        'attribute_unit_id' => $params['attribute_unit_id'],
                        'unit_option_value' => trim($ex),
                        'status' => $params['status']
                    ];
                    $attributeUnitOptionsModel = new \Models\AttributeUnitOptions();
                    $attributeUnitOptionsModel->assign($dataAttributeUnitOptionsValue);
                    $saveAttributeUnitOptionsModel = $attributeUnitOptionsModel->saveData("attribute_unit_options");
        
                    if(!$saveAttributeUnitOptionsModel) {
                        $this->errorCode = "RR302";
                        $this->errorMessages = "Failed to insert attribute unit option value";
                        return false;
                    }
                }

                // no duplicate check, so it's synced with the `attribute_unit_options` table
                $dataAttributeUnitValue = [
                    'attribute_unit_id' => $params['attribute_unit_id'],
                    'value' => trim($ex),
                    'status' => ($params['status'] == 10 ? 1 : 0)
                ];

                $attributeUnitValueModel = new \Models\AttributeUnitValue();
                $attributeUnitValueModel->assign($dataAttributeUnitValue);
                $saveAttributeUnitValueModel = $attributeUnitValueModel->saveData("attribute_unit_value");

                if(!$saveAttributeUnitValueModel) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "Failed to insert attribute unit value";
                    return false;
                }
            }
        }

        if(!empty($params['update'])) {
            foreach($params['update'] as $update ) {
                if(!empty($update['unit_option_value'])) {
                    // validate are value already exists in attribute option value
                    $attributeUnitOptionsModel = new \Models\AttributeUnitOptions();
                    $checkResult = $attributeUnitOptionsModel->findFirst("attribute_unit_id = ". $params['attribute_unit_id'] ." and unit_option_id = ".$update['unit_option_id']);

                    if (!empty($checkResult)) {
                        $currentUnitValue = $checkResult->getUnitOptionValue();

                        $attributeUnitOptionsModel = new \Models\AttributeUnitOptions();
                        $duplicateValue = $attributeUnitOptionsModel->findFirst("attribute_unit_id = ". $params['attribute_unit_id'] ."  and unit_option_id != ". $update['unit_option_id'] ." and unit_option_value = '". $update['unit_option_value'] ."'");

                        if (!empty($duplicateValue)) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Option value already exist in current attribute";
                            return false;
                        }

                        $array = [
                            'unit_option_id' => $update['unit_option_id'],
                            'unit_option_value' => $update['unit_option_value'],
                            'status' => $update['status']
                        ];

                        $attributeUnitOptionsModel = new \Models\AttributeUnitOptions();
                        $attributeUnitOptionsModel->setFromArray($array);
                        $saveAttributeUnitOptionsModel = $attributeUnitOptionsModel->saveData("attribute_unit_options");

                        if(!$saveAttributeUnitOptionsModel) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Failed to update attribute unit option value";
                            return false;
                        }
                    }

                    if (empty($currentUnitValue)) {
                        continue;
                    }

                    $attributeUnitValueModel = new \Models\AttributeUnitValue();
                    $checkResult = $attributeUnitValueModel->findFirst("attribute_unit_id = ". $params['attribute_unit_id'] . " and value = '". $currentUnitValue ."'");
                    if (!empty($checkResult)) { // no duplicate check, so it's synced with the `attribute_unit_options` table
                        $attributeUnitValueId = $checkResult->getAttributeUnitValueId();

                        $array = [
                            'attribute_unit_id' => $params['attribute_unit_id'],
                            'attribute_unit_value_id' => $attributeUnitValueId,
                            'value' => $update['unit_option_value'],
                            'status' => ($update['status'] == 10 ? 1 : 0)
                        ];

                        $attributeUnitValueModel = new \Models\AttributeUnitValue();
                        $attributeUnitValueModel->setFromArray($array);
                        $saveAttributeUnitValueModel = $attributeUnitValueModel->saveData("attribute_unit_value");

                        if(!$saveAttributeUnitValueModel) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Failed to update attribute unit value";
                            return false;
                        }
                    }
                }
            }
        }
    }

    public function updateVariantOption($params = array())
    {
        if(empty($params['attribute_id'])) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Attribute id not found";
            return false;
        }

        if(!empty(rtrim($params['attribute_option_value'], '|'))) {
            // rtrim pipe
            $exAttributeOptionValue = explode('|', rtrim($params['attribute_option_value'], '|'));

            foreach ($exAttributeOptionValue as $ex) {
                // validate are value already exists in attribute option value
                $attributeVariantOptionModel = new \Models\AttributeVariantOptions();
                $checkResult = $attributeVariantOptionModel->findFirst("attribute_id = ". $params['attribute_id'] ." and option_value = '". trim($ex) ."'");
                if (empty($checkResult)) {
                    $dataAttributeVariantOptions = [
                        'attribute_id' => $params['attribute_id'],
                        'option_value' => trim($ex),
                        'status' => $params['status']
                    ];
                    $attributeVariantOptionModel = new \Models\AttributeVariantOptions();
                    $attributeVariantOptionModel->assign($dataAttributeVariantOptions);
                    $saveAttributeVariantOptionModel = $attributeVariantOptionModel->saveData("attribute_variant_options");
        
                    if(!$saveAttributeVariantOptionModel) {
                        $this->errorCode = "RR302";
                        $this->errorMessages = "Failed to insert attribute variant option";
                        return false;
                    }
                }
            }
        }

        if(!empty($params['update'])) {
            foreach($params['update'] as $update ) {
                if(!empty($update['value'])) {
                    // validate are value already exists in attribute option value
                    $attributeVariantOptionModel = new \Models\AttributeVariantOptions();
                    $checkResult = $attributeVariantOptionModel->findFirst("attribute_id = ". $params['attribute_id'] ." and option_id = ".$update['option_id']);
                    if (!empty($checkResult)) {
                        $attributeVariantOptionModel = new \Models\AttributeVariantOptions();
                        $duplicateValue = $attributeVariantOptionModel->findFirst("attribute_id = ". $params['attribute_id'] ." and option_id != ". $update['option_id'] ." and option_value = '". $update['value'] ."'");

                        if (!empty($duplicateValue)) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Option value already exist in current attribute";
                            return false;
                        }

                        $array = [
                            'option_id' => $update['option_id'],
                            'option_value' => $update['value'],
                            'status' => $update['status']
                        ];

                        $attributeVariantOptionModel = new \Models\AttributeVariantOptions();
                        $attributeVariantOptionModel->setFromArray($array);
                        $saveAttributeVariantOptionModel = $attributeVariantOptionModel->saveData("attribute_variant_options");

                        if(!$saveAttributeVariantOptionModel) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Failed to update attribute variant option";
                            return false;
                        }
                    }
                }
            }
        }
    }
}