<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


class ProductRecommendation
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param array $params
     * @return array|void
     */
    public function getProductRecommendationDetail($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';

        $productDetails = array();

        $productRecModel = new \Models\ProductRecommendation();

        if($access_type == 'cms'){
            $result = $productRecModel->findFirst(
                [
                    "conditions" => "search_term ='".$params['search_key']."' AND status > -1"
                ]
            );

            if($result){
                return $result->toArray();
            }
            else{
                $this->errorCode = "RR302";
                $this->errorMessages = "No Product Recommendation";
                return;
            }

        }
        else{
            $result = $productRecModel->findFirst(
                [
                    "conditions" => "search_term ='".$params['search_key']."' AND status = 10"
                ]
            );

            if($result){
                $result = $result->toArray();

                $products = explode(',',$result['products']);

                foreach($products as $sku){
                    $productDetail = \Helpers\ProductHelper::getFlatProductData($sku);
                    $productDetails[] = $productDetail;
                }

                return $productDetails;
            }
            else{
                $this->errorCode = "RR302";
                $this->errorMessages = "No Product Recommendation";
                return;
            }
        }


    }

    public function getProductRecommendationList($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';

        $productDetails = array();

        $productRecModel = new \Models\ProductRecommendation();

        if($access_type == 'cms'){
            $result = $productRecModel->find(
                [
                    "columns" => "recommended_id, search_term, status",
                    "conditions" => "status > -1"
                ]
            );
        }
        else{
            $result = $productRecModel->find(
                [
                    "columns" => "recommended_id, search_term, status",
                    "conditions" => "status = 10"
                ]
            );
        }

        if($result){
            return $result->toArray();
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "No Product Recommendation";
            return;
        }
    }
}