<?php

namespace Library;

use \GuzzleHttp\Client as GuzzleClient;
use \Helpers\LogHelper;
use \Helpers\SimpleEncrypt;

class APIWrapperSelma
{
    protected $client;
    protected $headers;
    protected $param;
    protected $response;
    protected $result;
    protected $error;
    protected $message;
    protected $endPoint;
    protected $data;
    protected $sessionID;
    protected $is_ok;
    protected $otp;
    protected $base_uri;

    public function __construct($base_uri = "")
    {
        if (empty($base_uri)) {
            $base_uri = !(empty($_ENV['API_URL'])) ? $_ENV['API_URL'] : '';
        }
        $this->client = new GuzzleClient(['base_uri' => $base_uri]);
        $this->param = array();
        $this->headers = array();
        $this->base_uri = $base_uri;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * @param mixed $param
     */
    public function setParam($param)
    {
        $this->param = $param;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function getOTP()
    {
        return $this->otp;
    }

    /**
     * @param mixed $message
     */
    public function setMessages($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getEndPoint()
    {
        return $this->endPoint;
    }

    /**
     * @param mixed $endPoint
     */
    public function setEndPoint($endPoint)
    {
        if (getenv('ENVIRONMENT') != "production" && $this->base_uri == "http://10.110.100.23") {
            $endPoint = "/membership_api" . $endPoint;
        }
        $this->endPoint = $endPoint;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getSessionID()
    {
        return $this->sessionID;
    }

    /**
     * @param mixed $sessionID
     */
    public function setSessionID($sessionID)
    {
        $this->sessionID = $sessionID;
    }

        /**
     * @return mixed
     */
    public function getIsOK()
    {
        return $this->is_ok;
    }

    /**
     * @param mixed $error
     */
    public function setIsOK($isOK)
    {
        $this->is_ok = $isOK;
    }


    private function _validate()
    {
        if(empty($this->endPoint)) {
            LogHelper::log("api_call", "End point '". $this->endPoint ."'not found");
            return false;
        }

        /*if(empty($this->param)) {
            LogHelper::log("api_call", "Parameter is empty");
            return false;
        }*/

        return true;
    }

    private function _mapResponse()
    {
        if(empty($this->response)) {
            LogHelper::log("api_call", "Get empty response : ". $this->endPoint ." : " . json_encode($this->message));
            return;
        }

        $responseArray = json_decode($this->response->getBody()->getContents(), true);
        
        if(!empty($responseArray['result'])) {
            $this->result = $responseArray['result'];
            if ($this->result == "KO"){
                $this->error = $responseArray['msg'];
                LogHelper::log("api_call", "Error when calling api : " . json_encode($responseArray['msg']));
            }
            if(!empty($responseArray['data'])) {
                $this->data = $responseArray['data'];
            }
        } else {
            $this->data = $responseArray;
        }

        if(!empty($responseArray['msg'])) {
            $this->message = $responseArray['msg'];
        }

        if(!empty($responseArray['message'])) {
            $this->message = $responseArray['message'];
        }

        if(!empty($responseArray['Message'])) {
            $this->message = $responseArray['Message'];
        }

        if(!empty($responseArray['otp'])) {
            $this->otp = $responseArray['otp'];
        }

        if(!empty($responseArray['is_ok'])) {
            $this->is_ok = $responseArray['is_ok'];
        }

        if(!empty($responseArray['isOK'])) {
            $this->is_ok = $responseArray['isOK'];
        }


        if($this->response->getStatusCode() < 200 || $this->response->getStatusCode() >= 300) {
            LogHelper::log("api_call", "Error getting response : ". $this->endPoint ." : " . json_encode($this->message));
        }
    }

    public function sendSelma($method = "", $timeout = "60.0")
    {
        $valid = $this->_validate();
        if(!$valid) {
            return false;
        }

        $payload_data = $this->param;

        // add default content type
        // $this->headers['Content-Type'] =  'multipart/form-data';
        // add gzip support
        $this->headers['Accept-Encoding'] =  'gzip';    
        // if not register add for header
        $this->headers['MFDEVID'] =  '231312';
        if (isset($this->sessionID)) {
            $this->headers['MFLATLON'] =  'gzip';
            $this->headers['MFSESSID'] =  $this->sessionID;
        }

        try {
            switch($method) {
                case "get" : $this->response = $this->client->get($this->endPoint, ['timeout' => $timeout, 'headers' => $this->headers]);break;
                case "put" : $this->response = $this->client->put($this->endPoint, ['multipart' => $payload_data,'timeout' => $timeout, 'headers' => $this->headers]);break;
                case "delete" : $this->response = $this->client->delete($this->endPoint, ['timeout' => $timeout, 'headers' => $this->headers]);break;
                case "postRaw" : $this->response = $this->client->request('POST',$this->endPoint, ['multipart' => $payload_data,'timeout' => $timeout, 'headers' => $this->headers]);break;
                case "postRawJson" : $this->response = $this->client->request('POST',$this->endPoint, ['json' => $payload_data,'timeout' => $timeout, 'headers' => $this->headers]);break;
                case "post" : 
                default     :$this->response = $this->client->request('POST',$this->endPoint, 
                [
                    'multipart' => [
                        [
                            'name'=>'p_encrypt',
                            'contents'=>$payload_data,
                            'Content-type' => 'multipart/form-data',
                        ],
                    ],
                    'timeout' => $timeout,
                    'headers' => $this->headers,
                ]
            );
            break;
            }
        }  catch (\Exception $e) {
            $this->error = $e->getMessage();
            \Helpers\LogHelper::log("api_call", "Error getting response : ". $this->endPoint ." : " . json_encode($e->getMessage()));
            return false;
        }

        $this->_mapResponse();
        return true;
    }
}