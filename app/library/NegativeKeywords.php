<?php
namespace Library;

use Phalcon\Db as Database;
use Helpers\LogHelper;

class NegativeKeywords
{
    protected $negativeKeywordsModel;
    protected $errorCode;
    protected $errorMessages;

    public function __construct()
    {
        $this->negativeKeywordsModel = new \Models\NegativeKeywordsModel();
    }

    /**
     * @return mixed
     */
    public function setErrorCode($errorCode){
        $this->errorCode = $errorCode;
    }

    public function setErrorMessages($errorMessages){
        $this->errorMessages = $errorMessages;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function deleteNegativeKeyword($params = array())
    {
        $negativeKeywordsModel = new \Models\NegativeKeywordsModel();

        if(isset($params['keyword_id'])){
            $alertResult = $negativeKeywordsModel::findFirst("keyword_id = '". $params['keyword_id'] ."'");
    
            if(!$alertResult) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Negative keyword not found";
                return false;
            }
    
            $negativeKeywordsModel->assign($alertResult->toArray());
      
            $saveStatus = $negativeKeywordsModel->delete("delete_keyword");;
    
            if(!$saveStatus) {
                $this->errorCode = $negativeKeywordsModel->getErrorCode();
                $this->errorMessages[] = $negativeKeywordsModel->getErrorMessages();
                return false;
            }
        } else {
            $this->errorCode = "400";
            $this->errorCode = "Missing required parameter";

            return false;
        }


        return true;
    }

    public function getAllKeywords($params = array()){
        $allKeywords = array();

        $queryPaging = "SELECT keyword_id, keyword FROM negative_keywords ORDER BY keyword_id ASC";
        $queryTotal = "SELECT COUNT(keyword_id) AS total FROM negative_keywords;";

        if (isset($params['master_app_id'])) {
            if ($params['master_app_id'] == getenv('MASTER_APP_ID_OMS')) {
                unset($params['master_app_id']);
            }
            if (!isset($params['limit']) || !isset($params['offset'])){
                $this->errorCode = "400";
                $this->errorMessages = "Missing required parameters";
                return $allKeywords;
            }

            if ($params['limit'] != 0) {
                $queryPaging .= " LIMIT " . $params['limit'];
            }

            if ($params['offset'] != 0) {
                $queryPaging .= " OFFSET " . $params['offset'];
            }

            try {
                $negativeKeywordsModel = new \Models\NegativeKeywordsModel();
                $negativeKeywordsModel->useReadOnlyConnection();
    
                $result = $negativeKeywordsModel->getDi()->getShared($negativeKeywordsModel->getConnection())->query($queryPaging);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );
                $resultArray = $result->fetchAll();
                $allKeywords['list'] = $resultArray;

                $result = $negativeKeywordsModel->getDi()->getShared($negativeKeywordsModel->getConnection())->query($queryTotal);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );
                $countArray = $result->fetch();
                $allKeywords['recordsTotal'] = $countArray['total'];
            } catch (\Exception $e) {
                $allKeywords['list'] = array();
            }
        }
        return $allKeywords;
    }


    public function checkKeywords($keyword = false){
        if(isset($keyword)){
            $query = "SELECT keyword FROM negative_keywords WHERE keyword LIKE '". $keyword . "'";
            try {
                $negativeKeywordsModel = new \Models\NegativeKeywordsModel();
                $negativeKeywordsModel->useReadOnlyConnection();
    
                $result = $negativeKeywordsModel->getDi()->getShared($negativeKeywordsModel->getConnection())->query($query);
                
                if($result->numRows() > 0){
                    $this->setErrorCode(501);
                    $this->setErrorMessages("Found ". $result->numRows() . " keyword record(s) in the database named ". $keyword ."!");
                    LogHelper::log("update_negative_keyword", "count: " . $result->numRows());
                    return false;
                }
            } catch (\Exception $e) {
                $this->setErrorCode(501);
                $this->setErrorMessages("Couldn't connect to database");
                LogHelper::log("update_negative_keyword", "query error: " . $e);
                return false;
            }
        } else {
            $this->errorCode = "400";
            $this->errorCode = "Missing required parameter";

            return false;
        }

        return true;
    }

    public function searchKeyword($params = array()){
        $matchKeywords = array();

        if (isset($params['keyword'])) {
            $query = "SELECT keyword_id, keyword FROM negative_keywords WHERE keyword LIKE '%". $params['keyword'] ."%';";
            $queryTotal = "SELECT COUNT(keyword_id)  AS total FROM negative_keywords WHERE keyword LIKE '%". $params['keyword'] ."%';";

            try {
                $negativeKeywordsModel = new \Models\NegativeKeywordsModel();
                $negativeKeywordsModel->useReadOnlyConnection();
    
                $result = $negativeKeywordsModel->getDi()->getShared($negativeKeywordsModel->getConnection())->query($query);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );
                $resultArray = $result->fetchAll();
                $matchKeywords['list'] = $resultArray;

                $result = $negativeKeywordsModel->getDi()->getShared($negativeKeywordsModel->getConnection())->query($queryTotal);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );
                $countArray = $result->fetch();
                $matchKeywords['recordsTotal'] = $countArray['total'];
            } catch (\Exception $e) {
                $matchKeywords['list'] = array();
            }
        } else {
            $this->errorCode = "400";
            $this->errorMessages = "Missing required parameter";
            
            return;
        }
        return $matchKeywords;
    }
}