<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/5/2017
 * Time: 9:06 AM
 */

namespace Library;

use GuzzleHttp\Client;
use Helpers\ParserHelper;
use Helpers\RouteHelper;
use Models\MasterBrand;
use Models\MasterUrlKey;
use Models\SettingData;
use Models\SplashPage;
use Phalcon\Db as Database;

class Route
{

    protected $errorCode;
    protected $errorMessages;
    protected $statusCode;
    protected $statusCodeMessage;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return mixed
     */
    public function getStatusCodeMessage()
    {
        return $this->statusCodeMessage;
    }

    /**
     * @param mixed $statusCodeMessage
     */
    public function setStatusCodeMessage($statusCodeMessage)
    {
        $this->statusCodeMessage = $statusCodeMessage;
    }

    public function saveData($params = array()){
        // here we need to now if url_key valid

        //        {
        //             "data": {
        //             "url_key": "testing-multivariant-new",
        //             "name": "TESTING DONG",
        //             "additional_header": "",
        //             "additional_header_mobile": "",
        //             "inline_script": "",
        //             "inline_script_mobile": "",
        //             "section": "product",
        //             "reference_id": "3", // ID => category, product, splash_page, event
        //             "company_code": "ODI"
        //            }
        //        }

        $urlModel = new MasterUrlKey();
        $result = array();
        $urlKey = isset($params['url_key']) ? $params['url_key'] : '';
        $companyCode = isset($params['company_code']) ? $params['company_code'] : 'ODI';
        $referenceId = isset($params['reference_id']) ? $params['reference_id'] : 0;
        $section = isset($params['section']) ? $params['section'] : '';

        if (empty($urlKey) || empty($referenceId) || empty($section)) {
            // dont do save
            $this->errorCode = "RR101";
            $this->errorMessages = "url_key, reference_id and section are required.";
            $this->statusCode = "400";
            $this->statusCodeMessage = "Bad Request";
            return false;
        }

        // $message = RouteHelper::checkUrlKey($referenceId, $section, $urlKey, $companyCode);
        $oldURL = (isset($params['old_url_key'])) ? $params['old_url_key'] : '';
        $message = RouteHelper::checkUrlKey($referenceId, $section, $urlKey, $companyCode, $oldURL);

        if($message['url_key_status'] == 0 || $message['url_key_status'] == 1){ // url_key existed
            $this->errorCode = 'RR102';
            $this->errorMessages = "Url key existed";
            $this->statusCode = "500";
            $this->statusCodeMessage = "Internal Server Error";
            return false;
        } else { // completely new
            $urlData = RouteHelper::buildFormatUrlKey($params);
            $urlModel->setFromArray($urlData);
            $result = $urlModel->saveUrlKey();
        }

        if ($result) {
            $this->statusCode = "200";
            $this->statusCodeMessage = "Ok";
            return $result;
        } else {
            $this->errorCode = 'RR301';
            $this->errorMessages = 'Error occurred while trying to save url_key';
            $this->statusCode = "500";
            $this->statusCodeMessage = "Internal Server Error";
            return false;
        }
    }

    public function hardDeleteData($params = array()){
        $urlModel = new MasterUrlKey();
        // $result = array();
        $urlKey = isset($params['url_key']) ? $params['url_key'] : '';
        $companyCode = isset($params['company_code']) ? $params['company_code'] : 'ODI';
        $referenceId = isset($params['reference_id']) ? $params['reference_id'] : 0;
        $section = "promo_page";

        if (empty($urlKey) || empty($referenceId)) {
            // dont do save
            $this->errorCode = "RR101";
            $this->errorMessages = "url_key and reference_id are required.";
            $this->statusCode = "400";
            $this->statusCodeMessage = "Bad Request";
            return false;
        }

        // $message = RouteHelper::checkUrlKey($referenceId, $section, $urlKey, $companyCode);
        $message = RouteHelper::checkUrlKey($referenceId, $section, "", $companyCode,$urlKey);

        if($message['url_key_status'] == 0 || $message['url_key_status'] == 2){ // url_key not existed
            $this->errorCode = 'RR102';
            $this->errorMessages = "Url key not existed";
            $this->statusCode = "500";
            $this->statusCodeMessage = "Internal Server Error";
            return false;
        } else { // completely new
            $urlKey = $urlModel->findFirst([
                "columns" => "url_key, section, additional_header, document_title, reference_id",
                "conditions" => "reference_id ='".$referenceId."'"
            ]);
            $urlData = RouteHelper::buildFormatUrlKey($urlKey->toArray());
            $urlModel->setFromArray($urlData);
            $result = $urlModel->hardDeleteUrlKey();
        }

        if ($result) {
            $this->statusCode = "200";
            $this->statusCodeMessage = "Ok";
            return $result;
        } else {
            $this->errorCode = 'RR301';
            $this->errorMessages = 'Error occurred while trying to hard delete url_key';
            $this->statusCode = "500";
            $this->statusCodeMessage = "Internal Server Error";
            return false;
        }
    }

    public function updateData($params = array()){
            //    {
            //         "data": {
            //         "change_parent": "1",
            //         "url_key": "testing-multivariant-new",
            //         "old_url_key": "testing-old",
            //         "name": "TESTING DONG",
            //         "additional_header": "",
            //         "additional_header_mobile": "",
            //         "inline_script": "",
            //         "inline_script_mobile": "",
            //         "section": "product",
            //         "reference_id": "3", // document_id elastic
            //        }
            //    }

        $urlModel = new MasterUrlKey();
        $result = array();
        $urlKey = isset($params['url_key']) ? $params['url_key'] : '';
        $companyCode = isset($params['company_code']) ? $params['company_code'] : 'ODI';
        $oldUrlKey = isset($params['old_url_key']) ? $params['old_url_key'] : '';
        $referenceId = isset($params['reference_id']) ? $params['reference_id'] : 0;
        $section = isset($params['section']) ? $params['section'] : '';

        if (empty($urlKey) || empty($referenceId) || empty($section) || empty($oldUrlKey)) {
            // dont do save
            $this->errorCode = "RR101";
            $this->errorMessages = "url_key, old_url_key, reference_id and section are required.";
            $this->statusCode = "400";
            $this->statusCodeMessage = "Bad Request";
            return false;
        }

        $message = RouteHelper::checkUrlKey($referenceId, $section, $urlKey, $companyCode, $oldUrlKey);

        if($message['url_key_status'] == 0){ // url_key existed
            $this->errorCode = 'RR102';
            $this->errorMessages = "Url key existed";
            $this->statusCode = "500";
            $this->statusCodeMessage = "Internal Server Error";
            return false;
        } else if ($message['url_key_status'] == 1) { //  url_key used to be this url_key
            $urlData = RouteHelper::buildFormatUrlKey($params);
            $urlModel->setFromArray($urlData);
            $result = $urlModel->updateUrlKey($section,$urlKey,$oldUrlKey);
        } else { // completely new
            $urlData = RouteHelper::buildFormatUrlKey($params);
            $urlModel->setFromArray($urlData);
            $result = $urlModel->saveUrlKey();
        }

        if ($result) {
            $this->statusCode = "200";
            $this->statusCodeMessage = "Ok";
            return $result;
        } else {
            $this->errorCode = 'RR301';
            $this->errorMessages = 'Error occurred while trying to update url_key';
            $this->statusCode = "500";
            $this->statusCodeMessage = "Internal Server Error";
            return false;
        }
    }

    public function changeParent($params = array()){
//                {
//                     "data": {
//                     "child_sku_doc_id": "3787", => ref_id for sku
//                     "parent_sku_doc_id": "3788", => ref_id for parent_sku
//                     "section": "product",
//                    }
//                }

        $urlModel = new MasterUrlKey();
        $result = array();
        $companyCode = isset($params['company_code']) ? $params['company_code'] : 'ODI';
        $childReferenceId = isset($params['child_sku_doc_id']) ? $params['child_sku_doc_id'] : 0;
        $parentReferenceId = isset($params['parent_sku_doc_id']) ? $params['parent_sku_doc_id'] : 0;
        $section = isset($params['section']) ? $params['section'] : '';

        if (empty($childReferenceId) || empty($parentReferenceId) || empty($section)) {
            // dont do save
            $this->errorCode = "RR101";
            $this->errorMessages = "child_reference_id, child_sku_doc_id, parent_sku_doc_id and section are required.";
            $this->statusCode = "400";
            $this->statusCodeMessage = "Bad Request";
            return false;
        }

        // search latest parent url_key
        $routeData = \Helpers\RouteHelper::getRouteData($parentReferenceId,"product", "", $companyCode);
        $parentUrlKey = substr($routeData['url_key'],0,strrpos($routeData['url_key'],'.'));

        // update all children reference_id based on identifier
        $urlModel->setFromArray(array('reference_id' => $childReferenceId, 'section' => $section, 'company_code' => $companyCode));
        $resChangeParent = $urlModel->updateCustom(array('reference_id' => $parentReferenceId), array('section' => $section, 'reference_id' => $childReferenceId, 'company_code' => $companyCode));

        // update created at parent url key
        $urlModel->setFromArray(array('reference_id' => $parentReferenceId, 'section' => $section, 'company_code' => $companyCode));
        $resUpdateParentUrl = $urlModel->updateCustom(array('created_at' => date('Y-m-d H:i:s')), array('url_key' => $parentUrlKey, 'company_code' => $companyCode));

        if ($resChangeParent && $resUpdateParentUrl) {
            $this->statusCode = "200";
            $this->statusCodeMessage = "Ok";
            return array('affected_rows' => '1');
        } else {
            $this->errorCode = 'RR301';
            $this->errorMessages = 'Error occurred while trying to change parent url_key';
            $this->statusCode = "500";
            $this->statusCodeMessage = "Internal Server Error";
            return false;
        }
    }



    public function getAllRoute($params = array())
    {
        // not maintained - these code are supposed to be part of generate sitemaps
        $urlKeyModel = new MasterUrlKey();
        $section = isset($params['section'])?$params['section']:'';

        if(!empty($section)){
            $section = json_decode($section,true);

            $findBrand = strpos($section['section'],"brand");
            $findSisterCompany = strpos($section['section'],"supplier");

            if($findBrand == false && $findSisterCompany == false){ // gak ada brand & sister_company
                $section = explode(',',$section['section']);

                foreach ($section as $key => $val){
                    $fetchNum = 0;

                    if($val == "category"){
                        $val = "product_category";
                    }

                    if($val == "inspiration"){
                        $val = "product_inspiration";
                    }

                    if($val == "static page"){
                        $val = "static_page";
                    }

                    if($val == "product"){
                        $val = "product_variant";
                        $fetchNum = 1;
                    }

                    $sql  = "SELECT * FROM ";
                    $sql .= $val;
                    $sql .= " WHERE status = 10";

                    $urlKeyModel->useReadOnlyConnection();
                    $result = $urlKeyModel->getDi()->getShared($urlKeyModel->getConnection())->query($sql);

                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );

                    $activeRecord[$val] = $result->fetchAll(Database::FETCH_COLUMN, $fetchNum);
                    $activeRecord[$val] = "('".implode("','",$activeRecord[$val])."')";
                }

                // cari url key nya deh sesuai data yang ada
                foreach ($activeRecord as $key => $record){
                    if($key == 'product_category'){
                        $key = 'category';
                    }

                    if($key == 'product_inspiration'){
                        $key = 'inspiration';
                    }

                    if($key == 'product_variant'){
                        $key = 'product';
                    }

                    if($key == 'static_page'){
                        $key = 'static page';
                    }

                    $sql = "SELECT CONCAT('https://www.ruparupa.com/',url_key) as url_key, section, created_at FROM master_url_key WHERE section IN ";
                    $sql .= "('".$key."')";
                    $sql .= " AND reference_id IN ".$record;
                    $sql .= " AND created_at IN ( ";
                    $sql .= "SELECT MAX(created_at) FROM master_url_key WHERE section IN ";
                    $sql .= "('".$key."') AND reference_id IN ".$record." GROUP BY reference_id )";

                    $urlKeyModel->useReadOnlyConnection();
                    $result = $urlKeyModel->getDi()->getShared($urlKeyModel->getConnection())->query($sql);

                    // lanjut dsini
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );

                    $urlKeyNoAttribute[] = $result->fetchAll();
                }

                $result = $urlKeyNoAttribute;

                if(empty($result)){
                    $this->errorCode = "RR302";
                    $this->errorMessages = "No url_key found. Check your master_url_key table.";
                    return false;
                }
            }
            else{
                $sectionArr = explode(',',$section['section']);

                foreach ($sectionArr as $key => $val){
                    if($val == "brand" || $val == "supplier"){
                        $sectionWithAttribute[] = $val;
                    }
                    else{
                        $sectionNoAttribute[] = $val;
                    }
                }

                foreach ($sectionWithAttribute as $key => $val){
                    // first search brand or sister_company splash page
                    $splashPageModel = new SplashPage();
                    $splashPageData = $splashPageModel->find(
                        [
                            "columns" => "splash_page_id,attribute",
                            "conditions" => "status = 10 AND attribute='".$val."'"
                        ]
                    );

                    $splashPage[$val] = $splashPageData->toArray();
                }

                if(!empty($splashPage)){
                    foreach ($splashPage as $key => $val){
                        foreach ($val as $item => $value){
                            $splashPageIdArr[$key][] = $value['splash_page_id'];
                        }

                        $splashPageId = "('".implode("','",$splashPageIdArr[$key])."')";

                        $sql = "SELECT CONCAT('https://www.ruparupa.com/',url_key) as url_key,'".$key."' AS section, created_at FROM master_url_key WHERE section = 'splash page'";
                        $sql .= " AND reference_id IN ".$splashPageId." AND created_at IN ( ";
                        $sql .= "SELECT MAX(created_at) FROM master_url_key WHERE section = 'splash page' AND reference_id IN ";
                        $sql .= $splashPageId;
                        $sql .= " GROUP BY reference_id )";
                        $urlKeyModel->useReadOnlyConnection();
                        $result = $urlKeyModel->getDi()->getShared($urlKeyModel->getConnection())->query($sql);

                        $result->setFetchMode(
                            Database::FETCH_ASSOC
                        );

                        $urlKeyAttribute[$key] = $result->fetchAll();
                    }

                    if(!empty($urlKeyAttribute)){

                        foreach ($sectionNoAttribute as $key => $val){
                            $fetchNum = 0;

                            if($val == "category"){
                                $val = "product_category";
                            }

                            if($val == "inspiration"){
                                $val = "product_inspiration";
                            }

                            if($val == "static page"){
                                $val = "static_page";
                            }

                            if($val == "product"){
                                $val = "product_variant";
                                $fetchNum = 1;
                            }

                            $sql  = "SELECT * FROM ";
                            $sql .= $val;
                            $sql .= " WHERE status = 10";

                            $urlKeyModel->useReadOnlyConnection();
                            $result = $urlKeyModel->getDi()->getShared($urlKeyModel->getConnection())->query($sql);

                            // lanjut dsini
                            $result->setFetchMode(
                                Database::FETCH_ASSOC
                            );

                            $activeRecord[$val] = $result->fetchAll(Database::FETCH_COLUMN, $fetchNum);
                            $activeRecord[$val] = "('".implode("','",$activeRecord[$val])."')";
                        }

                        // cari url key nya deh sesuai data yang ada
                        foreach ($activeRecord as $key => $record){
                            if($key == 'product_category'){
                                $key = 'category';
                            }

                            if($key == 'product_inspiration'){
                                $key = 'inspiration';
                            }

                            if($key == 'product_variant'){
                                $key = 'product';
                            }

                            if($key == 'static_page'){
                                $key = 'static page';
                            }

                            $sql = "SELECT CONCAT('https://www.ruparupa.com/',url_key) as url_key, section, created_at FROM master_url_key WHERE section IN ";
                            $sql .= "('".$key."')";
                            $sql .= " AND reference_id IN ".$record;
                            $sql .= " AND created_at IN ( ";
                            $sql .= "SELECT MAX(created_at) FROM master_url_key WHERE section IN ";
                            $sql .= "('".$key."') AND reference_id IN ".$record." GROUP BY reference_id )";

                            $urlKeyModel->useReadOnlyConnection();
                            $result = $urlKeyModel->getDi()->getShared($urlKeyModel->getConnection())->query($sql);

                            // lanjut dsini
                            $result->setFetchMode(
                                Database::FETCH_ASSOC
                            );

                            $urlKeyNoAttribute[$key] = $result->fetchAll();
                        }

                        if(!empty($urlKeyNoAttribute)){
                            // dapet semua section yang lagi aktif
                            $result = array_merge($urlKeyAttribute,$urlKeyNoAttribute);
                        }
                        else{
                            $this->errorCode = "RR302";
                            $this->errorMessages = "No url_key found. Check your master_url_key table.";
                            return false;
                        }
                    }
                    else{
                        $this->errorCode = "RR302";
                        $this->errorMessages = "No url_key found with brand's id. Check your master_url_key table.";
                        return false;
                    }
                }
                else{
                    $this->errorCode = "RR302";
                    $this->errorMessages = "No active brand found";
                    return false;
                }
            }

            return $result;
        }
        else{
            $urlKey = $urlKeyModel->find();

            if(!$urlKey) {
                $this->errorCode = "RR302";
                $this->errorMessages[] = "Url Key not found";
                return false;
            }

            return $urlKey->toArray();
        }

    }

    public function getRouteDetail($params = array())
    {
        $canonicalTag = '';
        $globalHeader = '';
        $eventDetails = array();
        $staticPageDetails = array();
        $categoryDetails = array();

        foreach ($params as $key => $value) {
            $params[$key] = preg_replace('/[^0-9a-zA-Z-\/\.]+/', '', $params[$key]);
        }

        if(isset($params['access_type'])){
            if($params['access_type'] == "cms"){
                // berarti check url key dari gideon
                // first, we need to know what section it is
                $urlKeyModel = new MasterUrlKey();
                $urlKeyResult = $urlKeyModel->findFirst([
                    "columns" => "url_key, section, additional_header, inline_script, document_title, reference_id, company_code",
                    "conditions" => "url_key ='".$params['url_key']."' AND company_code ='".$params['company_code']."'"
                ]);

                if($urlKeyResult){
                    // this is for cms only
                    return $urlKeyResult->toArray();
                }
                else{
                    $urlKeyModel = new MasterUrlKey();
                    $urlKeyResult = $urlKeyModel->findFirst([
                        "columns" => "url_key, section, additional_header, inline_script, document_title, reference_id, company_code",
                        "conditions" => "url_key ='".$params['url_key']."'"
                    ]);

                    if ($urlKeyResult) {
                        if ($urlKeyResult->toArray()['section'] !== 'product') {
                            $this->errorCode = "RR302";
                            $this->errorMessages[] = "Url Key not found";
                            return false;
                        } else {
                            return $urlKeyResult->toArray();
                        }
                    }
                }
            }
        }

        /**
         * ToDo : Get Lastest Data ( Additional Header ) not based on company_code
         */
        $urlKeyModel = new MasterUrlKey();
        $urlKey = $urlKeyModel->findFirst([
            "columns" => "url_key, section, additional_header, inline_script, document_title, reference_id, company_code",
            "conditions" => "url_key ='".$params['url_key']."' AND company_code ='".$params['company_code']."'",
        ]);

        if(!$urlKey) {
            // it means product, try to find without company_code
            $urlKeyModel = new MasterUrlKey();
            $urlKey = $urlKeyModel->findFirst([
                "columns" => "url_key, section, additional_header, inline_script, document_title, reference_id, company_code",
                "conditions" => "url_key ='".$params['url_key']."'",
            ]);

            if (!$urlKey) {
                $this->errorCode = "RR302";
                $this->errorMessages[] = "Url Key not found";
                return false;
            } else {
                if ($urlKey->toArray()['section'] !== 'product') {
                    $this->errorCode = "RR302";
                    $this->errorMessages[] = "Url Key not found";
                    return false;
                }
            }
        }

        $urls = $urlKey->toArray();
        $section = $urls['section'];

        if ($section == "product") {
            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                        '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                        '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                        '/_search?pretty';

            $client = new Client();

            $body = '{
                "_source": ["status"],
                "query": {
                  "bool": {
                    "must": [
                      {
                        "match": {
                        "_id": "'.$urls['reference_id'].'"
                        }
                      }
                    ]
                  }
                }
            }';

            $response = $client->post($elasticUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);

            $result = \GuzzleHttp\json_decode($response->getBody(), true);

            if (!isset($result['hits']['hits'][0]['_source'])) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Route doesn't exist";
                return; 
            }

            $value = $result['hits']['hits'][0]['_source'];
            
            // check whether status is active and it should be stocked
            // if ($value['status'] <= 0) {
            //     $this->errorCode = "RR302";
            //     $this->errorMessages = "Route doesn't exist";
            //     return; 
            // }

            // Add status field to section product route response
            $urls["status"] = $value['status'];
        } else {
            if (!isset($params['company_code'])) {
                $this->errorCode = "RR302";
                $this->errorMessages[] = "Url Key not found because no company code found";
                return false;
            } else {
                if ($section == "event"){
                    $eventModel = new \Models\Event();
                    $result = $eventModel->findFirst(
                        [
                            "conditions" => "event_id=".$urls['reference_id']." AND status > 0"
                        ]
                    );
                    // if status active, should we validate based on datetime ?
                    if($result){
                        $eventDetails = $result->toArray();
                    }else{
                        $this->errorCode = "RR302";
                        $this->errorMessages = "Route doesn't exist";
                        return;
                    }
                } else if ($section === "static page") {
                    $staticPageModel = new \Models\StaticPage();
                    $result = $staticPageModel->findFirst(
                        [
                            "conditions" => "static_page_id = ".$urls['reference_id']." AND status > 0"
                        ]
                    );
        
                    if($result){
                        $staticPageDetails = $result->toArray();
                    }else{
                        $this->errorCode = "RR302";
                        $this->errorMessages = "Route doesn't exist";
                        return;
                    }
                } else if ($section === "custom_category" || $section === "category") {
                    $categoryModel = new \Models\ProductCategory();
                    $result = $categoryModel->findFirst(
                        [
                            "conditions" => "category_id = ".$urls['reference_id']." AND status > 0"
                        ]
                    );
        
                    if($result){
                        $categoryDetails = $result->toArray();
                    }else{
                        $this->errorCode = "RR302";
                        $this->errorMessages = "Route doesn't exist";
                        return;
                    }
                }
            }
        }

        // now, if product, we search without company code, instead we search and filtering as usual
        $latestUrlParam = [];

        if ($section == 'product') {
            $latestUrlParam = [
                "conditions" => "reference_id = '".$urls['reference_id']."' AND section='".$urls['section']."'",
                "order" => "created_at desc"
            ];
        } else {
            $latestUrlParam = [
                "conditions" => "reference_id = '".$urls['reference_id']."' AND section='".$urls['section']."' AND company_code='".$urls['company_code']."'",
                "order" => "created_at desc"
            ];
        }

        $urlKeys = $urlKeyModel->findFirst($latestUrlParam)->toArray(null,true);

        if($urlKeys['url_key'] != $params['url_key']){
            //$canonicalTag = '<link rel="canonical" href="'.$_ENV['BASE_URL'].$urlKeys['url_key'].'.html">';
            $urls['url_key'] = $urlKeys['url_key'];
            $urls['redirect'] = true;
        } else {
            $urls['redirect'] = false;
        }

        $additionalHeader = $urlKeys['additional_header'];
        $urls['inline_script'] = $urlKeys['inline_script'];
        $urls['document_title'] = $urlKeys['document_title'];

        /**
         * ToDo : Get Global Header
         */
        $settingDataModel = new SettingData();
        $settingData = $settingDataModel->findFirst([
            "columns" => "value",
            "conditions" => " setting = 'header'",
            "order" => "updated_at desc"
        ]);

        if($settingData){
            $setting = $settingData->toArray();
            $globalHeader = trim($setting['value']);
        }

        $finalHeader = $globalHeader.$additionalHeader;
        $urls['additional_header'] = $finalHeader;

        return $urls;
    }

    public function checkNextFlashSale($params = array()){
        $urlKey = $params['url_key'];
        $date = date('Y-m-d H:i:s');

        if($urlKey == "next-flash-sale"){
            // check if event on
            $eventModel = new \Models\Event();
            $eventStatus = $eventModel->findFirst(
                [
                    "conditions" => "event_id = 1"
                ]
            );

            if($eventStatus){
                $event = $eventStatus->toArray();

                if(strtotime($event['end_date']) < strtotime($date) && $event['status'] == 10){
                    $event['status'] = '0';
                }

                if(strtotime($event['start_date']) > strtotime($date)){
                    $event['status'] = '0';
                }

                if($event['status'] == '0'){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        else{
            return true;
        }
    }

    public function checkIdentifier($params = array()){
        if(empty($params)){
            $this->errorCode = 'RR001';
            $this->errorMessages = "Identifier required";
            return false;
        }

        $masterUrlKeyModel = new MasterUrlKey();
        // prevent caching
        $sql  ="SELECT COUNT(*) as total FROM master_url_key WHERE identifier=".$params['identifier'];
        $masterUrlKeyModel->useReadOnlyConnection();
        $result = $masterUrlKeyModel->getDi()->getShared($masterUrlKeyModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $route = $result->fetch();

        if($route['total'] > 0){
            // exist
            return array('exist' => '1');
        }
        else{
            return array('exist' => '0');
        }
    }

    public function getRouteDetailByReference($params = array())
    {
        $urlKeyModel = new \Models\MasterUrlKey();
        $urlKey = $urlKeyModel->findFirst([
            "columns" => "url_key, section, additional_header, document_title, reference_id",
            "conditions" => "reference_id ='".$params['reference_id']."'"
        ]);

        if(!$urlKey) {
            $this->errorCode = "RR302";
            $this->errorMessages[] = "reference_id not found";
            return false;
        }

        return $urlKey->toArray();
    }
}