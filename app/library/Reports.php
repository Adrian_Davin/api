<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 10/27/2017
 * Time: 15:36 PM
 */

namespace Library;

class Reports
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {

    }

    public function getAllOrder($params = array())
    {
        $salesOrderModel = new \Models\SalesOrder();

        $resultSalesOrder = $salesOrderModel::query();

        if (isset($params['discount'])) {
            $resultSalesOrder->andWhere("Models\SalesOrder.discount_amount >= ". $params['discount']);

            unset($params['discount']);
        }

        foreach($params as $key => $val)
        {
            if($key == "date_from") {
                $resultSalesOrder->andWhere("created_at >= '" . $val . "'");
            } else if ($key == "date_to") {
                $resultSalesOrder->andWhere("created_at  <= '" . $val . "'");
            } else if($key != "order_by") {
                $resultSalesOrder->andWhere("Models\SalesOrder.".$key ." = '" . trim($val) ."'");
            }
        }

        if(!empty($params["order_by"])) {
            $resultSalesOrder->orderBy($params["order_by"]);
        } else {
            $resultSalesOrder->orderBy("created_at DESC");
        }

        $result = $resultSalesOrder->execute();

        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your orders";
            return array();
        } else {
            $allOrders = array();
            foreach ($result as $salesOrder) {
                $salesOrderArray = $salesOrder->toArray([],true);
                unset($salesOrderArray['customer_id']);

                $salesOrderArray['customer'] = array();
                if($salesOrder->SalesCustomer) {
                    $salesOrderArray['customer'] = $salesOrder->SalesCustomer->toArray(["customer_id","customer_firstname","customer_lastname","customer_email","customer_is_guest"]);
                }

                if($salesOrder->SalesOrderPayment) {
                    $paymentArray = $salesOrder->SalesOrderPayment->toArray();
                    unset($paymentArray['sales_order_id']);
                    $salesOrderArray['payment'] = $salesOrder->SalesOrderPayment->getDataArray();
                }

                if($salesOrder->SalesRuleOrder) {
                    foreach($salesOrder->SalesRuleOrder as $key => $salesRuleOrder) {
                        $salesOrderArray['voucher'][$key] = $salesRuleOrder->toArray(['rule_id','voucher_code','voucher_amount_used']);
                        if($salesRuleOrder->SalesRuleVoucher)
                        $salesOrderArray['voucher'][$key]['type'] = $salesRuleOrder->SalesRuleVoucher->getType();
                    }

                }
                $allOrders[] = $salesOrderArray;
            }
            $allOrders['recordsTotal'] = count($allOrders);
        }
        return $allOrders;
    }
}