<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/2/2017
 * Time: 10:07 AM
 */

namespace Library;


class LastSeen
{
    protected $last_seen_id;
    protected $customer_id;

    /**
     * @var \Library\LastSeenProduct\Collection
     */
    protected $last_seen_products;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @var $mongodb_collection MongoDB Collections
     */
    protected $mongodb_collection;

    private $_sku_list = array();

    public function __construct()
    {
        $this->last_seen_products = new \Library\LastSeenProduct\Collection();
    }

    /**
     * @return mixed
     */
    public function getLastSeenId()
    {
        return $this->last_seen_id;
    }

    /**
     * @param mixed $last_seen_id
     */
    public function setLastSeenId($last_seen_id)
    {
        $this->last_seen_id = $last_seen_id;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param mixed $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function setLastSeen($params = array())
    {
        if(!empty($params['products'])) {
            foreach ($params['products'] as $product) {
                $this->setProducts($params['products']);
                $this->_sku_list[] = $product['sku'];
            }
        }

        if(!empty($params['last_seen_id']) || !empty($params['customer_id'])) {
            $this->last_seen_id = isset($params['last_seen_id']) ? $params['last_seen_id'] : "";
            $this->customer_id = isset($params['customer_id']) ? $params['customer_id'] : 0;
            $this->loadExistingData();
        }
    }
    public function setProducts($products = array())
    {
        foreach ($products as $product)
        {
            if(in_array($product['sku'], $this->_sku_list)) {
                continue;
            } else {
                $lastSeenProduct = new \Library\LastSeenProduct();
                $lastSeenProduct->setFromArray($product);

                if(empty($lastSeenProduct->getErrorCode())) {
                    $this->_sku_list[] = $product['sku'];
                    $this->last_seen_products->append($lastSeenProduct);
                } else {
                    $this->errorCode[] = $lastSeenProduct->getErrorCode();
                    $this->errorMessages[] = $lastSeenProduct->getErrorMessages();
                }
            }
        }
    }

    public function loadExistingData()
    {
        if(!empty($this->last_seen_id) || !empty($this->customer_id)) {
            // load data from mongo db
            $mongoDb = new \Library\Mongodb();
            $mongoDb->setCollection("last_seen");
            $this->mongodb_collection = $mongoDb->getCollection();

            $searchParam = [];
            if(!empty($this->last_seen_id)) {
                $id = new \MongoDB\BSON\ObjectID($this->last_seen_id);
                $searchParam['_id'] = $id;
            }

            if(!empty($this->customer_id) && empty($this->last_seen_id)) {
                $searchParam['customer_id'] = $this->customer_id;
            }

            if(!empty($searchParam)) {
                $result = $this->mongodb_collection->findOne($searchParam);

                if(!empty($result)) {
                    if(!empty($result['customer_id'])) {
                        $this->customer_id = $result['customer_id'];
                    }

                    if(!empty($result['last_seen_id']) && empty($this->last_seen_id)) {
                        $this->last_seen_id = $result['last_seen_id'];
                    }

                    $this->setProducts($result['last_seen_products']);
                    return true;
                }
            }

            if(empty($result) || empty($searchParam)) {
                $this->errorCode = "RR302";
                $this->errorMessages[] = "Session is empty or not found";
                return false;
            }
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages[] = "Session  not found";
            return false;
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);

        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) == 'object') {
                if($key == 'mongodb_collection') continue;
                $view[$key] = $thisArray[$key]->getDataArray(); // get everything
            } else {
                $view[$key] = $val;
            }
        }

        unset($view['errorCode']);
        unset($view['errorMessages']);
        unset($view['_sku_list']);

        return $view;
    }

    public function saveLastSeen()
    {
        $lastSeenData = $this->getDataArray();

        if(empty($this->mongodb_collection)) {
            // load data from mongo db
            $mongoDb = new \Library\Mongodb();
            $mongoDb->setCollection("last_seen");
            $this->mongodb_collection = $mongoDb->getCollection();
        }

        if(empty($this->last_seen_id)) {
            // insert new one
            $insertOneResult = $this->mongodb_collection->insertOne($lastSeenData);

            if($insertOneResult->getInsertedCount() > 0)
            {
                $lastInsertId = (array)$insertOneResult->getInsertedId();

                $this->last_seen_id = $lastInsertId['oid'];
            } else {
                $this->errorCode = "RR201";
                $this->errorMessages[] = "Failed to create shopping cart";
            }
        } else {
            $id = new \MongoDB\BSON\ObjectID($this->last_seen_id);
            $this->mongodb_collection->findOneAndReplace(['_id' => $id],$lastSeenData);
        }

        return true;

    }

}