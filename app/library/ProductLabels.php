<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use Helpers\CartHelper;
use Phalcon\Db as Database;
use function GuzzleHttp\json_decode;

class ProductLabels
{
    protected $params;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }/**
     * @param mixed $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllLabelList()
    {
        $labelsList = [];
        $results = [];
        $labelModel = new \Models\ProductLabels();

        $conditions = "status > 5 and ( now() between start_date and end_date ) ";
        if(isset($this->params['access_type'])){
            if($this->params['access_type'] == 'cms'){
                $conditions = "status > -1 ";
            }
        }

        if(isset($this->params['company_code'])){
            $companyCode = $this->params['company_code'];
            if($companyCode != 'ODI'){
                $conditions .= " AND company_code = '".$companyCode."'";
            }
        }

        $labelResult = $labelModel->find([
            "columns" => "product_label_id, name, company_code, url_cloudinary, sku, additional_specification, start_date, end_date, priority, status, sync_marketing, rule_id",
            "conditions" => $conditions,
            "order" => "priority DESC"
        ]);

        $results = $labelResult->toArray();
        if(count($results) > 0){
            return $results;
        }else {
            $this->errorCode="RR302";
            $this->errorMessages="No product label has been found";
            return;
        }
    }

    public function getURLCloudinaryList() {
        $productLabelModel = new \Models\ProductLabels();

        $companyCode = (isset($this->params['company_code'])) ? strtoupper($this->params['company_code']) : 'ODI';
        $sql  = "SELECT DISTINCT(url_cloudinary) as url_cloudinary FROM product_labels WHERE status > 5 AND (now() between start_date AND end_date) AND url_cloudinary <> '' AND company_code LIKE '%$companyCode%'";
        $productLabelModel->useReadOnlyConnection();
        $result = $productLabelModel->getDi()->getShared($productLabelModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $resultData = $result->fetchAll();
        $resultList = array();
        if (!empty($resultData)) {
            foreach ($resultData as $key => $value) {
                $resultList[] = $value['url_cloudinary'];    
            }

            return $resultList;
        } else {
            $this->errorCode="RR302";
            $this->errorMessages="No product label has been found";
            return;
        }
    }

    public function getAllLabelListExpired()
    {
        $labelsList = array();
        $labelModel = new \Models\ProductLabels();

        $labelResult = $labelModel->find([
            "columns" => "product_label_id, name, url_cloudinary, sku,company_code, additional_specification, start_date, end_date, priority, status",
            "conditions" => "status = 10 and ( now() > end_date )"
        ]);

        if(!empty($labelResult)){
            return $labelResult->toArray();
        }
        else {
            $this->errorCode="RR302";
            $this->errorMessages="No product label has been found";
            return;
        }
    }

    public function getLabelDetail($params = array())
    {
        $labelDetail = array();
        $id = $params['product_label_id'];

        $labelModel = new \Models\ProductLabels();

        $findLabel = $labelModel->findFirst(
            [
                "columns" => "product_label_id, name, url_cloudinary, sku, additional_specification, start_date, end_date, priority, company_code, status, sync_marketing, rule_id",
                "conditions" => "product_label_id='".$id."'",
            ]
        );

        if(!empty($findLabel)){
            $labelDetail = $findLabel->toArray();

            return $labelDetail;
        }
        else {
            $this->errorCode = "RR001";
            $this->errorMessages = "label_id does not exist";
            return;
        }
    }

    public function getLabelDetailByName($params = array())
    {
        $labelDetail = array();
        $name = $params['name'];

        $labelModel = new \Models\ProductLabels();

        $findLabel = $labelModel->findFirst(
            [
                "columns" => "product_label_id, name, url_cloudinary, sku, additional_specification, start_date, end_date, priority, status",
                "conditions" => "name='".$name."'",
            ]
        );

        if(!empty($findLabel)){
            $labelDetail = $findLabel->toArray();

            return $labelDetail;
        }
        else {
            $this->errorCode = "RR001";
            $this->errorMessages = "label_id does not exist";
            return;
        }
    }

    public function getAcePromo()
    {
        $labelModel = new \Models\ProductLabels();
        $sql = "SELECT * FROM temp_ace_promo_del";
        $labelModel->useReadOnlyConnection();
        $result = $labelModel->getDi()->getShared($labelModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        return $result->fetchAll();

    }

    public function getAcePromoExclude()
    {
        $labelModel = new \Models\ProductLabels();
        $sql = "SELECT * FROM temp_ace_promo_exclude_del";
        $labelModel->useReadOnlyConnection();
        $result = $labelModel->getDi()->getShared($labelModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        return $result->fetchAll();

    }

    public function saveAcePromo($sku){

        $data = explode(',',$sku);
        $dataFix = array();
        foreach($data as $row){
            $dataFix[] = "('".$row."')";
        }
        $dataFix = implode(',',$dataFix);

        $labelModel = new \Models\ProductLabels();

        // Truncate Table
        $sql = "Truncate Table temp_ace_promo_del ";
        $labelModel->useWriteConnection();
        $result = $labelModel->getDi()->getShared($labelModel->getConnection())->query($sql);

        // Insert promo ace
        $sql = "Insert into temp_ace_promo_del (sku) values ".$dataFix;
        $labelModel->useWriteConnection();
        $result = $labelModel->getDi()->getShared($labelModel->getConnection())->query($sql);

        return array("success");
    }

    /**
     * Commented for future use
     */
//    public function updateLabelToVariant($params = array()){
//        $labelModel = new \Models\ProductLabels();
//
//        // delete label first
//        $this->removeLabelFromVariant($params);
//
//        $id = $params['product_label_id'];
//
//        $labelData = $labelModel->findFirst([
//            "columns" => "sku, url_cloudinary, additional_specification",
//            "conditions" => "product_label_id='".$id."'"
//        ]);
//
//        $data = $labelData->toArray();
//
//        if(!empty($data)) {
//            $url_cloud = $data['url_cloudinary'];
//            $additional_specification = $data['additional_specification'];
//
//            $sku = "('" . str_replace(',', "','", $data['sku']) . "')";
//            $sku2 = explode(',', $data['sku']);
//
//            $productModel = new \Models\ProductVariant();
//
//            // update label to product_variant
//            $sql = "UPDATE product_variant SET label='" . $url_cloud . "' ";
//            $sql .= "WHERE sku IN " . $sku;
//            $productModel->useWriteConnection();
//            $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
//
//            // update additional_specification to product
//            $sql2 = "UPDATE product SET specification = CONCAT('" . $additional_specification . "',specification)";
//            $sql2 .= " WHERE product_id IN ( ";
//            $sql2 .= "SELECT product_id FROM product_variant WHERE sku IN " . $sku . ")";
//            $productModel->useWriteConnection();
//            $result2 = $productModel->getDi()->getShared($productModel->getConnection())->query($sql2);
//
//            //update old additional specification
//            if($result2->numRows() > 0){
//                $labelModel->setLabelFromArray(array('product_label_id' => $id, 'old_additional_specification' => $additional_specification));
//                $labelModel->saveData();
//
//                // if success
//                foreach ($sku2 as $item => $val) {
//                    //Update to elastic for future used
//                    $nsq = new Nsq();
//                    $message = [
//                        "data" => ["sku" => $val],
//                        "message" => "createCache"
//                    ];
//                    $nsq->publishCluster('product', $message);
//                }
//            }
//
//            return array("success");
//        }
//        else{
//            $this->errorCode = "RR303";
//            $this->errorMessages = "Label not found!";
//        }
//
//        return ;
//    }

//    public function removeLabelFromVariant($params = array()){
//        $labelModel = new \Models\ProductLabels();
//        $id = $params['product_label_id'];
//
//        // ini untuk nentuin kalo dia di generate langsung atau lewat batch
//        // kalo langsung, berarti kita perlu cari semua sku yang nyangkut untuk di buang specification
//        // base on old spec
//        $immediate_generate = isset($params['immediate_generate'])?$params['immediate_generate']:'';
//
//        $labelData = $labelModel->findFirst([
//            "columns" => "sku, url_cloudinary, old_additional_specification",
//            "conditions" => "product_label_id='".$id."'"
//        ]);
//
//        $data = $labelData->toArray();
//
//        if(!empty($data)) {
//            $url_cloud = $data['url_cloudinary'];
//            $old_additional_specification = $data['old_additional_specification'];
//
//            $sku = "('" . str_replace(',', "','", $data['sku']) . "')";
//            $sku2 = explode(',', $data['sku']);
//
//            $productModel = new \Models\ProductVariant();
//
//            // remove label if expired
//            $sql = "UPDATE product_variant SET label='' ";
//            $sql .= "WHERE sku IN " . $sku;
//            $productModel->useWriteConnection();
//            $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
//
////            if(!empty($immediate_generate)){
////                if($immediate_generate == '1'){
////                    foreach ($sku2 as $ctr => $sku) {
////                        // we need to delete spec label's according to the sku
////                        $labelModel = new \Models\ProductLabels();
////                        $labelData = $labelModel->find(
////                            [
////                                "conditions" => "sku LIKE '%," . $sku . ",%' OR sku LIKE '%," . $sku . "' OR sku LIKE '" . $sku . ",%' OR sku LIKE '" . $sku . "'"
////                            ]
////                        );
////
////
////                    }
////                }
////            }
//            // remove additional_specification if expired
//            $sql2 = "UPDATE product SET specification = REPLACE(specification,'" . $old_additional_specification . "','')";
//            $sql2 .= " WHERE product_id IN ( ";
//            $sql2 .= "SELECT product_id FROM product_variant WHERE sku IN " . $sku . ")";
//
//            $productModel->useWriteConnection();
//            $result2 = $productModel->getDi()->getShared($productModel->getConnection())->query($sql2);
//
//            foreach ($sku2 as $item => $val) {
//                //Update to elastic for future used
//                $nsq = new Nsq();
//                $message = [
//                    "data" => ["sku" => $val],
//                    "message" => "createCache"
//                ];
//                $nsq->publishCluster('product', $message);
//            }
//            return array("success");
//        }
//        else{
//            $this->errorCode = "RR303";
//            $this->errorMessages = "Label not found!";
//        }
//
//        return ;
//
//    }

    public function updateExpiredLabel($params = array()){
        $productModel = new \Models\ProductVariant();

        // remove label if expired
        $sql = "
                UPDATE product_labels 
                SET 
                    status = 0,
                    updated_at = NOW()
                WHERE
                    (status = 10 AND DATE_ADD(NOW(), interval -3 day) > end_date) OR (status = 5)
        ";
        $productModel->useWriteConnection();
        $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);

        if($result){
            return array('success');
        }else{
            $this->errorCode = "RR303";
            $this->errorMessages = "Label can not be updated!";
        }
        return ;

    }

    public function syncWithMarketing($flagLabelID = false, $flagLabelActionID = false, $params = array()) {
        if ($flagLabelID) {
            $conditions = $params['conditions'];
            $conditions = str_replace("\\","", $conditions);
            $conditionsArray = json_decode($conditions, true);

            $isValid = true;
            if (count($conditionsArray) > 0) {
                $existCategory = false;
                $skuOperator = '';
                foreach ($conditionsArray as $row) {
                    if ($row['attribute'] == 'items.category.category_id') {
                        $existCategory = true;
                    } else if ($row['attribute'] == 'items.sku') {
                        $skuOperator = $row['operator'];
                    }
                }

                if (!$existCategory && $skuOperator == "!=") {
                    $isValid = false;
                }
            } else {
                // no condition found, it means no sku & category in condition
                $isValid = false;
            }

            $syncMarketing = $this->getSyncMarketingValue($params['label_id']);
            if ($syncMarketing && $isValid) {
                /* temporary MVP to help content manage the label automatically
                make label status to 10 every save/update salesrule,
                so that our product batch can process the elastic data, 
                coz our product batch only process label that have status 10 => "conditions" => "status = 10 and ( now() > end_date )"
                then make it to 0 after that
                */
                $params['status'] = "10";

                $paramsUpdate = array(
                    "category_id" => 0,
                    "exclude_category_id" => 0,
                    "sku" => "",
                    "exclude_sku" => "",
                    "status" => $params['status']
                );

                foreach($conditionsArray as $row) {
                    $row['attribute'] = strtolower($row['attribute']);
                    if ($row['attribute'] == 'items.sku') {
                        switch ($row['operator']) {
                            case "==" :
                                $paramsUpdate['sku'] = $row['value'];
                                break;
                            case "!=" :
                                $paramsUpdate['exclude_sku'] = $row['value'];
                                break;            
                            case "()" :
                                $paramsUpdate['sku'] = $row['value'];
                                break;
                            case "!()" :
                                $paramsUpdate['exclude_sku'] = $row['value'];
                                break;                            
                        }
                    } else if ($row['attribute'] == 'items.category.category_id') {
                        switch ($row['operator']) {
                            case "==" :
                                $paramsUpdate['category_id'] = $row['value'];
                                break;
                            case "!=" :
                                $paramsUpdate['exclude_category_id'] = $row['value'];
                                break;            
                            case "()" :
                                $paramsUpdate['category_id'] = $row['value'];
                                break;
                            case "!()" :
                                $paramsUpdate['exclude_category_id'] = $row['value'];
                                break;                            
                        }
                    }
                }

                // clear label ES for missing sku
                $this->clearLabelES($paramsUpdate['sku'], $params['label_id']);

                $sql = "update product_labels set start_date = '".$params['from_date']."', end_date = '" . $params['to_date'] . "', updated_at = now()";
                foreach($paramsUpdate as $key => $value) {
                    $sql .= ",$key = '$value'";
                }

                $sql .= " where product_label_id = " . $params['label_id'];
                $productLabelModel = new \Models\ProductLabels();
                $productLabelModel->useWriteConnection();
                $result = $productLabelModel->getDi()->getShared($productLabelModel->getConnection())->query($sql);
                if($result->numRows() == 0){
                    \Helpers\LogHelper::log("product_labels","Failed to run query: " . $sql);
                }
            }
        }

        if ($flagLabelActionID) {
            $syncMarketing = $this->getSyncMarketingValue($params['label_action_id']);
            if ($syncMarketing) {
                if (empty($params['discount_item_sku']) && (isset($paramsUpdate['sku']) && !empty($paramsUpdate['sku']))) {
                    $params['discount_item_sku'] = $paramsUpdate['sku'];
                }

                $this->clearLabelES($params['discount_item_sku'], $params['label_action_id']);

                if (isset($params['discount_item_sku']) && !empty($params['discount_item_sku'])) {
                    $sql = "update product_labels set start_date = '".$params['from_date']."', end_date = '" . $params['to_date'] . "'";
                    $sql .= ", sku = '" . $params['discount_item_sku'] . "', updated_at = now(), status = " . $params['status'];
                    $sql .= " where product_label_id = " . $params['label_action_id'];
                    $productLabelModel = new \Models\ProductLabels();
                    $productLabelModel->useWriteConnection();
                    $result = $productLabelModel->getDi()->getShared($productLabelModel->getConnection())->query($sql);
                    if($result->numRows() == 0){
                        \Helpers\LogHelper::log("product_labels","Failed to run query: " . $sql);
                    }
                }
            }
        }
    }

    private function clearLabelES($paramsUpdateSKU = "", $labelID = 0 ) {
        if (empty($paramsUpdateSKU) || $labelID < 1) {
            return;
        }

        $skuClearLabelArr = $companyCodeArr = array();

        // get existing sku
        $labelData = $this->getLabelDetail(["product_label_id" => $labelID]);
        if (isset($labelData['sku']) && isset($labelData['company_code']) && !empty($labelData['sku']) && !empty($labelData['company_code'])) {
            $skuPayloadArr = explode(",", $paramsUpdateSKU);
            
            $skuTableArr = explode(",", $labelData['sku']);
            $companyCodeArr = explode(",", $labelData['company_code']);

            // detect missing sku
            foreach($skuTableArr as $value) {
                if (!in_array($value, $skuPayloadArr)) {
                    $skuClearLabelArr[] = $value;
                }
            }
        }

        if (count($skuClearLabelArr) > 0 && count($companyCodeArr) > 0) {
            $productHelper = new \Helpers\ProductHelper();
            foreach($skuClearLabelArr as $sku) {
                // get existing label 
                $variantData = $productHelper->getVariantsFromElastic($sku);
                if (isset($variantData[0]['label'])) {
                    $existingLabel = $variantData[0]['label'];
                    // clear label ES for missing sku
                    foreach($existingLabel as $key => $value) {
                        if (in_array($key, $companyCodeArr)) {
                            $existingLabel[$key] = "";
                        }
                    }
                    
                    $labelParams = "";
                    foreach($existingLabel as $key => $value) {
                        $labelParams .= '"' . $key . '": "' . $value . '",';
                    }

                    $labelParams = rtrim($labelParams, ',');
                    $productHelper->updateProductLabel($sku, $labelParams);
                }
            }
        }

        return;
    }

    private function getSyncMarketingValue($productLabelID = 0) {
        $syncMarketing = 0;
        $productLabelModel = new \Models\ProductLabels();

        $sql  = "select sync_marketing from product_labels where product_label_id = $productLabelID";
        $productLabelModel->useReadOnlyConnection();
        $result = $productLabelModel->getDi()->getShared($productLabelModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $resultData = $result->fetch();
        if (isset($resultData['sync_marketing']) && $resultData['sync_marketing'] > 0) {
            $syncMarketing = 1;
        }

        return $syncMarketing;
    }

    private function isDateActive($fromDate = '', $toDate = '') {
        $isActive = 0;
        $productLabelModel = new \Models\ProductLabels();

        $sql = "select now() >= '$fromDate' and now() <= '$toDate' as active";
        $productLabelModel->useReadOnlyConnection();
        $result = $productLabelModel->getDi()->getShared($productLabelModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $resultData = $result->fetch();
        if ($resultData['active'] > 0) {
            $isActive = 1;
        }

        return $isActive;
    }
}