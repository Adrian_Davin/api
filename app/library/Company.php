<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 2:56 PM
 */

namespace Library;

class Company
{
    protected $company;
    protected $companyAddress;
    protected $companyCustomer;

    protected $errorCode;
    protected $errorMessages;

    public function __construct()
    {
        $this->company = new \Models\Company();
        $this->companyAddress = new \Models\CompanyAddress();
        $this->companyCustomer = new \Models\Company2Customer();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    //get only company data
    public function getCompanyList($params = array()){
        $resultCompany = $this->company->query();
        $resultCompany->where("status >= 0");
        foreach($params as $key => $val)
        {
            $resultCompany->andWhere($key ." = '" . $val ."'");
        }
        $resultCompany->orderBy("created_at DESC");

        $result = $resultCompany->execute();

        if(empty($result->count())) {
            $this->errorCode = "404";
            $this->errorMessages = "No Company Found";
            return array();
        } else {
            $allData['list'] = $result->toArray();
            $allData['recordsTotal'] = $result->count();
        }

        return $allData;
    }

    //get all company and company address data
    public function getCompanyWithAddress($params = array())
    {
        if(isset($params['customer_id'])){
            $paramsCustomer["customer_id"] = $params['customer_id'];
            unset($params['customer_id']);
        }

        if(isset($params['address_type'])){
            $paramsAddress["address_type"] = $params['address_type'];
            unset($params['address_type']);
        }

        $resultCompany = $this->company->query();
        $resultCompany->where("status >= 0");
        foreach($params as $key => $val)
        {
            $resultCompany->andWhere($key ." = '" . $val ."'");
        }

        $resultCompany->orderBy("created_at DESC");

        $result = $resultCompany->execute();
        if($result) {
            $allCompany = array();
            foreach($result as $key => $company) {
                //$allCompany[$key] = $company->toArray(array(), true);

                $paramsCustomer["company_id"] = $company->getCompanyId();
                $companyCustomer = $this->getCompanyCustomer($paramsCustomer);
                foreach($companyCustomer as $customer) {
                    $allCompany[$key] = $customer;
                }

                $paramsAddress["company_id"] = $company->getCompanyId();
                $companyAddress = $this->getCompanyAddress($paramsAddress);
                foreach($companyAddress as $address) {
                    $allCompany[$key]['address'][] = $address;
                }
            }
        }else{
            return array();
        }

        return $allCompany;
    }

    //check duplicate name
    public function cekName($params = array()){
        // Find if company name already exist
        if(isset($params['company_id']))
            $result = $this->company->findFirst("name = '". $params['name'] ."' and company_id != ".$params['company_id']);
        else
            $result = $this->company->findFirst("name = '". $params['name'] ."'");

        if($result) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Company name already exists";
            return false;
        }
    }

    public function deleteCompany($params = array())
    {
        // Find if company is exist
        $companyResult = $this->company->findFirst("company_id = '". $params['company_id'] ."'");

        if(!$companyResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Company not found";
            return false;
        }

        $this->company->assign($companyResult->toArray());
        $this->company->setStatus(-1);;
        $saveStatus = $this->company->saveData("company");;

        if(!$saveStatus) {
            $this->errorCode = $this->company->getErrorCode();
            $this->errorMessages[] = $this->company->getErrorMessages();
            return false;
        }

        return true;
    }

    //get only company address data
    public function getCompanyAddress($params = array())
    {
        $resultAddress = $this->companyAddress->query();
        foreach($params as $key => $val)
        {
            $resultAddress->andWhere($key ." = '" . $val ."'");
        }
        $resultAddress->andWhere("status >= 0");
        $resultAddress->orderBy("created_at DESC");

        $result = $resultAddress->execute();

        $allAddress = array();
        foreach($result as $address) {
            $companyAddressModel = new \Models\CompanyAddress();
            $companyAddressModel->setFromArray($address->toArray());
            $allAddress[] =  $companyAddressModel->getDataArray(array(),true);
        }
        return $allAddress;
    }

    //save company and company address data
    public function saveAddressData($params = array())
    {
        if(empty($params['company_id'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Company not found, can no add address";
            return;
        }

        // Find if customer address is exist
        if(isset($params['address_id'])) {
            $addressResult = $this->companyAddress->findFirst("address_id = '". $params['address_id'] ."'");

            if(!$addressResult) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Address not found";
                return false;
            }
        }

        $this->companyAddress->setFromArray($params);
        $saveStatus = $this->companyAddress->saveData("company_address");

        if(!$saveStatus) {
            $this->errorCode = $this->companyAddress->getErrorCode();
            $this->errorMessages[] = $this->companyAddress->getErrorMessages();
            return false;
        }

        return true;
    }

    public function deleteCompanyAddress($params = array())
    {
        // Find if company address is exist
        $addressResult = $this->companyAddress->findFirst("address_id = '". $params['address_id'] ."'");

        if(!$addressResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Address not found";
            return false;
        }

        $this->companyAddress->assign($addressResult->toArray());
        $this->companyAddress->setStatus(-1);
        $saveStatus = $this->companyAddress->saveData("company_address");;

        if(!$saveStatus) {
            $this->errorCode = $this->companyAddress->getErrorCode();
            $this->errorMessages[] = $this->companyAddress->getErrorMessages();
            return false;
        }

        return true;
    }

    //get only company user data
    public function getCompanyCustomer($params = array())
    {
        $resultCustomer = $this->companyCustomer->query();
        foreach($params as $key => $val)
        {
            $resultCustomer->andWhere($key ." = '" . $val ."'");
        }
        $resultCustomer->andWhere("status >= 0");

        $result = $resultCustomer->execute();

        $allCustomer = array();
        foreach($result as $customer) {
            $companyCustomerModel = new \Models\Company2Customer();
            $companyCustomerModel->setFromArray($customer->toArray());
            $allCustomer[] =  $companyCustomerModel->getDataArray(array(),true);
        }
        return $allCustomer;
    }

    //save company user
    public function saveCustomerData($params = array())
    {
        if(empty($params['company_id'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Company not found, can not add user";
            return;
        }

        if(empty($params['email'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Email not found, can not add user";
            return;
        }

        // Find if customer is exist
        if(isset($params['customer_id'])) {
            $checkCustomerResult = $this->companyCustomer->findFirst("company_id != '". $params['company_id'] ."' and customer_id = '". $params['customer_id'] ."' and status >= 0");
            if($checkCustomerResult) {
                $this->errorCode = "RR302";
                $this->errorMessages = "User already exist at other company";
                return false;
            }else {
                $customerResult = $this->companyCustomer->findFirst("company_id = '" . $params['company_id'] . "' and customer_id = '" . $params['customer_id'] . "'");

                if (!$customerResult) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "User not found";
                    return false;
                } else {
                    //delete customer
                    $company2CustomerModel = new \Models\Company2Customer();
                    $company2CustomerModel->assign($customerResult->toArray());
                    $company2CustomerModel->setStatus(-1);
                    $saveStatus = $company2CustomerModel->saveData("company_2_customer");

                    if (!$saveStatus) {
                        $this->errorCode = $company2CustomerModel->getErrorCode();
                        $this->errorMessages[] = $company2CustomerModel->getErrorMessages();
                        return false;
                    }
                }
            }
        }

        // Find customer_id using email
        $customerModel = new \Models\Customer();
        $customerResult = $customerModel->findFirst("email = '". $params['email'] ."'");

        if(!$customerResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "User not found";
            return false;
        }else {
            unset($params['email']);
            $params['customer_id'] = $customerResult->toArray()['customer_id'];
        }

        $checkCustomerResult = $this->companyCustomer->findFirst("company_id != '". $params['company_id'] ."' and customer_id = '". $params['customer_id'] ."' and status >= 0");
        if($checkCustomerResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "User already exist at other company";
            return false;
        }else {
            $company2CustomerModel = new \Models\Company2Customer();
            $company2CustomerModel->setFromArray($params);
            $saveStatus = $company2CustomerModel->saveData("company_2_customer");

            if (!$saveStatus) {
                $this->errorCode = $company2CustomerModel->getErrorCode();
                $this->errorMessages[] = $company2CustomerModel->getErrorMessages();
                return false;
            }
        }
        return true;
    }

    public function deleteCompanyCustomer($params = array())
    {
        // Find if company customer is exist
        $customerResult = $this->companyCustomer->findFirst("company_id = '". $params['company_id'] ."' and customer_id = '". $params['customer_id'] ."'");

        if(!$customerResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "User not found";
            return false;
        }

        $this->companyCustomer->assign($customerResult->toArray());
        $this->companyCustomer->setStatus(-1);
        $saveStatus = $this->companyCustomer->saveData("company_2_customer");;

        if(!$saveStatus) {
            $this->errorCode = $this->companyCustomer->getErrorCode();
            $this->errorMessages[] = $this->companyCustomer->getErrorMessages();
            return false;
        }

        return true;
    }
}