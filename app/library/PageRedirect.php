<?php
/**
 * Created by PhpStorm.
 * User: iwan
 * Date: 24/05/18
 * Time: 10:06
 */

namespace Library;

use Phalcon\Cache\Frontend\Data;
use Models\PageRedirect as PageRedirectModel;

class PageRedirect
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllPageRedirect($params = array())
    {
        $companyCode = (isset($params['company_code']) && !empty($params['company_code'])) ? $params['company_code'] : 'ODI';
        $pageRedirectModel = new PageRedirectModel();

        if ($companyCode == 'ODI') {
            $query = [
                "conditions" => "status > -1"
            ];
        } else {
            $query = [
                "conditions" => "status > -1 AND company_code= '".$companyCode."'"
            ];
        }

        $pageRedirectData = $pageRedirectModel->find($query);

        $pageRedirectResult = $pageRedirectData->toArray();

        if(!empty($pageRedirectResult)){
            return $pageRedirectResult;
        }
        else {
            $this->errorCode="RR302";
            $this->errorMessages="No page redirect has been found";
            return;
        }
    }

    public function getDetailPageRedirect($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';
        $companyCode = (isset($params['company_code']) && !empty($params['company_code'])) ? $params['company_code'] : 'ODI';
        $identifier = isset($params['identifier']) ? $params['identifier'] : '';

        // we have to check whether it's id or url_key
        $keyword = '';
        if (!empty($identifier)) {
            if (is_numeric($identifier) && $access_type !== 'searching') {
                $page_redirect_id = $identifier;
            } else {
                $keyword = addslashes($identifier);
            }
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages = "No data found because no identifier supplied.";
            return;
        }

        $pageRedirectModel = new PageRedirectModel();

        $status = "= 10";
        if($access_type == "cms") {
            $status = "> -1";
        }

        if(isset($page_redirect_id)) {
            if ($companyCode == 'ODI') {
                $parameter = array(
                    "columns" => "page_redirect_id, keyword, url_path, company_code, status",
                    "conditions" => "page_redirect_id='" . $page_redirect_id . "' AND status " . $status
                );
            } else {
                $parameter = array(
                    "columns" => "page_redirect_id, keyword, url_path, company_code, status",
                    "conditions" => "page_redirect_id='" . $page_redirect_id . "' AND status " . $status . " AND company_code = '" . $companyCode . "'"
                );
            }
        } else {
            $parameter = array(
                "columns" => "page_redirect_id, keyword, url_path, company_code, status",
                "conditions" => "keyword='" . $keyword . "' AND status " . $status . " AND company_code = '" . $companyCode . "'"
            );
        }

        $findPage = $pageRedirectModel->findFirst($parameter);

        if(!empty($findPage)){
            $pageRedirectDetail = $findPage->toArray();

            return $pageRedirectDetail;
        }
        else {
            $this->errorCode = "RR302";
            $this->errorMessages = "No data found";
            return;
        }
    }
}