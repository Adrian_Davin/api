<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/18/2017
 * Time: 11:11 AM
 */

namespace Library;

class Redis
{
    public static function connect($redisHost = "",$redisPort = "",$redisDb = "")
    {
        if(empty($redisHost)) {
            $redisHost = $_ENV['REDIS_HOST'];
            $redisPort = (int) $_ENV['REDIS_PORT'];
            $redisDb = $_ENV['REDIS_DB'];
        }

        try {
            $redisObj = new \Redis();
            $redisObj->connect($redisHost, $redisPort);
            $redisObj->select($redisDb);

        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return $redisObj;
    }

    public static function connectRedisMarketingMaster($redisHost = "",$redisPort = "",$redisDb = "")
    {
        if(empty($redisHost)) {
            $redisHost = $_ENV['REDIS_HOST_MARKETING_MASTER'];
            $redisPort = (int) $_ENV['REDIS_PORT_MARKETING_MASTER'];
            $redisDb = $_ENV['REDIS_DB_MARKETING_MASTER'];
        }

        try {
            $redisObj = new \Redis();
            $redisObj->connect($redisHost, $redisPort);
            $redisObj->select($redisDb);

        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return $redisObj;
    }

    public static function connectRedisMarketingReader($redisHost = "",$redisPort = "",$redisDb = "")
    {
        if(empty($redisHost)) {
            $redisHost = $_ENV['REDIS_HOST_MARKETING_READER'];
            $redisPort = (int) $_ENV['REDIS_PORT_MARKETING_READER'];
            $redisDb = $_ENV['REDIS_DB_MARKETING_READER'];
        }

        try {
            $redisObj = new \Redis();
            $redisObj->connect($redisHost, $redisPort);
            $redisObj->select($redisDb);

        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return $redisObj;
    }
    
}
