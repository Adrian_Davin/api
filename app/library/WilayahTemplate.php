<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use Helpers\CartHelper;
use Phalcon\Cache\Frontend\Data;
use Phalcon\Db as Database;

class WilayahTemplate
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllRegion($params)
    {
        $regionList = array();
        $regionModel = new \Models\ScWilayahTemplate();

        $filter = "";
        if(!empty($params['supplier_id'])){
            $filter = " AND sc.supplier_id = '".$params['supplier_id']."' ";
        }

        $sql  = "SELECT sc.wilayah_template_id, sp.name AS 'supplier_name', sc.name AS 'template_name', sc.status FROM sc_wilayah_template sc, supplier sp WHERE ";
        $sql .= "sc.supplier_id = sp.supplier_id AND sc.status > -1";
        $sql .= $filter;
        $regionModel->useReadOnlyConnection();
        $result = $regionModel->getDi()->getShared($regionModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );


        if($result->numRows() > 0){
            $regionList = $result->fetchAll();

            return $regionList;
        }
        else {
            $this->errorCode="RR302";
            $this->errorMessages="No ownfleet template has been found";
            return;
        }
    }

    public function getRegionDetail($params = array())
    {
        $regionDetail = array();

        $regionModel = new \Models\ScWilayahTemplate();

        $findTemplate = $regionModel->findFirst(
            [
                "columns" => "wilayah_template_id, supplier_id, name, status, created_at",
                "conditions" => "wilayah_template_id='".$params['wilayah_template_id']."'",
            ]
        );

        if(!empty($findTemplate)){
            $regionDetail = $findTemplate->toArray();

            return $regionDetail;
        }
        else {
            $this->errorCode = "RR001";
            $this->errorMessages = "ownfleet_template_id does not exist";
            return;
        }
    }

}