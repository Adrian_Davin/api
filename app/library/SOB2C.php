<?php
/**
 * Library to send ruparupa order to SAP system
 */

namespace Library;

class SOB2C
{
    protected $order_id;
    protected $order_date;
    protected $order_type = "B2C";
    protected $cust_name1;
    protected $cust_name2;
    protected $email;
    protected $phone;
    protected $address1;
    protected $address2;
    protected $address3;
    protected $city;
    protected $kode_jalur;
    protected $country;
    protected $postal_code;
    protected $ship_cost;
    protected $ship_cost_gc = "";
    protected $grand_price;
    protected $remark;
    protected $sales_id = "";
    protected $cust_no = "";
    protected $source = "E_COMMERCE_MAGENTO_B2C";
    protected $sloc = "";
    protected $shipping_point = "";
    protected $req_dlv_date;
    protected $sales_office = "";
    protected $sales_group = "";
    protected $booking_id = "";
    protected $payer_name1 = "";
    protected $payer_name2 = "";
    protected $payer_addr1 = "";
    protected $payer_addr2 = "";
    protected $payer_addr3 = "";
    protected $payer_post_code = "";
    protected $payer_city = "";
    protected $payer_province = "";
    protected $payer_npwp = "";
    protected $payer_email = "";
    protected $invoice_id;
    protected $delivery_mtd = "DC";

    protected $company_code = 'ODI';

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    protected $invoiceItems;

    protected $parameter= array();

    public function __construct($invoice = null)
    {
        if(!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }

    /**
     * @param array $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    public function prepareDCParams()
    {
        if(empty($this->invoice)) {
            return false;
        }

        /**
         * @var $currentOrder \Models\SalesOrder
         */
        $currentOrder = $this->invoice->SalesOrder;

        /**
         * @var $shippingAddress \Models\SalesOrderAddress
         */
        $shippingAddress = null;

        /**
         * @var $billingAddress \Models\SalesOrderAddress
         */
        // $billingAddress = null;
        // if($currentOrder->SalesOrderAddress) {
        //     foreach($currentOrder->SalesOrderAddress as $orderAddress) {
        //         $orderAddressArray = $orderAddress->toArray();
        //         $salesOrderAddress = new \Models\SalesOrderAddress();
        //         $salesOrderAddress->setFromArray($orderAddressArray);
        //         if($orderAddress->getAddressType() == 'shipping') {
        //             $shippingAddress = $salesOrderAddress->getDataArray();
        //         } else {
        //             $billingAddress = $salesOrderAddress->getDataArray();
        //         }
        //     }
        // }

        if(!empty($this->invoice->SalesShipment)) {
            $shippingAddress = $this->invoice->SalesShipment->SalesOrderAddress->getDataArray();
            // foreach($this->invoice->SalesShipment as $rowObjShipment) {
            //     $shippingAddress = $rowObjShipment->SalesOrderAddress->getDataArray();
            // }
        }

        /*
         * Preorder if item not preorder remove prefix pre from order no
         */
        $invoiceDetail = !empty($this->invoice->toArray())? $this->invoice->toArray() : array();
        if(substr( $invoiceDetail['invoice_no'], 0, 3 ) === "PRE"){
            $orderId = $currentOrder->getOrderNo();
        }else{
            $orderId = str_replace("PRE","",$currentOrder->getOrderNo());
        }

        // appending invoiceId to maintain uniqueness in case of 1 order multiple DC
        // switch odi and invoice no, to handle multiple DC 
        $this->order_id =  $orderId . '_' .  substr($this->invoice->getInvoiceNo(),strlen($this->invoice->getInvoiceNo())-3, 3);
        $this->order_date = date('Ymd',strtotime($this->invoice->getCreatedAt()));
        $this->cust_name1 =  $shippingAddress['first_name'];
        $this->cust_name2 =  $shippingAddress['last_name'];
        $this->email = $currentOrder->SalesCustomer->getCustomerEmail();
        $this->phone = !empty($shippingAddress['phone']) ? $shippingAddress['phone'] : "";
        $this->address1 = substr($shippingAddress['full_address'],0,35);
        $this->address2 = ($addr2 = substr($shippingAddress['full_address'],35,35)) != false ? $addr2 : "";
        $this->address3 = ($addr3 = substr($shippingAddress['full_address'],70,35)) != false ? $addr3 : "";
        $this->city = $shippingAddress['kecamatan']['kecamatan_code'];
        $this->kode_jalur = (isset($shippingAddress['kecamatan']['kode_jalur'])) ? $shippingAddress['kecamatan']['kode_jalur'] : '';
        $this->country  = $shippingAddress['country']['country_code'];
        $this->postal_code  = !empty($shippingAddress['post_code']) ? $shippingAddress['post_code'] : "00000";

        $this->ship_cost = number_format( ($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount()) ,0, '.', '');
        $this->grand_price = number_format($this->invoice->getGrandTotal(),0, '.', '');

        /**
         * Send AWB as Shipping Remark to SAP
         */
        $this->remark = '';
        if(!empty($this->invoice->SalesShipment)) {
            //foreach($this->invoice->SalesShipment as $rowObjShipment) {
            //    $carrierCode = !empty($rowObjShipment->ShippingCarrier->getCarrierCode()) ? $rowObjShipment->ShippingCarrier->getCarrierCode() : "";
            //    $this->remark = $carrierCode . $rowObjShipment->getTrackNumber();
            //}
            $carrierCode = !empty($this->invoice->SalesShipment->ShippingCarrier->getCarrierCode()) ? $this->invoice->SalesShipment->ShippingCarrier->getCarrierCode() : "";
            $this->remark = $carrierCode . $this->invoice->SalesShipment->getTrackNumber();
        }

        // set request delivery date different per company
        $additionalRdd = getenv('ADDITIONAL_RDD_' . $currentOrder->getCompanyCode() ? $currentOrder->getCompanyCode() : 'ODI');
        if (intval($additionalRdd) > 0) {
            $this->req_dlv_date = date('Ymd', strtotime('+'.intval($additionalRdd).' days', strtotime($this->invoice->getCreatedAt())));
        } else {
            $this->req_dlv_date = date('Ymd', strtotime($this->invoice->getCreatedAt()));
        }

        /**
         * @todo : Implement payer here
         */
        $this->payer_name1 = "";
        $this->payer_name2 = "";
        $this->payer_addr1 = "";
        $this->payer_addr2 = "";
        $this->payer_addr3 = "";
        $this->payer_post_code = "";
        $this->payer_city = "";
        $this->payer_province = "";
        $this->payer_npwp = "";
        $this->payer_email = "";
        $this->invoice_id = $this->invoice->getInvoiceNo();

        // Genereate items
        $this->invoiceItems = $this->invoice->SalesInvoiceItem;

        $companyCode = $this->invoice->getCompanyCode();;
        $this->company_code = $companyCode;

        switch ($companyCode) {
            case "AHI" :
                $this->order_type = 'B2C_AHI';
                $this->source = 'E_COMMERCE_AHI_B2C';
                $this->sloc = '1005';

                // mutate SLOC for virtual warehouse

                $warehouse = explode("_",$this->invoice->getStoreCode());
                $productSite = isset($warehouse[1]) ? $warehouse[1] : "A001";

                $virtualWarehousesEnv = getenv('VIRTUAL_WAREHOUSES_' . $this->company_code);
                $virtualWarehouses = $virtualWarehousesEnv ? explode(',', $virtualWarehousesEnv) : [];

                if (is_array($virtualWarehouses) && count($virtualWarehouses) > 0) {
                    if (in_array($productSite, $virtualWarehouses)) {
                        $virtualWarehouseSloc = getenv('VIRTUAL_WAREHOUSE_SLOC_' . $this->company_code);
                        $this->sloc = isset($virtualWarehouseSloc) ? $virtualWarehouseSloc : "1000";
                    }
                }
                if ($currentOrder->getStoreCode() !== '') {
                    $this->sales_office = $currentOrder->getStoreCode();
                }
                break;
            default :
                $this->order_type = 'B2C';
                $this->source = 'E_COMMERCE_MAGENTO_B2C';
                $this->sloc = '';
                break;
        }


    }

    public function generateDCParams()
    {
        $thisArray = get_object_vars($this);
        $b2c_order_detail = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(is_scalar($val)) {
                $currentKey = strtoupper(str_replace("_", "", $key));
                $b2c_order_detail[$currentKey] = $val;
            }
        }

        $productDetail = $this->generateDCProductDetail();

        $parameter = array(
            "b2c_order_detail" => $b2c_order_detail,
            "b2c_product_detail" => $productDetail,
            'company_code' => $this->company_code
        );

        // save to xml in case we need to call it manually
        /*$xml_data = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Order_Data_B2C></Order_Data_B2C>');
        \Helpers\ParserHelper::arrayToXml($xml_data, $parameter);*/

        // Check if folder is exist, if not create it
        /*if (!file_exists(VAR_PATH.'/sob2c/')) {
            mkdir(VAR_PATH.'/sob2c/', 0777, true);
        }*/

        //$xml_data->asXML(VAR_PATH.'/sob2c/so_'. $this->invoice->getInvoiceNo() .'.xml');
        $parameter = array("Order_Data_B2C" => $parameter);
        $this->parameter = $parameter;
    }

    public function generateDCProductDetail()
    {
        $productDetail = array();

        $idProductDetail = 0;
        if(!empty($this->invoiceItems)) {
            /**
             * @var $item \Models\SalesInvoiceItem
             */
            foreach($this->invoiceItems as $item) {
                /*
               $warehouseAttribute = $item->ProductVariant->Product->ProductAttributeInt->filter(function($attribute) {
                   if ($attribute->getAttributeId() == 329) {
                       return $attribute;
                   }
                });

                $warehouseSelection = [];
                $productSite = "O001";
                if(count($warehouseAttribute) > 0) {
                    $warehouseSelection = $warehouseAttribute[0]->getDataArray();
                    if($warehouseSelection['value'] == 'H001') {
                        $productSite = "O002";
                    }
                }
                */
                preg_match_all('/([a-zA-Z0-9]+)SET([a-zA-Z0-9]+)/', $item->getSku(), $match);
                if (count($match) >= 3 && isset($match[1][0]) && isset($match[2][0])) {
                    $itemSku = $match[1][0];
                    $qtySet = $match[2][0];
                } else {
                    preg_match_all('/([a-zA-Z0-9]+)-PRE/', strtoupper($item->getSku()), $match);
                    if (count($match) >= 2 && isset($match[1][0])) {
                        $itemSku = $match[1][0];
                    } else {
                        $itemSku = $item->getSku();
                    }
                    $qtySet = 1;
                }
                $qtyOrdered = $item->getQtyOrdered() * $qtySet;

                $getProductDetailFromElastic = \Helpers\ProductHelper::getFlatProductDetail($itemSku);

                $store_code = $item->getStoreCode();
                if (substr($store_code,0,2) == 'DC') {
                    switch ($this->company_code) {
                        case 'AHI' :
                            $warehouse = explode("_",$store_code);
                            $productSite = isset($warehouse[1]) ? $warehouse[1] : "A001";

                            $virtualWarehousesEnv = getenv('VIRTUAL_WAREHOUSES_' . $this->company_code);
                            $virtualWarehouses = $virtualWarehousesEnv ? explode(',', $virtualWarehousesEnv) : [];

                            if (is_array($virtualWarehouses) && count($virtualWarehouses) > 0) {
                                if (in_array($productSite, $virtualWarehouses)) {
                                    $virtualWarehouseReplacement = getenv('VIRTUAL_WAREHOUSE_REPLACEMENT_' . $this->company_code);
                                    $productSite = isset($virtualWarehouseReplacement) ? $virtualWarehouseReplacement : "A001";
                                }
                            }
                            break;
                        default :
                            $productSite = "O001";
                            break;
                    }
                }

                $idProductDetail++;

                $finalUnitPrice = (int)$item->getSellingPrice() - (int)$item->getDiscountAmount();
                $unitPrice = (($finalUnitPrice <= 0) ? 1 : $finalUnitPrice) / $qtySet;
                $salesPrice = $unitPrice * $qtyOrdered;
                $consignmentFlag = $this->getConsimentFlag($item->getProductSource());

                $salesUom = \Helpers\ProductHelper::getSalesUom($itemSku);
                $productDetail[] = array(
                    "ID" => $idProductDetail,
                    "PRODUCTNO" => $itemSku,
                    "UNITPRICE" => intval($unitPrice),
                    "SALESPRICE" => intval($salesPrice),
                    "QUANTITY" => $qtyOrdered,
                    "UNIT" => $salesUom,
                    "CURRENCY" => "IDR", // For now we only using IDR for transaction
                    "CONDTYPE" => "",
                    "CONSIGNMENT_FLAG" => $consignmentFlag,
                    "WARRANTY_CODE" => $getProductDetailFromElastic['warranty']['warranty_code'],
                    "SITE" => $productSite
                );

                if(!empty((int)$this->invoice->SalesOrder->getGiftCardsAmount())) {
                    $unitPriceGc = $item->getGiftCardsAmount() / $qtySet;

                    // zvo4 item price can't be higher than item unit price
                    if($unitPriceGc>$unitPrice){
                        $unitPriceGc = $unitPrice;
                    }
                    $salesProceGc = $unitPriceGc * $qtyOrdered;

                    $productDetail[] = array(
                        "ID" => $idProductDetail,
                        "PRODUCTNO" => $itemSku,
                        "UNITPRICE" => intval($unitPriceGc),
                        "SALESPRICE" => intval($salesProceGc),
                        "QUANTITY" => $qtyOrdered,
                        "UNIT" => $salesUom,
                        "CURRENCY" => "IDR", // For now we only using IDR for transaction
                        "CONDTYPE" => "ZV04",
                        "CONSIGNMENT_FLAG" => $consignmentFlag,
                        "WARRANTY_CODE" => $getProductDetailFromElastic['warranty']['warranty_code'],
                        "SITE" => $productSite
                    );

                }
            }
        }
        return $productDetail;
    }

    public function getConsimentFlag($product_source = null)
    {
        $consimentFlag = "";
        if (!empty($product_source) && strpos($product_source, 'Konsinyasi') !== false ) {
            $consimentFlag = 1;
        }

        return $consimentFlag;
    }

    public function createSOB2C()
    {
        // Get sales invoice for this invoice
        $invoiceStoreCode = $this->invoice->getStoreCode();

        // Only create SOB2C if invoice store code is DC
        if($invoiceStoreCode === 'DC'){
            $nsq = new \Library\Nsq();
            $message = [
                "data" => json_encode($this->parameter),
                "message" => "createSOB2C"
            ];
            $nsq->publishCluster('transactionSap', $message);
        }
        
        return true;
    }
}