<?php
namespace Library;

class PickupPoint
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {

    }

    public function getAllPickupPoint($params = array())
    {
        $pickupPointModel = new \Models\PickupPoint();
        $resultPickupPoint = $pickupPointModel::query();

        foreach($params as $key => $val)
        {
            if($key == "pickup_id") {
                $resultPickupPoint->andWhere("pickup_id = $val");
            }

            if($key == "status") {
                $resultPickupPoint->andWhere("status = $val");
            }

            if($key == "user_role") {
                if ($val == "pic_dc") {
                    $resultPickupPoint->andWhere("pickup_code LIKE 'DC%'");
                }
            }
        }

        if(!empty($params["order_by"])) {
            $resultPickupPoint->orderBy($params["order_by"]);
        } else {
            $resultPickupPoint->orderBy("pickup_code ASC");
        }

        $result = $resultPickupPoint->execute();

        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your pickup point";
            $allPickupPoint = array();
        } else {
            $allPickupPoint = $result->toArray();
        }

        return $allPickupPoint;
    }

    public function getPickupPointData($pickup_id = "")
    {
        $pickupPoint = $this->getAllPickupPoint(["pickup_id" => $pickup_id]);
        return $pickupPoint[0];
    }
}