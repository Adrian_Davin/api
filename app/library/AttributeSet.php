<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/15/2017
 * Time: 3:00 PM
 * User: roesmien_ecomm
 * Date: 5/16/2017
 * Time: 9:33 AM
 */

namespace Library;
use Helpers\RouteHelper;
use Phalcon\Db as Database;
use Phalcon\Mvc\Model;

class AttributeSet
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function update2Attributes($params = array())
    {
        if($params['article_hierarchy'] != '') {
            if(empty($params['attribute_id'])) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Attribute id not found";
                return false;
            }
            
            // only accept article hierarchy lv 4
            if(strlen($params['article_hierarchy']) != 12) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Article Hierarchy must lv 4";
                return false;
            }

            // Check in product_category using article hierarcy that attribute_set_id not zero
            $productCategoryModel = new \Models\ProductCategory();
            $productCategoryResult = $productCategoryModel->findFirst("article_hierarchy = '". $params['article_hierarchy'] ."'");

            if (!empty($productCategoryResult)) {
                $attributeSetId = $productCategoryResult->toArray()['attribute_set_id'];
                if (empty($attributeSetId)) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "Attribute Id can't be zero";
                    return false;    
                }
            } else {
                $this->errorCode = "RR302";
                $this->errorMessages = "Article Hierarchy not found";
                return false;
            }

            // check if attribute set id already exists?
            $attributeSet2AttributeModel = new \Models\AttributeSet2Attribute();
            $attributeSet2AttributeResult = $attributeSet2AttributeModel->findFirst("attribute_set_id = ". $attributeSetId ." and attribute_id = ".$params['attribute_id']."");

            if (empty($attributeSet2AttributeResult)) {
                $dataAttributeSet2Attribute = [
                    'attribute_set_id' => $attributeSetId,
                    'attribute_id' => $params['attribute_id'],
                    'status' => $params['status']
                ];
                // insert attribute set id and attribute id to attribute set 2 attribute
                $attributeSet2AttributeModel = new \Models\AttributeSet2Attribute();
                $attributeSet2AttributeModel->assign($dataAttributeSet2Attribute);
                $saveAttributeSet2Attribute = $attributeSet2AttributeModel->saveData("attributeset2attribute");

                if(!$saveAttributeSet2Attribute) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "Failed to insert attribute 2 attribute";
                    return false;
                }
            }
        }

        if(!empty($params['update'])) {
            foreach($params['update'] as $update ) {
                $array = [
                    'attribute_set_2_attribute_id' => $update['attribute_set_2_attribute_id'],
                    'status' => $update['status']
                ];
                
                $attributeSet2AttributeModel = new \Models\AttributeSet2Attribute();
                $attributeSet2AttributeModel->setFromArray($array);
                $saveAttributeSet2Attribute = $attributeSet2AttributeModel->saveData("attributeset2attribute");
    
                if(!$saveAttributeSet2Attribute) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "Failed to update attribute 2 attribute";
                    return false;
                }
            }
        }
    }

    public function updateAttributeSet($params = array())
    {       
        $attributeSetModel = new \Models\AttributeSet();
        $attributeSetModel->setFromArray($params);
        $saveAttributeSet = $attributeSetModel->saveData("attributeset");

        if(!$saveAttributeSet) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Failed to update master attribute set";
            return false;
        }
    }

    public function createAttributeSet($params = array())
    {
        // only accept article hierarchy lv 4
        if(strlen($params['article_hierarchy']) != 12) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Article Hierarchy must lv 4";
            return false;
        }

        // get category id from product category
        $productCategoryModel = new \Models\ProductCategory();
        $productCategoryResult = $productCategoryModel->findFirst("article_hierarchy = '". $params['article_hierarchy'] ."'");

        if (!empty($productCategoryResult)) {
            $categoryId = $productCategoryResult->toArray()['category_id'];

            // check if categoryId already exists
            $productCategoryModel = new \Models\AttributeSet();
            $productCategoryResult = $productCategoryModel->findFirst("attribute_set_name = '". substr($params['article_hierarchy'], 0, 10) ."' and category_id = ".$categoryId."");

            if (!empty($productCategoryResult)) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Article Hierarchy already used with attribute set id ".$productCategoryResult->toArray()['attribute_set_id'];
                return false;
            }
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages = "Article Hierarchy not found";
            return false;
        }

        // save to table attribute set (save 10 digit AH)
        $dataAttributeSet = [
            'category_id' => $categoryId,
            'attribute_set_name' => substr($params['article_hierarchy'], 0, 10),
            'status' => $params['status']
        ];

        // insert attribute set id and attribute id to attribute set
        $attributeSetModel = new \Models\AttributeSet();
        $attributeSetModel->assign($dataAttributeSet);
        $saveAttributeSet = $attributeSetModel->saveData("attributeset");

        if(!$saveAttributeSet) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Failed to insert attribute set";
            return false;
        }

        // get last attribute set id
        $lastId = $attributeSetModel->findFirst(['order' => 'attribute_set_id desc']);
        $attributeSetId = !empty($lastId) ? $lastId->toArray()['attribute_set_id'] : 0;
    
        $defaultAttributeId = ['185','196','209','210','223','226','229','232','233','237','241','250','278','293','301','303','310','314','323','325','329','340','395','396'];
        // insert all default attribute id to attribute set 2 attribute
        foreach ($defaultAttributeId as $key => $attribute_id){
            $updateData[] = "(".$attributeSetId.",".$attribute_id.",".$params['status'].")";
        }

        $values = implode(',',$updateData);
        try {
            $attributeSet2AttributeModel = new \Models\AttributeSet2Attribute();
            $sql  = "INSERT INTO attribute_set_2_attribute (attribute_set_id, attribute_id, status) VALUES ".$values;
            $attributeSet2AttributeModel->useWriteConnection();
            $result = $attributeSet2AttributeModel->getDi()->getShared($attributeSet2AttributeModel->getConnection())->query($sql);

            return $attributeSetId;
        } catch (\Exception $e) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Failed to insert attribute default id";
            return false;
        }
    }
}