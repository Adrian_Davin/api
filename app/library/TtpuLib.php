<?php
namespace Library;

use Models\Ttpu;
use Models\SalesInvoice;
use Models\SalesInvoiceItem;
use Models\TtpuDetail;
use Library\Invoice;

class TtpuLib
{
    protected $errorCode;
    protected $errorMessages;
    protected $ttpuModel;
    protected $invoiceLib;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {
        $this->ttpuModel = new \Models\Ttpu();
        $this->salesInvoiceModel = new \Models\SalesInvoice();
        $this->salesInvoiceItems = new \Models\SalesInvoiceItem();
        $this->invoiceLib = new \Library\Invoice();
    }

    public function saveTtpu($invoiceList){
        if(empty($invoiceList)){
            return false;
        }

        $invArray = $invoiceList['invoices'];
        $ttpuId = "";
        foreach($invArray as $keyInv => $invoice){
            /*
            * save tppu header
            */
            $getInvoice = $this->invoiceLib->getAllInvoice(["invoice_no" => $invoice['invoice_no']]);
            $invoiceDetail = $getInvoice['list'][0];
            if($keyInv==0) {
                /*
                 * get Ttpu last id
                 */
                $lastId = $this->ttpuModel->findFirst(
                    [
                        'order' => ' ttpu_id desc '
                    ]
                );

                $lastTtpu = !empty($lastId) ? $lastId->toArray() : 0;
                $ttpuId = !empty($lastTtpu['ttpu_id']) ? $lastTtpu['ttpu_id'] : 0;

                if($invoiceList['page']=="fulfillment_center"){
                    $pre = "FC";
                }else if($invoiceList['page']=="store_fulfillment"){
                    $pre = "TP";
                }else if($invoiceList['page']=="stops"){
                    $pre = "TS";
                }

                $ttpuHeader['ttpu_no'] = $pre . date("Ymd").($ttpuId + 1) . rand(10,99);
                $ttpuHeader['store_code'] = $invoiceDetail['store']['store_code'];
                $ttpuHeader['status'] = "pending";
                $ttpuHeader['page'] = $invoiceList['page'];
                $ttpuHeader['created_at'] = date("Y-m-d H:i:s");
                $ttpuHeader['created_by'] = $invoiceList['created_by'];

                $this->ttpuModel->setFromArray($ttpuHeader);
                $ttpuId = $this->ttpuModel->saveTtpuHeader();
            }


            /*
             * save ttpu detail
             */
            if (!empty($ttpuId)) {
                $ttpuDetail['ttpu_id'] = $ttpuId;
                $ttpuDetail['order_no'] = $invoiceDetail['order_no'];
                $ttpuDetail['koli'] = $invoice['koli'];
                $ttpuDetail['invoice_no'] = $invoiceDetail['invoice_no'];
                $ttpuDetail['customer_name'] = $invoiceDetail['customer']['customer_firstname'] . " " . $invoiceDetail['customer']['customer_lastname'];
                $ttpuDetail['delivery_method'] = $invoiceDetail['delivery_method'];
                $this->ttpuDetailModel = new \Models\TtpuDetail();
                $this->ttpuDetailModel->setFromArray($ttpuDetail);
                $saveTtpuDetail = $this->ttpuDetailModel->saveTtpuDetail();

                foreach ($invoiceDetail['items'] as $items) {
                    if($items['status_fulfillment']) {
                        $ttpuItems['ttpu_detail_id'] = $saveTtpuDetail;
                        $ttpuItems['sku'] = $items['sku'];
                        $ttpuItems['qty'] = $items['qty_ordered'];
                        $this->ttpuItemsModel = new \Models\TtpuItems();
                        $this->ttpuItemsModel->setFromArray($ttpuItems);
                        $saveTtpuItems = $this->ttpuItemsModel->saveTtpuItems();
                    }
                }

            }
        }
        $ttpuHeader['ttpu_id'] = $ttpuId;
        return $ttpuHeader;

    }

    public function updateStatus($params){

        if(empty($params)){
            return false;
        }

        $ttpu = $this->ttpuModel->findFirst("ttpu_id ='".$params['ttpu_id']."'");
        $ttpuDetail = array();
        if(!empty($ttpu)){
            $ttpuDetail = $ttpu->toArray();
        }

        /*
        * save tppu header
        */
        $ttpuDetail['status'] = !empty($params['status'])? $params['status'] :  $ttpuDetail['status'];
        $ttpuDetail['updated_by'] = !empty($params['updated_by'])? $params['updated_by'] :  $ttpuDetail['updated_by'];
        $this->ttpuModel->setFromArray($ttpuDetail);
        $ttpuId = $this->ttpuModel->saveTtpuHeader();

        /*
         * get ttpu list invoice
         */
        $this->ttpuDetailModel = new \Models\TtpuDetail();
        $ttpuInvoiceList = $this->ttpuDetailModel->find("ttpu_id ='".$ttpuDetail['ttpu_id']."'");
        $ttpuListDetail = array();
        if(!empty($ttpuInvoiceList)){
            $ttpuListDetail = $ttpuInvoiceList->toArray();
        }


        $invoiceItems = new \Models\SalesInvoiceItem();
        if(!empty($ttpuListDetail)) {
            foreach($ttpuListDetail as $ttpuInv){
                $query = "UPDATE sales_invoice_item SET status_fulfillment = 'complete_handover' WHERE invoice_id IN (SELECT invoice_id FROM sales_invoice WHERE invoice_no = '".$ttpuInv['invoice_no']."')";
                $invoiceItems->useReadOnlyConnection();
                $resultItems = $invoiceItems->getDi()->getShared($invoiceItems->getConnection())->query($query);
            }
        }
        $ttpuHeader['ttpu_id'] = $ttpuId;
        return $ttpuHeader;

    }

    public function ttpuList($params){

        $ttpuModel = new \Models\Ttpu();

        $flagCountOnly = FALSE;
        $where_clause = "";

        $limit = "";
        if(isset($params['limit'])){
            $limit  = $params['limit'];
        }

        $offset ="";
        if(isset($params['offset'])){
            $offset = $params['offset'];
        }

        $params['store_code'] = !empty($params['store_code'])? $params['store_code'] : "";
        $params['query_type'] = !empty($params['query_type'])? $params['query_type'] : "";
        $params['filter_group'] = !empty($params['filter_group'])? $params['filter_group'] : "";
        $params['filter_by'] = !empty($params['filter_by'])? $params['filter_by'] : "";



        if($params['query_type'] == "count_only"){
            $flagCountOnly = TRUE;
        }

        if(!empty($params['store_code']) && $params['store_code'] != "all"){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " (\Models\Ttpu.store_code IN  (SELECT \Models\Store.store_code FROM \Models\Store WHERE \Models\Store.pickup_id = (SELECT \Models\PickupPoint.pickup_id FROM \Models\PickupPoint WHERE \Models\PickupPoint.pickup_code = '".$params['store_code']."' )) ";
            $where_clause .= " OR \Models\Ttpu.store_code = '".$params['store_code']."') ";

        }

        if(!empty($params['company_code']) && $params['company_code'] != "all"){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\Ttpu.company_code IN  ('".$params['company_code']."')";
        }

        if($params['filter_by'] != "all" && !empty($params['filter_by'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\Ttpu.status = '".$params['filter_by']."'";
        }

        if(!empty($params['filter_group'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\Ttpu.page = '".$params['filter_group']."'";
        }

        /*
         * search by key word
         */
        if(!empty($params['updated_by_keyword'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\Ttpu.updated_by IN (SELECT admin_user_id FROM \Models\AdminUser WHERE \Models\AdminUser.firstname LIKE '%".$params['updated_by_keyword']."%' OR lastname LIKE '%".$params['updated_by_keyword']."%')";
        }

        if(!empty($params['store_code_keyword'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\Ttpu.store_code LIKE '%".$params['store_code_keyword']."%'";
        }

        if(!empty($params['created_by_keyword'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\Ttpu.created_by IN (SELECT admin_user_id FROM \Models\AdminUser WHERE \Models\AdminUser.firstname LIKE '%".$params['created_by_keyword']."%' OR lastname LIKE '%".$params['created_by_keyword']."%')";
        }

        if(!empty($params['ttpu_no_keyword'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\Ttpu.ttpu_no LIKE '%".$params['ttpu_no_keyword']."%'";
        }

        if(!empty($params['invoice_no_keyword'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\Ttpu.ttpu_id IN( SELECT \Models\TtpuDetail.ttpu_id FROM \Models\TtpuDetail WHERE invoice_no = '".$params['invoice_no_keyword']."')";
        }


        if(!empty($params['user_type'])){
            $userTypeArr = explode("_",$params['user_type']);
            if($userTypeArr[0]=="3pl" AND !empty($userTypeArr[1])){
                $where_clause .= empty($where_clause)? " WHERE " : " AND ";
                $where_clause .= " \Models\Ttpu.ttpu_id IN( SELECT \Models\TtpuDetail.ttpu_id FROM \Models\TtpuDetail WHERE invoice_no IN ( SELECT \Models\SalesInvoice.invoice_no FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_id IN ( ";
                $where_clause .= " SELECT \Models\SalesShipment.invoice_id FROM \Models\SalesShipment WHERE \Models\SalesShipment.carrier_id = (SELECT \Models\ShippingCarrier.carrier_id FROM \Models\ShippingCarrier WHERE \Models\ShippingCarrier.carrier_code = '".$userTypeArr[1]."')";
                $where_clause .= " )))";
                $where_clause .= " AND \Models\Ttpu.status = 'approved'";
            }
        }

        if(!empty($params['store_code_filter'])){
            $where_clause .= empty($where_clause)? " WHERE " : " OR ";
            $storeCodeListArr = explode(",",$params['store_code_filter']);
            $storeCodeListArrStr = array();
            foreach($storeCodeListArr as $storeCodeList){
                $storeCodeListArrStr[] = "'".$storeCodeList."'";
            }
            $storeCodeListStr = implode(",",$storeCodeListArrStr);

            $where_clause .= " (\Models\Ttpu.store_code IN  (SELECT \Models\Store.store_code FROM \Models\Store WHERE \Models\Store.pickup_id = (SELECT \Models\PickupPoint.pickup_id FROM \Models\PickupPoint WHERE \Models\PickupPoint.pickup_code IN($storeCodeListStr) )) ";
            $where_clause = " OR \Models\Ttpu.store_code IN ($storeCodeListStr)) ";
        }

        foreach($params as $keyParam => $param) {
            if (strpos($keyParam, '_sorting') !== false) {
                $sort = "\Models\Ttpu.".str_replace("_sorting","",$keyParam)." $param";
            }
        }

        if(empty($sort)){
            $sort = " \Models\Ttpu.created_at DESC ";
        }

        $sqlLimit = "";
        if(isset($limit) && isset($offset)){
            $sqlLimit = "LIMIT $limit OFFSET $offset";
        }

        $columns = "ttpu_id, ttpu_no, store_code, status, created_at,updated_at";
        $columns .= ",(SELECT \Models\Store.name as store_name FROM \Models\Store WHERE \Models\Store.Store_code = \Models\Ttpu.Store_code LIMIT 1) as store_name";
        $columns .= ",(SELECT CONCAT(\Models\AdminUser.firstname,' ',\Models\AdminUser.lastname) FROM \Models\AdminUser WHERE \Models\AdminUser.admin_user_id = \Models\Ttpu.created_by) as created_by";
        $columns .= ",(SELECT CONCAT(\Models\AdminUser.firstname,' ',\Models\AdminUser.lastname) FROM \Models\AdminUser WHERE \Models\AdminUser.admin_user_id = \Models\Ttpu.updated_by) as updated_by";

        $query_count = "
                        SELECT
                            count(ttpu_id) as total
                        FROM
                            \Models\Ttpu";
        $query_count .= $where_clause;

        if (isset($limit) && isset($offset)) {
            $query = "SELECT " . $columns . " FROM \Models\Ttpu " . $where_clause . " ORDER BY $sort $sqlLimit";
        } else {
            $query = "SELECT " . $columns . " FROM \Models\Ttpu " . $where_clause . " ORDER BY $sort ";
        }

        try {
            $manager = $ttpuModel->getModelsManager();

            if ($flagCountOnly) {
                $allTtpuHeader['list'] = array('query_type' => 'count_only');
            } else {
                $allTtpuHeader['list'] = $manager->executeQuery($query)->toArray();
            }

            $total = $manager->executeQuery($query_count)->toArray();
            $allTtpuHeader['recordsTotal'] = $total[0]['total'];
        } catch (\Exception $e) {
            $allTtpuHeader['list'] = array();
        }
        return $allTtpuHeader;

    }

    public function detailTtpu($params){

        $ttpuDetailModel = new \Models\TtpuDetail();
        $ttpuItemsModel = new \Models\TtpuItems();

        $flagCountOnly = FALSE;
        $where_clause = "";

        $params['query_type']  = !empty($params['query_type'])? $params['query_type'] : "";
        $params['store_code']  = !empty($params['store_code'])? $params['store_code'] : "";
        $params['ttpu_no']     = !empty($params['ttpu_no'])? $params['ttpu_no'] : "";
        $params['filter_by']   = !empty($params['filter_by'])? $params['filter_by'] : "";

        if($params['query_type'] == "count_only"){
            $flagCountOnly = TRUE;
        }

        if($params['filter_by'] != "all" && !empty($params['filter_by'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\TtpuDetail.status = '".$params['filter_by']."'";
        }

        if($params['store_code'] != "all" && !empty($params['store_code'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\TtpuDetail.store_code = '".$params['store_code']."'";
        }

        if($params['ttpu_no'] && !empty($params['ttpu_no'])){
            $where_clause .= empty($where_clause)? " WHERE " : " AND ";
            $where_clause .= " \Models\TtpuDetail.ttpu_id = (SELECT \Models\Ttpu.ttpu_id FROM \Models\Ttpu WHERE \Models\Ttpu.ttpu_no = '".$params['ttpu_no']."')";
        }

        if(!empty($params['user_type'])){
            $userTypeArr = explode("_",$params['user_type']);
            if($userTypeArr[0]=="3pl" AND !empty($userTypeArr[1])){
                $where_clause .= empty($where_clause)? " WHERE " : " AND ";
                $where_clause .= " \Models\TtpuDetail.invoice_no IN ( SELECT \Models\SalesInvoice.invoice_no FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_id IN ( ";
                $where_clause .= " SELECT \Models\SalesShipment.invoice_id FROM \Models\SalesShipment WHERE \Models\SalesShipment.carrier_id = (SELECT \Models\ShippingCarrier.carrier_id FROM \Models\ShippingCarrier WHERE \Models\ShippingCarrier.carrier_code = '".$userTypeArr[1]."')";
                $where_clause .= " ))";
            }
        }
       
        $sort = " \Models\TtpuDetail.ttpu_detail_id DESC ";
        
        $columns = "ttpu_detail_id, ttpu_id, order_no, invoice_no, customer_name, delivery_method, koli";
        $columns .= ",(SELECT \Models\Ttpu.ttpu_no FROM \Models\Ttpu WHERE \Models\Ttpu.ttpu_id = \Models\TtpuDetail.ttpu_id LIMIT 1) as ttpu_no";
        $columns .= ",(SELECT \Models\Ttpu.status FROM \Models\Ttpu WHERE \Models\Ttpu.ttpu_id = \Models\TtpuDetail.ttpu_id LIMIT 1) as status";
        $columns .= ",(SELECT \Models\SalesInvoice.invoice_id FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_no = \Models\TtpuDetail.invoice_no LIMIT 1) as invoice_id";
        $columns .= ",(SELECT \Models\SalesInvoice.receipt_id FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_no = \Models\TtpuDetail.invoice_no LIMIT 1) as receipt_id";
        $columns .= ",(SELECT \Models\SalesInvoice.real_price FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_no = \Models\TtpuDetail.invoice_no LIMIT 1) as real_price";
        $columns .= ",(SELECT \Models\SalesShipment.shipment_id FROM \Models\SalesShipment WHERE \Models\SalesShipment.invoice_id = (SELECT \Models\SalesInvoice.invoice_id FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_no = \Models\TtpuDetail.invoice_no LIMIT 1) LIMIT 1) as shipment_id";
        $columns .= ",(SELECT \Models\SalesShipment.track_number FROM \Models\SalesShipment WHERE \Models\SalesShipment.invoice_id = (SELECT \Models\SalesInvoice.invoice_id FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_no = \Models\TtpuDetail.invoice_no LIMIT 1) LIMIT 1) as track_number";
        $columns .= ",(SELECT \Models\SalesShipmentTracking.status FROM \Models\SalesShipmentTracking WHERE \Models\SalesShipmentTracking.remark = 'picker_data' AND \Models\SalesShipmentTracking.shipment_id = (SELECT \Models\SalesShipment.shipment_id FROM \Models\SalesShipment WHERE \Models\SalesShipment.invoice_id = (SELECT \Models\SalesInvoice.invoice_id FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_no = \Models\TtpuDetail.invoice_no LIMIT 1) LIMIT 1)) as tracking_status";
        $columns .= ",(SELECT CONCAT(\Models\SalesOrderAddress.first_name,' ',\Models\SalesOrderAddress.last_name) FROM \Models\SalesOrderAddress WHERE \Models\SalesOrderAddress.address_type = 'shipping' AND \Models\SalesOrderAddress.sales_order_id = (SELECT \Models\SalesInvoice.sales_order_id FROM \Models\SalesInvoice WHERE \Models\SalesInvoice.invoice_no = \Models\TtpuDetail.invoice_no LIMIT 1) AND \Models\SalesOrderAddress.group_shipment = (SELECT \Models\SalesOrderItem.group_shipment FROM \Models\SalesOrderItem WHERE \Models\SalesOrderItem.sales_order_id = (SELECT \Models\SalesOrder.sales_order_id FROM \Models\SalesOrder WHERE \Models\SalesOrder.order_no = \Models\TtpuDetail.order_no) AND \Models\SalesOrderItem.sku = (SELECT \Models\TtpuItems.sku FROM \Models\TtpuItems WHERE \Models\TtpuItems.ttpu_detail_id = \Models\TtpuDetail.ttpu_detail_id LIMIT 1) LIMIT 1) LIMIT 1) as shipping_to";
        $columns .= ",(SELECT \Models\Ttpu.page FROM \Models\Ttpu WHERE \Models\Ttpu.ttpu_id = \Models\TtpuDetail.ttpu_id LIMIT 1) as page";
        $columns .= ",(SELECT \Models\Ttpu.store_code FROM \Models\Ttpu WHERE \Models\Ttpu.ttpu_id = \Models\TtpuDetail.ttpu_id LIMIT 1) as store_code";
        //$columns .= ",(SELECT CONCAT(\Models\AdminUser.firstname,' ',\Models\AdminUser.lastname) FROM \Models\AdminUser WHERE \Models\AdminUser.admin_user_id = (SELECT \Models\Ttpu.created_by FROM \Models\Ttpu WHERE \Models\Ttpu.ttpu_id = \Models\Ttpu.ttpu_id LIMIT 1 )) as created_by";

        $query_count = "
                        SELECT
                            count(ttpu_detail_id) as total
                        FROM
                            \Models\TtpuDetail";
        $query_count .= $where_clause;

        if (isset($limit) && isset($offset)) {
            $query = "SELECT " . $columns . " FROM \Models\TtpuDetail " . $where_clause . " ORDER BY $sort LIMIT $limit OFFSET $offset";
        } else {
            $query = "SELECT " . $columns . " FROM \Models\TtpuDetail " . $where_clause . " ORDER BY $sort ";
        }

        try {
            $manager = $ttpuDetailModel->getModelsManager();

            if ($flagCountOnly) {
                $allTtpuDetail['list'] = array('query_type' => 'count_only');
            } else {

                if($params['ttpu_no'] && !empty($params['ttpu_no'])) {
                    $queryHeaderColumn = "";
                    $queryHeaderColumn = ",(SELECT CONCAT(\Models\AdminUser.firstname,' ',\Models\AdminUser.lastname) FROM \Models\AdminUser WHERE \Models\AdminUser.admin_user_id = \Models\Ttpu.created_by) as created_by";
                    $queryHeaderDetail = "SELECT ttpu_id, page, ttpu_no, store_code, status, created_at,updated_at ".$queryHeaderColumn." FROM \Models\Ttpu WHERE \Models\Ttpu.ttpu_no = '".$params['ttpu_no']."'";
                    $dataHeaderDetail = $manager->executeQuery($queryHeaderDetail)->toArray();
                    $dataHeaderDetail = !empty($dataHeaderDetail[0])? $dataHeaderDetail[0] : array();
                }

                $allTtpuDetail['list'] = array();
                $data = $manager->executeQuery($query)->toArray();
                foreach($data as $keyItems => $detail){
                    $getItems = $ttpuItemsModel->find("ttpu_detail_id = ".$detail['ttpu_detail_id']);
                    $items = array("items" => !empty($getItems)? $getItems->toArray() : array());
                    $detail = array_merge($detail,$items);
                    $allTtpuDetail['list']['invoices'][$keyItems] = $detail;
                }
            }

            $allTtpuDetail['list'] = array_merge($dataHeaderDetail,$allTtpuDetail['list']);
            $total = $manager->executeQuery($query_count)->toArray();
            $allTtpuDetail['recordsTotal'] = $total[0]['total'];
        } catch (\Exception $e) {
            $allTtpuDetail['list'] = array();
        }

        return $allTtpuDetail;
    }

}