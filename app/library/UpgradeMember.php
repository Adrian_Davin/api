<?php

namespace Library;

use \Helpers\LogHelper;
use Models\Customer2Group;
use Models\CustomerGroup;

class UpgradeMember
{
    protected $P_BU_Code;
    protected $P_Card_ID;
    protected $P_Created_By;
    protected $P_Receive_No;

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    public function __construct($invoice = null)
    {
        if (!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function prepareUpgradeMemberParams($customerId)
    {
        $invoiceData = $this->invoice->getDataArray([], true);
        
        $invoiceStoreCode = $this->invoice->getStoreCode();
        $storeCode = '';

        $salesOrderItemModel = new \Models\SalesOrderItem;
        if(!empty($invoiceStoreCode) && $invoiceStoreCode === "A300") {
            $storeCode = 'AHI';

            $salesOrderItemCondition = array(
                "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "' AND sku = '". getenv('MEMBERSHIP_AHI_SKU') ."'"
            );
            $salesOrderItemResult = $salesOrderItemModel->findFirst($salesOrderItemCondition)->toArray();
        } else if(!empty($invoiceStoreCode) && $invoiceStoreCode === "H300") {
            $storeCode = 'HCI';

            $salesOrderItemCondition = array(
                "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "' AND sku = '". getenv('MEMBERSHIP_HCI_SKU') ."'"
            );
            $salesOrderItemResult = $salesOrderItemModel->findFirst($salesOrderItemCondition)->toArray();
        } else if(!empty($invoiceStoreCode) && $invoiceStoreCode === "H203") {
            $storeCode = 'SELMA';

            $salesOrderItemCondition = array(
                "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "' AND sku = '". getenv('MEMBERSHIP_SLM_SKU') ."'"
            );
        } else if(!empty($invoiceStoreCode) && $invoiceStoreCode === "T300") {
            $storeCode = 'TGI';

            $salesOrderItemCondition = array(
                "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "' AND sku = '". getenv('MEMBERSHIP_TGI_SKU') ."'"
            );
            $salesOrderItemResult = $salesOrderItemModel->findFirst($salesOrderItemCondition)->toArray();
        }

        $matches = array();
        preg_match('/(&|\?)temp_member=([a-zA-Z0-9]+)/i', $salesOrderItemResult['first_utm_parameter'], $matches);
        $memberNumber = $matches[2];

        $this->P_BU_Code = (isset($storeCode)? $storeCode : "");

        $selmaApps = ["selma-android", "selma-ios", "selma-huawei"];
        $informaApps = ["informa-android", "informa-ios", "informa-huawei"];
        $salesOrderModel = new \Models\SalesOrder;
        $salesOrderCondition = array(
            "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "'"
        );
        $salesOrderResult = $salesOrderModel->findFirst($salesOrderCondition);
    
        if(!empty($salesOrderResult)) {
            if($memberNumber == "" && !in_array($salesOrderResult->getDevice(), array_merge($selmaApps, $informaApps))) {
                $customer2GroupModel = new Customer2Group();

                $groupId = 0;
                    
                $customerGroupModel = new CustomerGroup();
                $customerGroupResult = $customerGroupModel->findFirst(
                    array(
                        "conditions" => "group_name = '" . $storeCode . "'"
                    )
                );

                if(!empty($customerGroupResult)) {
                    $groupId = $customerGroupResult->getGroupId();
                }

                if($groupId > 0) {
                    $findMemberNumber = $customer2GroupModel->findFirst(
                        array(
                            "conditions" => "customer_id = '" . $customerId . "' AND group_id = '" . $groupId . "'"
                        )
                    );

                    $memberNumber = !empty($findMemberNumber->getMemberNumber())? $findMemberNumber->getMemberNumber() : "";
                }
            }

            if ($memberNumber == "" && $storeCode == 'HCI' && in_array($salesOrderResult->getDevice(), $informaApps)) {
                $informaAppLib = new InformaApp();
                $customerInformaDetail = $informaAppLib->getCustomerDetail("customer_id = '" . $customerId . "'");
                if (!$customerInformaDetail) {
                    LogHelper::log("informa/informa_upgrade_member", "prepareUpgradeMemberParams errCode: ". $informaAppLib->getErrorCode() . " err: " . $informaAppLib->getErrorMessages() . " customerID: " . $customerId);
                } else {
                    $memberNumber = (isset($customerInformaDetail["informa_customer_tmp_member_no"])? $customerInformaDetail["informa_customer_tmp_member_no"] : "");
                }
            } else if($memberNumber == "" && $storeCode == 'SELMA' && in_array($salesOrderResult->getDevice(), $selmaApps)) {
                $selmaAppLib = new SelmaApp();
                $customerSelmaDetail = $selmaAppLib->getCustomerDetail("customer_id = '" . $customerId . "'");
                if (!$customerSelmaDetail) {
                    LogHelper::log("selma/selma_upgrade_member", "prepareUpgradeMemberParams errCode: ". $selmaAppLib->getErrorCode() . " err: " . $selmaAppLib->getErrorMessages() . " customerID: " . $customerId);
                } else {
                    $memberNumber = (isset($customerSelmaDetail["selma_customer_tmp_member_no"])? $customerSelmaDetail["selma_customer_tmp_member_no"] : "");
                }
            }
        }        

        $this->P_Card_ID = !empty($memberNumber)? $memberNumber : ""; 
        $this->P_Created_By = "ODI";
        $this->P_Receive_No = $invoiceData["invoice_no"];
    }

    public function getParams()
    {
        $thisArray = get_object_vars($this);
        $parameter = array();
        foreach ($thisArray as $key => $val) {
            if ($key !== "invoice") {
                $parameter[$key] = $val;
            }
        }

        return $parameter;
    }

    public function createUpgradeMember()
    {
        $parameter = $this->getParams();
        $nsq = new \Library\Nsq();
        $message = [
            "data" => json_encode($parameter),
            "message" => "upgradeMember",
            "invoice_no" => $this->invoice->getInvoiceNo()
        ];
        // LogHelper::log("test", "ugradeMember message ". json_encode($message));
        $nsq->publishCluster('transactionSap', $message);
        return true;
    }
}
