<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 05/09/2017
 * Time: 2:47 PM
 */

namespace Library;

class Sosmed
{
    protected $loginResult;
    protected $userParams;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @var $customer \Models\Customer
     */
    protected $customerModel;

    public function __construct()
    {
        $this->customerModel = new \Models\Customer();
    }

    /**
     * @return mixed
     */
    public function getLoginResult()
    {
        return $this->loginResult;
    }

    /**
     * @param mixed $loginResult
     */
    public function setLoginResult($loginResult)
    {
        $this->loginResult = $loginResult;
    }

    /**
     * @return mixed
     */
    public function getUserParams()
    {
        return $this->userParams;
    }

    /**
     * @param mixed $userParams
     */
    public function setUserParams($userParams)
    {
        $this->userParams = $userParams;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function loginUrl($params = array())
    {
        $loginUrl = '';
        if ($params['type'] == 'google') {
            $gClientRedirect = $_ENV['G_CLIENT_REDIRECT_URL_HOMEPAGE'];
            if (isset($params['requester'])) {
                if ($params['requester'] == 'homepage') {
                    $gClientRedirect = $_ENV['G_CLIENT_REDIRECT_URL_HOMEPAGE'];
                } elseif ($params['requester'] == 'payment') {
                    $gClientRedirect = $_ENV['G_CLIENT_REDIRECT_URL_PAYMENT'];
                }
            }

            if (!empty($gClientRedirect)) {
                $loginUrl = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' .
                    urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me') .
                    '&redirect_uri=' . urlencode($gClientRedirect) . '&response_type=code&client_id=' . $_ENV['G_CLIENT_ID'] . '&access_type=online';
            }
        } elseif($params['type'] == 'facebook') {
            $paramsFLib['requester'] = (isset($params['requester'])) ? $params['requester'] : 'homepage';
            $facebookLib = new \Library\Sosmed\Fb($paramsFLib);
            $loginUrl = $facebookLib->getLoginUrl($params);
        }

        return $loginUrl;
    }

    public function authorizeLogin($params = array())
    {
        if ($params['type'] == 'google') {
            $this->authorizeLoginGoogle($params);
        } elseif ($params['type'] == 'facebook') {
            $this->authorizeLoginFacebook($params);
        }

        return array();
    }

    public function authorizeLoginGoogle($params = array())
    {
        $token = (isset($params['token'])) ? $params['token'] : NULL;

        $googleLib = new \Library\Sosmed\Google();
        if (!isset($params['token'])) {
            if (isset($params['code'])) {
                $gClientRedirect = $_ENV['G_CLIENT_REDIRECT_URL_HOMEPAGE'];
                if (isset($params['requester'])) {
                    if ($params['requester'] == 'homepage') {
                        $gClientRedirect = $_ENV['G_CLIENT_REDIRECT_URL_HOMEPAGE'];
                    } elseif ($params['requester'] == 'payment') {
                        $gClientRedirect = $_ENV['G_CLIENT_REDIRECT_URL_PAYMENT'];
                    }
                }

                $paramsLib = [
                    'grant_type' => 'authorization_code',
                    'client_secret' => $_ENV['G_CLIENT_SECRET'],
                    'client_id' => $_ENV['G_CLIENT_ID'],
                    'redirect_uri' => $gClientRedirect,
                    'code' => $params['code']
                ];
                $googleLib->setFromArray($paramsLib);
                $token = $googleLib->getToken();
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Login failed, code value is required";
            }
        }

        \Helpers\LogHelper::log("sosmed-login-otp", "token = " . $token);
        if (!is_null($token)) {
            $paramsLib = [
                'token' => $token
            ];
            $googleLib->setFromArray($paramsLib);
            $userInfo = $googleLib->getUserinfo();
            \Helpers\LogHelper::log("sosmed-info", "User info: " . json_encode($userInfo), "info");
            if (!is_null($userInfo) && isset($userInfo['emails'][0]['value'])) {
                // Collect $userParams
                $userParams['email'] = $userInfo['emails'][0]['value'];
                // $userParams['password'] = $userInfo['id']; // set google id as password
                $userParams['tag'] = md5($userParams['email'].'google');
                if (isset($userInfo['name']['givenName'])) $userParams['first_name'] = preg_replace('/[^A-Za-z0-9-]/', '', $userInfo['name']['givenName']);
                if (isset($userInfo['name']['familyName'])) $userParams['last_name'] = preg_replace('/[^A-Za-z0-9-]/', '', $userInfo['name']['familyName']);
                if (isset($userInfo['gender'])) $userParams['gender'] = $userInfo['gender'];
                $userParams['company_code'] = (isset($params['company_code'])) ? $params['company_code'] : 'ODI';
                switch ($userParams['company_code']) {
                    case 'AHI':
                        $userParams['registered_by'] = 'google ace';
                        break;
                    case 'HCI':
                        $userParams['registered_by'] = 'google informa';
                        break;
                    case 'TGI':
                        $userParams['registered_by'] = 'google toys';
                        break;
                    default:
                        $userParams['registered_by'] = 'google';
                        break;
                }
                $userParams['login_by'] =  $userParams['registered_by'];
                
                // According to this ticket https://ruparupa.atlassian.net/browse/RRTICKET-12222, when we hit get user info to google, name.givenName may be empty
                // This can cause first_name to be empty and user will got error when hit register
                // So to avoid error when register social login, we'll use firstname from part of user email
                // Example: user with email "asdasd@gmail.com", the firstname would be "asdasd"
                if (empty($userParams['first_name'])) {
                    $dirtyFirstname = explode("@", $userParams['email'])[0];
                    $cleanedFirstName = preg_replace('/[^A-Za-z0-9-]/', '', $dirtyFirstname);
                    $userParams['first_name'] = $cleanedFirstName;
                    \Helpers\LogHelper::log("sosmed-empty-name", "Email: ". $userParams['email']. ", Generated firstname: ". $userParams['first_name'], "info");
                }
                $this->userParams = $userParams;
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Login failed, cannot found customer information";
            }
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Login failed, get token failed";
        }

        return array();
    }

    public function authorizeLoginFacebook($params = array())
    {
        $token = (isset($params['token'])) ? $params['token'] : NULL;

        $paramsFLib['requester'] = (isset($params['requester'])) ? $params['requester'] : 'homepage';
        $facebookLib = new \Library\Sosmed\Fb($paramsFLib);
        if (!isset($params['token'])) {
            if (isset($params['code'])) {
                $paramsLib = [
                    'code' => $params['code']
                ];
                $facebookLib->setFromArray($paramsLib);
                $responseToken = $facebookLib->getToken();
                if (!isset($responseToken['error'])) {
                    $token = $responseToken['data']['token'];
                } else {
                    $this->errorCode = "RR100";
                    $this->errorMessages = "Login failed, get token failed";
                }
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Login failed, code value is required";
            }
        }

        if (!is_null($token)) {
            $paramsLib = [
                'token' => $token
            ];
            $facebookLib->setFromArray($paramsLib);
            $responseUserinfo = $facebookLib->getUserinfo();
            if (!isset($responseUserinfo['error']) && isset($responseUserinfo['data']['email'])) {
                // Collect $userParams
                $userParams['email'] = $responseUserinfo['data']['email'];
                // $userParams['password'] = $responseUserinfo['data']['id']; // set facebook id as password
                $userParams['tag'] = md5($userParams['email'].'facebook');
                if (isset($responseUserinfo['data']['first_name'])) $userParams['first_name'] = preg_replace('/[^A-Za-z0-9-]/', '', $responseUserinfo['data']['first_name']);
                if (isset($responseUserinfo['data']['last_name'])) $userParams['last_name'] = preg_replace('/[^A-Za-z0-9-]/', '', $responseUserinfo['data']['last_name']);
                if (isset($responseUserinfo['data']['gender'])) $userParams['gender'] = $responseUserinfo['data']['gender'];
                $userParams['login_by'] = 'facebook';
                $userParams['registered_by'] = 'facebook';
                $userParams['company_code'] = (isset($params['company_code'])) ? $params['company_code'] : 'ODI';
                $this->userParams = $userParams;
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Login failed, cannot found customer information";
            }
        }

        return array();
    }

    // currently not using this anymore
    public function registerLogin($userParams)
    {
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($userParams);

        $customerResult = $this->getCustomerByEmail($userParams['email']);
        if (count($customerResult) > 0) {
            // login customer
            $this->loginResult = $customerLib->customerLogin($userParams);
        } else {
            // register customer
            $customerLib->registerCustomer(true, $userParams);
            if(!empty($customerLib->getErrorCode())) {
                $this->errorCode = "RR100";
                $this->errorMessages = "Register customer failed";
            } else {
                // Automatic login and get voucher list for this customer
                $this->loginResult = $customerLib->customerLogin($userParams);
                $this->loginResult['voucher_list'] = $customerLib->getVoucherList();
            }
        }

        return array();
    }

    public function getCustomerByEmail($email)
    {
        $customerData = array();
        $customerResult = $this->customerModel->findFirst(
            array(
                "conditions" => "email = '" . $email ."'",
                "order" => "last_login DESC"
            )
        );
        if($customerResult) {
            $customerData = $customerResult->toArray();
        }

        return $customerData;
    }

    public function getCustomerByPhone($phone)
    {
        $customerData = array();
        $customerResult = $this->customerModel->findFirst("phone = '" . $phone ."'");
        if($customerResult) {
            $customerData = $customerResult->toArray();
        }

        return $customerData;
    }
}