<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/15/2017
 * Time: 3:00 PM
 * User: roesmien_ecomm
 * Date: 5/16/2017
 * Time: 9:33 AM
 */

namespace Library;


use Library\APIWrapper;

class ProductStock
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function getListProductStock($params = array())
    {
        unset($params['master_app_id']); // Event Purpose
        unset($params['admin_user_id']); // Event Purpose

        $productStockModel = new \Models\ProductStock();

        $query = $productStockModel::query();

        foreach($params as $key => $val)
        {
            if($key == 'sku'){
                $tempSku = explode(',',$params['sku']);
                $listSku = array();
                foreach($tempSku as $rowSku){
                    $listSku[] = "'".$rowSku."'";
                }
                $listSku = implode(',',$listSku);

                $query->andWhere($key ." in (" . $listSku .") ");
            }else{
                $query->andWhere($key ." = '" . $val ."'");
            }
        }

        $query->orderBy('qty');

        $stock = $query->execute();

        $stockList = $stock->toArray();
        if(!empty($stockList)){
            return $stockList;
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "Product Stock not found";
            return;
        }
    }

    public function updateStockRevert($store_code = "", $qty = "", $sku = "", $process = "", $order_no = "", $company_code = "ODI")
    {
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));

        // Now we deduct stock for this item
        $apiWrapper->setEndPoint("legacy/stock/revert");
        $params = [
            "store_code" => $store_code,
            "qty" => (int)$qty,
            "sku" => $sku,
            "process" => $process,
            "order_no" => $order_no,
            "company_code" => strtolower($company_code)
        ];
        $apiWrapper->setParam($params);

        if($apiWrapper->send("put")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            \Helpers\LogHelper::log("stock_assignation", json_encode($apiWrapper->getError()));
            return false;
        }

        return true;
    }

    public function updateIsInStock($productId = "", $sku = "", $isInStock = 0)
    {
        $apiWrapper = new APIWrapper(getenv('API_URL_PRODUCT_GO'));

        // Now we deduct stock for this item
        $apiWrapper->setEndPoint("products/v1/stock/".$productId);
        $params = [
            "sku" => $sku,
            "is_in_stock" => (int)$isInStock
        ];
        $apiWrapper->setParam($params);

        if($apiWrapper->send("put")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            \Helpers\LogHelper::log("is_in_stock", json_encode($apiWrapper->getError()));
            return false;
        }

        return true;
    }

    public function updateStock($params = array())
    {
        $apiWrapper = new APIWrapper(getenv('STOCK_API'));

        // Now we deduct stock for this item
        $apiWrapper->setEndPoint("update_stock/");

        $apiWrapper->setParam($params);

        if($apiWrapper->send("put")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            \Helpers\LogHelper::log("stock_assignation", json_encode($apiWrapper->getError()));
            return false;
        }

        $message = $apiWrapper->getMessages();
        $affectedRows = explode('#',$message[1]);
        return (!empty($affectedRows)? $affectedRows[1]:1);
    }

    /**
     * Deduct stock for order
     * @param string $process
     * @param array $salesOrderItemIds
     * @param array $withCustomQuantity
     * format $withCustomQuantity = [
     *      [
     *          'sales_order_item_id' => 1,
     *          'quantity' => 1,
     *      ]
     * ]
     * @return bool|mixed
     */
    public function deductStockOrderV2($process = "", $salesOrderItemIds = array(), $withCustomQuantity = array())
    {
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));

        // Now we deduct stock for this item
        $apiWrapper->setEndPoint("stock/order/deduct");

        $apiWrapper->setParam(array(
            "process" => $process,
            "sales_order_item_ids" => $salesOrderItemIds,
            "with_quantity" => $withCustomQuantity
        ));

        if($apiWrapper->sendRequest("post", "60.0", true)) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = array($error['message']);
            \Helpers\LogHelper::log("deduct", json_encode($apiWrapper->getError()));
            return false;
        }

        return $apiWrapper->getRawData();
    }

    /**
    * Deduct stock for order
    * @param string $process
    * @param array $salesOrderItemIds
    * @param array $withCustomQuantity
    * format $withCustomQuantity = [
    *      [
    *          'sales_order_item_id' => 1,
    *          'quantity' => 1,
    *      ]
    *  ]
    * @return bool|mixed
    */
    public function revertStockOrderV2($process = "", $salesOrderItemIds = array(), $withCustomQuantity = array(), $channel = '', $shopID = 0)
    {
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));

        // Now we deduct stock for this item
        $apiWrapper->setEndPoint("stock/order/revert");

        $apiWrapper->setParam(array(
            "process" => $process,
            "sales_order_item_ids" => $salesOrderItemIds,
            "with_quantity" => $withCustomQuantity,
            "channel" => $channel,
            "shop_id" => $shopID
        ));

        if($apiWrapper->sendRequest("post", "60.0", true)) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = array($error['message']);
            \Helpers\LogHelper::log("deduct", json_encode($apiWrapper->getError()));
            return false;
        }

        return $apiWrapper->getRawData();
    }

    /**
     * Deduct stock for order
     * @param string $process
     * @param array $items
     * format $items = [
     *      [
     *          'sku' => 'X12345',
     *          'quantity' => 1,
     *          'store_code' => 'J366',
     *      ]
     *  ]
     * @return bool|mixed
     */
    public function deductStockV2($process = "", $order_no = "", $items = array(), $shopID = 0, $channel = '')
    {
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));

        // Now we deduct stock for this item
        $apiWrapper->setEndPoint("stock/deduct");

        $apiWrapper->setParam(array(
            "order_no" => $order_no,
            "process" => $process,
            "shop_id" => $shopID,
            "channel" => $channel,
            "items" => $items
        ));

        if($apiWrapper->sendRequest("post", "60.0", true)) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = array($error['message']);
            \Helpers\LogHelper::log("deduct", json_encode($apiWrapper->getError()));
            return false;
        }

        return $apiWrapper->getRawData();
    }

    /**
     * Revert stock for order
     * @param string $process
     * @param array $items
     * format $items = [
     *      [
     *          'sku' => 'X12345',
     *          'quantity' => 1,
     *          'store_code' => 'J366',
     *      ]
     *  ]
     * @return bool|mixed
     */
    public function revertStockV2($process = "", $order_no = "", $items = array())
    {
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));

        // Now we revert stock for this item
        $apiWrapper->setEndPoint("stock/revert");

        $apiWrapper->setParam(array(
            "order_no" => $order_no,
            "process" => $process,
            "items" => $items
        ));

        if($apiWrapper->sendRequest("post", "60.0", true)) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = array($error['message']);
            \Helpers\LogHelper::log("revert", json_encode($apiWrapper->getError()));
            return false;
        }

        return $apiWrapper->getRawData();
    }

    public function updateReservedStockShoped($params = array())
    {
        $apiWrapper = new APIWrapper(getenv('STOCK_API'));

        $apiWrapper->setEndPoint("sync/reserved");
        $apiWrapper->setParam($params);

        if($apiWrapper->send("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }

        return true;
    }
}
