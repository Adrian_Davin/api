<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Library;

use Helpers\LogHelper;

class AceWebAPI
{

  protected $errorCode;
  protected $errorMessages;

  /**
   * @return mixed
   */
  public function getErrorCode()
  {
    return $this->errorCode;
  }

  /**
   * @return mixed
   */
  public function getErrorMessages()
  {
    return $this->errorMessages;
  }

  public function __construct()
  {
    $this->customer = new \Models\Customer();
  }

  public function generateOtp($phone = "", $action = "")
  {
    // TODO:
    // Prod  : https://wslw.acehardware.co.id/api/
    // Dev   : http://acewebdevws.acehardware.co.id:44376/api/

    // Received Payload:
    // {
    //   "data": {
    //     "phone": "08111788639",
    //     "action": "register/CHANGE_HP/CHANGE_EMAIL/CHANGE_PASSKEY_WEB_ACE/VERIFY_HP"
    //   }
    // }

    if ($phone == "" || $action == "") {
      $this->errorCode = 400;
      $this->errorMessages = "Phone atau action kosong";
      return false;
    }

    // If payload is valid, generate otp using mt_rand(000000,999999) 6 digits random
    $otpNumber = mt_rand(100000, 999999);

    // Send Request Payload to POST /Add_OTP:
    // {
    //     "P_bu_code":"AHI", 
    //     "P_Cust_HP": "+628111788639", 
    //     "P_Cust_OTP": "123456", 
    //     "P_Created_on": "2020-07-13 10:54:46",
    //     "P_Created_by": "ODI", 
    //     "P_Request_Type": "register/CHANGE_HP/CHANGE_EMAIL/CHANGE_PASSKEY_WEB_ACE/VERIFY_HP" 
    // }

    $requestPayload = array(
      "P_bu_code" => "AHI",
      "P_Cust_HP" => $phone,
      "P_Cust_OTP" => strval($otpNumber),
      "P_Created_on" => date('Y-m-d H:i:s'),
      "P_Created_by" => "ODI",
      "P_Request_Type" => $action
    );

    LogHelper::log("generate_otp_payload", "MESSAGE: " . json_encode($requestPayload), "info");

    // Get Encrypted Data
    $encryptedPayload = $this->aceWebAPIEncrypt($requestPayload);
    if (!$encryptedPayload) {
      LogHelper::log("generate_otp_payload", "MESSAGE FAIL ENCRYPT: " . json_encode($requestPayload), "error");
      return false;
    }

    $responseRows = $this->aceAddOTP($encryptedPayload);
    if (!$responseRows) {
      LogHelper::log("generate_otp_payload", "MESSAGE: " . json_encode($requestPayload));
      return false;
    }

    LogHelper::log(
      "generate_otp_response",
      "Request Payload: " . json_encode($requestPayload) . ", Response Payload: " . json_encode($responseRows),
      "info"
    );

    return $otpNumber;
  }

  public function sendWhatsapp($phoneNumber = "", $template = "", $data = [])
  {
    $requestPayload = array(
      'P_BU_CODE' => 'AHI',
      'P_NO_HP' => (string) $phoneNumber,
      'P_TEMPLATE_TYPE' => '',
      'P_FROM' => '',
      'P_CARD_ID' => '',
      'P_OTP' => '',
      'P_PASSKEY' => ''
    );

    switch ($template) {
      case "register_otp":
        $requestPayload['P_TEMPLATE_TYPE'] = 'register_otp';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_REGISTER_OTP';
        $requestPayload['P_OTP'] = (string) $data['otp'];
        break;
      case "register_send_card_id":
        $requestPayload['P_TEMPLATE_TYPE'] = 'register_send_card_id';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_SEND_CARD_AFTER_REGISTER';
        $requestPayload['P_CARD_ID'] = $data['member_no'];
        $requestPayload['P_PASSKEY'] = (string) $data['passkey'];
        break;
      case "verify_hp":
        $requestPayload['P_TEMPLATE_TYPE'] = 'verify_hp';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_VERIFY_HP';
        $requestPayload['P_OTP'] = (string) $data['otp'];
        break;
      case "change_email":
        $requestPayload['P_TEMPLATE_TYPE'] = 'change_email';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_CHANGE_EMAIL';
        $requestPayload['P_OTP'] = (string) $data['otp'];
        break;
      case "forgot_passkey":
        $requestPayload['P_TEMPLATE_TYPE'] = 'forgot_passkey';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_FORGOT_PASSKEY';
        $requestPayload['P_CARD_ID'] = $data['member_no'];
        $requestPayload['P_PASSKEY'] = (string) $data['passkey'];
        break;
      case "change_passkey":
        $requestPayload['P_TEMPLATE_TYPE'] = 'change_passkey';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_CHANGE_PASSKEY';
        $requestPayload['P_OTP'] = (string) $data['otp'];
        break;
      case "redeem_otp":
        $requestPayload['P_TEMPLATE_TYPE'] = 'redeem_otp';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_REDEEM_OTP';
        $requestPayload['P_OTP'] = (string) $data['otp'];
        break;
      case "stamp_miss_ace":
        $requestPayload['P_TEMPLATE_TYPE'] = 'stamp_miss_ace';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_REDEEM_OTP';
        $requestPayload['P_OTP'] = (string) $data['otp'];
        $requestPayload['P_PASSKEY'] = '085627738211_';
        break;
      case "otp_send_voucher1":
        $requestPayload['P_TEMPLATE_TYPE'] = 'otp_send_voucher1';
        $requestPayload['P_FROM'] = 'ACE_MOBILE_OTP_SEND_VOUCHER';
        $requestPayload['P_OTP'] = (string) $data['otp'];
        break;
      default:
        $this->errorCode = 400;
        $this->errorMessages = 'Action not defined';
        return false;
    }

    LogHelper::log("send_whatsapp_payload", "REQUEST PAYLOAD: " . json_encode($requestPayload), "info");

    // Get Encrypted Data
    $encryptedPayload = $this->aceWebAPIEncrypt($requestPayload);
    if (!$encryptedPayload) {
      LogHelper::log("send_whatsapp_payload", "MESSAGE FAIL ENCRYPT: " . json_encode($requestPayload), "error");
      return false;
    }

    // if success, return success
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/send_wa");
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error occurs when trying to send whatsapp";

      return false;
    }

    if (!empty($apiWrapper->getError())) {
      LogHelper::log("send_whatsapp_payload", "MESSAGE: " . json_encode($requestPayload), "error");
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();

      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      LogHelper::log("send_whatsapp_payload", "MESSAGE: " . json_encode($requestPayload), "error");
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();

      return false;
    }

    LogHelper::log(
      "send_whatsapp_response",
      "Request Payload: " . json_encode($requestPayload) . ", Response Payload: " . json_encode($apiWrapper),
      "info"
    );

    return true;
  }

  public function validateOtp($phone = "", $otp_number = "", $action = "")
  {
    // TODO:
    // incoming payload must be: phone, otp_number and action

    // if valid, continue. Else, return error
    if (empty($phone) || empty($otp_number) || empty($action)) {
      $this->errorCode = 400;
      $this->errorMessages = "Phone, nomor OTP atau action kosong";
      return false;
    }

    // Send POST request to /Check_OTP with payload:
    // {
    //   "P_bu_code":"AHI", 
    //   "P_Cust_Hp": $phone, 
    //   "P_Cust_OTP": $otp_number, 
    //   "P_Created_on":"2020-07-13 10:55:46",
    //   "P_Request_Type":"register/CHANGE_HP/CHANGE_EMAIL/CHANGE_PASSKEY_WEB_ACE/VERIFY_HP ($action)" 
    // }
    $requestPayload = array(
      "P_bu_code" => "AHI",
      "P_Cust_Hp" => $phone,
      "P_Cust_OTP" => $otp_number,
      "P_Created_on" => date('Y-m-d H:i:s'),
      "P_Request_Type" => $action
    );

    LogHelper::log("validate_otp_payload", "MESSAGE: " . json_encode($requestPayload), "info");

    // Get Encrypted Data
    $encryptedPayload = $this->aceWebAPIEncrypt($requestPayload);
    if (!$encryptedPayload) {
      LogHelper::log("validate_otp_payload", "MESSAGE: " . json_encode($requestPayload), "error");
      return false;
    }

    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/Check_OTP");
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat validasi OTP";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }
    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();
      return false;
    }

    LogHelper::log(
      "validate_otp_response",
      "Request Payload: " . json_encode($requestPayload) . ", Response Payload: " . json_encode($apiWrapper),
      "info"
    );

    return true;
  }


  public function aceWebAPIEncrypt($payload = array())
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/test_encrypt");
    $apiWrapper->setParam($payload);

    if ($apiWrapper->sendRequest("post")) {
      $apiWrapper->formatResponse();
    } else {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat melakukan enkripsi payload";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();
      return false;
    }

    $response = $apiWrapper->getMessages();
    return $response;
  }

  public function aceRegisterCustomer($encryptedPayload = "")
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/Add_Cust_Data");
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengirim data customer";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();
      return false;
    }

    $responseRows = $apiWrapper->getRows();
    return $responseRows;
  }

  public function aceLoginCustomer($encryptedPayload = "", $loginType = "member")
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/login");
    if ($loginType == "phone") {
      $apiWrapper->setEndPoint("api/login_using_phone");
    }
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengirim data customer";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() == "gagal" ? "Request OTP telah melebihi batas" : $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Informasi login tidak valid";
      return false;
    }

    $responseRows = $apiWrapper->getRows();
    return $responseRows;
  }

  public function aceAddOTP($encryptedPayload = "")
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/Add_OTP");
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengirim data customer";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() == "gagal" ? "OTP request sudah mencapai limit" : $apiWrapper->getMessages();
      return false;
    }

    $responseRows = $apiWrapper->getRows();

    return $responseRows;
  }

  public function aceCheckCustomerVerification($encryptedPayload = "")
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/get_hp_mail_status");
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat cek verifikasi customer";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = "E-mail dan nomor handphone belum terverifikasi";
      return false;
    }

    $responseRows = $apiWrapper->getRows();
    return $responseRows;
  }

  public function aceGetCustomerData($encryptedPayload = "", $token = "")
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/Get_Cust_Data");
    $apiWrapper->setHeaders(array("Authorization" => $token));
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mendapatkan data customer";

      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();

      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ?
        $apiWrapper->getMessages() :
        "Error saat menampilkan data customer";

      return false;
    }

    $responseRows = $apiWrapper->getRows();

    return $responseRows;
  }

  public function aceCheckRegisteredData($encryptedPayload = "", $type = "")
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/Check_Existing_Email");
    if ($type == "phone") {
      $apiWrapper->setEndPoint("api/Check_Existing_Phone_Number");
    }

    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat cek verifikasi customer";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() == "true") {
      $this->errorCode = 400;
      if ($type == "phone") $type = "Nomor HP";
      $this->errorMessages = ucfirst($type) . " sudah terdaftar. Silakan gunakan yang lain.";
      return false;
    }

    return true;
  }

  public function getAceAction($action = "")
  {
    $actionMapping = array(
      "register" => "register",
      "change_phone" => "CHANGE_HP",
      "change_email" => "CHANGE_EMAIL",
      "change_passkey" => "CHANGE_PASSKEY_WEB_ACE",
      "verify_phone" => "VERIFY_HP"
    );

    if ($action != "" && array_key_exists($action, $actionMapping)) return $actionMapping[$action];

    return "";
  }

  public function changePasskey($encryptedPayload, $token)
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/Update_Passkey");
    $apiWrapper->setHeaders(
      [
        "Authorization" => $token
      ]
    );
    $apiWrapper->setParam(
      [
        "p_encrypt" => $encryptedPayload
      ]
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengganti pass key";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Error saat mengganti pass key";
      return false;
    }

    return true;
  }

  public function getPointHistoryAce($encryptedPayload, $token)
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/check_point_history");
    $apiWrapper->setHeaders(
      [
        "Authorization" => $token
      ]
    );
    $apiWrapper->setParam(
      [
        "p_encrypt" => $encryptedPayload
      ]
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengambil point history ACE";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Error saat mengambil point history ACE";
      return false;
    }

    return count($apiWrapper->getRows()) > 0 ?
      [
        "rows"          => $apiWrapper->getRows(),
        "count"         => $apiWrapper->getCount(),
        "point_expired" => $apiWrapper->getResponseArray()['point_exp']
      ] :
      true;
  }

  public function getTransactionHistoryAce($encryptedPayload, $token)
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/check_transaction_history");
    $apiWrapper->setHeaders(
      [
        "Authorization" => $token
      ]
    );
    $apiWrapper->setParam(
      [
        "p_encrypt" => $encryptedPayload
      ]
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengambil riwayat transaksi ACE";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Error saat mengambil riwayat transaksi ACE";
      return false;
    }

    return count($apiWrapper->getRows()) > 0 ?
      [
        "rows" => $apiWrapper->getRows(),
        "count" => $apiWrapper->getCount()
      ] :
      true;
  }

  public function aceVerifyEmail($encryptedPayload = "")
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/verify_email");
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengirim data customer";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();
      return false;
    }

    $responseRows = true;
    return $responseRows;
  }

  public function aceVerifyPhone($encryptedPayload = "")
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/verify_HP");
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengirim data customer";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();
      return false;
    }

    $responseRows = true;
    return $responseRows;
  }

  public function changeEmailAce($encryptedPayload)
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/change_email");
    $apiWrapper->setParam(
      [
        "p_encrypt" => $encryptedPayload
      ]
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengganti e-mail ACE";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Error saat mengganti e-mail ACE";
      return false;
    }

    return true;
  }

  public function changePhoneAce($encryptedPayload)
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/change_HP");
    $apiWrapper->setParam(
      [
        "p_encrypt" => $encryptedPayload
      ]
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengganti nomor handphone ACE";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Error saat mengganti e-mail ACE";
      return false;
    }

    return true;
  }

  public function forgetPasskeyAce($encryptedPayload)
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/Forgot_Passkey");
    $apiWrapper->setParam(
      [
        "p_encrypt" => $encryptedPayload
      ]
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengambil pass key ACE";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Error saat mengambil pass key ACE";
      return false;
    }

    return $apiWrapper->getRows();
  }

  public function changeMemberDataAce($encryptedPayload, $token)
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/Update_Cust_Data");
    $apiWrapper->setHeaders(
      [
        "Authorization" => $token
      ]
    );
    $apiWrapper->setParam(
      [
        "p_encrypt" => $encryptedPayload
      ]
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat mengganti data member ACE";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Error saat mengganti data member ACE";
      return false;
    }

    return count($apiWrapper->getRows()) > 0 ? $apiWrapper->getRows() : true;
  }

  public function loginLogoutFlagAce($encryptedPayload)
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/update_login_flag");
    $apiWrapper->setParam(
      [
        "p_encrypt" => $encryptedPayload
      ]
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat hit endpoint login logout flag ACE";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Error saat mengganti data member ACE";
      return false;
    }

    return true;
  }

  public function insertPointAce($payload = array())
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("api/insert_point");
    $apiWrapper->setParam($payload);

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Terjadi kesalahan saat menambahkan/memotong poin";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Terjadi kesalahan saat menambahkan/memotong poin";
      return false;
    }

    LogHelper::log(
      "redeem_point_ace_response",
      "Request Payload: " . json_encode($payload) . ", Successfully Redeemed"
    );

    return true;
  }

  #region Service Center

  public function checkSKUWarrantyAvailability($arrSKU)
  {
    $apiWrapper = new APIWrapper(getenv('SISCA_API_KL'));
    $apiWrapper->setEndPoint('warranty/check');
    $apiWrapper->setParam($arrSKU);
    if (!$apiWrapper->sendRequestStripes("get")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Terjadi kesalahan saat mendapatkan ketersediaan garansi barang";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $rawData = $apiWrapper->getRawData();

    return $rawData['data'];
  }

  public function checkWarrantyDetailsPerSKU($ace_warranty_id)
  {
    $apiWrapper = new APIWrapper(getenv('SISCA_API_KL'));
    $apiWrapper->setEndPoint('warranty/detail');
    $apiWrapper->setParam(
      array('WarrantyIdAce' => $ace_warranty_id)
    );
    if (!$apiWrapper->sendRequestStripes("get")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Terjadi kesalahan saat mendapatkan detail garansi";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    return $apiWrapper->getRawData();
  }

  public function registerWarranty($payload)
  {
    $apiWrapper = new APIWrapper(getenv('SISCA_API_KL'));
    $apiWrapper->setEndPoint('warranty/activation');
    $apiWrapper->setParam($payload);

    if (!$apiWrapper->sendRequestStripes("get")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Terjadi kesalahan saat mengeksekusi API pendaftaran garansi";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    return $apiWrapper->getRawData();
  }

  public function serviceWarrantyFeeConfirmation($payload)
  {
    $apiWrapper = new APIWrapper(getenv('SISCA_API_KL'));
    $apiWrapper->setEndPoint('warranty/sparepart-confirmation-response');
    $apiWrapper->setParam($payload);

    if (!$apiWrapper->sendRequestStripes("get")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Terjadi kesalahan saat mendapatkan ketersediaan garansi barang";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    return $apiWrapper->getRawData();
  }

  #endregion Service Center

  #region Tracking Installation

  public function trackingInstallationPerReceiptNumber($receiptNo, $sessionHeader)
  {
    $apiWrapper = new APIWrapper(getenv('CUSTOMER_API_KL'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/listtrx_install/$receiptNo");
    $apiWrapper->setHeaders(array("MFSESSID" => $sessionHeader));
    if (!$apiWrapper->sendRequestStripes("get")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages()
        ? $apiWrapper->getMessages()
        : "Terjadi kesalahan saat mendapatkan daftar instalasi";

      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    return $apiWrapper->getRawData();
  }

  public function trackingInstallationDetailPerTransactionNumberAndJob($transactionNo, $jobId, $sessionHeader)
  {
    $apiWrapper = new APIWrapper(getenv('CUSTOMER_API_KL'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/detailinstallation/$transactionNo/$jobId");
    $apiWrapper->setHeaders(array("MFSESSID" => $sessionHeader));
    if (!$apiWrapper->sendRequestStripes("get")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages()
        ? $apiWrapper->getMessages() : "Terjadi kesalahan saat mendapatkan detail instalasi";

      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();

      return false;
    }

    return $apiWrapper->getRawData();
  }

  public function trackingInstallationInstallerRoutes($transactionNo, $jobId, $sessionHeader)
  {
    $apiWrapper = new APIWrapper(getenv('CUSTOMER_API_KL'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/installroute/$transactionNo/$jobId");
    $apiWrapper->setHeaders(array("MFSESSID" => $sessionHeader));
    if (!$apiWrapper->sendRequestStripes("get")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages()
        ? $apiWrapper->getMessages() : "Terjadi kesalahan saat mendapatkan rute instalasi";

      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();

      return false;
    }

    return $apiWrapper->getRawData();
  }

  #endregion

  #region Diginex

  public function createTicketForRequestingSalesAssistant($payload = array())
  {
    $apiWrapper = new APIWrapper(getenv('DIGINEX_ENDPOINT_KL'), true);
    $apiWrapper->setEndPoint("createticket");
    $apiWrapper->setParam($payload);

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages()
        ? $apiWrapper->getMessages()
        : "Terjadi kesalahan saat melakukan request tiket sales assistant";

      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();
      return false;
    }

    LogHelper::log(
      "diginex_request_sales_assistant_response",
      "Request Payload: " . json_encode($payload) . "\n" .
        "Response Payload: " . json_encode($apiWrapper->getRawData()),
      "info"
    );

    return $apiWrapper->getRawData();
  }

  public function closeTicketSalesAssistantRequest($ticket_no)
  {
    $apiWrapper = new APIWrapper(getenv('DIGINEX_ENDPOINT_KL'), true);
    $apiWrapper->setEndPoint("closeTicket");
    $apiWrapper->setParam(
      array(
        'ticketNo' => $ticket_no
      )
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages()
        ? $apiWrapper->getMessages()
        : "Terjadi kesalahan saat melakukan close tiket sales assistant";

      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();
      return false;
    }

    LogHelper::log(
      "diginex_request_sales_assistant_close_response",
      "Ticket Number: " . $ticket_no . "\n" .
        "Response Payload: " . json_encode($apiWrapper->getRawData()),
      "info"
    );

    return true;
  }

  public function dispatchTicketSalesAssistantRequest($ticket_no, $request_assistance_id, $store_code, $store_aisle_name)
  {
    $apiWrapper = new APIWrapper(getenv('DIGINEX_ENDPOINT_DISPATCHER_KL'), true);
    $apiWrapper->setEndPoint("pushNotification");
    $apiWrapper->setParam(
      array(
        'data' => array(
          'channel'               => 'dispatcher',
          'message'               => "Customer menunggu di $store_aisle_name",
          'title'                 => 'TIKET BARU',
          'ticketNo'              => $ticket_no,
          'requestAssistanceId'   => (string)$request_assistance_id,
          'siteCode'              => $store_code
        ),
        'to'    => '/topics/all'
      )
    );

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages()
        ? $apiWrapper->getMessages()
        : "Terjadi kesalahan saat melakukan dispatch tiket sales assistant";

      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();
      return false;
    }

    LogHelper::log(
      "diginex_request_sales_assistant_dispatch_response",
      "Ticket Number: " . $ticket_no . "\n" .
        "Response Payload: " . json_encode($apiWrapper->getRawData()),
      "info"
    );

    return true;
  }

  #endregion
}
