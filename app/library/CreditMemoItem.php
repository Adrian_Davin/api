<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/24/2017
 * Time: 2:06 PM
 */

namespace Library;


class CreditMemoItem
{
    protected $sales_order_item_id;
    protected $sku;
    protected $store_code;
    protected $name;
    protected $price;
    protected $weight = 0;
    protected $shipping_amount = 0;
    protected $discount_amount = 0;
    protected $gift_cards_amount = 0;
    protected $qty_invoiced = 0;
    protected $qty_refunded = 0;

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

    /**
     * @param int $shipping_amount
     */
    public function setShippingAmount($shipping_amount)
    {
        $this->shipping_amount = $shipping_amount;
    }

    /**
     * @return int
     */
    public function getGiftCardsAmount()
    {
        return $this->gift_cards_amount;
    }

    /**
     * @param int $gift_cards_amount
     */
    public function setGiftCardsAmount($gift_cards_amount)
    {
        $this->gift_cards_amount = $gift_cards_amount;
    }

    /**
     * @return int
     */
    public function getQtyInvoiced()
    {
        return $this->qty_invoiced;
    }

    /**
     * @param int $qty_invoiced
     */
    public function setQtyInvoiced($qty_invoiced)
    {
        $this->qty_invoiced = $qty_invoiced;
    }

    /**
     * @return int
     */
    public function getQtyRefunded()
    {
        return $this->qty_refunded;
    }

    /**
     * @param int $qty_refunded
     */
    public function setQtyRefunded($qty_refunded)
    {
        $this->qty_refunded = $qty_refunded;
    }

    public function setFromArray($data_array = array())
    {
        foreach($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);

        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) == 'object') {
                if($key == 'collection') continue;
                $view[$key] = $thisArray[$key]->getDataArray(); // get everything
            } else {
                $view[$key] = $val;
            }
        }

        unset($view['errorCode']);
        unset($view['errorMessages']);

        return $view;
    }
}