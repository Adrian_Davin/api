<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 05/09/2017
 * Time: 2:47 PM
 */

namespace Library\Sosmed;

class Google
{
    protected $grant_type;
    protected $client_id;
    protected $client_secret;
    protected $code;
    protected $redirect_uri;
    protected $token;

    public function __construct()
    {

    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);

        return $thisArray;
    }

    public function getToken()
    {
        $client = new \GuzzleHttp\Client();
        $curlParam = [
            'form_params' => $this->getDataArray()
        ];

        if(!empty($_ENV['HTTP_PROXY'])) {
            $curlParam['proxy'] = ["http" => $_ENV['HTTP_PROXY']];
        }

        try {
            $response = $client->request('POST',$_ENV['G_GET_TOKEN_URL'],$curlParam );
        } catch (\Exception $e) {
            return null;
        }

        $responseArray = json_decode($response->getBody()->getContents(), true);
        if(!empty($responseArray['access_token'])) {
            return $responseArray['access_token'];
        } else {
            return null;
        }
    }

    public function getUserinfo()
    {
        $client = new \GuzzleHttp\Client();
        $curlParam = [
            'headers' => ['Authorization' => 'Bearer ' . $this->token]
        ];

        if(!empty($_ENV['HTTP_PROXY'])) {
            $curlParam['proxy'] = ["http" => $_ENV['HTTP_PROXY']];
        }

        try {
            $response = $client->request('GET',$_ENV['G_GET_USERINFO_URL'], $curlParam );
        } catch (\Exception $e) {
            return null;
        }

        $responseArray = json_decode($response->getBody()->getContents(), true);
        if(isset($responseArray['emails'])) {
            return $responseArray;
        } else {
            return null;
        }
    }
}