<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 05/09/2017
 * Time: 2:47 PM
 */

namespace Library\Sosmed;

require_once 'Facebook/autoload.php';

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class Fb
{
    protected $app_id;
    protected $app_secret;
    protected $redirect_url;
    protected $fb_permission;
    protected $fb;
    protected $helper;
    protected $code;
    protected $token;
    protected $requester;

    public function __construct($params = array())
    {
        if(!session_id()){
            session_start();
        }

        $this->app_id = $_ENV['F_APP_ID'];
        $this->app_secret = $_ENV['F_APP_SECRET'];

        if (isset($params['requester'])) {
            if ($params['requester'] == 'homepage') {
                $this->redirect_url = $_ENV['F_REDIRECT_URL_HOMEPAGE'];
            } elseif($params['requester'] == 'payment') {
                $this->redirect_url = $_ENV['F_REDIRECT_URL_PAYMENT'];
            }
        } else {
            $this->redirect_url = $_ENV['F_REDIRECT_URL_HOMEPAGE'];
        }

        $this->fb_permission = array('email');

        $this->fb = new Facebook(array(
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => 'v2.2',
        ));

        $this->helper = $this->fb->getRedirectLoginHelper();
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);

        return $thisArray;
    }

    public function getLoginUrl()
    {
        try {
            $loginURL = $this->helper->getLoginUrl($this->redirect_url, $this->fb_permission);
        } catch (\Exception $e) {
            $loginURL = $e->getMessage();
        }

        return $loginURL;
    }

    public function getToken()
    {
        $response = array();
        try {
            $_GET['code'] = $this->code;
            $response['data']['token'] = $this->helper->getAccessToken($this->redirect_url);
        } catch(FacebookResponseException $e) {
            $response['error'] = $e->getMessage();
        } catch(FacebookSDKException $e) {
            $response['error'] = $e->getMessage();
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return $response;
    }

    public function getUserinfo()
    {
        $response = array();
        try {
            // Put short-lived access token in session
            $_SESSION['facebook_access_token'] = (string) $this->token;

            // OAuth 2.0 client handler helps to manage access tokens
            $oAuth2Client = $this->fb->getOAuth2Client();

            // Exchanges a short-lived access token for a long-lived one
            $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
            $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

            // Set default access token to be used in script
            $this->fb->setDefaultAccessToken($_SESSION['facebook_access_token']);

            $profileRequest = $this->fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,picture');
            $response['data'] = $profileRequest->getGraphNode()->asArray();
        } catch(FacebookResponseException $e) {
            $response['error'] = $e->getMessage();
        } catch(FacebookSDKException $e) {
            $response['error'] = $e->getMessage();
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return $response;
    }
}