<?php
/**
 * Created by PhpStorm.
 * User: Aaron Putra
 * Date: 04/05/2017
 * Time: 15.53
 */

namespace Library;


class AdminUserLog
{
    protected $errorCode;
    protected $errorMessages;


    public function __construct()
    {

    }
    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function getAllUserLog(){
        $userLogModel = new \Models\AdminUserLog();
        $userLogResult = $userLogModel->find(
            [
                "columns" => "id_user_log, master_app_id, admin_user_id, action_group, action, ip_address, result, 
                full_action_name, short_details"
            ]
        );

        $logLists = array();

        if(!empty($userLogResult)){
            foreach ($userLogResult as $userLog => $val){
                $logLists[$userLog] = $val->toArray();
            }
            return $logLists;
        }
        else{
            $this->errorCode="RR302";
            $this->errorMessages="No user log has been found";
            return;
        }
    }

    public function getLogDetails($params = array())
    {
        $id_user_log = $params['id_user_log'];

        $userLogModel = new \Models\AdminUserLog();
        $userLogResult = $userLogModel->findFirst(
            [
                "columns" => "id_user_log, master_app_id, admin_user_id, action_group, action, ip_address, result, full_action_name,
                short_details, full_details, created_at",
                "conditions" => "id_user_log='".$id_user_log."'"
            ]
        );

        $logDetails = array();

        if(!empty($userLogResult)){
            $logDetails = $userLogResult->toArray();
            return $logDetails;
        }
        else {
            $this->errorCode = "RR302";
            $this->errorMessages = "id_user_log not found";
            return;
        }
    }
}