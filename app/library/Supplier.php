<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Library;

/*include __DIR__ . '/../../vendor/autoload.php';

$dotenv = new \Dotenv\Dotenv(__DIR__ . '/../../');
$dotenv->load();*/

class Supplier
{

    /**
     * @var $customer \Models\SupplierUser
     */
    protected $supplierUser;
    protected $supplierOrder;
    protected $supplier;

    protected $errorCode;
    protected $errorMessages;

    public function __construct()
    {
        $this->supplierUser = new \Models\SupplierUser();
        $this->supplier = new \Models\Supplier();
        //$this->supplierOrder = new \Models\SupplierOrder();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function setSupplierFromArray($data_array = array())
    {
        $this->supplierUser->setFromArray($data_array);
    }

    public function setOrderFromArray($data_array){
        $this->supplierOrder->setFromArray($data_array);
    }

    public function supplierLogin($paramsPayload = array())
    {
        if(empty($this->supplierUser->getEmail())) {
            $this->errorMessages[] = 'Email is required';
        }

        if(empty($this->supplierUser->getPassword())) {
            $this->errorMessages[] = 'Password is required';
        }else {
            $this->supplierUser->setPassword(md5($this->supplierUser->getPassword()));
        }

        if(!empty($this->errorMessages)) {
            $this->errorCode = "RR100";
            return array();
        }

        $param = array(
            "conditions" => "email = :email: AND password = :password: AND status >= 10",
            "bind" => array(
                "email" => $this->supplierUser->getEmail(),
                "password" => $this->supplierUser->getPassword()
            )
        );
        $supplierUserResult = $this->supplierUser->findFirst($param);

        if($supplierUserResult) {
            $supplierUserData = $supplierUserResult->toArray();
            
            $supplierData = $supplierUserResult->Supplier->toArray();

            $this->supplierUser->setSupplierUserId($supplierUserData['supplier_user_id']);
            $this->supplierUser->setFromArray($supplierUserData);
            $this->supplierUser->setLastLogin(date("Y-m-d H:i:s"));

            if(isset($paramsPayload['ip_address'])) {
                $this->supplierUser->setIpAddresss($paramsPayload['ip_address']);
            }

            $this->supplierUser->setSkipAttributeOnUpdate(['email','password','role_id','supplier_id','first_name','last_name','phone','status','role_id','updated_at','created_at']);
            //$this->supplierUser->unSetSkipAttributeOnUpdate(['last_login']);
            $this->supplierUser->saveUser();

            return $supplierUserData+$supplierData;
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages[] = "Email or Password not found";
        }

        return array();
    }

    public function getSupplierRoleList(){
        $supplierRoleModel = new \Models\SupplierRole();
        $data = $supplierRoleModel->find();
        $dataArr = $data->toArray();
        $response['roles'] = $dataArr;
        return $response;
    }

    public function getSupplierRole($params = array()){
        if ((int)$params["role_id"] <= 0) {
            $this->errorCode = 400;
            $this->errorMessages[] = "Role id must be specified";
            return;
        }
        $supplierRoleModel = new \Models\SupplierRole();
        $data = $supplierRoleModel->findFirst('role_id = '.$params["role_id"]);
        return $data;
    }

    public function saveSupplierRole($params = array()) {
        // Validate payload
        $validateErr = array();
        if (empty($params["role_name"])) {
            $validateErr[] = "Role name must be filled";
        }

        if (empty($params["rules"])) {
            $validateErr[] = "Rules must be filled";
        }

        if ((int) $params["status"] != 10 && (int) $params["status"] != 0) {
            $validateErr[] = "Status must be either 10 or 0";
        }

        if (count($validateErr) > 0) {
            $this->errorCode = 400;
            $this->errorMessages[] = $validateErr[0];
            return;
        }

        $supplierRoleModel = new \Models\SupplierRole();
        $supplierRoleModel->setFromArray($params);
        $response = array();

        if ($supplierRoleModel->saveData("supplier_role")) {
            $response = array(
                "status" => "recorded",
                "role_id" => $supplierRoleModel->getRoleId()
            );
        } else {
            $this->errorCode = 500;
            $this->errorMessages[] = "Failed insert supplier role";
            return;
        }
        
        return $response;
    }
}