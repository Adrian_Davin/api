<?php

namespace Library;

class SqlServer {
    protected $dbHost;
    protected $dbPort;
    protected $dbUser;
    protected $dbPass;
    protected $dbName;
    protected $connection;
    protected $queryStatement;

    public function __construct() {
        $this->dbHost = getenv("DB_HOST_HC");
        $this->dbPort = getenv("DB_PORT_HC");
        $this->dbUser = getenv("DB_USER_HC");
        $this->dbPass = getenv("DB_PASS_HC");
        $this->dbName = getenv("DB_NAME_HC");
        $this->connect();
    }

    public function connect() {
        try {
            $serverName = $this->dbHost.", ".$this->dbPort;
            $connectionInfo = array(
                "Database" => $this->dbName,
                "UID" => $this->dbUser,
                "PWD" => $this->dbPass
            );
            $conn = sqlsrv_connect( $serverName, $connectionInfo);
            if (!$conn) {
                $connError = sqlsrv_errors();
                $errMessage = !empty($connError[0]["message"]) ? $connError[0]["message"] : "Error when connect to database";
                throw new \Exception($errMessage, 500);
            } else {
                $this->connection = $conn;
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }
    }

    public function query($query = "", $params = array()) {
        try {
            if (empty($query)) {
                return false;
            }
            
            $stmt = false;
            if (!empty($params)) {
                $stmt = sqlsrv_query($this->connection, $query, $params);
            } else {
                $stmt = sqlsrv_query($this->connection, $query);
            }
    
            if ($stmt) {
                $result = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
                sqlsrv_free_stmt($stmt);
                return $result;
            } else {
                $sqlError = sqlsrv_errors();
                $errMessage = !empty($sqlError[0]["message"]) ? $sqlError[0]["message"] : "Error when connect to database";
                throw new \Exception($errMessage, 500);
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }
    }

    public function close() {
        sqlsrv_close($this->connection);
    }
    
}
