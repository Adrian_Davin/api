<?php

namespace Library;

class ProductSellerBatch
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getBatchList()
    {
        $sellerBatchModel = new \Models\ProductSellerBatch();
        $result = $sellerBatchModel->find();
        return $result->toArray();
    }

    public function saveBatch($params){
        $sellerBatchModel = new \Models\ProductSellerBatch();
        $sellerBatchModel->setFromArray($params);

        // Validate param
        $errors = array();
        if (empty($sellerBatchModel->getCsvFile())) {
            $errors[] = "Csv file must be filled";
        }

        if (empty($sellerBatchModel->getCreatedBy())) {
            $errors[] = "Created by file must be filled";
        }

        if (empty($sellerBatchModel->getUpdatedFields())) {
            $errors[] = "Updated fields file must be filled";
        }

        if (count($errors) > 0) {
            $this->errorCode = 400;
            $this->errorMessages = $errors[0];
            return;
        }

        // Insert to db
        $result = $sellerBatchModel->saveData('product_seller_batch');
        if (!$result) {
            $this->errorCode = 500;
            $this->errorMessages = "Failed insert product seller batch data";
            return;
        }

        $response = [
            "product_seller_batch_id" => (int)$sellerBatchModel->getProductSellerBatchId(),
            "status_inserted" => $result
        ];
        return $response;
    }
}