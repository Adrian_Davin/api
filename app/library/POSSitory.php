<?php

namespace Library;

use Helpers\ProductHelper;

class POSSitory
{   
    protected $COMPANY;
    protected $NOMOR_ORDER;
    protected $DOC_DATE;
    protected $DUEDATE;
    protected $SHIPTO;
    protected $SHIPTONAME;
    protected $SHIPFROM;
    protected $SHIPFROMNAME;
    protected $CUSTOMER;
    protected $ALAMAT1;
    protected $ALAMAT2;
    protected $CITY;
    protected $KECAMATAN;
    protected $KELURAHAN;
    protected $KODEPOS;
    protected $HP;
    protected $PHONE;
    protected $CARD_ID;
    protected $ARTICLE_LIST;
    protected $REMARK;
    protected $DELIVERY_METHOD;
    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    public function __construct($invoice = null)
    {
        if (!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function prepareParams()
    {
        $invoiceData = $this->invoice->getDataArray([], true);
        $salesCustomerModel = new \Models\SalesCustomer;
        $queryCondition = array(
            "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "'");
        $salesCustomerResult = $salesCustomerModel->findFirst($queryCondition)->toArray();
        $customerModel = new \Models\Customer;
        $customerQueryCond = array(
            "conditions" => "customer_id = '" . $salesCustomerResult['customer_id'] . "'");
        $customerResult = $customerModel->findFirst($customerQueryCond);
        $customerData = $customerResult->getDataArray([], true);
       
        $salesOrderAddressModel = new \Models\SalesOrderAddress;
        $salesOrderAddressQuery = array(
            "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "'");

        $this->COMPANY = "R100";
        $this->NOMOR_ORDER = $invoiceData["invoice_no"];    
        $this->DOC_DATE = $invoiceData["created_at"];
        $this->DUEDATE = date('Y-m-d H:i:s',strtotime('+3 hour',strtotime($invoiceData["created_at"])));
        $this->SHIPTO = !empty($customerData["groups"]["AHI"]) ? $customerData["groups"]["AHI"] : "";// card member
        $this->SHIPTONAME = $salesCustomerResult["customer_firstname"];
        $this->SHIPFROM = $invoiceData["store"]["store_code"];
        if($invoiceData["delivery_method"] === "ownfleet" || $invoiceData["delivery_method"] === "store_fulfillment")
        {
        $salesOrderAddressResult = $salesOrderAddressModel->find($salesOrderAddressQuery)->toArray();
        $salesOrderAddressData = [];
        foreach ($salesOrderAddressResult as $salesOrderData)
        {
            if($salesOrderData["address_type"] === "shipping")
            {
                $salesOrderAddressData = $salesOrderData; 
              //  break;
            }
            
        }
        $this->ALAMAT1 =$salesOrderAddressData["full_address"];
        $this->CITY = $invoiceData['shipment']['shipping_address']['city']['city_name'];
        $this->KECAMATAN = $invoiceData['shipment']['shipping_address']['kecamatan']['kecamatan_name'];
        $this->KODEPOS = $salesOrderAddressData["post_code"];
        $this->HP = $salesOrderAddressData["phone"];
        }
        else if( $invoiceData["delivery_method"] === "pickup")
        {
        // $this->SHIPFROMNAME = $invoiceData["pickup_code"]["pickup_code"];
        $salesOrderAddressResult = $salesOrderAddressModel->findFirst($salesOrderAddressQuery)->toArray();
        $this->ALAMAT1 ="kembangan";
        $this->CITY = "";
        $this->KECAMATAN = "";
        $this->KODEPOS = "";
        $this->HP = $salesOrderAddressResult["phone"];
        }
        $this->PHONE = "";
        $this->SHIPFROMNAME = $invoiceData["store"]["name"];
        $this->CUSTOMER = !empty($customerData["groups"]["AHI"]) ? $customerData["groups"]["AHI"] : "";
        $this->ALAMAT2 = "";
        $this->KELURAHAN = "";
        $this->CARD_ID = "";
        $this->DELIVERY_METHOD = $invoiceData["delivery_method"];   
        $this->INVOICE_ID = $invoiceData["invoice_id"]; 
        $this->REMARK = "";
    
     
        
        $this->ARTICLE_LIST = array();
        if (!empty($invoiceData) && count($invoiceData["items"]) > 0) {
            $items = $invoiceData["items"];
            foreach ($items as $item) {
                $dataDepartment = ProductHelper::getProductDepartment($item["sku"]);
                $data = array();
                $data["ARTICLE"] = $item["sku"];
                $data["ARTICLE_DESC"] = $item["name"];
                $data["QTY"] = $item["qty_ordered"];
                $data["UOM"] = ProductHelper::getSalesUom($item["sku"]);
                $data["PLAN_DATE"] = $this->DUEDATE;
                $data["DEPT"]= $dataDepartment["dept"];
                $data["DEPTNAME"]= $dataDepartment["dept_name"];
                $data["INVOICE_ITEM_ID"]= $item["invoice_item_id"];
                array_push($this->ARTICLE_LIST, $data);
            }
        }
 
    }

    public function getParams()
    {
        $thisArray = get_object_vars($this);
        $parameter = array();
        foreach ($thisArray as $key => $val) {
            if ($key !== "invoice") {
                $parameter[$key] = $val;
            }
        }

        return $parameter;
    }

    public function createPOSSitory()
    {
        $parameter = $this->getParams();
        $nsq = new \Library\Nsq();
        $payload = [
            "HEADER" => [$parameter]
        ];
        $message = [
            "data" => json_encode($payload),
            "message" => "postToSitory",
            "invoice_no" => $this->invoice->getInvoiceNo()
        ];
        $nsq->publishCluster('transactionSap', $message);
        return true;
    }
}
