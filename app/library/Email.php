<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/18/2017
 * Time: 11:11 AM
 */

namespace Library;

use \Library\Nsq as nsq;
use Phalcon\Db as Database;

class Email
{
    /**
     * @var \Models\SalesOrder
     */
    protected $orderObj;

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoiceObj;

    /**
     * @var \Models\SalesShipment
     */
    protected $shipmentObj;

    protected $subject;
    protected $body_message;
    protected $products;
    protected $shippingAddress;

    // Required field for sending email
    protected $templateId;
    protected $emailSender;
    protected $emailReceiver;
    protected $templateModel;

    // Optional field for sending email
    protected $emailCc;
    protected $emailTag;
    protected $attachments;

    // Optional field needed by some of pages
    protected $customerFirstName;
    protected $voucherCode;
    protected $voucherList;
    protected $voucherValue;
    protected $voucherExpired;
    protected $customerLastName;
    protected $storeName;
    protected $giftCard;
    protected $vaNumber;
    protected $pickupPoint;
    protected $pickedupDate;
    protected $dateLastPickup;
    protected $orderNo;
    protected $invoiceNo;
    protected $trackingNo;
    protected $courierName;
    protected $reorderUrl;
    protected $urlForgotPassword;
    protected $linkDownload;
    protected $pendingOrderList;
    protected $minimumTransaction;
    protected $invoiceList;
    protected $paymentMethod;
    protected $qrcode_url;
    protected $eta; 
    protected $parentOrderEta;

    // Notification
    protected $invCleanCareNotif;
    protected $invMpNotif;
    protected $invDesignerNotif;
    protected $invStoreReminder;

    // Credit memo
    protected $refund_as_cash;
    protected $refund_as_gift_card;

    // Shipment
    protected $orderDate;
    protected $shipmentDate;

    // Customer Review
    protected $linkReview;

    protected $companyCode;
    protected $supplierName;

    protected $errorCode;
    protected $errorMessages;

    // Miss Ace - Service Center
    protected $serviceAceServiceNumber;
    protected $serviceWarrantyCharge;
    protected $serviceWarrantyChargeLinkConfirmation;

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @param mixed $body_message
     */
    public function setBodyMessage($body_message)
    {
        $this->body_message = $body_message;
    }

    /**
     * @param array $orderObj
     */
    public function setOrderObj($orderObj)
    {
        $this->orderObj = $orderObj;
    }

    /**
     * @param array $invoiceObj
     */
    public function setInvoiceObj($invoiceObj)
    {
        $this->invoiceObj = $invoiceObj;

        if (empty($this->orderObj)) {
            $this->orderObj = $invoiceObj->SalesOrder;
        }

        if (empty($this->shipmentObj)) {
            $this->shipmentObj = $invoiceObj->SalesShipment;
        }
    }

    /**
     * Get the value of minimumTransaction
     */
    public function getMinimumTransaction()
    {
        return $this->minimumTransaction;
    }

    /**
     * Set the value of minimumTransaction
     *
     * @return  self
     */
    public function setMinimumTransaction($minimumTransaction)
    {
        $this->minimumTransaction = $minimumTransaction;

        return $this;
    }

    /**
     * @param array $shipmentObj
     */
    public function setShipmentObj($shipmentObj)
    {
        $this->shipmentObj = $shipmentObj;
    }

    /**
     * @param boolean $useInvoiceItem
     */
    public function setUseInvoiceItem($useInvoiceItem)
    {
        $this->useInvoiceItem = $useInvoiceItem;
    }

    public function setProductList($productList)
    {
        $this->products = $productList;
    }


    /**
     * @param mixed $templateId
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;
    }

    /**
     * @param mixed $dateLastPickup
     */
    public function setDateLastPickup($dateLastPickup)
    {
        $this->dateLastPickup = $dateLastPickup;
    }

    /**
     * @param mixed $pickupPoint
     */
    public function setPickupPoint($pickupPoint)
    {
        $this->pickupPoint = $pickupPoint;
    }

    /**
     * @param mixed $pickedupDate
     */
    public function setPickedupDate($pickedupDate)
    {
        $this->pickedupDate = $pickedupDate;
    }

    /**
     * @param mixed $orderNo
     */
    public function setOrderNo($orderNo)
    {
        $this->orderNo = $orderNo;
    }

    /**
     * @param mixed $reorderUrl
     */
    public function setReorderUrl($reorderUrl)
    {
        $this->reorderUrl = $reorderUrl;
    }

    /**
     * @param mixed $invoiceNo
     */
    public function setInvoiceNo($invoiceNo = "")
    {
        if (empty($invoiceNo) && !empty($this->shipmentObj)) {
            $this->invoiceNo = $this->shipmentObj->getIncrementId();
        } else {
            $this->invoiceNo = $invoiceNo;
        }
    }

    /**
     * @param mixed $courierName
     */
    public function setCourierName($courierName)
    {
        if (empty($invoiceNo) && !empty($this->shipmentObj)) {
            $this->courierName = $this->shipmentObj->getAllTracks()[0]->getTitle();
        } else {
            $this->courierName = $courierName;
        }
    }

    /**
     * @param mixed $trackingNo
     */
    public function setTrackingNo($trackingNo)
    {
        if (empty($invoiceNo) && !empty($this->shipmentObj)) {
            $this->trackingNo = $this->shipmentObj->getAllTracks()[0]->getNumber();
        } else {
            $this->trackingNo = $trackingNo;
        }
    }

    /**
     * @param mixed $voucherList
     */
    public function setVoucherList($voucherList)
    {
        $this->voucherList = $voucherList;
    }

    /**
     * @param mixed $voucherCode
     */
    public function setVoucherCode($voucherCode)
    {
        $this->voucherCode = $voucherCode;
    }

    /**
     * @param mixed $voucherValue
     */
    public function setVoucherValue($voucherValue)
    {
        $this->voucherValue = $voucherValue;
    }

    public function setCompanyCode($companyCode)
    {
        $this->companyCode = $companyCode;
    }

    public function setSupplierName($supplierName)
    {
        $this->supplierName = $supplierName;
    }

    /**
     * @param mixed $voucherExpired
     */
    public function setVoucherExpired($voucherExpired)
    {
        $this->voucherExpired = $voucherExpired;
    }

    /**
     * @param string $senderEmail
     * @param string $senderName
     */
    public function setEmailSender($senderEmail = '', $senderName = '')
    {
        if (empty($senderEmail)) {
            $senderEmail = $_ENV['SENDER_EMAIL'];
            $senderName = $_ENV['SENDER_NAME'];
        }

        if (!empty($senderName)) {
            $this->emailSender = $senderName . " <" . $senderEmail . ">";
        } else {
            $this->emailSender = $senderEmail;
        }
    }

    /**
     * @param string $emailReceiver
     * @param string $name
     * @param bool $isNeedName
     */
    public function setEmailReceiver($emailReceiver = '', $name = '', $isNeedName = true)
    {
        if (empty($name) && !empty($this->customerLastName)) {
            $name = $this->customerFirstName . " " . $this->customerLastName;
        }

        if (empty($emailReceiver) && !empty($this->orderObj)) {
            $emailReceiver = $this->orderObj->SalesCustomer->getCustomerEmail();
        }
        if (!empty($name) && $isNeedName) {
            $this->emailReceiver = $name . " <" . $emailReceiver . ">";
        } else if (!empty($emailReceiver)) {
            $this->emailReceiver = $emailReceiver;
        }
    }

    /**
     * @return mixed
     */
    public function getEmailCc()
    {
        return $this->emailCc;
    }

    /**
     * @param mixed $emailCc
     */
    public function setEmailCc($emailCc)
    {
        $this->emailCc = $emailCc;
    }

    /**
     * @return mixed
     */
    public function getEmailTag()
    {
        return $this->emailTag;
    }

    /**
     * @param mixed $emailTag
     */
    public function setEmailTag($emailTag)
    {
        $this->emailTag = $emailTag;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param mixed $attachments
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }


    /**
     * @param mixed $va_number
     */
    public function setVaNumber($vaNumber = "")
    {
        if (!empty($vaNumber)) {
            $this->vaNumber = $vaNumber;
        } else if (!empty($this->orderObj)) {
            $this->vaNumber = $this->orderObj->SalesOrderPayment->getVaNumber();
        }
    }

    /**
     * @param mixed $urlForgotPassword
     */
    public function setUrlForgotPassword($urlForgotPassword)
    {
        $this->urlForgotPassword = $urlForgotPassword;
    }

    /**
     * @param mixed $templateModel
     */
    public function setTemplateModel($templateModel)
    {
        $this->templateModel = $templateModel;
    }

    /**
     * @return mixed
     */
    public function getTemplateModel()
    {
        return $this->templateModel;
    }

    public function setStoreName($storeName)
    {
        $this->storeName = $storeName;
    }

    /**
     * Setting name for recipient email and greeting.
     * by default it'll get from order object, but you can set it manualy
     * @param string $customerLastName
     * @param string $customerFirstName
     */
    public function setName($customerLastName = '', $customerFirstName = '')
    {
        if (!empty($customerLastName)) {
            $this->customerLastName = $customerLastName;
        }

        if (!empty($customerFirstName)) {
            $this->customerFirstName = $customerFirstName;
        }

        if (empty($customerFirstName) && empty($customerLastName) && !empty($this->orderObj)) {
            $this->customerFirstName = $this->orderObj->SalesCustomer->getCustomerFirstname();
            $this->customerLastName = $this->orderObj->SalesCustomer->getCustomerLastname();
        }
    }

    /**
     * @return mixed
     */
    public function getInvProductNotif()
    {
        return $this->invProductNotif;
    }

    /**
     * @param mixed $invProductNotif
     */
    public function setInvProductNotif($invProductNotif)
    {
        $this->invProductNotif = $invProductNotif;
    }

    /**
     * @return mixed
     */
    public function getInvCleanCareNotif()
    {
        return $this->invCleanCareNotif;
    }

    /**
     * @param mixed $invCleanCareNotif
     */
    public function setInvCleanCareNotif($invCleanCareNotif)
    {
        $this->invCleanCareNotif = $invCleanCareNotif;
    }

    /**
     * @return mixed
     */
    public function getInvDesignerNotif()
    {
        return $this->invDesignerNotif;
    }

    /**
     * @param mixed $invDesignerNotif
     */
    public function setInvDesignerNotif($invDesignerNotif)
    {
        $this->invDesignerNotif = $invDesignerNotif;
    }

    /**
     * @return mixed
     */
    public function getInvMpNotif()
    {
        return $this->invMpNotif;
    }

    /**
     * @param mixed $invMpNotif
     */
    public function setInvMpNotif($invMpNotif)
    {
        $this->invMpNotif = $invMpNotif;
    }

    /**
     * @return mixed
     */
    public function getInvStoreReminder()
    {
        return $this->invStoreReminder;
    }

    /**
     * @param mixed $invStoreReminder
     */
    public function setInvStoreReminder($invStoreReminder)
    {
        $this->invStoreReminder = $invStoreReminder;
    }

    /**
     * @return mixed
     */
    public function getLinkDownload()
    {
        return $this->linkDownload;
    }

    /**
     * @param mixed $linkDownload
     */
    public function setLinkDownload($linkDownload)
    {
        $this->linkDownload = $linkDownload;
    }

    /**
     * @return mixed
     */
    public function getLinkReview()
    {
        return $this->linkReview;
    }

    /**
     * @param mixed $linkReview
     */
    public function setLinkReview($linkReview)
    {
        $this->linkReview = $linkReview;
    }

    /**
     * @return mixed
     */
    public function getPendingOrderList()
    {
        return $this->pendingOrderList;
    }

    /**
     * @param mixed $pendingOrderList
     */
    public function setPendingOrderList($pendingOrderList)
    {
        $this->pendingOrderList = $pendingOrderList;
    }

    /**
     * @return mixed
     */
    public function getInvoiceList()
    {
        return $this->invoiceList;
    }

    /**
     * @param mixed $invoiceList
     */
    public function setInvoiceList($invoiceList)
    {
        $this->invoiceList = $invoiceList;
    }

    public function getserviceAceServiceNumber()
    {
        return $this->serviceAceServiceNumber;
    }

    public function setserviceAceServiceNumber($serviceAceServiceNumber)
    {
        $this->serviceAceServiceNumber = $serviceAceServiceNumber;
    }

    public function getServiceWarrantyCharge()
    {
        return $this->serviceWarrantyCharge;
    }

    public function setServiceWarrantyCharge($serviceWarrantyCharge)
    {
        $this->serviceWarrantyCharge = $serviceWarrantyCharge;
    }

    public function getServiceWarrantyChargeLinkConfirmation()
    {
        return $this->serviceWarrantyChargeLinkConfirmation;
    }

    public function setServiceWarrantyChargeLinkConfirmation($serviceWarrantyChargeLinkConfirmation)
    {
        $this->serviceWarrantyChargeLinkConfirmation = $serviceWarrantyChargeLinkConfirmation;
    }

    /**
     * Get the value of paymetMethod
     */ 
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set the value of paymetMethod
     *
     * @return  self
     */ 
    public function setPaymentMethod($paymentMethod = '')
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Set the value of qrcode_url
     *
     * @return  self
     */
    public function setQRCodeURL($qrCodeURL = '')
    {
        $this->qrcode_url = $qrCodeURL;

        return $this;
    }

    public function getOrderEta(){
        return $this->eta;
    }  
        
    public function setOrderEta($eta){
        $this->eta = $eta;
    }
    
    public function getParentOrderEta(){
        return $this->parentOrderEta;
    }  
    
    public function setParentOrderEta($eta){
        $this->parentOrderEta = $eta;
    }

    /**
     * $gitfcard[c] = gift card voucher code
     * $giftcard[a] = gift card amount
     */
    private function _buildGiftcard()
    {
        $giftCard = array();
        if (!empty($this->orderObj)) {
            $giftCards = json_decode($this->orderObj->getGiftCards());
        }

        if (empty($giftCard)) {
            $this->giftCard['c'] = null;
            $this->giftCard['a'] = 0;
        } else {
            $voucherCode = "";
            foreach ($giftCards as $key => $card_info) {
                $voucherCode = $card_info->voucher_code . ",";
                $this->giftCard['a'] = $card_info->voucher_amount;
            }

            $this->giftCard['c'] = substr($voucherCode, 0, -1);
        }
    }

    public function buildProductList($showPrice = true, $useInvoiceItem = false, $params=null)
    {
        $products = array();

        if ($useInvoiceItem) {
            //$productList = $this->invoiceObj->getAllItems();
        } else {
            $order = $this->orderObj;
            $productList = $order->SalesOrderItem;
            if (!empty($order->getReferenceOrderNo())){
                if (isset($params["parent_invoice_no"])){
                    $salesShipmentModel = new \Models\SalesShipment(); 
                    $parentInvoiceShipmentData = $salesShipmentModel->getShipmentDataByInvoiceNo($params["parent_invoice_no"]);
                }
            }
        }

        $product = new \Models\Product();
        $paramSource = ["variants"];
        foreach ($productList as $item) {
            $getProductElastics = $product->getDetailFromElastic($item->getSku(), $paramSource);
            $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
            $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;

            $productItem = array(
                "image_name" => $imageName,
                "name" => $item->getName(),
                "sku" => $item->getSku(),
                "qty" => (int)$item->getQtyOrdered(), 
            );

            if (!empty($order->getReferenceOrderNo())){
                $currentTime = strtotime(date('Y-m-d H:i:s'));

                $leadTimeMin = (int)$item->getLeadTimeMin();
                $leadTimeMax =  (int)$item->getLeadTimeMax(); 
                $leadTimeUnit = $item ->getLeadTimeUnit();
                if ($leadTimeMin && $leadTimeMax && $leadTimeUnit){
                    $newLeadTime = array(
                        "min" => $leadTimeMin,
                        "max" =>  $leadTimeMax,
                        "unit" =>  $leadTimeUnit,
                    );
                    $newMinEta = strtotime(`+`.$newLeadTime["min"].` `.$newLeadTime["unit"], $currentTime);
                    $newMaxEta = strtotime(`+`.$newLeadTime["max"].` `.$newLeadTime["unit"], $currentTime);    
                    $this->eta = $newMaxEta;                     
                }
                
                if (isset($parentInvoiceShipmentData["lead_time_unit"]) && isset($parentInvoiceShipmentData["lead_time_min"]) && isset($parentInvoiceShipmentData["lead_time_max"])){
                    $oldLeadTime = array(
                        "min" => (int)$parentInvoiceShipmentData["lead_time_min"],
                        "max" => (int)$parentInvoiceShipmentData["lead_time_max"],
                        "unit" => $parentInvoiceShipmentData["lead_time_unit"]
                    ); 
                    $oldMinEta = strtotime(`+`.$oldLeadTime["min"].` `.$oldLeadTime["unit"], $currentTime);
                    $oldMaxEta = strtotime(`+`.$oldLeadTime["max"].` `.$oldLeadTime["unit"], $currentTime);
                    $this->parentOrderEta = $oldMaxEta; 
                }

                date_default_timezone_set("Asia/Jakarta");


                if (isset($newMinEta) && isset($newMaxEta)){
                    $newEta = array(
                        "min" => date('d-m-Y', $newMinEta),
                        "max" => date('d-m-Y', $newMaxEta),
                    );                     
                }


                if (isset($oldMinEta) && isset($oldMaxEta)){
                    $oldEta = array(
                        "min" => date('d-m-Y', $oldMinEta), 
                        "max" => date('d-m-Y', $oldMaxEta),
                    ); 
                }


                $productItem["new_lead_time"] = isset($newLeadTime) ? $newLeadTime : 0;
                $productItem["old_lead_time"] = isset($oldLeadTime) ? $oldLeadTime : 0;
                $productItem["new_eta"] = isset($newEta) ? $newEta : 0;
                $productItem["old_eta"] = isset($oldEta) ? $oldEta : 0;

                if (isset($parentInvoiceShipmentData["invoice_no"])){
                    $productItem["parent_invoice_no"] = $parentInvoiceShipmentData["invoice_no"];
                }
            } 

            if ($showPrice) {
                $productItem['sub_total'] = number_format($item->getRowTotal(), 0, ',', '.');
            }
            $products[] = $productItem;
        }

        $this->products = $products;
    }

    /**
     * Function to build list of shipping product item in invoice
     */
    public function buildShippingItems()
    {
        $products = array();

        $productList = $this->invoiceObj->SalesInvoiceItem;
        $product = new \Models\Product();
        $paramSource = ["variants"];
        foreach ($productList as $item) {
            $getProductElastics = $product->getDetailFromElastic($item->getSku(), $paramSource);
            $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
            $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;
            $productItem = array(
                "image_name" => $imageName,
                "name" => $item->getName(),
                "sku" => $item->getSku(),
                "qty" => $item->getQtyOrdered(),
                "sub_total" => number_format($item->getRowTotal(), 0, ',', '.')
            );

            $products[] = $productItem;
        }

        $this->products = $products;
    }

    /**
     * All notification will be send per invoice
     */
    public function buildNotificationItems()
    {
        if ($this->orderObj->SalesInvoice) {
            $this->orderNo = $this->orderObj->getOrderNo();

            $vtrSkuCampain = new \Models\VtrSkuCampaign();

            $product = new \Models\Product();
            $paramSource = ["variants"];

            /**
             * @var $orderInvoice \Models\SalesInvoice
             */
            foreach ($this->orderObj->SalesInvoice as $orderInvoice) {
                $qtyCleanCare = array();
                $qtyMp = array();
                $productStoreReminder = array();
                $invGrandTotal = 0;
                /**
                 * @var $invoiceItem \Models\SalesInvoiceItem
                 */
                foreach ($orderInvoice->SalesInvoiceItem as $invoiceItem) {
                    if (!empty($invoiceItem->getServiceDate())) {

                        $isCleanAndCare = $vtrSkuCampain::findFirst("sku = '" . $invoiceItem->getSku() . "'");

                        if ($isCleanAndCare) {

                            if ($isCleanAndCare->getCampaignName() == 'cleanandcare') {
                                $getProductElastics = $product->getDetailFromElastic($invoiceItem->getSku(), $paramSource);
                                $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                                $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;
                                $qtyCleanCare[] = array(
                                    "sku" => $invoiceItem->getSku(),
                                    "image_name" => $imageName,
                                    "name" => $invoiceItem->getName(),
                                    "qty" => (int)$invoiceItem->getQtyOrdered(),
                                    "date" => date('j F Y', strtotime($invoiceItem->getServiceDate())),
                                    "sub_total" => number_format($invoiceItem->getRowTotal(), 0, ',', '.')
                                );
                                $invGrandTotal += $invoiceItem->getRowTotal();
                            }

                            if ($isCleanAndCare->getCampaignName() == 'designer') {
                                $getProductElastics = $product->getDetailFromElastic($invoiceItem->getSku(), $paramSource);
                                $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                                $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;
                                $qtyDesigner[] = array(
                                    "sku" => $invoiceItem->getSku(),
                                    "image_name" => $imageName,
                                    "name" => $invoiceItem->getName(),
                                    "qty" => (int)$invoiceItem->getQtyOrdered(),
                                    "date" => date('j F Y', strtotime($invoiceItem->getServiceDate())),
                                    "sub_total" => number_format($invoiceItem->getRowTotal(), 0, ',', '.')
                                );
                                $invGrandTotal += $invoiceItem->getRowTotal();
                            }
                        }
                    }

                    if (substr($invoiceItem->getSupplierAlias(), 0, 2) == 'MP' && $invoiceItem->getSupplierAlias() != 'MP13') {
                        /**
                         * @angga
                         * @todo : change it to load data from elastic
                         */

                        $product = new \Models\Product();
                        $paramSource = ["variants", "suppplier"];
                        $getProductElastics = $product->getDetailFromElastic($invoiceItem->getSku(), $paramSource);
                        $getProducts    = $getProductElastics[0]['_source'];
                        $getVariant     = $getProducts['variants'][0];
                        $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                        $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;
                        $qtyMp[] = array(
                            "sku" => $invoiceItem->getSku(),
                            "image_name" => $imageName,
                            "sku_seller" => $getVariant['sku_seller'],
                            "name" => $invoiceItem->getName(),
                            "qty" => (int)$invoiceItem->getQtyOrdered()
                        );
                    }

                    // email reminder for specific store
                    $storeReminderEmail = getenv('STORE_REMINDER_ORDER_EMAIL');
                    $storeReminderEmailArray = explode(",", $storeReminderEmail);
                    if (in_array($invoiceItem->getStoreCode(), $storeReminderEmailArray)) {
                        $getProductElastics = $product->getDetailFromElastic($invoiceItem->getSku(), $paramSource);
                        $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                        $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;
                        $productStoreReminder[] = array(
                            "sku" => $invoiceItem->getSku(),
                            "image_name" => $imageName,
                            "product_name" => $invoiceItem->getName(),
                            "qty" => (int)$invoiceItem->getQtyOrdered()
                        );
                    }
                }

                if (!empty($qtyCleanCare)) {
                    $this->invCleanCareNotif[] = array("invoice_no" => $orderInvoice->getInvoiceNo(), "grand_total" => $invGrandTotal, "products" => $qtyCleanCare);
                }

                if (!empty($qtyDesigner)) {
                    $this->invDesignerNotif[] = array("invoice_no" => $orderInvoice->getInvoiceNo(), "grand_total" => $invGrandTotal, "products" => $qtyDesigner);
                }

                if (!empty($qtyMp)) {
                    $this->invMpNotif[] = array("invoice_no" => $orderInvoice->getInvoiceNo(), "supplier_id" => $orderInvoice->Supplier->getSupplierId(), "products" => $qtyMp);
                }

                if (!empty($productStoreReminder)) {
                    $this->invStoreReminder[] = array(
                        "pickup_code" => $orderInvoice->getPickupCode(),
                        "invoice_no" => $orderInvoice->getInvoiceNo(),
                        "invoice_date" => date('j F Y H:i:s', strtotime($orderInvoice->getCreatedAt())),
                        "products" => $productStoreReminder
                    );
                }
            }
        }
    }

    public function buildSuccessPaymentParam($preorder_list = array())
    {
        $this->orderObj->generateAddress();

        $sql = "SELECT
                    si.invoice_no,
                    si.handling_fee_adjust,
                    sii.invoice_item_id,
                    sii.name,
                    sii.sku,	
                    sii.qty_ordered as qty,	
                    sii.delivery_method,
                    sii.store_code,
                    s.name as 'store_name',
                    sii.supplier_alias,
                    sii.pickup_code,
                    COALESCE(mc.city_name, 'JAKARTA') as 'send_from',
	                sp.dikirim_oleh as 'send_by',
	                so.cart_id
                FROM
                    sales_invoice_item sii LEFT JOIN
                    sales_invoice si ON si.invoice_id = sii.invoice_id LEFT JOIN
                    sales_order so ON so.sales_order_id = si.sales_order_id LEFT JOIN
                    store s ON s.store_code = sii.store_code LEFT JOIN
                    supplier_address sa ON sa.supplier_address_id = s.supplier_address_id LEFT JOIN
                    supplier sp ON sp.supplier_id = sa.supplier_id LEFT JOIN
                    master_city mc ON mc.city_id = sa.city_id
                WHERE
                    si.sales_order_id =  " . $this->orderObj->getSalesOrderId() . ";";
        $orderItemArray = $this->runReadSql($sql);
        $splitMethodArray = array();
        $handlingFeeAdjust = 0;
        foreach ($orderItemArray as $keyOrderItem => $valueOrderItem) {
            # split by delivery method
            $deliveryGroup = array('delivery', 'ownfleet');
            if (in_array($valueOrderItem['delivery_method'], $deliveryGroup)) {
                $valueOrderItem['delivery_method'] = (substr($valueOrderItem['supplier_alias'], 0, 1) == 'M') ? 'marketplace' : $valueOrderItem['delivery_method'];
            }

            if ($valueOrderItem['sku'] == getenv('MEMBERSHIP_HCI_SKU') || $valueOrderItem['sku'] == getenv('MEMBERSHIP_AHI_SKU')) {
                $valueOrderItem['delivery_method'] = 'digital_product';
            }

            $valueOrderItem['store_code'] = ($valueOrderItem['delivery_method'] == 'store_fulfillment' || $valueOrderItem['delivery_method'] == 'pickup' || $valueOrderItem['delivery_method'] == 'digital_product') ? $valueOrderItem['pickup_code'] : $valueOrderItem['store_code'];
            $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['invoice_no'] = $valueOrderItem['invoice_no'];
            $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['invoice_item_id'] = $valueOrderItem['invoice_item_id'];
            $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['sku'] = $valueOrderItem['sku'];
            $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['name'] = $valueOrderItem['name'];
            $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['qty'] = $valueOrderItem['qty'];
            $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['delivery_method'] = $valueOrderItem['delivery_method'];
            $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['store_code'] = $valueOrderItem['store_code'];
            $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['store_name'] = $valueOrderItem['store_name'];

            if ($valueOrderItem['delivery_method'] == 'store_fulfillment' || $valueOrderItem['delivery_method'] == 'pickup') {
                $sql = "SELECT
                        mc.city_name
                    FROM
                        pickup_point pp LEFT JOIN 
                        master_city mc ON mc.city_id = pp.city_id
                    where pickup_code = '{$valueOrderItem['pickup_code']}';";
                $sendFromArray = $this->runReadSql($sql);
                $sendFrom = '-';
                if (count($sendFromArray) > 0) {
                    $sendFrom = $sendFromArray[0]['city_name'];
                }

                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['send_from'] = $this->removeCityPrefix($sendFrom);
            } else {
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['send_from'] = $this->removeCityPrefix($valueOrderItem['send_from']);
            }


            if ($valueOrderItem['delivery_method'] == 'pickup' || $valueOrderItem['delivery_method'] == 'digital_product') {
                $sql = "SELECT 
                          pickup_name, address_line_1, address_line_2, 
                          (SELECT COALESCE (city_name, '') FROM master_city WHERE city_id = pp.city_id) as city_name,
                          (SELECT COALESCE (province_name, '') FROM master_province WHERE province_code = pp.province_code) as region                
                        FROM pickup_point pp 
                        WHERE pickup_code = '{$valueOrderItem['pickup_code']}' LIMIT 1;";
                $pickupArray = $this->runReadSql($sql);
                $pickupName = $pickupAddressLine1 = $pickupAddressLine2 = $cityName = $region = '';
                if (isset($pickupArray[0])) {
                    $pickupName = $pickupArray[0]['pickup_name'];
                    $pickupAddressLine1 = $pickupArray[0]['address_line_1'];
                    $pickupAddressLine2 = $pickupArray[0]['address_line_2'];
                    $cityName = $pickupArray[0]['city_name'];
                    $region = $pickupArray[0]['region'];
                }
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['send_by'] = $pickupName;
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['customer_name'] = $pickupName;
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['street'] = $pickupAddressLine1;
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['kecamatan'] = $pickupAddressLine2;
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['city'] = $cityName;
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['region'] = $region;
            } else {
                $shippingAddress = is_object($this->orderObj->getShippingAddress()) ? $this->orderObj->getShippingAddress()->getDataArray() : [];
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['send_by'] = $valueOrderItem['send_by'];
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['customer_name'] = ucfirst($shippingAddress['first_name']) . ' ' . ucfirst($shippingAddress['last_name']);
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['street'] = $shippingAddress['full_address'];
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['kecamatan'] = $shippingAddress['kecamatan']['kecamatan_name'];
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['city'] = $shippingAddress['city']['city_name'];
                $splitMethodArray[$valueOrderItem['delivery_method']][$valueOrderItem['store_code']][$keyOrderItem]['region'] = $shippingAddress['province']['province_name'];
            }

            // If any handling fee
            $handlingFeeAdjust += $valueOrderItem['handling_fee_adjust'];
        }

        $productListDigitalProduct = array();
        $productListStoreFulfillment = $productListDelivery = $productListMarketplace = $productListOwnFleet = $productListPickup = array();
        if (isset($splitMethodArray['store_fulfillment'])) {
            $productListStoreFulfillment = $this->makeProductListNonPickup($splitMethodArray['store_fulfillment'], $preorder_list);
        }

        if (isset($splitMethodArray['delivery'])) {
            $productListDelivery = $this->makeProductListNonPickup($splitMethodArray['delivery'], $preorder_list);
        }

        if (isset($splitMethodArray['marketplace'])) {
            $productListMarketplace = $this->makeProductListNonPickup($splitMethodArray['marketplace'], $preorder_list);
        }

        if (isset($splitMethodArray['ownfleet'])) {
            $productListOwnFleet = $this->makeProductListNonPickup($splitMethodArray['ownfleet'], $preorder_list);
        }

        if (isset($splitMethodArray['pickup'])) {
            $productListPickup = $this->makeProductListPickup($splitMethodArray['pickup'], $preorder_list);
        }

        if (isset($splitMethodArray['digital_product'])) {
            $productListDigitalProduct = $this->makeProductListDigitalProduct($splitMethodArray['digital_product'], $preorder_list);
        }

        $dateObj = new \DateTime($this->orderObj->getCreatedAt());
        $orderDate = $dateObj->format('d F Y H:i:s');

        $payment_method = ucwords(str_replace("_", " ", $this->orderObj->SalesOrderPayment->getType()));
        $installmentTenor = $this->orderObj->SalesOrderPayment->getInstallmentTenor();
        if (!empty($installmentTenor)) {
            $payment_method = "Cicilan Kartu Kredit ( " . $installmentTenor . " Bulan )";
        }

        if (strtolower($this->orderObj->SalesOrderPayment->getType()) == "credit_card") {
            $payment_method = "Kartu Kredit";
        }
        if (strtolower($this->orderObj->SalesOrderPayment->getMethod()) == "ovo") {
            $payment_method = "Uang Elektronik (OVO)";
        }
        if (strtolower($this->orderObj->SalesOrderPayment->getMethod()) == "gopay") {
            $payment_method = "Uang Elektronik (GOPAY)";
        }
        if (strtolower($this->orderObj->SalesOrderPayment->getMethod()) == "free_payment") {
            $payment_method = "Free Payment";
        }
        if (strtolower($this->orderObj->SalesOrderPayment->getMethod()) == "qris") {
            $payment_method = "QRIS";
        }
        if ($this->orderObj->SalesOrderPayment->getMethod() == "bca_klikpay") {
            $payment_method = "Online Payment (KlikPay BCA)";
        }
        if ($this->orderObj->SalesOrderPayment->getMethod() == "danakini") {
            $payment_method = "Danakini";
        }

        $productListNonPickup = array_merge($productListDelivery, $productListStoreFulfillment, $productListMarketplace, $productListOwnFleet);
        $noteNonPickup = '';
        if (count($productListNonPickup) > 1) {
            $noteNonPickup = getenv("NOTE_NON_PICKUP");
        }

        $flagPickup = $flagNonPickup = $flagDigitalProduct = false;
        if (count($productListPickup) > 0) {
            $flagPickup = true;
        }

        if (count($flagDigitalProduct) > 0) {
            $flagDigitalProduct = true;
        }

        $receiptAddress = array();
        if (count($productListNonPickup) > 0) {
            $flagNonPickup = true;

            $receiptAddress['customer_name'] = $productListNonPickup[0]['customer_name'];
            $receiptAddress['street'] = $productListNonPickup[0]['street'];
            $receiptAddress['kecamatan'] = $productListNonPickup[0]['kecamatan'];
            $receiptAddress['city'] = $productListNonPickup[0]['city'];
            $receiptAddress['region'] = $productListNonPickup[0]['region'];
        }

        $cashbackTotal = 0;
        $flagCashback = false;
        // if (!empty($valueOrderItem['cart_id'])) {
        //     $cart = new \Library\Cart();
        //     $paramEmail['cart_id'] = $valueOrderItem['cart_id'];
        //     $cartResponse = $cart->checkCashBackPromotion($paramEmail);
        //     if ($cartResponse['is_have_promotion'] && $cartResponse['voucher_amount'] > 0) {
        //         $cashbackTotal = $cartResponse['voucher_amount'];
        //         $flagCashback = true;
        //     }
        // }

        // Gosend Template
        $subTotal = ($this->orderObj->getSubtotal() - $this->orderObj->getDiscountAmount()) + $handlingFeeAdjust;
        $shippingFee = $this->orderObj->getShippingAmount() - $this->orderObj->getShippingDiscountAmount();
        $giftCardAmount = $this->orderObj->getGiftCardsAmount();

        
        $salesruleOrderModel = new \Models\SalesruleOrder();
        $sql = "select voucher_code from salesrule_order where sales_order_id = " . $this->orderObj->getSalesOrderId();
        $salesruleOrderModel->useReadOnlyConnection();
        $result = $salesruleOrderModel->getDi()->getShared($salesruleOrderModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $voucherArray = $result->fetch();        


        $emailParam = array(
            "order_no" => $this->orderObj->getOrderNo(),
            "order_date" => $orderDate,
            "payment_method" => $payment_method,
            "product_non_pickup" => $productListNonPickup,
            "note_non_pickup" => $noteNonPickup,
            "receipt_address" => $receiptAddress,
            "product_pickup" => $productListPickup,
            "product_digital" => $productListDigitalProduct,
            "flag_digital_product" => $flagDigitalProduct,
            "flag_pickup" => $flagPickup,
            "flag_non_pickup" => $flagNonPickup,
            "flag_cashback" => $flagCashback,
            "sub_total" => number_format($subTotal, 0, ',', '.'),
            "shipment_fee" => number_format($shippingFee, 0, ',', '.'),
            "voucher_code" => (!empty($voucherArray) ? $voucherArray['voucher_code'] : ""),
            "gifcard_amount" => number_format($giftCardAmount, 0, ',', '.'),
            "grand_total" => number_format($this->orderObj->getGrandTotal(), 0, ',', '.'),
            "cashback_total" => number_format($cashbackTotal, 0, ',', '.'),
            "handling_fee" => number_format($this->orderObj->getHandlingFeeAdjust(), 0, ',', '.'),
            "mdr_customer"=> number_format($this->orderObj->getMdrCustomer(), 0, ',', '.')
        );

        $this->templateModel = $emailParam;
    }

    private function makeProductListDigitalProduct($splitMethodArray = array(), $preorder_list = array())
    {
        $i = 0;
        $product = new \Models\Product();
        $invoiceItemModel = new \Models\SalesInvoiceItem();
        $paramSource = ["variants"];
        foreach ($splitMethodArray as $rowStoreCode) {
            $rowStoreCode = array_values($rowStoreCode);
            $productList[$i]['invoice_no'] = $rowStoreCode[0]['invoice_no'];
            $productList[$i]['send_by'] = $rowStoreCode[0]['send_by'];
            $productList[$i]['store_name'] = $rowStoreCode[0]['store_name'];
            $productList[$i]['customer_name'] = $rowStoreCode[0]['customer_name'];
            $j = 0;
            foreach ($rowStoreCode as $rowProduct) {
                $getProductElastics = $product->getDetailFromElastic($rowProduct['sku'], $paramSource);
                $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;

                $productList[$i]['product_list'][$j]['name'] = $rowProduct['name'];
                $productList[$i]['product_list'][$j]['image_name'] = $imageName;
                $productList[$i]['product_list'][$j]['qty'] = $rowProduct['qty'];

                $productList[$i]['product_list'][$j]['preorder_note'] = "";
                foreach ($preorder_list as $preorder) {
                    if ($rowProduct['sku'] == $preorder['sku']) {
                        $productList[$i]['product_list'][$j]['preorder_note'] = $preorder['message'];
                    }
                }

                // Gosend template
                # get invoice item
                try {
                    $sql = "SELECT sku,name,qty_ordered as qty, selling_price,handling_fee_adjust,discount_amount, qty_ordered, is_free_item  FROM sales_invoice_item 
                            WHERE invoice_item_id = " . $rowProduct['invoice_item_id'];
                    $invoiceItemModel->useReadOnlyConnection();
                    $result = $invoiceItemModel->getDi()->getShared($invoiceItemModel->getConnection())->query($sql);
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );
                    $invoiceItemArray = $result->fetch();
                } catch (\Exception $e) {
                    \Helpers\LogHelper::log("email-gosend", print_r($e->getMessage(), true));
                }

                $invoiceItemModel->setFromArray($invoiceItemArray);
                if ($invoiceItemArray['is_free_item'] == 1) {
                    $productList[$i]['product_list'][$j]['sub_total'] = 0;
                } else {
                    // stupid dirty fix, due to repeating subtotal in email -> ex. INV209024667
                    $rowTotal = ($invoiceItemArray['selling_price'] + $invoiceItemArray['handling_fee_adjust'] - $invoiceItemArray['discount_amount'])
                        * $invoiceItemArray['qty'];
                    $productList[$i]['product_list'][$j]['sub_total'] = number_format($rowTotal, 0, ',', '.');
                }
                $j++;
            }
            $i++;
        }

        return $productList;
    }

    private function makeProductListPickup($splitMethodArray = array(), $preorder_list = array())
    {
        $i = 0;
        $product = new \Models\Product();
        $invoiceItemModel = new \Models\SalesInvoiceItem();
        $paramSource = ["variants"];
        $storeCodeNewRetail = $this->orderObj->getStoreCodeNewRetail();
        foreach ($splitMethodArray as $rowStoreCode) {
            $rowStoreCode = array_values($rowStoreCode);
            $productList[$i]['invoice_no'] = $rowStoreCode[0]['invoice_no'];
            $productList[$i]['send_by'] = $rowStoreCode[0]['send_by'];
            $productList[$i]['store_name'] = $rowStoreCode[0]['store_name'];
            $productList[$i]['customer_name'] = $rowStoreCode[0]['customer_name'];
            $productList[$i]['street'] = $rowStoreCode[0]['street'];
            $productList[$i]['kecamatan'] = $rowStoreCode[0]['kecamatan'];
            $productList[$i]['city'] = $rowStoreCode[0]['city'];
            $productList[$i]['region'] = $rowStoreCode[0]['region'];
            if (!empty($storeCodeNewRetail)) {
                $qrcodeDestUrl = getenv("STORE_DASHBOARD_URL") . "search?" . urlencode("keyword=" . $rowStoreCode[0]['invoice_no'] . "&bypass_processing=yes");
                $productList[$i]['qrcode_url'] = "https://chart.googleapis.com/chart?chl=" . $qrcodeDestUrl . "&cht=qr&chs=150x150";
            }

            $j = 0;
            foreach ($rowStoreCode as $rowProduct) {
                $getProductElastics = $product->getDetailFromElastic($rowProduct['sku'], $paramSource);
                $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;

                $productList[$i]['product_list'][$j]['name'] = $rowProduct['name'];
                $productList[$i]['product_list'][$j]['image_name'] = $imageName;
                $productList[$i]['product_list'][$j]['qty'] = $rowProduct['qty'];

                $productList[$i]['product_list'][$j]['preorder_note'] = "";
                foreach ($preorder_list as $preorder) {
                    if ($rowProduct['sku'] == $preorder['sku']) {
                        $productList[$i]['product_list'][$j]['preorder_note'] = $preorder['message'];
                    }
                }

                // Gosend template
                # get invoice item
                try {
                    $sql = "SELECT sku,name,qty_ordered as qty, selling_price,handling_fee_adjust,discount_amount, qty_ordered, is_free_item  FROM sales_invoice_item 
                            WHERE invoice_item_id = " . $rowProduct['invoice_item_id'];
                    $invoiceItemModel->useReadOnlyConnection();
                    $result = $invoiceItemModel->getDi()->getShared($invoiceItemModel->getConnection())->query($sql);
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );
                    $invoiceItemArray = $result->fetch();
                } catch (\Exception $e) {
                    \Helpers\LogHelper::log("email-gosend", print_r($e->getMessage(), true));
                }

                $invoiceItemModel->setFromArray($invoiceItemArray);
                if ($invoiceItemArray['is_free_item'] == 1) {
                    $productList[$i]['product_list'][$j]['sub_total'] = 0;
                } else {
                    // stupid dirty fix, due to repeating subtotal in email -> ex. INV209024667
                    $rowTotal = ($invoiceItemArray['selling_price'] + $invoiceItemArray['handling_fee_adjust'] - $invoiceItemArray['discount_amount'])
                        * $invoiceItemArray['qty'];
                    $productList[$i]['product_list'][$j]['sub_total'] = number_format($rowTotal, 0, ',', '.');
                }
                $j++;
            }
            $i++;
        }

        return $productList;
    }

    private function makeProductListNonPickup($splitMethodArray = array(), $preorder_list = array())
    {
        $i = 0;
        $product = new \Models\Product();
        $invoiceItemModel = new \Models\SalesInvoiceItem();
        $paramSource = ["variants"];
        foreach ($splitMethodArray as $rowStoreCode) {
            $rowStoreCode = array_values($rowStoreCode);
            $productList[$i]['invoice_no'] = $rowStoreCode[0]['invoice_no'];
            $productList[$i]['send_by'] = $rowStoreCode[0]['send_by'];
            $productList[$i]['send_from'] = $rowStoreCode[0]['send_from'];
            $productList[$i]['store_name'] = $rowStoreCode[0]['store_name'];
            $productList[$i]['customer_name'] = $rowStoreCode[0]['customer_name'];
            $productList[$i]['street'] = $rowStoreCode[0]['street'];
            $productList[$i]['kecamatan'] = $rowStoreCode[0]['kecamatan'];
            $productList[$i]['city'] = $rowStoreCode[0]['city'];
            $productList[$i]['region'] = $rowStoreCode[0]['region'];
            $j = 0;
            foreach ($rowStoreCode as $rowProduct) {
                $getProductElastics = $product->getDetailFromElastic($rowProduct['sku'], $paramSource);
                $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;

                $productList[$i]['product_list'][$j]['name'] = $rowProduct['name'];
                $productList[$i]['product_list'][$j]['image_name'] = $imageName;
                $productList[$i]['product_list'][$j]['qty'] = $rowProduct['qty'];

                $productList[$i]['product_list'][$j]['preorder_note'] = "";
                foreach ($preorder_list as $preorder) {
                    if ($rowProduct['sku'] == $preorder['sku']) {
                        $productList[$i]['product_list'][$j]['preorder_note'] = $preorder['message'];
                    }
                }
                // Gosend template
                # get invoice item
                try {
                    $sql = "SELECT sku,name,qty_ordered as qty, selling_price,handling_fee_adjust,discount_amount, qty_ordered, is_free_item  FROM sales_invoice_item 
                            WHERE invoice_item_id = " . $rowProduct['invoice_item_id'];
                    $invoiceItemModel->useReadOnlyConnection();
                    $result = $invoiceItemModel->getDi()->getShared($invoiceItemModel->getConnection())->query($sql);
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );
                    $invoiceItemArray = $result->fetch();
                } catch (\Exception $e) {
                    \Helpers\LogHelper::log("email-gosend", print_r($e->getMessage(), true));
                }

                $invoiceItemModel->setFromArray($invoiceItemArray);
                if ($invoiceItemArray['is_free_item'] == 1) {
                    $productList[$i]['product_list'][$j]['sub_total'] = 0;
                } else {
                    // stupid dirty fix, due to repeating subtotal in email -> ex. INV209024667
                    $rowTotal = ($invoiceItemArray['selling_price'] + $invoiceItemArray['handling_fee_adjust'] - $invoiceItemArray['discount_amount'])
                        * $invoiceItemArray['qty'];
                    $productList[$i]['product_list'][$j]['sub_total'] = number_format($rowTotal, 0, ',', '.');
                }

                $j++;
            }
            $i++;
        }

        return $productList;
    }

    private function removeCityPrefix($string = '')
    {
        $patterns = array();
        $patterns[0] = '/KAB /';
        $patterns[1] = '/KAB. /';
        $patterns[2] = '/KOTA ADMINISTRASI /';
        $patterns[3] = '/KOTA /';
        $replacements = array();
        $replacements[0] = '';
        $replacements[1] = '';
        $replacements[2] = '';
        $replacements[3] = '';

        return preg_replace($patterns, $replacements, $string);
    }

    public function generateAddress()
    {
        $this->orderObj->generateAddress();
        // @todo: improve when implementing new template email
        if (is_object($this->orderObj->getShippingAddress())) {
            $shippingAddress = $this->orderObj->getShippingAddress()->getDataArray();
            $this->shippingAddress = array(
                "customer_name" => ucfirst($shippingAddress['first_name']) . ' ' . ucfirst($shippingAddress['last_name']),
                "street" => $shippingAddress['full_address'],
                "kecamatan" => (isset($shippingAddress['kecamatan']['kecamatan_name'])) ? $shippingAddress['kecamatan']['kecamatan_name'] : '-',
                "city" => (isset($shippingAddress['city']['city_name'])) ? $shippingAddress['city']['city_name'] : '-',
                "region" => (isset($shippingAddress['province']['province_name'])) ? $shippingAddress['province']['province_name'] : '-',
                "postcode" => ($shippingAddress['post_code'] == NULL) ? "-" : $shippingAddress['post_code'],
                "country" => 'Indonesia',
                "telephone" => ($shippingAddress['phone'] == NULL) ? "-" : $shippingAddress['phone']
            );
        } else {
            $this->shippingAddress = array(
                "customer_name" => '-',
                "street" => '',
                "kecamatan" => '',
                "city" => '',
                "region" => '',
                "postcode" => '',
                "country" => 'Indonesia',
                "telephone" => ''
            );
        }
    }

    public function generateAddressByInvoiceID($invoiceID = 0)
    {
        $this->shippingAddress = array(
            "customer_name" => '-',
            "street" => '',
            "kecamatan" => '',
            "city" => '',
            "region" => '',
            "postcode" => '',
            "country" => 'Indonesia',
            "telephone" => ''
        );

        if ($invoiceID > 0) {
            $sql = "SELECT 
                CONCAT(CONCAT(UPPER(LEFT(soa.first_name,1)),SUBSTRING(soa.first_name,2)), ' ',  CONCAT(UPPER(LEFT(soa.last_name ,1)),SUBSTRING(soa.last_name ,2))) as customer_name,
                soa.full_address as street,
                (select kecamatan_name from master_kecamatan where kecamatan_id = soa.kecamatan_id limit 1) as kecamatan,
                (select city_name from master_city where city_id = soa.city_id limit 1) as city,
                (select province_name from master_province where province_id  = soa.province_id limit 1) as region,
                soa.post_code,
                soa.phone as telephone
            FROM sales_invoice si LEFT JOIN sales_shipment ss on ss.invoice_id = si.invoice_id 
            LEFT JOIN sales_order_address soa on soa.sales_order_address_id = ss.shipping_address_id 
            WHERE si.invoice_id = " . $invoiceID . ";";
            $addressArray = $this->runReadSql($sql);
            if (count($addressArray) > 0) {
                foreach ($addressArray as $row) {
                    $this->shippingAddress = array(
                        "customer_name" => $row['customer_name'],
                        "street" => $row['street'],
                        "kecamatan" => $row['kecamatan'],
                        "city" => $row['city'],
                        "region" => $row['region'],
                        "postcode" => $row['post_code'],
                        "country" => 'Indonesia',
                        "telephone" => $row['telephone']
                    );

                    break;
                }
            }           
        }      
    }

    /**
     * Build basic order email param
     * it content basic order, product, total summary information
     */
    public function buildOrderEmailParam($params=null)
    {
        $this->buildProductList(true, false, $params);

        $dateObj = new \DateTime($this->orderObj->getCreatedAt());
        $orderDate = $dateObj->format('d F Y H:i:s');

        $payment_method = ucwords(str_replace("_", " ", $this->orderObj->SalesOrderPayment->getType()));
        $expiredDate = date('d F Y H:i:s', strtotime($this->orderObj->SalesOrderPayment->getExpireTransaction()));

        $cashbackTotal = 0;
        $flagCashback = false;
        if (!empty($this->orderObj->getCartId())) {
            $cart = new \Library\Cart();
            $paramEmail['cart_id'] = $this->orderObj->getCartId();
               \Helpers\LogHelper::log('debug_625', '/app/Library/Email.php line 1352', 'debug');
            $cartResponse = $cart->checkCashBackPromotion($paramEmail);
            if ($cartResponse['is_have_promotion'] && $cartResponse['voucher_amount'] > 0) {
                $cashbackTotal = $cartResponse['voucher_amount'];
                $flagCashback = true;
            }
        }

        $storeNewRetail = array();
        $storeName = "";
        if (!empty($this->orderObj->getStoreCodeNewRetail()) && (substr($this->orderObj->getStoreCodeNewRetail(), 0, 1) == "A" || substr($this->orderObj->getStoreCodeNewRetail(), 0, 1) == "H" || substr($this->orderObj->getStoreCodeNewRetail(), 0, 1) == "J")) {
            $sql = "SELECT name FROM store WHERE store_code = '" . $this->orderObj->getStoreCodeNewRetail() . "'";
            $storeNewRetail = $this->runReadSql($sql);
            if (!empty($storeNewRetail)) {
                $storeName = $storeNewRetail[0]['name'];
            }
        }

        $emailParam = array(
            "order_no" => $this->orderObj->getOrderNo(),
            "order_date" => $orderDate,
            "payment_method" => $payment_method,
            "product" => $this->products,
            "va_number" => $this->orderObj->SalesOrderPayment->getVaNumber(),
            "va_bank" => $this->orderObj->SalesOrderPayment->getVaBank(),
            "sub_total" => number_format($this->orderObj->getSubtotal(), 0, ',', '.'),
            "shipment_fee" => number_format(($this->orderObj->getShippingAmount() - $this->orderObj->getShippingDiscountAmount()), 0, ',', '.'),
            "voucher_code" => "",
            "gifcard_amount" => number_format($this->orderObj->getGiftCardsAmount(), 0, ',', '.'),
            "grand_total" => number_format($this->orderObj->getGrandTotal(), 0, ',', '.'),
            "expired_date" => $expiredDate,
            "cashback_total" => number_format($cashbackTotal, 0, ',', '.'),
            "flag_cashback" => $flagCashback,
            "store_code_new_retail" => $storeName,
            "handling_fee" => number_format($this->orderObj->getHandlingFeeAdjust(), 0, ',', '.')
        );

        $orderNo = $this->orderObj->getOrderNo();
        $to = $this->emailReceiver;
        if (!empty($this->orderObj->getParentOrderNo())) {
            $emailParam['new_order_no'] = $orderNo;
            $emailParam['order_no'] = $this->orderObj->getParentOrderNo();
            if (count($this->products) > 0){
                $emailParam['reorder_message'] = "Pesanan ".$this->products[0]["parent_invoice_no"]." akan sampai paling lambat ".$this->products[0]["new_eta"].". Karena ketidaktersediaan stok di toko terdekat, pengiriman akan kami alihkan ke toko lain.";                
            }
        }
        $emailParam['link_status'] = getenv("BASE_URL") . "order-detail?orderId=$orderNo&email=$to";

        if (substr($emailParam['order_no'], 0, 4) == "ODIB" || substr($emailParam['order_no'], 0, 4) == "ODIC") {
            $emailParam['discount_amount'] = number_format($this->orderObj->getDiscountAmount() + $this->orderObj->getGiftCardsAmount(), 0, ',', '.');
            $subtotal = $this->orderObj->getSubtotal();
            $tax = ceil((float)($subtotal - (100/111) * $subtotal));
            $emailParam['tax_amount'] = number_format($tax, 0, ',', '.');
            $emailParam['sub_total'] = number_format($subtotal - $tax, 0, ',', '.');
        }

        $this->templateModel = $emailParam;
    }

    /**
     * Build email param for Email review
     */
    public function buildShippingEmailParam()
    {
        // Show all product in an order
        $this->buildProductList();

        $dateObj = new \DateTime($this->orderObj->getCreatedAt());
        $this->orderDate = $dateObj->format('d F Y H:i:s');

        $dateShipmentObj = new \DateTime($this->shipmentObj->getUpdatedAt());
        $this->shipmentDate = $dateShipmentObj->format('d F Y H:i:s');

        $emailParam = array(
            "order_no" => $this->orderObj->getOrderNo(),
            "order_date" => $this->orderDate,
            "product" => $this->products,
            "shipment_date" => $this->shipmentDate,
            "tracking_number" => $this->shipmentObj->getTrackNumber(),
            "courier_name" => $this->shipmentObj->ShippingCarrier->getCarrierName()
        );

        if (!empty($this->templateModel)) {
            $this->templateModel = array_merge($this->templateModel, $emailParam);
        } else {
            $this->templateModel = $emailParam;
        }
    }

    public function buildShipmentCheckAwbParams()
    {
        // product per invoice
        $this->buildShippingItems();

        $dateObj = new \DateTime($this->orderObj->getCreatedAt());
        $this->orderDate = $dateObj->format('d F Y H:i:s');

        $dateShipmentObj = new \DateTime($this->shipmentObj->getUpdatedAt());
        $this->shipmentDate = $dateShipmentObj->format('d F Y H:i:s');

        $emailParam = array(
            "order_no" => $this->orderObj->getOrderNo(),
            "order_date" => $this->orderDate,
            "product" => $this->products,
            "shipment_date" => $this->shipmentDate,
            "tracking_number" => $this->shipmentObj->getTrackNumber(),
            "courier_name" => $this->shipmentObj->ShippingCarrier->getCarrierName()
        );

        if (!empty($this->templateModel)) {
            $this->templateModel = array_merge($this->templateModel, $emailParam);
        } else {
            $this->templateModel = $emailParam;
        }
    }

    /**
     * Build email param for Email Shipment, email shipment processing and shipment completed
     */
    public function buildShipmentCheckAwbParam()
    {
        $dateObj = new \DateTime($this->orderObj->getCreatedAt());
        $this->orderDate = $dateObj->format('d F Y H:i:s');

        $dateShipmentObj = new \DateTime($this->shipmentObj->getUpdatedAt());
        $this->shipmentDate = $dateShipmentObj->format('d F Y H:i:s');

        $flagSap = $flagJne = false;
        if ($this->shipmentObj->ShippingCarrier) {
            $courierCode = ($this->shipmentObj->ShippingCarrier->getCarrierCode()) ? $this->shipmentObj->ShippingCarrier->getCarrierCode() : '';
            if ($courierCode == 'sap') {
                $flagSap = true;
            } elseif ($courierCode == 'jne') {
                $flagJne = true;
            }
        }

        $sql = "SELECT invoice_no, sales_order_id, delivery_method, pickup_code, (SELECT dikirim_oleh FROM supplier WHERE supplier_alias = si.supplier_alias LIMIT 1) as send_by FROM sales_invoice si WHERE sales_order_id = " . $this->orderObj->getSalesOrderId() .
            " AND delivery_method IN('delivery','store_fulfillment','marketplace','ownfleet') ORDER BY delivery_method";
        $invoiceArray = $this->runReadSql($sql);

        $shipmentSeqTotal = 0;
        $shipmentSeq = 1;
        $sendBy = $deliveryMethod = '';
        $seqArray = array();
        $params = array();
        foreach ($invoiceArray as $rowInvoice) {
            if ($rowInvoice['delivery_method'] == 'store_fulfillment') {
                $seqArray[$rowInvoice['delivery_method']][$rowInvoice['pickup_code']][] = $rowInvoice['invoice_no'];
            } else {
                $seqArray[$rowInvoice['delivery_method']][] = $rowInvoice['invoice_no'];
            }

            if ($rowInvoice['invoice_no'] == $this->invoiceObj->getInvoiceNo()) {
                $sendBy = $rowInvoice['send_by'];
                $params['delivery_method'] = $rowInvoice['delivery_method'];
                $params['pickup_code'] = $rowInvoice['pickup_code'];
                $params['sales_order_id'] = $rowInvoice['sales_order_id'];
            }
        }

        # set shipment sequence
        foreach ($seqArray as $keySeq => $valueSeq) {
            if ($keySeq == 'store_fulfillment') {
                $shipmentSeqTotal += count($seqArray['store_fulfillment']);
                foreach ($seqArray[$keySeq] as $keyFulfillment => $valueFulfillment) {
                    if (in_array($this->invoiceObj->getInvoiceNo(), $valueFulfillment)) {
                        $shipmentSeq = $shipmentSeqTotal;
                    }
                }
            } else {
                $shipmentSeqTotal++;
                if (in_array($this->invoiceObj->getInvoiceNo(), $seqArray[$keySeq])) {
                    $shipmentSeq = $shipmentSeqTotal;
                }
            }
        }

        if (!isset($params['delivery_method']) || !isset($params['pickup_code']) || !isset($params['sales_order_id'])) {
            $this->buildShippingItems();
        } else {
            # get invoice that have same pickup_code, sales_order_id and delivery_method
            $sql = "SELECT invoice_id,invoice_no FROM sales_invoice 
            WHERE delivery_method = '{$params['delivery_method']}' AND
            pickup_code = '{$params['pickup_code']}' AND
            sales_order_id = {$params['sales_order_id']};";
            $invoiceGroupList = $this->runReadSql($sql);
            $products = array();
            $productLib = new \Models\Product();
            $salesInvoiceItemLib = new \Models\SalesInvoiceItem();

            foreach ($invoiceGroupList as $rowInvoiceGroup) {
                # get invoice item
                $sql = "SELECT sku,name,qty_ordered as qty, selling_price,handling_fee_adjust,discount_amount, qty_ordered, is_free_item  FROM sales_invoice_item WHERE invoice_id = {$rowInvoiceGroup['invoice_id']};";
                $invoiceItemArray = $this->runReadSql($sql);

                # get credit memo item
                $sql = "SELECT sku,qty_refunded FROM sales_credit_memo_item WHERE invoice_no = '{$rowInvoiceGroup['invoice_no']}' AND qty_refunded > 0;";
                $creditMemoItemArray = $this->runReadSql($sql);
                if (count($creditMemoItemArray) > 0) {
                    # subtract invoice item with credit memo item
                    $removeKey = array();
                    foreach ($creditMemoItemArray as $rowCmItem) {
                        foreach ($invoiceItemArray as $keyInvoItem => $valueInvoItem) {
                            if ($rowCmItem['sku'] == $valueInvoItem['sku']) {
                                $invoiceItemArray[$keyInvoItem]['qty'] = $invoiceItemArray[$keyInvoItem]['qty'] - $rowCmItem['qty_refunded'];
                            }

                            if ($invoiceItemArray[$keyInvoItem]['qty'] == 0) {
                                if (!in_array($keyInvoItem, $removeKey)) {
                                    $removeKey[] = $keyInvoItem;
                                }
                            }
                        }
                    }

                    # remove sku that has zero qty
                    if (count($removeKey) > 0) {
                        foreach ($removeKey as $valueKey) {
                            unset($invoiceItemArray[$valueKey]);
                        }
                    }
                }

                // Gosend Template
                $paramSource = ["variants"];
                foreach ($invoiceItemArray as $keyInvoItem => $valueInvoItem) {
                    $salesInvoiceItemLib->setFromArray($valueInvoItem);
                    if ($valueInvoItem['is_free_item'] == 1) {
                        $invoiceItemArray[$keyInvoItem]['sub_total'] = 0;
                    } else {
                        $invoiceItemArray[$keyInvoItem]['sub_total'] = number_format($salesInvoiceItemLib->getRowTotal(), 0, ',', '.');
                    }

                    $getProductElastics = $productLib->getDetailFromElastic($valueInvoItem['sku'], $paramSource);
                    $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                    $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;
                    $invoiceItemArray[$keyInvoItem]['image_name'] = $imageName;
                }

                $products = array_merge($products, array_values($invoiceItemArray));
            }

            $this->products = $products;
        }

        // Gosend Template
        $subTotal = $this->invoiceObj->getSubtotal() - $this->invoiceObj->getDiscountAmount();
        $shippingFee = $this->invoiceObj->getShippingAmount() - $this->invoiceObj->getShippingDiscountAmount();
        $giftCardAmount = $this->invoiceObj->getGiftCardsAmount();
        $grandTotal = $this->invoiceObj->getGrandTotal();

        $salesShipmentTrackingModel = new \Models\SalesShipmentTracking();
        $sql = "select status from sales_shipment_tracking where shipment_id = " . $this->shipmentObj->getShipmentId();
        $salesShipmentTrackingModel->useReadOnlyConnection();
        $result = $salesShipmentTrackingModel->getDi()->getShared($salesShipmentTrackingModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $statusTracking = $result->fetch();
        $liveTracking = "";
        if (!empty($statusTracking)) {
            $statusTrackingArray = json_decode($statusTracking['status'], true);
            foreach ($statusTrackingArray["manifest"] as $row) {
                if (!empty($row['live_tracking'])) {
                    $liveTracking = $row['live_tracking'];
                    break;
                }
            }
        }
        $salesruleOrderModel = new \Models\SalesruleOrder();
        $sql = "select voucher_code from salesrule_order where sales_order_id = " . $this->orderObj->getSalesOrderId();
        $salesruleOrderModel->useReadOnlyConnection();
        $result = $salesruleOrderModel->getDi()->getShared($salesruleOrderModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $voucherArray = $result->fetch();

        $storeNewRetail = array();
        $storeName = "";
        if (!empty($this->orderObj->getStoreCodeNewRetail()) && ((substr($this->orderObj->getStoreCodeNewRetail(), 0, 1) == "A" && substr($this->invoiceObj->getStoreCode(), 0, 1) == "A")) || ((substr($this->orderObj->getStoreCodeNewRetail(), 0, 1) == "H" && substr($this->invoiceObj->getStoreCode(), 0, 1) == "H")) || ((substr($this->orderObj->getStoreCodeNewRetail(), 0, 1) == "J" && substr($this->invoiceObj->getStoreCode(), 0, 1) == "J"))) {
            $sql = "SELECT name FROM store WHERE store_code = '" . $this->orderObj->getStoreCodeNewRetail() . "'";
            $storeNewRetail = $this->runReadSql($sql);
            if (!empty($storeNewRetail)) {
                $storeName = $storeNewRetail[0]['name'];
            }
        }

        $emailParam = array(
            "order_no" => $this->orderObj->getOrderNo(),
            "invoice_no" => $this->invoiceObj->getInvoiceNo(),
            "order_date" => $this->orderDate,
            "product" => $this->products,
            "shipment_date" => $this->shipmentDate,
            "shipment_seq" => $shipmentSeq,
            "shipment_seq_total" => $shipmentSeqTotal,
            "send_by" => $sendBy,
            "track_type" => 'No Resi',
            "tracking_number" => $this->shipmentObj->getTrackNumber(),
            "link_tracking_gosend" => $liveTracking,
            "flag_sap" => $flagSap,
            "flag_jne" => $flagJne,
            "link_status" => getenv("BASE_URL") . "order-detail?orderId=" . $this->orderObj->getOrderNo() . "&email=" . $this->orderObj->Customer->getEmail() . "&invoice=" . $this->invoiceObj->getInvoiceNo(),
            "link_review" => getenv("BASE_URL") . "my-account?tab=my-review-rating",
            "sub_total" => number_format($subTotal, 0, ',', '.'),
            "shipment_fee" => number_format($shippingFee, 0, ',', '.'),
            "voucher_code" => (!empty($voucherArray) ? $voucherArray['voucher_code'] : ""),
            "gifcard_amount" => number_format($giftCardAmount, 0, ',', '.'),
            "grand_total" => number_format($grandTotal, 0, ',', '.'),
            "receipt_date" => date('d F Y'),
            "store_code_new_retail" => $storeName
        );

        if (!empty($this->templateModel)) {
            $this->templateModel = array_merge($this->templateModel, $emailParam);
        } else {
            $this->templateModel = $emailParam;
        }
    }

    public function buildAceOtpParam($otp, $expiredDate)
    {
        $emailParam = array(
            "access_code" => $otp,
            "action" => 'pergantian nomor ponsel',
            "valid_until" => $expiredDate
        );

        $this->templateModel = $emailParam;
    }

    public function buildAceForgotPasskeyParam($memberCode, $accessCode)
    {
        $emailParam = array(
            "Member_code" => $memberCode,
            "access_code" => $accessCode,
        );

        $this->templateModel = $emailParam;
    }

    public function buildVerifyEmailParam($otpNumber)
    {
        $emailParam = array(
            "otp_number" => $otpNumber,
        );

        $this->templateModel = $emailParam;
    }

    private function runReadSql($sql = '')
    {
        $shipmentModel = new \Models\SalesShipment();
        $shipmentModel->useReadOnlyConnection();
        $result = $shipmentModel->getDi()->getShared($shipmentModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $resultArray = $result->fetchAll();

        return $resultArray;
    }

    public function buildCreditMemoParam($credit_memo_data = array())
    {
        $productList = array();
        $product = new \Models\Product();
        $paramSource = ["variants"];
        foreach ($credit_memo_data['invoices'] as $invoice) {
            foreach ($invoice['credit_memo_items'] as $item) {
                $getProductElastics = $product->getDetailFromElastic($item['sku'], $paramSource);
                $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;
                $productList[] = [
                    "sku" => $item['sku'],
                    "image_name" => $imageName,
                    "name" => $item['name'],
                    "qty" => $item['qty_refunded'],
                ];
            }
        }

        $this->products = $productList;
        $this->refund_as_cash = $credit_memo_data['refund_cash'];
        $this->refund_as_gift_card = $credit_memo_data['refund_as_gift_card'];
        $this->orderNo = $credit_memo_data['order_no'];
        $this->templateModel['order_date'] = date('j F Y H:i:s', strtotime($credit_memo_data['salesOrder']['created_at']));
    }

    /**
     * Set templateModel value and Merge everything
     */
    private function _generateTemplateModel()
    {
        if (!empty($this->subject)) {
            $this->templateModel['subject'] = $this->subject;
        }

        if (!empty($this->body_message)) {
            $this->templateModel['body_message'] = $this->body_message;
        }

        if (!empty($this->customerFirstName)) {
            $this->templateModel['firstname'] = $this->customerFirstName;
        }

        if (!empty($this->customerLastName)) {
            $this->templateModel['lastname'] = $this->customerLastName;
        }

        if (isset($this->customerFirstName) && isset($this->customerLastName)) {
            $this->templateModel['customer_name'] = $this->customerFirstName . ' ' . $this->customerLastName;
        }

        if (!empty($this->storeName)) {
            $this->templateModel['store_name'] = $this->storeName;
        }

        if (!empty($this->vaNumber)) {
            $this->templateModel['va_number'] = $this->vaNumber;
        }

        if (!empty($this->pickedupDate)) {
            $this->templateModel['picked_date'] = $this->pickedupDate;
        }

        if (!empty($this->pickupPoint)) {
            $this->templateModel['pickup_point'] = $this->pickupPoint;
        }

        if (!empty($this->dateLastPickup)) {
            $this->templateModel['date_last_pickup'] = $this->dateLastPickup;
        }

        if (!empty($this->orderNo)) {
            $this->templateModel['order_no'] = $this->orderNo;
        }

        if (!empty($this->products) && empty($this->templateModel['product'])) {
            $this->templateModel['product'] = $this->products;
        }

        if (!empty($this->invoiceNo)) {
            $this->templateModel['invoice_no'] = $this->invoiceNo;
        }

        if (!empty($this->trackingNo)) {
            $this->templateModel['tracking_number'] = $this->trackingNo;
        }

        if (!empty($this->courierName)) {
            $this->templateModel['courier_name'] = $this->courierName;
        }

        if (!empty($this->reorderUrl)) {
            $this->templateModel['reorder_url'] = $this->reorderUrl;
        }

        if (!empty($this->voucherCode)) {
            $this->templateModel['voucher_code'] = $this->voucherCode;
        }

        if (!empty($this->voucherList)) {
            $this->templateModel['voucher_list'] = $this->voucherList;
        }

        if (!empty($this->voucherValue)) {
            $this->templateModel['voucher_value'] = $this->voucherValue;
        }

        if (!empty($this->voucherExpired)) {
            $this->templateModel['voucher_expired'] = $this->voucherExpired;
        }

        if (!empty($this->urlForgotPassword)) {
            $this->templateModel['url_forgot_password'] = $this->urlForgotPassword;
        }

        if (!empty($this->refund_as_gift_card)) {
            $this->templateModel['refund_as_gift_card'] = $this->refund_as_gift_card;
        }

        if (!empty($this->refund_as_cash)) {
            $this->templateModel['refund_as_cash'] = $this->refund_as_cash;
        }

        if (!empty($this->shippingAddress)) {
            $this->templateModel = array_merge($this->templateModel, $this->shippingAddress);
        }

        if (!empty($this->linkDownload)) {
            $this->templateModel['link_download'] = $this->linkDownload;
        }

        if (!empty($this->linkReview)) {
            $this->templateModel['link_review'] = $this->linkReview;
        }

        if (!empty($this->pendingOrderList)) {
            $this->templateModel['pending_order_list'] = $this->pendingOrderList;
        }

        if (!empty($this->minimumTransaction)) {
            $this->templateModel['minimum_transaction'] = $this->minimumTransaction;
        }

        if (!empty($this->invoiceList)) {
            $this->templateModel['invoice_list'] = $this->invoiceList;
        }

        if (!empty($this->supplierName)) {
            $this->templateModel['supplier_name'] = $this->supplierName;
        }

        if (!empty($this->serviceAceServiceNumber)) {
            $this->templateModel['service_ace_service_number'] = $this->serviceAceServiceNumber;
        }

        if (!empty($this->serviceWarrantyCharge)) {
            $this->templateModel['service_warranty_charge'] = $this->serviceWarrantyCharge;
        }

        if (!empty($this->serviceWarrantyChargeLinkConfirmation)) {
            $this->templateModel['service_warranty_charge_link_confirmation'] = $this->serviceWarrantyChargeLinkConfirmation;
        }

        if (!empty($this->paymentMethod)) {
            $this->templateModel['payment_method'] = $this->paymentMethod;
        }

        if (!empty($this->qrcode_url)) {
            $this->templateModel['qrcode_url'] = $this->qrcode_url;
        }

        if(!empty($this->eta)){
            $this->templateModel['eta'] = $this->eta;
        }
    }

    /**
     * Function to set parameter for the email and send the email
     * @return bool
     */
    public function send()
    {
        $this->_generateTemplateModel();

        $to = $this->emailReceiver;
        if (getenv('ENVIRONMENT') != "production") {
            $to = getenv('EMAIL_RECEIVER_TESTING');
        }

        $message = array(
            "templateId" => $this->templateId,
            "to" => $to,
            "from" => $this->emailSender,
            "templateModel" => $this->templateModel,
            "companyCode" => $this->companyCode
        );

        if (!empty($this->emailCc)) {
            $message['cc'] = $this->emailCc;
            if (getenv('ENVIRONMENT') != "production") {
                $message['cc'] = getenv('EMAIL_RECEIVER_TESTING');
            }
        }

        if (!empty($this->emailTag)) {
            $message['tag'] = $this->emailTag;
        }

        if (!empty($this->attachments)) {
            $message['attachments'] = $this->attachments;
        }

        $nsq = new Nsq();
        if (!$nsq->publishCluster('email', $message)) {
            $this->message[] = "Sending email failed";
            return false;
        } else {
            return true;
        }
    }

    public function sendProductNotificationEmail()
    {
        $this->templateModel = array(); // make it empty first
        foreach ($this->invProductNotif as $invoice) {
            $this->products = $invoice['products'];
            $this->send();
        }
    }

    public function sendCleanCareNotificationEmail()
    {
        $this->templateModel = array(); // make it empty first
        foreach ($this->invCleanCareNotif as $invoice) {
            $this->invoiceNo = $invoice['invoice_no'];
            $this->products = $invoice['products'];
            $this->templateModel['grand_total'] = number_format($invoice['grand_total'], 0, ',', '.');
            $this->templateModel['order_date'] = $this->orderObj->getCreatedAtFormated('long');

            $this->send();
        }
    }

    public function sendMpNotificationEmail()
    {
        foreach ($this->invMpNotif as $invoice) {
            $this->templateModel = array(); // make it empty first
            $supplierUserModel = new \Models\SupplierUser();
            $userList = $supplierUserModel->find("supplier_id = " . $invoice['supplier_id'] . " AND status = 10 ");
            $supplierModel = new \Models\Supplier();
            $supplierData = $supplierModel->findFirst("supplier_id = " . $invoice['supplier_id'] . " AND status = 10 ");

            if (!empty($userList->count()) && !empty($supplierData)) {
                $supplierModel->setFromArray($supplierData->toArray());
                $this->generateAddress();
                $this->invoiceNo = $invoice['invoice_no'];
                $this->products = $invoice['products'];
                foreach ($userList as $sellerAdmin) {
                    if (!empty($sellerAdmin->getEmail())) {
                        $this->setEmailCc($_ENV['CC_TECH_EMAIL']);
                        $this->setEmailReceiver($sellerAdmin->getEmail(), $sellerAdmin->getFirstName());
                        $this->storeName = $supplierModel->getName();
                        $this->send();
                    }
                }
            }
        }
    }

    public function sendStoreReminderOrderEmail()
    {
        foreach ($this->invStoreReminder as $invoice) {
            $this->templateModel = array(); // make it empty first
            $pickupModel = new \Models\PickupPoint();
            $pickupData = $pickupModel->findFirst(
                array(
                    'conditions' => 'pickup_code = "' . $invoice['pickup_code'] . '"',
                    'columns' => 'pickup_name, email_pic'
                )
            );

            if (!empty($pickupData)) {
                $this->invoiceNo = $invoice['invoice_no'];
                $this->products = $invoice['products'];
                $this->storeName = $pickupData['pickup_name'];
                $this->templateModel['invoice_date'] = $invoice['invoice_date'];

                if (!empty($pickupData['email_pic'])) {
                    $this->setEmailReceiver($pickupData['email_pic'], $pickupData['pickup_name']);

                    $this->send();
                }
            }
        }
    }

    public function sendSupplierReviewNotificationEmail($params = array())
    {

        $supplierUserModel = new \Models\SupplierUser();
        $supplier = new \Models\Supplier();
        $variant = new \Models\Product();
        $customer = new \Models\Customer();
        $invoiceLib = new \Library\Invoice();

        foreach ($params as $keySupplierId => $supplerReview) {
            $this->templateModel = "";

            $supplier->setFromArray(["supplier_id" => $keySupplierId]);
            $supplierData = $supplier->getSupplierDetail();

            $userList = $supplierUserModel->find("supplier_id = " . $keySupplierId);

            $this->templateModel['store_name'] = $supplierData['name'];
            $notificationList = array();
            $ol = 1;
            foreach ($supplerReview as $supplierOrder) {
                $notificationList[] = $supplierOrder;
                $products = array();
                foreach ($supplierOrder['products'] as $listProduct) {

                    /*
                     * Get product detail
                     */
                    $productQuery = $variant->findFirst(
                        array(
                            'conditions' => 'product_id in (SELECT \Models\ProductVariant.product_id FROM \Models\ProductVariant WHERE \Models\ProductVariant.sku IN("' . $listProduct['sku'] . '"))',
                        )
                    );

                    $productDetail = $productQuery->toArray();
                    $products[] = array(
                        "sku" => $listProduct['sku'],
                        "product_name" => $productDetail['name'],
                        "product_quality" => $listProduct['product_quality'],
                        "product_accuracy" => $listProduct['product_accuration'],
                        "packaging_product" => $supplierOrder['packaging_product'],
                        "delivery_time" => $supplierOrder['delivery_time'],
                        "product_review" => $listProduct['review'],
                        "image_review" => ""
                    );
                }

                /*
                 * get data customer
                 */
                $customerQuery = $customer->findFirst(
                    array(
                        'conditions' => 'customer_id = ' . $supplierOrder['customer_id'],
                    )
                );
                $customerData = $customerQuery->toArray();

                /*
                 * Get invoice Detail
                 */
                $invoiceData = $invoiceLib->getInvoiceDetail(["invoice_no" => $supplierOrder['invoice_no']]);
                
                $this->templateModel['review_order_list'][] = array(
                    "no" => $ol,
                    "order_no" => !empty($invoiceData['order_no']) ? $invoiceData['order_no'] : "",
                    "invoice_no" => $supplierOrder['invoice_no'],
                    "invoice_date" => !empty($invoiceData['created_at']) ? $invoiceData['created_at'] : "",
                    "customer_name" => $customerData['first_name'] . " " . $customerData['first_name'],
                    "tracking_number" => !empty($invoiceData['shipment']['track_number']) ? $invoiceData['shipment']['track_number'] : "",
                    "product" => $products,
                );
                $ol++;
            }

            foreach ($userList as $sellerAdmin) {
                if (!empty($sellerAdmin->getEmail())) {
                    $this->setEmailReceiver($sellerAdmin->getEmail(), $sellerAdmin->getFirstName());
                    $this->storeName = $sellerAdmin->getFirstName();
                    $this->send();
                }
            }
        }
    }

    public function sendMpContentNotification($params)
    { // ga ada function yg manggil ini ??

        $supplier = new \Models\Supplier();
        $supplier->setFromArray(["supplier_id" => $params['supplier_id']]);
        $supplierData = $supplier->getSupplierDetail();

        $templateID = \Helpers\GeneralHelper::getTemplateId("mp_create_product_content", "ODI");
        if (!empty($templateID)) {
            $receiverName = ucfirst(str_replace("_", " ", $_ENV['EMAIL_CONTENT_NAME']));

            $params['products'] = array();
            $product = new \Models\Product();
            $paramSource = ["variants"];
            foreach ($params['variant'] as $key => $variant) {
                $getProductElastics = $product->getDetailFromElastic($key, $paramSource);
                $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                $imageName = "https://res.cloudinary.com/ruparupa-com/image/upload/w_170,h_170,f_auto" . $imageURL;
                $params['products'][] = array(
                    "image_name" => $imageName,
                    "name" => ucfirst($params['product_name']),
                    "sku" => $key,
                    "seller_name" => ucfirst($supplierData['name']),
                );
            }
            $this->templateModel['receiver_name']   = $receiverName;
            $this->templateModel['products']        = $params['products'];
            $this->setEmailSender($_ENV['EMAIL_FROM']);
            $this->setEmailReceiver($_ENV['EMAIL_MP_CREATE_PRODUCT_CONTENT'], $receiverName);
            $this->setTemplateId($templateID);
            $this->setCompanyCode("ODI");
            $this->send();
        }
    }

    public function sendCustomerReviewNotificationEmail($params = array())
    {

        $this->templateModel['customer_name']   = $params['customer_name'];
        $this->templateModel['url_review']      = $params['url_review'];
        $this->templateModel['products']        = $params['products'];

        if (!empty($params['customer_email'])) {
            $this->setEmailReceiver($params['customer_email'], $params['customer_name']);
            $this->send();
        }
    }

    public function sendDesignerNotif()
    {
        $this->templateModel = array(); // make it empty first
        foreach ($this->invDesignerNotif as $invoice) {
            $this->invoiceNo = $invoice['invoice_no'];
            $this->products = $invoice['products'];
            $this->templateModel['grand_total'] = number_format($invoice['grand_total'], 0, ',', '.');
            $this->templateModel['order_date'] = $this->orderObj->getCreatedAtFormated('long');

            $this->send();
        }
    }

    public function sendEmail($params = array())
    {
        $flagSendEmail = 0;
        if (isset($params['order_no'])) {
            $salesOrder = new \Models\SalesOrder();
            $resultOrder = $salesOrder->findFirst("order_no = '" . $params['order_no'] . "'");

            if ($resultOrder) {
                $this->setOrderObj($resultOrder);
                $flagSendEmail++;
            }
        }

        if (isset($params['invoice_id'])) {
            $salesInvoice = new \Models\SalesInvoice();
            $resultInvoice = $salesInvoice->findFirst("invoice_id = " . $params['invoice_id']);

            if ($resultInvoice) {
                $this->setInvoiceObj($resultInvoice);
                $flagSendEmail++;
            }
        }

        //send basic email
        if (isset($params['basic_email'])) {
            $flagSendEmail++;
        }

        if ($flagSendEmail) {
            if (!empty($this->orderObj)) {
                if (isset($params['use_basic_order']))
                    $this->buildOrderEmailParam();

                if (isset($params['use_address']))
                    $this->generateAddress();
            }

            if (!empty($this->invoiceObj)) {
                // @todo function buildShippingItems will be overwritten with function buildProductList in buildShippingEmailParam
                $this->buildShippingItems();
                $this->buildShippingEmailParam();
            }

            if (isset($params['sender_email'])) {
                $this->setEmailSender($params['sender_email']);
            } else {
                $this->setEmailSender();
            }

            if (isset($params['first_name']) && isset($params['last_name'])) {
                $this->setName($params['last_name'], $params['first_name']);
            } else {
                $this->setName();
            }

            if (isset($params['customer_email'])) {
                $this->setEmailReceiver($params['customer_email']);
            } else {
                $this->setEmailReceiver();
            }

            if (isset($params['cc'])) {
                $this->setEmailCc($params['cc']);
            }

            if (isset($params['subject'])) {
                $this->setSubject($params['subject']);
            }

            if (isset($params['company_code'])) {
                $this->setCompanyCode($params['company_code']);
            }

            if (isset($params['email_tag'])) {
                $this->setEmailTag($params['email_tag']);
            }

            if (isset($params['voucher_code'])) {
                $this->setVoucherCode($params['voucher_code']);
            }

            if (isset($params['courier_name'])) {
                $this->setCourierName($params['courier_name']);
            }

            if (isset($params['link_download'])) {
                $this->setLinkDownload($params['link_download']);
            }

            if (isset($params['expiration_date'])) {
                $voucherExpired = date('j F Y', strtotime($params['expiration_date']));
                $this->setVoucherExpired($voucherExpired);
            }

            if (isset($params['voucher_value'])) {
                $this->setVoucherValue($params['voucher_value']);
            }

            if (isset($params['invoice_list'])) {
                if (!empty($params['invoice_list'])) {
                    $this->setInvoiceList(json_decode($params['invoice_list'], true));
                }
            }

            if (isset($params['attachments'])) {
                if (!empty($params['attachments'])) {
                    $this->setAttachments(json_decode($params['attachments'], true));
                }
            }

            $this->setTemplateId($params['template']);
            $this->send();
        } else {
            $this->errorCode = "404";
            $this->errorMessages = "Mail not send, Order or Invoice not found";
        }
    }

    public function buildMpPendingReceivedParam()
    {
        // {
        //     "supplier_name": "supplier_name_Value",
        //     "customer_name": "customer_name_Value",
        //     "order_no": "order_no_Value",
        //     "invoice_no": "invoice_no_Value",
        //     "product": [
        //         {
        //             "name": "name_Value",
        //             "sku": "sku_Value",
        //             "sku_seller": "sku_seller_Value",
        //             "qty": "qty_Value"
        //         }
        //     ]
        // }

        # supplier name
        $sql = "SELECT name FROM supplier WHERE supplier_alias = '{$this->invoiceObj->getSupplierAlias()}' LIMIT 1;";
        $supplierArr = $this->runReadSql($sql);
        if (count($supplierArr) > 0) {
            $this->setSupplierName($supplierArr[0]['name']);
        }

        # get invoice item
        $sql = "SELECT sku, name, qty_ordered as qty FROM sales_invoice_item WHERE invoice_id = {$this->invoiceObj->getInvoiceId()};";
        $invoiceItemArray = $this->runReadSql($sql);

        # get credit memo item
        $sql = "SELECT sku, qty_refunded FROM sales_credit_memo_item WHERE invoice_no = '{$this->invoiceObj->getInvoiceNo()}' AND qty_refunded > 0;";
        $creditMemoItemArray = $this->runReadSql($sql);
        if (count($creditMemoItemArray) > 0) {
            # subtract invoice item with credit memo item
            $removeKey = array();
            foreach ($creditMemoItemArray as $rowCmItem) {
                foreach ($invoiceItemArray as $keyInvoItem => $valueInvoItem) {
                    if ($rowCmItem['sku'] == $valueInvoItem['sku']) {
                        $invoiceItemArray[$keyInvoItem]['qty'] = $invoiceItemArray[$keyInvoItem]['qty'] - $rowCmItem['qty_refunded'];
                    }

                    if ($invoiceItemArray[$keyInvoItem]['qty'] == 0) {
                        if (!in_array($keyInvoItem, $removeKey)) {
                            $removeKey[] = $keyInvoItem;
                        }
                    }
                }
            }

            # remove sku that has zero qty
            if (count($removeKey) > 0) {
                foreach ($removeKey as $valueKey) {
                    unset($invoiceItemArray[$valueKey]);
                }
            }
        }

        $products = array();
        $productModel = new \Models\Product();
        $paramSource = ["variants"];
        foreach ($invoiceItemArray as $keyInvoItem => $valueInvoItem) {
            $getProductElastics = $productModel->getDetailFromElastic($valueInvoItem['sku'], $paramSource);
            $invoiceItemArray[$keyInvoItem]['sku_seller'] = isset($getProductElastics[0]['_source']['variants'][0]['sku_seller']) ? $getProductElastics[0]['_source']['variants'][0]['sku_seller'] : '';
        }

        $products = array_merge($products, array_values($invoiceItemArray));
        $this->products = $products;
        $this->templateModel['product'] = $this->products;
    }

    public function getEmailTemplateID($params = array())
    {
        $masterEmailTemplateModel = new \Models\MasterEmailTemplate();
        if (isset($params['company_code'])) {
            $params['company_code'] = strtoupper($params['company_code']);
        }

        $sql = '';
        if (isset($params['template_code']) && isset($params['company_code'])) {
            $sql = "template_code = '{$params['template_code']}' AND company_code = '{$params['company_code']}'";
        } else if (isset($params['template_code'])) {
            $sql = "template_code = '{$params['template_code']}'";
        } else if (isset($params['company_code'])) {
            $sql = "company_code = '{$params['company_code']}'";
        }

        $templateList = $masterEmailTemplateModel->find($sql);
        if (count($templateList) > 0) {
            return $templateList->toArray();
        } else {
            $this->errorCode = "202";
            $this->errorMessages = "Template ID not found";
        }
    }

    public function getCustomerFirstName()
    {
        return $this->customerFirstName;
    }

    public function getEmailReceiver()
    {
        return $this->orderObj->SalesCustomer->getCustomerEmail();;
    }

}
