<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/22/2017
 * Time: 2:17 PM
 */

namespace Library;

use DateTime;
use Phalcon\Db;
class Shipping
{
    protected $invoice_no;
    protected $order_no;
    protected $carrier_id;

    protected $errorCode;
    protected $errorMessages;

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getInvoiceNo()
    {
        return $this->invoice_no;
    }

    /**
     * @param mixed $invoice_no
     */
    public function setInvoiceNo($invoice_no)
    {
        $this->invoice_no = $invoice_no;
    }

    /**
     * @return mixed
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * @param mixed $order_no
     */
    public function setOrderNo($order_no)
    {
        $this->order_no = $order_no;
    }

    /**
     * @return mixed
     */
    public function getCarrierId()
    {
        return $this->carrier_id;
    }

    /**
     * @param mixed $carrier_id
     */
    public function setCarrierId($carrier_id)
    {
        $this->carrier_id = $carrier_id;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function generateAwb()
    {
        if(!empty($this->order_no)) {
            $salesOrderLib = new \Library\SalesOrder();
            $salesOrderData = $salesOrderLib->getSalesOrderDetail(['order_no' => $this->order_no]);
            if($salesOrderData) {
                // If client passing order number, there are possibility to have more than one invoice
                foreach($salesOrderData['invoices'] as $invoice) {
                    if($invoice['delivery_method'] == "delivery") {
                        // Direct call generate awb only for DC fulfillment
                        if (isset($invoice['store']['store_code']) && substr( $invoice['store']['store_code'], 0, 2 ) === "DC") {
                            $invoice['customer'] = $salesOrderData['customer'];
                            $this->validateAndGenerateAWB($invoice);
                        }
                    } else if (isset($invoice['shipment']['carrier'])) {
                        // shopee3pl cashless
                        $cashlessCarrier = array("17","18","19","20","21","43");
                        if (in_array($invoice['shipment']['carrier']['carrier_id'],$cashlessCarrier)) {
                            $this->validateAndGenerateAWB($invoice);
                        }
                        
                    }
                }

                return;
            }
        }

       if(!empty($this->invoice_no))
        {
            $salesOrderLib = new \Library\Invoice();
            $invoiceData = $salesOrderLib->getInvoiceDetail(['invoice_no' => $this->invoice_no]);
            if($invoiceData) {
                return $this->validateAndGenerateAWB($invoiceData, $this->carrier_id);
            }
        }

        if(empty($salesOrderData)) {
            $this->errorCode = "RR302";
            $this->errorMessages[] = "Data not found";
            return;
        }
    }

    public function validateAndGenerateAWB($invoice = array(), $carrier_id = '')
    {
        $awb = "";
        if (empty($carrier_id)) {
            if(empty($invoice['shipment']['carrier']['carrier_id'])) {
                return;
            }
            $carrier_id = $invoice['shipment']['carrier']['carrier_id'];
        }

        $direct_delivery_information = $this->getDirectDeliveryInformation($invoice['invoice_id']);

        if (isset($direct_delivery_information)) {
            if (isset($direct_delivery_information['use_direct_delivery']) && $direct_delivery_information['use_direct_delivery'] == 'true') {
                if (isset($direct_delivery_information['delivery_id']) && strtolower($direct_delivery_information['delivery_id']) == 'ownfleet') {
                    return "";
                }
            }
        }

        // only generate awb for delivery goods
        if($invoice['delivery_method'] == 'delivery' || $invoice['delivery_method'] == 'store_fulfillment') {
            switch($carrier_id)
            {
                case getenv("SAP_CARRIER_ID") : 
                    $awb = $this->_generateDataForSAPExpAWB($invoice);
                    break;
                case getenv("JNE_CARRIER_ID") : 
                    $awb = $this->_generateDataForJNEAWB($invoice, $carrier_id, $direct_delivery_information);
                    break;
                case getenv("JNEJOB_CARRIER_ID") : 
                    $awb = $this->_generateDataForJNEJOB($invoice, $carrier_id);
                    break;
                case getenv("JTR_CARRIER_ID") : 
                    $awb = $this->_generateDataForJNEAWB($invoice, $carrier_id, $direct_delivery_information);
                    break;
                case getenv("NINJAVAN_CARRIER_ID") : 
                    $awb = $this->_generateDataForNINJAVANAWB($invoice);
                    break;
                case getenv("NCS_CARRIER_ID") : 
                    $awb = $this->_generateDataForNCSAWB($invoice);
                    break;
                default: continue; break; // skip proeses for other
            }
        }

        if (isset($direct_delivery_information)) {
            if (isset($direct_delivery_information['use_direct_delivery']) && $direct_delivery_information['use_direct_delivery'] == 'true') {
                $this->_insertDirectDeliveryDetail($invoice['invoice_no'], $awb);
            }
        }

        return $awb;
    }

    /**
     * Generate AWB for SAP Express by calling it's API and save it to shipment
     * @param array $invoice
     * @throws HTTPException
     */
    private function _generateDataForSAPExpAWB($invoice = array())
    {
        if ($invoice['delivery_method'] == 'store_fulfillment') {
            $salesShipmentModel = new \Models\SalesShipment();

            $paramsGet['sales_order_id'] = $invoice['sales_order_id'];
            $paramsGet['pickup_code'] = $invoice['pickup_code']['pickup_code'];
            $qty = $salesShipmentModel->getQtyOrderedStoreFulfillment($paramsGet);
            $weight = $salesShipmentModel->getWeightOrderedStoreFulfillment($paramsGet);
        } else {
            $qty = (int)$invoice['qty_ordered'];
            $weight = $invoice['shipment']['total_weight'];
        }

        if ($qty == 0) $qty = 1;
        if ($weight == 0.00) $weight = 1.00;

        if ($invoice['shipment']['shipping_address']['phone'] == "0" || $invoice['shipment']['shipping_address']['phone'] == NULL) {
            $salesShipmentModelHp = new \Models\SalesShipment();
            $paramsGetHp['sales_order_id'] = $invoice['sales_order_id'];
            $recipient_handphone_number = $salesShipmentModelHp->getRecipientHandphone($paramsGetHp);
        } else {
            $recipient_handphone_number = $invoice['shipment']['shipping_address']['phone'];
        }

        $recipient_address = ($invoice['shipment']['shipping_address']['full_address'] == NULL) ? '-' : substr($invoice['shipment']['shipping_address']['full_address'], 0, 58);

        $params = [
            'shipment_date_string' => date("dmY"),
            'shipment_time_string' => date("H:i"),
            'unique_reference_no' => $invoice['invoice_no'],
            'city_origin_name' => $invoice['store']['city']['city_name'],
            'city_destination_name' =>  $invoice['shipment']['shipping_address']['city']['city_name'],
            'quantity' => (int)$qty,
            'weight' => $weight,
            'goods_value_are' => 1,
            'insurance_flag' => "0",
            'cod_shipment_cost_flag' => 0,
            'cod_goods_cost_flag' => 0,
            'goods_is_document_flag' => 0,
            'goods_need_packing_flag' => 0,
            'goods_is_boxed_flag' => 0,
            'goods_is_high_values_flag' => 0,
            'goods_is_electronic_flag' => 0,
            'goods_is_dangerous_flag' => 0,
            'goods_is_live_animal_flag' => 0,
            'goods_is_live_plant_flag' => 0,
            'goods_is_food_flag' => 0,
            'goods_textile_flag' => 0,
            'shipment_req_pickup_flag' => 0,
            'unique_shipment_no' => NULL,
            'shipper_province_name' => $invoice['store']['province']['province_name'],
            'shipper_district_name' => $invoice['store']['city']['city_name'],
            'shipper_subdistrict_code' => $invoice['store']['kecamatan']['sap_exp_code'],
            'shipper_subdistrict_name' => $invoice['store']['kecamatan']['kecamatan_name'],
            'shipper_address' => substr($invoice['store']['address'], 0, 58),
            'recipient_name' => $invoice['shipment']['shipping_address']['first_name'] . " " .$invoice['shipment']['shipping_address']['last_name'],
            'recipient_province_name' => $invoice['shipment']['shipping_address']['province']['province_name'],
            'recipient_district_name' => $invoice['shipment']['shipping_address']['city']['city_name'],
            'recipient_subdistrict_code' => $invoice['shipment']['shipping_address']['kecamatan']['sap_exp_code'],
            'recipient_subdistrict_name' => $invoice['shipment']['shipping_address']['kecamatan']['kecamatan_name'],
            'recipient_address' => $recipient_address,
            'recipient_handphone_number' => $recipient_handphone_number
        ];

        $shippingAssignation = empty($invoice['pickup_code']) ? $invoice['store']['store_code'] : $invoice['pickup_code']['pickup_code'];

        $courierCredentialModel = new \Models\ShippingCourierCredential();
        $courierData = $courierCredentialModel->getFromCache(['shipping_assignation' => $shippingAssignation . '_' . $invoice['company_code']]);

        // If not found then send it from DC
        if(empty($courierData)) {
            $courierData = $courierCredentialModel->getFromCache(['shipping_assignation' => $invoice['store']['store_code']]);
        }

        $params['allocation_code'] = $courierData['allocation_code'];
        $params['company_id'] = $courierData['company_id'];
        $sapUsername = $courierData['username'];
        $sapPassword = $courierData['password'];

        \Helpers\LogHelper::log("shipping_awb","params sap exp: " . json_encode($params));

        $sapExpress = new \Library\Shipping\SAPExpress();
        $sapExpress->setFromArray($params);
        $awbNumber = $sapExpress->generateAWB($sapUsername, $sapPassword);
        if(!empty($awbNumber)) {
            $shipmentData = [
                "shipment_id" => $invoice['shipment']['shipment_id'],
                "carrier_id" => getenv("SAP_CARRIER_ID"),
                "shipment_type" => "3pl",
                "track_number" => $awbNumber
            ];
            $shipmentModel = new \Models\SalesShipment();
            $shipmentModel->setFromArray($shipmentData);
            $shipmentModel->saveData("shipment");
        }

        return $awbNumber;
    }

    private function _getJNECustomerIdByAlias($alias)
    {
        switch ($alias) {
            case "AHI":
                return getenv('JNE_CUST_ID_AHI');
                break;
            case "HCI":
                return getenv('JNE_CUST_ID_HCI');
                break;
            case "FBI":
                return getenv('JNE_CUST_ID_FBI');
                break;
            case "KLV":
                return getenv('JNE_CUST_ID_KLV');
                break;
            case "KLS":
                return getenv('JNE_CUST_ID_KLS');
                break;
            case "KWI":
                return getenv('JNE_CUST_ID_KWI');
                break;
            case "TGI":
                return getenv('JNE_CUST_ID_TGI');
                break;
            case "GSP":
                return getenv('JNE_CUST_ID_GSP');
                break;
            default:
                return getenv('JNE_CUST_ID');
                break;
        }
    }

    private function _insertDirectDeliveryDetail($invoice_no, $awb)
    {
        $param = array();
        $param['invoice_no'] = $invoice_no;
        $param['awb'] = $awb;

        $apiWrapper = new APIWrapper(getenv('ORDER_API_V2'));
        $apiWrapper->setEndPoint("sales/invoice/dc_delivery/direct_delivery_detail");
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to insert direct delivery detail";            
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("direct_delivery_detail", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
        return true;
    }

    private function _generateDataForJNEAWB($invoice = array(), $carrier_id = 2, $direct_delivery_information = array())
    {
        $qty = (int)$invoice['qty_ordered'];
        $weight = $invoice['shipment']['total_weight'];

        if ($qty == 0) $qty = 1;
        if ($weight == 0.00) $weight = 1.00;

        if ($invoice['shipment']['shipping_address']['phone'] == "0" || $invoice['shipment']['shipping_address']['phone'] == NULL) {
            $salesShipmentModelHp = new \Models\SalesShipment();
            $paramsGetHp['sales_order_id'] = $invoice['sales_order_id'];
            $recipient_handphone_number = $salesShipmentModelHp->getRecipientHandphone($paramsGetHp);
        } else {
            $recipient_handphone_number = $invoice['shipment']['shipping_address']['phone'];
        }

        $recipient_address = ($invoice['shipment']['shipping_address']['full_address'] == NULL) ? '-' : $invoice['shipment']['shipping_address']['full_address'];
        $recipient_address = trim(preg_replace("/\s+/", " ",$recipient_address));
        
        $recipient_address1 = '-';
        $recipient_address2 = $recipient_address3 = '';
        if ($recipient_address != '-') {
            $recipient_address1 = substr($recipient_address, 0, 28);

            if (strlen($recipient_address) > 28) {
                $recipient_address2 = substr($recipient_address, 28, 28);
            }

            if (strlen($recipient_address) > 56) {
                $recipient_address3 = substr($recipient_address, 56, 28);
            }
        }
        $destKecamatanID = $invoice['shipment']['shipping_address']['kecamatan']['kecamatan_id'];

        $shipperCity = $invoice['store']['city']['city_name'];
        $shipperRegion = $invoice['store']['province']['province_name'];
        $shipperPhone = $invoice['store']['phone'];

        $custID = $this->_getJNECustomerIdByAlias($invoice['supplier']['supplier_alias']);

        $storeCoding = isset($invoice['store']['kecamatan']['coding']) ? $invoice['store']['kecamatan']['coding'] : '';
        $originKecamatanID = $invoice['store']['kecamatan']['kecamatan_id'];
        
        $shipper_address = ($invoice['store']['address'] == NULL) ? '-' : $invoice['store']['address'];
        if (isset($direct_delivery_information)) {
            if (isset($direct_delivery_information['use_direct_delivery']) && $direct_delivery_information['use_direct_delivery'] == 'true') {
                $shipper_address = ($direct_delivery_information['store']['address'] == NULL) ? '-' : $direct_delivery_information['store']['address'];
                $shipperCity = $direct_delivery_information['store']['city']['city_name'];
                $shipperRegion = $direct_delivery_information['store']['province']['province_name'];
                $shipperPhone = $direct_delivery_information['store']['phone'];
                $storeCoding = $direct_delivery_information['store']['kecamatan']['coding'];
                $originKecamatanID = $direct_delivery_information['store']['kecamatan']['kecamatan_id'];
                $custID = $this->_getJNECustomerIdByAlias($direct_delivery_information['supplier']['supplier_alias']);
            }
        }

        $shipper_address = trim(preg_replace("/\s+/", " ",$shipper_address)); 

        $shipper_address1 = '-';
        $shipper_address2 = $shipper_address3 = '';
        if ($shipper_address != '-') {
            $shipper_address1 = substr($shipper_address, 0, 28);

            if (strlen($shipper_address) > 28) {
                $shipper_address2 = substr($shipper_address, 28, 28);
            }

            if (strlen($shipper_address) > 56) {
                $shipper_address3 = substr($shipper_address, 56, 28);
            }
        }
        
        if ($storeCoding == 'TGR10123') { // Qbig, LW udah TGR10000
            $storeCoding = 'TGR10000';
        } elseif ($storeCoding == 'TGR10052') { // karawaci
            $storeCoding = 'TGR10000';
        } elseif ($storeCoding == 'BKI10100') { // DC
            $storeCoding = 'BKI10000';
        }

        $branch = substr($storeCoding,0,3) . '000';
        if(getenv('ENVIRONMENT') != "production") { 
            $branch = 'CGK000'; 
        } 

        $username = getenv('JNE_USERNAME');
        $apiKey = getenv('JNE_API_KEY');
        $supplierName = $this->getSupplierNameByInvoiceNo($invoice['invoice_no']);

        $params = [
            'username' => $username,
            'api_key' => $apiKey,
            'OLSHOP_SHIPPER_NAME' => $supplierName,
            'OLSHOP_CUST' => $custID,
            'OLSHOP_BRANCH' => $branch,
            'OLSHOP_ORIG' => $storeCoding,
            'OLSHOP_ORDERID' => $invoice['invoice_no'],
            'OLSHOP_SHIPPER_ADDR1' => $shipper_address1,
            'OLSHOP_SHIPPER_ADDR2' => $shipper_address2,
            'OLSHOP_SHIPPER_ADDR3' => $shipper_address3,
            'OLSHOP_SHIPPER_CITY' => $shipperCity,
            'OLSHOP_SHIPPER_REGION' => $shipperRegion,
            'OLSHOP_SHIPPER_ZIP' => '-',
            'OLSHOP_SHIPPER_PHONE' => $shipperPhone,
            'OLSHOP_RECEIVER_NAME' => $invoice['shipment']['shipping_address']['first_name'] . " " .$invoice['shipment']['shipping_address']['last_name'],
            'OLSHOP_RECEIVER_ADDR1' => $recipient_address1,
            'OLSHOP_RECEIVER_ADDR2' => $recipient_address2,
            'OLSHOP_RECEIVER_ADDR3' => $recipient_address3,
            'OLSHOP_RECEIVER_CITY' => $invoice['shipment']['shipping_address']['city']['city_name'],
            'OLSHOP_RECEIVER_REGION' => $invoice['shipment']['shipping_address']['province']['province_name'],
            'OLSHOP_RECEIVER_ZIP' => $invoice['shipment']['shipping_address']['post_code'],
            'OLSHOP_RECEIVER_PHONE' => $recipient_handphone_number,
            'OLSHOP_DEST' => $invoice['shipment']['shipping_address']['kecamatan']['coding'],
            'OLSHOP_SERVICE' => ($carrier_id == getenv('JTR_CARRIER_ID')) ? 'JTR':'REG',
            'OLSHOP_QTY' => (int)$qty,
            'OLSHOP_WEIGHT' => (int)$weight,
            'OLSHOP_GOODSTYPE' => '2',
            'OLSHOP_GOODSDESC' => 'Barang keperluan Rumah Tangga',
            'OLSHOP_INST' => 'Instruction',
            'OLSHOP_GOODSVALUE' => $invoice['subtotal'],
            'OLSHOP_INS_FLAG' => 'N',
            'OLSHOP_COD_FLAG' => 'N',
            'OLSHOP_COD_AMOUNT' => 0
        ];

        \Helpers\LogHelper::log("shipping_awb","params jne: " . json_encode($params),"info");

        $jneLib = new \Library\Shipping\JNE();
        $jneLib->setFromArray($params);
        $awbNumber = $jneLib->generateAWB();
        // Mock AWB
        if (!empty(strtolower(getenv("ENVIRONMENT"))) && strtolower(getenv("ENVIRONMENT")) != "production") {
            $awbNumber = sprintf("awbstg%d", rand(10,10000));
        }
        if(!empty($awbNumber)) {
            // insert awb to our account
            if(getenv('ENVIRONMENT') == "production") { 
                $jneLib->insertAWB($invoice['invoice_no'], $awbNumber); 
            } 
            
            // Calculate eta
            $eta = null;
            $usedLeadTimeMax = !empty($invoice["shipment"]["lead_time_max"]) ? (int) $invoice["shipment"]["lead_time_max"] : 0;

            // Fallback try get lead time from go-shipment-v2
            if ($usedLeadTimeMax <= 0) {
                $getLeadTimeParams = array(
                    "origin_kecamatan_id" => $originKecamatanID,
                    "dest_kecamatan_id" => $destKecamatanID,
                    "carrier_id" => $carrier_id
                );
                $leadTime = $jneLib->getShippingLeadTime($getLeadTimeParams);
                if (!empty($leadTime)) {
                    $usedLeadTimeMax = (int) $leadTime["max"];
                }
            }

            if ($usedLeadTimeMax > 0) {
                $etaRaw = date_add(new DateTime("now"), date_interval_create_from_date_string(sprintf("%d days", $usedLeadTimeMax)));
                $eta = $etaRaw->format('Y-m-d H:i:s');
            }
            
            $shipmentData = [
                "shipment_id" => $invoice['shipment']['shipment_id'],
                "carrier_id" => $carrier_id,
                "shipment_type" => "3pl",
                "track_number" => $awbNumber,
                "eta" => $eta
            ];
            $shipmentModel = new \Models\SalesShipment();
            $shipmentModel->setFromArray($shipmentData);
            $shipmentModel->saveData("shipment");
        }

        return $awbNumber;
    }    

    private function _generateDataForJNEJOB($invoice = array(), $carrierID)
    {
        if ($invoice['delivery_method'] == 'store_fulfillment') {
            $salesShipmentModel = new \Models\SalesShipment();

            $paramsGet['sales_order_id'] = $invoice['sales_order_id'];
            $paramsGet['pickup_code'] = $invoice['pickup_code']['pickup_code'];
            $qty = $salesShipmentModel->getQtyOrderedStoreFulfillment($paramsGet);
            $weight = $salesShipmentModel->getWeightOrderedStoreFulfillment($paramsGet);
        } else {
            $qty = (int)$invoice['qty_ordered'];
            $weight = $invoice['shipment']['total_weight'];
        }

        $qty = ($qty == 0) ? (string)1 : (string)$qty;
        $weight = ($weight == 0.00) ? 1.00 : $weight;

        if ($invoice['shipment']['shipping_address']['phone'] == "0" || $invoice['shipment']['shipping_address']['phone'] == NULL) {
            $salesShipmentModelHp = new \Models\SalesShipment();
            $paramsGetHp['sales_order_id'] = $invoice['sales_order_id'];
            $recipient_handphone_number = str_replace(" ", "", $salesShipmentModelHp->getRecipientHandphone($paramsGetHp));
        } else {
            $recipient_handphone_number = str_replace(" ", "", $invoice['shipment']['shipping_address']['phone']);
        }

        $recipient_handphone_number = substr($recipient_handphone_number, 0, 15);

        $recipient_address = ($invoice['shipment']['shipping_address']['full_address'] == NULL) ? '-' : $invoice['shipment']['shipping_address']['full_address'];
        $recipient_address = $this->sanitizeAddress($recipient_address);

        $recipient_address1 = '-';
        $recipient_address2 = $recipient_address3 = '';
        if ($recipient_address != '-') {
            $recipient_address1 = substr($recipient_address, 0, 30);

            if (strlen($recipient_address) > 30) {
                $recipient_address2 = substr($recipient_address, 30, 30);
            }

            if (strlen($recipient_address) > 60) {
                $recipient_address3 = substr($recipient_address, 60, 30);
            }
        }
        $destKecamatanID = $invoice['shipment']['shipping_address']['kecamatan']['kecamatan_id'];

        $shipper_address = ($invoice['store']['address'] == NULL) ? '-' : $invoice['store']['address'];
        $shipper_address = $this->sanitizeAddress($shipper_address);

        $shipper_address1 = '-';
        $shipper_address2 = $shipper_address3 = '';
        if ($shipper_address != '-') {
            $shipper_address1 = substr($shipper_address, 0, 30);

            if (strlen($shipper_address) > 30) {
                $shipper_address2 = substr($shipper_address, 30, 30);
            }

            if (strlen($shipper_address) > 60) {
                $shipper_address3 = substr($shipper_address, 60, 30);
            }
        }

        if (isset($invoice['store']['kecamatan']['coding'])) {
            if ($invoice['store']['kecamatan']['coding'] == 'CGK10100') { // jakarta
                $invoice['store']['kecamatan']['coding'] = 'CGK10000';
            } elseif ($invoice['store']['kecamatan']['coding'] == 'TGR10123') { // Qbig, LW udah TGR10000
                $invoice['store']['kecamatan']['coding'] = 'TGR10000';
            } elseif ($invoice['store']['kecamatan']['coding'] == 'TGR10052') { // karawaci
                $invoice['store']['kecamatan']['coding'] = 'TGR10000';
            }
        } else {
            $invoice['store']['kecamatan']['coding'] = '';
        }

        $originKecamatanID = $invoice['store']['kecamatan']['kecamatan_id'];

        $invoice['store']['phone'] = str_replace(" ", "", $invoice['store']['phone']);
        $provinceName = (isset($invoice['shipment']['shipping_address']['province']['province_name'])) ? $invoice['shipment']['shipping_address']['province']['province_name'] : '-';
        $kecamatanName = (isset($invoice['shipment']['shipping_address']['kecamatan']['kecamatan_name'])) ? $invoice['shipment']['shipping_address']['kecamatan']['kecamatan_name'] : '-';
        $cityName = (isset($invoice['shipment']['shipping_address']['city']['city_name'])) ? $invoice['shipment']['shipping_address']['city']['city_name'] : '-';
        $kecamatanCoding = (isset($invoice['shipment']['shipping_address']['kecamatan']['coding'])) ? $invoice['shipment']['shipping_address']['kecamatan']['coding'] : '-';
        $postCode = (isset($invoice['shipment']['shipping_address']['post_code'])) ? $invoice['shipment']['shipping_address']['post_code'] : '-';
        $params = [
            'INVOICE_NO' => $invoice['invoice_no'],
            'SHIPPER_NAME' => 'RUPARUPA',
            'SHIPPER_ADDR1' => $shipper_address1,
            'SHIPPER_CITY' => substr($invoice['store']['city']['city_name'], 0, 20),
            'SHIPPER_ZIP' => '-',
            'SHIPPER_REGION' => substr($invoice['store']['province']['province_name'], 0, 20),
            'SHIPPER_COUNTRY' => 'INDONESIA',
            'SHIPPER_CONTACT' => substr($invoice['store']['phone'], 0, 15),
            'SHIPPER_PHONE' => substr($invoice['store']['phone'], 0, 15),
            'RECEIVER_NAME' => substr($invoice['shipment']['shipping_address']['first_name'], 0, 15) . " " .substr($invoice['shipment']['shipping_address']['last_name'],0,13),
            'RECEIVER_ADDR1' => $recipient_address1,
            'RECEIVER_CITY' => substr($cityName, 0, 20),
            'RECEIVER_ZIP' => substr($postCode, 0, 5),
            'RECEIVER_REGION' => substr($provinceName, 0, 20),
            'RECEIVER_COUNTRY' => 'INDONESIA',
            'RECEIVER_CONTACT' => $recipient_handphone_number,
            'RECEIVER_PHONE' => $recipient_handphone_number,
            'ORIGIN_DESC' => substr($invoice['store']['kecamatan']['kecamatan_name'], 0, 98),
            'ORIGIN_CODE' => substr($invoice['store']['kecamatan']['coding'], 0, 10),
            'DESTINATION_DESC' => substr($kecamatanName, 0, 10),
            'DESTINATION_CODE' => substr($kecamatanCoding, 0, 10),
            'SERVICE_CODE' => $carrierID == getenv("JTR_CARRIER_ID") ? "JTR" : 'REG',
            'WEIGHT' => $weight,
            'QTY' => $qty,
            'GOODS_DESC' => 'Barang keperluan Rumah Tangga',
            'GOODS_AMOUNT' => $invoice['subtotal'],
            'INSURANCE_FLAG' => '',
            'INSURANCE_AMOUNT' => '',
            'DELIVERY_PRICE' => '1000',
            'SHIPPER_ADDR2' => $shipper_address2,
            'SHIPPER_ADDR3' => $shipper_address3,
            'RECEIVER_ADDR2' => $recipient_address2,
            'RECEIVER_ADDR3' => $recipient_address3
        ];

        \Helpers\LogHelper::log("shipping_jne_job","[REQUEST] params jne job: " . json_encode($params));

        $jneLib = new \Library\Shipping\JNE();
        $jobNumber = $jneLib->generateJOB($params);
        if(!empty($jobNumber)) {
            // Calculate eta
            $eta = null;

            $usedLeadTimeMax = !empty($invoice["shipment"]["lead_time_max"]) ? (int) $invoice["shipment"]["lead_time_max"] : 0;

            // Fallback try get lead time from go-shipment-v2
            if ($usedLeadTimeMax <= 0) {
                $getLeadTimeParams = array(
                    "origin_kecamatan_id" => $originKecamatanID,
                    "dest_kecamatan_id" => $destKecamatanID,
                    "carrier_id" => $carrierID
                );
                $leadTime = $jneLib->getShippingLeadTime($getLeadTimeParams);
                if (!empty($leadTime)) {
                    $usedLeadTimeMax = (int) $leadTime["max"];
                }
            }

            if ($usedLeadTimeMax > 0) {
                $etaRaw = date_add(new DateTime("now"), date_interval_create_from_date_string(sprintf("%d days", $usedLeadTimeMax)));
                $eta = $etaRaw->format('Y-m-d H:i:s');
            }

            $shipmentData = [
                "shipment_id" => $invoice['shipment']['shipment_id'],
                "carrier_id" => $carrierID == getenv("JTR_CARRIER_ID") ? getenv("JTR_CARRIER_ID") : getenv("JNEJOB_CARRIER_ID"),
                "shipment_type" => "3pl",
                "track_number" => $jobNumber,
                "eta" => $eta
            ];
            $shipmentModel = new \Models\SalesShipment();
            $shipmentModel->setFromArray($shipmentData);
            $shipmentModel->saveData("shipment");
        }

        \Helpers\LogHelper::log("shipping_jne_job","[RESPONSE] AWB: $jobNumber, invoice_no: {$invoice['invoice_no']} , store_code: {$invoice['store']['store_code']}");
        
        return $jobNumber;
    }

    public function sanitizeAddress($address = "") {
        $address = trim(preg_replace('/\s\s+/', ' ', $address)); // replace multiple spaces and newlines with a single space
        $address = str_replace("\r\n"," ",$address); // replace new line and tab
        $address = str_replace("\n"," ",$address); // replace new line
        $address = str_replace("–","-",$address); // replace problematic char
        $address = str_replace("/","//",$address); // replace problematic char
        $address = str_replace("\\","\\",$address); // replace problematic char

        return $address;
    }

    private function _generateDataForNinjaVanAWB($invoice = array())
    {
        $ninjavanLib = new \Library\Shipping\NinjaVan();

        $invoiceModel = new \Models\SalesInvoice();
        $sql = "select token from shipping_ninja_credential where now() >= valid_from and now() <= valid_to";
        $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
        $result->setFetchMode(Db::FETCH_ASSOC);
        $dataList = $result->fetchAll();
        if (count($dataList) == 0) {
            $token = $ninjavanLib->getToken();

            $validFrom = date('Y-m-d');
            $validTo = Date('Y-m-d', strtotime("+29 days"));

            $sql = "insert into shipping_ninja_credential VALUES('$token', '$validFrom', '$validTo');";
            $invoiceModel->getDi()->getShared('dbMaster')->execute($sql);
        } else {
            $token = $dataList[0]['token'];
        }

        if ($invoice['delivery_method'] == 'store_fulfillment') {
            $salesShipmentModel = new \Models\SalesShipment();

            $paramsGet['sales_order_id'] = $invoice['sales_order_id'];
            $paramsGet['pickup_code'] = $invoice['pickup_code']['pickup_code'];
            $weight = (int)$salesShipmentModel->getWeightOrderedStoreFulfillment($paramsGet);
        } else {
            $weight = (int)$invoice['shipment']['total_weight'];
        }

        if ($weight == 0.00) {
            $weight = 1;
        }

        if ($invoice['shipment']['shipping_address']['phone'] == "0" || $invoice['shipment']['shipping_address']['phone'] == NULL) {
            $salesShipmentModelHp = new \Models\SalesShipment();
            $paramsGetHp['sales_order_id'] = $invoice['sales_order_id'];
            $recipient_handphone_number = $salesShipmentModelHp->getRecipientHandphone($paramsGetHp);
        } else {
            $recipient_handphone_number = $invoice['shipment']['shipping_address']['phone'];
        }

        $recipient_address = ($invoice['shipment']['shipping_address']['full_address'] == NULL) ? '-' : substr($invoice['shipment']['shipping_address']['full_address'], 0, 58);
        $recipient_address = str_replace(array("\n", "\t", "\r"), ' ', $recipient_address);

        $currentDate = date("Y-m-d");
        $tomorrowDate = date('Y-m-d', strtotime($currentDate. ' + 1 days'));
        $body = '{
                    "service_type":"Parcel",
                    "service_level":"Standard",
                    "requested_tracking_id":null,
                    "reference": {
                        "merchant_order_number":"'.$invoice['invoice_no'].'"
                    },                    
                    "from" : {
                        "name":"RUPARUPA",     
                        "phone_number":"(021) 582-9191",    
                        "email":"help@ruparupa.com",
                        "address": {
                            "address1":"'.$invoice['store']['address_line_2'].', '.$invoice['store']['city']['city_name'] .', '.$invoice['store']['province']['province_name'].'",    
                            "address2":"",
                            "country":"ID",
                            "postcode":"60198"
                        }               
                    },
                    "to" : {
                        "name":"'.$invoice['shipment']['shipping_address']['first_name'] . ' ' .$invoice['shipment']['shipping_address']['last_name'].'",     
                        "phone_number":"'.$recipient_handphone_number.'",    
                        "email":"'.$invoice['customer']['customer_email'].'",
                        "address": {
                            "address1":"'.$recipient_address.', '.$invoice['shipment']['shipping_address']['city']['city_name'] .', '.$invoice['shipment']['shipping_address']['province']['province_name'].'",  
                            "address2":"",
                            "country":"ID",
                            "postcode":"'.$invoice['shipment']['shipping_address']['post_code'].'"
                        }               
                    },
                    "parcel_job": {
                        "is_pickup_required": true,
                        "pickup_address" : {
                            "name":"'.$invoice['store']['pickup_name'].'",     
                            "phone_number":"(021) 582-9191",    
                            "email":"help@ruparupa.com",
                            "address": {
                                "address1":"'.$invoice['store']['address_line_1'].'",  
                                "address2":"'.$invoice['store']['address_line_2'].'",
                                "country":"ID",
                                "postcode":""
                            }               
                        },                         
                        "pickup_date":"'.$currentDate.'",
                        "pickup_service_type": "Scheduled",
                        "pickup_service_level": "Standard",
                        "pickup_timeslot": {
                            "start_time": "09:00",
                            "end_time": "18:00",
                            "timezone": "Asia/Jakarta"
                        },
                        "delivery_start_date": "'.$tomorrowDate.'",
                        "delivery_timeslot": {
                            "start_time": "09:00",
                            "end_time": "22:00",
                            "timezone": "Asia/Jakarta"
                        },
                        "delivery_instructions": "",
                        "dimensions": {
                            "weight":'.$weight.',
                            "size": "L"
                        }
                    }
                }';

        \Helpers\LogHelper::log("shipping_awb","params ninjavan: " . json_encode($body));
        $awbNumber = $ninjavanLib->generateAWB($body, $token);
        if(!empty($awbNumber)) {
            $shipmentData = [
                "shipment_id" => $invoice['shipment']['shipment_id'],
                "carrier_id" => getenv("NINJAVAN_CARRIER_ID"),
                "shipment_type" => "3pl",
                "track_number" => $awbNumber
            ];
            $shipmentModel = new \Models\SalesShipment();
            $shipmentModel->setFromArray($shipmentData);
            $shipmentModel->saveData("shipment");
        }

        return $awbNumber;
    }

    private function _generateDataForNCSAWB($invoice = array())
    {
        $ncsLib = new \Library\Shipping\NCS();
        $ncsParams = array('invoice_no' => $invoice['invoice_no']);
        $awb = $ncsLib->generateAWB($ncsParams);
        if ($awb == '') {
            $this->errorCode = 400;
            $this->errorMessages = "NCS return error";
            return null;
        }
        return $awb;
    }

    private function _getShopeeAWB($order_no = '', $invoice = array())
    {
        $awb = '';
        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_API'));
        $apiWrapper->setEndPoint("shopee/order/awb/".$order_no);
        if($apiWrapper->sendRequest("get")) {
            $apiWrapper->formatResponse();
        } else {
            \Helpers\LogHelper::log("jne_cashless_shopee", "failed get tracking no : ".$order_no);                    
        }
        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();
        if($data['message'] != "success") {
            \Helpers\LogHelper::log("jne_cashless_shopee", "failed get tracking no : ".$data['errors']['message']);
        }else {
            $awb = $data['data']['tracking_no'];
        }

        if(!empty($awb)) {
            
            $shipmentData = [
                "shipment_id" => $invoice['shipment']['shipment_id'],                
                "shipment_type" => "3pl",
                "track_number" => $awb
            ];
            $shipmentModel = new \Models\SalesShipment();
            $shipmentModel->setFromArray($shipmentData);
            $shipmentModel->saveData("shipment");
        }
        return $awb;
    }

    private function createAndSaveAWBShopeeV2($order_no = '', $invoice = array())
    {
        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_V2_API'));
        $apiWrapper->setEndPoint("shopee/v2/logistic/ship/dropoff");
        $params = [
            "shipment_id" => (int)$invoice['shipment']['shipment_id'],
        ];
        $apiWrapper->setParam($params);

        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            \Helpers\LogHelper::log("jne_cashless_shopee", "failed to request drop off for order ".$order_no);                    
        }
        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        if($data['message'] != "success") {
            \Helpers\LogHelper::log("jne_cashless_shopee", "failed to request drop off for order, error = ".$data['errors']['message']); //belum bisa log errornya
        }else{
            $shipmentData = [
                "shipment_id" => $invoice['shipment']['shipment_id'],                
                "shipment_type" => "3pl",
            ];
            $shipmentModel = new \Models\SalesShipment();
            $shipmentModel->setFromArray($shipmentData);
            $shipmentModel->saveData("shipment");
        }
    }

    public function getCarrierList($params = array())
    {
        $shippingCarrierModel = new \Models\ShippingCarrier();
        $resultShippingCarrier = $shippingCarrierModel::query();
        foreach($params as $key => $val)
        {
            if($key == "carrier_id") {
                $resultShippingCarrier->andWhere("carrier_id = $val");
            }

            if($key == "carrier_code") {
                $resultShippingCarrier->andWhere("carrier_code = '$val'");
            }
        }

        $result = $resultShippingCarrier->execute();
        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your carrier data";
            $allCarrier = array();
        } else {
            $i = 0;
            foreach ($result as $carrier) {
                $carrierArray[$i] = $carrier->toArray([], true);
                $i++;
            }
            $allCarrier = $carrierArray;
        }

        return $allCarrier;
    }

    public function getAllCarrier()
    {
        $shippingCarrierModel = new \Models\ShippingCarrier();
        $resultShippingCarrier = $shippingCarrierModel->findWithoutCache(
            [
                'conditions' => 'status = 1'
                ]
        );

        $shippingCarrier = $resultShippingCarrier->toArray();

        if(!$shippingCarrier) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found any carrier data";
        }

        return $shippingCarrier;
    }

    private function getDirectDeliveryInformation($invoiceID){
        $invoiceModel = new \Models\SalesInvoice();

        $sql = "SELECT
                    identifier,
                    value
                FROM sales_shipment_additional_data
                WHERE
                    invoice_id = {$invoiceID}
                    AND
                    identifier IN ('delivery_id','use_direct_delivery')
                ";

        $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
        $result->setFetchMode(Db::FETCH_ASSOC);
        $additionalDatas = $result->fetchAll();

        $resultData = array();
        foreach($additionalDatas as $data){
            $resultData[$data['identifier']] = $data['value'];
        }

        if (isset($resultData['use_direct_delivery']) && $resultData['use_direct_delivery'] == 'true') {
            $sql = "SELECT
                        source_dc
                    FROM sales_invoice_booking_dc
                    WHERE
                        invoice_id = {$invoiceID}
                        AND
                        source_dc != ''
                        AND
                        source_dc IS NOT NULL
                    LIMIT 1
                    ";

            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(Db::FETCH_ASSOC);
            $sourceDC = $result->fetch();

            $storeCodeDC = "";
            if (isset($sourceDC['source_dc']) && $sourceDC['source_dc'] != '') {
                $storeCodeDC = 'DC'.$sourceDC['source_dc'];
                $resultData['source_dc_storecode'] = $storeCodeDC;

                $storeModel = new \Models\Store();
                $storeResult = $storeModel->findFirst(["store_code = '{$storeCodeDC}'"]);
                
                $resultData['store']  = $storeResult->toArray(array('store_id','store_code','name','email_manager'));
                if($storeResult->SupplierAddress) {
                    $supplierAddress = new \Models\SupplierAddress();
                    $supplierAddress->assign($storeResult->SupplierAddress->toArray());
                    $storeAddress = $supplierAddress->getDataArray([],true);
                    unset($storeAddress['supplier_address_id']);
                    unset($storeAddress['supplier_address_type']);
                    unset($storeAddress['supplier_addresscol']);
                    $resultData['store'] = array_merge($resultData['store'],$storeAddress);
                }
    
                if($storeResult->PickupPoint) {
                    $pickupPointModel = new \Models\PickupPoint();
                    $pickupPointModel->assign($storeResult->PickupPoint->toArray());
                    $storeAddress = $pickupPointModel->getDataArray([],true);
                    $resultData['store'] = array_merge($resultData['store'],$storeAddress);
                }
            }
        }

        return $resultData;
    }

    private function getSupplierNameByInvoiceNo($invoiceNo){

        $invoiceModel = new \Models\SalesInvoice();

        $sql = "SELECT (
                    SELECT sp.name 
                    FROM supplier sp
                    WHERE sp.supplier_id = (SELECT s.supplier_id FROM store s WHERE s.store_code = si.store_code LIMIT 1)
                ) AS name
                FROM sales_invoice AS si
                WHERE invoice_no = '{$invoiceNo}'
                LIMIT 1";


        $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
        $result->setFetchMode(Db::FETCH_ASSOC);
        $supplierName = $result->fetch();

        $cleanSupplierName = 'RUPARUPA' . strtoupper(str_replace(' ', '', $supplierName['name']));
        if(strlen($cleanSupplierName) > 30){
            return substr($cleanSupplierName, 0, 30);
        }
        return $cleanSupplierName;
    }
}