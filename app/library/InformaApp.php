<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/10/2017
 * Time: 2:07 PM
 */

namespace Library;

use DateTime;
use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Models\CustomerInformaOtpTemp;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class InformaApp
{
    protected $invoice_no;
    protected $order_no;
    protected $credit_memo_no;
    protected $amount;

    protected $errorCode;
    protected $errorMessages;
    protected $customerNewModel;
    protected $appVersion;
    protected $errorData = [];

    public $voucherId;

    /**
     * @return mixed
     */

    protected $customer;

    protected $customerInformaDetails;

    protected $marketingLib;

    protected $kawanLamaSystemLib;

    public function __construct()
    {
        $this->customer = new \Models\Customer();
        $this->customerInformaDetails = new \Models\CustomerInformaDetails();
        $this->marketingLib = new Marketing();
        $this->kawanLamaSystemLib = new KawanLamaSystem();
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getErrorData()
    {
        return $this->errorData;
    }  

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function setAppVersion($params) {
        $this->appVersion = isset($params["x-app-version"]) ? $params["x-app-version"] : "";
    }


    public function registerCustomerInforma($sending_welcome_email = true, $params = array())
    {
        $logFile = "informa/informa_register_customer";
        $logFileError = "informa/informa_register_customer_error";
        $storeCodeNewRetail = isset($params['store_code_new_retail']) && !empty($params['store_code_new_retail']) ? $params['store_code_new_retail'] : "";

        $requiredKey = ["name", "phone", "email","password","address"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        if (!filter_var($params["email"], FILTER_VALIDATE_EMAIL)) {
            $this->errorCode = 400;
            $this->errorMessages = "Format e-mail tidak valid";
            return false;
        }

        $formattedPhone = $this->formatPhoneNumber($params["phone"]);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format Nomor Handphone tidak valid";
            return false;
        }

        $customerDetailTmp = $this->getCustomerData("email = '" . $params["email"] . "'");
        if(! $customerDetailTmp && $this->getErrorCode() != 404) {
            $this->logReqRes($logFileError, "getCustomerData", $params, $customerDetailTmp, "", $this->errorMessages);
            return false;
        }
        if($customerDetailTmp["status"] == 1){
            $this->errorCode = 400;
            $this->errorMessages = "Akun sudah dihapus dan tidak dapat digunakan kembali.";
            $this->logReqRes($logFileError, "informaRegisterDeletedAccount", $params, $customerDetailTmp, $this->errorMessages);
            return false;
        }
        if ($customerDetailTmp) {
            $this->errorCode = 303;
            $this->errorMessages = "E-mail sudah terdaftar. Silakan gunakan yang lain.";
            $this->logReqRes($logFileError, "getCustomerData", $params, $customerDetailTmp, "", $this->errorMessages);
            return false;
        }
        
        $rupaRupaPhone = $this->ruparupaFormatPhone($params["phone"]);
        $customerDetailTmp = $this->getCustomerData("phone = '" . $rupaRupaPhone . "'");
        if(! $customerDetailTmp && $this->getErrorCode() != 404) {
            return false;
        }
        if($customerDetailTmp["status"] == 1){
            $this->errorCode = 303;
            $this->errorMessages = "Akun sudah dihapus dan tidak dapat digunakan kembali.";
            $this->logReqRes($logFileError, "informaRegisterDeletedAccount", $params, $customerDetailTmp, "", $this->errorMessages);
            return false;
        }
        if ($customerDetailTmp) {
            $this->errorCode = 303;
            $this->errorMessages = "Nomor HP sudah terdaftar. Silakan gunakan yang lain.";
            $this->logReqRes($logFileError, "getCustomerData", $params, $customerDetailTmp, "", $this->errorMessages);
            return false;
        }

        $this->errorCode = null;
        $this->errorMessages = null;

        $name = $this->getFirstAndLastName($params["name"]);
        $customerEntities = [
            "first_name" => $name["first_name"],
            "last_name" => $name["last_name"],
            "phone" => $params["phone"],
            "email" => $params["email"],
            "password" => $params["password"],
            "company_code" => "HCI",
            "status" => 10,
            "is_phone_verified" => 10,
            "is_email_verified" => 0,
            "last_login" => date("Y-m-d H:i:s"),
        ];
        if(!empty($params["birth_date"])) {
            $customerEntities["birth_date"] = $params["birth_date"];
        }

        $customerNewModel = new \Models\Customer();
        $customerNewModel->setFromArray($customerEntities);
        $customerNewModel->setRegisteredBy("informa (mobile apps)");

        // Transaction
        $manager = new TxManager();
        $manager->setDbService($customerNewModel->getConnection());
        $transaction = $manager->get();

        try {
            if (! $customerNewModel->saveData("register_informa_from_login", "", $transaction)) {
                $this->errorCode = 400;
                $this->errorMessages = "Gagal menyimpan Customer Informa";
                $this->logReqRes($logFileError, "saveData", $customerEntities, false, "", $this->errorMessages);
                $transaction->rollback($this->errorMessages);
                return false;
            }

            // Format Phone
            $hoPhone = $this->formatPhoneNumberHO($params["phone"]);

            $informaWebAPI = new \Library\InformaWebAPI();
            $registerRequestPayload = [
                "email" => $params["email"],
                "fname" => $params["name"],
                "passwd" => $params["password"],
                "nohp" => $hoPhone,
                "address" => $params["address"],
                "provider" => "app"
            ];
        
            $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($registerRequestPayload);
            if (!$encryptedPayload) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "informaWebAPIEncrypt", $registerRequestPayload, $encryptedPayload, "", $this->errorMessages);  
                $transaction->rollback($this->errorMessages);
                return false;
            }
    
            $informaRegisterCustomer = $informaWebAPI->informaRegisterCustomer($encryptedPayload);
            $this->logReqRes($logFile, "informaRegisterCustomer", $registerRequestPayload, $informaRegisterCustomer, "");
            if (!$informaRegisterCustomer) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "informaRegisterCustomer", $registerRequestPayload, $informaRegisterCustomer, "", $this->errorMessages);
                $transaction->rollback($this->errorMessages);
                return false;
            }

        }   catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        // call myprofile API for get is_member

        $informaPCustID = "";
        $informaPCardID = "";
        $informaKTP = null;
        $informaMaritalStatus = null;
        $informaCardKey = "informa_customer_member_no";

        $customerInformaDetailsEntities = [
            "informa_customer_id" => $informaPCustID,
            $informaCardKey => $informaPCardID,
            "customer_id" => $customerNewModel->getCustomerId(),
            "ktp" => $informaKTP,
            "marital_status" => $informaMaritalStatus,
            "is_migrated" => 0,
        ];

        $this->customerInformaDetails->setFromArray($customerInformaDetailsEntities);
        if (! $this->customerInformaDetails->saveData("register_informa_from_login")) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal menyimpan Customer Informa";
            $this->logReqRes($logFileError, "saveData", $customerInformaDetailsEntities, false, "", $this->errorMessages);
            return false;
        }

        // In here we need to pass the value of email, phone and company_code to queue-v2 for linking customer
        $customerHelper = new \Helpers\CustomerHelper();
        $customerHelper->linkingCustomerUsingNsq($params["email"], $params["phone"], "HCI");
        
        $response = [
            "success" => true,
            "customer_id" => $customerNewModel->getCustomerId(),
        ];
        return $response;
    }

    public function customerLogin($data_array = array())
    {
        $logFile = "informa/informa_customer_login";
        $logFileError = "informa/informa_customer_login_error";
        $maxLoginAttempt = getenv("INFORMA_APP_MAX_LOGIN_ATTEMPT");
        $customerInformaDetailsMdl = new \Models\CustomerInformaDetails();
        // TODO: add login attempt validation when wrong password more than 5 times
        $validate = $this->validateRequired($data_array, ["email","password"]);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }
        
        $password = $data_array["password"];
        $email = $data_array["email"];
        
        $requestPayload["email"] = $email;
        $requestPayload["passwd"] = $password;

        $customer = false;
        // $customer = $this->getCustomerData("email = '" . $requestPayload["email"] . "'");
        $customer = $this->getCustomerDataNew(
            "email = ?",
            array(
                "email" => $requestPayload["email"]
            )
        );
        
        if(! $customer && $this->getErrorCode() != 404) {
            $this->logReqRes($logFileError, "getCustomerData", $requestPayload, $customer, "", $this->errorMessages);
            return false;
        }
        if ($customer["status"] == 1) {
            $this->errorCode = 400;
            $this->errorMessages = "Akun sudah dihapus dan tidak dapat digunakan kembali.";
            $this->logReqRes($logFileError, "informaLoginDeletedAccount", $requestPayload, $customer, $this->errorMessages);
            return false;
        }

        $customerInformaDetail = $this->getCustomerDetail("customer_id = '" . $customer["customer_id"] . "'");
        if (!$customerInformaDetail && $this->getErrorCode() != 404) {
            $this->logReqRes($logFileError, "getCustomerDetail", $requestPayload, $customerInformaDetail, "", $this->errorMessages);
            return false;
        }

        $currentDate = new DateTime();
        $loginAttemptDate = new DateTime($customerInformaDetail['login_attempt_date']);
        $diff = $currentDate->diff($loginAttemptDate);

        if ($customerInformaDetail['login_attempt'] >= $maxLoginAttempt) {            
            if ($diff->days > 0) {
                $customerInformaDetail['login_attempt'] = 0;
                $customerInformaDetailsMdl->assign($customerInformaDetail);
                $customerInformaDetailsMdl->save();
            } else {
                $this->errorCode = 400;
                $this->errorMessages = "Anda melebihi kesempatan percobaan masuk. Klik 'Lupa Password' atau coba kembali dalam 1x24 jam";
                $this->logReqRes($logFileError, "informaLoginDeletedAccount", $requestPayload, $customer, $this->errorMessages);
                return false;
            }
        }
    


        $this->errorCode = null;
        $this->errorMessages = null;

        $informaWebAPI = new \Library\InformaWebAPI();
        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $requestPayload, $encryptedPayload, "", $this->errorMessages);
            return false;
        }

        $informaLogin = $informaWebAPI->loginCustomer($encryptedPayload, "");
        $this->logReqRes($logFile, "loginCustomer", $requestPayload, $informaLogin);
        if (!$informaLogin) {
            if ($customerInformaDetail != false) {
                $lastLoginAttemptDate = strtotime($loginAttemptDate->format('Y-m-d'));
                $currentDateToTime =  strtotime($currentDate->format('Y-m-d'));
                if ($lastLoginAttemptDate < $currentDateToTime) {
                    $customerInformaDetail['login_attempt'] = 1;
                } else {
                    $customerInformaDetail['login_attempt'] += 1;
                }
                $customerInformaDetail['login_attempt_date'] = date("Y-m-d H:i:s");
                $customerInformaDetailsMdl->assign($customerInformaDetail);
                $customerInformaDetailsMdl->save();
                $this->errorMessages = "Email atau password yang Anda masukkan tidak sesuai";
                if (($maxLoginAttempt - $customerInformaDetail['login_attempt']) < 3) {
                    $this->errorMessages .= ". silahkan coba lagi (" . (3 - ($maxLoginAttempt - $customerInformaDetail['login_attempt'])) . "/3)";
                } else {
                    $this->errorMessages .= ". Jika ada keluhan silakan klik icon chat di kanan bawah";
                }
            } else {
                $this->errorMessages = $informaWebAPI->getErrorMessages() !== "" ? $informaWebAPI->getErrorMessages() : "Login Information Tidak Valid";
            }
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->logReqRes($logFileError, "loginCustomer", $requestPayload, $informaLogin, "", $this->errorMessages);            
            return false;
        }
        $customerData= $this->getCustomerDataInforma($informaLogin);
        if (!empty($this->errorCode)) {
            return false;
        }
        $useMemberPhone = false;
        $custPhone = $customerData['notelp'];
        $name = $this->getFirstAndLastName($customerData["fname"]);

        $customerEntities = [
            "first_name" => $name["first_name"],
            "last_name" => $name["last_name"],
            "email" => $customerData["email"],
            "password" => $password,
            "company_code" => "HCI",
            "status" => 10,
            "last_login" => date("Y-m-d H:i:s"),
        ];

        $customerInformaDetailsEntities = [
            "informa_uid" => $customerData["uid"],
            "session_id" => $informaLogin["sessionid"],
        ];

        $memberData = array();
        $memberData["P_CUST_ID"] = "";
        $memberData["P_CARD_ID"] = "";
        $informaCardKey = "informa_customer_member_no";
        $isPaidMember = false;

        if ($customerData['is_member'] != 0){
            $memberData = $this->getMemberDataInforma($informaLogin["uid"]);
            if (!empty($this->errorCode)) {
                return false;
            }
            if (isset($memberData["P_JENIS_KELAMIN"])) {
                $customerEntities["gender"] = $this->getSexString($memberData["P_JENIS_KELAMIN"]);
            }

            if (isset($memberData["P_TANGGAL_LAHIR"])) {
                $dateTime = date_create_from_format('d/m/Y', $memberData["P_TANGGAL_LAHIR"]);
                $customerEntities["birth_date"] = date_format($dateTime, 'Y-m-d');
            }

            if ($memberData["P_NO_KTP"]) {
                $customerInformaDetailsEntities["ktp"] = $memberData["P_NO_KTP"];
            }

            if ($memberData["P_STATUS_PERKAWINAN"]) {
                $customerInformaDetailsEntities["marital_status"] = $memberData["P_STATUS_PERKAWINAN"];
            }

            if (substr($memberData["P_CARD_ID"], 0, 3) == "TIM") {
                $informaCardKey = "informa_customer_tmp_member_no";
            } else {
                $isPaidMember = true;
            }

            if (($custPhone == "" || $custPhone == "-") && ($memberData["P_NO_HP"] != "0000")) {
                $custPhone = $memberData["P_NO_HP"];
                $useMemberPhone = true;
            }

            $nowDate = new \DateTime("now");
            $expDate = new \DateTime($memberData["P_EXPIRED_DATE"]);    
            $customerEntities["group"] = [
                "HCI" => $memberData["P_CARD_ID"],
                "HCI_expiration_date" => $expDate->format('Y-m-d 23:59:59'),
                "HCI_is_verified" => $nowDate <= $expDate ? 10 : 5,
            ];
        } 

        if ($custPhone == "" || $custPhone == "-") {
            $isPhoneValid = false;
        } else {
            $phone = $this->ruparupaFormatPhone($custPhone);
            $customerEntities["phone"] = $phone;
            $isPhoneValid = true;
        }
        $customerInformaDetailsEntities["informa_customer_id"] = $memberData["P_CUST_ID"];
        $customerInformaDetailsEntities[$informaCardKey] = $memberData["P_CARD_ID"];

        $customerNewModel = new \Models\Customer();
        
        $customer = false;
        $isMigrated = false;

        $customer = $this->getCustomerData("email = '" . $customerData["email"] . "' AND status = 10 AND company_code = 'HCI'");
        if (!$customer) {
            if ($this->getErrorCode() != 404) {
                return false;
            }
            $customerInformaDetailsEntities["is_migrated"] = 10;
            $isMigrated = true;

            $customerNewModel->setRegisteredBy("informa (mobile apps)");
        } else {
            $customerEntities["customer_id"] = $customer["customer_id"];            
            $customerInformaDetail = $this->getCustomerDetail("customer_id = '" . $customer["customer_id"] . "'");
            if (!$customerInformaDetail) {
                if ($this->getErrorCode() != 404) {
                    return false;
                }
            } else {
                $customerInformaDetailsEntities["customer_informa_details_id"] = $customerInformaDetail["customer_informa_details_id"];
            }        
        }
        
        $customerNewModel->setFromArray($customerEntities);
        if (!$customerNewModel->saveData("register_informa_from_login")) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal menyimpan Customer Informa";
            $this->logReqRes($logFileError, "saveData", $customerEntities, false, "", $this->errorMessages);
            return false;
        }

        $customerInformaDetailsEntities["customer_id"] = $customerNewModel->getCustomerId();

        $this->customerInformaDetails->setFromArray($customerInformaDetailsEntities);
        if (!$this->customerInformaDetails->saveData("register_informa_from_login")) {
            $this->errorCode = $this->customerInformaDetails->getErrorCode();
            $this->errorMessages = $this->customerInformaDetails->getErrorMessages();
            $this->logReqRes($logFileError, "saveData", $customerInformaDetailsEntities, false, "", $this->errorMessages);
            return false;
        }

        $customerInformaData = $this->getCustomerData("customer_id = '" . $customerNewModel->getCustomerId() . "'");
        if (!$customerInformaData) {
            if ($this->getErrorCode() != 404) 
            return false;
        } else {
            if (isset($customerInformaData["gender"])) {
                $customerData["sex"] =  $this->getSexId($customerInformaData["gender"]);
            }
            if (isset($customerInformaData["birth_date"])) {
                $customerData["birth_date"] =  $customerInformaData["birth_date"];
            }
        }

        $customerInformaDetailData = $this->getCustomerDetail("customer_id = '" . $customerNewModel->getCustomerId() . "'");
        if (!$customerInformaDetailData) {
            if ($this->getErrorCode() != 404) 
            return false;
        } else {
            if (isset($customerInformaDetailData["ktp"])) {
                $customerData["ktp"] = $customerInformaDetailData["ktp"];
            }

            if (isset($customerInformaDetailData["marital_status"])) {
                $customerData["marital_status"] = $customerInformaDetailData["marital_status"];
            }
        }

        if ($isMigrated) {
            // Update last login to now on migrate
            $customerUpdateModel = new \Models\Customer();
            $customerEntities["customer_id"] = $customerNewModel->getCustomerId();
            $customerEntities["last_login"] = date("Y-m-d H:i:s");
            $customerUpdateModel->setFromArray($customerEntities);
            if (!$customerUpdateModel->saveData("update_informa_from_login")) {
                $this->errorCode = 400;
                $this->errorMessages = "Gagal menyimpan Customer Informa";
                $this->logReqRes($logFileError, "saveData", $customerEntities, false, "", $this->errorMessages);
                return false;
            }    
        }

        $currentCartId = empty($data_array['cart_id']) ? "" : $data_array['cart_id'];
        $cartLib = new \Library\Cart();
        $storeCodeNewRetail = isset($data_array['store_code_new_retail']) ? $data_array['store_code_new_retail'] : '';
        $respMergeCart = $cartLib->getCustomerCartId($customerNewModel->getCustomerId(), $currentCartId, "ODI", $storeCodeNewRetail);
        $shoppingCartId = !empty($respMergeCart['data']['cart_id']) ? $respMergeCart['data']['cart_id'] : "";
        $respMergeMinicart = $cartLib->getCustomerMiniCartId($customerNewModel->getCustomerId(), $currentCartId, "ODI", $storeCodeNewRetail);
        $shoppingMiniCartId = !empty($respMergeMinicart['data']['cart_id']) ? $respMergeMinicart['data']['cart_id'] : "";
        
        if ($useMemberPhone) {
            $successUpdatePhone = $this->changeProfilePhoneCustomerInforma($customerData["uid"], $informaLogin["sessionid"], $customerNewModel->getCustomerId(), $customerEntities["phone"]);
            if (!$successUpdatePhone) {
                $this->logReqRes($logFileError, "changeProfilePhoneCustomerInforma", $customerInformaDetailsEntities, false, "", $this->errorMessages);
                return false;
            }
        }

        $customerData["member"] = $memberData;
        $responseData = $this->mappingCustomerData($customerData);
        $responseData["customer_id"] = $customerNewModel->getCustomerId();
        $responseData["details"] = $customerInformaDetailsEntities;
        $responseData["cart_id"] = $shoppingCartId;
        $responseData["minicart_id"] = $shoppingMiniCartId;
        $responseData["is_phone_valid"] = $isPhoneValid;
        $responseData["use_member_phone"] = $useMemberPhone;
        $responseData["is_paid_member"] = $isPaidMember;

        if ($isPhoneValid) {
            // In here we need to pass the value of email, phone and company_code to queue-v2 for linking customer
            $customerHelper = new \Helpers\CustomerHelper();
            $customerHelper->linkingCustomerUsingNsq($customerEntities["email"], $customerEntities["phone"], "HCI");
        }

        if ($customerInformaDetail != false) {
            $customerInformaDetailsEntities['login_attempt'] = 0;
            $customerInformaDetailsEntities['login_attempt_date'] = date("Y-m-d H:i:s");
            $customerInformaDetailsMdl->assign($customerInformaDetailsEntities);
            $customerInformaDetailsMdl->save();
        }  
                    
        return $responseData;
    }

      public function getCustomerDetail($condition = "")
    {
        $logFileError = "informa/informa_get_customer_detail_error";
        try {
            $customer = new \Models\CustomerInformaDetails();
            $param['conditions'] = $condition == "" ? $condition : "(" . $condition . ")";
            $result = $customer->findFirst($param);    

        } catch (\Exception $e) {
            return false;
        }

        if (! empty($result)) {
            $this->errorCode = null;
            return $result->toArray();
        } 
        $this->errorCode = 404;
        $this->errorMessages = "Customer detail tidak ditemukan";

        $this->logReqRes($logFileError, "EmptyData", $condition, false, "", $this->errorMessages);
        return false;
        
    }

    public function getCustomerData($condition = "", $companyCode = "HCI")
    {
        $logFileError = "informa/informa_get_customer_data_error";
        try {
            $customer = new \Models\Customer();
            $param['conditions'] = $condition == "" ? $condition : "(" . $condition . " AND company_code = '".$companyCode."')";
            $result = $customer->findFirst($param);    

        } catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage() ? $e->getMessage() : "Error saat mendapatkan customer data";
        }

        if (! empty($result)) {
            $this->errorCode = null;
            return $result->toArray();
        } 
        
        $this->errorCode = 404;
        $this->errorMessages = "Customer data tidak ditemukan";
        $this->logReqRes($logFileError, "EmptyData", $condition, false, "", $this->errorMessages);
        return false;
        
    }

    public function getCustomerDataNew($condition = "", $bindings = array(), $companyCode = "HCI")
    {
        $logFileError = "informa/informa_get_customer_data_error";

        $bindData = \Helpers\GeneralHelper::prepareDynamicBindStatement($condition, $bindings);
        $bindData = $bindData . " AND company_code = '".$companyCode."'";

        try {
            $customer = new \Models\Customer();
            $result = $customer->findFirst([
                $bindData, 
                $bindings
            ]);    

        } catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage() ? $e->getMessage() : "Error saat mendapatkan customer data";
        }

        if (! empty($result)) {
            $this->errorCode = null;
            return $result->toArray();
        } 
        
        $this->errorCode = 404;
        $this->errorMessages = "Customer data tidak ditemukan";
        $this->logReqRes($logFileError, "EmptyData", $condition, false, "", $this->errorMessages);
        return false;
        
    }

    public function forgetPasskey($email, $companyId = "HCI") {
        $customerInformaDetailsMdl = new \Models\CustomerInformaDetails();
        $logFileError = "informa/informa_forget_passkey_error";
        $requestPayload["email"] = $email != false ? $email : "";
        $maxLoginAttempt = getenv("INFORMA_APP_MAX_LOGIN_ATTEMPT");        

        $customer = $this->getCustomerData("email = '" . $requestPayload["email"] . "' and status = 10");        
        if (isset($customer["customer_id"])) {
            $customerInformaDetail = $this->getCustomerDetail("customer_id = '" . $customer["customer_id"] . "'");            
        }


        if (isset($customerInformaDetail) && $customerInformaDetail != false ) {
            $currentDate = new DateTime();
            $forgotPassAttemptDate = new DateTime($customerInformaDetail['forgot_pass_attempt_date']);
            $diff = $currentDate->diff($forgotPassAttemptDate);
            if ($customerInformaDetail['forgot_pass_attempt'] >= $maxLoginAttempt) {
                if ($diff->days > 0) {
                    $customerInformaDetail['forgot_pass_attempt'] = 0;
                    $customerInformaDetailsMdl->assign($customerInformaDetail);
                    $customerInformaDetailsMdl->save();
                } else {
                    $this->errorCode = 400;
                    $this->errorMessages = "Sudah melebihi batas pengajuan lupa password. Anda dapat melakukan pengajuan kembali dalam 1x24 jam";
                    $this->logReqRes($logFileError, "validateForgotPasskey", $requestPayload, $customer, $this->errorMessages);
                    return false;
                }
            }
        }



        $informaWebAPI = new \Library\InformaWebAPI();
        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($requestPayload);
        if (! $encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $requestPayload, false, "", $this->errorMessages);
            return false;
        }

        $forgetPassKey = $informaWebAPI->forgetPasskey($encryptedPayload);
        if(! $forgetPassKey) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "forgetPasskey", $requestPayload, false, "", $this->errorMessages);

            return false;
        }
        if (isset($customerInformaDetail) && $customerInformaDetail != false) {
            $lastForgotAttemptDate = strtotime($forgotPassAttemptDate->format('Y-m-d'));
            $currentDateToTime =  strtotime($currentDate->format('Y-m-d'));
            if ($lastForgotAttemptDate < $currentDateToTime) {
                $customerInformaDetail['forgot_pass_attempt'] = 1;
            } else {
                $customerInformaDetail['forgot_pass_attempt'] += 1;
            }
            $customerInformaDetail['forgot_pass_attempt_date'] = date("Y-m-d H:i:s");
            $customerInformaDetail['login_attempt'] = 0;
            $customerInformaDetailsMdl->assign($customerInformaDetail);
            $customerInformaDetailsMdl->save();
        }
        return $forgetPassKey;
    }

    Public function changePasskey($customerId, $email, $oldPass, $newPass, $uid, $sessionId)
    {   
        $logFile = "informa/informa_change_password";
        $logFileError = "informa/informa_change_password_error";
        $newPassTmp = $newPass;
        $newPass = hash('sha256', $newPass);
        $dateNow = date("Y-m-d H:i:s");

        try {
            $customer = new \Models\Customer();
            $manager = new TxManager();
            $manager->setDbService($customer->getConnection());
            $transaction = $manager->get();

            $sql = "UPDATE customer SET password = '".$newPass."', updated_at = '" . $dateNow . "' WHERE customer_id = " . $customerId;
            $customer->useWriteConnection();
            $customer->getDi()->getShared($customer->getConnection())->query($sql);

            $customer->setTransaction($transaction);

            $changePassKeyInforma = $this->changePasskeyInforma($customerId, $email, $oldPass, $newPassTmp, $uid, $sessionId);
            if(! $changePassKeyInforma) {
                $transaction->rollback($this->errorMessages);
                return false;
            }

            $transaction->commit();
            return $changePassKeyInforma;
        }catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage();

            $this->logReqRes($logFileError, "changePasskey", $email, false, $customerId, $this->errorMessages);
        }

        return false;

    }

    private function changePasskeyInforma($customerId, $email, $oldPassword, $newPassword, $uid, $sessionId) {
        $logFile = "informa/informa_change_pass_key";
        $logFileError = "informa/informa_change_pass_key_error";

        $requestPayload["email"] = $email;
        $requestPayload["oldpasswd"] = $oldPassword;
        $requestPayload["newpasswd"] = $newPassword;

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $informaWebAPI = new \Library\InformaWebAPI();
        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($requestPayload);
        if (! $encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $requestPayload, false, $customerId, $this->errorMessages);
            return false;
        }

        $changePassKey = $informaWebAPI->changePasskey($encryptedPayload, $auth);
        if(! $changePassKey) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();

            $this->logReqRes($logFileError, "changePasskey", $requestPayload, false, $customerId, $this->errorMessages);
            return false;
        }

        return $changePassKey;
    }

    public function getCustomerDataInforma($data = array())
    {
        $logFile = "informa/informa_get_customer_data_hci";
        $logFileError = "informa/informa_get_customer_data_hci_error";

        //uid;seassonID
        $informaWebAPI = new \Library\InformaWebAPI();
        $customerNewModel = new \Models\Customer();

        $customer = $informaWebAPI->getCustomerData($data);
        $this->logReqRes($logFile, "getCustomerData", $data, $customer);
        if (! $customer) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getCustomerData", $data, $customer, $data["uid"], $this->errorMessages);
            return false;
        }

        if(empty($customer)) {
            $this->errorCode = 404;
            $this->errorMessages = ["Customer tidak ditemukan"];
            $this->logReqRes($logFileError, "getCustomerData", $data, false, $data["uid"], $this->errorMessages);
            return false;
        }

        return $customer;
    }

    public function getMemberDataInforma($uid)
    {
        $logFile = "informa/informa_get_member_data";
        $logFileError = "informa/informa_get_member_data_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $requestPayload["P_UID"] = $uid;
        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $requestPayload, false, "", $this->errorMessages);
            return false;
        }

        $memberDetail = $informaWebAPI->getMemberData($encryptedPayload);
        $this->logReqRes($logFile, "getMemberData", $requestPayload, $memberDetail);
        if (! $memberDetail) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getMemberData", $requestPayload, false, "", $this->errorMessages);
            return false;
        }

        if(empty($memberDetail)) {
            $this->errorCode = 404;
            $this->errorMessages = ["Customer tidak ditemukan"];
            $this->logReqRes($logFileError, "getMemberData", $requestPayload, false, "", $this->errorMessages);
            return false;
        }
        return $memberDetail;
    }   

    public function updateCustomerData($customer = array(), $customerInformaDetail = array(), $updateData = array(), $isMember = false)
    {
        $logFile = "informa/informa_update_customer";
        $logFileError = "informa/informa_update_customer_error";

        $fullName = $updateData["full_name"];
        $name = $this->getFirstAndLastName($fullName);    
        // update Data profile
        // {
        //     "fname": "Andy H",
        //     "nohp": "088877726363",
        //     "address": "Jl. Puri Indah Oke",
        //     "city": "13"
        // }

        $profilePayload["city"] = $updateData["city"]; 
        $profilePayload["fname"] = $updateData["full_name"]; 
        $profilePayload["nohp"] = ""; // Phone and email cannot be updated thru this endpoint 
        $profilePayload["address"] = $updateData["address"]; 

        $customerModel = new \Models\Customer();
        $customerDetailModel = new \Models\CustomerInformaDetails();

        $manager = new TxManager();
        $manager->setDbService($customerModel->getConnection());
        $transaction = $manager->get();

        $successUpdateMember = false;

        try {
            // save data to Customer
            $customerModel = $customerModel->findFirst("customer_id = '" . $customer["customer_id"] . "'");
            if (!empty($fullName)) {
                $customerModel->first_name = $name["first_name"];
                $customerModel->last_name = $name["last_name"];
            }
            if (!empty($updateData["birth_date"]))
                $customerModel->birth_date = $updateData["birth_date"];
   
            if (isset($updateData["marital_status"]))
                $customerModel->gender = $this->getSexString($updateData["sex"]);
    
            
            $customerModel->updated_at = date("Y-m-d H:i:s");
            $customerModel->save();
            $customerModel->setTransaction($transaction);


            $customerDetailModel = $customerDetailModel->findFirst("customer_id = '" . $customer["customer_id"] . "'");

            if (isset($updateData["ktp"]))
                $customerDetailModel->ktp = $updateData["ktp"];

            if (isset($updateData["marital_status"]))
                $customerDetailModel->marital_status = $updateData["marital_status"];


            $customerDetailModel->updated_at = date("Y-m-d H:i:s");
            $customerDetailModel->save();
            $customerDetailModel->setTransaction($transaction);
            
            # change profile data
            $informaWebAPI = new \Library\InformaWebAPI();
            $encryptedProfilePayload = $informaWebAPI->informaWebAPIEncrypt($profilePayload);
            if (!$encryptedProfilePayload) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "informaWebAPIEncrypt", $profilePayload, $encryptedProfilePayload, $customer["customer_id"], $this->errorMessages);
                $transaction->rollback($this->errorMessages);
              
                return false;
            }

            $auth["uid"] = $customerInformaDetail["informa_uid"];
            $auth["sessionid"] = $customerInformaDetail["session_id"];
            $successUpdateProfile = $informaWebAPI->editProfile($encryptedProfilePayload, $auth);
            $this->logReqRes($logFile, "editProfile", $profilePayload, $successUpdateProfile);
            if (! $successUpdateProfile) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "editProfile", $profilePayload, $successUpdateProfile, $customer["customer_id"], $this->errorMessages);
                $transaction->rollback($this->errorMessages);

                return false;
            }

            if ($isMember) {
                // update Member Data
                // {    
                //     "P_CUST_ID": "371ce9b2"
                //     "P_NO_KTP": "1588558456885555",
                //     "P_TANGGAL_LAHIR": "01/06/1998",
                //     "P_TEMPAT_LAHIR": "Oke",
                //     "P_STATUS_PERKAWINAN": 2,
                //     "P_JENIS_KELAMIN": 1
                // }

                $memberPayload["P_CUST_ID"] = $customerInformaDetail["informa_customer_id"];
                $memberPayload["P_NO_KTP"] = $updateData["ktp"];
                $memberPayload["P_TANGGAL_LAHIR"] = $updateData["birth_date"];
                $memberPayload["P_TEMPAT_LAHIR"] = $updateData["birth_place"];
                $memberPayload["P_STATUS_PERKAWINAN"] = $updateData["marital_status"];
                $memberPayload["P_JENIS_KELAMIN"] = $updateData["sex"];
        
                # change member data
                $encryptedMemberPayload = $informaWebAPI->informaWebAPIEncrypt($memberPayload);
                if (!$encryptedMemberPayload) {
                    $this->errorCode = $informaWebAPI->getErrorCode();
                    $this->errorMessages = $informaWebAPI->getErrorMessages();
                    $this->logReqRes($logFileError, "informaWebAPIEncrypt", $memberPayload, $encryptedMemberPayload, $customer["customer_id"], $this->errorMessages);
                    $transaction->rollback($this->errorMessages);
                  
                    return false;
                }
                $successUpdateMember = $informaWebAPI->editMemberData($encryptedMemberPayload);
                $this->logReqRes($logFile, "editMemberData", $memberPayload, $successUpdateMember, $customer["customer_id"]);
                if (! $successUpdateMember) {
                    $this->errorCode = $informaWebAPI->getErrorCode();
                    $this->errorMessages = $informaWebAPI->getErrorMessages();
                    $this->logReqRes($logFileError, "editMemberData", $memberPayload, $successUpdateMember, $customer["customer_id"], $this->errorMessages);
                    $transaction->rollback($this->errorMessages);
    
                    return false;
                }
            }

        }catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();
        return true;

    }

    private function formatPhoneNumberInternational($phone) {
        $phoneList = [
            "60", // Malaysia
            "65", // Singapore
            "66", // Thailand
            "61", // Australia
            "86", // China
            "81", // Japan
            "82", // South Korea
            "1", // United States
            "33", // France
            "91", // India
            "84", // Vietnam
            "63", // Philippines
            "852", // Hong Kong
            "966", // Saudi Arabia
        ];

        foreach ($phoneList as $phonePrefix) {
            if (substr($phone, 0, strlen($phonePrefix)) == $phonePrefix) {
                $phone = "+" . $phone;
                break;
            }
        }

        return $phone;
    }

    private function formatPhoneNumberAll($phone) {
        if (substr($phone, 0, 2) == "08") {
            $phone = substr_replace($phone, "+628", 0, 2);
        } else if (substr($phone, 0, 2) == "62") {
            $phone = substr_replace($phone, "+62", 0, 2);
        } else {
            $phone = $this->formatPhoneNumberInternational($phone);
        }

        return $phone;
    }


    private function formatPhoneNumber($phone) {
        if (substr($phone, 0, 2) == "08") {
            return substr_replace($phone, "+628", 0, 2);
        } else {
            $phoneInternational = $this->formatPhoneNumberInternational($phone);
            if (strcmp($phoneInternational, $phone) === 0) { // return false if the data didn't change (not found)
                return false;
            }
            return $phoneInternational;
        }
    }

    private function formatNormalPhoneNumber($phone) {
        if (substr($phone, 0, 4) == "+628") {
            return substr_replace($phone, "08", 0, 4);
        }

        return false;
    }

    private function formatPhoneNumberHO($phone) {
        if (substr($phone, 0, 2) == "08") {
            return substr_replace($phone, "628", 0, 2);
        }
        if (substr($phone, 0, 4) == "+628") {
            return substr_replace($phone, "628", 0, 4);
        }
    
        return $phone;
    }

    private function ruparupaFormatPhone($phone) {
        $phone = preg_replace('/[^0-9]/', '', $phone);

        if(substr($phone, 0, 2) == "62") {
            return substr_replace($phone, "0", 0, 2);
        }

        return $phone;
    }

    private function getFirstAndLastName($fullName) {
        $fullNameArr = explode(" ", $fullName);
        $firstName = $fullNameArr[0];
        $lastName = "";
        if(count($fullNameArr) > 1) {
            array_shift($fullNameArr);

            $lastName = implode(" " ,$fullNameArr);
        }

        return [
            "first_name" => $firstName,
            "last_name" => $lastName,
        ];
    }

    public function getFullName($firstName, $lastName) {
        $fullName = $firstName;
        if (!empty($lastName) && !is_null($lastName) && isset($lastName)) {
            $fullName = $firstName . " " . $lastName;
        }

        return $fullName;
    }


    public function validateRequired(array $data, array $fields, $checkValueEmpty = true) {
        $response = [];

        foreach ($fields as $field) {
            if(! array_key_exists($field, $data) || ($checkValueEmpty && empty($data[$field]))) {
                $field = str_replace("_", " ", $field);
                $message = ucwords($field) . " Diperlukan";
                $response[] = $message;
            }
        }

        return count($response) > 0 ? $response : false;
    }

    public function mappingCustomerData(array $customer) {

        $member = $customer["member"]; 
        $name = $this->getFirstAndLastName($customer["fname"]);
        $phone = $this->ruparupaFormatPhone($customer["notelp"]);

        $customerMap = [];
        $memberDetail = [];
        if ($member["P_CUST_ID"] != "" ){
            $memberDetail["id_card"] = $member["P_CARD_ID"];
            $memberDetail["member_no"] = $member["P_CUST_ID"];
            $memberDetail["member_name"] = $member["P_MEMBER_NAME"];
            $memberDetail["member_exp_date"] = $member["P_EXPIRED_DATE"];
            $memberDetail["member_email"] = strtolower($member["P_EMAIL"]);
            $memberDetail["member_phone"] = $member["P_NO_HP"];
            $memberDetail["birth_place"] = $member["P_TEMPAT_LAHIR"];
            $memberDetail['is_email_verified'] = $member["P_MAIL_STATUS"];
            $memberDetail['is_phone_verified'] = $member["P_HP_STATUS"];
        }

        $customerMap["first_name"] = $name["first_name"];
        $customerMap["last_name"] = $name["last_name"];
        $customerMap["email"] = $customer["email"];
        $customerMap["phone"] = $phone;
        $customerMap["is_member"] = $customer["is_member"];
        $customerMap["address"] = $customer["addr"];
        $customerMap["city"] = $customer["city"];
        $customerMap["sex"] = -1;
        $customerMap["birth_date"] = "";
        $customerMap["ktp"] = "";
        $customerMap["marital_status"] = -1;

        if (isset($customer["sex"])) {
            $customerMap["sex"] = $customer["sex"];
        } else if (isset($member["P_JENIS_KELAMIN"])) {
            $customerMap["sex"] = $member["P_JENIS_KELAMIN"];
        }

        if (isset($customer["birth_date"])) {
            $customerMap["birth_date"] = $customer["birth_date"];
        } else if (isset($member["P_TANGGAL_LAHIR"])) {
            $dateTime = date_create_from_format('d/m/Y', $member["P_TANGGAL_LAHIR"]);
            $customerMap["birth_date"] = date_format($dateTime, 'Y-m-d');
        }

        if (isset($customer["ktp"])) {
            $customerMap["ktp"] = $customer["ktp"];
        } else if (isset($member["P_NO_KTP"])) {
            $customerMap["ktp"] = $member["P_NO_KTP"];
        }

        if (isset($customer["marital_status"])) {
            $customerMap["marital_status"] = $customer["marital_status"];
        } else if (isset($member["P_STATUS_PERKAWINAN"])) {
            $customerMap["marital_status"] = $member["P_STATUS_PERKAWINAN"];
        }

        $customerMap["member_detail"] = $memberDetail;

        return $customerMap;
    }

    public function getUserVoucherCustomerInforma($uid, $sessionId) 
    {
        $logFileError = "informa/informa_get_customer_voucher_error";
        $informaWebAPI = new \Library\InformaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $voucherInfo = $informaWebAPI->getMemberVoucher($auth);
        if (! $voucherInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getMemberVoucher", $auth, $voucherInfo, $uid, $this->errorMessages);

            return false;
        }

        return $voucherInfo;
    }

    public function getListNotifCustomerInforma($uid, $sessionId, $page) 
    {
        $logFileError = "informa/informa_get_list_notif_customer_error";

        $informaWebAPI = new \Library\InformaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $response = $informaWebAPI->getMemberListNotif($auth, $page);
        if (! $response) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();

            $this->logReqRes($logFileError, "getMemberListNotif", $auth, $response, $uid, $this->errorMessages);

            return false;
        }

        return $response;
    }

    public function getTransactionHistCustomerInforma($uid, $sessionId) 
    {
        $logFileError = "informa/informa_get_transaction_hist_error";

        $informaWebAPI = new \Library\InformaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $result = $informaWebAPI->getTransactionHist($auth);
        if (! $result) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getTransactionHist", $auth, $result, $uid, $this->errorMessages);

            return false;
        }

        return $result;
    }

    public function getTieringCustomerInforma($uid, $sessionId) 
    {
        $logFileError = "informa/informa_get_tiering_error";
        $informaWebAPI = new \Library\InformaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $result = $informaWebAPI->getTiering($auth);
        if (! $result) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getTiering", $auth, $result, $uid, $this->errorMessages);
            return false;
        }

        return $result;
    }

    public function getDigitalReceiptCustomerInforma($uid, $sessionId, $receiptNo) 
    {
        $logFileError = "informa/informa_get_digital_receipt_error";
        $informaWebAPI = new \Library\InformaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $payload["receipt_no"] = $receiptNo;
        $payload["company_code"] = "HCI";    

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $payload, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $receiptInfo = $informaWebAPI->getDigitalReceipt($auth, $encryptedPayload);
        if (! $receiptInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getDigitalReceipt", $payload, $receiptInfo, $uid, $this->errorMessages);
            return false;
        }

        return $receiptInfo;
    }

    public function logoutCustomerInforma($uid, $sessionId) {
        $logFileError = "informa/informa_logout";
        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $logoutInfo = $informaWebAPI->logoutUser($auth);
        if (! $logoutInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "logoutUser", $auth, $logoutInfo, $uid, $this->errorMessages);
            return false;
        }

        return $logoutInfo;
    }

    public function checkReceiptNoCustomerInforma($auth, $receiptNo) {
        $logFileError = "informa/informa_check_receipt_no";
        $informaWebAPI = new \Library\InformaWebAPI();

        $payload = [
            "receiptno" => $receiptNo,
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $payload, $encryptedPayload, $auth["uid"], $this->errorMessages);
            return false;
        }

        $checkResult = $informaWebAPI->informaCheckReceiptNo($auth, $encryptedPayload);
        if (!$checkResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaCheckReceiptNo", $payload, $checkResult, $auth["uid"], $this->errorMessages);
            return false;
        }

        return $checkResult;
    }

    
    public function transactionDeliveryListCustomerInforma($uid, $sessionId, $receiptNo) {
        $logFileError = "informa/informa_transaction_delivery_list_error";
        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $checkResult = $this->checkReceiptNoCustomerInforma($auth, $receiptNo);
        if (!$checkResult) {
            return false;
        }

        $txDetailInfo = $informaWebAPI->transactionDeliveryList($auth, $receiptNo);
        if (! $txDetailInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "transactionDeliveryList", $receiptNo, $txDetailInfo, $uid, $this->errorMessages);
            return false;
        }
   
        if (count($txDetailInfo) < 1) {
            $this->errorCode = 404;
            $this->errorMessages = "No Data Found";
            $this->logReqRes($logFileError, "transactionDeliveryList", $receiptNo, $txDetailInfo, $uid, $this->errorMessages);
            return false;
        }

        foreach($txDetailInfo as $index => $item) {
            if (isset($item["dlv_status"])) {
                $txDetailInfo[$index]["dlv_status_desc"] = $this->getDeliveryInfo($item["dlv_status"]);
            }
        }

        return $txDetailInfo;
    }



    public function transactionDeliveryDetailCustomerInforma($uid, $sessionId, $noTrans, $jobId, $siteId) {
        $logFileError = "informa/informa_transaction_delivery_detail_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $logReq = [
            "noTrans" => $noTrans,
            "jobId" => $jobId,
            "siteId" => $siteId,
        ];

        $deliveryInfo = $informaWebAPI->deliveryDetail($auth, $noTrans, $jobId, $siteId);
        if (! $deliveryInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "deliveryDetail", $logReq, $deliveryInfo, $uid, $this->errorMessages);
            return false;
        }

        if (isset($deliveryInfo["dlv_status"])) {
            $deliveryInfo["dlv_status_desc"] = $this->getDeliveryInfo($deliveryInfo["dlv_status"]);
        }

        return $deliveryInfo;
    }

    public function getDeliveryInfo($deliveryStatus) {
        switch ($deliveryStatus) {
            case "0":
                $deliveryInfoDesc = "Menunggu";break;
            case "1":
                $deliveryInfoDesc = "Siap Kirim";break;
            case "2":
                $deliveryInfoDesc = "Dalam Perjalanan";break;
            case "3":
                $deliveryInfoDesc = "Terkirim";break;
            case "4":
                $deliveryInfoDesc = "Dibatalkan";break;
            case "5":
                $deliveryInfoDesc = "Ditunda";break;
            case "6":
            case "99":
                $deliveryInfoDesc = "Dikirimkan oleh Ekspedisi";break;
            default:
                $deliveryInfoDesc = "";
        }
        return $deliveryInfoDesc;
    }

    public function satisfactionRateCustomerInforma($uid, $sessionId, $requestParams = array()) {
        $logFileError = "informa/informa_satisfaction_rate_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($requestParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $requestParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $csatInfo = $informaWebAPI->postSatisfactionRate($auth, $encryptedPayload);
        if (! $csatInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "postSatisfactionRate", $requestParams, $csatInfo, $uid, $this->errorMessages);
            return false;
        }

        return $csatInfo;
    }

    public function verifyEmailCustomerInforma($uid, $sessionId) {
        $logFileError = "informa/informa_verify_email_error";
        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataInforma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataInforma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        $verifyParams = [
            "P_MEMBER_NAME" => $memberData["P_MEMBER_NAME"],
            "P_EMAIL" => $memberData["P_EMAIL"],
            "P_CUST_ID" => $memberData["P_CUST_ID"],
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $verifyEmailInfo = $informaWebAPI->verifyEmail($auth, $encryptedPayload);
        if (! $verifyEmailInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "verifyEmail", $verifyParams, $verifyEmailInfo, $uid, $this->errorMessages);
            return false;
        }

        return $verifyEmailInfo;
    }

    public function sendChangePhoneOTPCustomerInforma($uid, $sessionId, $newPhone) {
        $logFile = "informa/informa_send_change_phone_otp";
        $logFileError = "informa/informa_send_change_phone_otp_error";
        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataInforma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataInforma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        // Format Phone
        $hoPhone = $this->formatPhoneNumberHO($newPhone);

        $verifyParams = [
            "P_NEW_HP" => $hoPhone,
            "P_EMAIL" => $memberData["P_EMAIL"],
            "P_CARD_ID" => $memberData["P_CARD_ID"],
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $changePhoneOTPResult = $informaWebAPI->informaSendChangePhoneOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "informaSendChangePhoneOTP", $verifyParams, $changePhoneOTPResult, $uid);
        if (! $changePhoneOTPResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaSendChangePhoneOTP", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        return $changePhoneOTPResult;
    }

    public function submitChangePhoneOTPCustomerInforma($uid, $sessionId, $newPhone, $otp) {
        $logFile = "informa/informa_submit_change_phone_otp";
        $logFileError = "informa/informa_submit_change_phone_otp_error";
        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataInforma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataInforma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        // Format Phone
        $hoPhone = $this->formatPhoneNumberHO($newPhone);
        
        $verifyParams = [
            "P_NEW_HP" => $hoPhone,
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_ID" => $memberData["P_CUST_ID"],
            "P_CUST_OTP" => $otp,
        ];

        $memberPhone = $this->ruparupaFormatPhone($memberData["P_NO_HP"]);
        if (strtolower($newPhone) == strtolower($memberPhone)) {
            $this->logReqRes($logFile, "informaSkipSubmitChangePhoneOTP", $verifyParams, $memberData, $uid);
            return true;
        }

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $changePhoneOTPResult = $informaWebAPI->informaSubmitChangePhoneOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "informaSubmitChangePhoneOTP", $verifyParams, $changePhoneOTPResult, $uid);
        if (! $changePhoneOTPResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaSubmitChangePhoneOTP", $verifyParams, $changePhoneOTPResult, $uid, $this->errorMessages);
            return false;
        }

        return $changePhoneOTPResult;
    }

    public function verifyPhoneCustomerInforma($uid, $sessionId) {
        $logFile = "informa/informa_verify_phone";
        $logFileError = "informa/informa_verify_phone_error";
        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataInforma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataInforma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        $verifyParams = [
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_HP" => $memberData["P_NO_HP"],
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $verifyPhoneInfo = $informaWebAPI->verifyPhone($auth, $encryptedPayload);
        $this->logReqRes($logFile, "verifyPhone", $verifyParams, $verifyPhoneInfo, $uid);
        if (! $verifyPhoneInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "verifyPhone", $verifyParams, $verifyPhoneInfo, $uid, $this->errorMessages);
            return false;
        }

        return $verifyPhoneInfo;
    }

    public function sendChangeEmailOTPCustomerInforma($uid, $sessionId, $newEmail) {
        $logFile = "informa/informa_send_change_email_otp";
        $logFileError = "informa/informa_send_change_email_otp_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataInforma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataInforma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        $verifyParams = [
            "P_NEW_EMAIL" => $newEmail,
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_HP" => $memberData["P_NO_HP"],
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $sendChangeEmailResult = $informaWebAPI->informaSendChangeEmailOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "informaSendChangeEmailOTP", $verifyParams, $sendChangeEmailResult, $uid);
        if (! $sendChangeEmailResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();

            $this->logReqRes($logFileError, "informaSendChangeEmailOTP", $verifyParams, $sendChangeEmailResult, $uid, $this->errorMessages);
            return false;
        }

        return $sendChangeEmailResult;
    }

    public function submitChangeEmailOTPCustomerInforma($uid, $sessionId, $newEmail, $otp) {
        $logFile = "informa/informa_submit_change_email_otp";
        $logFileError = "informa/informa_submit_change_email_otp_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataInforma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataInforma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }
    
        $verifyParams = [
            "P_NEW_EMAIL" => $newEmail,
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_ID" => $memberData["P_CUST_ID"],
            "P_CUST_OTP" => $otp,
        ];

        if (strtolower($newEmail) == strtolower($memberData["P_EMAIL"])) {
            $this->logReqRes($logFile, "informaSkipSubmitChangeEmailOTP", $verifyParams, $memberData, $uid);
            return true;
        }

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getMemberDataInforma", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $changeEmailOTPResult = $informaWebAPI->informaSubmitChangeEmailOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "informaSubmitChangeEmailOTP", $verifyParams, $changeEmailOTPResult, $uid);
        if (! $changeEmailOTPResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaSubmitChangeEmailOTP", $verifyParams, $changeEmailOTPResult, $uid, $this->errorMessages);
            return false;
        }

        return $changeEmailOTPResult;
    }

    public function verifyPhoneSubmitOTPCustomerInforma($uid, $sessionId, $otp) {
        $logFile = "informa/informa_submit_verify_phone_otp";
        $logFileError = "informa/informa_submit_verify_phone_otp_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataInforma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataInforma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }
    
        $verifyParams = [
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_ID" => $memberData["P_CUST_ID"],
            "P_CUST_OTP" => $otp,
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $changeEmailOTPResult = $informaWebAPI->informaVerifyPhoneSubmitOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "informaVerifyPhoneSubmitOTP", $verifyParams, $changeEmailOTPResult, $uid);
        if (! $changeEmailOTPResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaVerifyPhoneSubmitOTP", $verifyParams, $changeEmailOTPResult, $uid, $this->errorMessages);
            return false;
        }

        return $changeEmailOTPResult;
    }

    public function getRedeemVoucherListCustomerInforma($uid, $sessionId) {
        $logFileError = "informa/informa_get_redeem_voucher_list_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $informaResult = $informaWebAPI->informaRedeemVoucherList($auth);
        if (! $informaResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaRedeemVoucherList", $auth, $informaResult, $uid, $this->errorMessages);
            return false;
        }

        return $informaResult;
    }

    public function redeemVoucherSendOTPCustomerInforma($uid, $sessionId) {
        $logFile = "informa/informa_redeem_voucher_send_otp";
        $logFileError = "informa/informa_redeem_voucher_send_otp_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $verifyParams = [
            "P_UID" => $uid,
            "P_REQUEST_TYPE" => "1",
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $informaResult = $informaWebAPI->informaRedeemVoucherSendOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "informaRedeemVoucherSendOTP", $verifyParams, $informaResult, $uid);
        if (! $informaResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaRedeemVoucherSendOTP", $verifyParams, $informaResult, $uid, $this->errorMessages);
            return false;
        }

        return $informaResult;
    }

    public function redeemVoucherSubmitOTPCustomerInforma($uid, $sessionId, $voucherCode, $otp) {
        $logFile = "informa/informa_redeem_voucher_submit_otp";
        $logFileError = "informa/informa_redeem_voucher_submit_otp_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $verifyParams = [
            "voucherid" => $voucherCode,
            "P_CUST_OTP" => $otp,
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $informaResult = $informaWebAPI->informaRedeemVoucherSubmitOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "informaRedeemVoucherSubmitOTP", $verifyParams, $informaResult, $uid);
        if (! $informaResult) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaRedeemVoucherSubmitOTP", $verifyParams, $informaResult, $uid, $this->errorMessages);
            return false;
        }

        return $informaResult;
    }


    public function changeProfileEmailCustomerInforma($uid, $sessionId, $customerId, $oldEmail, $newEmail) {
        $logFile = "informa/informa_change_profile_email";
        $logFileError = "informa/informa_change_profile_email_error";

        if (strtolower($oldEmail) == strtolower($newEmail)) {
            $this->logReqRes($logFile, "informaSkipChangeProfileEmailCustomerInforma", $oldEmail, $newEmail, $uid);
            return true;
        }

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $customerModel = new \Models\Customer();
        $manager = new TxManager();
        $manager->setDbService($customerModel->getConnection());
        $transaction = $manager->get();
        
        try {
            // save data to Customer
            $customerModel = $customerModel->findFirst("customer_id = '" . $customerId . "'");
            $customerModel->email = $newEmail;
            $customerModel->updated_at = date("Y-m-d H:i:s");
            $customerModel->save();
            $customerModel->setTransaction($transaction);

            // Change informa data
            $payload = [
                [
                    'name'=>'p_uid',
                    'contents'=>$uid,
                    'Content-type' => 'multipart/form-data',    
                ],
                [
                    'name'=>'p_old_email',
                    'contents'=>$oldEmail,
                    'Content-type' => 'multipart/form-data',    
                ],
                [
                    'name'=>'p_new_email',
                    'contents'=>$newEmail,
                    'Content-type' => 'multipart/form-data',    
                ],
            ];

            $informaResult = $informaWebAPI->informaChangeProfileEmail($auth, $payload);
            if (! $informaResult) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "informaChangeProfileEmail", $payload, $informaResult, $uid, $this->errorMessages);
                $transaction->rollback($this->errorMessages);    
                return false;
            }

        }   catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        return true;
    }

    public function changeProfilePhoneCustomerInforma($uid, $sessionId, $customerId, $newPhone) {
        $logFileError = "informa/informa_change_profile_email_error";

        $phone = $this->ruparupaFormatPhone($newPhone);
        $formattedPhone = $this->formatPhoneNumber($phone);

        if (!$formattedPhone) {
           $this->errorCode = 400;
           $this->errorMessages = "Format nomor handphone tidak valid";
           return false;
        }

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $customerModel = new \Models\Customer();
        $manager = new TxManager();
        $manager->setDbService($customerModel->getConnection());
        $transaction = $manager->get();
        
        try {
            // save data to Customer
            $customerModel = $customerModel->findFirst("customer_id = '" . $customerId . "'");
            $customerModel->phone = $phone;
            $customerModel->updated_at = date("Y-m-d H:i:s");
            $customerModel->save();
            $customerModel->setTransaction($transaction);

            // Format Phone
            $hoPhone = $this->formatPhoneNumberHO($phone);

            $profilePayload = [
                'nohp' => $hoPhone,
                'city' => '',
                'address' => '',
                'fname' => '',
            ];

            $encryptedProfilePayload = $informaWebAPI->informaWebAPIEncrypt($profilePayload);
            if (!$encryptedProfilePayload) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "informaWebAPIEncrypt", $profilePayload, $encryptedProfilePayload, $uid, $this->errorMessages);
                $transaction->rollback($this->errorMessages);
              
                return false;
            }

            $successUpdateProfile = $informaWebAPI->editProfile($encryptedProfilePayload, $auth);
            if (! $successUpdateProfile) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "editProfile", $profilePayload, $successUpdateProfile, $uid, $this->errorMessages);
                $transaction->rollback($this->errorMessages);

                return false;
            }


        }   catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        return true;
    }

    public function transactionInstallationListCustomerInforma($uid, $sessionId, $receiptNo) {
        $logFileError = "informa/informa_transaction_installation_list_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $checkResult = $this->checkReceiptNoCustomerInforma($auth, $receiptNo);
        if (!$checkResult) {
            return false;
        }

        $txDetailInfo = $informaWebAPI->installationList($auth, $receiptNo);
        if (! $txDetailInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "installationList", $receiptNo, $txDetailInfo, $uid, $this->errorMessages);

            return false;
        }

        if (count($txDetailInfo) < 1) {
            $this->errorCode = 404;
            $this->errorMessages = "No Data Found";
            $this->logReqRes($logFileError, "installationList", $receiptNo, $txDetailInfo, $uid, $this->errorMessages);

            return false;
        }

        foreach($txDetailInfo as $index => $item) {
            if (isset($item["ins_status"])) {
                $txDetailInfo[$index]["ins_status_desc"] = $this->getDeliveryInfo($item["ins_status"]);
            }
        }

        return $txDetailInfo;
    }


    public function transactionInstallationDetailCustomerInforma($uid, $sessionId, $noTrans, $jobId) {
        $logFileError = "informa/informa_transaction_installation_detail_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $logReq = [
            "noTrans" => $noTrans,
            "jobId" => $jobId,
        ];

        $installationInfo = $informaWebAPI->installationDetail($auth, $noTrans, $jobId);
        if (! $installationInfo) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "installationDetail", $logReq, $installationInfo, $uid, $this->errorMessages);

            return false;
        }

        if (isset($installationInfo["ins_status"])) {
            $installationInfo["ins_status_desc"] = $this->getInstallationInfo($installationInfo["ins_status"]);
        }

        return $installationInfo;
    }

    public function getInstallationInfo($installationStatus) {
        switch ($installationStatus) {
            case "0":
                $installationInfo = "Menunggu";break;
            case "1":
                $installationInfo = "Siap Berangkat";break;
            case "2":
                $installationInfo = "Dalam Perjalanan";break;
            case "3":
                $installationInfo = "Terpasang";break;
            case "4":
                $installationInfo = "Dibatalkan";break;
            case "5":
                $installationInfo = "Ditunda";break;
            case "6":
                $installationInfo = "Terpasang - Belum Lengkap";break;
            default:
                $installationInfo = "";
        }

        return $installationInfo;
    }

    public function payWithPointCustomerInforma($uid, $sessionId, $otp) {
        $logFile = "informa/informa_pay_with_point";
        $logFileError = "informa/informa_pay_with_point_error";
        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $payload = [
            'P_UID' => $uid,
            'OTP' => $otp,
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $payload, $encryptedPayload, $uid, $this->errorMessages);
            $transaction->rollback($this->errorMessages);
          
            return false;
        }
       
        $result = $informaWebAPI->informaPayWithPoint($auth, $encryptedPayload);
        $this->logReqRes($logFile, "informaPayWithPoint", $payload, $result, $uid);
        if (! $result) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaPayWithPoint", $payload, $result, $uid, $this->errorMessages);

            return false;
        }

       return $result;
    }

    public function getReceiptListCustomerInforma($uid, $sessionId, $page) {
        $logFileError = "informa/informa_get_receipt_list_error";

        $informaWebAPI = new \Library\InformaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $result = $informaWebAPI->informaGetReceiptList($auth, $page);
        if (! $result) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaGetReceiptList", $page, $result, $uid, $this->errorMessages);

            return false;
        }

       return $result;
    }

    public function getSexString($sexId) {
        $result = "";
        switch ($sexId) {
            case 0:
                $result = "female";break;
            case 1:
                $result = "male";break;
            default:
                break;
        }

        return $result;
    }

    public function getSexId($sexString) {
        $result = -1;
        switch ($sexString) {
            case "female":
                $result = 0;break;
            case "male":
                $result = 1;break;
            default:
                break;
        }

        return $result;

    }

    public function getPointHistoryCustomerInforma($customerId, $page) {
        $logFileError = "informa/informa_get_point_history_error";
        $informaWebAPI = new \Library\InformaWebAPI();

        $endDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime($endDate . ' +1 day'));

        $payload = [
            "P_BU_Code" => "HCI",
            "P_Cust_Id" => $customerId,
            "P_Page_No" => $page,
            "P_Page_Size" => "10",
            "P_Date_From" => "1900-01-01",
            "P_Date_To" => $endDate
        ];

        $result = $informaWebAPI->informaGetPointHistory($payload);
        if (! $result) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaGetPointHistory", $payload, $result, $customerId, $this->errorMessages);

            return false;
        }

       return $result;

    }


    public function isEmailAvailable($email, $customerId) {

        try {
            $customer = new \Models\Customer();
            $param['conditions'] = "email = '".$email."' AND status = 10 AND company_code = 'HCI' AND customer_id != " . $customerId;
            $result = $customer->findFirst($param);    

        }catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage() ? $e->getMessage() : "Error saat mendapatkan customer";
        }

        return empty($result); 
    }

    public function isPhoneAvailable($phone, $customerId) {

        $rupaRupaPhone = $this->ruparupaFormatPhone($phone);

        try {
            $customer = new \Models\Customer();
            $param['conditions'] = "phone = '".$rupaRupaPhone."' AND status = 10 AND company_code = 'HCI' AND customer_id != " . $customerId;
            $result = $customer->findFirst($param);    

        }catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage() ? $e->getMessage() : "Error saat mendapatkan customer";
        }

        return empty($result); 
    }

    public function getMembershipDataInforma($informaCustomerID) {
        $logFileError = "informa/informa_get_membership_data_error";

        $informaWebAPI = new \Library\InformaWebAPI();

        $payload = [
            "P_Cust_Id" => $informaCustomerID,
        ];

        $result = $informaWebAPI->informaGetMembershipData($payload);
        if (! $result) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaGetMembershipData", $payload, $result, $informaCustomerID, $this->errorMessages);

            return false;
        }

        $resultFiltered = [];
        if (count($result["rows"]) > 0) {
            $resultFiltered = [
                "Exp_Date" => $result["rows"][0]["Exp_Date"],
                "Cust_Add" => $result["rows"][0]["Cust_Add"],
                "AutoRen" => $result["rows"][0]["AutoRen"],
                "Card_Ref" => $result["rows"][0]["Card_Ref"],
                "Ref_Date" => $result["rows"][0]["Ref_Date"],
                "Reg_Date" => $result["rows"][0]["Reg_Date"],
                "cust_poscode" => $result["rows"][0]["cust_poscode"],
                "Jml_Point" => $result["rows"][0]["Jml_Point"],
                "Point_exp" => $result["rows"][0]["Point_exp"],
                "point_exp_date" => $result["rows"][0]["point_exp_date"],
                "member_type" => $result["rows"][0]["member_type"],
                "description" => $result["rows"][0]["description"],
                "tier_exp_date" => $result["rows"][0]["tier_exp_date"],
                "total_trans_period" => $result["rows"][0]["total_trans_period"],
                "Flag_ultimate" => $result["rows"][0]["Flag_ultimate"],
            ];
        }

       return $resultFiltered;
    }

    public function checkCustomerWithUpdateInformaMemberSKU($customerId)
    {
        $salesOrderModel = new \Models\SalesOrder();
        
        $query = "SELECT \Models\SalesOrderItem.sku FROM \Models\SalesOrder
                    INNER JOIN \Models\SalesOrderItem
                WHERE \Models\SalesOrder.customer_id = $customerId 
                AND \Models\SalesOrder.status != 'canceled'
                AND \Models\SalesOrderItem.sku = '".getenv("MEMBERSHIP_HCI_SKU")."' LIMIT 1";
        
        $manager = $salesOrderModel->getModelsManager();
        $salesOrder = $manager->executeQuery($query);
        if (sizeof($salesOrder) === 0) {
            return true;
        }

        return false;
    }

    public function getCheckEmailCustomerInforma($email) {
        $logFile = "informa/informa_get_check_email";
        $logFileError = "informa/informa_get_check_email_error";

        $informaWebAPI = new \Library\InformaWebAPI();

        $payload = [
            "P_Cust_Mail" => $email,
            "P_Company_Type" => "I",
        ];

        $result = $informaWebAPI->informaGetCheckEmail($payload);
        $this->logReqRes($logFile, "informaGetCheckEmail", $payload, $result);
        if (! $result) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaGetCheckEmail", $payload, $result, "", $this->errorMessages);

            return false;
        }

        return $result;
    }

    public function postConnectMemberCustomerInforma($customerId, $auth, $fullName, $email, $irNumber, $custId) {
        $logFile = "informa/informa_post_connect_member";
        $logFileError = "informa/informa_post_connect_member_error";
        $informaWebAPI = new \Library\InformaWebAPI();

        $payload = [
            "cardid" => $irNumber,
            "fname" => $fullName,
            "email" => $email,
            "ismilist" => "0",
        ];

        $encryptedPayload = $informaWebAPI->informaWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaWebAPIEncrypt", $payload, $encryptedPayload, $customerId, $this->errorMessages);
            return false;
        }

        // Transaction
        $customerDetailModel = new \Models\CustomerInformaDetails();
        $manager = new TxManager();
        $manager->setDbService($customerDetailModel->getConnection());
        $transaction = $manager->get();        
        $result = [];

        try {
            $customerDetailModel = $customerDetailModel->findFirst("customer_id = '" . $customerId . "'");
            
            $customerDetailModel->updated_at = date("Y-m-d H:i:s");
            $customerDetailModel->setTransaction($transaction);

            // TODO: try catch logging
            $result = $informaWebAPI->informaPostConnectMember($auth, $encryptedPayload);
            $this->logReqRes($logFile, "informaPostConnectMember", $payload, $result, $customerId);
            if (! $result) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "informaPostConnectMember", $payload, $result, $customerId, $this->errorMessages);
                $transaction->rollback();
                return false;
            }

            $customerDetailModel->informa_customer_id = $custId;

            if (substr($irNumber, 0, 3) == "TIM") {
                $customerDetailModel->informa_customer_tmp_member_no = isset($result["cardid"]) ? $result["cardid"] : "";
            } else {
                $customerDetailModel->informa_customer_member_no = isset($result["cardid"]) ? $result["cardid"] : "";
            }

            $customerDetailModel->save();
            $customerDetailModel->setTransaction($transaction);

        }catch (\Exception $e) {
            return false;
        }

        $transaction->commit();

       return $result;
    }

    public function postRegisterMemberCustomerInforma($customerData) {
        $logFile = "informa/informa_register_member";
        $logFileError = "informa/informa_register_member_error";
        $aceWebAPI = new \Library\AceWebAPI();

        $fullName = $this->getFullName($customerData["first_name"], isset($customerData["last_name"]) ? $customerData["last_name"] : "");

        $sex = "0";
        if (isset($customerData["gender"])) {
            $sex = $this->getSexId($customerData["gender"]);
        }

        $customerDataInforma = $this->getCustomerDetail("customer_id = '" . $customerData["customer_id"] . "'");
        if(! $customerDataInforma) {
            return false;
        }

        $ktp = "";
        if (isset($customerDataInforma["ktp"])) {
            $ktp = $customerDataInforma["ktp"];
        }

        $statusMarital = "";
        if (isset($customerDataInforma["marital_status"])) {
            $statusMarital = $customerDataInforma["marital_status"];
        }


        $payload = [
            "P_Company" => "HCI",
            "P_Cust_Name" => $fullName,
            "P_Cust_Add" => "",
            "P_Cust_Hp" => isset($customerData["phone"]) ? $this->formatPhoneNumberHO($customerData["phone"]) : "",
            "P_Cust_Mail" => $customerData["email"],
            "P_Cust_Birth_Date" => isset($customerData["birth_date"]) ? $customerData["birth_date"] : "",
            "P_Cust_Birth_Place" => "",
            "P_Cust_Sex" => strval($sex),
            "P_Cust_City" => "",
            "P_Cust_PosCode" => "",
            "P_Cust_Add_Ktp" => "",
            "P_Cust_Phone" => "",
            "P_Cust_IdCard" => $ktp,
            "P_Cust_Religion" => "",
            "P_Cust_Stat_Marital" => $statusMarital,
            "P_Cust_citizenship" => "",
            "P_Cust_Employ" => "",
            "P_Cust_Purpose" => "",
            "P_Cust_AutoRen" => "",
            "P_Card_Ref" => "",
            "P_Reference_Date" => "",
            "P_Company_Code" => "00",
            "P_Cust_Ref" => "APP",
            "P_Cust_FreeF" => "0"
        ];

        // Encrypted Payload
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "aceWebAPIEncrypt", $payload, $encryptedPayload, $customerData["customer_id"], $this->errorMessages);
            return false;
        }

        // Transaction
        $customerDetailModel = new \Models\CustomerInformaDetails();
        $manager = new TxManager();
        $manager->setDbService($customerDetailModel->getConnection());
        $transaction = $manager->get();
        $result = [];

        try {
            // save data to Customer
            $customerDetailModel = $customerDetailModel->findFirst("customer_id = '" . $customerData["customer_id"] . "'");
            $customerDetailModel->setTransaction($transaction);

            $result = $aceWebAPI->aceRegisterCustomer($encryptedPayload);
            $this->logReqRes($logFile, "aceRegisterCustomer", $payload, $result, $customerData["customer_id"]);
            if (!$result) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "aceRegisterCustomer", $payload, $result, $customerData["customer_id"], $this->errorMessages);
                $transaction->rollback();
                return false;
            }

            $customerDetailModel->informa_customer_tmp_member_no = isset($result[0]["Card"]) ? $result[0]["Card"] : "";
            $customerDetailModel->informa_customer_id = isset($result[0]["Cust_Id"]) ? $result[0]["Cust_Id"] : "";

            $customerDetailModel->updated_at = date("Y-m-d H:i:s");
            $customerDetailModel->save();
            $customerDetailModel->setTransaction($transaction);

        }catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

       return $result;
    }

    public function updateTemporaryMemberCustomerInforma($custDetailID, $checkEmailData) {
        $logFileError = "informa/informa_update_tmp_member_error";

        $customerInformaDetailsEntities = [
            "customer_informa_details_id" => $custDetailID,
            "informa_customer_id" => $checkEmailData["cust_id"],
            "informa_customer_tmp_member_no" => $checkEmailData["card_id"],
            "updated_at" => date("Y-m-d H:i:s"),
        ];

        $this->customerInformaDetails->setFromArray($customerInformaDetailsEntities);
        if (!$this->customerInformaDetails->saveData("informa/informa_update_tmp_member")) {
            $this->errorCode = $this->customerInformaDetails->getErrorCode();
            $this->errorMessages = $this->customerInformaDetails->getErrorMessages();
            $this->logReqRes($logFileError, "saveData", $customerInformaDetailsEntities, false, "", $this->errorMessages);
            return false;
        }

        return true;
    }

    public function transactionInstallationRouteCustomerInforma($auth, $notrans, $jobid) {
        $logFileError = "informa/informa_get_installation_route_error";

        $informaWebAPI = new \Library\InformaWebAPI();

        $logReq = [
            "notrans" => $notrans,
            "jobid" => $jobid,
        ];

        $result = $informaWebAPI->informaInstallationRoute($auth, $notrans, $jobid);
        if (! $result) {
            $this->errorCode = $informaWebAPI->getErrorCode();
            $this->errorMessages = $informaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "informaInstallationRoute", $logReq, $result, $auth["uid"], $this->errorMessages);

            return false;
        }

       return $result;
    }

    function logReqRes($logfile, $prefix = "", $req = false, $res = false, $custId = "", $errMsg = "") {
        $doLog = true;
        $logType = "info";
        $excludedErrMsg = [];
 
        if (substr($logfile, -6) == "_error") {
            $logType = "error";
        }

        if (isset($req["data"])) {
            $req["data"] = "***";
        }

        if (isset($req["sessionid"])) {
            $req["sessionid"] = "***";
        }

        if (isset($req["password"])) {
            $req["password"] = "***";
        }

        if (isset($req["passwd"])) {
            $req["passwd"] = "***";
        }

        if (isset($req["oldpasswd"])) {
            $req["oldpasswd"] = "***";
        }

        if (isset($req["newpasswd"])) {
            $req["newpasswd"] = "***";
        }

        if (isset($res["password"])) {
            $res["password"] = "***";
        }

        $log = $prefix . " -- Request: " . json_encode($req) . " -- Response: " . json_encode($res);
        if ($custId != "") {
            $log = $log . " -- Customer ID: " . $custId;
        }
        if ($errMsg != "") {
            $log = $log . " -- ErrMessage: " . $errMsg;
            if (in_array($errMsg, $excludedErrMsg)) {
                $doLog = false;
            }
        }
        if (!empty($this->appVersion)) {
            $log = $log . " -- AppVersion: " . $this->appVersion;
        }

        if ($doLog) {
            LogHelper::log($logfile, $log, $logType);
            if ($logType == "error" && getenv('SLACK_ERROR_INFORMA') == "true") {
                $formattedLog = $logfile . " -- " . $log;

                $this->sendSlackNotif($formattedLog, getenv('SLACK_ERROR_INFORMA_CHANNEL'), getenv('OPS_ISSUE_SLACK_USERNAME'));
                $this->sendDiscordNotif(getenv('DISCORD_HOOK_INFORMA'), $formattedLog, getenv('DISCORD_HOOK_INFORMA_USERNAME'));
            }
        }
    }

    public function sendSlackNotif($message, $channel, $username)
    {
        $notificationText = $message;
        $params = [
            "channel" => $channel,
            "username" => $username,
            "text" => $message,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    public function sendDiscordNotif($webhookUrl, $message, $username) {
        $notificationText = $message;
        $params = [
            "username" => $username,
            "content" => $message,
        ];

        // call queue for webhook discord
        $nsq = new \Library\Nsq();
        $message = [
            "webhook_url" => $webhookUrl,
            "data" => $params,
            "message" => "discordNotif"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    public function setCustomer2Group($customerId, $memberNo, $expiredDate) {
        $logFileError = "informa/informa_set_customer_2_group_error";
        $nowDate = new \DateTime("now");
        $expDate = new \DateTime($expiredDate);    
        $customerEntities = [
            "customer_id" => $customerId,
            "group" => [
                "HCI" => $memberNo,
                "HCI_expiration_date" => $expDate->format('Y-m-d 23:59:59'),
                "HCI_is_verified" => $nowDate <= $expDate ? 10 : 5,
            ]
        ];
        $customerNewModel = new \Models\Customer();

        $customerNewModel->setFromArray($customerEntities);
        if (!$customerNewModel->saveData("informa/informa_set_customer_2_group")) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal memperbarui Customer Informa";
            $this->logReqRes($logFileError, "saveData", $customerEntities, false, $customerId, $this->errorMessages);
            return false;
        }

        return true;
    }

    public function isCurrentDate($date) {
        $dateNow = date("Y-m-d");
        return $date == $dateNow;
    }
    
    public function postPwpSendOTPInforma($informaAuth, $customerId, $phoneNo, $cardId, $redeemPoint, $cartId, $skipCart) {
        $logFile = "informa/informa_pwp_send_otp";
        $logFileError = "informa/informa_pwp_send_otp_error";

        $validate = $this->validatePwpOTPInforma($informaAuth, $redeemPoint, $cartId, $skipCart);
        if (!$validate) {
            $this->logReqRes($logFileError, "validatePwpOTPInforma", $informaAuth, $validate, $customerId, $this->errorMessages);
            return false;
        }

        $informaWebAPI = new \Library\InformaWebAPI();
        $aceWebAPI = new \Library\AceWebAPI();
        $informaOtpTempModel = new \Models\CustomerInformaOtpTemp();

        $formattedPhone = $this->formatPhoneNumberAll($phoneNo);
        $otp = GeneralHelper::generateRandomNumber(6);

        $manager = new TxManager();
        $manager->setDbService($informaOtpTempModel->getConnection());
        $transaction = $manager->get();

        $otpType = "pay_with_point";
        if ($skipCart) {
            $otpType = "redeem_voucher_online";
        }
        
        try {
            $validate = $informaOtpTempModel->validateInformaSendOTP($customerId, "phone", $formattedPhone, $otp, $otpType, $transaction, $validateCount);
            if (getenv("INFORMA_ENABLE_OTP_LIMIT_DAILY")) {
                if ($skipCart) {
                    $remainingSubmitOTP = getenv("INFORMA_REDEEM_ONLINE_VOUCHER_OTP_LIMIT_DAILY") - $validateCount;
                } else {
                    $remainingSubmitOTP = getenv("INFORMA_PWP_OTP_LIMIT_DAILY") - $validateCount;
                }
            } else {
                $remainingSubmitOTP = 1;
            }

            if (!$validate) {
                $this->errorCode = $informaOtpTempModel->getErrorCode();
                $this->errorMessages = $informaOtpTempModel->getErrorMessages();
                $this->errorData = [
                    "phone" => $formattedPhone,
                    "cooldown_sec" => $informaOtpTempModel->getCountdownSec(),
                    "remaining_submit_otp" => $remainingSubmitOTP,
                ];
                
                $this->logReqRes($logFileError, "validateInformaSendOTP", $phoneNo, $validate, $customerId, $this->errorMessages);
                return false;
            }
            $request = [
                "P_BU_CODE" => "HCI",
                "P_NO_HP" =>  $formattedPhone,
                "P_TEMPLATE_TYPE" => "pay_with_point",
                "P_FROM" => "INFORMA_MOBILE_PAY_WITH_POINT",
                "P_CARD_ID" => $cardId,
                "P_OTP" => $otp,
                "P_PASSKEY" => ""
            ];
    
            // Encrypted Payload
            $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($request);
            if (!$encryptedPayload) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "aceWebAPIEncrypt", $request, $encryptedPayload, $customerId, $this->errorMessages);
                $transaction->rollback();
                return false;
            }
    
            $result = $informaWebAPI->informaPwpSendOTP($encryptedPayload);
            $this->logReqRes($logFile, "informaPwpSendOTP", $request, $result, $customerId);
            if (!$result) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "informaPwpSendOTP", $request, $result, $customerId, $this->errorMessages);
                $transaction->rollback();
                return false;
            }
        } catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        $otpResp = "";
        if (getenv('ENVIRONMENT') != 'production') {
            $otpResp = $otp;
        }

        $response = [
            "success" => $result,
            "phone" => $formattedPhone,
            "otp" => $otpResp,
            "remaining_submit_otp" => $remainingSubmitOTP,
        ];

        return $response;
    }

    public function postPwpSubmitOTPInforma($informaAuth, $customerId, $phoneNo, $cardId, $informaCustId, $otp, $redeemPoint, $cartId, $skipCart) {
        $logFile = "informa/informa_pwp_submit_otp";
        $logFileError = "informa/informa_pwp_submit_otp_error";

        $informaWebAPI = new \Library\InformaWebAPI();

        $validate = $this->validatePwpOTPInforma($informaAuth, $redeemPoint, $cartId, $skipCart);
        if (!$validate) {
            $this->logReqRes($logFileError, "validatePwpOTPInforma", $informaAuth, $validate, $customerId, $this->errorMessages);
            return false;
        }

        $informaOtpTempModel = new \Models\CustomerInformaOtpTemp();
        $customerPointRedeemModel = new \Models\CustomerPointRedeem();
        $salesRuleVoucherModel = new \Models\SalesruleVoucher();
        $customerPointRedeemModel->useWriteConnection();
        $salesRuleVoucherModel->useWriteConnection();

        $manager = new TxManager();
        $manager->setDbService($informaOtpTempModel->getConnection());
        $manager->setDbService($customerPointRedeemModel->getConnection());
        $manager->setDbService($salesRuleVoucherModel->getConnection());
        $transaction = $manager->get();

        $otpType = "pay_with_point";
        $ruleId = getenv('REDEEM_SALES_RULE_HCI');
        if ($skipCart) {
            $otpType = "redeem_voucher_online";
            $ruleId = getenv('REDEEM_ONLINE_VOUCHER_SALES_RULE_HCI');
        }
       
        // validate otp
        $formattedPhone = $this->formatPhoneNumberAll($phoneNo);
        try {
            $validate = $informaOtpTempModel->validateInformaSubmitOTP($customerId, "phone", $formattedPhone, $otp, $otpType, $transaction);
            if (!$validate) {
                $this->errorCode = $informaOtpTempModel->getErrorCode();
                $this->errorMessages = $informaOtpTempModel->getErrorMessages();
                $logData = [
                    "cart_id" => $cartId,
                    "phone" => $formattedPhone,
                    "otp" => $otp,
                    "otp_type" => $otpType,
                ];
                $this->logReqRes($logFileError, "validateInformaSubmitOTP", $logData, $validate, $customerId, $this->errorMessages);
                return false;
            }

            if (getenv("INFORMA_ENABLE_OTP_LIMIT_DAILY")) {
                if ($skipCart) {
                    $remainingSubmitOTP = getenv("INFORMA_REDEEM_ONLINE_VOUCHER_OTP_LIMIT_DAILY") - $validate;
                } else {
                    $remainingSubmitOTP = getenv("INFORMA_PWP_OTP_LIMIT_DAILY") - $validate;
                }
            } else {
                $remainingSubmitOTP = 1;
            }

            $voucherGeneratorParams = [
                'rule_id' => $ruleId,
                'customer_id' => $customerId,
                'company_code' => 'ODI',
                'type' => 5, // Type = redeem
                'expiration_date' => date("Y-m-d 23:59:59", strtotime("+6 month", time())),
                'qty' => 1,
                'voucher_amount' => (int)$redeemPoint * (int)getenv('POINT_VOUCHER_WORTH_HCI'),
                'length' => 7,
                'format' => 'alphanum',
                'prefix' => getenv('POINT_REDEEM_PREFIX_HO'),
            ];

            // Special Redeem Informa
            $informaSpecialRedeemStr = getenv('INFORMA_SPECIAL_REDEEM');
            $informaSpcRedeemStartDate = getenv("INFORMA_SPECIAL_REDEEM_START_DATE");
            $informaSpcRedeemEndDate = getenv("INFORMA_SPECIAL_REDEEM_END_DATE");
            $informaSpecialRedeemArr = '';
            $informaSpecialRedeem = false;
            $pointFactorInformaSpcRedeem = 0;

            if ($informaSpecialRedeemStr != '' && $informaSpcRedeemStartDate != '' && $informaSpcRedeemEndDate != '' && $skipCart) {
                $today = date("Y-m-d H:i:s");
                if ($today >= $informaSpcRedeemStartDate && $today <= $informaSpcRedeemEndDate) {
                    $informaSpecialRedeem = true;
                }
            }

            // Env format: point_redeemed=voucher_amount;point_redeemed=voucher_amount;... (Example: 100=300000;200=700000)
            if ($informaSpecialRedeem) {
                $informaSpecialRedeemArr = explode(";", $informaSpecialRedeemStr);
                for ($i = 0; $i < sizeof($informaSpecialRedeemArr); $i++) {
                    $specialRedeems = explode("=", $informaSpecialRedeemArr[$i]);
                    $pointInforma = $specialRedeems[0];
                    $voucherAmountInforma = $specialRedeems[1];

                    if ($redeemPoint == $pointInforma){
                        $voucherGeneratorParams['voucher_amount'] = $voucherAmountInforma;
                        $pointFactorInformaSpcRedeem = $voucherAmountInforma / $pointInforma; // Factor is how much money converted for every point used
                        $voucherGeneratorParams['expiration_date'] = date("Y-m-d 23:59:59", strtotime("+10 days", time()));
                    }
                }
            }

            $this->marketingLib->generateVoucher($voucherGeneratorParams);
            $this->marketingLib->validateGeneratedVoucher($voucherGeneratorParams);
    
            $voucherGeneratorParams['voucher_code'] = $this->marketingLib->getVoucherList()[0];

            // Save to table salesrule_voucher
            $salesRuleVoucherModel->assign($voucherGeneratorParams);
            if (!$salesRuleVoucherModel->save()) {
                $this->errorCode = 400;
                $this->errorMessages = implode('|', $salesRuleVoucherModel->getMessages());
                $this->logReqRes($logFileError, "saveSalesRuleVoucherModel", $voucherGeneratorParams, false, $customerId, $this->errorMessages);
                return false;
            }
            $salesRuleVoucherModel->setTransaction($transaction);

            $redeemModelParams = [
                'customer_id' => $customerId,
                'point_redeemed' => $redeemPoint,
                'sales_rule_voucher_id' => $salesRuleVoucherModel->getVoucherId(),
                'company_code' => 'HCI',
                'created_at' => date('Y-m-d')
            ];

            // Save to table customer_point_redeem
            $customerPointRedeemModel->assign($redeemModelParams);
            if (!$customerPointRedeemModel->save()) {
                $this->errorCode = 400;
                $this->errorMessages = implode('|', $customerPointRedeemModel->getMessages());
                $this->logReqRes($logFileError, "saveCustomerPointRedeemModel", $redeemModelParams, false, $customerId, $this->errorMessages);
                return false;
            }
            $customerPointRedeemModel->setTransaction($transaction);

            $now = new \DateTime("now");
            $pointRedeemed = (int)$redeemPoint * -1;
    
            // Adding '-' after CP
            $voucherAsParam = substr_replace($voucherGeneratorParams['voucher_code'], '-', 2, 0);

            $req = array(
                "P_company_type" => "I",
                "P_cust_id" => $informaCustId,
                "P_card_id" => $cardId,
                "P_receive_no" => $voucherAsParam, // ex: CP-AF789C78
                "P_jmlkenapoint" => "0", // Default when redeem point
                "P_factor" => getenv('POINT_VOUCHER_WORTH_HCI'), // Factor is how much money converted for every point used
                "P_reward" => "0", // Because customer doesn't get reward from this transaction
                "P_jmlpoint" => (string)$pointRedeemed,
                "P_createdate" => $now->format('Y-m-d H:i:s')
            );
      

            if ($informaSpecialRedeem) {
                $informaSpecialRedeemArr = explode(";", $informaSpecialRedeemStr);
                for ($i = 0; $i < sizeof($informaSpecialRedeemArr); $i++) {
                    $specialRedeems = explode("=", $informaSpecialRedeemArr[$i]);
                    $pointInforma = $specialRedeems[0];
                    $voucherAmountInforma = $specialRedeems[1];

                    if ($redeemPoint == $pointInforma){
                        $req['P_factor'] = (string)$pointFactorInformaSpcRedeem;
                        $voucherGeneratorParams['voucher_amount'] = $voucherAmountInforma;
                        $voucherGeneratorParams['expiration_date'] = date("Y-m-d 23:59:59", strtotime("+10 days", time()));
                    }
                }
            }

            $pointRedeemed = $informaWebAPI->insertPointInforma($req);
            $this->logReqRes($logFile, "insertPointInforma", $req, $pointRedeemed, $customerId);
            if (!$pointRedeemed) {
                $this->errorCode = $informaWebAPI->getErrorCode();
                $this->errorMessages = $informaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "insertPointInforma", $req, $pointRedeemed, $customerId, $this->errorMessages);
                return false;
            }
        }  catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        // Posting Journal 3601
        $this->kawanLamaSystemLib->postingJournal($salesRuleVoucherModel, "HCI");
        $this->logReqRes($logFile, "postingJournal", $salesRuleVoucherModel->toArray(), true, $customerId);

        if (!$skipCart) {
            $cartAPI = new \Library\Cart();
    
            $cartData = $cartAPI->postCartRedeemVoucher($cartId, $voucherGeneratorParams['voucher_code']);
            if (!$cartData) {
                $this->errorCode = $cartAPI->getErrorCode();
                $this->errorMessages = $cartAPI->getErrorMessages();
                $this->logReqRes($logFileError, "postCartRedeemVoucher", $cartId, $cartData, $customerId, $this->errorMessages);
    
                return false;
            }
        }

        $response = [
            "success" => true,
            "remaining_submit_otp" => $remainingSubmitOTP,
        ];

        return $response;
    }

    public function validatePwpOTPInforma($informaAuth, $redeemPoint, $cartId, $skipCart) {
        if (!$skipCart) {
            $cartAPI = new \Library\Cart();
    
            $cartData = $cartAPI->getCartByCartId($cartId);
            if (!$cartData) {
                $this->errorCode = $cartAPI->getErrorCode();
                $this->errorMessages = json_encode($cartAPI->getErrorMessages());
    
                return false;
            }
   
            $voucherAmount = (int)$redeemPoint * (int)getenv('POINT_VOUCHER_WORTH_HCI'); // n * 2500

            $calcTotal = (int)$cartData['subtotal'] - (int)$cartData['discount_amount'] - (int)$cartData['gift_cards_amount'];
            if ((int)$voucherAmount > (int)$calcTotal) {
                $this->errorCode = 400;
                $this->errorMessages = "Penukaran point tidak boleh melebihi harga yang harus dibayarkan";
    
                return false;
            }
        }

        $tieringData = $this->getTieringCustomerInforma($informaAuth["uid"], $informaAuth["sessionid"]);
        if(! $tieringData)
            return $this->sendErrorResponse($this->informaAppLib->getErrorCode(), $this->informaAppLib->getErrorMessages());

        $customerPoints = 0;
        $customerPoints = (int) $tieringData["point"];
        if ((int) $redeemPoint > $customerPoints) {
            $this->errorCode = 400;
            $this->errorMessages = "Point yang ditukarkan tidak boleh lebih dari poin yang dimiliki";

            return false;
        }        

        return true;
    }

    public function getCustomerVoucherDeals($customerId) {
        $params = [
            "customer_id" => $customerId,
            "include_type" => getenv('VOUCHER_TYPE_OFFLINE_DEALS'),
        ];
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($params);
        $customerLib->setConditionsFromArray($params);
        $voucherList = $customerLib->getVoucherList();

        return $voucherList;
    }

    public function appendDealsVoucher($storeVoucher, $dealsVouchers) {
        $result = [
            "totamount" => isset($storeVoucher["totamount"]) ? $storeVoucher["totamount"] : 0,
            "arrvoucher" => isset($storeVoucher["arrvoucher"]) ? $storeVoucher["arrvoucher"] : [],
        ];

        foreach ($dealsVouchers as $dealsVoucher) {
            if ($dealsVoucher["status"] == "Sudah Digunakan" || $dealsVoucher["status"] == "Kadaluarsa") {
                continue;
            }
            $spliced = false;
            $amt = (int)$dealsVoucher["voucher_amount"];
            $formattedDeals = [
                "data" => [
                    "vouchercode" => $dealsVoucher["voucher_code"],
                    "amount" => (string)$amt,
                    "voucher_category" => "store_voucher_deals",
                    "type" => $dealsVoucher["type"],
                    "expired_date" => $dealsVoucher["expiration_datetime"],
                    "tnc" => $dealsVoucher["tnc"],
                    "deals_data" => $dealsVoucher["deals_data"],
                ]
            ];

            foreach ($result["arrvoucher"] as $index => $voucher) {
                if ($formattedDeals["data"]["expired_date"] >= $voucher["expired_date"]) {
                    array_splice($result["arrvoucher"], $index, 0, $formattedDeals);
                    $result["totamount"]++;
                    $spliced = true;
                    break;
                }
            }

            if (!$spliced) {
                array_push($result["arrvoucher"], $formattedDeals["data"]);
                $result["totamount"]++;
            }
        }
        
        return $result;
    }

}