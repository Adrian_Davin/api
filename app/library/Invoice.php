<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Library;

use Helpers\InvoiceHelper;
use Library\Marketing as MarketingLib;
use \Datetime;
use Phalcon\Db;
use Library\Nsq;
use Library\ProductStock;
use \Helpers\CartHelper;
use \Helpers\LogHelper;
use \Helpers\GeneralHelper;
use \GuzzleHttp\Client as GuzzleClient;
use Phalcon\Db as Database;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Models\SalesB2BInformation as SalesB2BInfoModel;
use Models\SalesruleOrder;

class Invoice
{

    /**
     * @var $invoiceList \Models\Invoice\Collection
     */
    protected $invoiceList;

    /**
     * @var \Models\SalesInvoice
     */
    protected $salesInvoice;

    /**
     * @var \Models\SalesOrder
     */

    protected $fakturPajakInvoice;

    /**
     * @var \Models\FakturPajakInvoice
     */

    protected $salesOrder;

    protected $errorCode;
    protected $errorMessages;



    /**
     * @return mixed
     */
    public function getSalesInvoice()
    {
        return $this->salesInvoice;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    public function getAllInvoice($params = array())
    {
        $params['query_type'] = !empty($params['query_type']) ? $params['query_type'] : "";
        $invoiceModel = new \Models\SalesInvoice();

        $resultInvoice = $invoiceModel::query();
        $where_clause = "";
        $countWhere = 0;
        $countSorting = 0;
        $sorting = "";

        foreach ($params as $key => $param) {
            if (strpos($key, '_ordering') !== false) {
                $keyParams = str_replace("_ordering", "", $key);
                if ($keyParams == "status") {
                    $sort = "invoice_status";
                } else if ($keyParams == "order_date") {
                    $sort = "created_at";
                } else {
                    $sort = $keyParams;
                }

                $sorting = "\Models\SalesInvoice." . str_replace("_ordering", "", $sort) . " " . $param;
                unset($params[$key]);
                $countSorting++;
            }
        }

        //multiple search
        if (!empty($params['order_date'])) {
            $params['order_date'] = date("Y-m-d", strtotime($params['order_date']));
            $resultInvoice->andWhere("created_at >= '" . $params['order_date'] . " 00:00:00'");
            $resultInvoice->andWhere("created_at  <= '" . $params['order_date'] . " 23:59:59'");
            $countWhere++;
            unset($params['order_date']);
        }

        if (!empty($params['subtotal'])) {
            $resultInvoice->andWhere("subtotal = '" . $params['subtotal'] . "'");
            $where_clause .= ($countWhere > 0) ? " AND " : "";
            $where_clause .= " subtotal  = '" . $params['subtotal'] . "'";
            $countWhere++;
            unset($params['subtotal']);
        }

        if (!empty($params['order_no'])) {
            $resultInvoice->andWhere("sales_order_id IN (SELECT \Models\SalesOrder.sales_order_id FROM \Models\SalesOrder WHERE \Models\SalesOrder.order_no LIKE '%" . $params['order_no'] . "%')");
            $where_clause .= ($countWhere > 0) ? " AND " : "";
            $where_clause .= "sales_order_id IN (SELECT \Models\SalesOrder.sales_order_id FROM \Models\SalesOrder WHERE \Models\SalesOrder.order_no LIKE '%" . $params['order_no'] . "%')";
            unset($params['order_no']);
            $countWhere++;
        }

        if (!empty($params['order_type'])) {
            $resultInvoice->andWhere("sales_order_id IN (SELECT \Models\SalesOrder.sales_order_id FROM \Models\SalesOrder WHERE \Models\SalesOrder.order_type = '" . $params['order_type'] . "')");
            $where_clause .= ($countWhere > 0) ? " AND " : "";
            $where_clause .= "sales_order_id IN (SELECT \Models\SalesOrder.sales_order_id FROM \Models\SalesOrder WHERE \Models\SalesOrder.order_type = '" . $params['order_type'] . "')";
            unset($params['order_type']);
            $countWhere++;
        }

        if (!empty($params['customer_name'])) {
            $resultInvoice->andWhere("sales_order_id IN (SELECT \Models\SalesCustomer.sales_order_id FROM \Models\SalesCustomer WHERE ( \Models\SalesCustomer.customer_firstname LIKE '%" . $params['customer_name'] . "%' OR \Models\SalesCustomer.customer_lastname LIKE '%" . $params['customer_name'] . "%' ))");
            $where_clause .= ($countWhere > 0) ? " AND " : "";
            $where_clause .= "sales_order_id IN (SELECT \Models\SalesCustomer.sales_order_id FROM \Models\SalesCustomer WHERE ( \Models\SalesCustomer.customer_firstname LIKE '%" . $params['customer_name'] . "%' OR \Models\SalesCustomer.customer_lastname LIKE '%" . $params['customer_name'] . "%' ))";
            unset($params['customer_name']);
            $countWhere++;
        }

        if (!empty($params['shipment_type'])) {
            $resultInvoice->andWhere("invoice_id IN (SELECT \Models\SalesShipment.invoice_id FROM \Models\SalesShipment WHERE \Models\SalesShipment.shipment_type LIKE '%" . $params['shipment_type'] . "%')");
            $where_clause .= "invoice_id IN (SELECT \Models\SalesShipment.invoice_id FROM \Models\SalesShipment WHERE \Models\SalesShipment.shipment_type LIKE '%" . $params['shipment_type'] . "%')";
            unset($params['shipment_type']);
            $countWhere++;
        }

        if (isset($params['limit']) && isset($params['offset'])) {
            $limit = $params['limit'];
            $offset = $params['offset'];
            unset($params['limit']);
            unset($params['offset']);
        }
        //.end of multiple search

        foreach ($params as $key => $val) {
            if ($key == "date_from") {
                $resultInvoice->andWhere("created_at >= '" . $val . " 00:00:00'");
            } else if ($key == "date_to") {
                $resultInvoice->andWhere("created_at  <= '" . $val . " 23:59:59'");
            } else if ($key == "invoice_date") {
                $dateFrom = $val . " 00:00:00";
                $dateTo = $val . " 23:59:59";
                $resultInvoice->betweenWhere("created_at", $dateFrom, $dateTo);
            } else if ($key == "sap_so_number") {
                if (strtoupper($val) == 'NULL') {
                    $resultInvoice->andWhere("sap_so_number IS NULL");
                    $resultInvoice->andWhere("invoice_status <> 'canceled'");
                } else {
                    $resultInvoice->andWhere($key . " = '" . $val . "'");
                }
            } else if ($key == "supplier_alias") {
                if ($countWhere > 0) {
                    $where_clause .= " AND ";
                }

                if (strtoupper($val) == "MPMP") {
                    $resultInvoice->andWhere("supplier_alias LIKE 'MP%'");
                    $where_clause .= "supplier_alias LIKE 'MP%'";
                } else {
                    $resultInvoice->andWhere($key . " = '" . $val . "'");
                    $where_clause .= $key . " = '" . $val . "'";
                }
                $countWhere++;
            } elseif (isset($params['shipment_status'])) {
                $shipmentStatusArray = explode(',', $params['shipment_status']);
                $shipmentParams = array();
                foreach ($shipmentStatusArray as $value) {
                    if ($value == 'confirmed') {
                        $shipmentParams[] = "'new'";
                        $shipmentParams[] = "'shipped'";
                        $where_clause .= ($countWhere > 0) ? " AND " : " ";
                        $where_clause .= " \Models\SalesInvoice.invoice_status IN ('processing','stand_by','picked') ";
                        $resultInvoice->andWhere("\Models\SalesInvoice.invoice_status IN ('processing','stand_by','picked')");
                    } else if ($value == 'new') {
                        $shipmentParams[] = "'new'";
                        $shipmentParams[] = "'shipped'";
                        $where_clause .= ($countWhere > 0) ? " AND " : " ";
                        $where_clause .= " \Models\SalesInvoice.invoice_status = 'new' ";
                        $resultInvoice->andWhere("\Models\SalesInvoice.invoice_status = 'new'");
                    } else {
                        $shipmentParams[] = "'" . $value . "'";
                    }
                }

                $where_clause .= ($countWhere > 0) ? " AND " : "";
                $shipmentStrParams = implode(",", $shipmentParams);
                $where_clause .= "invoice_id IN (SELECT \Models\SalesShipment.invoice_id FROM \Models\SalesShipment WHERE \Models\SalesShipment.shipment_status IN (" . $shipmentStrParams . "))";
                $resultInvoice->andWhere("invoice_id IN (SELECT \Models\SalesShipment.invoice_id FROM \Models\SalesShipment WHERE \Models\SalesShipment.shipment_status IN (" . $shipmentStrParams . "))");
                $countWhere++;
            } else {
                $excludeArray = array('master_app_id', 'query_type', 'limit', 'offset');
                if (in_array($key, $excludeArray) == false) {
                    $resultInvoice->andWhere($key . " = '" . $val . "'");
                }
            }
        }

        $limitQuery = "";
        if (!empty($limit)) {
            $limitQuery = " LIMIT $limit OFFSET $offset ";
        }
        if (empty($sorting)) {
            $resultInvoice->orderBy("\Models\SalesInvoice.created_at DESC $limitQuery ");
        } else {
            $resultInvoice->orderBy($sorting);
        }
        $allInvoice = array();
        if ($params['query_type'] != "count_only") {
            $result = $resultInvoice->execute();
            foreach ($result as $key => $invoice) {
                $invoiceModel = new \Models\SalesInvoice();
                $invoiceModel->setFromArray($invoice->toArray(array(), true));

                $allInvoice[$key] = $invoiceModel->getDataArray([], true);
                $allInvoice[$key]['order_no'] = $invoice->SalesOrder->getOrderNo();
                $allInvoice[$key]['order_status'] = $invoice->SalesOrder->getStatus();
                if (!empty($invoice->SalesOrder->SalesCustomer)) {
                    $allInvoice[$key]['customer'] = $invoice->SalesOrder->SalesCustomer->toArray(array("customer_id", "customer_firstname", "customer_lastname", "customer_email", "customer_is_guest"));
                }

                // Revamp pickup voucher response format, unset old data
                unset($allInvoice[$key]['pickup_voucher']);

                $tempSalesComment = $invoice->SalesComment->toArray();
                $countChat = 0;
                if (count($tempSalesComment) > 0) {
                    foreach ($tempSalesComment as $valueComment) {
                        foreach ($valueComment as $keyComment => $itemComment) {
                            if ($keyComment == 'sales_comment_id') {
                                if ($valueComment['flag'] == 'chat') {
                                    $countChat++;
                                }
                            }
                            break;
                        }
                    }
                }

                $invoiceMdr = 0;
                foreach ($allInvoice[$key]['items'] as $invoiceItems => $invoiceItem) {
                    $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
                    $salesOrderItemMdr = $salesOrderItemMdrModel->find("sales_order_item_id = " . $invoiceItem['sales_order_item_id']);
                    if ($salesOrderItemMdr != false) {
                        foreach ($salesOrderItemMdr->toArray() as $itemMdr) {
                            $invoiceMdr += $itemMdr['value_customer'];
                        }
                    }
                }
                    
                $allInvoice[$key]['total_comment_chat'] = $countChat;
                $allInvoice[$key]['mdr_customer'] = $invoiceMdr; 

                if($invoice->SalesOrder->getMdrCustomer() > 0 ){
                    $allInvoice[$key]['grand_total'] +=$invoiceMdr;
                }                
                       

            }
        }

        $data['list'] = $allInvoice;
        $data['recordsTotal'] = 0;
        if (empty($params['invoice_no'])) {
            //count total list
            if (!empty($where_clause)) {
                $where_clause = " WHERE " . $where_clause;
            }
            $query_count = "
                SELECT
                    count(invoice_id) as total
                FROM
                    \Models\SalesInvoice $where_clause";
            $manager = $invoiceModel->getModelsManager();
            $total = $manager->executeQuery($query_count)->toArray();
            $data['recordsTotal'] = $total[0]['total'];

            

        }
       


        return $data;
    }

    private static function getAllPickupCode()
    {
        $query = "SELECT pickup_code FROM pickup_point WHERE pickup_code LIKE 'DC%'";

        $invoiceModel = new \Models\SalesInvoice();

        $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $final = $result->fetchAll();

        $returnVal = array();

        foreach ($final as $key => $value) {
            $returnVal[$key] = $value['pickup_code'];
        }

        return $returnVal;
    }

    private static function getStoreCodeByPickupCode($pickupCode = '')
    {
        $query = "SELECT s.store_code FROM store s LEFT JOIN pickup_point pp ON pp.pickup_id = s.pickup_id
                    WHERE pp.pickup_code = '{$pickupCode}' AND s.store_code like '1000%'";

        $invoiceModel = new \Models\SalesInvoice();

        $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $final = $result->fetch();

        return $final['store_code'];
    }

    private static function determineStatusType($invoiceId = '')
    {
        if ($invoiceId == '') {
            return '';
        } else {
            $instant_sameday_carrier_id = getenv('INSTANT_SAMEDAY_CARRIER_ID');
            $query  =   'SELECT count(*) AS result
                        FROM sales_invoice si JOIN sales_shipment ss ON (si.invoice_id = ss.invoice_id)
                        WHERE ';

            $filterArray = array();
            $arrayIncrement = 0;

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('shipped')";
            $filterArray[$arrayIncrement++]['status'] = "Shipped";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('received')";
            $filterArray[$arrayIncrement++]['status'] = "Received";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('canceled')";
            $filterArray[$arrayIncrement++]['status'] = "Canceled";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('new')
                                                        AND ISNULL(si.sap_so_number)
                                                        AND si.invoice_status IN ('new')";
            $filterArray[$arrayIncrement++]['status'] = "Pending Refund";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('new')
                                                        AND ISNULL(si.sap_so_number)
                                                        AND si.invoice_status IN ('new')";
            $filterArray[$arrayIncrement++]['status'] = "Pending SO SAP";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND si.sales_order_id IN (SELECT 
                                                            sales_order_id
                                                        FROM
                                                            sales_order so
                                                        WHERE
                                                        so.status = 'hold'
                                                                AND SUBSTRING(so.order_no, 1, 4) = 'ODIS')";
            $filterArray[$arrayIncrement++]['status'] = "Shopee Hold";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND si.sales_order_id IN (SELECT 
                                                            sales_order_id
                                                        FROM
                                                            sales_order so
                                                        WHERE
                                                            so.status = 'hold'
                                                                AND SUBSTRING(so.order_no, 1, 4) = 'ODIT')";
            $filterArray[$arrayIncrement++]['status'] = "Tokopedia Hold";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND si.sales_order_id IN (SELECT 
                                                            sales_order_id
                                                        FROM
                                                            sales_order so
                                                        WHERE
                                                        so.status = 'hold'
                                                                AND SUBSTRING(so.order_no, 1, 4) = 'ODIK')";
            $filterArray[$arrayIncrement++]['status'] = "Tiktok Hold";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('new')
                                                        AND si.sap_so_number IS NOT NULL
                                                        AND si.invoice_status IN ('new')
                                                        AND EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND sii.status_fulfillment = 'incomplete')";
            $filterArray[$arrayIncrement++]['status'] = "Incomplete";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('stand_by')
                                                        AND si.sap_so_number IS NOT NULL
                                                        AND si.invoice_status IN ('stand_by')
                                                        AND NOT EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND (sii.status_fulfillment = 'incomplete'
                                                                OR sii.status_fulfillment IS NULL))";
            $filterArray[$arrayIncrement++]['status'] = "Stand By";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('stand_by')
                                                        AND si.sap_so_number IS NOT NULL
                                                        AND si.invoice_status IN ('stand_by')
                                                        AND EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND sii.status_fulfillment = 'incomplete')
                                                        AND EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND sii.status_fulfillment = 'complete')";
            $filterArray[$arrayIncrement++]['status'] = "Partial Incomplete (Stand By & Incomplete)";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('new')
                                                        AND si.sap_so_number IS NOT NULL
                                                        AND EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND (SUBSTRING(SUBSTRING_INDEX(sii.sku, '-', - 1),
                                                                1,
                                                                3) = 'PRE'))
                                                        AND si.invoice_status IN ('new')
                                                        AND EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND sii.status_fulfillment IS NULL)";
            $filterArray[$arrayIncrement++]['status'] = "Pending DO - pre";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('new')
                                                        AND si.sap_so_number IS NOT NULL
                                                        AND si.sales_order_id IN (SELECT 
                                                            sales_order_id
                                                        FROM
                                                            sales_order
                                                        WHERE
                                                            SUBSTRING(order_no, 1, 4) IN ('ODIT' , 'ODIS', 'ODIK')
                                                                AND status NOT IN ('hold'))
                                                                AND NOT EXISTS( SELECT
                                                                1
                                                            FROM
                                                                sales_invoice_item
                                                            WHERE
                                                                sales_invoice_item.invoice_id = si.invoice_id
                                                                    AND (SUBSTRING(SUBSTRING_INDEX(sales_invoice_item.sku, '-', - 1),
                                                                    1,
                                                                    3) = 'PRE'))
                                                        AND si.invoice_status IN ('new')
                                                        AND EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND sii.status_fulfillment IS NULL)
                                                        AND ss.invoice_id NOT IN (
                                                            SELECT invoice_id from sales_shipment as ss2 
                                                            LEFT JOIN shipping_carrier as sc2 ON ss2.carrier_id = sc2.carrier_id where
                                                            sc2.carrier_id IN (" . $instant_sameday_carrier_id . "))";
            $filterArray[$arrayIncrement++]['status'] = "Pending DO - MP";

            $filterArray[$arrayIncrement] = array();
            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('new')
                                                        AND si.sap_so_number IS NOT NULL
                                                        AND EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND SUBSTRING(SUBSTRING_INDEX(sii.sku, '-', - 1),
                                                                1,
                                                                3) <> 'PRE')
                                                        AND si.sales_order_id NOT IN (SELECT 
                                                            sales_order_id
                                                        FROM
                                                            sales_order
                                                        WHERE
                                                            SUBSTRING(order_no, 1, 4) IN ('ODIT' , 'ODIS', 'ODIK'))
                                                        AND si.invoice_status IN ('new')
                                                        AND EXISTS( SELECT 
                                                            1
                                                        FROM
                                                            sales_invoice_item sii
                                                        WHERE
                                                            sii.invoice_id = si.invoice_id
                                                                AND sii.status_fulfillment IS NULL)";
            $filterArray[$arrayIncrement++]['status'] = "Pending DO";

            $filterArray[$arrayIncrement]['filter'] = "(substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                                                        AND si.invoice_status != 'hold_by_tech'
                                                        AND si.company_code = 'ODI'
                                                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                                                        AND ss.shipment_status IN ('new')
                                                        AND si.sap_so_number IS NOT NULL
                                                        AND si.sales_order_id IN (SELECT 
                                                                sales_order_id
                                                            FROM
                                                                sales_order
                                                            WHERE
                                                                SUBSTRING(order_no, 1, 4) IN ('ODIT' , 'ODIS', 'ODIK'))
                                                        AND si.invoice_status IN ('new')
                                                        AND EXISTS(SELECT 
                                                                1
                                                            FROM
                                                                sales_invoice_item sii
                                                            WHERE
                                                                sii.invoice_id = si.invoice_id
                                                                AND sii.status_fulfillment IS NULL)
                                                        AND ss.invoice_id IN (
                                                            SELECT invoice_id from sales_shipment as ss2 
                                                            LEFT JOIN shipping_carrier as sc2 ON ss2.carrier_id = sc2.carrier_id where
                                                            sc2.carrier_id IN (" . $instant_sameday_carrier_id . "))";
            $filterArray[$arrayIncrement++]['status'] = "Pending DO - Instant";

            $queryTail = ' AND si.invoice_id = ' . $invoiceId . ';';

            $invoiceModel = new \Models\SalesInvoice();

            foreach ($filterArray as $id) {
                try {
                    $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query . $id['filter'] . $queryTail);
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );
                    $countArray = $result->fetch();

                    if ($countArray['result'] > 0) {
                        return $id['status'];
                    }
                } catch (\Exception $e) {
                    continue;
                }
            }

            return 'Invalid Status';
        }
    }

    private function mutateDCToSource($dcString)
    {
        $dcMap = [
            'ODI' => ['DC', '1000DC'],
            'AHI' => ['DC_A001', 'DCA001', '1000DCA001'],
            'KWI' => ['DCK001', '1000DCK001'],
            'TGI' => ['DCT001'],
            'KLV' => ['DCV001', '1000DCV001'],
            'KLS' => ['DCS001', '1000DCS001'],
            'HCI' => ['DCH001', '1000DCH001', 'DCH014', '1000DCH014'],
        ];
        foreach ($dcMap as $key => $value) {
            if (in_array($dcString, $value)) {
                return $key;
            }
        }
        return 'Undefined';
    }

    // For invoice dashboard in oms
    public function getAllInvoiceCustom($params = array())
    {
        if (isset($params['limit']) && isset($params['offset'])) {
            $limit = $params['limit'];
            $offset = $params['offset'];
            unset($params['limit']);
            unset($params['offset']);
        }

        $query_count = "SELECT count(si.invoice_id) as total                    
                FROM sales_invoice si join sales_shipment ss on ss.invoice_id = si.invoice_id 
                join sales_order so on so.sales_order_id = si.sales_order_id
                join sales_customer sc on sc.sales_order_id = so.sales_order_id";

        $flagCountOnly = FALSE;
        $columns = "*";
        $where_clause = '';
        $sort = '';
        $flagSort = 0;
        $consumer = '';

        if (isset($params['master_app_id'])) {
            if ($params['master_app_id'] == $_ENV['MASTER_APP_ID_OMS']) {
                unset($params['master_app_id']);

                if (isset($params['query_type'])) {
                    if ($params['query_type'] == 'count_only') {
                        $flagCountOnly = TRUE;
                    }

                    unset($params['query_type']);
                }

                if (isset($params['delivery_method'])) {
                    if ($params['delivery_method'] == 'delivery' || $params['delivery_method'] == 'delivery,ownfleet') {
                        $columns = "
                        si.invoice_id, si.sales_order_id, si.delivery_method,
                        so.order_no, so.status as order_status,
                        si.invoice_no, si.created_at, si.invoice_status,si.store_code,
                        ss.shipment_status,
                        COALESCE(sc.customer_firstname, 'customer') as customer_firstname,
                        COALESCE(sc.customer_lastname, 'ruparupa') as customer_lastname,
                        si.sap_so_number,
                        '' as outbond_delivery,
                        ss.track_number, COALESCE(ss.shipped_date, '') as shipped_date, COALESCE(ss.carrier_id, '') as carrier_id,
                        (SELECT COUNT(sales_comment_id) FROM sales_comment sct WHERE sct.invoice_id = si.invoice_id AND flag = 'chat') as total_comment_chat";

                        if (isset($params['pending_do_state']) && ($params['pending_do_state'] == 'pre_mp' || $params['pending_do_state'] == 'stand_by')) {
                            $columns = "
                            si.invoice_id, si.sales_order_id, si.delivery_method,
                            so.order_no, so.status as order_status,
                            si.invoice_no, si.created_at,
                            '' as invoice_status,
                            si.store_code, ss.shipment_status,
                            COALESCE(sc.customer_firstname, 'customer') as customer_firstname,
                            COALESCE(sc.customer_lastname, 'ruparupa') as customer_lastname,
                            si.sap_so_number,
                            '' as outbond_delivery,
                            ss.track_number, COALESCE(ss.shipped_date, '') as shipped_date, COALESCE(ss.carrier_id, '') as carrier_id, ss.express_courier_state,
                            (SELECT COUNT(sales_comment_id) FROM sales_comment sct WHERE sct.invoice_id = si.invoice_id AND flag = 'chat') as total_comment_chat,
                            ss.shipment_id";
                        }
                    } else {
                        $columns = "
                        si.invoice_id, si.sales_order_id, si.delivery_method,
                        so.order_no, si.invoice_no, si.created_at, si.invoice_status,
                        ss.shipment_status,ss.shipment_id, COALESCE(carrier_id,'') as carrier_id,
                        COALESCE(sc.customer_firstname, 'customer') as customer_firstname,
                        COALESCE(sc.customer_lastname, 'ruparupa') as customer_lastname,
                        si.store_code,ss.track_number,
                        (SELECT status FROM sales_shipment_tracking sst WHERE sst.shipment_id = ss.shipment_id AND sst.remark = 'tracking_awb' ORDER BY sst.shipment_tracking_id DESC LIMIT 1) as shipment_log,
                        (SELECT name FROM store s where s.store_code = si.store_code LIMIT 1) as store_name,
                        si.receipt_id, si.real_price, si.pickup_voucher, COALESCE(si.pin_pos,'') as pin_pos,
                        (SELECT concat(type,'|||',method) FROM sales_order_payment sop where sop.sales_order_id = si.sales_order_id limit 1) as payment_method, 
                        (SELECT IF(ss.carrier_id is not null,(SELECT carrier_name FROM shipping_carrier scr WHERE scr.carrier_id = ss.carrier_id),'')) as courier,
                        (SELECT COUNT(sales_comment_id) FROM sales_comment sct WHERE sct.invoice_id = si.invoice_id AND flag = 'chat') as total_comment_chat";
                    }
                }

                if (isset($params['consumer'])) {
                    $consumer = $params['consumer'];
                    unset($params['consumer']);

                    $sort = " si.store_code ASC, si.created_at DESC ";

                    if ($consumer == 'menu_sales_invoice') {
                        $columns = "
                        si.invoice_id, si.invoice_no, si.created_at as invoice_date,so.order_no,
                        COALESCE(sc.customer_firstname, 'customer') as customer_firstname,
                        COALESCE(sc.customer_lastname, 'ruparupa') as customer_lastname,
                        si.grand_total, si.sap_so_number, si.pickup_voucher, si.delivery_method, si.invoice_status, si.store_code";
                    } else if ($consumer == 'download_invoice_b2b') {
                        $columns = "
                        si.invoice_id, si.invoice_no, si.created_at as invoice_date,so.order_no,
                        COALESCE(sc.customer_firstname, 'customer') as customer_firstname,
                        COALESCE(sc.customer_lastname, 'ruparupa') as customer_lastname,
                        sc.customer_email, sop.method as payment_method, sop.status as payment_status,
                        si.grand_total, si.sap_so_number, si.pickup_voucher, si.delivery_method, si.invoice_status, si.store_code";

                        $query_count .= " JOIN sales_order_payment sop ON so.sales_order_id = sop.sales_order_id";
                        $sort = " si.created_at DESC ";
                    }
                }

                $where_clause = '';

                if (count($params) > 0) {
                    $flagOperator = 0;
                    $where_clause .= " WHERE ";
                    if (isset($params['date_from']) && isset($params['date_to'])) {
                        $dateFrom = $params['date_from'] . " 00:00:00";
                        $dateTo   = $params['date_to'] . " 23:59:59";
                        $where_clause .= "(si.created_at BETWEEN '$dateFrom' AND '$dateTo')";
                        $flagOperator++;
                    } elseif (isset($params['date_from'])) {
                        $where_clause .= "si.created_at >= '" . $params['date_from'] . " 00:00:00'";
                        $flagOperator++;
                    } elseif (isset($params['date_to'])) {
                        $where_clause .= "si.created_at <= '" . $params['date_to'] . " 23:59:59'";
                        $flagOperator++;
                    }

                    if ($flagOperator) {
                        $where_clause .= " AND si.invoice_status != 'hold_by_tech'";
                    } else {
                        $where_clause .= "si.invoice_status != 'hold_by_tech'";
                        $flagOperator++;
                    }

                    if (isset($params['pickup_code']) || isset($params['store_code'])) {
                        if ($flagOperator) $where_clause .= " AND ";
                        if (!empty($params['filter_by']) && $params['filter_by'] == 'pending_refund') {
                            if (!isset($params['pickup_code'])) {
                                $params['pickup_code'] = "all";
                            }
                            if ($params['pickup_code'] <> 'all') {
                                $pickupCodes = explode(",", $params['pickup_code']);
                                $storeCodes = array();
                                foreach ($pickupCodes as $key => $pickupCode) {
                                    $storeCodes[$key] = Invoice::getStoreCodeByPickupCode($pickupCode);
                                }
                                $where_clause .= "si.store_code IN ('" . implode("','", $storeCodes) . "') AND si.pickup_code IN ('" . implode("','", $pickupCodes) . "')";
                                $flagOperator++;
                            } else {
                                $pickupCodes = Invoice::getAllPickupCode();
                                $storeCodes = array();
                                foreach ($pickupCodes as $key => $pickupCode) {
                                    $storeCodes[$key] = Invoice::getStoreCodeByPickupCode($pickupCode);
                                }
                                $where_clause .= "si.store_code IN ('" . implode("','", $storeCodes) . "') AND si.pickup_code IN ('" . implode("','", $pickupCodes) . "')";
                                $flagOperator++;
                            }
                        } elseif (isset($params['pickup_code'])) {
                            if ($params['pickup_code'] <> 'all') {
                                $pickupCodes = explode(",", $params['pickup_code']);
                                $where_clause .= "si.pickup_code IN ('" . implode("','", $pickupCodes) . "')";
                                $flagOperator++;
                            } else {
                                $where_clause .= "si.pickup_code LIKE 'DC%'";
                                $flagOperator++;
                            }
                        } elseif (isset($params['store_code'])) {
                            $storeCodeArr = explode(",", $params['store_code']);
                            $storeInArr = array();
                            foreach ($storeCodeArr as $storeCode) {
                                $storeInArr[] = "si.store_code = '" . $storeCode . "'";
                            }

                            $where_clause .= "(";
                            $where_clause .= implode(" OR ", $storeInArr);
                            $where_clause .= ")";
                            $flagOperator++;
                        }
                    }

                    if (isset($params['company_code'])) {
                        $companyCodeArray = explode(",", strtoupper($params['company_code']));
                        if (!in_array("ODI", $companyCodeArray)) {
                            if ($flagOperator) $where_clause .= " AND ";

                            if (count($companyCodeArray) > 1) {
                                $where_clause .= "(";
                                $loopLike = '';
                                foreach ($companyCodeArray as $valCompanyCode) {
                                    if ($valCompanyCode == "GSP") {
                                        $loopLike .= "si.store_code LIKE 'R%' OR ";
                                    } else {
                                        $loopLike .= "si.store_code LIKE '{$valCompanyCode[0]}%' OR ";
                                    }
                                }
                                $loopLike = rtrim($loopLike, " OR ");
                                $where_clause .= $loopLike . ")";
                            } else {
                                if ($companyCodeArray[0] == "GSP") {
                                    $where_clause .= "si.store_code LIKE 'R%'";
                                } else {
                                    $where_clause .= "si.store_code LIKE '{$companyCodeArray[0][0]}%'";
                                }
                            }

                            $flagOperator++;
                        }
                    }

                    if (isset($params['delivery_method'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $deliveryMethodArray = explode(',', $params['delivery_method']);
                        $deliveryParams = '';
                        foreach ($deliveryMethodArray as $value) {
                            $deliveryParams .= "'" . $value . "',";
                        }

                        $deliveryParams = rtrim($deliveryParams, ',');
                        $where_clause .= "si.delivery_method IN(" . $deliveryParams . ")";

                        $flagOperator++;
                    }

                    if (isset($params['courier_id'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $courierIDArray = explode(',', $params['courier_id']);
                        $courierIDParams = '';
                        foreach ($courierIDArray as $value) {
                            $courierIDParams .= "'" . $value . "',";
                        }

                        $courierIDParams = rtrim($courierIDParams, ',');
                        $where_clause .= "ss.carrier_id IN(" . $courierIDParams . ")";

                        $flagOperator++;
                    }

                    if (isset($params['shipment_status'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $shipmentStatusArray = explode(',', $params['shipment_status']);
                        $shipmentParams = '';
                        foreach ($shipmentStatusArray as $value) {
                            $shipmentParams .= "'" . $value . "',";
                        }

                        $shipmentParams = rtrim($shipmentParams, ',');
                        $where_clause .= "ss.shipment_status IN(" . $shipmentParams . ")";

                        $flagOperator++;
                    }

                    if (isset($params['so_sap_state'])) {
                        $soSapStateArr = array('empty', 'exist');
                        if (in_array($params['so_sap_state'], $soSapStateArr)) {
                            if ($flagOperator) $where_clause .= " AND ";
                            $flagOperator++;
                        }

                        if ($params['so_sap_state'] == 'empty') {
                            $where_clause .= "ISNULL(si.sap_so_number)";
                        } elseif ($params['so_sap_state'] == 'exist') {
                            $where_clause .= "si.sap_so_number IS NOT NULL";
                        }
                    }

                    if (isset($params['pending_do_state'])) {
                        $doSapStateArr = array('only', 'pre_only', 'pre_mp', 'instant');
                        if (in_array($params['pending_do_state'], $doSapStateArr)) {
                            if ($flagOperator) $where_clause .= " AND ";
                            $flagOperator++;
                        }

                        $instant_sameday_carrier_id = getenv('INSTANT_SAMEDAY_CARRIER_ID');

                        if ($params['pending_do_state'] == 'only') {
                            $where_clause .= "EXISTS
                                            (
                                                SELECT 1 FROM sales_invoice_item sii
                                                WHERE sii.invoice_id = si.invoice_id
                                                AND SUBSTRING(SUBSTRING_INDEX(sii.sku,'-',-1),1,3) <> 'PRE'
                                            ) AND si.sales_order_id NOT IN (
                                                SELECT sales_order_id from sales_order where
                                                SUBSTRING(order_no,1,4) in ('ODIT','ODIS', 'ODIK')
                                            ) ";
                        }

                        if ($params['pending_do_state'] == 'pre_only') {
                            $where_clause .= "EXISTS
                                            (
                                                SELECT 1 FROM sales_invoice_item sii
                                                WHERE sii.invoice_id = si.invoice_id
                                                AND (
                                                    SUBSTRING(SUBSTRING_INDEX(sii.sku,'-',-1),1,3) = 'PRE'
                                                )
                                            ) ";
                        }

                        if ($params['pending_do_state'] == 'pre_mp') {
                            $where_clause .= " si.sales_order_id IN (
                                                SELECT sales_order_id from sales_order where
                                                SUBSTRING(order_no,1,4) in ('ODIT','ODIS', 'ODIK') AND status not in ('hold')
                                            ) AND NOT EXISTS( SELECT
                                            1
                                        FROM
                                            sales_invoice_item
                                        WHERE
                                            sales_invoice_item.invoice_id = si.invoice_id
                                                AND (SUBSTRING(SUBSTRING_INDEX(sales_invoice_item.sku, '-', - 1),
                                                1,
                                                3) = 'PRE'))
                                            AND ss.invoice_id NOT IN (
                                                SELECT invoice_id from sales_shipment as ss2 
                                                LEFT JOIN shipping_carrier as sc2 ON ss2.carrier_id = sc2.carrier_id where
                                                sc2.carrier_id IN (" . $instant_sameday_carrier_id . "))";
                        }

                        if ($params['pending_do_state'] == 'instant') {
                            $where_clause .= " ss.invoice_id IN (
                                                SELECT invoice_id from sales_shipment as ss2 
                                                LEFT JOIN shipping_carrier as sc2 ON ss2.carrier_id = sc2.carrier_id where
                                                sc2.carrier_id IN (" . $instant_sameday_carrier_id . "))";
                        }
                    }

                    if (isset($params['pickup_voucher_state'])) {
                        $pickupVoucherStateArr = array('empty', 'exist');
                        if (in_array($params['pickup_voucher_state'], $pickupVoucherStateArr)) {
                            if ($flagOperator) $where_clause .= " AND ";
                            $flagOperator++;
                        }

                        if ($params['pickup_voucher_state'] == 'empty') {
                            $where_clause .= "ISNULL(si.pickup_voucher)";
                        } elseif ($params['pickup_voucher_state'] == 'exist') {
                            $where_clause .= "si.pickup_voucher IS NOT NULL";
                        }
                    }

                    if (isset($params['receipt_id'])) {
                        $pickupVoucherStateArr = array('empty', 'exist');
                        if (in_array($params['receipt_id'], $pickupVoucherStateArr)) {
                            if ($flagOperator) $where_clause .= " AND ";
                            $flagOperator++;
                        }

                        if ($params['receipt_id'] == 'empty') {
                            $where_clause .= " (ISNULL(si.receipt_id) OR si.receipt_id = '') ";
                        } elseif ($params['receipt_id'] == 'exist') {
                            $where_clause .= " (si.receipt_id IS NOT NULL OR si.receipt_id <> '') ";
                        }
                    }

                    if (isset($params['invoice_status'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $invoiceStatusArray = explode(',', $params['invoice_status']);
                        $invoiceParams = '';
                        foreach ($invoiceStatusArray as $value) {
                            $invoiceParams .= "'" . $value . "',";
                        }

                        $invoiceParams = rtrim($invoiceParams, ',');
                        $where_clause .= "si.invoice_status IN (" . $invoiceParams . ")";
                    }

                    if (isset($params['status_fulfillment_item'])) {
                        // all status_fulfillment must same
                        $statusFulfillmentArr = array('complete', 'incomplete', 'empty');
                        if (in_array($params['status_fulfillment_item'], $statusFulfillmentArr)) {
                            if ($flagOperator) $where_clause .= " AND ";
                            $flagOperator++;
                        }

                        if ($params['status_fulfillment_item'] == 'complete') {
                            $where_clause .= "NOT EXISTS
                                            (
                                                SELECT 1 FROM sales_invoice_item sii
                                                WHERE sii.invoice_id = si.invoice_id
                                                AND (sii.status_fulfillment = 'incomplete' OR sii.status_fulfillment IS NULL)
                                            )";
                        } elseif ($params['status_fulfillment_item'] == 'incomplete') {
                            $where_clause .= "EXISTS
                                            (
                                                SELECT 1 FROM sales_invoice_item sii
                                                WHERE sii.invoice_id = si.invoice_id
                                                AND sii.status_fulfillment = 'incomplete'
                                            )";
                        } elseif ($params['status_fulfillment_item'] == 'empty') {
                            $where_clause .= "EXISTS
                                            (
                                                SELECT 1 FROM sales_invoice_item sii
                                                WHERE sii.invoice_id = si.invoice_id
                                                AND sii.status_fulfillment IS NULL
                                            )";
                        }
                    }

                    if (!empty($params['source_dc'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $params['source_dc'] = strtoupper($params['source_dc']);
                        $where_clause .= "si.supplier_alias regexp '" . $params['source_dc'] . "' and si.pickup_code regexp 'DC'";;

                        $flagOperator++;
                    }

                    if (!empty($params['order_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $params['order_keyword'] = strtoupper($params['order_keyword']);
                        $where_clause .= "si.sales_order_id IN (
                            SELECT sales_order_id 
                            FROM sales_order so 
                            WHERE so.order_no like '%" . $params['order_keyword'] . "%'
                        )";

                        $flagOperator++;
                    }

                    if (isset($params['order_type'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $where_clause .= "si.sales_order_id IN (
                            SELECT sales_order_id 
                            FROM sales_order so 
                            WHERE so.order_type = '" . $params['order_type'] . "'
                        )";

                        $flagOperator++;
                    }

                    if (isset($params['invoice_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $params['invoice_keyword'] = strtoupper($params['invoice_keyword']);
                        $where_clause .= "invoice_no LIKE '%" . $params['invoice_keyword'] . "%'";

                        $flagOperator++;
                    }

                    if (isset($params['invoice_status_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $where_clause .= "invoice_status = '" . $params['invoice_status_keyword'] . "'";

                        $flagOperator++;
                    }

                    if (isset($params['shipment_status_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $where_clause .= " ss.shipment_status = '" . $params['shipment_status_keyword'] . "'";

                        $flagOperator++;
                    }

                    if (isset($params['invoice_date_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $dateFrom = $params['invoice_date_keyword'] . " 00:00:00";
                        $dateTo   = $params['invoice_date_keyword'] . " 23:59:59";

                        $dateFromArr = explode('-', $params['invoice_date_keyword']);
                        $dateToArr   = explode('-', $params['invoice_date_keyword']);

                        if (count($dateFromArr) == 2) {
                            $dateFrom = $dateFromArr[0] . "-" . $dateFromArr[1] . "-01 00:00:00";
                        }

                        if (count($dateToArr) == 2) {
                            $dateTo = $dateToArr[0] . "-" . $dateToArr[1] . "-" . date('t', strtotime($dateToArr[0] . "-" . $dateToArr[1])) . " 23:59:59";
                        }

                        $where_clause .= "si.created_at BETWEEN '$dateFrom' AND '$dateTo'";

                        $flagOperator++;
                    }

                    if (isset($params['so_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $params['so_keyword'] = strtoupper($params['so_keyword']);
                        $where_clause .= "si.sap_so_number LIKE '%" . $params['so_keyword'] . "%'";
                        $flagOperator++;
                    }

                    if (!empty($params['customer_name_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $where_clause .= "si.sales_order_id IN (
                            SELECT sales_order_id FROM sales_customer sc 
                            WHERE sc.customer_firstname like '%" . $params['customer_name_keyword'] . "%' 
                                OR sc.customer_lastname like '%" . $params['customer_name_keyword'] . "%')";
                        $flagOperator++;
                    }

                    if (!empty($params['store_code_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $params['store_code_keyword'] = strtoupper($params['store_code_keyword']);
                        $where_clause .= "(si.store_code LIKE '%" . $params['store_code_keyword'] . "%' 
                            OR si.pickup_code LIKE '%" . $params['store_code_keyword'] . "%' )";
                        $flagOperator++;
                    }

                    if (!empty($params['pickup_voucher_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $params['pickup_voucher_keyword'] = strtoupper($params['pickup_voucher_keyword']);
                        $where_clause .= "si.pickup_voucher LIKE '%" . $params['pickup_voucher_keyword'] . "%'";
                        $flagOperator++;
                    }

                    if (!empty($params['receipt_id_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $params['receipt_id_keyword'] = strtoupper($params['receipt_id_keyword']);
                        $where_clause .= "si.receipt_id LIKE '%" . $params['receipt_id_keyword'] . "%'";
                        $flagOperator++;
                    }

                    if (!empty($params['customer_email_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $where_clause .= " si.sales_order_id IN (
                            select sales_order_id from sales_customer sc 
                            WHERE COALESCE(
                                (SELECT customer_email FROM sales_customer sc WHERE sc.sales_order_id = si.sales_order_id LIMIT 1), 
                                'customer@ruparupa.com'
                            ) like '%" . $params['customer_email_keyword'] . "%')";
                        $flagOperator++;
                    }

                    if (!empty($params['track_number_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $where_clause .= " ss.track_number like '%" . $params['track_number_keyword'] . "%'";
                        $flagOperator++;
                    }

                    if (!empty($params['carrier_id_keyword'])) {
                        if ($flagOperator) $where_clause .= " AND ";

                        $where_clause .= " ss.carrier_id = " . $params['carrier_id_keyword'];
                        $flagOperator++;
                    }

                    if (!empty($params['data_for'])) {
                        if ($params['data_for'] == "ttpu") {
                            if ($flagOperator) $where_clause .= " AND ";
                            if ($params['delivery_method'] == "pickup,store_fulfillment") {
                                $page = "fulfillment_center";
                            } elseif ($params['delivery_method'] == "pickup") {
                                $page = "stops";
                            } else {
                                $page = "store_fulfillment";
                            }

                            $where_clause .= " invoice_no NOT IN (
                                SELECT invoice_no FROM ttpu_detail td 
                                WHERE td.ttpu_id IN (SELECT ttpu_id FROM ttpu t WHERE t.status != 'rejected' AND t.page = '" . $page . "')
                            )";
                        } else if ($params['data_for'] == "shopee_hold") {
                            if ($flagOperator) $where_clause .= " AND ";
                            $where_clause .= " si.sales_order_id in (select sales_order_id from sales_order so where so.status = 'hold' and SUBSTRING(so.order_no,1,4) = 'ODIS')";
                        } else if ($params['data_for'] == "tokopedia_hold") {
                            if ($flagOperator) $where_clause .= " AND ";
                            $where_clause .= " si.sales_order_id in (select sales_order_id from sales_order so where so.status = 'hold' and SUBSTRING(so.order_no,1,4) = 'ODIT')";
                        } else if ($params['data_for'] == "tiktok_hold") {
                            if ($flagOperator) $where_clause .= " AND ";
                            $where_clause .= " si.sales_order_id in (select sales_order_id from sales_order so where so.status = 'hold' and SUBSTRING(so.order_no,1,4) = 'ODIK')";
                        }
                    }

                    if (!empty($params['store_code_filter'])) {
                        if ($flagOperator) $where_clause .= " AND ";
                        $storeCodeListArr = explode(",", $params['store_code_filter']);
                        $storeCodeListArrStr = array();
                        foreach ($storeCodeListArr as $storeCodeList) {
                            $storeCodeListArrStr[] = "'" . $storeCodeList . "'";
                        }
                        $storeCodeListStr = implode(",", $storeCodeListArrStr);

                        $where_clause .= " si.store_code IN (" . $storeCodeListStr . ")";
                        $flagOperator++;
                    }

                    if (!empty($params['filter_by'])) {
                        if ($params['filter_by'] == "pending_do" || $params['filter_by'] == "stand_by") {
                            if ($flagSort) $sort .= " , ";

                            $sort .= " FIELD(ss.carrier_id, 7, 8) DESC ";
                            $flagSort++;
                        }

                        if ($params['filter_by'] == "stand_by") {
                            if ($flagSort) $sort .= " , ";

                            $sort .= " FIELD(ss.express_courier_state, 3) DESC ";
                            $flagSort++;
                        }
                    }

                    if (!empty($params['customer_email_sorting'])) {
                        if ($flagSort) $sort .= " , ";
                        $params['customer_email_sorting'] = strtoupper($params['customer_email_sorting']);
                        $sort .= " customer_email " . $params['customer_email_sorting'];
                        $flagSort++;
                    }

                    if (!empty($params['customer_name_sorting'])) {
                        if ($flagSort) $sort .= " , ";
                        $params['customer_name_sorting'] = strtoupper($params['customer_name_sorting']);
                        $sort .= " customer_firstname " . $params['customer_name_sorting'];
                        $flagSort++;
                    }

                    if (!empty($params['store_code_sorting'])) {
                        if ($flagSort) $sort .= " , ";

                        $params['store_code_sorting'] = strtoupper($params['store_code_sorting']);
                        $sort .= " si.store_code " . $params['store_code_sorting'];
                        $flagSort++;
                    }

                    if (!empty($params['invoice_date_sorting'])) {
                        if ($flagSort) $sort .= " , ";

                        $params['invoice_date_sorting'] = strtoupper($params['invoice_date_sorting']);
                        $sort .= " si.created_at " . $params['invoice_date_sorting'];
                    }

                    if (!empty($params['pin_pos_sorting'])) {
                        if ($flagSort) $sort .= " , ";

                        $params['pin_pos_sorting'] = strtoupper($params['pin_pos_sorting']);
                        $sort .= " si.pin_pos " . $params['pin_pos_sorting'];
                    }
                }
            }
        }

        $query_count .= $where_clause;
        if (empty($sort)) {
            $sort = " si.created_at DESC ";
            if (isset($params['pending_do_state'])) {
                if ($params['pending_do_state'] == 'pre_mp') {
                    $sort =  " si.created_at";
                }
            } else if (isset($params['shipment_status'])) {
                if ($params['shipment_status'] == "shipped" || $params['shipment_status'] == "canceled") {
                    $sort = "";
                }
            }
        }

        if ($flagCountOnly) {
            $query = $query_count;
        } else {
            $query = "SELECT " . $columns . ", si.status_so 
                FROM sales_invoice si join sales_shipment ss on ss.invoice_id = si.invoice_id 
                join sales_order so on so.sales_order_id = si.sales_order_id 
                join sales_customer sc on sc.sales_order_id = so.sales_order_id ";

            if ($consumer == "download_invoice_b2b") {
                $query .= "join sales_order_payment sop on so.sales_order_id = sop.sales_order_id ";
            }

            $query .= $where_clause;

            if (!empty($sort)) {
                $query .= " ORDER BY $sort";
            }

            if (isset($limit) && isset($offset)) {
                $query .= " LIMIT $limit OFFSET $offset";
            }
        }

        try {
            $invoiceModel = new \Models\SalesInvoice();
            $invoiceModel->useReadOnlyConnection();

            if ($flagCountOnly) {
                $allInvoice['list'] = array('query_type' => 'count_only');
            } else {
                $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );
                $resultArray = $result->fetchAll();

                // We don't need additional information for download invoice B2B
                if ($consumer != "download_invoice_b2b") {
                    // Additional Information
                    if (count($resultArray) > 0) {

                        // collect invoices and order no
                        $invoices = [];
                        $orders = [];
                        foreach ($resultArray as $rowResult) {
                            if (!empty($rowResult['invoice_no'])) {
                                $invoices[] = "'" . $rowResult['invoice_no'] . "'";
                            }

                            if (!empty($rowResult['order_no'])) {
                                $orders[] = "'" . $rowResult['order_no'] . "'";
                            }
                        }

                        $invoicesQuery = implode(',', $invoices);
                        $ordersQuery = implode(',', $orders);

                        // Additional Information - Sales Order Vendor
                        if (isset($params['pending_do_state']) && ($params['pending_do_state'] == 'pre_mp' || $params['pending_do_state'] == 'stand_by')) {
                            $query = "SELECT 
                                            ship_at,
                                            order_no
                                        FROM
                                            sales_order_vendor
                                        WHERE
                                            order_no in (" . $ordersQuery . ")
                                        ";
                            $resultVendor = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);
                            $resultVendor->setFetchMode(
                                Database::FETCH_ASSOC
                            );
                            $resultArrayVendor = $resultVendor->fetchAll();
                        }

                        // Additional Information - Sales Invoice OD
                        $soCiReleaseDate = new \DateTime(getenv('SO_CI_RELEASE_DATE'));
                        $query = "SELECT 
                            sio.invoice_no,
                            IF (MIN(si.created_at) < '{$soCiReleaseDate->format('Y-m-d')}',
                            REPLACE(GROUP_CONCAT(DISTINCT (IF(sio.od_intercom = '',
                                    IF(sio.message = '', '', sio.message),
                                    CONCAT(sio.od_intercom,
                                            ' | ',
                                            sio.sku,
                                            ' | ',
                                            sio.qty,
                                            ' | ',
                                            CONCAT(SUBSTRING(sio.gi_date, 1, 4),
                                                    '-',
                                                    SUBSTRING(sio.gi_date, 5, 2),
                                                    '-',
                                                    SUBSTRING(sio.gi_date, 7, 2)))))),
                            ',',
                            '<br>'),
                            REPLACE(GROUP_CONCAT(DISTINCT (IF(sio.od_odi = '',
                                    IF(sio.message = '', '', sio.message),
                                    CONCAT(sio.od_odi,
                                            ' | ',
                                            sio.sku,
                                            ' | ',
                                            sio.delivery_qty,
                                            ' | ',
                                            CONCAT(SUBSTRING(sio.gi_odi_date, 1, 4),
                                                    '-',
                                                    SUBSTRING(sio.gi_odi_date, 5, 2),
                                                    '-',
                                                    SUBSTRING(sio.gi_odi_date, 7, 2)))))),
                            ',',
                            '<br>')) AS outbond_delivery
                        FROM sales_invoice_od sio
                        LEFT JOIN sales_invoice si ON si.invoice_no = sio.invoice_no 
                        WHERE sio.invoice_no IN ({$invoicesQuery})
                        GROUP BY sio.invoice_no
                        ";
                        $resultOutbound = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query);
                        $resultOutbound->setFetchMode(
                            Database::FETCH_ASSOC
                        );
                        $resultArrayOutbound = $resultOutbound->fetchAll();

                        // Merge Result
                        for ($i = 0; $i < count($resultArray); $i++) {
                            if (!empty($params['filter_by']) && $params['filter_by'] == 'all') {
                                $resultArray[$i]['status'] = Invoice::determineStatusType($resultArray[$i]['invoice_id']);
                            } else {
                                $resultArray[$i]['status'] = '';
                            }
                            $resultArray[$i]['source_order_no'] = $this->mutateDCToSource($resultArray[$i]['store_code']);
                            foreach ($resultArrayOutbound as $rowOutbound) {
                                if ($resultArray[$i]['invoice_no'] == $rowOutbound['invoice_no']) {
                                    $resultArray[$i]['outbond_delivery'] = $rowOutbound['outbond_delivery'];
                                }
                            }
                            if (isset($params['pending_do_state']) && ($params['pending_do_state'] == 'pre_mp' || $params['pending_do_state'] == 'stand_by')) {
                                foreach ($resultArrayVendor as $rowVendor) {
                                    if ($resultArray[$i]['order_no'] == $rowVendor['order_no']) {
                                        $resultArray[$i]['invoice_status'] = $rowVendor['ship_at'];
                                    }
                                }
                            }
                        }
                    }

                    if (!empty($params['invoice_status_sorting'])) {
                        $invoiceStatus = array();

                        foreach ($resultArray as $key => $value) {
                            $invoiceStatus[$key] = $value['invoice_status'];
                        }

                        switch ($params['invoice_status_sorting']) {
                            case 'asc':
                                array_multisort($invoiceStatus, SORT_ASC, $resultArray);
                                break;
                            case 'desc':
                                array_multisort($invoiceStatus, SORT_DESC, $resultArray);
                                break;
                        }
                    }

                    $resultCount = count($resultArray);

                    if ($resultCount > 0) {
                        $now = new DateTime();

                        // // Get sla time
                        $slaOrderQuery = '
                            select
                                si.sla_age
                            from
                                sales_invoice si
                                where
                                    invoice_id in (';

                        $invoiceIDTuppleFilter = '';
                        for ($invoiceIDindex = 0; $invoiceIDindex < $resultCount; $invoiceIDindex++) {
                            $invoiceIDTuppleFilter .= $resultArray[$invoiceIDindex]["invoice_id"];
                            if ($invoiceIDindex != $resultCount - 1) $invoiceIDTuppleFilter .= ',';
                        }

                        $slaOrderQuery .= $invoiceIDTuppleFilter;
                        $slaOrderQuery .= ') ';

                        $slaOrderQuery .= " order by field (si.invoice_id, ";
                        $slaOrderQuery .= $invoiceIDTuppleFilter;
                        $slaOrderQuery .= ') ';

                        $slaListQueryObject = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($slaOrderQuery);
                        $resultOutbound->setFetchMode(
                            Database::FETCH_ASSOC
                        );

                        $slaList = $slaListQueryObject->fetchAll();

                        for ($invoiceIDindex = 0; $invoiceIDindex < $resultCount; $invoiceIDindex++) {

                            if ($slaList[$invoiceIDindex]['sla_age'] != NULL) {
                                $sla_age = intval($slaList[$invoiceIDindex]['sla_age']);
                                $hour = (int)floor($sla_age / 60);
                                $minute = $sla_age % 60;
                                $resultArray[$invoiceIDindex]['sla_order'] = str_pad($hour,  2, "0", STR_PAD_LEFT) . ":" . sprintf("%'02d", $minute);
                            } else {
                                $createdDate = new DateTime($resultArray[$invoiceIDindex]['created_at']);

                                if ($resultArray[$invoiceIDindex]['shipment_status'] == "new") {
                                    $interval = $now->diff($createdDate);

                                    $hourDiff =
                                        $interval->format("%a")
                                        * 24
                                        + $interval->h;

                                    $resultArray[$invoiceIDindex]['sla_order']
                                        = str_pad($hourDiff,  2, "0", STR_PAD_LEFT) . ":" . sprintf("%'02d", $interval->i);
                                } else if (
                                    $resultArray[$invoiceIDindex]['shipment_status'] == "stand_by" ||
                                    $resultArray[$invoiceIDindex]['shipment_status'] == "shipped" ||
                                    $resultArray[$invoiceIDindex]['shipment_status'] == "received"
                                ) {
                                    $resultArray[$invoiceIDindex]['sla_order'] = NULL;
                                } else {
                                    $resultArray[$invoiceIDindex]['sla_order'] = "";
                                }
                            }
                        }
                    }
                }

                $allInvoice['list'] = $resultArray;
            }

            // count data
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($query_count);
            $result->setFetchMode(
                Database::FETCH_ASSOC
            );
            $countArray = $result->fetch();
            $allInvoice['recordsTotal'] = $countArray['total'];
        } catch (\Exception $e) {
            $allInvoice['list'] = array();
        }

        return $allInvoice;
    }

    public function getInvoiceDetail($params = array())
    {
        $allInvoice = $this->getAllInvoice($params);

        if ($allInvoice) {
            return !empty($allInvoice['list'][0]) ? $allInvoice['list'][0] : array();
        } else {
            return array();
        }
    }

    public function getAllInvoiceTotalDC($params = array())
    {
        try {
            $invoiceModel = new \Models\SalesInvoice();

            $storeFilter = "";
            if (isset($params['pickup_code'])) {
                if ($params['pickup_code'] <> 'all') {
                    $pickupCodes = explode(",", $params['pickup_code']);
                    $storeFilter .= " AND si.pickup_code IN ('" . implode("','", $pickupCodes) . "')";
                } else {
                    $storeFilter .= " AND si.pickup_code LIKE 'DC%'";
                }
            }

            // ALL
            // [rr-master-app-id] => 1
            // [rr-filter-by] => all
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
                SELECT 
                    COUNT(si.invoice_id) AS total
                FROM
                    sales_invoice si
                JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id
                WHERE
                    si.invoice_status != 'hold_by_tech' 
                        AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                        AND si.company_code = 'ODI'
                        AND si.delivery_method IN ('delivery' , 'ownfleet')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['all'] = $result->fetch()['total'];

            // Pending SO SAP
            // [rr-master-app-id] => 1
            // [rr-invoice-status] => new
            // [rr-shipment-status] => new
            // [rr-so-sap-state] => empty
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC
            // [rr-company-code] => ODI
            $sql = "
                SELECT 
                    COUNT(si.invoice_id) AS total
                FROM
                    sales_invoice si
                        JOIN
                    sales_shipment ss ON ss.invoice_id = si.invoice_id
                WHERE
                    si.invoice_status != 'hold_by_tech'
                        AND substr(si.store_code,1,2) = 'DC'
                        AND si.company_code = 'ODI'
                        AND si.delivery_method IN ('delivery' , 'ownfleet')
                        AND ss.shipment_status IN ('new')
                        AND ISNULL(si.sap_so_number)
                        AND si.invoice_status IN ('new')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['pending_so_sap'] = $result->fetch()['total'];

            // Pending DO
            // [rr-master-app-id] => 1
            // [rr-invoice-status] => new
            // [rr-shipment-status] => new
            // [rr-so-sap-state] => exist
            // [rr-status-fulfillment-item] => empty
            // [rr-pending-do-state] => only
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                    JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech' 
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('new')
                    AND si.sap_so_number IS NOT NULL
                    AND EXISTS( SELECT 
                        1
                    FROM
                        sales_invoice_item sii
                    WHERE
                        sii.invoice_id = si.invoice_id
                            AND SUBSTRING(SUBSTRING_INDEX(sii.sku, '-', - 1),
                            1,
                            3) <> 'PRE')
                    AND si.sales_order_id NOT IN (SELECT 
                        sales_order_id
                    FROM
                        sales_order
                    WHERE
                        SUBSTRING(order_no, 1, 4) IN ('ODIT' , 'ODIS', 'ODIK'))
                    AND si.invoice_status IN ('new')
                    AND EXISTS( SELECT 
                        1
                    FROM
                        sales_invoice_item sii
                    WHERE
                        sii.invoice_id = si.invoice_id
                            AND sii.status_fulfillment IS NULL)
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['pending_do'] = $result->fetch()['total'];

            // Pending DO PRE
            // [rr-master-app-id] => 1
            // [rr-invoice-status] => new
            // [rr-shipment-status] => new
            // [rr-so-sap-state] => exist
            // [rr-status-fulfillment-item] => empty
            // [rr-pending-do-state] => pre_only
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                    JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('new')
                    AND si.sap_so_number IS NOT NULL
                    AND EXISTS( SELECT 
                        1
                    FROM
                        sales_invoice_item sii
                    WHERE
                        sii.invoice_id = si.invoice_id
                            AND (SUBSTRING(SUBSTRING_INDEX(sii.sku, '-', - 1),
                            1,
                            3) = 'PRE'))
                    AND si.invoice_status IN ('new')
                    AND EXISTS( SELECT 
                        1
                    FROM
                        sales_invoice_item sii
                    WHERE
                        sii.invoice_id = si.invoice_id
                            AND sii.status_fulfillment IS NULL)
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['pending_do_pre'] = $result->fetch()['total'];

            // Pending DO MP
            // [rr-master-app-id] => 1
            // [rr-invoice-status] => new
            // [rr-shipment-status] => new
            // [rr-so-sap-state] => exist
            // [rr-status-fulfillment-item] => empty
            // [rr-pending-do-state] => pre_mp
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI

            $instant_sameday_carrier_id = getenv('INSTANT_SAMEDAY_CARRIER_ID');

            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                    JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('new')
                    AND si.sap_so_number IS NOT NULL
                    AND si.sales_order_id IN (SELECT 
                        sales_order_id
                    FROM
                        sales_order
                    WHERE
                        SUBSTRING(order_no, 1, 4) IN ('ODIT' , 'ODIS', 'ODIK')
                            AND status NOT IN ('hold'))
                            AND NOT EXISTS( SELECT
                            1
                        FROM
                            sales_invoice_item
                        WHERE
                            sales_invoice_item.invoice_id = si.invoice_id
                                AND (SUBSTRING(SUBSTRING_INDEX(sales_invoice_item.sku, '-', - 1),
                                1,
                                3) = 'PRE'))
                    AND si.invoice_status IN ('new')
                    AND EXISTS( SELECT 
                        1
                    FROM
                        sales_invoice_item sii
                    WHERE
                        sii.invoice_id = si.invoice_id
                            AND sii.status_fulfillment IS NULL)
                    AND ss.invoice_id NOT IN (
                        SELECT invoice_id from sales_shipment as ss2 
                        LEFT JOIN shipping_carrier as sc2 ON ss2.carrier_id = sc2.carrier_id where
                        sc2.carrier_id IN (" . $instant_sameday_carrier_id . "))
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['pending_do_mp'] = $result->fetch()['total'];


            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                    JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('new')
                    AND si.sap_so_number IS NOT NULL
                    AND si.sales_order_id IN (SELECT 
                        sales_order_id
                    FROM
                        sales_order
                    WHERE
                        SUBSTRING(order_no, 1, 4) IN ('ODIT' , 'ODIS', 'ODIK')
                            AND status NOT IN ('hold'))
                    AND si.invoice_status IN ('new')
                    AND EXISTS( SELECT 
                            1
                        FROM
                            sales_invoice_item sii
                        WHERE
                            sii.invoice_id = si.invoice_id 
                            AND sii.status_fulfillment IS NULL)
                    AND ss.invoice_id IN (
                        SELECT invoice_id from sales_shipment as ss2 
                        LEFT JOIN shipping_carrier as sc2 ON ss2.carrier_id = sc2.carrier_id where
                        sc2.carrier_id IN (" . $instant_sameday_carrier_id . "))
            ";

            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['pending_do_instant'] = $result->fetch()['total'];

            // Incomplete
            // [rr-master-app-id] => 1
            // [rr-invoice-status] => new
            // [rr-shipment-status] => new
            // [rr-so-sap-state] => exist
            // [rr-status-fulfillment-item] => incomplete
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                    JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND si.sap_so_number IS NOT NULL
                    AND si.invoice_status IN ('new', 'stand_by', 'shipped', 'received')
                    AND EXISTS( SELECT 
                        1
                    FROM
                        sales_invoice_item sii
                    WHERE
                        sii.invoice_id = si.invoice_id
                            AND sii.status_fulfillment = 'incomplete')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['incomplete'] = $result->fetch()['total'];

            // Stand By
            // [rr-master-app-id] => 1
            // [rr-invoice-status] => new
            // [rr-shipment-status] => new
            // [rr-so-sap-state] => exist
            // [rr-status-fulfillment-item] => complete
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                    JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('stand_by')
                    AND si.sap_so_number IS NOT NULL
                    AND si.invoice_status IN ('stand_by')
                    AND EXISTS( SELECT 
                            1
                        FROM
                            sales_invoice_item sii
                        WHERE
                            sii.invoice_id = si.invoice_id
                                AND sii.status_fulfillment = 'complete')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['stand_by'] = $result->fetch()['total'];

            // Shipped
            // [rr-master-app-id] => 1
            // [rr-shipment-status] => shipped
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('shipped')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['shipped'] = $result->fetch()['total'];

            // Received
            // [rr-master-app-id] => 1
            // [rr-shipment-status] => received
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('received')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['received'] = $result->fetch()['total'];

            // Canceled
            // [rr-master-app-id] => 1
            // [rr-shipment-status] => canceled
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('canceled')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['canceled'] = $result->fetch()['total'];

            // Pending Refund
            // [rr-master-app-id] => 1
            // [rr-invoice-status] => new
            // [rr-so-sap-state] => empty
            // [rr-shipment-status] => new
            // [rr-store-code] => 1000DC
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
                    JOIN
                sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND substr(si.store_code,1,6) = '1000DC'
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND ss.shipment_status IN ('new')
                    AND ISNULL(si.sap_so_number)
                    AND si.invoice_status IN ('new')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['pending_refund'] = $result->fetch()['total'];

            // Shopee Hold
            // [rr-master-app-id] => 1
            // [rr-data-for] => shopee_hold
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
            JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND si.sales_order_id IN (SELECT 
                        sales_order_id
                    FROM
                        sales_order so
                    WHERE
                    so.status = 'hold'
                            AND SUBSTRING(so.order_no, 1, 4) = 'ODIS')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['shopee_hold'] = $result->fetch()['total'];

            // Tokopedia Hold
            // [rr-master-app-id] => 1
            // [rr-data-for] => tokopedia_hold
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
            JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND si.sales_order_id IN (SELECT 
                        sales_order_id
                    FROM
                        sales_order so
                    WHERE
                        so.status = 'hold'
                            AND SUBSTRING(so.order_no, 1, 4) = 'ODIT')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['tokopedia_hold'] = $result->fetch()['total'];

            // Tiktok Hold
            // [rr-master-app-id] => 1
            // [rr-data-for] => tiktok_hold
            // [rr-delivery-method] => delivery,ownfleet
            // [rr-query-type] => count_only
            // [rr-store-code] => DC,1000DC
            // [rr-company-code] => ODI
            $sql = "
            SELECT 
                COUNT(si.invoice_id) AS total
            FROM
                sales_invoice si
            JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                si.invoice_status != 'hold_by_tech'
                    AND (substr(si.store_code,1,2) = 'DC' OR substr(si.store_code,1,6) = '1000DC')
                    AND si.company_code = 'ODI'
                    AND si.delivery_method IN ('delivery' , 'ownfleet')
                    AND si.sales_order_id IN (SELECT 
                        sales_order_id
                    FROM
                        sales_order so
                    WHERE
                        so.status = 'hold'
                            AND SUBSTRING(so.order_no, 1, 4) = 'ODIK')
            ";
            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['tiktok_hold'] = $result->fetch()['total'];

            // Return DC
            // [rr-master-app-id] => 1
            // [rr-data-for] => return
            $hiddenCancelledStatusDate = date("Y-m-d", strtotime("-1 months"));
            $excludeInvoiceEnv = getenv('EXCLUDE_REFUND_DC');
            $excludeInvoice = '';
            if ($excludeInvoiceEnv != '') {
                $excludeInvoice = " AND srrd.invoice_no NOT IN ('" . implode("','", explode(",", $excludeInvoiceEnv)) . "')";
            }

            $sql = "
            SELECT 
                COUNT(srrd.return_no) AS total
            FROM 
                sales_return_refund_dc srrd
            LEFT JOIN sales_invoice si on si.invoice_no = srrd.invoice_no
            WHERE 
                ((status <> 'cancelled_return' AND srrd.created_at < '" . $hiddenCancelledStatusDate . "') 
                    OR srrd.created_at > '" . $hiddenCancelledStatusDate . "')
            " . $excludeInvoice;

            $sql .= $storeFilter;

            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $total['return'] = $result->fetch()['total'];
        } catch (\Exception $e) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your data";
            return array();
        }

        return $total;
    }

    public function getDetailInvoiceLabel($params = array())
    {
        try {
            $invoiceModel = new \Models\SalesInvoice();
            $sql = "
            select 
                (
                    select order_no from sales_order
                    where si.sales_order_id  = sales_order_id
                ) as order_no,
                invoice_no,
                ss.track_number,
                soa.first_name,
                soa.last_name,
                soa.full_address,
                (
                    select kecamatan_name from master_kecamatan 
                    where soa.kecamatan_id = kecamatan_id
                ) as kecamatan_name,
                (
                    select city_name from master_city
                    where soa.city_id = city_id
                ) as city_name,
                (
                    select province_name from master_province 
                    where soa.province_id = province_id
                ) as province_name,
                soa.phone,
                soa.post_code
            from sales_invoice si
            left join sales_shipment ss on ss.invoice_id = si.invoice_id
            left join sales_order_address soa on ss.shipping_address_id = soa.sales_order_address_id
            where si.invoice_no = '" . $params['invoice_no'] . "'
            ";
            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $invoiceShipment = $result->fetch();

            $sql = "
                select
                    name,
                    qty_ordered
                from sales_invoice_item 
                where invoice_id in (
                    select invoice_id from sales_invoice where invoice_no = '" . $params['invoice_no'] . "'
                )
            ";
            $invoiceModel->useReadOnlyConnection();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );
            $invoiceItems = $result->fetchAll();
            $invoiceShipment['items'] = $invoiceItems;
        } catch (\Exception $e) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your data";
            return array();
        }

        return $invoiceShipment;
    }

    public function setStatusHold($sales_order_id = "", $remark = "", $order_no = "")
    {
        if (empty($sales_order_id)) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Sales order id is required";
            return;
        }

        $orderLib = new \Library\SalesOrder();
        $paramsUpdate['sales_order_id'] = $sales_order_id;
        $paramsUpdate['status'] = "hold";
        $orderLib->prepareOrderUpdate($paramsUpdate);
        if (empty($orderLib->getErrorCode())) {
            $orderLib->updateData();

            // save the remark
            $remarkHoldModel = new \Models\RemarkHold();
            $paramsHold['sales_order_id'] = $sales_order_id;
            $paramsHold['remark'] = $remark;
            $remarkHoldModel->setFromArray($paramsHold);
            $remarkHoldModel->save();
        }

        $params_data = array(
            "message" => "[ALERT] Hold Order : " . $order_no . ".\nRemark : " . $remark,
            "slack_channel" => $_ENV['HOLD_ORDER_SLACK_CHANNEL'],
            "slack_username" => $_ENV['OPS_SLACK_USERNAME']
        );

        $this->notificationError($params_data);

        if (strpos($remark, "qty")) {
            $params_data = array(
                "message" => "[ALERT] Hold Order : " . $order_no . ".\nRemark : " . $remark,
                "slack_channel" => $_ENV['MERCHANDISE_SLACK_CHANNEL'],
                "slack_username" => $_ENV['OPS_SLACK_USERNAME']
            );

            $this->notificationError($params_data);
        }
    }

    private function getOrderStatus($order_no = "")
    {
        if (empty($order_no)) {
            return "";
        }

        $salesOrderModel = new \Models\SalesOrder();
        $sql = "SELECT status FROM sales_order where order_no ='" . $order_no . "' limit 1";
        $result = $salesOrderModel->getDi()->getShared('dbMaster')->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $status = $result->fetchAll()[0]['status'];
        return $status;
    }

    private function updateOrderStatus($order_no = "", $status = "")
    {
        $salesOrderModel = new \Models\SalesOrder();
        $sql = "UPDATE sales_order SET status = '" . $status . "' WHERE order_no = '" . $order_no . "'";
        $result = $salesOrderModel->getDi()->getShared('dbMaster')->execute($sql);
        return $result;
    }

    private function isSalesOrderItemIdExist($sales_order_item_id = 0)
    {
        $salesOrderModel = new \Models\SalesOrder();
        $sql = "SELECT count(invoice_item_id) as totalCount FROM sales_invoice_item where sales_order_item_id =" . $sales_order_item_id;
        $result = $salesOrderModel->getDi()->getShared('dbMaster')->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $totalCount = $result->fetchAll()[0]['totalCount'];
        if ($totalCount > 0) {
            return true;
        }

        return false;
    }

    public function createInvoice($order_no = "", $payloadData = array())
    {
        if (empty($order_no)) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Order no is required";
            return;
        }

        // Prevent race condition
        $orderStatus = $this->getOrderStatus($order_no);
        if ($orderStatus <> 'approved') { // prevent to interupt approved hold order process
            if ($orderStatus == 'processing') {
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Order already in processing status";
                return;
            }

            // decide to choose this func to update order status, becasue it's more fast
            if (!$this->updateOrderStatus($order_no, 'processing')) {
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Update order status failed";
                return;
            }
        }
        // end prevent race condition

        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderData = $salesOrderLib->getSalesOrderDetail(['order_no' => $order_no]);
        if (count($salesOrderData) == 0) {
            $this->errorCode = "RR106";
            $this->errorMessages[] = "Order data not found";
            return;
        }

        /*
         * Update order to processing before create invoice
         * to prevent invoice creating twice
         */
        $this->salesOrder = $salesOrderLib->getSalesOrder();
        if (empty($this->salesOrder)) {
            $this->salesOrder = new \Models\SalesOrder();
            $this->salesOrder->assign($salesOrderData);
        }

        $this->salesOrder->setCustomerId($salesOrderData['customer']['customer_id']);

        $orderStatus = "processing";
        if ($salesOrderData['order_type'] == 'additional_cost') {
            $orderStatus = "complete";
        }
        
        $this->salesOrder->setStatus($orderStatus);
        $this->salesOrder->saveData("invoice", $salesOrderData['order_no'] . "-updateStatusSalesOrder");

        $vendor = "";
        $isVendorsOrder = false;
        if (substr($order_no, 0, 4) == "ODIS" || substr($order_no, 0, 4) == "ODIT" || substr($order_no, 0, 4) == "ODIK") {
            $isVendorsOrder = true;
            if (substr($order_no, 0, 4) == "ODIS") {
                $vendor = "shopee";
            } else if (substr($order_no, 0, 4) == "ODIT") {
                $vendor = "tokopedia";
            } else if (substr($order_no, 0, 4) == "ODIK") {
                $vendor = "tiktok";
            }
        }

        /** Start order validation **/

        //cek if customer using marketing gift voucher , and total is same with voucher value
        $subTotalGiftCards = 0;
        $salesOrderData['gift_cards'] = !empty($salesOrderData['gift_cards']) ? $salesOrderData['gift_cards'] : '[]';
        $giftCards = json_decode($salesOrderData['gift_cards']);
        $holdByVoucher = false;
        $blackListVoucher = explode(',', getenv('BLACKLIST_VOUCHER'));
        $ruleIdReleaseHold = explode(',', getenv('RULE_ID_RELEASE_HOLD'));
        $ruleIdHold = explode(',', getenv('RULE_ID_HOLD'));
        $posVoucherRuleIDs = [getenv('POS_VOUCHER_RULE_ID_AHI'), getenv('POS_VOUCHER_RULE_ID_HCI'), getenv('POS_VOUCHER_RULE_ID_TGI'), getenv('POS_VOUCHER_RULE_ID_CORP')];
        $posVoucherType = [getenv('VOUCHER_TYPE_SELL'), getenv('VOUCHER_TYPE_GIFT')];
        $flagGiftCardRedeem = false;
        $flagGiftCardGeneral = false;
        $flagRuleIdRelease = false;
        $flagRuleIdHold = false;
        $isPosVoucherExists = false;

        foreach ($giftCards as $gift) {

            if ($gift->voucher_type == 3 || $gift->voucher_type == 5) {
                $subTotalGiftCards += $gift->voucher_amount_used;
            }

            if ($gift->voucher_type == 2 || $gift->voucher_type == 5 || $gift->voucher_type == 10 ) {
                $flagGiftCardRedeem = true;
            } else if ($gift->voucher_type != 1 && $gift->voucher_type != 6) {
                $flagGiftCardGeneral = true;
            }

            // Hold Order by voucher code
            if (in_array($gift->voucher_code, $blackListVoucher)) {
                $holdByVoucher = true;
            }

            // Un Hold Order by Rule Id
            if (in_array($gift->rule_id, $ruleIdReleaseHold)) {
                $flagRuleIdRelease = true;
            }

            // Hold Order by Rule Id
            if (in_array($gift->rule_id, $ruleIdHold)) {
                $flagRuleIdHold = true;
            }

            // check if pos voucher exists
            if (in_array($gift->rule_id, $posVoucherRuleIDs)) {
                $isPosVoucherExists = true;
            }

        }

        // $flagSkuHoldQty = false;
        // $sku = '';
        $holdByShippingAmount = false;

        $ownfleetInformaItemIds = array();
        $ownfleetInformaFreeDelivName = getenv('OWNFLEET_INFORMA_FREE_DELIVERY_NAME');
        $ownfleetInformaFreeDelivNameLen = strlen($ownfleetInformaFreeDelivName);
        $orderPrefix = substr($salesOrderData['order_no'], 0, 4);
        $count1000DCItemQty = 0;
        foreach ($salesOrderData['items'] as $item) {
            // $qtySku = intval($item['qty_ordered']);
            // if ($qtySku >= 50 && ($orderPrefix != 'ODIR' && $orderPrefix != 'ODIA' && $orderPrefix != 'ODIH' && $orderPrefix != 'ODII')) {
            //     $flagSkuHoldQty = true;
            //     $sku = $item['sku'];
            // }
            if ($ownfleetInformaFreeDelivNameLen > 0) {
                $jsonArray = json_decode($item['cart_rules'], true);
                foreach ($jsonArray as $cartRule) {
                    if (substr($cartRule['name'], 0, $ownfleetInformaFreeDelivNameLen) == $ownfleetInformaFreeDelivName) {
                        array_push($ownfleetInformaItemIds, intval($item['sales_order_item_id']));
                    }
                }
            }

            $storeHold = explode(',', getenv('STORE_HOLD'));
            $isHoldStoreCode = false;
            if (in_array($item['store_code'], $storeHold)) {
                $isHoldStoreCode = true;
            }

            if ($item['shipping_amount'] < $item['shipping_discount_amount']) {
                $holdByShippingAmount = true;
            }

            if (substr($item['store_code'], 0, 6) == "1000DC") {
                $count1000DCItemQty += $item['qty_ordered'];
            }
        }

        $holdStatus = false;

        // check if the order contain sku that include in sku list to hold
        $productModel = new \Models\Product();
        $paramCheckSku['order_no'] = $order_no;
        $isBlacklistSku = $productModel->isBlacklistSku($paramCheckSku);

        //check MySQL Database is the order TP is process
        $totalRow = 0;
        $salesOrderTpModel = new \Models\SalesOrderTp();

        $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='" . $order_no . "' AND status = 'process' AND source = 'Create TP'";
        $result = $salesOrderTpModel->getDi()->getShared('dbMaster')->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $totalRow = $result->fetchAll()[0]['totalOdi'];
        // if there is process TP then hold the order
        if ($totalRow > 0) {
            $holdStatus = true;
            $this->setStatusHold($salesOrderData['sales_order_id'], "Order memiliki TP yang masih diproses atau error", $salesOrderData['order_no']);
            $this->errorCode = "RR404";
            $this->errorMessages[] = "Order memiliki TP yang masih diproses, Order temporary Hold.";
        }

        $checkExistTPsql = "SELECT SUM(qty) as totalItemQty FROM sales_order_tp where odi ='" . $order_no . "' AND source = 'Create TP'";
        $resultCheckExistsTPQty = $salesOrderTpModel->getDi()->getShared('dbMaster')->query($checkExistTPsql);

        $resultCheckExistsTPQty->setFetchMode(
            Db::FETCH_ASSOC
        );

        $totalRowCheckExistsTPQty = $resultCheckExistsTPQty->fetchAll()[0]['totalItemQty'];

        // hold order if tp not found but store code is 1000DC
        if ($count1000DCItemQty > 0 && $count1000DCItemQty > $totalRowCheckExistsTPQty){
            $holdStatus = true;
            $this->setStatusHold($salesOrderData['sales_order_id'], "Order seharusnya memiliki TP namun belum terbuat", $salesOrderData['order_no']);
            $this->errorCode = "RR404";
            $this->errorMessages[] = "Order seharusnya memiliki TP namun belum terbuat.";
        }

        // https://ruparupa.atlassian.net/browse/RRTICKET-3168
        if ($isBlacklistSku == 'yes' && $salesOrderData['status'] <> 'approved') {
            $holdStatus = true;
            $this->setStatusHold($salesOrderData['sales_order_id'], "SKU blacklist", $salesOrderData['order_no']);
            $this->errorCode = "RR404";
            $this->errorMessages[] = "SKU is in Blacklist, Order temporary Hold.";
        }

        if (
            $salesOrderData['shipping_amount'] - $salesOrderData['shipping_discount_amount'] == $salesOrderData['grand_total'] &&
            $salesOrderData['status'] <> 'approved' &&
            substr($salesOrderData['order_no'], 0, 4) != "ODIN"
        ) {
            if (!$flagGiftCardRedeem) {
                if (!$flagRuleIdRelease) {
                    $holdStatus = true;
                    $this->setStatusHold($salesOrderData['sales_order_id'], "Grand total exclude shipping 0 without voucher redeem / refund", $salesOrderData['order_no']);
                    $this->errorCode = "RR404";
                    $this->errorMessages[] = "Grand total exclude shipping = 0, without voucher redeem / refund.";
                }
            }
        }

        if ($holdByVoucher && $salesOrderData['status'] <> 'approved') {
            $holdStatus = true;
            $this->setStatusHold($salesOrderData['sales_order_id'], "Blacklist Voucher Code", $salesOrderData['order_no']);
            $this->errorCode = "RR404";
            $this->errorMessages[] = "Blacklist Voucher Code, Order temporary Hold.";
        }

        if ($holdByShippingAmount && $salesOrderData['status'] <> 'approved') {
            $holdStatus = true;
            $this->setStatusHold($salesOrderData['sales_order_id'], "Shipping amount minus, voucher pickup does not match the grand total", $salesOrderData['order_no']);
            $this->errorCode = "RR404";
            $this->errorMessages[] = "Shipping amount minus, voucher pickup does not match the grand total, Order temporary Hold.";
        }

        if (!empty($salesOrderData['payment']) && $salesOrderData['payment']['type']  == 'term_of_payment') {
            $isHoldStatusTOP = true;
            $messageHoldSap = "Hold because have no so sap status";

            if (!empty($payloadData["so_sap_status"])) {
                $soSapStatus = $payloadData["so_sap_status"];
                $messageHoldSap = "Hold because status TOP is ".$soSapStatus;
                
                if ($soSapStatus == "A-Approved" || $soSapStatus == "D - Released" || $soSapStatus == "-Not performed") {
                    $isHoldStatusTOP = false;
                }    
            }
            
            if ($isHoldStatusTOP) {
                $holdStatus = true;
                $this->setStatusHold($salesOrderData['sales_order_id'], $messageHoldSap, $salesOrderData['order_no']);
                $this->errorCode = "RR400";
                $this->errorMessages[] = "SO SAP Status hold the order";
            }
        }

        // if ($flagSkuHoldQty && $salesOrderData['status'] <> 'approved') {
        //     $holdStatus = true;
        //     $this->setStatusHold($salesOrderData['sales_order_id'], "Sku " . $sku . " qty ordered is greater than 50", $salesOrderData['order_no']);
        //     $this->errorCode = "RR404";
        //     $this->errorMessages[] = "Sku " . $sku . " qty ordered is greater than 50, Order temporary Hold.";
        // }

        if ((!empty($salesOrderData['payment']) && ($salesOrderData['payment']['status'] != 'paid' && $salesOrderData['payment']['status'] != 'over_paid'))) {
            $this->errorCode = "RR404";
            $this->errorMessages[] = "Payment status not valid for create invoice";
        }

        // if there is pos voucher, then do pos voucher transaction
        if ($isPosVoucherExists && $salesOrderData['status'] <> 'approved') {
            $posParams = array(
                'cart_id' => $salesOrderData['cart_id'],
                'order_no' => $salesOrderData['order_no']
            );
            $transPOSVoucher = $this->transPOSVoucher($posParams);
            if (!$transPOSVoucher) {
                $holdStatus = true;
                $this->setStatusHold($salesOrderData['sales_order_id'], "POS transaction failed", $salesOrderData['order_no']);
                $this->errorCode = "RR400";
                $this->errorMessages[] = "Transaction POS voucher failed";
            }

            // Slack Notif
            $params_data = array(
                "message" => $voucherOfflineMsg,
                "slack_channel" => $_ENV['OPS_ISSUE_SLACK_CHANNEL'],
                "slack_username" => $_ENV['OPS_ISSUE_SLACK_USERNAME']
            );
            $this->notificationError($params_data);
        }

        // check order by email
        $paramsCheck['email'] = isset($salesOrderData['customer']['customer_email']) ? $salesOrderData['customer']['customer_email'] : "";
        $paramsCheck['customer_id'] = isset($salesOrderData['customer']['customer_id']) ? $salesOrderData['customer']['customer_id'] : "";
        $suspectedAbuser = explode(',', getenv('SUSPECTED_ABUSER'));
        if (in_array($paramsCheck['email'], $suspectedAbuser)) {
            $holdStatus = true;
            $this->setStatusHold($salesOrderData['sales_order_id'], "Suspect Customer abuser by Email ", $salesOrderData['order_no']);
            $this->errorCode = "RR400";
            $this->errorMessages[] = "Suspect Customer abuser by Email ";

            $emailSlackNotifMsg = "Order by email " . $paramsCheck['email'] . " with order no " . $salesOrderData['order_no'];
            $emailSlackNotif = array(
                "message" => $emailSlackNotifMsg,
                "slack_channel" => $_ENV['OPS_ISSUE_SLACK_CHANNEL'],
                "slack_username" => $_ENV['OPS_ISSUE_SLACK_USERNAME']
            );
            $this->notificationError($emailSlackNotif);
        }

        // Store Hold
        $storeHoldStart = getenv('STORE_HOLD_START');
        $storeHoldEnd = getenv('STORE_HOLD_END');
        $nowDateTime = date('Y-m-d H:i:s');
        if (!empty($storeHold) && !empty($storeHoldStart) && !empty($storeHoldEnd)) {
            if ($nowDateTime >= $storeHoldStart && $nowDateTime <= $storeHoldEnd) {
                if ($isHoldStoreCode && $salesOrderData['status'] <> 'approved') {
                    $holdStatus = true;
                    $this->setStatusHold($salesOrderData['sales_order_id'], "Order Hold because actived Store Hold", $salesOrderData['order_no']);
                    $this->errorCode = "RR400";
                    $this->errorMessages[] = "Order Hold Because actived Store Hold";
                }
            }
        }

        // RRTICKET-24880 - Hold order if contains Rule ID
        if ($flagRuleIdHold && $salesOrderData['status'] <> 'approved') {
            $holdStatus = true;
            $holdRemark = "Order Hold because contains Rule ID " . getenv('RULE_ID_HOLD');
            $this->setStatusHold($salesOrderData['sales_order_id'], $holdRemark, $salesOrderData['order_no']);
            $this->errorCode = "RR404";
            $this->errorMessages[] = $holdRemark;
        }

        if ($salesOrderData['remark_hold'] == "failed redeem stamps voucher") {
            $holdStatus = true;
            $holdRemark = "Order Hold because failed redeem stamps voucher";
            $this->setStatusHold($salesOrderData['sales_order_id'], $holdRemark, $salesOrderData['order_no']);
            $this->errorCode = "RR404";
            $this->errorMessages[] = $holdRemark;
        }

        if (!empty($this->errorCode)) {
            // Revert to pending only if not on hold status
            if (!$holdStatus) {
                $this->salesOrder->setStatus("new");
                $this->salesOrder->saveData("invoice", $salesOrderData['order_no'] . "-revertStatusSalesOrderNotHold");
            }

            return;
        }

        /** End order validation **/

        $salesOrderData['status'] = "processing";

        if ($salesOrderData['order_type'] == 'additional_cost') {
            $salesOrderData['status'] = "complete";
        }

        //check MySQL Database is the order TP is success
        $checkSalesOrderTpSuccess = 0;
        $salesOrderTpModel = new \Models\SalesOrderTp();

        if ($isVendorsOrder == true) {
            $sqlCheckTPSuccess = "SELECT sku FROM sales_order_tp where odi ='" . $order_no . "' AND status = 'success' AND source = 'Create TP' ";
            $resultTPSuccess = $salesOrderTpModel->getDi()->getShared('dbMaster')->query($sqlCheckTPSuccess);

            $resultTPSuccess->setFetchMode(
                Db::FETCH_ASSOC
            );

            $checkSalesOrderTpSuccess = $resultTPSuccess->fetchAll();
        }

        //check MySQL Database is the order TP is failed
        $checkSalesOrderTp = 0;
        $salesOrderTpModel = new \Models\SalesOrderTp();

        $sql = "SELECT sku, message FROM sales_order_tp where odi ='" . $order_no . "' AND status = 'failed' AND source = 'Create TP' ";
        $result = $salesOrderTpModel->getDi()->getShared('dbMaster')->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $checkSalesOrderTp = $result->fetchAll();

        // Get the highest group shipment
        $groupShipmentContainer = 0;
        $groupShipmentContainerDC = -1;
        $checkTPSuccess = 0;
        $isContainsErrorOtherThanOutOfStock = false;
        foreach ($salesOrderData['items'] as $item) {
            if ($groupShipmentContainer < $item['group_shipment']) {
                $groupShipmentContainer = $item['group_shipment'];
            }
            if ($item['store_code'] == "DC") {
                $groupShipmentContainerDC = $item['group_shipment'];
            }
            // Check is there any error tp
            if (strpos($item['store_code'], '1000DC') !== false) {
                $checkStatus = 0;
                foreach ($checkSalesOrderTp as $row) {
                    if (strpos($row['message'], 'E Deficit of Unrestricted-use 0 EA') != true) {
                        $isContainsErrorOtherThanOutOfStock = true;
                    }

                    if ($item["sku"] == $row["sku"]) {
                        $checkStatus = 1;
                        break;
                    }
                }
                if ($checkStatus == 0) {
                    $checkTPSuccess = 1;
                }
            }
        }

        // Check each item
        $indexItem = 0;
        $groupShipmentContainer = $groupShipmentContainer + 1;
        foreach ($salesOrderData['items'] as $item) {
            // check if the order TP is still in process or failed
            if (count($checkSalesOrderTp) == 0) {
                //Change 1000DC Store Code into DC so DC store and 1000DC store invoice can be combined into one invoice
                if (strpos($item['store_code'], '1000DC') !== false) {
                    $salesOrderData['items'][$indexItem]["store_code"] = str_replace("1000", "", $item['store_code']);
                    // Change group assignment if there are DC item
                    if ($groupShipmentContainerDC != -1) {
                        \Helpers\LogHelper::log('test_split_invoice_1000DC', 'masuk if ada store dc');
                        $salesOrderData['items'][$indexItem]['group_shipment'] = $groupShipmentContainerDC;

                        $salesOrderItemModel = new \Models\SalesOrderItem;
                        $sql = 'UPDATE sales_order_item SET group_shipment = ' . $groupShipmentContainerDC . ' WHERE sales_order_item_id = ' . $item["sales_order_item_id"] . '';
                        $salesOrderItemModel->useWriteConnection();
                        try {
                            $salesOrderItemModel->getDi()->getShared($salesOrderItemModel->getConnection())->query($sql);
                        } catch (\Exception $e) {
                            \Helpers\LogHelper::log('error_invoice_1000DC', 'Failed update 1000DC group shipment item to DC group shipment where all tp is succes for order no : ' . $salesOrderData["order_no"] . ', ERROR : ' . $e->getMessage());
                            return;
                        }
                    }
                }
            } else {
                //Change 1000DC Store Code into DC so DC store and 1000DC store invoice can be combined into one invoice
                if (strpos($item['store_code'], '1000DC') !== false) {
                    $isChanged = 0;
                    // Check is the sku is the error tp sku
                    foreach ($checkSalesOrderTp as $row) {
                        \Helpers\LogHelper::log('error_invoice_1000DC', 'sku : ' . $item["sku"] . ', check sku : ' . $row["sku"]);
                        if ($isVendorsOrder == true && $isContainsErrorOtherThanOutOfStock == true) {
                            $isChanged = 1;
                            break;
                        } else if ($isVendorsOrder == true && count($checkSalesOrderTpSuccess) != 0) {
                            break;
                        } else {
                            if ($item["sku"] == $row["sku"]) {
                                // $item["store_code"] = "exclude";
                                $isChanged = 1;
                                // break;
                            }
                        }
                    }

                    if ($isChanged == 0) {
                        $salesOrderData['items'][$indexItem]["store_code"] = str_replace("1000", "", $item['store_code']);
                        if ($groupShipmentContainerDC != -1) {
                            $salesOrderData['items'][$indexItem]['group_shipment'] = $groupShipmentContainerDC;

                            $salesOrderItemModel = new \Models\SalesOrderItem;
                            $sql = 'UPDATE sales_order_item SET group_shipment = ' . $groupShipmentContainerDC . ' WHERE sales_order_item_id = ' . $item["sales_order_item_id"] . '';
                            $salesOrderItemModel->useWriteConnection();

                            try {
                                $salesOrderItemModel->getDi()->getShared($salesOrderItemModel->getConnection())->query($sql);
                            } catch (\Exception $e) {
                                \Helpers\LogHelper::log('error_invoice_1000DC', 'Failed update 1000DC group shipment item to DC group shipment for order no : ' . $salesOrderData["order_no"] . ', ERROR : ' . $e->getMessage());
                                return;
                            }
                        }
                    } else {
                        // Change group shipment and add new row in sales_order_address if there are no DC item
                        if ($groupShipmentContainerDC == -1) {
                            // Check is there sku that tp is success
                            if ($checkTPSuccess == 1) {
                                $salesOrderAddressModel = new \Models\SalesOrderAddress;

                                // Check is data address exists
                                $sqlCheck = "SELECT sales_order_id
                                            FROM sales_order_address
                                            WHERE sales_order_id = " . $salesOrderData['sales_order_id'] . "
                                            AND group_shipment = " . $groupShipmentContainer . "
                                            ";
                                $result = $salesOrderAddressModel->getDi()->getShared('dbMaster')->query($sqlCheck);

                                $result->setFetchMode(
                                    Db::FETCH_ASSOC
                                );

                                $checkDataAddress = $result->fetchAll();

                                if (count($checkDataAddress) < 1) {
                                    // Get data from sales_order_address
                                    $sqlCheck = "SELECT sales_order_id,company_address_id,country_id,province_id,city_id,kecamatan_id,kelurahan_id,first_name,last_name,phone,full_address,post_code,address_type,address_name,geolocation,carrier_id,group_shipment 
                                    FROM sales_order_address
                                    WHERE sales_order_id = " . $salesOrderData['sales_order_id'] . "
                                    AND group_shipment = " . $item['group_shipment'] . "
                                    ";
                                    $result = $salesOrderAddressModel->getDi()->getShared('dbMaster')->query($sqlCheck);

                                    $result->setFetchMode(
                                        Db::FETCH_ASSOC
                                    );

                                    $dataAddressNew = $result->fetchAll();
                                    // Add new row in sales_order_address
                                    $sql = "INSERT INTO sales_order_address (sales_order_id,company_address_id,country_id,province_id,city_id,kecamatan_id,kelurahan_id,first_name,last_name,phone,full_address,post_code,address_type,address_name,geolocation,carrier_id,group_shipment)";
                                    $sql .= 'VALUES(
                                        ' . $dataAddressNew[0]["sales_order_id"] . ',
                                        "' . $dataAddressNew[0]["company_address_id"] . '",
                                        ' . $dataAddressNew[0]["country_id"] . ',
                                        ' . $dataAddressNew[0]["province_id"] . ',
                                        ' . $dataAddressNew[0]["city_id"] . ',
                                        ' . $dataAddressNew[0]["kecamatan_id"] . ',
                                        ' . $dataAddressNew[0]["kelurahan_id"] . ',
                                        "' . $dataAddressNew[0]["first_name"] . '",
                                        "' . $dataAddressNew[0]["last_name"] . '",
                                        "' . $dataAddressNew[0]["phone"] . '",
                                        "' . $dataAddressNew[0]["full_address"] . '",
                                        "' . $dataAddressNew[0]["post_code"] . '",
                                        "' . $dataAddressNew[0]["address_type"] . '",
                                        "' . $dataAddressNew[0]["address_name"] . '",
                                        "' . $dataAddressNew[0]["geolocation"] . '",
                                        ' . $dataAddressNew[0]["carrier_id"] . ',
                                        ' . $groupShipmentContainer . '
                                    )
                                    ';
                                    $salesOrderAddressModel->useWriteConnection();
                                    try {
                                        $salesOrderAddressModel->getDi()->getShared($salesOrderAddressModel->getConnection())->query($sql);
                                    } catch (\Exception $e) {
                                        \Helpers\LogHelper::log('error_invoice_1000DC', 'Failed inserting new row to sales_order_address for order no : ' . $salesOrderData["order_no"] . ', ERROR : ' . $e->getMessage());
                                        return;
                                    }
                                }

                                // Change group shipment

                                $salesOrderData['items'][$indexItem]['group_shipment'] = $groupShipmentContainer;

                                $salesOrderItemModel = new \Models\SalesOrderItem;
                                $sql = 'UPDATE sales_order_item SET group_shipment = ' . $groupShipmentContainer . ' WHERE sales_order_item_id = ' . $item["sales_order_item_id"] . '';
                                $salesOrderItemModel->useWriteConnection();
                                try {
                                    $salesOrderItemModel->getDi()->getShared($salesOrderItemModel->getConnection())->query($sql);
                                } catch (\Exception $e) {
                                    \Helpers\LogHelper::log('error_invoice_1000DC', 'Failed updating 1000DC group shipment item to new group shipment for order no : ' . $salesOrderData["order_no"] . ', ERROR : ' . $e->getMessage());
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            $indexItem = $indexItem + 1;
        }


        $shipmentGroup = $this->groupShipment($salesOrderData);
        $invoiceGroup = $this->splitInvoice($salesOrderData['items'], $checkSalesOrderTp);
        $invoiceList = array();
        $invoiceItemMergedList = array();
        $preorderList = array();
        $salesOrderItemList = array();
        $invoiceNo = "";

        $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
        $salesOrderItemMdr = $salesOrderItemMdrModel->find("sales_order_id = ".$salesOrderData['sales_order_id']);
        
        foreach ($invoiceGroup as $groupShipment => $invoiceByPreorder) {
            foreach ($invoiceByPreorder as $isPreOrder => $invoices) {
                // Begin Duplicate sales_order_item_id Validation
                $flagDuplicateSOID = false;
                foreach ($invoices as $item) {
                    $isExistSoiID = $this->isSalesOrderItemIdExist($item['sales_order_item_id']);

                    if (!in_array($item['sales_order_item_id'], $salesOrderItemList) && !$isExistSoiID) {
                        $salesOrderItemList[] = $item['sales_order_item_id'];
                    } else {
                        $flagDuplicateSOID = true;
                        break;
                    }
                }

                if ($flagDuplicateSOID) {
                    continue;
                }
                // End Duplicate sales_order_item_id Validation

                $invoiceModel = new \Models\SalesInvoice();
                $notSafe = true;
                $invoiceNo = ($isPreOrder == 10) ? \Helpers\Transaction::generateInvoicePreOrderNo() : \Helpers\Transaction::generateInvoiceNo();
                while ($notSafe) {
                    $sql = "SELECT count(invoice_no) as total_invoice from sales_invoice where invoice_no = '$invoiceNo'";
                    $result = $invoiceModel->getDi()->getShared('dbMaster')->query($sql);
                    $result->setFetchMode(Database::FETCH_ASSOC);

                    $invoiceChecker = $result->fetchAll()[0]['total_invoice'];

                    if ($invoiceChecker == 0) {
                        $notSafe = false;
                    }

                    if ($notSafe) {
                        $invoiceNo = ($isPreOrder == 10) ? \Helpers\Transaction::generateInvoicePreOrderNo() : \Helpers\Transaction::generateInvoiceNo();
                    }
                }

                $invoiceStatus = "new";
                if ($salesOrderData['order_type'] == 'additional_cost') {
                    $invoiceStatus = "received";
                }

                $invoiceData = [
                    'invoice_status' => $invoiceStatus,
                    'sales_order_id' => $salesOrderData['sales_order_id'],
                    "invoice_no" => $invoiceNo,
                    'company_code' => $salesOrderData['company_code']
                ];

                $salesOrderSubtotalWithHandling = $salesOrderData['subtotal'] + $salesOrderData['handling_fee_adjust'];

                $shippingAmount = $shippingDiscountAmount = $invoiceGiftCardAmount = $invoiceDiscountAmount = $handlingFeeAmount = 0;
                $invoiceItems = $shipmentList = $salesOrderItemIds = array();
                foreach ($invoices as $item) {
                    $invoiceData['delivery_method'] = $item['delivery_method'];
                    $invoiceData['store_code'] = $item['store_code'];
                    $invoiceData['supplier_alias'] = $item['supplier_alias'];
                    $invoiceData['pickup_code'] = !empty($item['pickup_code']) ? $item['pickup_code'] : "";
                    $invoiceGiftCardAmount += ($item['gift_cards_amount'] * $item['qty_ordered']);
                    // Prorate discount amount
                    if (!empty($salesOrderData['discount_amount'])) {
                        $invoiceDiscountAmount += ($item['discount_amount'] * $item['qty_ordered']);
                    }

                    if ($item['preorder_date'] != null && $item['preorder_date'] != '') {
                        $preorderItem['message'] =  "Produk akan mulai dikirimkan pada tanggal : " . $item['preorder_date'];
                        $preorderItem['sku'] = $item['sku'];
                        $preorderList[] =  $preorderItem;
                    }

                    if ($salesOrderItemMdr != false) {                        
                     foreach ($salesOrderItemMdr->toArray() as $itemMdr){
                        if ($item['sales_order_item_id'] == $itemMdr['sales_order_item_id'] ){
                            $item['mdr_customer'] = $itemMdr['value_customer'];
                            break;
                        }                         
                     }
                    }
                    

                    $invoiceItems[] = $item;
                    $salesOrderItemIds[] = $item['sales_order_item_id'];

                    // dont round the shipping & shipping discount amount, coz it will make different value with the total in the cart
                    $shippingAmount += $item['shipping_amount'] * $item['qty_ordered'];
                    $shippingDiscountAmount += $item['shipping_discount_amount'] * $item['qty_ordered'];
                    $handlingFeeAmount += $item['handling_fee_adjust'] * $item['qty_ordered'];

                    // Prepare shipment list
                    if (!isset($shipmentList[$item['group_shipment']])) {
                        $shipmentList[$item['group_shipment']] = $shipmentGroup[$item['group_shipment']];

                        if ($salesOrderData['order_type'] == "additional_cost") {
                            $shipmentList[$item['group_shipment']]['shipment_status'] = "received";
                        }
                    }
                }

                $sql = "SELECT coalesce(sum(voucher_amount_difference),0) as selisih, 
                               coalesce(sum(split_voucher_amount_difference),0) as selisih_split_voucher,
                               coalesce(sum(split_discount_amount_difference),0) as selisih_split_discount
                        from sales_order_item_giftcards 
                        where sales_order_item_id in (" . implode(",", $salesOrderItemIds) . ")";
                $result = $invoiceModel->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(Database::FETCH_ASSOC);

                $selisihResult = $result->fetchAll()[0];
                $selisih = $selisihResult['selisih'];
                $selisihSplitVoucher = $selisihResult['selisih_split_voucher'];
                $selisihSplitDiscount = $selisihResult['selisih_split_discount'];

                $invoiceData['gift_cards_amount'] = $invoiceGiftCardAmount + $selisih + $selisihSplitVoucher;
                $invoiceData['discount_amount'] = $invoiceDiscountAmount + $selisihSplitDiscount;
                $invoiceData['shipping_amount'] = $shippingAmount;
                $invoiceData['shipping_discount_amount'] = $shippingDiscountAmount;
                $invoiceData['handling_fee_adjust'] = $handlingFeeAmount;
                $invoiceData['items'] = $invoiceItems;
                $invoiceData['shipment'] = $shipmentList;
                $invoiceModel->setFromArray($invoiceData);

                // If order B2B Informa/Ace, store all invoice item in order to merge all invoice item
                if ($salesOrderData['order_type'] === "b2b_informa" || $salesOrderData['order_type'] === "b2b_ace") {
                    $b2bItems = $invoiceModel->getInvoiceItems();

                    for ($i = 0; $i < count($b2bItems); $i++) {
                        $installationSku = "";
                        if ($salesOrderData['order_type'] === "b2b_ace") {
                            $installationSku = getenv('INSTALLATION_ACEB2B_SKU');
                        } else {
                            $installationSku = getenv('INSTALLATION_INFORMAB2B_SKU');
                        }

                        if ($b2bItems[$i]->getSku() != $installationSku ||
                            ($b2bItems[$i]->getSku() == $installationSku && $b2bItems[$i]->getRowTotal() > 0)) { // B2b installation sku
                            $invoiceItemMergedList[] = $b2bItems[$i];
                        }
                    }
                }

                if (!$invoiceModel->checkExists()) {
                    $invoiceList[] = $invoiceModel;
                }
            }
        }

        if (count($invoiceList) === 0) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "No valid invoice found for this order";
            return;
        }

        $this->invoiceList = new \Models\Invoice\Collection($invoiceList);

        try {
            \Helpers\LogHelper::log('debug_625', '/app/library/Invoice.php line 2679', 'debug');
            $this->invoiceList->createInvoice();
        } catch (\Exception $e) {

            if (substr($order_no, 0, 4) == "ODIS" || substr($order_no, 0, 4) == "ODIT" || substr($order_no, 0, 4) == "ODIK") {
                $salesOrderModel = new \Models\SalesOrder();
                $sql = "SELECT shop_name FROM master_store_vendor msv WHERE msv.shop_id = (
                    SELECT shop_id FROM sales_order_vendor sov WHERE sov.order_no = '{$this->salesOrder->getOrderNo()}'
                ) AND msv.vendor = '{$vendor}'";

                $salesOrderModel->useReadOnlyConnection();
                $result = $salesOrderModel->getDi()->getShared($salesOrderModel->getConnection())->query($sql);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );
                $shopName = $result->fetch();

                $params_data = array();

                if (substr($order_no, 0, 4) == "ODIS") {
                    $params_data = array(
                        "message" => "Order " . $this->salesOrder->getSourceOrderNo() . " dari toko " . $shopName["shop_name"] . " mengalami error pada proses create invoice",
                        "slack_channel" => $_ENV['SHOPEE_ERROR_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['ORDER_SLACK_USERNAME']
                    );
                }

                if (substr($order_no, 0, 4) == "ODIT") {
                    $params_data = array(
                        "message" => "Order " . $this->salesOrder->getSourceOrderNo() . " dari toko " . $shopName["shop_name"] . " mengalami error pada proses create invoice",
                        "slack_channel" => $_ENV['TOKOPEDIA_ERROR_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['ORDER_SLACK_USERNAME']
                    );
                }

                $this->notificationError($params_data);
            }

            $this->salesOrder->setStatus("new");
            $this->salesOrder->saveData("invoice", $salesOrderData['order_no'] . "-revertStatusSalesOrder");
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        // move here
        // Send purchase event to GA4
        $this->sendPurchaseEventToGa4($salesOrderData, $invoiceList);

        foreach ($invoiceList as $invoice){
            if($invoice->getInvoiceStatus() === "new") {
                $param = array(
                    'data' => array(
                        'invoice_no' => $invoice->getInvoiceNo(),
                        'invoice_id' => $invoice->getInvoiceId(),
                        'action' => "store_reminder_new_order",
                    ),
                );
                \Helpers\LogHelper::log('debug_625', 'Send Email to Store for New Order', 'debug');
                $this->sendEmailToStoreForNewOrder($param);
            }
        }

        // Generate AWB using queue     
        $nsq = new \Library\Nsq();
        $message = [
            "data" => array(
                "order_no" => $salesOrderData['order_no'],
            ),
            "message" => "generateAwb"
        ];
        $nsq->publishCluster('shipment', $message);

        $salesOrderPayment = new \Models\SalesOrderPayment;
        $payment = $salesOrderPayment::findFirst("sales_order_id = '" . $salesOrderData['sales_order_id'] . "'");

        // If order B2B Informa/Ace, then send order to Internal Kawan Lama system (POS and SAP) with merged all invoice item
        if ( ($salesOrderData['order_type'] === "b2b_informa" || $salesOrderData['order_type'] === "b2b_ace") && sizeof($invoiceList) > 1 && sizeof($invoiceItemMergedList) > 0) {
            $invoiceDataModelMerged = new \Models\SalesInvoice();
            $invoiceDataModelMerged = $invoiceList[0];
            $invoiceDataModelMerged->setInvoiceItems($invoiceItemMergedList);
            $mergedInvoiceList = array();
            $mergedInvoiceList[] = $invoiceDataModelMerged;


            // Send order to Internal Kawan Lama system (POS and SAP) using merged invoice list
            $this->invoiceList = new \Models\Invoice\Collection($mergedInvoiceList);
            if ($payment->getMethod() != "term_of_payment") {
                $this->invoiceList->sendToKLSys();
            } else {
                $salesb2binfo = new SalesB2BInfoModel();
                $datab2binfo = $salesb2binfo::findFirst("sales_order_id = '" . $salesOrderData['sales_order_id'] . "'");

                $paramUpdateSoSapNumber = array(
                    "invoice_no" => $invoiceNo,
                    "sap_so_number" => $datab2binfo->getSoSapNumber()
                );

                $this->prepareUpdateSoSapNumber($paramUpdateSoSapNumber);
            }

            // Revert to original invoice list
            $this->invoiceList = new \Models\Invoice\Collection($invoiceList);
        } else {
            // Send order to Internal Kawan Lama system (POS and SAP)
            if ($payment->getMethod() != "term_of_payment") {
                $this->invoiceList->sendToKLSys();
            } else {
                $salesb2binfo = new SalesB2BInfoModel();
                $datab2binfo = $salesb2binfo::findFirst("sales_order_id = '" . $salesOrderData['sales_order_id'] . "'");

                $paramUpdateSoSapNumber = array(
                    "invoice_no" => $invoiceNo,
                    "sap_so_number" => $datab2binfo->getSoSapNumber()
                );

                $this->prepareUpdateSoSapNumber($paramUpdateSoSapNumber);
            }
        }

        $this->invoiceList->sendToSitory();
        // Sending usage of gift card
        if ($this->salesOrder->getGiftCardsAmount() > 0) {
            $journalSAP = new \Library\JournalSAP("", $this->salesOrder);
            $headerStatus = $journalSAP->prepareHeader();
            $createRedeemJournalItems = [
                "AHI" => [],
                "HCI" => [],
                "TGI" => [],
            ];

            if ($headerStatus) {
                $gosendRuleId = explode(',', getenv('GOSEND_RULE_ID'));

                foreach ($salesOrderData['items'] as $item) {
                    $itemGiftcardsModel = new \Models\SalesOrderItemGiftcards();
                    $gc_datas = $itemGiftcardsModel->find("sales_order_item_id = " . $item["sales_order_item_id"])->toArray();
                    if (!$gc_datas) {
                        continue;
                    }

                    $supplierModel = new \Models\Supplier();
                    $sql = "SELECT supplier.supplier_alias FROM store LEFT JOIN supplier ON store.supplier_id = supplier.supplier_id WHERE store.store_code = '" . $item['store_code'] . "'";
                    $result = $supplierModel->getDi()->getShared('dbMaster')->query($sql);
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $resultData = $result->fetchAll()[0];

                    if (!$resultData || !isset($resultData['supplier_alias']) || empty($resultData['supplier_alias'])) {
                        \Helpers\LogHelper::log("log_voucher_offline", "No supplier alias found on store code: " . $item['store_code']);
                        continue;
                    }

                    foreach ($gc_datas as $gc_data) {
                        // Prepare redeem journal               
                        if (in_array($gc_data['voucher_type'], $posVoucherType)) {
                            $journalSAP->prepareRedeemJournalGroup($createRedeemJournalItems, $gc_data['voucher_code'], $gc_data['voucher_amount_used'], $resultData['supplier_alias'], "pos_voucher");
                        }
                    }
                }

                // Create redeem journal
                foreach ($createRedeemJournalItems as $value) {
                    if (isset($value[0])) {
                        $journalSAP->createJournal("redeem", $value);
                    }
                }

                // Send reclass journal
                $giftCards = json_decode($this->salesOrder->getGiftCards(), true);
                foreach ($giftCards as $gc_data) {
                    if (in_array($gc_data['rule_id'], $gosendRuleId)) {
                        continue;
                    } elseif (substr($gc_data['voucher_code'], 0, 2) == "OS") {
                        continue;
                    }
                    $journalSAP->prepareReclassJournal($gc_data, false);
                    $journalSAP->createJournal("reclass");

                    if ($gc_data['voucher_source'] === "coin-exchange-to-product") {
                        if (intval($gc_data['voucher_amount_used']) > intval($gc_data['coin_amount'])) {
                            $tempGcData = $gc_data;
                            $tempGcData['voucher_code'] = $this->salesOrder->getOrderNo();
                            $journalSAP->prepareReclassJournal($tempGcData, false);
                            $journalSAP->createJournal("reclass");   
                        }
                    }               
                }
            }
        }

        // as a flag if store code from sales order contains 1000DC
        $isStoreCodeOrder1000DC = strpos($this->salesOrder->getStoreCode(), '1000DC') !== false;
        
        // Move stock on hold to new place
        $this->_deductStock($salesOrderData['items'], "shipment", $salesOrderData['order_no'], $isStoreCodeOrder1000DC);
        
        \Helpers\LogHelper::log('GA4', "[INFO]  INVOICE CREATED");
        // $this->checkPromotion('',$salesOrderData);
        $isVendor = false;
        if ($salesOrderData['device'] == "vendor") {
            $isVendor = true;
        }

        $this->checkMarketingInvoice($salesOrderData['cart_id'], $salesOrderData['order_no'], $isVendor, false);

        foreach ($invoiceList as $invoice) {
            # send email pembayaran sukses except for reorder & term of payment
            if ($payment->getMethod() != "term_of_payment") {
                if (empty($salesOrderData['reference_order_no'])) {
                    $this->sendSuccessPaymentNotif($preorderList, $invoice);
                } else {
                    $reorderParams['invoice_no'] = $invoice->getInvoiceNo();
                    $this->processReorderNewInvoice($reorderParams);
                }
            }

            $nsq = new \Library\Nsq();
            $message = [
                "data" => array(
                    "invoice_id" => $invoice->getInvoiceId(),
                    "order_no" => $salesOrderData['order_no'],
                ),
                "message" => "sendWAInstantCourier"
            ];
            $nsq->publishCluster('afterSales', $message);

            $fakturPajakInvoiceParams['invoice_no'] = $invoice->getInvoiceNo();
            $fakturPajakInvoiceParams['invoice_id'] = $invoice->getInvoiceId();
            $fakturPajakInvoiceParams['sales_order_id'] =  $invoice->getSalesOrderId();
            $this->insertFakturPajakInvoice($fakturPajakInvoiceParams);
        }

        $this->sendOrderNotification($salesOrderData['company_code']);

        // Check for Referrral Program
        if ($salesOrderData['device'] == "ios" || $salesOrderData['device'] == 'android' || $salesOrderData['device'] == 'rr-ios' || $salesOrderData['device'] == 'rr-android') {
            $referralProgramParams['customer_id'] = $paramsCheck['customer_id'];
            $referralProgramParams['order_no'] = $order_no;
            $customerReferralLib = new \Library\CustomerReferral();
            $customerReferralLib->checkReferralFirstPurchase($referralProgramParams);
        }

        if (count($ownfleetInformaItemIds) > 0) {
            $this->updateOwnfleetShipment($ownfleetInformaItemIds);
        }

        $salesOrderTrackerLib = new \Library\SalesOrderTracker();
        $salesOrderTrackerLib->invoiceSaveTrackerProcess($salesOrderData['sales_order_id']);

        $analyticsParams['order_no'] = $salesOrderData['order_no'];
        $this->sendCartAnalytics($analyticsParams);
        \Helpers\LogHelper::log('GA4', "[INFO]  INVOICE CREATED " . $salesOrderData['customer']['customer_email']);
        // for push notification abadon cart Emarsys
        if (isset($salesOrderData['customer']['customer_email']) && isset($salesOrderData['order_no']) && isset($salesOrderData['company_code']) && isset($invoiceList)) {
            if ($salesOrderData['device'] == "mobile" || $salesOrderData['device'] == "desktop" || $salesOrderData['device'] == "ios" || $salesOrderData['device'] == "android" || $salesOrderData['device'] == "rr-ios" || $salesOrderData['device'] == "rr-android" || $salesOrderData['device'] == "informa-android" || $salesOrderData['device'] == "informa-ios" || $salesOrderData['device'] == "informa-huawei") {
                $invoice_no_list = array();
                $totalSales = $salesOrderData['subtotal'] + $salesOrderData['handling_fee_adjust'] - $salesOrderData['discount_amount'];

                foreach ($invoiceList as $invoice) {
                    array_push($invoice_no_list, $invoice->getInvoiceNo());
                }

                $paid_at = "";
                if (count($invoiceList) > 0) {
                    $paid_at = $invoiceList[0]->getCreatedAt();
                }

                // Get lexicon item category from elastic
                $categoryLexicon = array();
                $categoryLexiconLvl1 = array();
                $categoryLexiconLvl2 = array();
                $categoryLexiconLvl3 = array();
                $categoryLexiconLvl4 = array();
                foreach ($salesOrderData['items'] as $item) {
                    $esProduct = $productModel->searchProductFromElastic($item['sku']);
                    if ($esProduct) {
                        $lexiconLvl1 = (isset($esProduct['categories_vendor'][0]['name']) ? $esProduct['categories_vendor'][0]['name'] : '');
                        $lexiconLvl2 = (isset($esProduct['categories_vendor'][1]['name']) ? $esProduct['categories_vendor'][1]['name'] : '');
                        $lexiconLvl3 = (isset($esProduct['categories_vendor'][2]['name']) ? $esProduct['categories_vendor'][2]['name'] : '');
                        $lexiconLvl4 = (isset($esProduct['categories_vendor'][3]['name']) ? $esProduct['categories_vendor'][3]['name'] : '');
                        $categoryLexiconCombine = $lexiconLvl1 . ">" . $lexiconLvl2 . ">" . $lexiconLvl3 . ">" . $lexiconLvl4;
                        array_push($categoryLexicon, $categoryLexiconCombine);
                        array_push($categoryLexiconLvl1, $lexiconLvl1);
                        array_push($categoryLexiconLvl2, $lexiconLvl2);
                        array_push($categoryLexiconLvl3, $lexiconLvl3);
                        array_push($categoryLexiconLvl4, $lexiconLvl4);
                    };
                }

                // publish to emarsys topic
                $nsq = new \Library\Nsq();
                $message = [
                    "message" => "customEvent",
                    "data" => [
                        "order_no" => $salesOrderData['order_no'],
                        "invoices" => $invoice_no_list,
                        "company_code" => $salesOrderData['company_code'],
                        "customer_id" => $salesOrderData['customer']['customer_id'],
                        "customer_email" => $salesOrderData['customer']['customer_email'],
                        "grand_total" => $salesOrderData['grand_total'],
                        "cart_id" => $salesOrderData['cart_id'],
                        "invoices_data" => $invoiceList,
                        "gift_cards" => $giftCards,
                        "device" => $salesOrderData['device'],
                        "paid_at" => $paid_at,
                        "event_type" => 'order_paid',
                        "total_sales" => $totalSales,
                        "category_lexicon" => $categoryLexicon,
                        "category_lexicon_lvl_1" => $categoryLexiconLvl1,
                        "category_lexicon_lvl_2" => $categoryLexiconLvl2,
                        "category_lexicon_lvl_3" => $categoryLexiconLvl3,
                        "category_lexicon_lvl_4" => $categoryLexiconLvl4
                    ]
                ];

                if (!$nsq->publishCluster('emarsys', $message)) {
                    \Helpers\LogHelper::log('emarsys_push_notif', "[FAILED] Publish to NSQ || Order No : " . $salesOrderData['order_no'] . ", Invoice No : " . $invoice_no_list . " ");
                }

                // publish to affiliate topic
                $nsq = new \Library\Nsq();
                $message = [
                    "message" => "potentialCommission",
                    "data" => [
                        "cart_id" => $salesOrderData['cart_id'],
                        "sales_order_id" => $salesOrderData['sales_order_id']
                    ]
                ];

                if (!$nsq->publishCluster('affiliate', $message)) {
                    \Helpers\LogHelper::log('affiliate', "[FAILED] Publish to NSQ || Order No : " . $salesOrderData['order_no'] . ", Sales Order ID : " . $salesOrderData['order_no'] . " ");
                }
            } else {
                \Helpers\LogHelper::log('GA4', "ELSE 1");
            }
        } else {
            \Helpers\LogHelper::log('GA4', "ELSE 2");
        }

        // Send message to customer to notify payment success if order_type is wa_shopping or ctwa_shopping
        if (isset($salesOrderData['order_type']) && isset($invoiceList)) {
            if (($salesOrderData['order_type'] == "wa_shopping" || $salesOrderData['order_type'] == "ctwa_shopping") && count($invoiceList) > 0) {
                $invoice_no_list = array();
                foreach ($invoiceList as $invoice) {
                    array_push($invoice_no_list, $invoice->getInvoiceNo());
                }

                // Publish to send WA Shopping message queue
                $nsq = new \Library\Nsq();
                $message = [
                    "message" => "whatsappShoppingSendMessage",
                    "data" => [
                        "recipient_phone" => $salesOrderData['customer']['customer_phone'],
                        "template" => "payment_successful",
                        "value" => [
                            "invoice_no" => join(", ", $invoice_no_list)
                        ]
                    ]
                ];

                if (!$nsq->publishCluster('transaction', $message)) {
                    \Helpers\LogHelper::log('wa_shopping_payment_success', "[FAILED] Publish to NSQ || Order No : " . $salesOrderData['order_no'] . ", Invoice No : " . $invoice_no_list . " ");
                }
            }
        }

        $salesOrderPayment = new \Models\SalesOrderPayment;
        $payment = $salesOrderPayment::findFirst("sales_order_id = '" . $salesOrderData['sales_order_id'] . "'");
        $payment->setCanCancel(0);
        $payment->saveData("savePayment");

        $upgradeSkuGold = getenv('SKU_UPGRADE_GOLD');
        foreach ($salesOrderData['items'] as $item) {
            if ($item['sku'] == $upgradeSkuGold) {
                $isLevelUpgrade = $this->instantUpgradeLevelUnify($salesOrderData['customer']['customer_id']);
                if (!$isLevelUpgrade) {
                    \Helpers\LogHelper::log('cohesive_instant_upgrade', "[FAILED] cohesive instant upgrade || Order No : " . $salesOrderData['order_no'] . ", customerID : " . $salesOrderData['customer']['customer_id'] . " ");
                }
                break;
            }
        }
    
        if (isset($salesOrderData['order_type']) && ($salesOrderData['order_type'] == "b2b_informa" || $salesOrderData['order_type'] == "b2b_ace" || $salesOrderData['order_type'] == "additional_cost")){
            $this->processB2BInvoice($salesOrderData);
        } else {
            // Check if current order contain installation
            $this->checkInvoiceContainInstallation($invoiceList, $salesOrderData);
        }

        return;
    }

    private function sendPurchaseEventToGa4($salesOrderData, $invoiceList) {
        foreach ($invoiceList as $invoice) {
            $invoiceItems = $invoice->getInvoiceItems();
            $salesSource = $this->getSalesSourceBySalesOrderItemId($invoiceItems[0]->getSalesOrderItemId());
            if ($salesSource == "Shopee" || $salesSource == "Tokopedia" || $salesSource == "Tiktok") {
                return;
            }
        }
        $cartData = $this->getCartData($salesOrderData['cart_id']);
        if (!$cartData) {
            \Helpers\LogHelper::log("GA4", "[ERROR] Failed to get cart data for cart id " . $salesOrderData['cart_id']);
        }

        \Helpers\LogHelper::log("GA4", "[INFO] Start to send purchase event to GA4 for cart_id " . $salesOrderData['cart_id']);
        $processedGA4Items = $this->processGA4Items($cartData);

        $identifierID = "";
        if ($salesOrderData['device'] == "ios" || $salesOrderData['device'] == "android" ||
            $salesOrderData['device'] == "rr-ios" || $salesOrderData['device'] == "rr-android" || 
            $salesOrderData['device'] == "informa-android" || $salesOrderData['device'] == "informa-ios" ||
            $salesOrderData['device'] == "informa-huawei") {

            $identifierID = $cartData['ga4_app_instance_id'];
        } else if ($salesOrderData['device'] == "desktop" || $salesOrderData['device'] == "mobile") {
            $identifierID = $cartData['ga4_client_id'];
        }

        $nsq = new \Library\Nsq();
        $message = [
            "message" => "eventPurchase",
            "data" => [
                "identifier_id"=> $identifierID,
                "customer_id"=> $salesOrderData['customer']['customer_id'],
                "device"=> $salesOrderData['device'],
                "sales_source"=> "RR Webs",
                "events"=> [
                    "name"=> "purchase",
                    "params"=> [
                        "currency"=> "IDR",
                        "value"=> ($salesOrderData['grand_total'] + 0),
                        "transaction_id"=> $salesOrderData['order_no'],
                        "items"=> $processedGA4Items
                    ]
                ]
            ]
        ];

        \Helpers\LogHelper::log("GA4", "[INFO] Sending purchase event to GA4 with message " . json_encode($message));
        if (!$nsq->publishCluster('GA4', $message)) {
            \Helpers\LogHelper::log('GA4', "[FAILED] Publish to NSQ || Order No : " . $salesOrderData['order_no'] . ", Invoice No : " . $invoice->getInvoiceNo() . " ");
        }
        \Helpers\LogHelper::log("GA4", "[INFO] Finish to send purchase event to GA4 for cart_id " . $salesOrderData['cart_id']);

        foreach ($invoiceList as $invoice) {
            \Helpers\LogHelper::log("GA4", "[INFO] Start to send purchase event to GA4 for invoice_no " . $invoice->invoice_no);
            $invoiceItems = $invoice->getInvoiceItems();
            $invoiceItemSkus = array();

            foreach ($invoiceItems as $item) {
                $invoiceItemSkus[$item->getSku()] = true;
            }

            $processedInvoiceGA4Items = array();

            foreach ($processedGA4Items as $item) {
                if (isset($invoiceItemSkus[$item['item_id']])) {
                    array_push($processedInvoiceGA4Items, $item);
                }
            }

            \Helpers\LogHelper::log("GA4", "[INFO] SalesOrderItemId: " . $invoiceItems[0]->getSalesOrderItemId());
            \Helpers\LogHelper::log("GA4", "[INFO] SalesSource: " . $salesSource);
            $salesSource = $this->getSalesSourceBySalesOrderItemId($invoiceItems[0]->getSalesOrderItemId());
            if ($salesSource != "Informa Online" && $salesSource != "Ace Online" && $salesSource != "Toys Kingdom Online") {
                \Helpers\LogHelper::log("GA4", "[INFO] Skip sending purchase event to GA4 for invalid sales source: " . $salesSource);
                continue;
            }
            $identifierID = $cartData['ga4_client_id'];

            $nsq = new \Library\Nsq();
            $message = [
                "message" => "eventPurchase",
                "data" => [
                    "identifier_id"=> $identifierID,
                    "customer_id"=> $salesOrderData['customer']['customer_id'],
                    "device"=> $salesOrderData['device'],
                    "sales_source"=> $salesSource,
                    "events"=> [
                        "name"=> "purchase",
                        "params"=> [
                            "currency"=> "IDR",
                            "value"=> ($invoice->getGrandTotal() + 0),
                            "transaction_id"=> $salesOrderData['order_no'],
                            "items"=> $processedInvoiceGA4Items
                        ]
                    ]
                ]
            ];

            \Helpers\LogHelper::log("GA4", "[INFO] Sending purchase event to GA4 for invoice_no " . $invoice->getInvoiceNo() . " with message " . json_encode($message));
            if (!$nsq->publishCluster('GA4', $message)) {
                \Helpers\LogHelper::log('GA4', "[FAILED] Publish to NSQ || Order No : " . $salesOrderData['order_no'] . ", Invoice No : " . $invoice->getInvoiceNo() . " ");
            }
            \Helpers\LogHelper::log("GA4", "[INFO] Finish to send purchase event to GA4 for cart_id " . $salesOrderData['cart_id']);
        }
    }

    private function getSalesSourceBySalesOrderItemId($salesOrderItemId) {
        $salesOrderIteFlatmModel = new \Models\SalesOrderItemFlat();
        $sql = "select sales_source from sales_order_item_flat where sales_order_item_id = '" . $salesOrderItemId . "'";
        $selectId = $salesOrderIteFlatmModel->getDi()->getShared('dbMaster')->query($sql);
        $selectId->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $results = $selectId->fetchAll();
        if (isset($results[0]['sales_source'])) {
            return $results[0]['sales_source'];   
        }
    }

    private function getCompanyCodeByStoreCode($store_code) {
        $storeModel = new \Models\Store();
        $sql = "select mbs.company_code from store s
            JOIN master_brand_store mbs ON mbs.brand_store_id = s.brand_store_id
            WHERE s.store_code = '" . $store_code . "'";

        $selectId = $storeModel->getDi()->getShared('dbMaster')->query($sql);
        $selectId->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $results = $selectId->fetchAll();
        if (isset($results[0]['company_code'])) {
            return $results[0]['company_code'];   
        }
    }

    private function getCartData($cartID) {
        $cartAPI = new \Library\Cart;
        $cartData = $cartAPI->getCartByCartId($cartID);  
        
        if (!$cartData) {
            \Helpers\LogHelper::log("get_cart_data", "[ERROR] Failed to get cart data for cart id " . $cartData['cart_id'] . ". Response error: " . json_encode($cartAPI->getErrorMessages()));
            $this->errorCode = $cartAPI->getErrorCode();
            $this->errorMessages = json_encode($cartAPI->getErrorMessages());
            return false;
        }

        return $cartData;
    }

    private function sendEmailToStoreForNewOrder($param = array()) {
        $apiWrapper = new APIWrapper(getenv('ORDER_API'));
        $apiWrapper->setEndPoint("email/send");
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed tosend email to store for new order, please try again";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("SendEmailToStoreForNewOrderFailed", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
        return true;
    }

    private function processB2BInvoice($salesOrderData = array())
    {
        $cartAPI = new \Library\Cart;
        $cartData = $cartAPI->getCartByCartId($salesOrderData['cart_id']);
        if (!$cartData) {
            \Helpers\LogHelper::log("process_b2b_invoice", "[ERROR] Failed to get cart data for cart id " . $salesOrderData['cart_id'] . ". Response error: " . json_encode($cartAPI->getErrorMessages()));
            $this->errorCode = $cartAPI->getErrorCode();
            $this->errorMessages = json_encode($cartAPI->getErrorMessages());
            return false;
        }

        if (!isset($cartData['b2b']) || !isset($cartData['b2b']['pending_order_id']) || $cartData['b2b']['pending_order_id'] == 0) {
            $this->errorCode = 404;
            $this->errorMessages = "Pending order id not found";
            return false;
        }

        if (isset($salesOrderData['order_type']) && ($salesOrderData['order_type'] == "b2b_informa" || $salesOrderData['order_type'] == "b2b_ace")) {
            $paramProcessB2b['pending_order_id'] = (int)$cartData['b2b']['pending_order_id'];
            $paramProcessB2b['customer_id'] = (int)$salesOrderData['customer']['customer_id'];
            $this->processPendingOrderB2b($paramProcessB2b);

        } else if (isset($salesOrderData['order_type']) && $salesOrderData['order_type'] == "additional_cost") {
            $paramUpdatePODetailB2b['pending_order_id'] = (int)$cartData['b2b']['pending_order_id'];
            $paramUpdatePODetailB2b['pending_order_status'] = 'processing';
            $paramUpdatePODetailDataB2b['reference_key'] = 'sales_order_id';
            $paramUpdatePODetailDataB2b['reference_key_value'] = (int)$salesOrderData['sales_order_id'];
            $paramUpdatePODetailDataB2b['status'] = 'completed';
            $paramUpdatePODetailB2b['pending_order_details'][] = $paramUpdatePODetailDataB2b;
            $this->createOrUpdatePODetailB2b($paramUpdatePODetailB2b);
        }

        return true;
    }

    private function checkInvoiceContainInstallation($invoiceList = array(), $salesOrderData = array())
    {
        $orderNo = $salesOrderData['order_no'];
        $salesOrderItemInstallationMdl = new \Models\SalesOrderItemInstallation;
        $salesOrderItemInstallation = $salesOrderItemInstallationMdl->findFirst('sales_order_id = ' . $salesOrderData['sales_order_id']);
        if ($salesOrderItemInstallation) { // Handle when create installation with order
            $isNeedInstallation = $salesOrderItemInstallation->getIsNeedInstallation();
            if ($isNeedInstallation == 0) { // Doesn't need installation, probably need installation from DC
                return;
            }
            
            $this->createInstallation($orderNo, $salesOrderData['customer']['customer_id'], $salesOrderData['customer']['customer_email']);
            if (!empty($this->errorCode)) {
                return;
            }
        } else if (substr($orderNo, 0, 4) == "ODIN") { // Handle when create installation order
            $cartAPI = new \Library\Cart;
            $cartData = $cartAPI->getCartByCartId($salesOrderData['cart_id']);
            if (!$cartData) {
                \Helpers\LogHelper::log("installation_invoice", "[ERROR] Failed to get cart data for cart id " . $salesOrderData['cart_id'] . ". Response error: " . json_encode($cartAPI->getErrorMessages()));
                $this->errorCode = $cartAPI->getErrorCode();
                $this->errorMessages = json_encode($cartAPI->getErrorMessages());
                return;
            }

            if (!isset($cartData['sales_installation_id']) || $cartData['sales_installation_id'] == 0) {
                \Helpers\LogHelper::log("installation_invoice", "[ERROR] Sales installation id for cart_id " . $salesOrderData['cart_id'] . " not found");
                $this->errorCode = 404;
                $this->errorMessages = "Sales installation ID not found on cart";
                return;
            }

            $salesInstallationMdl = new \Models\SalesInstallation;
            $salesInstallation = $salesInstallationMdl->findFirst("sales_installation_id = " . $cartData['sales_installation_id']);
            if (!$salesInstallation) {
                \Helpers\LogHelper::log("installation_invoice", "[ERROR] Sales installation data for sales_installation_id " . $cartData['sales_installation_id'] . " not found");
                $this->errorCode = 404;
                $this->errorMessages = "Sales installation data not found";
                return;
            }

            $salesInstallation->setInstallationInvoiceID($invoiceList[0]->invoice_id); // Installation order made on level invoice, so we just need first invoice data of current installation order (1 installation order, 1 invoice)
            $salesInstallation->setStatus("pending_installation");
            $result = $salesInstallation->update();
            if ($result === false) {
                \Helpers\LogHelper::log("installation_invoice", "[ERROR] Failed to update sales installation data for sales_installation_id " . $cartData['sales_installation_id'] . ". Installation order no: " . $orderNo . " || Installation invoice id: " . $invoiceList[0]->invoice_id);
                $this->errorCode = 500;
                $this->errorMessages = "Failed to update sales installation data";
                return;
            }

            // If installation order b2b
            if (isset($cartData['reference_installation_pending_order_no']) && $cartData['reference_installation_pending_order_no'] != "") {
                $paramUpdatePODetailB2b['pending_order_no'] = $cartData['reference_installation_pending_order_no'];
                $paramUpdatePODetailB2b['pending_order_status'] = 'processing';
                $paramUpdatePODetailDataB2b['reference_key'] = 'sales_installation_id';
                $paramUpdatePODetailDataB2b['reference_key_value'] = (int)$cartData['sales_installation_id'];
                $paramUpdatePODetailDataB2b['status'] = $paramUpdatePODetailB2b['pending_order_status'];
                $paramUpdatePODetailB2b['pending_order_details'][] = $paramUpdatePODetailDataB2b;
                $this->createOrUpdatePODetailB2b($paramUpdatePODetailB2b);
            }
            
            $salesInstallationLib = new \Library\SalesInstallation;
            $installationHistoryParam = array(
                'sales_installation_id' => $cartData['sales_installation_id'],
                'installation_order_no' => $salesInstallation->getInstallationOrderNo(),
                'installation_invoice_id' => $salesInstallation->getInstallationInvoiceID(),
                'value' => "pending_installation",
            );

            $salesInstallationLib->insertInstallationHistory($installationHistoryParam);
        }
    }

    private function createOrUpdatePODetailB2b($param = array()) {
        $apiWrapper = new APIWrapper(getenv('ORDER_B2B_API'));
        $apiWrapper->setEndPoint("pending_order_detail");
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to update pending order detail B2b, please try again";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("PendingOrderDetailB2b", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
        return true;
    }

    private function processPendingOrderB2b($param = array()) {
        $apiWrapper = new APIWrapper(getenv('ORDER_B2B_API'));
        $apiWrapper->setEndPoint("pending_order/process");
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to process B2b pending order, please try again";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("PendingOrderB2b", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
        return true;
    }

    private function createInstallation($orderNo, $customerID, $customerEmail)
    {
        $installationData = array();

        $apiWrapper = new APIWrapper(getenv('INSTALLATION_API'));
        $apiWrapper->setEndPoint("installation/with_order");
        $param = array();
        $param['order_no'] = $orderNo;
        $param['customer_id'] = (int)$customerID;
        $param['customer_email'] = $customerEmail;
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to create installation data";
            return $installationData;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("installation", "[ERROR] Failed to create installation for order number " . $orderNo . ". Response error: " . json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return $installationData;
        }

        $installationData = $apiWrapper->getData();
        if (empty($installationData)) {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to get installation data response";
            return $installationData;
        }

        return $installationData;
    }

    public function updateOwnfleetShipment($ownfleetInformaItemIds)
    {
        $apiWrapper = new APIWrapper(getenv('SHIPMENT_API'));

        $params = [
            "sales_order_data_id" => $ownfleetInformaItemIds
        ];

        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("ownfleet/update");

        if (!$apiWrapper->send("post")) {
            \Helpers\LogHelper::log('ownfleet_update_marketing', '[ERROR] when calling shipment API ' . json_encode($ownfleetInformaItemIds));
        }
        return;
    }

    public function insertFakturPajakInvoice($params = array())
    {

        $this->fakturPajakInvoice = new \Models\FakturPajakInvoice();
        $SalesFakturPajakModel = new \Models\SalesFakturPajak();
        $res = $SalesFakturPajakModel->find('sales_order_id = "' . $params['sales_order_id'] . '"');

        $customerCompanyData = array();
        if (isset($res->toArray()[0])) {
            $customerCompanyData = $res->toArray()[0];
            $CustomerCompanyModel = new \Models\CustomerCompany();
            $res = $CustomerCompanyModel->find('npwp_image = "' . $customerCompanyData['npwp_image'] . '"');
            $temp = $res->toArray()[0];
            $customerCompanyId = $temp['customer_company_id'];
            $dataFaktur = $params;
        }

        if (!empty($customerCompanyData)) {
            date_default_timezone_set("Asia/Bangkok");
            $dataFaktur['customer_company_id'] = $customerCompanyId;
            $dataFaktur['company_name'] = $customerCompanyData['company_name'];
            $dataFaktur['company_address'] = $customerCompanyData['company_address'];
            $dataFaktur['recipient_email'] = $customerCompanyData['recipient_email'];
            $dataFaktur['npwp_image'] = $customerCompanyData['npwp_image'];
            $dataFaktur['company_npwp'] = $customerCompanyData['company_npwp'];
            $dataFaktur['status'] = 'new';
            $dataFaktur['created_at'] =  date("Y-m-d h:i:sa");
            $dataFaktur['nitku'] = $customerCompanyData['nitku'];
            $dataFaktur['tipe_wajib_pajak'] = $customerCompanyData['tipe_wajib_pajak'];
            if (is_null($customerCompanyData['is_npwp_address']) || $customerCompanyData['is_npwp_address'] == '') {
                $dataFaktur['is_npwp_address'] = "no";
            } else {
                $dataFaktur['is_npwp_address'] = $customerCompanyData['is_npwp_address'];
            }
            $this->fakturPajakInvoice->setFromArray($dataFaktur);
            $this->fakturPajakInvoice->saveData("faktur_pajak_invoice");
        }
        return;
    }

    public function releaseCashbackAbuser($params = array())
    {
        if (empty($params['order_no'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Order no is required";
            return;
        }

        $order_no = $params['order_no'];

        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderData = $salesOrderLib->getSalesOrderDetail(['order_no' => $order_no]);

        $this->checkMarketingInvoice($salesOrderData['cart_id'], $order_no, false, false);

        if (empty($this->errorCode)) {
            $data['sales_order_id'] = $params['sales_order_id'];
            $data['invoice_id'] = 0;
            $data['comment'] = "Release Cashback : Approved";
            $data['admin_user_id'] = $params['admin_user_id'];;
            $data['flag'] = 'info';
            $commentModel = new \Models\SalesComment();
            $commentModel->setFromArray($data);
            $commentModel->saveData();
        }

        return;
    }

    public function unreleaseCashbackAbuser($params = array())
    {
        if (empty($params['order_no'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Order no is required";
            return;
        }

        // update sales_cashback_hold
        $this->salesCashbackHoldRemove($params);
        // insert sales_comment
        $data['sales_order_id'] = $params['sales_order_id'];
        $data['invoice_id'] = 0;
        $data['comment'] = "Unrelease Cashback : " . $params['comment'];
        $data['admin_user_id'] = $params['admin_user_id'];;
        $data['flag'] = 'info';
        $commentModel = new \Models\SalesComment();
        $commentModel->setFromArray($data);
        $commentModel->saveData();

        // send email 
        $order_no = $params['order_no'];
        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderData = $salesOrderLib->getSalesOrderDetail(['order_no' => $order_no]);

        $this->salesOrder = new \Models\SalesOrder();
        $this->salesOrder->assign($salesOrderData);

        $emailHelper = new \Library\Email();
        $emailHelper->setEmailSender();
        $emailHelper->setOrderObj($this->salesOrder);
        $emailHelper->setEmailTag("unrelease_customer_cashback");
        $templateID = \Helpers\GeneralHelper::getTemplateId("unrelease_customer_cashback", 'ODI');
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode("ODI");
        $emailHelper->setName();
        $emailHelper->setEmailReceiver();
        $emailHelper->setOrderNo($order_no);
        $emailHelper->send();
        return;
    }

    public function groupShipment($salesOrderData = array())
    {
        $orderType = $salesOrderData['order_type'];
        $groupShipmentData = $shipmentItemData = array();
        foreach ($salesOrderData['items'] as $rowOrderItem) {
            if (!isset($groupShipmentData[$rowOrderItem['group_shipment']])) {
                if ($rowOrderItem['delivery_method'] != 'pickup') {
                    $salesOrderAddress = new \Models\SalesOrderAddress();
                    $result = $salesOrderAddress::findFirst(["sales_order_id = " . $salesOrderData['sales_order_id'] . " AND group_shipment = " . $rowOrderItem['group_shipment'] . ""]);
                    if (!$result) {
                        $this->errorCode = "400";
                        $this->errorMessages[] = "Cannot get address for group shipment: " . $rowOrderItem['group_shipment'];
                        return;
                    }

                    $resultArray = $result->getDataArray();
                }

                if ($rowOrderItem['delivery_method'] == 'pickup') {
                    $groupShipmentData[$rowOrderItem['group_shipment']] = [
                        'shipment_status' => 'new',
                        'shipment_type' => "pickup",
                        'group_shipment' => $rowOrderItem['group_shipment'],
                        'total_weight' => 1, // set default 1, we calculate again later
                        'company_code' => $salesOrderData['company_code']
                    ];
                } elseif ($rowOrderItem['delivery_method'] == "delivery" || $rowOrderItem['delivery_method'] == 'store_fulfillment' || $rowOrderItem['delivery_method'] == "ownfleet") {
                    $groupShipmentData[$rowOrderItem['group_shipment']] = [
                        'shipping_address_id' => $resultArray['sales_order_address_id'],
                        'shipment_status' => $orderType == "additional_cost" ? "received" : "new",
                        'carrier_id' => $resultArray['carrier_id'],
                        'shipment_type' => $rowOrderItem['delivery_method'] == "ownfleet" ? "ownfleet" : "3pl",
                        'group_shipment' => $rowOrderItem['group_shipment'],
                        'total_weight' => 1,
                        'company_code' => $salesOrderData['company_code']
                    ];

                    // Set lead time on sales shipment
                    if (!empty($rowOrderItem['lead_time_min'])) {
                        $groupShipmentData[$rowOrderItem['group_shipment']]["lead_time_min"] = $rowOrderItem['lead_time_min'];
                        $groupShipmentData[$rowOrderItem['group_shipment']]["initial_lead_time_min"] = $rowOrderItem['lead_time_min'];
                    }

                    if (!empty($rowOrderItem['lead_time_max'])) {
                        $groupShipmentData[$rowOrderItem['group_shipment']]["lead_time_max"] = $rowOrderItem['lead_time_max'];
                        $groupShipmentData[$rowOrderItem['group_shipment']]["initial_lead_time_max"] = $rowOrderItem['lead_time_max'];
                    }

                    if (!empty($rowOrderItem['lead_time_unit'])) {
                        $groupShipmentData[$rowOrderItem['group_shipment']]["lead_time_unit"] = $rowOrderItem['lead_time_unit'];
                        $groupShipmentData[$rowOrderItem['group_shipment']]["initial_lead_time_unit"] = $rowOrderItem['lead_time_unit'];
                    }

                    // set insurance info
                    if (!empty($rowOrderItem['insurance_fee'])) {
                        $groupShipmentData[$rowOrderItem['group_shipment']]["insurance_fee"] = $rowOrderItem['insurance_fee'];
                    }

                    if (!empty($rowOrderItem['must_use_insurance'])) {
                        $groupShipmentData[$rowOrderItem['group_shipment']]["must_use_insurance"] = $rowOrderItem['must_use_insurance'];
                    }

                    if (!empty($rowOrderItem['insurance_applied'])) {
                        $groupShipmentData[$rowOrderItem['group_shipment']]["insurance_applied"] = $rowOrderItem['insurance_applied'];
                    }
                }
            }

            // used for calculate total weight
            $shipmentItemData[$rowOrderItem['group_shipment']][] = $rowOrderItem;
        }

        // calculate total weight and set data for shipment
        foreach ($shipmentItemData as $keyShipment => $valShipment) {
            // Use the countTotalPackagingWeight function to calculate total_weight
            // If packaging_weight is empty and returns zero weight after calculation
            // To calculate total_weight, we'll use the countWeight function
            $totalWeight = \Helpers\CartHelper::countTotalPackagingWeight($valShipment);
            if (empty($totalWeight['shipping'])) {
                $totalWeight = \Helpers\CartHelper::countWeight($valShipment);
            }

            // If delivery method on current group shipment is not pickup, we also have to check whether in current group shipment,
            // chosen carrier is shipper's carrier / not, if it's then we just use final_weight as sales_shipment.total_weight
            $usedOrderItem = count($valShipment) > 0 ? $valShipment[0] : [];
            if (!empty($usedOrderItem) && $usedOrderItem["delivery_method"] != "pickup") {
                // Get carrier id from sales_order_address
                $salesOrderAddressModel = new \Models\SalesOrderAddress();
                $getSalesOrderAddressParams["conditions"] = sprintf("sales_order_id = %d AND group_shipment = %d", (int) $salesOrderData['sales_order_id'], (int) $keyShipment);
                $salesOrderAddressResult = $salesOrderAddressModel->findFirst($getSalesOrderAddressParams);
                if (empty($salesOrderAddressResult)) {
                    $this->errorCode = 400;
                    $this->errorMessages[] = sprintf("Failed get sales order address data, sales_order_id: %d, group shipment: %d", (int) $salesOrderData['sales_order_id'], (int) $keyShipment);
                    return;
                }

                // Check carrier's vendor name
                $carrierVendorModel = new \Models\ShippingCarrierVendor();
                $getCarrierVendorParams["conditions"] = sprintf("carrier_id = %d", $salesOrderAddressResult->getCarrierId());
                $carrierVendorResult = $carrierVendorModel->findFirst($getCarrierVendorParams);
                if (!empty($carrierVendorResult) && $carrierVendorResult->getVendorName() == "shipper") {
                    $totalWeight["shipping"] = $usedOrderItem["final_weight"];
                }
            }

            $groupShipmentData[$keyShipment]['total_weight'] = $totalWeight['shipping'];
        }

        return $groupShipmentData;
    }

    private function _deductStock($order_items = [], $process, $order_no = '', $isStoreCodeOrder1000DC = false)
    {
        if (empty($order_items)) {
            return;
        }

        if ($process == "") {
            return;
        }

        $productStock = new ProductStock();
        $orderItemIds = array_column($order_items, 'sales_order_item_id');

        $orderItemIdsInt = array();
        foreach ($orderItemIds as $orderItemId) {
            $orderItemIdsInt[] = (int)$orderItemId;
        }

        if (!((substr($order_no, 0, 4) == "ODIS" || substr($order_no, 0, 4) == "ODIT" || substr($order_no, 0, 4) == "ODIK") && !$isStoreCodeOrder1000DC)) {
            $productStock->deductStockOrderV2($process, $orderItemIdsInt);
        }
    }

    /**
     * Making invoice from order items
     * invoice will be group by store code and delivery method
     * @param array $items
     * @return array
     */
    public function splitInvoice($items = array(), $totalRow = array())
    {
        if (empty($items)) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Items is required";
            return array();
        }

        // Get the highest group shipment
        // $groupShipmentContainer = 0;
        // $groupShipmentContainerDC = -1;
        // foreach($items as $item)
        // {
        //     if ($groupShipmentContainer < $item['group_shipment']){
        //         $groupShipmentContainer = $item['group_shipment'];
        //     }
        //     if ($item['store_code'] == "DC"){
        //         $groupShipmentContainerDC = $item['group_shipment'];
        //     }
        // }

        // // // Add 1 to groupShipmentContainer for 1000DC
        // $groupShipmentContainer = $groupShipmentContainer + 1;

        // first we group all item data for invoice
        $invoice = array();
        foreach ($items as $item) {
            // check if the order TP is still in process or failed
            // if(count($totalRow) == 0){
            //     //Change 1000DC Store Code into DC so DC store and 1000DC store invoice can be combined into one invoice
            //     if($item['store_code']=="1000DC"){
            //         $item["store_code"] = "DC";
            //         // Change group shipment into DC group shipment if DC item exists in order
            //         if ($groupShipmentContainerDC != -1){
            //             $item['group_shipment'] = $groupShipmentContainerDC;
            //         }
            //     }
            // }else{
            //     $isChanged = 0;
            //     foreach($totalRow as $row){
            //         if($item['store_code']=="1000DC" && $item["sku"]==$row["sku"]){
            //             // $item["store_code"] = "exclude";
            //             $isChanged = 1;
            //             break;
            //         }
            //     }
            //     if($isChanged == 0){
            //         if($item['store_code']=="1000DC"){
            //             $item["store_code"] = "DC";
            //             // Change group shipment into DC group shipment if DC item exists in order
            //             if ($groupShipmentContainerDC != -1){
            //                 $item['group_shipment'] = $groupShipmentContainerDC;
            //             }
            //         }
            //     }else if($isChanged == 1){
            //         // Change group shipment for 1000DC store code
            //         $item['group_shipment'] = $groupShipmentContainer;
            //     }

            // }

            $supplierAlias = $item['supplier_alias'];
            $storeCode = $item['store_code'];
            $deliveryMethod = $item['delivery_method'];
            $courierPickup = ($item['delivery_method'] == 'pickup') ? $item['store_code'] : $item['shipping_address']['carrier_id'];
            $geolocation = (isset($item['shipping_address']['geolocation'])) ? $item['shipping_address']['geolocation'] : 'geolocation_empty';
            $groupShipment = $item['group_shipment'];
            $statusPreorder = $item['status_preorder'];
            $invoice[$groupShipment][$statusPreorder][] = $item;
        }

        return $invoice;
    }

    public function prepareInvoiceUpdate($params = array())
    {
        $deliveryMethod = '';
        $salesInvoiceModel = new \Models\SalesInvoice();
        if (array_key_exists('invoice_id', $params)) {
            if (array_key_exists('receipt_id', $params)) {
                $salesInvoiceArray = array();
                $salesInvoiceResult = $salesInvoiceModel->findFirst("invoice_id = '" . $params['invoice_id'] . "'");
                if ($salesInvoiceResult) {
                    $salesInvoiceArray = $salesInvoiceModel->findFirst("invoice_id = '" . $params['invoice_id'] . "'")->toArray();
                }

                if (count($salesInvoiceArray) > 0) {
                    foreach ($salesInvoiceArray as $key => $value) {
                        if ($key == 'invoice_id') {
                            $deliveryMethod = $salesInvoiceArray['delivery_method'];
                            break;
                        }
                    }

                    if ($deliveryMethod <> '') {
                        $allowedDeliveryMethod = array('pickup', 'store_fulfillment');
                        if (!in_array($deliveryMethod, $allowedDeliveryMethod)) {
                            $this->errorCode = "RR100";
                            $this->errorMessages = "Wrong delivery method";
                            return null;
                        }
                    } else {
                        $this->errorCode = "RR100";
                        $this->errorMessages = "Delivery method not found";
                        return null;
                    }
                } else {
                    $this->errorCode = "RR100";
                    $this->errorMessages = "Invoice not found";
                    return null;
                }

                $params['receipt_id'] = ($params['receipt_id'] == '') ? null : $params['receipt_id'];
            }

            // another validation if required
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Invoice id can not empty";
            return null;
        }
        $salesInvoiceModel->setFromArrayUpdate($params);
        $this->salesInvoice = $salesInvoiceModel;
    }

    public function prepareInvoiceUpdatePicupVoucher($params = array())
    {
        $deliveryMethod = '';
        $salesInvoiceModel = new \Models\SalesInvoice();
        if (array_key_exists('invoice_no', $params)) {

            $pin_pos = isset($params['pin_pos']) ? $params['pin_pos'] : '';
            $invoiceModel = new \Models\SalesInvoice();

            $updateData = "";
            if (!empty($params['pickup_voucher'])) {
                if (empty($updateData)) {
                    $updateData .= "";
                } else {
                    $updateData .= " ,";
                }

                $updateData .= "pickup_voucher='" . $params['pickup_voucher'] . "'";
            }

            if (!empty(trim($pin_pos))) {
                if (empty($updateData)) {
                    $updateData .= "";
                } else {
                    $updateData .= " ,";
                }

                $updateData .= "pin_pos='" . $pin_pos . "'";
            }

            if (!empty($updateData)) {
                $sql = "UPDATE sales_invoice SET " . $updateData . "  WHERE invoice_no='" . $params['invoice_no'] . "'";
                $result = $invoiceModel->getDi()->getShared('dbMaster')->execute($sql);
                if ($result) {
                    $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                    $dataInvoice['invoice_no'] = $params['invoice_no'];
                    $kawanLamaSystemLib->setFromArray($dataInvoice);
                    $kawanLamaSystemLib->createJournal('pickup');

                    // Komisi 0% Propose
                    $kawanLamaSystemLib->createJournal('incoming');
                    $kawanLamaSystemLib->createJournal('mdr');

                    $this->errorCode = "";
                    $this->errorMessages = "";
                    return $result;
                }
            }

            return false;
            // another validation if required
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Invoice no can not empty";
            return null;
        }
    }

    public function processRegeneratePickupVoucher($params = array())
    {
        $this->errorCode = "";
        $this->errorMessages = "";

        if (array_key_exists('invoice_no', $params)) {
            $invoiceNo = $params['invoice_no'];
            $pinPos = isset($params['pin_pos']) ? $params['pin_pos'] : '';
            $pickupVoucher = isset($params['pickup_voucher']) ? $params['pickup_voucher'] : '';
            $amount = isset($params['amount']) ? $params['amount'] : '';

            if (!empty($invoiceNo) && !empty($pickupVoucher) && !empty($pinPos) && is_numeric($amount)) {
                $invoiceModel = new \Models\SalesInvoice();
                $sql = "UPDATE sales_invoice SET pickup_voucher = '{$pickupVoucher}', pin_pos = '{$pinPos}'  WHERE invoice_no = '{$invoiceNo}'";
                $result = $invoiceModel->getDi()->getShared('dbMaster')->execute($sql);
                if ($result) {
                    // create journal 3602 regenerate pickup voucher
                    $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                    $dataInvoice['invoice_no'] = $invoiceNo;
                    $dataInvoice['amount'] = $amount;
                    $kawanLamaSystemLib->setFromArray($dataInvoice);
                    $kawanLamaSystemLib->createJournalRegeneratePickup();

                    return true;
                }

                $this->errorCode = "RR100";
                $this->errorMessages = "Data cant empty";
                return false;
            }
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Invoice no needed!";
            return false;
        }
    }

    public function prepareUpdateSoSapNumber($params = array())
    {
        if (array_key_exists('invoice_no', $params)) {
            $invoiceModel = new \Models\SalesInvoice();
            $salesPaymentModel = new \Models\SalesOrderPayment();
            $invoiceOdModel = new \Models\SalesInvoiceOd();

            $invoiceModel->useWriteConnection();

            $manager = new TxManager();
            $manager->setDbService($invoiceModel->getConnection());
            $transaction = $manager->get();

            $invoiceModel->setTransaction($transaction);
            $invoiceOdModel->setTransaction($transaction);

            try {
                $salesOrderId = "";
                $orderType = "";

                $sql = "SELECT
                            si.invoice_id,
                            si.invoice_no,
                            si.sales_order_id,
                            si.store_code,
                            si.created_at,
                            so.order_type,
                            sbbi.so_sap_no
                        FROM
                            sales_invoice si
                        JOIN 
                            sales_order so ON si.sales_order_id = so.sales_order_id
                        LEFT JOIN
                            sales_b2b_information sbbi ON sbbi.sales_order_id = so.sales_order_id
                        WHERE
                            si.invoice_no = '" . $params['invoice_no'] . "' LIMIT 1";

                $result = $invoiceModel->getDi()->getShared('dbSlave')->query($sql);
                $result = $result->fetchAll();
                if (count($result) > 0) {
                    $salesOrderId = $result[0]['sales_order_id'];
                    $orderType = $result[0]['order_type'];
                } else {
                    $this->errorCode = "RR100";
                    $this->errorMessages = "Invoice not found";
                    return null;
                }

                $resultInvoices = array();
                $paymentInfo = array();

                // TOP purpose : https://ruparupa.atlassian.net/browse/B2B-115
                $sql = "SELECT
                            method
                        FROM
                            sales_order_payment
                        WHERE
                            sales_order_id = " . $salesOrderId;

                $resultPayment = $salesPaymentModel->getDi()->getShared('dbSlave')->query($sql);
                $paymentInfo = $resultPayment->fetch();

                // For b2b_informa/b2b_ace order type, because there's invoice merging that causes 1 so_sap_number can be used on more than one invoice
                if ($orderType === "b2b_informa" || $orderType === "b2b_ace") {
                    $sql = "SELECT
                                invoice_id,
                                invoice_no
                            FROM
                                sales_invoice
                            WHERE
                                sales_order_id = " . $salesOrderId;

                    $resultInvoices = $invoiceModel->getDi()->getShared('dbSlave')->query($sql);
                    $resultInvoices = $resultInvoices->fetchAll();

                    if ($paymentInfo["method"] == "term_of_payment") {
                        $params['sap_so_number'] = $result[0]['so_sap_no'];
                    }
                } else {
                    $resultInvoices = $result;
                }

                foreach ($resultInvoices as $res) {
                    $sql = "SELECT
                                    sii.sku,
                                    sii.sales_order_item_id
                                FROM
                                    sales_invoice_item sii 
                                WHERE
                                    invoice_id = '" . $res['invoice_id'] . "'";

                    $result = $invoiceModel->getDi()->getShared('dbSlave')->query($sql);
                    $result->setFetchMode(
                        \Phalcon\Db::FETCH_ASSOC
                    );
                    $invoices = $result->fetchAll();

                    $sku = count($invoices) > 0 ? $invoices[0]['sku'] : '';
                    $sql = "SELECT so_sap_odi FROM sales_invoice_od WHERE so_sap_odi = '" . $params['sap_so_number'] . "' AND sku = '{$sku}' LIMIT 1";
                    $result = $invoiceOdModel->getDi()->getShared('dbSlave')->query($sql);
                    $result->setFetchMode(
                        \Phalcon\Db::FETCH_ASSOC
                    );
                    $isExistDatas = $result->fetchAll();

                    if (count($isExistDatas) === 0) {
                        $sql = "INSERT INTO sales_invoice_od (invoice_no, invoice_id, sales_order_item_id, so_sap_odi, sku, created_at, updated_at) VALUES ";
                        foreach ($invoices as $invoice) {
                            $sql .= "(";
                            $sql .= "'" . $res['invoice_no'] . "', ";
                            $sql .= "'" . $res['invoice_id'] . "', ";
                            $sql .= "'" . $invoice['sales_order_item_id'] . "', ";
                            $sql .= "'" . $params['sap_so_number'] . "', ";
                            $sql .= "'" . $invoice['sku'] . "', ";
                            $sql .= "'" . date('Y-m-d H:i:s') . "', ";
                            $sql .= "'" . date('Y-m-d H:i:s') . "'";
                            $sql .= "),";
                        }
                        $sql = substr($sql, 0, -1);
                        $result = $invoiceOdModel->getDi()->getShared('dbMaster')->query($sql);
                    }

                    $this->InsertSLADC($res['invoice_no'], 1, "so_sap");

                    $sql = "UPDATE sales_invoice SET sap_so_number = '" . $params['sap_so_number'] . "' WHERE invoice_no='" . $res['invoice_no'] . "'";
                    $result = $invoiceModel->getDi()->getShared('dbMaster')->query($sql);

                    if ($result && ($orderType == "b2b_ace" || $orderType == "b2b_informa")) {
                        // Margin 0% Propose
                        if ($paymentInfo["method"] != "term_of_payment" && $res['store_code'] != 'DC') {
                            $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                            $dataInvoice['invoice_no'] = $res['invoice_no'];
                            $kawanLamaSystemLib->setFromArray($dataInvoice);
                            $kawanLamaSystemLib->createJournal('sales');
                            $kawanLamaSystemLib->createJournal('incoming');
                            $kawanLamaSystemLib->createJournal('mdr');
                        }
                    }
                }

                if ($result && ($orderType != "b2b_ace" && $orderType != "b2b_informa")) {
                    // Margin 0% Propose
                    if ($paymentInfo["method"] != "term_of_payment" && $res['store_code'] != 'DC') {
                        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                        $dataInvoice['invoice_no'] = $params['invoice_no'];
                        $kawanLamaSystemLib->setFromArray($dataInvoice);
                        $kawanLamaSystemLib->createJournal('sales');
                        $kawanLamaSystemLib->createJournal('incoming');
                        $kawanLamaSystemLib->createJournal('mdr');
                    }
                }
                $transaction->commit();

                $this->errorCode = "";
                $this->errorMessages = "";
                return $result;
            } catch (\Exception $e) {
                $transaction->rollback($e->getMessage());
                $this->errorCode = "RR400";
                $this->errorMessages = "Failed update so sap number";
                return null;
            }
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Invoice no can not empty";
            return null;
        }
    }

    public function InsertSLADC($invoice_no, $admin_id = 1, $sla_type = "so_sap")
    {
        $apiWrapper = new APIWrapper(getenv('ORDER_API_V2'));
        $apiWrapper->setParam(null);
        $apiWrapper->setEndPoint("dc_dashboard/sla?invoice_no={$invoice_no}&admin_id={$admin_id}&sla_type={$sla_type}");
        if ($apiWrapper->sendRequest("put")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to insert DC SLA";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("sla_age_dc", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
        return true;
    }

    public function updateData()
    {
        try {
            //get old sales invoice status to compare
            $salesInvoiceModel = new \Models\SalesInvoice();
            $salesInvoiceResult = $salesInvoiceModel->findFirst('invoice_id = ' . $this->salesInvoice->getInvoiceId());

            $this->salesInvoice->saveData('sales_invoice');

            $productStock = new ProductStock();

            //cek if invoice already canceled do not revert
            if ($salesInvoiceResult) {
                $invoiceStatus = $salesInvoiceResult->toArray()['invoice_status'];
                if ($invoiceStatus != 'canceled' && $this->salesInvoice->getInvoiceStatus() == 'canceled') {
                    //revert back stock product if status not full refund
                    if ($salesInvoiceResult->SalesInvoiceItem) {
                        $salesOrderItemIds = array();
                        $revertWithCustomQuantity = array();

                        foreach ($salesInvoiceResult->SalesInvoiceItem as $invoiceItem) {
                            if ($invoiceStatus != 'full_refund') {
                                $item = $invoiceItem->toArray(['sku', 'store_code', 'qty_ordered', 'sales_order_item_id']);
                                $qty_ordered = $item['qty_ordered'];
                                //if status partial refund, there is credit memo. so we need to subtract qty ordered
                                if ($invoiceStatus == 'partial_refund') {
                                    $salesCreditMemoItemModel = new \Models\SalesCreditMemoItem();
                                    $salesCreditMemoItemResult = $salesCreditMemoItemModel->find('sales_order_item_id = ' . $item['sales_order_item_id'] . ' and sku="' . $item['sku'] . '" and qty_refunded > 0');
                                    if ($salesCreditMemoItemResult) {
                                        foreach ($salesCreditMemoItemResult as $creditMemoItem) {
                                            $qty_ordered -= $creditMemoItem->toArray()['qty_refunded'];
                                        }
                                    }
                                    
                                    $revertWithCustomQuantity[] = array(
                                        'sales_order_item_id' => (int)$invoiceItem->getSalesOrderItemId(),
                                        'quantity' => (int)$qty_ordered
                                    );
                                }

                                $salesOrderItemIds[] = (int)$invoiceItem->getSalesOrderItemId();
                            }
                        }
                        if (!empty($salesOrderItemIds) || !empty($revertWithCustomQuantity)) {
                            $productStock->revertStockOrderV2('shipment', $salesOrderItemIds, $revertWithCustomQuantity);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Update sales invoice failed, please contact ruparupa tech support";
        }
    }

    public function sendOrderNotification($companyCode = "ODI")
    {
        /**
         * @var $emailHelper Email
         */
        $emailHelper = new \Library\Email();
        $emailHelper->setOrderObj($this->salesOrder);
        $emailHelper->generateAddress();
        $emailHelper->setName();
        $emailHelper->setEmailSender();
        $emailHelper->buildNotificationItems();

        $totalInvCleanCareNotif = $emailHelper->getInvCleanCareNotif();
        if (!empty($totalInvCleanCareNotif)) {
            $templateID = \Helpers\GeneralHelper::getTemplateId("clean_care", $companyCode);
            $receiverEmail = $_ENV['PIC_CLEAN_CARE_EMAIL'];

            $emailHelper->setEmailTag("team_order_clean_and_care_design_interior");
            $emailHelper->setTemplateId($templateID);
            $emailHelper->setEmailReceiver($receiverEmail, '', false);
            $emailHelper->setEmailCc("");
            $emailHelper->setCompanyCode($companyCode);
            $emailHelper->sendCleanCareNotificationEmail();
        }

        $totalInvDesignerNotif = $emailHelper->getInvDesignerNotif();
        if (!empty($totalInvDesignerNotif)) {
            $templateID = \Helpers\GeneralHelper::getTemplateId("designer", $companyCode);
            $receiverEmail = getenv('PIC_DESIGNER_EMAIL');

            $emailHelper->setTemplateId($templateID);
            $emailHelper->setCompanyCode($companyCode);
            $emailHelper->setEmailReceiver($receiverEmail, '', false);
            $emailHelper->setEmailCc("");
            $emailHelper->sendDesignerNotif();
        }

        $totalInvMp = $emailHelper->getInvMpNotif();
        if (!empty($totalInvMp)) {
            $templateID = \Helpers\GeneralHelper::getTemplateId("mp_notification", $companyCode);

            $emailHelper->setTemplateId($templateID);
            $emailHelper->setCompanyCode($companyCode);
            $emailHelper->setEmailTag("mp_new_order_with_no_invoice");
            $emailHelper->setEmailCc("");
            $emailHelper->sendMpNotificationEmail();
        }

        // As requested on ASP-2169
        // $invStoreReminder = $emailHelper->getInvStoreReminder();
        // if (!empty($invStoreReminder)) {
        //     $templateID = \Helpers\GeneralHelper::getTemplateId("store_reminder_order", $companyCode);

        //     $emailHelper->setTemplateId($templateID);
        //     $emailHelper->setCompanyCode($companyCode);
        //     $emailHelper->setEmailTag("store_reminder_order");
        //     $emailHelper->setEmailCc("");
        //     $emailHelper->sendStoreReminderOrderEmail();
        // }
    }

    public function sendSuccessPaymentNotif($preorderList, $invoice)
    {
        $companyCode = $this->salesOrder->company_code;
        $isOrderAutoReceived = InvoiceHelper::isOrderAutoReceived($this->salesOrder, $invoice);
        $order_no = $this->salesOrder->order_no;

        $prefix_order = substr($order_no, 0, 4);

        if ($prefix_order != 'ODIS' && $prefix_order != 'ODIT' && $prefix_order != 'ODIK') {
            /**
             * @var $emailHelper Email
             */
            $emailHelper = new \Library\Email();
            $emailHelper->setOrderObj($this->salesOrder);
            $emailHelper->setEmailSender();
            $emailHelper->setEmailReceiver();
            $emailHelper->setName();
            \Helpers\LogHelper::log('debug_625', '/app/Library/Invoice.php line 3549', 'debug');
            $emailHelper->buildSuccessPaymentParam($preorderList);
            $emailHelper->setEmailTag("cust_pembayaran_diterima");
            if (!empty($companyCode)) {
                if ($this->salesOrder->getDevice() == 'ace-ios' || $this->salesOrder->getDevice() == 'ace-android') {
                    $companyCode = 'AHI';
                } else if ($this->salesOrder->getDevice() == 'informa-ios' || $this->salesOrder->getDevice() == 'informa-android' || $this->salesOrder->getDevice() == 'informa-huawei') {
                    $companyCode = 'HCI';
                } else if ($this->salesOrder->getDevice() == 'selma-ios' || $this->salesOrder->getDevice() == 'selma-android' || $this->salesOrder->getDevice() == 'selma-huawei') {
                    $companyCode = 'SLM';
                }
                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_paid_gosend_preorder_new", $companyCode);
                if ($isOrderAutoReceived) {
                    $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

                    $qrCodeDestinationUrl = getenv("STORE_DASHBOARD_URL") . "search?" . "keyword=" . $order_no . "&bypass_processing=yes";

                    //Template QR is only for ODI
                    $templateID = \Helpers\GeneralHelper::getTemplateId("payment_paid_new_retail_qr_code_new", "ODI");
                    // Create token payload as a JSON string
                    $payload = json_encode(['data' => $qrCodeDestinationUrl, 'iss' => "api.core"]);        // Encode Header to Base64Url String
                    $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
                    
                    // Encode Payload to Base64Url String
                    $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
                    
                    // Create Signature Hash
                    $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, getenv('QR_SECRET_KEY'), true);
                    
                    // Encode Signature to Base64Url String
                    $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));
                    
                    // Create JWT
                    $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
                    
                    $qrCodeUrl = getenv("WAPI_URL")."qrcode/" . $jwt . "/250";
                    $emailHelper->setQRCodeURL($qrCodeUrl);
                }
                $emailHelper->setTemplateId($templateID);
                $emailHelper->setCompanyCode($companyCode);
            }
            // push send notifications
            $firstNameCustomer = $emailHelper->getCustomerFirstName();
            $emailReceiver = $emailHelper->getEmailReceiver();

            // push send notifications
            $notificationHelper = new \Library\Notification();
            $iconSuccessPaymentNotif = getenv('PUSH_NOTIF_ICON_ODI');

            // Override icon according to company code if exist
            $iconByCompanyCode = getenv(sprintf("PUSH_NOTIF_ICON_%s", strtoupper($companyCode)));
            if (!empty($iconByCompanyCode)) {
                $iconSuccessPaymentNotif = $iconByCompanyCode;
            }

            $notificationHelper->setTitle("Hai " . $firstNameCustomer . ", Pembayaran Sudah diterima");
            $notificationHelper->setBody("Pesanan kamu akan segera kami proses ya");
            $notificationHelper->setKey("token");
            $notificationHelper->setIcon($iconSuccessPaymentNotif);
            $notificationHelper->setPageType("order-detail");
            $notificationHelper->setUrl(getenv('RUPARUPA_URL') . "order-detail?" . urlencode("orderId=" . $order_no . "&email=" . $emailReceiver));

            $invoiceNo = $invoice->getInvoiceNo();
            $isOrderAutoReceivedPushNotif = "is_order_auto_received_false";
            if ($isOrderAutoReceived){
                $isOrderAutoReceivedPushNotif = "is_order_auto_received_true";
            }
            $notificationHelper->setUrlKey(sprintf('%s,%s,%s,%s', $order_no, $emailReceiver, $invoiceNo, $isOrderAutoReceivedPushNotif));
            $notificationHelper->setCustomerID($this->salesOrder->customer_id);

            if ($prefix_order == 'ODIN') {
                $notificationHelper->setPageType("");
                $notificationHelper->setUrl("");
                $notificationHelper->setUrlKey("");
            }

            if ($isOrderAutoReceived) {
                $companyCode = "ODI";
            }
            $customerDeviceToken = new \Models\CustomerDeviceToken();
            $customerDeviceToken = $customerDeviceToken->getCDTByCustIdForPushNotif($this->salesOrder->customer_id);

            $notificationHelper->PushNotifInbox();

            $pushNotifList = explode(',', getenv('PUSH_NOTIF_BU_CODE'));
            if (count($customerDeviceToken) > 0 && in_array($companyCode, $pushNotifList)) {
                $filteredToken = $notificationHelper->FilterTokenByHighestPriority($customerDeviceToken);

                foreach ($filteredToken as $filterToken) {
                    $notificationHelper->setValue($filterToken["device_token"]);

                    if ($prefix_order == 'ODIN' && ($filterToken["os"] == "android" || $filterToken["os"] == "ios")) {
                        $salesInstallationMdl = new \Models\SalesInstallation;
                        $salesInstallation = $salesInstallationMdl->findFirst("installation_order_no = '" . $order_no . "'");

                        if (!$salesInstallation) {
                            \Helpers\LogHelper::log("installation", "[ERROR] Sales installation data for installation_order_no " . $order_no . " not found");
                            continue;
                        }
                        $salesInstallationID = $salesInstallation->getSalesInstallationID();

                        $notificationHelper->setPageType("my-installation-detail");
                        $notificationHelper->setUrl("https://ruparupa.page.link/?link=" . getenv('RUPARUPA_URL') . "my-account?tab=my-installation-detail&id=" . $salesInstallationID . "&email=" . $emailReceiver);
                        $notificationHelper->setUrlKey("$salesInstallationID,$emailReceiver");
                    }

                    $notificationHelper->PushNotif($companyCode);
                }
            }

            $cartType = $this->salesOrder->getCartType();
            if ($cartType == 'informa_b2b') {
                $emailHelper->setEmailCc(getenv('PIC_INFORMA_B2B_ORDER_EMAIL'));
            } else if ($cartType == 'ace_b2b') {
                $emailHelper->setEmailCc(getenv('PIC_ACE_B2B_ORDER_EMAIL'));
            }

            $emailHelper->send();
        }
    }

    public function checkMarketingInvoice($cart_id, $order_no, $isVendor, $checkAbuser = true)
    {
        $apiWrapper = new APIWrapper(getenv('CART_API'));

        $apiWrapper->setEndPoint("marketing/invoice/" . $cart_id);

        if ($isVendor) {
            // Disable create voucher marketplace acquisition due to https://ruparupa.atlassian.net/browse/RW-31
            return true;
            // $apiWrapper->setEndPoint("marketing/invoice/vendor/" . $cart_id);
        }

        if ($checkAbuser) {
            $apiWrapper->setHeaders(
                [
                    "Abuser-Check" => "1"
                ]
            );
        }

        if ($apiWrapper->send("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "RR401";
            $this->errorMessages = "Check marketing failed";
            return false;
        }

        // if return 202 set sales_cashback_hold
        $errorResponse = $apiWrapper->getError();
        if ($errorResponse['code'] == 202) {
            $params["order_no"] = $order_no;
            $this->salesCashbackHold($params);
        } else if ($errorResponse['code'] == 0) {
            $params["order_no"] = $order_no;
            $this->salesCashbackHoldRemove($params);
        }

        return true;
    }

    public function getMDRDetail($invoiceNo = "")
    {
        $apiWrapper = new APIWrapper(getenv('FINANCE_API'));
        $apiWrapper->setEndPoint("mdr/detail/" . $invoiceNo);

        if ($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "RR401";
            $this->errorMessages = "Get MDR Failed";
            return false;
        }

        $errorResponse = $apiWrapper->getError();
        $data = $apiWrapper->getData();
        return $data;
    }

    public function transPOSVoucher($params)
    {
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("marketing/voucher/pos/transaction");
        $resultData = array();

        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
            $resultData = $apiWrapper->getData();
        } else {
            $this->errorCode = "RR400";
            $this->errorMessages[] = "Transaction POS voucher failed";
            \Helpers\LogHelper::log('pos_voucher', 'Transaction POS voucher failed : ' . json_encode($params));

            return false;
        }

        return true;
    }

    /**
     * @deprecated
     * Use checkMarketingInvoice() instead
     */
    public function checkPromotion($order_no = '', $sales_order_data = [])
    {
        if (empty($order_no) && empty($sales_order_data)) {
            $this->errorCode = "RR101";
            $this->errorMessages[] = "Order no is required";
            return false;
        }

        /**
         * For reorder item don't apply any promotion
         */
        if (!empty($sales_order_data['reference_order_no'])) {
            return false;
        }

        $marketingLib = new \Library\Marketing();
        $promotionType = ['invoice_created'];
        $invoicePromo = $marketingLib->getMarketingPromotion($promotionType);

        if (empty($invoicePromo)) {
            return false;
        } else {
            // Get invoice
            if (empty($sales_order_data)) {
                $orderLib = new \Library\SalesOrder();
                $orderData = $orderLib->getSalesOrderDetail(['order_no' => $order_no]);
            } else {
                $orderData = $sales_order_data;
            }

            if (empty($orderData)) {
                $this->errorCode = "RR302";
                $this->errorMessages[] = "Order Not found";
                return false;
            }

            $cart_id = $orderData['cart_id'];
            $cartModel = new \Models\Cart();
            $cartModel->setCartId($cart_id);
            $cartData = $cartModel->loadCartData(true);

            if (empty($cartData)) {
                $cartData = $orderData;
            }

            $invoicePromotion = json_decode($invoicePromo['invoice_created'], true);
            $marketingLib = new MarketingLib();
            if (!empty($invoicePromotion)) {
                foreach ($invoicePromotion as $promotion) {
                    $isActiveTime = \Helpers\GeneralHelper::checkActiveBetweenDate($promotion['from_date'], $promotion['to_date']);

                    if (!$isActiveTime) {
                        continue;
                    }

                    $canContinue = $marketingLib->prerequisiteConditionCheck($cartData, $promotion);

                    if (!$canContinue) {
                        continue;
                    }

                    $is_allow_group = false;
                    if (isset($promotion['customer_group_id']) && $promotion['customer_group_id'] != "") {
                        $allowedGroup = explode(",", $promotion['customer_group_id']);
                        foreach ($cartData['customer']['customer_group'] as $customer_group) {
                            if (in_array($customer_group, $allowedGroup)) {
                                $is_allow_group = true;
                            }
                        }
                    }

                    // If this group not allow to get this promotion, skip it
                    if (!$is_allow_group) {
                        continue;
                    }

                    if (empty($promotion['conditions'])) {
                        $matchCondition = true;
                    } else {
                        \Helpers\LogHelper::log('debug_625', '/app/library/Invoice.php line 3773', 'debug');
                        $matchCondition = $cartModel->marketingConditionCheck($promotion['conditions'], $cartData);
                    }

                    // Apply condition
                    if ($matchCondition) {
                        $action = $promotion['action'];

                        // For now only support cash_back promotion in invoice
                        $voucherCodeApplied = "";
                        if ($action['applied_action'] == 'cash_back') {
                            /**
                             * If using voucher, check if voucher already redeem or not
                             */
                            $useVoucher = false;
                            if ($promotion['use_voucher'] == 1) {
                                $giftCardInfo = json_decode($orderData['gift_cards'], true);
                                foreach ($giftCardInfo as $gcId => $gcVal) {
                                    if ($gcVal['rule_id'] == $promotion['rule_id'] && empty($gcVal['applied'])) {
                                        $voucherCodeApplied = $gcVal['voucher_code'];
                                        $giftCardInfo[$gcId]['applied'] = true;
                                        $useVoucher = true;
                                    }
                                }

                                if ($useVoucher == false) {
                                    return false;
                                }

                                $orderData['gift_cards'] = json_encode($giftCardInfo);

                                $orderModel = new \Models\SalesOrder();
                                $orderModel->setFromArray($orderData);
                                $orderModel->saveData("invoice");
                            }

                            // Get voucher that have generated before
                            $rule_id = $action['associate_rule_id'];

                            /**
                             * Condition how we get the voucher, generate on the fly or already exist
                             * 1 => generate on the fly
                             * 0 => Use the one that already exist
                             */
                            $getCashback = false;
                            $voucher_expired = date('Y-m-d H:i:s', strtotime(' +1 month '));
                            $totalQtyitem = 0;
                            $voucherList = [];
                            if (isset($action['voucher_generated']) && $action['voucher_generated'] == 1) {
                                $actionExpired = $action['voucher_expired'];
                                $dateOrder = new \DateTime($orderData['created_at']);
                                $dateOrder->modify('+' . $actionExpired . ' day');
                                $voucher_expired = $dateOrder->format('Y-m-d H:i:s');

                                $discount_amount = !empty($action['discount_amount']) ? $action['discount_amount'] : 0;
                                $amountAttribute = !empty($action['attributes']) ? $action['attributes'] : "subtotal";
                                $discountType = !empty($action['discount_type']) ? $action['discount_type'] : "percent";

                                if ($amountAttribute == 'shipping_amount') {
                                    $originAmount = intval($orderData['shipping_amount'] - $orderData['shipping_discount_amount']);
                                } else {
                                    $conditionAtribute = explode(".", $amountAttribute);
                                    if (count($conditionAtribute) > 1) {
                                        // If condition more than one it's mean item attribute
                                        $itemAttribute = $conditionAtribute[1];

                                        $promotion['conditions'] = \Helpers\CartHelper::keepAttributeConditions($promotion['conditions']);
                                        $originAmountAttribute = 0;
                                        if (!empty($promotion['conditions'])) {
                                            $promotion['conditions'] = \Helpers\CartHelper::removeAtributeCondition($promotion['conditions']);

                                            $conditions = $promotion['conditions'];
                                            // handle grand_total_exclude_shipping
                                            foreach ($conditions as $key => $value) {
                                                if ($value['attribute'] == 'grand_total_exclude_shipping') {
                                                    unset($conditions[$key]);
                                                }
                                            }

                                            $originAmountAttribute = 0;
                                            foreach ($cartData['items'] as $item) {
                                                \Helpers\LogHelper::log('debug_625', '/app/library/Invoice.php line 3854', 'debug');
                                                $conditonCheckingStatus = $marketingLib->marketingConditionCheck($conditions, $item);
                                                $matchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;

                                                //$matchCondition = $cartModel->marketingConditionCheck($promotion['conditions'], $item);
                                                if ($matchCondition) {
                                                    $originAmountAttribute += $item[$itemAttribute];

                                                    if (isset($promotion['action']['max_quantity'])) {
                                                        $promotion['action']['max_quantity'] = empty($promotion['action']['max_quantity']) ? 0 : $promotion['action']['max_quantity'];

                                                        if ($promotion['action']['max_quantity'] != 0) {
                                                            if ($item['qty_ordered'] > $promotion['action']['max_quantity']) {
                                                                $totalQtyitem += $promotion['action']['max_quantity'];
                                                            } else {
                                                                $totalQtyitem += $item['qty_ordered'];
                                                            }
                                                        } else if ($promotion['action']['max_quantity'] == 0) {
                                                            $totalQtyitem += $item['qty_ordered'];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $originAmountAttribute = isset($orderData[$amountAttribute]) ? intval($orderData[$amountAttribute]) : 0;
                                    }

                                    if ($amountAttribute == "grand_total") {
                                        $shippingAmount = intval($orderData['shipping_amount']) - intval($orderData['shipping_discount_amount']);
                                        $originAmount = intval($originAmountAttribute) - $shippingAmount;
                                    } else {
                                        $originAmount = $originAmountAttribute;
                                    }

                                    // Finaly we need to deduct amount with gift card usage
                                    $originAmount = $originAmount - $orderData['gift_cards_amount'];
                                }

                                if ($discountType == 'fix') {
                                    $voucherAmount = \Helpers\CartHelper::countDiscountAmountUsed($originAmount, $discount_amount);
                                } else if ($discountType == 'percent') {
                                    $discountAmountGet = round((intval($discount_amount) / 100) * $originAmount);
                                    $voucherAmount = \Helpers\CartHelper::countDiscountAmountUsed($originAmount, $discountAmountGet);

                                    if (isset($action['max_redemption'])) {
                                        if (!empty((int) $action['max_redemption']) && $voucherAmount > $action['max_redemption']) {
                                            $voucherAmount = intval($action['max_redemption']);
                                        }
                                    }
                                } else if ($discountType == 'fix_multiple') {
                                    $voucherAmount = intval($discount_amount) * (($totalQtyitem == 0) ? 1 : $totalQtyitem);
                                }

                                if (!empty($voucherAmount) && $voucherAmount > 0) {

                                    if (!empty((int) $action['split_amount'])) {
                                        $totalVoucherGet = floor($voucherAmount / intval($action['split_amount']));
                                        $parameters = [
                                            "rule_id" => $rule_id,
                                            "type" => 4,
                                            "created_by" => 0,
                                            "expiration_date" => $voucher_expired,
                                            "qty" => $totalVoucherGet,
                                            "voucher_amount" => $action['split_amount'],
                                            "length" => 10,
                                            "prefix" => $orderData['order_no'],
                                            "format" => "order"
                                        ];

                                        // Now set that voucher amount is split amount, not the total amount that should get
                                        $voucherAmount = (int) $action['split_amount'];
                                    } else {
                                        $parameters = [
                                            "rule_id" => $rule_id,
                                            "type" => 4,
                                            "created_by" => 0,
                                            "expiration_date" => $voucher_expired,
                                            "qty" => 1,
                                            "voucher_amount" => $voucherAmount,
                                            "length" => 0,
                                            "prefix" => $orderData['order_no']
                                        ];
                                    }

                                    $marketingLib = new \Library\Marketing();
                                    $marketingLib->generateVoucher($parameters);

                                    // set voucher code cashback
                                    $voucherList = $marketingLib->getVoucherList();

                                    if (!empty($voucherList)) {
                                        $customerId = 0;
                                        if (!empty($this->salesOrder) && !empty($this->salesOrder->SalesCustomer->getCustomerId())) {
                                            $customerId = $this->salesOrder->SalesCustomer->getCustomerId();
                                        } else if (!empty($cartData['customer']['customer_id'])) {
                                            $customerId = $cartData['customer']['customer_id'];
                                        }

                                        foreach ($voucherList as $voucherCode) {
                                            $parameters['voucher_code'] = $voucherCode;
                                            $parameters['limit_used'] = 1;
                                            $parameters['max_redemption'] = $voucherAmount;
                                            $parameters['status'] = 10;
                                            $parameters['customer_id'] = $customerId;
                                            $salesRuleVoucherModel = new \Models\SalesruleVoucher();
                                            $salesRuleVoucherModel->setFromArray($parameters);
                                            $salesRuleVoucherModel->saveData("voucher");
                                        }
                                        $getCashback = true;
                                    }
                                }
                            } else {
                                $voucherModel = new \Models\SalesruleVoucher();
                                $parameters = [
                                    'conditions' => 'rule_id = ' . $rule_id . ' AND times_used = 0 AND status = 10'
                                ];
                                $voucher_result = $voucherModel::findFirst($parameters);

                                if ($voucher_result) {
                                    $customerId = 0;
                                    if (!empty($this->salesOrder) && !empty($this->salesOrder->SalesCustomer->getCustomerId())) {
                                        $customerId = $this->salesOrder->SalesCustomer->getCustomerId();
                                    } else if (!empty($cartData['customer']['customer_id'])) {
                                        $customerId = $cartData['customer']['customer_id'];
                                    }
                                    $voucherModel->assign($voucher_result->toArray());
                                    $voucherModel->expiration_date = $voucher_expired;
                                    $voucherModel->status = 15;
                                    $voucherModel->customer_id = $customerId;
                                    $voucherModel->saveData("voucher");

                                    $voucherAmount = $voucher_result->getVoucherAmount();
                                    $voucherCode = $voucher_result->getVoucherCode();
                                    $getCashback = true;
                                }
                            }

                            if ($getCashback) {
                                // Sending email
                                $email = new \Library\Email();
                                $email->setEmailSender();
                                $email->setName($this->salesOrder->SalesCustomer->getCustomerLastname(), $this->salesOrder->SalesCustomer->getCustomerFirstname());
                                $email->setEmailReceiver($this->salesOrder->SalesCustomer->getCustomerEmail());
                                $email->setEmailTag("cust_voucher_cashback");

                                if (!empty($action['email_template_id'])) {
                                    $email->setTemplateId($action['email_template_id']);
                                } else {
                                    $templateID = \Helpers\GeneralHelper::getTemplateId("cash_back", $this->salesOrder->getCompanyCode());
                                    $email->setCompanyCode($this->salesOrder->getCompanyCode());
                                    $email->setTemplateId($templateID);
                                }

                                if (!empty($voucherList) && count($voucherList) > 1) {
                                    $newList = [];
                                    foreach ($voucherList as $myVoucher) {
                                        $newList[] = ['voucher_code' => $myVoucher];
                                    }
                                    $email->setVoucherList($newList);
                                } else {
                                    $email->setVoucherCode($voucherCode);
                                }

                                $email->setVoucherValue(number_format($voucherAmount, 0, ',', '.'));
                                $email->setVoucherExpired(date('j F Y H:i:s', strtotime($voucher_expired)));
                                $email->send();

                                /**
                                 * Save ussage case back rule to salesrule_customer
                                 */
                                $voucherUsageModel = new \Models\SalesruleCustomer();

                                // If not found we need to save it
                                if (empty($isValidUsage)) {
                                    $customerVoucherUsage = [
                                        "rule_id" => $promotion['rule_id'],
                                        "voucher_code" => $voucherCodeApplied,
                                        "customer_id" => $this->salesOrder->SalesCustomer->getCustomerId(),
                                        "times_used" => 1
                                    ];
                                } else {
                                    $customerVoucherUsage = $isValidUsage;
                                    $customerVoucherUsage['times_used']++;
                                }

                                $voucherUsageModel->setFromArray($customerVoucherUsage);
                                $voucherUsageModel->saveData("invoice");

                                // save rule to order
                                $orderModel = new \Models\SalesOrder();
                                $orderModel->setFromArray($orderData);

                                $promotion['cashback_amount'] = $voucherAmount;

                                $cartRulesArray = [];
                                if (!empty($orderData['cart_rules'])) {
                                    $cartRulesArray = json_decode($orderData['cart_rules']);
                                }

                                $currentRule[] = $promotion;
                                $updatedCartRules = array_merge($cartRulesArray, $currentRule);
                                $orderModel->setCartRules(json_encode($updatedCartRules));
                                $orderModel->saveData();

                                // disini untuk mensave salesrule_order invoice created
                                $param['salesrule_order_id'] = '';
                                $param['sales_order_id'] = $this->salesOrder->sales_order_id;
                                $param['rule_id'] = $promotion['rule_id'];

                                $salesruleOrderModel = new \Models\SalesruleOrder();
                                $salesruleOrderModel->setFromArray($param);
                                $salesruleOrderModel->save('salesrule_order');

                                if ($promotion['stop_rules_processing'] == true) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    private function _getItemValueFromCondition($item = array(), $conditions = array(), $key = 1)
    {
        if (is_scalar($item[$conditions[$key]])) {
            $itemValue = $item[$conditions[$key]];
        } else if (is_array($item[$conditions[$key]]) && isset($item[$conditions[$key]][0])) {
            return null;
        } else {
            $newItem = $item[$conditions[$key]];
            $key++;
            $itemValue = $this->_getItemValueFromCondition($newItem, $conditions, $key);
        }

        return $itemValue;
    }

    public function notificationError($params_data = array())
    {
        $notificationText = $params_data["message"];
        $params = [
            "channel" => $params_data["slack_channel"],
            "username" => $params_data["slack_username"],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    // public function pushnotifMobile($customerId, $customerFirstname)
    // {

    //     // $client = new GuzzleClient();
    //     // $curlParam = [
    //     //     'timeout'=>'10.0',
    //     //     'headers' => ['Content-Type' => 'application/json'],
    //     //     'body' => json_encode(
    //     //         ['data' => [
    //     //             'customer_id' => $customerId,
    //     //             'title' => 'Pembayaran Diterima',
    //     //             'body' => 'Hi '.$customerFirstname.', terima kasih telah membayar pesanan kamu. Mohon ditunggu ya.'
    //     //         ]]                
    //     //     )
    //     // ];

    //     // $responseApi = $client->request('POST',$_ENV['PUSH_NOTIF_API']."/push-device",$curlParam );
    //     // LogHelper::log("notification-mobile", "Response : '". $responseApi->getBody());

    //     $nsq = new Nsq();
    //     $message = [
    //         "data" => [
    //             'customer_id' => $customerId,
    //             'title' => 'Pembayaran Diterima',
    //             'body' => 'Hi ' . $customerFirstname . ', terima kasih telah membayar pesanan kamu. Mohon ditunggu ya.'
    //         ],
    //         "message" => "pushNotif"
    //     ];
    //     $nsq->publishCluster('transaction', $message);
    // }

    public function updateReceiptId($params = array())
    {
        if (!empty($params["invoice_no"])) {
            $invoiceModel = new \Models\SalesInvoice();
            $updateData = "";
            if (!empty($params['receipt_id'])) {
                $updateData .= "receipt_id='" . $params['receipt_id'] . "'";
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Receipt id can not empty";
                return null;
            }

            if (!empty($updateData)) {
                $sql = "UPDATE sales_invoice SET " . $updateData . "  WHERE invoice_no='" . $params['invoice_no'] . "'";
                $result = $invoiceModel->getDi()->getShared('dbMaster')->execute($sql);
                if ($result) {
                    $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                    $dataInvoice['invoice_no'] = $params['invoice_no'];
                    $kawanLamaSystemLib->setFromArray($dataInvoice);
                    $kawanLamaSystemLib->createJournal('pickup');

                    $this->errorCode = "";
                    $this->errorMessages = "";
                    return $result;
                }
            }

            return false;
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Invoice no can not empty";
            return null;
        }
    }

    public function getVoucherList($params = array())
    {
        $invoice = array();
        $isOldPickupVoucher = true;

        $invoicePickupVoucherModel = new \Models\SalesInvoicePickupVoucher();
        $invoicePickupVoucherModel->useReadOnlyConnection();

        $sql = "SELECT * FROM sales_invoice_pickup_voucher WHERE pickup_voucher ='" . $params['voucher_id'] . "' AND status = 10 LIMIT 1";
        $result = $invoicePickupVoucherModel->getDi()->getShared('dbReader')->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $salesInvoicePickupVoucher = $result->fetchAll();

        if($salesInvoicePickupVoucher){
            $isOldPickupVoucher = false;
        }

        if (!empty($salesInvoicePickupVoucher[0]['invoice_no'])) {
            $params['invoice_no'] = $salesInvoicePickupVoucher[0]['invoice_no'];
        } else {
            $invoiceModel = new \Models\SalesInvoice();
            $invoiceModel->useReadOnlyConnection();
            $sql = "SELECT * FROM sales_invoice WHERE pickup_voucher = '" . $params['voucher_id'] . "'";
            $result = $invoiceModel->getDi()->getShared('dbReader')->query($sql);
    
            $result->setFetchMode(
                Database::FETCH_ASSOC
            );
    
            $invoice = $result->fetchAll();
            if (!empty($invoice[0]['invoice_no'])) {
                $params['invoice_no'] = $invoice[0]['invoice_no'];
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Invoice not found for voucher " . $params['voucher_id'];
                return null;
            }
        }

        if (!$invoice) {
            $invoiceModel = new \Models\SalesInvoice();
            $invoiceModel->useReadOnlyConnection();
            $sql = "SELECT * FROM sales_invoice WHERE invoice_no ='" . $params['invoice_no'] . "'";
            $result = $invoiceModel->getDi()->getShared('dbReader')->query($sql);
            $result->setFetchMode(
                Database::FETCH_ASSOC
            );
    
            $invoice = $result->fetchAll();
            if (!$invoice) {
                $this->errorCode = "RR100";
                $this->errorMessages = "Invoice not found for voucher " . $params['voucher_id'];
                return null;
            }
        }

        $payload = [
            'voucher_id' => $params['voucher_id'],
            'invoice_no' => $params['invoice_no'],
            'created_date' => date('m/d/Y',strtotime($invoice[0]['created_at'])),
            'is_old_pickup_voucher' => $isOldPickupVoucher
        ];

        $invoiceCreatedAtStr = $invoice[0]['created_at'];
        $invoiceCreatedAt = strtotime($invoiceCreatedAtStr);

        $invoiceModel = new \Models\SalesInvoice();
        $invoiceModel->useReadOnlyConnection();
        $sql = "SELECT su.supplier_alias
            FROM sales_invoice si
            INNER JOIN store s ON s.store_code = si.store_code
            INNER JOIN supplier su ON su.supplier_id = s.supplier_id
            WHERE si.invoice_id = ". $invoice[0]['invoice_id'];
        $buResult = $invoiceModel->getDi()->getShared('dbReader')->query($sql);
        $buResult->setFetchMode(
            Database::FETCH_ASSOC
        );
      
        $buResult = $buResult->fetchAll();
        if (!$buResult) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Supplier not found for invoice id " . $invoice[0]['invoice_id'];
            return;
        }
        
        $newMopCheck = $this->isNewMop($buResult[0]['supplier_alias'], $invoice[0]['store_code']);
        if ($newMopCheck['error_message'] != "") {
            $this->errorCode = "RR002";
            $this->errorMessages[] = $newMopCheck['error_message'];
            return;
        }

        $response = array();
        if (!$newMopCheck['is_new_mop']) {
            $payload['is_old_pickup_voucher'] = true;
        } 

        $response = $this->getVoucherPosNewMop($payload);

        if (empty($response)) {
            $this->errorCode = "RR002";
            $this->errorMessages[] = "Mohon maaf sedang terjadi kesalahan sistem. Silakan dicoba kembali atau dapat menghubungi Ruparupa Tech Support";
            return;
        }

        return $response;
    }

    public function getVoucherPosOldMop ($params = array()){
        $body = [
            "CreateDate" => "",
            "companyGroup" => "AHI",
            "Source" => "",
            "Type" => "",
            "VoucherNo" => $params['voucher_id']
        ];
        $data = array();

        $apiWrapper = new APIWrapper(getenv('VOUCHER_POS_END_POINT'));
        $apiWrapper->setEndPoint("GET_STATUS_VOUCHER_PR");
        $apiWrapper->setParam($body);

        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }

        $data = $apiWrapper->getData();
        return $data;
    }

    public function getVoucherPosNewMop($params = array()){
        // If pickup voucher is created before new mop deployment/activation, then don't send invoice_no in payload
        // because it will cause "data not found" error from it corp
        if ($params['is_old_pickup_voucher']) {
            $params['invoice_no'] = "";
        } else {
            $params['voucher_id'] = "";
        }

        $client = new \nusoap_client($_ENV['GET_VOUCHER_POS_END_POINT'], 'wsdl');
        $err = $client->getError();
        if ($err) {
            $this->errorCode = "RR002";
            $this->errorMessages[] = $err;
            return;
        }

        $response = $client->Call("GET_STATUS_VOUCHER_PR", [
            'createdate' => $params['created_date'],
            'companyGroup' => "AHI",
            'source' => "4",
            'type' => "6",
            'VoucherNo' => $params['voucher_id'],
            'invoiceNo' => $params['invoice_no']
        ]);

        $data = json_decode($response['GET_STATUS_VOUCHER_PRResult'], true);
        return $data;
    }

    public function getReceiptId($params = array())
    {
        $salesInvoiceModel = new \Models\SalesInvoice();
        $sql = "SELECT au.admin_user_id, au.email, si.receipt_id, sl.sales_order_id, sl.invoice_id, sl.shipment_id FROM sales_log sl JOIN admin_user au ON sl.admin_user_id = au.admin_user_id JOIN sales_invoice si ON sl.invoice_id = si.invoice_id
        WHERE sl.invoice_id = ( SELECT invoice_id FROM sales_invoice WHERE pickup_voucher = '" . $params['voucher_id'] . "') AND affected_field = 'receipt_id' AND value = si.receipt_id ORDER BY sl.log_time DESC LIMIT 1";
        $selectReceipt = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql);
        $selectReceipt->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $selectReceiptResult = $selectReceipt->fetchAll();

        if (empty($selectReceiptResult)) {
            $sql = "SELECT au.admin_user_id, au.email, si.receipt_id, sl.log_time, sl.sales_order_id, sl.invoice_id, sl.shipment_id FROM sales_log sl JOIN admin_user au ON sl.admin_user_id = au.admin_user_id JOIN sales_invoice si ON sl.invoice_id = si.invoice_id
            WHERE sl.invoice_id = ( SELECT invoice_id FROM sales_invoice_pickup_voucher WHERE pickup_voucher = '" . $params['voucher_id'] . "' AND status = 10 LIMIT 1) AND affected_field = 'receipt_id' AND value = si.receipt_id ORDER BY sl.log_time DESC LIMIT 1";
            $selectReceipt = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql);
            $selectReceipt->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $selectReceiptResult = $selectReceipt->fetchAll();
        }

        return $selectReceiptResult;
    }

    public function getReceiptLog($params = array())
    {
        $salesInvoiceModel = new \Models\SalesInvoice();

        $sql = "SELECT au.admin_user_id, au.email, si.receipt_id, sl.log_time, sl.sales_order_id, sl.invoice_id, sl.shipment_id FROM sales_log sl JOIN admin_user au ON sl.admin_user_id = au.admin_user_id JOIN sales_invoice si ON sl.invoice_id = si.invoice_id
        WHERE sl.invoice_id = ( SELECT invoice_id FROM sales_invoice WHERE pickup_voucher = '" . $params['voucher_id'] . "') AND affected_field = 'expired_voucher' ORDER BY sl.log_time DESC LIMIT 1";
        $selectEmail = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql);
        $selectEmail->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $selectEmailResult = $selectEmail->fetchAll();

        if (empty($selectEmailResult)) {
            $sql = "SELECT au.admin_user_id, au.email, si.receipt_id, sl.log_time, sl.sales_order_id, sl.invoice_id, sl.shipment_id FROM sales_log sl JOIN admin_user au ON sl.admin_user_id = au.admin_user_id JOIN sales_invoice si ON sl.invoice_id = si.invoice_id
            WHERE sl.invoice_id = ( SELECT invoice_id FROM sales_invoice_pickup_voucher WHERE pickup_voucher = '" . $params['voucher_id'] . "' AND status = 10 LIMIT 1) AND affected_field = 'expired_voucher' ORDER BY sl.log_time DESC LIMIT 1";
            $selectEmail = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql);
            $selectEmail->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $selectEmailResult = $selectEmail->fetchAll();
        }

        return $selectEmailResult;
    }

    public function updatePosVoucher($params = array())
    {
        $invoiceId = 0; 
        $isOldPickupVoucher = true;

        if (empty($params['invoice_id'])) {
            $salesInvoiceModel = new \Models\SalesInvoice();
            $sql0 = "SELECT sl.sales_order_id, sl.invoice_id, sl.shipment_id FROM sales_log sl JOIN sales_invoice si ON sl.invoice_id = si.invoice_id
            WHERE sl.invoice_id = ( SELECT invoice_id FROM sales_invoice WHERE pickup_voucher = '" . $params['Voucher_Id'] . "') ORDER BY sl.log_time DESC LIMIT 1";
            $selectId = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql0);
            $selectId->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $getAllId = $selectId->fetchAll();

            if (empty($getAllId)) {
                $sql0 = "SELECT sl.sales_order_id, sl.invoice_id, sl.shipment_id FROM sales_log sl JOIN sales_invoice si ON sl.invoice_id = si.invoice_id
                WHERE sl.invoice_id = ( SELECT invoice_id FROM sales_invoice_pickup_voucher WHERE pickup_voucher = '" . $params['Voucher_Id'] . "' AND status = 10 LIMIT 1) ORDER BY sl.log_time DESC LIMIT 1";
                $selectId = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql0);
                $selectId->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $getAllId = $selectId->fetchAll();

                if (empty($getAllId)) {
                    $sql0 = "SELECT si.invoice_id FROM sales_invoice si WHERE si.pickup_voucher = '" . $params['Voucher_Id'] . "' LIMIT 1";
                    $selectId = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql0);
                    $selectId->setFetchMode(
                        \Phalcon\Db::FETCH_ASSOC
                    );
                    $getAllId = $selectId->fetchAll();

                    if (empty($getAllId)) {
                        $sql0 = "SELECT sipv.invoice_id FROM sales_invoice_pickup_voucher sipv WHERE sipv.pickup_voucher = '" . $params['Voucher_Id'] . "' LIMIT 1";
                        $selectId = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql0);
                        $selectId->setFetchMode(
                            \Phalcon\Db::FETCH_ASSOC
                        );
                        $getAllId = $selectId->fetchAll();
                    }
                }
            }

            $invoiceId = $getAllId[0]['invoice_id'];

            $sql1 = "SELECT admin_user_id FROM admin_user WHERE email = '" . $params['email'] . "' ORDER BY admin_user_id DESC LIMIT 1";
            $result = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql1);
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $result_user = $result->fetchAll();
            $val = date("Y-m-d", strtotime("+7 day"));
            $sql2 = "INSERT INTO sales_log (sales_order_id, invoice_id, shipment_id, admin_user_id, actions, affected_field, value, log_time, flag) 
            VALUES ('" . $getAllId[0]['sales_order_id'] . "','" . $getAllId[0]['invoice_id'] . "','" . $getAllId[0]['shipment_id'] . "','" . $result_user[0]['admin_user_id'] . "','update voucher expired date #" . $params['Voucher_Id'] . "',
            'expired_voucher','" . $val . "',now(),'voucher_dashboard'); ";
            $salesInvoiceModel->useWriteConnection();
            $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql2);
        } else {
            $invoiceId = $params['invoice_id'];
            $salesInvoiceModel = new \Models\SalesInvoice();
            $sql1 = "SELECT admin_user_id FROM admin_user WHERE email = '" . $params['email'] . "' ORDER BY admin_user_id DESC LIMIT 1";
            $result = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql1);
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $result_user = $result->fetchAll();

            $val = date("Y-m-d", strtotime("+7 day"));
            $sql2 = "INSERT INTO sales_log (sales_order_id, invoice_id, shipment_id, admin_user_id, actions, affected_field, value, log_time, flag) 
            VALUES ('" . $params['sales_order_id'] . "','" . $params['invoice_id'] . "','" . $params['shipment_id'] . "','" . $result_user[0]['admin_user_id'] . "','update voucher expired date #" . $params['Voucher_Id'] . "',
            'expired_voucher','" . $val . "',now(),'voucher_dashboard'); ";
            $salesInvoiceModel->useWriteConnection();
            $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql2);
        }

        if ($invoiceId != 0) {
            $salesInvoiceModel = new \Models\SalesInvoice();
            $sql = "SELECT si.invoice_no, si.store_code FROM sales_invoice si WHERE si.invoice_id = " . $invoiceId . " LIMIT 1";
            $db = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql);
            $db->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $invoice = $db->fetchAll();
        }

        $invoiceNo = '';
        if($invoice){
            $invoiceNo = $invoice[0]['invoice_no'];
        }

        $invoicePickupVoucherModel = new \Models\SalesInvoicePickupVoucher();
        $invoicePickupVoucherModel->useReadOnlyConnection();

        $sql = "SELECT * FROM sales_invoice_pickup_voucher WHERE pickup_voucher ='" . $params['Voucher_Id'] . "' AND status = 10 LIMIT 1";
        $result = $invoicePickupVoucherModel->getDi()->getShared('dbReader')->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $pvData = $result->fetchAll();    
        if($pvData){
            $isOldPickupVoucher = false;
        }

        $payload = [
            "voucher_id" => $params['Voucher_Id'],
            "invoice_no" => $invoiceNo,
            "email" => (isset($params['email']) ? $params['email'] : ''),
            "expired_date" => date("Y-m-d", strtotime("+7 day")),
            "is_old_pickup_voucher" => $isOldPickupVoucher
        ];

        $invoiceModel = new \Models\SalesInvoice();
        $sql = "SELECT su.supplier_alias
            FROM sales_invoice si
            INNER JOIN store s ON s.store_code = si.store_code
            INNER JOIN supplier su ON su.supplier_id = s.supplier_id
            WHERE si.invoice_id = ". $invoiceId;
        $buResult = $invoiceModel->getDi()->getShared('dbReader')->query($sql);
        $buResult->setFetchMode(
            Database::FETCH_ASSOC
        );
  
        $buResult = $buResult->fetchAll();
        if (!$buResult) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Supplier not found for invoice id " . $invoiceId;
            return;
        }

        $newMopCheck = $this->isNewMop($buResult[0]['supplier_alias'], $invoice[0]['store_code']);
        if ($newMopCheck['error_message'] != "") {
            $this->errorCode = "RR002";
            $this->errorMessages[] = $newMopCheck['error_message'];
            return;
        }
       
        $message = array();
        if (!$newMopCheck['is_new_mop']) {
            $payload["is_old_pickup_voucher"] = true;
        } 

        $message = $this->updatePosVoucherNewMop($payload);

        return $message;
    }

    public function updatePosVoucherOldMop($params = array()){
        $body = [
            "Update_By" => $params['email'],
            "Remarks" => "",
            "Expired_Date" => $params['expired_date'],
            "VoucherNo" => $params['voucher_id']
        ];

        $apiWrapper = new APIWrapper(getenv('VOUCHER_POS_END_POINT'));
        $apiWrapper->setEndPoint("UPDATE_EXPIRED_PICKUP_VOUCHER");
        $apiWrapper->setParam($body);

        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }
        $message = $apiWrapper->getMessages();

        return $message;
    }

    public function updatePosVoucherNewMop($params = array()){
        if ($params['is_old_pickup_voucher']) {
            $params["invoice_no"] = "";
        } else {
            $params["voucher_id"] = "";
        }

        $body = [
            "Update_By" => $params['email'],
            "Remarks" => "",
            "Expired_Date" => $params['expired_date'],
            "VoucherNo" => $params['voucher_id'],
            "Invoice_No" => $params['invoice_no']
        ];

        $apiWrapper = new APIWrapper(getenv('UPDATE_VOUCHER_POS_END_POINT'));
        $apiWrapper->setEndPoint("UPDATE_EXPIRED_PICKUP_VOUCHER");
        $apiWrapper->setParam($body);

        $logIdentifier = $params['is_old_pickup_voucher'] ? $params['voucher_id'] : $params['invoice_no'];
        \Helpers\LogHelper::log('pickup_voucher', '[UpdatePosVoucherNewMop][' . $logIdentifier . '] request: ' . json_encode($body), 'debug');

        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }
        $message = $apiWrapper->getMessages();

        \Helpers\LogHelper::log('pickup_voucher', '[UpdatePosVoucherNewMop][' . $logIdentifier . '] response: ' . json_encode($apiWrapper->getResponseArray()), 'debug');

        return $message;
    }

    public function isNewMop($bu = "", $storeCode = ""){
        $response = array(
            'error_message' => "",
            'is_new_mop' => false
        ); 

        if ($bu == "" || $storeCode == "") {
            $response['error_message'] = "bu or store code is empty";
            return $response;
        }

        $apiWrapper = new APIWrapper(getenv('ORDER_STORE_API'));
        $apiWrapper->setEndPoint("feature-flag?key=new_mop");

        if ($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
        } else {
            $response['error_message'] = "Failed to send request to order store api";
            return false;
        }
        
        $result = $apiWrapper->getData();
        if(!$result){
            $response['error_message'] = "Failed to get response from order store api";
            return $response;
        }

        if ($result['data']['is_all_bu'] == true) {
            $response['is_new_mop'] = true;
            return $response;
        }

        $listBUCount = count($result['data']['list_active_store']);
        foreach ($result['data']['list_active_store'] as $businessUnit) {
            if ($businessUnit['business_unit'] == $bu) {
                $listStoreCount = count($businessUnit['list_store_code']);
                if ($listStoreCount < 1) {
                    $response['is_new_mop'] = true;
                    return $response;
                }

                foreach ($businessUnit['list_store_code'] as $storeCodeData) {
                    if ($storeCodeData['store_code'] == $storeCode) {
                        $response['is_new_mop'] = true;
                        return $response;
                    }
                }
            }
        }
        
    
        return $response;
    }

    public function updateInvoiceSapSoNumber($params = array())
    {
        $invoiceNo = $params["invoice_no"];
        if (!empty($invoiceNo)) {
            $invoiceModel = new \Models\SalesInvoice();
            $updateCondition = "";
            $now = date("Y-m-d H:i:s");

            if (!empty($params['sap_so_number'])) {
                $updateCondition .= "sap_so_number='" . $params['sap_so_number'] . "', invoice_status='received', updated_at='" . $now . "'";
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Sap so number can't be empty";
                return null;
            }

            if (!empty($updateCondition)) {
                $sql1 = "UPDATE sales_invoice SET " . $updateCondition . "  WHERE invoice_no='" . $invoiceNo . "'";
                $updateSapSoresult = $invoiceModel->getDi()->getShared('dbMaster')->execute($sql1);

                if (!$updateSapSoresult) {
                    $this->errorCode = "RR100";
                    $this->errorMessages = "error when insert / updating data";
                    return null;
                }
            }

            $salesInvoiceQueryCondition = array(
                "conditions" => "invoice_no = '" . $invoiceNo . "'"
            );
            $salesInvoiceResult = $invoiceModel->findFirst($salesInvoiceQueryCondition)->toArray();

            // update sales_shipment and sales_order for store code S702
            if ($salesInvoiceResult['store_code'] == 'S702') {
                $salesOrderModel = new \Models\SalesOrder;
                $salesOrderQueryCondition = array(
                    "conditions" => "sales_order_id = '" . $salesInvoiceResult['sales_order_id'] . "'"
                );
                $salesOrderResult = $salesOrderModel->findFirst($salesOrderQueryCondition)->toArray();
    
                $salesShipmentModel = new \Models\SalesShipment;
                $sql2 = "UPDATE sales_shipment SET shipment_status= 'received', updated_at='" . $now . "' WHERE invoice_id='" . $salesInvoiceResult['invoice_id'] . "' ";
                $updateSalesShipmentResult = $salesShipmentModel->getDi()->getShared('dbMaster')->execute($sql2);
    
                $sql3 = "UPDATE sales_order SET status= 'complete', updated_at='" . $now . "' WHERE sales_order_id='" . $salesOrderResult['sales_order_id'] . "' ";
                $updateSalesOrderResult = $salesOrderModel->getDi()->getShared('dbMaster')->execute($sql3);

                if (!$updateSalesShipmentResult || !$updateSalesOrderResult) {
                    $this->errorCode = "RR100";
                    $this->errorMessages = "error when insert / updating data";
                    return null;
                }
            }
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Invoice no can't be empty";
            return null;
        }

        $this->errorCode = "";
        $this->errorMessages = "";
        return "success";
    }

    public function updateMemberNumber($params = array())
    {
        $invoiceNo = $params["invoice_no"];
        if (!empty($invoiceNo)) {
            $invoiceModel = new \Models\SalesInvoice();
            $insertCondition = "";
            $updateCondition = "";

            $salesInvoiceQueryCondition = array(
                "conditions" => "invoice_no = '" . $invoiceNo . "'"
            );
            $salesInvoiceResult = $invoiceModel->findFirst($salesInvoiceQueryCondition)->toArray();

            $salesOrderModel = new \Models\SalesOrder;
            $salesOrderQueryCondition = array(
                "conditions" => "sales_order_id = '" . $salesInvoiceResult['sales_order_id'] . "'"
            );
            $salesOrderResult = $salesOrderModel->findFirst($salesOrderQueryCondition)->toArray();

            if (substr($params['card_no'], 0, 2) === 'AR') {
                $groupId = "4";
            } else if (substr($params['card_no'], 0, 3) === 'IR7' || substr($params['card_no'], 0, 3) === 'IR8') {
                $groupId = "9";
            } else if (substr($params['card_no'], 0, 2) === 'IR') {
                $groupId = "5";
            } else if (substr($params['card_no'], 0, 2) === 'SC') {
                $groupId = "6";
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Card number wrong format";
                return null;
            }

            $now = date("Y-m-d H:i:s");
            if (!empty($params['card_no'])) {
                $insertCondition .= "customer_id='" . $salesOrderResult['customer_id'] . "' ,member_number='" . $params['card_no'] . "', group_id='" . $groupId . "', update_timestamp='" . $now . "'";
                $updateCondition .= "member_number='" . $params['card_no'] . "', update_timestamp='" . $now . "' WHERE customer_id='" . $salesOrderResult['customer_id'] . "' AND group_id='" . $groupId . "'";
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Card number can't be empty";
                return null;
            }

            if (!empty($salesOrderResult)) {
                $customer2GroupModel = new \Models\Customer2Group;
                $findExistingGroupId = $customer2GroupModel->findFirst(
                    array(
                        'conditions' => 'customer_id = "' . $salesOrderResult['customer_id'] . '" AND group_id = "' . $groupId . '" '
                    )
                );

                if ($findExistingGroupId) {
                    $sql1 = "UPDATE customer_2_group SET " . $updateCondition . "";
                    $insertMemberNumberResult = $customer2GroupModel->getDi()->getShared('dbMaster')->execute($sql1);
                } else {
                    $sql1 = "INSERT INTO customer_2_group SET " . $insertCondition . "";
                    $insertMemberNumberResult = $customer2GroupModel->getDi()->getShared('dbMaster')->execute($sql1);
                }

                $salesShipmentModel = new \Models\SalesShipment;
                $sql2 = "UPDATE sales_shipment SET shipment_status= 'received', track_number='" . $params['card_no'] . "', updated_at='" . $now . "' WHERE invoice_id='" . $salesInvoiceResult['invoice_id'] . "' ";
                $updateSalesShipmentResult = $salesShipmentModel->getDi()->getShared('dbMaster')->execute($sql2);

                $sql3 = "UPDATE sales_order SET status= 'complete', updated_at='" . $now . "' WHERE sales_order_id='" . $salesOrderResult['sales_order_id'] . "' ";
                $updateSalesOrderResult = $salesOrderModel->getDi()->getShared('dbMaster')->execute($sql3);

                $updateCustomerAceDetailResult = true;
                $updateCustomerInformaDetailResult = true;
                $updateCustomerSelmaDetailResult = true;

                if ($groupId === "4") {
                    $customerAceDetailsModel = new \Models\CustomerAceDetails;
                    $findExistingCustomerAceDetail = $customerAceDetailsModel->findFirst(
                        array(
                            'conditions' => "customer_id = '" . $salesOrderResult['customer_id'] . "'"
                        )
                    );
                    if ($findExistingCustomerAceDetail) {
                        $sql4 = "UPDATE 
                                    customer_ace_details
                                SET 
                                    ace_customer_member_no = '" . $params['card_no'] . "', updated_at = '" . date("Y-m-d H:i:s") . "'
                                WHERE 
                                    customer_id = " . $salesOrderResult['customer_id'];
                        $updateCustomerAceDetailResult = $customerAceDetailsModel->getDi()->getShared('dbMaster')->execute($sql4);
                    }
                } else if ($groupId === "5") {
                    $customerInformaDetailsModel = new \Models\CustomerInformaDetails;
                    $findExistingCustomerInformaDetail = $customerInformaDetailsModel->findFirst(
                        array(
                            'conditions' => "customer_id = '" . $salesOrderResult['customer_id'] . "'"
                        )
                    );

                    if ($findExistingCustomerInformaDetail) {
                        $sql5 = "UPDATE 
                                    customer_informa_details
                                SET 
                                    informa_customer_member_no = '" . $params['card_no'] . "', updated_at = '" . date("Y-m-d H:i:s") . "'
                                WHERE 
                                    customer_id = " . $salesOrderResult['customer_id'];
                        $updateCustomerInformaDetailResult = $customerInformaDetailsModel->getDi()->getShared('dbMaster')->execute($sql5);
                    }
                } else if ($groupId === "9") {
                    $customerSelmaDetailsModel = new \Models\CustomerSelmaDetails;
                    $findExistingCustomerSelmaDetail = $customerSelmaDetailsModel->findFirst(
                        array(
                            'conditions' => "customer_id = '" . $salesOrderResult['customer_id'] . "'"
                        )
                    );

                    if ($findExistingCustomerSelmaDetail) {
                        $sql9 = "UPDATE 
                                    customer_selma_details
                                SET 
                                    selma_customer_member_no = '" . $params['card_no'] . "', updated_at = '" . date("Y-m-d H:i:s") . "'
                                WHERE 
                                    customer_id = " . $salesOrderResult['customer_id'];
                        $updateCustomerSelmaDetailResult = $customerSelmaDetailsModel->getDi()->getShared('dbMaster')->execute($sql9);
                    }
                }
                if (
                    $insertMemberNumberResult &&
                    $updateSalesShipmentResult &&
                    $updateSalesOrderResult &&
                    $updateCustomerAceDetailResult &&
                    $updateCustomerInformaDetailResult &&
                    $updateCustomerSelmaDetailResult
                ) {
                    $this->errorCode = "";
                    $this->errorMessages = "";
                    return "success";
                } else {
                    $this->errorCode = "RR100";
                    $this->errorMessages = "error when insert / updating data";
                    return null;
                }
            }

            return false;
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Sales Invoice not found";
            return null;
        }
    }

    public function processReorderNewInvoice($params = array())
    {
        $apiWrapper = new APIWrapper(getenv('REORDER_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("reorder/new_invoice");

        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();

            // \Helpers\LogHelper::log('reorder','[SUCCESS] ' . json_encode($resultData));
        } else {
            \Helpers\LogHelper::log('reorder', '[ERROR] when calling reorder process new invoice ' . json_encode($params));
        }
    }

    public function sendCartAnalytics($params = array())
    {
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("cart/analytics");

        if (!$apiWrapper->sendRequest("post")) {
            \Helpers\LogHelper::log('cart', '[ERROR] when calling cart analytics ' . json_encode($params));
        }
    }

    public function salesCashbackHold($params = array())
    {
        $salesInvoiceModel = new \Models\SalesInvoice();
        $sql = "INSERT INTO sales_cashback_hold (order_no, status, type) values ('" . $params['order_no'] . "', 1, 'cashback')";
        try {
            $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log('error_cashback_hold', 'Failed inserting new row to sales_cashback_hold order no : ' . $params["order_no"] . ', ERROR : ' . $e->getMessage());
            $this->errorCode = "RR100";
            $this->errorMessages = "Failed inserting new row to sales_cashback_hold";
            return;
        }

        // slack channel
        $params_data = array(
            "message" => "Voucher Cashback Hold : " . $params['order_no'] . " - customer suspected as abuser",
            "slack_channel" => $_ENV['CASHBACK_HOLD_SLACK_CHANNEL'],
            "slack_username" => $_ENV['ORDER_SLACK_USERNAME']
        );
        $this->notificationError($params_data);
    }

    public function salesCashbackHoldRemove($params = array())
    {
        $salesInvoiceModel = new \Models\SalesInvoice();

        $sql = "SELECT COUNT(order_no) as totalOdi FROM sales_cashback_hold where order_no ='" . $params["order_no"] . "' AND status = 1 AND type = 'cashback'";
        $result = $salesInvoiceModel->getDi()->getShared('dbMaster')->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $totalRow = $result->fetchAll()[0]['totalOdi'];

        if ($totalRow > 0) {
            $sql = "UPDATE sales_cashback_hold SET status = 0 WHERE order_no = '" . $params["order_no"] . "'";
            try {
                $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
            } catch (\Exception $e) {
                \Helpers\LogHelper::log('error_cashback_hold', 'Failed update sales_cashback_hold order no : ' . $params["order_no"] . ', ERROR : ' . $e->getMessage());
                return;
            }
        }
    }

    private function instantUpgradeLevelUnify($customerId)
    {

        $apiWrapper = new APIWrapper(getenv('GO_CUSTOMER_COHESIVE'));
        $apiWrapper->setEndPoint("membership/" . $customerId . "/level-instant-upgrade");
        $apiWrapper->setHeaders(['Content-Type' => 'application/json']);
        $params = [
            'to_level' => '2',
        ];
        $apiWrapper->setParam($params);

        LogHelper::log(
            'level-instant-upgrade',
            '[SUCCESS] ' . json_encode($apiWrapper->getParam()),
            'info'
        );

        if ($apiWrapper->sendRequest("put")) {
            $apiWrapper->formatResponse();
            LogHelper::log(
                'level-instant-upgrade',
                '[SUCCESS] ' . json_encode($apiWrapper->getData()),
                'info'
            );
        } else {
            LogHelper::log(
                'level-instant-upgrade',
                '[ERROR] When calling go-unify endpoint ' . "/membership/" . $customerId . "/level-instant-upgrade" . json_encode($apiWrapper->getError()) .
                    ' [PAYLOAD] ' . json_encode($apiWrapper->getParam())
            );
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];

            return false;
        }


        return true;
    }

    private function processGA4Items ($cartData) {
        $processedItems = array();
        $indexItem = 0;

        foreach($cartData['items'] as $item) {
            // Get Coupon Data
            $marketingGiftCards = $item['marketing']['gift_cards'];
            $marketingGiftCardsArray = json_decode($marketingGiftCards, true);

            $voucherCodes = array();
            foreach ($marketingGiftCardsArray as $giftCard) {
                $voucherCode = $giftCard['voucher_code'];
                array_push($voucherCodes, $voucherCode);
            }
            // End of Get Coupon Data

            // Calculate Discount
            $discountMarketing = $item['marketing']['discount_amount'];
            $totalDiscount = $item['prices']['normal_price'] - $item['prices']['selling_price'] + $discountMarketing;
            // End of Calculate Discount

            $processedItem = [
                "item_id" => $item['sku'],
                "item_name" => $item['name'],
                "affiliation" => $item['supplier_alias'],
                "coupon" => implode(',', $voucherCodes),
                "discount" => $totalDiscount,
                "index" => $indexItem,
                "item_brand" => $item['brand'],
                "item_category" => $item['breadcrumbs'][0],
                "item_category2" => $item['breadcrumbs'][1],
                "item_category3" => $item['breadcrumbs'][2],
                "item_category4" => $item['breadcrumbs'][3],
                "item_list_id" => $item['category']['id'],
                "item_list_name" => $item['category']['name'],
                "price" => $item['prices']['normal_price'],
                "quantity" => $item['qty_ordered'],
                "store_code" => $item['shipping']['store_code']
            ];
            array_push($processedItems, $processedItem);

            $indexItem++;
        }
        return $processedItems;
    }
}
