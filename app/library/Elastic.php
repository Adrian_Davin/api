<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/18/2017
 * Time: 11:11 AM
 */

namespace Library;

use \Elasticsearch\ClientBuilder;

class Elastic
{
    /**
     * @var \Elasticsearch\Client
     */
    public $elasticClient;

    public $index;

    public $type;

    public $body;

    public $found;

    /**
     * @var array
     */
    protected $errors;

    protected $rawResult;

    protected $totalData = 0;

    protected $formatedResult;

    protected $hosts;
    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getRawResult()
    {
        return $this->rawResult;
    }

    /**
     * @param mixed $rawResult
     */
    public function setRawResult($rawResult)
    {
        $this->rawResult = $rawResult;
    }

    /**
     * @return mixed
     */
    public function getTotalData()
    {
        return $this->totalData;
    }

    /**
     * @param mixed $totalData
     */
    public function setTotalData($totalData)
    {
        $this->totalData = $totalData;
    }

    /**
     * @return mixed
     */
    public function getFormatedResult()
    {
        return $this->formatedResult;
    }

    /**
     * @param mixed $formatedResult
     */
    public function setFormatedResult($formatedResult)
    {
        $this->formatedResult = $formatedResult;
    }


    /**
     * Elastic constructor.
     */
    public function __construct()
    {
        $this->hosts =[
            $_ENV['ELASTIC_HOST'].':'.$_ENV['ELASTIC_PORT']
        ];

        $logger = ClientBuilder::defaultLogger($_ENV['LOG_PATH']."elastic.log");

        try {
            $this->elasticClient = ClientBuilder::create()->setRetries(2)->setLogger($logger)->setHosts($this->hosts)->build();
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internnal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

    }

    /**
     * Formating result from elastic to get only core data
     * @param array $result
     */
    public function formatResult($result = array())
    {
        if(empty($result) && !empty($this->rawResult)) {
            $result = $this->rawResult;
        } else {
            // overite current raw result
            $this->rawResult = $result;
        }

        if($result['hits']['total'] == 0) {
            return;
        }

        $hits = empty($result['hits']) ? array() : $result['hits'];
        $this->totalData = empty($hits) ? 0 : $hits['total'];
        if(isset($result['found'])) {
            $this->found = $result['found'];
        }

        $results = array();
        if($this->totalData> 0) {
            foreach($hits['hits'] as $key => $data)
            {
                $temp["_id"] = $data["_id"];
                $results[] = array_merge($temp, $data["_source"]);
            }
        }

        // if using GET (not search) it's not return hits or total, only return found or not so mapped it
        if (isset($result['found'])) {
            if(empty($results) && $result['found'] === true) {
                $temp["_id"] = $result["_id"];
                $results[] = array_merge($temp, $result["_source"]);
            }
        }

        if(count($results) == 1) {
            $this->formatedResult = $results[0];
        } else {
            $this->formatedResult = $results;
        }
    }

    /**
     * Encapsulation for searching in elastic
     * @throws HTTPException
     */
    public function search($parameter = array())
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => $this->body
        ];

        if (!empty($parameter)) {
            $params = array_merge($params, $parameter);
        }

        try {
            $this->rawResult = $this->elasticClient->search($params);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
            return false;
        }

        $this->formatResult();
    }

    public function scroll($params = array())
    {
        $this->formatedResult = array();

        try {
            $this->rawResult = $this->elasticClient->scroll($params);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
            return false;
        }

        $this->formatResult();
    }

    /**
     * Encapsulation for getting data in elastic
     * @param null $id
     * @throws HTTPException
     * @return bool
     */
    public function get($id = null)
    {
        if(empty($id)) {
            return false;
        }

        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id
        ];

        try {
            $this->rawResult = $this->elasticClient->get($params);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
            return false;
        }

        $this->formatResult();
        return true;
    }

    /**
     * Encapsulation for indexing data to elastic
     * @param $id int
     * @throws HTTPException
     * @return boolean
     */
    public function create($id = null)
    {
        if(empty($id)) {
            return false;
        }

        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => $this->body,
            'id' => $id
        ];


        try {
            $this->rawResult = $this->elasticClient->index($params);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
            return false;
        }

        return true;

    }

    /**
     * Encapsulation for updating data to elastic
     * @param null $id
     * @throws HTTPException
     * @return bool
     */
    public function update($id = null)
    {
        if(empty($id)) {
            return false;
        }

        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => $this->body,
            'id' => $id
        ];

        try {
            $this->rawResult = $this->elasticClient->update($params);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
            return false;
        }

        return true;
    }

    /**
     * Encapsulation for deleting data to elastic
     * @param null $id
     * @throws HTTPException
     * @return bool
     */
    public function delete($id = null)
    {
        if(empty($id)) {
            return false;
        }

        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id
        ];

        try {
            $this->rawResult = $this->elasticClient->delete($params);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
            return false;
        }

        return true;
    }

    public function batch($bodies = array())
    {
        $params = ['body' => []];

        for ($i = 1; $i <= count($bodies); $i++) {
            $params['body'][] = [
                'index' => [
                    '_index' => 'my_index',
                    '_type' => 'my_type',
                    '_id' => $bodies[$i-1]['product_id']
                ]
            ];

            $params['body'][] = $bodies[$i-1];

            // Every 1000 documents stop and send the bulk request
            if ($i % 100 == 0) {
                $responses = $this->elasticClient->bulk($params);

                // erase the old bulk request
                $params = ['body' => []];

                // unset the bulk response when you are done to save memory
                unset($responses);
            }
        }

        // Send the last batch if it exists
        if (!empty($params['body'])) {
           $responses = $this->elasticClient->bulk($params);
        }

        return $responses;

    }

    public function putSettings(){
        $path = $this->hosts[0].'/products/';

        $this->postClose($path);

        //update settings
        try {
            $client = new \GuzzleHttp\Client();
            $client->put($path.'_settings',array(
                'content-type' => 'application/json',
                'body' => $this->body
            ));
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
        }

        $this->postOpen($path);
    }

    public function postClose($path){
        try {
            $client = new \GuzzleHttp\Client();
            $client->post($path.'_close');
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
        }
    }

    public function postOpen($path){
        try {
            $client = new \GuzzleHttp\Client();
            $client->post($path.'_open');
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("elastic", $e->getMessage(), "Error");
        }
    }

}