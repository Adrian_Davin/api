<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;

use Helpers\RouteHelper;
use Phalcon\Cache\Frontend\Data;
use Phalcon\Db as Database;
use Models\SplashPage as SplashPageModel;

class SplashPage
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllSplashPage($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';

        $splashPageList = array();
        $splashPageModel = new SplashPageModel();

        if($access_type == "cms"){
            $parameter = array(
                "columns" => "splash_page_id, title, status",
                "conditions" => "status > -1"
            );
        }
        else{
            $parameter = array(
                "columns" => "splash_page_id, title, status",
                "conditions" => "status = 10"
            );
        }

        $splashPageList = $splashPageModel->find($parameter);

        if(!empty($splashPageList->toArray())){
            $splashPageResult = $splashPageList->toArray();

            foreach ($splashPageResult as $page => $val){
                $routeData = RouteHelper::getRouteData($val['splash_page_id'], 'splash page', $access_type);
                if(!empty($routeData)){
                    $splashPageResult[$page]['url_key'] = $routeData['url_key'];
                }
                else{
                    $splashPageResult[$page]['url_key'] = "";
                }
            }
            return $splashPageResult;
        }
        else {
            $this->errorCode="RR302";
            $this->errorMessages="No splash page has been found";
            return;
        }
    }

    public function getSplashPageDetail($params = array())
    {
        $splashPageDetail = array();
        $page_id = $params['splash_page_id'];
        $access_type = isset($params['access_type'])?$params['access_type']:'';

        $splashPageModel = new SplashPageModel();

        if($access_type == "cms"){
            $parameter = [
                "conditions" => "splash_page_id='".$page_id."' AND status > -1"
            ];
        }
        else{
            $parameter = [
                "conditions" => "splash_page_id='".$page_id."' AND status = 10"
            ];
        }

        $findPage = $splashPageModel->findFirst($parameter);

        if(!empty($findPage)){
            $splashPageDetail = $findPage->toArray();
            $routeData = RouteHelper::getRouteData($page_id, 'splash page', $access_type);

            $splashPageDetail['url_key'] = $routeData['url_key'];
            $splashPageDetail['additional_header'] = $routeData['additional_header'];
            $splashPageDetail['inline_script'] = $routeData['inline_script'];
            $splashPageDetail['inline_script_seo'] = $routeData['inline_script_seo'];
            $splashPageDetail['inline_script_seo_mobile'] = $routeData['inline_script_seo_mobile'];

            return $splashPageDetail;
        }
        else {
            $this->errorCode = "RR001";
            $this->errorMessages = "No data found";
            return;
        }
    }

    public function getSplashPageUrlKey($id = '', $splashPageType = ''){
        if(empty($id) || empty($splashPageType)){
            return false;
        }

        $splashPageModel = new \Models\SplashPage();
        $sql  = "SELECT CONCAT(url_key,'.html') as url_key FROM master_url_key WHERE reference_id = (
                        select splash_page_id from splash_page where attribute ='".$splashPageType."' and attribute_value = ".$id.")
                        and section = 'splash page'";
        $splashPageModel->useReadOnlyConnection();
        $result = $splashPageModel->getDi()->getShared($splashPageModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $urlKey = $result->fetchAll();

        if(!empty($urlKey)){
            return $urlKey[0];
        }
        else{
            return array();
        }
    }

}