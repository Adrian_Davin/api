<?php
/**
 * Library to send ruparupa order to SAP system
 */

namespace Library;

use Phalcon\Db as Database;
use Helpers\LogHelper;

class SOCIB2B
{
    
    protected $paramWsdl = array();
 
    protected $order_id;
    protected $order_date;
    protected $order_type;
    protected $cust_sold_no;
    protected $cust_ship_no;
    protected $cust_bill_no;
    protected $req_dlv_date;
    protected $ship_cost;
    protected $grand_price;
    protected $partial_ship = "";
    protected $remark;
    protected $source;
    protected $sales_id;
    protected $sales_office = "O300";
    protected $sales_grp;
    protected $shipping_point = "";
    protected $ship_condition = "Z6"; //land & sea E-Commerce
    protected $forwarder;
    protected $dp_value = "";
    protected $tax_class = "";
    protected $ship_name_1 = "";
    protected $ship_name_2 = "";
    protected $ship_address_1 = "";
    protected $ship_address_2 = "";
    protected $ship_address_3 = "";
    protected $ship_city = "";
    protected $ship_portal = "";
    protected $ship_phone = "";
    protected $ship_email = "";
    protected $bill_block = "";
    protected $dlv_block = "";
    protected $company_code = "";
    protected $site = "";

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    protected $invoiceItems;

    protected $cart;

    protected $parameter= array();    

    public function __construct($invoice = null)
    {
        if(!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }
    /**
     * @param array $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    public function getOrderType()
    {
        return $this->order_type;
    }

    public function prepareDCParamsSOCIB2B()
    {
        $companyModel = New \Models\CompanyProfileDC();
        if(empty($this->invoice)) {
            return false;
        }

        /**
         * @var $currentOrder \Models\SalesOrder
         */
        $currentOrder = $this->invoice->SalesOrder;
        $shipToId = $billToId = $soldToId = $currentOrder->getCompanyId();

        $forwardingToId = "";
        $orderDate = date_create($currentOrder->getCreatedAt());

        $this->order_id = $currentOrder->getOrderNo();
        $this->order_date = date_format($orderDate, "Ymd");        
        $this->cust_sold_no = $soldToId;
        $this->cust_ship_no = $shipToId;
        $this->cust_bill_no = $billToId;
        $this->ship_cost = number_format( ($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount()) ,0, '.', '');

        $grandPrice = $this->invoice->getGiftCardsAmount() + $this->invoice->getGrandTotal();
        $this->grand_price = number_format($grandPrice,0, '.', '');
        $this->partial_ship = "";
        $this->forwarder = $forwardingToId;
        /**
         * Sending remark to SAP
         */
        $this->remark = "";
        
        $this->shipping_point = '';
        $this->forwarder = '';
        $this->dp_value = '';
        $this->tax_class = '';

        // Genereate items
        $this->invoiceItems = $this->invoice->SalesInvoiceItem;
        
        $carrier_code = '';
        $carrier_id = 0;
        
        if (!empty($this->invoice->SalesShipment) && !empty($this->invoice->SalesShipment->SalesOrderAddress)) {
            $address = $this->invoice->SalesShipment->SalesOrderAddress;

            $address->setSalesOrderId($this->invoice->getSalesOrderId());
            $address->setAddressType('shipping');
            $address = $address->generateEntity();

            $ship_address = [];
            
            for ($i = 0; $i < 3; $i++) {
                $ship_address[$i] = mb_substr($address['full_address'], $i*35, 35, "utf-8");
            }

            $carrier_id = $this->invoice->SalesShipment->getCarrierId();
            
            $sql = "SELECT carrier_name, carrier_code
                        from shipping_carrier 
                        where carrier_id = ".$carrier_id;
            $result = $companyModel->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(Database::FETCH_ASSOC);
            $carrierResult = $result->fetchAll()[0];
            $carrier_name = $carrierResult['carrier_name'];  
            $carrier_code = $carrierResult['carrier_code'];

            $customer = $this->invoice->SalesOrder->SalesCustomer;
            // for cust_name1 = add carrier name
            $this->ship_name_1 = substr($address['first_name'], 0, 35);
            $this->ship_name_2 = substr($address['last_name'], 0, 35)." - ".$carrier_name;
            $this->ship_address_1 = $ship_address[0];
            $this->ship_address_2 = (!empty($ship_address[1])) ? $ship_address[1] : "";
            $this->ship_address_3 = (!empty($ship_address[2])) ? $ship_address[2] : "";
            $this->ship_city = substr($address['city_name'], 0, 35);
            $this->ship_portal = "";
            $this->ship_phone = $address['phone'];
            $this->ship_email = substr($customer->getCustomerEmail(), 0, 35);
            $this->bill_block = "";
            $this->dlv_block = "";
        }

        $interval = new \DateInterval('PT24H');
        $interval->invert = 0;
        if($currentOrder->getOrderType() === "b2b_informa" || $currentOrder->getOrderType() === "b2b_ace"){       
            $interval = new \DateInterval('P2D');
            $interval->invert = 0;
        }

        $delivDateTime = clone $orderDate;
        if ($carrier_id !== 0) {
            if (substr($this->order_id,0,4) == "ODIS" || substr($this->order_id,0,4) == "ODIT" || substr($this->order_id,0,4) == "ODIK") {
                $salesOrderVendorModel = new \Models\SalesOrderVendor();
                $salesOrderVendor = $salesOrderVendorModel->findFirst('order_no = "'.$currentOrder->getOrderNo().'"');
                $delivDateTime = date_create($salesOrderVendor->ship_at);

                $interval = new \DateInterval("PT0S");
                $interval->invert = 1;

                foreach (explode(",", getenv('INSTANT_SAMEDAY_CARRIER_ID')) as $carrier_instant_id) {
                    if (strval($carrier_id) == $carrier_instant_id) {
                        $delivDateTime = $orderDate;
                        $interval = new \DateInterval('PT23H59M');
                        $interval->invert = 0;
                        break;
                    }
                }
            }
        }
        $delivDateTime = $delivDateTime->add($interval);
        $this->req_dlv_date = date_format($orderDate, "Ymd");

        $delivDateStr = date_format($delivDateTime, "d.m.Y");
        $delivTimeStr = date_format($delivDateTime, "H:i:s");

        $store_code = $this->invoice->getStoreCode();
        if (substr($store_code, 0, 4) == "1000") {
            $store_code = substr($store_code,4);
        }
        // get the company profile by store_code
        $companyProfile = $companyModel->getCompanyByStoreCode($store_code);

        if (substr($store_code,0,2) == 'DC' && strlen($store_code) > 2) {
            $store_code = substr($store_code,2);
        }
        $this->site = $store_code;

       
        if(!empty($companyProfile)){
            $this->order_type = 'B2B_'.$companyProfile["company_code"];
            $this->source = "E_COMMERCE_".$companyProfile["company_code"]."_B2B";
            $this->sales_id = $companyProfile["salesman_code"];
            $this->sales_office = $companyProfile["sales_office"];
            $this->sales_grp = $companyProfile["sales_group"];

            $this->company_code = $companyProfile["company_code"];
        } else {
            $this->order_type = "B2B_ODI";
            $this->sales_id = "";
            $this->sales_office = "";
            $this->sales_grp = "";
            $this->source = "E_COMMERCE_ODI_B2B";
        }

        if ($this->invoice->getStoreCode() == "DC") {
            $this->sales_id = getenv('DC_SALES_ID');
            $this->sales_grp = "O00";
            $this->sales_office = "O300";
        }

        $remarkDelivId = $carrier_code;
        if (strpos(strtolower($remarkDelivId), 'ownfleet') !== false) {
            $remarkDelivId = "OWNFLEET";
        }

        //cek prefix if lazada or shopee or tokopedia set ship_condition to z6. pickup E-Commerce
        if(substr($this->order_id,0,3) == "LZD" || substr($this->order_id,0,4) == "ODIS" || substr($this->order_id,0,4) == "ODIT" || substr($this->order_id,0,4) == "ODIK"){
            $this->ship_condition = "Z6";

            $salesOrderVendorModel = new \Models\SalesOrderVendor();
            $salesOrderVendor = $salesOrderVendorModel->findFirst('order_no = "'.$currentOrder->getOrderNo().'"');

            $remarkDelivId = $carrier_code;

            if (strpos(strtolower($remarkDelivId), 'ownfleet') !== false) {
                $remarkDelivId = "OWNFLEET";
            }
            
            $overrideData = array();
            if(substr($this->order_id,0,4) == "ODIS") {
                $this->remark = "TYP_ORD : SHOPEE\nDEL_ID : ".$remarkDelivId."\nDEL_TIME : ".$delivTimeStr."\nDEL_DATE : ".$delivDateStr."\nSP-" . $this->invoice->getInvoiceNo();
                $overrideData = \Library\VendorStoreMapping::GetShopeeMapping($salesOrderVendor->shop_id);
            } else if(substr($this->order_id,0,4) == "ODIT") {
                $skuPromoTokpedDC = "";
                $additionalRemark = "";

                $skuPromoTokpedDC = getenv("SKU_PROMO_TOKPED_DC");
                
                if(!empty($skuPromoTokpedDC)){
                    $skuPromoTokpedDCArray = explode(",", $skuPromoTokpedDC);
                    if(!empty($this->invoiceItems)) {
                        foreach($this->invoiceItems as $item) {
                            if(in_array($item->getSku(), $skuPromoTokpedDCArray)){
                                $additionalRemark = getenv("REMARK_PROMO_TOKPED_DC");
                            }
                        }
                    }
                }
                $this->remark = "TYP_ORD : TOKOPEDIA\nDEL_ID : ".$remarkDelivId."\nDEL_TIME : ".$delivTimeStr."\nDEL_DATE : ".$delivDateStr."\nTK-" . $this->invoice->getInvoiceNo()." ".$additionalRemark;
                $overrideData = \Library\VendorStoreMapping::GetTokopediaMapping($salesOrderVendor->shop_id);
            } else if (substr($this->order_id,0,4) == "ODIK") {
                $this->remark = "TYP_ORD : TIKTOK\nDEL_ID : ".$remarkDelivId."\nDEL_TIME : ".$delivTimeStr."\nDEL_DATE : ".$delivDateStr."\nTT-" . $this->invoice->getInvoiceNo();
                $overrideData = \Library\VendorStoreMapping::GetTiktokMapping($salesOrderVendor->shop_id);
            }

            if (sizeof($overrideData) > 0) {
                $this->sales_id = $overrideData["sales_id"];
                $this->sales_office = $overrideData["sales_office"];
                $this->sales_grp = $overrideData["sales_group"];
            }
        }
    
        $salesOrderItemFlatModel = new \Models\SalesOrderItemFlat;

        foreach ($this->invoiceItems as $item) {
            $soItemFlatData = $salesOrderItemFlatModel->findFirst("sales_order_item_id = " . $item->sales_order_item_id);
            if ($soItemFlatData && $soItemFlatData->department_name === getenv("ROLKA_DEPARTMENT_NAME")) {
                $this->order_type = "B2B_HCI";
                $this->sales_id = getenv("ROLKA_DEPARTMENT_SALES_ID");
                $this->sales_office = "H300";
                $this->sales_grp = "H65";
                $this->source = "E_COMMERCE_HCI_B2B";
                $this->company_code = "HCI";
                
                $carrierCode = !empty($this->invoice->SalesShipment->ShippingCarrier->getCarrierCode()) ? $this->invoice->SalesShipment->ShippingCarrier->getCarrierCode() : "";
                if ($carrierCode == "ownfleet_informa") {
                    $carrierCode = "OWNFLEET";
                }
                
                break;
            }
        }
        
        if ($currentOrder->getOrderType() === "b2b_informa" || $currentOrder->getOrderType() === "b2b_ace" || $currentOrder->getOrderType() === "installation") {
            $invoiceCollection = $this->invoice->getInvoiceItems();
            
            // Modify invoice item, grandprice, and shipcost due to invoice item merging if invoice more than 1
            if (count($invoiceCollection) > 1) {
                $this->grand_price = $currentOrder->getGrandTotal();
                $this->ship_cost = number_format(($currentOrder->getShippingAmount() - $currentOrder->getShippingDiscountAmount()) ,0, '.', '');
                $this->invoiceItems = array();
                foreach($invoiceCollection as $invoice) {
                    foreach($invoice as $invoiceItem) {
                        $this->invoiceItems[] = $invoiceItem;
                    }
                }
            }

            $salesOrderId = 0;
            if ($currentOrder->getOrderType() === "installation") {
                $salesInstallationMdl = new \Models\SalesInstallation();
                $pendingOrderMdl = new \Models\SalesPendingOrder();
                $salesOrderMdl = new \Models\SalesOrder();

                $salesInstallationData = $salesInstallationMdl->findFirst("installation_order_no = '" . $currentOrder->getOrderNo() ."'");
                if (!$salesInstallationData) {
                    return false;
                }
        
                $refPendingOrderId = $salesInstallationData->getReferencePendingOrderID();
                $pendingOrderData = $pendingOrderMdl->findFirst("pending_order_id = " . $refPendingOrderId);
                if (!$pendingOrderData) {
                    return false;
                }

                $parentOrderNo = $pendingOrderData->getParentOrderNo();
                $salesOrderData = $salesOrderMdl->findFirst("order_no = '" . $parentOrderNo . "'");
                if (!$salesOrderData) {
                    return false;
                }

                $salesOrderId = $salesOrderData->getSalesOrderId();

            } else {
                $salesOrderId = $currentOrder->getSalesOrderId();
            }
            
            $customerB2bAccount = new \Models\CustomerB2bAccount();                        
            $companySalesman = new \Models\CompanySalesman();
            
            $customerType = "";
            
            if ($currentOrder->getOrderType() !== "additional_cost") {
                $salesB2bInformationModel = new \Models\SalesB2BInformation();  
                $salesB2bInformationData = $salesB2bInformationModel->findFirst("sales_order_id = " . $salesOrderId);
                $customerType = $salesB2bInformationData->getOrderType();
                $loglat = explode(",",$this->invoice->SalesShipment->SalesOrderAddress->geolocation);

                $typeOrdValue = "";
                if ($currentOrder->getOrderType() == "b2b_ace") {
                    $typeOrdValue = "B2B ACE";
                } else {
                    $typeOrdValue = "B2B INFORMA";
                }

                $requestedDeliveryDateRaw = $salesB2bInformationData->getRequestDeliveryDate();
                $requestedDeliveryDate = new \DateTime($requestedDeliveryDateRaw);
                $requestedDeliveryDateFormatted = $requestedDeliveryDate->format('d.m.Y');
                $this->remark = "TYP_ORD : ".$typeOrdValue."\nDEL_ID : ".$remarkDelivId."\nDEL_TIME : ".$delivTimeStr."\nDEL_DATE : ".$requestedDeliveryDateFormatted."\nB2B-" . $this->invoice->getInvoiceNo();
                $this->remark .= "\nprov:ownfleet;svc:;awb:;lg:".$loglat[0].";lt:".$loglat[1].";|".$salesB2bInformationData->getRemark();
            }
                                        
            
            if ($customerType == "b2b" || $currentOrder->getOrderType() === "additional_cost") { // B2B order and additional_cost                                                                                     
                $salesman = $companySalesman->findFirst("company_salesman_id = " .  $this->invoice->SalesOrder->SalesCustomer->CustomerB2bCompany->getCompanySalesmanId());
                if (getenv('ENVIRONMENT') != "production") {
                    $this->cust_sold_no = $this->cust_ship_no = $this->cust_bill_no = '1100001046';
                } else {
                    $account = $customerB2bAccount->findFirst("customer_b2b_account_id = " . $this->invoice->SalesOrder->SalesCustomer->CustomerB2bCompany->CustomerCompanyData->getCustomerB2bAccountId());
                    $this->cust_sold_no = $this->cust_ship_no = $this->cust_bill_no = $account->getSap_no();
                }

                $currentOrderType = $currentOrder->getOrderType();
                if ($currentOrderType == "additional_cost") {
                    $customerB2bCompanyMdl = new \Models\CustomerB2bCompany();
                    $customerB2bCompany = $customerB2bCompanyMdl->findFirst("customer_id = " . $currentOrder->getCustomerId());
                    if ($customerB2bCompany) {
                        return false;
                    }

                    if ($customerB2bCompany->getCompanyCode() == "AHI") {
                        $currentOrderType = "b2b_ace";
                    } else {
                        $currentOrderType = "b2b_informa";
                    }
                }

                if ($currentOrderType == "b2b_ace") {
                    $this->sales_office = "A300";
                    $this->order_type = "B2B_AHI";
                    $this->source = "E_COMMERCE_AHI_B2B";
                } else {
                    $this->sales_office = "H300";
                    $this->order_type = "B2B_HCI";
                    $this->source = "E_COMMERCE_HCI_B2B";
                }
                
                $this->sales_id = $salesman->getSalesman_code();
                $this->sales_grp = $salesman->getSales_group();

            } else { // B2C order
                if (getenv('ENVIRONMENT') != "production") {
                    if ($currentOrder->getOrderType() === "b2b_ace") {
                        $this->cust_sold_no = $this->cust_ship_no = $this->cust_bill_no = '1100000013';
                        $this->sales_id = "112233";
                    } else {
                        $this->cust_sold_no = $this->cust_ship_no = $this->cust_bill_no = '1100001046';
                    }
                } else {
                    if ($currentOrder->getOrderType() === "b2b_ace") {
                        $this->sales_id = "168836";
                    }

                    $this->cust_sold_no = $this->cust_ship_no = $this->cust_bill_no = '1100139800';
                }

                if ($currentOrder->getOrderType() === "b2b_ace") {
                    $this->sales_grp = "A00";
                    $this->sales_office = "A300";
                    $this->order_type = "B2B_AHI";
                    $this->source = "E_COMMERCE_AHI_B2B";
                } else {
                    $this->sales_id = "022424";
                    $this->sales_grp = "H64";
                    $this->sales_office = "H300";
                    $this->order_type = "B2B_HCI";
                    $this->source = "E_COMMERCE_HCI_B2B";
                }
            }
        }
    }

    public function generateDCParamsSOCIB2B()
    {
        $currentOrder = $this->invoice->SalesOrder;
        $orderType = $currentOrder->getOrderType();
        $thisArray = get_object_vars($this);        
        $b2b_order_detail = array();
        $underscoreParams = array('sales_grp');
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if($key == "company_code") {
                continue;
            }

            // Skip ship_cost as header when order is B2B informa/Ace
            if ($key == "ship_cost" && ($orderType == 'b2b_informa' || $orderType == 'b2b_ace')) {
                continue;
            }

            if(is_scalar($val)) {
                if(in_array($key,$underscoreParams)){
                    $currentKey = strtoupper($key);
                }else{
                    $currentKey = strtoupper(str_replace("_", "", $key));
                }

                $b2b_order_detail[$currentKey] = $val;
            }  
        }

        //Handle create SO for order type additional_cost 
        $productDetail = array();

        if ($orderType == 'additional_cost') {
            $orderNo =  $currentOrder->getOrderNo();
            $productDetail = $this->generateDCProductDetailSOCIB2BAdditionalCost($orderNo);
        } else {
            $productDetail = $this->generateDCProductDetailSOCIB2B();
        }

        $parameter = array(
            "HEADER" => $b2b_order_detail,
            "T_ITEM" => $productDetail,
            "company_code" => $this->company_code
        );

        $this->parameter = $parameter;
    }

    public function generateDCProductDetailSOCIB2B()
    {
        $productDetail = array();

        if (empty($this->invoiceItems)){
            return $productDetail;
        }

        $currentOrder = $this->invoice->SalesOrder;
        $orderType = $currentOrder->getOrderType();

        $idProductDetail = 0;
        $listDC = array();
        $mdrSkuArr = array();

        /**
         * @var $item \Models\SalesInvoiceItem
         */
        foreach($this->invoiceItems as $item) {
            $idProductDetail++;

            $finalUnitPrice = (int)$item->getSellingPrice() - (int)$item->getDiscountAmount();

            // Include price zone for b2b_ace/b2b_informa order type
            if ($orderType == "b2b_ace" || $orderType == "b2b_informa") {
                $finalUnitPrice += (int)$item->getHandlingFeeAdjust();
            }

            $unitPrice = ($finalUnitPrice <= 0) ? 1 : $finalUnitPrice;
            $salesPrice = $unitPrice * $item->getQtyOrdered();

            $store_code = $item->getStoreCode();
            $productSite = $store_code;

            if (substr($store_code, 0, 4) == "1000") {
                $store_code = substr($store_code,4);
            }
            if (substr($store_code,0,2) == 'DC' && strlen($store_code) > 2) {
                $productSite = substr($store_code,2);
            }

            $stgeLoc = "1021";
            if ($item->getStoreCode() == 'DC') {
                $stgeLoc = "1000";
                $productSite = "O001";
            }

            $itemSku = $item->getSku();
            preg_match_all('/([a-zA-Z0-9]+)-PRE/', strtoupper($item->getSku()), $match);
            if (count($match) >= 2 && isset($match[1][0])) {
                $itemSku = $match[1][0];
            }

            // Modify product site for Informa/Ace B2B installation Sku
            $installationInformaB2BSku = getenv('INSTALLATION_INFORMAB2B_SKU');
            $installationAceB2BSku = getenv('INSTALLATION_ACEB2B_SKU');
            if ($itemSku == $installationAceB2BSku) {
                $productSite = "A300";
            } else if ($itemSku == $installationInformaB2BSku) {
                $productSite = "H300";
            }

            $salesUom = \Helpers\ProductHelper::getSalesUom($itemSku);

            $poNumber = '';
            $poLevelItem = '';

            $productDetail[] = array(
                "ID" => $idProductDetail,
                "PRODUCTNO" => $itemSku,
                "SALESPRICE" => intval($salesPrice),
                "QUANTITY" => intval($item->getQtyOrdered()),
                "UNIT" => $salesUom,
                "CURRENCY" => "IDR", // For now we only using IDR for transaction
                "STGE_LOC" => $stgeLoc,
                "SITE" => $productSite,
                "SITE_VENDOR" => "",
                "CONDTYPE" => "",
                "INDENT" => "",
                "VENDOR" => "", 
                "CONFIRMNO" => "", 
                "PONUMBER" => $poNumber,
                "POLEVELITEM" => $poLevelItem,
                "SERIALNUMBER" => "" 
            );

            if ($orderType == "b2b_informa" || $orderType == "b2b_ace" || $orderType == "additional_cost") {
                $shipCost = $this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount();

                if ($shipCost > 0) {
                    if ($itemSku != $installationInformaB2BSku && $itemSku != $installationAceB2BSku) {
                        $addToArray = false;
                        
                        $itemShippingAmount = ($item->getShippingAmount() - $item->getShippingDiscountAmount()) * $item->getQtyOrdered();

                        if ($idProductDetail == 1) {
                            $addToArray = true;
                        } else {
                            for ($i = 0; $i < count($listDC); $i++) {
                                $addToArray = true;
                                if ($listDC[$i]["dc"] == $productSite) {
                                    $listDC[$i]["ship_cost"] += $itemShippingAmount;
                                    $addToArray = false;
                                    break;
                                }
                            }
                        }
            
                        if ($addToArray) {
                            $listDC[] = array(
                                "dc" => $productSite,
                                "ship_cost" => $itemShippingAmount,
                            );
                        }
                    }
                }    
            }

            // Check if current sku needs to apply MDR on customer
            $isApplyMdrOnCust = \Helpers\ProductHelper::isApplyMdrOnCust($itemSku);

            // If it needs to apply MDR, check if it has MDR fee on customer
            $salesOrderItemMdrArr = array();
            if ($isApplyMdrOnCust) {
                $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
                $salesOrderItemMdr = $salesOrderItemMdrModel->findFirst('sales_order_item_id = ' . $item->getSalesOrderItemId());
                
                if($salesOrderItemMdr){
                    $salesOrderItemMdrArr = $salesOrderItemMdr->toArray();
                }
            }

            // If it has MDR fee on customer, get MDR sku based on MDR fee, then prepare the parameter to append to product detail
            if (!empty($salesOrderItemMdrArr) && !empty($salesOrderItemMdrArr['value_customer'] && !empty($salesOrderItemMdrArr['tiering_id']))) {
                $mdrPaymentTieringModel = new \Models\MdrPaymentTiering();
                $mdrPaymentTiering = $mdrPaymentTieringModel->findFirst("tiering_id = " . $salesOrderItemMdrArr['tiering_id']);
                if($mdrPaymentTiering){
                    $mdrPaymentTieringArr = $mdrPaymentTiering->toArray();
                }
                
                $mdrSku = "";
                if (!empty($mdrPaymentTieringArr)) {
                    $mdrSku = $mdrPaymentTieringArr['sku'];

                    $mdrSkuArr[] = array(
                        "sku" => $mdrSku,
                        "price" => $salesOrderItemMdrArr["value_customer"],
                        "stge_loc" => $stgeLoc,
                        "site" => $productSite
                    );
                }
            }
        }

        if (!empty($listDC)) {
            foreach($listDC as $dc) {
                $idProductDetail++;

                $itemSku = "";

                if ($orderType == "b2b_ace") {
                    $itemSku = getenv("SHIPPING_ACEB2B_SKU");
                } else {
                    $itemSku = getenv('SHIPPING_INFORMAB2B_SKU');
                }

                $productDetail[] = array(
                    "ID" => $idProductDetail,
                    "PRODUCTNO" => $itemSku,
                    "SALESPRICE" => intval($dc["ship_cost"]),
                    "QUANTITY" => 1,
                    "CURRENCY" => "IDR",
                    "STGE_LOC" => "1021",
                    "SITE" => $dc["dc"],
                    "SITE_VENDOR" => "",
                    "CONDTYPE" => "",
                    "INDENT" => "",
                    "VENDOR" => "", 
                    "CONFIRMNO" => "", 
                    "PONUMBER" => "",
                    "POLEVELITEM" => "",
                    "SERIALNUMBER" => "",
                    "ORIGINALPRICE" => 0,
                    "DISCOUNT" => "0",
                );
            }
        }

        // Append MDR sku to product detail
        if (!empty($mdrSkuArr)) {
            foreach($mdrSkuArr as $mdrSku) {
                $idProductDetail++;

                $productDetail[] = array(
                    "ID" => $idProductDetail,
                    "PRODUCTNO" => $mdrSku["sku"],
                    "SALESPRICE" => intval($mdrSku["price"]),
                    "QUANTITY" => 1,
                    "UNIT" => "",
                    "CURRENCY" => "IDR", 
                    "STGE_LOC" => $mdrSku["stge_loc"],
                    "SITE" => $mdrSku["site"],
                    "SITE_VENDOR" => "",
                    "CONDTYPE" => "",
                    "INDENT" => "",
                    "VENDOR" => "", 
                    "CONFIRMNO" => "", 
                    "PONUMBER" => "",
                    "POLEVELITEM" => "",
                    "SERIALNUMBER" => "" 
                );
            }
        }

        return $productDetail;
    }

    public function createSOCIB2B() {
        $nsq = new \Library\Nsq();

        $data_param = json_encode($this->parameter);

        if ($data_param == false) {
            LogHelper::log('create_soci_b2b', print_r($this->parameter, true), 'ENCODE PARAM');
        }

        $message = [
            "data" => $data_param,
            "message" => "createSOCIB2B",
            "invoice_no" => $this->invoice->getInvoiceNo()
        ];
        $nsq->publishCluster('transactionSap', $message);

        return true;
    }

    public function generateDCProductDetailSOCIB2BAdditionalCost(string $orderNo) {
        $productDetail = array();
        $apiWrapper = new APIWrapper(getenv('ORDER_API_V2'));
        $apiWrapper->setEndPoint("kawanlama/so/param/item/".$orderNo);
        if ($apiWrapper->sendRequest("get")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed generate product detail additional cost";
            return false;
        }

        $productDetail = $apiWrapper->getData();
        return $productDetail;
    }
}