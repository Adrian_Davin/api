<?php
namespace Library;

use Monolog\Registry,
    Phalcon\Mvc\Model\Transaction\Manager as TxManager,
    Phalcon\Mvc\Model\Transaction\Failed as TxFailed;


/**
 * Class SalesRemark
 * @package Library
 *
 * @todo
 * @ImanSuhardiman , class never used
 *
 */

class SalesRemark
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @var \Models\SalesInvoiceRemark
     */
    protected $salesRemark;

    /**
     * @return mixed
     */
    public function getSalesRemark()
    {
        return $this->salesRemark;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {

    }

    public function getAllRemark($params = array())
    {
        $salesRemarkModel = new \Models\SalesInvoiceRemark();
        $resultSalesRemark = $salesRemarkModel::query();

        foreach($params as $key => $val)
        {
            if($key == "remark_id") {
                $resultSalesRemark->andWhere("remark_id = " . $val);
            }

            if($key == "invoice_id") {
                $resultSalesRemark->andWhere("invoice_id = " . $val);
            }

            if($key == "master_app_id") {
                $resultSalesRemark->andWhere("master_app_id = " . $val);
            }
        }

        $result = $resultSalesRemark->execute();

        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your remarks";
            return array();
        } else {

            return $result->toArray();
        }
    }
}