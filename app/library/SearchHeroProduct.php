<?php
/**
 * Created by PhpStorm.
 * User: iwan
 * Date: 30/08/18
 * Time: 9:55
 */

namespace Library;


class SearchHeroProduct
{
    /**
     * @var $searchHeroProduct \Models\SearchHeroProduct
     */
    protected $searchHeroProduct;
    protected $errorCode;
    protected $errorMessages;

    public function __construct()
    {
        $this->searchHeroProduct = new \Models\SearchHeroProduct();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getListHeroProduct($params = array()){
        $access_type = isset($params['access_type'])?$params['access_type']:'';
        $searchHeroProductModel = new \Models\SearchHeroProduct();

        $resultHeroProduct = $searchHeroProductModel::query();
        if ($access_type == 'cms') {
            $resultHeroProduct->andWhere("Models\SearchHeroProduct.status > -1");
        } else {
            $resultHeroProduct->andWhere("Models\SearchHeroProduct.status = 10");
        }

        if (isset($params['search_hero_product_id'])) {
            $resultHeroProduct->andWhere("Models\SearchHeroProduct.search_hero_product_id = ". $params['search_hero_product_id']);
        }

        if (isset($params['keyword'])) {
            $params['keyword'] = addslashes($params['keyword']);
            $resultHeroProduct->andWhere("Models\SearchHeroProduct.keyword like \"%". $params['keyword']."%\"");
        }

        if (isset($params['search_keyword'])) {
            $params['search_keyword'] = addslashes($params['search_keyword']);
            $resultHeroProduct->andWhere("Models\SearchHeroProduct.keyword = \"". $params['search_keyword']."\"");
        }

        if (isset($params['hero_product'])) {
            $params['hero_product'] = addslashes($params['hero_product']);
            $resultHeroProduct->andWhere("Models\SearchHeroProduct.hero_product like \"%". $params['hero_product']."%\"");
        }

        $resultHeroProduct->orderBy("search_hero_product_id DESC");

        $resultTotal = $resultHeroProduct->execute();

        if (isset($params['limit']) && isset($params['offset'])) {
            $resultHeroProduct->limit($params['limit'],$params['offset']);
        }

        $result = $resultHeroProduct->execute();

        if(empty($result->count())) {
            $this->errorCode = "404";
            $this->errorMessages = "No Order Found";
            return array();
        } else {
            $allHeroProduct['list'] = $result->toArray();
            $allHeroProduct['recordsTotal'] = $resultTotal->count();
        }

        return $allHeroProduct;
    }

    public function deleteHeroProduct($params = array())
    {
        $searchHeroProductModel = new \Models\SearchHeroProduct();

        // Find if hero product id is exist
        $alertResult = $searchHeroProductModel::findFirst("search_hero_product_id = '". $params['search_hero_product_id'] ."'");

        if(!$alertResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Hero Product not found";
            return false;
        }

        $searchHeroProductModel->assign($alertResult->toArray());
        $searchHeroProductModel->setStatus(-1);
        $saveStatus = $searchHeroProductModel->saveData("search_hero_product");;

        if(!$saveStatus) {
            $this->errorCode = $searchHeroProductModel->getErrorCode();
            $this->errorMessages[] = $searchHeroProductModel->getErrorMessages();
            return false;
        }

        return true;
    }
}