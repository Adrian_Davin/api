<?php
namespace Library;

use Phalcon\Db as Database;

class Shopee
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {

    }

    public function checkAllIncomplete($params = array())
    {
        if (isset($params['invoice_id'])) {
            $salesInvoiceItemModel = new \Models\SalesInvoiceItem();
            $salesInvoiceItemResult = $salesInvoiceItemModel->find('invoice_id = '.$params['invoice_id']);
            $salesInvoiceItemArray = $salesInvoiceItemResult->toArray(); 

            $flagAllIncomplete = true;
            foreach($salesInvoiceItemArray as $row) {
                if ($row['status_fulfillment'] != 'incomplete') {
                    $flagAllIncomplete = false;
                }
            }

            if ($flagAllIncomplete) {
                $orderNo = $storeCode = "";

                $sql = "select so.order_no, si.store_code from sales_order so left join sales_invoice si on si.sales_order_id = so.sales_order_id
                where si.invoice_id = " . $params['invoice_id'] . " limit 1";
                $result = $this->runReadSql($sql);
                if (isset($result[0])) {
                    $orderNo = $result[0]['order_no'];
                    $storeCode = $result[0]['store_code'];
                }

                //in this line we must add validation to reason stok_habis / lainnya
                $cancelParams = [
                    "order_no" => $orderNo
                ];

                $categoryReasonEmptyStock = [8, 9, 18, 22, 23];
                // $categoryReasonOther = [10,11,12,13,14,15,19,20,21,33];
                $reason = "";

                foreach($salesInvoiceItemArray as $row) {
                    if (in_array($row['reason'],$categoryReasonEmptyStock)) {
                        $reason = "";
                        break;
                    }else {
                        $reason = $row['note'];
                    }
                }

                $vendorName = $this->decideVendorName($orderNo);
                if($vendorName == "tokopedia" && $reason != ""){
                    $cancelParams["reason"] = $reason;
                }

                if (!empty($orderNo)) {
                    
                    if (!empty($vendorName)) {
                        // TODO: simplify this code when we integrate with shopee v2 to all shopee store

                        if ($this->isIntegratedShopeeV2($orderNo)){
                            $responseAPI = $this->callCancelOrderShopeeV2($orderNo);
                        }else{
                            $responseAPI = $this->callOrderCancel($cancelParams, $vendorName);
                        }
                        
                        if (isset($responseAPI['message'])) {
                            if ($responseAPI['message'] != 'success') {
                                \Helpers\LogHelper::log('vendor', "[ERROR] Failed to cancel order $vendorName " . $orderNo . ' ('.$storeCode.'), response: ' . json_encode($responseAPI));
                            }
                        }
                    }
                }
            }
        }        
    }
    
    private function runReadSql($sql = '')
    {
        $salesInvoiceItemModel = new \Models\SalesInvoiceItem();
        $salesInvoiceItemModel->useReadOnlyConnection();
        $result = $salesInvoiceItemModel->getDi()->getShared($salesInvoiceItemModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $resultArray = $result->fetchAll();

        return $resultArray;
    }

    private function callOrderCancel($params = array(), $vendorName = "") {
        $data = array();

        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint($vendorName."/order/cancel");        
        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }

        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        return $data;
    }

    private function callCancelOrderShopeeV2($orderNo = "") {
        $data = array();
        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_V2_API'));
        $apiWrapper->setEndPoint("/shopee/v2/order/cancel/".$orderNo);        
        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }

        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        return $data;
    }

    public function isIntegratedShopeeV2($orderNo = "") {
        $isIntegrated = false;
        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_V2_API'));
        $apiWrapper->setEndPoint("/shopee/v2/order/integrated/".$orderNo);        
        if($apiWrapper->sendRequest("get")) {
            $apiWrapper->formatResponse();
        }

        $data = $apiWrapper->getData();


        if (isset($data)) {
            $isIntegrated = $data;
        }

        return $isIntegrated;
    }

    private function decideVendorName($orderNo = "")
    {
        $vendorName = "";
        if (strpos($orderNo, 'ODIS') !== false) {
            $vendorName = 'shopee';
        } else if (strpos($orderNo, 'ODIT') !== false) {
            $vendorName = 'tokopedia';
        } else if (strpos($orderNo, 'ODIK') !== false) {
            $vendorName = 'tiktok';
        }

        return $vendorName;
    }

    public function getSalesData($invoiceNo) {
        $retVal = array();
        $retVal['sales_order_id'] = '';
        $retVal['invoice_id'] = '';
        $retVal['store_code'] = '';
        $retVal['shipment_id'] = '';
        $retVal['shipment_status'] = '';
        $retVal['sap_so_number'] = '';
        $retVal['status_so'] = '';
                
        $sql = "SELECT si.sales_order_id, si.invoice_id, ss.shipment_id, si.store_code, ss.shipment_status, si.sap_so_number, si.status_so
                FROM sales_invoice si
                LEFT JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id
                WHERE si.invoice_no = '{$invoiceNo}'";
        $result = $this->runReadSql($sql);
        if (isset($result[0])) {
            $retVal['sales_order_id'] = $result[0]['sales_order_id'];
            $retVal['invoice_id'] = $result[0]['invoice_id'];
            $retVal['store_code'] = $result[0]['store_code'];
            $retVal['shipment_id'] = $result[0]['shipment_id'];
            $retVal['shipment_status'] = $result[0]['shipment_status'];
            $retVal['sap_so_number'] = $result[0]['sap_so_number'];
            $retVal['status_so'] = $result[0]['status_so'];
        }
        return $retVal;
    }

    public function orderCancelAcceptReject($params = array())
    {
        if (isset($params['vendor_status']) && isset($params['order_no']) && isset($params['invoice_no']) && isset($params['admin_user_id'])) {
            $vendorName = $this->decideVendorName($params['order_no']);
            if (empty($vendorName)) {
                $this->errorCode = "RR400";
                $this->errorMessages = "Order No not belong to any Vendor";
                return;
            }

            if ($params['vendor_status'] == 'approve_request' || $params['vendor_status'] == 'confirm_cancel' ) {
                $parameter = $this->getSalesData($params['invoice_no']);

                $salesOrderID = $parameter['sales_order_id'];
                $invoiceID = $parameter['invoice_id'];
                $shipmentID = $parameter['shipment_id'];
                $storeCode = $parameter['store_code'];

                if ($salesOrderID == '') {
                    $this->errorCode = "RR400";
                    $this->errorMessages = "Failed to update data";
                    return;
                }
                
                $salesInvoiceItemModel = new \Models\SalesInvoiceItem();
                $salesInvoiceItemResult = $salesInvoiceItemModel->find('invoice_id = '.$invoiceID);
                $salesInvoiceItemArray = $salesInvoiceItemResult->toArray(); 

                $statusFulfillmentItem = $salesInvoiceItemArray[0]['status_fulfillment'];

                $flagLog = (substr( $storeCode, 0, 2 ) === "DC") ? 'dc_dashboard':'store_dashboard';    
                $actions = "customer $vendorName request to cancel";
                $affected_field = "Customer Request";
                $valueLog = $params['vendor_status'];
                $skuList = array();
                if ($params['vendor_status'] == 'approve_request') {
                    $sql = "SELECT sku FROM sales_invoice_item 
                    WHERE invoice_id = $invoiceID";
                    $result = $this->runReadSql($sql);
                    if (count($result) > 0) {
                        $skuList = array();
                        foreach($result as $val) {
                            $skuList[] = $val['sku'];
                        }
                    }
                }

                if (!isset($skuList) && $params['vendor_status'] != "confirm_cancel") {
                    $this->errorCode = "RR400";
                    $this->errorMessages = "Failed to update data";
                    return;
                }       

                // TODO: simplify this code when we integrate with shopee v2 to all shopee store

                if ( $this->isIntegratedShopeeV2($params['order_no']) ){
                    $responseAPI = $this->CallAcceptBuyerCancellation($params['order_no']);
                }else{
                    $responseAPI = $this->callOrderAccept(["order_no" => $params['order_no']], $vendorName);
                }

                // Spoof success 
                // $responseAPI = array(
                //     "message" => "success",
                // );

                if (isset($responseAPI['message'])) {
                    if ($responseAPI['message'] == 'success') {
                        $salesInvoiceModel = new \Models\SalesInvoice();
                        if ($params['vendor_status'] == 'approve_request') {
                            $sql = "START TRANSACTION; 

                            UPDATE sales_order SET status = 'processing' 
                                WHERE order_no = '{$params['order_no']}';

                            UPDATE sales_invoice SET status_fulfillment = 'batal' 
                                WHERE invoice_no = '{$params['invoice_no']}';

                            UPDATE sales_invoice_item SET status_fulfillment = 'incomplete', reason = 21, note = '{$vendorName} Customer request cancel (approved)' 
                                WHERE invoice_id = (SELECT invoice_id FROM sales_invoice WHERE invoice_no = '{$params['invoice_no']}' LIMIT 1);";

                            $flagLog = 'store_dashboard';    
                            if (substr( $storeCode, 0, 2 ) === "DC") {
                                $flagLog = 'dc_dashboard';
                            }

                            $comment = '';    
                            foreach($skuList as $val) {
                                $sql .= " INSERT INTO sales_log (
                                    sales_order_id, invoice_id, shipment_id, 
                                    admin_user_id, actions, affected_field, 
                                    value, log_time, flag
                                ) VALUES (
                                    {$salesOrderID},{$invoiceID},{$shipmentID},
                                    {$params['admin_user_id']},'set status to incomplete for sku: {$val}, reason = 21','invoice_item.status_fulfillment',
                                    'incomplete',now(),'{$flagLog}'
                                );";

                                $comment .= "sku: {$val}, reason: Customer request cancel order, note: {$vendorName} Customer request cancel (approved) | ";
                            }

                            $comment = rtrim($comment, ' | ');
                            $sql .= " INSERT INTO sales_comment (
                                sales_order_id, invoice_id, comment, 
                                admin_user_id, created_at, flag, 
                                user_group, topic_id
                            ) VALUES (
                                {$salesOrderID},{$invoiceID},'{$comment}',
                                {$params['admin_user_id']},now(),'chat',
                                'ruparupa', 9
                            );";

                            $sql .= " COMMIT;"; 
                        } else if ($params['vendor_status'] == 'confirm_cancel') {
                            $flagLog = 'store_dashboard';    
                            if (substr( $storeCode, 0, 2 ) === "DC") {
                                $flagLog = 'dc_dashboard';
                            }

                            $sql = "
                            START TRANSACTION; 
                            
                            INSERT INTO sales_log (
                                sales_order_id, invoice_id, shipment_id, 
                                admin_user_id, actions, affected_field, 
                                value, log_time, flag
                            ) VALUES (
                                {$salesOrderID},{$invoiceID},{$shipmentID},
                                {$params['admin_user_id']},'{$storeCode} incomplete order - {$vendorName} confirm','invoice_item.status_fulfillment',
                                'incomplete',now(),'{$flagLog}'
                            );

                            INSERT INTO sales_comment (
                                sales_order_id, invoice_id, comment,
                                topic_id, admin_user_id, user_group,
                                flag, created_at
                            ) VALUES (
                                {$salesOrderID}, {$invoiceID}, '{$storeCode} - {$params['invoice_no']} incomplete order - {$vendorName} confirm',
                                9 ,{$params['admin_user_id']}, 'ruparupa',
                                'chat', now()
                            );
                            
                            COMMIT;
                            ";
                        }
                        $salesInvoiceModel->useWriteConnection();                                               
                        try {
                            $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
                        } catch (\Exception $e){ 
                            $this->errorCode = "RR400";
                            $this->errorMessages = "Failed to update data";
                        }
                        return;

                    }
                }

                $this->errorCode = "RR400";
                $this->errorMessages = "Failed to approve vendor order";
            } else if ($params['vendor_status'] == 'reject_request') {
                $parameter = $this->getSalesData($params['invoice_no']);

                $salesOrderID = $parameter['sales_order_id'];
                $invoiceID = $parameter['invoice_id'];
                $shipmentID = $parameter['shipment_id'];
                $storeCode = $parameter['store_code'];

                $flagLog = (substr( $storeCode, 0, 2 ) === "DC") ? 'dc_dashboard':'store_dashboard';    
                $actions = "customer $vendorName request to cancel";
                $affected_field = "Customer Request";
                $valueLog = $params['vendor_status'];

                // TODO: simplify this code when we integrate with shopee v2 to all shopee store

                if ($this->isIntegratedShopeeV2($params['order_no'])){
                    $responseAPI = $this->CallRejectBuyerCancellation($params['order_no']);
                }else{
                    $responseAPI = $this->callOrderReject(["order_no" => $params['order_no']], $vendorName);
                }

                if (isset($responseAPI['message'])) {
                    if ($responseAPI['message'] == 'success') {
                        $salesInvoiceModel = new \Models\SalesInvoice();
                        $sql = "START TRANSACTION;
                        UPDATE sales_order SET status = 'processing' WHERE order_no = '{$params['order_no']}';";

                         //record request customer approval for tokped and shopee
                        $sql .= " INSERT INTO sales_log (
                            sales_order_id, invoice_id,shipment_id, 
                            admin_user_id,actions,affected_field,value,log_time, flag
                        ) VALUES (
                            {$salesOrderID},{$invoiceID},{$shipmentID},{$params['admin_user_id']},'{$actions}','{$affected_field}','{$valueLog}',now(),'{$flagLog}'
                        );
                        COMMIT;";

                        $salesInvoiceModel->useWriteConnection();                                               
                        try {
                            $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
                        } catch (\Exception $e){
                            $this->errorCode = "RR400";
                            $this->errorMessages = "Failed to update data";
                        }

                        return;
                    }
                }

                $this->errorCode = "RR400";
                $this->errorMessages = "Failed to reject vendor order";
            } else if ($params['vendor_status'] == 'set_incomplete_item'){
                // run if order canceled from tokopedia seller
                $salesInvoiceModel = new \Models\SalesInvoice();
                $salesOrderVendorModel = new \Models\SalesOrderVendor();

                $salesOrderVendor = $salesOrderVendorModel->findFirst('order_no = "'.$params['order_no'].'"');
                $statusSalesOrderVendor = $salesOrderVendor->order_status;
                if ($statusSalesOrderVendor == 'Order rejected by seller' || $statusSalesOrderVendor == 'INCOMPLETE') {
                    return;
                }
                
                $parameter = $this->getSalesData($params['invoice_no']);
                $salesOrderID = $parameter['sales_order_id'];
                $invoiceID = $parameter['invoice_id'];
                $shipmentID = $parameter['shipment_id'];
                $storeCode = $parameter['store_code'];
                $statusShipment = str_replace("_"," ",ucfirst($parameter['shipment_status']));

                $salesInvoiceItemModel = new \Models\SalesInvoiceItem();
                $salesInvoiceItemResult = $salesInvoiceItemModel->find('invoice_id = '.$invoiceID);
                $salesInvoiceItemArray = $salesInvoiceItemResult->toArray(); 

                $statusFulfillmentItem = $salesInvoiceItemArray[0]['status_fulfillment'];

                if (substr( $storeCode, 0, 2 ) == "DC") {
                    if ($parameter['sap_so_number'] == null){
                        $statusShipment = "Pending SO SAP";
                    }else if ($parameter['so_status'] == "" && $statusFulfillmentItem == null){
                        $statusShipment = "Pending DO MP";
                    }else if ($statusShipment == "New"){
                        $statusShipment = "Stand by";
                    }
                }

                $sql = "
                START TRANSACTION; 

                UPDATE sales_invoice SET status_fulfillment = 'batal' 
                                WHERE invoice_no = '{$params['invoice_no']}';

                UPDATE sales_invoice_item SET status_fulfillment = 'incomplete', reason=48, note ='timeout autocancel' 
                                WHERE invoice_id = (SELECT invoice_id FROM sales_invoice WHERE invoice_no = '{$params['invoice_no']}' LIMIT 1);
                
                INSERT INTO sales_log (
                    sales_order_id, invoice_id, shipment_id, 
                    admin_user_id, actions, affected_field, 
                    value, log_time, flag
                ) VALUES (
                    {$salesOrderID},{$invoiceID},{$shipmentID},
                    {$params['admin_user_id']},'{$statusShipment} - Timeout {$vendorName} Autocancel','invoice_item.status_fulfillment',
                    'incomplete',now(), 'chat'
                );

                INSERT INTO sales_comment (
                    sales_order_id, invoice_id, comment,
                    topic_id, admin_user_id, user_group,
                    flag, created_at
                ) VALUES (
                    {$salesOrderID}, {$invoiceID}, '{$statusShipment} - Timeout {$vendorName} Autocancel',
                    9 ,{$params['admin_user_id']}, 'ruparupa',
                    'chat', now()
                );
                
                COMMIT;
                ";

                try {
                    $salesInvoiceModel->useWriteConnection();                                           
                    $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
                } catch (\Exception $e){
                    $this->errorCode = "RR400";
                    $this->errorMessages = "Failed to update data";
                }
            } else if ($params['vendor_status'] == 'auto_cancel'){
                // run if order canceled from tokopedia seller
                $salesInvoiceModel = new \Models\SalesInvoice();
                $salesOrderVendorModel = new \Models\SalesOrderVendor();

                $salesOrderVendor = $salesOrderVendorModel->findFirst('order_no = "'.$params['order_no'].'"');
                $statusSalesOrderVendor = $salesOrderVendor->order_status;
                if ($statusSalesOrderVendor == 'Order rejected by seller' || $statusSalesOrderVendor == 'INCOMPLETE') {
                    return;
                }

                $parameter = $this->getSalesData($params['invoice_no']);
                $salesOrderID = $parameter['sales_order_id'];
                $invoiceID = $parameter['invoice_id'];
                $shipmentID = $parameter['shipment_id'];
                $storeCode = $parameter['store_code'];
                $originShipmentStatus = $parameter['shipment_status'];
                $statusShipment = str_replace("_"," ",ucfirst($parameter['shipment_status']));

                $salesInvoiceItemModel = new \Models\SalesInvoiceItem();
                $salesInvoiceItemResult = $salesInvoiceItemModel->find('invoice_id = '.$invoiceID);
                $salesInvoiceItemArray = $salesInvoiceItemResult->toArray(); 

                $statusFulfillmentItem = $salesInvoiceItemArray[0]['status_fulfillment'];

                if (substr( $storeCode, 0, 2 ) == "DC") {
                    if ($parameter['sap_so_number'] == null){
                        $statusShipment = "Pending SO SAP";
                    }else if ($parameter['so_status'] == "" && $statusFulfillmentItem == null){
                        $statusShipment = "Pending DO MP";
                    }else if ($statusShipment == "New"){
                        $statusShipment = "Stand by";
                    }
                }

                $sql = "
                START TRANSACTION;
                ";

                if ($originShipmentStatus == "new" || $originShipmentStatus == "processing"){
                    $sql = $sql . "

                    UPDATE sales_invoice 
                    SET status_fulfillment = 'batal' 
                    WHERE invoice_no = '{$params['invoice_no']}';

                    UPDATE sales_invoice_item 
                    SET status_fulfillment = 'incomplete', reason=48, note ='timeout autocancel' 
                    WHERE invoice_id = (
                        SELECT invoice_id 
                        FROM sales_invoice 
                        WHERE invoice_no = '{$params['invoice_no']}' 
                        LIMIT 1
                    );
                    ";
                }

                $sql = $sql . "

                INSERT INTO sales_log (
                    sales_order_id, invoice_id, shipment_id, 
                    admin_user_id, actions, affected_field, 
                    value, log_time, flag
                ) VALUES (
                    {$salesOrderID},{$invoiceID},{$shipmentID},
                    {$params['admin_user_id']},'{$statusShipment} - Timeout {$vendorName} Autocancel','invoice.status_fulfillment',
                    'batal',now(), 'chat'
                );

                INSERT INTO sales_log (
                    sales_order_id, invoice_id, shipment_id, 
                    admin_user_id, actions, affected_field, 
                    value, log_time, flag
                ) VALUES (
                    {$salesOrderID},{$invoiceID},{$shipmentID},
                    {$params['admin_user_id']},'{$statusShipment} - Timeout {$vendorName} Autocancel','invoice_item.status_fulfillment',
                    'incomplete',now(), 'chat'
                );

                INSERT INTO sales_comment (
                    sales_order_id, invoice_id, comment,
                    topic_id, admin_user_id, user_group,
                    flag, created_at
                ) VALUES (
                    {$salesOrderID}, {$invoiceID}, '{$statusShipment} - Timeout {$vendorName} Autocancel',
                    9 ,{$params['admin_user_id']}, 'ruparupa',
                    'chat', now()
                );
                
                COMMIT;
                ";

                try {
                    $salesInvoiceModel->useWriteConnection();                                           
                    $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
                } catch (\Exception $e){
                    $this->errorCode = "RR400";
                    $this->errorMessages = "Failed to update data";
                }

                // Only update if store code is DC
                if (substr( $storeCode, 0, 2 ) == "DC") {
                    // Update sales_invoice.invoice_status and sales_shipment.shipment_status to canceled for dc dashboard v2
                    $statusInvoiceAndShipment = "canceled";
                    $sql = "
                    START TRANSACTION;

                    UPDATE sales_invoice
                    SET invoice_status = '{$statusInvoiceAndShipment}'
                    WHERE invoice_id = {$invoiceID};

                    UPDATE sales_shipment
                    SET shipment_status = '{$statusInvoiceAndShipment}'
                    WHERE invoice_id = {$invoiceID};
                    
                    COMMIT;
                    ";
                    try {
                        $salesInvoiceModel->useWriteConnection();                                           
                        $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
                    } catch (\Exception $e){
                        $this->errorCode = "RR400";
                        $this->errorMessages = "Failed to update data sales_invoice.invoice_status & sales_shipment.shipment_status";
                    }
                }
            } else if ($params['vendor_status'] == 'confirm_request_cancel') {

                $salesInvoiceModel = new \Models\SalesInvoice();

                $parameter = $this->getSalesData($params['invoice_no']);
                $storeCode = $parameter['store_code'];
                $salesOrderID = $parameter['sales_order_id'];
                $invoiceID = $parameter['invoice_id'];
                $shipmentID = $parameter['shipment_id'];
                $storeCode = $parameter['store_code'];
                $originShipmentStatus = $parameter['shipment_status'];

                $flagLog = 'store_dashboard';    
                if (substr( $storeCode, 0, 2 ) === "DC") {
                    $flagLog = 'dc_dashboard';
                }

                $sql = "
                START TRANSACTION;
                ";

                if ($originShipmentStatus === "new" || $originShipmentStatus === "processing"){
                    $sql = $sql . "

                    UPDATE sales_invoice 
                    SET status_fulfillment = 'batal' 
                    WHERE invoice_no = '{$params['invoice_no']}';

                    UPDATE sales_invoice_item 
                    SET status_fulfillment = 'incomplete', reason = 21, note = '{$vendorName} Customer request cancel (approved)' 
                    WHERE invoice_id = (
                        SELECT invoice_id 
                        FROM sales_invoice 
                        WHERE invoice_no = '{$params['invoice_no']}' 
                        LIMIT 1
                    );";
                }

                $sql = $sql . "

                INSERT INTO sales_log (
                    sales_order_id, invoice_id, shipment_id, 
                    admin_user_id, actions, affected_field, 
                    value, log_time, flag
                ) VALUES (
                    {$salesOrderID},{$invoiceID},{$shipmentID},
                    {$params['admin_user_id']},'{$storeCode} request cancel order - {$vendorName} confirmed','invoice.status_fulfillment',
                    'batal',now(),'{$flagLog}'
                );
                
                INSERT INTO sales_log (
                    sales_order_id, invoice_id, shipment_id, 
                    admin_user_id, actions, affected_field, 
                    value, log_time, flag
                ) VALUES (
                    {$salesOrderID},{$invoiceID},{$shipmentID},
                    {$params['admin_user_id']},'{$storeCode} request cancel order - {$vendorName} confirmed','invoice_item.status_fulfillment',
                    'incomplete',now(),'{$flagLog}'
                );

                INSERT INTO sales_comment (
                    sales_order_id, invoice_id, comment,
                    topic_id, admin_user_id, user_group,
                    flag, created_at
                ) VALUES (
                    {$salesOrderID}, {$invoiceID}, '{$storeCode} - {$params['invoice_no']} request cancel order - {$vendorName} confirm',
                    9 ,{$params['admin_user_id']}, 'ruparupa',
                    'chat', now()
                );
                
                COMMIT;
                ";

                $salesInvoiceModel->useWriteConnection(); 


                try {
                    $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
                } catch (\Exception $e){ 
                    $this->errorCode = "RR400";
                    $this->errorMessages = "Failed to update data";
                }
                return;
            }

        } else {
            $this->errorCode = "RR400";
            $this->errorMessages = "order_no, invoice_no, vendor_status and admin_user_id are mandatory";
        }

        return;
    }

    public function updateReturnShopee($params = array())
    {
        if (isset($params['order_no_vendor']) && isset($params['admin_user_id'])) {
            $orderNoVendor = $params['order_no_vendor'];
            $adminUserID = $params['admin_user_id'];
            unset($params['order_no_vendor'], $params['admin_user_id']);
            if (count($params) > 0) {
                if (isset($params['tracking_no_shopee_to_rupa'])) {
                    $sql = "SELECT si.sales_order_id, si.invoice_id, ss.shipment_id
                    FROM sales_invoice si
                    LEFT JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id
                    LEFT JOIN sales_order so ON so.sales_order_id = si.sales_order_id
                    LEFT JOIN sales_order_vendor sov ON sov.order_no = so.order_no
                    LEFT JOIN sales_return_vendor srv ON srv.order_no_vendor = sov.order_no_vendor
                    WHERE srv.order_no_vendor = '$orderNoVendor' LIMIT 1";
                    $result = $this->runReadSql($sql);
                    if (isset($result[0])) {
                        $salesOrderID = $result[0]['sales_order_id'];
                        $invoiceID = $result[0]['invoice_id'];
                        $shipmentID = $result[0]['shipment_id'];
                    }

                    if (!isset($salesOrderID)) {
                        $this->errorCode = "RR400";
                        $this->errorMessages = "Failed to update data";
                        return;
                    }

                    $salesInvoiceModel = new \Models\SalesInvoice();                        
                    $sql = "START TRANSACTION;        

                    UPDATE sales_return_vendor SET tracking_no_shopee_to_rupa = '{$params['tracking_no_shopee_to_rupa']}' 
                        WHERE order_no_vendor = '{$orderNoVendor}';

                    INSERT INTO sales_log (sales_order_id, invoice_id, shipment_id, admin_user_id, actions, affected_field, value, log_time, flag) 
                        VALUES ({$salesOrderID},{$invoiceID},{$shipmentID},{$adminUserID},'update Tracking No Shopee2Rupa #{$orderNoVendor}','sales_return_vendor.tracking_no_shopee_to_rupa','{$params['tracking_no_shopee_to_rupa']}',now(),'store_dashboard');

                    COMMIT;";
                    $salesInvoiceModel->useWriteConnection();                                               
                    try {
                        $salesInvoiceModel->getDi()->getShared($salesInvoiceModel->getConnection())->query($sql);
                    } catch (\Exception $e){
                        $this->errorCode = "RR400";
                        $this->errorMessages = "Failed to update data";
                    }                
                }
            }
        } else {
            $this->errorCode = "RR400";
            $this->errorMessages = "order_no_vendor and admin_user_id are mandatory";
        }

        return;
    }

    private function callOrderReject($params = array(), $vendorName = "") {
        $data = array();

        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint($vendorName."/order/cancellation/reject");        
        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }

        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        return $data;
    }

    private function CallRejectBuyerCancellation($orderNo = "") {
        $data = array();        

        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_V2_API'));
        $apiWrapper->setEndPoint("/shopee/v2/order/cancellation/reject/".$orderNo);        
        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }

        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        return $data;
    }

    private function callOrderAccept($params = array(), $vendorName = "") {
        $data = array();        

        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint($vendorName."/order/cancellation/accept");        
        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }

        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        return $data;
    }

    private function CallAcceptBuyerCancellation($orderNo = "") {
        $data = array();        

        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_V2_API'));
        $apiWrapper->setEndPoint("/shopee/v2/order/cancellation/accept/".$orderNo);        
        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }

        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        return $data;
    }

    // For invoice dashboard in oms
    public function getReturnShopeeData($params = array())
    {
        if (isset($params['limit']) && isset($params['offset'])) {
            $limit = $params['limit'];
            $offset = $params['offset'];
            unset($params['limit']);
            unset($params['offset']);
        }

        $where_clause = $sort = '';
        $flagSort = $flagOperator = 0;
        $query = "SELECT 
            srv.order_no_vendor,
            sov.order_no,
            si.invoice_no,
            si.created_at as invoice_date,
            CONCAT(COALESCE(sc.customer_firstname,''),' ',COALESCE(sc.customer_lastname,'')) as customer_name,
            si.store_code,
            srv.status as return_status,
            srv.tracking_no_shopee_to_rupa as awb_shopee2rupa,
            si.sap_so_number,
            (SELECT coalesce(credit_memo_no, '') from sales_credit_memo where order_no = so.order_no limit 1) as credit_memo_no,
            (SELECT coalesce(credit_memo_id, '') from sales_credit_memo where order_no = so.order_no limit 1) as credit_memo_id,
            srv.text_reason as return_reason 
        FROM sales_return_vendor srv LEFT JOIN sales_order_vendor sov ON sov.order_no_vendor = srv.order_no_vendor
        LEFT JOIN sales_order so ON so.order_no = sov.order_no
        LEFT JOIN sales_customer sc ON sc.sales_order_id = so.sales_order_id
        LEFT JOIN sales_invoice si ON si.sales_order_id = so.sales_order_id";

        if (count($params) > 0) {                     
            if (isset($params['invoice_keyword'])) {
                $where_clause .= "si.invoice_no LIKE '%" . $params['invoice_keyword'] . "%'";
                $flagOperator++;
            }

            if (isset($params['invoice_date_keyword'])) {
                if ($flagOperator) $where_clause .= " AND ";
                $dateFrom = $params['invoice_date_keyword']." 00:00:00";
                $dateTo   = $params['invoice_date_keyword']." 23:59:59";
                $where_clause .= "(si.created_at BETWEEN '$dateFrom' AND '$dateTo')";
                $flagOperator++;
            }

            if (!empty($params['customer_name_keyword'])) {
                if ($flagOperator) $where_clause .= " AND ";
                $where_clause .= "so.sales_order_id IN (SELECT sales_order_id FROM sales_customer WHERE customer_firstname like '%" . $params['customer_name_keyword'] . "%' OR customer_lastname like '%" . $params['customer_name_keyword'] . "%')";
                $flagOperator++;
            }

            if (isset($params['store_code_keyword'])) {
                if ($flagOperator) $where_clause .= " AND ";
                $where_clause .= "si.store_code LIKE '%" . $params['store_code_keyword'] . "%'";
                $flagOperator++;
            }

            if (isset($params['return_status_keyword'])) {
                if ($flagOperator) $where_clause .= " AND ";
                $where_clause .= "srv.status LIKE '%" . $params['return_status_keyword'] . "%'";
                $flagOperator++;
            }

            if (isset($params['order_no_vendor'])) {
                if ($flagOperator) $where_clause .= " AND ";
                $where_clause .= "srv.order_no_vendor = '{$params['order_no_vendor']}'";
                $flagOperator++;
            }

            if (isset($params['so_sap_keyword'])) {
                if ($flagOperator) $where_clause .= " AND ";
                $where_clause .= "si.sap_so_number LIKE '%" . $params['so_sap_keyword'] . "%'";
            }

            if (!empty($where_clause)) {
                $where_clause = " WHERE " . $where_clause;   
            }

            if (isset($params['invoice_date_sorting'])) {
                $sort .= " si.created_at ".$params['invoice_date_sorting'];
                $flagSort++;
            }

            if (isset($params['store_code_sorting'])) {
                if ($flagSort) $sort .= " ,";
                $sort .= " si.store_code ".$params['store_code_sorting'];
                $flagSort++;
            }

            if (isset($params['return_status_sorting'])) {
                if ($flagSort) $sort .= " ,";
                $sort .= " srv.status ".$params['return_status_sorting'];
                $flagSort++;
            }

            if (isset($params['customer_name_sorting'])) {
                if ($flagSort) $sort .= " ,";
                $sort .= " sc.customer_firstname ".$params['customer_name_sorting'];
            }
        }

        if(empty($sort)){
            $sort = " srv.created_at ASC";
        }

        $query .= $where_clause . " ORDER BY $sort ";
        if (isset($limit) && isset($offset)) {
            $query .= "LIMIT $limit OFFSET $offset";
        }

        $result = $this->runReadSql($query);
        if (count($result) > 0) {
            for ($i=0; $i < count($result); $i++) { 
                // get return item list
                $query = "SELECT sku, name, amount AS qty 
                FROM sales_return_vendor_item 
                WHERE sales_return_vendor_id = (SELECT sales_return_vendor_id FROM sales_return_vendor WHERE order_no_vendor = '{$result[$i]['order_no_vendor']}' LIMIT 1)";
                $resultItem = $this->runReadSql($query);
                $result[$i]['items'] = $resultItem;
            }
        }

        $allReturnData['list'] = $result;
        $allReturnData['recordsTotal'] = count($result);

        return $allReturnData;
    }

    public function getOrderShopee($params = array())
    {
        $data = array();
        if (isset($params['limit']) && isset($params['offset'])) {            
            $orders = array();
            $shopeeOrderModel = new \Models\ShopeeOrder();
            $orders = $shopeeOrderModel->getOrderShopee($params);
            
            $data['recordsTotal'] = $orders['count'];
            $data['data'] = $orders['data'];
        }

        return $data;
    }

    public function getDataEscrow($params = array())
    {
        $data = array();
        if (isset($params['startdate']) && isset($params['enddate'])) {            
            // $orders = array();
            $shopeeOrderModel = new \Models\ShopeeOrder();
            $data['data'] = $shopeeOrderModel->getDataEscrow($params);
        }

        return $data;
    }
}