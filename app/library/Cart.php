<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 7/6/2017
 * Time: 8:46 AM
 */

namespace Library;

use Exception;
use Helpers\GeneralHelper;
use \Models\Cart as CartModel;
use \Models\ShippingAssignation as ShippingAssignation;
use \Library\Redis\MasterCCBank;
use \Library\Redis\MarketingPromotion;
use \Library\Marketing as MarketingLib;
use \Library\Payment as PaymentLib;
use Phalcon\Test\Mvc\Model\Behavior\Helper;

/**
 * Class Cart
 * @package Library
 *
 */
class Cart
{
    /**
     * @var \Models\Cart
     */
    protected $cart_model;

    protected $error_code;
    protected $error_messages;

    private $_current_discount;

    public function __construct()
    {
        $this->cart_model = new CartModel();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->error_messages;
    }

    public function setCartId($cartId = "")
    {
        $this->cart_model->setCartId($cartId);
    }

    public function setReservedOrderNo($orderNo = "")
    {
        $this->cart_model->setReservedOrderNo($orderNo);
    }

    public function getCartId()
    {
        if(!empty($this->cart_model)) {
            return $this->cart_model->getCartId();
        } else {
            return null;
        }
    }

    public function getToken()
    {
        if(!empty($this->cart_model)) {
            return $this->cart_model->getToken();
        } else {
            return null;
        }
    }

    /**
     * @param array $params
     * updating / creating Cart model from passed API params
     * perform required calculations
     */
    public function setFromParam($params = [])
    {
        // updating cart data from params (items, shipping, billing, customer, etc)
        $this->cart_model->setCartData($params);   
        
        // initialize cart reserved order no
        $this->cart_model->initiateReserveOrderNo();
        
        $this->cart_model->countSubtotalAndQtyItems();
        $this->cart_model->completeCustomerInformation();
        $this->cart_model->completeShippingInformation();        
        
        $sourceOrder = $this->cart_model->getSourceOrder();
        if(!empty($sourceOrder) && $sourceOrder == 'lazada') {
            $this->cart_model->assignLazadaItem();
        } else {
            $this->cart_model->checkRestrictedEvent();
            // in this function we also count handling fee
            $this->cart_model->checkStockAndAssignation();
        }
        
        $this->cart_model->countShippingAndHandlingFee();
        $this->cart_model->countGrandTotal();
        $this->cart_model->defineGroupShipment();
        
        $this->errorChecker();
    }

    public function savePayment($params = [])
    {
        if(empty($params['payment'])) {
            $this->error_code = 400;
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "No payment data to process";
        }

        $this->cart_model->initialize($params['cart_id']);
        $this->cart_model->setPayment($params['payment']);

        $this->errorChecker();
    }

    public function savePoNumber($params = [])
    {
        if(empty($params['number'])) {
            $this->error_code = 400;
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "No po number data to process";
        }

        $this->cart_model->initialize($params['cart_id']);
        $this->cart_model->setPoNumber($params);

        $this->errorChecker();
    }

    public function deleteCartItems($params)
    {
        if(empty($params['cart_id'])) {
            $this->error_code = 400;
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "cart_id is required";

        }
        $this->cart_model->setCartId($params['cart_id']);
        $cartData = $this->cart_model->loadCartData();

        if(empty($cartData['items'])) {
            $this->error_code = "RR204";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart have no item(s)";
            return;
        }

        $itemToDelete = empty($params['items']) ? [] : $params['items'];
        $referenceItemToDelete = [];
        foreach($cartData['items'] as $keyItem => $item) {
            if(in_array($item['sku'],$itemToDelete)) {
                if(!empty($item['parent_id'])) {
                    $referenceItemToDelete[] = $item['parent_id'];
                }
                $referenceItemToDelete[] = $item['id'];
                unset($cartData['items'][$keyItem]);
            }
        }

        // Now remove item with this reference
        foreach($cartData['items'] as $keyItem => $item) {
            if(in_array($item['parent_id'],$referenceItemToDelete) && $item['parent_id'] != 0) {
                unset($cartData['items'][$keyItem]);
            }
        }

        $finalCartData = $this->cart_model->resetMarketingData($cartData);
        $finalCartData['gift_cards_amount'] = 0;
        $this->cart_model->setFromArray($finalCartData);
        $this->cart_model->countSubtotalAndQtyItems();
        $this->cart_model->countShippingAndHandlingFee();
        $this->cart_model->countGrandTotalExcludeShipping();
        \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 195', 'debug');
        $this->checkMarketingPromotion();
        \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 197', 'debug');
        $this->cart_model->checkGiftcartApplied();
        $this->saveCart();
        $this->errorChecker();
    }

    public function errorChecker()
    {
        if(!empty($this->cart_model->getErrorMessages())) {
            $this->error_code = $this->cart_model->getErrorCode();
            $this->error_messages = $this->cart_model->getErrorMessages();
        }
    }

    public function saveCart()
    {
        $this->cart_model->saveCart();
        $this->errorChecker();
    }

    public function checkMarketingPromotion($promotion_type = [])
    {
        \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 218', 'debug');
        $this->cart_model->checkMarketingPromotion($promotion_type);
        /* Commented because double call
        // Make sure free item can get free shipping promotion
        if(!in_array('shipping',$promotion_type)) {
            $this->cart_model->checkMarketingPromotion(['shipping']);
        }*/
        
        // Validate shipping discount amount can not more than shipping amount
        if($this->cart_model->getShippingDiscountAmount() > $this->cart_model->getShippingAmount()) {
            $this->cart_model->setShippingDiscountAmount($this->cart_model->getShippingAmount());
        }
        
        // Now we resign if there any free item
        $this->cart_model->stockAssignationMixSku();

        $this->errorChecker();
    }

    public function checkGiftcartApplied($filterGiftCards = array())
    {
        \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 240', 'debug');
        $this->cart_model->checkGiftcartApplied($filterGiftCards);
        $this->errorChecker();
    }

    /**
     * @deprecated
     * Please use go-cart to get cart data
     */
    public function loadCartData($reset_shipping = 0, $modifyItems = 0)
    {
        $modifiedItem = array();
        $sender = '';

        if($reset_shipping == 1) {
            $this->cart_model->initialize();
            $this->cart_model->resetShippingRate();
            \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 257', 'debug');
            $this->cart_model->checkGiftcartApplied();

            $cartData = $this->cart_model->getDataArray();
        } else {
            $cartData = $this->cart_model->loadCartData(true);
        }

        if ($modifyItems == 1) {
            if(!empty($cartData) && !empty($cartData['items'])){
                foreach ($cartData['items'] as $key => $val) {
                    $cartData['items'][$key]['free_items'] = array();
                    // Before Grouping, pls add free_items
                    if (!empty($val['is_free_item']) && !empty($val['parent_id'])) {
                        unset($cartData['items'][$key]);

                        $found = array_search($val['parent_id'], array_column($cartData['items'], 'id'));
                        if ($found === '0' || $found === 0 || $found) {
                            $cartData['items'][$found]['shipping']['shipping_amount'] += $val['shipping']['shipping_amount'];
                            $cartData['items'][$found]['shipping']['shipping_discount_amount'] += $val['shipping']['shipping_discount_amount'];
                            $freeItem['sku'] = $val['sku'];
                            $freeItem['name'] = $val['name'];
                            $freeItem['qty_ordered'] = $val['qty_ordered'];
                            $freeItem['primary_image_url'] = $val['primary_image_url'];
                            $freeItem['prices']['selling_price'] = $val['prices']['selling_price'];
                            $freeItem['url_key'] = $val['url_key'];
                            $cartData['items'][$found]['free_items'][] = $freeItem;
                        }
                    }
                }                
                
                // group the cart item by sender, storeCode, deliveryMethod, carrierID and geoLocation
                $cartCustom = array();
                foreach ($cartData['items'] as $rowItem){
                    $sender = $this->getSender($rowItem);
                    $storeCode = $rowItem['shipping']['store_code'];
                    $courierPickup = ($rowItem['shipping']['delivery_method'] == 'pickup') ? $rowItem['shipping']['store_code'] : $rowItem['shipping']['carrier_id'];
                    $geolocation = (isset($rowItem['shipping']['shipping_address']['geolocation']) && !empty($rowItem['shipping']['shipping_address']['geolocation'])) ? $rowItem['shipping']['shipping_address']['geolocation'] : 'geolocation_empty';
                    
                    // complete the address
                    if (count($rowItem['shipping_address']) > 0) {
                        if (!isset($rowItem['shipping_address']['province']['province_name'])) {
                            $resultProvince = $this->cart_model->getDataProvince($rowItem['shipping_address']['province']['province_id']);
                            if (count($resultProvince) > 0) {
                                $rowItem['shipping_address']['province'] = $resultProvince;
                            }
                        }
    
                        if (!isset($rowItem['shipping_address']['city']['city_name'])) {
                            $resultCity = $this->cart_model->getDataCity($rowItem['shipping_address']['city']['city_id']);
                            if (count($resultCity) > 0) {
                                $rowItem['shipping_address']['city'] = $resultCity;
                            }
                        }
    
                        if (!isset($rowItem['shipping_address']['kecamatan']['kecamatan_name'])) {
                            $resultKecamatan = $this->cart_model->getDataKecamatan($rowItem['shipping_address']['kecamatan']['kecamatan_id']);
                            if (count($resultKecamatan) > 0) {
                                $rowItem['shipping_address']['kecamatan'] = $resultKecamatan;
                            }
                        }
                    }

                    $cartCustom[$sender][$storeCode][$rowItem['delivery_method']][$courierPickup][$geolocation][] = $rowItem; 
                }

                // convert the group to ordinary array
                $tempItem = array();
                foreach($cartCustom as $keySender => $valueSender) {
                    foreach($valueSender as $keyStoreCode => $valueStoreCode) {
                        foreach($valueStoreCode as $keyDeliveryMethod => $valueDeliveryMethod) {
                            foreach($valueDeliveryMethod as $keyCarrierID => $valueCarrierID) {
                                foreach($valueCarrierID as $keyGeoLocation => $valueGeoLocation) {
                                    $tempItem[] = $valueGeoLocation; 
                                }   
                            }          
                        }
                    }
                }

                // complete $tempItem header
                $modifiedItem = array();
                $parentCount = 0;
                foreach($tempItem as $rowHeader) {
                    $tempShippingFee = $tempHandlingFee = $tempShippingFeeDiscount = 0;
                    foreach($rowHeader as $rowItemChild) {
                        $modifiedItem[$parentCount]['sender'] = $this->getSender($rowItemChild);
                        $modifiedItem[$parentCount]['delivery_method'] = $rowItemChild['delivery_method'];
                        if (isset($rowItemChild['carrier_id']) && !empty($rowItemChild['carrier_id'])) {
                            $modifiedItem[$parentCount]['carrier'] = $this->getCarrierData($rowItemChild['carrier_id']);
                        } else {
                            $modifiedItem[$parentCount]['carrier'] = array();
                        }
                        
                        if (isset($rowItemChild['pickup_code'])) {
                            $modifiedItem[$parentCount]['origin_pickup'] = $this->getPickupData($rowItemChild['pickup_code']);
                        }
                        
                        if ($rowItemChild['delivery_method'] <> 'pickup') {
                            if (!isset($rowItemChild['shipping_address']['geolocation'])) {
                                $rowItemChild['shipping_address']['geolocation'] = '';
                            }
                        }
                        
                        $modifiedItem[$parentCount]['shipping_address'] = $rowItemChild['shipping_address'];
                        if (isset($modifiedItem[$parentCount]['shipping_address']['kecamatan']['kecamatan_code'])) {
                            if (!isset($modifiedItem[$parentCount]['shipping_address']['address_name'])) {
                                $modifiedItem[$parentCount]['shipping_address']['address_name'] = $rowItemChild['shipping_address']['address_name']  = '';
                            }                        
                        }


                        $modifiedItem[$parentCount]['details'][] = $rowItemChild;
                        $tempShippingFee += $rowItemChild['shipping_amount'];
                        $tempShippingFeeDiscount += $rowItemChild['shipping_discount_amount'];
                        $tempHandlingFee += $rowItemChild['handling_fee_adjust'];
                    }

                    $modifiedItem[$parentCount]['shipping_fee'] = $tempShippingFee;
                    $modifiedItem[$parentCount]['shipping_fee_discount'] = $tempShippingFeeDiscount;
                    $modifiedItem[$parentCount]['handling_fee'] = $tempHandlingFee;
                    $parentCount++;
                }
                
                $cartData['items'] = $modifiedItem;
            }
        }

        $this->errorChecker();

        return $cartData;
    }

    public function getSender($row)
    {
        $sender = 'Ruparupa';
        if ($row['product_source'] == 'Marketplace') {
            $supplierModel = new \Models\Supplier();
            $supplierData = $supplierModel->findFirst([
                "columns" => "dikirim_oleh",
                "conditions" => "supplier_alias='".$row['supplier_alias']."'"
            ]);

            if ($supplierData) {
                $sender = $supplierData['dikirim_oleh'];
            }
        }

        return $sender;
    }

    public function getPickupData($pickupCode)
    {
        $pickup = array();
        $pickupModel = new \Models\PickupPoint();
        $pickupData = $pickupModel::findFirst([
            "conditions" => "pickup_code='".$pickupCode."'"
        ]);

        if ($pickupData) {
            $result = $pickupData->getDataArray();
            $pickup['pickup_code'] = $result['pickup_code'];
            $pickup['pickup_name'] = $result['pickup_name'];
            $pickup['geolocation'] = (isset($result['geolocation'])) ? $result['geolocation'] : '';
            $pickup['is_express_courier'] = $result['is_express_courier'];
            if (!isset($result['address_line_2'])) {
                $pickup['pickup_address'] = $result['address_line_1'];
            } else {
                $pickup['pickup_address'] = $result['address_line_1'] . ', ' . $result['address_line_2'];
            }
        }

        return $pickup;
    }

    public function getCarrierData($carrierID)
    {
        $carrier = array();
        $carrierModel = new \Models\ShippingCarrier();
        $carrierData = $carrierModel::findFirst([
            "conditions" => "carrier_id= $carrierID"
        ]);

        if ($carrierData) {
            $result = $carrierData->getDataArray();
            $carrier['carrier_id'] = $result['carrier_id'];
            $carrier['carrier_code'] = $result['carrier_code'];
            $carrier['carrier_name'] = $result['carrier_name'];
        }

        return $carrier;
    }

    public function getDataArray()
    {
        return $this->cart_model->getDataArray();
    }

    public function countShippigAndHandling($params = array())
    {
        // Load shopping cart
        $this->cart_model->initialize($params['cart_id']);
        $this->cart_model->resetShippingRate();
        $this->cart_model->countShippingAndHandlingFee();

        \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 459', 'debug');
        $this->checkMarketingPromotion(['shipping']);

        // Validate shipping discount amount can not more than shipping amount
        if($this->cart_model->getShippingDiscountAmount() > $this->cart_model->getShippingAmount()) {
            $this->cart_model->setShippingDiscountAmount($this->cart_model->getShippingAmount());
        }

        \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 470', 'debug');
        $this->checkGiftcartApplied();
        $this->errorChecker();
    }

    public function checkPayment($params = array())
    {
        $paymmentLib = new PaymentLib();

        // Load shopping cart
        $this->cart_model->initialize($params['cart_id']);
        $cartData = $this->cart_model->getDataArray();
        $originCartData = $cartData;

        $shippingAmount = intval($cartData['shipping_amount'] - $cartData['shipping_discount_amount']);

        if($shippingAmount != 0) {
            $shippingAmount = number_format(($cartData['shipping_amount'] - $cartData['shipping_discount_amount']) + $cartData['handling_fee_adjust'], 0,",",".");
        } else {
            $shippingAmount = number_format($shippingAmount, 0,",",".");
        }

        // Prepare sending back data to consumer
        $responseParam = [
            'subtotal' => $cartData['subtotal'],
            'shipping' => $shippingAmount,
            'gift_voucher' => $cartData['gift_cards_amount'],
            'discount' => $cartData['discount_amount'],
            'grand_total' => number_format($cartData['grand_total'], 0,",","."),
            'grand_total_raw' => (int)$cartData['grand_total'],
            'shipping_discount_amount' => -1,
            'is_have_promotion' => false,
            'is_can_installment' => false,
            'is_allowed_online' => false,
            'method' => '',
            'payment_location' => 'inpage',
            'mesesage' => '',
            'bank_name' => '',
            'applied_promo' => []
        ];

        // Check if bin number is not rejected by ruparupa.com
        $params['payment']['card_number'] = !empty($params['payment']['card_number'])? $params['payment']['card_number'] : "";
        if ( $paymmentLib->isAllowedCreditCard($params['payment']['card_number'])) {
            $responseParam['is_allowed_online'] = true;
        }

        /**
         * Credit card vendor checking
         */
        $vendorInfo = $paymmentLib->creditCardVendorChecking($params['payment']['card_number']);
        $responseParam['method'] = $vendorInfo['payment_method'];
        $responseParam['payment_location'] = $vendorInfo['payment_location'];
        $responseParam['mesesage'] = $vendorInfo['message'];

        /**
         * Credit card installment
         */
        list($responseParam['is_can_installment'], $responseParam['bank_name']) = $paymmentLib->isCanInstallment($params['payment']['card_number'],(int)$cartData['grand_total']);

        /**
         * @todo : get type of bank here (if needed in future)
         */
        
        if($responseParam['method'] == ''){
            $responseParam['method'] = $params['payment']['method'];
        }

        /**
         * Start checking bin number for promotion
         */
        $marketingLib = new Marketing();
        $promotionType = ['payment'];

        $marketingPromo = $marketingLib->getMarketingPromotion($promotionType);

        if(empty($marketingPromo)) {
            return $responseParam;
        } else {
            $paymentPromotion = json_decode($marketingPromo['payment'], true);

            // List rule that already been applied before
            $appliedRuleIds = [];
            if(!empty($cartData['cart_rules'])) {
                $appliedRules = json_decode($cartData['cart_rules']);
                if(!empty($appliedRules)) {
                    foreach ($appliedRules as $key => $rule) {
                        $appliedRuleIds[] = $rule->rule_id;
                    }
                }
            }

            $isHavePromo = false;
            if(count($paymentPromotion) > 0) {
                $marketingLib = new MarketingLib();

                if(!empty($params['payment']['card_number'])) {
                    $cardNo = substr(str_replace(' ','',$params['payment']['card_number']),0,6);
                    $params['payment']['cc_bin'] = $cardNo;
                }

                if (empty($params['method']) && !empty($responseParam['method'])) {
                    $params['payment']['method'] = $responseParam['method'];
                }

                $cartData['payment'] = $params['payment'];

                foreach ($paymentPromotion as $promotion) {

                    // if already applied then skip
                    if(in_array($promotion['rule_id'], $appliedRuleIds)) {
                        continue;
                    }

                    if (isset($params['flag_cashback'])) {
                        if ($params['flag_cashback']) {
                            if (isset($promotion['action']['applied_action'])) {
                                if ($promotion['action']['applied_action'] = 'cash_back') {
                                    $promotion['uses_per_customer'] = 0;
                                }
                            }
                        }
                    }
                       
                    $canContinue = $marketingLib->prerequisiteConditionCheck($cartData,$promotion);

                    if(!$canContinue) {
                        continue;
                    }
                     \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 595', 'debug');
                    $isMatchCondition = $this->cart_model->marketingConditionCheck($promotion['conditions'],$cartData);

                    // Apply promotion
                    if($isMatchCondition) {
                        $isHavePromo = true;
                        $applyUsingVoucher = false;

                        /**
                         * If using voucher, check if voucher already redeem or not
                         */
                        $voucherCode = "";
                        if($promotion['use_voucher'] == 1) {
                            $giftCardInfo = json_decode($this->cart_model->getGiftCards(),true);
                            $foundInGiftcard = false;
                            foreach ($giftCardInfo as $gcId => $gcVal) {
                                if($gcVal['rule_id'] == $promotion['rule_id'] && empty($gcVal['voucher_amount_used'])) {
                                    $voucherCode = $gcVal['voucher_code'];
                                    if($promotion['action']['applied_action'] == 'use_voucher') {
                                        $applyUsingVoucher = true;
                                    }
                                    $foundInGiftcard = true;
                                    unset($giftCardInfo[$gcId]);
                                }
                            }

                            if($foundInGiftcard) {
                                $this->cart_model->setGiftCards(json_encode($giftCardInfo));
                            } else {
                                return false;
                            }
                        }

                        if(empty(intval($promotion['action']['max_redemption']))) {
                            $promotion['action']['max_redemption'] = "0.00";
                        }

                        if($applyUsingVoucher) {
                            $updateCart = isset($params['save_promotion']) ? $params['save_promotion'] : false;
                            \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 634', 'debug');
                            $this->cart_model->redeemVoucher($voucherCode, true,false,$updateCart);
                        } else {
                           $promotion['type'] = 'payment'; 
                           $this->cart_model->applyMarketingAction($promotion);
                        }

                        if($promotion['stop_rules_processing'] == true) {
                            break;
                        }
                    }
                }
            }

            /**
             * @todo : move it to top, need to check it in global state
             */
            if(!empty($params['payment']['card_number'])) {
                $responseParam['bank_name'] = "";
                $bankIssuer = MasterCCBank::getBank($params['payment']['card_number']);
                if(!empty($bankIssuer)) {
                    $responseParam['bank_name'] = $bankIssuer['bank_name'];
                }
            }

            if($isHavePromo) {
                $appliedPromotion = [];
                $discount_amount = 0;
                foreach ($this->cart_model->getCurrentDiscount() as $key => $promo) {
                    $appliedPromotion[] = $promo['name'];
                    $discount_amount += (int)$promo['discount_amount'];
                }

                $cartData = $this->cart_model->getDataArray();

                if($shippingAmount != 0) {
                    $shippingAmount = number_format(($cartData['shipping_amount'] - $cartData['shipping_discount_amount']) + $cartData['handling_fee_adjust'], 0,",",".");
                } else {
                    $shippingAmount = number_format($shippingAmount, 0,",",".");
                }

                $responseParam['subtotal'] = $cartData['subtotal'];
                $responseParam['shipping'] = $shippingAmount;
                $responseParam['gift_voucher'] = $cartData['gift_cards_amount'];
                $responseParam['discount_amount'] = $cartData['discount_amount'];
                $responseParam['grand_total'] = number_format($cartData['grand_total'], 0,",",".");
                $responseParam['grand_total_raw'] = (int)$cartData['grand_total'];
                $responseParam['shipping_discount_amount'] = (($originCartData['shipping_amount'] == $originCartData['shipping_discount_amount'])? -1 : (int)$cartData['shipping_discount_amount']);

                // Now we check for posibility of cashback promotion
                if(!empty($responseParam['bank_name'])) {
                    $MarketingPromotionCache = MarketingPromotion::getPromotionCache(date('Ymd'), ['invoice_created']);
                    if(!empty($MarketingPromotionCache)) {
                        $marketingPromo = json_decode($MarketingPromotionCache['invoice_created'], true);
                        foreach ($marketingPromo as $promotion) {
                            // Check if promotion name have bank name in it
                            if (strpos(strtolower($promotion['name']), $responseParam['bank_name']) !== false) {
                                foreach ($promotion['conditions'] as $condition) {
                                    // Check if promotion is applied to payment
                                    if (strpos($condition['attribute'], 'payment') !== false) {
                                        $appliedPromotion[] = $promotion['name'];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if(isset($params['payment']['save_promotion']) && ($params['payment']['save_promotion'] == true || $params['payment']['save_promotion'] == "true") ) {
                    $this->saveCart();
                }

                $responseParam['applied_promo'] = $appliedPromotion;
                $responseParam['is_have_promotion'] = true;
            } else {
                $responseParam['is_have_promotion'] = false;
            }

            return $responseParam;
        }
    }

    public function checkCashBackPromotion($params = array())
    {
        $marketingLib = new Marketing();
        $promotionType = ['invoice_created'];

        $marketingPromo = $marketingLib->getMarketingPromotion($promotionType);

        // Load shopping cart
        $this->cart_model->initialize($params['cart_id']);
        $cartData = $this->cart_model->getDataArray();

        // Prepare sending back data to consumer
        $responseParam = [
            'is_have_promotion' => false
        ];

        if(empty($marketingPromo)) {
            return $responseParam;
        } else {
            $cashbackPromotion = json_decode($marketingPromo['invoice_created'], true);

            // List rule that already been applied before
            $appliedRuleIds = [];
            if(!empty($cartData['cart_rules'])) {
                $appliedRules = json_decode($cartData['cart_rules']);
                if(!empty($appliedRules)) {
                    foreach ($appliedRules as $key => $rule) {
                        $appliedRuleIds[] = $rule->rule_id;
                    }
                }
            }

            $isHavePromo = false;
            if(count($cashbackPromotion) > 0) {
                $marketingLib = new MarketingLib();

                if(!empty($params['payment']['card_number'])) {
                    $cardNo = substr(str_replace(' ','',$params['payment']['card_number']),0,6);
                    $params['payment']['cc_bin'] = $cardNo;
                }

                $cartData['payment'] = isset($cartData['payment']) ? $cartData['payment'] : "";

                foreach ($cashbackPromotion as $promotion) {
                    // if already applied then skip
                    if(in_array($promotion['rule_id'], $appliedRuleIds)) {
                        $isHavePromo = true;
                        continue;
                    }

                    if (isset($params['flag_cashback'])) {
                        if ($params['flag_cashback']) {
                            if (isset($promotion['action']['applied_action'])) {
                                if ($promotion['action']['applied_action'] = 'cash_back') {
                                    $promotion['uses_per_customer'] = 0;
                                }
                            }
                        }
                    }

                    $canContinue = $marketingLib->prerequisiteConditionCheck($cartData,$promotion);

                    if(!$canContinue) {
                        continue;
                    }

                    $isValidUsage = $marketingLib->maxUsageValidation($promotion, $cartData['customer']['customer_id']);
                    if($promotion['uses_per_customer'] > 0) {
                        if($isValidUsage === false) {
                            continue;
                        }
                    }

                    $is_allow_group = false;
                    if(isset($promotion['customer_group_id']) && $promotion['customer_group_id'] != "") {
                        $allowedGroup = explode(",",$promotion['customer_group_id']);
                        foreach ($cartData['customer']['customer_group'] as $customer_group) {
                            if(in_array($customer_group,$allowedGroup)) {
                                $is_allow_group = true;
                            }
                        }
                    }

                    // If this group not allow to get this promotion, skip it
                    if(!$is_allow_group) {
                        continue;
                    }

                    $conditions = \Helpers\CartHelper::removeConditionBasedOnAttribute($promotion['conditions'],"payment");
                      \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 805 '.$cartData['reserved_order_no'], 'debug');
                    $isMatchCondition = $this->cart_model->marketingConditionCheck($conditions,$cartData);

                    // Apply promotion
                    if($isMatchCondition) {
                        $isHavePromo = true;
                        $applyUsingVoucher = false;

                        /**
                         * If using voucher, check if voucher already redeem or not
                         */
                        $voucherCode = "";
                        if($promotion['use_voucher'] == 1) {
                            $giftCardInfo = json_decode($this->cart_model->getGiftCards(),true);
                            $foundInGiftcard = false;
                            foreach ($giftCardInfo as $gcId => $gcVal) {
                                if($gcVal['rule_id'] == $promotion['rule_id'] && empty($gcVal['voucher_amount_used'])) {
                                    $voucherCode = $gcVal['voucher_code'];
                                    if($promotion['action']['applied_action'] == 'use_voucher') {
                                        $applyUsingVoucher = true;
                                    }
                                    $foundInGiftcard = true;
                                    unset($giftCardInfo[$gcId]);
                                }
                            }

                            if($foundInGiftcard) {
                                $this->cart_model->setGiftCards(json_encode($giftCardInfo));
                            } else {
                                continue;
                            }
                        }

                        if($applyUsingVoucher) {
                            $updateCart = isset($params['save_promotion']) ? $params['save_promotion'] : false;
                                       \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 841', 'debug');
                            $this->cart_model->redeemVoucher($voucherCode, true,false,$updateCart);
                        } else {
                            $rule_id = $promotion['action']['associate_rule_id'];

                            $totalQtyitem = 0;
                            if(isset($promotion['action']['voucher_generated']) && $promotion['action']['voucher_generated'] == 1) {
                                $action = $promotion['action'];
                                $discount_amount = !empty($action['discount_amount']) ? $action['discount_amount'] : 0;
                                $amountAttribute = !empty($action['attributes']) ? $action['attributes'] : "subtotal";
                                $discountType = !empty($action['discount_type']) ? $action['discount_type'] : "percent";

                                if($amountAttribute == 'shipping_amount') {
                                    $originAmount = intval($cartData['shipping_amount'] - $cartData['shipping_discount_amount']);
                                } else {
                                    $conditionAtribute = explode(".",$amountAttribute);
                                    if(count($conditionAtribute) > 1) {
                                        // If condition more than one it's mean item attribute
                                        $itemAttribute = $conditionAtribute[1];

                                        $promotion['conditions'] = \Helpers\CartHelper::keepAttributeConditions($promotion['conditions']);
                                        $originAmountAttribute = 0;
                                        if(!empty($promotion['conditions'])) {
                                            $promotion['conditions'] = \Helpers\CartHelper::removeAtributeCondition($promotion['conditions']);
                                            
                                            $conditions = $promotion['conditions'];
                                            // handle grand_total_exclude_shipping
                                            foreach($conditions as $key => $value) {
                                                if ($value['attribute'] == 'grand_total_exclude_shipping') {
                                                    unset($conditions[$key]);
                                                }
                                            }

                                            $cashbackSku = array();
                                            foreach ($cartData['items'] as $item) {
                                                \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 874', 'debug');
                                                $conditonCheckingStatus = $marketingLib->marketingConditionCheck($conditions,$item);
                                                $matchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;
                                                
                                                //$matchCondition = $this->cart_model->marketingConditionCheck($promotion['conditions'],$item);
                                                if($matchCondition) {
                                                    $originAmountAttribute += $item[$itemAttribute];
                                                    $cashbackSku[] = $item['sku'];

                                                    if(isset($promotion['action']['max_quantity'])) {
                                                        $promotion['action']['max_quantity'] = empty($promotion['action']['max_quantity']) ? 0 : $promotion['action']['max_quantity'];

                                                        if($promotion['action']['max_quantity'] != 0) {
                                                            if($item['qty_ordered'] > $promotion['action']['max_quantity']) {
                                                                $totalQtyitem += $promotion['action']['max_quantity'];
                                                            } else {
                                                                $totalQtyitem += $item['qty_ordered'];
                                                            }
                                                        } else if($promotion['action']['max_quantity'] == 0) {
                                                            $totalQtyitem += $item['qty_ordered'];
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    } else {
                                        $promotion['conditions'] = \Helpers\CartHelper::removeAtributeCondition($promotion['conditions']);
                                        $originAmountAttribute = isset($cartData[$amountAttribute]) ? intval($cartData[$amountAttribute]) : 0;
                                        $cashbackSku = array();
                                        foreach ($cartData['items'] as $item) {
                                             \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 902', 'debug');
                                            $matchCondition = $this->cart_model->marketingConditionCheck($promotion['conditions'],$item);
                                            if($matchCondition) {
                                                $cashbackSku[] = $item['sku'];
                                            }
                                        }
                                    }

                                    if($amountAttribute == "grand_total") {
                                        $shippingAmount = intval($cartData['shipping_amount']) - intval($cartData['shipping_discount_amount']);
                                        $originAmount = intval($originAmountAttribute) - $shippingAmount;
                                    } else {
                                        $originAmount = $originAmountAttribute;
                                    }

                                    // Finaly we need to deduct amount with gift card usage
                                    $originAmount = $originAmount - $cartData['gift_cards_amount'];
                                }

                                if($discountType == 'fix') {
                                    $voucherAmount =  $discount_amount;
                                } else if ($discountType == 'percent') {
                                    $discountAmountGet = round((intval($discount_amount) / 100) * $originAmount);
                                    $voucherAmount =  \Helpers\CartHelper::countDiscountAmountUsed($originAmount,$discountAmountGet);

                                    if(isset($action['max_redemption'])) {
                                        if(!empty((int)$action['max_redemption']) && $voucherAmount > $action['max_redemption']) {
                                            $voucherAmount = intval($action['max_redemption']);
                                        }
                                    }
                                } else if ($discountType == 'fix_multiple') {
                                    $voucherAmount =  $discount_amount * (($totalQtyitem == 0) ? 1 : $totalQtyitem);
                                }

                                if($voucherAmount <= 0) {
                                    $isHavePromo = false;
                                }
                            } else {
                                $voucherModel = new \Models\SalesruleVoucher();
                                $parameters = [
                                    'conditions' => 'rule_id = ' . $rule_id . ' AND times_used = 0 AND status = 10'
                                ];
                                $voucher_result = $voucherModel::findFirst($parameters);

                                if ($voucher_result) {
                                    $voucherAmount = $voucher_result->getVoucherAmount();
                                }
                            }
                        }

                        if($promotion['stop_rules_processing'] == true) {
                            break;
                        }
                    }
                }
            }
            
            $messages = array();

            // Is have promotion cashback
            if($isHavePromo) {
                $appliedPromotion = [];
                $discount_amount = 0;
                foreach ($this->cart_model->getCurrentDiscount() as $key => $promo) {
                    $appliedPromotion[] = $promo['name'];
                    $discount_amount += (int)$promo['discount_amount'];
                }

                $responseParam['sku'] = array();
                if(isset($cashbackSku)) {
                    $responseParam['sku'] = $cashbackSku;
                }
                if(isset($rule_id)) {
                    $responseParam['rule_id'] = $rule_id;
                }

                $responseParam['is_have_promotion'] = true;
                $responseParam['voucher_amount'] = isset($voucherAmount) ? $voucherAmount : 0;
                $messages[] = "Selamat, Anda berpotensi mendapatkan cashback Rp ".number_format($responseParam['voucher_amount'],0,',','.');
            } else {
                $responseParam['is_have_promotion'] = false;
            }

            $paymentCashback = explode(',', getenv('PAYMENT_CASHBACK_RULE_ID'));
            $gosend = explode(',', getenv('GOSEND_RULE_ID'));
            if(!empty($cartData['cart_rules'])) {
                $appliedRules = json_decode($cartData['cart_rules'], true);
                if(!empty($appliedRules)) {
                    foreach ($appliedRules as $key => $rule) {
                        if(in_array($rule['rule_id'],$paymentCashback)){
                            $messages[] = "Selamat, Anda berpotensi mendapatkan Promo ".$rule['name'];
                            continue;
                        }elseif(in_array($rule['rule_id'],$gosend)){
                            $messages[] = "Selamat, Anda mendapatkan Free Ongkir GO-SEND*";
                            continue;
                        }     
                    }
                }
            }
            $responseParam['messages'] = $messages;

            return $responseParam;
        }

    }

    public function checkVoucherPromotion($params = array())
    {
        $marketingLib = new Marketing();
        $promotionType = ['shopping_cart'];

        // set default ruleId
        $ruleId = 1383;
        if(getenv('GMS_RULE_ID')) {
            $ruleId = getenv('GMS_RULE_ID');
            $ruleId = explode(',', $ruleId);
        };

        $marketingPromo = $marketingLib->getMarketingPromotion($promotionType);

        // Load shopping cart
        $this->cart_model->initialize($params['cart_id']);
        $cartData = $this->cart_model->getDataArray();

        // Prepare sending back data to consumer
        $responseParam = [
            'is_have_promotion' => false
        ];

        if(empty($marketingPromo)) {
            return $responseParam;
        } else {
            $isHavePromo = false;
            $voucherPromotion = json_decode($marketingPromo['shopping_cart'], true);
            usort($voucherPromotion, function($a, $b) {
                return $b['rule_id'] - $a['rule_id'];
            });
            usort($voucherPromotion, function($a, $b) {
                return $b['priority'] - $a['priority'];
            });

            if(count($voucherPromotion) > 0) {
                foreach($voucherPromotion as $promotion) {
                    if (isset($cartData['customer']['customer_id'])) {
                        $isValidUsage = $marketingLib->maxUsageValidation($promotion, $cartData['customer']['customer_id']);
                        if ($promotion['uses_per_customer'] > 0) {
                            if ($isValidUsage === false) {
                                continue;
                            }
                        }
                    }

                    $is_allow_group = false;
                    if(isset($promotion['customer_group_id']) && $promotion['customer_group_id'] != "") {
                        $allowedGroup = explode(",",$promotion['customer_group_id']);
                        foreach ($cartData['customer']['customer_group'] as $customer_group) {
                            if(in_array($customer_group,$allowedGroup)) {
                                $is_allow_group = true;
                            }
                        }
                    }

                    // If this group not allow to get this promotion, skip it
                    if(!$is_allow_group) {
                        continue;
                    }
                    foreach($promotion['conditions'] as $condition) {
                        // cari yang conditionsnya cart_rules.rule_id = $ruleid
                        if ($condition['attribute'] == 'cart_rules.rule_id' && in_array($condition['value'], $ruleId)) {
                             \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 1071', 'debug');
                            $isMatchCondition = $this->cart_model->marketingConditionCheck($promotion['conditions'],$cartData);
                            if ($isMatchCondition) {
                                $totalQtyitem = 0;
                                $action = $promotion['action'];
                                $discount_amount = !empty($action['discount_amount']) ? $action['discount_amount'] : 0;
                                $amountAttribute = !empty($action['attributes']) ? $action['attributes'] : "subtotal";
                                $discountType = !empty($action['discount_type']) ? $action['discount_type'] : "percent";

                                $conditionAtribute = explode(".",$amountAttribute);
                                $discountAmountItem = 0;
                                if(count($conditionAtribute) > 1) {
                                    // If condition more than one it's mean item attribute
                                    $itemAttribute = $conditionAtribute[1];

                                    $promotion['conditions'] = \Helpers\CartHelper::keepAttributeConditions($promotion['conditions']);
                                    $originAmountAttribute = 0;
                                    if(!empty($promotion['conditions'])) {
                                        $promotion['conditions'] = \Helpers\CartHelper::removeAtributeCondition($promotion['conditions']);
                                        $cashbackSku = array();
                                        foreach ($cartData['items'] as $item) {
                                             \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 1092', 'debug');
                                            $matchCondition = $this->cart_model->marketingConditionCheck($promotion['conditions'],$item);
                                            if($matchCondition) {
                                                $discountAmountItem += $item['discount_amount'];
                                                $originAmountAttribute += $item[$itemAttribute];
                                                $cashbackSku[] = $item['sku'];
                                                $totalQtyitem += $item['qty_ordered'];
                                            }
                                        }
                                    }
                                } else {
                                    $promotion['conditions'] = \Helpers\CartHelper::removeAtributeCondition($promotion['conditions']);
                                    $originAmountAttribute = isset($cartData[$amountAttribute]) ? intval($cartData[$amountAttribute]) : 0;
                                    $cashbackSku = array();
                                    foreach ($cartData['items'] as $item) {
                                         \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 1110', 'debug');
                                        $matchCondition = $this->cart_model->marketingConditionCheck($promotion['conditions'],$item);
                                        if($matchCondition) {
                                            $cashbackSku[] = $item['sku'];
                                        }
                                    }
                                }

                                if($amountAttribute == "grand_total") {
                                    $shippingAmount = intval($cartData['shipping_amount']) - intval($cartData['shipping_discount_amount']);
                                    $originAmount = intval($originAmountAttribute) - $shippingAmount;
                                } else {
                                    $originAmount = $originAmountAttribute;
                                }

                                $voucherAmount = 0;
                                if($discountAmountItem != 0) {
                                    $voucherAmount =  $discountAmountItem;
                                }
                                else if($discountType == 'fix') {
                                    $voucherAmount =  $discount_amount;
                                } else if ($discountType == 'percent') {
                                    $discountAmountGet = round((intval($discount_amount) / 100) * $originAmount);
                                    $voucherAmount =  \Helpers\CartHelper::countDiscountAmountUsed($originAmount,$discountAmountGet);
                                    if(isset($action['max_redemption'])) {
                                        if(!empty((int)$action['max_redemption']) && $voucherAmount > $action['max_redemption']) {
                                            $voucherAmount = intval($action['max_redemption']);
                                        }
                                    }
                                } else if ($discountType == 'fix_multiple') {
                                    $voucherAmount =  $discount_amount * (($totalQtyitem == 0) ? 1 : $totalQtyitem);
                                }

                                if($voucherAmount > 0) {
                                    $isHavePromo = true;
                                }
                            }
                        }
                    }
                }
            }


            if($isHavePromo) {
                $appliedPromotion = [];
                $discount_amount = 0;
                foreach ($this->cart_model->getCurrentDiscount() as $key => $promo) {
                    $appliedPromotion[] = $promo['name'];
                    $discount_amount += (int)$promo['discount_amount'];
                }

                $responseParam['sku'] = array();
                if(isset($cashbackSku)) {
                    $responseParam['sku'] = $cashbackSku;
                }

                $responseParam['is_have_promotion'] = true;
                $responseParam['voucher_amount'] = isset($voucherAmount) ? $voucherAmount : 0;
            } else {
                $responseParam['is_have_promotion'] = false;
            }

            return $responseParam;
        }
    }

    /**
     * Get customer cart_id
     * @param int $customer_id
     * @param string $current_cart_id
     * @param string $company_code
     * @return mixed|string
     */
    public function getCustomerCartId($customer_id = 0, $current_cart_id = "",$company_code = 'ODI', $store_code_new_retail = '',$isUseAA2=false)
    {
        if(empty($customer_id)) {
            return "";
        }

        // $shoppingCartArray = $this->cart_model->getCustomerCartData($customer_id,$company_code);

        // before do this, we must define this $current_cart_id is from old cart or not.
        // determined by length cart_id
        // if the current_cart_id is old cart we must create a new cart with hit endpoint to go-cart

        $params['cart_id'] = $current_cart_id;
        $params['customer']['customer_id'] = (int)$customer_id;
        $params['customer']['customer_is_guest'] = (int)1;
        if (!empty($store_code_new_retail)) {
            $params['store_code_new_retail'] = $store_code_new_retail;
        }
        $resp = $this->mergeCartCustomer($params,$isUseAA2);

        // if(!empty($current_cart_id) && count($shoppingCartArray) >= 1) {
        //     $this->mergeShoppingCartItem($shoppingCartArray,$current_cart_id,$company_code);
        //     $cart_id = $current_cart_id;
        // } else if(count($shoppingCartArray) > 0) {
        //     $cart_id = $shoppingCartArray[0]['cart_id'];

        //     if (count($shoppingCartArray) > 1) {
        //         unset($shoppingCartArray[0]);
        //         foreach ($shoppingCartArray as $cart) {
        //             if($cart['cart_id'] != $current_cart_id) {
        //                 $cart['status'] = "expired";
        //                 $this->cart_model->setCartId($cart['cart_id']);
        //                 $this->cart_model->saveToDb($cart);
        //             }
        //         }
        //     }
        // } else if(!empty($current_cart_id)) {
        //     $params['cart_id'] = $current_cart_id;
        //     $params['customer']['customer_id'] = $customer_id;
        //     $params['do_count_shipping'] = true;
        //     $params['company_code'] = $company_code;
        //     $this->setFromParam($params);

        //     if(empty($this->cart_model->getErrorMessages())) {
        //         $this->checkMarketingPromotion();
        //         $this->checkGiftcartApplied();
        //         $this->saveCart();
        //     }
        // } else {
        //     $this->cart_model->initialize();
        //     $cart_id = $this->cart_model->getCartId();
        //     $params['cart_id'] = $cart_id;
        //     $params['customer']['customer_id'] = $customer_id;
        //     $params['do_count_shipping'] = true;
        //     $params['company_code'] = $company_code;
        //     $this->setFromParam($params);

        //     if(empty($this->cart_model->getErrorMessages())) {
        //         $this->checkMarketingPromotion();
        //         $this->checkGiftcartApplied();
        //         $this->saveCart();
        //     }

        // }

        return $resp;
    }

    public function mergeCartCustomer($params = array(),$isUseAA2=false) {
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setParam($params);
        if ($isUseAA2){
            $apiWrapper->setEndPoint("v2/cart/merge/login");
        }else{
            $apiWrapper->setEndPoint("cart/merge/login");
        }
        $resultData = array();
        
        if($apiWrapper->sendRequest("put")) {
            $apiWrapper->formatResponse();
            // Prevent error not shown when merge cart failed
            if (!empty($apiWrapper->getError())) {
                $resultData['error'] = $apiWrapper->getError();
            }
            $resultData['data'] = $apiWrapper->getData();
            
            \Helpers\LogHelper::log('cart','[SUCCESS] merge ' . json_encode($resultData));
        } else {
            \Helpers\LogHelper::log('cart','[ERROR] when calling cart API ' . json_encode($params));
        }

        return $resultData;
    }

    public function getCustomerMiniCartId($customer_id = 0, $current_cart_id = "",$payment_cart_id = "",$company_code = 'ODI', $store_code_new_retail = '')
    {
        if(empty($customer_id)) {
            return "";
        }

        // $shoppingCartArray = $this->cart_model->getCustomerCartData($customer_id,$company_code);

        // before do this, we must define this $current_cart_id is from old cart or not.
        // determined by length cart_id
        // if the current_cart_id is old cart we must create a new cart with hit endpoint to go-cart

        $params['minicart_id'] = $current_cart_id;
        $params['customer']['customer_id'] = (int)$customer_id;
        if ($customer_id == 0 ) {
            $params['customer']['customer_is_guest'] = (int)1;
        } else {
            $params['customer']['customer_is_guest'] = (int)0;
        }
        $params['payment_cart_id'] = $payment_cart_id;
        if (!empty($store_code_new_retail)) {
            $params['store_code_new_retail'] = $store_code_new_retail;
        }
        $resp = $this->mergeMiniCartCustomer($params);      

        // if(!empty($current_cart_id) && count($shoppingCartArray) >= 1) {
        //     $this->mergeShoppingCartItem($shoppingCartArray,$current_cart_id,$company_code);
        //     $cart_id = $current_cart_id;
        // } else if(count($shoppingCartArray) > 0) {
        //     $cart_id = $shoppingCartArray[0]['cart_id'];

        //     if (count($shoppingCartArray) > 1) {
        //         unset($shoppingCartArray[0]);
        //         foreach ($shoppingCartArray as $cart) {
        //             if($cart['cart_id'] != $current_cart_id) {
        //                 $cart['status'] = "expired";
        //                 $this->cart_model->setCartId($cart['cart_id']);
        //                 $this->cart_model->saveToDb($cart);
        //             }
        //         }
        //     }
        // } else if(!empty($current_cart_id)) {
        //     $params['cart_id'] = $current_cart_id;
        //     $params['customer']['customer_id'] = $customer_id;
        //     $params['do_count_shipping'] = true;
        //     $params['company_code'] = $company_code;
        //     $this->setFromParam($params);

        //     if(empty($this->cart_model->getErrorMessages())) {
        //         $this->checkMarketingPromotion();
        //         $this->checkGiftcartApplied();
        //         $this->saveCart();
        //     }
        // } else {
        //     $this->cart_model->initialize();
        //     $cart_id = $this->cart_model->getCartId();
        //     $params['cart_id'] = $cart_id;
        //     $params['customer']['customer_id'] = $customer_id;
        //     $params['do_count_shipping'] = true;
        //     $params['company_code'] = $company_code;
        //     $this->setFromParam($params);

        //     if(empty($this->cart_model->getErrorMessages())) {
        //         $this->checkMarketingPromotion();
        //         $this->checkGiftcartApplied();
        //         $this->saveCart();
        //     }

        // }

        return $resp;
    }

    public function mergeMiniCartCustomer($params = array()) {
        $apiWrapper = new APIWrapper(getenv('CART_API'));        
        $apiWrapper->setEndPoint("cart/minicart/merge/login");
        $apiWrapper->setParam($params);
        $resultData = array();                
        if($apiWrapper->sendRequest("put")) {
            $apiWrapper->formatResponse();
            // Prevent error not shown when merge minicart failed
            if (!empty($apiWrapper->getError())) {
                $resultData['error'] = $apiWrapper->getError();
            }
            $resultData['data'] = $apiWrapper->getData();            
            \Helpers\LogHelper::log('cart','[SUCCESS] merge ' . json_encode($resultData));
        } else {
            \Helpers\LogHelper::log('cart','[ERROR] when calling cart API ' . json_encode($params));
        }

        return $resultData;
    }

    public function migrateCartCustomer($params = array()) {
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("cart/migrate");
        $resultData = array();
        
        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
            $resultData = $apiWrapper->getData();
            
            \Helpers\LogHelper::log('cart','[SUCCESS] merge ' . json_encode($resultData));
        } else {
            \Helpers\LogHelper::log('cart','[ERROR] when calling cart API ' . json_encode($params));
        }

        return $resultData;
    }

    public function updateCartWithoutMarketing($params = array()) {
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("cart/update");
        $resultData = array();
        
        if($apiWrapper->sendRequest("put")) {
            $apiWrapper->formatResponse();
            $resultData = $apiWrapper->getData();
            
            \Helpers\LogHelper::log('cart','[SUCCESS] update reserved_order_no ' . json_encode($resultData));
        } else {
            \Helpers\LogHelper::log('cart','[ERROR] update reserved_order_no ' . json_encode($params));
        }

        return $resultData;
    }

    public function mergeShoppingCartItem($shopping_cart_array = [],$current_cart_id = "", $company_code = 'ODI')
    {
        $itemList = [];
        $customerInfo = [];
        foreach ($shopping_cart_array as $cart) {
            if (!empty($cart['items'])) {
                foreach ($cart['items'] as $item) {
                    if(!isset($itemList[$item['sku']])) {
                        $itemList[$item['sku']] = $item;
                    }
                }
            }

            if($cart['cart_id'] != $current_cart_id) {
                $cart['status'] = "expired";
                $this->cart_model->setCartId($cart['cart_id']);
                $this->cart_model->saveToDb($cart);
            }

            $customerInfo = $cart['customer'];
        }

        $this->cart_model->setCartId($current_cart_id);
        $cartData = $this->loadCartData();

        if(!empty($cartData['items'])) {
            foreach ($cartData['items'] as $item) {
                if(!isset($itemList[$item['sku']])) {
                    $itemList[$item['sku']] = $item;
                }
            }
        }

        $itemList = array_values($itemList);
        $params['items'] = $itemList;
        $params['customer'] = $customerInfo;
        $params['do_count_shipping'] = true;
        $params['company_code'] = $company_code;

        $this->setFromParam($params);
        \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 1444', 'debug');
        $this->checkMarketingPromotion();
        \Helpers\LogHelper::log('debug_625', '/app/library/Cart.php line 1446', 'debug');
        $this->checkGiftcartApplied();
        $this->saveCart();
    }

    public function cartValidation($params = [])
    {
        if (empty($params['cart_id'])) {
            $this->error_code = 400;
            $this->error_messages['type'] = "global";  
            $this->error_messages['value'] = "";  
            $this->error_messages['message'] = "cart_id is required"; 
            return false;
        }

        // Load shopping cart
        $this->cart_model->initialize($params['cart_id']);

        /** Let's start doing validation */
        $this->cart_model->validateCart();
        
        $this->cart_model->stockAssignationMixSku();
        
        // Validate Gosend
        $this->cart_model->courierValidation();
        
        /** We need to update last status of cart after validation */
        $this->cart_model->saveCart();

        $this->errorChecker();
    }

    public function shippingAssignation($cartData = [], $companyCode = false){

        $pickupCode = [];
        if(!empty($cartData['shipping_address']['kecamatan']['kecamatan_code'])) {
            $kecamatanCode = $cartData['shipping_address']['kecamatan']['kecamatan_code'];

            if(empty($companyCode)){
                $companyCode = 'ODI';
            }

            $cartItemModel = new \Models\CartItem();
            $shippingAssignationList = $cartItemModel->getShippingAssignationList($kecamatanCode, $companyCode);

            foreach ($shippingAssignationList as $assignation) {
                $pickupCode[] = $assignation['pickup_code'];
            }
            $pickupCode = array_unique($pickupCode);
        }
        return $pickupCode;
    }
    public function updateMinicart($param = array())
    {        
        if (empty($param)) {
            return false;
        }        
        try {
            // Load shopping card data
                $mongoDb = new \Library\Mongodb();
                $mongoDb->setCollection(getenv('MINI_CART_COLLECTION'));
                $collection = $mongoDb->getCollection();
                $updateResult = $collection->updateOne(
                    ['minicart_id' => $param['minicart_id']],
                    ['$set' => ['items' => $param['items']]]
                );
            } catch (Exception $e){
                \Helpers\LogHelper::log("mongo", $e->getMessage());
        
                $this->errorCode = 500;
                $this->errorMessages = "update minicart fail";
                return false;
            }

        return true;
    }
    public function getCartByCartId($cartId){
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setEndPoint("/cart/" . $cartId);

        if (!$apiWrapper->sendRequest("get")) {
            $this->error_code = 400;
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = $apiWrapper->getError();
            return false;
        }

        $apiWrapper->formatResponse();
    
        if (!empty($apiWrapper->getError())) {
          $this->error_code = 400;
          $this->error_messages['type'] = "global";
          $this->error_messages['value'] = "";
          $this->error_messages['message'] = $apiWrapper->getError();
          return false;
        }

        return $apiWrapper->getData();
    }

    public function postCartRedeemVoucher($cartId, $voucherCode){
        // Directly assign voucher to existing cart
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setEndPoint("/v2/marketing/voucher/redeem");
        $apiWrapper->setParam(
            array(
                'cart_id' => $cartId,
                'voucher_code' => $voucherCode
            )
        );

        if (!$apiWrapper->sendRequest("post")) {
            $this->errorCode = 400;
            $this->errorMessages = "Error occurs when trying to redeem voucher";
            return false;
        }

        $apiWrapper->formatResponse();

        if (!empty($apiWrapper->getError())) {
            $this->errorCode = 400;
            $this->errorMessages = $apiWrapper->getError();
            return false;
          }

        return $apiWrapper->getData();
    }


    public function updatePayment($param = array())
    {
        
        if (empty($param)) {
            return false;
        }
        // Load shopping card data
        $apiWrapper = new APIWrapper(getenv("CART_API"));
        $apiWrapper->setEndPoint("/cart/update/payment");
        $apiWrapper->setParam($param);
    
        if ($apiWrapper->sendRequest("put")) {    
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to update your shopping cart, please try again";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("api", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }

        return true;
    }

}





