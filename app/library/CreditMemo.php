<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/24/2017
 * Time: 9:26 AM
 */

namespace Library;


use Elasticsearch\Endpoints\Cat\Help;
use Phalcon\Db as Database;
use \Library\SalesOrderTp;
class CreditMemo
{
    /**
     * @var $collection MongoDB Collections
     */
    protected $collection;
    protected $credit_memo_id;
    protected $order_no;
    protected $created_at;
    protected $created_by;
    protected $reorder;
    protected $subtotal = 0;
    protected $shipping_amount = 0;
    protected $customer_penalty = 0;
    protected $refund_as_gift_card = 0;
    protected $refund_cash = 0;
    protected $credit_memo_no;
    protected $voucher_code;
    protected $send_email = "no";
    protected $status = "pending";
    protected $type = "general";
    protected $feedback_finance;
    protected $feedback_cso;
    protected $images;

    /**
     * @var \Library\CreditMemo\Invoices\Collection
     */
    protected $invoices;

    /**
     * @var \Models\SalesOrder
     */
    protected $salesOrder;

    protected $errorCode;
    protected $errorMessages;

    public function __construct()
    {
        $mongoDb = new \Library\Mongodb();
        $mongoDb->setCollection("credit_memo");
        $this->collection = $mongoDb->getCollection();
        $indexes = $this->collection->listIndexes();

        $totalIndex = 0;
        foreach($indexes as $index) {
            $totalIndex++;
        }

        //$indexes = $this->collection->dropIndex('created_at_1');
        if($totalIndex == 1) {
            $this->collection->createIndex(['expired_at' => 1], ["expireAfterSeconds" => 0]); // expired after 30 minute
        }

        $this->errorMessages = array();
    }
    
    /**
     * Get the value of created_by
     */ 
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set the value of created_by
     *
     * @return  self
     */ 
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * Get the value of reorder
     */ 
    public function getReorder()
    {
        return $this->reorder;
    }

    /**
     * Set the value of reorder
     *
     * @return  self
     */ 
    public function setReorder($reorder)
    {
        $this->reorder = $reorder;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreditMemoId()
    {
        return $this->credit_memo_id;
    }

    /**
     * @param mixed $credit_memo_id
     */
    public function setCreditMemoId($credit_memo_id)
    {
        $this->credit_memo_id = $credit_memo_id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $credit_memo_id
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getVoucherCode()
    {
        return $this->voucher_code;
    }

    /**
     * @param mixed $voucher_code
     */
    public function setVoucherId($voucher_code)
    {
        $this->voucher_code = $voucher_code;
    }

    /**
     * @return mixed
     */
    public function getCreditMemoNo()
    {
        return $this->credit_memo_no;
    }

    /**
     * @param mixed $credit_memo_no
     */
    public function setCreditMemoNo($credit_memo_no)
    {
        $this->credit_memo_no = $credit_memo_no;
    }

    /**
     * @return int
     */
    public function getSubTotal()
    {
        return $this->subtotal;
    }

    /**
     * @param int $subtotal
     */
    public function setSubTotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return int
     */
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

    /**
     * @param int $shipping_amount
     */
    public function setShippingAmount($shipping_amount)
    {
        $this->shipping_amount = $shipping_amount;
    }

    /**
     * @return int
     */
    public function getCustomerPenalty()
    {
        return $this->customer_penalty;
    }

    /**
     * @param int $customer_penalty
     */
    public function setCustomerPenalty($customer_penalty)
    {
        $this->customer_penalty = $customer_penalty;
    }

    /**
     * @return int
     */
    public function getRefundAsGiftcard()
    {
        return $this->refund_as_gift_card;
    }

    /**
     * @param int $refund_as_gift_card
     */
    public function setRefundAsGiftcard($refund_as_gift_card)
    {
        $this->refund_as_gift_card = $refund_as_gift_card;
    }

    /**
     * @return int
     */
    public function getRefundCash()
    {
        return $this->refund_cash;
    }

    /**
     * @param int $refund_cash
     */
    public function setRefundCash($refund_cash)
    {
        $this->refund_cash = $refund_cash;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * cek total price, total gift card apa sesuai dengan yang ada, gak main timpa aja
     */
    public function setFromArray($data_array = array())
    {
        $this->customer_penalty = !empty($data_array['customer_penalty']) ? $data_array['customer_penalty'] : 0;

        if(empty($this->order_no )) {
            $this->order_no = !empty($data_array['order_no']) ? $data_array['order_no'] : "";
        }

        if(!empty($data_array['credit_memo_id'])) {
            $this->credit_memo_id = $data_array['credit_memo_id'];
        }

        if(!empty($data_array['send_email'])) {
            $this->send_email = strtolower($data_array['send_email']);
        }

        if(!empty($data_array['type'])) {
            $this->type = $data_array['type'];
        }

        foreach($data_array as $key => $val) {
            if($key == 'invoices')
            {
                $creditMemoInvoice = array();
                foreach($val as $invoiceNo => $invoice) {
                    $creditMemoInvoiceLib = new \Library\CreditMemoInvoice();
                    $invoice['invoice_no'] = $invoiceNo;
                    $creditMemoInvoiceLib->setFromArray($invoice);

                    $this->subtotal += $creditMemoInvoiceLib->getSubTotal();
                    $this->shipping_amount += round($creditMemoInvoiceLib->getShippingAmount());
                    $this->refund_as_gift_card += round($creditMemoInvoiceLib->getGiftCardsAmount());

                    $creditMemoInvoice[] = $creditMemoInvoiceLib;
                }

                $this->invoices = new \Library\CreditMemo\Invoices\Collection($creditMemoInvoice);
            }
        }

        $maxTotalRefund = $this->subtotal + $this->shipping_amount;

        if(isset($data_array['shipping_amount']) && $data_array['shipping_amount'] < $this->shipping_amount) {
            $this->shipping_amount = $data_array['shipping_amount'];
            $maxTotalRefund = $this->subtotal + $this->shipping_amount;
        }

        if(!empty($this->customer_penalty) && $this->customer_penalty > $maxTotalRefund) {
            $this->customer_penalty = 0;

            // Also include error message
            $this->errorCode = "RR100";
            $this->errorMessages = "Total refund too much";
        }

        if(isset($data_array['refund_as_gift_card']) && $data_array['refund_as_gift_card'] > $maxTotalRefund) {
            $this->refund_as_gift_card = 0;

            // Also include error message
            $this->errorCode = "RR100";
            $this->errorMessages = "Total refund too much";
        } else if(isset($data_array['refund_as_gift_card'])) {
            $this->refund_as_gift_card = $data_array['refund_as_gift_card'];
        }

        $this->refund_cash = $maxTotalRefund - $this->refund_as_gift_card - $this->customer_penalty ;

        if(empty($this->credit_memo_no)) {
            $this->credit_memo_no = \Helpers\Transaction::generateCreditMemoNo();
        }

        return true;
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);

        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) == 'object') {
                if($key == 'collection') continue;
                if($key == 'expired_at') continue;
                $view[$key] = $thisArray[$key]->getDataArray(); // get everything
            } else {
                $view[$key] = $val;
            }
        }

        unset($view['errorCode']);
        unset($view['errorMessages']);

        return $view;
    }

    /**
     * prepare credit memo
     *
     * @param array $params
     * @return null
     */
    public function prepareCreditMemo($params = array())
    {
        if(empty($params['credit_memo_id']) && empty($params['order_no'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "order_no is empty";
            return null;
        } else if(!empty($params['credit_memo_id'])) {
            $creditMemoData = $this->loadFromCache(['credit_memo_id' => $params['credit_memo_id']]);
            $currentData = $this->formatRequestArray($creditMemoData);

            $this->status = !empty($params['status'])? $params['status'] : $creditMemoData['status'];
            $this->type = !empty($params['type'])? $params['type'] : $creditMemoData['status'];
            $this->feedback_cso = !empty($params['feedback_cso'])? $params['feedback_cso'] : $creditMemoData['feedback_cso'];
            $this->feedback_finance = !empty($params['feedback_finance'])? $params['feedback_finance'] : $creditMemoData['feedback_finance'];
            $this->images = !empty($params['images'])? $params['images'] : $creditMemoData['images'];
            $this->created_at = !empty($params['created_at'])? $params['created_at'] : $creditMemoData['created_at'];
            $this->credit_memo_no = $creditMemoData['credit_memo_no'];

            if(empty($creditMemoData) && empty($params['items'])) {
                $this->errorMessages = array();
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Item not found";
                return null;
            } else if(empty($creditMemoData)) {
                return null;
            } else {
                $this->setFromArray($currentData);
            }
        } else {
            $this->order_no = $params['order_no'];
            $this->created_at = date("Y-m-d H:i:s");
        }

        if(empty($params['items']) && !empty($currentData['items'])) {
            /*$this->setFromArray($params);
            return;*/
            $params['items'] = $currentData['items'];
        } else if(empty($params['items'])){
            $this->setFromArray($params);
            return;
        }

        $salesOrderLib = new \Library\SalesOrder();
        $salesOrderData = $salesOrderLib->getSalesOrderDetail(['order_no' => $this->order_no]);
        $creditMemoQuota = $salesOrderLib->getQuotaCreditMemo($this->order_no);

        if(empty($salesOrderData)) {
            $this->errorCode = $salesOrderLib->getErrorCode();
            $this->errorMessages = $salesOrderLib->getErrorMessages();
            return null;
        } else if (empty($salesOrderData['invoices'])) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Invoice not found, can not create credit memo.";
            return null;
        }

        // From this point we will recalculate everything
        $this->subtotal = 0;
        $this->refund_as_gift_card = 0;
        $this->refund_cash = 0;
        $this->shipping_amount = 0;

        // just a little mapping so we can easily used it
        $creditMemoItems = array();
        foreach($params['items'] as $item) {
            $creditMemoItems[$item['sales_order_item_id']]['qty_refunded'] = $item['qty'];
        }

        // qty validation
        foreach($salesOrderData['items'] as $soItem) {
            $sales_order_item_id = $soItem['sales_order_item_id'];
            if($soItem["is_free_item"] == 0) {
                if (isset($creditMemoItems[$sales_order_item_id])) {
                    if ($creditMemoItems[$sales_order_item_id]['qty_refunded'] > $creditMemoQuota[$soItem["sales_order_item_id"]]) {
                        $this->errorCode = "RR302";
                        $this->errorMessages = 'Invalid qty to refund item "' . $soItem['name'] . '"';
                        return null;
                    }
                }
            }            
        }

        // we need to count shipping cost for this item, so we need to get weight of item
        /*foreach($salesOrderData['items'] as $soItem) {
            $sales_order_item_id = $soItem['sales_order_item_id'];
            if(isset($creditMemoItems[$sales_order_item_id])) {
                $itemWeight = \Helpers\CartHelper::countWeight([$soItem]);
                $creditMemoItems[$sales_order_item_id]['weight'] = ($itemWeight / $soItem['qty_ordered']);
                $creditMemoItems[$sales_order_item_id]['name'] = $soItem['name'];
            }
        }*/

        // Map items to invoice and put some necessary data
        $creditMemoInvoices = array();
        $isInvoiceFullRefund = 0;
        foreach($salesOrderData['invoices'] as $invoice) {
            $invoiceNo = $invoice['invoice_no'];
            $creditMemoInvoiceItems = array();

            // STD-174 -- logic get existing crm no longer required, get available crm quota from go-order-v2
            // // Get existing credit memo item
            // $creditMemoItemsModel = new \Models\SalesCreditMemoItem();
            // $creditMemoItemsArray = $creditMemoItemsModel->find("invoice_no = '" . $invoiceNo . "'")->toArray();
            // $existingCreditMemoItems = array();
            // if(!empty($creditMemoItemsArray)) {
            //     foreach($creditMemoItemsArray as $existingItem) {
            //         $existingCreditMemoItems[$existingItem['sales_order_item_id']] = $existingItem['qty_refunded'];
            //     }
            // }

            // Count total item that do full refund in this invoice
            $isItemFullRefund = 0;
            //$this->refund_as_gift_card = 0;
            foreach($invoice['items'] as $item) {
                $sales_order_item_id = $item['sales_order_item_id'];
                if(isset($creditMemoItems[$sales_order_item_id])) {
                    if((int)$creditMemoItems[$sales_order_item_id]["qty_refunded"] > 0){
                        $totalRefunded = $creditMemoItems[$sales_order_item_id]['qty_refunded'];
    
                        if($totalRefunded > $creditMemoQuota[$sales_order_item_id]) {
                            $this->errorCode = "RR100";
                            $this->errorMessages[] = "Qty for refund with sku " . $item['sku'] ." not match with qty ordered";
                            continue;
                        } else if ($creditMemoItems[$sales_order_item_id]['qty_refunded'] == $creditMemoQuota[$sales_order_item_id]) {
                            $isItemFullRefund++;
                        }
    
                        // STD-174 -- logic below seems unused
                        // Prorate discount amount, only counting prorate discount for not free item
                        // $itemDiscount = 0;
                        // if(!empty(intval($invoice['discount_amount'])) && $item['is_free_item'] == 0) {
                        //     // Discount per item
                        //     $itemRowTotal = (int)$item['row_total'];
                        //     $itemDiscount = round(($itemRowTotal / ($invoice['subtotal'] * $item['qty_ordered'] )) * $invoice['discount_amount']);
                        // }
    
                        // Prorate shipping discount amount
                        /*$shippingDiscountAmount = 0;
                        if(!empty($invoice['shipping_discount_amount'])) {
                            $shippingAmount = $item['shipping_amount'] - $item['shipping_discount_amount']; // Real shipping amount
                            $shippingDiscountAmount = \Helpers\CartHelper::countDiscountAmountUsed($shippingAmount, $invoice['shipping_discount_amount']);
                        }*/
    
                        $shippingAmount = ($item['shipping_amount'] - $item['shipping_discount_amount']);
    
                        $creditMemoItems[$sales_order_item_id]['sales_order_item_id'] = $sales_order_item_id;
                        $creditMemoItems[$sales_order_item_id]['sku'] = $item['sku'];
                        $creditMemoItems[$sales_order_item_id]['store_code'] = $item['store_code'];
                        $creditMemoItems[$sales_order_item_id]['name'] = $item['name'];
                        $creditMemoItems[$sales_order_item_id]['qty_invoiced'] = $item['qty_ordered'];
                        $creditMemoItems[$sales_order_item_id]['price'] = ($item['subtotal']);
                        $creditMemoItems[$sales_order_item_id]['gift_cards_amount'] = $item['gift_cards_amount'];
                        $creditMemoItems[$sales_order_item_id]['shipping_amount'] = ($shippingAmount < 0) ? 0 : $shippingAmount ;
    
                        $creditMemoInvoiceItems[] = $creditMemoItems[$sales_order_item_id];
                    }
                    
                }
            }

            if(!empty($creditMemoInvoiceItems)) {
                $creditMemoInvoices[$invoiceNo]['total_weight'] = empty($invoice['shipment']) ? 0 : (int)$invoice['shipment']['total_weight'];
                $creditMemoInvoices[$invoiceNo]['items'] = $creditMemoInvoiceItems;

                // check item that already have status_fulfillment refunded
                $isItemAlreadyRefunded = 0;
                foreach($invoice['items'] as $checkItem) {
                    if ($checkItem['status_fulfillment'] == 'refunded') {
                        $isItemAlreadyRefunded++;
                    }
                }

                // if all item is refunded than update invoice status to full refund
                if(($isItemFullRefund + $isItemAlreadyRefunded) >= count($invoice['items'])) {
                    $creditMemoInvoices[$invoiceNo]['refund_status'] = "full_refund";
                    $isInvoiceFullRefund++;
                } else {
                    $creditMemoInvoices[$invoiceNo]['refund_status'] = "partial_refund";
                }
            }
        }

        $this->salesOrder = new \Models\SalesOrder();
        $this->salesOrder->setCustomerId($salesOrderData['customer']['customer_id']);
        $this->salesOrder->assign($salesOrderData);
        $this->salesOrder->setCustomerId($salesOrderData['customer']['customer_id']);

        if($isInvoiceFullRefund == count($salesOrderData['invoices'])) {
            $this->salesOrder->setStatus("complete");
        }

        // Distribute shipping amount
        /* comment because distributing shipping_amount already implement in cart
         * foreach($creditMemoInvoices as $keyInv => $invoice) {
            if($invoice['shipping_amount'] > 0) {
                $shipping_per_kg = $invoice['shipping_amount'] / $invoice['total_weight'];
                foreach($invoice['items'] as $key => $val) {
                    $itemShippingAmount = $val['weight'] * $shipping_per_kg;
                    $invoice['items'][$key]['shipping_amount'] = $itemShippingAmount;
                }
                unset($invoice['shipping_amount']);
                $creditMemoInvoices[$keyInv] = $invoice;
            }
        }*/

        $creditMemo = [
            "order_no" => $this->order_no,
            "invoices" => $creditMemoInvoices,
            "customer_penalty" => empty($params['customer_penalty']) ? 0 : $params['customer_penalty']
        ];

        if(isset($params['refund_as_gift_card'])) {
            $creditMemo['refund_as_gift_card'] = intval($params['refund_as_gift_card']);
        }

        if(isset($params['shipping_amount'])) {
            $creditMemo['shipping_amount'] = intval($params['shipping_amount']);
        }

        if(isset($params['refund_cash'])) {
            $creditMemo['refund_cash'] = intval($params['refund_cash']);
        }

        if(!empty($params['credit_memo_id'])) {
           $creditMemo['credit_memo_id'] = $params['credit_memo_id'];
        }

        if(!empty($params['status'])) {
            $creditMemo['status'] = $params['status'];
        }

        if(!empty($params['type'])) {
            $creditMemo['type'] = $params['type'];
        }

        $this->errorMessages = [];
        $this->errorCode = "";

        $this->setFromArray($creditMemo);

        if (!empty($params['credit_memo_no'])) {
            $this->credit_memo_no = $params['credit_memo_no'];
        }
    }

    public function saveToCache()
    {
        $creditMemoData = $this->getDataArray();

        if(empty($this->credit_memo_id)) {
            $creditMemoData['expired_at'] = new \MongoDB\BSON\UTCDateTime( (time() + (30*60)) * 1000 );
            $insertOneResult = $this->collection->insertOne($creditMemoData);
            $lastInsertId = (array)$insertOneResult->getInsertedId();

            if($insertOneResult->getInsertedCount() > 0)
            {
                $lastInsertId = (array)$insertOneResult->getInsertedId();
                $this->credit_memo_id = $lastInsertId['oid'];

                $id = new \MongoDB\BSON\ObjectID($this->credit_memo_id);
                $creditMemoData['credit_memo_id'] = $lastInsertId['oid'];
                $result = $this->collection->findOneAndReplace(['_id' => $id],$creditMemoData);
            } else {
                $this->errorCode = "RR201";
                $this->errorMessages[] = "Failed to create shopping cart";
            }
        } else {
            $id = new \MongoDB\BSON\ObjectID($this->credit_memo_id);
            $result = $this->collection->findOneAndReplace(['_id' => $id],$creditMemoData);
        }
    }

    public function countCreditMemoForOrderNo($order_no) {
        if (empty($order_no)) {
            return 0;
        }

        $brandModel = new \Models\SalesCreditMemo();
        $sql = "SELECT count(credit_memo_id) as count FROM sales_credit_memo scm WHERE order_no = '".$order_no."'";
        $result = $brandModel->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $totalRow = $result->fetchAll()[0]['count'];

        return $totalRow + 1;
    }

    public function loadFromCache($params = [])
    {
        if(empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "credit_memo_id is required";
            return null;
        }

        // load data from mongo db
        $searchParam = [];
        if(isset($params['credit_memo_id'])) {
            if(preg_match('/^[0-9a-f]{24}$/i', $params['credit_memo_id']) === 1) {
                $id = new \MongoDB\BSON\ObjectID($params['credit_memo_id']);
                $searchParam = ['_id' => $id];
            }            
        }

        $searchParam = array_merge($searchParam,$params);

        $result = $this->collection->findOne($searchParam);

        if($result) {
            unset($result['_id']);
            $result['credit_memo_id'] = $params['credit_memo_id'];
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages[] = "credit_memo_id not found or already expired";
            return null;
        }

        return $result;
    }

    /**
     * format json saved credit memo to request json data for validation purpose
     *
     * @param array $data_array
     * @return null
     *
     */
    public function formatRequestArray($data_array = array())
    {
        if(empty($data_array['order_no']))
        {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "order_no not found";
            return null;
        }

        if(!empty($data_array['invoices'])) {
            foreach ($data_array['invoices'] as $invoice) {
                $requestArray['invoices'][$invoice['invoice_no']] = $invoice;
            }

            $requestArray['items'] = array();
            foreach($data_array['invoices'] as $invoices) {
                foreach($invoices['credit_memo_items'] as $creditMemoItem) {
                    $requestArray['items'][] = ['sales_order_item_id' => $creditMemoItem['sales_order_item_id'],'qty' => $creditMemoItem['qty_refunded']];
                }
            }
        }

        $requestArray['salesOrder'] = $data_array['salesOrder'];
        $requestArray['order_no'] = $data_array['order_no'];
        if(isset($data_array['subtotal'])) {
            $requestArray['subtotal'] = $data_array['subtotal'];
        }

        if(isset($data_array['shipping_amount'])) {
            $requestArray['shipping_amount'] = $data_array['shipping_amount'];
        }

        if(isset($data_array['customer_penalty'])) {
            $requestArray['customer_penalty'] = $data_array['customer_penalty'];
        }

        if(isset($data_array['refund_as_gift_card'])) {
            $requestArray['refund_as_gift_card'] = $data_array['refund_as_gift_card'];
        }

        if(isset($data_array['refund_cash'])) {
            $requestArray['refund_cash'] = $data_array['refund_cash'];
        }

        if(isset($data_array['type'])) {
            $requestArray['type'] = $data_array['type'];
        }

        $requestArray['credit_memo_no'] = $data_array['credit_memo_no'];

        return $requestArray;

    }

    public function saveData()
    {
        if(empty($this->errorCode)) {
            $creditMemoData = $this->getDataArray();
            if(empty($creditMemoData['invoices'])) {
                $this->errorCode = 404;
                $this->errorMessages[] = "Invoice not found";
                return false;
            }

            // Check Invoice Status
            $invoiceStatus = array();
            $invoiceModel = new \Models\SalesInvoice();
            $invoiceCreditMemo = array();
            foreach($creditMemoData['invoices'] as $invoiceMemo){
                $invoiceData = $invoiceModel->findFirst(
                    array(
                        'conditions' => ' invoice_no = "'.$invoiceMemo['invoice_no'].'" ',
                        'columns' => 'invoice_status,store_code,invoice_id,sales_order_id,pickup_voucher'
                    )
                );

                if($invoiceData){
                    $invoiceStatus[$invoiceMemo['invoice_no']]['invoice_status'] = $invoiceData->toArray()['invoice_status'];
                    // Send Slack Notification if the refunded order is 1000DC order
                    if(strpos($invoiceData->toArray()['store_code'], 'DC') !== false){
                        $salesOrderTpLib = new \Library\SalesOrderTp();
                        $totalRow = $salesOrderTpLib->CheckSalesOrderTpForSlackNotif($invoiceData->toArray()['invoice_id']);
                        if ($totalRow > 0){
                            // Get order no for slack message
                            $salesOrderModel = new \Models\SalesOrder();
                            $orderId = $invoiceData->toArray()['sales_order_id'];
                            $order = $salesOrderModel->findFirst(
                                array(
                                    'conditions' => ' sales_order_id = '.$orderId.' ',
                                    'columns' => 'order_no'
                                )
                            );        
                            $orderNo = $order->toArray()['order_no'];

                            // Get sku for slack message
                            $messageSku = '';
                            foreach ($invoiceMemo['credit_memo_items'] as $sku){
                                $messageSku = $messageSku.$sku['sku'].",";
                            }

                            // Remove the trailing ","
                            $messageSku = substr($messageSku,0,strlen($messageSku)-1);
                            $salesOrderTpLib->createSlackNotifCreditMemo($invoiceMemo['invoice_no'],$orderNo,$messageSku);
                        }
                    }
                    $invoiceCreditMemo[$invoiceMemo['invoice_no']] = $invoiceData;
                }
            }

            unset($creditMemoData['credit_memo_id']);
            $creditMemoModel = new \Models\SalesCreditMemo();
            $creditMemoModel->setFromArray($creditMemoData);

            try {
                $creditMemoModel->saveData();
            } catch (\Exception $e) {
                throw new \Library\HTTPException(
                    "Save credit memo failed, please contact ruparupa tech support",
                    500,
                    array(
                        'msgTitle' => 'Internal Server Error',
                        'dev' => $e->getMessage()
                    )
                );
            }

            // Update sales order status
            $this->salesOrder->saveData("credit_memo");

            //revert stock and update shipment status
            $salesOrderModel = new \Models\SalesOrder();
            $orderItemIDRefund = array();
            $invoiceItemModel = new \Models\SalesInvoiceItem();
            foreach($creditMemoData['invoices'] as $invoice){
                $process = 'shipment';

                $sql = "SELECT COUNT(sales_log_id) as count FROM sales_log WHERE invoice_id = (SELECT si.invoice_id FROM sales_invoice si WHERE si.invoice_no = '".$invoice['invoice_no']."') AND affected_field = 'sales_invoice.invoice_status' AND value = 'stand_by';";
                $result = $creditMemoModel->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $totalRow = $result->fetchAll()[0]['count'];
                $totalRow = intval($totalRow);

                
                $status = $invoiceStatus[$invoice['invoice_no']]['invoice_status'];
                if ($status != '') {
                    $process = 'complete';
                }

                if ($status == 'new' || $status == 'processing'){
                    $process = 'shipment';

                    if ($totalRow > 0){
                        $process = 'complete';
                    }
                }

                $deductStockShopedparamsItems = array();
                $productStockLib = new \Library\ProductStock();

                $revertParams = array();

                foreach($invoice['credit_memo_items'] as $creditMemoItem){
                    // check is the item is 1000DC or not
                    if($creditMemoItem['qty_refunded'] > 0){
                        $orderItemIDRefund[] = $creditMemoItem['sales_order_item_id'];
                        $revertParams[] = array(
                            'sales_order_item_id' => (int)$creditMemoItem['sales_order_item_id'],
                            'quantity' => (int)$creditMemoItem['qty_refunded']
                        );

                        //check MySQL Database is the order TP is process
                        $totalRow = 0;

                        $salesOrderTpModel = new \Models\SalesOrderTp();
                        $sql = "SELECT store_code FROM sales_order_tp WHERE sku = '".$creditMemoItem['sku']."' AND odi = '".$creditMemoData['order_no']."'";
                        $result = $salesOrderTpModel->getDi()->getShared('dbMaster')->query($sql);

                        $result->setFetchMode(
                            Database::FETCH_ASSOC
                        );

                        $rowStoreCode = "";
                        $results = $result->fetchAll();
                        if (count($results) > 0) {
                            $rowStoreCode = $results[0]['store_code'];
                        }

                        $store_code = $creditMemoItem['store_code'];
                        if($rowStoreCode != ""){
                            $store_code = "1000DC".$rowStoreCode;
                        }
                        

                        if (substr($store_code,0,6) == '1000DC') {
                            if (substr($creditMemoData['order_no'], 0, 4) == "ODIT" || substr($creditMemoData['order_no'], 0, 4) == "ODIS" || substr($creditMemoData['order_no'], 0, 4) == "ODIK"){
                                array_push($deductStockShopedparamsItems, [
                                    "sku" => $creditMemoItem['sku'],
                                    "store_code" => $store_code,
                                    "increment_quantity" => (int)$creditMemoItem['qty_refunded']
                                ]);
                            }
                        }
                    }
                }

                $channel = '';
                $shopID = 0;
                if (!empty($deductStockShopedparamsItems)){
                    $salesOrderVendorModel = new \Models\SalesOrderVendor();
                    $salesOrderVendor = $salesOrderVendorModel->findFirst('order_no = "' . $this->order_no . '"');
                    $shopID = intval($salesOrderVendor->toArray()['shop_id']);
                    
                    if (substr($creditMemoData['order_no'], 0, 4) == "ODIT"){
                        $channel = 'tokopedia';
                    } elseif (substr($creditMemoData['order_no'], 0, 4) == "ODIS"){
                        $channel = 'shopee';
                    } elseif (substr($creditMemoData['order_no'], 0, 4) == "ODIK"){
                        $channel = 'tiktok';
                    }
                }
                
                $productStockLib->revertStockOrderV2($process, array(), $revertParams, $channel, $shopID);

                // update shipment when invoice status is full_refund
                if($invoice['refund_status'] == 'full_refund'){
                    $sql = "UPDATE sales_shipment ss
                        INNER JOIN sales_invoice si ON si.invoice_id = ss.invoice_id 
                        SET ss.shipment_status = 'canceled'
                        WHERE si.invoice_no = '" . $invoice['invoice_no'] . "'";
                    $salesOrderModel->useWriteConnection();
                    $salesOrderModel->getDi()->getShared($salesOrderModel->getConnection())->query($sql);
                }
            }            

            // Delete from cache? used for resend journal
            //$id = new \MongoDB\BSON\ObjectID($this->credit_memo_id);
            //$this->collection->deleteOne(['_id' => $id]);

            $customerId = $this->salesOrder->SalesCustomer->getCustomerId();

            // Generate Gift card
            $creditMemoVoucher = '';
            if($this->refund_as_gift_card > 0) {
                $parameters = [
                    "rule_id" => getenv('REFUND_SALES_RULE_' . $this->salesOrder->getCompanyCode()),
                    'company_code' => $this->salesOrder->getCompanyCode(),
                    "type" => 2,
                    "created_by" => $this->created_by,
                    "expiration_date" => date("Y-m-d H:i:s", strtotime("+6 month", time())),
                    "qty" => 1,
                    "voucher_amount" => $this->refund_as_gift_card,
                    "length" => 10,
                    "format" => "alphanum",
                    "prefix" => "CM"
                ];

                $marketingLib = new \Library\Marketing();
                $marketingLib->generateVoucher($parameters);
                $marketingLib->validateGeneratedVoucher($parameters);
                $voucherList = $marketingLib->getVoucherList();

                if(count($voucherList) > 0) {
                    $creditMemoVoucher = $voucherList[0]; // Just get the first one
                    $parameters['voucher_code'] = $creditMemoVoucher;
                    $parameters['customer_id'] = $customerId; // Add customer ID related to this customer
                    $salesRuleVoucherModel = new \Models\SalesruleVoucher();
                    $salesRuleVoucherModel->setFromArray($parameters);
                    $salesRuleVoucherModel->saveData("voucher");
                    $voucherId = $salesRuleVoucherModel->getVoucherId();

                    $creditMemoModel->setVoucherId($voucherId);
                    $creditMemoModel->setSkipAttributeOnUpdate(['created_at']);
                    $creditMemoModel->update();
                    $this->voucher_code = $creditMemoVoucher;
                }
            }

            // Sending refund report to SAP
            $journalSAP = new \Library\JournalSAP(null,$this->salesOrder);
            $headerStatus = $journalSAP->prepareHeader();
            $journalSAP->setBonnummer($creditMemoModel->getCreditMemoNo());
            if($headerStatus) {
                // replace 3603 with 3609
                $creditMemoData['voucher_code'] = $creditMemoVoucher;
                if($this->reorder == 'no'){
                    foreach($creditMemoData['invoices'] as $key => $rowInvoice){
                        if(strpos($invoiceCreditMemo[$rowInvoice['invoice_no']]['store_code'], 'DC') !== false){ // DC
                            $storeCode = str_replace('DC','',$invoiceCreditMemo[$rowInvoice['invoice_no']]['store_code']);
                            
                            if(substr($storeCode, 0, 4) == "1000") {
                                $storeCode = str_replace('1000','',$storeCode);
                            }
                                                       
                            $creditMemoData['invoices'][$key]['assignment_customer_deposit'] = $storeCode.$creditMemoData['order_no'];
                            $creditMemoData['assignment_customer_deposit'] = $storeCode.$creditMemoData['order_no'];
                        }else{ // Store
                            $storeCode = $invoiceCreditMemo[$rowInvoice['invoice_no']]['store_code'];
                            $pickupVoucher = $invoiceCreditMemo[$rowInvoice['invoice_no']]['pickup_voucher'];
                            $creditMemoData['invoices'][$key]['assignment_customer_deposit'] = $storeCode.$pickupVoucher;
                            $creditMemoData['assignment_customer_deposit'] = $storeCode.$pickupVoucher;
                        }        
                    }

                    if($this->salesOrder->getOrderType()=='B2B'){
                        if($this->salesOrder->getDevice() == 'vendor'){
                            $status = $journalSAP->prepareRefundVendorJournal($creditMemoData);
                            if($status) {
                                $journalSAP->createJournal("refund_vendor");
                            }
                        }else{
                            $status = $journalSAP->prepareRefundJournal($creditMemoData);
                            if($status) {
                                $journalSAP->createJournalB2B("credit_memo");
                            }
                        }
                    }elseif($this->salesOrder->getOrderType()=='b2b_informa' || $this->salesOrder->getOrderType() == 'b2b_ace'){
                        $paymentType = $this->salesOrder->SalesOrderPayment->getType();
                        if($paymentType == "term_of_payment"){
                            // Only Serve case 1 and 2
                            // case 1
                            // 1 no order | 2 invoice odi ( H001 & H014 ) | 1 invoice sap 
                            // sales ? 2 idoc per site invoice odi 
                            // incoming ? 2 idoc per site invoice odi 
                            // credit memo ? 1 idoc untuk all site ( item 10 akan banyak )

                            // case 2
                            // 1 no order | 2 invoice odi ( H001 & H014 ) | 2 invoice sap 
                            // sales ? 2 idoc per site invoice odi 
                            // incoming ? 2 idoc per site invoice odi 
                            // credit memo ? 1 idoc untuk all site ( item 10 akan banyak )
                            foreach($creditMemoData["invoices"] as $key => $rowsCreditMemoInvoices){
                                $creditMemoData['invoices'][$key]["invoice_sap_no"] = "";
                                $salesInvoiceSapModel = new \Models\SalesInvoiceSap();
                                $sql = "SELECT 
                                            invoice_sap_no 
                                        FROM sales_invoice_sap sip 
                                        LEFT JOIN sales_invoice si on si.invoice_id = sip.invoice_id
                                        WHERE si.invoice_no = '".$rowsCreditMemoInvoices['invoice_no']."' limit 1";
                                $result = $salesInvoiceSapModel->getDi()->getShared('dbMaster')->query($sql);
                                $result->setFetchMode(Database::FETCH_ASSOC);
                                $resultInvoiceSap = $result->fetch();
                                if (count($resultInvoiceSap) > 0) {
                                    $creditMemoData['invoices'][$key]["invoice_sap_no"] = $resultInvoiceSap['invoice_sap_no'];
                                }
                                $status = $journalSAP->prepareRefundB2BTopJournal($creditMemoData);
                                if($status) {
                                    $journalSAP->createJournal("credit_memo_b2b_top");
                                }
                            }
                            
                        }else{
                            $status = $journalSAP->prepareRefundJournal($creditMemoData);
                            if($status) {
                                $journalSAP->createJournal("credit_memo");
                            }    
                        }
                        
                    }else{
                        $status = $journalSAP->prepareRefundJournal($creditMemoData);
                        if($status) {
                            $journalSAP->createJournal("credit_memo");
                        }
                    }
                }else{
                    $returnFunc = $journalSAP->prepareReorderJournal($creditMemoData);
                    if ($returnFunc['status']) {
                        $journalSAP->createJournal("reorder_cmr_3609");
                    }

                    if ($returnFunc['usedGiftGC'] > 0) {
                        $status = $journalSAP->prepareVoucherJournal($creditMemoData, $returnFunc['usedGiftGC']);
                        if ($status) {
                            $journalSAP->createJournal("vgift_cmr_3604");
                        }
                    }
                }
                // Note by Anggy (13 januari 2020) : voucher marketing dan registrasi digabung aja.
                // if ($returnFunc['usedRegisGC'] > 0) {
                //     $status = $journalSAP->prepareVoucherJournal($creditMemoData, $returnFunc['usedRegisGC']);
                //     if ($status) {
                //         $journalSAP->createJournal("vregis_cmr_3604");
                //     }
                // }
                
            }

            if (count($orderItemIDRefund) > 0) {
                foreach($orderItemIDRefund as $valOrderItemID) {
                    $result = $invoiceItemModel->updateStatusFulfillmentInvoiceItem($valOrderItemID);
                    if(!$result){
                        \Helpers\LogHelper::log("reorder", "failed update status fulfillment to refund for sales_order_item_id: $valOrderItemID");
                    }
                }
            }
            
            if($this->send_email == 'yes') {
                // Sending email
                $email = new \Library\Email();
                $email->setEmailSender();
                $email->setName($this->salesOrder->SalesCustomer->getCustomerLastname(),$this->salesOrder->SalesCustomer->getCustomerFirstname());
                $email->setEmailReceiver($this->salesOrder->SalesCustomer->getCustomerEmail());
                $email->setEmailTag("credit_memo");

                $templateID = \Helpers\GeneralHelper::getTemplateId("credit_memo_voucher", $this->salesOrder->getCompanyCode());
                if($this->refund_as_gift_card > 0) {
                    $voucherCode = $marketingLib->getVoucherList();
                    $voucherExpired = date('j F Y', strtotime($parameters['expiration_date']));

                    $email->setTemplateId($templateID);
                    $email->setCompanyCode($this->salesOrder->getCompanyCode());
                    $email->setVoucherCode($voucherCode[0]);
                    $email->setVoucherExpired($voucherExpired);
                    $email->setEmailTag("cust_cancel_order_no_stock_gift_card");
                } else {
                    $email->setEmailTag("cust_cancel_order_no_stock_cash");
                    $email->setTemplateId($templateID);
                }

                $email->buildCreditMemoParam($creditMemoData);
                $email->send();
            }
        }
    }

    public function getCreditMemo($param){
        if(empty($param['order_no']))
        {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "order_no not found";
            return null;
        }

        // get credit memo list
        $creditMemo = new \Models\SalesCreditMemo();
        if(!empty($param['credit_memo_id'])){
            $creditMemoDatas = $creditMemo->find("credit_memo_id = '" . $param['credit_memo_id'] . "'")->toArray();
        }else{
            $creditMemoDatas = $creditMemo->find("order_no = '" . $param['order_no'] . "'")->toArray();
        }

        $creditMemoItems =  new \Models\SalesCreditMemoItem();
        $creditMemos = array();
        $i = 0 ;
        foreach($creditMemoDatas as $creditMemo){
            $items = $creditMemoItems->find("credit_memo_id = '".$creditMemo['credit_memo_id']."'")->toArray();
            $creditMemos[$i] = $creditMemo+array("items"=>$items);
            $i++;
        }

        return $creditMemos;
    }

    public function getCreditMemoList($param){
        // get credit memo list
        $creditMemoOrder = new \Models\SalesOrder();
        $creditMemoOrderDatas = $creditMemoOrder->find()->toArray();

        $creditMemoAddress = new \Models\SalesOrderAddress();
        $creditMemoAddressDatas = $creditMemoAddress->find()->toArray();

        $creditMemo = new \Models\SalesCreditMemo();
        $creditMemoDatas = $creditMemo->find()->toArray();

        $creditMemoItems =  new \Models\SalesCreditMemoItem();
        $creditMemos = array();
        $orderData = array();
        $i = 0 ;
        foreach($creditMemoDatas as $creditMemo){
            //getOrderData
            foreach($creditMemoOrderDatas as $creditMemoOrderData){
                if($creditMemoOrderData['order_no'] == $creditMemo['order_no']){
                    $orderData['order'] = $creditMemoOrderData;
                    break;
                }
            }

            foreach($creditMemoAddressDatas as $creditMemoAddressData){
                if($creditMemoAddressData['sales_order_id'] == $orderData['order']['sales_order_id']){
                    $addressData[$creditMemoAddressData['address_type']] = $creditMemoAddressData;
                    break;
                }
            }

            $items = $creditMemoItems->find("credit_memo_id = '".$creditMemo['credit_memo_id']."'")->toArray();
            $creditMemos[$i] = $creditMemo+$orderData+$addressData+array("items"=>$items);
            $i++;
        }

        return $creditMemos;
    }

    public function getCreditMemoListAll($params){

        $params['type'] = !empty($params['type'])? $params['type'] : "";
        $params['status'] = !empty($params['status'])? strtolower($params['status']): "";
        $resultData = array();
        $creditMemoFromDb['recordsTotal'] = 0;
        $dataFromMongo['recordsTotal'] = 0;
        $dataFromMongo['list'] = array();

        if (isset($params['kredivo_id_keyword'])) {
              $salesOrderModel = new \Models\SalesOrder();
              $params['order_no_keyword'] = $salesOrderModel->getOrderNoByKredivoId($params['kredivo_id_keyword']);
        }

        if($params['status'] != "approved") {
            $dataFromMongo = $this->getCreditMemoMongoList($params);
            $resultData = $dataFromMongo['list'];
        }

        if(empty($params['status']) || $params['status'] == "all" || $params['status'] == "approved") {
            $salesCreditMemo = new \Models\SalesCreditMemo();
            $search_param['type'] = !empty($search_param['type'])? $search_param['type'] : "";

            if($params['type'] != "count_only" && isset($params['limit']) && isset($params['offset'])) {
                $params['limit'] = $params['limit'] - count($dataFromMongo['list']);
                $offsetCeil = 0;
                if($params['limit']>0) {
                    $offsetCeil = ceil($dataFromMongo['recordsTotal'] / $params['limit']) - 1;
                }

                $offsetCeil = ($offsetCeil<0)? 0 : $offsetCeil;
                $params['offset'] = $params['offset'] - $offsetCeil;
            }

            $creditMemoFromDb = $salesCreditMemo->getCreditMemoList($params);
            if($params['type'] != "count_only" && isset($params['limit'])) {
                if (count($dataFromMongo['list']) < $params['limit']) {
                    if (!empty($creditMemoFromDb['list'])) {
                        foreach ($creditMemoFromDb['list'] as $key => $fetchData) {
                            $fetchData['status'] = !empty($fetchData['status']) ? $fetchData['status'] : "approved";
                            $resultData[] = $fetchData;
                        }
                    }
                }
            }
        }

        $creditMemoFromDb['recordsTotal'] = !empty($creditMemoFromDb['recordsTotal'])? $creditMemoFromDb['recordsTotal'] : 0;
        $dataFromMongo['recordsTotal'] = !empty($dataFromMongo['recordsTotal'])? $dataFromMongo['recordsTotal'] : 0;
        $creditMemoData['recordsTotal'] = $creditMemoFromDb['recordsTotal']+$dataFromMongo['recordsTotal'];
        $creditMemoData['list'] = $resultData;
        return $creditMemoData;
    }

    public function getCreditMemoMongoList($search_param = null){

        if(isset($search_param['master_app_id'])) unset($search_param['master_app_id']);
        if(empty($search_param['type'])) unset($search_param['type']);
        if(empty($search_param['status'])) unset($search_param['status']);

        $cmrData = array();

        /*
         * Set pagination data
         */
        $pagination = array();
        if (isset($search_param['limit'])) {
            $pagination['limit'] = (int)$search_param['limit'];
            unset($search_param['limit']);
        }

        if (isset($search_param['offset'])) {
            $pagination['skip'] = (int)$search_param['offset'];
            unset($search_param['offset']);
        }

        $pagination['sort'] = array('created_at' => -1);

        /*
         * default filtering data
         */
        $searchText = array();
        if (isset($search_param['cmr_no_keyword'])) {
            $searchText['credit_memo_no'] = $search_param['cmr_no_keyword'];
            unset($search_param['cmr_no_keyword']);
        }

        if (isset($search_param['order_no_keyword'])) {
            if (is_array($search_param['order_no_keyword'])) {
                $searchText['order_no'] = array(
                    '$in' => $search_param['order_no_keyword']
                );
            } else {
                $searchText['order_no'] = $search_param['order_no_keyword'];
            }
            unset($search_param['order_no_keyword']);
        }

        if (isset($search_param['order_date_keyword'])) {
            $searchText['salesOrder.created_at'] = $search_param['order_date_keyword'];
            unset($search_param['order_date_keyword']);
        }

        if (empty($search_param) || $search_param['status'] == 'all' ) {
            unset($search_param['status']);
            $search_param = array(
                '$or' => array(
                    array("status" => "pending"),
                    array("status" => "waiting_feedback_cso"),
                    array("status" => "waiting_feedback_finance"),
                    array("status" => "pending approve"),
                    array("status" => "reject")
                )
            );
        }elseif($search_param['status'] == 'pending'){
            unset($search_param['status']);
            $search_param = array(
                '$or' => array(
                    array("status" => "pending"),
                    array("status" => "waiting_feedback_cso"),
                    array("status" => "waiting_feedback_finance"),
                    array("status" => "pending approve")
                )
            );
        }

        $search_param['type'] = !empty($search_param['type'])? $search_param['type'] : "";
        if($search_param['type'] != "count_only") {

            unset($search_param['type']);

            /*
             * Find data
             */
            $search_param = array_merge($search_param,$searchText);
            if (empty($search_param)) {
                $cmrList = $this->collection->find(array(), $pagination);
            } else {
                $cmrList = $this->collection->find($search_param, $pagination);

            }

            $customerModel = new \Models\Customer();
            $salesOrderModel = new \Models\SalesOrder();
            $orderNoList = array();
            foreach ($cmrList as $cmr) {
                array_push($orderNoList, $cmr['order_no']);
                $cmrOid = (array)$cmr['_id'];
                $cmrId = $cmrOid['oid'];
                $getDataCustomer = $customerModel->findFirst("customer_id = '" . $cmr['salesOrder']['customer_id'] . "'");
                $customerData = !empty($getDataCustomer)? $getDataCustomer->toArray() : "";
                $customerFirstName = !empty($customerData['first_name']) ? $customerData['first_name'] : "";
                $customerLastName = !empty($customerData['last_name']) ? $customerData['last_name'] : "";
                $customerName = $customerFirstName . " " . $customerLastName;
                unset($cmr['_id']);
                $cmrSelected['credit_memo_id'] = $cmr['credit_memo_id'];
                $cmrSelected['credit_memo_no'] = $cmr['credit_memo_no'];
                $cmrSelected['order_no'] = $cmr['order_no'];
                $cmrSelected['order_created_at'] = $cmr['salesOrder']['created_at'];
                $cmrSelected['customer_name'] = $customerName;
                $cmrSelected['subtotal'] = $cmr['subtotal'];
                $cmrSelected['created_at'] = $cmr['created_at'];
                $cmrSelected['status'] = $cmr['status'];
                $cmrSelected['type'] = !empty($cmr['type'])? $cmr['type']:"general";
                $cmrSelected['invoices'] = $cmr['invoices'];
                $cmrSelected['kredivo_id'] = null;
                $cmrData[] = array("credit_memo_id" => $cmrId) + $cmrSelected;
            }

            $kredivoIds = $salesOrderModel->getKredivoId($orderNoList);
            if (count($kredivoIds) > 0) {
                foreach ($cmrData as $idx => $cmr) {
                    foreach ($kredivoIds as $kredivo) {
                        if ($kredivo['order_no'] == $cmr['order_no']) {
                            $cmrData[$idx]['kredivo_id'] = $kredivo['kredivo_id'];
                        }
                    }
                }
            }
        }

        if(!empty($search_param['type'])) unset($search_param['type']);
        $recordsTotal = $this->collection->count($search_param);
        if($recordsTotal>0) {
            $data['list'] = $cmrData;
            $data['recordsTotal'] = $recordsTotal;
        }else{
            $this->errors = 'Data not found';
            $this->errorCode = 404;
            $data['list'] = array();
            $data['recordsTotal'] = 0;
        }

        return $data;

    }
}