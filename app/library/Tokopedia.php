<?php
namespace Library;

class Tokopedia
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {

    }

    public function getOrderTokopedia($params = array())
    {
        $data = array();
        if (isset($params['limit']) && isset($params['offset'])) {            
            $orders = array();
            $tokopediaOrderModel = new \Models\TokopediaOrder();
            $orders = $tokopediaOrderModel->getOrderTokopedia($params);
            
            $data['recordsTotal'] = $orders['count'];
            $data['data'] = $orders['data'];
        }

        return $data;
    }

    public function getDataEscrowTokopedia($params = array())
    {
        $data = array();
        if (isset($params['startdate']) && isset($params['enddate'])) {            
            $tokopediaOrderModel = new \Models\TokopediaOrder();
            $data['data'] = $tokopediaOrderModel->getDataEscrowTokopedia($params);
        }

        return $data;
    }
}