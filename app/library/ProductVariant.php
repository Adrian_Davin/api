<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/19/2017
 * Time: 3:31 PM
 */

namespace Library;


use Models\ProductStock;

class ProductVariant
{
    protected $errors;
    protected $messages;
    protected $data;

    protected $sku = '';
    protected $product_variant_id = '';

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $messages
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }


    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getProductVariantId()
    {
        return $this->product_variant_id;
    }

    /**
     * @param string $product_variant_id
     */
    public function setProductVariantId($product_variant_id)
    {
        $this->product_variant_id = $product_variant_id;
    }



    public function checkStatusGlobal($paramsStatus = array()){

        $productVariantModel = new \Models\ProductVariant();
        $conditions = '';
        if(!empty($this->product_variant_id)){
            $conditions = ' product_variant_id = '.$this->product_variant_id.' ';
        }elseif(!empty($this->sku)){
            $conditions = ' sku = "'.$this->sku.'" ';
        }
        $data_sku = $productVariantModel->findFirst(
            array(
                'conditions' => $conditions
            )
        );

        if($data_sku){

            /*
             * Checking Product Source
             */
            $productAttributeInt = $data_sku->Product->ProductAttributeInt->toArray();
            $productSource = 428; // Default Inter Company
            foreach($productAttributeInt as $row){
                if($row['attribute_id'] == 293){
                    $paramsStatus['product_source'] = $row['value'];
                }
            }

            $paramsStatus += $data_sku->toArray();
            $status_global = \Helpers\ProductHelper::checkStatusGlobal($paramsStatus);

            $productVariantModel->useWriteConnection();
            $productVariantModel->getDi()->getShared($productVariantModel->getConnection())->updateAsDict($productVariantModel->getSource(),$status_global,$conditions);

            //check apakah salah satu product variant aktive maka product parent aktiv
            $productParent = $data_sku->Product->toArray();
            $conditions = ' product_id = '.$productParent['product_id'].' AND status=10';
            $finalSku = $productVariantModel->find(
                array(
                    'conditions' => $conditions
                )
            );

            if(count($finalSku->toArray())>0){
                $parentStatus = array(
                    "status" => 10
                );

            }else{
                $parentStatus = array(
                    "status" => 0
                );
            }

            $productParentModel = new \Models\Product();
            $productParentModel->useWriteConnection();
            $parentConditions = ' product_id = '.$productParent['product_id'].' ';
            $productParentModel->getDi()->getShared($productParentModel->getConnection())->updateAsDict($productParentModel->getSource(),$parentStatus,$parentConditions);


            // update product images if status si approved
            if(!empty($paramsStatus['status_mp']) && !empty($paramsStatus['status_buyer_mp'])){
                if( $paramsStatus['status_mp'] == "approved" and $paramsStatus['status_buyer_mp'] == "approved"){
                    $productImageModel = new \Models\ProductImages();
                    $productImageModel->useWriteConnection();
                    $conditions = ' product_variant_id = '.$this->product_variant_id;

                    $dataImages = $productImageModel->find(
                        array(
                            'conditions' => $conditions
                        )
                    );

                    foreach($dataImages->toArray() as $dataImage) {
                        $imgConditions = ' product_variant_id = '.$this->product_variant_id.' AND image_id = '.$dataImage['image_id'];
                        $images['image_url'] = $dataImage['image_beta_url'];
                        $productImageModel->getDi()->getShared($productImageModel->getConnection())->updateAsDict($productImageModel->getSource(),$images,$imgConditions);
                    }
                }
            }

        }
    }

    public function changeProductSupplier($params){
        $productVariantModel = new \Models\ProductVariant();
        $productVariantModel->useReadOnlyConnection();

        if(!empty($params['sku'])){
            $conditions = ' sku = "'.$params['sku'].'" ';
        }
        $data_sku = $productVariantModel->findFirst(
            array(
                'conditions' => $conditions
            )
        );
        $variant = $data_sku->toArray();

        //update product
        $getProductId = "SELECT product_id FROM product_variant WHERE sku = '".$params['sku']."'";
        $sql = "UPDATE product SET supplier_id = '".$params['supplier_id']."' WHERE product_id = ($getProductId)";
        $productVariantModel->getDi()->getShared($productVariantModel->getConnection())->query($sql);

        $sku = $productVariantModel->generateSkuMp($params);
        $sqlAvirant = "UPDATE product_variant SET sku = '".$sku."' WHERE product_variant_id = '".$variant['product_variant_id']."'";
        $result = $productVariantModel->getDi()->getShared($productVariantModel->getConnection())->query($sqlAvirant);
        $affectRow = $result->numRows();
        if($affectRow>0){

            //generate new last sku
            $reservedModel = new \Models\ProductVariantReservedSku();
            $reservedModel->setSku($sku);
            $reservedModel->setSupplierId($params['supplier_id']);
            $reservedModel->saveData('product_variant_reserved_sku');

            //getAvailabelImages
            $variantImages = new \Models\ProductImages();
            $conditions = ' product_variant_id = "'.$variant['product_variant_id'].'" ';
            $dataImages = $variantImages->find(
                array(
                    'conditions' => $conditions
                )
            );
            $dataVariantImage = $dataImages->toArray();

            $data['product_variant_id'] = $variant['product_variant_id'];
            $data['sku'] = $sku;
            $data['supplier_id'] = $params['supplier_id'];
            $data['images'] = $dataVariantImage;
            $this->data = $data;
        }
        $this->messages[] = $affectRow." Data Updated";

    }

    public function updateMinMaxStock($params = array()){
        if(empty($params)){
            return false;
        }

        $productStockModel = new ProductStock();

        // Process update min-max stock
        $sql  = "UPDATE product_stock SET min = ( CASE sku ";
        $sql .= $params['minString'];
        $sql .= " END), max = ( CASE sku ";
        $sql .= $params['maxString'];
        $sql .= " END), update_minmax_date = ( CASE sku ";
        $sql .= $params['updateMinMaxString'];
        $sql .= " END) WHERE sku IN (".$params['stringSku'].") AND store_code IN (".$params['stringStoreCode'].")";
        $productStockModel->useWriteConnection();

        try{
           $result = $productStockModel->getDi()->getShared($productStockModel->getConnection())->query($sql);
        }catch (\Exception $e){
            $this->errors = "RR303";
            $this->messages = "Something gone wrong when trying to update min-max stock";
            return;
        }

        return array('affected_rows' => $result->numRows());

    }

}