<?php
namespace Library;

class Store
{
    protected $errorCode;
    protected $errorMessages;
    protected $storeModel;
    protected $pickupPointModel;
    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {
        $this->storeModel = new \Models\Store();
        $this->pickupPointModel = new \Models\PickupPoint();
    }

    public function getAllStore($params = array())
    {
        $storeModel = new \Models\Store();
        $resultStore = $storeModel::query();
        foreach($params as $key => $val)
        {
            if($key == "supplier_id"){
                $resultStore->andWhere("supplier_id = $val");
            }

            if($key == "store_id") {
                $resultStore->andWhere("store_id = $val");
            }

            if($key == "store_code") {
                $storeCodeArr = explode(",", $params['store_code']);

                $storeInQuery = "";
                foreach ($storeCodeArr as $val) {
                    $storeInQuery .= "'" . $val . "',";
                }

                $storeInQuery = rtrim($storeInQuery, ",");
                $resultStore->andWhere("store_code in ($storeInQuery)");
            }

            if($key == "type") {
                $resultStore->andWhere("type in ($val)");
            }
            if($key == "fulfillment_center") {
                $resultStore->andWhere("fulfillment_center = $val");
            }
            if($key == "pickup_center") {
                $resultStore->andWhere("pickup_center = $val");
            }
            if($key == "status") {
                $resultStore->andWhere("status = $val");
            }
            if($key == "name") {
                $resultStore->andWhere("name like '%".$val."%'");
            }
            if($key == "brand_store_id") {
                $resultStore->andWhere("brand_store_id = $val");
            }
            if($key == "is_able_gift") {
                $resultStore->andWhere("is_able_gift = $val");
            }
        }

        if(!empty($params["order_by"])) {
            $resultStore->orderBy($params["order_by"]);
        } else {
            $resultStore->orderBy("name ASC");
        }


        $resultTotal = $resultStore->execute();

        if (isset($params['limit']) && isset($params['offset'])) {
            $resultStore->limit($params['limit'], $params['offset']);
        }

        $result = $resultStore->execute();

        if(!empty($params["store_code"])) {
            if(isset($params["include_assignation_group"]) && $params["include_assignation_group"] == 0){
                $storeAssignationResult = "";
            }else{
                $headers = 
                [
                    "rr-type" => "national", 
                    "rr-store_assignation_group_id" => 3
                ];
                $apiWrapper = new APIWrapper(getenv('ASSIGNATION_API'));
                $apiWrapper->setHeaders($headers);
                $apiWrapper->setEndPoint("assignation/store-group/".$params["store_code"]);
                    
                if($apiWrapper->sendRequest("get")) {
                    $apiWrapper->formatResponse();
                } 
    
                $storeAssignationResult = $apiWrapper->getData();
            }
            
        }

        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your store";
            $allStore = array();
        } else {
            $i = 0;
            foreach ($result as $store) {
                $storeArray[$i] = $store->toArray([], true);
                unset($storeArray[$i]['pickup_id']);
                unset($storeArray[$i]['supplier_id']);

                $storeArray[$i]['store_assignation_group'] = (isset($storeAssignationResult)? $storeAssignationResult[0]['store_assignation_group'] : "");

                $storeArray[$i]['pickup_point'] = array();
                if ($store->PickupPoint) {
                    $storeArray[$i]['pickup_point'] = $store->PickupPoint->toArray();
                }

                $storeArray[$i]['supplier'] = array();
                if ($store->Supplier) {
                    $storeArray[$i]['supplier'] = $store->Supplier->toArray();
                }

                $i++;
            }

            $allStore['list'] = $storeArray;
            $allStore['recordsTotal'] = $resultTotal->count();
        }

        return $allStore;
    }

    public function getStoreData($store_id = "")
    {   
        $store = $this->getAllStore(["store_id" => $store_id]);
        return $store['list'][0];
    }

    public function saveStore($params = array())
    {
        // if store id exists we must check before update it
        if(isset($params['store_id'])) {
            $storeResult = $this->storeModel->findFirst("store_id = '". $params['store_id'] ."'");

            if(!$storeResult) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Store not found";
                return false;
            }

            if (!isset($params['pickup_code'])) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Parameter pickup_code is required";
                return false;
            }

            if(empty($params['pickup_id'])) {
                $storeCode = $this->pickupPointModel->findFirst("pickup_code = '". $params['pickup_code'] ."'");
                if($storeCode) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "Pickup already exists";
                    return false;
                }
            } else {
                $storeCode = $this->pickupPointModel->findFirst("pickup_code = '". $params['pickup_code'] ."' AND pickup_id != '". $params['pickup_id'] ."'");
                if($storeCode) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "Pickup already exists";
                    return false;
                }
            }
        } else {
            // check if store code already exists, why? store code is unique
            $storeCode = $this->storeModel->findFirst("store_code = '". $params['store_code'] ."'");

            if($storeCode) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Store already exists";
                return false;
            }

            // check if pickup code already exists, why? pickup code is unique
            $storeCode = $this->pickupPointModel->findFirst("pickup_code = '". $params['pickup_code'] ."'");

            if($storeCode) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Pickup already exists";
                return false;
            }
        }

        // first we need to insert / update pickup
        // second we update store
        // third if update we need to update store in mongodb

        $this->pickupPointModel->setFromArray($params);
        $savePickupPoint = $this->pickupPointModel->saveData("pickup_point");
        $pickupPointCache = $this->pickupPointModel->getPickupPointCache($params);
        $this->pickupPointModel->setPickupPointCached($pickupPointCache);
        $this->pickupPointModel->afterUpdate();
        if(!$savePickupPoint) {
            $this->errorCode = $this->pickupPointModel->getErrorCode();
            $this->errorMessages[] = $this->pickupPointModel->getErrorMessages();
            return false;
        } else if(!isset($params['pickup_id'])){
            // get last attribute id
            $lastId = $this->pickupPointModel->findFirst(['order' => 'pickup_id desc']);
            $params['pickup_id'] = !empty($lastId) ? $lastId->toArray()['pickup_id'] : 0;
        }
        
        $this->storeModel->setFromArray($params);
        $saveStatus = $this->storeModel->saveData("store");
        $storeCache = $this->storeModel->getStoreCache($params);
        $this->storeModel->setStoreCached($storeCache);
        $this->storeModel->afterUpdate();
        if(!$saveStatus) {
            $this->errorCode = $this->storeModel->getErrorCode();
            $this->errorMessages[] = $this->storeModel->getErrorMessages();
            return false;
        }

        return true;
    }
}