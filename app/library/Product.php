<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/19/2017
 * Time: 3:31 PM
 */

namespace Library;

use Helpers\ProductHelper;
use Models\CartItem;
use Models\ProductStock;
use Phalcon\Db as Database;

class Product
{
    /**
     * @var \Models\Product\Collection
     */
    protected $products;

    /**
     * @var \Models\Product\ProductVariant\Collection
     */
    protected $productsVariant;

    protected $errors;

    protected $error_code;
    protected $error_messages;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return \Models\Product\ProductVariant\Collection
     */
    public function getProductsVariant()
    {
        return $this->productsVariant;
    }

    /**
     * @param \Models\Product\ProductVariant\Collection $productsVariant
     */
    public function setProductsVariant($productsVariant)
    {
        $this->productsVariant = $productsVariant;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * @param mixed $error_code
     */
    public function setErrorCode($error_code)
    {
        $this->error_code = $error_code;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->error_messages;
    }

    /**
     * @param mixed $error_messages
     */
    public function setErrorMessages($error_messages)
    {
        $this->error_messages = $error_messages;
    }



    public function setProductsFromArray($data_array = "")
    {
        if (empty($data_array)) {
            return;
        }

        $products = array();
        foreach ($data_array as $product) {
            $productObj = new \Models\Product();
            $productObj->setFromArray($product);

            $products[] = $productObj;
        }

        $this->products = new \Models\Product\Collection($products);
    }

    public function search($type = "", $terms)
    {
        $result = array();

        if ($type == 'category' || $type == 'search') {
            $productModel = new \Models\Product();
            $resultAll = $productModel->searchProduct($terms);

            if (empty($productModel->getErrors())) {
                $this->setProductsFromArray($resultAll);

                $result = $this->products->getProductListArray();
            } else {
                $this->errors = $productModel->getErrors();
            }
        } elseif ($type == 'product') {
            $productModel = new \Models\Product();
            $productModel->setFromArray($terms);
            $result = $productModel->getProductDataFromCache();
            $this->errors = $productModel->getErrors();
        }

        return $result;
    }

    /**
     * @todo : Move it to GO or change query to elastic
     * @param $param
     * @return array
     */
    public function getProduct2CatBreakDown($param)
    {
        $category = array();

        $indexCat = 0;

        $product2catModel = new \Models\Product2Category();
        $product2catData = $product2catModel->find([
            'conditions' => ' product_id = ' . $param['product_id'],
            'order' => ' is_default desc '
        ]);

        if ($product2catData) {
            foreach ($product2catData->toArray() as $cat) {
                $productCatModel = new \Models\ProductCategory();
                $productCatData = $productCatModel->findFirst(' category_id = ' . $cat['category_id'] . ' and status > 0 ');
                if ($productCatData) {
                    $catId = str_replace('/', ',', $productCatData->toArray()['path']);
                    $productCatExtractData = $productCatModel->find(' category_id in (' . $catId . ') and status > 0 ');
                    if ($productCatExtractData) {
                        // TODO : no need to loop, get the first is_default only
                        foreach ($productCatExtractData->toArray() as $rowExtract) {

                            $routeData = \Helpers\RouteHelper::getRouteData($rowExtract['category_id'], "category");
                            $category[$indexCat][] = array(
                                'category_id' => $rowExtract['category_id'],
                                'name' => $rowExtract['name'],
                                'url_key' => !empty($routeData['url_key']) ? $routeData['url_key'] : ''
                            );
                        }
                        $indexCat++;
                    }
                }
            }
        } else {
            $this->error_code = "RR302";
            $this->error_messages = "No Any Categories Related";
        }

        return $category;
    }

    public function setProductsVariantFromArray($data_array  = array())
    {
        if (empty($data_array)) {
            return;
        }

        $products = array();
        foreach ($data_array as $key => $productVariant) {
            $sku = key($productVariant);
            $variantArray = $productVariant[$sku];

            if (!empty($sku)) {
                $variantArray['sku'] = $sku;
            }

            $variantObj = new \Models\ProductVariant();
            $variantObj->setFromArray($variantArray);

            $variants[] = $variantObj;
        }

        $this->productsVariant = new \Models\Product\ProductVariant\Collection($variants);
    }

    public function updateProductStock($stock_information = array())
    {
        $errorMessage = array();
        foreach ($stock_information['location'] as $productLocation) {
            $productLocation['sku'] = $stock_information['sku'];
            $productStockModel = new \Models\ProductStock();
            $productStockModel->setFromArray($productLocation);
            $productStockModel->saveData("product");

            if (!empty($productStockModel->getErrorMessages())) {
                $errorMessage = array_merge($errorMessage, $productStockModel->getErrorMessages());
            }
        }

        $this->updateProductStockStatus(array('sku' => $stock_information['sku']));

        $this->errors = $errorMessage;
    }

    /*
     * Everytime we update stock qty need to do check if stock is available
     * if stock is not available then update product data in database and elastic
     */
    public function updateProductStockStatus($params = array())
    {
        // this is for updating SINGLE is_in_stock for variant -> product
        if (empty($params)) {
            return false;
        }

        if (isset($params['stock_id'])) {
            // we have to find the SKU
            $productStockModel = new ProductStock();
            $resultProduct = $productStockModel->findFirst(
                [
                    "columns" => "sku",
                    "conditions" => "stock_id=" . $params['stock_id']
                ]
            );

            if ($resultProduct) {
                $sku = $resultProduct->toArray()['sku'];
            } else {
                return array('errors' => 'stock_id not found');
            }
        }

        if (isset($params['sku'])) {
            $sku = $params['sku'];
        }

        //search variant id
        $variantModel = new \Models\ProductVariant();
        $variantResult = $variantModel->findFirst(
            [
                "columns" => "product_variant_id,product_id,sku",
                "conditions" => "sku='" . $sku . "'"
            ]
        );

        if ($variantResult) {
            // update current variant is_in_stock
            $productVariant = $variantResult->toArray();
            $productId = $productVariant['product_id'];

            unset($productVariant['product_id']);

            $sql = "SELECT SUM(qty) as totalQty FROM product_stock where sku ='" . $sku . "'";
            $variantModel->useReadOnlyConnection();
            $result = $variantModel->getDi()->getShared($variantModel->getConnection())->query($sql);
            $result->setFetchMode(
                Database::FETCH_ASSOC
            );

            $totalQtyPerVariant = $result->fetchAll()[0]['totalQty'];

            if ($totalQtyPerVariant > 0) {
                $productVariant['is_in_stock'] = '1';
            } else {
                $productVariant['is_in_stock'] = '0';
            }

            $variantModel->setFromArray($productVariant);
            $variantModel->saveData();
            $variantErrors = $variantModel->getErrors();

            if (!empty($variantErrors)) {
                return array('errors' => $variantErrors[0]);
            } else {
                $productModel = new \Models\Product();
                //update product is_in_stock
                $sql  = "SELECT product_id, SUM(is_in_stock) AS is_in_stock FROM product_variant WHERE product_id=" . $productId;
                $sql .= " GROUP BY product_id";
                $productModel->useReadOnlyConnection();
                $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );

                $resultProduct = $result->fetchAll()[0];
                if ($resultProduct['is_in_stock'] <= 0) {
                    // update product is_in_stock
                    $parameter = array('product_id' => $productId, 'is_in_stock' => '0');
                } else {
                    $parameter = array('product_id' => $productId, 'is_in_stock' => '1');
                }

                $productModel->setFromArray($parameter);
                $productModel->saveData();

                if (!empty($this->errors)) {
                    return array('errors' => $this->errors);
                } else {
                    ProductHelper::updateProductToElastic(array('0' => array('sku' => $sku)));
                    return array('success' => 'true');
                }
            }
        } else {
            return array('errors' => 'Variant not found');
        }
    }

    /**
     * @param string $sku
     * @param string $kecamatan_code
     * @param int $qty_ordered
     * @return bool
     */
    public function isProductCanDeliveryToLocation($sku = "", $kecamatan_code = "", $qty_ordered = 1, $company_code = "ODI")
    {
        if (empty($sku)) {
            $this->error_code = "400";
            $this->error_messages[] = "sku is required";
        }

        if (empty($kecamatan_code)) {
            $this->error_code = "400";
            $this->error_messages[] = "kecamatan_code is required";
        }

        $cartItem = new CartItem();
        $cartItem->setSku($sku);
        $cartItem->setQtyOrdered($qty_ordered);

        /**
         * getting product source and supplier alias
         */
        $productModel = new \Models\Product();
        $query = [
            'from' => 0,
            'size' => 50,
            "query" => [
                "bool" => [
                    "must" => [
                        "nested" => [
                            "path" => "variants",
                            "query" => [
                                "match" => [
                                    "variants.sku.faceted" => $sku
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $productsData = $productModel->searchProduct("", $query);

        if (!empty($productsData)) {

            if (isset($productsData[0])) {
                $productData = $productsData[0];
            } else {
                $productData = $productsData;
            }

            $cartItem->setProductSource('Inter Company');
            foreach ($productData['attributes'] as $row) {
                if (isset($row['attribute_option']['option_value']) && $row['attribute_id'] == 293) {
                    $cartItem->setProductSource($row['attribute_option']['option_value']);
                }
            }

            $variantData = '';
            foreach ($productData['variants'] as $variant) {
                if ($variant['sku'] == $sku) {
                    $variantData = $variant;
                    break;
                }
            }

            if (isset($variantData['ownfleet_template_id'])) {
                $cartItem->setOwnfleetTemplateId($variantData['ownfleet_template_id']);
            }

            if (isset($productData['supplier']['supplier_alias'])) {
                $cartItem->setSupplierAlias($productData['supplier']['supplier_alias']);
            }

            $cartItem->checkStockAndAssignation($kecamatan_code, $company_code);
        } else {
            $this->error_code = 404;
            $this->error_messages = "Stock not found";
            $can_delivery = [
                'can_delivery' => false,
                'can_delivery_gosend' => false,
            ];
            return $can_delivery;
        }

        if (!empty($cartItem->getErrorMessages())) {
            $this->error_code = $cartItem->getErrorCode();
            $this->error_messages = $cartItem->getErrorMessages();
            $can_delivery = [
                'can_delivery' => false,
                'can_delivery_gosend' => false,
            ];
            return $can_delivery;
        } else {
            $can_delivery = [
                'can_delivery' => $cartItem->isCanDelivery(),
                'can_delivery_gosend' => $cartItem->isCanDeliveryGosend(),
            ];
            return $can_delivery;
        }
    }

    public function populateProducts()
    {
        // AHI, HCI, TGI
        $resultAce = $this->insertAceProducts();

        if (!empty($resultAce['errors'])) {
            $this->error_code = $resultAce['errors'];
            $this->error_messages = $resultAce['messages'];
            return;
        }

        $resultInforma = $this->insertInformaProducts();

        if (!empty($resultInforma['errors'])) {
            $this->error_code = $resultInforma['errors'];
            $this->error_messages = $resultInforma['messages'];
            return;
        }

        $resultToys = $this->insertToysProducts();

        if (!empty($resultToys['errors'])) {
            $this->error_code = $resultToys['errors'];
            $this->error_messages = $resultToys['messages'];
            return;
        }

        return array_merge($resultAce, $resultInforma, $resultToys);
    }

    public function insertAceProducts()
    {
        // 6531 -> ACE
        $productModel = new \Models\Product();
        $productResult = $productModel->find(
            [
                "columns" => "product_id",
                "conditions" => "supplier_id=59"
            ]
        );

        if (!empty($productResult->toArray())) {
            $productResult = $productResult->toArray();
            foreach ($productResult as $key => $product) {
                $aceProductArr[] = "(" . $product['product_id'] . ",6531,0,10000)";
            }

            $aceProductString = implode(',', $aceProductArr);

            $sql  = "DELETE FROM product_2_category WHERE category_id=6531";
            $productModel->useWriteConnection();

            try {
                $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
            } catch (\Exception $e) {
                return array(
                    'errors' => 'RR304',
                    'messages' => 'Delete ACE products from product_2_category failed.'
                );
            }

            $sql = "INSERT INTO product_2_category VALUES ";
            $sql .= $aceProductString;
            $productModel->useWriteConnection();

            try {
                $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
            } catch (\Exception $e) {
                return array(
                    'errors' => 'RR301',
                    'messages' => 'Insert ACE products into product_2_category failed.'
                );
            }

            return array(
                'total_ace_products' => $result->numRows()
            );
        }
    }

    public function insertInformaProducts()
    {
        // 6532 -> INFORMA
        $productModel = new \Models\Product();
        $productResult = $productModel->find(
            [
                "columns" => "product_id",
                "conditions" => "supplier_id=60"
            ]
        );

        if (!empty($productResult->toArray())) {
            $productResult = $productResult->toArray();
            foreach ($productResult as $key => $product) {
                $informaProductsArr[] = "(" . $product['product_id'] . ",6532,0,10000)";
            }

            $informaProductsString = implode(',', $informaProductsArr);

            $sql  = "DELETE FROM product_2_category WHERE category_id=6532";
            $productModel->useWriteConnection();

            try {
                $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
            } catch (\Exception $e) {
                return array(
                    'errors' => 'RR304',
                    'messages' => 'Delete INFORMA products from product_2_category failed.'
                );
            }

            $sql = "INSERT INTO product_2_category VALUES ";
            $sql .= $informaProductsString;
            $productModel->useWriteConnection();

            try {
                $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
            } catch (\Exception $e) {
                return array(
                    'errors' => 'RR301',
                    'messages' => 'Insert INFORMA products into product_2_category failed.'
                );
            }

            return array(
                'total_informa_products' => $result->numRows()
            );
        }
    }

    public function insertToysProducts()
    {
        // 6533 -> Toys
        $productModel = new \Models\Product();
        $productResult = $productModel->find(
            [
                "columns" => "product_id",
                "conditions" => "supplier_id=61"
            ]
        );

        if (!empty($productResult->toArray())) {
            $productResult = $productResult->toArray();
            foreach ($productResult as $key => $product) {
                $toysProductsArr[] = "(" . $product['product_id'] . ",6533,0,10000)";
            }

            $toysProductsString = implode(',', $toysProductsArr);

            $sql  = "DELETE FROM product_2_category WHERE category_id=6533";
            $productModel->useWriteConnection();

            try {
                $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
            } catch (\Exception $e) {
                return array(
                    'errors' => 'RR304',
                    'messages' => 'Delete TOYS products from product_2_category failed.'
                );
            }

            $sql = "INSERT INTO product_2_category VALUES ";
            $sql .= $toysProductsString;
            $productModel->useWriteConnection();

            try {
                $result = $productModel->getDi()->getShared($productModel->getConnection())->query($sql);
            } catch (\Exception $e) {
                return array(
                    'errors' => 'RR301',
                    'messages' => 'Insert TOYS products into product_2_category failed.'
                );
            }

            return array(
                'total_toys_products' => $result->numRows()
            );
        }
    }

    public function revertStock($params = array())
    {
        try {
            /**
             * Go-Stock
             */
            $productStockLib = new \Library\ProductStock();
            $productStockLib->updateStockRevert($params['store_code'], $params['qty_ordered'], $params['sku'], $params['process'], $params['order_no'], $params['company_code']);

            $storeModel = new \Models\Store();
            $storeData = $storeModel->findFirst(
                array(
                    'conditions' => 'store_code = "' . $params['store_code'] . '"',
                    'columns' => 'pickup_id'
                )
            );

            //revert category mapping stock
            if (isset($storeData->toArray()['pickup_id'])) {
                $pickupId = $storeData->toArray()['pickup_id'];
                $categoryStockMappingModels = new \Models\CategoryStockMapping();
                $categoryStockMappingResult = $categoryStockMappingModels->find("sku = '" . $params['sku'] . "' and pickup_id = '" . $pickupId . "'");
                if ($categoryStockMappingResult->count() > 0) {
                    //declare default variable
                    $categoryStockQty = 1;
                    foreach ($categoryStockMappingResult as $categoryStockMapping) {
                        $categoryStockQty = $categoryStockMapping->getQty();
                        $categoryLastStock = $categoryStockQty + $params['qty_ordered'];
                        $categoryStockMapping->assign($categoryStockMapping->toArray());
                        $categoryStockMapping->setQty($categoryLastStock);
                        $categoryStockMapping->saveData();
                    }


                    if ($categoryStockQty <= 0) {
                        /**
                         * @todo :update is_in_stock_category to 1 if before revert qty <= 0
                         */
                    }
                }
            }

            // no need to revert DC... if store code that want to revert is 1000DC...
            if (isset($params['flag_revert_stock'])) {
                if ($params['flag_revert_stock'] == '1000DC_only') {
                    return;
                }
            }

            // revert DC as well when reverting 1000DC but not process shipment
            if (strpos($params['store_code'], '1000DC') !== false && $params['process'] == "payment") {
                $productStockLib = new \Library\ProductStock();

                $productStockLib->updateStockRevert(str_replace("1000", "", $params['store_code']) . "-1000", $params['qty_ordered'], $params['sku'], $params['process'], $params['order_no'], $params['company_code']);

                // Not Used
                // $storeModel = new \Models\Store();
                // $storeData = $storeModel->findFirst(
                //     array(
                //         'conditions' => 'store_code = "DC"',
                //         'columns' => 'pickup_id'
                //     )
                // );

                // //revert category mapping stock
                // if(isset($storeData->toArray()['pickup_id'])) {
                //     $pickupId = $storeData->toArray()['pickup_id'];
                //     $categoryStockMappingModels = new \Models\CategoryStockMapping();
                //     $categoryStockMappingResult = $categoryStockMappingModels->find("sku = '" . $params['sku'] . "' and pickup_id = '" . $pickupId . "'");
                //     if ($categoryStockMappingResult->count() > 0) {
                //         //declare default variable
                //         $categoryStockQty = 1;
                //         foreach ($categoryStockMappingResult as $categoryStockMapping) {
                //             $categoryStockQty = $categoryStockMapping->getQty();
                //             $categoryLastStock = $categoryStockQty + $params['qty_ordered'];
                //             $categoryStockMapping->assign($categoryStockMapping->toArray());
                //             $categoryStockMapping->setQty($categoryLastStock);
                //             $categoryStockMapping->saveData();
                //         }

                //         if ($categoryStockQty <= 0) {
                //             /**
                //              * @todo :update is_in_stock_category to 1 if before revert qty <= 0
                //              */
                //         }
                //     }
                // }
            }

            /**
             * Update Is In Stock
             */
            // check max qty
            //            if($productStockLib->getMaxQuantity($params['sku'],1)){
            //                $productId = \Helpers\ProductHelper::getIdFromElastic($params);
            //                $productStockLib->updateIsInStock($productId,$params['sku'],1);
            //            }

        } catch (\Exception $e) {
            $this->error_code = 'RR303';
            $this->error_messages = 'Failed to revert stock';
        }
    }

    // get all sku, category_id and is in stock
    public function getAllCategoryStockMapping()
    {
        $arrIsInStock = array();
        try {
            // cek category mapping stock and update is_in_stock
            $categoryStockMappingModels = new \Models\CategoryStockMapping();
            $categoryStockMappingResult = $categoryStockMappingModels->find();
            if ($categoryStockMappingResult->count() > 0) {
                foreach ($categoryStockMappingResult as $categoryStockMapping) {
                    $categoryStock = $categoryStockMapping->toArray();
                    $sku = $categoryStock['sku'];
                    $category_id = $categoryStock['category_id'];
                    $qty = $categoryStock['qty'];
                    if (isset($arrIsInStock[$sku][$category_id]) && $arrIsInStock[$sku][$category_id] == 0) {
                        // cek if is in stock still 0, and there is another pickup id in same category. cek qty > 0 set is in stock to 1
                        $arrIsInStock[$sku][$category_id] = $qty > 0 ? 1 : 0;
                    } else if (!isset($arrIsInStock[$sku][$category_id])) {
                        // if qty > 0 set is in stock to 1
                        $arrIsInStock[$sku][$category_id] = $qty > 0 ? 1 : 0;
                    }
                }
            }
        } catch (\Exception $e) {
            $this->error_code = 'RR303';
            $this->error_messages = 'Failed to get all category stock mapping';
        }
        return $arrIsInStock;
    }

    public function checkCategoryIsInStock($params = array())
    {
        if (empty($params)) {
            $this->error_code = 400;
            $this->error_messages[] = "Params is required";
            return false;
        }

        $pickupId = $params['pickup_id'];
        $sku = $params['sku'];
        $qty = $params['qty'];
        $categoryStockMappingModels = new \Models\CategoryStockMapping();
        $categoryStockMappingResult = $categoryStockMappingModels->find("sku = '" . $sku . "' and pickup_id = '" . $pickupId . "'");
        if ($categoryStockMappingResult->count() > 0) {
            foreach ($categoryStockMappingResult as $categoryStockMapping) {
                $categoryStockQty = $categoryStockMapping->getQty();
                $categoryId = $categoryStockMapping->getCategoryId();
                if ($params['action'] == 'deduct') {
                    $categoryLastStock = $categoryStockQty - $qty;
                } else {
                    $categoryLastStock = $categoryStockQty + $qty;
                }

                $categoryStockMapping->assign($categoryStockMapping->toArray());
                $categoryStockMapping->setQty($categoryLastStock);
                try {
                    $categoryStockMapping->saveData("transaction");

                    // update elastic category is in stock
                    $catStockMappingModels = new \Models\CategoryStockMapping();
                    $catStockMappingResult = $catStockMappingModels->find("sku = '" . $sku . "' and category_id = '" . $categoryId . "'");
                    if ($catStockMappingResult->count() > 0) {
                        $isInStock = 0;
                        foreach ($catStockMappingResult as $catStockMapping) {
                            $categoryStock = $catStockMapping->toArray();
                            $qty = $categoryStock['qty'];
                            if ($isInStock <= 0) {
                                $isInStock = $qty > 0 ? 1 : 0;
                            }
                        }

                        sleep(1);
                        $parameter = array(
                            'sku' => $sku,
                            'category_id' => $categoryId,
                            'field' => 'is_in_stock',
                            'value' => $isInStock
                        );
                        $affectedRow = \Helpers\ProductHelper::updateCategoryIsInStock($parameter);

                        if ($affectedRow == 0) {
                            $this->error_code = 501;
                            $this->error_messages[] = "Failed to update status";
                            return false;
                        }
                    }
                } catch (\Exception $e) {
                    throw new \Exception("updateCategoryStock : " . $e->getMessage());
                }
            }
        }

        return true;
    }

    // mapping at table category stock mapping
    public function categoryStockMapping($params = array())
    {
        $categoryStockMappingModel = new \Models\CategoryStockMapping();
        $categoryStockMappingModel->categoryStockMapping($params);

        if (!empty($categoryStockMappingModel->getErrorCode())) {
            $this->error_code = $categoryStockMappingModel->getErrorCode();
            $this->error_messages = $categoryStockMappingModel->getErrorMessages();
        }
    }

    // This function is already unmaintain, moved to go-stock
    public function getMaximumStock($sku = "", $kecamatan_code = "", $company_code = "ODI", $store_code = "", $on_hand_only = 0, $device = "", $business_unit = "", $is_product_scanned = 0, $source = "")
    {

        if (empty($sku)) {
            $this->error_code = "400";
            $this->error_messages[] = "sku is required";
        }

        // if(empty($kecamatan_code)) {
        //     $this->error_code = "400";
        //     $this->error_messages[] = "kecamatan_code is required";
        // }

        $maxQty = 0;

        $query_param = "company_code=" . strtolower($company_code);
        if (!empty($store_code)) {
            $query_param .= "&store_code=" . $store_code;
        }
        if (!empty($on_hand_only)) {
            $query_param .= "&on_hand_only=" . $on_hand_only;
        }
        if (!empty($business_unit)) {
            $query_param .= "&business_unit=" . $business_unit;
        }
        if (!empty($source)) {
            $query_param .= "&source=" . $source;
        }

        // Load shopping card data
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
        $apiWrapper->setEndPoint("legacy/stock/check_available/" . $sku . "?" . $query_param);

        if ($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
        } else {
            return 0;
        }

        if (!empty($apiWrapper->getError())) {
            return 0;
        }

        // maybe cart is found, but data is empty
        $productStockData = $apiWrapper->getData();

        if (!empty($productStockData['max_qty']) && $productStockData['max_qty'] > 0) {
            $productStock = $productStockData['stock_list'];

            // Compare with attribute fulfillment_center from Elastic : https://ruparupa.atlassian.net/browse/RRTIC-76

            $productHelper = new \Helpers\ProductHelper();
            $finalReturnStores = [];
            $fulfillmentCenter = "";
            $fulfillmentStart = "";
            $fulfillmentEnd = "";
            $now = date("Y-m-d H:i:s");

            $attributesData = $productHelper->getAttributeFromElastic($sku);
            if (!empty($attributesData)) {
                foreach ($attributesData as $rowAttribute) {
                    if ($rowAttribute['attribute_name'] == "fulfillment_center") {
                        if ($rowAttribute["attribute_value"] != "") {
                            $fulfillmentCenter = explode(";", $rowAttribute["attribute_value"]);
                            foreach ($fulfillmentCenter as $indexFulfill => $rowFulfillment) {
                                $fulfillmentCenter[$indexFulfill] = trim($rowFulfillment);
                            }
                        }
                    }
                    if ($rowAttribute['attribute_name'] == "fulfillment_start_date") {
                        $fulfillmentStart = $rowAttribute["attribute_value"];
                    }
                    if ($rowAttribute['attribute_name'] == "fulfillment_end_date") {
                        $fulfillmentEnd = $rowAttribute["attribute_value"];
                    }
                }
            }

            $finalReturnStores = $productStock;

            if ($on_hand_only != 1 || $is_product_scanned !=1) {
                if ($now > $fulfillmentStart and $now < $fulfillmentEnd) {
                    if (!empty($fulfillmentCenter)) {
                        $finalReturnStores = [];
                        foreach ($productStock as $storeDetail) {
                            if (in_array($storeDetail['store_code'], $fulfillmentCenter)) {
                                $finalReturnStores[] = $storeDetail;
                            }
                        }
                    }
                }
            }

            $productPriceModel = new \Models\ProductPrice();
            $productPrice = $productPriceModel->getSellingPriceBySKU($sku);
            if ($productPrice['data']['price_pir'] > $productPrice['data']['selling_price']) {
                foreach ($finalReturnStores as $key => $stock) {
                    if (strpos($stock['store_code'], 'DC') !== false) {
                        unset($productStock[$key]);
                    }
                }
            }

            $tempStoreCode = array();
            foreach ($finalReturnStores as $stock) {
                $tempStoreCode[] = $stock['store_code'];
                $tempQtyStoreCode[$stock['store_code']] = $stock['qty'];
            }

            $storeModel = new \Models\Store();
            $storeDatas = $storeModel->findX(
                array(
                    'conditions' => "store_code IN ('" . implode("','", $tempStoreCode) . "') AND status = 10 "
                )
            );

            foreach ($storeDatas as $storeData) {
                if ($storeData) {
                    if ($device == "informa-android" || $device == "informa-ios" || $device == "informa-huawei") {
                        // 59: ACE, 60: SUSEN, 61: TOYS KINGDOM
                        if (($storeData->getSupplierId() == 59) || ($storeData->getSupplierId() == 61)) {
                            continue;
                        } else if (strpos(strtolower($storeData->getName()), 'selma') !== false) {
                            continue;
                        }
                    } else if ($device == "ace-android" || $device == "ace-ios" || $device == "ace-huawei") {
                        // 59: ACE
                        if (($storeData->getSupplierId() != 59)) {
                            continue;
                        }
                    } else if ($device == "selma-android" || $device == "selma-ios" || $device == "selma-huawei") {
                        // empty if block needed. `strpos({string}, 'selma') !== true` always returns true.
                        // only strpos({string}, 'selma') !== false behaves as expected
                        if (strpos(strtolower($storeData->getName()), 'selma') !== false) {
                        } else {
                            continue;
                        }
                    }

                    $qty = $tempQtyStoreCode[$storeData->getStoreCode()];
                    if ($qty > $maxQty) {
                        $maxQty = $qty;
                    }
                }
            }

            return $maxQty;
        }

        return 0;
    }

    // get max qty from store dc and store fullfillment
    public function getMaxQtyDelivery($sku, $kecamatan_code)
    {
        $maxQty = 0;

        $shippingAssignationModel = new \Models\ShippingAssignation();
        $shippingAssignationResult = $shippingAssignationModel->findFirst("kecamatan_code = '" . $kecamatan_code . "'");
        $shippingAssignationData = $shippingAssignationResult->assignation;
        $shippingAssignationList = explode(",", str_replace(" ", "", $shippingAssignationData));

        foreach ($shippingAssignationList as $assignationList) {
            // if DC not need to get store code from pickup point
            if (strtoupper(substr($assignationList, 0, 2)) == 'DC') {
                $productStockModel = new \Models\ProductStock();
                $productStockResult = $productStockModel->findFirst("sku = '" . $sku . "' and store_code = '" . $assignationList . "'");
                if ($productStockResult) {
                    if ($productStockResult->toArray()['qty'] > $maxQty) {
                        $maxQty = $productStockResult->toArray()['qty'];
                    }
                }
            } else {
                // get qty per store code on pickup point
                $pickupPointModel = new \Models\PickupPoint();
                $pickupPointResult = $pickupPointModel->findFirst("pickup_code = '" . $assignationList . "'");
                if ($pickupPointResult) {
                    if (!empty($pickupPointResult->Store->count())) {
                        foreach ($pickupPointResult->Store as $store) {
                            $storeCode = $store->toArray()['store_code'];
                            $productStockModel = new \Models\ProductStock();
                            $productStockResult = $productStockModel->findFirst("sku = '" . $sku . "' and store_code = '" . $storeCode . "'");
                            if ($productStockResult) {
                                if ($productStockResult->toArray()['qty'] > $maxQty) {
                                    $maxQty = $productStockResult->toArray()['qty'];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $maxQty;
    }

    function checkSku($orderNo)
    {
        $productModel = new \Models\Product();
        $isBlacklistSku = $productModel->isBlacklistSku(["order_no" => $orderNo]);
        if ($isBlacklistSku == 'yes') {
            $params = [
                "channel" => $_ENV['CA_SLACK_CHANNEL'],
                "username" => $_ENV['CA_SLACK_USERNAME'],
                "text" => "Order alert $orderNo, remark: Order held because SKU exist in sku_to_hold table",
                "icon_emoji" => ':robot_face:'
            ];

            // call queue for webhook slack
            $nsq = new Nsq();
            $message = [
                "data" => $params,
                "message" => "customerAlert"
            ];
            $nsq->publishCluster('transaction', $message);
        }
    }

    public function checkIsInStock($sku = '')
    {
        if (empty($sku)) {
            $this->error_code = 400;
            $this->error_messages[] = "SKU is required";
            return;
        }

        // update current variant is_in_stock
        $stockModel = new \Models\ProductStock();
        $sql = "SELECT SUM(qty) as totalQty FROM product_stock where sku ='" . $sku . "'";
        $stockModel->useReadOnlyConnection();
        $result = $stockModel->getDi()->getShared($stockModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $totalQtyPerVariant = $result->fetchAll()[0]['totalQty'];

        $params['sku'] = $sku;
        if ($totalQtyPerVariant > 0) {
            $params['is_in_stock'] = '1';
        } else {
            $params['is_in_stock'] = '0';
        }

        $affectedRow = \Helpers\ProductHelper::updateIsInStock($params);

        if ($affectedRow == 0) {
            $this->error_code = 501;
            $this->error_messages[] = "Faild to update status";
            return false;
        }

        return true;
    }

    public function getProductHandlingFeeAdjust($request){
        $sku = $request->getQuery("sku", "string");
        $zoneID = $request->getQuery("zone_id", "int");
        $sellingPrice = $request->getQuery("selling_price", "int");

        if (empty($sku)) {
            $this->error_code = 400;
            $this->error_messages[] = "SKU is required";
            return;
        } 

        if (empty($zoneID)) {
            $this->error_code = 400;
            $this->error_messages[] = "Zone ID is required";
            return;
        } 

        if (empty($sellingPrice)) {
            $this->error_code = 400;
            $this->error_messages[] = "SellingPrice is required";
            return;
        }

        $cartItemModel = new \Models\CartItem();
        
        $cartItemModel->setSku($sku);
        $cartItemModel->setZoneId($zoneID);
        $cartItemModel->setSellingPrice($sellingPrice);

        $cartItemModel->countHandlingFeeAdjust(); 

        $result = [
            "handling_fee_adjust" => $cartItemModel->getHandlingFeeAdjust()
        ];

        return $result;
    }
}
