<?php

namespace Library;


use phalcon\Db as Database;
use \GuzzleHttp\Client as GuzzleClient;
use Helpers\LogHelper;

class Notification
{

    protected $title;
    protected $body;
    protected $type;
    protected $key;
    protected $token;
    protected $value;
    protected $optional;
    protected $icon;
    protected $page_type;
    protected $url;
    protected $url_key;
    protected $customer_id;

    // GET
    public function getTitle()
    {
        return $this->title;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getKey()
    {
        return $this->token;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getOptional()
    {
        return $this->optional;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getPageType()
    {
        return $this->page_type;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getUrlKey()
    {
        return $this->url_key;
    }

    public function getCustomerID()
    {
        return $this->customer_id;
    }

    // SET
    public function setTitle($title)
    {
        return $this->title = $title;
    }

    public function setBody($body)
    {
        return $this->body = $body;
    }

    public function setType($type)
    {
        return $this->type = $type;
    }

    public function setKey($key)
    {
        return $this->key = $key;
    }

    public function setToken($token)
    {
        return $this->token = $token;
    }

    public function setValue($value)
    {
        return $this->value = $value;
    }

    public function setOptional($optional)
    {
        return $this->optional = $optional;
    }

    public function setIcon($icon)
    {
        return $this->icon = $icon;
    }

    public function setPageType($page_type)
    {
        return $this->page_type = $page_type;
    }

    public function setUrl($url)
    {
        return $this->url = $url;
    }

    public function setUrlKey($url_key)
    {
        return $this->url_key = $url_key;
    }

    public function setCustomerID($customer_id)
    {
        return $this->customer_id = $customer_id;
    }

    public function FilterTokenByHighestPriority($customerDeviceToken)
    {
        $custToken = [];
        $priorityPoint = array(
            "android" => 3,
		    "ios" => 3,
		    "mobile-web" => 2,
		    "desktop-web" => 1,
        );

        if (count($customerDeviceToken) <= 0) {
            return $custToken;
        }

        $currPriorityPoint = 0;

        foreach ($customerDeviceToken as $deviceToken) {
            $typeD = $deviceToken["os"];

            if (isset($priorityPoint[$typeD]) && $priorityPoint[$typeD] > $currPriorityPoint){
                $currPriorityPoint = $priorityPoint[$typeD];
            }
        }

        foreach ($customerDeviceToken as $deviceToken) {
            $typeD = $deviceToken["os"];

            if (isset($priorityPoint[$typeD]) && $priorityPoint[$typeD] == $currPriorityPoint){
                $custToken[] = $deviceToken;
            }
        }

        return $custToken;
    }

    public function PushNotif($buCode = '')
    {
        if(empty($buCode)){
            return;
        }

        $client = new GuzzleClient();
        $curlParam = [
            'timeout' => '10.0',
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode(
                [
                    'title' => $this->title,
                    'body' => $this->body,
                    'type' => [
                        'key' => $this->key,
                        'value' => $this->value
                    ],
                    'optional' => [
                        'icon' => $this->icon,
                        'page_type' => $this->page_type,
                        'url' => $this->url,
                        'url_key' => $this->url_key,
                        'customer_id' => (int)$this->customer_id
                    ]
                ]
            )
        ];

        try {
            switch ($buCode){
                case 'ODI': 
                    $urlPushNotif = getenv('PUSH_NOTIF_URL')."push";
                    break;
                case 'AHI':
                    $urlPushNotif = getenv('PUSH_NOTIF_URL')."push-ahi";
                    break;
                case 'HCI':
                    $urlPushNotif = getenv('PUSH_NOTIF_URL')."push-hci";
                    break;
            }

            $response = $client->request('POST', $urlPushNotif, $curlParam);
            LogHelper::log("push-notification", "Response push notif : '" . $response->getBody());
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("push-notification", "Error when push notif to  : " . $this->url . " error : " . $e->getMessage());
            return null;
        }

        return true;
    }

    public function PushNotifInbox()
    {
        $client = new GuzzleClient();
        $curlParam = [
            'timeout' => '10.0',
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode(
                [
                    'title' => $this->title,
                    'body' => $this->body,
                    'type' => [
                        'key' => $this->key,
                    ],
                    'optional' => [
                        'icon' => $this->icon,
                        'page_type' => $this->page_type,
                        'url' => $this->url,
                        'url_key' => $this->url_key,
                        'customer_id' => (int)$this->customer_id
                    ]
                ]
            )
        ];

        try {
            $response = $client->request('POST', getenv('PUSH_NOTIF_URL')."push-inbox", $curlParam);
            LogHelper::log("push-notification", "Response push inbox : '" . $response->getBody());
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("push-notification", "Error when push notif to  : " . $this->url . " error : " . $e->getMessage());
            return null;
        }

        return true;
    }

    public function SendNotificationDiscord($discordWebhook = array(), $title = "", $statusReason = "", $status = "", $serviceName = "API Core Service")
    {
        $client = new GuzzleClient();
        $curlParam = [
            'timeout' => '10.0',
            'headers' => [
                'Content-Type' => 'application/json',
                'x-api-key' => getenv('DISCORD_API_KEY')
            ],
            'body' => json_encode(
                [
                    'source' => 'ruparupa.custom',
                    'title' => $title,
                    'from' => $serviceName,
                    'status' => $status,
                    'status_reason' => $statusReason,
                    'discord_webhook_url' => $discordWebhook,
                    'discord_attributes' => [
                        'username' => 'Ruparupa Bot',
                        'avatar_url' => 'https://res.cloudinary.com/ruparupa-com/image/upload/v1543829312/SEO/Logo/logo_ruparupa_square.png',
                        'color_block' => 15105570
                    ],
                    'email_pic' => [],
                    'phone_number' => []
                ]
            )
        ];

        try {
            $response = $client->request('POST', getenv('DISCORD_API'), $curlParam);
            LogHelper::log("push-notification", "Response discord alert: " . $response->getBody());
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("push-notification", "Error when send discord alert, message: " . $e->getMessage());
            return $e->getMessage();
        }

        return '';
    }
}
