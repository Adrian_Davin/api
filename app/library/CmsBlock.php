<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use Helpers\CartHelper;
use Phalcon\Cache\Frontend\Data;
use Phalcon\Db as Database;
use \Helpers\LogHelper;

class CmsBlock
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllCmsBlock($params = array())
    {
        // only for CMS
        $cmsBlockList = array();
        $cmsBlockModel = new \Models\CmsBlock();
        $companyCode = (isset($params['company_code']) && !empty($params['company_code'])) ? $params['company_code'] : 'ODI';
        $query = [];

        if ($companyCode == 'ODI') {
            // grant all access
            if(isset($params['id'])) {
                $query = [
                        "columns" => "cms_block_id, title, body_html, company_code",
                        "conditions" => "status > -1 and cms_block_id in(".$params['id'].")"
                ];
            } else {
                $query = [
                        "columns" => "cms_block_id, title, status, company_code",
                        "conditions" => "status > -1"
                    ];
            }
        } else {
            if(isset($params['id'])) {
                $query = [
                        "columns" => "cms_block_id, title, body_html, company_code",
                        "conditions" => "status > -1 and company_code = '".$companyCode."' and cms_block_id in(".$params['id'].")"
                ];
            } else {
                $query = [
                        "columns" => "cms_block_id, title, status, company_code",
                        "conditions" => "status > -1 and company_code = '".$companyCode."'"
                    ];
            }
        }

        $result = $cmsBlockModel->find($query);
        $cms = $result->toArray();
        if(!empty($cms)){
            return $cms;
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "No data.";
        }

        return;
    }

    public function getCmsBlockDetail($params = array())
    {
        $cmsBlockModel = new \Models\CmsBlock();
        $companyCode = (isset($params['company_code']) && !empty($params['company_code'])) ? $params['company_code'] : 'ODI';
        $source = isset($params['source']) && !empty($params['source']) ? $params['source'] : '';
        $cms_block = $params['cms_block'];
        $flagCmsQuery = false;

        if ($source !== '') {
            // means from gideon or other else
            if ($companyCode == "ODI") {
                $flagCmsQuery = true;
            }
        }

        if ($flagCmsQuery) {
            if(is_numeric($cms_block)) {
                $query = [
                    "conditions" => "cms_block_id='".$cms_block."' AND status > -1",
                ];
            } else {
                $query = [
                        "conditions" => "identifier='".$cms_block."' AND status > -1",
                    ];
            }
        } else {
            if(is_numeric($cms_block)) {
                $query = [
                    "conditions" => "cms_block_id='".$cms_block."' AND status > -1 AND company_code='".$companyCode."'",
                ];
            } else {
                $query = [
                        "conditions" => "identifier='".$cms_block."' AND status > -1 AND company_code='".$companyCode."'",
                    ];
            }
        }
        
        $findCms = $cmsBlockModel->findFirst($query);

        if($findCms){
            return $findCms->toArray();
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "No CMS block detail has been found.";
        }
    }

}