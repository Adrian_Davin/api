<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use Helpers\CartHelper;

class SpecialPrice
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllSpecialPrice($params = array())
    {
        $specialPriceDetails = array();
        $specialPriceDetails3 = array();
        $specialPriceModel = new \Models\SpecialPrice();

        if(isset($params['sku'])){
            $sku = explode(',',$params['sku']);
            $skuFix = array();
            foreach($sku as $rowSku){
                $skuFix[] = "'".str_replace("'","",str_replace('"','',trim($rowSku)))."'";
            }
            $skuFix = implode(',',$skuFix);
            
            $sql = "
            SELECT 
                special_price_id,
                sku,
                special_price,
                start_date,
                end_date,
                file_csv,
                created_user,
                created_at,
                label,
                additional_specification
            FROM
                special_price
            WHERE
                sku IN ('.$skuFix.')
            ORDER BY created_at DESC
                    ";
            $result = $specialPriceModel->getDi()->getShared('dbMaster')->query($sql);
        }else{
            $sql = "
            SELECT 
                special_price_id,
                sku,
                special_price,
                start_date,
                end_date,
                file_csv,
                created_user,
                created_at,
                label,
                additional_specification
            FROM
                special_price
            ORDER BY created_at DESC
                ";
        
            if(isset($params['company_code'])){
                if($params['company_code'] != "ODI"){
                    $sql = "
                        SELECT 
                            distinct(a.special_price_id) as special_price_id,
                            a.sku,
                            a.special_price,
                            a.start_date,
                            a.end_date,
                            a.file_csv,
                            a.created_user,
                            a.created_at,
                            a.label,
                            a.additional_specification
                        FROM
                            special_price a
                        LEFT JOIN
                            admin_user b ON a.created_user = b.email
                        LEFt join
                            admin_user_attribute_value c on b.admin_user_id = c.admin_user_id 
                        where 
                            c.admin_user_attribute_id = 15 and
                            c.value = '".$params['company_code']."'
                        ORDER BY a.created_at DESC
                        ";
                }
            }
            $result = $specialPriceModel->getDi()->getShared('dbMaster')->query($sql);
        }

        if($result->numRows() > 0){
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            return $result->fetchAll();
        }
        else {
            $this->errorCode = "RR001";
            $this->errorMessages = "No special price has been found";
            return;
        }
    }


}