<?php

namespace Library\AceHardware;

use Exception;
use Helpers\LogHelper;
use Library\APIWrapper;
use Library\KawanLamaSystem;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class PayWithPoint
{
    protected $errorCode;
    protected $errorMessages;
    protected $kawanLamaSystemLib;

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();
    }

    public function validatePointRedeemAgainstCartTransaction($pointRedeem, $cartId)
    {
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setEndPoint("/cart/" . $cartId);
        if ($apiWrapper->sendRequest("get")) {
            $apiWrapper->formatResponse();
        } else {
            LogHelper::log('redeem_point_ace', '[ERROR]: ' . json_encode($apiWrapper->getData()));
            $this->errorCode = 400;
            $this->errorMessages = "Gagal untuk mendapatkan customer cart";

            return false;
        }

        $customerCart = $apiWrapper->getData();
        $voucherAmount = (int)$pointRedeem * (int)getenv('POINT_VOUCHER_WORTH_AHI'); // n * 2500
        if ((int)$voucherAmount > (int)$customerCart['subtotal']) {
            $this->errorCode = 400;
            $this->errorMessages = "Penukaran point tidak boleh melebihi harga yang harus dibayarkan";

            return false;
        }

        return true;
    }

    public function applyVoucherToCart($cartId, $voucherCode)
    {
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setEndPoint("/v2/marketing/voucher/redeem");
        $apiWrapper->setParam(
            array(
                'cart_id' => $cartId,
                'voucher_code' => $voucherCode
            )
        );
        if ($apiWrapper->sendRequest("post")) {
            if (!empty($apiWrapper->getError())) {
                LogHelper::log('redeem_point_ace', '[ERROR] when calling cart API ' . json_encode($apiWrapper->getError()));
                $this->errorCode = 400;
                $this->errorMessages = $apiWrapper->getMessages();

                return false;
            }

            $apiWrapper->formatResponse();
            LogHelper::log('redeem_point_ace', '[SUCCESS] merge ' . json_encode($apiWrapper->getData()));
        } else {
            LogHelper::log('redeem_point_ace', '[ERROR] when calling cart API ' . json_encode($apiWrapper->getError()));
            $this->errorCode = 400;
            $this->errorMessages = "Terjadi kesalahan saat menggunakan voucher pada keranjang belanja anda, silahkan coba lagi";

            return false;
        }

        return true;
    }

    public function saveAndProcessPointRedeem($customerId, $pointRedeem, $voucherGenerated, $customerAceDetail)
    {
        $customerPointRedeem = new \Models\CustomerPointRedeem();
        $salesRuleVoucher = new \Models\SalesruleVoucher();
        $customerPointRedeem->useWriteConnection();
        $salesRuleVoucher->useWriteConnection();
        $manager = new TxManager();
        $manager->setDbService($customerPointRedeem->getConnection());
        $manager->setDbService($salesRuleVoucher->getConnection());
        $transaction = $manager->get();
        $customerPointRedeem->setTransaction($transaction);
        $salesRuleVoucher->setTransaction($transaction);

        try {
            // Save to table salesrule_voucher
            $salesRuleVoucher->assign($voucherGenerated);
            if (false === $salesRuleVoucher->save()) {
                throw new Exception(
                    implode('|', $salesRuleVoucher->getMessages())
                );
            }

            // Save to table customer_point_redeem
            $customerPointRedeem->assign(
                [
                    'customer_id' => $customerId,
                    'point_redeemed' => $pointRedeem,
                    'sales_rule_voucher_id' => $salesRuleVoucher->getVoucherId(),
                    'company_code' => 'AHI',
                    'created_at' => date('Y-m-d')
                ]
            );
            if (false === $customerPointRedeem->save()) {
                throw new Exception(
                    implode('|', $customerPointRedeem->getMessages())
                );
            }

            // Redeem Point To Ace API
            $pointRedeemed = $this->kawanLamaSystemLib->redeemPointAce(
                $customerAceDetail['ace_customer_id'],
                $customerAceDetail['ace_customer_member_no'],
                $voucherGenerated['voucher_code'],
                $pointRedeem
            );
            if (!$pointRedeemed) {
                throw new Exception(
                    $this->kawanLamaSystemLib->getErrorMessages()
                );
            }

            // Commit the transactions
            $transaction->commit();
        } catch (\Exception $ex) {
            $transaction->rollback($ex->getMessage());
            LogHelper::log("redeem_point_ace", "EXCEPTION OCCURED: " . $ex->getMessage() .
                " ,CUSTOMER ID: " . $customerId);

            $this->errorCode = 500;
            $this->errorMessages = $ex->getMessage();

            return null;
        }

        return $salesRuleVoucher;
    }
}
