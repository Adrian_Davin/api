<?php

namespace Library\AceHardware;

use Exception;
use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Helpers\ServiceCenterHelper;
use Library\Email;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class ServiceCenter
{
    protected $errorCode;
    protected $errorMessages;

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {
    }

    private function getDigitalReceiptDataByReceiptId($receipt_id)
    {
        $apiWrapper = new \Library\APIWrapper(getenv('ORDER_API'));
        $apiWrapper->setEndPoint("/digital-receipts/$receipt_id/detail");
        if ($apiWrapper->sendRequest("get")) {
            $apiWrapper->formatResponse();
        } else {
            LogHelper::log('service_center_warranty_fetch_digital_receipt', '[ERROR]: ' . json_encode($apiWrapper->getData()));
            return null;
        }

        return $apiWrapper->getData();
    }

    private function validateSerialNumberOrWarrantyNumber($serialNumber, $warrantyNumber)
    {
        $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();

        $isSerialNumberOrWarrantyNumberExist = $customerAceOrderWarrantyModel->findFirst([
            'conditions' => 'serial_number =:serial_number: OR warranty_number=:warranty_number:',
            'bind' => ['serial_number' => $serialNumber, 'warranty_number' => $warrantyNumber]
        ]);

        if ($isSerialNumberOrWarrantyNumberExist)
            return false;

        return true;
    }

    public function saveDataToCustomerAceOrderWarranty($payload)
    {
        try {
            // Save to table customer_ace_order_warranty
            $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
            $customerAceOrderWarrantyModel->assign($payload);
            if (false === $customerAceOrderWarrantyModel->save()) {
                throw new Exception(
                    implode('|', $customerAceOrderWarrantyModel->getMessages())
                );
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat menyimpan data garansi";
            LogHelper::log("service_center_save_data", "MESSAGE: " . $this->errorMessages);

            return false;
        }

        return true;
    }

    public function saveDataToCustomerAceOrderWarrantyNonMembership($payload)
    {
        try {
            // Save to table customer_ace_order_warranty
            $customerAceOrderWarrantyNonMembershipModel = new \Models\CustomerAceOrderWarrantyNonMembership();
            $customerAceOrderWarrantyNonMembershipModel->assign($payload);
            if (false === $customerAceOrderWarrantyNonMembershipModel->save()) {
                throw new Exception(
                    implode('|', $customerAceOrderWarrantyNonMembershipModel->getMessages())
                );
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat menyimpan data garansi";
            LogHelper::log("service_center_save_data", "MESSAGE: " . $this->errorMessages);

            return false;
        }

        return true;
    }

    public function serviceCenterWarrantyCheckupByOrderNo($order_no)
    {
        $warrantyAvailabilities = [];

        if (empty($order_no)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "No Order Diperlukan";
            return null;
        }

        try {
            $aceWebAPI = new \Library\AceWebAPI();
            $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
            $productModel = new \Models\Product();
            $salesOrderModel = new \Models\SalesOrder();
            $salesInvoiceModel = new \Models\SalesInvoice();
            $salesInvoiceItemModel = new \Models\SalesInvoiceItem();

            if (substr($order_no, 0, 3) === 'ODI') {
                $salesOrder = $salesOrderModel->findFirst("order_no = '$order_no'");
                $salesInvoices = $salesInvoiceModel->find("sales_order_id = '" . $salesOrder->getSalesOrderId() . "'");
            } else if (substr($order_no, 0, 3) === 'INV') {
                $salesInvoices = $salesInvoiceModel->find("invoice_no = '" . $order_no . "'");
            }

            foreach ($salesInvoices as $salesInvoice) {
                $salesInvoiceItems = $salesInvoiceItemModel
                    ->find("invoice_id = '" . $salesInvoice->getInvoiceId() . "'")
                    ->toArray();

                $arrSKUItems = array_reduce($salesInvoiceItems, function ($result, $item) {
                    $item = (array) $item;
                    $key = $item['invoice_item_id'];
                    unset($item['invoice_item_id']);

                    $result[$key] = $item['sku'];

                    return $result;
                }, array());

                $skuWarrantyAvailabilityResult = $aceWebAPI->checkSKUWarrantyAvailability(
                    array('SKU' => array_values($arrSKUItems))
                );
                if (!$skuWarrantyAvailabilityResult) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Terjadi kesalahan saat mendapatkan data pada API HO";
                    return false;
                }

                $arrWarrantyAvailabilityResult = array_reduce($skuWarrantyAvailabilityResult, function ($result, $item) {
                    $item = (array) $item;
                    $key = $item['SKU'];
                    unset($item['SKU']);

                    $result[$key] = $item['IsWarranty'];

                    return $result;
                }, array());

                foreach ($salesInvoiceItems as $salesInvoiceItem) {
                    $aceWarrantyId = null;
                    $warrantyStatus = null;
                    $unregisteredWarranty = $salesInvoiceItem['qty_ordered'];

                    $elasticProduct = $productModel->searchProductFromElastic($salesInvoiceItem['sku']);

                    $customerAceOrderWarranties = $customerAceOrderWarrantyModel->find(
                        "sales_invoice_item_id = '" . $salesInvoiceItem['invoice_item_id'] . "'"
                    )->toArray();

                    if (isset($customerAceOrderWarranties) && count($customerAceOrderWarranties) > 0) {
                        foreach ($customerAceOrderWarranties as $orderWarranty) {
                            $aceWarrantyId = $orderWarranty['ace_warranty_id'];
                            if (
                                $orderWarranty['new_item_replacement_warranty_date'] >= date('Y-m-d') ||
                                $orderWarranty['spare_part_warranty_date'] >= date('Y-m-d') ||
                                $orderWarranty['service_warranty_date'] >= date('Y-m-d')
                            )
                                $warrantyStatus = 'active';
                            else
                                $warrantyStatus = 'expired';

                            array_push(
                                $warrantyAvailabilities,
                                array(
                                    "sales_invoice_item_id" => $salesInvoiceItem['invoice_item_id'],
                                    "ace_warranty_id"       => $aceWarrantyId,
                                    "sku_number"            => $elasticProduct['variants'][0]['sku'],
                                    "sku_name"              => $elasticProduct['name'],
                                    "sku_image"             => $elasticProduct['variants'][0]['images'][0]['image_url'],
                                    "is_warranty"           => $arrWarrantyAvailabilityResult[$salesInvoiceItem['sku']],
                                    "warranty_status"       => $warrantyStatus,
                                    "shipment_status"       => $salesInvoice->getInvoiceStatus()
                                )
                            );
                            $unregisteredWarranty--;
                        }
                    }

                    if ($unregisteredWarranty > 0) {
                        $warrantyAvailabilityPerQuantity = ServiceCenterHelper::multiplyWarrantyAvailabilitySKUDataPerQuantity(
                            array(
                                "sales_invoice_item_id" => $salesInvoiceItem['invoice_item_id'],
                                "ace_warranty_id"       => null,
                                "sku_number"            => $elasticProduct['variants'][0]['sku'],
                                "sku_name"              => $elasticProduct['name'],
                                "sku_image"             => $elasticProduct['variants'][0]['images'][0]['image_url'],
                                "is_warranty"           => $arrWarrantyAvailabilityResult[$salesInvoiceItem['sku']],
                                "warranty_status"       => null,
                                "shipment_status"       => $salesInvoice->getInvoiceStatus()
                            ),
                            $unregisteredWarranty
                        );

                        $warrantyAvailabilities = array_merge($warrantyAvailabilities, $warrantyAvailabilityPerQuantity);
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat mendapatkan data daftar garansi";
            LogHelper::log("service_center_warranty_checkup", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return $warrantyAvailabilities;
    }

    public function serviceCenterWarrantyCheckupByReceiptId($receipt_id)
    {
        $warrantyAvailabilities = [];

        if (empty($receipt_id)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "No Receipt Diperlukan";
            return null;
        }

        try {
            $aceWebAPI = new \Library\AceWebAPI();

            $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
            $productModel = new \Models\Product();

            $receiptDetail = $this->getDigitalReceiptDataByReceiptId($receipt_id);
            if (!$receiptDetail) {
                return $this->sendErrorResponse(
                    400,
                    "Gagal mendapatkan data receipt"
                );
            }

            $arrSKUItems = array_map(function ($val) {
                return $val['sku'];
            }, $receiptDetail['products']);

            $skuWarrantyAvailabilityResult = $aceWebAPI->checkSKUWarrantyAvailability(
                array('SKU' => array_values($arrSKUItems))
            );
            if (!$skuWarrantyAvailabilityResult) {
                $this->errorCode = 400;
                $this->errorMessages = "Terjadi kesalahan saat mendapatkan data pada API HO";
                return false;
            }

            $arrWarrantyAvailabilityResult = array_reduce($skuWarrantyAvailabilityResult, function ($result, $item) {
                $item = (array) $item;
                $key = $item['SKU'];
                unset($item['SKU']);

                $result[$key] = $item['IsWarranty'];

                return $result;
            }, array());

            foreach ($receiptDetail['products'] as $product) {
                $aceWarrantyId = null;
                $warrantyStatus = null;
                $unregisteredWarranty = $product['qty'];

                $elasticProduct = $productModel->searchProductFromElastic($product['sku']);

                $customerAceOrderWarranties = $customerAceOrderWarrantyModel->find(
                    "receipt_id_and_sku = '" . $receipt_id . "-" . $product['sku'] . "'"
                )->toArray();

                if (isset($customerAceOrderWarranties) && count($customerAceOrderWarranties) > 0) {
                    foreach ($customerAceOrderWarranties as $orderWarranty) {
                        $aceWarrantyId = $orderWarranty['ace_warranty_id'];
                        if (
                            $orderWarranty['new_item_replacement_warranty_date'] >= date('Y-m-d') ||
                            $orderWarranty['spare_part_warranty_date'] >= date('Y-m-d') ||
                            $orderWarranty['service_warranty_date'] >= date('Y-m-d')
                        )
                            $warrantyStatus = 'active';
                        else
                            $warrantyStatus = 'expired';

                        array_push(
                            $warrantyAvailabilities,
                            array(
                                "sales_invoice_item_id" => null,
                                "receipt_id_and_sku"    => $receipt_id . "-" . $product['sku'],
                                "ace_warranty_id"       => $aceWarrantyId,
                                "sku_number"            => $elasticProduct['variants'][0]['sku'],
                                "sku_name"              => $elasticProduct['name'],
                                "sku_image"             => $elasticProduct['variants'][0]['images'][0]['image_url'],
                                "is_warranty"           => $arrWarrantyAvailabilityResult[$product['sku']],
                                "warranty_status"       => $warrantyStatus
                            )
                        );
                        $unregisteredWarranty--;
                    }
                }

                if ($unregisteredWarranty > 0) {
                    $warrantyAvailabilityPerQuantity = ServiceCenterHelper::multiplyWarrantyAvailabilitySKUDataPerQuantity(
                        array(
                            "sales_invoice_item_id" => null,
                            "receipt_id_and_sku"    => $receipt_id . "-" . $product['sku'],
                            "ace_warranty_id"       => null,
                            "sku_number"            => $elasticProduct['variants'][0]['sku'],
                            "sku_name"              => $elasticProduct['name'],
                            "sku_image"             => $elasticProduct['variants'][0]['images'][0]['image_url'],
                            "is_warranty"           => $arrWarrantyAvailabilityResult[$product['sku']],
                            "warranty_status"       => null
                        ),
                        $unregisteredWarranty
                    );

                    $warrantyAvailabilities = array_merge($warrantyAvailabilities, $warrantyAvailabilityPerQuantity);
                }
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat mendapatkan data daftar garansi";
            LogHelper::log("service_center_warranty_checkup_offline", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return $warrantyAvailabilities;
    }

    public function serviceCenterWarrantyDetailsCheckPerWarrantyID($ace_warranty_id)
    {
        $dataToReturn = array();
        if (empty($ace_warranty_id)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "Nomor Garansi Ace Diperlukan";
            return null;
        }

        try {
            $aceWebAPI = new \Library\AceWebAPI();
            $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
            $salesInvoiceModel = new \Models\SalesInvoice();
            $salesInvoiceItemModel = new \Models\SalesInvoiceItem();

            $customerAceOrderWarranty = $customerAceOrderWarrantyModel->findFirst(
                "ace_warranty_id = '$ace_warranty_id'"
            );

            $salesInvoiceItem = $salesInvoiceItemModel->findFirst(
                "invoice_item_id = '" . $customerAceOrderWarranty->getSalesInvoiceItemId() . "'"
            )->toArray();

            $salesInvoice = $salesInvoiceModel->findFirst(
                "invoice_id = '" . $salesInvoiceItem['invoice_id'] . "'"
            )->toArray();

            if (
                ($customerAceOrderWarranty->getNewItemReplacementWarrantyDate() >= date('Y-m-d')) ||
                ($customerAceOrderWarranty->getSparePartWarrantyDate() >= date('Y-m-d')) ||
                ($customerAceOrderWarranty->getServiceWarrantyDate() >= date('Y-m-d'))
            ) {
                $dataToReturn = array(
                    "change_new_item_warranty_date" => $customerAceOrderWarranty->getNewItemReplacementWarrantyDate(),
                    "spare_part_warranty_date" => $customerAceOrderWarranty->getSparePartWarrantyDate(),
                    "service_warranty" => $customerAceOrderWarranty->getServiceWarrantyDate(),
                    "status" => "Available",
                    "sku" => $salesInvoiceItem['sku'],
                    "receipt_number" => !empty($salesInvoice['receipt_id']) ? $salesInvoice['receipt_id'] : null,
                    "warranty_number" => $customerAceOrderWarranty->getWarrantyNumber(),
                    "serial_number" => $customerAceOrderWarranty->getSerialNumber(),
                    "spareparts" => !empty($customerAceOrderWarranty->getSparePartsDetailWarranty()) ?
                        $customerAceOrderWarranty->getSparePartsDetailWarranty() :
                        null
                );
            } else {
                $warrantyDetails = $aceWebAPI->checkWarrantyDetailsPerSKU($ace_warranty_id);
                if (!$warrantyDetails) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Terjadi kesalahan saat mendapatkan data pada API HO";
                    return false;
                }

                $customerAceOrderWarranty->setNewItemReplacementWarrantyDate(date('Y-m-d', strtotime($warrantyDetails['ReplaceExpired'])));
                $customerAceOrderWarranty->setSparePartWarrantyDate(date('Y-m-d', strtotime($warrantyDetails['SpareExpired'])));
                $customerAceOrderWarranty->setServiceWarrantyDate(date('Y-m-d', strtotime($warrantyDetails['ServiceExpired'])));

                $formattedSpareparts = [];
                if (isset($warrantyDetails['Spareparts']) && !empty($warrantyDetails['Spareparts'])) {
                    foreach ($warrantyDetails['Spareparts'] as $sparePart) {
                        array_push(
                            $formattedSpareparts,
                            array(
                                'sku' => $sparePart['SparePartSKU'],
                                'spare_part_name' => $sparePart['SparePartName'],
                                'sku_warranty_date' => $sparePart['SKUWarrantyDate']
                            )
                        );
                    }
                    $customerAceOrderWarranty->setSparePartsDetailWarranty(json_encode($formattedSpareparts));
                }

                $result = $customerAceOrderWarranty->update();
                if (false === $result) {
                    return $this->sendErrorResponse(
                        400,
                        implode('|', $customerAceOrderWarranty->getMessages())
                    );
                }

                $dataToReturn = array(
                    "change_new_item_warranty_date" => date('Y-m-d', strtotime($warrantyDetails['ReplaceExpired'])),
                    "spare_part_warranty_date" => date('Y-m-d', strtotime($warrantyDetails['SpareExpired'])),
                    "service_warranty" => date('Y-m-d', strtotime($warrantyDetails['ServiceExpired'])),
                    "status" => "Available",
                    "sku" => $salesInvoiceItem['sku'],
                    "receipt_number" => !empty($salesInvoice['receipt_id']) ? $salesInvoice['receipt_id'] : null,
                    "warranty_number" => $customerAceOrderWarranty->getWarrantyNumber(),
                    "serial_number" => $customerAceOrderWarranty->getSerialNumber(),
                    "spareparts" => (isset($warrantyDetails['Spareparts']) && !empty($warrantyDetails['Spareparts'])) ?
                        json_encode($formattedSpareparts) :
                        []
                );
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat mendapatkan data daftar garansi";
            LogHelper::log("service_center_warranty_details", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return $dataToReturn;
    }

    public function serviceCenterWarrantyDetailsCheckPerWarrantyIDOfflineTransaction($ace_warranty_id)
    {
        $dataToReturn = array();
        if (empty($ace_warranty_id)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "Nomor Garansi Ace Diperlukan";
            return null;
        }

        try {
            $aceWebAPI = new \Library\AceWebAPI();
            $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();

            $customerAceOrderWarranty = $customerAceOrderWarrantyModel->findFirst(
                "ace_warranty_id = '$ace_warranty_id'"
            );

            if (
                ($customerAceOrderWarranty->getNewItemReplacementWarrantyDate() >= date('Y-m-d')) ||
                ($customerAceOrderWarranty->getSparePartWarrantyDate() >= date('Y-m-d')) ||
                ($customerAceOrderWarranty->getServiceWarrantyDate() >= date('Y-m-d'))
            ) {
                $dataToReturn = array(
                    "change_new_item_warranty_date" => $customerAceOrderWarranty->getNewItemReplacementWarrantyDate(),
                    "spare_part_warranty_date" => $customerAceOrderWarranty->getSparePartWarrantyDate(),
                    "service_warranty" => $customerAceOrderWarranty->getServiceWarrantyDate(),
                    "status" => "Available",
                    "sku" => explode('-', $customerAceOrderWarranty->getReceiptIdAndSku())[1],
                    "receipt_number" => explode('-', $customerAceOrderWarranty->getReceiptIdAndSku())[0],
                    "warranty_number" => $customerAceOrderWarranty->getWarrantyNumber(),
                    "serial_number" => $customerAceOrderWarranty->getSerialNumber(),
                    "spareparts" => !empty($customerAceOrderWarranty->getSparePartsDetailWarranty()) ?
                        $customerAceOrderWarranty->getSparePartsDetailWarranty() :
                        null
                );
            } else {
                $warrantyDetails = $aceWebAPI->checkWarrantyDetailsPerSKU($ace_warranty_id);
                if (!$warrantyDetails) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Terjadi kesalahan saat mendapatkan data pada API HO";
                    return false;
                }

                $customerAceOrderWarranty->setNewItemReplacementWarrantyDate(date('Y-m-d', strtotime($warrantyDetails['ReplaceExpired'])));
                $customerAceOrderWarranty->setSparePartWarrantyDate(date('Y-m-d', strtotime($warrantyDetails['SpareExpired'])));
                $customerAceOrderWarranty->setServiceWarrantyDate(date('Y-m-d', strtotime($warrantyDetails['ServiceExpired'])));

                $formattedSpareparts = [];
                if (isset($warrantyDetails['Spareparts']) && !empty($warrantyDetails['Spareparts'])) {
                    foreach ($warrantyDetails['Spareparts'] as $sparePart) {
                        array_push(
                            $formattedSpareparts,
                            array(
                                'sku' => $sparePart['SparePartSKU'],
                                'spare_part_name' => $sparePart['SparePartName'],
                                'sku_warranty_date' => $sparePart['SKUWarrantyDate']
                            )
                        );
                    }
                    $customerAceOrderWarranty->setSparePartsDetailWarranty(json_encode($formattedSpareparts));
                }

                $result = $customerAceOrderWarranty->update();
                if (false === $result) {
                    return $this->sendErrorResponse(
                        400,
                        implode('|', $customerAceOrderWarranty->getMessages())
                    );
                }

                $dataToReturn = array(
                    "change_new_item_warranty_date" => date('Y-m-d', strtotime($warrantyDetails['ReplaceExpired'])),
                    "spare_part_warranty_date" => date('Y-m-d', strtotime($warrantyDetails['SpareExpired'])),
                    "service_warranty" => date('Y-m-d', strtotime($warrantyDetails['ServiceExpired'])),
                    "status" => "Available",
                    "sku" => explode('-', $customerAceOrderWarranty->getReceiptIdAndSku())[1],
                    "receipt_number" => explode('-', $customerAceOrderWarranty->getReceiptIdAndSku())[0],
                    "warranty_number" => $customerAceOrderWarranty->getWarrantyNumber(),
                    "serial_number" => $customerAceOrderWarranty->getSerialNumber(),
                    "spareparts" => (isset($warrantyDetails['Spareparts']) && !empty($warrantyDetails['Spareparts'])) ?
                        json_encode($formattedSpareparts) :
                        []
                );
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat mendapatkan data daftar garansi";
            LogHelper::log("service_center_warranty_details", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return $dataToReturn;
    }

    public function validateWarrantyActivation($serialNumber, $warrantyNumber, $salesInvoiceItemId)
    {
        $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
        $salesInvoiceModel = new \Models\SalesInvoice();
        $salesInvoiceItemModel = new \Models\SalesInvoiceItem();

        $isValidSerialOrWarrantyNumber = $this->validateSerialNumberOrWarrantyNumber($serialNumber, $warrantyNumber);
        if (!$isValidSerialOrWarrantyNumber) {
            $this->errorCode = 400;
            $this->errorMessages = "Nomor Seri atau Nomor Garansi untuk SKU ini sudah tersimpan di database";

            return null;
        }

        $salesInvoiceItem = $salesInvoiceItemModel->findFirst("invoice_item_id = '" . $salesInvoiceItemId . "'")
            ->toArray();
        if (!$salesInvoiceItem) {
            $this->errorCode = 400;
            $this->errorMessages = "Data sales invoice item tidak ditemukan";

            return null;
        }

        $salesInvoice = $salesInvoiceModel->findFirst("invoice_id = '" . $salesInvoiceItem['invoice_id'] . "'")
            ->toArray();
        if (!$salesInvoice) {
            $this->errorCode = 400;
            $this->errorMessages = "Data sales invoice tidak ditemukan";

            return null;
        }

        $customerAceOrderWarranties = $customerAceOrderWarrantyModel->find("sales_invoice_item_id = '" . $salesInvoiceItemId . "'")
            ->toArray();
        if (count($customerAceOrderWarranties) >= $salesInvoiceItem['qty_ordered']) {
            $this->errorCode = 400;
            $this->errorMessages = "Aktifasi garansi untuk semua SKU ini sudah dilakukan";

            return null;
        }

        return array(
            'salesInvoiceItemSKU' => $salesInvoiceItem['sku'],
            'salesInvoiceReceiptId' => isset($salesInvoice['receipt_id']) ? $salesInvoice['receipt_id'] : $salesInvoice['invoice_no'],
            'salesInvoiceCreatedAt' => $salesInvoice['created_at']
        );
    }

    public function validateWarrantyActivationOfflineTransaction($serialNumber, $warrantyNumber, $receiptId, $sku)
    {
        $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();

        $isValidSerialOrWarrantyNumber = $this->validateSerialNumberOrWarrantyNumber($serialNumber, $warrantyNumber);
        if (!$isValidSerialOrWarrantyNumber) {
            $this->errorCode = 400;
            $this->errorMessages = "Nomor Seri atau Nomor Garansi untuk SKU ini sudah tersimpan di database";

            return null;
        }

        $receiptDetail = $this->getDigitalReceiptDataByReceiptId($receiptId);
        if (!$receiptDetail) {
            return $this->sendErrorResponse(
                400,
                "Gagal mendapatkan data receipt"
            );
        }

        $arrSKUItems = array_reduce($receiptDetail['products'], function ($result, $item) {
            $item = (array) $item;
            $key = $item['sku'];
            unset($item['sku']);

            $result[$key] = $item;

            return $result;
        }, array());

        $customerAceOrderWarranties = $customerAceOrderWarrantyModel->find("receipt_id_and_sku = '" . $receiptDetail['receipt_id'] . "-" . $sku . "'")
            ->toArray();
        if (count($customerAceOrderWarranties) >= $arrSKUItems[$sku]['qty']) {
            $this->errorCode = 400;
            $this->errorMessages = "Aktifasi garansi untuk semua SKU ini sudah dilakukan";

            return null;
        }

        return array(
            'sku' => $sku,
            'receiptId' => $receiptId,
            'orderDate' => $receiptDetail['order_date']
        );
    }

    public function serviceCenterActivateWarranty($customerId, $salesInvoiceItemId, $receiptIdAndSku, $payload)
    {
        $aceWarrantyId = null;

        if (empty($payload)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "Data untuk mengatifkan garansi tidak boleh kosong";
            return null;
        }

        try {
            $aceWebAPI = new \Library\AceWebAPI();

            LogHelper::log("warranty_activation_payload", "PAYLOAD: " . json_encode($payload));

            $response = $aceWebAPI->registerWarranty($payload);
            if (!$response) {
                throw new Exception(
                    "Terjadi kesalahan saat melakukan transaksi API aktivasi garansi"
                );
            }

            if ($response['is_ok'] == false) {
                throw new Exception(
                    $response['message']
                );
            }

            $aceWarrantyId = $response['data']['WarrantyId'];

            // Save to table customer_ace_order_warranty
            $saveData = $this->saveDataToCustomerAceOrderWarranty([
                'customer_id' => $customerId,
                'sales_invoice_item_id' => $salesInvoiceItemId,
                'receipt_id_and_sku' => $receiptIdAndSku,
                'warranty_code' => $response['data']['WarrantyCode'],
                'ace_warranty_id' => $response['data']['WarrantyId'],
                'serial_number' => $response['data']['SerialNumber'],
                'warranty_number' => $response['data']['WarrantyNumber']
            ]);
            if (!$saveData) {
                $this->errorCode = $this->getErrorCode();
                $this->errorMessages = $this->getErrorMessages();
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat mendapatkan data daftar garansi";
            LogHelper::log("service_center_warranty_details", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return $aceWarrantyId;
    }

    public function getServiceWarrantyDataPerCustomerWithOfflineFlag($customerId, $isOffline = false)
    {
        $orderedResults = [];
        $results = [];

        $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
        $customerAceWarrantyServiceModel = new \Models\CustomerAceWarrantyService();
        $salesInvoiceItemModel = new \Models\SalesInvoiceItem();
        $productModel = new \Models\Product();

        $querySearch = "";
        if ($isOffline) {
            $querySearch = "customer_id = $customerId AND receipt_id_and_sku IS NOT NULL";
        } else {
            $querySearch = "customer_id = $customerId AND sales_invoice_item_id IS NOT NULL";
        }

        $customerAceOrderWarranties = $customerAceOrderWarrantyModel->find($querySearch)->toArray();

        if (!empty($customerAceOrderWarranties)) {
            foreach ($customerAceOrderWarranties as $customerAceOrderWarranty) {
                $customerAceWarrantyServices = $customerAceWarrantyServiceModel
                    ->find("customer_ace_order_warranty_id = '" . $customerAceOrderWarranty['customer_ace_order_warranty_id'] . "'")
                    ->toArray();
                if (!$customerAceWarrantyServices) {
                    continue;
                }

                $sku = null;
                if ($isOffline) {
                    $sku = explode('-', $customerAceOrderWarranty['receipt_id_and_sku'])[1];
                    if (!$sku) {
                        throw new Exception(
                            "Terjadi kesalahan saat mendapatkan data sku dari receipt customer"
                        );
                    }
                } else {
                    $salesInvoiceItem = $salesInvoiceItemModel->findFirst("invoice_item_id = '" . $customerAceOrderWarranty['sales_invoice_item_id'] . "'")->toArray();
                    if (!$salesInvoiceItem) {
                        $this->errorCode = 400;
                        $this->errorMessages = "Terjadi kesalahan saat mendapatkan data invoice item milik customer";

                        return null;
                    }

                    $sku = $salesInvoiceItem['sku'];
                }

                $elasticProduct = $productModel->searchProductFromElastic($sku);

                foreach ($customerAceWarrantyServices as $customerAceWarrantyService) {
                    array_push(
                        $results,
                        array(
                            "sku" => $elasticProduct['variants'][0]['sku'],
                            "sku_name" => $elasticProduct['name'],
                            "sku_image" => $elasticProduct['variants'][0]['images'][0]['image_url'],
                            "ace_service_number" => $customerAceWarrantyService['ace_service_number'],
                            "service_status" => $customerAceWarrantyService['status'],
                            "created_at" => $customerAceWarrantyService['created_at']
                        )
                    );
                }
            }
            $orderedResults = GeneralHelper::array_sort_by_created_at($results, "DESC");
        }

        return $orderedResults;
    }

    public function displaceNonMembershipDataToMembershipTable($customerId, $aceCustomerId)
    {
        $result = false;

        $customerAceOrderWarrantyModel = new \Models\CustomerAceOrderWarranty();
        $customerAceWarrantyServiceModel = new \Models\CustomerAceWarrantyService();
        $customerAceOrderWarrantyNonMembershipModel = new \Models\CustomerAceOrderWarrantyNonMembership();
        $customerAceWarrantyServiceNonMembershipModel = new \Models\CustomerAceWarrantyServiceNonMembership();

        $customerAceOrderNonMembershipWarranties = $customerAceOrderWarrantyNonMembershipModel
            ->find("ace_customer_id = '$aceCustomerId' AND status = 0");

        if ($customerAceOrderNonMembershipWarranties->count()) {
            try {
                $customerAceOrderWarrantyModel->useWriteConnection();
                $customerAceWarrantyServiceModel->useWriteConnection();
                $manager = new TxManager();
                $manager->setDbService($customerAceOrderWarrantyModel->getConnection());
                $manager->setDbService($customerAceWarrantyServiceModel->getConnection());
                $transaction = $manager->get();
                $customerAceOrderWarrantyModel->setTransaction($transaction);
                $customerAceWarrantyServiceModel->setTransaction($transaction);

                foreach ($customerAceOrderNonMembershipWarranties as $item) {
                    $customerAceNonMembershipWarrantyServices = $customerAceWarrantyServiceNonMembershipModel
                        ->find('customer_ace_order_warranty_non_membership_id = ' . $item->getCustomerAceOrderWarrantyNonMembershipId());

                    $customerAceOrderWarrantyModel->assign(
                        [
                            'customer_id' => $customerId,
                            'receipt_id_and_sku' => $item->getReceiptIdAndSku(),
                            'warranty_code' => $item->getWarrantyCode(),
                            'ace_warranty_id' => $item->getAceWarrantyId(),
                            'serial_number' => !empty($item->getSerialNumber()) ? $item->getSerialNumber() : null,
                            'warranty_number' => !empty($item->getWarrantyNumber()) ? $item->getWarrantyNumber() : null,
                            'new_item_replacement_warranty_date' => !empty($item->getNewItemReplacementWarrantyDate())
                                ? $item->getNewItemReplacementWarrantyDate() : null,
                            'spare_part_warranty_date' => !empty($item->getSparePartWarrantyDate())
                                ? $item->getSparePartWarrantyDate() : null,
                            'service_warranty_date' => !empty($item->getServiceWarrantyDate())
                                ? $item->getServiceWarrantyDate() : null,
                            'spare_parts_detail_warranty' => !empty($item->getSparePartsDetailWarranty())
                                ? $item->getSparePartsDetailWarranty() : null,
                            'created_at' => date('Y-m-d'),
                            'updated_at' => date('Y-m-d')
                        ]
                    );
                    if (false === $customerAceOrderWarrantyModel->save()) {
                        throw new Exception(
                            implode('|', $customerAceOrderWarrantyModel->getMessages())
                        );
                    }

                    if ($customerAceNonMembershipWarrantyServices->count()) {
                        $customerAceNonMembershipWarrantyServices = $customerAceNonMembershipWarrantyServices->toArray();
                        foreach ($customerAceNonMembershipWarrantyServices as $itemService) {
                            $customerAceWarrantyServiceModel->assign([
                                'customer_ace_order_warranty_id' => $customerAceOrderWarrantyModel->customer_ace_order_warranty_id,
                                'ace_service_number' => $itemService['ace_service_number'],
                                'status' => $itemService['status'],
                                'service_warranty_charge' => $itemService['service_warranty_charge'],
                                'service_warranty_items' => $itemService['service_warranty_items'],
                                'service_date' => $itemService['service_date'],
                                'service_expected_finished_date' => $itemService['service_expected_finished_date']
                            ]);

                            if (false === $customerAceWarrantyServiceModel->save()) {
                                throw new Exception(
                                    implode('|', $customerAceWarrantyServiceModel->getMessages())
                                );
                            }
                        }
                    }

                    $item->setStatus(10);

                    $result = $item->update();
                    if (false === $result) {
                        throw new Exception(
                            implode('|', $customerAceOrderWarrantyModel->getMessages())
                        );
                    }
                }
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback($ex->getMessage());

                LogHelper::log("displace_membership_data", "MESSAGE: " . $ex->getMessage() . ", ACE_CUSTOMER_ID: " . $aceCustomerId);

                $this->errorCode = 503;
                $this->errorMessages = $ex->getMessage();
            }
        }
        $result = true;

        return $result;
    }

    #region Email Sender
    public function sendEmailItemReceivedByStore($customerEmail, $customerFirstName, $customerLastName, $aceServiceNumber, $isOffline = false)
    {
        $emailHelper = new Email();
        $emailHelper->setEmailSender();
        $emailHelper->setName($customerLastName, $customerFirstName);
        $emailHelper->setEmailReceiver($customerEmail);
        $emailHelper->setserviceAceServiceNumber($aceServiceNumber);


        $deepLink = "https://acemobileapp.page.link/?link=https://www.ruparupa.com/ace/service-center?id=" . $aceServiceNumber .
            "&apn=id.co.acehardware.acerewards&isi=1056027974&ibi=id.co.acehardware.ACE-Hardware-Indonesia";
        if ($isOffline) {
            $deepLink = "https://acerewards.page.link/?link=https://www.ruparupa.com/ace/service-center?idOffline=" . $aceServiceNumber .
                "&apn=id.co.acehardware.acerewards&isi=1056027974&ibi=id.co.acehardware.ACE-Hardware-Indonesia&cid=7653943913344598126&_icp=1";
        }
        $emailHelper->setServiceWarrantyChargeLinkConfirmation($deepLink);

        $emailTag = 'ace_service_center_item_received';
        $companyCode = 'AHI';
        $templateID = GeneralHelper::getTemplateId($emailTag, $companyCode);

        $emailHelper->setEmailTag($emailTag);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
    }

    public function sendEmailPaymentConfirmation($customerEmail, $customerFirstName, $customerLastName, $aceServiceNumber, $isOffline = false)
    {
        $emailHelper = new Email();
        $emailHelper->setEmailSender();
        $emailHelper->setName($customerLastName, $customerFirstName);
        $emailHelper->setEmailReceiver($customerEmail);

        $deepLink = "https://acemobileapp.page.link/?link=https://www.ruparupa.com/ace/service-center?id=" . $aceServiceNumber .
            "&apn=id.co.acehardware.acerewards&isi=1056027974&ibi=id.co.acehardware.ACE-Hardware-Indonesia";
        if ($isOffline) {
            $deepLink = "https://acerewards.page.link/?link=https://www.ruparupa.com/ace/service-center?idOffline=" . $aceServiceNumber .
                "&apn=id.co.acehardware.acerewards&isi=1056027974&ibi=id.co.acehardware.ACE-Hardware-Indonesia&cid=7653943913344598126&_icp=1";
        }
        $emailHelper->setServiceWarrantyChargeLinkConfirmation($deepLink);

        $emailTag = 'ace_service_center_payment_information';
        $companyCode = 'AHI';
        $templateID = GeneralHelper::getTemplateId($emailTag, $companyCode);

        $emailHelper->setEmailTag($emailTag);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
    }

    public function sendEmailPaymentDisapproval($customerEmail, $customerFirstName, $customerLastName)
    {
        $emailHelper = new Email();
        $emailHelper->setEmailSender();
        $emailHelper->setName($customerLastName, $customerFirstName);
        $emailHelper->setEmailReceiver($customerEmail);

        $emailTag = 'ace_service_center_payment_disapproval';
        $companyCode = 'AHI';
        $templateID = GeneralHelper::getTemplateId($emailTag, $companyCode);

        $emailHelper->setEmailTag($emailTag);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
    }

    public function sendEmailPaymentAgreement($customerEmail, $customerFirstName, $customerLastName, $serviceWarrantyCharge)
    {
        $emailHelper = new Email();
        $emailHelper->setEmailSender();
        $emailHelper->setName(
            $customerLastName,
            $customerFirstName
        );
        $emailHelper->setEmailReceiver($customerEmail);
        $emailHelper->setServiceWarrantyCharge(
            GeneralHelper::formatNumberToRupiah($serviceWarrantyCharge, 0)
        );

        $emailTag = 'ace_service_center_payment_agreement';
        $companyCode = 'AHI';
        $templateID = GeneralHelper::getTemplateId($emailTag, $companyCode);

        $emailHelper->setEmailTag($emailTag);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
    }

    public function sendEmailReadyToPickup($customerEmail, $customerFirstName, $customerLastName)
    {
        $emailHelper = new \Library\Email();
        $emailHelper->setEmailSender();
        $emailHelper->setName($customerLastName, $customerFirstName);
        $emailHelper->setEmailReceiver($customerEmail);

        $emailTag = 'ace_service_center_ready_to_pickup';
        $companyCode = 'AHI';
        $templateID = GeneralHelper::getTemplateId($emailTag, $companyCode);

        $emailHelper->setEmailTag($emailTag);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
    }

    public function sendEmailFinishedNotification($customerEmail, $customerFirstName, $customerLastName)
    {
        $emailHelper = new \Library\Email();
        $emailHelper->setEmailSender();
        $emailHelper->setName($customerLastName, $customerFirstName);
        $emailHelper->setEmailReceiver($customerEmail);

        $emailTag = 'ace_service_center_finished_notification';
        $companyCode = 'AHI';
        $templateID = GeneralHelper::getTemplateId($emailTag, $companyCode);

        $emailHelper->setEmailTag($emailTag);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
    }
    #endregion
}
