<?php

namespace Library\AceHardware;

use Exception;
use Helpers\LogHelper;
use Library\KawanLamaSystem;
use Library\Employee;

class StoreReferral
{
    protected $errorCode;
    protected $errorMessages;
    protected $kawanLamaSystemLib;
    protected $employeeLib;

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();
        $this->employeeLib = new Employee();
    }

    public function saveStoreReferral($params = array())
    {
        // $customerId, $employeeReferralNip, $type, $salesOrderId
        if (empty($params['customer_id']) || empty($params['employee_referral_nip']) || empty($params['type'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Mandatory parameter not specified";

            return null;
        }

        if ($params['type'] === 'order' && empty($params['sales_order_id'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "sales_order_id parameter not specified";

            return null;
        }

        $customerAceStoreReferralModel = new \Models\CustomerAceStoreReferral();
        try {
            $employeeResult = $this->employeeLib->getEmployeeDataByNip(
                array('nip' => $params['employee_referral_nip'])
            );
            if (empty($employeeResult)) {
                $this->errorCode = 'RR106';
                $this->errorMessages[] = "Employee data not found";

                LogHelper::log(
                    "store_referral_save",
                    "MESSAGE: Employee Referral NIP " . $params['employee_referral_nip'] . " Not found when saving data with customer id " .
                        $params['customer_id'] . " and type " . $params['type']
                );

                return null;
            }

            $customerAceStoreReferralModel->assign([
                'customer_id' => $params['customer_id'],
                'sales_order_id' => isset($params['sales_order_id']) ? $params['sales_order_id'] : null,
                'employee_referral_nip' => $employeeResult['nip'],
                'employee_referral_name' => $employeeResult['name'],
                'employee_referral_site_code_sap' => isset($employeeResult['site_code_sap']) ? $employeeResult['site_code_sap'] : null,
                'type' => $params['type']
            ]);

            if (false === $customerAceStoreReferralModel->save()) {
                $this->errorCode = 400;
                $this->errorMessages[] = implode('|', $customerAceStoreReferralModel->getMessages());

                LogHelper::log(
                    "store_referral_save",
                    "MESSAGE: Something wrong happened when saving data with " .
                        "Employee Referral NIP " . $params['employee_referral_nip'] . ", customer id " .
                        $params['customer_id'] . " and type " . $params['type']
                );

                return null;
            }
        } catch (Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat menyimpan data store referral";

            LogHelper::log("store_referral_save", "MESSAGE: " . $this->errorMessages);

            return null;
        }

        return true;
    }
}
