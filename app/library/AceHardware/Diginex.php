<?php

namespace Library\AceHardware;

use Exception;
use Helpers\LogHelper;
use Library\KawanLamaSystem;
use Library\Marketing;
use Library\MongodbAutomata;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class Diginex
{
    protected $errorCode;
    protected $errorMessages;
    protected $kawanLamaSystemLib;
    protected $marketingLib;
    protected $mongoDbAutomataLib;

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {
        $this->kawanLamaSystemLib = new KawanLamaSystem();
        $this->marketingLib = new Marketing();
        $this->mongoDbAutomataLib = new MongodbAutomata();
    }

    public function getMongoDBDataForCheckCustomerRedeemKioskScan($ace_customer_member_no)
    {
        $dataToReturn = array();

        $this->mongoDbAutomataLib->setCollection(getenv('DIGINEX_CUSTOMER_COLLECTION'));
        $collection = $this->mongoDbAutomataLib->getCollection();
        $getCustomerDataPerToday = $collection->findOne([
            'ace_customer_member_no'    => $ace_customer_member_no,
            'check_in_source'           => 'kiosk',
            'scan_kiosk_type'           => 'claim',
            'is_process'                => false,
            'check_in_time'             => [
                '$gt'   => date('Y-m-d 00:00:00'),
                '$lt'   => date('Y-m-d H:i:s')
            ],
        ], ['sort' => ['_id' => -1]]);

        if ($getCustomerDataPerToday) {
            $dataToReturn = $getCustomerDataPerToday;

            $getCustomerDataPerToday['is_process'] = true;
            $collection->findOneAndReplace(['_id' => $getCustomerDataPerToday['_id']], $getCustomerDataPerToday);
        }

        return $dataToReturn;
    }

    public function writeDiginexCheckInCustomerToMongo($arrParams)
    {
        $dataToInsertToMongo = $arrParams;

        $this->mongoDbAutomataLib->setCollection(getenv('DIGINEX_CUSTOMER_COLLECTION'));
        $collection = $this->mongoDbAutomataLib->getCollection();
        $getCustomerDataPerToday = $collection->findOne([
            'ace_customer_id'   => $arrParams['ace_customer_id'],
            'store_code'        => $arrParams['store_code'],
            'check_in_time'     => [
                '$gte'   => date('Y-m-d 00:00:00'),
                '$lt' => date('Y-m-d H:i:s')
            ],
        ], ['sort' => ['_id' => -1]]);

        if ($arrParams['check_in_source'] === 'kiosk') {
            if (!$getCustomerDataPerToday) {
                $dataToInsertToMongo['same_day_check_in_count'] = 1;
                $dataToInsertToMongo['is_need_redirection'] = true;
            } else {
                $dataToInsertToMongo['same_day_check_in_count'] =
                    $getCustomerDataPerToday['same_day_check_in_count'] + 1;
                $dataToInsertToMongo['is_need_redirection'] = true;
            }
        } else {
            if (!$getCustomerDataPerToday) {
                $dataToInsertToMongo['same_day_check_in_count'] = 1;
                $dataToInsertToMongo['is_need_redirection'] = true;
            } else {
                $dataToInsertToMongo['same_day_check_in_count'] =
                    $getCustomerDataPerToday['same_day_check_in_count'] + 1;
                $dataToInsertToMongo['is_need_redirection'] = true;
            }
        }

        return $collection->insertOne($dataToInsertToMongo);
    }

    public function getMongoDBDataForCustomerSalesAssistantRequest($ticket_number)
    {
        $dataToReturn = array();

        $this->mongoDbAutomataLib->setCollection(getenv('DIGINEX_CUSTOMER_ASSISTANT_COLLECTION'));
        $collection = $this->mongoDbAutomataLib->getCollection();
        $getCustomerSalesAssistantRequest = $collection->findOne([
            'ticket_number'             => $ticket_number
        ], ['sort' => ['_id' => -1]]);

        if ($getCustomerSalesAssistantRequest) {
            $dataToReturn = $getCustomerSalesAssistantRequest;

            if (
                strtotime(date('Y-m-d H:i:s')) - strtotime($getCustomerSalesAssistantRequest['updated_at']) >= 30 &&
                $getCustomerSalesAssistantRequest['status'] === "menunggu" &&
                $getCustomerSalesAssistantRequest['is_need_closing'] === false
            ) {
                $dataToReturn['is_need_closing'] = true;
            } else {
                $dataToReturn['is_need_closing'] = false;
            }

            unset($dataToReturn['_id']);
            unset($dataToReturn['created_at']);
            unset($dataToReturn['updated_at']);
        }

        return $dataToReturn;
    }

    public function writeDiginexCustomerSalesAssistantToMongo($arrParams)
    {
        $dataToInsertToMongo = $arrParams;

        $this->mongoDbAutomataLib->setCollection(getenv('DIGINEX_CUSTOMER_ASSISTANT_COLLECTION'));
        $collection = $this->mongoDbAutomataLib->getCollection();
        $getCustomerSalesAssistantRequest = $collection->findOne([
            'ace_customer_member_no'    => $arrParams['ace_customer_member_no'],
            'ticket_number'             => $arrParams['ticket_number']
        ], ['sort' => ['_id' => -1]]);

        if ($getCustomerSalesAssistantRequest) {
            $dataToInsertToMongo = $getCustomerSalesAssistantRequest;

            $dataToInsertToMongo['sales_assistant_id'] = $arrParams['sales_assistant_id'];
            $dataToInsertToMongo['sales_assistant_name'] = $arrParams['sales_assistant_name'];
            $dataToInsertToMongo['status'] = $arrParams['status'];
            $dataToInsertToMongo['is_need_closing'] = $arrParams['is_need_closing'];
            $dataToInsertToMongo['updated_at'] = $arrParams['updated_at'];

            $collection->findOneAndReplace(['_id' => $getCustomerSalesAssistantRequest['_id']], $dataToInsertToMongo);
        } else {
            if ($arrParams['status'] === 'menunggu') {
                $dataToInsertToMongo['request_source'] = 'ace (mobile apps)';
            } else {
                $dataToInsertToMongo['request_source'] = 'kiosk';
            }
            $collection->insertOne($dataToInsertToMongo);
        }

        return $dataToInsertToMongo;
    }

    /**
     * Validate does the customer already use the voucher 10
     * times in a month
     */
    public function validateDiginexVoucherUsageCountPerMonth($customer_id)
    {
        $isValid = true;

        $salesRuleVoucherModel = new \Models\SalesruleVoucher();
        try {
            $diginexVouchers = $salesRuleVoucherModel->getVoucherUsageAndLimitByRuleIDCustomerIDAndDate(
                $customer_id,
                getenv("DIGINEX_RULE_ID"),
                date('Y-m-01'), // First day of the current month
                date('Y-m-t') // Last day of the current month
            );

            if ($diginexVouchers) {
                if (
                    ($diginexVouchers[0]['created_at'] >= date('Y-m-d') &&
                        $diginexVouchers[0]['times_used'] > 0
                    ) ||
                    ($diginexVouchers[0]['times_used'] > 0 &&
                        $diginexVouchers[0]['voucher_usage_created_at'] >= date('Y-m-d')
                    )
                ) {
                    $isValid = false;
                    $this->errorCode = 400;
                    $this->errorMessages = "Customer sudah melakukan claim & menggunakan voucher hari ini";
                } else {
                    $arrVoucherUsed = array_filter($diginexVouchers, function ($val) {
                        return ($val['times_used'] > 0);
                    });

                    if (count($arrVoucherUsed) >= 10) {
                        $isValid = false;
                        $this->errorCode = 400;
                        $this->errorMessages = "Customer sudah claim dan menggunakan voucher sebanyak 10 kali selama periode bulan berjalan";
                    }
                }
            }
        } catch (Exception $ex) {
            $isValid = false;

            LogHelper::log(
                "diginex_claim_voucher",
                "EXCEPTION OCCURED When validating diginex voucher usage: " . $ex->getMessage() .
                    " ,CUSTOMER ID: " . $customer_id
            );
            $this->errorCode = 500;
            $this->errorMessages = $ex->getMessage();
        }

        return $isValid;
    }

    public function claimDiginexVoucher($customer_id, $store_code, $store_name = '')
    {
        $salesRuleVoucher = null;

        $salesRuleVoucherModel = new \Models\SalesruleVoucher();
        $salesRuleVoucherModel->useWriteConnection();
        $manager = new TxManager();
        $manager->setDbService($salesRuleVoucherModel->getConnection());
        $transaction = $manager->get();
        $salesRuleVoucherModel->setTransaction($transaction);

        try {
            $voucherGeneratorParams = [
                'rule_id'           => getenv('DIGINEX_RULE_ID'),
                'customer_id'       => $customer_id,
                'company_code'      => 'ODI',
                'type'              => 3, // Type = General
                'expiration_date'   => date("Y-m-d 23:59:59"),
                'qty'               => 1,
                'length'            => 8,
                'voucher_amount'    => getenv('DIGINEX_VOUCHER_AMOUNT'),
                'prefix'            => getenv('DIGINEX_REDEEM_PREFIX'),
                'format'            => 'alphanum',
                'note'              => "$store_code;$store_name"
            ];
            $this->marketingLib->generateVoucher($voucherGeneratorParams);
            $this->marketingLib->validateGeneratedVoucher($voucherGeneratorParams);
            $voucherGeneratorParams['voucher_code'] = $this->marketingLib->getVoucherList()[0];

            $freshCustomerDiginexVoucher = $salesRuleVoucherModel->findFirst(
                "customer_id = '$customer_id' AND rule_id= '" . getenv('DIGINEX_RULE_ID') . "' AND DATE(created_at) <= '" . date('Y-m-t') . "' AND times_used = 0"
            );
            if (!$freshCustomerDiginexVoucher) {
                // Save to table salesrule_voucher
                $salesRuleVoucher = $salesRuleVoucherModel->assign($voucherGeneratorParams);
                if (false === $salesRuleVoucherModel->save()) {
                    throw new Exception(
                        implode('|', $salesRuleVoucherModel->getMessages())
                    );
                }
            } else {
                $freshCustomerDiginexVoucher->setVoucherCode($voucherGeneratorParams['voucher_code']);
                $freshCustomerDiginexVoucher->setExpirationDate($voucherGeneratorParams['expiration_date']);
                $freshCustomerDiginexVoucher->setNote($voucherGeneratorParams['note']);
                $freshCustomerDiginexVoucher->setCreatedAt(date('Y-m-d H:i:s'));

                $result = $freshCustomerDiginexVoucher->update();
                if (false === $result) {
                    throw new Exception(
                        implode('|', $freshCustomerDiginexVoucher->getMessages())
                    );
                }

                // Because update doesn't return the updated data
                $salesRuleVoucher = $freshCustomerDiginexVoucher;
            }

            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback($ex->getMessage());

            LogHelper::log(
                "diginex_claim_voucher",
                "EXCEPTION OCCURED When generate claim voucher: " . $ex->getMessage() .
                    " ,CUSTOMER ID: " . $customer_id
            );

            $this->errorCode = 500;
            $this->errorMessages = $ex->getMessage();

            return false;
        }

        return $salesRuleVoucher;
    }

    public function saveSalesAssistant($params = array())
    {
        $customerAceStoreReferralModel = new \Models\CustomerAceStoreReferral();
        try {
            $customerReferral = $customerAceStoreReferralModel->findFirst(
                "customer_id = '" . $params['customer_id'] . "' AND sales_assistant_ticket_no = '" . $params['sales_assistant_ticket_no'] . "'"
            );
            if (empty($customerReferral)) {
                $customerAceStoreReferralModel->assign($params);
                if (false === $customerAceStoreReferralModel->save()) {
                    $this->errorCode = 500;
                    $this->errorMessages[] = implode('|', $customerAceStoreReferralModel->getMessages());
                    LogHelper::log(
                        "save_sales_assistant",
                        "MESSAGE: Something wrong happened when saving data with " .
                            "\nEmployee Referral NIP :" . $params['employee_referral_nip'] . "\nCustomer ID :" .
                            $params['customer_id'] . "\nType :" . $params['type']
                    );

                    return false;
                }
            } else {
                $customerReferral->setStatus($params['status']);

                $result = $customerReferral->update();
                if (false === $result) {
                    throw new Exception(
                        implode('|', $customerReferral->getMessages())
                    );
                }
            }
        } catch (Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Terjadi kesalahan saat menyimpan data sales assistant";
            LogHelper::log("save_sales_assistant", "MESSAGE: " . $this->errorMessages);

            return false;
        }

        return true;
    }
}
