<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/12/2017
 * Time: 4:00 PM
 */

namespace Library;


class MongodbAutomata extends \Phalcon\DI\Injectable
{
    /**
     * @var \MongoDB\Database
     */
    protected $mongoObj;

    /**
     * @var \MongoDB\Collection
     */
    protected $collection;

    function __construct()
    {
        $this->mongoObj = (new \MongoDB\Client(
            $_ENV['MONGO_URI_AUTOMATA'],
            [],
            [
                'typeMap' => [
                    'array' => 'array',
                    'document' => 'array',
                    'root' => 'array',
                ],
            ]
        ))->{$_ENV['MONGO_DB_AUTOMATA']};
    }

    /**
     * @return mixed
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param mixed $collection
     */
    public function setCollection($collection)
    {

        $this->collection = $this->mongoObj->{$collection};
    }
}
