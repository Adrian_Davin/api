<?php

namespace Library\Shipping;

use Library\APIWrapper;

class NCS
{
    protected $invoice_no;

    public function __construct()
    {

    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);


        return $thisArray;
    }

    public function generateAWB($params = array()) {
        $apiWrapper = new APIWrapper(getenv('SHIPMENT_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("ncs/pickup_request");
        $awbNumber = '';
        
        if($apiWrapper->send("post")) {
            $apiWrapper->formatResponse();
            $resultData = $apiWrapper->getData();
            if (is_array($resultData)) {
                $awbNumber = $resultData['awb'];
            }
            
            \Helpers\LogHelper::log('shipment_ncs_awb','[SUCCESS] generate ' . json_encode($resultData));
        } else {
            \Helpers\LogHelper::log('shipment_ncs_awb','[ERROR] when calling shipment API ' . json_encode($params));
        }

        return $awbNumber;
    }

}