<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/22/2017
 * Time: 2:30 PM
 */

namespace Library\Shipping;


class SAPExpress
{
    protected $allocation_code;
    protected $shipment_date_string;
    protected $shipment_time_string;
    protected $unique_reference_no;
    protected $company_id;
    protected $company_name;
    protected $city_origin_name;
    protected $city_destination_name;
    protected $service_name;
    protected $quantity;
    protected $weight;
    protected $insurance_flag = 0;
    protected $cod_shipment_cost_flag = 0;
    protected $cod_goods_cost_flag = 0;
    protected $goods_is_document_flag = 0;
    protected $goods_need_packing_flag = 0;
    protected $goods_is_boxed_flag = 0;
    protected $goods_value_are;
    protected $goods_is_high_values_flag = 0;
    protected $goods_is_electronic_flag = 0;
    protected $goods_is_dangerous_flag = 0;
    protected $goods_is_live_animal_flag = 0;
    protected $goods_is_live_plant_flag = 0;
    protected $goods_is_food_flag = 0;
    protected $goods_textile_flag = 0;
    protected $shipment_req_pickup_flag = 1;
    protected $shipper_name;
    protected $shipper_province_name;
    protected $shipper_district_name;
    protected $shipper_subdistrict_code;
    protected $shipper_subdistrict_name;
    protected $shipper_address;
    protected $shipper_handphone_number;
    protected $recipient_name;
    protected $recipient_province_name;
    protected $recipient_district_name;
    protected $recipient_subdistrict_code;
    protected $recipient_subdistrict_name;
    protected $recipient_address;
    protected $recipient_handphone_number;

    public function __construct()
    {
        $this->company_name = $_ENV['COMPANY_NAME'];
        $this->service_name = $_ENV['SERVICE_NAME'];
        $this->shipper_name = $_ENV['SHIPPER_NAME'];
        $this->shipper_handphone_number = $_ENV['SHIPPER_HANDPHONE_NUMBER'];
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);


        return $thisArray;
    }

    public function generateAWB($username = FALSE, $password = FALSE)
    {
        $client = new \GuzzleHttp\Client();
        $curlParam = [
            'timeout'=>'20.0',
            'auth' =>  [$username, $password],
            'form_params' => $this->getDataArray()
        ];

        if(!empty($_ENV['HTTP_PROXY'])) {
            $curlParam['proxy'] = ["http" => $_ENV['HTTP_PROXY']];
        }
        try {
            $response = $client->request('POST',$_ENV['SAP_AWB_END_POINT'],$curlParam );
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("shipping_awb","Error when generate awb invoice no : " .$this->unique_reference_no." error : " . $e->getMessage());
            return null;
        }

        $responseArray = json_decode($response->getBody()->getContents(), true);

        if(empty($responseArray['status']['error'])) {
            return $responseArray['status']['awb'];
        } else {
            return null;
        }
    }

    public function checkAWB($track_number = FALSE)
    {
        $courierCredentialModel = new \Models\ShippingCourierCredential();
        $courierData = $courierCredentialModel->getFromCache(['shipping_assignation' => 'DC']);

        $client = new \GuzzleHttp\Client();
        $curlParam = [
            'timeout'=>'20.0',
            'auth' =>  [$courierData['username'], $courierData['password']]
        ];

        if(!empty($_ENV['HTTP_PROXY'])) {
            $curlParam['proxy'] = ["http" => $_ENV['HTTP_PROXY']];
        }

        $responseArray = array();
        try {
            $response = $client->request('GET', $_ENV['SAP_CHECK_AWB'].$track_number, $curlParam );
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("shipping_awb","Error when checking awb no : " . $track_number . ", error : " . $e->getMessage());
        }

        return $responseArray;
    }

}