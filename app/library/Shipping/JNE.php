<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 20/10/2017
 * Time: 2:30 PM
 */

namespace Library\Shipping;

use Library\APIWrapper;
use Phalcon\Db as Database;
class JNE
{
    protected $username;
    protected $api_key;
    protected $OLSHOP_BRANCH;
    protected $OLSHOP_ORIG;
    protected $OLSHOP_ORDERID;
    protected $OLSHOP_SHIPPER_NAME;
    protected $OLSHOP_SHIPPER_ADDR1;
    protected $OLSHOP_SHIPPER_ADDR2;
    protected $OLSHOP_SHIPPER_ADDR3;
    protected $OLSHOP_SHIPPER_CITY;
    protected $OLSHOP_SHIPPER_REGION;
    protected $OLSHOP_SHIPPER_ZIP;
    protected $OLSHOP_SHIPPER_PHONE;
    protected $OLSHOP_CUST;
    protected $OLSHOP_RECEIVER_NAME;
    protected $OLSHOP_RECEIVER_ADDR1;
    protected $OLSHOP_RECEIVER_ADDR2;
    protected $OLSHOP_RECEIVER_ADDR3;
    protected $OLSHOP_RECEIVER_CITY;
    protected $OLSHOP_RECEIVER_REGION;
    protected $OLSHOP_RECEIVER_ZIP;
    protected $OLSHOP_RECEIVER_PHONE;
    protected $OLSHOP_DEST;
    protected $OLSHOP_SERVICE;
    protected $OLSHOP_QTY;
    protected $OLSHOP_WEIGHT;
    protected $OLSHOP_GOODSTYPE;
    protected $OLSHOP_GOODSDESC;
    protected $OLSHOP_INST;
    protected $OLSHOP_GOODSVALUE;
    protected $OLSHOP_INS_FLAG;

    public function __construct()
    {

    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);


        return $thisArray;
    }

    public function generateAWB()
    {
        $client = new \GuzzleHttp\Client();
        $curlParam = [
            'timeout' => '15.0',
            'form_params' => $this->getDataArray()
        ];

        if(!empty($_ENV['HTTP_PROXY'])) {
            $curlParam['proxy'] = ["http" => $_ENV['HTTP_PROXY']];
        }
        try {
            $response = $client->request('POST',$_ENV['JNE_GENERATE_AWB'],$curlParam );
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("shipping_awb","Error when generate awb invoice no : " .$this->OLSHOP_ORDERID." error : " . $e->getMessage());
            return null;
        }

        $responseArray = json_decode($response->getBody()->getContents(), true);
        \Helpers\LogHelper::log("shipping_awb",$this->OLSHOP_ORDERID.": ".json_encode($responseArray),"info");
        if (isset($responseArray['detail'][0]['status'])) {
            if($responseArray['detail'][0]['status'] == 'sukses') {
                return $responseArray['detail'][0]['cnote_no'];
            } else if ($responseArray['detail'][0]['status'] == 'Error' && isset($responseArray['detail'][0]['cnote_no'])) {
                if (!empty($responseArray['detail'][0]['cnote_no'])) {
                    return $responseArray['detail'][0]['cnote_no'];
                }
            }
        }        

        $msg = "Get Shipping AWB failed on : ".$this->OLSHOP_ORDERID." JNE. Caused by: ".json_encode($responseArray);

        //check if alert has requested or not
        // $invoiceModel = new \Models\SalesInvoice();
        // $sql = "SELECT msg from alert_logs where msg = '$msg'";
        // $result = $invoiceModel->getDi()->getShared('dbMaster')->query($sql);
        // $result->setFetchMode( Database::FETCH_ASSOC );
        // $resultData = $result->fetchAll()[0];
        
        // if(!empty($resultData['msg'])){
        //     return null;
        // }
        // //save alert on db 
        // $sql = "INSERT INTO alert_logs (invoice_no,msg,alert_type) VALUES ('".$this->OLSHOP_ORDERID."','".$msg."','awb')";
        // $invoiceModel->getDi()->getShared('dbMaster')->execute($sql);


        \Helpers\LogHelper::log("shipping_awb",$this->OLSHOP_ORDERID.": ".json_encode($responseArray));
        $params_data = array(
            "message" => $msg,
            "slack_channel" => $_ENV['SHIPPING_SLACK_CHANNEL'],
            "slack_username" => $_ENV['OPS_SLACK_USERNAME']
        );
        $this->notificationError($params_data);

        $carrier = $this->OLSHOP_SERVICE == 'REG'? 'JNE' : 'JTR';
        $reason = !empty($responseArray['detail'][0]['reason']) ? $responseArray['detail'][0]['reason'] : 'response AWB kosong';
        $notificationHelper = new \Library\Notification();
        $notificationHelper->SendNotificationDiscord([getenv('DISCORD_FAILED_GENERATE_AWB_WEBHOOK_URL')], sprintf("FAILED GENERATE AWB %s - %s", $carrier, $this->OLSHOP_ORDERID), sprintf("Gagal generate AWB dengan alasan **%s**",$reason), "FAILED");

        return null;
    }

    public function insertAWB($invoiceNo, $awbNo)
    {
        $client = new \GuzzleHttp\Client();
        $curlParam = [
            'timeout' => '20.0',
            'form_params' => array('username' => $_ENV['JNE_USERNAME'], 'api_key' => $_ENV['JNE_API_KEY'], 'ORDER_ID' => $invoiceNo, 'AWB_NUMBER' => $awbNo)
        ];

        if(!empty($_ENV['HTTP_PROXY'])) {
            $curlParam['proxy'] = ["http" => $_ENV['HTTP_PROXY']];
        }
        try {
            $response = $client->request('POST',$_ENV['JNE_INSERT_AWB'],$curlParam );
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("shipping_awb","Error when insert awb invoice no : " .invoiceNo." error : " . $e->getMessage());
            return null;
        }

        $responseArray = json_decode($response->getBody()->getContents(), true);
        if (isset($responseArray['status'])) {
            if (strtolower($responseArray['status']) == 'success') {
                // update insert_to_3pl to true based on track_number
                $shipmentModel = new \Models\SalesShipment();
                $sql = "UPDATE sales_shipment SET insert_to_3pl = 'true' WHERE track_number='" . $awbNo . "'";
                $shipmentModel->getDi()->getShared('dbMaster')->execute($sql);
            }
        }
    }

    public function checkAWB($track_number = FALSE)
    {
        $client = new \GuzzleHttp\Client();
        $curlParam = [
            'timeout' => '15.0',
            'form_params' => array('username' => $_ENV['JNE_USERNAME'], 'api_key' => $_ENV['JNE_API_KEY'])
        ];

        if(!empty($_ENV['HTTP_PROXY'])) {
            $curlParam['proxy'] = ["http" => $_ENV['HTTP_PROXY']];
        }

        $responseArray = array();
        try {
            \Helpers\LogHelper::log("shipping_awb_jne_latency","AWB no : " . $track_number . " - start");
            $response = $client->request('POST', $_ENV['JNE_CHECK_AWB'].'/'.$track_number, $curlParam );
            \Helpers\LogHelper::log("shipping_awb_jne_latency","AWB no : " . $track_number . " - end");
            $responseArray = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("shipping_awb","Error when checking awb no : " . $track_number . ", error : " . $e->getMessage());
        }

        return $responseArray;
    }

    public function generateJOB($params = array()) {
        $apiWrapper = new APIWrapper(getenv('SHIPMENT_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("jne/req_job");
        $jobNumber = '';
        try {
            if($apiWrapper->send("post")) {
                $apiWrapper->formatResponse();
                $resultData = $apiWrapper->getData();
                $jobNumber = (isset($resultData['no_tiket'])) ? $resultData['no_tiket'] : '';

                if (isset($resultData['reason']) && isset($params['INVOICE_NO'])) {
                    if ($resultData['reason'] == 'JOB Number Already Exist' && $params['INVOICE_NO'] != '') {
                        $jobNumber = 'BC-' . $params['INVOICE_NO'];
                    }
                }

                \Helpers\LogHelper::log('shipment_jne_job','[SUCCESS] generate ' . json_encode($resultData));
            } else {
                \Helpers\LogHelper::log('shipment_jne_job','[ERROR] when calling shipment API ' . json_encode($params));
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log('shipment_jne_job','[ERROR] when calling shipment API: ' . $e->getMessage());
        }

        return $jobNumber;
    }

    public function notificationError($params_data = array()){
        $notificationText = $params_data["message"];
        $params = [ 
            "channel" => $params_data["slack_channel"],
            "username" => $params_data["slack_username"],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];

        $nsq->publishCluster('transaction', $message);

        // send email to tech@ruparupa.atlassian.net
        // disabled because this code spam the ticket dashboard
        // $email = array(            
        //     "to" => "tech@ruparupa.atlassian.net",
        //     "from" => "help@ruparupa.com",            
        //     "companyCode" => "ODI",
        //     "subject" => "AWB ALERT",
        //     "msg" => $notificationText
        // );

        // $message = [
        //     "data" => $email,
        //     "message" => "sendWithoutTemplate"
        // ];
        
        // $nsq->publishCluster('email', $message);
    }

    public function getShippingLeadTime($params = array()) {
        try {
            $leadTime = array();
            $apiWrapper = new APIWrapper(getenv('SHIPMENT_API_V2'));
            $apiWrapper->setEndPoint("carrier/lead-time");
            $apiWrapper->setParam([
                "origin" => [
                    "kecamatan_id" => (int) $params["origin_kecamatan_id"]
                ],
                "destination" => [
                    "kecamatan_id" => (int) $params["dest_kecamatan_id"]
                ],
                "list_carrier_id" => [(int) $params["carrier_id"]]
            ]);

            \Helpers\LogHelper::log('shipment_lead_time', sprintf('Get lead time, request: %s', json_encode($params)), "info");
            
            $isSuccess = $apiWrapper->sendRequest("post");
            $apiWrapper->formatResponse();
            if (!$isSuccess) {
                $errBody = $apiWrapper->getErrorBody();
                throw new \Exception($errBody->message);
            }

            $respData = $apiWrapper->getData();
            \Helpers\LogHelper::log('shipment_lead_time', sprintf('Get lead time success, response: %s', json_encode($respData)), "info");
            if (!empty($respData)) {
                $leadTime = $respData[0]["lead_time"];
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log('shipment_lead_time', sprintf('Get lead time failed, error: %s', $e->getMessage()));
        }
       
        return $leadTime;
    }
}