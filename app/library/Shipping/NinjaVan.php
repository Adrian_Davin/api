<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 26/3/2018
 * Time: 2:30 PM
 */

namespace Library\Shipping;

class NinjaVan
{
    public function __construct()
    {

    }

    public function getToken()
    {
        $client = new \GuzzleHttp\Client();
        $body = '{ 
                    "client_id":"' . getenv('NINJAVAN_CLIENT_ID') . '",
                    "client_secret":"' . getenv('NINJAVAN_CLIENT_SECRET') . '",
                    "grant_type":"client_credentials"
                 }';

        $curlParam = [
            'timeout' => '20.0',
            'headers'  => ['content-type' => 'application/json'],
            'body' => $body
        ];

        try {
            $response = $client->request('POST', $_ENV['NINJAVAN_GET_TOKEN_URI'], $curlParam );
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("shipping_awb","[ninjavan]Error when get token, "." error : " . $e->getMessage());
            return null;
        }

        $responseArray = json_decode($response->getBody()->getContents(), true);
        if (!isset($responseArray['access_token'])) {
            return null;
        } else {
            return $responseArray['access_token'];
        }
    }

    public function generateAWB($body, $token)
    {
        $client = new \GuzzleHttp\Client();
        $curlParam = [
            'timeout' => '20.0',
            'headers'  => ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $token],
            'body' => $body
        ];

        try {
            $response = $client->request('POST', $_ENV['NINJAVAN_CREATE_AWB_URI'], $curlParam );
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("shipping_awb","[ninjavan]Error when generate awb, "." error : " . $e->getMessage());
            return null;
        }

        $responseArray = json_decode($response->getBody()->getContents(), true);
        if (!isset($responseArray['tracking_number'])) {
            return null;
        } else {
            return $responseArray['tracking_number'];
        }
    }
}