<?php

/**
 * Created by PhpStorm.
 * User: kevin.aldiansyah
 * Date: 11/04/2021
 * Time: 09:36 AM
 */

namespace Library;

use Helpers\ProductHelper,
    Phalcon\Db;
use mysql_xdevapi\Exception;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

use Helpers\LogHelper;

class CustomerReferral
{

    protected $errorCode;
    protected $errorMessages;
    protected $isDuplicateDevice;

    public function __construct()
    {
        $this->customer = new \Models\Customer();
        $this->isDuplicateDevice = false;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @return bool
     */
    public function getIsDuplicateDevice()
    {
        return $this->isDuplicateDevice;
    }

    public function setIsDuplicateDevice($value)
    {
        $this->isDuplicateDevice = $value;
    }

    public function getReferralRewardData($params = array()){
        // Additional params: name_search, date_from, date_to (YYYY-MM-DD), done_shopping ("10" / else)
        $searchQueryName = '';
        $searchQueryTime = '';
        $searchQueryTimePeriod = '';
        $searchQueryFirstPurchase = '';
        // set true by default, pass "0" for "Sudah Daftar" tab, "10" for "Sudah Belanja" tab
        $isDoneShopping = !isset($params['done_shopping']) || (isset($params['done_shopping']) && $params['done_shopping'] == "10");

        if (isset($params['name_search']) && !empty($params['name_search'])) {
            $lowerSearch = strtolower($params['name_search']);
            $searchQueryName = ' AND (LOWER(c.first_name) LIKE "%'. $lowerSearch .'%" OR LOWER(c.last_name) LIKE "%'. $lowerSearch .'%") ';
        }

        if ($isDoneShopping) {
            $selectDate = 'cre.created_at';
            $selectDatePeriod = 'cre.created_at';
            $searchQueryFirstPurchase = ' AND cre.reward_status IN (5,10) ';
        } else {
            $selectDate = 'c.created_at';
            $selectDatePeriod = 'cre.created_at';
            $searchQueryFirstPurchase = ' AND (cr.first_purchase_status = 0 OR (cr.first_purchase_status = 10 AND cre.reward_status = 0))';
        }

        if (isset($params['date_from']) && isset($params['date_to']) && !empty($params['date_from']) && !empty($params['date_to'])) {
            // validate date 6 month
            $dateFrom = date("Y-m-d", strtotime($params['date_from']));
            $last6MonthDate = date("Y-m-d", strtotime("-6 months"));

            if ($dateFrom < $last6MonthDate) {
                $this->errorCode = "RR100";
                $this->errorMessages = "Minimum date must be less than 6 months from today";
                return false;    
            }

            $searchQueryTime = ' AND '. $selectDate .' BETWEEN "' . $params['date_from'] . ' 00:00:00" AND "' . $params['date_to'] . ' 23:59:59"';
            
            $searchQueryTimePeriod = ' AND '. $selectDatePeriod .' BETWEEN "' . $params['date_from'] . ' 00:00:00" AND "' . $params['date_to'] . ' 23:59:59"';
        }

        $customerReferralEventModel = new \Models\CustomerReferralEvent();
        $customerId = (int) $params['customer_id'];
        $sql = "SELECT CONCAT(c.first_name,' ',c.last_name) as full_name, sv.voucher_amount, sv.percentage, sv.max_redemption, cr.reward_status AS invited_reward_status, cre.reward_status AS referral_reward_status, c.created_at AS register_date, cre.created_at AS invoice_date, cr.first_purchase_status
        FROM customer_referral cr
        LEFT JOIN customer_referral_event cre ON cr.invited_customer_id = cre.invited_customer_id
        LEFT JOIN customer c ON cr.invited_customer_id = c.customer_id
        LEFT JOIN salesrule_voucher sv ON cre.voucher_id = sv.voucher_id
        WHERE cr.reward_status = 10 AND cr.referral_customer_id = " . $customerId . $searchQueryName . $searchQueryFirstPurchase . $searchQueryTime . "  ORDER BY ".$selectDate." DESC";
        if (isset($params['offset']) && isset($params['limit'])) {
            $limit = (int) $params['limit'];
            $offset = (int) $params['offset'];
            $sql .=  " LIMIT ". $limit ." OFFSET ". $offset ."";
        }
       
        $selectReceipt = $customerReferralEventModel->getDi()->getShared('dbReader')->query($sql);
        $selectReceipt->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $selectReceiptResult = $selectReceipt->fetchAll();

        $rewardListResult = array();
        foreach($selectReceiptResult as $result) {
            if ($result['voucher_amount'] == 0 && $result['percentage'] > 0){
                $voucher_amount = $result['max_redemption'];
            }
            else{
                $voucher_amount = $result['voucher_amount'];
            }

            $parameters = [
                "full_name" => $result['full_name'],
                "voucher_amount" => $voucher_amount,
                "date" => $isDoneShopping ? $result['invoice_date'] : $result['register_date'],
                "reward_pending" => $result['first_purchase_status'] == "0" || ($result['first_purchase_status'] == "10" && $result['referral_reward_status'] != "10"),
                "reward_limit_reached" => $result['referral_reward_status'] == "5",
            ];
            array_push($rewardListResult,$parameters);
        }

        $totalRewards = 0;
        $totalAmount = 0;
        $periodAmount = 0;

        // get total rewards and voucher amount, percentage, redemption
        $sqlTotal = "SELECT COUNT(sv.voucher_id) AS total_rewards, COALESCE(SUM(CASE WHEN sv.voucher_amount = 0 AND sv.percentage > 0 THEN sv.max_redemption END), 0) + COALESCE(SUM(CASE WHEN sv.voucher_amount > 0 THEN sv.voucher_amount END), 0) AS total_amount
        FROM customer_referral_event cre
        JOIN customer c ON cre.invited_customer_id = c.customer_id
        JOIN salesrule_voucher sv ON cre.voucher_id = sv.voucher_id
        WHERE cre.reward_status = 10 AND cre.referral_customer_id =" . $customerId;

        $selectTotalReceipt = $customerReferralEventModel->getDi()->getShared('dbReader')->query($sqlTotal);
        $selectTotalReceipt->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $selectTotalReceiptResult = $selectTotalReceipt->fetchAll();
        if (!empty($selectTotalReceiptResult)){
            $totalRewards = $selectTotalReceiptResult[0]['total_rewards'];
            $totalAmount = $selectTotalReceiptResult[0]['total_amount'];
        }

        if ($searchQueryTimePeriod != '') {
            // get this period earnings
            $sqlPeriodEarning = $sqlTotal . $searchQueryTimePeriod;
            $selectPeriodEarning = $customerReferralEventModel->getDi()->getShared('dbReader')->query($sqlPeriodEarning);
            $selectPeriodEarning->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $selectPeriodEarningResult = $selectPeriodEarning->fetchAll();
            if (!empty($selectPeriodEarningResult)){
                $periodAmount = $selectPeriodEarningResult[0]['total_amount'];
            }
        } else {
            $periodAmount = $totalAmount;
        }

        $parametersResponse = [
            "period_amount" => $periodAmount,
            "total_amount" => $totalAmount,
            "total_rewards" => $totalRewards,
            "monthly_reward_limit" => getenv('REFERRAL_MONTHLY_REWARD_LIMIT'),
            "reward_list" => $rewardListResult
        ];

        $responseResult = array();
        array_push($responseResult,$parametersResponse);
        return $responseResult;
    }

    // Onelink API
    // public function createReferralLinkFromApi($params = array()){
    //     $apiWrapper = new \Library\APIWrapper(getenv('CUSTOMER_API'));
    //     $apiWrapper->setParam($params);
    //     $apiWrapper->setEndPoint("/onelink");

    //     if(!$apiWrapper->sendRequest("post")) {
    //         \Helpers\LogHelper::log("onelink_referral","[ERROR] when calling customer API " . json_encode($params));
    //         return false;
    //     }
    //     else{
    //         \Helpers\LogHelper::log("onelink_referral","[INFO] success calling customer API " . json_encode($params));
    //         $apiWrapper->formatResponse();
    //     }

    //     $customerApiResponse = $apiWrapper->getData();
    //     if ($apiWrapper->getError() != null){
    //         \Helpers\LogHelper::log("onelink_referral","[ERROR] getting API response " . json_encode($params));
    //         return null;
    //     }

    //     return $customerApiResponse;
    // }

    // public function updateReferralLinkFromApi($params = array()){
    //     $apiWrapper = new \Library\APIWrapper(getenv('CUSTOMER_API'));
    //     $apiWrapper->setParam($params);
    //     $apiWrapper->setEndPoint("/onelink");

    //     if(!$apiWrapper->sendRequest("put")) {
    //         \Helpers\LogHelper::log("onelink_referral","[ERROR] when calling customer API " . json_encode($params));
    //         return false;
    //     }
    //     else{
    //         \Helpers\LogHelper::log("onelink_referral","[INFO] success calling customer API " . json_encode($params));
    //         $apiWrapper->formatResponse();
    //     }

    //     $customerApiResponse = $apiWrapper->getData();
    //     if ($apiWrapper->getError() != null){
    //         \Helpers\LogHelper::log("onelink_referral","[ERROR] getting API response " . json_encode($params));
    //         return null;
    //     }

    //     return $customerApiResponse;
    // }

    // public function getReferralLinkFromApi($params = array()){
    //     $apiWrapper = new \Library\APIWrapper(getenv('CUSTOMER_API'));
    //     $apiWrapper->setParam($params);
    //     $apiWrapper->setEndPoint("/onelink?id=" . $params["id"]);

    //     if(!$apiWrapper->send("get")) {
    //         \Helpers\LogHelper::log("onelink_referral", "[ERROR] when calling customer API" . json_encode($params));
    //         return false;
    //     }
    //     else{
    //         \Helpers\LogHelper::log("onelink_referral","[INFO] success calling customer API " . json_encode($params));
    //         $apiWrapper->formatResponse();
    //     }

    //     $customerApiResponse = $apiWrapper->getData();
    //     if ($apiWrapper->getError() != null){
    //         \Helpers\LogHelper::log("onelink_referral","[ERROR] getting API response " . json_encode($params));
    //         return null;
    //     }
    //     return $customerApiResponse;
    // }

    public function upsertCustomerReferralLink($params = array()){   
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return false;
        }
        
        // Init Model
        $referralLinkModel = new \Models\CustomerReferralLink;
        $responseResult = array();
        
        if (isset($params["customer_id"]) && isset($params["channel"])) {
            if(empty($params["referral_tactical_id"])){
                $referralLinkParam["conditions"] = "customer_id = '". $params["customer_id"] ."' AND channel = '". $params["channel"] ."' AND type = 'invite'";
            }
            else{
                $referralLinkParam["conditions"] = "customer_id = '". $params["customer_id"] ."' AND channel = '". $params["channel"] ."' AND referral_tactical_id = '". $params["referral_tactical_id"] ."' AND type = 'tactical'";
            }
            
            $referralLinkFound = $referralLinkModel->findFirst($referralLinkParam);

            $dateNow = date("Y-m-d H:i:s");
            $date = new \DateTime($dateNow);
            $date->add(new \DateInterval('P60D')); // set expired date to 60 days
            $addedDate = $date->format('Y-m-d H:i:s');

            if (empty($referralLinkFound)) {
                //  new referral link -> insert to db
                if (!empty($params["referral_tactical_id"])){
                    $referralCode = $this->generateReferralTacticalRefCode($params["customer_id"], $params["referral_tactical_id"]);
                }
                else{
                    $referralCode = $this->generateRefCode($params["customer_id"]);
                }

                if (!$referralCode){
                    $this->errorMessages[] = 'Generate referral code failed';
                    $this->errorCode = '400';
                    return false;
                }
                else {
                    $params["referral_code"] = $referralCode;
                }

                if(!empty($params["referral_tactical_id"])){
                    $params["type"] = "tactical";
                } else{
                    $params["type"] = "invite";
                }

                $referralLinkModel->setFromArray($params);
                $referralLinkModel->setExpiredAt($addedDate);
                $saveStatus = $referralLinkModel->saveData("referral_link");
                if(!$saveStatus) {
                    $this->errorCode = $referralLinkModel->getErrorCode();
                    $this->errorMessages[] = $referralLinkModel->getErrorMessages();
                    return false;
                }
                return true;
            } else {
                // update 
                $queryUpdateReferralCode = '';
                if (is_null($referralLinkFound->getReferralCode())) {
                    if(!empty($params["referral_tactical_id"])){
                        $referralCode = $this->generateReferralTacticalRefCode($params["customer_id"], $params["referral_tactical_id"]);
                    } else{
                        $referralCode = $this->generateRefCode($params["customer_id"]);
                    }
                    if (!$referralCode){
                        $this->errorMessages[] = 'Generate referral code failed';
                        $this->errorCode = '400';
                        return false;
                    }
                    else{
                        $queryUpdateReferralCode = ",referral_code = '".$referralCode."'";
                    }
                }
                else{
                    $referralCode = $referralLinkFound->getReferralCode();
                }

                if(!empty($params["referral_tactical_id"])){
                    $sql = "UPDATE customer_referral_link SET status = 10,expired_notification = 0,token = '" . $params["token"] . "' ,referral_link = '" . $params["referral_link"] . "',expired_at = '" . $addedDate . "' ".$queryUpdateReferralCode." WHERE customer_id = " . $params["customer_id"] . " AND channel ='" . $params["channel"]."'AND type = 'tactical' AND referral_tactical_id =" . $params["referral_tactical_id"];
                }
                else{
                    $sql = "UPDATE customer_referral_link SET status = 10,expired_notification = 0,token = '" . $params["token"] . "' ,referral_link = '" . $params["referral_link"] . "',expired_at = '" . $addedDate . "' ".$queryUpdateReferralCode." WHERE customer_id = " . $params["customer_id"] . " AND channel ='". $params["channel"]."' AND type = 'invite'";
                }
                   
                $referralLinkModel->useWriteConnection();
                $updateResult = $referralLinkModel->getDi()->getShared($referralLinkModel->getConnection())->query($sql);
                if(!$updateResult) {
                    $this->errorCode = $referralLinkModel->getErrorCode();
                    $this->errorMessages[] = $referralLinkModel->getErrorMessages();
                    return false;
                }
                return $updateResult->numRows();
            }

        }else {
            $this->errorCode = '400';
            $this->errorMessages = 'Parameters are not complete';
            return false;
        }
    }
    
    public function getCustomerReferralLink($params = array()){
        
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return false;
        }

        // Init Model
        $referralLinkModel = new \Models\CustomerReferralLink;
        $responseResult = array();
        
        if (isset($params["customer_id"]) && isset($params["channel"])) {
            if(empty($params["referral_tactical_id"])){
                $referralLinkParam["conditions"] = "customer_id = '". $params["customer_id"] ."' AND channel = ". $params["channel"] . " AND type = 'invite'";
            }
            else{
                $referralLinkParam["conditions"] = "customer_id = '". $params["customer_id"] . "' AND channel = ". $params["channel"] . " AND referral_tactical_id = '". $params["referral_tactical_id"] ."' AND type = 'tactical'";
            }
            $referralLinkFound = $referralLinkModel->findFirst($referralLinkParam);

            if (empty($referralLinkFound)) {
                $this->errorMessages[] = 'Data tidak ditemukan';
                $this->errorCode = '404';
                return false;
            }else {
                // get rupper first and last name
                $customerModel = new \Models\Customer;
                $customerParam["conditions"] = "customer_id = ". $params["customer_id"];
                $customerFound = $customerModel->findFirst($customerParam);

                $expiredDate = $referralLinkFound->getExpiredAt();
                $dateNow = date("Y-m-d H:i:s");
                $availability = true ;
                $customerId=(int)$params["customer_id"];
                if ($expiredDate < $dateNow){
                    // update
                    $sql = "UPDATE customer_referral_link SET status = 0 WHERE customer_id = " . $customerId . " AND channel =". $params["channel"]."";
                    $referralLinkModel->useWriteConnection();
                    $updateResult = $referralLinkModel->getDi()->getShared($referralLinkModel->getConnection())->query($sql);
                    if(!$updateResult) {
                        $this->errorCode = $referralLinkModel->getErrorCode();
                        $this->errorMessages[] = $referralLinkModel->getErrorMessages();
                        return false;
                    }
                    $availability = false;
                }

                $responseResult["available"] = $availability;
                $responseResult["channel"] = (string)$referralLinkFound->getChannel();
                $responseResult["referral_link"] = (string)$referralLinkFound->getReferralLink();
                $responseResult["referral_code"] = (string)$referralLinkFound->getReferralCode();
                $responseResult["token"] = (string)$referralLinkFound->getToken();
                $responseResult["expired_at"] = (string)$referralLinkFound->getExpiredAt();
                $responseResult["customer"] = [
                    "first_name" => (string)$customerFound->getFirstName(),
                    "last_name" => (string)$customerFound->getLastName(),
                ];

                return $responseResult;
            }
        }else {
            $this->errorCode = '400';
            $this->errorMessages = 'Parameters are not complete';
            return false;
        }
    }

    public function getFirstPurchaseStatus($customer_id){
       
        if (empty($customer_id)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return false;
        }

        //init models
        $referralEventModel = new \Models\CustomerReferralEvent;
        $responseResult = array();

        $customerId = (int) $customer_id;

        $sql = "SELECT COUNT(DISTINCT so.sales_order_id) as count_invoice
		FROM sales_invoice si
		JOIN sales_order so ON so.sales_order_id = si.sales_order_id
		WHERE so.device != 'vendor'
        AND so.customer_id = " . $customerId . "  LIMIT 2";
    
        $selectReceipt = $referralEventModel->getDi()->getShared('dbReader')->query($sql);
        $selectReceipt->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $selectReceiptResult = $selectReceipt->fetchAll();

        if (!empty($selectReceiptResult)) {
            foreach($selectReceiptResult as $result) {
                if ((int)$result["count_invoice"] > 1)
                {
                    $responseResult["status"] = 'completed_purchases';
                }else {
                    $responseResult["status"] = 'first_purchase';
                }
                
            }
        }
        return $responseResult;
    }

    public function checkReferralFirstPurchase($params = array()){
       
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return false;
        }

        //init models
        $referralCustomerModel = new \Models\CustomerReferral;
        $responseResult = array();

        $customerId = (int) $params['customer_id'];

        $sql = "SELECT cr.first_purchase_status,cr.referral_customer_id FROM customer_referral cr 
        WHERE cr.invited_customer_id  = " . $customerId . " AND cr.first_purchase_status = 0";
        
        $selectReceipt = $referralCustomerModel->getDi()->getShared('dbReader')->query($sql);
        $selectReceipt->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $selectReceiptResult = $selectReceipt->fetchAll();
        if (!empty($selectReceiptResult)) {
            $responseResult = $this->getFirstPurchaseStatus($params["customer_id"]);
            if($responseResult['status']!="completed_purchases"){
                foreach($selectReceiptResult as $result) {
                    $referralEventParams['referral_customer_id']= $result['referral_customer_id'];
                    $referralEventParams['invited_customer_id']= $customerId;
                    $referralEventParams['event_name']= 'first_purchase';
                    $referralEventParams['order_no']= $params['order_no'];
                    $referralEventParams['reward_status']= 0;
                                            
                    $customerReferraEventlModel = new \Models\CustomerReferralEvent();
                    $customerReferraEventlModel->setFromArray($referralEventParams);
                    $saveStatus = $customerReferraEventlModel->saveData("referral_event");
                    if(!$saveStatus) {
                        $this->errorCode = $customerReferraEventlModel->getErrorCode();
                        $this->errorMessages[] = $customerReferraEventlModel->getErrorMessages();
                        return false;
                    }
                    $logId = $saveStatus;
                    // LogHelper::log("customer_referral", "[Log ID: ".$logId."] Data for Login, Params: ".json_encode($saveStatus), "info");
                    try {
                        $sql = "UPDATE customer_referral SET first_purchase_status = 10 WHERE invited_customer_id =" . $customerId . "";
                        $referralCustomerModel->useWriteConnection();
                        $updateResult = $referralCustomerModel->getDi()->getShared($referralCustomerModel->getConnection())->query($sql);
                    } catch (\Exception $e) {
                            $this->errorCode = "500";
                            $this->errorMessages = "Terjadi kendala pada saat memperbaharui data customer referral";
                            return false;
                    }
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
        return true;
    }

    public function referralCustomerRegistration($params = array()){
        // Make sure customer not get double invited
        $customerId = $params['customer_id'];

        // check device
        // comment from this ....
        if (isset($params['unique_id']) && !empty($params['unique_id'])) {
            $customerDeviceModel = new \Models\CustomerDevice();
            $customerDataDevice = $customerDeviceModel->findFirst('device_id = "' . $params['unique_id'] . '" AND type = "' . $params['type'] . '"');
            if (!$customerDataDevice) {
                // insert new
                $insertCustomerDeviceParams = [
                    "customer_id" => $customerId,
                    "device_id" => $params['unique_id'],
                    "type" => $params['type'],
                ];
                $customerDeviceModel->setFromArray($insertCustomerDeviceParams);
                $customerDeviceModel->saveData("customer_device");
            } else {
                // return failed due to already registered
                // insert into customer_referral table
                $referralParams['referral_customer_id']= $params['referral_id'];
                $referralParams['invited_customer_id']= $customerId;
                $referralParams['device']= $params['user_agent'];
                $referralParams['reward_status']= 5;
                $referralParams['first_purchase_status']= 0;
            
                $customerReferralModel = new \Models\CustomerReferral();
                $customerReferralModel->setFromArray($referralParams);
                $customerReferralModel->saveData("referral");

                LogHelper::log("duplicate_device", "[referralCustomerRegistration] Params: ".json_encode($params), "info");

                $this->setIsDuplicateDevice(true);
                return true;
            }
        }
        // .... to this for disabling duplicate device checking


        $customerReferralModel = new \Models\CustomerReferral();
        $customerDataReferral = $customerReferralModel->findFirst('invited_customer_id = ' . $customerId);
        if(!$customerDataReferral) {
            // Generate referral voucher to invited customer
            $salesRuleVoucherModel = new \Models\SalesruleVoucher();
            $ruleId = getenv('RULE_ID_REFERRAL');
            $customerDataVoucher = $salesRuleVoucherModel->findFirst('customer_id = ' . $customerId . ' and rule_id = ' . $ruleId);
            if (!$customerDataVoucher) {
               //Generate voucher referral
                $prefix = getenv('REFERRAL_VOUCHER_PREFIX');
                $parameters = [ 
                    "rule_id" => $ruleId,
                    "type" => 3,
                    "created_by" => 0,
                    "expiration_date" => date('Y-m-d H:i:s', strtotime('+30 days')),
                    "qty" => 1,
                    "voucher_amount" =>getenv('REFERRAL_VOUCHER_AMOUNT'),
                    "length" => getenv('REFERRAL_VOUCHER_LENGTH'),
                    "prefix" => $prefix,
                    "format" => "alphanum"
                ];
                $marketingLib = new \Library\Marketing();
                $marketingLib->generateVoucher($parameters);
                $voucherList = $marketingLib->getVoucherList();

                if (!empty($voucherList)) {
                    foreach ($voucherList as $voucherCode) {
                        $parameters['voucher_code'] = $voucherCode;
                        $parameters['limit_used'] = 1;
                        if (empty($parameters['voucher_amount'])){
                            $parameters['percentage'] = getenv('REFERRAL_VOUCHER_PERCENTAGE');
                            $parameters['max_redemption'] = getenv('REFERRAL_VOUCHER_MAX_REDEMPTION');
                        }
                        $parameters['status'] = 10;
                        $parameters['customer_id'] = $customerId;
                        $salesRuleVoucherModel = new \Models\SalesruleVoucher();
                        $salesRuleVoucherModel->setFromArray($parameters);
                        $salesRuleVoucherModel->saveData("voucher");
                    }
                }
                // insert into customer_referral table
                $referralParams['referral_customer_id']= $params['referral_id'];
                $referralParams['invited_customer_id']= $customerId;
                $referralParams['device']= $params['user_agent'];
                $referralParams['reward_status']= 10;
                $referralParams['voucher_id']=  $salesRuleVoucherModel->getVoucherId(); //get voucherID
                $referralParams['first_purchase_status']= 0;
               
                $customerReferralModel = new \Models\CustomerReferral();
                $customerReferralModel->setFromArray($referralParams);
                $customerReferralModel->saveData("referral");
            }
        }
        return true;
    }

    public function getReferralCustomerIdByReferralCode($referralCode) {
        $referralLinkModel = new \Models\CustomerReferralLink;
        $referralLinkData = $referralLinkModel->findFirst('referral_code = "' . $referralCode . '"');
        if (!$referralLinkData) {
            return false;
        }

        $referralCust["customer_id"] = $referralLinkData->getCustomerId();
        $referralCust["type"] = $referralLinkData->getType();

        return $referralCust;
    }

    public function generateRefCode($customerID) {
        $customerModel = new \Models\Customer;
        $customerData = $customerModel->findFirst('customer_id = "' . $customerID . '"');
        if (!$customerData) {
            return false;
        }

        // Get first 5 character from first name, randomize other until 8 character length
        $upperFirstName = strtoupper(substr(trim($customerData->getFirstName()), 0, 5));
        $randomizedLength = 8 - strlen($upperFirstName);
        $myRandom = strtoupper(\Phalcon\Text::random(\Phalcon\Text::RANDOM_ALNUM , $randomizedLength));

        return $upperFirstName.$myRandom;
    }

    public function getCustomerReferralCode($params = array()){
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return false;
        }
        
        // Init Model
        $referralLinkModel = new \Models\CustomerReferralLink;
        if(!isset($params["customer_id"]) || !isset($params["channel"])) {
            $this->errorCode = '400';
            $this->errorMessages = 'Parameters are not complete';
            return false;
        }
        
        if(!empty($params["referral_tactical_id"])){
            $type = "tactical";
        } else{
            $type = "invite";
        }

        // Get From Referral Link, if empty data || empty referral_code, then generate and upsert referralcode
        if ($type == "tactical"){
            $referralLinkParam["conditions"] = "customer_id = ". $params["customer_id"] ." AND channel = '". $params["channel"] ."'" . "AND type = 'tactical' AND referral_tactical_id = " . $params['referral_tactical_id'];
        }
        else if ($type == "invite"){
            $referralLinkParam["conditions"] = "customer_id = ". $params["customer_id"] ." AND channel = '". $params["channel"] ."' AND type = 'invite'";
        }
       
        $referralLinkFound = $referralLinkModel->findFirst($referralLinkParam);
        if (empty($referralLinkFound) || empty($referralLinkFound->getReferralCode()) || is_null($referralLinkFound->getReferralCode())) {
            if ($type == "tactical"){
                $referralCode = $this->generateReferralTacticalRefCode($params["customer_id"], $params["referral_tactical_id"]);
            }
            else if ($type == "invite"){
                $referralCode = $this->generateRefCode($params["customer_id"]);
            }
            if (!$referralCode){
                $this->errorMessages[] = 'Generate referral code failed';
                $this->errorCode = '400';
                return false;
            }
        } else {
            $referralCode = $referralLinkFound->getReferralCode();
            return $referralCode;
        }

        $upsertParams = [
            "customer_id" => $params["customer_id"],
            "channel" => $params["channel"],
            "referral_code" => $referralCode,
            "type" => $type,
            "referral_tactical_id" => $params["referral_tactical_id"]
        ];

        if (empty($referralLinkFound)) {
            // insert new data
            $referralLinkModel->setFromArray($upsertParams);
            $saveStatus = $referralLinkModel->saveData("referral_link");
            if(!$saveStatus) {
                $this->errorCode = $referralLinkModel->getErrorCode();
                $this->errorMessages[] = $referralLinkModel->getErrorMessages();
                return false;
            }

            return $referralCode;
        }

        // update data
        $upsertParams["customer_referral_link_id"] = $referralLinkFound->getCustomerReferralLinkId();
        $referralLinkModel->setFromArray($upsertParams);
        $updateStatus = $referralLinkModel->saveData("referral_link");
        if(!$updateStatus) {
            $this->errorCode = $referralLinkModel->getErrorCode();
            $this->errorMessages[] = $referralLinkModel->getErrorMessages();
            return false;
        }

        return $referralCode;
    }

    public function generateReferralTacticalRefCode($customerID, $referralTacticalID) {
        $customerModel = new \Models\Customer;
        $customerData = $customerModel->findFirst('customer_id = "' . $customerID . '"');
        if (!$customerData) {
            return false;
        }

        $referralTacticalModel = new \Models\ReferralTactical;
        $referralTacticalData = $referralTacticalModel->findFirst('referral_tactical_id = "' . $referralTacticalID . '"');
        if (!$referralTacticalData) {
            return false;
        }

        // Referral code format: 2 character from campaign code + 4 character from customer first name + 3 random alphanum character
        $randomizedLength = 3;
        $custNameLen = 4;
        $campaignCode = strtoupper(substr(trim($referralTacticalData->getCampaignCode()), 0, 2));
        $upperFirstName = strtoupper(substr(trim($customerData->getFirstName()), 0, $custNameLen));
        $upperFirstName = preg_replace("/[^A-Za-z0-9]/", '', $upperFirstName);

        if (strlen($upperFirstName) < 4){
            $randomizedLength += ($custNameLen - strlen($upperFirstName));
        } 

        do{
            $myRandom = strtoupper(\Phalcon\Text::random(\Phalcon\Text::RANDOM_ALNUM , $randomizedLength));
            $referralCode = $campaignCode.$upperFirstName.$myRandom;
        } while($this->checkIfReferralCodeExist($referralCode));

        return $referralCode;
    }

    public function checkIfReferralCodeExist($referralCode){
        $referralLinkModel = new \Models\CustomerReferralLink;
        $referralLinkData = $referralLinkModel->findFirst('referral_code = "' . $referralCode . '"');

        if (!$referralLinkData) {
            return false;
        }
        return true;
    }
}