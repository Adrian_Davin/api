<?php
namespace Library;

class AdminUser
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @var \Models\AdminUser
     */
    protected $adminUser;

    /**
     * @return \Models\AdminUser
     */
    public function getAdminUser()
    {
        return $this->adminUser;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {

    }

    public function getAllAdminUsers($params = array())
    {
        $adminUserModel = new \Models\AdminUser();
        $adminUserAttributeValue = new \Models\AdminUserAttributeValue();
        $gideonAdminRole = new \Models\GideonAdminRole();

        $result = $adminUserModel->getListAdminUser($params);

        if (isset($params['master_app_id'])) {
            $i = 0;
            foreach ($result as $adminUser){
                $attributeValues = $adminUserAttributeValue->getAdminListUserAttributeValue($adminUser['admin_user_id'], $params['master_app_id']);

                $j = 0;
                foreach($attributeValues as $attr){
                    $attributeValues[$j]['master_app'] = array(
                        'master_app_id' => $attributeValues[$j]['master_app_id'],
                        'master_app_name' => $attributeValues[$j]['master_app_name'],
                    );
                    unset($attributeValues[$j]['master_app_name']);

                    $attributeValues[$j]['admin_user_attribute'] = array(
                        'admin_user_attribute_id' => $attributeValues[$j]['admin_user_attribute_id'],
                        'admin_user_attribute_name' => $attributeValues[$j]['admin_user_attribute_name'],
                        'description' => $attributeValues[$j]['description'],
                    );
                    unset($attributeValues[$j]['admin_user_attribute_id']);
                    unset($attributeValues[$j]['admin_user_attribute_name']);
                    unset($attributeValues[$j]['description']);

                    $j++;
                }

                $result[$i]['attribute_value'] = $attributeValues;
                
                if ($params['master_app_id'] == 2) {
                    $gideonAdminRoleResult = $gideonAdminRole->getAdminRole($params['master_app_id'], 2, $adminUser['admin_user_id']);
                    $result[$i]['gideon_admin_role'] = $gideonAdminRoleResult;
                }

                $i++;
            }
        }

        return $result;
    }

    public function getSumAdminUser($params = array()){
        $adminUserModel = new \Models\AdminUser();
        return $adminUserModel->getSumListAdminUser($params);
    }

    public function getAllAdminUser($params = array())
    {
        $adminUserModel = new \Models\AdminUser();
        $resultAdminUser = $adminUserModel::query();
        //$adminUserModel::columns('admin_user_id,email,password,firstname, lastname, concat(firstname," ",lastname) as full_name');
        $master_app_id = '';
        foreach($params as $key => $val) {
            if($key == "admin_user_id") {
                $resultAdminUser->andWhere("admin_user_id = $val");
            }

            if($key == "email") {
                $resultAdminUser->andWhere("email = '$val'");
            }

            if($key == "firstname") {
                $resultAdminUser->andWhere("firstname = '$val''");
            }

            if($key == "master_app_id") {
                $master_app_id = $val;
            }
        }

        if(!empty($params["order_by"])) {
            $resultAdminUser->orderBy($params["order_by"]);
        } else {
            $resultAdminUser->orderBy("firstname ASC");
        }

        $result = $resultAdminUser->execute();

        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your admin user data";
            $allAdminUser = array();
        } else {
            $adminUserArray = array();
            $i = 0;
            foreach ($result as $adminUser) {
                $adminUserArray[$i] = $adminUser->toArray([], true);

                $adminUserArray[$i]['attribute_value'] = array();
                if (isset($adminUser->AdminUserAttributeValue)) {
                    $adminUserArray[$i]['attribute_value'] = $adminUser->AdminUserAttributeValue->toArray();

                    if ($master_app_id <> '') $flagCheckByMasterAppId = false;
                    for ($j=0; $j<count($adminUserArray[$i]['attribute_value']); $j++) {
                        // check params app id is not empty
                        if ($master_app_id <> '') {

                            if ($master_app_id == $adminUserArray[$i]['attribute_value'][$j]['master_app_id']) {

                                $flagCheckByMasterAppId = true;
                            } else {

                                // remove other app id attribute value
                                foreach ($adminUserArray[$i]['attribute_value'][$j] as $key1 => $val1) {
                                    unset($adminUserArray[$i]['attribute_value'][$j][$key1]);
                                }
                                continue;
                            }
                        }

                        $admin_user_attribute_id = $adminUserArray[$i]['attribute_value'][$j]['admin_user_attribute_id'];
                        unset($adminUserArray[$i]['attribute_value'][$j]['admin_user_attribute_id']);
                        unset($adminUserArray[$i]['attribute_value'][$j]['admin_user_id']);

                        $masterAppModel = new \Models\MasterApp();
                        $resultApp = $masterAppModel::findFirst("master_app_id = " . $adminUserArray[$i]['attribute_value'][$j]['master_app_id']);
                        $adminUserArray[$i]['attribute_value'][$j]['master_app'] = $resultApp->toArray();

                        $adminUserAttributeModel = new \Models\AdminUserAttribute();
                        $resultAttribute = $adminUserAttributeModel::findFirst("admin_user_attribute_id = " . $admin_user_attribute_id);
                        $adminUserArray[$i]['attribute_value'][$j]['admin_user_attribute'] = $resultAttribute->toArray();
                    }

                    // remove user that don't have choosen master app id
                    if ($master_app_id <> '') {
                        if (!$flagCheckByMasterAppId) {
                            unset($adminUserArray[$i]);
                        } else {

                            foreach ($adminUserArray[$i]['attribute_value'] as $key2 => $val2) {
                                if (count($adminUserArray[$i]['attribute_value'][$key2]) == 0) {
                                    unset($adminUserArray[$i]['attribute_value'][$key2]);
                                }else{
                                    // Getting Role Id Gideon
                                    if($adminUserArray[$i]['attribute_value'][$key2]['admin_user_attribute']['admin_user_attribute_id'] == 2){

                                        if($master_app_id == 2){
                                            $role_id = $adminUserArray[$i]['attribute_value'][$key2]['value'];
                                            $gideonAdminRoleMod = new \Models\GideonAdminRole();

                                            $roleData = $gideonAdminRoleMod->findFirst(' role_id = '.$role_id);
                                            $adminUserArray[$i]['gideon_admin_role'] = $roleData->getDataArray();
                                        }

                                    }
                                }
                            }
                            // reindex array
                            $adminUserArray[$i]['attribute_value'] = array_values($adminUserArray[$i]['attribute_value']);
                        }
                    }
                }

                $i++;
            }

            $allAdminUser = array_values($adminUserArray);
        }

        return $allAdminUser;
    }

    public function getAllAdminUserCustom($params = array())
    {
        $adminUserModel = new \Models\AdminUser();

        if (isset($params['limit']) && isset($params['offset'])) {
            $limit = $params['limit'];
            $offset = $params['offset'];
            unset($params['limit']);
            unset($params['offset']);
        }

        $query_count = "
                SELECT
                    count(admin_user_id) as total
                FROM
                    \Models\AdminUser";

        $columns = "*";
        $where_clause = '';
        $flagOperator = 0;
        if (isset($params['master_app_id'])) {
            if ($params['master_app_id'] == $_ENV['MASTER_APP_ID_OMS'] || $params['master_app_id'] == $_ENV['MASTER_APP_ID_MARKETING']) {
                $master_app_id = $params['master_app_id'];
                unset($params['master_app_id']);

                $columns = "
               	admin_user_id,
                (".$master_app_id.") as master_app_id,
                email,
                firstname,
                lastname,
                coalesce ((SELECT value FROM \Models\AdminUserAttributeValue WHERE 
                            \Models\AdminUserAttributeValue.admin_user_attribute_id = 15 AND 
                            \Models\AdminUserAttributeValue.admin_user_id = \Models\AdminUser.admin_user_id AND 
                            \Models\AdminUserAttributeValue.master_app_id = ". $master_app_id ."), 'ODI')
                AS company_code,
                case (SELECT value FROM \Models\AdminUserAttributeValue WHERE 
                        \Models\AdminUserAttributeValue.admin_user_attribute_id = 5 AND 
                        \Models\AdminUserAttributeValue.admin_user_id = \Models\AdminUser.admin_user_id AND 
                        \Models\AdminUserAttributeValue.master_app_id = ". $master_app_id .")
                WHEN 10 THEN 'Enabled'
                WHEN 0 THEN 'Disabled'
                WHEN 5	THEN 'Locked'
                ELSE '-' END as status,
                (SELECT master_app_name FROM \Models\MasterApp WHERE \Models\MasterApp.master_app_id = ". $master_app_id .") as master_app_name,
                CASE " . $master_app_id . "
                WHEN 1 THEN 
                    (SELECT role_name FROM \Models\OmsAdminRole WHERE 
                        \Models\OmsAdminRole.role_id = (
                        SELECT value FROM \Models\AdminUserAttributeValue WHERE \Models\AdminUserAttributeValue.admin_user_attribute_id = 2 AND 
                            \Models\AdminUserAttributeValue.admin_user_id = \Models\AdminUser.admin_user_id AND 
                            \Models\AdminUserAttributeValue.master_app_id = 1
                        )
                    )
                WHEN 4 THEN 
                    (SELECT role_name FROM \Models\MarketingAdminRole WHERE 
                        \Models\MarketingAdminRole.role_id = (
                        SELECT value FROM \Models\AdminUserAttributeValue WHERE \Models\AdminUserAttributeValue.admin_user_attribute_id = 2 AND 
                            \Models\AdminUserAttributeValue.admin_user_id = \Models\AdminUser.admin_user_id AND 
                            \Models\AdminUserAttributeValue.master_app_id = 4
                        )
                    )    
                ELSE '-' END AS role";

                $where_clause .= " WHERE ";

                $where_clause .= "\Models\AdminUser.admin_user_id in (SELECT DISTINCT(\Models\AdminUserAttributeValue.admin_user_id) FROM \Models\AdminUserAttributeValue WHERE \Models\AdminUserAttributeValue.master_app_id = " . $master_app_id .")";
                $flagOperator++;
            }
        }

        if (isset($params['email_keyword'])) {
            if ($flagOperator) {
                $where_clause .= " AND ";
            } else {
                $where_clause .= " WHERE ";
            }

            $params['email_keyword'] = strtolower($params['email_keyword']);
            $where_clause .= "LOWER(\Models\AdminUser.email) LIKE '%" . strtolower($params['email_keyword']) . "%'";
            $flagOperator++;
        }

        if (isset($params['username_keyword'])) {
            if ($flagOperator) {
                $where_clause .= " AND ";
            } else {
                $where_clause .= " WHERE ";
            }

            $params['username_keyword'] = strtolower($params['username_keyword']);
            $where_clause .= "LOWER(CONCAT(\Models\AdminUser.firstname, ' ', \Models\AdminUser.lastname)) LIKE '%" . strtolower($params['username_keyword']) . "%'";
        }

        if (isset($params['token'])) {
            if ($flagOperator) {
                $where_clause .= " AND ";
            } else {
                $where_clause .= " WHERE ";
            }

            $where_clause .= "\Models\AdminUser.token = '". $params['token'] ."'";
        }

        $query_count .= $where_clause;

        if (isset($limit) && isset($offset)) {
            $query = "SELECT " . $columns . " FROM \Models\AdminUser " . $where_clause . " ORDER BY email DESC LIMIT $limit OFFSET $offset";
        } else {
            $query = "SELECT " . $columns . " FROM \Models\AdminUser " . $where_clause . " ORDER BY email DESC";
        }

        try {
            $manager = $adminUserModel->getModelsManager();
            $result = $manager->executeQuery($query)->toArray();
            $allUsers['list'] = $result;

            $total = $manager->executeQuery($query_count)->toArray();
            $allUsers['recordsTotal'] = $total[0]['total'];
        } catch (\Exception $e) {
            $allUsers['list'] = array();
        }

        return $allUsers;
    }

    public function getAdminUserData($admin_user_id = "", $master_app_id = "")
    {
        $allAdminUser = $this->getAllAdminUser(["admin_user_id" => $admin_user_id, "master_app_id" => $master_app_id]);
        if(!isset($allAdminUser[0])) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your admin user detail";
            return false;
        } else {
            return $allAdminUser[0];
        }
    }

    public function getListApps($params = array())
    {
        $masterAppModel = new \Models\MasterApp();
        $resultMasterApp = $masterAppModel::query();
        foreach($params as $key => $val)
        {
            if($key == "master_app_id") {
                $resultMasterApp->andWhere("master_app_id = $val");
            }
        }

        $result = $resultMasterApp->execute();
        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your application list data";
            $allApps = array();
        } else {
            $i = 0;
            foreach ($result as $masterApp) {
                $masterAppArray[$i] = $masterApp->toArray([], true);
                $i++;
            }
            $allApps = $masterAppArray;
        }
        return $allApps;
    }

    public function userAttributeValueList($params = array())
    {
        $userAttributeValueModel = new \Models\AdminUserAttributeValue();
        $resultUserAttributeValue = $userAttributeValueModel::query();
        foreach($params as $key => $val)
        {
            if($key == "admin_user_id") {
                $resultUserAttributeValue->andWhere("admin_user_id = $val");
            }

            if($key == "master_app_id") {
                $resultUserAttributeValue->andWhere("master_app_id = $val");
            }

            if($key == "admin_user_attribute_id") {
                $resultUserAttributeValue->andWhere("admin_user_attribute_id = $val");
            }

            if($key == "value") {
                $resultUserAttributeValue->andWhere("value = '$val'");
            }

        }

        $result = $resultUserAttributeValue->execute();
        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your user attribute list data";
            $allUserAttValue = array();
        } else {
            $i = 0;
            foreach ($result as $userAttributeValue) {
                $userAttributeValueArray[$i] = $userAttributeValue->toArray([], true);
                $i++;
            }
            $allUserAttValue = $userAttributeValueArray;
        }
        return $allUserAttValue;
    }
}