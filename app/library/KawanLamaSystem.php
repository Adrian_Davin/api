<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/10/2017
 * Time: 2:07 PM
 */

namespace Library;

use Exception;
use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Helpers\EmployeeHelper;
use Models\CustomerAceOtpTemp;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class KawanLamaSystem
{
    protected $invoice_no;
    protected $invoice_sap_no;
    protected $order_no;
    protected $credit_memo_no;
    protected $amount;

    protected $errorCode;
    protected $errorMessages;

    public $voucherId;

    /**
     * @return mixed
     */

    protected $customer;
    protected $customerAceDetails;

    public function __construct()
    {
        $this->customer = new \Models\Customer();
        $this->customerAceDetails = new \Models\CustomerAceDetails();
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;

        return $this;
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function createPickupVoucher()
    {
        if (empty($this->invoice_no)) {
            $this->errorCode = 400;
            $this->errorMessages[] = "Nomor Invoice diperlukan";
            return;
        }

        $invoiceNo = $this->invoice_no;
        if (stripos($this->invoice_no, "-R") !== false) {
            $invoiceNo = str_replace('-R', '', $this->invoice_no);
        } else if (stripos($this->invoice_no, "-S") !== false) {
            $invoiceNo = str_replace('-S', '', $this->invoice_no);
        } else if (stripos($this->invoice_no, "-T") !== false) {
            $invoiceNo = str_replace('-T', '', $this->invoice_no);
        }

        $invoice = new \Models\SalesInvoice();
        $resultInvoice = $invoice->findFirst("invoice_no = '" . $invoiceNo . "'");
        if ($resultInvoice) {
            $invoice->assign($resultInvoice->toArray([]));
            $POSVoucher = new \Library\POSVoucher($invoice);
            $POSVoucher->prepareParams();
            $POSVoucher->setNoInvoice($this->invoice_no);
            if ($this->amount) {
                $POSVoucher->setAmount($this->amount);
            }

            $voucherCode = $POSVoucher->createPOSVoucher();

            // Note: Create journal moved to automata
            if ($voucherCode === false) {
                $errMsg[] = "Gagal melakukan generate pickup voucher";

                if (!empty($POSVoucher->voucherId)) {
                    $this->voucherId = $POSVoucher->voucherId;
                }

                $this->errorCode = 400;
                $this->errorMessages = $errMsg;
                return false;
            }
        } else {
            $this->errorCode = 400;
            $this->errorMessages[] = "Invoice tidak ditemukan";
            return false;
        }

        return true;
    }

    public function createJournal($journal_type = "")
    {
        if ($journal_type != "refund" && empty($this->invoice_no)) {
            if ($journal_type != "reclass" && $journal_type != "reclassReverse" && $journal_type != "sales_b2b_top" && $journal_type != "incoming_b2b_top") {
                $this->errorCode = "RR202";
                $this->errorMessages[] = "Nomor invoice diperlukan";
                return false;
            }
        } else if ($journal_type == 'refund' && empty($this->credit_memo_no)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "Credit memo no diperlukan";
            return false;
        }

        if (empty($journal_type)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "Journal tidak diketahui";
            return false;
        }

        $creditMemoData = [];
        $invoice = "";
        $salesOrder = "";
        $invoiceSapResult = "";
        $orderType = 'B2C';
        if ($journal_type != "refund") {
            $file_location = VAR_PATH . '/journal/' . $journal_type . '/journal_' . $this->invoice_no . '.xml';
            $use_file_param = false;
            $JournalParam = array();
            if (file_exists($file_location)) {
                $xmlContent = file_get_contents($file_location);
                $JournalParam = \Helpers\XML2ArrayHelper::createArray($xmlContent);
                $use_file_param = true;
            }
        } else if ($journal_type == 'refund') {
            $use_file_param = false;
            $creditMemoModel = new \Models\SalesCreditMemo();
            $creditMemoResult = $creditMemoModel::findFirst(["credit_memo_no" => $this->credit_memo_no]);
            $this->order_no = $creditMemoResult->getOrderNo();

            $creditMemo = new \Library\CreditMemo();
            $creditMemoData = $creditMemo->loadFromCache(["credit_memo_no" => $this->credit_memo_no]);

            if (empty($creditMemoData)) {
                return;
            }
        }

        if ($use_file_param == false) {
            if (!empty($this->invoice_no)) {
                $invoice = new \Models\SalesInvoice();
                $invoiceResult = $invoice->findFirst("invoice_no = '" . $this->invoice_no . "'");
                $invoice->assign($invoiceResult->toArray([]));
            } else if (!empty($this->order_no)) {
                $salesOrder = new \Models\SalesOrder();
                $salesOrderResult = $salesOrder::findFirst("order_no = '" . $this->order_no . "'");
                $salesOrder->assign($salesOrderResult->toArray([]));
            } else if (!empty($this->invoice_sap_no)) {

                $invoiceSap = new \Models\SalesInvoiceSap();
                $sql = "
                SELECT
                    sip.invoice_sap_no,
                    si.invoice_no,
                    si.invoice_id,
                    so.order_no,
                    si.store_code
                FROM
                    sales_invoice_sap sip
                        LEFT JOIN
                    sales_invoice si ON si.invoice_id = sip.invoice_id
                        LEFT JOIN
                    sales_order so ON so.sales_order_id = si.sales_order_id
                WHERE
                    sip.invoice_sap_no = '" . $this->invoice_sap_no . "'";
                $dbResult = $invoiceSap->getDi()->getShared("dbMaster")->query($sql);
                $dbResult->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $invoiceSapResult = $dbResult->fetchAll();
            }
        }

        $journalSAP = new \Library\JournalSAP($invoice, $salesOrder, $invoiceSapResult);
        $invoiceItemsSQL = "
            SELECT
                sii.sku,
                sii.status_fulfillment,
                sii.qty_ordered,
                sii.selling_price,
                sii.handling_fee_adjust,
                sii.discount_amount,
                sii.gift_cards_amount,
                sii.shipping_amount,
                sii.shipping_discount_amount
            FROM sales_invoice_item sii
            WHERE sii.invoice_id = '{invoice_id}'";

        if (empty($JournalParam)) {
            $headerStatus = $journalSAP->prepareHeader();
            if ($headerStatus) {
                switch ($journal_type) {
                    case "pickup":
                        $device = $invoice->SalesOrder->getDevice();
                        // check if device != vendor or (device == vendor and invoice->store_code contains DC)
                        if ($device != "vendor" || preg_match('/(DC|1000DC)/', $invoice->getStoreCode())) {
                            $journalSAP->preparePickupVoucherJournal();
                            $journalSAP->createJournal("pickup");
                        }
                        break;
                    case "sales":
                        $device = $invoice->SalesOrder->getDevice();
                        // check if device != vendor or (device == vendor and invoice->store_code contains DC)
                        if ($device != "vendor" || preg_match('/(DC|1000DC)/', $invoice->getStoreCode())) {
                            $journalSAP->prepareSalesJournal();
                            $journalSAP->createJournal("sales");
                        }
                        break;
                    case "sales_b2b_top":
                        foreach ($invoiceSapResult as $rowsInvoiceSap) {
                            $invoiceItem = new \Models\SalesInvoiceItem();
                            $dbResult = $invoiceItem->getDi()->getShared("dbMaster")->query(str_replace(
                                "{invoice_id}",
                                $rowsInvoiceSap["invoice_id"],
                                $invoiceItemsSQL
                            ));
                            $dbResult->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
                            $invoiceItems = $dbResult->fetchAll();

                            $journalSAP->prepareSalesB2BTopJournal($rowsInvoiceSap, $invoiceItems);
                            $journalSAP->createJournal("sales_b2b_top");
                        }
                        break;
                    case "incoming_b2b_top":
                        foreach ($invoiceSapResult as $rowsInvoiceSap) {
                            $invoiceItem = new \Models\SalesInvoiceItem();
                            $dbResult = $invoiceItem->getDi()->getShared("dbMaster")->query(str_replace(
                                "{invoice_id}",
                                $rowsInvoiceSap["invoice_id"],
                                $invoiceItemsSQL
                            ));
                            $dbResult->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
                            $invoiceItems = $dbResult->fetchAll();

                            $journalSAP->prepareIncomingB2BTopJournal($rowsInvoiceSap, $invoiceItems);
                            $journalSAP->createJournal("incoming_b2b_top");
                        }
                        break;
                    case "pickupReverse":
                        $journalSAP->preparePickupReverseJournal();

                        if (getenv('VORGDATUM_REVERSAL') == "") {
                            $journalSAP->setVorgdatum(date("Ymd"));
                        } else {
                            $journalSAP->setVorgdatum(getenv('VORGDATUM_REVERSAL'));
                        }

                        $journalSAP->createJournal("pickup_reversal");
                        break;
                    case "reclass":
                        if (empty($salesOrder)) {
                            $this->errorCode = "RR202";
                            $this->errorMessages[] = "Sales Order tidak ditemukan";
                            return false;
                        }

                        $giftCards = json_decode($salesOrder->getGiftCards(), true);

                        // Comment all this code, because no any case we separate the giftcards with voucher code as array index 
                        // Put usage gift card together, because we separate each gift card usage each item
                        // $gift_card_codes = array();
                        // $gift_card_list = array();
                        // foreach ($giftCards as $giftCard) {
                        //     $voucher_code = $giftCard['voucher_code'];
                        //     if(!in_array($voucher_code, $gift_card_codes)) {
                        //         $gift_card_list[$voucher_code] = $voucher_code;
                        //     } else {
                        //         $gift_card_list[$voucher_code]['voucher_amount'] = $gift_card_list[$voucher_code]['voucher_amount'] + $voucher_code['voucher_amount'];
                        //         $gift_card_list[$voucher_code]['voucher_amount_used'] = $gift_card_list[$voucher_code]['voucher_amount_used'] + $voucher_code['voucher_amount_used'];
                        //     }
                        // }

                        $gosendRuleId = explode(',', getenv('GOSEND_RULE_ID'));
                        foreach ($giftCards as $gift_card) {
                            if (in_array($gift_card['rule_id'], $gosendRuleId)) {
                                continue;
                            }
                            $journalSAP->prepareReclassJournal($gift_card, false);
                            $journalSAP->createJournal("reclass");
                        }

                        break;
                    case "reclassReverse":
                        if (empty($salesOrder)) {
                            $this->errorCode = "RR202";
                            $this->errorMessages[] = "Sales Order tidak ditemukan";
                            return false;
                        }

                        $giftCards = json_decode($salesOrder->getGiftCards(), true);
                        $gosendRuleId = explode(',', getenv('GOSEND_RULE_ID'));
                        foreach ($giftCards as $gift_card) {
                            if (in_array($gift_card['rule_id'], $gosendRuleId)) {
                                continue;
                            }
                            $journalSAP->prepareReclassJournal($gift_card, true);
                            if (getenv('VORGDATUM_REVERSAL') == "") {
                                $journalSAP->setVorgdatum(date("Ymd"));
                            } else {
                                $journalSAP->setVorgdatum(getenv('VORGDATUM_REVERSAL'));
                            }
                            $journalSAP->createJournal("reclass_reversal");
                        }

                        break;
                        // For redeem and refund can not build the param here, so skip it
                    case "redeem":
                    case "refund":
                        // Sending refund report to SAP
                        $journalSAP = new \Library\JournalSAP(null, $salesOrder);
                        $headerStatus = $journalSAP->prepareHeader();
                        $journalSAP->setBonnummer($this->credit_memo_no);
                        if ($headerStatus) {
                            $status = $journalSAP->prepareRefundJournal($creditMemoData);
                            if ($status) {
                                $journalSAP->createJournal("credit_memo");
                            }
                        }
                        break;
                    case "incoming":
                        if ($invoice->SalesOrder->SalesOrderPayment->getMethod() != "free_payment") {
                            if (!empty($this->invoice_no)) {
                                $device = $invoice->SalesOrder->getDevice();
                                if ($device != "vendor") {
                                    $journalSAP->prepareIncomingJournal();
                                    $journalSAP->createJournal("incoming");
                                }
                            } else {
                                $journalSAP->prepareIncomingJournal();
                                $journalSAP->createJournal("incoming");
                            }
                        }

                        break;
                    case "incomingReverse":
                        if ($invoice->SalesOrder->SalesOrderPayment->getMethod() != "free_payment") {
                            if (getenv('VORGDATUM_REVERSAL') == "") {
                                $journalSAP->setVorgdatum(date("Ymd"));
                            } else {
                                $journalSAP->setVorgdatum(getenv('VORGDATUM_REVERSAL'));
                            }
                            if (!empty($this->invoice_no)) {
                                $device = $invoice->SalesOrder->getDevice();
                                if ($device != "vendor") {
                                    $journalSAP->prepareIncomingReverseJournal();
                                    $journalSAP->createJournal("incoming_reversal");
                                }
                            } else {
                                $journalSAP->prepareIncomingJournal();
                                $journalSAP->createJournal("incoming_reversal");
                            }
                        }

                        break;
                    case "mdr":
                        if ($invoice->SalesOrder->SalesOrderPayment->getMethod() != "free_payment" && $invoice->SalesOrder->SalesOrderPayment->getMethod() != "manual_transfer") {
                            if (!empty($this->invoice_no)) {
                                $device = $invoice->SalesOrder->getDevice();
                                if ($device != "vendor") {
                                    $journalSAP->prepareMDRJournal();
                                    $journalSAP->createJournal("mdr");
                                }
                            } else {
                                $journalSAP->prepareMDRJournal();
                                $journalSAP->createJournal("mdr");
                            }
                        }
                        break;
                    case "mdrReverse":
                        if ($invoice->SalesOrder->SalesOrderPayment->getMethod() != "free_payment") {
                            if (getenv('VORGDATUM_REVERSAL') == "") {
                                $journalSAP->setVorgdatum(date("Ymd"));
                            } else {
                                $journalSAP->setVorgdatum(getenv('VORGDATUM_REVERSAL'));
                            }
                            if (!empty($this->invoice_no)) {
                                $device = $invoice->SalesOrder->getDevice();
                                if ($device != "vendor") {
                                    $journalSAP->prepareMDRReverseJournal();
                                    $journalSAP->createJournal("mdr_reversal");
                                }
                            } else {
                                $journalSAP->prepareMDRReverseJournal();
                                $journalSAP->createJournal("mdr_reversal");
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        } else {
            //cek b2b or b2c
            if ($orderType == 'B2B') {
                //set is_balance?
                $journalSAP->setIsBalance($JournalParam['IS_BALANCE']);
                $journalSAP->sendJournalB2B($JournalParam, $journal_type, $use_file_param);
            } else {
                //set is_balance?
                if (isset($JournalParam['Journal_B2C']['IS_BALANCE'])) {
                    $journalSAP->setIsBalance($JournalParam['Journal_B2C']['IS_BALANCE']);
                    unset($JournalParam['Journal_B2C']['IS_BALANCE']);
                }

                $journalSAP->sendJournal($JournalParam, $journal_type, $use_file_param);
            }
        }

        return true;
    }

    public function createJournalRegeneratePickup()
    {
        if (empty($this->invoice_no)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "Nomor Invoice diperlukan";
            return false;
        }

        $invoice = $salesOrder = "";
        if (!empty($this->invoice_no)) {
            $invoice = new \Models\SalesInvoice();
            $invoiceResult = $invoice->findFirst("invoice_no = '" . $this->invoice_no . "'");
            $invoice->assign($invoiceResult->toArray([]));
        }

        $journalSAP = new \Library\JournalSAP($invoice, $salesOrder);
        $headerStatus = $journalSAP->prepareHeader();
        if ($headerStatus) {
            $journalSAP->prepareRegeneratePickupVoucherJournal($this->amount);
            $journalSAP->createJournal("regenerate_pickup_3602");
        }

        return true;
    }

    public function createSoB2C()
    {
        if (empty($this->invoice_no)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "No Invoice Diperlukan";
            return false;
        }

        $file_location = VAR_PATH . '/sob2c/so_' . $this->invoice_no . '.xml';
        $invoice = new \Models\SalesInvoice();
        $salesOrderParam = array();
        if (file_exists($file_location)) {
            $xmlContent = file_get_contents($file_location);
            $salesOrderParam = \Helpers\XML2ArrayHelper::createArray($xmlContent);
        } else if (!empty($this->invoice_no)) {
            $resultInvoice = $invoice->findFirst("invoice_no = '" . $this->invoice_no . "'");
            if ($resultInvoice) {
                $invoice->assign($resultInvoice->toArray([]));
            } else {
                $this->errorCode = 404;
                $this->errorMessages[] = "Invoice Tidak Ditemukan";
                return false;
            }
        }

        $soB2C = new \Library\SOB2C($invoice);
        if (empty($salesOrderParam)) {
            $soB2C->prepareDCParams();
            $soB2C->generateDCParams();
        } else {
            $soB2C->setParameter($salesOrderParam);
        }

        $soB2C->createSOB2C();
    }

    public function createSoB2B()
    {
        if (empty($this->invoice_no)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "No Invoice Diperlukan";
            return false;
        }

        $invoice = new \Models\SalesInvoice();
        $resultInvoice = $invoice->findFirst("invoice_no = '" . $this->invoice_no . "'");
        if ($resultInvoice) {
            $invoice->assign($resultInvoice->toArray([]));
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Invoice Tidak Ditemukan";
            return false;
        }

        $soB2B = new \Library\SOB2B($invoice);
        $soB2B->prepareDCParams();
        $soB2B->generateDCParams();

        $soB2B->createSOB2B();
    }

    public function createSoRegisterMember()
    {
        if (empty($this->invoice_no)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "No Invoice Diperlukan";
            return false;
        }

        $invoice = new \Models\SalesInvoice();
        $resultInvoice = $invoice->findFirst("invoice_no = '" . $this->invoice_no . "'");
        if ($resultInvoice) {
            $invoice->assign($resultInvoice->toArray([]));
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Invoice Tidak Ditemukan";
            return false;
        }

        $SalesRegisterMember = new \Library\SalesRegisterMember($invoice);
        $SalesRegisterMember->prepareSalesRegisterMemberParams();
        $SalesRegisterMember->generateSalesRegisterMemberParams();
        $SalesRegisterMember->createSalesRegisterMember();
    }

    public function checkStatusMember($payload = array())
    {

        $curlParam = [
            'timeout' => '30.0',
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode(
                $payload
            )
        ];

        $client = new \GuzzleHttp\Client();
        $responseApi = $client->request('POST', $_ENV['MEMBERSHIP_API_KL'] . "/api/Check_Member_Status", $curlParam);
        return $responseApi->getBody();
    }

    public function checkPointMember($payload = array())
    {

        $curlParam = [
            'timeout' => '30.0',
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode(
                $payload
            )
        ];

        LogHelper::log("check_point_member", sprintf("Hit check point member, member_no: %s, request: %s", $payload['P_card_id'], json_encode($payload)), "info");
        $client = new \GuzzleHttp\Client();
        $responseApi = $client->request('POST', $_ENV['MEMBERSHIP_API_KL'] . "/Get_Point", $curlParam);
        $rawResponseBody = $responseApi->getBody();
        LogHelper::log("check_point_member", sprintf("Hit check point member, member_no: %s, response: %s", $payload['P_card_id'], $rawResponseBody), "info");
        $responseBody = json_decode($rawResponseBody);

        return $responseBody;
    }

    public function checkMemberData($params = array())
    {
        // Payload
        // {
        //     "data": {
        //         "member_id": "IR00378355",
        //         "member_phone": "",
        //         "email": "",
        //         "bu_id": "HCI",
        //         "store_code": ""
        //     }
        // }

        // Default bu_id will be AHI
        // Since storemode mobile app before 3.2.5 version doesnt send store_code nor bu_id on payload
        // Previous version of ho membership endpoint could only check ace membership

        // Define used bu_id
        $buID = !empty($params['bu_id']) ? $params['bu_id'] : "AHI";

        // Get bu_id using store_code if it's passed
        // Right now the only client passing store_code is storemode
        if (!empty($params['store_code'])) {
            // Get supplier alias by store code
            $storeModel = new \Models\Store();
            $storeQuery = sprintf("store_code = '%s'", $params['store_code']);
            $storeData = $storeModel::findFirst($storeQuery);
            if ($storeData) {
                $buID = $storeData->Supplier->getSupplierAlias();
            }
        }

        // Need to check phone number with different format since membership from ho have multiple member phone format 
        // Existing phone format 62,+62, and 0
        $phoneFormatList = array("62", "+62", "0");
        foreach ($phoneFormatList as $phoneFormat) {
            // Preparing payload for klsys membership endpoint 
            $bodyPayload = json_encode([
                "card_id" =>  !empty($params['member_id']) ? $params['member_id'] : "",
                "bu_id" =>  $buID,
                "mem_hp" =>  substr($params["member_phone"], 0, 1) == "0" ? sprintf("%s%s", $phoneFormat, substr($params["member_phone"], 1)) : $params['member_phone'],
                "email" =>  !empty($params['email']) ? $params['email'] : ""
            ]);

            $curlParam = [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $_ENV['MEMBERSHIP_DATA_SEGMENTED_TOKEN']
                ],
                'timeout' => '30',
                'body' => $bodyPayload
            ];

            LogHelper::log("membership_detail", sprintf("Hit membership api, request: %s", $bodyPayload), "info");

            $client = new \GuzzleHttp\Client();
            $klsysURL = sprintf("%s%s", $_ENV['MEMBERSHIP_DATA_SEGMENTED_API'], "/cust_segment_viewer");
            $klsysAPIResponse = $client->request('POST', $klsysURL, $curlParam);
            $memberData = json_decode($klsysAPIResponse->getBody(), true);

            LogHelper::log("membership_detail", sprintf("Hit membership api, request: %s, response: %s", $bodyPayload, json_encode($memberData)), "info");

            // Break loop if found membership data or search by other params beside member phone number
            if (count($memberData['rows']) > 0 || empty($params["member_phone"])) {
                break;
            }
        }

        // If rows is empty and is_ok value false then data not found
        if (empty($memberData['rows']) && $memberData['is_ok'] == 'false') {
            $this->errorCode = 400;
            $this->errorMessages = "Data member tidak ditemukan";
            return;
        }

        $isDuplicate = false;
        if (count($memberData['rows']) > 1) {
            $isDuplicate = true;
        }

        $response = array(
            "is_duplicate" => $isDuplicate,
            "message" => $memberData['message'],
            "member_data" => $memberData["rows"],
        );

        return $response;
    }

    public function checkAceMemberData($params = array())
    {
        $curlParam = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $_ENV['ACE_DATA_SEGMENTED_TOKEN']
            ],
            'body' => json_encode([
                "card_id" => isset($params['member_id']) ? $params['member_id'] : "",
                "mem_hp" => isset($params['member_number']) ? $params['member_number'] : ""
            ])
        ];

        $client = new \GuzzleHttp\Client();
        $responseApi = $client->request('POST', $_ENV['ACE_DATA_SEGMENTED_API'] . "/cust_segment_viewer", $curlParam);
        return $responseApi->getBody();
    }

    public function customerLoginAce($data_array = array())
    {
        // TODO: add login attempt validation when wrong password more than 5 times
        $validate = $this->validateRequired($data_array, ["password"]);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        if (strlen($data_array["password"]) != 6) {
            $this->errorCode = 400;
            $this->errorMessages = "Panjang Password harus 6 digit";
            return false;
        }

        $hasMemberNumber = array_key_exists("member_no", $data_array) && $data_array["member_no"] != null;
        $hasPhone = array_key_exists("phone", $data_array) && $data_array["phone"] != null;

        if (!$hasMemberNumber && !$hasPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Nomor Handphone atau Nomor Member diperlukan";
            return false;
        }

        $loginMethod = "member";
        $password = $data_array["password"];

        $requestPayload["P_Passkey"] = $password;
        if ($hasPhone) {
            $phone = $data_array["phone"];
            $formattedPhone = GeneralHelper::formatPhoneNumber($phone);
            if (!$formattedPhone) {
                $this->errorCode = 400;
                $this->errorMessages = "Format Nomor Handphone tidak valid";
                return false;
            }

            $requestPayload["P_BU_Code"] = "AHI";
            $requestPayload["P_Cust_HP"] = $formattedPhone;
            $loginMethod = "phone";
        } else $requestPayload["P_Card_Id"] = $data_array["member_no"];

        $fingerprint = isset($data_array["fingerprint"]) ? $data_array["fingerprint"] : "";

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $aceLogin = $aceWebAPI->aceLoginCustomer($encryptedPayload, $loginMethod);
        if (!$aceLogin) {
            if ($aceWebAPI->getErrorMessages() == "Terdapat 2 nomor HP yang sama") {
                $this->errorCode = 303;
            } else {
                $this->errorCode = $aceWebAPI->getErrorCode();
            }

            $this->errorMessages = $aceWebAPI->getErrorMessages() !== "" ? $aceWebAPI->getErrorMessages() : "Login Information Tidak Valid";
            LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }
        LogHelper::log("customer_login_ace_response", "ACE LOGIN CUSTOMER | Response: " . json_encode($aceLogin));

        $customerAceId = $aceLogin[0]["cust_id"];

        $customerDataAce = $this->getCustomerDataAce([
            "ace_customer_id" => $customerAceId,
            "customer_token" => $aceLogin[0]["token"]
        ]);
        if (!$customerDataAce) return false;
        $customerDataAce = $customerDataAce[0];

        if ($customerDataAce['Cust_Hp'] == "" || $customerDataAce['Cust_Hp'] == "-") {
            $this->errorCode = 307;
            $this->errorMessages = "ACE Customer Phone tidak valid";
            return false;
        }

        $requestPayload = [
            "P_Cust_ID" => $customerAceId
        ];
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $isPhoneVerified = 0;
        $isEmailVerified = 0;
        $customerVerification = $aceWebAPI->aceCheckCustomerVerification($encryptedPayload);
        if ($customerVerification) {
            $isPhoneVerified = $customerVerification[0]["HP_VALID"] == "1" ? 10 : 0;
            $isEmailVerified = $customerVerification[0]["MAIL_VALID"] == "1" ? 10 : 0;
        }
        LogHelper::log("customer_login_ace_response", "ACE CHECK CUSTOMER VERIFICATION | Response: " . json_encode($customerVerification));

        $customerAceDetailAction = "update";

        $customerAceDetailCondition = "ace_customer_id = '" . $customerAceId . "'";
        if (
            preg_match("/^[0][^0].{0,}$/", $customerAceId)
        ) {
            $customerAceDetailCondition = "ace_customer_id = '" . $customerAceId . "' OR ace_customer_id = $customerAceId";
        }
        $customerAceDetail = $this->getCustomerAceDetail($customerAceDetailCondition);
        if (!$customerAceDetail) {
            if ($this->getErrorCode() != 404) return false;

            $customerAceDetailAction = "create";
        }

        if ($customerDataAce["Cust_Mail"] != "-" && $customerDataAce["Cust_Mail"] != "") {
            $customerDetailTmp = $this->getCustomerDetail("email = '" . $customerDataAce["Cust_Mail"] . "' AND company_code = 'AHI'");
            if ($customerDetailTmp && $customerAceDetailAction == "create") {
                $this->errorCode = 303;
                $this->errorMessages = "E-mail yang dimasukkan sudah digunakan oleh akun lain";
                LogHelper::log("customer_login_ace", "MESSAGE: Duplicate user email: " . $customerDataAce["Cust_Mail"] . " Customer Ace Id: " . $customerAceId);
                return false;
            }

            if ($customerDetailTmp && $customerAceDetailAction == "update" && $customerDetailTmp["customer_id"] != $customerAceDetail["customer_id"]) {
                $this->errorCode = 303;
                $this->errorMessages = "E-mail yang dimasukkan sudah digunakan oleh akun lain";
                LogHelper::log("customer_login_ace", "MESSAGE: Duplicate user email: " . $customerDataAce["Cust_Mail"] . " Customer Ace Id: " . $customerAceId);
                return false;
            }
        }

        $phone = $this->ruparupaFormatPhone($customerDataAce["Cust_Hp"]);
        // tambahin kondisi company type AHI
        $customerDetailTmp = $this->getCustomerDetail("phone = '" . $phone . "' AND company_code = 'AHI'");

        if ($customerDetailTmp && $customerAceDetailAction == "create") {
            $this->errorCode = 303;
            $this->errorMessages = "Nomor telepon yang dimasukkan sudah digunakan oleh akun lain";
            LogHelper::log("customer_login_ace", "MESSAGE: Duplicate user phone number: " . $customerDataAce["Cust_Hp"] . " Customer Ace Id: " . $customerAceId);
            return false;
        }

        if ($customerDetailTmp && $customerAceDetailAction == "update" && $customerDetailTmp["customer_id"] != $customerAceDetail["customer_id"]) {
            $this->errorCode = 303;
            $this->errorMessages = "Nomor Telepon yang dimasukkan sudah digunakan oleh akun lain";
            LogHelper::log("customer_login_ace", "MESSAGE: Duplicate user phone number: " . $customerDataAce["Cust_Hp"] . " Customer Ace Id: " . $customerAceId);
            return false;
        }

        $name = $this->getFirstAndLastName($customerDataAce["Cust_Name"]);

        // in here we need to check if it's already expired or not
        $now = new \DateTime("now");
        $expDate = new \DateTime($customerDataAce["Exp_Date"]);

        $customerEntities = [
            "first_name" => $name["first_name"],
            "last_name" => $name["last_name"],
            "phone" => $phone,
            "email" => $customerDataAce["Cust_Mail"],
            "password" => $password,
            "company_code" => "AHI",
            "status" => 10,
            "birth_date" => str_replace("/", "-", $customerDataAce["Cust_Birth_date"]),
            "is_phone_verified" => $isPhoneVerified,
            "is_email_verified" => $isEmailVerified,
            "last_login" => date("Y-m-d H:i:s"),
        ];
        if ($fingerprint != "") $customerEntities["fingerprint"] = $fingerprint;

        $customerAceDetailsEntities = [
            "ace_customer_id" =>  (string)$customerDataAce["Cust_Id"],
            "ace_customer_member_no" => (string)$customerDataAce["Card_Id"],
            "customer_token" => $aceLogin[0]["token"],
            "status" => ($now <= $expDate) ? 1 : 0,
            "expired_date" => $expDate->format('Y-m-d 23:59:59')
        ];

        $customerEntities["group"] = [
            "AHI" => $customerDataAce["Card_Id"],
            "AHI_expiration_date" => $expDate->format('Y-m-d 23:59:59'),
            "AHI_is_verified" => ($now <= $expDate) ? 10 : 5
        ];

        $customerNewModel = new \Models\Customer();
        switch ($customerAceDetailAction) {
            case "update":
                $customerAceDetailsEntities["customer_ace_details_id"] = $customerAceDetail["customer_ace_details_id"];
                $customerAceDetailsEntities["customer_id"] = $customerAceDetail["customer_id"];

                $this->customerAceDetails->setFromArray($customerAceDetailsEntities);
                if (!$this->customerAceDetails->saveData("register_ace_from_login")) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Gagal update Customer Detail ACE";
                    LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
                    return false;
                }

                $customer = $this->getCustomerDetail("customer_id = " . $customerAceDetail["customer_id"]);
                if (!$customer) {
                    if ($this->getErrorCode() != 404) return false;

                    $customerNewModel->setFromArray($customerEntities);
                    $customerNewModel->setRegisteredBy("ace (mobile apps)");
                    if (!$customerNewModel->saveData("register_ace_from_login")) {
                        $this->errorCode = 400;
                        $this->errorMessages = "Gagal menyimpan Customer ACE";
                        LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
                        return false;
                    }
                } else {
                    $customerEntities["customer_id"] = $customerAceDetail["customer_id"];

                    LogHelper::log("customer_update_group", "MESSAGE: " . json_encode($customerEntities) . "; customerAceDetailAction: " . $customerAceDetailAction);

                    $customerNewModel->setFromArray($customerEntities);
                    if (!$customerNewModel->saveData("register_ace_from_login")) {
                        $this->errorCode = 400;
                        $this->errorMessages = "Gagal update Customer ACE";
                        LogHelper::log("customer_update_group", "MESSAGE: " . $this->errorMessages);
                        return false;
                    }
                }
                break;
            case "create":
                $customerNewModel->setFromArray($customerEntities);
                $customerNewModel->setRegisteredBy("ace (mobile apps)");
                if (!$customerNewModel->saveData("register_ace_from_login")) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Gagal menyimpan Customer ACE";
                    LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
                    return false;
                }

                $customerAceDetailsEntities["customer_id"] = $customerNewModel->getCustomerId();
                $this->customerAceDetails->setFromArray($customerAceDetailsEntities);
                if (!$this->customerAceDetails->saveData("register_ace_from_login")) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Gagal menyimpan Customer Detail ACE";
                    LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
                    return false;
                }
                break;
        }

        $customerData = $customerNewModel->getDataArray();

        $currentCartId = empty($data_array['cart_id']) ? "" : $data_array['cart_id'];
        $cartLib = new \Library\Cart();
        $storeCodeNewRetail = isset($data_array['store_code_new_retail']) ? $data_array['store_code_new_retail'] : '';
        $respMergeCart = $cartLib->getCustomerCartId($customerNewModel->getCustomerId(), $currentCartId, "ODI", $storeCodeNewRetail);
        $shoppingCartId = !empty($respMergeCart['data']['cart_id']) ? $respMergeCart['data']['cart_id'] : "";
        $customerData["cart_id"] = $shoppingCartId;


        // In here we need to pass the value of email, phone and company_code to queue-v2 for linking customer
        $customerHelper = new \Helpers\CustomerHelper();
        $customerHelper->linkingCustomerUsingNsq($customerData["email"], $customerData["phone"], $customerData["company_code"]);

        // Hit login logout flag API
        $loginLogoutEncryptedPayload = $aceWebAPI->aceWebAPIEncrypt([
            'p_card_id' => $customerDataAce["Card_Id"],
            'p_flag_login' => '1'
        ]);
        if (!$loginLogoutEncryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $isVoucherClaim = $this->claimSentVoucher($customerAceDetail["customer_id"], strtolower($customerDataAce["Cust_Mail"]));
        if (!$isVoucherClaim) {
            LogHelper::log("customer_login_ace", "MESSAGE: Something wrong claiming sent voucher(s)");
            return false;
        }

        $isLoginFlagSent = $aceWebAPI->loginLogoutFlagAce($loginLogoutEncryptedPayload);
        if (!$isLoginFlagSent) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages() !== "" ? $aceWebAPI->getErrorMessages() : "Login Flag Tidak Terkirim";
            LogHelper::log("customer_login_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }
        // End of hit login logout flag API

        // /api/
        $customerData["ace_customer_id"] = isset($customerAceDetailsEntities["ace_customer_id"])
            ? $customerAceDetailsEntities["ace_customer_id"] : null;

        // Unsetting some variables before return the result
        unset($customerData["password"]);
        unset($customerData["created_at"]);
        unset($customerData["updated_at"]);
        unset($customerData["company_code"]);

        return $customerData;
    }

    public function customerLogoutAce($data_array = array())
    {
        $requiredKey = ["customer_id"];
        $validate = $this->validateRequired($data_array, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        $customerAceDetail = $this->getCustomerAceDetail("customer_id = " . $data_array['customer_id']);
        if (!$customerAceDetail) return false;

        // Hit login logout flag API
        $aceWebAPI = new \Library\AceWebAPI();
        $loginLogoutEncryptedPayload = $aceWebAPI->aceWebAPIEncrypt([
            'p_card_id' => $customerAceDetail["ace_customer_member_no"],
            'p_flag_login' => '2'
        ]);
        if (!$loginLogoutEncryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("customer_logout_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $isLoginFlagSent = $aceWebAPI->loginLogoutFlagAce($loginLogoutEncryptedPayload);
        if (!$isLoginFlagSent) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages() !== "" ? $aceWebAPI->getErrorMessages() : "Login Flag Tidak Terkirim";
            LogHelper::log("customer_logout_ace", "MESSAGE: " . $this->errorMessages . ",CUSTOMERID: " . $data_array['customer_id']);
            return false;
        }
        // End of hit login logout flag API

        return true;
    }

    public function registerCustomerAce($sending_welcome_email = true, $params = array())
    {
        $storeCodeNewRetail = isset($params['store_code_new_retail']) && !empty($params['store_code_new_retail']) ? $params['store_code_new_retail'] : "";

        $requiredKey = ["name", "phone", "email", "otp_number", "birth_date"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        if (!filter_var($params["email"], FILTER_VALIDATE_EMAIL)) {
            $this->errorCode = 400;
            $this->errorMessages = "Format e-mail tidak valid";
            return false;
        }

        $formattedPhone = GeneralHelper::formatPhoneNumber($params["phone"]);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format Nomor Handphone tidak valid";
            return false;
        }

        if (strlen($params["otp_number"]) != 6) {
            $this->errorCode = 400;
            $this->errorMessages = "Kode OTP harus 6 digit";
            return false;
        }

        $isEmailAvailableAce = $this->checkEmailAvailableAce($params["email"], "A");
        if (!$isEmailAvailableAce) return false;

        $isPhoneAvailableAce = $this->checkPhoneAvailableAce($params["phone"], "A");
        if (!$isPhoneAvailableAce) return false;

        $aceWebAPI = new \Library\AceWebAPI();
        $validateOtp = $aceWebAPI->validateOtp($formattedPhone, $params["otp_number"], "register");
        if (!$validateOtp) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            return false;
        }

        $databaseFormatBirthDate = GeneralHelper::from_indonesian_date($params["birth_date"]);
        $employeeHRIS = array();
        $employeeReferralID = (isset($params['employee_referral_id']) && !empty($params['employee_referral_id'])) ? $params['employee_referral_id'] : null;
        if (isset($employeeReferralID)) {
            $employeeLib = new \Library\Employee();
            $employeeHRIS = $employeeLib->getEmployeeDataByNip(
                array(
                    'nip' => $employeeReferralID,
                    'business_unit' => "AHI"
                )
            );
        }

        $registerRequestPayload = [
            "P_Company" => "AHI",
            "P_Cust_Name" => $params["name"],
            "P_Cust_Add" => "",
            "P_Cust_Hp" => $formattedPhone,
            "P_Cust_Mail" => $params["email"],
            "P_Cust_Birth_Date" => $databaseFormatBirthDate,
            "P_Cust_Birth_Place" => "",
            "P_Cust_Sex" => "0",
            "P_Cust_City" => "",
            "P_Cust_PosCode" => "",
            "P_Cust_Add_Ktp" => "",
            "P_Cust_Phone" => "",
            "P_Cust_IdCard" => "",
            "P_Cust_Religion" => "",
            "P_Cust_Stat_Marital" => "",
            "P_Cust_citizenship" => "",
            "P_Cust_Employ" => "",
            "P_Cust_Purpose" => "",
            "P_Cust_AutoRen" => "",
            "P_Card_Ref" => "",
            "P_Reference_Date" => "",
            "P_Company_Code" => !empty($employeeHRIS) ? $employeeHRIS["site_code_legacy"] : "00",
            "P_Cust_Ref" => !empty($employeeHRIS) ? "AP-" . $employeeHRIS["nip"] : "APP",
            "P_Cust_FreeF" => "1"
        ];

        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($registerRequestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("register_customer_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $aceRegisterCustomer = $aceWebAPI->aceRegisterCustomer($encryptedPayload);
        if (!$aceRegisterCustomer) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = "Terjadi kesalahan koneksi";

            LogHelper::log("register_customer_ace", "MESSAGE: " . $aceWebAPI->getErrorMessages() . ",PAYLOAD:" . json_encode($registerRequestPayload));

            return false;
        }

        LogHelper::log("register_customer_ace_response", "ACE REGISTER CUSTOMER | Request Payload: " . json_encode($registerRequestPayload) . " Response: " . json_encode($aceRegisterCustomer));

        $aceRegisterCustomer = $aceRegisterCustomer[0];

        $requestPayload = [
            "P_Cust_id" => $aceRegisterCustomer["Cust_Id"],
            "P_Is_phone_verified" => "1",
            "P_created_on" => date('Y-m-d H:i:s')
        ];
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("register_customer_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $verificationResponseRows = $aceWebAPI->aceVerifyPhone($encryptedPayload);
        if (!$verificationResponseRows) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("register_customer_ace", "MESSAGE: " . $aceWebAPI->getErrorMessages());
            return false;
        }
        LogHelper::log("register_customer_ace_response", "ACE VERIFY DATA | Request Payload: " . json_encode($requestPayload) . " Response: " . json_encode($verificationResponseRows));

        $customerDetailTmp = $this->getCustomerDetail("email = '" . $params["email"] . "'");
        if (!$customerDetailTmp && $this->getErrorCode() != 404) {
            return false;
        }
        if ($customerDetailTmp) {
            $this->errorCode = 303;
            $this->errorMessages = "E-mail sudah terdaftar. Silakan gunakan yang lain.";
            LogHelper::log("register_customer_ace", "MESSAGE: Duplicate user email: " . $params["email"] . " Card Ace Id: " . $aceRegisterCustomer["Card"]);
            return false;
        }

        $rupaRupaPhone = $this->ruparupaFormatPhone($params["phone"]);
        $customerDetailTmp = $this->getCustomerDetail("phone = '" . $rupaRupaPhone . "'");
        if (!$customerDetailTmp && $this->getErrorCode() != 404) {
            return false;
        }
        if ($customerDetailTmp) {
            $this->errorCode = 303;
            $this->errorMessages = "Nomor HP sudah terdaftar. Silakan gunakan yang lain.";
            LogHelper::log("register_customer_ace", "MESSAGE: Duplicate user phone number: " . $params["phone"] . " Card Ace Id: " . $aceRegisterCustomer["Card"]);
            return false;
        }

        $name = $this->getFirstAndLastName($params["name"]);
        $customerEntities = [
            "first_name" => $name["first_name"],
            "last_name" => $name["last_name"],
            "phone" => $params["phone"],
            "email" => $params["email"],
            "password" => $aceRegisterCustomer["Passkey"],
            "company_code" => "AHI",
            "status" => 10,
            "birth_date" => $databaseFormatBirthDate,
            "is_phone_verified" => 10,
            "is_email_verified" => 0,
            "last_login" => date("Y-m-d H:i:s"),
        ];

        $customerNewModel = new \Models\Customer();
        $customerNewModel->setFromArray($customerEntities);
        $customerNewModel->setRegisteredBy("ace (mobile apps)");
        if (!$customerNewModel->saveData("register_ace_from_login")) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal menyimpan Customer ACE";
            LogHelper::log("register_customer_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $customerAceDetailsEntities = [
            "ace_customer_id" => (string)$aceRegisterCustomer["Cust_Id"],
            "ace_customer_member_no" => (string)$aceRegisterCustomer["Card"],
            "customer_id" => $customerNewModel->getCustomerId(),
        ];

        $this->customerAceDetails->setFromArray($customerAceDetailsEntities);
        if (!$this->customerAceDetails->saveData("register_ace_from_login")) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal menyimpan Customer ACE";
            LogHelper::log("register_customer_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        if (isset($employeeReferralID)) {
            $storeReferralLib = new \Library\AceHardware\StoreReferral();
            $storeReferralLib->saveStoreReferral([
                'customer_id' => $customerNewModel->getCustomerId(),
                'employee_referral_nip' => $params['employee_referral_id'],
                'type' => 'register',
                'sales_order_id' => null
            ]);
            if (!$storeReferralLib) {
                $this->errorCode = 400;
                $this->errorMessages = "Gagal menyimpan data store referral";
                LogHelper::log("register_customer_ace", "MESSAGE: " . $this->errorMessages);
                return false;
            }
        }

        $sendOtpRows = $aceWebAPI->sendWhatsapp(
            $formattedPhone,
            'register_send_card_id',
            array(
                'passkey' => $aceRegisterCustomer["Passkey"],
                'member_no' => $aceRegisterCustomer["Card"]
            )
        );
        if (!$sendOtpRows) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("register_customer_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return [
            "member_no" => $aceRegisterCustomer["Card"],
            "password" => $aceRegisterCustomer["Passkey"],
            "phone" => "",
            "store_code_new_retail" => $storeCodeNewRetail,
            "company_code" => "AHI"
        ];
    }

    public function registerCustomerMemberAce($params)
    {
        $requiredKey = ["customer_id", "name", "phone", "email", "otp_number", "birth_date"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        if (!filter_var($params["email"], FILTER_VALIDATE_EMAIL)) {
            $this->errorCode = 400;
            $this->errorMessages = "Email format tidak valid";
            return false;
        }

        $formattedPhone = GeneralHelper::formatPhoneNumber($params["phone"]);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format Nomor Handphone tidak valid";
            return false;
        }

        if (strlen($params["otp_number"]) != 6) {
            $this->errorCode = 400;
            $this->errorMessages = "Kode OTP harus berupa 6 digit";
            return false;
        }

        $isEmailAvailableAce = $this->checkEmailAvailableAce($params["email"], "A");
        if (!$isEmailAvailableAce) return false;

        $isPhoneAvailableAce = $this->checkPhoneAvailableAce($params["phone"], "A");
        if (!$isPhoneAvailableAce) return false;

        $aceWebAPI = new \Library\AceWebAPI();
        $validateOtp = $aceWebAPI->validateOtp($formattedPhone, $params["otp_number"], "register");
        if (!$validateOtp) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            return false;
        }

        $registerRequestPayload = [
            "P_Company" => "AHI",
            "P_Cust_Name" => $params["name"],
            "P_Cust_Add" => "",
            "P_Cust_Hp" => $formattedPhone,
            "P_Cust_Mail" => $params["email"],
            "P_Cust_Birth_Date" => GeneralHelper::from_indonesian_date($params["birth_date"]),
            "P_Cust_Birth_Place" => "",
            "P_Cust_Sex" => "0",
            "P_Cust_City" => "",
            "P_Cust_PosCode" => "",
            "P_Cust_Add_Ktp" => "",
            "P_Cust_Phone" => "",
            "P_Cust_IdCard" => "",
            "P_Cust_Religion" => "",
            "P_Cust_Stat_Marital" => "",
            "P_Cust_citizenship" => "",
            "P_Cust_Employ" => "",
            "P_Cust_Purpose" => "",
            "P_Cust_AutoRen" => "",
            "P_Card_Ref" => "",
            "P_Reference_Date" => "",
            "P_Company_Code" => "00",
            "P_Cust_Ref" => "APP",
            "P_Cust_FreeF" => "1"
        ];

        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($registerRequestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("register_customer_member_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $aceRegisterCustomer = $aceWebAPI->aceRegisterCustomer($encryptedPayload);
        if (!$aceRegisterCustomer) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("register_customer_member_ace", "MESSAGE: " . $this->errorMessages . ",PAYLOAD:" . json_encode($registerRequestPayload));
            return false;
        }

        LogHelper::log("register_customer_member_ace_response", "ACE REGISTER CUSTOMER | Request Payload: " . json_encode($registerRequestPayload) . " Response: " . json_encode($aceRegisterCustomer));

        $aceRegisterCustomer = $aceRegisterCustomer[0];

        $requestPayload = [
            "P_Card_Id" => $aceRegisterCustomer["Card"],
            "P_Passkey" => $aceRegisterCustomer["Passkey"]
        ];

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("register_customer_member_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $aceLogin = $aceWebAPI->aceLoginCustomer($encryptedPayload, "member");
        if (!$aceLogin) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages() !== "" ? $aceWebAPI->getErrorMessages() : "Login information tidak valid";
            LogHelper::log("register_customer_member_ace", "MESSAGE: " . $this->errorMessages . ",PAYLOAD: " . json_encode($requestPayload));
            return false;
        }
        LogHelper::log("register_customer_member_ace_response", "ACE LOGIN CUSTOMER | Request Payload: " . json_encode($requestPayload) . " Response: " . json_encode($aceLogin));

        $aceLogin = $aceLogin[0];

        $customerDataAce = $this->getCustomerDataAce([
            "ace_customer_id" => $aceLogin["cust_id"],
            "customer_token" => $aceLogin["token"]
        ]);
        if (!$customerDataAce) return false;

        $customerDataAce = $customerDataAce[0];

        $nowDate = new \DateTime("now");
        $expDate = new \DateTime($customerDataAce["Exp_Date"]);
        $customerEntities = [
            "customer_id" => $params["customer_id"],
            "group" => [
                "AHI" => $customerDataAce["Card_Id"],
                "AHI_expiration_date" => $expDate->format('Y-m-d 23:59:59'),
                "AHI_is_verified" => $nowDate <= $expDate ? 10 : 5,
            ]
        ];

        $customer = new \Models\Customer();
        $customer->setFromArray($customerEntities);
        if (!$customer->saveData()) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal menyimpan Customer ACE";
            LogHelper::log("register_customer_member_ace_", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $sendOtpRows = $aceWebAPI->sendWhatsapp(
            $formattedPhone,
            'register_send_card_id',
            array(
                'passkey' => $aceRegisterCustomer["Passkey"],
                'member_no' => $aceRegisterCustomer["Card"]
            )
        );
        if (!$sendOtpRows) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("register_customer_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return true;
    }

    public function generateOtpAce($params = array())
    {
        $requiredKey = ["action"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        $aceWebApiLib = new \Library\AceWebAPI();
        $now = new \DateTime('NOW');
        $now->add(new \DateInterval('PT2M'));
        $expirationTime =  $now->format('Y-m-d H:i:s');

        switch ($params["action"]) {
            case "register":
                return $this->generateOTPRegister($params, $aceWebApiLib);
            case "verify_phone":
                return $this->generateOTPVerifyPhone($params, $aceWebApiLib);
            case "verify_email":
                return $this->generateOTPAceVerifyEmail($params);
            case "validate_change_phone":
                return $this->generateGenericOTP(
                    'change_phone',
                    'verify_hp',
                    $params,
                    $aceWebApiLib,
                    $expirationTime
                );
            case "validate_change_email":
                return $this->generateGenericOTP(
                    'change_email',
                    'change_email',
                    $params,
                    $aceWebApiLib,
                    $expirationTime
                );
            case "change_phone":
                return $this->generateOTPChangeNewPhone($params, $aceWebApiLib);
            case "change_email":
                return $this->generateOTPChangeNewEmail($params, $expirationTime);
            case "redeem_point":
                return $this->generateGenericOTP(
                    'redeem_point',
                    'redeem_otp',
                    $params, // [customer_id, 'send_to' = phone, 'phone' = 08xxx]
                    $aceWebApiLib,
                    $expirationTime
                );
            case "digital_stamp":
                return $this->generateGenericOTP(
                    'digital_stamp',
                    'stamp_miss_ace',
                    $params, // [customer_id, 'send_to' = phone, 'phone' = 08xxx]
                    $aceWebApiLib,
                    $expirationTime
                );
            case "send_voucher":
                return $this->generateGenericOTP(
                    'send_voucher',
                    'otp_send_voucher1',
                    $params, // [customer_id, 'send_to' = phone, 'phone' = 08xxx]
                    $aceWebApiLib,
                    $expirationTime
                );
            default:
                $this->errorCode = 400;
                $this->errorMessages = "Action tidak ditemukan";
                return false;
        }
    }

    public function generateOTPAceVerifyEmail($params = array())
    {
        $requiredKey = ["customer_id", "email"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        return $this->sendVerifyEmailOtp($params["customer_id"], $params["email"]);
    }

    public function generateOTPRegister(array $params, AceWebAPI $aceWebApiLib)
    {
        $requiredKey = ["phone"];

        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }
        $formattedPhone = GeneralHelper::formatPhoneNumber($params["phone"]);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format nomor handphone tidak valid";
            return false;
        }
        $generateOtp = $this->generateOTP($formattedPhone, $params, $aceWebApiLib);
        if (!$generateOtp) return false;

        $sendPhoneOTP = $this->sendPhoneOTPToWhatsapp($aceWebApiLib, $generateOtp, $formattedPhone, "register_otp");
        if (!$sendPhoneOTP) return false;

        return true;
    }

    public function generateOTPVerifyPhone(array $params,  AceWebAPI $aceWebApiLib)
    {
        $requiredKey = ["phone"];

        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }
        $formattedPhone = GeneralHelper::formatPhoneNumber($params["phone"]);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Phone number format is not valid";
            return false;
        }
        $generateOtp = $this->generateOTP($formattedPhone, $params, $aceWebApiLib);
        if (!$generateOtp) return false;

        $sendPhoneOTP = $this->sendPhoneOTPToWhatsapp($aceWebApiLib, $generateOtp, $formattedPhone, "verify_hp");
        if (!$sendPhoneOTP) return false;

        return true;
    }

    /**
     * @param string $customerAceOtpType valid type field in 
     * customer_ace_otp_temp table
     * @param string $whatsappTemplate valid whatsapp template to use
     * @param array $params (req body)
     * @param AceWebAPI $aceWebApiLib (Ace endpoint library)
     * @param string $expirationTime (Stringify expiration datetime)
     * @return bool
     */
    public function generateGenericOTP(
        string $customerAceOtpType,
        string $whatsappTemplate,
        array $params,
        AceWebAPI $aceWebApiLib,
        string $expirationTime
    ) {
        $requiredKey = ["customer_id", "send_to", "phone"];

        if ($params["send_to"] !== "phone") array_push($requiredKey, "email");

        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        $customerAceDetail = $this->getCustomerAceDetail("customer_id = " . $params['customer_id']);
        if (!$customerAceDetail) return false;

        $customerAce = $this->getCustomerDataAce($customerAceDetail);
        if (!$customerAce) return false;

        $customerAce = $customerAce[0];

        $customer = $this->getCustomerDetail("customer_id = '" . $params["customer_id"] . "' AND " . $params["send_to"] . " = '" . $params[$params["send_to"]] . "'");
        if (!$customer || $this->getErrorCode() == 404 || $this->getErrorCode() == 503) {
            $this->errorCode = 400;
            $this->errorMessages = "Data Customer tidak ditemukan";
            return false;
        }

        $lengthOtp = 6;
        $lastName = array_key_exists("last_name", $customer) ? $customer : "";
        // Send OTP to email
        if ($params["send_to"] == "email") {
            if (!filter_var($customerAce["Cust_Mail"], FILTER_VALIDATE_EMAIL)) {
                $this->errorCode = 400;
                $this->errorMessages = "E-mail format tidak valid";
                return false;
            }
            $isUpsertCustOtpTemp = $this->generateUpsertCustOtpTemp($lengthOtp, $params["customer_id"], "email", $customerAce["Cust_Mail"], $customerAceOtpType);
            if (!$isUpsertCustOtpTemp) {
                return false;
            }
            $isSendEmailOtp = $this->sendOtpEmail($isUpsertCustOtpTemp, $expirationTime, $customerAce["Cust_Mail"], $customer["first_name"], $lastName, $customerAceOtpType);
            if (!$isSendEmailOtp) {
                return false;
            }

            return true;
        }
        // Send OTP to phone
        if ($params["send_to"] == "phone") {
            $formattedWhatsappPhoneNumber = GeneralHelper::formatPhoneNumberToValidNumberForWhatsapp($customerAce["Cust_Hp"]);
            if (!$formattedWhatsappPhoneNumber) {
                $this->errorCode = 400;
                $this->errorMessages = "Format nomor handphone tidak valid";
                return false;
            }

            $isUpsertCustOtpTemp = $this->generateUpsertCustOtpTemp($lengthOtp, $params["customer_id"], "phone", $customerAce["Cust_Hp"],  $customerAceOtpType);
            if (!$isUpsertCustOtpTemp) {
                return false;
            }

            $isSendPhoneOTP = $this->sendPhoneOTPToWhatsapp($aceWebApiLib, $isUpsertCustOtpTemp, $customerAce["Cust_Hp"], $whatsappTemplate);
            if (!$isSendPhoneOTP) {
                return false;
            }

            return true;
        }
    }

    public function generateOTPChangeNewPhone(array $params, AceWebAPI $aceWebApiLib)
    {
        $LENGTH_OTP = 6;
        $OLD_TYPE = "valid_change_phone";

        $requiredKey = ["customer_id", "phone", "new_phone", "otp_number"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        $formattedOldPhone = GeneralHelper::formatPhoneNumber($params["phone"]);
        if (!$formattedOldPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format nomor handphone lama tidak valid";
            return false;
        }

        $formattedNewPhone = GeneralHelper::formatPhoneNumber($params["new_phone"]);
        if (!$formattedNewPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format nomor handphone baru tidak valid";
            return false;
        }

        $checkUniqueNewPhoneData = $this->checkUniquePrimaryData("phone", $params["customer_id"], $params["new_phone"]);
        if (!$checkUniqueNewPhoneData) return false;

        $customerDetail = $this->getCustomerDetail("customer_id = '" . $params["customer_id"] . "' AND phone = '" . $params["phone"] . "'");
        if (!$customerDetail || $this->getErrorCode() == 404 || $this->getErrorCode() == 503) {
            $this->errorCode = 400;
            $this->errorMessages = "Nomor handphone tidak ditemukan " . $params["phone"];
            LogHelper::log("get_detail_customer_by_old_phone", "Data Change: phone ,Cust Id: " . $params["customer_id"] . " ,Old Data: " . $params["phone"]);
            return false;
        }

        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $customerAceOtpTemp = $customerAceOtpTempModel->findFirst("type = '" . $OLD_TYPE . "' AND customer_id = '" . $params["customer_id"] . "' AND access_code = '" . $params["otp_number"] . "'");

        if (!$customerAceOtpTemp) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal mendapatkan customer ACE OTP temp";
            LogHelper::log("validateOtpExpired", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $generatedRandomNumber = $this->generateUpsertCustOtpTemp(
            $LENGTH_OTP,
            $params["customer_id"],
            "phone",
            $formattedNewPhone,
            "change_phone"
        );
        if (!$generatedRandomNumber) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal melakukan insert/update Customer ACE OTP temp";
            LogHelper::log("generateOTPChangeNewPhone", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $isSendPhoneOTP = $this->sendPhoneOTPToWhatsapp($aceWebApiLib, $generatedRandomNumber, $formattedNewPhone, "verify_hp");
        if (!$isSendPhoneOTP) {
            $this->errorCode = 400;
            $this->errorMessages = "gagal mengirim OTP";
            return false;
        }

        return true;
    }

    public function generateOTPChangeNewEmail(array $params, string $expirationTime)
    {
        $LENGTH_OTP = 6;
        $OLD_TYPE = "valid_change_email";

        $requiredKey = ["customer_id", "email", "new_email", "otp_number"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        if (!filter_var($params["email"], FILTER_VALIDATE_EMAIL)) {
            $this->errorCode = 400;
            $this->errorMessages = "Format e-mail lama tidak valid";
            return false;
        }

        if (!filter_var($params["new_email"], FILTER_VALIDATE_EMAIL)) {
            $this->errorCode = 400;
            $this->errorMessages = "Format e-mail baru tidak valid";
            return false;
        }

        $customerDetailByNewData = $this->getCustomerDetail("email = '" . $params["new_email"] . "'");
        if ($customerDetailByNewData) {
            $this->errorCode = 400;
            $this->errorMessages = "E-mail baru tidak boleh sama dengan email lama";
            LogHelper::log("get_unique_new_email_phone", "Data Change: email ,Cust Id: " . $params["customer_id"] . " ,New Data: " . $params["new_email"] . ", Already exist in database");
            return false;
        }

        $customerDetail = $this->getCustomerDetail("customer_id = '" . $params["customer_id"] . "' AND email = '" . $params["email"] . "'");
        if (!$customerDetail || $this->getErrorCode() == 404 || $this->getErrorCode() == 503) {
            $this->errorCode = 400;
            $this->errorMessages = "E-mail tidak ditemukan " . $params["email"];
            LogHelper::log("get_detail_customer_by_old_email", "Data Change: email ,Cust Id: " . $params["customer_id"] . " ,Old Data: " . $params["email"]);
            return false;
        }

        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $customerAceOtpTemp = $customerAceOtpTempModel->findFirst("type = '" . $OLD_TYPE . "' AND customer_id = '" . $params["customer_id"] . "' AND access_code = '" . $params["otp_number"] . "'");

        if (!$customerAceOtpTemp) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal mendapatkan customer ACE OTP temp";
            LogHelper::log("validateOtpExpired", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $generatedRandomNumber = $this->generateUpsertCustOtpTemp(
            $LENGTH_OTP,
            $params["customer_id"],
            "email",
            $params["new_email"],
            "change_email"
        );
        if (!$generatedRandomNumber) {
            $this->errorCode = 400;
            $this->errorMessages = "gagal insert/update customer ACE OTP temp";
            LogHelper::log("generateOTPChangeNewEmail", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $lastName = array_key_exists("last_name", $customerDetail) ? $customerDetail : "";
        $isSendEmailOtp = $this->sendOtpEmail($generatedRandomNumber, $expirationTime, $params["new_email"], $customerDetail["first_name"], $lastName, "change_email");
        if (!$isSendEmailOtp) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal mengirim OTP";
            return false;
        }

        return true;
    }

    public function generateUpsertCustOtpTemp($length, $customerId, $dataChange, $dataRequest, $type)
    {
        $generatedRandomNumber = GeneralHelper::generateRandomNumber($length);
        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $manager = new TxManager();
        $manager->setDbService($customerAceOtpTempModel->getConnection());
        $transaction = $manager->get();

        if ($dataChange != "phone" && $dataChange != "email") {
            $this->errorCode = 400;
            $this->errorMessages = "Data change harus berupa nomor handphone atau e-mail";
            LogHelper::log("generateUpsertCustOtpTemp", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        try {
            $customerAceOtpTemp = $customerAceOtpTempModel->findFirst($dataChange . " = '" . $dataRequest . "' AND type = '" . $type . "' AND customer_id ='" . $customerId . "'");

            if (!$customerAceOtpTemp) {
                $sql = "INSERT INTO customer_ace_otp_temp (" . $dataChange . ", request_count, access_code, customer_id, type) ";
                $sql .= "VALUES('" . $dataRequest . "', '1', '" . $generatedRandomNumber . "', '" . $customerId . "','" . $type . "')";
            } else {
                $requestCount = $customerAceOtpTemp->getRequestCount() + 1;
                $sql = "UPDATE customer_ace_otp_temp SET request_count = '" . $requestCount . "', access_code = '" . $generatedRandomNumber . "' WHERE customer_ace_otp_temp_id = '" . $customerAceOtpTemp->getCustomerAceOtpTempId() . "'";
            }

            $customerAceOtpTempModel->useWriteConnection();
            $customerAceOtpTempModel->getDi()->getShared($customerAceOtpTempModel->getConnection())->query($sql);
            $customerAceOtpTempModel->setTransaction($transaction);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal insert/update customer ACE OTP temp";
            LogHelper::log("generateUpsertCustOtpTemp", "MESSAGE: " . $this->errorMessages);
            return false;
        }
        $transaction->commit();

        return $generatedRandomNumber;
    }

    public function generateOTP(string $formattedPhone, array $params, AceWebAPI $aceWebApiLib)
    {
        $mappedAction = $aceWebApiLib->getAceAction($params["action"]);
        if ($mappedAction == "") {
            $this->errorCode = 400;
            $this->errorMessages = "Action tidak ditemukan";
            return false;
        }

        $generatedOtp = $aceWebApiLib->generateOtp($formattedPhone, $mappedAction);

        if (!$generatedOtp) {
            $this->errorCode = $aceWebApiLib->getErrorCode();
            $this->errorMessages = $aceWebApiLib->getErrorMessages();
            LogHelper::log("generate_otp_ace", "MESSAGE: " . $this->errorMessages . ",Action " . $mappedAction . ", Phone" . $formattedPhone);
            return false;
        }

        return $generatedOtp;
    }

    public function validateOtpAce($params = array())
    {
        $requiredKey = ["action", "otp_number"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        if (strlen($params["otp_number"]) != 6) {
            $this->errorCode = 400;
            $this->errorMessages = "Nomor OTP tidak valid";
            return false;
        }

        $aceWebApiLib = new \Library\AceWebAPI();

        switch ($params["action"]) {
            case "verify_email":
                return $this->validateOtpVerifyEmail($params);
            case "verify_phone":
                return $this->validateOtpVerifyPhone($params, $aceWebApiLib);
            case "validate_change_phone":
                return $this->validateGenericOtp('change_phone', $params);
            case "validate_change_email":
                return $this->validateGenericOtp('change_email', $params);
            case "change_phone":
                return $this->validateOtpChangeNewPhone($params);
            case "change_email":
                return $this->validateOtpChangeNewEmail($params);
            case "redeem_point":
                return $this->validateGenericOtp('redeem_point', $params);
            case "digital_stamp":
                return $this->validateGenericOtp('digital_stamp', $params);
            case "send_voucher":
                return $this->validateGenericOtp('send_voucher', $params);
            default:
                $this->errorCode = 400;
                $this->errorMessages = "Action tidak ditemukan";
                return false;
        }
    }

    public function validateOtpChangeNewPhone($params)
    {
        $requiredKey = ["customer_id", "phone", "otp_number"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        $formattedPhone = GeneralHelper::formatPhoneNumber($params["phone"]);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format nomor handphone tidak valid";
            return false;
        }

        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $manager = new TxManager();
        $manager->setDbService($customerAceOtpTempModel->getConnection());
        $transaction = $manager->get();

        $customerAceOtpTemp = $customerAceOtpTempModel->findFirst("type = 'change_phone' AND customer_id ='" . $params['customer_id'] . "' AND access_code ='" . $params['otp_number'] . "'");
        if (!$customerAceOtpTemp) {
            $this->errorCode = 400;
            $this->errorMessages = "Nomor OTP Salah";
            return false;
        }

        $validateOtpExpired = $this->validateOtpExpired($params["otp_number"], $customerAceOtpTemp);
        if (!$validateOtpExpired) {
            $this->incrementValidateCount($customerAceOtpTemp);
            return false;
        }

        $checkUniquePrimaryData = $this->checkUniquePrimaryData("phone", $params["customer_id"], $params["phone"]);
        if (!$checkUniquePrimaryData) return false;

        $customerAceDetail = $this->getCustomerAceDetail("customer_id = " . $params['customer_id']);
        if (!$customerAceDetail) return false;

        try {
            $changePhoneAce = $this->changeCustomerEmailPhoneValidate(
                $params['customer_id'],                // CustomerId
                $customerAceDetail["ace_customer_id"], // Ace Customer Id
                "phone",                               // Phone or Email
                $formattedPhone,                       // New Data
                $params['otp_number'],                 // OTP Number
                "1"                                    // IsDataVerified Flag
            );
            if (!$changePhoneAce) return false;
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Failed to insert/update customer ace otp temp";
            LogHelper::log("validate_otp_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }
        $transaction->commit();

        return true;
    }

    public function validateOtpChangeNewEmail($params)
    {
        $requiredKey = ["customer_id", "email", "otp_number"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        if (!filter_var($params["email"], FILTER_VALIDATE_EMAIL)) {
            $this->errorCode = 400;
            $this->errorMessages = "E-mail format tidak valid";
            return false;
        }

        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $manager = new TxManager();
        $manager->setDbService($customerAceOtpTempModel->getConnection());
        $transaction = $manager->get();

        $customerAceOtpTemp = $customerAceOtpTempModel->findFirst("type = 'change_email' AND customer_id ='" . $params['customer_id'] . "' AND access_code ='" . $params['otp_number'] . "'");
        if (!$customerAceOtpTemp) {
            $this->errorCode = 400;
            $this->errorMessages = "Nomor OTP Salah";
            return false;
        }

        $validateOtpExpired = $this->validateOtpExpired($params["otp_number"], $customerAceOtpTemp);
        if (!$validateOtpExpired) {
            $this->incrementValidateCount($customerAceOtpTemp);
            return false;
        }

        $checkUniquePrimaryData = $this->checkUniquePrimaryData("email", $params["customer_id"], $params["email"]);
        if (!$checkUniquePrimaryData) return false;

        $customerAceDetail = $this->getCustomerAceDetail("customer_id = " . $params['customer_id']);
        if (!$customerAceDetail) return false;

        try {
            $changeEmailAce = $this->changeCustomerEmailPhoneValidate(
                $params['customer_id'],                // CustomerId
                $customerAceDetail["ace_customer_id"], // Ace Customer Id
                "email",                               // Phone or Email
                $params['email'],                      // New Data
                $params['otp_number'],                 // OTP Number
                "1"                                    // IsDataVerified Flag
            );
            if (!$changeEmailAce) return false;
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal insert/update customer ACE OTP temp";
            LogHelper::log("validate_otp_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }
        $transaction->commit();

        return true;
    }

    public function validateGenericOtp(string $customerAceOtpType, array $params)
    {
        try {
            $requiredKey = ["customer_id", "action", "otp_number"];
            $validate = $this->validateRequired($params, $requiredKey);
            if (
                $validate &&
                (!array_key_exists('email', $params) || !array_key_exists('phone', $params))
            ) {
                $this->errorCode = 400;
                $this->errorMessages = $validate
                    ? $validate
                    : "Parameter email/phone wajib ada!";
                return false;
            }

            $typeToUpdate = $customerAceOtpType;
            if ($customerAceOtpType === 'change_email') $typeToUpdate = 'valid_change_email';
            else if ($customerAceOtpType === 'change_phone') $typeToUpdate = 'valid_change_phone';

            $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();

            $customerAceOtpTemp = $customerAceOtpTempModel->findFirst(
                "access_code = '" . $params["otp_number"] . "' 
                AND type = '$customerAceOtpType' AND customer_id ='" . $params["customer_id"] . "'"
            );
            if (!$customerAceOtpTemp) {
                $this->errorCode = 400;
                $this->errorMessages = "Nomor OTP Salah";
                LogHelper::log("validate_otp_ace", "ACTION: " . $params['action'] . " ,MESSAGE: " . $this->errorMessages);
                return false;
            }

            $validateOtpExpired = $this->validateOtpExpired($params["otp_number"], $customerAceOtpTemp);
            if (!$validateOtpExpired) {
                $this->incrementValidateCount($customerAceOtpTemp);
                return false;
            }

            $validateCount = $customerAceOtpTemp->getValidateCount() + 1;

            $sql = "UPDATE customer_ace_otp_temp 
            SET validate_count = '" . $validateCount . "', 
            type = '$typeToUpdate' 
            WHERE customer_ace_otp_temp_id = '" . $customerAceOtpTemp->getCustomerAceOtpTempId() . "'";

            LogHelper::log("debug", "MESSAGE: " . $sql);

            $customerAceOtpTemp->useWriteConnection();
            $customerAceOtpTemp->getDi()->getShared($customerAceOtpTemp->getConnection())->query($sql);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = "Error saat melakukan update Customer Ace OTP";
            LogHelper::log("validate_otp_ace", "MESSAGE: " . $this->errorMessages . " ,CUSTOMER ID: " . $params["customer_id"]);
            return false;
        }

        return true;
    }

    public function validateOtpVerifyEmail(array $params)
    {
        $requiredKey = ["customer_id", "email"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();

        $customerAceOtpTemp = $customerAceOtpTempModel->findFirst("customer_id = '" . $params["customer_id"] . "' AND email = '" . $params["email"] . "' AND type = 'verify_email'");
        if (!$customerAceOtpTemp) {
            $this->errorCode = 404;
            $this->errorMessages = "Kamu harus melakukan verifikasi menggunakan email ini " . $params["email"] . " terlebih dahulu";

            return false;
        }

        $validateOtpExpired = $this->validateOtpExpired($params["otp_number"], $customerAceOtpTemp);
        if (!$validateOtpExpired) {
            $this->incrementValidateCount($customerAceOtpTemp);
            return false;
        }

        $customerAceDetail = $this->getCustomerAceDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerAceDetail) return false;

        $verifyEmailAce = $this->verifyEmailAce(
            $customerAceDetail["ace_customer_id"],
            "1",
            $customerAceOtpTemp->getCustomerAceOtpTempId()
        );
        if (!$verifyEmailAce) return false;

        try {
            $customer = new \Models\Customer();
            $manager = new TxManager();
            $manager->setDbService($customer->getConnection());
            $transaction = $manager->get();

            $dateNow = date("Y-m-d H:i:s");
            $sql = "UPDATE customer SET updated_at = '" . $dateNow . "' , is_email_verified = 10 WHERE customer_id = " . $params["customer_id"];

            $customer->useWriteConnection();
            $customer->getDi()->getShared($customer->getConnection())->query($sql);
            $customer->setTransaction($transaction);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Error saat mengganti customer e-mail";
            LogHelper::log("verify_customer_email", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $customerAceDetail["ace_customer_id"]);
        }
        $transaction->commit();

        return true;
    }

    public function validateOtpVerifyPhone(array $params, AceWebAPI $aceWebApiLib)
    {
        $requiredKey = ["customer_id", "phone"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        $formattedPhone = GeneralHelper::formatPhoneNumber($params["phone"]);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format nomor handphone tidak valid";
            return false;
        }

        $validateOtp = $this->validateOtpFromAceEndpoint($formattedPhone, $params, $aceWebApiLib);
        if (!$validateOtp) return false;

        $customerAceDetail = $this->getCustomerAceDetail("customer_id = '" . $params["customer_id"] . "'");
        if (!$customerAceDetail) return false;

        $verifyPhone = $this->verifyPhoneAce($customerAceDetail["ace_customer_id"], "1");
        if (!$verifyPhone) return false;

        try {
            $customer = new \Models\Customer();
            $manager = new TxManager();
            $manager->setDbService($customer->getConnection());
            $transaction = $manager->get();

            $dateNow = date("Y-m-d H:i:s");
            $sql = "UPDATE customer SET updated_at = '" . $dateNow . "' , is_phone_verified = 10 WHERE customer_id = " . $params["customer_id"];

            $customer->useWriteConnection();
            $customer->getDi()->getShared($customer->getConnection())->query($sql);
            $customer->setTransaction($transaction);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Error saat mengganti customer e-mail";
            LogHelper::log("verify_customer_phone", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $customerAceDetail["ace_customer_id"]);
        }
        $transaction->commit();

        return true;
    }

    public function validateOtpFromAceEndpoint(string $formattedPhone, array $params, $aceWebApiLib)
    {
        $mappedAction = $aceWebApiLib->getAceAction($params["action"]);
        if ($mappedAction == "") {
            $this->errorCode = 400;
            $this->errorMessages = "Action tidak ditemukan";
            return false;
        }

        $validateOtpResponse = $aceWebApiLib->validateOtp($formattedPhone, $params["otp_number"], $mappedAction);
        if (!$validateOtpResponse) {
            $this->errorCode = $aceWebApiLib->getErrorCode();
            $this->errorMessages = $aceWebApiLib->getErrorMessages() == "HP tidak ditemukan"
                ? "OTP yang dimasukkan salah"
                : $aceWebApiLib->getErrorMessages();
            LogHelper::log("validate_otp_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return true;
    }

    public function verifyPhoneAce($aceCustomerId, $isPhoneVerified)
    {
        $aceWebApiLib = new \Library\AceWebAPI();
        $requestPayload["P_Cust_id"] = $aceCustomerId;
        $requestPayload["P_Is_phone_verified"] = $isPhoneVerified;
        $requestPayload["P_created_on"] = date("Y-m-d H:i:s");

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log("verify_phone_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);
            return false;
        }

        $verifyPhone = $aceWebApiLib->aceVerifyPhone($encryptedPayload);
        if (!$verifyPhone) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log("verify_phone_ace", "MESSAGE: " . $this->errorMessages . " , VERIFY PHONE | ACE CUSTOMER ID: " . $aceCustomerId);
            return false;
        }

        return true;
    }

    public function verifyEmailAce($aceCustomerId, $isEmailVerified, $customerAceOtpTempId)
    {
        $aceWebApiLib = new \Library\AceWebAPI();
        $requestPayload["P_Cust_id"] = $aceCustomerId;
        $requestPayload["P_Is_verified"] = $isEmailVerified;
        $requestPayload["P_created_on"] = date("Y-m-d H:i:s");

        $customerAceOtpTempModel = new CustomerAceOtpTemp();

        try {
            $manager = new TxManager();
            $manager->setDbService($customerAceOtpTempModel->getConnection());
            $transaction = $manager->get();
            $sql = "DELETE FROM customer_ace_otp_temp WHERE customer_ace_otp_temp_id='" . $customerAceOtpTempId . "'";
            $customerAceOtpTempModel->useWriteConnection();
            $customerAceOtpTempModel->getDi()->getShared($customerAceOtpTempModel->getConnection())->query($sql);

            $customerAceOtpTempModel->setTransaction($transaction);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = "Error saat menghapus Customer Ace OTP";
            LogHelper::log("change_customer_email", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log("verify_email_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);
            return false;
        }

        $verifyEmail = $aceWebApiLib->aceVerifyEmail($encryptedPayload);
        if (!$verifyEmail) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log("verify_email_ace", "MESSAGE: " . $this->errorMessages . " , VERIFY EMAIL | ACE CUSTOMER ID: " . $aceCustomerId);
            return false;
        }

        $transaction->commit();

        return true;
    }

    public function getCustomerDataAce($data = [])
    {
        $requestPayload["P_Cust_Id"] = $data["ace_customer_id"];

        LogHelper::log(
            "get_customer_data_ace_payload",
            "REQUEST PAYLOAD: " . json_encode($requestPayload),
            "info"
        );

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log(
                "get_customer_data_ace_payload",
                "ENCRYPT ERROR MESSAGE: " . $this->errorMessages . " , Request Payload: " . json_encode($requestPayload),
                "error"
            );
            return false;
        }

        $customer = $aceWebAPI->aceGetCustomerData($encryptedPayload, "token " . $data['customer_token']);
        if (!$customer) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log(
                "get_customer_data_ace_payload",
                "REQUEST API ERROR: " . $this->errorMessages . "REQUEST PAYLOAD: " . json_encode($requestPayload),
                "error"
            );
            return false;
        }

        LogHelper::log(
            "get_customer_data_ace_response",
            "Request Payload: " . json_encode($requestPayload) . " Response: " . json_encode($customer),
            "info"
        );

        if (empty($customer)) {
            $this->errorCode = 404;
            $this->errorMessages = ["Customer tidak ditemukan"];

            LogHelper::log(
                "get_customer_data_ace_response",
                "ERROR MESSAGE: " . $this->errorMessages . " , Request Payload: " . json_encode($customer),
                "error"
            );
            return false;
        }

        $requestPayload["P_Cust_ID"] = $data["ace_customer_id"];

        LogHelper::log(
            "get_customer_data_ace_payload_verification",
            "Request Payload: " . json_encode($requestPayload),
            "info"
        );

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log(
                "get_customer_data_ace_payload_verification",
                "ENCRYPT ERROR MESSAGE: " . $this->errorMessages . "Request Payload: " . json_encode($requestPayload),
                "error"
            );
            return false;
        }

        $aceCheckCustomerVerification = $aceWebAPI->aceCheckCustomerVerification($encryptedPayload);
        if (!$aceCheckCustomerVerification) {
            if ($aceWebAPI->getErrorMessages() != "Email dan nomor handphone belum diverifikasi") {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();

                LogHelper::log(
                    "get_customer_data_ace_payload_verification",
                    "MESSAGE: " . $this->errorMessages . " , GET PRIMARY DATA VERIFY | ACE CUSTOMER ID: " . $data["ace_customer_id"],
                    "error"
                );
                return false;
            }

            $aceCheckCustomerVerification[0]['HP_VALID'] = "0";
            $aceCheckCustomerVerification[0]['MAIL_VALID'] = "0";
        }

        LogHelper::log(
            "get_customer_data_ace_response_verification",
            "Request Payload: " . json_encode($requestPayload) . " Response: " . json_encode($aceCheckCustomerVerification),
            "info"
        );

        $customer[0]['Cust_Mail'] = strtolower($customer[0]['Cust_Mail']);
        $customer[0]['HP_VALID'] = $aceCheckCustomerVerification[0]['HP_VALID'];
        $customer[0]['MAIL_VALID'] = $aceCheckCustomerVerification[0]['MAIL_VALID'];

        return $customer;
    }

    public function getCustomerAceDetail($condition)
    {
        try {
            $customerAceDetail = new \Models\CustomerAceDetails();
            $param['conditions'] = $condition;
            $result = $customerAceDetail->findFirst($param);

            if ($result) {
                LogHelper::log("get_customer_ace_detail", "SUCCESS MESSAGE: " . json_encode($result->toArray()));
                return $result->toArray();
            }

            $this->errorCode = 404;
            $this->errorMessages = "Customer ACE Detail tidak ditemukan";
        } catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = "Error saat mendapatkan customer ACE detail";
        }

        LogHelper::log("get_customer_ace_detail", "MESSAGE: " . $this->errorMessages . " ,condition: " . $condition);
        return false;
    }

    public function getCustomerDetail($condition = "")
    {
        try {
            $customer = new \Models\Customer();
            $param['conditions'] = $condition == "" ? $condition : "(" . $condition . " AND company_code = 'AHI')";
            $result = $customer->findFirst($param);
        } catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage() ? $e->getMessage() : "Error saat mendapatkan customer detail";
        }

        if ($result) {
            LogHelper::log("get_customer_detail", "SUCCESS MESSAGE: " . json_encode($result->toArray()));
            $this->errorCode = null;
            return $result->toArray();
        }

        $this->errorCode = 404;
        $this->errorMessages = "Customer detail tidak ditemukan";
        LogHelper::log("get_customer_detail", "MESSAGE: " . $this->errorMessages . " ,condition: " . $condition);
        return false;
    }

    public function mappingCustomerDataAce(array $customer)
    {
        $maritalStatus = "";
        if (array_key_exists("Cust_Stat_marital", $customer)) {
            switch ($customer["Cust_Stat_marital"]) {
                case "1":
                    $maritalStatus = "Menikah";
                    break;
                case "2":
                    $maritalStatus = "Belum Menikah";
                    break;
                case "3":
                    $maritalStatus = "Lainnya";
                    break;
            }
        }

        $sex = "";
        if (array_key_exists("Cust_Sex", $customer)) {
            switch ($customer["Cust_Sex"]) {
                case "False":
                    $sex = "Pria";
                    break;
                case "True":
                    $sex = "Wanita";
                    break;
            }
        }

        //Convert point exp date to 'd month y' format
        $pointExpDate = $this->getStringValFromArr($customer, "point_exp_date");
        $pointExpDate = str_replace('/', '-', $pointExpDate);
        $newPointExpDate = date("d M Y", strtotime($pointExpDate));

        $customerMap = [];
        $customerMap["member_no"] = $this->getStringValFromArr($customer, "Card_Id");
        $customerMap["customer_name"] = $this->getStringValFromArr($customer, "Cust_Name");
        $customerMap["member_exp_date"] = $this->getStringValFromArr($customer, "Exp_Date");
        $customerMap["phone"] = $this->getStringValFromArr($customer, "Cust_Hp");
        $customerMap["email"] = $this->getStringValFromArr($customer, "Cust_Mail");
        $customerMap["total_point"] = $this->getStringValFromArr($customer, "Jml_Point");
        $customerMap["total_point_expired"] = $this->getStringValFromArr($customer, "Point_exp");
        $customerMap["point_exp_date"] = $newPointExpDate;
        $customerMap["sex"] = $sex;
        $customerMap["birth_date"] = $this->getStringValFromArr($customer, "Cust_Birth_date");
        $customerMap["birth_place"] = $this->getStringValFromArr($customer, "Cust_Birth_place");
        $customerMap["id_card"] = $this->getStringValFromArr($customer, "cust_idCard");
        //$customerMap['is_phone_verified'] = $this->getStringValFromArr($customer, "HP_VALID");
        //$customerMap['is_email_verified'] = $this->getStringValFromArr($customer, "MAIL_VALID");
        $customerMap['is_phone_verified'] = ($this->getStringValFromArr($customer, "HP_VALID") == "1") ? "10" : $this->getStringValFromArr($customer, "HP_VALID");
        $customerMap['is_email_verified'] = ($this->getStringValFromArr($customer, "MAIL_VALID") == "1") ? "10" : $this->getStringValFromArr($customer, "MAIL_VALID");
        $customerMap["marital_status"] = $maritalStatus;
        $customerMap['is_allow_upgrade_member'] = $this->getStringValFromArr($customer, "Is_Allow_Upgrade_Member");

        return $customerMap;
    }

    public function validateRequired(array $data, array $fields, $checkValueEmpty = true)
    {
        $response = [];

        foreach ($fields as $field) {
            if (!array_key_exists($field, $data) || ($checkValueEmpty && empty($data[$field]))) {
                $field = str_replace("_", " ", $field);
                $message = ucwords($field) . " Diperlukan";
                $response[] = $message;
            }
        }

        return count($response) > 0 ? $response : false;
    }

    public function changePassKey($customerId, $oldPass, $newPass, $token, $aceCustomerId)
    {
        $customerDetail = $this->getCustomerDetail("customer_id = " . $customerId);
        if ((!$customerDetail) || $customerDetail["status"] < 10) {
            $this->errorCode = 400;
            $this->errorMessages = "Customer not found";

            LogHelper::log("change_pass_key", "MESSAGE: " . $this->errorMessages . " ,CUSTOMER ID: " . $customerId);
            return false;
        }

        $newPassTmp = $newPass;
        $newPass = hash('sha256', $newPass);
        $dateNow = date("Y-m-d H:i:s");

        try {
            $customer = new \Models\Customer();
            $manager = new TxManager();
            $manager->setDbService($customer->getConnection());
            $transaction = $manager->get();

            $sql = "UPDATE customer SET password = '" . $newPass . "', updated_at = '" . $dateNow . "' WHERE customer_id = " . $customerId;
            $customer->useWriteConnection();
            $customer->getDi()->getShared($customer->getConnection())->query($sql);

            $customer->setTransaction($transaction);

            $changePassKeyAce = $this->changePasskeyAce($aceCustomerId, $oldPass, $newPassTmp, $token);
            if (!$changePassKeyAce) {
                $transaction->rollback($this->errorMessages);
                return false;
            }

            $transaction->commit();
            return true;
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage();
            LogHelper::log("change_pass_key", "MESSAGE: " . $this->errorMessages . " ,CUSTOMER ID: " . $customerId);
        }

        return false;
    }

    public function getPointHistoryAce($buCode, $page, $size, $dateFrom, $dateTo, $token, $aceCustomerId)
    {
        $requestPayload["P_BU_Code"] = $buCode;
        $requestPayload["P_Cust_Id"] = $aceCustomerId;
        $requestPayload["P_Page_No"] = $page;
        $requestPayload["P_Page_Size"] = $size;
        $requestPayload["P_Date_From"] = $dateFrom;
        $requestPayload["P_Date_To"] = $dateTo;

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("get_point_history_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        $pointHistoryAce = $aceWebAPI->getPointHistoryAce($encryptedPayload, "token " . $token);
        if (!$pointHistoryAce) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("get_point_history_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        LogHelper::log("get_point_history_ace_response", "GET POINT HISTORY ACE | Request Payload: " . json_encode($requestPayload) . " Response: " . json_encode($pointHistoryAce));

        return $pointHistoryAce;
    }

    public function getTransactionHistoryAce($buCode, $page, $size, $dateFrom, $dateTo, $transactionType, $token, $aceCustomerId)
    {
        $requestPayload["P_BU_Code"] = $buCode;
        $requestPayload["P_Cust_Id"] = $aceCustomerId;
        $requestPayload["P_Page_No"] = $page;
        $requestPayload["P_Page_Size"] = $size;
        $requestPayload["P_Date_From"] = $dateFrom;
        $requestPayload["P_Date_To"] = $dateTo;
        $requestPayload["P_Trans_Type"] = $transactionType;

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("get_transaction_history_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        $transactionHistoryAce = $aceWebAPI->getTransactionHistoryAce($encryptedPayload, "token " . $token);
        if (!$transactionHistoryAce) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("get_transaction_history_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        LogHelper::log("get_transaction_history_ace_response", "GET TRANSACTION HISTORY ACE | Request Payload: " . json_encode($requestPayload) . " Response: " . json_encode($transactionHistoryAce));
        return $transactionHistoryAce;
    }

    public function getOnlineTransactionHistoryAce($page, $size, $dateFrom, $dateTo, $customerId)
    {
        $salesOrderResult = [];
        $salesOrderCount = 0;
        $salesOrderLibrary = new \Library\SalesOrder();
        try {
            $customerDetail = $this->getCustomerDetail("customer_id = '" . $customerId . "'");
            if (!$customerDetail) {
                $this->errorCode = 400;
                $this->errorMessages = "Customer: $customerId , bukan customer ACE!";
                LogHelper::log("get_online_transaction_history_ace", "MESSAGE: " . $this->errorMessages . " ,CUSTOMER ID: " . $customerId);

                return false;
            }

            $limit = (int)$size;
            $offset = (int)($page - 1) * $limit;
            $salesOrderQuery = $salesOrderLibrary->getSalesOrderSummary(
                array(
                    "customer_id" => $customerId,
                    "date_from" => date('Y-m-d', strtotime($dateFrom)),
                    "date_to" => date('Y-m-d', strtotime($dateTo)),
                    "limit" => $limit,
                    "offset" => $offset,
                    "exclude_order" => "ODIS,ODIT,ODIK"
                )
            );
            if (isset($salesOrderQuery) || count($salesOrderQuery['list']) > 0) {
                foreach ($salesOrderQuery['list'] as $order) {
                    array_push($salesOrderResult, array(
                        'created_at' => $order['created_at'],
                        'grand_total' => $order['grand_total'],
                        'order_no' => $order['order_no'],
                        'status' => $order['status'],
                    ));
                }
                $salesOrderCount = $salesOrderQuery['count'];
            }
        } catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage();
            LogHelper::log("get_online_transaction_history_ace", "EXCEPTION MESSAGE: " . $e->getMessage() . " ,CUSTOMER ID: " . $customerId);
        }

        $dataToReturn['data'] = $salesOrderResult;
        $dataToReturn['count'] = $salesOrderCount;

        return $dataToReturn;
    }

    public function checkEmailAvailableAce($email, $companyType)
    {
        $requestPayload["P_Cust_Mail"] = $email;
        $requestPayload["P_Company_Type"] = $companyType;

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("check_email_available_ace", "MESSAGE: " . $this->errorMessages . " ,EMAIL: " . $email);

            return false;
        }

        $isEmailAvailableAce = $aceWebAPI->aceCheckRegisteredData($encryptedPayload, "email");
        if (!$isEmailAvailableAce) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("check_email_available_ace", "MESSAGE: " . $this->errorMessages . " ,EMAIL: " . $email);

            return false;
        }

        LogHelper::log("check_email_available_ace_response", "ACE CHECK REGISTERED DATA | Request Payload: " . json_encode($requestPayload) . " .Type: Email, Response: " . json_encode($isEmailAvailableAce));

        return $isEmailAvailableAce;
    }

    public function checkPhoneAvailableAce($phone, $companyType)
    {
        $requestPayload["P_Cust_Hp"] = $phone;
        $requestPayload["P_Company_Type"] = $companyType;

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("check_phone_available_ace", "MESSAGE: " . $this->errorMessages . " ,PHONE: " . $phone);

            return false;
        }

        $isPhoneAvailableAce = $aceWebAPI->aceCheckRegisteredData($encryptedPayload, "phone");
        if (!$isPhoneAvailableAce) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("check_phone_available_ace", "MESSAGE: " . $this->errorMessages . " ,PHONE: " . $phone);

            return false;
        }
        LogHelper::log("check_phone_available_ace_response", "ACE CHECK REGISTERED DATA | Request Payload: " . json_encode($requestPayload) . " .Type: Phone, Response: " . json_encode($isPhoneAvailableAce));

        return $isPhoneAvailableAce;
    }

    public function checkUniquePrimaryData($dataChange, $customerId, $newData)
    {
        if ($dataChange != "phone" && $dataChange != "email") {
            $this->errorCode = 400;
            $this->errorMessages = "Data change harus berupa nomor handphone atau email";

            return false;
        }

        $customerDetail = $this->getCustomerDetail($dataChange . " = '" . $newData . "' AND customer_id != " . $customerId);
        if ($customerDetail) {
            $this->errorCode = 400;
            $this->errorMessages = "Data " . $dataChange . " sudah ada";
            LogHelper::log("check_unique_primary_data", "Data Change: " . $dataChange . " ,Cust Id: " . $customerId . " ,New Data: " . $newData);

            return false;
        }

        $this->errorCode = null;
        $this->errorMessages = null;
        return true;
    }

    public function checkCustomerWithUpdateAceMemberSKU($customerId)
    {
        $salesOrderModel = new \Models\SalesOrder();

        $query = "SELECT \Models\SalesOrderItem.sku FROM \Models\SalesOrder
                    INNER JOIN \Models\SalesOrderItem
                WHERE \Models\SalesOrder.customer_id = $customerId 
                AND \Models\SalesOrderItem.sku = '" . getenv("MEMBERSHIP_AHI_SKU") . "'";

        $manager = $salesOrderModel->getModelsManager();
        $salesOrder = $manager->executeQuery($query);
        if (sizeof($salesOrder) === 0) {
            return true;
        }

        return false;
    }

    public function createCustomerEmail($customerId, $aceCustomerId, $email)
    {
        $customerDetail = $this->getCustomerDetail("customer_id = '" . $customerId . "'");
        if ((!$customerDetail) || $customerDetail["status"] < 10) {
            $this->errorCode = 400;
            $this->errorMessages = "Customer tidak ditemukan";
            LogHelper::log("create_customer_email", "MESSAGE: " . $this->errorMessages . " ,CUSTOMER ID: " . $customerId);

            return false;
        }

        $dateNow = date("Y-m-d H:i:s");

        try {
            $customer = new \Models\Customer();
            $manager = new TxManager();
            $manager->setDbService($customer->getConnection());
            $transaction = $manager->get();

            $sql = "UPDATE customer SET email = '" . $email . "', updated_at = '" . $dateNow . "' WHERE customer_id = " . $customerId;
            $customer->useWriteConnection();
            $customer->getDi()->getShared($customer->getConnection())->query($sql);
            $customer->setTransaction($transaction);

            $changeEmailAce = $this->changeEmailAce($aceCustomerId, $email, "0");
            if (!$changeEmailAce) {
                $transaction->rollback($this->getErrorMessages());
                return false;
            }

            $transaction->commit();
            return true;
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Error saat membuat customer e-mail";
            LogHelper::log("create_customer_email", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);
        }

        return false;
    }

    public function changeCustomerEmail($customerId, $aceCustomerId, $email, $oldEmail, $otpNumber, $isEmailVerified)
    {
        $customerDetail = $this->getCustomerDetail("customer_id = '" . $customerId . "'");
        if ((!$customerDetail) || $customerDetail["status"] < 10) {
            $this->errorCode = 400;
            $this->errorMessages = "Customer tidak ditemukan";
            LogHelper::log("change_customer_email", "MESSAGE: " . $this->errorMessages . " ,CUSTOMER ID: " . $customerId);

            return false;
        }

        $dateNow = date("Y-m-d H:i:s");

        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $customerAceOtpTemp = $customerAceOtpTempModel->findFirst("email = '" . $oldEmail . "' AND type = 'change_email' AND customer_id = '" . $customerId . "'");
        if (!$customerAceOtpTemp) {
            $this->errorCode = 400;
            $this->errorMessages = "Error saat mendapatkan Customer Ace OTP";
            LogHelper::log("change_customer_email", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        if ($customerAceOtpTemp->getAccessCode() != $otpNumber) {
            $requestCount = $customerAceOtpTemp->getRequestCount() + 1;
            $this->errorCode = 400;
            $this->errorMessages = "Nomor OTP Salah";
            LogHelper::log("change_customer_email", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            $sql = "UPDATE customer_ace_otp_temp SET request_count = '" . $requestCount . "' WHERE customer_ace_otp_temp_id='" . $customerAceOtpTemp->getCustomerAceOtpTempId() . "'";
            $customerAceOtpTemp->useWriteConnection();
            $customerAceOtpTemp->getDi()->getShared($customerAceOtpTemp->getConnection())->query($sql);

            return false;
        }

        try {
            $managerCustAceOtpTemp = new TxManager();
            $managerCustAceOtpTemp->setDbService($customerAceOtpTempModel->getConnection());
            $transactionCustAceOtpTemp = $managerCustAceOtpTemp->get();
            $sql = "DELETE FROM customer_ace_otp_temp WHERE customer_ace_otp_temp_id='" . $customerAceOtpTemp->getCustomerAceOtpTempId() . "'";
            $customerAceOtpTemp->useWriteConnection();
            $customerAceOtpTemp->getDi()->getShared($customerAceOtpTemp->getConnection())->query($sql);

            $customerAceOtpTemp->setTransaction($transactionCustAceOtpTemp);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = "Error saat menghapus Customer Ace OTP";
            LogHelper::log("change_customer_email", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        try {
            $customer = new \Models\Customer();
            $manager = new TxManager();
            $manager->setDbService($customer->getConnection());
            $transaction = $manager->get();

            $sql = "UPDATE customer SET email = '" . $email . "', updated_at = '" . $dateNow . "' WHERE customer_id = " . $customerId;
            $customer->useWriteConnection();
            $customer->getDi()->getShared($customer->getConnection())->query($sql);

            $customer->setTransaction($transaction);

            $changeEmailAce = $this->changeEmailAce($aceCustomerId, $email, $isEmailVerified);
            if (!$changeEmailAce) {
                $transactionCustAceOtpTemp->rollback($this->getErrorMessages());
                $transaction->rollback($this->getErrorMessages());
                return false;
            }

            $transactionCustAceOtpTemp->commit();
            $transaction->commit();
            return true;
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Error saat mengganti customer e-mail";
            LogHelper::log("change_customer_email", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);
        }

        return false;
    }

    public function changeCustomerEmailPhoneValidate($customerId, $aceCustomerId, $type,  $data, $otpNumber, $isDataVerified)
    {
        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $customerModel = new \Models\Customer();
        $manager = new TxManager();
        $manager->setDbService($customerAceOtpTempModel->getConnection());
        $manager->setDbService($customerModel->getConnection());
        $transaction = $manager->get();
        try {
            $dateNow = date("Y-m-d H:i:s");

            $customerDetail = $this->getCustomerDetail("customer_id = '" . $customerId . "'");
            if ((!$customerDetail) || $customerDetail["status"] < 10) {
                $this->errorCode = 400;
                $this->errorMessages = "Customer tidak ditemukan";
                LogHelper::log("change_customer_" . $type, "MESSAGE: " . $this->errorMessages . " ,CUSTOMER ID: " . $customerId);

                return false;
            }

            // Update Customer
            if ($type == "email") {
                $changeEmailAce = $this->changeEmailAce($aceCustomerId, $data, $isDataVerified);
                if (!$changeEmailAce) return false;

                $sql = "UPDATE customer SET email = '" . $data . "' , 
                    updated_at = '" . $dateNow . "' , is_email_verified = 10 
                    WHERE customer_id = " . $customerId;
            } else {
                $changePhoneAce = $this->changePhoneAce($aceCustomerId, $data, $isDataVerified);
                if (!$changePhoneAce) return false;

                $normalPhoneNumberFormat = GeneralHelper::formatNormalPhoneNumber($data);
                if (!$normalPhoneNumberFormat) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Format nomor handphone tidak valid";

                    $transaction->rollback($this->getErrorMessages());

                    return false;
                }
                $sql = "UPDATE customer SET phone = '" . $normalPhoneNumberFormat . "' , 
                    updated_at = '" . $dateNow . "' , is_phone_verified = 10 
                    WHERE customer_id = " . $customerId;
            }
            $customerModel->useWriteConnection();
            $customerModel->getDi()->getShared($customerModel->getConnection())->query($sql);
            $customerModel->setTransaction($transaction);
            // End of Update Customer

            // Delete Customer Ace OTP Temp
            $sql = "DELETE FROM customer_ace_otp_temp WHERE customer_id='" . $customerId . "' AND access_code='" . $otpNumber . "'";

            $customerAceOtpTempModel->useWriteConnection();
            $customerAceOtpTempModel->getDi()->getShared($customerAceOtpTempModel->getConnection())->query($sql);
            $customerAceOtpTempModel->setTransaction($transaction);
            // End of Delete Customer Ace OTP Temp

            $transaction->commit();

            return true;
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage()
                ? $exception->getMessage()
                : "changeCustomerEmailPhoneValidateNew Exception Occurs!";
            LogHelper::log("change_customer_" . $type, "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            $transaction->rollback($this->getErrorMessages());

            return false;
        }

        return false;
    }

    public function changeCustomerPhone($customerId, $aceCustomerId, $phone, $oldPhone, $otpNumber, $isPhoneVerified)
    {
        $customerDetail = $this->getCustomerDetail("customer_id = '" . $customerId . "'");
        if ((!$customerDetail) || $customerDetail["status"] < 10) {
            $this->errorCode = 400;
            $this->errorMessages = "Customer tidak ditemukan";
            LogHelper::log("change_customer_phone", "MESSAGE: " . $this->errorMessages . " ,CUSTOMER ID: " . $customerId);

            return false;
        }

        $dateNow = date("Y-m-d H:i:s");

        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $customerAceOtpTemp = $customerAceOtpTempModel->findFirst("phone = '" . $oldPhone . "' AND type = 'change_phone' AND customer_id = '" . $customerId . "'");
        if (!$customerAceOtpTemp) {
            $this->errorCode = 400;
            $this->errorMessages = "Error saat mendapatkan Customer ACE OTP";
            LogHelper::log("change_customer_phone", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        if ($customerAceOtpTemp->getAccessCode() != $otpNumber) {
            $requestCount = $customerAceOtpTemp->getRequestCount() + 1;
            $this->errorCode = 400;
            $this->errorMessages = "Nomor OTP Salah";
            LogHelper::log("change_customer_phone", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            $sql = "UPDATE customer_ace_otp_temp SET request_count = '" . $requestCount . "' WHERE customer_ace_otp_temp_id='" . $customerAceOtpTemp->getCustomerAceOtpTempId() . "'";
            $customerAceOtpTemp->useWriteConnection();
            $customerAceOtpTemp->getDi()->getShared($customerAceOtpTemp->getConnection())->query($sql);

            return false;
        }

        try {
            $managerCustAceOtpTemp = new TxManager();
            $managerCustAceOtpTemp->setDbService($customerAceOtpTempModel->getConnection());
            $transactionCustAceOtpTemp = $managerCustAceOtpTemp->get();
            $sql = "DELETE FROM customer_ace_otp_temp WHERE customer_ace_otp_temp_id='" . $customerAceOtpTemp->getCustomerAceOtpTempId() . "'";
            $customerAceOtpTemp->useWriteConnection();
            $customerAceOtpTemp->getDi()->getShared($customerAceOtpTemp->getConnection())->query($sql);

            $customerAceOtpTemp->setTransaction($transactionCustAceOtpTemp);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = "Error saat menghapus Customer ACE OTP";
            LogHelper::log("change_customer_phone", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        try {
            $customer = new \Models\Customer();
            $manager = new TxManager();
            $manager->setDbService($customer->getConnection());
            $transaction = $manager->get();

            $sql = "UPDATE customer SET phone = '" . $phone . "', updated_at = '" . $dateNow . "' WHERE customer_id = " . $customerId;
            $customer->useWriteConnection();
            $customer->getDi()->getShared($customer->getConnection())->query($sql);

            $customer->setTransaction($transaction);

            $changePhoneAce = $this->changePhoneAce($aceCustomerId, $phone, $isPhoneVerified);
            if (!$changePhoneAce) {
                $transactionCustAceOtpTemp->rollback($this->getErrorMessages());
                $transaction->rollback($this->getErrorMessages());
                return false;
            }

            $transactionCustAceOtpTemp->commit();
            $transaction->commit();
            return true;
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Error saat mengganti customer phone";
            LogHelper::log("change_customer_phone", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);
        }

        return false;
    }

    public function changeEmailAce($aceCustomerId, $email, $isEmailVerified)
    {
        $requestPayload["P_Cust_id"] = $aceCustomerId;
        $requestPayload["P_Cust_Mail"] = $email;
        $requestPayload["P_Is_verified"] = $isEmailVerified;
        $requestPayload["P_created_on"] = date("Y-m-d H:i:s");

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("change_email_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        $changeEmailAce = $aceWebAPI->changeEmailAce($encryptedPayload);
        if (!$changeEmailAce) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("change_email_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER_ID: " . $aceCustomerId);

            return false;
        }
        LogHelper::log("change_email_ace_response", "CHANGE EMAIL ACE | Request Payload: " . json_encode($requestPayload) . " Response: " . json_encode($changeEmailAce));

        return $changeEmailAce;
    }

    public function changePhoneAce($aceCustomerId, $phone, $isPhoneVerified)
    {
        $requestPayload["P_Cust_id"] = $aceCustomerId;
        $requestPayload["P_Cust_HP"] = $phone;
        $requestPayload["P_Is_phone_verified"] = $isPhoneVerified;
        $requestPayload["P_created_on"] = date("Y-m-d H:i:s");

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("change_phone_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);

            return false;
        }

        $changePhoneAce = $aceWebAPI->changePhoneAce($encryptedPayload);
        if (!$changePhoneAce) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("change_phone_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER_ID: " . $aceCustomerId);

            return false;
        }
        LogHelper::log("change_phone_ace_response", "CHANGE PHONE ACE | Request Payload: " . json_encode($requestPayload) . " Response: " . json_encode($changePhoneAce));

        return $changePhoneAce;
    }

    public function forgetPasskeyAce($email, $phone, $companyId = "AHI")
    {
        $requestPayload["P_Cust_Mail"] = $email != false ? $email : "";
        $requestPayload["P_Cust_Hp"] = $phone != false ? $phone : "";
        $requestPayload["P_Company"] = $companyId;

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("forget_passkey_ace", "MESSAGE: " . $this->errorMessages);

            return false;
        }

        $forgetPassKey = $aceWebAPI->forgetPasskeyAce($encryptedPayload);
        if (!$forgetPassKey) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("forget_passkey_ace", "MESSAGE: " . $this->errorMessages);

            return false;
        }

        return $forgetPassKey;
    }

    public function sendPasskeyToCustomer($email, $phone, $passkey, $cardId)
    {
        $aceWebAPI = new \Library\AceWebAPI();

        $message = "Passkey aplikasi ACE Indonesia Anda adalah $passkey. Setelah login, segera ubah dan buat kombinasi passkey baru demi keamanan.";

        if ($phone) {
            $formatPhoneNumber = GeneralHelper::formatPhoneNumber($phone);
            if (!$formatPhoneNumber) {
                $this->errorCode = 400;
                $this->errorMessages = "Format nomor handphone tidak valid";
                LogHelper::log("send_passkey_to_customer", "MESSAGE: " . $this->errorMessages);
                return false;
            }

            $sendOtpRows = $aceWebAPI->sendWhatsapp(
                $formatPhoneNumber,
                'forgot_passkey',
                array(
                    'member_no' => $cardId,
                    'passkey' => $passkey
                )
            );
            if (!$sendOtpRows) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();
                LogHelper::log("send_passkey_to_customer", "MESSAGE: " . $this->errorMessages);
                return false;
            }
        }

        if ($email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->errorCode = 400;
                $this->errorMessages = "E-mail format tidak valid";
                return false;
            }

            $requestPayload["P_Card_Id"] = $cardId;
            $requestPayload["P_Passkey"] = $passkey;

            $aceWebAPI = new \Library\AceWebAPI();
            $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
            if (!$encryptedPayload) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();
                LogHelper::log("send_passkey_to_customer", "MESSAGE: Email, " . $this->errorMessages);
                return false;
            }

            $aceLogin = $aceWebAPI->aceLoginCustomer($encryptedPayload);
            if (!$aceLogin) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = "Card ID dan Passkey tidak valid";
                LogHelper::log("send_passkey_to_customer", "MESSAGE: Email, " . $this->errorMessages . ",PAYLOAD: " . json_encode($requestPayload));
                return false;
            }

            $customerDataAce = $this->getCustomerDataAce([
                "ace_customer_id" => $aceLogin[0]["cust_id"],
                "customer_token" => $aceLogin[0]["token"]
            ]);
            if (!$customerDataAce) return false;
            $name = $this->getFirstAndLastName($customerDataAce[0]["Cust_Name"]);

            $this->sendForgotPasskeyEmail($cardId, $passkey, $email, $name["first_name"], $name["last_name"]);
        }

        return true;
    }

    public function updateCustomerAce($data, $token, $customerId)
    {
        $phone = $this->ruparupaFormatPhone($this->getStringValFromArr($data, "Cust_Hp"));
        $formattedPhone = GeneralHelper::formatPhoneNumber($phone);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format nomor handphone tidak valid";
            return false;
        }
        $requestPayload["P_Cust_Id"] = $this->getStringValFromArr($data, "Cust_Id");
        $requestPayload["P_Cust_Name"] = $this->getStringValFromArr($data, "Cust_Name");
        $requestPayload["P_Cust_Add"] = $this->getStringValFromArr($data, "Cust_Add");
        $requestPayload["P_Cust_Hp"] = $formattedPhone;
        $requestPayload["P_Cust_Mail"] = $this->getStringValFromArr($data, "Cust_Mail");
        $requestPayload["P_Cust_Birth_Date"] = $this->getStringValFromArr($data, "Cust_Birth_date");
        $requestPayload["P_Cust_Birth_Place"] = $this->getStringValFromArr($data, "Cust_Birth_place");
        $requestPayload["P_Cust_Sex"] = $this->getStringValFromArr($data, "Cust_Sex");
        $requestPayload["P_Cust_City"] = $this->getStringValFromArr($data, "Cust_City");
        $requestPayload["P_Cust_PosCode"] = $this->getStringValFromArr($data, "cust_poscode");
        $requestPayload["P_Cust_Add_Ktp"] = "";
        $requestPayload["P_Cust_Phone"] = "";
        $requestPayload["P_Cust_IdCard"] = $this->getStringValFromArr($data, "cust_idCard");
        $requestPayload["P_Cust_Religion"] = "";
        $requestPayload["P_Cust_Stat_Marital"] = $this->getStringValFromArr($data, "Cust_Stat_marital");
        $requestPayload["P_Cust_citizenship"] = "";
        $requestPayload["P_Cust_Employ"] = "";
        $requestPayload["P_Cust_Purpose"] = "";
        $requestPayload["P_Cust_AutoRen"] = $this->getStringValFromArr($data, "AutoRen");

        $customer = new \Models\Customer();
        $manager = new TxManager();
        $manager->setDbService($customer->getConnection());
        $transaction = $manager->get();

        try {
            $fullName = $requestPayload["P_Cust_Name"];
            $name = $this->getFirstAndLastName($fullName);

            $customer = $customer->findFirst("customer_id = '" . $customerId . "'");
            $customer->first_name = $name["first_name"];
            $customer->last_name = $name["last_name"];
            $customer->email = $requestPayload["P_Cust_Mail"];
            $customer->phone = $phone;
            $customer->birth_date = $requestPayload["P_Cust_Birth_Date"];
            $customer->updated_at = date("Y-m-d H:i:s");
            $customer->save();
            $customer->setTransaction($transaction);
            $aceWebAPI = new \Library\AceWebAPI();
            $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);

            if (!$encryptedPayload) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();
                LogHelper::log("update_customer_ace", "MESSAGE: " . $this->errorMessages);
                $transaction->rollback();

                return false;
            }

            $changeMemberDataAce = $aceWebAPI->changeMemberDataAce($encryptedPayload, "token " . $token);
            if (!$changeMemberDataAce) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();
                LogHelper::log("update_customer_ace", "MESSAGE: " . $this->errorMessages);
                $transaction->rollback();

                return false;
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage();
            LogHelper::log("update_customer_ace", "MESSAGE: " . $this->errorMessages . " " . $exception->getMessage());

            return false;
        }

        $transaction->commit();
        LogHelper::log("update_customer_ace", "SUCCESS MESSAGE: " . $changeMemberDataAce);
        return $changeMemberDataAce;
    }

    public function changePrimaryCustomerAddress($addressId, $customerId)
    {
        $customerAddressModel = new \Models\CustomerAddress();
        $customerAddressModel2 = new \Models\CustomerAddress();
        $customerAddress = $customerAddressModel::findFirst("address_id = '" . $addressId . "' and customer_id = '" . $customerId . "'");
        if (!$customerAddress || $customerAddress->getStatus() < 0) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Alamat tidak ditemukan";
            return false;
        }
        if ($customerAddress->getIsDefault() == 1) return true;

        $manager = new TxManager();
        $manager->setDbService($customerAddressModel->getConnection());
        $manager->setDbService($customerAddressModel2->getConnection());
        $transaction = $manager->get();

        try {
            $sql = "UPDATE customer_address SET is_default = 0 WHERE customer_id = '" . $customerId . "' AND is_default > 0";
            $customerAddressModel2->useWriteConnection();
            $customerAddressModel2->getDi()->getShared($customerAddressModel2->getConnection())->query($sql);
            $customerAddressModel2->setTransaction($transaction);

            $customerAddress->setIsDefault(1);
            $customerAddress->save();
            $customerAddressModel->setTransaction($transaction);
            $customerAddress = $customerAddress->toArray();

            $customerAceDetail = $this->getCustomerAceDetail("customer_id = " . $customerId);
            if (!$customerAceDetail) return false;

            $customer = $this->getCustomerDataAce($customerAceDetail);
            if (!$customer) return false;

            $updateData = [
                "address" => $customerAddress["full_address"],
                "post_code" => $customerAddress["post_code"],
            ];
            $updatedData = $this->updateCustomerData($customer[0], $updateData);
            $changeAddressAce = $this->updateCustomerAce($updatedData, $customerAceDetail["customer_token"], $customerId);
            if (!$changeAddressAce) return false;

            $transaction->commit();
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal melakukan update alamat utama customer ACE";
            LogHelper::log("change_primary_customer_address", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return true;
    }

    public function updateCustomerData($customer, $updateData)
    {
        if (array_key_exists("marital_status", $updateData)) {
            $maritalStatus = "";

            switch ($updateData["marital_status"]) {
                case "Menikah":
                    $maritalStatus = "1";
                    break;
                case "Belum Menikah":
                    $maritalStatus = "2";
                    break;
                case "Lainnya":
                    $maritalStatus = "3";
                    break;
            }

            $customer["Cust_Stat_marital"] = $maritalStatus;
        }

        if (array_key_exists("sex", $updateData)) {
            $sex = "";

            switch ($updateData["sex"]) {
                case "Pria":
                    $sex = "0";
                    break;
                case "Wanita":
                    $sex = "1";
                    break;
            }

            $customer["Cust_Sex"] = $sex;
        }

        if (array_key_exists("address", $updateData)) $customer["Cust_Add"] = $updateData["address"];
        // if(array_key_exists("phone", $updateData)) $customer["Cust_Hp"] = $updateData["phone"];
        // if(array_key_exists("email", $updateData)) $customer["Cust_Mail"] = $updateData["email"];
        if (array_key_exists("birth_date", $updateData)) $customer["Cust_Birth_date"] = $updateData["birth_date"];
        if (array_key_exists("birth_place", $updateData)) $customer["Cust_Birth_place"] = $updateData["birth_place"];
        if (array_key_exists("city", $updateData)) $customer["Cust_City"] = $updateData["city"];
        if (array_key_exists("post_code", $updateData)) $customer["cust_poscode"] = $updateData["post_code"];
        if (array_key_exists("id_card", $updateData)) $customer["cust_idCard"] = $updateData["id_card"];
        if (array_key_exists("auto_ren", $updateData)) $customer["AutoRen"] = $updateData["auto_ren"];

        return $customer;
    }

    public function getCustomerAddressAce($customerId, $limit = 100)
    {
        $addressList = [];

        try {
            $customerAddressModel = new \Models\CustomerAddress();

            $resultAddress = $customerAddressModel::query();

            $resultAddress->andWhere("customer_id = " . $customerId);
            $resultAddress->andWhere("status > 0");
            $resultAddress->orderBy("is_default DESC, address_id DESC");
            $resultAddress->limit($limit);

            $result = $resultAddress->execute();

            foreach ($result as $address) {
                $customerAddressModel = new \Models\CustomerAddress();
                $customerAddressModel->setFromArray($address->toArray());
                $addressList[] = $customerAddressModel->getDataArray();
            }
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal mendapatkan customer address ACE";
            LogHelper::log("get_customer_address_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return $addressList;
    }

    public function createCustomerAddressAce($params)
    {
        $customerAddressModel = new \Models\CustomerAddress();
        $customerAddressModel = $customerAddressModel->findFirst("customer_id = " . $params["customer_id"] . " AND status > 0");
        $firstCustomerAddress = false;
        if (!$customerAddressModel) $firstCustomerAddress = true;

        try {
            $customerAddressModel = new \Models\CustomerAddress();
            $manager = new TxManager();
            $manager->setDbService($customerAddressModel->getConnection());
            $transaction = $manager->get();
            $customerAddressModel->customer_id = $this->getStringValFromArr($params, "customer_id");
            $customerAddressModel->first_name = $this->getStringValFromArr($params, "first_name");
            $customerAddressModel->last_name = $this->getStringValFromArr($params, "last_name");
            $customerAddressModel->full_address = $this->getStringValFromArr($params, "full_address");
            $customerAddressModel->phone = $this->getStringValFromArr($params, "phone");
            $customerAddressModel->post_code = $this->getStringValFromArr($params, "post_code");
            $customerAddressModel->province_id = intval($params["province"]["province_id"]);
            $customerAddressModel->city_id = intval($params["city"]["city_id"]);
            $customerAddressModel->kecamatan_id = intval($params["kecamatan"]["kecamatan_id"]);
            $customerAddressModel->address_name = $this->getStringValFromArr($params, "address_name");
            $customerAddressModel->geolocation = $this->getStringValFromArr($params, "geolocation");
            $customerAddressModel->kelurahan_id = $this->getStringValFromArr($params, "kelurahan") != "" ? intval($this->getStringValFromArr($params, "kelurahan")["kelurahan_id"]) : null;
            $customerAddressModel->country_id = getenv("DEFAULT_SHIPPING_COUNTRY_ID");
            $customerAddressModel->address_type = "shipping";
            $customerAddressModel->is_default = $firstCustomerAddress ? 1 : 0;
            $customerAddressModel->status = 10;
            $customerAddressModel->save();
            $customerAddressModel->setTransaction($transaction);

            if ($firstCustomerAddress) {
                $customerAceDetail = $this->getCustomerAceDetail("customer_id = " . $params['customer_id']);
                if (!$customerAceDetail) return false;

                $customer = $this->getCustomerDataAce($customerAceDetail);
                if (!$customer) return false;

                $updateData = [
                    "address" => $params["full_address"],
                    "post_code" => $params["post_code"],
                ];
                $updatedData = $this->updateCustomerData($customer[0], $updateData);
                $changeAddressAce = $this->updateCustomerAce($updatedData, $customerAceDetail["customer_token"], $params["customer_id"]);
                if (!$changeAddressAce) return false;
            }

            $transaction->commit();
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal saat membuat alamat customer ACE";
            LogHelper::log("create_customer_address_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return true;
    }

    public function updateCustomerAddressAce($addressId, $params)
    {
        $customerAddressModel = new \Models\CustomerAddress();
        $customerAddress = $customerAddressModel::findFirst("address_id = '" . $addressId . "' and customer_id = '" . $params["customer_id"] . "'");
        if (!$customerAddress || $customerAddress->getStatus() < 0) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Alamat tidak ditemukan";
            return false;
        }
        $manager = new TxManager();
        $manager->setDbService($customerAddressModel->getConnection());
        $transaction = $manager->get();
        $customerAddress = $customerAddress->toArray();

        try {
            $customerAddressModel->address_id = $addressId;
            $customerAddressModel->customer_id = $this->getStringValFromArr($params, "customer_id");
            $customerAddressModel->first_name = $this->getStringValFromArr($params, "first_name");
            $customerAddressModel->last_name = $this->getStringValFromArr($params, "last_name");
            $customerAddressModel->full_address = $this->getStringValFromArr($params, "full_address");
            $customerAddressModel->phone = $this->getStringValFromArr($params, "phone");
            $customerAddressModel->post_code = $this->getStringValFromArr($params, "post_code");
            $customerAddressModel->province_id = intval($params["province"]["province_id"]);
            $customerAddressModel->city_id = intval($params["city"]["city_id"]);
            $customerAddressModel->kecamatan_id = intval($params["kecamatan"]["kecamatan_id"]);

            // Used kelurahan data priority
            $usedKelurahanID = null;
            if (!empty($params["kelurahan"]["kelurahan_id"])) {
                // 1. Used from params
                $usedKelurahanID = (int) $params["kelurahan"]["kelurahan_id"];
            } else if (!empty($customerAddress["kelurahan_id"])) {
                // 2. Used from customer_address
                $usedKelurahanID = (int) $customerAddress["kelurahan_id"];
            }

            // Check whether kelurahan data is still within passed kecamatan data, we must do this because older apps doesn't pass kelurahan data
            // and they may change kecamatan data without changing kelurahan data
            if (!empty($params["kecamatan"]["kecamatan_id"]) && empty($params["kelurahan"]["kelurahan_id"]) && !empty($customerAddress["kelurahan_id"])) {
                $masterKelurahanModel = new \Models\MasterKelurahan();
                $masterKelurahan = $masterKelurahanModel::findFirst(sprintf("kecamatan_id = %d and kelurahan_id = %d", (int) $params["kecamatan"]["kecamatan_id"], (int) $customerAddress["kelurahan_id"]));
                if (!$masterKelurahan) {
                    // Means customer updating from older apps and current kelurahan is not within new kecamatan
                    $usedKelurahanID = null;
                }
            }

            if (is_null($usedKelurahanID)) {
                $customerAddressModel->setForceUpdateFields(['kelurahan_id']);
            }

            $customerAddressModel->setKelurahanId($usedKelurahanID);
            $customerAddressModel->address_name = $this->getStringValFromArr($params, "address_name");
            $customerAddressModel->geolocation = $this->getStringValFromArr($params, "geolocation");
            $customerAddressModel->save();
            $customerAddressModel->setTransaction($transaction);

            if ($customerAddress["is_default"] == 1) {
                $customerAceDetail = $this->getCustomerAceDetail("customer_id = " . $params['customer_id']);
                if (!$customerAceDetail) return false;

                $customer = $this->getCustomerDataAce($customerAceDetail);
                if (!$customer) return false;

                $updateData = [
                    "address" => $params["full_address"],
                    "post_code" => $params["post_code"],
                ];
                $updatedData = $this->updateCustomerData($customer[0], $updateData);
                $changeAddressAce = $this->updateCustomerAce($updatedData, $customerAceDetail["customer_token"], $params["customer_id"]);
                if (!$changeAddressAce) return false;
            }

            $transaction->commit();
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal update alamat customer ACE";
            LogHelper::log("update_customer_address_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return true;
    }

    public function deleteCustomerAddressAce($addressId, $needDecrypt = false, $customerId = null)
    {
        $customerAddressModel = new \Models\CustomerAddress();

        if ($needDecrypt && $addressId) {
            $addressId = \Helpers\SimpleEncrypt::decrypt($addressId);
        }

        $addressResult = $customerAddressModel::findFirst("address_id = '" . $addressId . "'");
        if (!$addressResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Alamat tidak ditemukan";
            return false;
        }

        if ($customerId) {
            $checkResult = $customerAddressModel::findFirst("address_id = '" . $addressId . "' and customer_id = '" . $customerId . "'");

            if (!$checkResult) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Alamat tidak ditemukan";
                return false;
            }
        }

        $customerAddressModel->assign($addressResult->toArray());
        $customerAddressModel->setStatus(-1);;
        $saveStatus = $customerAddressModel->saveData("customer_address");;

        if (!$saveStatus) {
            $this->errorCode = $customerAddressModel->getErrorCode();
            $this->errorMessages[] = $customerAddressModel->getErrorMessages();
            return false;
        }

        return true;
    }

    public function updateWebhook(
        $customerId,
        $email,
        $phoneNumber
    ) {
        $setQuerySQL = "";
        if (
            !empty($email) &&
            !empty($phoneNumber) &&
            $email === '-' &&
            $phoneNumber === '-'
        ) {
            $setQuerySQL = "email = NULL, phone = NULL, is_phone_verified = 0, is_email_verified = 0";
        } else if (!empty($email) && !empty($phoneNumber)) {
            $setQuerySQL = "email = '" . $email . "', phone = '" . $phoneNumber . "', is_phone_verified = 0, is_email_verified = 0";
        } else if (!empty($email) && empty($phoneNumber)) {
            $setQuerySQL = "email = '" . $email . "', is_email_verified = 0";
        } else if (empty($email) && !empty($phoneNumber)) {
            $setQuerySQL = "phone = '" . $phoneNumber . "', is_phone_verified = 0";
        }

        $dateNow = date("Y-m-d H:i:s");
        try {
            $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
            $customerModel = new \Models\Customer();
            $customerAceDetailsModel = new \Models\CustomerAceDetails();

            $manager = new TxManager();
            $manager->setDbService($customerAceOtpTempModel->getConnection());
            $manager->setDbService($customerModel->getConnection());
            $manager->setDbService($customerAceDetailsModel->getConnection());
            $transaction = $manager->get();

            $sql = "UPDATE customer SET " . $setQuerySQL . ", updated_at = '" . $dateNow . "' WHERE customer_id = '" . $customerId . "'";
            $customerModel->useWriteConnection();
            $customerModel->getDi()->getShared($customerModel->getConnection())->query($sql);
            $customerModel->setTransaction($transaction);

            $sql = "UPDATE customer_ace_details SET customer_token = '', updated_at = '" . $dateNow . "' WHERE customer_id = '" . $customerId . "'";
            $customerAceDetailsModel->useWriteConnection();
            $customerAceDetailsModel->getDi()->getShared($customerAceDetailsModel->getConnection())->query($sql);
            $customerAceDetailsModel->setTransaction($transaction);

            $sql = "DELETE FROM customer_ace_otp_temp WHERE customer_id='" . $customerId . "'";
            $customerAceOtpTempModel->useWriteConnection();
            $customerAceOtpTempModel->getDi()->getShared($customerAceOtpTempModel->getConnection())->query($sql);
            $customerAceOtpTempModel->setTransaction($transaction);

            $transaction->commit();

            LogHelper::log("update_webhook", "Update Success, email: $email , phone number: $phoneNumber , customer_id: " . $customerId);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Error saat update e-mail menggunakan webhook";

            LogHelper::log("update_webhook", $this->errorMessages);

            $transaction->rollback($this->getErrorMessages());

            return false;
        }

        return true;
    }

    public function loginStore($params = array())
    {
        if (empty($params["store_code"]) || empty($params["password"])) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter tidak lengkap";
            return;
        }

        $storeModel = new \Models\Store;
        $sql = "SELECT status FROM store WHERE store_code = '" . $params["store_code"] . "' LIMIT 1";
        $dbResult = $storeModel->getDi()->getShared("dbMaster")->query($sql);
        $dbResult->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $result = $dbResult->fetchAll();
        if (empty($result[0])) {
            $this->errorCode = 400;
            $this->errorMessages = "Store tersebut belum terdaftar";
            return;
        }

        if ((int)$result[0]["status"] != 10) {
            $this->errorCode = 400;
            $this->errorMessages = "Store tidak aktif";
            return;
        }

        // Validate password, each store only have one password which stored on env
        if ($params["password"] == getenv("STORE_PASSWORD")) {
            return array(
                "store_code" => $params["store_code"]
            );
        } else {
            $this->errorCode = 400;
            $this->errorMessages = "Data store tidak valid";
            return;
        }
    }

    public function sendOtpEmail($otp, $expiredDate, $email, $firstname, $lastname, $type)
    {
        if (!($type == "change_email" || $type == "change_phone")) {
            $this->errorCode = 400;
            $this->errorMessages = "Tipe send OTP e-mail tidak valid";

            LogHelper::log("send_otp_email_ace", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $companyCode = "AHI";

        $emailHelper = new \Library\Email();
        $emailHelper->setEmailSender();
        $emailHelper->setEmailReceiver($email);
        $emailHelper->setName($lastname, $firstname);
        $emailHelper->buildAceOtpParam($otp, $expiredDate);

        $emailTag = ($type === "change_email") ? "ace_hardware_otp_email" : "ace_hardware_otp_phone";
        $templateID = \Helpers\GeneralHelper::getTemplateId($emailTag, $companyCode);

        $emailHelper->setEmailTag($emailTag);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
        return true;
    }

    public function sendForgotPasskeyEmail($memberCode, $accessCode, $email, $firstname, $lastname)
    {
        $companyCode = "AHI";
        $salesOrderModel = new \Models\SalesOrder();
        $emailHelper = new \Library\Email();
        $emailHelper->setOrderObj($salesOrderModel);
        $emailHelper->setEmailSender();
        $emailHelper->setEmailReceiver($email);
        $emailHelper->setName($lastname, $firstname);
        $emailHelper->buildAceForgotPasskeyParam($memberCode, $accessCode);
        $emailHelper->setEmailTag("forgot_passkey");

        $templateID = \Helpers\GeneralHelper::getTemplateId("forgot_passkey", $companyCode);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
    }

    public function sendVerifyEmailWithEmail($email, $firstname, $lastname, $otpNumber)
    {
        $companyCode = "AHI";
        $emailHelper = new \Library\Email();
        $emailHelper->setEmailSender();
        $emailHelper->setEmailReceiver($email);
        $emailHelper->setName($lastname, $firstname);
        $emailHelper->buildVerifyEmailParam($otpNumber);
        $emailHelper->setEmailTag("ace_verify_email");

        $templateID = \Helpers\GeneralHelper::getTemplateId("ace_verify_email", $companyCode);
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode($companyCode);

        $emailHelper->send();
    }

    public function sendVerifyEmailOtp($customerId, $email)
    {
        $generatedRandomNumber = GeneralHelper::generateRandomNumber(6);
        $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
        $manager = new TxManager();
        $manager->setDbService($customerAceOtpTempModel->getConnection());
        $transaction = $manager->get();

        $customerAceOtpTemp = $customerAceOtpTempModel->findFirst("email = '" . $email . "' AND type = 'verify_email' AND customer_id ='" . $customerId . "'");

        try {
            if (!$customerAceOtpTemp) {
                $sql = "INSERT INTO customer_ace_otp_temp (email, phone, access_code, customer_id, type) ";
                $sql .= "VALUES('" . $email . "', '', '" . $generatedRandomNumber . "', '" . $customerId . "','verify_email')";
            } else {
                $requestCount = $customerAceOtpTemp->getRequestCount() + 1;
                $sql = "UPDATE customer_ace_otp_temp SET request_count = '" . $requestCount . "', access_code = '" . $generatedRandomNumber . "' WHERE customer_ace_otp_temp_id = '" . $customerAceOtpTemp->getCustomerAceOtpTempId() . "'";
            }
            $customerAceOtpTempModel->useWriteConnection();
            $customerAceOtpTempModel->getDi()->getShared($customerAceOtpTempModel->getConnection())->query($sql);
            $customerAceOtpTempModel->setTransaction($transaction);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal melakukan insert/update customer ace OTP temp";
            LogHelper::log("send_verify_otp_email", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        $customer = $this->getCustomerDetail("customer_id = '" . $customerId . "'");
        if (!$customer) {
            return false;
        }

        $lastname = $this->getStringValFromArr($customer, "last_name");
        $this->sendVerifyEmailWithEmail($email, $customer["first_name"], $lastname, $generatedRandomNumber);

        $transaction->commit();

        return true;
    }

    public function validateOtpExpired($otp, $customerAceOtpTemp)
    {
        if ($customerAceOtpTemp->getAccessCode() != $otp) {
            $this->errorCode = 400;
            $this->errorMessages = "OTP and salah";

            return false;
        }

        if (
            $this->getDiffDateInSecond(
                $customerAceOtpTemp->getUpdatedAt(),
                date("Y-m-d H:i:s")
            ) > 300
        ) {
            $this->errorCode = 400;
            $this->errorMessages = "OTP anda telah expired, mohon melakukan verifikasi lagi";

            return false;
        }

        return true;
    }

    public function incrementValidateCount($customerAceOtpTemp)
    {
        try {
            $customerAceOtpTempModel = new \Models\CustomerAceOtpTemp();
            $validateCount = $customerAceOtpTemp->getValidateCount() + 1;
            $sql = "UPDATE customer_ace_otp_temp SET validate_count = '" . $validateCount . "' WHERE customer_ace_otp_temp_id = '" . $customerAceOtpTemp->getCustomerAceOtpTempId() . "'";
            $customerAceOtpTempModel->useWriteConnection();
            $customerAceOtpTempModel->getDi()->getShared($customerAceOtpTempModel->getConnection())->query($sql);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal melakukan increment validate count verify e-mail";
            LogHelper::log("increment_validate_count_verify_email", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        return true;
    }

    public function redeemPointAce($aceCustomerId, $aceMemberNo, $voucherCode, $pointRedeem)
    {
        $now = new \DateTime("now");
        $pointRedeemed = (int)$pointRedeem * -1;

        // Adding '-' after CP
        $voucherAsParam = substr_replace($voucherCode, '-', 2, 0);

        $aceWebAPI = new \Library\AceWebAPI();
        $req = array(
            "P_company_type" => "A",
            "P_cust_id" => $aceCustomerId,
            "P_card_id" => $aceMemberNo,
            "P_receive_no" => $voucherAsParam, // ex: CP-AF789C78
            "P_jmlkenapoint" => "0", // Default when redeem point
            "P_factor" => getenv('POINT_VOUCHER_WORTH_AHI'), // Factor is how much money converted for every point used
            "P_reward" => "0", // Because customer doesn't get reward from this transaction
            "P_jmlpoint" => (string)$pointRedeemed,
            "P_createdate" => $now->format('Y-m-d H:i:s')
        );

        $pointRedeemed = $aceWebAPI->insertPointAce($req);
        if (!$pointRedeemed) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            LogHelper::log("redeem_point_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER_ID: " . $aceCustomerId);

            return false;
        }

        return $pointRedeemed;
    }

    public function postingJournal($salesRuleVoucherModel, $companyCode = "AHI")
    {
        // Create journal redeem
        $journalLib = new \Library\JournalSAP();
        $journalLib->setVoucher($salesRuleVoucherModel);
        $journalLib->setCompanyCode($salesRuleVoucherModel->getcompanyCode());
        $journalLib->prepareHeader();

        $prepareStatus = $journalLib->prepareRedeemJournal(
            $companyCode,
            $salesRuleVoucherModel->getVoucherCode()
        );
        if ($prepareStatus) {
            $journalLib->createJournal("redeem");
        }

        // Send voucher code to customer email
        $customerModel = new \Models\Customer();
        $customerData = $customerModel::findFirst("customer_id = " . $salesRuleVoucherModel->getCustomerId());

        $email = new \Library\Email();
        $email->setEmailSender();
        $email->setName(
            $customerData->getLastName(),
            $customerData->getFirstName()
        );
        $email->setEmailReceiver($customerData->getEmail());
        $email->setEmailTag("redeem_point");
        if ($companyCode == "AHI") {
            $email->setTemplateId(\Helpers\GeneralHelper::getTemplateId(
                "ace_pay_with_point_confirmation",
                "AHI"
            ));
        } else if ($companyCode == "HCI") {
            $email->setCompanyCode($companyCode);
            $email->setTemplateId(\Helpers\GeneralHelper::getTemplateId(
                "informa_pay_with_point_confirmation",
                "HCI"
            ));
        } else if ($companyCode == "SLM") {
            $email->setCompanyCode($companyCode);
            $email->setTemplateId(\Helpers\GeneralHelper::getTemplateId(
                "selma_pay_with_point_confirmation",
                "SLM"
            ));
        }

        $email->send();

        return true;
    }

    public function createSoCIB2C()
    {
        if (empty($this->invoice_no)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "No Invoice Diperlukan";
            return false;
        }

        $invoice = new \Models\SalesInvoice();
        $resultInvoice = $invoice->findFirst("invoice_no = '" . $this->invoice_no . "'");
        if ($resultInvoice) {
            $invoice->assign($resultInvoice->toArray([]));
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Invoice Tidak Ditemukan";
            return false;
        }

        $soB2C = new \Library\SOCIB2C($invoice);
        $soB2C->prepareDCParamsSOCIB2C();
        $soB2C->generateDCParamsSOCIB2C();
        $soB2C->createSOCIB2C();
    }

    public function createSoCIB2B()
    {
        if (empty($this->invoice_no)) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "No Invoice Diperlukan";
            return false;
        }

        $invoice = new \Models\SalesInvoice();
        $resultInvoice = $invoice->findFirst("invoice_no = '" . $this->invoice_no . "'");
        if ($resultInvoice) {
            $invoice->assign($resultInvoice->toArray([]));
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Invoice Tidak Ditemukan";
            return false;
        }

        $soB2B = new \Library\SOCIB2B($invoice);
        $soB2B->prepareDCParamsSOCIB2B();
        $soB2B->generateDCParamsSOCIB2B();
        $soB2B->createSOCIB2B();
    }

    #region private function

    private function claimSentVoucher($customerId, $email)
    {

        //Check for any pending voucher from other customer
        $aceSendVoucher = new \Models\AceSendVoucher();
        $receivedVoucherResult = $aceSendVoucher->find(
            array(
                'conditions' => "email_receiver = '" . $email . "' and is_claim = 0 ",
            )
        );

        if (!empty($receivedVoucherResult)) {
            $receivedVoucher = $receivedVoucherResult->toArray();

            foreach ($receivedVoucher as $voucher) {
                $salesruleVoucher = new \Models\SalesruleVoucher();

                $salesruleVoucher->updateVoucherCustomerID([
                    'customer_id' => $customerId,
                    'voucher_code' => $voucher['voucher_code']
                ]);

                $result = $aceSendVoucher->updateIsClaim($voucher['voucher_code'], $email);
                if (!$result) {
                    return false;
                }
            }
        }
        return true;
    }

    private function changePasskeyAce($aceCustomerId, $oldPassword, $newPassword, $token)
    {
        $requestPayload["P_Cust_Id"] = $aceCustomerId;
        $requestPayload["P_Passkey_old"] = $oldPassword;
        $requestPayload["P_Passkey_new"] = $newPassword;
        $requestPayload["P_Company_Type"] = "A";

        $aceWebAPI = new \Library\AceWebAPI();
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log("change_pass_key_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);
            return false;
        }

        $changePassKey = $aceWebAPI->changePasskey($encryptedPayload, "token " . $token);
        if (!$changePassKey) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();

            LogHelper::log("change_pass_key_ace", "MESSAGE: " . $this->errorMessages . " ,ACE CUSTOMER ID: " . $aceCustomerId);
            return false;
        }

        return true;
    }

    private function getFirstAndLastName($fullName)
    {
        $fullNameArr = explode(" ", $fullName);
        $firstName = $fullNameArr[0];
        $lastName = "";
        if (count($fullNameArr) > 1) {
            array_shift($fullNameArr);

            $lastName = implode(" ", $fullNameArr);
        }

        return [
            "first_name" => $firstName,
            "last_name" => $lastName,
        ];
    }

    public function getDiffDateInSecond($firstDate, $secondDate)
    {
        $firstDate = new \DateTime($firstDate);
        $secondDate = new \DateTime($secondDate);

        return abs($secondDate->getTimestamp() - $firstDate->getTimestamp());
    }

    private function getStringValFromArr(array $data, $key)
    {
        return array_key_exists($key, $data) ? $data[$key] : "";
    }

    private function ruparupaFormatPhone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);

        if (substr($phone, 0, 2) == "62") {
            return substr_replace($phone, "0", 0, 2);
        }

        return $phone;
    }

    private function sendPhoneOTPToWhatsapp(AceWebAPI $aceWebApiLib, $generatedOtp = "", $formattedPhone = "", $template = "")
    {
        $sendOtpRows = $aceWebApiLib->sendWhatsapp($formattedPhone, $template, array('otp' => $generatedOtp));
        if (!$sendOtpRows) {
            $this->errorCode = $aceWebApiLib->getErrorCode();
            $this->errorMessages = $aceWebApiLib->getErrorMessages();
            LogHelper::log("send_phone_otp_ace", "MESSAGE: " . $this->errorMessages . ", Action " . $template . ", Phone" . $formattedPhone);
            return false;
        }

        return true;
    }

    #endregion
}
