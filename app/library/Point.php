<?php
/**
 * Checking and Redeem point for Kawan Lama Group
 *
 * Here User can check their point and redeem it as Gift Card Redeem Voucher
 *
 * For Testing purpose use following credential :
 * company     : AHI
 * cardID      : AR00025373
 * PIN         : 123456
 *
 */

namespace Library;

class Point
{
    protected $customer_id;
    protected $company_id;
    protected $member_id;
    protected $member_pin;
    protected $point;
    protected $amount;

    protected $voucherCode;

    protected $errorCode;
    protected $errorMessages;

    protected $company_code = 'ODI';

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @return mixed
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }

    /**
     * @param mixed $voucherCode
     */
    public function setVoucherCode($voucherCode)
    {
        $this->voucherCode = $voucherCode;
    }

    /**
     * @return mixed
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param mixed
     */
    public function setCompanyCode($companyCode)
    {
        $this->company_code = $companyCode;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function setFromArray($data_array = array())
    {
        foreach($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    /**
     * middleware to call point checking Kawan Lama webservice
     */
    public function pointCheck()
    {
        if(empty($this->company_id)) {
            $this->errorMessages[] = "Company ID is required";
        }

        if(empty($this->member_pin)) {
            $this->errorMessages[] = "Member PIN is required";
        }

        if(empty($this->member_id)) {
            $this->errorMessages[] = "Member ID is required";
        }

        if(empty($this->customer_id)) {
            $this->errorMessages[] = "Customer ID is required";
        }

        if(!empty($this->errorMessages)) {
            $this->errorCode = "RR100";
            return;
        }

        // Check member is_verified
        $customer2GroupModel = new \Models\Customer2Group();
        $customer2GroupParam["conditions"] = sprintf("customer_id = %d AND member_number = '%s'", (int) $this->customer_id, $this->member_id);
        $customer2GroupData = $customer2GroupModel->findFirst($customer2GroupParam);
        if (!$customer2GroupData) {
            $this->errorCode = 400;
            $this->errorMessages[] = "Data member tidak ditemukan";
            return;
        }

        $eligibleIsVerifiedList = [10];
        if (!in_array((int)$customer2GroupData->getIsVerified(), $eligibleIsVerifiedList)) {
            $companyName = "membership";
            switch ($customer2GroupData->getGroupId()) {
                case 4:
                    $companyName = "ID ACE Rewards";
                    break;
                case 5:
                    $companyName = "ID Informa Rewards";
                    break;
                case 6:
                    $companyName = "Toys Kingdom Smile Club";
                    break;
                case 9:
                    $companyName = "ID Selma Rewards";
                    break;
            }

            $this->errorCode = 400;
            $this->errorMessages[] = sprintf("Nomor handphone %s kamu tidak sama dengan yang digunakan di akun ruparupa. Silakan sesuaikan nomor handphone kamu", $companyName);
            return;
        }
        
        // mock check point process for staging env 
        if ($_ENV['ENVIRONMENT'] != "production"){
            // Init Model
            $mockPointModel = new \Models\MockPoint;
            $mockPointParam["conditions"] = "member_id = '". $this->member_id ."'";
            $mockPointFound = $mockPointModel->findFirst($mockPointParam);
            if (empty($mockPointFound)) {
                //insert to db 
                $params = array();
                $params["member_id"] = $this->member_id;
                $mockPointModel->setFromArray($params);
                $saveStatus = $mockPointModel->saveData("mock_point");
                if(!$saveStatus) {
                    $this->errorCode = $mockPointModel->getErrorCode();
                    $this->errorMessages[] = $mockPointModel->getErrorMessages();
                }
                $this->point = 10000;
            }else{
                $this->point = $mockPointFound->getPoint();
            }
            return;
        } 

        $client = new \nusoap_client($_ENV['REDEEEM_END_POINT'], 'wsdl');

        $err = $client->getError();
        if ($err) {
            $this->errorCode = "RR002";
            $this->errorMessages[] = $err;
            return;
        }

        $response = $client->Call("getPoint", [
            'company' => $this->company_id,
            'cardID' => $this->member_id,
            'passKey' => $this->member_pin
        ]);

        if (empty($response)) {
            $this->errorCode = "RR002";
            $this->errorMessages[] = "Mohon maaf sedang terjadi kesalahan sistem. Silakan dicoba kembali atau dapat menghubungi Customer Service RupaRupa";
            return;
        }

        \Helpers\LogHelper::log("check_point", "Response [". $this->member_id ."] : " . json_encode($response));
        $response = json_decode($response['getPointResult'],true);

        if (isset($response[0]['point'])) {
            $this->point = $response[0]['point'];
        } else {
            $errorMessage = !empty($response[0]['errMsg']) ? $response[0]['errMsg'] : "Mohon maaf sedang terjadi kesalahan sistem. Silakan dicoba kembali atau dapat menghubungi Customer Service RupaRupa";
            $this->errorCode = "RR002";
            $this->errorMessages[] = $errorMessage;
        }
        return;
    }

    /**
     * Execute redeem membership point
     */
    public function redeemPoint()
    {
        if(empty($this->company_id)) {
            $this->errorMessages[] = "Company ID is required";
        }

        if(empty($this->member_pin)) {
            $this->errorMessages[] = "Member PIN is required";
        }

        if(empty($this->member_id)) {
            $this->errorMessages[] = "Member ID is required";
        }

        if(empty($this->amount)) {
            $this->errorMessages[] = "Amount can't be empty";
        }

        if(empty($this->company_code)) {
            $this->errorMessages[] = "Company code is required";
        }

        if(empty($this->customer_id)) {
            $this->errorMessages[] = "Customer ID is required";
        }

        $tempMemberList = ["TIM", "TAM"];
        if (!empty($this->member_id) && in_array(substr(trim($this->member_id), 0, 3), $tempMemberList)) {
            $this->errorMessages[] = "Member ". substr(trim($this->member_id), 0, 3) ." tidak dapat melakukan redeem point";
        }

        if(!empty($this->errorMessages)) {
            $this->errorCode = "RR100";
            return;
        }

        // Check member is_verified
        $customer2GroupModel = new \Models\Customer2Group();
        $customer2GroupParam["conditions"] = sprintf("customer_id = %d AND member_number = '%s'", (int) $this->customer_id, $this->member_id);
        $customer2GroupData = $customer2GroupModel->findFirst($customer2GroupParam);
        if (!$customer2GroupData) {
            $this->errorCode = 400;
            $this->errorMessages[] = "Data member tidak ditemukan";
            return;
        }

        $eligibleIsVerifiedList = [10];
        if (!in_array((int)$customer2GroupData->getIsVerified(), $eligibleIsVerifiedList)) {
            $companyName = "membership";
            switch ($customer2GroupData->getGroupId()) {
                case 4:
                    $companyName = "ID ACE Rewards";
                    break;
                case 5:
                    $companyName = "ID Informa Rewards";
                    break;
                case 6:
                    $companyName = "Toys Kingdom Smile Club";
                    break;
                case 9:
                    $companyName = "ID Selma Rewards";
                    break;
            }

            $this->errorCode = 400;
            $this->errorMessages[] = sprintf("Nomor handphone %s kamu tidak sama dengan yang digunakan di akun ruparupa. Silakan sesuaikan nomor handphone kamu", $companyName);
            return;
        }

        // Check if redeem is already in progress
        $inProgress = \Library\Redis\PointRedeem::getPointRedeemProgress($this->customer_id);

        if (!$inProgress || $inProgress == "1") {
            $this->errorCode = 400;
            $this->errorMessages[] = "Tukar Poin tidak dapat dilakukan saat ini, silakan coba dalam 1 menit";
            \Helpers\LogHelper::log("redeem_point", $this->member_id." -> "."Tukar Poin tidak dapat dilakukan saat ini, silakan coba dalam 1 menit");
            return;

        } else {
            \Library\Redis\PointRedeem::setPointRedeemProgress($this->customer_id);
        }

        $now = date("Ymd");

        // mock redeem point process for staging env 
        if ($_ENV['ENVIRONMENT'] != "production") {

            // Mock redeem point conversion
            $redeemPointTable = array(
                "AHI" => array(
                    "25000" => "10",
                    "50000" => "20",
                    "100000" => "40"
                ),
                "HCI" => array(
                    "25000" => "10",
                    "50000" => "20",
                    "100000" => "40"
                ),
                "TGI" => array(
                    "25000" => "100",
                    "50000" => "200",
                    "100000" => "400"
                )
            );

            $convertedPoint = $redeemPointTable[$this->company_id][$this->amount];
            if (empty($convertedPoint)) {
                $this->errorCode = "RR002";
                $this->errorMessages[] = "company_id atau amount tidak valid";
                return;
            }
        
            // Init Model
            $mockPointModel = new \Models\MockPoint;
            $mockPointParam["conditions"] = "member_id = '". $this->member_id ."'";
            $mockPointFound = $mockPointModel->findFirst($mockPointParam);

            if (empty($mockPointFound)) {
                $this->errorCode = "RR002";
                $this->errorMessages[] = "Data member tidak valid";
                return;
            }

            $point = $mockPointFound->getPoint();
            if ($point < $convertedPoint){
                //error message insufficient point 
                $this->errorCode = "RR002";
                $this->errorMessages[] = printf("Poin anda tidak mencukupi untuk melakukan penukaran, Jumlah poin yang dibutuhkan %s poin, Jumlah Poin Anda %s poin",$point,$convertedPoint);
                return;
            }

            // Save to DB voucher
            $voucherModel = new \Models\SalesruleVoucher();
            $dateExpires = date('Y-m-d', strtotime("+6 months", strtotime(date("Y-m-d"))));
            $voucherCode = "RPSTG".$this->member_id.$this->company_id.time().rand(10,1000);

            $this->voucherCode = $voucherCode;

            if(!empty($_ENV['ANIV_REDEEM_DATE_FROM']) && !empty($_ENV['ANIV_REDEEM_DATE_TO'])) {
                if ($now >= $_ENV['ANIV_REDEEM_DATE_FROM'] && $now <= $_ENV['ANIV_REDEEM_DATE_TO']) {
                    $this->amount = $this->amount * 2;
                }
            }

            $params = [
                "rule_id" => $_ENV['REDEEM_SALES_RULE_' . $this->company_code],
                "voucher_code" =>  $voucherCode,
                "voucher_amount" =>  $this->amount,
                "expiration_date" => $dateExpires,
                "customer_id" => $this->customer_id,
                "status" => "10",
                "type" => 5,
                "created_at" => date("Y-m-d H:i:s"),
                "member_id" => $this->member_id,
                "company_code" => "ODI"
            ];
            $voucherModel->setFromArray($params);

            $saveStatus = false;
            try {
                $saveStatus = $voucherModel->saveData("redeem_point");
            } catch (\Library\HTTPException $e) {
                \Helpers\LogHelper::log("confirm_redeem_point", 'failed to save to database ' . $e->getMessage());
            }
            if(!$saveStatus){
                $this->errorCode = "500";
                $this->errorMessages[] = 'Terjadi kesalahan penukaran point';
            }

            // update mock point record
            $point -= $convertedPoint;
            $upsertParams = [
                "mock_point_id" =>  $mockPointFound->getMockPointId(),
                "member_id" => $mockPointFound->getMemberId(),
                "point" =>  $point
            ];

            $mockPointModel->setFromArray($upsertParams);
            $updateStatus = $mockPointModel->saveData("mock_point");
            if(!$updateStatus) {
                $this->errorCode = $mockPointModel->getErrorCode();
                $this->errorMessages[] = $mockPointModel->getErrorMessages();
                return false;
            }
            return;
        }

        $client = new \nusoap_client($_ENV['REDEEEM_END_POINT'], 'wsdl');

        $err = $client->getError();
        if ($err) {
            $this->errorCode = "RR002";
            $this->errorMessages[] = $err;

             // Delete Redeem Point Progress
             \Library\Redis\PointRedeem::deletePointRedeemProgress($this->customer_id);
            return;
        }

        /**
         * Step 1 of 2, "authorize" for point redemption
         * expected response is RedeemID to be use to actually execute redemption
         */
        $response = $client->Call("redeemPoint", [
            'company' => $this->company_id,
            'custID' => $this->customer_id,
            'cardID' => $this->member_id,
            'passKey' => $this->member_pin ,
            'Amount'=>$this->amount
        ]);

        // Delete Redeem Point Progress
        \Library\Redis\PointRedeem::deletePointRedeemProgress($this->customer_id);

        if(empty($response['redeemPointResult'])) {
            $this->errorCode = "RR002";
            $this->errorMessages[] = "Connection failed when try connecting to server";
            \Helpers\LogHelper::log("redeem_point", $this->member_id." -> ".json_encode($response));
            //\Helpers\LogHelper::log("redeem_point", $this->member_id." -> ".print_r($client->getError(),true));
            return;
        } else {
            \Helpers\LogHelper::log("redeem_point", $this->member_id." -> ".json_encode($response));
            $response = json_decode($response['redeemPointResult'],true);
        }

        /**
         * in case of valid / authorized redemption attempt
         */
        if(isset($response[0]['RedeemID']))
        {
            $reConfirmRedeem = $client->Call("confirmRedeem", [
                'RedeemID' =>$response[0]['RedeemID'],
                'company_Id' => $this->company_id,
                'cust_ID' => $this->customer_id,
                'cardID' => $this->member_id
            ]);
            \Helpers\LogHelper::log("confirm_redeem_point", $response[0]['RedeemID']." -> ".json_encode($reConfirmRedeem));
            if (isset($reConfirmRedeem['confirmRedeemResult'])) {
                $reConfirmRedeem = json_decode($reConfirmRedeem['confirmRedeemResult'], true);
            }else{
                \Helpers\LogHelper::log("confirm_redeem_point", $response[0]['RedeemID']." -> ".$reConfirmRedeem);
            }

            $voucherCode = "";
            if(isset($reConfirmRedeem[0]['VoucherID'])){
                $voucherCode = !empty($reConfirmRedeem[0]['VoucherID'])? $reConfirmRedeem[0]['VoucherID'] : "";
            }
            
            if($voucherCode != "" && !empty($voucherCode))
            {
                // Save to DB voucher
                $voucherModel = new \Models\SalesruleVoucher();
                $dateExpires  = date("Y-m-d");
                $dateExpires  = date('Y-m-d', strtotime("+6 months", strtotime($dateExpires)));

                $this->voucherCode = $voucherCode;

                if(!empty($_ENV['ANIV_REDEEM_DATE_FROM']) && !empty($_ENV['ANIV_REDEEM_DATE_TO'])) {
                    if ($now >= $_ENV['ANIV_REDEEM_DATE_FROM'] && $now <= $_ENV['ANIV_REDEEM_DATE_TO']) {
                        $this->amount = $this->amount * 2;
                    }
                }

                $params = [
                    "rule_id" => $_ENV['REDEEM_SALES_RULE_' . $this->company_code],
                    "voucher_code" =>  $voucherCode,
                    "voucher_amount" =>  $this->amount,
                    "expiration_date" => $dateExpires,
                    "customer_id" => $this->customer_id,
                    "status" => "10",
                    "type" => 5,
                    "created_at" => date("Y-m-d H:i:s"),
                    "member_id" => $this->member_id,
                    "company_code" => "ODI"
                ];
                $voucherModel->setFromArray($params);

                $saveStatus = false;
                try {
                    $saveStatus = $voucherModel->saveData("redeem_point");
                } catch (\Library\HTTPException $e) {
                    \Helpers\LogHelper::log("confirm_redeem_point", 'failed to save to database ' . $e->getMessage());
                }

                if ($saveStatus) {
                    // Create journal redeem
                    $journalLib = new \Library\JournalSAP();
                    $journalLib->setVoucher($voucherModel);
                    $journalLib->setCompanyCode($this->company_code);
                    $journalLib->prepareHeader();
                    $prepareStatus = $journalLib->prepareRedeemJournal($this->company_id, $this->voucherCode);
                    if($prepareStatus) {
                        $journalLib->createJournal("redeem");
                    }

                    // Send voucher code to customer email
                    $customerModel = new \Models\Customer();
                    $customerData = $customerModel::findFirst("customer_id = " . $this->customer_id);

                    $email = new \Library\Email();
                    $email->setEmailSender();
                    $email->setName($customerData->getLastName(),$customerData->getFirstName());
                    $email->setEmailReceiver($customerData->getEmail());
                    $email->setEmailTag("redeem_point");

                    $templateID = \Helpers\GeneralHelper::getTemplateId("customer_redeem", $this->company_code);
                    $email->setTemplateId($templateID);
                    $email->setCompanyCode($this->company_code);
                    $email->setVoucherCode($voucherCode);
                    $email->setVoucherExpired(date('j F Y', strtotime($dateExpires)));
                    $email->send();

                } else {
                    $this->errorCode = "500";
                    $this->errorMessages[] = 'Terjadi kesalahan penukaran point';
                }
            }
        }
        

        if(!empty($response[0]['errMsg']) && $response[0]['hasError'] == 'true') {
            $errorMessage=$response[0]['errMsg'];
            $this->errorCode = "RR002";
            $this->errorMessages[] = $errorMessage;
        }

        return;

    }
}