<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use Helpers\CartHelper;
use Helpers\RouteHelper;
use Phalcon\Cache\Frontend\Data;
use Phalcon\Db as Database;
use Models\StaticPage as StaticPageModel;
use Models\MasterUrlKey as MasterUrlKey;

class StaticPage
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllStaticPage($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';
        $companyCode = (isset($params['company_code']) && !empty($params['company_code'])) ? $params['company_code'] : 'ODI';
        $staticPageList = array();
        $staticPageModel = new StaticPageModel();

        if ($companyCode == 'ODI') {
            $query = [
                "conditions" => "status > -1"
            ];
        } else {
            $query = [
                "conditions" => "status > -1 AND company_code= '".$companyCode."'"
            ];
        }

        $staticPageData = $staticPageModel->find($query);

        $staticPageResult = $staticPageData->toArray();

        if(!empty($staticPageResult)){
            foreach ($staticPageResult as $page => $val){
                if ($companyCode == 'ODI') {
                    $routeData = RouteHelper::getRouteData($val['static_page_id'], 'static page', $access_type, $companyCode);
                    if (empty($routeData)) {
                        $routeData = RouteHelper::getRouteData($val['static_page_id'], 'static page', $access_type, 'HCI');
                        if (empty($routeData)) {
                            $routeData = RouteHelper::getRouteData($val['static_page_id'], 'static page', $access_type, 'AHI');
                        }
                    }
                } else {
                    $routeData = RouteHelper::getRouteData($val['static_page_id'], 'static page', $access_type, $companyCode);
                }

                $staticPageResult[$page]['url_key'] = (isset($routeData['url_key'])) ? $routeData['url_key'] : '' ;
                $staticPageResult[$page]['additional_header'] = (isset($routeData['additional_header'])) ? $routeData['additional_header'] : '' ;
            }
            return $staticPageResult;
        }
        else {
            $this->errorCode="RR302";
            $this->errorMessages="No static page has been found";
            return;
        }
    }

    public function getStaticPageDetail($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';
        $companyCode = (isset($params['company_code']) && !empty($params['company_code'])) ? $params['company_code'] : 'ODI';
        $identifier = isset($params['identifier']) ? $params['identifier'] : '';

        // we have to check whether it's id or url_key
        if (!empty($identifier)) {
            if (is_numeric($identifier)) {
                $page_id = $identifier;
            } else {
                $master_url_model = new MasterUrlKey();
                $urlKey = array();

                // if not numeric, it's by url key
                $sql  = "SELECT reference_id FROM master_url_key WHERE url_key = '".$identifier."' AND company_code ='".$companyCode."' ORDER BY created_at DESC LIMIT 1";
                $master_url_model->useWriteConnection();
                $result = $master_url_model->getDi()->getShared($master_url_model->getConnection())->query($sql);

                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );

                $urlData = $result->fetchAll();
                if (!empty($urlData)) {
                    $page_id = $urlData[0]['reference_id'];
                } else {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "No data found because no identifier supplied.";
                    return;
                }
            }
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages = "No data found because no identifier supplied.";
            return;
        }
        
        $staticPageDetail = array();
        $staticPageModel = new StaticPageModel();

        if ($companyCode == 'ODI') {
            // grant all access
            // if cms, identifier is static page id
            if($access_type == "cms"){
                $parameter = array(
                    "columns" => "static_page_id, title, body_html, body_html_mobile, company_code, status",
                    "conditions" => "static_page_id='".$page_id."' AND status > -1"
                );
            }
            else{
                $parameter = array(
                    "columns" => "static_page_id, title, body_html, body_html_mobile, company_code, status",
                    "conditions" => "static_page_id='".$page_id."' AND status = 10"
                );
            }
        } else {
            if($access_type == "cms"){
                $parameter = array(
                    "columns" => "static_page_id, title, body_html, body_html_mobile, company_code, status",
                    "conditions" => "static_page_id='".$page_id."' AND status > -1 AND company_code = '".$companyCode."'"
                );
            }
            else{
                $parameter = array(
                    "columns" => "static_page_id, title, body_html, body_html_mobile, company_code, status",
                    "conditions" => "static_page_id='".$page_id."' AND status = 10 AND company_code = '".$companyCode."'"
                );
            }
        }

        $findPage = $staticPageModel->findFirst($parameter);

        if(!empty($findPage)){
            $staticPageDetail = $findPage->toArray();
            $staticPageDetail['body_html'] = html_entity_decode($staticPageDetail['body_html']);

            if($access_type != 'cms'){
                $staticPageDetail['body_html'] = $this->searchCmsBlock($staticPageDetail['body_html']);
            }

            $routeData = RouteHelper::getRouteData($page_id, 'static page', $access_type, $staticPageDetail['company_code']);
            $staticPageDetail['url_key'] = (isset($routeData['url_key'])) ? $routeData['url_key'] : '' ;
            $staticPageDetail['additional_header'] = (isset($routeData['additional_header'])) ? $routeData['additional_header'] : '' ;
            $staticPageDetail['additional_header_mobile'] = $routeData['additional_header_mobile'];
            $staticPageDetail['inline_script'] = $routeData['inline_script'];
            $staticPageDetail['inline_script_mobile'] = $routeData['inline_script_mobile'];
            $staticPageDetail['document_title'] = $routeData['document_title'];

            return $staticPageDetail;
        }
        else {
            $this->errorCode = "RR302";
            $this->errorMessages = "No data found";
            return;
        }
    }

    public function searchCmsBlock($body_html = ""){
        /**
         * Format Placeholder : @cms_block:id@
         * Ex. @cms_block:32@
         */

        if(empty($body_html)){
            return false;
        }

        preg_match_all('~\{{cms_block:(.+?)\}}~',$body_html,$matches);

        $cmsBlockBody = [];
        foreach ($matches[1] as $key => $blockId){
            $cmsBlockModel = new \Models\CmsBlock();
            $result = $cmsBlockModel->findFirst(
                [
                    "columns" => "body_html",
                    "conditions" => "cms_block_id=".$blockId
                ]
            );
            if($result){
                $cmsBlockBody[$key]['find'] = "{{cms_block:".$blockId."}}";
                $cmsBlockBody[$key]['value'] = $result->toArray()['body_html'];
            }else{
                // if not found, replacing with blank
                $cmsBlockBody[$key]['find'] = "{{cms_block:".$blockId."}}";
                $cmsBlockBody[$key]['value'] = '';
            }
        }

        // replace placeholder
        foreach ($cmsBlockBody as $key => $value){
           $body_html = str_replace($value['find'],$value['value'],$body_html);
        }

        return $body_html;
    }

}