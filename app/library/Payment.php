<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 6/21/2017
 * Time: 2:09 PM
 */

namespace Library;

use \Helpers\LogHelper;

class Payment
{
    protected $paymentList;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function loadPaymentData()
    {
        // Get Payment list form monggo
        try {
            // load data from mongo db
            $mongoDb = new Mongodb();
            $mongoDb->setCollection(getenv('PAYMENT_COLLECTION'));
            $collection = $mongoDb->getCollection();
            $result = $collection->findOne();
            unset($result['_id']);
            $this->paymentList = $result;
        }catch (\Exception $e) {
           $this->paymentList = [];
           LogHelper::log("payment", $e->getMessage());
        }
    }

    public function getPaymentList()
    {
        return $this->paymentList;
    }

    public function getInstallmentList()
    {
        if(empty($this->paymentList)) {
            $this->loadPaymentData();
        }

        if (empty($this->paymentList)) {
            return [];
        }

        $bankList = $this->paymentList['credit_card_installment']['bank_list'];
        $installmentList = [];
        if(!empty($bankList)) {
            foreach ($bankList as $bank) {
                if ($bank['status'] == true) {
                    $installmentList[] = [
                        "bank_name" => $bank['label'],
                        "bank_name_ori" => $bank['bank_name'],
                        "bank_logo" => $bank['bank_logo'],
                        "bin" => $bank['bin'],
                        "tenor" => $bank['tenor']
                    ];
                }
            }
        }

        return $installmentList;
    }

    public function isAllowedCreditCard($cc_bin = "")
    {
        if(empty($this->paymentList)) {
            $this->loadPaymentData();
        }

        if (empty($this->paymentList) || empty($cc_bin)) {
            return false;
        }

        $rejectedBin = $this->paymentList['credit_card']['rejected_bin'];

        if (in_array($cc_bin,$rejectedBin)) {
            return false;
        }

        return true;
    }

    public function creditCardVendorChecking($cc_bin)
    {
        if(empty($this->paymentList)) {
            $this->loadPaymentData();
        }

        if (empty($this->paymentList) || empty($cc_bin)) {
            return false;
        }

        $creditCardVendorList = $this->paymentList['credit_card']['bank_list'];

        $vendorInfo = [
            'payment_method' => '',
            'payment_location' => '',
            'message' => ''
        ];
        /**
         * Only check active vendor
         */
        foreach ($creditCardVendorList as $vendor) {
            if($vendor['status'] === true) {
                $vendorInfo = [
                    'payment_method' => $vendor['payment_method'],
                    'payment_location' => $vendor['payment_location'],
                    'message' => $vendor['message']
                ];

                if(empty($vendor['bin'])) {
                    break;
                } else {
                    $binList = explode(",",$vendor['bin']);
                    if(in_array($cc_bin,$binList)) {
                        break;
                    }
                }
            }
        }

        return $vendorInfo;
    }

    public function isCanInstallment($bin = '',$grand_total = '')
    {
        $installmentList = $this->getInstallmentList();

        $canInstallment = false;
        $bankname = '';
        foreach ($installmentList as $bank) {
            $binList = explode(",",$bank['bin']);

            if(in_array($bin,$binList)) {
                if(!empty($bank['tenor'])) {
                    $bankname = $bank['bank_name_ori'];
                    foreach ($bank['tenor'] as $tenor) {
                        if($grand_total >= $tenor['minimum_payment']) {
                            $canInstallment = true;
                        }
                    }
                }

                break;
            }
        }

        return [$canInstallment,$bankname];

    }

}