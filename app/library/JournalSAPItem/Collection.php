<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Library\JournalSAPItem;


class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Library\JournalSAPItem
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function prepareItemParam()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->prepareItemParam();
            $view[$idx] = $itemArray;

            $this->next();
        }
        return $view;
    }
}