<?php

namespace Library;

use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;

/**
 * Class HTTPException
 * @package Library
 *
 * Modified default exception, so we can log it and send response to client instate of throw exp error
 *
 * @message : Use to send message to client for error information
 * @code : Message code, please use standart error message, see /Libary/Response for some example
 * @errorArray : use for extra data to be use for anything
 *   dev : used for logging message, please give spesific error so dev can trace it and help to fix the bug
 *
 */
class HTTPException extends \Exception
{
	public $message;
	public $errorCode;
	public $devMessage;
	public $errorTitle;


	public function __construct($message = "", $code = 500, $errorArray = array()){
		$this->message = $message;
		$this->code = $code;
		$this->devMessage = @$errorArray['dev'];
		$this->errorTitle = @$errorArray['msgTitle'];

		
	}

	public function send(){
		$di = \Phalcon\DI::getDefault();

		// Log for dev tracing purpose
		$logger = new Logger("HTTPException");
		$stream = new StreamHandler($_ENV['LOG_PATH']."error.log", Logger::DEBUG);
		$dateFormat = "Y-m-d H:i:s";
		$output = "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n";
		$formatter = new \Monolog\Formatter\LineFormatter($output, $dateFormat, true,true);
		$stream->setFormatter($formatter);
		$logger->pushHandler($stream);
		$logger->addError("Error Code : " . $this->getCode() . "\n File :" . $this->getFile() . " on line " . $this->getLine() ."\n Message : " . $this->devMessage);

		$errorMsg = array("errors" => array("code" => $this->getCode(), "title" => $this->errorTitle, "Message" => $this->message));
	
		$rupaResponse = new \Library\Response();
		$rupaResponse->setContentType("application/json");
		$rupaResponse->setHeaderStatus($this->getCode());
		$rupaResponse->setContent($errorMsg);
		$rupaResponse->send();

		return true;
	}


}