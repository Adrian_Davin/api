<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/2/2017
 * Time: 10:10 AM
 */

namespace Library;


class LastSeenProduct
{
    protected $sku;
    protected $name;
    protected $weight;
    protected $packaging_height;
    protected $packaging_width;
    protected $packaging_length;
    protected $packaging_uom;
    protected $qty_ordered;
    protected $price;
    protected $special_price;
    protected $product_source;
    protected $primary_image_url;
    protected $supplier_alias;
    protected $updated_at;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function setFromArray($data_array = array(), $get_other_data = true)
    {
        foreach ($data_array as $key => $val) {
            $this->{$key} = $val;
        }

        if($get_other_data) {
            $productData = \Helpers\ProductHelper::getFlatProductData($this->sku);

            if(!empty($productData)) {
                $this->setFromArray($productData, false);
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Product with sku . ". $this->sku ."  is not found";
            }
        }

        if(empty($this->updated_at)) {
            $this->updated_at = time();
        }

        return true;
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);

        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) == 'object') {
                $view[$key] = $thisArray[$key]->getDataArray();
            } else if(is_scalar($val) || is_array($val)) {
                $view[$key] = $val;
            }
        }

        unset($view['errorCode']);
        unset($view['errorMessages']);

        return $view;
    }



}