<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


class ProductSynonym
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param array $params
     * @return array|void
     */
    public function getProductSynonymDetail($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';

        $productDetails = array();

        $productSynModel = new \Models\ProductSynonym();

        if($access_type == 'cms'){
            $result = $productSynModel->findFirst(
                [
                    "conditions" => "synonym_id ='".$params['synonym_id']."' AND status > -1"
                ]
            );
        }
        else{
            $result = $productSynModel->findFirst(
                [
                    "conditions" => "synonym_id ='".$params['synonym_id']."' AND status = 10"
                ]
            );
        }

        if($result){
            return $result->toArray();
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "No Product Synonym";
            return;
        }
    }

    public function getProductSynonymList($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';

        $productDetails = array();

        $productSynModel = new \Models\ProductSynonym();

        if($access_type == 'cms'){
            $result = $productSynModel->find(
                [
                    "columns" => "synonym_id,query_text,synonym_for,status",
                    "conditions" => "status > -1"
                ]
            );
        }
        else{
            $result = $productSynModel->find(
                [
                    "columns" => "synonym_id,query_text,synonym_for,status",
                    "conditions" => "status = 10"
                ]
            );
        }

        if($result){
            return $result->toArray();
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "No Product Synonym";
            return;
        }
    }

    public function saveSynonymsFile(){
        $productSynModel = new \Models\ProductSynonym();
        $resultProductSyn = $productSynModel::query();
        $resultProductSyn->andWhere("Models\ProductSynonym.status = 10");
        $result = $resultProductSyn->execute();
        $params = '';
        foreach($result->toArray() as $syn){
            $queryText = str_replace(array("\n\r", "\n", "\r"),'', trim($syn['query_text']));
            $synonymFor = str_replace(array("\n\r", "\n", "\r"),'', trim($syn['synonym_for']));
            $tempQueryText = '';
            $tempSynonymFor = '';

            // replace all space to _
            $ex = explode(',',trim($queryText));
            foreach($ex as $exp) {
                $queryParam = str_replace(' ', '_', trim($exp));
                if ($queryParam !== '') {
                    $tempQueryText .= $queryParam . ', ';
                }
            }

            // replace all space to _
            $ex = explode(',',trim($synonymFor));
            foreach($ex as $exp) {
                $queryParam = str_replace(' ', '_', trim($exp));
                if ($queryParam !== '') {
                    $tempSynonymFor .= $queryParam.', ';
                }
            }

            // merge text
            $params .= rtrim($tempQueryText,', ').', '.rtrim($tempSynonymFor).PHP_EOL;
        }

        $params = rtrim($params,', '.PHP_EOL);
        //save to file
        $fp = fopen($_ENV['PATH_SYNONYMS'].'synonyms.txt', 'w+');
        fwrite($fp, strtolower($params));
        fclose($fp);
    }
}