<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use Models\Product;
use Phalcon\Db as Db;
use Helpers\CartHelper;
use Helpers\RouteHelper;
use Helpers\ProductHelper;
use Models\Event as EventModel;
use Models\CategoryStockMapping;

class Event
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllEvent($params = array())
    {
        $eventModel = new EventModel();
        $access_type = isset($params['access_type'])?$params['access_type']:'';
        $date = date('Y-m-d H:i:s');
        
        $eventDetails = array();        
        $query = array();

        if ($params['company_code'] != "ODI") {
            $query = [
                "columns" => "event_id, title, identifier, term_conditions, status, start_date, end_date, big_banner, small_banner, is_active_small_banner, products, company_code, lock_qty",
                "conditions" => "status > -1 and company_code like '%".$params['company_code']."%'"
            ];
        }else {
            $query = [
                "columns" => "event_id, title, identifier, term_conditions, status, start_date, end_date, big_banner, small_banner, is_active_small_banner, products, company_code, lock_qty",
                "conditions" => "status > -1"
            ];
        }

        $eventArray = $eventModel->find($query);

        if(!empty($eventArray)){
            foreach ($eventArray as $item => $val){
                $companyCode = isset($val['company_code']) ? $val['company_code'] : 'ODI';
                $eventDetails[$item] = $val->toArray();

                $eventDetails[$item]['is_active_small_banner'] = boolval($eventDetails[$item]['is_active_small_banner']);

                if($access_type != "cms"){
                    // ubah status yang gak lagi aktif jadi 0
                    if(strtotime($eventDetails[$item]['end_date']) < strtotime($date) && $eventDetails[$item]['status'] == 10){
                        $eventDetails[$item]['status'] = '0';
                    }

                    if(strtotime($eventDetails[$item]['start_date']) > strtotime($date)){
                        $eventDetails[$item]['status'] = '0';
                    }
                }

                $routeData = RouteHelper::getRouteData($val['event_id'], "event", $access_type, $companyCode);
                if(!empty($routeData)){
                    $eventDetails[$item]['url_key'] = $routeData['url_key'];
                }else{
                    $eventDetails[$item]['url_key'] = "";
                }
            }

            return $eventDetails;
        }
        else {
            $this->errorCode = "RR302";
            $this->errorMessages = "No events has been found";
            return;
        }
    }

    public function getEventDetail($params = array())
    {
        $eventModel = new EventModel();
        $accessType = isset($params['access_type'])?$params['access_type']:'';
        $masterAppId = isset($params['master_app_id'])?$params['master_app_id']:'';
        $companyCode = (isset($params['company_code']) && !empty($params['company_code'])) ? $params['company_code'] : 'ODI';
        $identifier = (isset($params['identifier']) && !empty($params['identifier'])) ? $params['identifier'] : '';
        $date = date('Y-m-d H:i:s');

        if($accessType == "cms"){
            $parameter = array(
                "columns" => "event_id, title, identifier, term_conditions, status, start_date, end_date, big_banner, small_banner, is_active_small_banner, products, company_code, lock_qty",
                "conditions" => "event_id='".$identifier."' AND status > -1"
            );

            if (!is_numeric($identifier)) {
                $parameter = array(
                    "columns" => "event_id, title, identifier, term_conditions, status, start_date, end_date, big_banner, small_banner, is_active_small_banner, products, company_code, lock_qty",
                    "conditions" => "identifier='".$identifier."' AND company_code = '".$companyCode."' AND status > -1"
                );
            }
        } else{
            $parameter = array(
                "columns" => "event_id, title, identifier, term_conditions, status, start_date, end_date, big_banner, small_banner, is_active_small_banner, products, company_code, lock_qty",
                "conditions" => "event_id='".$identifier."' AND status = 10"
            );

            if (!is_numeric($identifier)) {
                $parameter = array(
                    "columns" => "event_id, title, identifier, term_conditions, status, start_date, end_date, big_banner, small_banner, is_active_small_banner, products, company_code, lock_qty",
                    "conditions" => "identifier='".$identifier."' AND company_code = '".$companyCode."' AND status = 10"
                );
            }
        }

        $findEvent = $eventModel->findFirst($parameter);
        if(!empty($findEvent)){
            $eventDetails = $findEvent->toArray();
            $eventID = $eventDetails['event_id'];
            if($accessType != "cms"){
                // ubah status yang gak lagi aktif jadi 0
                if(strtotime($eventDetails['end_date']) < strtotime($date) && $eventDetails['status'] == 10){
                    $eventDetails['status'] = '0';
                }

                if(strtotime($eventDetails['start_date']) > strtotime($date)){
                    $eventDetails['status'] = '0';
                }
            }

            $eventDetails['is_active_small_banner'] = boolval($eventDetails['is_active_small_banner']);

            $routeData = RouteHelper::getRouteData($eventDetails['event_id'], "event",$accessType, $companyCode);
            $eventDetails['url_key'] = $routeData['url_key'];
            $eventDetails['inline_script'] = $routeData['inline_script'];
            $eventDetails['additional_header'] = $routeData['additional_header'];

            $sql = "select sku,store_code from event_products where event_id = $eventID";
            $eventModel->useReadOnlyConnection();
            $resultEventProducts = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);
            $resultEventProducts->setFetchMode(Db::FETCH_ASSOC);
            $resultEventProductsArray = $resultEventProducts->fetchAll();
            $eventDetails['product_list'] = $resultEventProductsArray;

            return $eventDetails;
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages = "Data not found";
            return;
        }
    }

    public function handlePreviousEvent(){
        $eventModel = new \Models\Event();
        $dateMinus = date('Y-m-d H:i:s', strtotime('-1 hour'));

        $sql  = "SELECT * FROM event WHERE '".$dateMinus."' BETWEEN start_date AND end_date LIMIT 1";
        $eventModel->useReadOnlyConnection();
        $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

        $resultEvent->setFetchMode(
            Db::FETCH_ASSOC
        );

        $events = $resultEvent->fetchAll();

        if(!empty($events)){
            $events = $events[0];
            // delete stock and switch event id
            $products = explode(',', $events['products']);

            // switch event
            // $sql = "UPDATE event SET event_id = (event_id+100), status = 0 WHERE event_id = ".$events['event_id'];
            $sql = "UPDATE event SET event_id = 
                        (((SELECT selected_value FROM (SELECT MAX(event_id) AS selected_value FROM event) AS sub_selected_value) + 1)), 
                        STATUS = 0
                    WHERE event_id = ".$events['event_id'];
            $eventModel->useReadOnlyConnection();
            $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

            foreach ($products as $sku){
                $sql = "SELECT sku, store_code_not_null FROM event_stock_null WHERE sku ='".$sku."'";
                $eventModel->useReadOnlyConnection();
                $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

                $resultEvent->setFetchMode(
                    Db::FETCH_ASSOC
                );

                if (!empty($resultEvent->fetchAll())) {
                    $events = $resultEvent->fetchAll()[0];

                    $store_code_not_null = "'".str_replace(',',"','",$events['store_code_not_null'])."'";

                    // reset back the qty bk to qty
                    $sql = "UPDATE product_stock SET qty = qty_bk WHERE sku ='".$events['sku']."' AND store_code NOT IN (".$store_code_not_null.")";
                    $eventModel->useReadOnlyConnection();
                    $resultResetStock = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

                    // call category stock mapping model
                    $categoryModel = new CategoryStockMapping();
                    $categoryModel->categoryStockMapping(array('sku' => $events['sku']));

                    // update to elastic
                    $this->updateEventToElastic($sku);
//                $productVariantModel = new \Models\ProductVariant();
//                $productVariantModel->setSku($sku);
//                $variantDetail = $productVariantModel->getProductVariantDetail();
//
//                $productModel = new \Models\Product();
//                $productModel->saveToCache($variantDetail);
                }
            }
        }
    }

    public function handleNextEvent(){
        $eventModel = new \Models\Event();
        $date = date('Y-m-d H:i:s');

        $sql  = "SELECT * FROM event WHERE '".$date."' BETWEEN start_date AND end_date LIMIT 1";
        $eventModel->useReadOnlyConnection();
        $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

        $resultEvent->setFetchMode(
            Db::FETCH_ASSOC
        );

        $events = $resultEvent->fetchAll();

        if(!empty($events)){
            $events = $events[0];
            // delete stock and switch event id
            $products = explode(',', $events['products']);

            // switch event
            $sql = "UPDATE event SET event_id = 1, status = 10 WHERE event_id = ".$events['event_id'];
            $eventModel->useReadOnlyConnection();
            $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

            foreach ($products as $sku){
                $sql = "SELECT sku, store_code_not_null FROM event_stock_null WHERE sku ='".$sku."'";
                $eventModel->useReadOnlyConnection();
                $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

                $resultEvent->setFetchMode(
                    Db::FETCH_ASSOC
                );

                if (!empty($resultEvent->fetchAll())) {
                    $events = $resultEvent->fetchAll()[0];

                    $store_code_not_null = "'".str_replace(',',"','",$events['store_code_not_null'])."'";

                    // reset back the qty bk to qty
                    $sql = "UPDATE product_stock SET qty_bk = qty, qty = 0 WHERE sku ='".$events['sku']."' AND store_code NOT IN (".$store_code_not_null.")";
                    $eventModel->useReadOnlyConnection();
                    $resultResetStock = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

                    // call category stock mapping model
                    $categoryModel = new CategoryStockMapping();
                    $categoryModel->categoryStockMapping(array('sku' => $events['sku']));

                    // update to elastic
                    $this->updateEventToElastic($sku);
//                $productVariantModel = new \Models\ProductVariant();
//                $productVariantModel->setSku($sku);
//                $variantDetail = $productVariantModel->getProductVariantDetail();
//
//                $productModel = new \Models\Product();
//                $productModel->saveToCache($variantDetail);
                }
            }
        }
    }

    public function updateEventToElastic($sku = ''){
        // UPDATE DOC JUST FOR EVENT
        $productVariantModel = new \Models\ProductVariant();
        $productRes = $productVariantModel->findFirst(
            [
                "columns" => "product_id",
                "conditions" => "sku='".$sku."'"
            ]
        );

        if($productRes){
            $productId = $productRes->toArray()['product_id'];

            $elasticLib = new Elastic();

            $eventModel = new \Models\Event();
            $eventDetails = array();
            $result = $eventModel->find(
                [
                    "conditions" => "products LIKE '%,".$sku.",%' OR products LIKE '%,".$sku."' OR products LIKE '".$sku.",%' OR products LIKE '".$sku."'"
                ]
            );

            $result = $result->toArray();

            if(!empty($result)){
                foreach($result as $res => $val){
                    if($val['status'] > 0){

                        $routeData = \Helpers\RouteHelper::getRouteData($val['event_id'], 'event');

                        if(!empty($routeData)){
                            $eventDetails[$res]['event_id'] = $val['event_id'];
                            $eventDetails[$res]['start_date'] = $val['start_date'];
                            $eventDetails[$res]['end_date'] = $val['end_date'];
                            $eventDetails[$res]['title'] = $val['title'];
                            $eventDetails[$res]['url_key'] = $routeData['url_key'];
                        }
                    }
                }
            }

            $finalEvent = array_values($eventDetails);

            $params = [
                'body' => [
                    'doc' => [
                        'events' => $finalEvent
                    ]
                ]
            ];

            $elasticLib->index = "products";
            $elasticLib->type = "product";
            $elasticLib->body = $params;

            $updateStatus = $elasticLib->update($productId);
        }
    }

    public function switchEvent(){

        $this->handlePreviousEvent();
        $this->handleNextEvent();

        return true;
    }

    public function switchEventNormal(){
        $eventModel = new \Models\Event();
        $date = date('Y-m-d H:i:s');

//        $sql  = "SELECT * FROM event WHERE '".$date."' BETWEEN start_date AND end_date LIMIT 1";
        $sql = "SELECT products FROM event WHERE event_id = 1";
        $eventModel->useReadOnlyConnection();
        $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

        $resultEvent->setFetchMode(
            Db::FETCH_ASSOC
        );

        $events = $resultEvent->fetchAll();

        if(!empty($events)){
            $events = $events[0];
            // delete stock and switch event id
            $products = explode(',', $events['products']);

            // switch event
//            $sql = "UPDATE event SET event_id = 1, status = 10 WHERE event_id = ".$events['event_id'];
//            $eventModel->useReadOnlyConnection();
//            $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

            foreach ($products as $sku){
                $sql = "SELECT sku, store_code_not_null FROM event_stock_null WHERE sku ='".$sku."'";
                $eventModel->useReadOnlyConnection();
                $resultEvent = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

                $resultEvent->setFetchMode(
                    Db::FETCH_ASSOC
                );

                $res = $resultEvent->fetchAll();

                if (!empty($res)) {

                    $store_code_not_null = "'".str_replace(',',"','",$res[0]['store_code_not_null'])."'";

                    // reset back the qty bk to qty
                    $sql = "UPDATE product_stock SET qty_bk = qty, qty = 0 WHERE sku ='".$res[0]['sku']."' AND store_code NOT IN (".$store_code_not_null.")";
                    $eventModel->useWriteConnection();
                    $resultResetStock = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);

                    // call category stock mapping model
//                    $categoryModel = new CategoryStockMapping();
//                    $categoryModel->categoryStockMapping(array('sku' => $events['sku']));

                    // update to elastic
                    //$this->updateEventToElastic($sku);
//                $productVariantModel = new \Models\ProductVariant();
//                $productVariantModel->setSku($sku);
//                $variantDetail = $productVariantModel->getProductVariantDetail();
//
//                $productModel = new \Models\Product();
//                $productModel->saveToCache($variantDetail);
                }
            }
        }
    }

}