<?php

namespace Library;


use phalcon\Db as Database;

class CustomerCreditCard
{

    protected $errorCode;
    protected $errorMessages;
    protected $response;

    public function getResponse()
    {
        return $this->response;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }


    public function getCustomerCCData($customer_id)
    {
        $custCCArray = array();

        $sql = "SELECT * FROM customer_credit_card WHERE customer_id = '$customer_id' AND status = 10";

        $customerCCModel = new \Models\CustomerCreditCard();

        $customerCCModel->useReadOnlyConnection();
        $result = $customerCCModel->getDi()->getShared($customerCCModel->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $custCCArray = $result->fetchAll();

        } else {
            $this->errorCode = "404";
            $this->errorMessages = "Data kartu kredit tidak ditemukan.";
        }

        return $custCCArray;
    }
   

  
}
