<?php
/**
 * Library to handle generate voucher to be used in store
 * all param name is named to be match of system requirement
 */

namespace Library;


class POSVoucher
{
    protected $ToCompany;
    protected $NoBU;
    protected $NoSO;
    protected $NoInvoice;
    protected $Amount;
    protected $source;
    protected $HandlingFee;
    protected $company_code = 'ODI';
    protected $errorMessage;

    public $voucherId;

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    public function __construct($invoice = null)
    {
        if(!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    /**
     * @param mixed $NoInvoice
     */
    public function setNoInvoice($NoInvoice)
    {
        $this->NoInvoice = $NoInvoice;
    }

    /**
     * @param mixed $Amount
     */
    public function setAmount($amount)
    {
        $this->Amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->Amount;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
    /**
     * @param mixed $error
     */
    public function setErrorMessage($error)
    {
        $this->errorMessage = $error;
    }

    public function prepareParams()
    {
        $this->ToCompany = $this->invoice->Store->Supplier->getSupplierAlias();
        $this->NoBU = $this->invoice->getStoreCode();
        $this->NoSO = $this->invoice->SalesOrder->getOrderNo();
        $this->NoInvoice = $this->invoice->getInvoiceNo();
        $this->Amount = $this->countVoucherAmount();
        $this->HandlingFee = 0; // For now we don't used this, maybe we need it to send shipping amount
        $this->company_code = $this->invoice->getCompanyCode();
        $this->source = 2;
    }

    public function countVoucherAmount()
    {
        $amount = ($this->invoice->getSubtotal() - $this->invoice->getDiscountAmount()) + ($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount());

        $salesInstallationMdl = new \Models\SalesInstallation;
        $salesInstallation = $salesInstallationMdl->findFirst("installation_order_no = '" .$this->invoice->SalesOrder->getOrderNo(). "'");
        if ($salesInstallation) {
            $installationCompanyCode = $salesInstallation->getCompanyCode();
            if ($installationCompanyCode == "AHI" && $amount == 0) {
                $amount = 1;
            }
        }

        $amountFreePayment = 0;
        // add Rp. 1 for each free items
        foreach($this->invoice->SalesInvoiceItem as $item)
        {
            /**
             * @var $item \Models\SalesInvoiceItem
             */
            // if($item->getRowTotal() == 0) {
            //     if($item->getisFreeItem() == 0){
            //         $amount += 1;
            //     }                
            // }
            $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
            $salesOrderItemMdr = $salesOrderItemMdrModel->findFirst(sprintf("sales_order_item_id = %s", $item->getSalesOrderItemId()));
            if (!empty($salesOrderItemMdr)) {
                $mdrValueCustomer = !empty($salesOrderItemMdr->getValueCustomer()) ? $salesOrderItemMdr->getValueCustomer() : 0;
                if ($mdrValueCustomer > 0){
                    $amount += (int)$mdrValueCustomer;
                }
            }
            if ($amount > 0) {
                continue;
            }

            $amountFreePaymentPerSKU = 1 * $item->getQtyOrdered();
            $amountFreePayment += $amountFreePaymentPerSKU;
        }

        if ($amount == 0) {
            $amount = $amountFreePayment;
        }

        return number_format($amount, 0, '.', '');
    }

    public function getPickupVoucherParams(){
        $thisArray = get_object_vars($this);
        $parameter = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(is_scalar($val)) {
                $parameter[$key] = $val;
            }
        }

        return $parameter;
    }

    /**
     * @return string
     */
    public function createPOSVoucher()
    {
        $parameter = $this->getPickupVoucherParams();
        $nsq = new \Library\Nsq();
        $message = [
            "data" => json_encode($parameter),
            "invoice_no" => $parameter['NoInvoice'],
            "message" => "createPickupVoucher"
        ];
        $nsq->publishCluster('transactionSap', $message);

        return true;
    }

    public function publishPickupVoucher($params)
    {
        $apiWrapper = new APIWrapper(getenv('ORDER_STORE_API'));
        $apiWrapper->setEndPoint("invoices/pickup-voucher/publish");

        $payload = [];

        if (isset($params["invoice_no"])) {
            $payload["invoice_no"] = $params["invoice_no"];
        }

        $apiWrapper->setParam($payload);
        $apiWrapper->sendRequest("post");

        if (!empty($apiWrapper->getError())) {
            $this->setErrorMessage($apiWrapper->getError());
            return false;
        }

        if (!empty($apiWrapper->getErrorBody())) {
            $this->setErrorMessage($apiWrapper->getErrorBody()->message);
            return false;
        }
        return true;
    }
}