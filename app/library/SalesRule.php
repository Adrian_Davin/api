<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Library;

use Phalcon\Db as Database;

class SalesRule
{

    /**
     * @var $customer \Models\Customer
     */
    protected $customer;

    /**
     * @var $customerToken \Models\CustomerToken
     */
    protected $customerToken;

    protected $errorCode;
    protected $errorMessages;

    const PROTECT_QUOTA_ONLY_VOUCHER_SERBU = false;
    const VOUCHER_PREFIX_SERBU = "SERBA8";

    public function __construct()
    {
        $this->salesrule = new \Models\Salesrule();
        $this->salesruleVoucher = new \Models\SalesruleVoucher();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getVoucherDetail($params)
    {

        $voucher = $this->salesruleVoucher->getDetailVoucher($params);

        if(!empty($voucher)){
            $finalVoucher = $voucher;
            $finalVoucher['created_at'] = date('j F Y H:i:s', strtotime($voucher['created_at']));
            $finalVoucher['expiration_date'] = date('j F Y H:i:s', strtotime($voucher['expiration_date']));
            $finalVoucher['rule_expiration_date'] = date('j F Y H:i:s', strtotime($voucher['to_date']));
            $finalVoucher['status'] = ($voucher['status'] == '10' || $voucher['status'] == '15') ? "Active" : "Inactive";
            $finalVoucher['customer'] = array();
            if($voucher['customer_id'] != 0){

                $customerModel = new \Models\Customer();
                $customerData = $customerModel->findFirst([
                    'conditions' => ' customer_id = '.$voucher['customer_id'].' and status > -1 ',
                    'columns' => 'customer_id, first_name, last_name, email, phone, gender, status, birth_date'
                ]);

                if(!empty($customerData)) {
                    $finalVoucher['customer'] = $customerData->toArray();
                    switch ($finalVoucher['customer']['status']) {
                        case 0 : $finalVoucher['customer']['status'] =  "disabled"; break;
                        case 5 : $finalVoucher['customer']['status'] =  "quest"; break;
                        case 10 : $finalVoucher['customer']['status'] =  "enabled"; break;
                    }
                }
            }

            switch ($voucher['type']) {
                case 1 : $finalVoucher['type'] =  "Registration"; break;
                case 2 : $finalVoucher['type'] =  "Refund"; break;
                case 3 : $finalVoucher['type'] =  "General"; break;
                case 4 : $finalVoucher['type'] =  "Cashback"; break;
                case 5 : $finalVoucher['type'] =  "Redeem Point"; break;
            }
            unset($finalVoucher['customer_id']);
            unset($finalVoucher['to_date']);
            return $finalVoucher;

        }else{
            $this->errorCode = "RR106";
            $this->errorMessages[] = "Data not Found";
            return false;
        }
    }

    public function getVoucherTotal($params)
    {
        $total_voucher = $this->salesruleVoucher->count('rule_id = '.$params['rule_id'].' and status > -1 ');
        if($total_voucher || ($total_voucher == 0)){
            return array('total_voucher' => $total_voucher);
        }else{
            return false;
        }

    }

    public function getAllSalesVoucher($params = array())
    {


        $salesruleVoucherModel = new \Models\SalesruleVoucher();

        $flagCountOnly = FALSE;
        if (isset($params['query_type'])) {
            if ($params['query_type'] == 'count_only') {
                $flagCountOnly = TRUE;
            }

            unset($params['query_type']);
        }

        $result = $salesruleVoucherModel::query();

        if (isset($params['exp_from_hour']) && isset($params['exp_to_hour'])) {
            $fromDate = date('Y-m-d H:i:s', strtotime('+'.$params['exp_from_hour'].' hour'));
            $toDate = date('Y-m-d H:i:s', strtotime('+'.$params['exp_to_hour'].' hour'));
            $result->betweenWhere("Models\SalesruleVoucher.expiration_date", $fromDate, $toDate);
            unset($params['exp_from_hour'],$params['exp_to_hour']);
        }

        if(isset($params['exp_to_date'])) {
            $result->andWhere("expiration_date <= '" . $params['exp_to_date'] . "'");
            unset($params['exp_to_date']);
        }

        if(isset($params['type_in'])) {
            $result->andWhere("type in ('" . $params['type_in'] . "')");
            unset($params['type_in']);
        }

        foreach($params as $key => $val)
        {
            $result->andWhere($key ." = '" . $val ."'");
        }

        $result = $result->execute();
        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your data";
            $allSalesVoucher = array();
        } else {
            $salesVoucherArray = array();
            $i = 0;
            foreach ($result as $adminUser) {
                $salesVoucherArray[$i] = $adminUser->toArray([], true);
                $i++;
            }

            $allSalesVoucher = array_values($salesVoucherArray);

            if ($flagCountOnly) {
                $allSalesVoucher['list'] = array('query_type' => 'count_only');
            } else {
                $allSalesVoucher['list'] = $allSalesVoucher;
            }

            $allSalesVoucher['recordsTotal'] = count($allSalesVoucher) - 1;
        }

        return $allSalesVoucher;
    }

    public function countBySalesOrderId($params = array()){

        $salesRuleModel = new \Models\Salesrule();

        $sql = '
            SELECT COUNT(salesrule_order_id) AS sum
            FROM salesrule_order 
            WHERE sales_order_id = ' . $params['sales_order_id'] . ' 
            AND voucher_code IS NOT NULL;
        ';

        $salesRuleModel->useReadOnlyConnection();
        $result = $salesRuleModel->getDi()->getShared($salesRuleModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $result = $result->fetch();
        return $result;
    }

    public function getListVoucherCode($params = array()){
        $salesRuleModel = new \Models\Salesrule();
        $sql = "
            SELECT DISTINCT voucher_code 
            FROM salesrule_order 
            WHERE sales_order_id = {$params['sales_order_id']};
        ";

        $salesRuleModel->useReadOnlyConnection();
        $result = $salesRuleModel->getDi()->getShared($salesRuleModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $result = $result->fetchAll();
        return $result;
    }

    public function removeVoucherHistoryTransaction($params = array(), $vouchers = array()){
        $salesRuleOrderModel = new \Models\SalesruleOrder();
        

        // if (empty($params["release_promo"])) {   
        //     $params["release_promo"] == false;
        // }

        $voucherConcated = "";

        $sql = "
        START TRANSACTION;  
        
        UPDATE sales_order 
        SET status = 'canceled'
        WHERE status = 'new' 
        AND sales_order_id = {$params['sales_order_id']};
        ";

        $sql .= "
        UPDATE sales_order_payment
        SET status = 'expire'
        WHERE sales_order_id = {$params['sales_order_id']};
        ";

        // if ($params["release_promo"] == true ){
        foreach($vouchers as $voucher){
            if (self::PROTECT_QUOTA_ONLY_VOUCHER_SERBU) {
                if (substr($voucher['voucher_code'], 0, strlen(self::VOUCHER_PREFIX_SERBU)) == self::VOUCHER_PREFIX_SERBU) {
                    continue;
                }

                $sql .= "
                UPDATE salesrule_voucher
                SET limit_used = limit_used + 1
                WHERE voucher_code = '{$voucher['voucher_code']}' 
                AND limit_used > 0
                AND voucher_source <> 'coin_redeem'
                AND voucher_source <> 'coin-exchange-to-product'
                AND CASE
                    WHEN `type` = 10 THEN limit_used = times_used
                    ELSE TRUE
                END;
            ";
            } else {
                $sql .= "
                UPDATE salesrule_voucher
                SET limit_used = limit_used + 1
                WHERE voucher_code = '{$voucher['voucher_code']}' 
                AND limit_used > 0
                AND voucher_source <> 'coin_redeem'
                AND voucher_source <> 'coin-exchange-to-product'
                AND CASE
                    WHEN `type` = 3 THEN customer_id <> 0
                    WHEN `type` = 10 THEN limit_used = times_used
                    ELSE TRUE
                END;
            ";
            }
            
            $voucherConcated .= "{$voucher['voucher_code']};";
        }
        // }

        $sql .= "
        DELETE FROM salesrule_order
        WHERE sales_order_id = {$params['sales_order_id']};
        ";

        // if ($params["release_promo"] == true ){
        $sql .=
        "
        INSERT INTO sales_log (
                                admin_user_id, actions, affected_field, value, flag, sales_order_id, log_time
                            ) VALUES (
                                {$params['admin_user_id']},
                                'remove voucher history and add limit_used',
                                'salesrule_voucher.limit_used, salesrule_order',
                                '{$voucherConcated}',
                                'voucher',
                                {$params['sales_order_id']},
                                now()
                            );
                            ";
        // }
        $sql .= "                        
        COMMIT;";

        $salesRuleOrderModel->useWriteConnection();
        try {
            $salesRuleOrderModel->getDi()->getShared($salesRuleOrderModel->getConnection())->query($sql);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
}