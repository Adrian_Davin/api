<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/24/2017
 * Time: 2:05 PM
 */

namespace Library;


class CreditMemoInvoice
{
    protected $invoice_no;
    protected $refund_status;

    protected $credit_memo_items;

    protected $shipping_amount;
    protected $subtotal;
    protected $gift_cards_amount;

    /**
     * @return mixed
     */
    public function getInvoiceNo()
    {
        return $this->invoice_no;
    }

    /**
     * @param mixed $invoice_no
     */
    public function setInvoiceNo($invoice_no)
    {
        $this->invoice_no = $invoice_no;
    }

    /**
     * @return mixed
     */
    public function getRefundStatus()
    {
        return $this->refund_status;
    }

    /**
     * @param mixed $refund_status
     */
    public function setRefundStatus($refund_status)
    {
        $this->refund_status = $refund_status;
    }

    /**
     * @return mixed
     */
    public function getCreditMemoItems()
    {
        return $this->credit_memo_items;
    }

    /**
     * @param mixed $credit_memo_items
     */
    public function setCreditMemoItems($credit_memo_items)
    {
        $this->credit_memo_items = $credit_memo_items;
    }

    /**
     * @return mixed
     */
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

    /**
     * @param mixed $shipping_amount
     */
    public function setShippingAmount($shipping_amount)
    {
        $this->shipping_amount = $shipping_amount;
    }

    /**
     * @return mixed
     */
    public function getSubTotal()
    {
        return $this->subtotal;
    }

    /**
     * @param mixed $subtotal
     */
    public function setSubTotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return mixed
     */
    public function getGiftCardsAmount()
    {
        return $this->gift_cards_amount;
    }

    /**
     * @param mixed $gift_cards_amount
     */
    public function setGiftCardsAmount($gift_cards_amount)
    {
        $this->gift_cards_amount = $gift_cards_amount;
    }

    public function setFromArray($data_array = array())
    {
        // We count gift cards amount from here, reset it's value
        $this->gift_cards_amount = 0;

        foreach($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }

            if($key == 'items')
            {
                $creditMemoItems = array();
                foreach($val as $sku => $item) {
                    $creditMemoItemLib = new \Library\CreditMemoItem();
                    $creditMemoItemLib->setFromArray($item);

                    // Price in credit memo item already final no need for calculation
                    $subTotal = $creditMemoItemLib->getPrice() * $creditMemoItemLib->getQtyRefunded();
                    $this->shipping_amount += $creditMemoItemLib->getShippingAmount() * $creditMemoItemLib->getQtyRefunded();
                    $this->subtotal += $subTotal;

                    // Get GC amount per item that refunded
                    $this->gift_cards_amount += $creditMemoItemLib->getGiftCardsAmount() * $creditMemoItemLib->getQtyRefunded();

                    $creditMemoItems[] = $creditMemoItemLib;
                }

                $this->credit_memo_items = new \Library\CreditMemo\Items\Collection($creditMemoItems);
            }
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);

        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) == 'object') {
                $view[$key] = $thisArray[$key]->getDataArray(); // get everything
            } else {
                $view[$key] = $val;
            }
        }

        unset($view['errorCode']);
        unset($view['errorMessages']);

        return $view;
    }

}