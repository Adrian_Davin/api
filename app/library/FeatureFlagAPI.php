<?php


namespace library;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use Helpers\LogHelper;

class FeatureFlagAPI
{
    public function getFeatureFlag($queryParams)
    {
        $url = sprintf("%s%s", getenv('ORDER_STORE_API'), "feature-flag");
        try {
            $client = new GuzzleClient();
            $response = $client->get($url, ["query" => $queryParams]);
            $body = $response->getBody()->getContents();
            $parsedResponseBody = json_decode($body, true);
            return $parsedResponseBody["data"];
        } catch (RequestException $e) {
            $errMessage = sprintf("Failed get feature flag, query_params: %s, error: %s", json_encode($queryParams), $e->getMessage());
            LogHelper::log("feature_flag", $errMessage);
            return false;
        }
    }
}