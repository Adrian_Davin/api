<?php
/**
 * Created by PhpStorm.
 * User: hendywijaya
 * Date: 28/11/17
 * Time: 23.33
 */

namespace Library;

class MobileAppsNotification {

    public function checkLocation ($params) {
        $distance = $this->distance($params['lat'], $params['long'], -7.771292000000001, 110.34600499999999, 'K');
        if ($distance <= 1) {
            return true;
        }
        else {
            return false;
        }
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function sendNotification ($token) {

        $notificationPayload = [
            "dry_run" => false,
            "to" => $token,
            "content_available" => true,
            "priority" => "high",
            "notification" => [
                "title"=>"Ruparupa.com",
                "body"=>"Selamat datang di lokasi Ruparupa Skyshi.",
                "click_action"=> "home_screen"
            ],
            "data" => [
                "my_custom_data"=> [
                    "url_key" => "https=>//www.ruparupa.com/promotions/hurom.html?utm_source=appspush"
                ],
                "custom_notification"=> [
                    "title"=>"Ruparupa.com",
                    "body"=>"Juicer Hurom harga terbaik di 11-11 tahun ini, mulai dari Rp. 199.000!",
                    "content_available" => true,
                    "priority" => "high",
                    "show_in_foreground"=> true,
                    "click_action"=> "home_screen",
                    "my_custom_data"=> [
                        "url_key" => "https=>//www.ruparupa.com/promotions/hurom.html?utm_source=appspush"

                    ]
                ],
                "click_action"=> "home_screen"
            ]
        ];
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'key=AIzaSyD402ReTZw5alcnY6yyGM15T_y4TRf3wx8'
        ];
        $guzzleClient = new \GuzzleHttp\Client();
        $response = $guzzleClient->post('https://fcm.googleapis.com/fcm/send', ['json' => $notificationPayload, 'timeout' => 10,'headers' => $headers ]);
        return ['response' => json_decode($response->getBody()->getContents(), true), 'payload' => $notificationPayload];
    }

}