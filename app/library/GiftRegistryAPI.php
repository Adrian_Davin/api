<?php


namespace Library;

use Helpers\LogHelper;

class GiftRegistryAPI
{
    protected $errorCode;
    protected $errorMessage;
    protected $data;


    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
    /**
     * @return mixed
     */
    public function setErrorCode($error)
    {
        $this->errorCode = $error;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
    /**
     * @param mixed $error
     */
    public function setErrorMessage($error)
    {
        $this->errorMessage = $error;
    }
    public function getData()
    {
        return $this->data;
    }
    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
    public function updateRegistryItemOrder($params)
    {
        $apiWrapper = new APIWrapper(getenv('GIFT_API'));
        $apiWrapper->setEndPoint("registry/item/order");

        $payload = [];
        if (isset($params["registry_id"])) {
            $payload["registry_id"] = $params["registry_id"];
        }

        if (isset($params["sku"])) {
            $payload["sku"] = $params["sku"];
        }

        if (isset($params["quantity_paid"])) {
            $payload["quantity_paid"] = (int)$params["quantity_paid"];
        }

        if (isset($params["quantity_on_hold_payment"])) {
            $payload["quantity_on_hold_payment"] = (int)$params["quantity_on_hold_payment"];
        }

        if (isset($params["order_no"])) {
            $payload["order_no"] = $params["order_no"];
        }

        if (isset($params["cart_id"])) {
            $payload["cart_id"] = $params["cart_id"];
        }

        if (isset($params["invoice_no"])) {
            $payload["invoice_no"] = $params["invoice_no"];
        }

        $apiWrapper->setParam($payload);
        $apiWrapper->sendRequest("put");

        if (!empty($apiWrapper->getError())) {
            $this->errorCode = 400;
            $this->errorMessage = $apiWrapper->getError();

            return false;
        }

        if (!empty($apiWrapper->getErrorBody())){
            $this->setErrorCode(400);
            $this->setErrorMessage($apiWrapper->getErrorBody()->message);
            return false;
        }
        return true;
    }
    public function deleteRegistryItemOrder($params)
    {
        $apiWrapper = new APIWrapper(getenv('GIFT_API'));
        $apiWrapper->setEndPoint("registry/item/order");

        $payload = [];
        if (isset($params["registry_id"])) {
            $payload["registry_id"] = $params["registry_id"];
        }

        if (isset($params["sku"])) {
            $payload["sku"] = $params["sku"];
        }

        if (isset($params["order_no"])) {
            $payload["order_no"] = $params["order_no"];
        }

        $apiWrapper->setParam($payload);
        $apiWrapper->sendRequest("delete");

        if (!empty($apiWrapper->getError())) {
            $this->errorCode = 400;
            $this->errorMessage = $apiWrapper->getError();

            return false;
        }

        if (!empty($apiWrapper->getErrorBody())){
            $this->setErrorCode(400);
            $this->setErrorMessage($apiWrapper->getErrorBody()->message);
            return false;
        }
        return true;
    }
    public function getRegistry($params)
    {
        $apiWrapper = new APIWrapper(getenv('GIFT_API'));

        $payload = [];
        if (isset($params["registry_id"])) {
            $payload['registry_id'] = $params["registry_id"];
        }

        if (isset($params["customer_id"])) {
            $payload['customer_id'] = $params["customer_id"];
        }

        if (count($payload) > 0) {
            $query = http_build_query($payload);
            $queryParam = sprintf("registry?%s" , $query);
            $apiWrapper->setEndPoint($queryParam);
        } else {
            $apiWrapper->setEndPoint("registry");
        }
        $apiWrapper->sendRequest("get");
        if (!empty($apiWrapper->getError())) {
            $this->setErrorCode(400);
            $this->setErrorMessage($apiWrapper->getError());
            return false;
        }

        if (!empty($apiWrapper->getErrorBody())){
            $this->setErrorCode(400);
            $this->setErrorMessage($apiWrapper->getErrorBody()->message);
            return false;
        }

        $apiWrapper->formatResponse();
        if (count($apiWrapper->getData()) <= 0) {
            $this->setErrorCode(400);
            $this->setErrorMessage("data not found");
            return false;
        }

        $this->setData($apiWrapper->getData()[0]);
        return true;
    }
}