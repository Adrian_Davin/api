<?php

namespace Library;

use \Datetime;

class SalesInstallation
{
    public function insertInstallationHistory($params = array())
    {
        if (empty($params) || !isset($params["sales_installation_id"]) || !isset($params['value'])) {
            return false;
        }

        $apiWrapper = new APIWrapper(getenv('MONGO_LOG_API'));
        $apiWrapper->setHeaders(array("x-api-key" => getenv('MONGO_LOG_TOKEN')));
        
        if (getenv('ENVIRONMENT') != 'production') {
            $apiWrapper->setEndPoint("/staging/insert");
        } else {
            $apiWrapper->setEndPoint("/insert");
        }
        
        $currentDateTime = new DateTime();
        $formattedDateTime = $currentDateTime->format(('Y-m-d\TH:i:sP'));

        $payload = [
            'collection'        => getenv("MONGO_INSTALLATION_LOG_COLLECTION"),
            'data'              => [
                'identifier'        => strval($params['sales_installation_id']),
                'identifier_type'   => 'sales_installation_id',
                'type'              => 'installation',
                'log'               => [
                    'installation_order_no'     => ($params['installation_order_no'] ? $params['installation_order_no'] : ''),
                    'installation_invoice_id'   => ($params['installation_invoice_id'] ? intval($params['installation_invoice_id']) : 0), 
                    'actions'                   => 'update installation status to ' . $params['value'],
                    'affected_field'            => 'sales_installation.status',
                    'value'                     => $params['value'],
                    'flag'                      => 'store_dashboard',
                ],
                'admin_user_id' => 1,
                'timestamp' => $formattedDateTime,
            ]
        ];

        $apiWrapper->setParam($payload);

        if($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();

            \Helpers\LogHelper::log("installation_log", json_encode($apiWrapper->getResponse()));
        }
        
        return true;
    }
}