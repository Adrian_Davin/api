<?php

namespace Library;

use Helpers\InvoiceHelper;
use Monolog\Registry,
    Phalcon\Mvc\Model\Transaction\Manager as TxManager,
    Phalcon\Mvc\Model\Transaction\Failed as TxFailed,
    Models\SalesInvoice,
    Phalcon\Db,
    Library\Nsq;

class SalesShipment
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @var \Models\SalesShipment
     */
    protected $salesShipment;

    /**
     * @return mixed
     */
    public function getSalesShipment()
    {
        return $this->salesShipment;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {
    }

    public function getAllShipment($params = array())
    {
        $salesShipmentModel = new \Models\SalesShipment();
        $resultSalesShipment = $salesShipmentModel::query();

        foreach ($params as $key => $val) {
            if ($key == "shipment_id") {
                $resultSalesShipment->andWhere("shipment_id = " . $val);
            }

            if ($key == "invoice_id") {
                $resultSalesShipment->andWhere("invoice_id = " . $val);
            }

            if ($key == "carrier_id") {
                $carrierIdPart = explode(',', $val);
                $valIn = '';
                foreach ($carrierIdPart as $carrierIDValue) {
                    $valIn .= "$carrierIDValue,";
                }

                $valIn = rtrim($valIn, ',');
                $resultSalesShipment->andWhere("carrier_id in( " . $valIn . ")");
            }

            if ($key == "shipment_status") {
                $shipmentStatusPart = explode(',', $val);
                $valIn = '';
                foreach ($shipmentStatusPart as $statusValue) {
                    $valIn .= "'$statusValue',";
                }

                $valIn = rtrim($valIn, ',');
                $resultSalesShipment->andWhere("shipment_status in( " . $valIn . ")");
            }

            if ($key == "shipment_type") {
                $shipmentTypePart = explode(',', $val);
                $valIn = '';
                foreach ($shipmentTypePart as $typeValue) {
                    $valIn .= "'$typeValue',";
                }
                $valIn = rtrim($valIn, ',');
                $resultSalesShipment->andWhere("shipment_type in( " . $valIn . ")");
            }
        }

        if (!empty($params["order_by"])) {
            $resultSalesShipment->orderBy($params["order_by"]);
        } else {
            $resultSalesShipment->orderBy("created_at DESC");
        }

        $result = $resultSalesShipment->execute();

        if (empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your shipments";
            return array();
        } else {
            $i = 0;
            foreach ($result as $salesShipment) {
                $salesShipmentArray[$i] = $salesShipment->toArray([], true);
                unset($salesShipmentArray[$i]['carrier_id']);

                $salesShipmentArray[$i]['carrier'] = array();
                if ($salesShipment->ShippingCarrier) {
                    $salesShipmentArray[$i]['carrier'] = $salesShipment->ShippingCarrier->toArray();
                }

                if ($salesShipment->ShipmentTracking) {
                    $salesShipmentArray[$i]['shipment_tracking'] = $salesShipment->ShipmentTracking->toArray();
                }
                $i++;
            }

            return $salesShipmentArray;
        }
    }

    public function getSalesShipmentData($shipment_id = "")
    {
        $allShipment = $this->getAllShipment(["shipment_id" => $shipment_id]);
        return $allShipment[0];
    }

    public function prepareSalesShipment($params = array())
    {
        if (array_key_exists('invoice_no', $params)) {
            $salesInvoiceModel = new \Models\SalesInvoice();
            $salesInvoiceResult = $salesInvoiceModel->findFirst("invoice_no = '" . $params['invoice_no'] . "'");
            if ($salesInvoiceResult) {
                $shipmentId = $salesInvoiceResult->salesShipment->getShipmentId();
                if ($params['shipment_id'] != $shipmentId) {
                    $this->errorCode = "RR100";
                    $this->errorMessages = "Shipment not found";
                    return null;
                }
            }
        }

        $salesShipmentModel = new \Models\SalesShipment();
        if (array_key_exists('shipment_id', $params)) {
            $params['invoice_id'] = '';

            // get shipment data by shipment id
            $salesShipmentArray = array();
            $salesShipmentResult = $salesShipmentModel->findFirst("shipment_id = '" . $params['shipment_id'] . "'");
            if ($salesShipmentResult) {
                $salesShipmentArray = $salesShipmentModel->findFirst("shipment_id = '" . $params['shipment_id'] . "'")->toArray();
            }

            if (count($salesShipmentArray) > 0) {
                foreach ($salesShipmentArray as $key => $value) {
                    if ($key == 'shipment_id') {
                        $params['invoice_id'] = $salesShipmentArray['invoice_id'];
                        break;
                    }
                }
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Shipment not found";
                return null;
            }

            date_default_timezone_set("Asia/Jakarta");
            if (isset($params['shipment_status']) && $params['shipment_status'] == 'shipped' && !isset($params['shipped_date'])) {
                $params['shipped_date'] = date("Y-m-d H:i:s");
            } elseif (isset($params['shipment_status']) && $params['shipment_status'] == 'received' && !isset($params['delivered_date'])) {
                $params['delivered_date'] = date("Y-m-d H:i:s");
            }

            $salesShipmentModel->setFromArray($params);
            $this->salesShipment = $salesShipmentModel;
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "Shipment id is empty";
            return null;
        }
    }

    public function updateData($params = array())
    {
        // for order vendor purpose        
        $orderNo = "";
        $deliveryMethod = "";
        try {
            $salesShipmentModel = new \Models\SalesShipment();
            $salesShipmentResult = $salesShipmentModel->findFirst("shipment_id = " . $params['shipment_id']);
            if ($salesShipmentResult) {
                $salesShipmentArray = $salesShipmentResult->toArray();
                $currentShipmentStatus = '';
                if (count($salesShipmentArray) > 0) {
                    $currentShipmentStatus = $salesShipmentArray['shipment_status'];
                }

                if (isset($params['shipment_status'])) {
                    if ($currentShipmentStatus == 'received') {
                        return;
                    } elseif ($params['shipment_status'] == 'picked' && $currentShipmentStatus == 'picked') {
                        return;
                    } elseif ($params['shipment_status'] == 'shipped' && $currentShipmentStatus == 'shipped') {
                        return;
                    }
                }

                $invoiceID = $salesShipmentArray['invoice_id'];
            }

            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->salesShipment->useWriteConnection();
            $manager->setDbService($this->salesShipment->getConnection());
            $transaction = $manager->get();
            $this->salesShipment->setTransaction($transaction);
            $this->salesShipment->saveData('sales_shipment');

            // Update shipment tracking for pending_received
            if (isset($params['shipment_status']) && $params['shipment_status'] == "pending_received") {
                $currShipmentTracking = $this->getShipmentTracking($params['shipment_id']);
                if (count($currShipmentTracking) > 0) {
                    $currShipmentTracking = $currShipmentTracking[0];
                }

                $newShipmentTrackingStatus = [
                    "info" => [
                        "courier_name" => "Ruparupa",
                        "delivery_date" => "-",
                        "status" => "pending_received",
                        "no_awb" => "-",
                        "shipping_to" => "-"
                    ],
                    "manifest" => [
                        [
                            "courier_name" => "",
                            "mfcnote_no" => "-",
                            "manifest_date" => date('d-M-Y H:i:s'),
                            "city_name" => "",
                            "keterangan" => "pending_received"
                        ]
                    ]
                ];

                $newShipmentTracking = [
                    "shipment_id" => $params['shipment_id'],
                    "remark" => "tracking_awb",
                    "updated_by" => "customer"
                ];

                $isDuplicateStatus = false;
                if (!empty($currShipmentTracking) && (int) $currShipmentTracking['shipment_tracking_id'] > 0) {
                    $currShipmentTrackingStatus = json_decode($currShipmentTracking['status'], true);
                    if (count($currShipmentTrackingStatus['manifest']) > 0) {
                        $lastIndex = count($currShipmentTrackingStatus['manifest']) - 1;
                        $lastStatus = $currShipmentTrackingStatus['manifest'][$lastIndex]['keterangan'];

                        if ($lastStatus == "pending_received") {
                            $isDuplicateStatus = true;
                        }
                    }
                    
                    if (!$isDuplicateStatus) {
                        $currShipmentTrackingStatus['manifest'][] = $newShipmentTrackingStatus['manifest'][0];
                        $newShipmentTrackingStatus['manifest'] = $currShipmentTrackingStatus['manifest'];
                        $newShipmentTracking['shipment_tracking_id'] = $currShipmentTracking['shipment_tracking_id'];
                    }
                }

                if (!$isDuplicateStatus) {
                    $newShipmentTracking['status'] = json_encode($newShipmentTrackingStatus);
                    $shipmentTrackingModel = new \Models\SalesShipmentTracking;
                    $shipmentTrackingModel->setTransaction($transaction);
                    $shipmentTrackingModel->setFromArray($newShipmentTracking);
                    $shipmentTrackingModel->saveData("sales_shipment_tracking");
                }
            }

            $getInvoiceData = $this->getInvoiceDataByInvoiceId($this->salesShipment->getInvoiceId());
            $salesOrderId = $getInvoiceData['sales_order_id'];
            $invoiceStatus = $getInvoiceData['invoice_status'];
            $storeCode = $getInvoiceData['store_code'];
            $deliveryMethod = $getInvoiceData['delivery_method'];
            $invoiceNo = $getInvoiceData['invoice_no'];

            //for order vendor purpose
            $salesOrderModel = new \Models\SalesOrder();
            $salesOrderResult = $salesOrderModel->findFirst("sales_order_id = " . $salesOrderId);
            $orderNo = $salesOrderResult->getOrderNo();

            $salesInvoice = new \Models\SalesInvoice();
            // dont update invoice status if refund status found
            $forbiddenStatus = array('partial_refund', 'full_refund');
            $allowedStatus = array('processing', 'stand_by', 'ready_to_pickup', 'ready_to_ship', 'picked', 'shipped', 'received', 'canceled');
            $updatedInvoiceStatus = '';
            if (isset($params['shipment_status']) && !in_array($invoiceStatus, $forbiddenStatus)) {
                if (in_array($this->salesShipment->getShipmentStatus(), $allowedStatus)) {
                    try {
                        // Update invoice with shipment status
                        $updateDataInvoice = [
                            "invoice_id" => $this->salesShipment->getInvoiceId(),
                            "invoice_status" => $this->salesShipment->getShipmentStatus()
                        ];
                        $salesInvoice->setFromArrayUpdate($updateDataInvoice);
                        $salesInvoice->setTransaction($transaction);
                        $salesInvoice->saveData('sales_invoice');

                        // for log purpose
                        $updatedInvoiceStatus = $this->salesShipment->getShipmentStatus();
                    } catch (\Exception $e) {
                        $transaction->rollback(
                            "updateInvoice : " . $e->getMessage()
                        );
                    }
                }
            }

            $transaction->commit();

            // if shipment status and invoice status is received then insert to review_product_online_offline table
            // when invoice received, track event mission based
            if (isset($params['shipment_status']) && $params['shipment_status'] == "received") {
                $this->insertInvoiceReview($invoiceNo);
                $this->missionEventTrack($invoiceNo);
            }

            // if shipment status to shipped need to check for send_cart_greeting
            if (isset($params['shipment_status']) && $params['shipment_status'] == "shipped") {
                $orderDevice = $salesOrderResult->getDevice();
                if ($orderDevice != "vendor"){
                    $this->sendCartGiftGreeting($salesOrderResult->getCartId());
                }
            }

            if ((substr($orderNo, 0, 4) == "ODIT" || substr($orderNo, 0, 4) == "ODIS" || substr($orderNo, 0, 4) == "ODIK")
                && $params['shipment_status'] == "shipped"
                && strpos($storeCode, "DC") === false
                ){

                $hasCreateJournalPickup = false;

                $salesLogModel = new \Models\SalesLog();
                $salesLogs = $salesLogModel->find(sprintf("invoice_id = %d AND affected_field IN ('invoice_status','sales_invoice.invoice_status','shipment_status','sales_shipment.shipment_status')",$this->salesShipment->getInvoiceId()));
                foreach ($salesLogs as $salesLog) {
                    if ($salesLog->getValue() == "ready_to_pickup"){
                        $hasCreateJournalPickup = true;
                    }
                }

                if (!$hasCreateJournalPickup){

                    $paramsJournalPickup = [
                        "is_hold"=>false,
                        "invoice_no"=>$invoiceNo,
                        "vorgdatum"=>date("Ymd"),
                    ];

                    $this->CreateJournalPickupStore($paramsJournalPickup);
                }
            }
            
        } catch (\Exception $e) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Update sales shipment sequence failed, please contact ruparupa tech support";
        }

        // update trigger by customer, make the log
        if (empty($this->errorCode) && (isset($params['caller']) && isset($params['invoice_no']) && isset($params['shipment_status']))) {
            if ($params['caller'] == 'frontend') {
                $salesInvoiceResult = $salesInvoice->findFirst("invoice_no = '{$params['invoice_no']}'");
                if ($salesInvoiceResult) {
                    $salesInvoiceArray = $salesInvoiceResult->toArray();
                    $paramLog['sales_order_id'] = $salesInvoiceArray['sales_order_id'];
                    $paramLog['invoice_id'] = $salesInvoiceArray['invoice_id'];
                    $paramLog['shipment_id'] = $params['shipment_id'];
                    $paramLog['admin_user_id'] = substr($storeCode, 0, 1) == "M" ? 0 : 2;
                    $paramLog['supplier_user_id'] = substr($storeCode, 0, 1) == "M" ? 411 : 0;
                    $paramLog['actions'] = "update shipment status #{$salesInvoiceArray['invoice_no']} to {$params['shipment_status']}";
                    $paramLog['affected_field'] = "shipment_status";
                    $paramLog['value'] = $params['shipment_status'];
                    $paramLog['log_time'] = date("Y-m-d H:i:s");
                    $paramLog['flag'] = substr( $salesInvoiceArray['store_code'], 0, 2 ) === "DC" ? "dc_dashboard" : "store_dashboard";
                    if (substr($storeCode, 0, 1) == "M") {
                        $paramLog['flag'] = "seller_dashboard";
                    } 
                    $salesLogModel = new \Models\SalesLog();
                    $salesLogModel->setFromArray($paramLog);
                    $salesLogModel->saveData("sales_log");

                    if (!empty($updatedInvoiceStatus)) {
                        $paramLog['actions'] = "update invoice status #{$salesInvoiceArray['invoice_no']} to {$updatedInvoiceStatus}";
                        $paramLog['affected_field'] = "invoice_status";
                        $paramLog['value'] = $updatedInvoiceStatus;

                        $salesLogModel = new \Models\SalesLog();
                        $salesLogModel->setFromArray($paramLog);
                        $salesLogModel->saveData("sales_log");
                    }
                }
            }
        } else if (isset($params['shipment_status']) && !empty($params['shipment_status']) && isset($invoiceID)) {
            $salesInvoiceResult = $salesInvoice->findFirst("invoice_id = $invoiceID");
            if ($salesInvoiceResult) {
                $salesInvoiceArray = $salesInvoiceResult->toArray();
                $paramLog['sales_order_id'] = $salesInvoiceArray['sales_order_id'];
                $paramLog['invoice_id'] = $salesInvoiceArray['invoice_id'];
                $paramLog['shipment_id'] = $params['shipment_id'];
                $paramLog['admin_user_id'] =  !empty($params['admin_user_id']) ? $params['admin_user_id'] : (substr($storeCode, 0, 1) == "M" ? 0 : 1);
                $paramLog['supplier_user_id'] = substr($storeCode, 0, 1) == "M" ? 103 : 0;
                $paramLog['actions'] =  !empty($params['request_by']) ? "* update shipment status #{$salesInvoiceArray['invoice_no']} to {$params['shipment_status']}, request by: {$params['request_by']}" : "* update shipment status #{$salesInvoiceArray['invoice_no']} to {$params['shipment_status']}";
                $paramLog['affected_field'] = "shipment_status";
                $paramLog['value'] = $params['shipment_status'];
                $paramLog['log_time'] = date("Y-m-d H:i:s");
                $paramLog['flag'] = substr( $salesInvoiceArray['store_code'], 0, 2 ) === "DC" ? "dc_dashboard" : "store_dashboard";
                if (substr($storeCode, 0, 1) == "M") {
                    $paramLog['flag'] = "seller_dashboard";
                }

                $salesLogModel = new \Models\SalesLog();
                $salesLogModel->setFromArray($paramLog);
                $salesLogModel->saveData("sales_log");
                
                if (!empty($params['track_number'])) {
                    $paramLog['actions'] = "update track_number #{$salesInvoiceArray['invoice_no']} to {$params['track_number']}, request by: {$params['request_by']}";
                    $paramLog['affected_field'] = "track_number";
                    $paramLog['value'] = $params['track_number'];
                    $salesLogModel = new \Models\SalesLog();
                    $salesLogModel->setFromArray($paramLog);                            
                    $salesLogModel->saveData("sales_log");
                }

                if (!empty($params['carrier_id'])) {
                    $paramLog['actions'] = "update carrier_id #{$salesInvoiceArray['invoice_no']} to {$params['carrier_id']}, request by: {$params['request_by']}";
                    $paramLog['affected_field'] = "carrier_id";
                    $paramLog['value'] = $params['carrier_id'];
                    $salesLogModel = new \Models\SalesLog();
                    $salesLogModel->setFromArray($paramLog);                            
                    $salesLogModel->saveData("sales_log");
                }

                if (!empty($updatedInvoiceStatus)) {
                    $paramLog['actions'] =  !empty($params['request_by']) ? "update invoice status #{$salesInvoiceArray['invoice_no']} to {$updatedInvoiceStatus}, request by: {$params['request_by']}" : "update shipment status #{$updatedInvoiceStatus} to {$params['shipment_status']}";
                    $paramLog['affected_field'] = "invoice_status";
                    $paramLog['value'] = $updatedInvoiceStatus;
                    $salesLogModel = new \Models\SalesLog();
                    $salesLogModel->setFromArray($paramLog);
                    $salesLogModel->saveData("sales_log");
                }
            }
        } else if (isset($params['carrier_id']) && !empty($params['carrier_id']) && isset($invoiceID)) {
            $salesInvoiceResult = $salesInvoice->findFirst("invoice_id = $invoiceID");
            if ($salesInvoiceResult) {
                $salesInvoiceArray = $salesInvoiceResult->toArray();
                $paramLog['sales_order_id'] = $salesInvoiceArray['sales_order_id'];
                $paramLog['invoice_id'] = $salesInvoiceArray['invoice_id'];
                $paramLog['shipment_id'] = $params['shipment_id'];
                $paramLog['admin_user_id'] = substr($storeCode, 0, 1) == "M" ? 0 : 1;
                $paramLog['supplier_user_id'] = substr($storeCode, 0, 1) == "M" ? 103 : 0;
                $paramLog['actions'] = "* update carrier ID #{$salesInvoiceArray['invoice_no']} to {$params['carrier_id']}";
                $paramLog['affected_field'] = "carrier_id";
                $paramLog['value'] = $params['carrier_id'];
                $paramLog['log_time'] = date("Y-m-d H:i:s");
                $paramLog['flag'] = substr( $salesInvoiceArray['store_code'], 0, 2 ) === "DC" ? "dc_dashboard" : "store_dashboard";
                if (substr($storeCode, 0, 1) == "M") {
                    $paramLog['flag'] = "seller_dashboard";
                }
                $salesLogModel = new \Models\SalesLog();
                $salesLogModel->setFromArray($paramLog);
                $salesLogModel->saveData("sales_log");
            }
        }

        if (empty($this->errorCode) && isset($params['shipment_status'])) {
            $salesInvoiceOrder = new \Models\SalesInvoice();
            if ($this->salesShipment->getShipmentStatus() == 'received') {
                // search, is there any invoice that have status not received
                $resultInvoice = $salesInvoiceOrder::query();
                $resultInvoice->andWhere("sales_order_id = " . $salesOrderId);
                $resultInvoice->andWhere("invoice_status <> 'received'");
                $result = $resultInvoice->execute();
                if (empty($result->count())) {
                    // Update order to complete
                    $salesOrder = new \Models\SalesOrder();
                    $updateDataOrder = [
                        "sales_order_id" => $salesOrderId,
                        "status" => "complete"
                    ];
                    $salesOrder->setFromArray($updateDataOrder);
                    $salesOrder->saveData('sales_order');

                    // Trigger event order_frequency for mission based purpose
                    $salesOrderResult = $salesOrderModel->findFirst("sales_order_id = " . $salesInvoiceArray['sales_order_id']);
                    $orderNo = $salesOrderResult->getOrderNo();
                    $this->missionEventTrackOrder($orderNo);
                }

                /* 
                    Update stock to complete only for DC and Marketplace.
                    for store, stock released when status set to stand_by
                */
                if (substr( $storeCode, 0, 2 ) === "DC" || $storeCode[0] == "M") {
                    $params['invoice_id'] = $this->salesShipment->getInvoiceId();
                    $this->updateStockStatus($params);
                }
            } elseif ($this->salesShipment->getShipmentStatus() == 'canceled') {
                // search, is there any invoice that have status not canceled
                $resultInvoice = $salesInvoiceOrder::query();
                $resultInvoice->andWhere("sales_order_id = " . $salesOrderId);
                $resultInvoice->andWhere("invoice_status <> 'canceled'");
                $result = $resultInvoice->execute();
                if (empty($result->count())) {
                    // Update order to canceled
                    $salesOrder = new \Models\SalesOrder();
                    $updateDataOrder = [
                        "sales_order_id" => $salesOrderId,
                        "status" => "canceled"
                    ];
                    $salesOrder->setFromArray($updateDataOrder);
                    $salesOrder->saveData('sales_order');
                }
            }

            $sendEmail = false;
            $emailStatus = array('picked', 'shipped', 'pending_received', 'received');
            if (in_array($this->salesShipment->getShipmentStatus(), $emailStatus)) {
                if (isset($params['send_email'])) {
                    if ($params['send_email'] == 'yes') {
                        $sendEmail = true;
                    }
                } else {
                    $sendEmail = true;
                }
            } else if (isset($params['send_email'])) {
                if ($params['send_email'] == 'yes') {
                    $sendEmail = true;
                }
            }

            if ($sendEmail) {
                // Send the email if update data is success
                $paramsSendEmail['invoice_id'] = $this->salesShipment->getInvoiceId();
                $paramsSendEmail['shipment_id'] = $params['shipment_id'];
                if ($this->salesShipment->getShipmentStatus() == 'picked') {
                    if ($salesShipmentArray['carrier_id'] == getenv("GOSEND_SAMEDAY_CARRIER_ID") || $salesShipmentArray['carrier_id'] == getenv("GOSEND_INSTANT_CARRIER_ID")) {
                        $paramsSendEmail['shipment_status'] = 'shipped';
                        $this->sendEmail($paramsSendEmail);
                    }
                } elseif ($this->salesShipment->getShipmentStatus() == 'shipped') {
                    if ($salesShipmentArray['carrier_id'] != getenv("GOSEND_SAMEDAY_CARRIER_ID") && $salesShipmentArray['carrier_id'] != getenv("GOSEND_INSTANT_CARRIER_ID")) {
                        $paramsSendEmail['shipment_status'] = 'shipped';
                        $this->sendEmail($paramsSendEmail);
                    }
                } elseif ($this->salesShipment->getShipmentStatus() == 'received') {
                    $paramsSendEmail['shipment_status'] = 'received';
                    $this->sendEmail($paramsSendEmail);
                    if (isset($params['invoice_no'])) {
                        $nsq = new \Library\Nsq();
                        $message = [
                            "data" => array(
                                "invoice_no" => $params['invoice_no'],
                            ),
                            "message" => "sendEmailFreeInstallation"
                        ];
                        $nsq->publishCluster('afterSales', $message);
                    }
                } elseif ($this->salesShipment->getShipmentStatus() == 'pending_received' && substr($storeCode, 0, 1) == "M") {
                    // Send email to admin sellercenter
                    $paramsSendEmail['shipment_status'] = 'pending_received';
                    $this->sendEmail($paramsSendEmail);
                }
            }

            $needToUpdateTokopedia = array('picked', 'shipped' ,'received');
            $needToUpdateTiktok = array('shipped' ,'received');
            // tokopedia order, with ownfleet delivery and status shipment to picked or received
            // request to order vendor to update order status tokopedia
            if (substr($orderNo, 0, 4) == "ODIT" && $deliveryMethod == "ownfleet" && in_array($params['shipment_status'], $needToUpdateTokopedia)) {
                $this->UpdateShipmentStatusTokopedia($params['shipment_id'], $params['shipment_status']);
            } else if (substr($orderNo, 0, 4) == "ODIK" && $deliveryMethod == "ownfleet" && in_array($params['shipment_status'], $needToUpdateTiktok)){
                $this->updateOwnfleetTiktok($orderNo, $params['shipment_status']);
            }
        }
    }

    private function getInvoiceDataByInvoiceId($invoice_id)
    {
        $salesInvoice = new \Models\SalesInvoice();

        $salesInvoiceArray = array();
        $salesInvoiceResult = $salesInvoice->findFirst("invoice_id = " . $invoice_id);
        if ($salesInvoiceResult) {
            $salesInvoiceArray = $salesInvoiceResult->toArray();
        }
        $params = array();
        if (count($salesInvoiceArray) > 0) {
            $params['sales_order_id'] = $salesInvoiceArray['sales_order_id'];
            $params['invoice_status'] = $salesInvoiceArray['invoice_status'];
            $params['store_code'] = $salesInvoiceArray['store_code'];
            $params['delivery_method'] = $salesInvoiceArray['delivery_method'];
            $params['invoice_no'] = $salesInvoiceArray['invoice_no'];
        }

        return $params;
    }

    // Shipment Tracking
    public function getAllShipmentTracking($params = array())
    {
        $salesShipmentTrackingModel = new \Models\SalesShipmentTracking();
        $resultSalesShipmentTracking = $salesShipmentTrackingModel::query();

        foreach ($params as $key => $val) {
            if ($key == "shipment_tracking_id") {
                $resultSalesShipmentTracking->andWhere("shipment_tracking_id = " . $val);
            }

            if ($key == "shipment_id") {
                $resultSalesShipmentTracking->andWhere("shipment_id = " . $val);
            }

            if ($key == "remark") {
                $resultSalesShipmentTracking->andWhere("remark like '%" . $val . "%'");
            }
        }

        if (!empty($params["order_by"])) {
            $resultSalesShipmentTracking->orderBy($params["order_by"]);
        } else {
            $resultSalesShipmentTracking->orderBy("updated_at DESC");
        }

        $result = $resultSalesShipmentTracking->execute();

        if (empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your shipments tracking";
            return array();
        } else {
            $allShipmentTracking = $result->toArray();
        }
        return $allShipmentTracking;
    }

    public function getSalesShipmentTrackingData($shipment_tracking_id = "")
    {
        $allShipmentTracking = $this->getAllShipmentTracking(["shipment_tracking_id" => $shipment_tracking_id]);
        return $allShipmentTracking[0];
    }

    public function roleBackShipmentStatus($params)
    {
        $salesShipmentModel = new \Models\SalesShipment();
        $salesShipmentArray = array();
        $salesShipmentResult = $salesShipmentModel->findFirst("shipment_id = '" . $params['shipment_id'] . "'");
        if ($salesShipmentResult) {
            $salesShipmentArray = $salesShipmentModel->findFirst("shipment_id = '" . $params['shipment_id'] . "'")->toArray();
        }

        if (strtolower($salesShipmentArray['shipment_status']) == 'delivered') {
            $params['shipment_status'] = "shipped";
        } elseif (strtolower($salesShipmentArray['shipment_status']) == 'pending_delivered') {
            $params['shipment_status'] = "shipped";
        } elseif (strtolower($salesShipmentArray['shipment_status']) == 'shipped') {
            $params['shipment_status'] = "new";

            //update invoice status
            $salesInvoice = new \Models\SalesInvoice();
            $salesInvoice->findFirst("invoice_id = '" . $salesShipmentArray['invoice_id'] . "'");
            if ($salesInvoice) {
                $salesInvoiceArray = $salesInvoice->findFirst("invoice_id = '" . $salesShipmentArray['invoice_id'] . "'")->toArray();
            }

            $invoiceParams = $salesInvoiceArray;
            $invoiceParams['sales_order_id'] = $salesInvoiceArray['sales_order_id'];
            $invoiceParams['invoice_status'] = "new";
            $salesInvoice->setFromArray($invoiceParams);
            $salesInvoice->saveData('sales_invoice');
        }

        if (!empty($params['shipment_status'])) {
            $salesShipmentModel->setFromArray($params);
            $salesShipmentModel->saveData('sales_shipment');
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages = "shipment status is empty";
        }
    }

    //<editor-fold desc="Batch Check AWB">
    /**
     * Check is shipment tracking data exist searching by shipment id and shipment status
     *
     * @param $shipmentId int needed for searching criteria
     * @param $status int needed for searching criteria
     * @return array Return array from the query
     */
    private function getShipmentTracking($shipmentId = FALSE, $status = FALSE)
    {
        $shipmentTrackingArray = array();
        $shipmentTracking = new \Models\SalesShipmentTracking();
        if ($shipmentId && $status) {
            $shipmentTrackingArray = $shipmentTracking->find("shipment_id = '$shipmentId' and status = '$status' and remark = 'tracking_awb'")->toArray();
        } elseif ($shipmentId) {
            $shipmentTrackingArray = $shipmentTracking->find("shipment_id = '$shipmentId' and remark = 'tracking_awb'")->toArray();
        }

        return $shipmentTrackingArray;
    }

    /**
     * This function is to save Shipment Tracking data
     *
     * @param array $params Parameter that needed for the save process
     */
    private function saveRowShipmentTrackingData($params = array())
    {
        if (count($params) > 0) {
            $salesShipmentTracking = new \Models\SalesShipmentTracking();
            $additionalInfo = (isset($params['additional_info'])) ? $params['additional_info'] : '';
            $insertShipmentTracking = [
                "shipment_id" => $params['shipment_id'],
                "status" => $params['status'],
                "additional_info" => $additionalInfo,
                "remark" => "tracking_awb",
                "updated_by" => "batchprocessor"

            ];

            // update mode
            if (isset($params['shipment_tracking_id'])) {
                $insertShipmentTracking['shipment_tracking_id'] = $params['shipment_tracking_id'];
            }

            $salesShipmentTracking->setFromArray($insertShipmentTracking);
            $salesShipmentTracking->saveData('sales_shipment_tracking');
        }
    }

    /**
     * Function to Check AWB status, act as a conductor to organized the update status and send email
     *
     * @param array $params Parameter from Api parameter MUST contain shipment_id, track_number and carrier_id
     * @return string
     */
    public function checkAwbBatch($params = array())
    {
        try {
            if (count($params) > 0) {
                $resultShipmentStatusChanged = 0;
                $jneGroupCarrierID = array(getenv("JNE_CARRIER_ID"), getenv("JNEJOB_CARRIER_ID"), getenv("JTR_CARRIER_ID"));
                foreach ($params as $row) {
                    if ($row['carrier_id'] == $_ENV['SAP_CARRIER_ID']) {
                        $checkAwb = $this->checkAwbSapExpress($row['shipment_id'], $row['track_number']);
                        $resultShipmentStatusChanged = $this->processCheckAwb($row['shipment_id'], $checkAwb, $_ENV['SAP_CARRIER_ID']);
                    } elseif (in_array($row['carrier_id'], $jneGroupCarrierID)) {
                        $checkAwb = $this->checkAwbJne($row['shipment_id'], $row['track_number'], $row['carrier_id']);
                        $resultShipmentStatusChanged = $this->processCheckAwb($row['shipment_id'], $checkAwb, $row['carrier_id']);
                    } else if ($row['carrier_id'] == getenv("GOSEND_SAMEDAY_CARRIER_ID") || $row['carrier_id'] == getenv("GOSEND_INSTANT_CARRIER_ID")) {
                        $checkAwb = $this->checkAwbGoSend($row['shipment_id'], $row['manifest']);
                        $resultShipmentStatusChanged = $this->processCheckAwb($row['shipment_id'], $checkAwb, $row['carrier_id']);
                    } else if ($row['carrier_id'] == getenv("NCS_CARRIER_ID")) {
                        $checkAwb = $this->checkAwbNcs($row['shipment_id'], $row['manifest']);
                        $resultShipmentStatusChanged = $this->processCheckAwb($row['shipment_id'], $checkAwb, $row['carrier_id']);
                    } else if ($row['carrier_id'] == getenv("QRIM_CARRIER_ID")) {
                        $checkAwb = $this->checkAwbQrim($row['shipment_id'], $row['manifest']);
                        $resultShipmentStatusChanged = $this->processCheckAwb($row['shipment_id'], $checkAwb, $row['carrier_id']);
                    } else {
                        continue;
                    }
                }

                return "$resultShipmentStatusChanged Shipment Status has changed";
            } else {
                return 'No data found';
            }
        } catch (\Exception $e) {
            return "Check Awb Process failed";
        }
    }

    /**
     * Function to update AWB status data source from oms/ninjavan webhooks
     *
     * @param array $params Parameter from Api parameter MUST contain tracking_id, status, comments & pod_name (can empty), timestamp
     * @return string
     */
    public function ninjaWebhooks($params = array())
    {
        try {
            if (count($params) > 0) {
                $resultShipmentStatusChanged = 0;

                $invoiceModel = new SalesInvoice();
                $sql = "SELECT coalesce(shipment_id,'') as shipment_id, coalesce(shipping_address_id,'') as shipping_address_id FROM sales_shipment where track_number = '" . $params['tracking_id'] . "'";
                $invoiceModel->useReadOnlyConnection();
                $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
                $result->setFetchMode(Db::FETCH_ASSOC);
                $dataList = $result->fetchAll();
                $shipmentID = $dataList[0]['shipment_id'];
                $shippingAddressID = $dataList[0]['shipping_address_id'];
                if (empty($shipmentID)) {
                    return 'No data found';
                }

                $formatData = $this->ninjaAwbFormatData($params, $shipmentID, $shippingAddressID);
                if (count($formatData) > 0) {
                    $resultShipmentStatusChanged = $this->processCheckAwb($shipmentID, $formatData, $_ENV['NINJAVAN_CARRIER_ID']);
                }

                return "$resultShipmentStatusChanged Shipment Status has changed";
            } else {
                return 'No data found';
            }
        } catch (\Exception $e) {
            return "Update Awb status Process failed";
        }
    }

    public function ninjaAwbFormatData($params = array(), $shipmentID = FALSE, $shippingAddressID = FALSE)
    {
        if (empty($params['pod_name'])) {
            $params['pod_name'] = null;
        }

        // last status means latest status
        $data['last_status'] = $params['status'];

        $invoiceModel = new SalesInvoice();
        $customerName = '-';
        if (!empty($shippingAddressID)) {
            $sql = "select concat(first_name, ' ', last_name) as customer_name from sales_order_address where sales_order_address_id = $shippingAddressID";
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(Db::FETCH_ASSOC);
            $dataList = $result->fetchAll();
            if (isset($dataList[0]['customer_name'])) {
                $customerName = $dataList[0]['customer_name'];
            }
        }

        $sql = "select status from sales_shipment_tracking where shipment_id = $shipmentID and remark = 'tracking_awb' limit 1";
        $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
        $result->setFetchMode(Db::FETCH_ASSOC);
        $dataList = $result->fetchAll();
        if (isset($dataList[0]['status'])) {
            // merge with old data
            $statusTrackingArray = json_decode($dataList[0]['status'], true);
            $paramsManifest = $statusTrackingArray['manifest'];

            // handling update tracking with same shipment status
            foreach ($statusTrackingArray['manifest'] as $row) {
                if ($row['cn_status'] == $params['status']) {
                    return array();
                }
            }

            $countManifest = count($statusTrackingArray['manifest']);
            $runningNumber = $countManifest + 1;
            $paramsManifest[$countManifest]['no'] = $runningNumber;
            $paramsManifest[$countManifest]['cn_no'] = $params['tracking_id'];
            $paramsManifest[$countManifest]['cn_status'] = $params['status'];
            $paramsManifest[$countManifest]['cn_date'] = $params['timestamp'];
            $paramsManifest[$countManifest]['cn_desc'] = $params['comments'];
            $paramsManifest[$countManifest]['cn_receiveby'] = $params['pod_name'];
        } else {
            // new data
            $paramsManifest[0]['no'] = 1;
            $paramsManifest[0]['cn_no'] = $params['tracking_id'];
            $paramsManifest[0]['cn_status'] = $params['status'];
            $paramsManifest[0]['cn_date'] = $params['timestamp'];
            $paramsManifest[0]['cn_desc'] = $params['comments'];
            $paramsManifest[0]['cn_receiveby'] = $params['pod_name'];
        }

        $data['manifest'] = array_merge(array('info' => array(
            'courier_name' => 'Ninja Van', 'delivery_date' => $params['timestamp'], 'status' => $params['status'],
            'no_awb' => $params['tracking_id'], 'shipping_to' => $customerName
        )), array('manifest' => $paramsManifest));

        return $data;
    }

    public function processCheckAwb($shipmentId = FALSE, $checkAwb = array(), $carrierId = FALSE)
    {
        $resultShipmentStatusChanged = 0;
        if (count($checkAwb) > 0) {
            $awbLastStatus = strtolower(stripslashes(str_replace("'", "", $checkAwb['last_status'])));
            if (!empty($awbLastStatus)) {
                // Save the tracking status
                $paramsTracking['shipment_id'] = $shipmentId;
                $paramsTracking['status'] = json_encode($checkAwb['manifest']);
                if (isset($checkAwb['cnote'])) {
                    $paramsTracking['additional_info'] = json_encode($checkAwb['cnote']);
                }

                $paramsTracking['remark'] = 'tracking_awb';
                $paramsTracking['updated_by'] = 'batchprocessor';
                $result = $this->getShipmentTracking($shipmentId);
                if (count($result) > 0) {
                    // update
                    $paramsTracking['shipment_tracking_id'] = $result[0]['shipment_tracking_id'];
                }

                $this->saveRowShipmentTrackingData($paramsTracking);

                $shipmentStatus = $this->filterAwbStatus($awbLastStatus, $carrierId);
                if (!empty($shipmentStatus)) {
                    $paramsUpdateShipment['shipment_id'] = $shipmentId;
                    $paramsUpdateShipment['shipment_status'] = $shipmentStatus;
                    $paramsUpdateShipment['carrier_id'] = $carrierId;
                    $resultShipmentStatusChanged += $this->updateShipmentStatusByAwb($paramsUpdateShipment);

                }
            }
        }

        return $resultShipmentStatusChanged;
    }

    /**
     * Function to check AWB and get the last awb status
     *
     * @param $awb_number
     * @return array
     */
    public function checkAwbSapExpress($shipment_id, $awb_number = FALSE)
    {
        $sapExpressLib = new \Library\Shipping\SAPExpress();
        $checkAwb = array();
        if ($awb_number) {
            $responseArray = $sapExpressLib->checkAWB($awb_number);
            if (isset($responseArray['connotes'])) {
                // get last status
                $checkAwb['last_status'] = $responseArray['laststatus']['status'];

                foreach ($responseArray['connotes'] as $key => $connotes) {
                    $responseArray['connotes'][$key]['courier_name'] = 'SAP Express';
                }

                // get manifest array for insert / update to sales_shipment_tracking table
                $checkAwb['manifest'] = (isset($responseArray['connotes'])) ?
                    array_merge(array('info' => array(
                        'courier_name' => 'SAP Express', 'delivery_date' => $responseArray['cn_date'], 'status' => $responseArray['laststatus']['status'],
                        'no_awb' => $responseArray['cn_no'], 'shipping_to' => $responseArray['cn_consignee_name']
                    )), array('manifest' => $responseArray['connotes'])) : array();

                $shipmentTrackings = $this->getShipmentTracking($shipment_id);
                foreach ($shipmentTrackings as $shipmentTracking) {
                    $status = $shipmentTracking['status'];
                    if (!empty($status)) {
                        $statusData = json_decode($status, true);
                        foreach ($statusData['manifest'] as $key => $manifest) {
                            if ($manifest['courier_name'] == 'SAP Express') {
                                unset($statusData['manifest'][$key]);
                            }
                        }
                    }
                    $checkAwb['manifest']['manifest'] = array_merge($statusData['manifest'], $checkAwb['manifest']['manifest']);
                }
            }
        }

        return $checkAwb;
    }

    /**
     * Function to check AWB and get the last awb status
     *
     * @param $awb_number
     * @return array
     */
    public function checkAwbJne($shipment_id, $awb_number = FALSE, $carrierID)
    {
        $jneLib = new \Library\Shipping\JNE();
        $checkAwb = array();
        if ($awb_number) {
            $responseArray = $jneLib->checkAWB($awb_number);
            $courierName = (int) $carrierID == (int) getenv("JTR_CARRIER_ID") ? "JNE JTR" : "JNE";
            if (isset($responseArray['cnote'])) {
                // get last status
                $checkAwb['last_status'] = $responseArray['cnote']['pod_status'];

                // get manifest array for insert / update to sales_shipment_tracking table
                if (isset($responseArray['history'])) {
                    $dateFormat = 'd-M-Y H:i:s';
                    foreach ($responseArray['history'] as $history) {
                        $manifest[] = array(
                            'courier_name' => $courierName,
                            'mfcnote_no' => $responseArray['cnote']['cnote_no'],
                            'manifest_date' => date($dateFormat, strtotime($history['date'])),
                            'keterangan' => $history['desc']
                        );
                    }
                    $checkAwb['manifest'] = array(
                        'info' => array(
                            'courier_name' => $courierName,
                            'delivery_date' => date($dateFormat, strtotime($responseArray['cnote']['cnote_date'])),
                            'status' => $checkAwb['last_status'],
                            'no_awb' => $responseArray['cnote']['cnote_no'],
                            'shipping_to' => $responseArray['cnote']['cnote_receiver_name']
                        ),
                        'manifest' => $manifest
                    );
                } else {
                    $checkAwb['manifest'] = array(
                        'info' => array(
                            'courier_name' => $courierName,
                            'delivery_date' => '-',
                            'status' => $checkAwb['last_status'],
                            'no_awb' => $awb_number,
                            'shipping_to' => '-'
                        )
                    );
                }

                $checkAwb['cnote'] = $responseArray['cnote'];

                $shipmentTrackings = $this->getShipmentTracking($shipment_id);
                foreach ($shipmentTrackings as $shipmentTracking) {
                    $status = $shipmentTracking['status'];
                    if (!empty($status)) {
                        $statusData = json_decode($status, true);
                        foreach ($statusData['manifest'] as $key => $manifest) {
                            if ($manifest['courier_name'] == $courierName) {
                                unset($statusData['manifest'][$key]);
                            }
                        }
                    }

                    if (!empty($statusData['manifest'])) {
                        $checkAwb['manifest']['manifest'] = array_merge($statusData['manifest'], $checkAwb['manifest']['manifest']);
                    }
                }
            } else if (isset($responseArray['error'])) {
                // Commented until task separate manual awb done
                /*
                $errorMsg = $responseArray['error'];
                if (!empty($errorMsg)) {
                    $params = [
                        "channel" => getenv("TRACKING_AWB_SLACK_CHANNEL"),
                        "username" => getenv("OPS_SLACK_USERNAME"),
                        "text" => "Fail to tracking AWB: {$awb_number} ({$courierName}), Error message from API {$courierName}: {$errorMsg}",
                        "icon_emoji" => ':robot_face:'
                    ];
        
                    // call queue for webhook slack
                    $nsq = new Nsq();
                    $message = [
                        "data" => $params,
                        "message" => "customerAlert"
                    ];
                    $nsq->publishCluster('transaction', $message);
                }
                */
            }
        }

        return $checkAwb;
    }

    /**
     * Function to check AWB and get the last awb status
     *
     * @param $awb_number
     * @return array
     */
    public function checkAwbGoSend($shipment_id, $manifest = array())
    {
        $checkAwb = array();

        if (count($manifest) > 0) {
            $lastIndex = count($manifest['manifest']) - 1;
            $checkAwb['last_status'] = $manifest['manifest'][$lastIndex]['keterangan'];
            $checkAwb['manifest'] = $manifest;

            foreach ($manifest as $key => $connotes) {
                $manifest[$key]['courier_name'] = 'GoSend';
            }

            $shipmentTrackings = $this->getShipmentTracking($shipment_id);
            foreach ($shipmentTrackings as $shipmentTracking) {
                $status = $shipmentTracking['status'];
                if (!empty($status)) {
                    $statusData = json_decode($status, true);
                    foreach ($statusData['manifest'] as $key => $manifest) {
                        if ($manifest['courier_name'] == 'GoSend') {
                            unset($statusData['manifest'][$key]);
                        }
                    }
                }
                $checkAwb['manifest']['manifest'] = array_merge($statusData['manifest'], $checkAwb['manifest']['manifest']);
            }
        }

        return $checkAwb;
    }

    /**
     * Function to check AWB and get the last awb status
     *
     * @param $awb_number
     * @return array
     */
    public function checkAwbNcs($shipment_id, $manifest = array())
    {
        $checkAwb = array();
        if (count($manifest) > 0) {
            $lastIndex = count($manifest) - 1;
            // get last status
            $checkAwb['last_status'] = $manifest[$lastIndex]['Status'];

            $dateOldFormat = 'd/m/Y H:i:s';
            $dateNewFormat = 'd-M-Y H:i:s';

            $formatDate = \DateTime::createFromFormat($dateOldFormat, $manifest[0]['TimeStamp']);
            $delivery_date = $formatDate->format($dateNewFormat);

            $manifestFormat = array();
            for ($i = 0; $i < count($manifest); $i++) {
                $formatDate = \DateTime::createFromFormat($dateOldFormat, $manifest[$i]['TimeStamp']);
                $manifest_date = $formatDate->format($dateNewFormat);

                $manifestFormat[$i]['courier_name'] = 'NCS';
                $manifestFormat[$i]['mfcnote_no'] = $manifest[$i]['AWB'];
                $manifestFormat[$i]['manifest_date'] = $manifest_date;
                $manifestFormat[$i]['city_name'] = $manifest[$i]['Station'];
                $manifestFormat[$i]['keterangan'] = $manifest[$i]['Comment'];
                if ($manifest[$i]['Comment'] == "Success Delivered") {
                    $relationName = '';
                    if ($manifest[$i]['RelationName'] != "YBS (Yang Bersangkutan)") {
                        $relationName = " ({$manifest[$i]['RelationName']})";
                    }

                    $manifestFormat[$i]['keterangan'] = $manifest[$i]['Comment'] . ' to ' . $manifest[$i]['Recipient'] . $relationName;
                }

                $manifestFormat[$i]['live_tracking'] = '';
                $manifestFormat[$i]['driver_info'] = array(
                    'driver_name' => '',
                    'driver_phone' => '',
                    'driver_photo' => '',
                    'vehicle_number' => ''
                );
            }

            // get manifest array for insert / update to sales_shipment_tracking table
            $checkAwb['manifest'] = array_merge(
                array(
                    'info' => array(
                        'courier_name' => 'NCS',
                        'delivery_date' => $delivery_date,
                        'status' => $checkAwb['last_status'],
                        'no_awb' => $manifest[0]['AWB'],
                        'shipping_to' => $manifest[$lastIndex]['Recipient']
                    )
                ),
                array('manifest' => $manifestFormat)
            );

            $shipmentTrackings = $this->getShipmentTracking($shipment_id);
            foreach ($shipmentTrackings as $shipmentTracking) {
                $status = $shipmentTracking['status'];
                if (!empty($status)) {
                    $statusData = json_decode($status, true);
                    foreach ($statusData['manifest'] as $key => $manifest) {
                        if ($manifest['courier_name'] == 'NCS') {
                            unset($statusData['manifest'][$key]);
                        }
                    }
                }
                $checkAwb['manifest']['manifest'] = array_merge($statusData['manifest'], $checkAwb['manifest']['manifest']);
            }
        }

        return $checkAwb;
    }

    /**
     * Function to check AWB and get the last awb status
     *
     * @param $awb_number
     * @return array
     */
    public function checkAwbQrim($shipment_id, $manifest = array())
    {
        $checkAwb = array();
        if (count($manifest) > 0) {
            $lastIndex = count($manifest) - 1;
            // get last status
            $checkAwb['last_status'] = $manifest[$lastIndex]['status_name'];

            $dateOldFormat = 'Y-m-d H:i:s';
            $dateNewFormat = 'd-M-Y H:i:s';

            $formatDate = \DateTime::createFromFormat($dateOldFormat, $manifest[0]['timestamp']);
            $delivery_date = $formatDate->format($dateNewFormat);

            $manifestFormat = array();
            for ($i = 0; $i < count($manifest); $i++) {
                $formatDate = \DateTime::createFromFormat($dateOldFormat, $manifest[$i]['timestamp']);
                $manifest_date = $formatDate->format($dateNewFormat);

                $manifestFormat[$i]['courier_name'] = 'Qrim';
                $manifestFormat[$i]['mfcnote_no'] = $manifest[$i]['awb'];
                $manifestFormat[$i]['manifest_date'] = $manifest_date;
                $manifestFormat[$i]['keterangan'] = $manifest[$i]['status'];
            }

            // get manifest array for insert / update to sales_shipment_tracking table
            $checkAwb['manifest'] = array_merge(
                array(
                    'info' => array(
                        'courier_name' => 'Qrim',
                        'delivery_date' => $delivery_date,
                        'status' => $checkAwb['last_status'],
                        'no_awb' => $manifest[$lastIndex]['awb'],
                        'shipping_to' => ""
                    )
                ),
                array('manifest' => $manifestFormat)
            );

            $shipmentTrackings = $this->getShipmentTracking($shipment_id);
            foreach ($shipmentTrackings as $shipmentTracking) {
                $status = $shipmentTracking['status'];
                if (!empty($status)) {
                    $statusData = json_decode($status, true);
                    foreach ($statusData['manifest'] as $key => $manifest) {
                        if ($manifest['courier_name'] == 'Qrim') {
                            unset($statusData['manifest'][$key]);
                        }
                    }
                }
                $checkAwb['manifest']['manifest'] = array_merge($statusData['manifest'], $checkAwb['manifest']['manifest']);
            }
        }

        return $checkAwb;
    }

    /**
     * Function to mapping status 3pl and ruparupa status
     *
     * @param $awbLastStatus
     * @return string
     */
    private function filterAwbStatus($awbLastStatus = FALSE, $carrierId = FALSE)
    {
        $shipmentStatus = '';
        if ($awbLastStatus && $carrierId) {
            $jneGroupCarrierID = array(getenv("JNE_CARRIER_ID"), getenv("JNEJOB_CARRIER_ID"), getenv("JTR_CARRIER_ID"));
            if ($carrierId == $_ENV['SAP_CARRIER_ID']) {
                $processingStatus = array('entri verified', 'outgoing', 'outgoing(smu)', 'incoming', 'delivery', 'undelivered');
                if (in_array($awbLastStatus, $processingStatus)) {
                    $shipmentStatus = 'shipped';
                } elseif ($awbLastStatus == 'delivered') {
                    $shipmentStatus = 'received';
                }
            } else if (in_array($carrierId, $jneGroupCarrierID)) {
                $processingStatus = array('manifested', 'on process', 'on transit', 'received on destination');
                if (in_array($awbLastStatus, $processingStatus)) {
                    $shipmentStatus = 'shipped';
                } elseif ($awbLastStatus == 'delivered') {
                    $shipmentStatus = 'received';
                }
            } else if ($carrierId == $_ENV['NINJAVAN_CARRIER_ID']) {
                $processingStatus = array('successful pickup', 'on vehicle for delivery');
                if (in_array($awbLastStatus, $processingStatus)) {
                    $shipmentStatus = 'shipped';
                } elseif ($awbLastStatus == 'completed' || $awbLastStatus == 'successful delivery') {
                    $shipmentStatus = 'received';
                }
            } else if ($carrierId == $_ENV['GOSEND_SAMEDAY_CARRIER_ID'] || $carrierId == $_ENV['GOSEND_INSTANT_CARRIER_ID']) {
                $processingStatus = array('finding driver', 'allocated', 'enroute pickup', 'picked', 'enroute drop', 'on hold');
                if (in_array($awbLastStatus, $processingStatus)) {
                    $shipmentStatus = 'shipped';
                } else if ($awbLastStatus == 'completed') {
                    $shipmentStatus = 'received';
                }
            } else if ($carrierId == $_ENV['NCS_CARRIER_ID']) {
                // 'data entry (manifested)', 'pickup by courier', 'bagging', 'sort to department', 'arrival at destination', 'on delivery by courier', 'undelivered'
                $processingStatus = array('de', 'pu', 'bg', 'st', 'ar', 'od', 'un');
                if (in_array($awbLastStatus, $processingStatus)) {
                    $shipmentStatus = 'shipped';
                } else if ($awbLastStatus == 'ok') {
                    $shipmentStatus = 'received';
                }
            } else if ($carrierId == $_ENV['QRIM_CARRIER_ID']) {
                $processingStatus = array('arrived at origin hub', 'in transit', 'pending reschedule', 'arrived at hub', 'on vehicle for delivery');
                if (in_array($awbLastStatus, $processingStatus)) {
                    $shipmentStatus = 'shipped';
                } else if ($awbLastStatus == 'delivered') {
                    $shipmentStatus = 'received';
                }
            }
        }

        return $shipmentStatus;
    }

    /**
     * Function to update shipment status and command to send email
     *
     * @param array $params
     * @return int Return 1 if no error (for add count status changed), return 0 if error / no status change needed
     */
    public function updateShipmentStatusByAwb($params = array())
    {
        try {
            if (array_key_exists('shipment_id', $params)) {
                $salesShipmentModel = new \Models\SalesShipment();
                $salesShipmentResult = $salesShipmentModel->findFirst("shipment_id = " . $params['shipment_id']);
                if ($salesShipmentResult) {
                    $salesShipmentArray = $salesShipmentResult->toArray();
                    $currentShipmentStatus = '';
                    if (count($salesShipmentArray) > 0) {
                        $currentShipmentStatus = $salesShipmentArray['shipment_status'];
                        $invoiceId = $salesShipmentArray['invoice_id'];
                    }

                    if (!empty($currentShipmentStatus) && $currentShipmentStatus <> 'canceled') {
                        if ($params['shipment_status'] == 'shipped') {
                            if ($currentShipmentStatus == 'shipped') {
                                return 0;
                            } elseif ($currentShipmentStatus == 'received') {
                                return 0;
                            }
                        } elseif ($params['shipment_status'] == 'received') {
                            if ($currentShipmentStatus == 'received') {
                                return 0;
                            }
                        }

                        $params['insert_to_3pl'] = 'true';

                        date_default_timezone_set("Asia/Jakarta");
                        if ($params['shipment_status'] == 'shipped' && !isset($params['shipped_date'])) {
                            $params['shipped_date'] = date("Y-m-d H:i:s");
                        } elseif ($params['shipment_status'] == 'received' && !isset($params['delivered_date'])) {
                            $params['delivered_date'] = date("Y-m-d H:i:s");
                        }

                        $salesShipmentModel->setFromArray($params);
                        $salesShipmentModel->saveData("shipment");

                        $salesInvoiceModel = new \Models\SalesInvoice();
                        $salesInvoiceResult = $salesInvoiceModel->findFirst("invoice_id = " . $invoiceId);
                        $salesInvoiceArray = array();
                        if ($salesInvoiceResult) {
                            $salesInvoiceArray = $salesInvoiceResult->toArray();
                        }

                        if (count($salesInvoiceArray) > 0) {

                            // sales log for shipment status
                            $salesLogModel = new \Models\SalesLog();
                            $paramLog['sales_order_id'] = $salesInvoiceArray['sales_order_id'];
                            $paramLog['invoice_id'] = $salesInvoiceArray['invoice_id'];
                            $paramLog['shipment_id'] = $params['shipment_id'];
                            $paramLog['supplier_user_id'] = substr($salesInvoiceArray['store_code'], 0, 1) == 'M' ? 103 : 0;
                            $paramLog['admin_user_id'] = substr($salesInvoiceArray['store_code'], 0, 1) == 'M' ? 0 : 1;
                            $paramLog['actions'] = "** update shipment status #{$salesInvoiceArray['invoice_no']} to {$params['shipment_status']}";
                            $paramLog['affected_field'] = "shipment_status";
                            $paramLog['value'] = $params['shipment_status'];
                            $paramLog['log_time'] = date("Y-m-d H:i:s");
                            $paramLog['flag'] = ((substr( $salesInvoiceArray['store_code'], 0, 2 ) === "DC") ? "dc_dashboard" : "store_dashboard");
                            if (substr($salesInvoiceArray['store_code'], 0, 1) == 'M') {
                                $paramLog['flag'] = "seller_dashboard";
                            }
                            $salesLogModel->setFromArray($paramLog);
                            $salesLogModel->saveData("sales_log");

                            $paramsInvoice['invoice_id'] = $invoiceId;
                            $paramsInvoice['invoice_status'] = $params['shipment_status'];

                            $invoiceStatus = $salesInvoiceArray['invoice_status'];
                            $storeCode = $salesInvoiceArray['store_code'];

                            // dont update invoice status if refund status found
                            $forbiddenStatus = array('partial_refund', 'full_refund');
                            if (!in_array($invoiceStatus, $forbiddenStatus)) {
                                $this->updateInvoiceStatus($paramsInvoice);

                                // sales log for invoice status
                                if (isset($salesInvoiceArray)) {

                                    $salesLogModel = new \Models\SalesLog();
                                    $paramLog['actions'] = "update invoice status #{$salesInvoiceArray['invoice_no']} to {$paramsInvoice['invoice_status']}";
                                    $paramLog['affected_field'] = "invoice_status";
                                    $paramLog['value'] = $paramsInvoice['invoice_status'];
                                    // Doesn't need to set flag anymore because it's already set before
                                    // $paramLog['flag'] = (($salesInvoiceArray['store_code'] == 'DC') ? "dc_dashboard" : "store_dashboard");
                                    $salesLogModel->setFromArray($paramLog);
                                    $salesLogModel->saveData("sales_log");
                                }

                                $this->updateOrderStatus($paramsInvoice);
                            }

                            $sendEmail = true;
                            if (isset($params['send_email'])) {
                                if ($params['send_email'] == 'no') {
                                    $sendEmail = false;
                                }
                            }

                            // queue buat logistic init JNE ke tokopedia
                            // $isJne = false;
                            // $isTokopedia = false;
                            // $jneGroupCarrierID = array(getenv("JNE_CARRIER_ID"), getenv("JNEJOB_CARRIER_ID"), getenv("JTR_CARRIER_ID"));
                            // if (in_array($params['carrier_id'], $jneGroupCarrierID)) { 
                            //     $isJne = true;
                            // }                                                      
                            // $salesOrderModel = new \Models\SalesOrder();
                            // $salesOrderResult = $salesOrderModel->findFirst("sales_order_id = " . $salesInvoiceArray['sales_order_id']);                            
                            // $orderNo = $salesOrderResult->getOrderNo();
                            // if (substr($orderNo, 0, 4) === "ODIT"){
                            //     $isTokopedia = true;
                            // }

                            // if (empty($salesShipmentModel->getErrorCode()) && $params['shipment_status'] == 'shipped' && $isJne && $isTokopedia ) {                                
                            //     $this->sendTrackNoVendor($params['shipment_id'], "init");
                            // }

                            if (empty($salesShipmentModel->getErrorCode()) && $sendEmail) {
                                // Send the email if update data is success
                                $paramsSendEmail['invoice_id'] = $invoiceId;
                                $paramsSendEmail['shipment_id'] = $params['shipment_id'];
                                if ($params['shipment_status'] == 'shipped') {
                                    if (substr( $storeCode, 0, 2 ) === "DC") {
                                        // for DC stock release when shipped
                                        $this->updateStockStatus($paramsInvoice);
                                    }

                                    $paramsSendEmail['shipment_status'] = 'shipped';
                                    $this->sendEmail($paramsSendEmail);
                                } elseif ($params['shipment_status'] == 'received') {
                                    if ($storeCode[0] == "M") {
                                        $this->updateStockStatus($paramsInvoice);
                                    }

                                    $paramsSendEmail['shipment_status'] = 'received';
                                    $this->sendEmail($paramsSendEmail);

                                    $nsq = new \Library\Nsq();
                                    $message = [
                                        "data" => array(
                                            "invoice_id" => $invoiceId,
                                        ),
                                        "message" => "sendEmailFreeInstallation"
                                    ];
                                    $nsq->publishCluster('afterSales', $message);
                                }

                                return 1;
                            } else if (!$sendEmail) {
                                return 1;
                            } else {
                                return 0;
                            }
                        } else {
                            return 0;
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("salesShipment", $e->getMessage(), "Error");
            return 0;
        }
    }

    /**
     * Function to update invoice status
     * @param array $params
     */
    private function updateInvoiceStatus($params = array())
    {
        $salesInvoiceModel = new \Models\SalesInvoice();

        $paramsInvoice['invoice_id'] = $params['invoice_id'];
        $paramsInvoice['invoice_status'] = $params['invoice_status'];
        $salesInvoiceModel->setFromArrayUpdate($paramsInvoice);
        $salesInvoiceModel->saveData("sales_invoice");
    }

    /**
     * Function to update order status
     * @param array $params
     */
    private function updateOrderStatus($params = array())
    {
        $salesInvoiceModel = new \Models\SalesInvoice();

        $salesInvoiceResult = $salesInvoiceModel->findFirst("invoice_id = " . $params['invoice_id']);
        $orderId = $salesInvoiceResult->getSalesOrderId();
        if ($orderId) {
            if ($params['invoice_status'] == 'received') {
                $salesInvoiceResult = $salesInvoiceModel->find("sales_order_id = $orderId AND invoice_status not in ('partial_refund','full_refund','received','canceled')");
                if ($salesInvoiceResult) {
                    $salesInvoiceArray = $salesInvoiceResult->toArray();
                    if (count($salesInvoiceArray) == 0) {
                        $salesOrderModel = new \Models\SalesOrder();
                        $paramsOrder['sales_order_id'] = $orderId;
                        $paramsOrder['status'] = 'complete';
                        $salesOrderModel->setFromArray($paramsOrder);
                        $salesOrderModel->saveData("sales_order");

                        // re-check the order status
                        $salesOrderResult = $salesOrderModel->findFirst("sales_order_id = " . $paramsOrder['sales_order_id']);
                        $currentOrderStatus = $salesOrderResult->getStatus();
                        $orderNo = $salesOrderResult->getOrderNo();
                        if ($paramsOrder['status'] == 'complete' && $currentOrderStatus == 'complete') {
                            // send the email to customer
                            $paramsEmail['invoice_id'] = $params['invoice_id'];
                            $paramsEmail['link_review'] = $_ENV['LINK_CUSTOMER_REVIEW'] . $orderNo;
                            // wait for final email template
                            //$this->sendEmailReview($paramsEmail);
                        }
                    }
                }
            }
        }
    }

    private function updateStockStatus($params = array())
    {
        if (!empty($params['sales_order_id'])) {
            // Update stock status
            $salesOrderModel = new \Models\SalesOrder();
            $salesOrderData = $salesOrderModel->findFirst("sales_order_id = " . $params['sales_order_id']);

            if ($salesOrderData && $salesOrderData->SalesOrderItem) {
                $productStock = new ProductStock();
                $orderItemIds = array();
                /**
                 * @var $salesItem \Models\SalesOrderItem
                 */
                foreach ($salesOrderData->SalesOrderItem as $salesItem) {
                    $orderItemIds[] = (int)$salesItem->getSalesOrderItemId();
                }
                $status = $productStock->deductStockOrderV2("complete", $orderItemIds);
            }
        } elseif (!empty($params['invoice_id'])) {
            // Update stock status
            $salesInvoiceModel = new \Models\SalesInvoice();
            $salesInvoiceData = $salesInvoiceModel->findFirst("invoice_id = " . $params['invoice_id']);

            if ($salesInvoiceData && $salesInvoiceData->SalesInvoiceItem) {
                $productStock = new ProductStock();
                $orderItemIds = array();
                /**
                 * @var $salesItem \Models\SalesInvoiceItem
                 */
                foreach ($salesInvoiceData->SalesInvoiceItem as $salesItem) {
                    $orderItemIds[] = (int)$salesItem->getSalesOrderItemId();
                }
                $status = $productStock->deductStockOrderV2("complete", $orderItemIds);
            }
        }
    }

    /**
     * Function to Get data for the email and send the email
     *
     * @param array $params
     */
    public function sendEmail($params = array())
    {
        if (count($params) > 0) {
            $salesInvoiceModel = new \Models\SalesInvoice();
            $salesInvoiceObj = $salesInvoiceModel->findFirst("invoice_id = " . $params['invoice_id']);
            $salesInvoiceArray = array("company_code" => "ODI");
            if ($salesInvoiceObj) {
                $salesInvoiceArray = $salesInvoiceModel->findFirst("invoice_id = " . $params['invoice_id'])->toArray();
            }
            $emailHelper = new \Library\Email();
            $emailHelper->setInvoiceObj($salesInvoiceObj);
            if (isset($params['shipment_id'])) {
                $salesShipmentModel = new \Models\SalesShipment();
                $salesShipmentResult = $salesShipmentModel->findFirst("shipment_id = " . $params['shipment_id']);
                $emailHelper->setShipmentObj($salesShipmentResult);
            }
            $emailHelper->setEmailSender();
            $emailHelper->buildShipmentCheckAwbParam();
            
            if (isset($params['invoice_id']) && $params['invoice_id'] <> "") {
                $emailHelper->generateAddressByInvoiceID($params['invoice_id']);
            } else {
                $emailHelper->generateAddress();
            }

            $salesOrderModel = new \Models\SalesOrder();
            $salesOrderResult = $salesOrderModel->findFirst("sales_order_id = " . $salesInvoiceArray['sales_order_id']);
            $emailCompanyCode = $salesInvoiceArray['company_code'];
            if (!empty($salesOrderResult)) {
                $salesOrderArray = $salesOrderResult->toArray();
                if (!empty($salesOrderArray['store_code_new_retail']) && substr($salesOrderArray['store_code_new_retail'], 0, 1) == "A" && substr($salesInvoiceArray['store_code'], 0, 1) == "A") {
                    $emailCompanyCode = "AHI";
                } else if (\Helpers\GeneralHelper::isStoreCodeSelma($salesOrderArray['store_code_new_retail']) && \Helpers\GeneralHelper::isStoreCodeSelma($salesInvoiceArray['store_code'])) {
                    $emailCompanyCode = "SLM";
                } else if (!empty($salesOrderArray['store_code_new_retail']) && substr($salesOrderArray['store_code_new_retail'], 0, 1) == "H" && substr($salesInvoiceArray['store_code'], 0, 1) == "H") {
                    $emailCompanyCode = "HCI";
                } else if (!empty($salesOrderArray['store_code_new_retail']) && substr($salesOrderArray['store_code_new_retail'], 0, 1) == "J" && substr($salesInvoiceArray['store_code'], 0, 1) == "J") {
                    $emailCompanyCode = "HCI";
                } else if (!empty($salesOrderArray['device']) && in_array($salesOrderArray['device'], ['ace-ios', 'ace-android'])){
                    $emailCompanyCode = "AHI";
                }

                if (!empty($salesOrderArray['order_no']) && (substr($salesOrderArray['order_no'], 0, 4) == "ODII" || substr($salesOrderArray['order_no'], 0, 4) == "ODIH")) {
                    $emailCompanyCode = "HCI";
                }

                if (!empty($salesOrderArray['order_no']) && (substr($salesOrderArray['order_no'], 0, 4) == "ODIL" || substr($salesOrderArray['order_no'], 0, 4) == "ODIM")) {
                    $emailCompanyCode = "SLM";
                }
            }

            if ($params['shipment_status'] == 'shipped') {
                $prefix_order = substr($salesInvoiceObj->salesorder->order_no, 0, 4);
        
                if ($prefix_order != 'ODIS' && $prefix_order != 'ODIT' && $prefix_order != 'ODIK') {
                    $emailHelper->setEmailTag("cust_shipment_process");
                    $templateID = \Helpers\GeneralHelper::getTemplateId("shipment_processing", $emailCompanyCode);

                    // Filter Template Gosend
                    if (!empty($salesShipmentResult)) {
                        $salesShipmentArray = $salesShipmentResult->toArray();
                        if ($salesShipmentArray['carrier_id'] == getenv('GOSEND_INSTANT_CARRIER_ID') || $salesShipmentArray['carrier_id'] == getenv('GOSEND_SAMEDAY_CARRIER_ID')) {
                            $templateID = \Helpers\GeneralHelper::getTemplateId("shipment_processing_gosend", $emailCompanyCode);
                        } else {
                            $templateID = \Helpers\GeneralHelper::getTemplateId("shipment_processing", $emailCompanyCode);
                        }
                    }
                    $emailHelper->setTemplateId($templateID);
                    $emailHelper->setCompanyCode($emailCompanyCode);
                    $emailHelper->setName();
                    $emailHelper->setEmailReceiver();
                    $emailHelper->send();
       
                    // push send notifications
                    $firstNameCustomer = $emailHelper->getCustomerFirstName();
                    $emailReceiver = $emailHelper->getEmailReceiver();
                    $notificationHelper = new \Library\Notification();
                    $iconSuccessPaymentNotif = getenv('PUSH_NOTIF_ICON_ODI');
                    // Override icon according to company code if exist
                    $iconByCompanyCode = getenv(sprintf("PUSH_NOTIF_ICON_%s", strtoupper($emailCompanyCode)));
                    if (!empty($iconByCompanyCode)) {
                        $iconSuccessPaymentNotif = $iconByCompanyCode;
                    }
                    $custId =  $salesInvoiceObj->salesorder->customer_id;
                    $notificationHelper->setTitle("Pesanan Kamu Dalam Perjalanan! 🚚");
                    $notificationHelper->setBody("Pantau status pesananmu disini");
                    $notificationHelper->setKey("token");
                    $notificationHelper->setIcon($iconSuccessPaymentNotif);
                    $notificationHelper->setPageType("order-detail");
                    $notificationHelper->setUrl(getenv('RUPARUPA_URL') . "order-detail?" . urlencode("orderId=" . $salesInvoiceObj->salesorder->order_no . "&email=" . $emailReceiver));
                    $notificationHelper->setUrlKey($salesInvoiceObj->salesorder->order_no . "," . $emailReceiver.",".$salesInvoiceObj->getInvoiceNo());
                    $notificationHelper->setCustomerID($custId);
                    $customerDeviceToken = new \Models\CustomerDeviceToken();
                    $customerDeviceToken = $customerDeviceToken->getCDTByCustIdForPushNotif($custId);

                    $notificationHelper->PushNotifInbox();

                    $pushNotifList = explode(',', getenv('PUSH_NOTIF_BU_CODE'));

                    if (count($customerDeviceToken) > 0 && in_array($emailCompanyCode, $pushNotifList)) {
                        $filteredToken = $notificationHelper->FilterTokenByHighestPriority($customerDeviceToken);

                        foreach($filteredToken as $filterToken){
                            $notificationHelper->setValue($filterToken["device_token"]);
                            $notificationHelper->PushNotif($emailCompanyCode);
                        }
                    }
                }
            } elseif ($params['shipment_status'] == 'received') {
                $prefixOrderNo = substr($salesInvoiceObj->salesorder->order_no, 0, 4);
        
                if ($prefixOrderNo != 'ODIS' && $prefixOrderNo != 'ODIT' && $prefixOrderNo != 'ODIK') {
                    if ($prefixOrderNo == "ODIR" || $prefixOrderNo == "ODIB" || $prefixOrderNo == "ODIC") {
                        $templateCode = "shipment_complete";
                    } else {
                        $templateCode = "shipment_complete_review";

                        if ($salesOrderArray['device'] == "informa-android" || $salesOrderArray['device'] == "informa-ios" || $salesOrderArray['device'] == "informa-huawei") {
                            $emailHelper->setLinkReview("https://informamobileapp.page.link/review-rating");
                        } else if ($salesOrderArray['device'] == "ace-android" || $salesOrderArray['device'] == "ace-ios") {
                            $emailHelper->setLinkReview("https://acemobileapp.page.link/?link=https://www.ruparupa.com?page=ProfileReviewRating&apn=id.co.acehardware.acerewards&isi=1056027974&ibi=id.co.acehardware.ACE-Hardware-Indonesia&cid=8485359881257355541&_icp=1");
                            $emailCompanyCode = "AHI";
                        } else if ($salesOrderArray['device'] == "selma-android" || $salesOrderArray['device'] == "selma-ios" || $salesOrderArray['device'] == "selma-huawei") {
                            $emailHelper->setLinkReview("https://selmamobileapp.page.link/review-rating");
                        }
                    }

                    $emailHelper->setEmailTag("cust_shipment_completed");
                    $templateID = \Helpers\GeneralHelper::getTemplateId($templateCode, $emailCompanyCode);
                    $emailHelper->setTemplateId($templateID);
                    $emailHelper->setCompanyCode($emailCompanyCode);
                    $emailHelper->setName();
                    $emailHelper->setEmailReceiver();
                    $emailHelper->send();

                    // push send notifications
                    $firstNameCustomer = $emailHelper->getCustomerFirstName();
                    $emailReceiver = $emailHelper->getEmailReceiver();
                    $custId =  $salesInvoiceObj->salesorder->customer_id;
                    $notificationHelper = new \Library\Notification();
                    $iconSuccessPaymentNotif = getenv('PUSH_NOTIF_ICON_ODI');
                    // Override icon according to company code if exist
                    $iconByCompanyCode = getenv(sprintf("PUSH_NOTIF_ICON_%s", strtoupper($emailCompanyCode)));
                    if (!empty($iconByCompanyCode)) {
                        $iconSuccessPaymentNotif = $iconByCompanyCode;
                    }
                    $notificationHelper->setTitle("Terima kasih Telah Berbelanja!");
                    $notificationHelper->setBody("Semoga kamu suka dengan produknya. Dapatkan Voucher hingga Rp50.000 dengan memberi ulasan pada akun saya");
                    $notificationHelper->setKey("token");
                    $notificationHelper->setIcon($iconSuccessPaymentNotif);
                    $notificationHelper->setPageType("order-detail");
                    $notificationHelper->setUrl(getenv('RUPARUPA_URL') . "order-detail?" . urlencode("orderId=" . $salesInvoiceObj->salesorder->order_no . "&email=" . $emailReceiver));

                    $isOrderAutoReceivedPushNotif = "is_order_auto_received_false";
                    $isOrderAutoReceived = InvoiceHelper::isOrderAutoReceived($salesOrderResult, $salesInvoiceObj);
                    if ($isOrderAutoReceived){
                        $isOrderAutoReceivedPushNotif = "is_order_auto_received_true";
                    }
                    $notificationHelper->setUrlKey(sprintf(
                        '%s,%s,%s,%s',
                        $salesInvoiceObj->salesorder->order_no,
                        $emailReceiver,
                        $salesInvoiceObj->getInvoiceNo(),
                        $isOrderAutoReceivedPushNotif
                    ));
                    $notificationHelper->setCustomerID($custId);
                    $customerDeviceToken = new \Models\CustomerDeviceToken();
                    $customerDeviceToken = $customerDeviceToken->getCDTByCustIdForPushNotif($custId);

                    $notificationHelper->PushNotifInbox();

                    $pushNotifList = explode(',', getenv('PUSH_NOTIF_BU_CODE'));

                    if (count($customerDeviceToken) > 0 && in_array($emailCompanyCode, $pushNotifList)) {
                        $filteredToken = $notificationHelper->FilterTokenByHighestPriority($customerDeviceToken);

                        foreach($filteredToken as $filterToken){
                            $notificationHelper->setValue($filterToken["device_token"]);
                            $notificationHelper->PushNotif($emailCompanyCode);
                        }
                    }

                    if ($salesOrderArray['cart_type'] == "gift"){
                        $cartModel = new \Models\Cart();
                        $cartModel->setCartId($salesOrderArray['cart_id']);

                        $headers = [
                            'No-Refresh' => "1",
                        ];
                        $cartData = $cartModel->getCartDetailV2($headers);
                        if (!empty($cartModel->getErrorCode()) && !empty($cartModel->getErrorMessages()['message'])) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = $cartModel->getErrorMessages()['message'];
                            return false;
                        }

                        $giftAPI = new \Library\GiftRegistryAPI();
                        $getRegistryPayload = [
                            "registry_id" => $cartData['gift_registry']['registry_id'],
                            "customer_id" => $cartData['customer']['customer_id'],
                        ];
                        $ok = $giftAPI->getRegistry($getRegistryPayload);
                        if (!$ok) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = $giftAPI->getErrorMessage()['message'];
                            return false;
                        }

                        $this->sendAlertGiftRegistryinvoiceReceived($cartData, $giftAPI->getData(), $salesInvoiceArray["invoice_no"], $salesInvoiceObj->salesorder->order_no);
                    }
                }
            } elseif ($params['shipment_status'] == 'pending_received') {
                // build param for pending_received email
                $emailHelper->buildMpPendingReceivedParam();
                $emailHelper->setEmailTag("mp_pending_received_order");
                $templateID = \Helpers\GeneralHelper::getTemplateId("mp_pending_received_order", $emailCompanyCode);
                $emailHelper->setTemplateId($templateID);
                $emailHelper->setCompanyCode($salesInvoiceArray['company_code']);
                $emailHelper->setName();
                $emailHelper->setEmailReceiver("merchandising@ruparupa.com", "Ruparupa Merchandising");
                $emailHelper->send();
            }
        }
    }

    private function sendAlertGiftRegistryinvoiceReceived($cartData, $giftData, $invoiceNo, $orderNo)
    {
        $product = [];
        foreach ($cartData['items'] as $item) {
            $product[] = (object)[
                'image_name' => $item['primary_image_url'],
                'name' => $item['name'],
                'sku' => $item['sku'],
                'qty' => $item['qty_ordered'],

            ];
        }
        if ($giftData["link_detail"]['is_custom'] && !empty($giftData["link_detail"]['url_key'])) {
            $customUrl = $giftData["link_detail"]['url_key'];
        } else {
            $customUrl = $giftData["link_detail"]['default_url_key'];
        }

        $templateModel = [
            "owner_name" => $giftData["customer"]["first_name"],
            "order_no" => $orderNo,
            "received_date" => date("Y-m-d H:i:s"),
            "product" => $product,
            "gift_registry_url" => sprintf("%s/%s", getenv('GIFT_BASE_URL'), $giftData["link_detail"]["default_url_key"]),
            "gift_registry_url_view" => $customUrl
        ];
        $emailHelper = new \Library\Email();
        $templateID = \Helpers\GeneralHelper::getTemplateId("gift_registry_invoice_received", "ODI");
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode("ODI");
        $emailHelper->setEmailSender();
        $emailHelper->setEmailReceiver($giftData["customer"]["email"]);
        $emailHelper->setTemplateModel($templateModel);
        $emailHelper->setEmailTag("gift_registry_invoice_received");
        $emailHelper->send();

        $notificationHelper = new \Library\Notification();
        $iconODI = getenv('PUSH_NOTIF_ICON_ODI');
        $companyCode = "ODI";
        $notificationHelper->setTitle("Gift List kamu sudah diterima!");
        $notificationHelper->setBody("Bikin lagi yuk untuk event seru lainnya.");
        $notificationHelper->setKey("token");
        $notificationHelper->setIcon($iconODI);
        $notificationHelper->setPageType("gift-registry-detail-order");
        $notificationHelper->setUrl(sprintf("%sgift-registry/%s?invoice_no=%s",getenv('RUPARUPA_URL'), $giftData["link_detail"]["default_url_key"], $invoiceNo));
        $registryId = $giftData['registry_id'];
        $notificationHelper->setUrlKey("$registryId,$invoiceNo");

        $customerDeviceToken = new \Models\CustomerDeviceToken();
        $customerDeviceToken = $customerDeviceToken->getCDTByCustIdForPushNotif($giftData["customer"]['customer_id']);
        $pushNotifList = explode(',', getenv('PUSH_NOTIF_BU_CODE'));
        if (count($customerDeviceToken) > 0 && in_array($companyCode, $pushNotifList)) {
            $filteredToken = $notificationHelper->FilterTokenByHighestPriority($customerDeviceToken);
            foreach($filteredToken as $filterToken){
                $notificationHelper->setValue($filterToken["device_token"]);
                $notificationHelper->PushNotif($companyCode);
            }
        }
    }

    /**
     * Function to Get data for the review email and send the email
     *
     * @param array $params
     */
    public function sendEmailReview($params = array())
    {
        if (count($params) > 0) {
            $salesInvoiceModel = new \Models\SalesInvoice();
            $salesInvoiceObj = $salesInvoiceModel->findFirst("invoice_id = " . $params['invoice_id']);
            $salesInvoiceArray = array("company_code" => "ODI");
            if ($salesInvoiceObj) {
                $salesInvoiceArray = $salesInvoiceModel->findFirst("invoice_id = " . $params['invoice_id'])->toArray();
            }

            $emailHelper = new \Library\Email();
            $emailHelper->setInvoiceObj($salesInvoiceObj);
            $emailHelper->setName();
            $emailHelper->setEmailSender();
            $emailHelper->setEmailTag("Customer Review");
            $emailHelper->buildShippingItems();
            $emailHelper->buildShippingEmailParam();
            $emailHelper->setLinkReview($params['link_review']);
            $emailHelper->setEmailTag("cust_review_product_rr");
            $templateID = \Helpers\GeneralHelper::getTemplateId("customer_review", $salesInvoiceArray['company_code']);
            $emailHelper->setCompanyCode($salesInvoiceArray['company_code']);
            $emailHelper->setTemplateId($templateID);
            $emailHelper->setEmailReceiver();
            $emailHelper->send();
        }
    }
    //</editor-fold>

    /**
     * Note : update shipment_id, refers to invoice_no
     */
    public function processSalesShipmentBatch()
    {
        try {
            $salesShipmentModel = new \Models\SalesShipment();
            $sql = "UPDATE sales_shipment_batch a
                    INNER JOIN
                    (SELECT 
                        shipment_id,
                            (SELECT 
                                    invoice_no
                                FROM
                                    sales_invoice
                                WHERE
                                    c.invoice_id = invoice_id) AS invoice_no
                    FROM
                        sales_shipment c
                    ) b ON a.invoice_no = b.invoice_no 
                        SET 
                            a.shipment_id = b.shipment_id";
            $salesShipmentModel->useWriteConnection();
            $salesShipmentModel->getDi()->getShared($salesShipmentModel->getConnection())->execute($sql);
        } catch (\Exception $e) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not process your data, error on processSalesShipmentBatch";
        }
    }

    /**
     * Note : 
     */
    public function getSalesShipmentBatch()
    {
        try {
            $salesShipmentModel = new \Models\SalesShipment();
            $sql = "select * from sales_shipment_batch";
            $salesShipmentModel->useReadOnlyConnection();
            $result = $salesShipmentModel->getDi()->getShared($salesShipmentModel->getConnection())->query($sql);
            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $dataList = $result->fetchAll();
        } catch (\Exception $e) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your data";
            return array();
        }

        return $dataList;
    }

    // send shipment_id to go order vendor
    public function sendTrackNoVendor($param = "", $type = "")
    {
        $nsq = new Nsq();
        $message = [
            "message" => "sendTrackingNoShopee",
            "shipment_id" => $param,
            "type" => $type
        ];
        $nsq->publishCluster('shopee', $message);
    }

    public function insertInvoiceReview($invoiceNo)
    {
        $params['invoice_no'] = $invoiceNo;
        $apiWrapper = new APIWrapper(getenv('REVIEW_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("v2/review/insert");

        if ($apiWrapper->sendRequest("post")) {
            \Helpers\LogHelper::log('review', '[ERROR] when calling go-review ' . json_encode($params));
        }
    }
    
    // update shipment status vendor tokopedia
    public function UpdateShipmentStatusTokopedia($shipmentID, $shipmentStatus)
    {
        $nsq = new Nsq();
        $message = [
            "message" => "setShipmentStatusTokopedia",
            "shipment_id" => $shipmentID,
            "type" => $shipmentStatus
        ];
        $nsq->publishCluster('tokopedia', $message);
    }

    private function sendCartGiftGreeting($cartID){

        // Send e_greeting based on cart
        $cartModel = new \Models\Cart();
        $cartModel->setCartId($cartID);

        $headers = [
            'no-refresh' => "1",
            'fetch-carrier' => "0"
        ];
        $cartData = $cartModel->getCartDetailV2($headers);
        if (!empty($cartModel->getErrorCode()) && !empty($cartModel->getErrorMessages()['message'])) {
            LogHelper::log("send_gift_greeting",sprintf("Failed to get cart detail for cart_id:%s ERROR:%s", $cartID, $cartModel->getErrorMessages()['message']), "info");
            return;
        }

        if (!empty($cartData['cart_greeting']['sender_name'])){
            // Preparing payload based on method
            $messageAPILib = new \Library\MessageAPI();

            if (!empty($cartData['cart_greeting']['phone'])){
                // Chat message payload
                $messagePayload = [
                    "recipient" => $cartData['cart_greeting']['phone'],
                    "channel" => "chat-image",
                    "content" => [
                        "template_name" => "send_as_gift_with_image",
                        "placeholders" => [
                            $cartData['cart_greeting']['receiver_name'],
                            $cartData['cart_greeting']['sender_name'],
                            $cartData['cart_greeting']['message']
                        ],
                        "image_url" => getenv('GIFT_GREETING_ICON_URL')
                    ]
                ];
            }else if (!empty($cartData['cart_greeting']['email'])) {
                // Get template from master email template
                $templateID = GeneralHelper::getTemplateId("send_gift_greeting", "ODI");
                if (!$templateID) {
                    LogHelper::log('send_gift_greeting', sprintf("Email template for send_gift_greeting not found for cart_id:%s", $cartID), "info");
                    return;
                }
               
                // Email message payload
                $messagePayload = [
                    "recipient" => $cartData['cart_greeting']['email'],
                    "channel" => "email",
                    "content" => [
                        "company_code" => "ODI",
                        "template_id" => $templateID,
                        "template_model" => [
                            "sender_name" => $cartData['cart_greeting']['sender_name'], 
                            "receiver_name" => $cartData['cart_greeting']['receiver_name'],
                            "message" => $cartData['cart_greeting']['message']
                        ],
                        "tag"=>"send_gift_greeting",
                    ]
                ];
            }

            $messages = [$messagePayload];
            $messageAPILib->setMessages($messages);
            $messageAPILib->setAppSender("api");
            $sendMessageResp = $messageAPILib->sendSingleGeneralMessage();
            if (!empty($sendMessageResp["message_receipts"])){
                if ($sendMessageResp["message_receipts"][0]["status"] == "error") {
                    LogHelper::log('send_gift_greeting',sprintf("Failed to send general meessage for cart_id:%s ERROR:%s", $cartID,$sendMessageResp["message_receipts"][0]["error_message"]), "info");
                }
            }
        }
    }

    public function missionEventTrack($invoiceNo)
    {
        $params['invoice_no'] = $invoiceNo;
        $apiWrapper = new APIWrapper(getenv('ORDER_API_V2'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("mission/event-track");

        if ($apiWrapper->sendRequest("post")) {
            \Helpers\LogHelper::log('mission', '[ERROR] when calling go-order-v2 ' . json_encode($params));
        }
    }

    public function missionEventTrackOrder($orderNo)
    {
        $params['order_no'] = $orderNo;
        $apiWrapper = new APIWrapper(getenv('ORDER_API_V2'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("mission/event-track-order");

        if ($apiWrapper->sendRequest("post")) {
            \Helpers\LogHelper::log('mission', '[ERROR] when calling go-order-v2 ' . json_encode($params));
        }
    }

    private function CreateJournalPickupStore($params)
    {
        $apiWrapper = new APIWrapper(getenv('FINANCE_API'));
        $apiWrapper->setEndPoint("journals/invoice/store/pickup");
        $apiWrapper->setParam($params);

        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "RR400";
            $this->errorMessages = "Post Store Pickup Failed";
            return false;
        }

        $errorResponse = $apiWrapper->getError();
        $data = $apiWrapper->getData();
        return $data;
    }

    private function updateOwnfleetTiktok($order_no = '', $status = '')
    {

        try {
            $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_V2_API'));
            $apiWrapper->setEndPoint("tiktok/logistic/consumer/status");
            $params = [
                "order_no" => $order_no,
                "status" => $status,
            ];
            $apiWrapper->setParam($params);
    
            if($apiWrapper->sendRequest("post")) {
                $apiWrapper->formatResponse();
            }

        }catch (\Exception $ex) {
            \Helpers\LogHelper::log("shipment_tiktok", "failed update shipment tiktok : ".print_r($ex, true));
        }
        
    }
}
