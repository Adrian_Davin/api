<?php

namespace Library;

use \Helpers\LogHelper;

class SalesRegisterMember
{
    protected $ORDERID;
    protected $ORDERDATE;
    protected $ORDERTYPE;
    protected $CUSTNAME1;
    protected $CUSTNAME2;
    protected $EMAIL;
    protected $PHONE;
    protected $ADDRESS1;
    protected $ADDRESS2;
    protected $ADDRESS3;
    protected $CITY;
    protected $SALESID;
    protected $SALESOFFICE;
    protected $SALES_GRP;
    protected $KODEJALUR = "";
    protected $COUNTRY = "";
    protected $POSTALCODE = "";
    protected $SHIPCOST = "";
    protected $GRANDPRICE = "";
    protected $PARTIALSHIP = "";
    protected $REMARK;
    protected $CUSTNO = "";
    protected $SOURCE;
    protected $REQDLVDATE;
    protected $SHIPCONDITION = "Z1";
    protected $DPVALUE = "";
    protected $PAYERNAME1 = "";
    protected $PATERNAME2 = "";
    protected $PAYERADDR1 = "";
    protected $PAYERADDR2 = "";
    protected $PAYERADDR3 = "";
    protected $PAYERPOSTCODE = "";
    protected $PAYERCITY = "";
    protected $PAYERPROVINCE = "";
    protected $PAYERNPWP = "";
    protected $PAYEREMAIL = "";
    protected $BILLBLOCK = "";
    protected $DLVBLOCK = "";

    protected $ID = "1";
    protected $PRODUCTNO;
    protected $SALESPRICE;
    protected $QUANTITY;
    protected $UNIT = "EA";
    protected $CURRENCY = "IDR";
    protected $SITE;
    protected $SITE_VENDOR = "";
    protected $CONDTYPE = "";
    protected $INDENT = "";
    protected $VENDOR = "";
    protected $CONFIRMNO = "";
    protected $STGE_LOC = "1000"; 
    protected $SERIALNUMBER = "";

    protected $parameter= array();

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    public function __construct($invoice = null)
    {
        if (!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function prepareSalesRegisterMemberParams()
    {
        $invoiceData = $this->invoice->getDataArray([], true);
        
        $invoiceStoreCode = $this->invoice->getStoreCode();

        if(!empty($invoiceStoreCode) && $invoiceStoreCode === "A300") {
            $orderType = 'B2C_AHI';
            $source = 'E_COMMERCE_AHI_B2C';
            $salesOffice = 'A300';
            $salesGroup = 'A04';
            $remark = 'Membership AHI';
            $salesId = '127014';
        } else if(!empty($invoiceStoreCode) && $invoiceStoreCode === "H300") {
            $orderType = 'B2C_HCI';
            $source = 'E_COMMERCE_HCI_B2C';
            $salesOffice = 'H300';
            $salesGroup = 'H13';
            $remark = 'Membership HCI';
            $salesId = '149181';
        } else if(!empty($invoiceStoreCode) && $invoiceStoreCode === "H203") {
            $orderType = 'B2C_SLM';
            $source = 'E_COMMERCE_SLM_B2C';
            $salesOffice = 'H203';
            $salesGroup = 'H13';
            $remark = 'Membership Selma';
            $salesId = '127014';
        } else if(!empty($invoiceStoreCode) && $invoiceStoreCode === "T300") {
            $orderType = 'B2C_TGI';
            $source = 'E_COMMERCE_TGI_B2C';
            $salesOffice = 'T300';
            $salesGroup = 'T05';
            $remark = 'Membership TGI';
            $salesId = '127014'; 
        } else if(!empty($invoiceStoreCode) && $invoiceStoreCode === "S702" ) { // make sure store code choosen later
            $orderType = 'B2C_KLS';
            $source = 'E_COMMERCE_KLS_B2C';
            $salesOffice = 'S702';
            $salesGroup = 'S88';
            $remark = 'Membership Cohesive ODI - KLS';
            $salesId = '140385';
        } 

        $salesOrderModel = new \Models\SalesOrder;
        $salesOrderQueryCondition = array(
            "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "'"
        );
        $salesOrderResult = $salesOrderModel->findFirst($salesOrderQueryCondition)->toArray();

        $salesOrderAddressModel = new \Models\SalesOrderAddress;
        $salesOrderAddressQueryCondition = array(
            "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "'"
        );
        $salesOrderAddressResult = $salesOrderAddressModel->findFirst($salesOrderAddressQueryCondition)->toArray();

        $customerModel = new \Models\Customer;
        $customerQueryCondition = array(
            "conditions" => "customer_id = '" . $salesOrderResult['customer_id'] . "'"
        );
        $customerResult = $customerModel->findFirst($customerQueryCondition)->toArray();

        $masterCityModel = new \Models\MasterCity;
        $masterCityQueryCondition = array(
            "conditions" => "city_id = '" . $salesOrderAddressResult['city_id'] . "'"
        );
        $masterCityResult = $masterCityModel->findFirst($masterCityQueryCondition)->toArray();

        $this->ORDERID = $invoiceData["invoice_no"];
        $this->ORDERDATE = date("Ymd", strtotime($invoiceData["created_at"]));
        $this->ORDERTYPE = (isset($orderType)? $orderType : "");
        $this->CUSTNAME1 = $salesOrderAddressResult["first_name"];
        $this->CUSTNAME2 = (!empty($salesOrderAddressResult["last_name"])? $salesOrderAddressResult["last_name"] : "");
        $this->EMAIL = $customerResult["email"];
        $this->PHONE = $salesOrderAddressResult["phone"];
        $this->ADDRESS1 = (!empty(substr($salesOrderAddressResult["full_address"], 0, 49))? substr($salesOrderAddressResult["full_address"], 0, 49) : "");
        $this->ADDRESS2 = (!empty(substr($salesOrderAddressResult["full_address"], 50, 99))? substr($salesOrderAddressResult["full_address"], 50, 99) : "");
        $this->ADDRESS3 = (!empty(substr($salesOrderAddressResult["full_address"], 100, 149))? substr($salesOrderAddressResult["full_address"], 100, 149) : "");
        $this->CITY = $masterCityResult["city_name"];
        $this->SALESID = (isset($salesId)? $salesId : "");
        $this->SALESOFFICE = (isset($salesOffice)? $salesOffice : "");
        $this->SALES_GRP = (isset($salesGroup)? $salesGroup : "");
        $this->REMARK = (isset($remark)? $remark : "");
        $this->SOURCE = (isset($source)? $source : "");
        $this->REQDLVDATE = date("Ymd");

        $this->PRODUCTNO = $invoiceData['items'][0]['sku'];
        $this->SALESPRICE = $invoiceData['items'][0]['selling_price'];
        $this->QUANTITY = $invoiceData['items'][0]['qty_ordered'];
        $this->SITE = $invoiceData['items'][0]['store_code'];

        // Temporary until store S702 has been created
        if(!empty($invoiceStoreCode) && $invoiceStoreCode === "A666" || $invoiceStoreCode === "A364" || $invoiceStoreCode === "J374") {
            $this->SITE = "S702";
        }
    }

    public function generateSalesRegisterMemberParams()
    {
        $header = array(
            "ORDERID" => $this->ORDERID,
            "ORDERDATE" => $this->ORDERDATE,
            "ORDERTYPE" => $this->ORDERTYPE,
            "CUSTNAME1" => $this->CUSTNAME1,
            "CUSTNAME2" => $this->CUSTNAME2,
            "EMAIL" => $this->EMAIL,
            "PHONE" => $this->PHONE,
            "ADDRESS1" => $this->ADDRESS1,
            "ADDRESS2" => $this->ADDRESS2,
            "ADDRESS3" => $this->ADDRESS3,
            "CITY" => $this->CITY,
            "SALESID" => $this->SALESID,
            "SALESOFFICE" => $this->SALESOFFICE,
            "SALES_GRP" => $this->SALES_GRP,
            "KODEJALUR" => $this->KODEJALUR,
            "COUNTRY" => $this->COUNTRY,
            "POSTALCODE" => $this->POSTALCODE,
            "SHIPCOST" => $this->SHIPCOST,
            "GRANDPRICE" => $this->GRANDPRICE,
            "PARTIALSHIP" => $this->PARTIALSHIP,
            "REMARK" => $this->REMARK,
            "CUSTNO" => $this->CUSTNO,
            "SOURCE" => $this->SOURCE,
            "REQDLVDATE" => $this->REQDLVDATE,
            "SHIPCONDITION" => $this->SHIPCONDITION,
            "DPVALUE" => $this->DPVALUE,
            "PAYERNAME1" => $this->PAYERNAME1,
            "PATERNAME2" => $this->PATERNAME2,
            "PAYERADDR1" => $this->PAYERADDR1,
            "PAYERADDR2" => $this->PAYERADDR2,
            "PAYERADDR3" => $this->PAYERADDR3,
            "PAYERPOSTCODE" => $this->PAYERPOSTCODE,
            "PAYERCITY" => $this->PAYERCITY,
            "PAYERPROVINCE" => $this->PAYERPROVINCE,
            "PAYERNPWP" => $this->PAYERNPWP,
            "PAYEREMAIL" => $this->PAYEREMAIL,
            "BILLBLOCK" => $this->BILLBLOCK,
            "DLVBLOCK" => $this->DLVBLOCK
        );

        $t_item = array(
            "ID" => $this->ID,
            "PRODUCTNO" => $this->PRODUCTNO,
            "SALESPRICE" => $this->SALESPRICE,
            "QUANTITY" => $this->QUANTITY,
            "UNIT" => $this->UNIT,
            "CURRENCY" => $this->CURRENCY,
            "SITE" => $this->SITE,
            "SITE_VENDOR" => $this->SITE_VENDOR,
            "CONDTYPE" => $this->CONDTYPE,
            "INDENT" => $this->INDENT,
            "VENDOR" => $this->VENDOR,
            "CONFIRMNO" => $this->CONFIRMNO,
            "STGE_LOC" => $this->STGE_LOC,
            "SERIALNUMBER" => $this->SERIALNUMBER
        );

        $parameter = array(
            "HEADER" => $header,
            "T_ITEM" => $t_item
        );

        $this->parameter = $parameter;
    }

    public function createSalesRegisterMember()
    {
        $nsq = new \Library\Nsq();
        $message = [
            "data" => json_encode($this->parameter),
            "message" => "salesRegisterMember",
            "invoice_no" => $this->invoice->getInvoiceNo()
        ];
        // LogHelper::log("test", "salesRegister message ". json_encode($message));
        $nsq->publishCluster('transactionSap', $message);
        return true;
    }
}
