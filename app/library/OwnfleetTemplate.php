<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use Helpers\CartHelper;
use Models\SupplierOwnfleetTemplate;
use Phalcon\Cache\Frontend\Data;
use Phalcon\Db as Database;

class OwnfleetTemplate
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllTemplate($params)
    {
        $templateList = array();
        $ownfleetModel = new \Models\SupplierOwnfleetTemplate();

        $filter = "";
        if(!empty($params['supplier_id'])){
            $filter = " AND sc.supplier_id = '".$params['supplier_id']."' ";
        }

        $sql  = "SELECT sc.ownfleet_template_id, sp.name AS 'supplier_name', sc.name AS 'template_name', sc.status FROM supplier_ownfleet_template sc, supplier sp WHERE ";
        $sql .= "sc.supplier_id = sp.supplier_id AND sc.status > -1";
        $sql .= $filter;
        $ownfleetModel->useReadOnlyConnection();
        $result = $ownfleetModel->getDi()->getShared($ownfleetModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );


        if($result->numRows() > 0){
            $templateList = $result->fetchAll();

            return $templateList;
        }
        else {
            $this->errorCode="RR302";
            $this->errorMessages="No ownfleet template has been found";
            return;
        }
    }

    public function getTemplateDetail($params = array())
    {
        $ownfleetDetail = array();
        $finalData = array();

        $ownfleetModel = new SupplierOwnfleetTemplate();

        $sql  = "SELECT sct.ownfleet_template_id, sct.supplier_id, sct.name, sct.status, scr.origin_region, scr.dest_district_code,";
        $sql .= "scr.rate, scr.is_flat, scr.carrier_code, scr.include_exclude FROM supplier_ownfleet_template sct, supplier_ownfleet_rate scr ";
        $sql .= "WHERE sct.ownfleet_template_id = scr.ownfleet_template_id AND sct.ownfleet_template_id =".$params['ownfleet_template_id'];
        $ownfleetModel->useReadOnlyConnection();
        $result = $ownfleetModel->getDi()->getShared($ownfleetModel->getConnection())->query($sql);

        if($result->numRows()>0){

            $result->setFetchMode(
                Database::FETCH_ASSOC
            );

            $ownfleetDetail = $result->fetchAll();

            $finalData['ownfleet_template_id'] = $ownfleetDetail[0]['ownfleet_template_id'];
            $finalData['supplier_id'] = $ownfleetDetail[0]['supplier_id'];
            $finalData['name'] = $ownfleetDetail[0]['name'];
            $finalData['status'] = $ownfleetDetail[0]['status'];

            foreach($ownfleetDetail as $key => $val){
                unset($ownfleetDetail[$key]['ownfleet_template_id']);
                unset($ownfleetDetail[$key]['supplier_id']);
                unset($ownfleetDetail[$key]['name']);
                unset($ownfleetDetail[$key]['status']);

                $finalData['shipping_rate'][$key]['origin_region'] = $val['origin_region'];
                $finalData['shipping_rate'][$key]['dest_district_code'] = $val['dest_district_code'];
                $finalData['shipping_rate'][$key]['rate'] = $val['rate'];
                $finalData['shipping_rate'][$key]['is_flat'] = $val['is_flat'];
                $finalData['shipping_rate'][$key]['carrier_code'] = $val['carrier_code'];
                $finalData['shipping_rate'][$key]['include_exclude'] = $val['include_exclude'];
            }

            return $finalData;
        }
        else {
            $this->errorCode = "RR001";
            $this->errorMessages = "ownfleet_template_id does not exist";
            return;
        }
    }

}