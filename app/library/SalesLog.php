<?php
namespace Library;

class SalesLog
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @var \Models\SalesLog
     */
    protected $salesLog;

    /**
     * @return \Models\SalesLog
     */
    public function getSalesLog()
    {
        return $this->salesLog;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {

    }

    public function getAllSalesLog($params = array())
    {
        $salesLogModel = new \Models\SalesLog();
        $resultSalesLog = $salesLogModel::query();

        foreach($params as $key => $val)
        {
            if($key == "sales_log_id") {
                $resultSalesLog->andWhere("sales_log_id = $val");
            }

            if($key == "sales_order_id") {
                $resultSalesLog->andWhere("sales_order_id = $val");
            }

            if($key == "invoice_id") {
                $resultSalesLog->andWhere("invoice_id = $val");
            }

            if($key == "admin_user_id") {
                $resultSalesLog->andWhere("admin_user_id = $val");
            }

            if($key == "flag") {
                $resultSalesLog->andWhere("flag = '$val'");
            }

            if($key == "actions") {
                $resultSalesLog->andWhere("actions like '%$val%'");
            }
        }

        if(!empty($params["order_by"])) {
            $resultSalesLog->orderBy($params["order_by"]);
        } else {
            $resultSalesLog->orderBy("log_time DESC");
        }

        $result = $resultSalesLog->execute();

        if(empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your sales log";
            $allSalesLog = array();
        } else {
            $allSalesLog = $result->toArray();
        }

        return $allSalesLog;
    }

    public function getSalesLogData($sales_log_id = "")
    {
        $allSalesLog = $this->getAllSalesLog(["sales_log_id" => $sales_log_id]);
        return $allSalesLog[0];
    }
}