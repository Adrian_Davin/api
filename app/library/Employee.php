<?php

namespace Library;

use Exception;
use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Helpers\EmployeeHelper;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class Employee
{

    protected $errorCode;
    protected $errorMessages;


    /**
     * @return mixed
     */

    public function __construct()
    {

    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;

        return $this;
    }
    
    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function loginNip($params = array())
    {
        if (empty($params["nip"]) || empty($params["password"])) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Payload tidak sesuai";
            return;
        }

        $sqlSrv = new \Library\SqlServer();
        $query = "SELECT Employee_Name, Employee_Email, birth_date,Site_Code_SAP,organization_Name FROM " . getenv("TABLE_USER_HC") . " WHERE Employee_ID = ?";
        $sqlParam = array($params["nip"]);
        $results = $sqlSrv->query($query, $sqlParam);
        $sqlSrv->close();

        // Result empty means that NIP doesn't registered yet.
        if (empty($results)) {
            $this->errorCode = 400;
            $this->errorMessages = "Data Anda belum terdaftar";
            return;
        }

        // Get Sister company by store code
        $sisterCompany = "";
        if (!empty($results['Site_Code_SAP'])) {
            $storeModel = new \Models\Store;
            $storeParam["conditions"] = "store_code = '". $results['Site_Code_SAP']."'";
            $storeResult = $storeModel->findFirst($storeParam);
            if ($storeResult){
                $sisterCompany =  $storeResult->Supplier->getSupplierAlias();
            }
        }

        // Get department bu from hris
        $employeeDeptList= array();
        $hrisEmployeeDept = "";
        $hrisDeptID = 0;
        $departmentBuModel = new \Models\DepartmentBu;
        if (!empty($sisterCompany) && strpos($results['organization_Name'], '-STORE OP-') !== false){
            // organization_Name format from hris ACE-STORE OP-Appliances-CIBUBUR TIMES SQUARE
            $organizationName = explode("-",$results['organization_Name']);
            $hrisEmployeeDept = $organizationName[2];
            $departmentBuParam["conditions"] = "name = '". $hrisEmployeeDept ."' and sister_company = '".$sisterCompany."'";
            $departmentBu = $departmentBuModel->findFirst($departmentBuParam);
            if ($departmentBu){
                $hrisDeptID = $departmentBu->getDepartementBUId();
                array_push($employeeDeptList,array(
                    'id_department_bu' =>  $departmentBu->getDepartementBUId(),
                    'name' => $departmentBu->getName()
                ));
            }
        }

        // Get employee custom department list
        $employeeDepartmentModel = new \Models\EmployeeDepartment;
        $employeeDepartmentParam["conditions"] = "nip = '". $params["nip"] ."'";
        $employeeDepartmentList = $employeeDepartmentModel->find($employeeDepartmentParam);
        if (!empty($employeeDepartmentList->count())) {
            foreach ($employeeDepartmentList as $employeeDepartment) {
                if ($hrisDeptID != $employeeDepartment->getDepartementBUId()){
                    $departmentBuParam["conditions"] = "id_department_bu = '". $employeeDepartment->getDepartementBUId() ."'";
                    $departmentBu = $departmentBuModel->findFirst($departmentBuParam);
                    if ($departmentBu){
                        array_push($employeeDeptList,array(
                            'id_department_bu' =>  $departmentBu->getDepartementBUId(),
                            'name' => $departmentBu->getName()
                        ));
                    }
                }
            }
        }

        // check from employee table
        $employeeModel = new \Models\Employee;
        $queryParam = "nip = '". $params["nip"] ."'";
        $employeeParam["conditions"] = $queryParam;
        $employeeFound = $employeeModel->findFirst($employeeParam);
        $employeeCustomEmail = "";
        $userPass = "";
        if ($employeeFound) {
            // Employee custom password
            $userPass = $employeeFound->getPassword();
            $params["password"] = hash('sha256', $params["password"]);
            $employeeCustomEmail = $employeeFound->getEmail();
        }else {
            // Default password template: <dd><mm><yy><2 digit akhir NIP>
            $day = $results["birth_date"]->format("d");
            $month = $results["birth_date"]->format("m");
            $year = $results["birth_date"]->format("y");
            $last2DigitNip = substr($params["nip"], strlen($params["nip"]) - 2, 2);
            $userPass = $day . $month . $year . $last2DigitNip;
        }

        // Validate password
        if ($params["password"] == $userPass) {
            return array(
                "nip" => $params["nip"],
                "name" => $results["Employee_Name"],
                "email" => $results["Employee_Email"],
                "site_code_sap" => $results["Site_Code_SAP"],
                "employee_custom_email" => $employeeCustomEmail,
                "employee_hris_department" => $hrisEmployeeDept,
                "employee_custom_department_list" => $employeeDeptList
            );
        } else {
            $this->errorCode = 400;
            $this->errorMessages = "Data Anda tidak valid";
            return;
        }
    }

    public function getEmployeeStats($params = array())
    {
        if (empty($params["nip"])) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Payload tidak sesuai";
            return;
        }
        // Retrieve total sales group by sales_order_id
        $querySales = "
            SELECT
                so.order_no,
                SUM((sii.selling_price - sii.discount_amount) * sii.qty_ordered) as subtotal,
                COALESCE((SELECT type FROM sales_employee_item sei WHERE sei.sales_order_item_id = sii.sales_order_item_id AND status = 10 AND nip = '" . $params["nip"] . "' ORDER BY created_at DESC LIMIT 1), '') as type,
                so.created_at
            FROM
                sales_order so
            JOIN sales_invoice si ON
                si.sales_order_id = so.sales_order_id
            JOIN sales_invoice_item sii ON
                sii.invoice_id = si.invoice_id
            WHERE
                COALESCE((SELECT sales_employee_item_id FROM sales_employee_item sei WHERE sei.sales_order_item_id = sii.sales_order_item_id AND status = 10 AND nip = '" . $params["nip"] . "' ORDER BY created_at DESC LIMIT 1), 0) > 0
                AND si.receipt_id is not null
                AND LEFT(so.order_no,4) = 'ODIR'
                AND sii.status_fulfillment != 'refunded'
                AND si.created_at >= DATE_FORMAT(NOW() ,'%Y-%m-01')
                AND si.created_at <= DATE_FORMAT(NOW() ,'%Y-%m-31')
            GROUP BY
                si.sales_order_id
         ";
        $salesInvoiceModel = new \Models\SalesInvoice;
        $dbResult = $salesInvoiceModel->getDi()->getShared("dbReader")->query($querySales);
        $dbResult->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $salesResults = $dbResult->fetchAll();
        $totalSales = 0;
        $totalOrder = count($salesResults);
        $salesDetailsList = array();
        // Because we collect the sales based on grouping by sales_order_id then 
        // we need to calculate the totalSales again for this specific NIP
        // and then we also have to count the rows to get the total order
        foreach ($salesResults as $sr) {
            $totalSales += (int) $sr["subtotal"];
            $salesDetailsList[] = array(
                'order_no' => $sr["order_no"],
                'type' => $sr["type"],
                'subtotal' => (int) $sr["subtotal"],
                'created_at' => $sr["created_at"],
            );
        }

        $result = array(
            "total_sales" => $totalSales,
            "total_order" => $totalOrder,
            "sales_details" => $salesDetailsList,
        );
        return $result;
    }

    public function getEmployeeDataByNip($params = array())
    {
        if (empty($params["nip"])) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return;
        }

        $whereQueryStatement = " WHERE Employee_ID = ?";
        $sqlQueryParameters = array($params["nip"]);

        if (!empty($params["business_unit"])) {
            $sapCompanyId = EmployeeHelper::getSAPCompanyIDByBusinessUnitCode($params["business_unit"]);
            $whereQueryStatement = " WHERE Employee_ID = ? AND sapCompID = ?";
            $sqlQueryParameters = array($params["nip"], $sapCompanyId);
        }

        $sqlSrv = new \Library\SqlServer();
        $query = "SELECT Employee_Name, Employee_Email, Site_Code_SAP, Site_Code_Legacy,organization_Name FROM " . getenv("TABLE_USER_HC") . $whereQueryStatement;
        $sqlParam = $sqlQueryParameters;
        $results = $sqlSrv->query($query, $sqlParam);
        $sqlSrv->close();

        // Result empty means that NIP doesn't registered yet.
        if (empty($results)) {
            $this->errorCode = 400;
            $this->errorMessages = "Data Anda belum terdaftar";
            return;
        }

        // Get Sister company by store code
        $sisterCompany = "";
        if (!empty($results['Site_Code_SAP'])) {
            $storeModel = new \Models\Store;
            $storeParam["conditions"] = "store_code = '". $results['Site_Code_SAP']."'";
            $storeResult = $storeModel->findFirst($storeParam);
            if ($storeResult){
                $sisterCompany =  $storeResult->Supplier->getSupplierAlias();
            }
        }

        // Get department bu from hris
        $employeeDeptList= array();
        $hrisEmployeeDept = "";
        $hrisDeptID = 0;
        $departmentBuModel = new \Models\DepartmentBu;
        if (!empty($sisterCompany) && strpos($results['organization_Name'], '-STORE OP-') !== false){
            // organization_Name format from hris ACE-STORE OP-Appliances-CIBUBUR TIMES SQUARE
            $organizationName = explode("-",$results['organization_Name']);
            $hrisEmployeeDept = $organizationName[2]; 
            $departmentBuParam["conditions"] = "name = '". $hrisEmployeeDept ."' and sister_company = '".$sisterCompany."'";
            $departmentBu = $departmentBuModel->findFirst($departmentBuParam);
            if ($departmentBu){
                $hrisDeptID = $departmentBu->getDepartementBUId();
                array_push($employeeDeptList,array(
                    'id_department_bu' =>  $departmentBu->getDepartementBUId(),
                    'name' => $departmentBu->getName()
                ));
            }
        }

        // Get employee custom department list
        $employeeDepartmentModel = new \Models\EmployeeDepartment;
        $employeeDepartmentParam["conditions"] = "nip = '". $params["nip"] ."'";
        $employeeDepartmentList = $employeeDepartmentModel->find($employeeDepartmentParam);
        if (!empty($employeeDepartmentList->count())) {
            foreach ($employeeDepartmentList as $employeeDepartment) {
                if ($hrisDeptID != $employeeDepartment->getDepartementBUId()){
                    $departmentBuParam["conditions"] = "id_department_bu = '". $employeeDepartment->getDepartementBUId() ."'";
                    $departmentBu = $departmentBuModel->findFirst($departmentBuParam);
                    if ($departmentBu){
                        array_push($employeeDeptList,array(
                            'id_department_bu' =>  $departmentBu->getDepartementBUId(),
                            'name' => $departmentBu->getName()
                        ));
                    }
                }
            }
        }

        return array(
            "nip" => $params["nip"],
            "name" => $results["Employee_Name"],
            "email" => $results["Employee_Email"],
            "site_code_sap" => $results["Site_Code_SAP"],
            "site_code_legacy" => $results["Site_Code_Legacy"],
            "employee_hris_department" => $hrisEmployeeDept,
            "employee_custom_department_list" => $employeeDeptList
        );
    }

    public function checkEmployee($params = array()) {
        // Payload
        // {
        //     "nip" : "165677", may empty
        //     "email" : "ruparupadev@gmail.com" may empty
        // }
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Payload tidak sesuai";
            return;
        }

        // Init Employee Model
        $responseResult = array(
            'is_exist' => false
        );
        $employeeModel = new \Models\Employee;
        $queryParam = "";

        // Query param (nip or email)
        foreach($params as $key => $val) {
            if (!empty($params[$key])){
                $queryParam = $key. "='". $val ."'";
            }
        }

        $employeeParam["conditions"] = $queryParam;
        $employeeFound = $employeeModel->findFirst($employeeParam);
        if (!empty($employeeFound)) {
            $responseResult["nip"] = $employeeFound->getNip();
            $responseResult["email"] = $employeeFound->getEmail();
            $responseResult["is_exist"] = true;
        }

        return $responseResult;
    }

    public function generateEmployeeOtp($params = array()){ 
        // Payload
        // {
        //     "nip": "165677",
        //     "email :"ruparupadev@gmail.com",
        // }
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Payload tidak sesuai";
            return false;
        }

        $requiredParam = ["nip", "email","action"];
        foreach ($requiredParam as $value) {
            if (empty($params[$value])) {
                $this->errorCode = 400;
                $this->errorMessages = "Payload tidak sesuai";
                return;
            }
        }

        // Validate and change action's value to bahasa
        $translatedAction = "";
        switch ($params["action"]) {                
            case "change-password":
                $translatedAction = "perubahan kata sandi";
                break;
            case "forgot-password":
                $translatedAction = "mengatur ulang kata sandi";
                break;
            default:
                $this->errorCode = 400;
                $this->errorMessages = "Action tidak ditemukan";
                return false;
        }

        // Get employee name from HRIS
        $sqlSrv = new \Library\SqlServer();
        $hrisQuery = "SELECT Employee_Name FROM " . getenv("TABLE_USER_HC") . " WHERE Employee_ID = ?";
        $sqlParam = array($params["nip"]);
        $hrisResults = $sqlSrv->query($hrisQuery, $sqlParam);
        $sqlSrv->close();

        // Result empty means that NIP doesn't registered yet.
        if (empty($hrisResults)) {
            $this->errorCode = 400;
            $this->errorMessages = "Data anda belum terdaftar";
            return;
        }

        // Init model employee_otp
        $responseResult = array();
        $employeeOtpData = [];
        $otpNumber = GeneralHelper::generateRandomNumber(6);
        $validUntil = date('Y-m-d H:i:s', strtotime('+5 minute'));
        
        $employeeOtpModel = new \Models\EmployeeOtp();
        $employeeOtpParam["conditions"] = "nip = '". $params["nip"] ."' AND email = '". $params["email"] ."'";
        $employeeOtpFound = $employeeOtpModel->findFirst($employeeOtpParam);
        // If employee otp exists add existing id
        if (!empty($employeeOtpFound)) {
            $employeeOtpData["id"] = $employeeOtpFound->getId();
        }
        $employeeOtpData["email"] = $params["email"];
        $employeeOtpData["nip"] = $params["nip"];
        $employeeOtpData["access_code"] = $otpNumber;
        $employeeOtpData["valid_until"] = $validUntil;

        $employeeOtpModel->setFromArray($employeeOtpData);
        if (!$employeeOtpModel->saveData("generate_employee_otp")) {
            $this->errorCode = 500;
            $this->errorMessages = "Gagal mengirim OTP";
            return false;
        }

        // Init email library to send otp notification
        $emailParam = array(
            "firstname" => $hrisResults['Employee_Name'],
            "access_code" => $otpNumber,
            "action" => $translatedAction,
            "valid_until" => $validUntil
        );
        $emailHelper = new \Library\Email();
        $emailHelper->setEmailSender();
        $emailHelper->setEmailReceiver($params['email']);
        $emailHelper->setTemplateModel($emailParam);
        $templateID = \Helpers\GeneralHelper::getTemplateId("cust_request_otp_via_email", "ODI");
        if (!$templateID) {
            $this->errorCode = 500;
            $this->errorMessages = "Gagal mengirim OTP";
            return false;
        }

        $emailHelper->setEmailTag("cust_request_otp_via_email");
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode("ODI");
        $emailHelper->send();
        
        $responseResult["message"] = getenv("ENVIRONMENT") != "production" ? $otpNumber : "Kode OTP berhasil dikirimkan";

        return $responseResult;
    }

    public function validateEmployeeOtp($params = array()){   
        // Payload
        // {
        //     "nip": "165677",
        //     "email :"ruparupadev@gmail.com",
        //     "access_code" : "123456"
        // }
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Payload tidak sesuai";
            return false;
        }

        $requiredParam = ["nip", "email", "access_code"];
        foreach ($requiredParam as $value) {
            if (empty($params[$value])) {
                $this->errorCode = 400;
                $this->errorMessages = "Payload tidak sesuai";
                return;
            }
        }

        // Init EmployeeOtp Model to get employee otp record
        $responseResult = array();
        $dateNow = date("Y-m-d H:i:s");
        $employeeOtpModel = new \Models\EmployeeOtp;
        
        $employeeOtpParam["conditions"] = "nip = '". $params["nip"] ."' AND email = '". $params["email"] ."' AND access_code ='".$params["access_code"]."'";
        $employeeOtpFound = $employeeOtpModel->findFirst($employeeOtpParam);
        if (empty($employeeOtpFound)) {
            $this->errorCode = 400;
            $this->errorMessages = "Kode OTP tidak valid";
            return;
        }

        // Validate otp valid until time 
        if($employeeOtpFound->getValidUntil() < $dateNow){
            $this->errorCode = 400;
            $this->errorMessages = "Kode OTP tidak valid";
            return;
        }
        // Delete validated otp record
        $employeeOtpFound->delete();

        $responseResult["nip"] = $params["nip"];
        $responseResult['email'] = $params["email"];
        $responseResult['status'] = "valid";

        return $responseResult;
    }

    public function upsertEmployee($params = array()){  
        // Payload
        // {
        //     "nip": "165677",
        //     "email :"ruparupadev@gmail.com",
        //     "password" : "123456"
        // }
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Payload tidak sesuai";
            return false;
        }

        $requiredParam = ["nip"];
        foreach ($requiredParam as $value) {
            if (empty($params[$value])) {
                $this->errorCode = 400;
                $this->errorMessages = "Payload tidak sesuai";
                return;
            }
        }

        // Only take filled param as upsertParam
        $upsertParam = [];
        foreach($params as $key => $val) {
            if (!empty($params[$key])){
                $upsertParam[$key] = $val;
            }
        }

        // Validate password length if exist
        if (!empty($upsertParam["password"]) && strlen($upsertParam["password"]) < 5) {
            $this->errorCode = '400';
            $this->errorMessages = 'Panjang kata sandi minimal 5 karakter';
            return false;
        }

        // Hash password if exist
        if (!empty($upsertParam["password"])) {
            $upsertParam["password"] = hash('sha256', $upsertParam["password"]);
        }

        // Init Employee model
        $responseResult = array();
        $employeeModel = new \Models\Employee;
        $employeeParam["conditions"] = "nip = '". $upsertParam["nip"] ."'";
        $employeeFound = $employeeModel->findFirst($employeeParam);
        $employeeModel->setFromArray($upsertParam);
        if (!empty($employeeFound)) {
            // Override upsert payload if employee already exist
            $employeeModel->setId($employeeFound->getId());
        }

        // Catch any error if upsert saveData operation failed
        try {
            $upsertStatus = $employeeModel->saveData("employee");
        } catch (\Exception $e) {
            LogHelper::log("upsertEmployee", sprintf("Failed upsert employee payload : %s error : %s", json_encode($upsertParam),$e->getMessage()),"error");
            $this->errorCode = '500';
            $this->errorMessages = "Gagal menyimpan data employee";
            return false;
        }

        $responseResult["nip"] = $params["nip"];
        $responseResult["status"] = "upserted";
        
        return $responseResult;
    }

    public function upsertEmployeeDepartment($params = array()){  
        // Payload
        // {
        //     "nip": "165677",
        //     "department_list" :[
        //         {
        //           "id_department_bu": "1"
        //         }
        //      ]
        // }
        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Payload tidak sesuai";
            return false;
        }

        $requiredParam = ["nip"];
        foreach ($requiredParam as $value) {
            if (empty($params[$value])) {
                $this->errorCode = 400;
                $this->errorMessages = "Payload tidak sesuai";
                return;
            }
        }

        // Init employee department model
        $responseResult = array();
        $employeeDepartmentModel = new \Models\EmployeeDepartment;
        $employeeDepartmentParam["conditions"] = "nip = '". $params["nip"] ."'";
        $employeeDepartmentList= $employeeDepartmentModel->find($employeeDepartmentParam);
        if(!empty($employeeDepartmentList->count())){
            // delete existing employee department
            $employeeDepartmentList->delete();
        }

        // insert new employee department
        foreach ($params["department_list"] as $employeeDepartment) {
            $employeeDepartmentUpdateModel = new \Models\EmployeeDepartment;
            $employeeDepartmentUpdateModel->setFromArray(array(
                'nip' => $params["nip"],
                'id_department_bu' =>  $employeeDepartment['id_department_bu'],
            ));
            $employeeDepartmentUpdateModel->save();
        }

        $responseResult["nip"] = $params["nip"];
        $responseResult["status"] = "upserted";

        return $responseResult;
    }
 // end of library
}
