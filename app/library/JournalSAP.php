<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 3/3/2017
 * Time: 9:56 AM
 */

namespace Library;

use Phalcon\Db as Database;

class JournalSAP
{
    protected $werks;
    protected $vorgdatum;
    protected $bonnummer;
    protected $vorgangart;
    protected $docnum;
    protected $company_code = 'ODI';
    protected $invoiceNo;

    private $_is_balance = false;

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    /**
     * @var \Models\SalesInvoiceSap
     */
    protected $invoiceSap;

    /**
     * @var \Models\SalesOrder
     */
    protected $sales_order;

    /**
     * @var \Models\SalesruleVoucher
     */
    protected $voucher;

    /**
     * @var \Library\JournalSAPItem\Collection
     */
    protected $items = array();

    protected $gift_cards_items = array();

    const VOUCHER_SOURCE_COIN_EXCHANGE_TO_PRODUCT = "coin-exchange-to-product";
    const ORDER_NO_PREFIX_ODI = "ODI";
    const VOUCHER_MARKETING_BUDGET = "marketing_budget";
    const VOUCHER_MERCHANDISE_BUDGET = "merchandise_budget";

    /**
     * @return string
     */
    public function getCompanyCode(): string
    {
        return $this->company_code;
    }

    /**
     * @param string $company_code
     */
    public function setCompanyCode(string $company_code)
    {
        $this->company_code = $company_code;
    }


    public function __construct($invoice = null, $sales_order = null, $invoiceSap = null)
    {
        if (!empty($invoice)) {
            $this->invoice = $invoice;
            $this->company_code = $invoice->getCompanyCode();
        }

        if (!empty($sales_order)) {
            $this->sales_order = $sales_order;
            $this->company_code = $sales_order->getCompanyCode();
        }

        if (!empty($invoiceSap)) {
            $this->invoiceSap = $invoiceSap;
            $this->company_code = "ODI";
        }
    }

    /**
     * @param \Models\SalesInvoice $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @param \Models\SalesInvoiceSap $invoiceSap
     */
    public function setInvoiceSap($invoiceSap)
    {
        $this->invoiceSap = $invoiceSap;
    }

    /**
     * @param \Models\SalesOrder $sales_order
     */
    public function setSalesOrder($sales_order)
    {
        $this->sales_order = $sales_order;
    }

    /**
     * @param \Models\SalesruleVoucher $voucher
     */
    public function setVoucher($voucher)
    {
        $this->voucher = $voucher;
    }

    /**
     * @return mixed
     */
    public function getBonnummer()
    {
        return $this->bonnummer;
    }

    /**
     * @param mixed $bonnummer
     */
    public function setBonnummer($bonnummer)
    {
        $this->bonnummer = $bonnummer;
    }

    /**
     * @param bool $is_balance
     */
    public function setIsBalance($is_balance)
    {
        $this->_is_balance = $is_balance;
    }

    /**
     * Get the value of vorgdatum
     */ 
    public function getVorgdatum()
    {
        return $this->vorgdatum;
    }

    /**
     * Set the value of vorgdatum
     *
     * @return  self
     */ 
    public function setVorgdatum($vorgdatum)
    {
        $this->vorgdatum = $vorgdatum;

        return $this;
    }

    public function setFromArray($data_array = array())
    {
        if (isset($data_array['b2c_journal_header'])) {
            foreach ($data_array['b2c_journal_header'] as $keyHeader => $valHeader) {
            }
        }
    }

    public function prepareHeader()
    {
        if ($this->invoice) {
            $postingDate = date_format(date_create($this->invoice->getCreatedAt()), "Ymd");
        } else if ($this->voucher) {
            $postingDate = date_format(date_create($this->voucher->getCreatedAt()), "Ymd");
        } else {
            $postingDate = date("Ymd");
        }

        switch ($this->company_code) {
            case 'AHI':
                $this->werks = "A300";
                break;
            default:
                $this->werks = "O300";
                break;
        }

        if ($this->invoice) {
            $this->bonnummer = $this->invoice->SalesOrder->getOrderNo();
            //if b2b interco concat bonnumer with ponumber
            if ($this->invoice->SalesOrder->getOrderType() == 'B2B') {
                $this->concatPoNumber();
            }
        } else if ($this->sales_order) {
            $this->bonnummer = $this->sales_order->getOrderNo();
            //if b2b interco concat bonnumer with ponumber
            if ($this->sales_order->getOrderType() == 'B2B') {
                $this->concatPoNumber();
            }
        } else if ($this->voucher) {
            $this->bonnummer = $this->voucher->getVoucherCode();
        } else if ($this->invoiceSap) {
            $this->bonnummer = $this->invoiceSap[0]['order_no'];
        } else {
            \Helpers\LogHelper::log("create_journal", "Order number not found", "error");
            return false;
        }


        $this->vorgdatum = $postingDate;

        return true;
    }

    // TODO: adjust with new cart
    public function concatPoNumber()
    {
        // $mongoDb = new Mongodb();
        // $mongoDb->setCollection("cart");
        // $this->collectionCart = $mongoDb->getCollection();
        // $cart = $this->collectionCart->findOne(['reserved_order_no' => $this->bonnummer]);

        // if($cart){
        //     if(isset($cart['po']['number'])){
        //         $this->bonnummer = $this->bonnummer.'|'.$cart['po']['number'];
        //     }
        // }
    }

    public function createRedeemJournal($voucherCode, $companyID = "", $type = "")
    {
        $voucherModel = new \Models\SalesruleVoucher();
        $params = [
            'conditions' => "voucher_code = '" . $voucherCode . "'"
        ];
        $voucherResult = $voucherModel->findFirst($params);

        if ($type == "pos_voucher") {
            $companyID = $voucherResult->getNote();
            
            $klvGroup = array("CORP", "KLG");
            if (in_array($companyID, $klvGroup)) {
                $companyID = "KLV";
            }
        }

        // Create journal redeem
        $this->setVoucher($voucherResult);
        $this->setCompanyCode($voucherResult->getCompanyCode());

        $prepareStatus = $this->prepareRedeemJournal(
            $companyID, 
            $voucherResult->getVoucherCode()
        );
        if ($prepareStatus) {
            $this->createJournal("redeem");
        }
        
        return true;
    }

    public function prepareRedeemJournalGroup(&$createRedeemJournalItems, $voucherCode, $voucherAmount, $companyID = "", $type = "")
    {
        $this->vorgangart = !empty($_ENV['VORGANGART_REDEEM']) ? $_ENV['VORGANGART_REDEEM'] : "3601";

        if ($type == "pos_voucher") {
            if (isset($companyID)) {
                if (isset($createRedeemJournalItems[$companyID][0])) {
                    $sumVoucher = false;
                    foreach ($createRedeemJournalItems[$companyID] as $index => $value) {
                        if ($index == 0) {
                            $createRedeemJournalItems[$companyID][$index]["WRBTR"] += round($voucherAmount);
                        } else if ($value["ZUONR"] == $voucherCode) {
                            $createRedeemJournalItems[$companyID][$index]["WRBTR"] += round($voucherAmount);
                            $sumVoucher = true;
                        }
                    }

                    if (!$sumVoucher) {
                        array_push($createRedeemJournalItems[$companyID],[
                            "WAERS" => "IDR",
                            "POSNR" => 2,
                            "ZUONR" => $voucherCode,
                            "WRBTR" => round($voucherAmount),     
                        ]);
                    }
                } else {
                    $createRedeemJournalItems[$companyID][0] = [
                        "WAERS" => "IDR",
                        "POSNR" => 1,
                        "ZUONR" => $companyID,
                        "WRBTR" => round($voucherAmount),
                    ];

                    $createRedeemJournalItems[$companyID][1] = [
                        "WAERS" => "IDR",
                        "POSNR" => 2,
                        "ZUONR" => $voucherCode,
                        "WRBTR" => round($voucherAmount),
                    ];
                }   
            }            
        }
        
        return true;
    }

    public function prepareRedeemJournal($company_id = "", $voucher_code = "")
    {
        if (empty($company_id)) {
            \Helpers\LogHelper::log("create_journal", "Create journal for redeem failed, empty company id", "error");
            return false;
        } else if ($company_id == "SLM" || $company_id == "HCI"){
            $company_id = "HCIR";
        }
        
        $this->vorgangart = !empty($_ENV['VORGANGART_REDEEM']) ? $_ENV['VORGANGART_REDEEM'] : "3601";

        if (empty($voucher_code)) {
            $voucher_code = $company_id;
        }

        // get giftcard amount
        $POSNR[2] = intval($this->voucher->getVoucherAmount());

        switch ($this->company_code) {
            case 'AHI':
                $POSNR[1] = 0;
                $POSNR[3] = intval($this->voucher->getVoucherAmount());
                $this->_is_balance = ($POSNR[3] - $POSNR[2] == 0) ? true : false;
                break;
            default:
                $POSNR[1] = intval($this->voucher->getVoucherAmount());
                $POSNR[3] = 0;
                $this->_is_balance = ($POSNR[1] - $POSNR[2] == 0) ? true : false;
                break;
        }

        $items = array();
        for ($i = 1; $i <= 3; $i++) {
            switch ($i) {
                case 2:
                    $ZUONR = $voucher_code;
                    break;
                case 3:
                    $ZUONR = $voucher_code;
                    break;
                default:
                    $ZUONR  = $company_id;
                    break;
            }

            if ($this->company_code == 'AHI' && $i == 1) {
                continue;
            } else if ($this->company_code == 'ODI' && $i == 3) {
                continue;
            }

            $journalItem = array(
                "POSNR" => $i,
                "ZUONR" => $ZUONR,
                "WRBTR" => round($POSNR[$i]),
            );
            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;
        }

        $this->items = new \Library\JournalSAPItem\Collection($items);

        return true;
    }

    /**
     * BOPIS Journal
     * @param int $pickup_voucher_amount
     */
    public function preparePickupReverseJournal()
    {
        $this->vorgangart = "3608";

        $shippingAmount = round($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount());

        if ($shippingAmount < 0) {
            $shippingAmount = 0;
        }

        $giftCardUssage = round($this->invoice->getGiftCardsAmount());
        $discountAmount = round($this->invoice->getDiscountAmount());
        $mdrCustomer = 0;

        $invoiceItemList = $this->invoice->SalesInvoiceItem;
        if (!empty($invoiceItemList)) {
            foreach ($invoiceItemList as $invoiceItem) {
                $mdrItem = $invoiceItem->SalesOrderItemMdr;
                if (empty($mdrItem)) {
                    continue;
                }

                $itemMdrCustomer = $mdrItem->getValueCustomer();
                if ($itemMdrCustomer <= 0) {
                    continue;
                }

                $mdrCustomer += $itemMdrCustomer;
            }
        }

        $customerPaidWithCash = round(($this->invoice->getSubtotal() + $shippingAmount + $mdrCustomer) - $giftCardUssage - $discountAmount);

        // AR AMOUNT : Customer membayar sejumlah
        $POSNR[5] = ($customerPaidWithCash < 0) ? 0 : $customerPaidWithCash; // Dr. AR B2C (Customer)

        // nilai gift card yang terpakai + shipping GC
        $POSNR[4] = $giftCardUssage; // Dr. AR Clearing

        // selisih nilai harga barang dan pembayaran yang diproporsikan ke barang bila ada transport cost dibayar dengan gift card
        $POSNR[3] = 0;

        // mdr yang dibayarkan oleh customer
        $POSNR[7] = $mdrCustomer;

        // Voucher Pick Up (BOPIS) Amount
        $pickup_voucher_amount = round($this->invoice->getSubtotal() - $discountAmount);
        
        // Transport Cost Amount (for delivery from store process)
        $POSNR[6] = $shippingAmount;
        $POSNR[2] = $pickup_voucher_amount;
        $credit = intval($POSNR[3]) + intval($POSNR[2]) + intval($POSNR[6]) + intval($POSNR[7]);
        
        $flagReorder = false;
        $referenceOrderNo = $this->invoice->SalesOrder->getReferenceOrderNo();
        if (!empty($referenceOrderNo)) {
            $flagReorder = true;
        }

        if ($flagReorder && $POSNR[6] == 0) {
            $POSNR[1] = 0;
            $POSNR[4] = $pickup_voucher_amount;
            $POSNR[3] = 0;
            $POSNR[2] = $pickup_voucher_amount;
            $POSNR[5] = 0;
            $POSNR[6] = 0;
            $POSNR[7] = 0;

            $this->_is_balance = true;
        } else {
            $debet = intval($POSNR[5]) + intval($POSNR[4]);
            $this->_is_balance = ($debet - $credit == 0) ? true : false;
        }
        
        $items = array();

        // Marketplace
        $prefixSupplierAlias = substr($this->invoice->getSupplierAlias(),0,2);
        if($prefixSupplierAlias == 'MP') {
            $digitSupplierAlias = str_replace("MP","",$this->invoice->getSupplierAlias());
            $zounr4 = $digitSupplierAlias . $this->invoice->SalesOrder->getOrderNo();
        }else{
            if ($this->invoice->getStoreCode() == "F353") {
                $zounr4 = $this->invoice->getStoreCode() . $this->invoice->getReceiptId();
            } else if (
                    substr(trim($this->invoice->getStoreCode()), 0, 1) == "C" || 
                    substr(trim($this->invoice->getStoreCode()), 0, 1) == "F" || 
                    trim($this->invoice->getStoreCode()) == "A300" || 
                    trim($this->invoice->getStoreCode()) == "H300" ||
                    trim($this->invoice->getStoreCode()) == "S702"
                ) {
                $zounr4 = $this->invoice->getStoreCode() . $this->invoice->SalesOrder->getOrderNo();
            } else {
                $zounr4 = $this->invoice->getStoreCode() . $this->invoice->getPickupVoucher();
            }
        }
        
        for ($i = 2; $i <= 7; $i++) {
            $zuonr     = ($i == 2 || $i == 6 || $i == 7) ? $zounr4 : $this->invoice->SalesOrder->getOrderNo();

            $journalItem = array(
                "POSNR" => ($i == 6 || $i == 7) ? 2 : $i,
                "ZUONR" => $zuonr,
                "WRBTR" => round($POSNR[$i]),
            );
            
            if ($this->invoice->SalesOrder->getOrderType() == 'B2B') {
                if(strpos($this->invoice->SalesOrder->getOrderNo(), 'ODIS') !== false){
                    if($i == 5){
                        $journalItem = array(
                            "POSNR" => 20,
                            "ZUONR" => $zuonr,
                            "WRBTR" => round($POSNR[$i]),
                        );
                    }
                    
                }elseif(strpos($this->invoice->SalesOrder->getOrderNo(), 'ODIT') !== false){
                    if($i == 5){
                        $journalItem = array(
                            "POSNR" => 21,
                            "ZUONR" => $zuonr,
                            "WRBTR" => round($POSNR[$i]),
                        );
                    }
                }elseif(strpos($this->invoice->SalesOrder->getOrderNo(), 'ODIK') !== false){
                    if($i == 5){
                        $journalItem = array(
                            "POSNR" => 24,
                            "ZUONR" => $zuonr,
                            "WRBTR" => round($POSNR[$i]),
                        );
                    }
                }else{
                    $this->_is_balance = false;
                    break;
                }
            }

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;
        }

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }

    /**
     * BOPIS Journal
     * @param int $pickup_voucher_amount
     */
    public function preparePickupVoucherJournal($pickup_voucher_amount = 0)
    {
        $this->vorgangart = !empty($_ENV['VORGANGART_PICKUP_VOUCHER']) ? $_ENV['VORGANGART_PICKUP_VOUCHER'] : "3602";

        $shippingAmount = round($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount());

        if ($shippingAmount < 0) {
            $shippingAmount = 0;
        }

        $giftCardUssage = round($this->invoice->getGiftCardsAmount());
        $discountAmount = round($this->invoice->getDiscountAmount());
        $mdrCustomer = 0;

        $invoiceItemList = $this->invoice->SalesInvoiceItem;
        if (!empty($invoiceItemList)) {
            foreach ($invoiceItemList as $invoiceItem) {
                $mdrItem = $invoiceItem->SalesOrderItemMdr;
                if (empty($mdrItem)) {
                    continue;
                }

                $itemMdrCustomer = $mdrItem->getValueCustomer();
                if ($itemMdrCustomer <= 0) {
                    continue;
                }

                $mdrCustomer += $itemMdrCustomer;
            }
        }

        $customerPaidWithCash = round(($this->invoice->getSubtotal() + $shippingAmount + $mdrCustomer) - $giftCardUssage - $discountAmount);

        // AR AMOUNT : Customer membayar sejumlah
        $POSNR[1] = ($customerPaidWithCash < 0) ? 0 : $customerPaidWithCash; // Dr. AR B2C (Customer)

        // nilai gift card yang terpakai + shipping GC
        $POSNR[2] = $giftCardUssage; // Dr. AR Clearing

        // selisih nilai harga barang dan pembayaran yang diproporsikan ke barang bila ada transport cost dibayar dengan gift card
        $POSNR[3] = 0;

        // mdr yang dibayarkan oleh customer
        $POSNR[7] = $mdrCustomer;

        // Voucher Pick Up (BOPIS) Amount
        $pickup_voucher_amount = round($this->invoice->getSubtotal() - $discountAmount);
        
        // Transport Cost Amount (for delivery from store process)
        $POSNR[5] = $shippingAmount;
        switch ($this->company_code) {
            case 'AHI':
                $POSNR[4] = 0;
                $POSNR[6] = $pickup_voucher_amount;
                $credit = intval($POSNR[3]) + intval($POSNR[6]) + intval($POSNR[5]);
                break;
            default:
                $POSNR[4] = $pickup_voucher_amount;
                $POSNR[6] = 0;
                $credit = intval($POSNR[3]) + intval($POSNR[4]) + intval($POSNR[5]) + intval($POSNR[7]);
                break;
        }

        $flagReorder = false;
        $referenceOrderNo = $this->invoice->SalesOrder->getReferenceOrderNo();
        if (!empty($referenceOrderNo)) {
            $flagReorder = true;
        }

        if ($flagReorder && $POSNR[5] == 0) {
            $POSNR[1] = 0;
            $POSNR[2] = $pickup_voucher_amount;
            $POSNR[3] = 0;
            $POSNR[4] = $pickup_voucher_amount;
            $POSNR[5] = 0;
            $POSNR[6] = 0;
            $POSNR[7] = 0;

            $this->_is_balance = true;
        } else {
            $debet = intval($POSNR[1]) + intval($POSNR[2]);
            $this->_is_balance = ($debet - $credit == 0) ? true : false;
        }



        if ($giftCardUssage < 0) {
            \Helpers\LogHelper::log("journal_giftcard_minus", "Gift card amount, pada invoice, minus", "error");
            $notificationText = "Alert : Gift Card amount pada journal sales, minus : " . $this->invoice->getInvoiceNo();
            $params = [
                "channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                "username" => $_ENV['MARKETING_SLACK_USERNAME'],
                "text" => $notificationText,
                "icon_emoji" => ':robot_face:'
            ];

            // call queue for webhook slack
            $nsq = new \Library\Nsq();
            $message = [
                "data" => $params,
                "message" => "customerAlert"
            ];
            $nsq->publishCluster('transaction', $message);
            $this->_is_balance = false;
        }

        $items = array();
        // Marketplace
        $prefixSupplierAlias = substr($this->invoice->getSupplierAlias(),0,2);
        if($prefixSupplierAlias == 'MP') {
            $digitSupplierAlias = str_replace("MP","",$this->invoice->getSupplierAlias());
            $zounr4 = $digitSupplierAlias . $this->invoice->SalesOrder->getOrderNo();
        }else{
            if ($this->invoice->getStoreCode() == "F353") {
                $zounr4 = $this->invoice->getStoreCode() . $this->invoice->getReceiptId();
            } else if (
                    substr(trim($this->invoice->getStoreCode()), 0, 1) == "C" || 
                    substr(trim($this->invoice->getStoreCode()), 0, 1) == "F" || 
                    trim($this->invoice->getStoreCode()) == "A300" || 
                    trim($this->invoice->getStoreCode()) == "H300" ||
                    trim($this->invoice->getStoreCode()) == "S702"
                ) {
                $zounr4 = $this->invoice->getStoreCode() . $this->invoice->SalesOrder->getOrderNo();
            } else {
                $zounr4 = $this->invoice->getStoreCode() . $this->invoice->getPickupVoucher();
            }
        }

        $isKredivoJournal = ($this->invoice->SalesOrder->SalesOrderPayment->getType() == "credit_non_cc" && $this->invoice->SalesOrder->SalesOrderPayment->getMethod() == "kredivo");
        $isDanakiniJournal = ($this->invoice->SalesOrder->SalesOrderPayment->getType() == "credit_non_cc" && $this->invoice->SalesOrder->SalesOrderPayment->getMethod() == "danakini");

        for ($i = 1; $i <= 7; $i++) {
            $zuonr     = ($i == 4 || $i == 5 || $i == 7) ? $zounr4 : $this->invoice->SalesOrder->getOrderNo();

            if ($i == 6) {
                // Marketplace
                $prefixSupplierAlias = substr($this->invoice->getSupplierAlias(),0,2);
                if($prefixSupplierAlias == 'MP') {
                    $digitSupplierAlias = str_replace("MP","",$this->invoice->getSupplierAlias());
                    $zounr = $digitSupplierAlias . $this->invoice->SalesOrder->getOrderNo();
                }else{
                    // Chatime Phase 1
                    if ($this->invoice->getStoreCode() == "F353") {
                        $zounr = $this->invoice->getStoreCode() . $this->invoice->getReceiptId();
                    } else if (
                            substr(trim($this->invoice->getStoreCode()), 0, 1) == "C" || 
                            substr(trim($this->invoice->getStoreCode()), 0, 1) == "F" || 
                            trim($this->invoice->getStoreCode()) == "A300" || 
                            trim($this->invoice->getStoreCode()) == "H300" ||
                            trim($this->invoice->getStoreCode()) == "S702"
                        ) {
                        $zuonr = $this->invoice->getStoreCode() . $this->invoice->SalesOrder->getOrderNo();
                    } else {
                        $zuonr = $this->invoice->getStoreCode() . $this->invoice->getPickupVoucher();
                    }
                }
                
            }

            if ($this->company_code == 'AHI' && $i == 4) {
                continue;
            } else if ($this->company_code == 'ODI' && $i == 6) {
                continue;
            }

            $journalItem = array(
                "POSNR" => ($i == 5 || $i == 7) ? 4 : $i,
                "ZUONR" => $zuonr,
                "WRBTR" => round($POSNR[$i]),
            );

            // if kredivo, change POSNR 1 to 20, then add KUNNR
            if ($isKredivoJournal && $i == 1){
                if (substr($this->invoice->SalesOrder->getOrderNo(), 0, 3) == "ODI"){
                    $kredivoKunnr = $_ENV['KREDIVO_KUNNR'];
                    $journalItem += array(
                        "KUNNR" => $kredivoKunnr,
                    );
                    $journalItem["POSNR"] = 20;
                }
            }

            // if danakini, change POSNR 1 to 20, then add KUNNR
            if ($isDanakiniJournal && $i == 1){
                if (substr($this->invoice->SalesOrder->getOrderNo(), 0, 3) == "ODI"){
                    $danakiniKunnr = $_ENV['DANAKINI_KUNNR'];
                    $journalItem += array(
                        "KUNNR" => $danakiniKunnr,
                    );
                    $journalItem["POSNR"] = 22;
                }
            }

            //if b2b add additional item, only add KUNNR in POSNR 1 and 3
            elseif ($this->invoice->SalesOrder->getOrderType() == 'B2B') {
                if (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIS") {
                    $this->vorgangart = "3612"; // Shopee
                } elseif (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIT") {
                    $this->vorgangart = "3613"; // Tokopedia
                } elseif (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIK") {
                    // TODO: tiktok development, need confirmation
                    $this->vorgangart = "3613"; // Tiktok
                }

                $billToId = $this->invoice->SalesOrder->getCompanyId();

                $kunnr = ($i == 1) ? $billToId : '';
                $journalItem += array(
                    "KOSTL" => '',
                    "KUNNR" => $kunnr,
                    "LIFNR" => ''
                );
            }

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;
        }

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }

    /**
     * SO CI Journal
     */
    public function prepareSalesJournal()
    {
        $this->vorgangart = !empty($_ENV['VORGANGART_PICKUP_VOUCHER']) ? $_ENV['VORGANGART_PICKUP_VOUCHER'] : "3602";

        $paymentType = $this->invoice->SalesOrder->SalesOrderPayment->getType();
        $paymentMethod = $this->invoice->SalesOrder->SalesOrderPayment->getMethod();

        if ($this->invoice->SalesOrder->getOrderType() == 'B2B') {
            if (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIS") {
                $this->vorgangart = "3612"; // Shopee
            } elseif (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIT") {
                $this->vorgangart = "3613"; // Tokopedia
            } elseif (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIK") {
                $this->vorgangart = "3620"; // tiktok
            }
        }

        $shippingAmount = round($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount());
        if ($shippingAmount < 0) {
            $shippingAmount = 0;
        }

        $discountAmount = round($this->invoice->getDiscountAmount());
        $sales_amount = round($this->invoice->getSubtotal() - $discountAmount);
        $giftCardUssage = round($this->invoice->getGiftCardsAmount());
        $customerPaidWithCash = round(($this->invoice->getSubtotal() + $shippingAmount) - $giftCardUssage - $discountAmount);

        // Validation Debet Credit
        $credit = intval($sales_amount) + intval($shippingAmount);
        $debet = intval($customerPaidWithCash) + intval($giftCardUssage);
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        // Validation Giftcards Usage
        if ($giftCardUssage < 0) {
            \Helpers\LogHelper::log("journal_giftcard_minus", "Gift card amount, pada invoice, minus", "error");
            $notificationText = "Alert : Gift Card amount pada journal sales, minus : " . $this->invoice->getInvoiceNo();
            $params = [
                "channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                "username" => $_ENV['MARKETING_SLACK_USERNAME'],
                "text" => $notificationText,
                "icon_emoji" => ':robot_face:'
            ];

            // call queue for webhook slack
            $nsq = new \Library\Nsq();
            $message = [
                "data" => $params,
                "message" => "customerAlert"
            ];
            $nsq->publishCluster('transaction', $message);
            $this->_is_balance = false;
        }

        if($this->_is_balance){
            // AR AMOUNT : Customer membayar sejumlah
            $POSNR_AR = 1;
            if($paymentType=="credit_non_cc" && $paymentMethod=="kredivo"){
                $POSNR_AR = 20;
            }elseif($paymentType=="credit_non_cc" && $paymentMethod=="danakini"){
                $POSNR_AR = 22;
            }
            $journalItem = array(
                "POSNR" => $POSNR_AR,
                "ZUONR" => $this->invoice->SalesOrder->getOrderNo(),
                "WRBTR" => ($customerPaidWithCash < 0) ? 0 : $customerPaidWithCash // Dr. AR B2C (Customer)
            );

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            // nilai gift card yang terpakai + shipping GC
            $journalItem = array(
                "POSNR" => 2,
                "ZUONR" => $this->invoice->SalesOrder->getOrderNo(),
                "WRBTR" => $giftCardUssage // Dr. AR B2C (Customer)
            );

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            // Sales
            $journalItem = array(
                "POSNR" => 4,
                "ZUONR" => str_replace('DC','',$this->invoice->getStoreCode()) . $this->invoice->SalesOrder->getOrderNo(),
                "WRBTR" => $sales_amount // customer deposit
            );

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            // Shipping Cost
            $journalItem = array(
                "POSNR" => 4,
                "ZUONR" => str_replace('DC','',$this->invoice->getStoreCode()) . $this->invoice->SalesOrder->getOrderNo(),
                "WRBTR" => $shippingAmount // customer deposit
            );
            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            $this->items = new \Library\JournalSAPItem\Collection($items);
        }
                
    }

    public function prepareSalesB2BTopJournal($invoiceSap = array(), $invoiceSapItems = array())
    {
        $this->vorgangart = !empty($_ENV['VORGANGART_PICKUP_VOUCHER']) ? $_ENV['VORGANGART_PICKUP_VOUCHER'] : "3602";

        if(isset($invoiceSap["invoice_no"])){
            if(!empty($invoiceSap["invoice_no"])){
                $this->invoiceNo = $invoiceSap["invoice_no"];
            }
        }

        $subtotal = 0;
        $shippingAmount = 0;
        $shippingAmountDiscount = 0;
        $giftCardsUsage = 0;
        $discountAmount = 0;

        foreach ($invoiceSapItems as $invoiceItem) {
            // Ignore "refunded" items
            if ($invoiceItem['status_fulfillment'] == 'refunded') {
                continue;
            }

            $qtyOrdered = $invoiceItem['qty_ordered'];

            $subtotal += (($invoiceItem['selling_price'] + $invoiceItem['handling_fee_adjust']) * $qtyOrdered);
            $shippingAmount += ($invoiceItem['shipping_amount'] * $invoiceItem['qty_ordered']);
            $shippingAmountDiscount += ($invoiceItem['shipping_discount_amount'] * $qtyOrdered);
            $giftCardsUsage += ($invoiceItem['gift_cards_amount'] * $qtyOrdered);
            $discountAmount += ($invoiceItem['discount_amount'] * $qtyOrdered);
        }

        $totalShippingAmount = round($shippingAmount - $shippingAmountDiscount);
        if ($totalShippingAmount < 0) {
            $totalShippingAmount = 0;
        }

        $giftCardsUsage = round($giftCardsUsage);
        $discountAmount = round($discountAmount);
        $sales_amount = round($subtotal - $discountAmount);
        $customerPaidWithCash = round(($subtotal + $totalShippingAmount) - $giftCardsUsage - $discountAmount);

        $credit = intval($sales_amount) + intval($totalShippingAmount);
        $debet = intval($customerPaidWithCash) + intval($giftCardsUsage);
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        if ($giftCardsUsage < 0) {
            \Helpers\LogHelper::log("journal_giftcard_minus", "Gift card amount, pada invoice, minus", "error");
            $notificationText = "Alert : Gift Card amount pada journal sales, minus : " . $this->invoice->getInvoiceNo();
            $params = [
                "channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                "username" => $_ENV['MARKETING_SLACK_USERNAME'],
                "text" => $notificationText,
                "icon_emoji" => ':robot_face:'
            ];

            // call queue for webhook slack
            $nsq = new \Library\Nsq();
            $message = [
                "data" => $params,
                "message" => "customerAlert"
            ];
            $nsq->publishCluster('transaction', $message);
            $this->_is_balance = false;
        }

        if($this->_is_balance){
            
            $POSNR_AR = 1;
            $journalItem = array(
                "POSNR" => $POSNR_AR,
                "ZUONR" => $invoiceSap['order_no'] . "|" .$invoiceSap['invoice_sap_no'],
                "WRBTR" => ($customerPaidWithCash < 0) ? 0 : $customerPaidWithCash // Dr. AR B2C (Customer)
            );

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;


            $journalItem = array(
                "POSNR" => 2,
                "ZUONR" => $invoiceSap['order_no'] . "|" .$invoiceSap['invoice_sap_no'],
                "WRBTR" => $giftCardsUsage // Dr. AR B2C (Customer)
            );

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            
            $journalItem = array(
                "POSNR" => 4,
                "ZUONR" => str_replace('DC','',$invoiceSap['store_code']) . $invoiceSap['order_no'] . "|" .$invoiceSap['invoice_sap_no'],
                "WRBTR" => $sales_amount // customer deposit
            );

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            
            $journalItem = array(
                "POSNR" => 4,
                "ZUONR" => str_replace('DC','',$invoiceSap['store_code']) . $invoiceSap['order_no'] . "|" .$invoiceSap['invoice_sap_no'],
                "WRBTR" => $totalShippingAmount // customer deposit
            );
            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            $this->items = new \Library\JournalSAPItem\Collection($items);
        }
                
    }

    /**
     * Incoming Journal - Komisi 0%
     */
    public function prepareIncomingJournal()
    {
        $this->vorgangart = !empty($_ENV['VORGANGART_INCOMING']) ? $_ENV['VORGANGART_INCOMING'] : "3602";
        $this->_is_balance = true;

        // Setup Variable
        $paymentCcType = $this->invoice->SalesOrder->SalesOrderPayment->getCcType();
        $paymentVaBank = $this->invoice->SalesOrder->SalesOrderPayment->getVaBank();
        $paymentType = $this->invoice->SalesOrder->SalesOrderPayment->getType();
        $paymentMethod = $this->invoice->SalesOrder->SalesOrderPayment->getMethod();
        
        $isOdiOrderNo = substr($this->invoice->SalesOrder->getOrderNo(), 0, 3) == "ODI";

        $debetIncoming = 8; // value between 10 - 14, depend on payment bank
        $creditARB2C = 7; 
        if(strtolower($paymentType) == "credit_card" || strtolower($paymentType) == "debit_card"){
            if(strtolower($paymentCcType) == "bca"){
                $debetIncoming = 12;
            }else if(strtolower($paymentCcType) == "bni"){
                $debetIncoming = 13;
            }else if(strtolower($paymentCcType) == "bri"){
                $debetIncoming = 14;
            }else if(strtolower($paymentCcType) == "mandiri"){
                $debetIncoming = 11;
            }else if(strtolower($paymentCcType) == "cimb"){
                $debetIncoming = 25;
            }
        }else if(strtolower($paymentType) == "bank_transfer"){
            if(strtolower($paymentVaBank) == "bca"){
                $debetIncoming = 12;
            }else{
                $debetIncoming = 10;
            }
        }else if(strtolower($paymentType) == "manual_transfer"){
            $debetIncoming = 10;
        }else if(strtolower($paymentType) == "e_money"){
            $debetIncoming = 10;
        }else if($isOdiOrderNo && $paymentType=="credit_non_cc" && $paymentMethod=="kredivo"){
            $debetIncoming = 10;
            $creditARB2C = 21;
        }else if($isOdiOrderNo && $paymentType=="credit_non_cc" && $paymentMethod=="danakini"){
            $debetIncoming = 10;
            $creditARB2C = 23;
        }
        else{
            $this->_is_balance = false; // If there have another payment type, then No Send Journal
        }

        $mdrCustomer = 0;

        $invoiceItemList = $this->invoice->SalesInvoiceItem;
        if (!empty($invoiceItemList)) {
            foreach ($invoiceItemList as $invoiceItem) {
                $mdrItem = $invoiceItem->SalesOrderItemMdr;
                if (empty($mdrItem)) {
                    continue;
                }

                $itemMdrCustomer = $mdrItem->getValueCustomer();
                if ($itemMdrCustomer <= 0) {
                    continue;
                }

                $mdrCustomer += $itemMdrCustomer;
            }
        }

        $shippingAmount = round($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount());

        if ($shippingAmount < 0) {
            $shippingAmount = 0;
        }

        $giftCardUssage = round($this->invoice->getGiftCardsAmount());
        $discountAmount = round($this->invoice->getDiscountAmount());
        $customerPaidWithCash = round(($this->invoice->getSubtotal() + $shippingAmount + $mdrCustomer) - $giftCardUssage - $discountAmount);

        $items = [];

        // AR AMOUNT : Customer membayar sejumlah - Dr. Incoming (Assignment A - ODI*)
        $journalItem = array(
            "POSNR" => $debetIncoming,
            "ZUONR" => $this->invoice->SalesOrder->getOrderNo(),
            "WRBTR" => ($customerPaidWithCash < 0) ? 0 : round($customerPaidWithCash)
        );

        if ($isOdiOrderNo && $paymentType=="credit_non_cc" && $paymentMethod=="kredivo"){
            $journalItem["KUNNR"] = $_ENV['KREDIVO_KUNNR'];
        }elseif ($isOdiOrderNo && $paymentType=="credit_non_cc" && $paymentMethod=="danakini"){
            $journalItem["KUNNR"] = $_ENV['DANAKINI_KUNNR'];
        }

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        // AR AMOUNT : Customer membayar sejumlah - Cr. AR B2C (Assignment A - ODI*)
        $journalItem = array(
            "POSNR" => $creditARB2C,
            "ZUONR" => $this->invoice->SalesOrder->getOrderNo(),
            "WRBTR" => ($customerPaidWithCash < 0) ? 0 : round($customerPaidWithCash)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }

    public function prepareIncomingB2BTopJournal($invoiceSap = array(), $invoiceSapItems = array())
    {
        $this->vorgangart = !empty($_ENV['VORGANGART_INCOMING']) ? $_ENV['VORGANGART_INCOMING'] : "3602";
        $this->_is_balance = true;

        if(isset($invoiceSap["invoice_no"])){
            if(!empty($invoiceSap["invoice_no"])){
                $this->invoiceNo = $invoiceSap["invoice_no"];
            }
        }

        // Setup Variable
        $debetIncoming = 10; // manual transfer
        $creditARB2C = 7;

        $subtotal = 0;
        $shippingAmount = 0;
        $shippingAmountDiscount = 0;
        $giftCardsUsage = 0;
        $discountAmount = 0;

        foreach ($invoiceSapItems as $invoiceItem) {
            // Ignore "refunded" items
            if ($invoiceItem['status_fulfillment'] == 'refunded') {
                continue;
            }

            $qtyOrdered = $invoiceItem['qty_ordered'];

            $subtotal += (($invoiceItem['selling_price'] + $invoiceItem['handling_fee_adjust']) * $qtyOrdered);
            $shippingAmount += ($invoiceItem['shipping_amount'] * $invoiceItem['qty_ordered']);
            $shippingAmountDiscount += ($invoiceItem['shipping_discount_amount'] * $qtyOrdered);
            $giftCardsUsage += ($invoiceItem['gift_cards_amount'] * $qtyOrdered);
            $discountAmount += ($invoiceItem['discount_amount'] * $qtyOrdered);
        }

        $totalShippingAmount = round($shippingAmount - $shippingAmountDiscount);
        if ($totalShippingAmount < 0) {
            $totalShippingAmount = 0;
        }

        $giftCardsUsage = round($giftCardsUsage);
        $discountAmount = round($discountAmount);
        $customerPaidWithCash = round(($subtotal + $totalShippingAmount) - $giftCardsUsage - $discountAmount);

        $items = [];

        // AR AMOUNT : Customer membayar sejumlah - Dr. Incoming (Assignment A - ODI*)
        $journalItem = array(
            "POSNR" => $debetIncoming,
            "ZUONR" => $invoiceSap['order_no'] . "|" .$invoiceSap['invoice_sap_no'],
            "WRBTR" => ($customerPaidWithCash < 0) ? 0 : round($customerPaidWithCash)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        // AR AMOUNT : Customer membayar sejumlah - Cr. AR B2C (Assignment A - ODI*)
        $journalItem = array(
            "POSNR" => $creditARB2C,
            "ZUONR" => $invoiceSap['order_no'] . "|" .$invoiceSap['invoice_sap_no'],
            "WRBTR" => ($customerPaidWithCash < 0) ? 0 : round($customerPaidWithCash)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }


    /**
     * Incoming Journal - Komisi 0%
     */
    public function prepareIncomingReverseJournal()
    {
        $this->vorgangart = "3608";
        $this->_is_balance = true;

        // Setup Variable
        $paymentCcType = $this->invoice->SalesOrder->SalesOrderPayment->getCcType();
        $paymentVaBank = $this->invoice->SalesOrder->SalesOrderPayment->getVaBank();
        $paymentType = $this->invoice->SalesOrder->SalesOrderPayment->getType();

        $kreditIncoming = 9; // value between 9 - 13, depend on payment bank
        $creditARB2C = 19; 
        if(strtolower($paymentType) == "credit_card"){
            if(strtolower($paymentCcType) == "bca"){
                $kreditIncoming = 11;
            }else if(strtolower($paymentCcType) == "bni"){
                $kreditIncoming = 12;
            }else if(strtolower($paymentCcType) == "bri"){
                $kreditIncoming = 13;
            }else if(strtolower($paymentCcType) == "mandiri"){
                $kreditIncoming = 10;
            }else if(strtolower($paymentCcType) == "cimb"){
                $kreditIncoming = 25;
            }
        }else if(strtolower($paymentType) == "bank_transfer"){
            if(strtolower($paymentVaBank) == "bca"){
                $kreditIncoming = 11;
            }else{
                $kreditIncoming = 9;
            }
        }else if(strtolower($paymentType) == "e_money"){
            $kreditIncoming = 9;
        }else{
            $this->_is_balance = false; // If there have another payment type, then No Send Journal
        }

        $shippingAmount = round($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount());

        if ($shippingAmount < 0) {
            $shippingAmount = 0;
        }

        $giftCardUssage = round($this->invoice->getGiftCardsAmount());
        $discountAmount = round($this->invoice->getDiscountAmount());
        $customerPaidWithCash = round(($this->invoice->getSubtotal() + $shippingAmount) - $giftCardUssage - $discountAmount);

        $items = [];

        // AR AMOUNT : Customer membayar sejumlah - Dr. Incoming (Assignment A - ODI*)
        $journalItem = array(
            "POSNR" => $kreditIncoming,
            "ZUONR" => $this->invoice->SalesOrder->getOrderNo(),
            "WRBTR" => ($customerPaidWithCash < 0) ? 0 : round($customerPaidWithCash)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        // AR AMOUNT : Customer membayar sejumlah - Cr. AR B2C (Assignment A - ODI*)
        $journalItem = array(
            "POSNR" => $creditARB2C,
            "ZUONR" => $this->invoice->SalesOrder->getOrderNo(),
            "WRBTR" => ($customerPaidWithCash < 0) ? 0 : round($customerPaidWithCash)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }

    /**
     * MDR Journal - Komisi 0%
     */
    public function prepareMDRJournal()
    {
        $this->vorgangart = !empty($_ENV['VORGANGART_MDR']) ? $_ENV['VORGANGART_MDR'] : "3602";
        $this->_is_balance = true;

        // Setup Variable
        $paymentCcType = $this->invoice->SalesOrder->SalesOrderPayment->getCcType();
        $paymentVaBank = $this->invoice->SalesOrder->SalesOrderPayment->getVaBank();
        $paymentType = $this->invoice->SalesOrder->SalesOrderPayment->getType();
        $paymentMethod = $this->invoice->SalesOrder->SalesOrderPayment->getMethod();
        $invoiceItems = $this->invoice->SalesInvoiceItem;
        
        $creditIncoming = 8;
        $debetSundry = 15; // value between 15 - 19, depend on payment bank
        if(strtolower($paymentType) == "credit_card" || strtolower($paymentType) == "debit_card"){
            if(strtolower($paymentCcType) == "bca"){
                $debetSundry = 17;
            }else if(strtolower($paymentCcType) == "bni"){
                $debetSundry = 18;
            }else if(strtolower($paymentCcType) == "bri"){
                $debetSundry = 19;
            }else if(strtolower($paymentCcType) == "mandiri"){
                $debetSundry = 16;
            }else if(strtolower($paymentCcType) == "cimb"){
                $debetSundry = 24;
            }
        }else if(strtolower($paymentType) == "bank_transfer"){
            if(strtolower($paymentVaBank) == "bca"){
                $debetSundry = 17;
            }else{
                $debetSundry = 15;
            }
        }else if(strtolower($paymentType) == "e_money"){
            $debetSundry = 15;
        }else if(strtolower($paymentType) == "credit_non_cc"){
            $debetSundry = 15;
        }else{
            $this->_is_balance = false; // If there have another payment type, then No Send Journal
        }

        // Calculate MDR
        $sundry = 0;
        $incoming = 0;

        //$invoiceLib = new \Library\Invoice();
        //$dataMDR = $invoiceLib->getMDRDetail($this->invoice->getInvoiceNo());
        //$sundry = $incoming = $dataMDR['value'];
        $value = 0;
        if(count($invoiceItems) > 0){
            foreach($invoiceItems as $rowItem){
                $getModel = new \Models\SalesInvoiceItem();
                $getModel->useReadOnlyConnection();
                $sql  = "SELECT value,mdr_type FROM sales_order_item_mdr WHERE sales_order_item_id = ".$rowItem->getSalesOrderItemId();
                $result = $getModel->getDi()->getShared($getModel->getConnection())->query($sql);
                $result->setFetchMode(Database::FETCH_ASSOC);
                $resultData = $result->fetch();
                $value += $resultData['value'];
            }
        }else{
            $this->_is_balance = false; // If there no any have invoice items
        }

        $sundry = $incoming = $value;
        
        $items = [];

        // Dr. Sundry Account (Assignment B - Pickup)
        $journalItem = array(
            "POSNR" => $debetSundry,
            "ZUONR" => $this->invoice->SalesOrder->getOrderNo(),
            "WRBTR" => ($sundry < 0) ? 0 : round($sundry)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        // Cr. Incoming (Assignment A - ODI*)
        // MDR Store
        $journalItem = array(
            "POSNR" => $creditIncoming,
            "ZUONR" => $this->invoice->getStoreCode() . $this->invoice->getPickupVoucher(),
            "WRBTR" => ($incoming < 0) ? 0 : round($incoming)
        );

        // MDR DC
        if (strpos($this->invoice->getStoreCode(), 'DC') !== false) {
            $journalItem = array(
                "POSNR" => $creditIncoming,
                "ZUONR" => str_replace('DC','',$this->invoice->getStoreCode()) . $this->invoice->SalesOrder->getOrderNo(),
                "WRBTR" => ($incoming < 0) ? 0 : round($incoming)
            );
        }

        // MDR Chatime
        if (substr(trim($this->invoice->getStoreCode()), 0, 1) == "F" || substr(trim($this->invoice->getStoreCode()), 0, 1) == "C") {
            $journalItem = array(
                "POSNR" => $creditIncoming,
                "ZUONR" => $this->invoice->getStoreCode() . $this->invoice->SalesOrder->getOrderNo(),
                "WRBTR" => ($incoming < 0) ? 0 : round($incoming)
            );
        }

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }

    /**
     * MDR Journal - Komisi 0%
     */
    public function prepareMDRReverseJournal()
    {
        $this->vorgangart = "3608";
        $this->_is_balance = true;

        // Setup Variable
        $paymentCcType = $this->invoice->SalesOrder->SalesOrderPayment->getCcType();
        $paymentVaBank = $this->invoice->SalesOrder->SalesOrderPayment->getVaBank();
        $paymentType = $this->invoice->SalesOrder->SalesOrderPayment->getType();
        $paymentMethod = $this->invoice->SalesOrder->SalesOrderPayment->getMethod();
        $invoiceItems = $this->invoice->SalesInvoiceItem;
        
        $creditIncoming = 7;
        $debetSundry = 14; // value between 14 - 18, depend on payment bank
        if(strtolower($paymentType) == "credit_card"){
            if(strtolower($paymentCcType) == "bca"){
                $debetSundry = 16;
            }else if(strtolower($paymentCcType) == "bni"){
                $debetSundry = 17;
            }else if(strtolower($paymentCcType) == "bri"){
                $debetSundry = 18;
            }else if(strtolower($paymentCcType) == "mandiri"){
                $debetSundry = 15;
            }else if(strtolower($paymentCcType) == "cimb"){
                $debetSundry = 24;
            }
        }else if(strtolower($paymentType) == "bank_transfer"){
            if(strtolower($paymentVaBank) == "bca"){
                $debetSundry = 16;
            }else{
                $debetSundry = 14;
            }
        }else if(strtolower($paymentType) == "e_money"){
            $debetSundry = 14;
        }else if(strtolower($paymentType) == "credit_non_cc") {
            $debetSundry = 14;
        }else{
            $this->_is_balance = false; // If there have another payment type, then No Send Journal
        }

        // Calculate MDR
        $sundry = 0;
        $incoming = 0;

        //$invoiceLib = new \Library\Invoice();
        //$dataMDR = $invoiceLib->getMDRDetail($this->invoice->getInvoiceNo());
        //$sundry = $incoming = $dataMDR['value'];
        $value = 0;
        if(count($invoiceItems) > 0){
            foreach($invoiceItems as $rowItem){
                $getModel = new \Models\SalesInvoiceItem();
                $getModel->useReadOnlyConnection();
                $sql  = "SELECT value,mdr_type FROM sales_order_item_mdr WHERE sales_order_item_id = ".$rowItem->getSalesOrderItemId();
                $result = $getModel->getDi()->getShared($getModel->getConnection())->query($sql);
                $result->setFetchMode(Database::FETCH_ASSOC);
                $resultData = $result->fetch();
                $value += $resultData['value'];
            }
        }else{
            $this->_is_balance = false; // If there no any have invoice items
        }

        $sundry = $incoming = $value;
        
        $items = [];

        // Dr. Sundry Account (Assignment B - Pickup)
        $journalItem = array(
            "POSNR" => $debetSundry,
            "ZUONR" => $this->invoice->SalesOrder->getOrderNo(),
            "WRBTR" => ($sundry < 0) ? 0 : round($sundry)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        // Cr. Incoming (Assignment A - ODI*)
        $journalItem = array(
            "POSNR" => $creditIncoming,
            "ZUONR" => $this->invoice->getStoreCode() . $this->invoice->getPickupVoucher(),
            "WRBTR" => ($incoming < 0) ? 0 : round($incoming)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }

    public function prepareRegeneratePickupVoucherJournal($pickup_voucher_amount = 0)
    {
        $this->vorgangart = !empty(getenv('VORGANGART_PICKUP_VOUCHER')) ? getenv('VORGANGART_PICKUP_VOUCHER') : "3602";

        // nilai amount pickup voucher yg baru (nilai didapat dari current complete item amount)
        $POSNR[2] = $POSNR[4] = $pickup_voucher_amount;

        $POSNR[1] = $POSNR[3] = $POSNR[5] = 0;

        $debet = intval($POSNR[2]);
        $credit = intval($POSNR[4]);
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        $items = array();
        $zounr4 = $this->invoice->getStoreCode() . $this->invoice->getPickupVoucher();
        for ($i = 1; $i <= 5; $i++) {
            $zuonr     = ($i == 4) ? $zounr4 : $this->invoice->SalesOrder->getOrderNo();

            if ($i == 6) {
                $zuonr = $this->invoice->getStoreCode() . $this->invoice->getPickupVoucher();
            }

            $journalItem = array(
                "POSNR" => $i,
                "ZUONR" => $zuonr,
                "WRBTR" => round($POSNR[$i]),
            );

            //if b2b add additional item, only add KUNNR in POSNR 1 and 3
            if ($this->invoice->SalesOrder->getOrderType() == 'B2B') {
                if (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIS") {
                    $this->vorgangart = "3612"; // Shopee
                } elseif (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIT") {
                    $this->vorgangart = "3613"; // Tokopedia
                } elseif (substr($this->invoice->SalesOrder->getOrderNo(), 0, 4) == "ODIK") {
                    // TODO: tiktok development, need confirm
                    $this->vorgangart = "3613"; // tiktok
                }

                $billToId = $this->invoice->SalesOrder->getCompanyId();

                $kunnr = ($i == 1) ? $billToId : '';
                $journalItem += array(
                    "KOSTL" => '',
                    "KUNNR" => $kunnr,
                    "LIFNR" => ''
                );
            }

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;
        }

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }

    public function prepareRefundVendorJournal($creditMemoData = array())
    {
        if (empty($creditMemoData)) {
            \Helpers\LogHelper::log("create_journal", "Credit memo data not found", "error");
            return false;
        }

        $this->vorgangart = "3608";

        $debet = intval($creditMemoData['shipping_amount'] + $creditMemoData['subtotal']); // Customer Deposit
        $credit = intval($creditMemoData['refund_cash']); // Refund as Cash
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        $POSNRdebet = 2;
        $POSNRcredit = 0;
        if (($this->sales_order->getOrderType() == 'B2B')){
            if(strpos($this->sales_order->getOrderNo(), 'ODIS') !== false){
                $POSNRcredit = 20;
            }elseif(strpos($this->sales_order->getOrderNo(), 'ODIT') !== false){
                $POSNRcredit = 21;
            }elseif(strpos($this->sales_order->getOrderNo(), 'ODIK') !== false){
                $POSNRcredit = 24;
            }else{
                $this->_is_balance = false;
            }
        }else{
            $this->_is_balance = false;
        }
        $items = [];

        // Dr. Sundry Account (Assignment B - Pickup)
        $journalItem = array(
            "POSNR" => $POSNRdebet,
            "ZUONR" => $creditMemoData["assignment_customer_deposit"],
            "WRBTR" => ($debet < 0) ? 0 : round($debet)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        // Cr. Incoming (Assignment A - ODI*)
        $journalItem = array(
            "POSNR" => $POSNRcredit,
            "ZUONR" => $this->sales_order->getOrderNo(),
            "WRBTR" => ($credit < 0) ? 0 : round($credit)
        );

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        $this->items = new \Library\JournalSAPItem\Collection($items);

        return true;
    }


    /**
     * Credit memo
     * @param array $creditMemoData
     * @return bool
     */
    public function prepareRefundJournal($creditMemoData = array())
    {
        if (empty($creditMemoData)) {
            \Helpers\LogHelper::log("create_journal", "Credit memo data not found", "error");
            return false;
        }

        $this->vorgangart = !empty($_ENV['VORGANGART_CREDIT_MEMO']) ? $_ENV['VORGANGART_CREDIT_MEMO'] : "3603";

        // POSNR 1 - cash against item (Total barang yang dibayar cash oleh customer & di mau direfund)
        $prorateGc = 0;
        foreach ($creditMemoData['invoices'] as $invoice) {
            $prorateGc += $invoice['gift_cards_amount'];
        }

        if ($this->sales_order->getGrandTotal() > 0) {
            $cashPaidByCustomer = 0;
            foreach ($creditMemoData['invoices'] as $invoice) {
                //$cashPaidByCustomer += ($invoice['subtotal'] + $invoice['shipping_amount']) - $invoice['gift_cards_amount'];
                $cashPaidByCustomer += ($invoice['subtotal'] - $invoice['gift_cards_amount']);
                //$cashPaidByCustomer += ($invoice['subtotal'] + ($invoice['shipping_amount'] - $invoice['shipping_discount_amount']) - $invoice['gift_cards_amount']);
            }
            $POSNR[1] = ceil($cashPaidByCustomer);
        } else {
            $POSNR[1] = 0;
        }
        $POSNR[2] = 0; // Penalty / Adjustment Fee
        $POSNR[3] = $creditMemoData['refund_as_gift_card']; // Refund as New GC
        $POSNR[4] = 0; // unused
        // @todo: fix temporary journal switching
        $POSNR[5] = $creditMemoData['customer_penalty'];
        $POSNR[6] = 0; // unused
        $POSNR[7] = 0; // unused
        $POSNR[8] = $creditMemoData['refund_cash']; // Refund as Cash

        // change to 0 based on https://ruparupa.atlassian.net/browse/FIN-17
        $POSNR[1] = 0;
        $POSNR[9] = 0;
        $POSNR[10] = $creditMemoData['shipping_amount']; // Refund Shipping
        $SUBTOTAL = round($invoice['subtotal']);

        $debet = intval($POSNR[1]) + intval($POSNR[10]) + intval($POSNR[9]) + intval($SUBTOTAL);
        $credit = intval($POSNR[2]) + intval($POSNR[3]) + $POSNR[5] + $POSNR[6] + $POSNR[7] + intval($POSNR[8]);
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        // If not balance probably rounding problem in PONSR 1
        if ($this->_is_balance === false) {
            $POSNR[1]--;

            // Recheck again if it alrady balance
            $debet = intval($POSNR[1]) + intval($POSNR[10]) + intval($POSNR[9]);
            $credit = intval($POSNR[2]) + intval($POSNR[3]) + $POSNR[5] + $POSNR[6] + $POSNR[7] + intval($POSNR[8]);
            $this->_is_balance = ($debet - $credit == 0) ? true : false;
        }

        for ($i = 1; $i <= 10; $i++) {
            switch ($i) {
                case 3:
                    $ZUONR = $creditMemoData['voucher_code'];
                    break;
                case 5:
                    $ZUONR = $creditMemoData['order_no'];
                    break;
                case 6:
                case 7:
                    $ZUONR = !empty($creditMemoData['voucher_code']) ? $creditMemoData['voucher_code'] : $creditMemoData['credit_memo_no'];
                    break;
                case 10:
                    $ZUONR = $creditMemoData['assignment_customer_deposit'];
                    break;
                default:
                    $ZUONR = $creditMemoData['order_no'];
                    break;
            }

            $journalItem = array(
                "POSNR" => $i,
                "ZUONR" => $ZUONR,
                "WRBTR" => round($POSNR[$i]),
            );

            //if b2b add additional item, only add KUNNR in POSNR 1 and 4, and add LIFNR in POSNR 8
            // if (($this->sales_order->getOrderType() == 'B2B') &&
            //     !((strpos($this->sales_order->getOrderNo(), 'ODIS') || strpos($this->sales_order->getOrderNo(), 'ODIT')) !== false)
            // ) {
            //     $billToId = $this->sales_order->getCompanyId();
            //     $vendorSapId = $billToId;
               
            //     $kunnr = ($i == 1 || $i == 4) ? $billToId : '';
            //     $lifnr = ($i == 8) ? $vendorSapId : '';
            //     $journalItem += array(
            //         "KOSTL" => '',
            //         "KUNNR" => $kunnr,
            //         "LIFNR" => $lifnr
            //     );
            // }

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            // double posnr 10 based on https://ruparupa.atlassian.net/browse/FIN-17
            if ($i == 10) {
                $journalItem = array(
                    "POSNR" => $i,
                    "ZUONR" => $ZUONR,
                    "WRBTR" => round($SUBTOTAL),
                );

                $journalItemLib = new \Library\JournalSAPItem();
                $journalItemLib->setFromArray($journalItem);
                $items[] = $journalItemLib;
            }
        }

        $this->items = new \Library\JournalSAPItem\Collection($items);

        return true;
    }

    public function prepareRefundB2BTopJournal($creditMemoData = array())
    {
        if (empty($creditMemoData)) {
            \Helpers\LogHelper::log("create_journal", "Credit memo data not found", "error");
            return false;
        }

        $this->vorgangart = !empty($_ENV['VORGANGART_CREDIT_MEMO']) ? $_ENV['VORGANGART_CREDIT_MEMO'] : "3603";

        $items = [];
        $debet = 0;
        $credit = 0;

        // Refund as Giftcards
        $journalItem = array(
            "POSNR" => 3,
            "ZUONR" => $creditMemoData['voucher_code'],
            "WRBTR" => ($creditMemoData["refund_as_gift_card"] < 0) ? 0 : round($creditMemoData["refund_as_gift_card"])
        );
        $credit += $journalItem["WRBTR"];

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        // Customer Pinalty
        $journalItem = array(
            "POSNR" => 5,
            "ZUONR" => $creditMemoData['order_no'],
            "WRBTR" => ($creditMemoData["customer_penalty"] < 0) ? 0 : round($creditMemoData["customer_penalty"])
        );
        $credit += $journalItem["WRBTR"];

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        // Refund Cash
        $journalItem = array(
            "POSNR" => 8,
            "ZUONR" => $creditMemoData['order_no'],
            "WRBTR" => ($creditMemoData['refund_cash'] < 0) ? 0 : round($creditMemoData['refund_cash'])
        );
        $credit += $journalItem["WRBTR"];

        $journalItemLib = new \Library\JournalSAPItem();
        $journalItemLib->setFromArray($journalItem);
        $items[] = $journalItemLib;

        foreach($creditMemoData["invoices"] as $rowInvoice){
            // Shipping
            $journalItem = array(
                "POSNR" => 10,
                "ZUONR" => $rowInvoice['assignment_customer_deposit']."|".$rowInvoice['invoice_sap_no'],
                "WRBTR" => ($rowInvoice['shipping_amount'] < 0) ? 0 : round($rowInvoice['shipping_amount'])
            );
            $debet += $journalItem["WRBTR"];

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;

            // Subtotal
            $journalItem = array(
                "POSNR" => 10,
                "ZUONR" => $rowInvoice['assignment_customer_deposit']."|".$rowInvoice['invoice_sap_no'],
                "WRBTR" => ($rowInvoice['subtotal'] < 0) ? 0 : round($rowInvoice['subtotal'])
            );
            $debet += $journalItem["WRBTR"];

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;
        }
        $this->items = new \Library\JournalSAPItem\Collection($items);
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        return true;
    }

    /**
     * Reorder
     * @param array $reorderData
     * @return bool
     */
    public function prepareReorderJournal($reorderData = array())
    {
        $usedGiftGC = $usedRegisGC = 0;

        $returnFunc = array(
            'status' => false,
            'usedGiftGC' => $usedGiftGC,
            'usedRegisGC' => $usedRegisGC
        );

        $referenceOrderNoDefault = "";
        if (isset($reorderData['order_no']) && !empty($reorderData['order_no'])) {
            $referenceOrderNoDefault = $reorderData['order_no'];
        }   

        if (empty($reorderData)) {
            \Helpers\LogHelper::log("monitoring_journal", "Cant send journal Credit memo, data not found", "error");
            return $returnFunc;
        } else if (!isset($reorderData['invoices'][0]['credit_memo_items'][0]['sku'])) {
            \Helpers\LogHelper::log("monitoring_journal", "Cant send journal Credit memo, item not found", "error");
            return $returnFunc;
        } else if (empty($referenceOrderNoDefault)) {
            \Helpers\LogHelper::log("monitoring_journal", "Cant send journal reference_order_no not found for credit_memo_no: " . $reorderData['credit_memo_no'], "error");
            return $returnFunc;
        }

        // get origin order
        $getModel = new \Models\SalesInvoice();
        $getModel->useReadOnlyConnection();
        $keywordRefOrderNo = $referenceOrderNo = $referenceOrderNoDefault;
        do {
            $sql  = "SELECT reference_order_no FROM sales_order WHERE order_no = '{$keywordRefOrderNo}' LIMIT 1";
            $result = $getModel->getDi()->getShared($getModel->getConnection())->query($sql);
            $result->setFetchMode(Database::FETCH_ASSOC);
            $resultData = $result->fetchAll();
            $keywordRefOrderNo = $resultData[0]['reference_order_no'];
            if ($keywordRefOrderNo != "") {
                $referenceOrderNo = $keywordRefOrderNo;
            }
        } while ($keywordRefOrderNo != "");

        $sql = "select so.subtotal as subtotal_order, so.discount_amount as discount_amount_order, so.gift_cards,
        (select sum(handling_fee_adjust * qty_ordered) from sales_order_item where sales_order_id = so.sales_order_id) as handling_fee_adjust
        from sales_order so where so.order_no = '$referenceOrderNo'";
        $result = $getModel->getDi()->getShared($getModel->getConnection())->query($sql);
        $result->setFetchMode(Database::FETCH_ASSOC);
        $resultData = $result->fetchAll();

        $giftcards = $resultData[0]['gift_cards'];
        $giftcardsArr =  array();
        if ($giftcards != '[]' && $giftcards != "") {
            $giftcardsArr = json_decode($giftcards, true);
        }

        $subtotalOrder = $resultData[0]['subtotal_order'];
        $discountAmountOrder = $resultData[0]['discount_amount_order'];
        $handlingFeeAdjust = $resultData[0]['handling_fee_adjust'];

        if ($giftcardsArr && count($giftcardsArr) > 0) {
            $notAllowedGC = array(getenv("SALESRULE_REDEEM"), getenv("SALESRULE_VOUCHER_PRODUCT"), getenv("SALESRULE_VOUCHER_PRODUCT_UNWITHDRAWN"));
            foreach ($giftcardsArr as $rowGC) {
                $voucherType = strval($rowGC['voucher_type']);
                if ($voucherType == "1") {
                    $usedRegisGC = $rowGC['voucher_amount_used'];
                } else if (!in_array($rowGC['rule_id'], $notAllowedGC)) {
                    $usedGiftGC += $rowGC['voucher_amount_used'];
                }
            }
        }

        $subtotalReorder = $reorderData['subtotal'];
        $totalVoucherMarketing = $usedRegisGC + $usedGiftGC;
        $arClearing = ($subtotalReorder / (($subtotalOrder + $handlingFeeAdjust) - $discountAmountOrder)) * $totalVoucherMarketing;

        $arPOSClearing = $subtotalReorder - $arClearing;

        $this->vorgangart = !empty($_ENV['VORGANGART_REORDER']) ? $_ENV['VORGANGART_REORDER'] : "3609";

        $POSNR[1] = 0;
        $POSNR[2] = $arClearing;
        $POSNR[3] = $arPOSClearing;
        $POSNR[4] = 0;
        $POSNR[5] = 0;
        $POSNR[6] = 0;
        $POSNR[7] = 0;
        $POSNR[8] = 0;
        $POSNR[9] = 0;
        $POSNR[10] = 0;
        $POSNR[11] = $subtotalReorder;

        $debet = intval($POSNR[2] + $POSNR[3]);
        $credit = intval($POSNR[11]);
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        for ($i = 1; $i <= 11; $i++) {
            // check if $i = 11 then using reorderData['voucher_code'] instead if order_no ticket: https://ruparupa.atlassian.net/browse/FIN-200
            $zuonr = ($i == 11) ? $reorderData['voucher_code'] : $reorderData['order_no'];

            $journalItem = array(
                "POSNR" => $i,
                "ZUONR" => $zuonr,
                "WRBTR" => round($POSNR[$i]),
            );

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;
        }

        $this->items = new \Library\JournalSAPItem\Collection($items);

        $returnFunc = array(
            'status' => true,
            'usedGiftGC' => $arClearing,
            'usedRegisGC' => $arClearing
        );

        return $returnFunc;
    }

    public function prepareVoucherJournal($reorderData = array(), $useGC = 0)
    {
        $this->vorgangart = !empty($_ENV['VORGANGART_RECLASS']) ? $_ENV['VORGANGART_RECLASS'] : "3604";

        $POSNR[1] = $useGC;
        $POSNR[2] = 0;
        $POSNR[3] = $useGC;
        $POSNR[4] = 0;
        $POSNR[5] = 0;

        $debet = intval($POSNR[1]);
        $credit = intval($POSNR[3]);
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        for ($i = 1; $i <= 5; $i++) {
            $journalItem = array(
                "POSNR" => $i,
                "ZUONR" => $reorderData['order_no'],
                "WRBTR" => round($POSNR[$i]),
            );

            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;
        }

        $this->items = new \Library\JournalSAPItem\Collection($items);

        return true;
    }

    /**
     * Gift card usage
     * @param array $gc_data
     * @return bool
     */
    public function prepareReclassJournal($gc_data = [],$reversal = false)
    {
        if (empty($gc_data)) {
            return false;
        }

        $this->vorgangart = !empty($_ENV['VORGANGART_RECLASS']) ? $_ENV['VORGANGART_RECLASS'] : "3604";
        if($reversal){
            $this->vorgangart = "3615";
        }

        $gift_card_marketing = 0;
        $gift_card_redeem_refund = 0;
        $gift_card_actual_usage = intval($gc_data['voucher_amount_used']);

        /**
         * If voucher type is refund(2) or redeem(5) or redeem O2O(sell and gift)
         */
        $orderNo = $this->sales_order->getOrderNo();
        $voucherCode = $gc_data['voucher_code'];
        $voucherSource = $gc_data['voucher_source'];
        $coinAmount = intval($gc_data['coin_amount']);
        $budgetVoucherExchangeToProduct = $this->checkBudgetVoucherExchangetoProduct($voucherCode, $voucherSource, $gift_card_actual_usage, $coinAmount);

        if ($gc_data['voucher_type'] == '2' || $gc_data['voucher_type'] == '5' || $gc_data['voucher_type'] == getenv('VOUCHER_TYPE_SELL') || $gc_data['voucher_type'] == getenv('VOUCHER_TYPE_GIFT')) {
            $gift_card_redeem_refund += intval($gc_data['voucher_amount']);
        } else {
            $voucherCode = $orderNo;
            $gift_card_marketing += intval($gc_data['voucher_amount']);
        }

        if ($gift_card_marketing <= $gift_card_actual_usage) {
            $leftover_balance_marketing = 0;
        } else {
            $leftover_balance_marketing = $gift_card_marketing - $gift_card_actual_usage;
        }

        if ($gift_card_redeem_refund <= $gift_card_actual_usage) {
            $leftover_balance_redeem_refund = 0;
        } else {
            $leftover_balance_redeem_refund = $gift_card_redeem_refund - $gift_card_actual_usage;
        }

        $POSNR[1] = $gift_card_marketing;            // AMOUNT General GIFCARD
        $POSNR[2] = $gift_card_redeem_refund;        // AMOUNT Redeem Point & CM GIFT CARD
        $POSNR[3] = $gift_card_actual_usage;         // Total used Giftcard amount
        $POSNR[4] = $leftover_balance_marketing;    // Total forfeit balance from POSNR 1
        $POSNR[5] = $leftover_balance_redeem_refund; // Total forfeit balance from POSNR 2

        // check if rule_id is pay with coin
        // https://ruparupa.atlassian.net/browse/FIN-191
        $payWithCointRuleId = explode(',', getenv('RULE_ID_PAY_WITH_COIN'));
        $zuonr2 = $voucherCode;
        if (in_array($gc_data['rule_id'], $payWithCointRuleId)) {
            $POSNR[1] = 0;
            $POSNR[2] = $gift_card_marketing;
            $zuonr2 = $gc_data['voucher_code'];
        }

        if ($budgetVoucherExchangeToProduct === static::VOUCHER_MERCHANDISE_BUDGET) {
            $POSNR[1] = 0;
            $POSNR[2] = $coinAmount;
            $POSNR[3] = $coinAmount;
            $zuonr2 = $gc_data['voucher_code'];
        }elseif ($budgetVoucherExchangeToProduct === static::VOUCHER_MARKETING_BUDGET) {
            $POSNR[1] = $gift_card_actual_usage - $coinAmount;
            $POSNR[3] = $gift_card_actual_usage - $coinAmount;
        }

        $debet = $POSNR[1] + $POSNR[2];
        $credit = $POSNR[3] + $POSNR[4] + $POSNR[5];
        $this->_is_balance = ($debet - $credit == 0) ? true : false;

        $items = array();
        for ($i = 1; $i <= 5; $i++) {
            // Skip iteration for custom journal coin-exchange-to-product
            if (($i === 1 || $i === 4 || $i === 5) && $budgetVoucherExchangeToProduct === static::VOUCHER_MERCHANDISE_BUDGET) {
                continue;
            }
            if (($i === 2 || $i === 4 || $i === 5) && $budgetVoucherExchangeToProduct === static::VOUCHER_MARKETING_BUDGET) {
                continue;
            }        

            switch ($i) {
                case 2:
                    $ZUONR = $zuonr2;
                    break;
                case 3:
                    $ZUONR = $orderNo;
                    break;
                default:
                    $ZUONR  = $voucherCode;
                    break;
            }

            $journalItem = array(
                "POSNR" => $i,
                "ZUONR" => $ZUONR,
                "WRBTR" => round($POSNR[$i]),
            );
            $journalItemLib = new \Library\JournalSAPItem();
            $journalItemLib->setFromArray($journalItem);
            $items[] = $journalItemLib;
        }

        $this->items = new \Library\JournalSAPItem\Collection($items);
    }

    private function _prepareParam($journalItem = false)
    {
        $thisArray = get_object_vars($this);
        $journalHeader = array();
        // filter object type out of this
        foreach ($thisArray as $key => $val) {
            if (is_scalar($val) && $key != '_is_balance' && $key != 'docnum' && $key != 'company_code') {
                $journalHeader[strtoupper($key)] = $val;
            }
        }

        if (!$journalItem) {
            $journalItem = $this->items->prepareItemParam();
        } else {
            $this->_is_balance = true;
        }

        $parameter = array("b2c_journal_header" => $journalHeader, "b2c_journal_item" => $journalItem);

        return $parameter;
    }

    public function createJournal($journal_type = "pickup", $journalItem = false)
    {
        $parameter = $this->_prepareParam($journalItem);
        $journalData = array("Journal_B2C" => $parameter);

        $this->sendJournal($journalData, $journal_type);
        return true;
    }

    public function sendJournal($parameters = array(), $journal_type = "pickup", $use_file_param = false)
    {
        if (empty($parameters)) {
            return;
        }

        if ($this->_is_balance == false) {
            \Helpers\LogHelper::log("inbalance_journal", json_encode($parameters), "error");
            return false;
        }

        // Store log in XML format
        if (!$use_file_param) {
            //$this->createLogXml($journal_type);
        }

        $message = array();
        if (!empty($this->invoice)) {
            $invoice = $this->invoice->toArray();
            $message += ["invoice_no" => $invoice['invoice_no']];
        } else if(!empty($this->invoiceNo)) {
            $message += ["invoice_no" => $this->invoiceNo];
        }
            

        // Add parameter company_code to journal;
        $parameters += ['company_code' => $this->company_code];

        $nsq = new \Library\Nsq();
        $message += [
            "data" => json_encode($parameters),
            "message" => "createJournal",
            "type" => $journal_type
        ];
        $nsq->publishCluster('transactionSap', $message);

        return true;
    }

    public function createJournalB2B($journal_type = "pickup")
    {
        $parameter = $this->_prepareParam();
        $parameter = array("HEADER" => $parameter['b2c_journal_header'], "T_ITEM" => $parameter['b2c_journal_item']);
        $this->sendJournalB2B($parameter, $journal_type);

        return true;
    }

    public function sendJournalB2B($parameters = array(), $journal_type = "pickup", $use_file_param = false)
    {
        if (empty($parameters)) {
            return;
        }

        if ($this->_is_balance == false) {
            \Helpers\LogHelper::log("inbalance_journal", json_encode($parameters), "error");
            return false;
        }

        \Helpers\LogHelper::log("create_journal_b2b", "=====================", "info");
        \Helpers\LogHelper::log("create_journal_b2b", "TYPE : " . $journal_type, "info");

        $sapPath = $_ENV['SAP_PATH'];
        $enviroment = $_ENV['SERVICE_ENVIROMENT'];
        $client     = new \SoapClient($sapPath . $enviroment . '/SI_B2C_ODI_INTF_Out.wsdl', array('login' => $_ENV['B2B_WSDL_LOGIN'], 'password' => $_ENV['B2B_WSDL_PASS'], "trace" => 1, "exception" => 1));
        try {
            $response = $client->__soapCall('SI_B2C_ODI_INTF_Out', array('parameters' => $parameters));

            $return = "";
            if (isset($response->DOCNUM)) {
                $this->docnum = $response->DOCNUM;
                $return = 'Docnum #' . $this->docnum;
            } else {
                if (is_array($response->E_MESSAGE)) {
                    foreach ($response->E_MESSAGE as $eMsg) {
                        $return = $return . $eMsg . ' | ';
                    }
                } else {
                    $return = $response->E_MESSAGE;
                }
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("create_journal_b2b", $e->getMessage());
            return false;
        }

        // Store log in XML format
        if (!$use_file_param) {
            $this->saveJournal($journal_type);
        } else {
            $this->updateJournal($journal_type, $parameters);
        }

        $invoiceNo = '';
        if (!empty($this->invoice)) {
            $invoiceNo = $this->invoice->getInvoiceNo();
        }
        $log_msg = 'Bopis Journal Result for Order ID #' . $this->bonnummer . ' Invoice No #' . $invoiceNo . ' DATA : ' . $return;
        \Helpers\LogHelper::log("create_journal_b2b", $log_msg, "info");
        \Helpers\LogHelper::log("create_journal_b2b", "=====================", "info");

        return true;
    }

    public function createLogXml($journal_type = "pickup")
    {
        $parameter = $this->_prepareParam();
        $parameter += array('IS_BALANCE' => $this->_is_balance);

        // save to xml we need it for log
        $xml_data = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Journal_B2C></Journal_B2C>');
        \Helpers\ConversionHelper::arrayToXml($xml_data, $parameter);

        // Check if folder is exist, if not create it
        if (!file_exists(VAR_PATH . '/journal/' . $journal_type . '/')) {
            mkdir(VAR_PATH . '/journal/' . $journal_type . '/', 0777, true);
        }

        if (!empty($this->invoice)) {
            $xml_data->asXML(VAR_PATH . '/journal/' . $journal_type . '/journal_' . $this->invoice->getInvoiceNo() . '.xml');
        } else {
            $sufix = "";
            if ($journal_type == "reclass") {
                $sufix = "-" . \Phalcon\Text::random(\Phalcon\Text::RANDOM_NOZERO, 2);
            }
            $xml_data->asXML(VAR_PATH . '/journal/' . $journal_type . '/journal_' . $this->bonnummer . $sufix . '.xml');
        }

        return true;
    }

    public function saveJournal($journal_type = "pickup")
    {
        //save to mongodb
        $mongoDb = new Mongodb();
        $mongoDb->setCollection($journal_type);
        $collection = $mongoDb->getCollection();

        $parameter = $this->_prepareParam();
        if (!empty($this->invoice)) {
            if ($this->invoice->SalesOrder->getOrderType() == 'B2B') {
                $parameter = array("HEADER" => $parameter['b2c_journal_header'], "T_ITEM" => $parameter['b2c_journal_item']);
                $parameter += array("DOCNUM" => $this->docnum);
            }
        } else if (!empty($this->sales_order)) {
            if ($this->sales_order->getOrderType() == 'B2B') {
                $parameter = array("HEADER" => $parameter['b2c_journal_header'], "T_ITEM" => $parameter['b2c_journal_item']);
                $parameter += array("DOCNUM" => $this->docnum);
            }
        }
        $parameter += array('IS_BALANCE' => $this->_is_balance);

        if (!empty($this->invoice)) {
            $parameter += array('JOURNAL_NO' => $this->invoice->getInvoiceNo());
        } else {
            $parameter += array('JOURNAL_NO' => $this->bonnummer);
        }

        $collection->insertOne($parameter);
    }

    public function updateJournal($journal_type = "pickup", $parameters)
    {
        $mongoDb = new Mongodb();
        $mongoDb->setCollection($journal_type);
        $collection = $mongoDb->getCollection();
        $getData = $collection->findOne(['JOURNAL_NO' => $parameters['JOURNAL_NO']]);
        if ($getData) {
            $id = $getData['_id'];
            $getData['DOCNUM'] = $this->docnum;
            $collection->findOneAndReplace(['_id' => $id], $getData);
        }
    }

    public function checkBudgetVoucherExchangetoProduct($voucherCode, $voucherSource, $voucherAmountUsed, $coinAmount) {
        $budget = "";
    
        if ($voucherSource === static::VOUCHER_SOURCE_COIN_EXCHANGE_TO_PRODUCT && strlen($voucherCode) > 0) {
            $budget = static::VOUCHER_MERCHANDISE_BUDGET;
            if (strlen($voucherCode) > 2 && $voucherAmountUsed > $coinAmount) {
                if (substr($voucherCode, 0, 3) === static::ORDER_NO_PREFIX_ODI) {
                    $budget = static::VOUCHER_MARKETING_BUDGET;
                } else {
                    $budget = static::VOUCHER_MERCHANDISE_BUDGET;
                }
            }
        }
    
        return $budget;
    }
    
}
