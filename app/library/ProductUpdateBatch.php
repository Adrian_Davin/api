<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use Helpers\CartHelper;
use helpers\GeneralHelper;
use Helpers\ProductHelper;
use Models\AttributeSet;
use Models\Product2Category;
use Models\ProductCategory;

class ProductUpdateBatch
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllUpdateBatch()
    {
        $updateBatchModel = new \Models\ProductUpdateBatch();
        $updateBatchResult = $updateBatchModel->find(
            array(
                'columns' => 'product_update_batch_id, file_csv, updated_fields, total_updated, total_data, created_at, updated_at, created_user'
            )
        );
        if(!empty($updateBatchResult)){

            return $updateBatchResult->toArray();
        }
        else {
            $this->errorCode = "RR001";
            $this->errorMessages = "No Update Batch has been found";
            return;
        }
    }

    public function saveUpdateBatch($params){
        $updateBatchModel = new \Models\ProductUpdateBatch();
        $updateBatchModel->setFromArray($params);

        $updateBatchModel->saveData('product_update_batch');

        return $updateBatchModel->getProductUpdateBatchId();
    }

    public function processUpdateBatch($paramUpdateBatch){
        $updateBatchModel = new \Models\ProductUpdateBatch();

        $attributeSapModel = new \Models\AttributeSap();

        $productModel = new \Models\Product();

        $productVariantModel = new \Models\ProductVariant();

        $productAttributeVarcharModel = new \Models\ProductAttributeVarchar();
        $productAttributeIntModel = new \Models\ProductAttributeInt();

        $attributeSap = array('department_bu','product_group');
        $product = array('supplier_id','name','tag_search','meta_keyword','description','specification','package_content','packaging_uom','packaging_height','packaging_width','packaging_length','product_height','product_width','product_length','weight');
        $productVariant = array('status_buyer','status_content','status_image');

        $productAttributeVarcharArr = array('material');
        $productAttributeVarchar = array(
                                    'material' => 241
        );

        $productAttributeArr = array('installation_type');

        $productAttribute = array(
            'installation_type' => 396
        );

        $flagSuccess = 0;
        foreach($paramUpdateBatch['updated_data'] as $row){
            $params = array();

            $productStringSet = array();
            $productVariantStringSet = array();
            $attributeSapStringSet = array();
            $productAttributeVarcharSet = array();
            $flag = FALSE;
            foreach($row as $key => $val){
                if(in_array($key,$attributeSap)){
                    if($key == 'product_group'){
                        //update product 2 category
                        $dataToUpdate['sku'] = $row['sku'];
                        $dataToUpdate['product_group'] = $val;
                        $this->updateProduct2CategoryByProductGroup($dataToUpdate);
                        $this->updateProductAttributeSet($dataToUpdate);
                    }

                    $attributeSapStringSet[] = $key." = '".$val."'";
                }
                if(in_array($key,$product)){

                    $productStringSet[] = $key." = ".(empty($val)? 0:"'".$val."'");
                }
                if(in_array($key,$productVariant)){
                    $productVariantStringSet[] = $key." = '".$val."'";
                }
                if(in_array($key,$productAttributeVarcharArr)){
                    $productAttributeVarcharSet[$key]['value'] = "value = '".$val."'";
                    $productAttributeVarcharSet[$key]['attribute_id'] = $productAttributeVarchar[$key];
                }
                if(in_array($key,$productAttributeArr)){
                    $productAttributeStringSet[$key]['value'] = "".$val."";
                    $productAttributeStringSet[$key]['attribute_id'] = $productAttribute[$key];
                }
            }

            $params['sku'] = $row['sku'];
            if(!empty($attributeSapStringSet)){
                $params['stringSet'] = implode(',',$attributeSapStringSet);
                if($attributeSapModel->updateBatch($params)){
                    $flag = TRUE;
                }
            }

            if(!empty($productStringSet)){
                $params['stringSet'] = implode(',',$productStringSet);
                if($productModel->updateBatch($params)){
                    $flag = TRUE;
                }
            }

            if(!empty($productVariantStringSet)){
                $params['stringSet'] = implode(',',$productVariantStringSet);
                if($productVariantModel->updateVariantBatch($params)){
                    $flag = TRUE;
                }
            }

            if(!empty($productAttributeVarcharSet)){

                foreach($productAttributeVarcharSet as $key => $rowVarchar){
                    $params['stringSet'] = $rowVarchar['value'];
                    $params['attributeId'] = $rowVarchar['attribute_id'];
                    $productAttributeVarcharModel->updateBatch($params);
                }

            }

            if(!empty($productAttributeStringSet)){
                foreach($productAttributeStringSet as $key => $rowAttr){
                    $rowAttr['sku'] = $row['sku'];
                    if($productAttributeIntModel->updateBatch($rowAttr)){
                        $flag = TRUE;
                    }
                }

            }

            if($flag){
                $flagSuccess ++;
            }
        }

        /**
         * update product_update_batch
         */

        $productBatch = $updateBatchModel->findFirst(' product_update_batch_id = '.$paramUpdateBatch['product_update_batch_id']);

        $paramUpdateBatch['total_updated'] = $productBatch->toArray()['total_updated'] + $flagSuccess;
        unset($paramUpdateBatch['updated_data']);
        $updateBatchModel->setFromArray($paramUpdateBatch);
        $updateBatchModel->saveData('product_update_batch');

        // send to queue
        ProductHelper::updateProductToElastic(array("0" => array('sku' => $params['sku'])));

        return $flagSuccess;

    }

    public function updateProduct2CategoryByProductGroup($parameters = array()){
        //step 1: search product_id
        $productVariantModel = new \Models\ProductVariant();
        $productVariantResult = $productVariantModel->findFirst(
            [
                "columns" => "product_id",
                "conditions" => "sku='".$parameters['sku']."'"
            ]
        );

        if($productVariantResult){
            $productId = $productVariantResult->toArray()['product_id'];

            //step 2: search in product_category based on product_group
            $productCategoryModel = new ProductCategory();
            $categoryResult = $productCategoryModel->findFirst(
                [
                    "columns" => "path",
                    "conditions" => "article_hierarchy='".$parameters['product_group']."'"
                ]
            );

            if($categoryResult){
                $path = str_replace('/',',',$categoryResult->toArray()['path']);

                //step 3: delete all in product_2_category with these categories and product_id of this sku
                $product2CatModel = new Product2Category();

                $sql  = "DELETE FROM product_2_category WHERE product_id=".$productId;
                $sql .= " AND category_id IN (".$path."); UPDATE product_2_category SET is_default=0 WHERE product_id=".$productId;
                $product2CatModel->useWriteConnection();
                $product2CatModel->getDi()->getShared($product2CatModel->getConnection())->query($sql);

                $path = explode(',',$path);
                $total = count($path);
                $idx = 0;
                foreach ($path as $categoryId){
                    if($idx == ($total-1)){
                        $updateData[] = "(".$productId.",".$categoryId.",1,10000)";
                    }
                    else{
                        $updateData[] = "(".$productId.",".$categoryId.",0,10000)";
                        $idx++;
                    }
                }

                $values = implode(',',$updateData);
                //step 4: insert product_2_category
                $sql  = "INSERT INTO product_2_category VALUES ".$values;
                $product2CatModel->useWriteConnection();
                $result = $product2CatModel->getDi()->getShared($product2CatModel->getConnection())->query($sql);

                unset($parameters['product_group']);
                //ProductHelper::updateProductToElastic(array(0 => $parameters));

                return $result->numRows();
            }
            else{
                return array(
                    'errors' => 'Path not found.'
                );
            }
        }
        else{
            return array(
                'errors' => 'Product not found.'
            );
        }
    }

    public function updateProductAttributeSet($parameters = array()){
        if(empty($parameters)){
            return false;
        }
        //step 1: search attribute set id in attribute set
        $attributeSetName = substr($parameters['product_group'],0,10);

        $productVariantModel = new \Models\ProductVariant();
        $productResult = $productVariantModel->findFirst(
            [
                "columns" => "product_id",
                "conditions" => "sku='".$parameters['sku']."'"
            ]
        );

        if($productResult){
            $productId = $productResult->toArray()['product_id'];

            $attributeSetModel = new AttributeSet();
            $attributeSetResult = $attributeSetModel->findFirst(
                [
                    "columns" => "attribute_set_id",
                    "conditions" => "attribute_set_name='".$attributeSetName."'"
                ]
            );

            if($attributeSetResult){
                $attributeSetId = $attributeSetResult->toArray()['attribute_set_id'];

                $productModel = new \Models\Product();
                $productModel->setProductId($productId);
                $productModel->setAttributeSetId($attributeSetId);
                $productModel->saveData();
            }
        }

    }

}