<?php

namespace Library;


use Helpers\ProductHelper;

class Tukarvoucher
{
    protected $data;
    protected $errorCode;
    protected $errorMessages;

    protected $tukarvoucherModel;

    /**
     * @var $customer \Models\Customer
     */
    protected $customer;

    /**
     * @var $salesruleVoucher \Models\SalesruleVoucher
     */
    protected $salesruleVoucher;

    /**
     * @var $customerLib \Library\Customer
     */
    protected $customerLib;

    public function __construct()
    {
        $this->customer = new \Models\Customer();
        $this->salesruleVoucher = new \Models\SalesruleVoucher();
        $this->customerLib = new \Library\Customer();
        $this->tukarvoucherModel = new \Models\MarketingTukarvoucher();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * Get the value of data
     */ 
    public function getData()
    {
        return $this->data;
    }

    public function setTukarvoucherFromArray($data_array = array())
    {
        $this->tukarvoucherModel->setFromArray($data_array);
        return true;
    }

    public function validateCustomer(){

        // Validate Email Exist
        $email = $this->tukarvoucherModel->getCustomerEmail();
        $resultCustomer = $this->customer->findFirst([
            'email = :email:',
            'bind' => [
                'email' => $email
            ]
        ]);

        if($resultCustomer){
            $this->customer->setFromArray($resultCustomer->toArray());
            return TRUE;
        }else{
            return FALSE;
        }
        
    }

    public function validateCustomerVoucherExist($rule_id = 0){

        // Validate customer Exist in salesrule_voucher
        $id = $this->tukarvoucherModel->getCustomerId();
        $resultCustomer = $this->salesruleVoucher->findFirst([
            'customer_id = :customer_id: and rule_id = :rule_id:',
            'bind' => [
                'customer_id' => $id,
                'rule_id' => $rule_id
            ]
        ]);

        if($resultCustomer){
            return TRUE;
        }else{
            return FALSE;
        }
        
    }

    public function saveData($log_file_name = 'marketing_tukarvoucher'){
        
        $data = array();

        // Save to DB, table marketing _tukarvoucher
        
        // Validate Customer
        $isCustomerExist = $this->validateCustomer();
        if(!$isCustomerExist){
            // Create Ruparupa Customer
            $paramRegister = array(
                "email" => $this->tukarvoucherModel->getCustomerEmail(),
                "first_name" => $this->tukarvoucherModel->getCustomerFirstName(),
                "password" => $this->tukarvoucherModel->getCustomerPassword(),
                "phone" => $this->tukarvoucherModel->getCustomerPhone(),
                "last_name" => $this->tukarvoucherModel->getCustomerLastName(),
                "company_code" => "ODI",
                "registered_by" => "HCI"
            );
            $this->customerLib->setCustomerFromArray($paramRegister);
            $this->customerLib->registerCustomer(true);
            $customerId = $this->customerLib->getCustomer()->getCustomerId();
        }else{
            $customerId = 0;
            $customerId = $this->customer->getCustomerId();
        }
        
        /**
         * Create Voucher code base on last customer id inserted
         */
        $this->tukarvoucherModel->setCustomerId($customerId);    
        
        // validate exist in salesrule voucher
        $isCustomerExistSalesruleVoucher = $this->validateCustomerVoucherExist(getenv("TUKARVOUCHER_RULE_ID"));
        if(!$isCustomerExistSalesruleVoucher){
            $salesRuleVoucherModel = new \Models\SalesruleVoucher();
            $customerDenom = $this->tukarvoucherModel->getCustomerDenom();
            $minimumTransaction = 300000;
            $ruleIdRelated = array(
                50000 => getenv("TUKARVOUCHER_RULE_ID")
            );
            $prefix = "15EO";
            $parameters = [
                "rule_id" => $ruleIdRelated[$customerDenom],
                "type" => 3,
                "created_by" => 0,
                "expiration_date" => date('Y-m-d 23:59:59', strtotime('+ 14 days')),
                "qty" => 1,
                "voucher_amount" => $customerDenom,
                "length" => 6,
                "prefix" => $prefix,
                "format" => "alphanum"
            ];
            $marketingLib = new \Library\Marketing();
            $marketingLib->generateVoucher($parameters);
            $voucherList = $marketingLib->getVoucherList();
    
            if (!empty($voucherList)) {
                foreach ($voucherList as $voucherCode) {
                    $parameters['voucher_code'] = $voucherCode;
                    $parameters['limit_used'] = 1;
                    $parameters['max_redemption'] = 99999999;
                    $parameters['status'] = 10;
                    $parameters['customer_id'] = $customerId;
                    $salesRuleVoucherModel = new \Models\SalesruleVoucher();
                    $salesRuleVoucherModel->setFromArray($parameters);
                    $salesRuleVoucherModel->saveData("voucher");
                }
            }
            
            // Send Email with template
            $email = new \Library\Email();
            $email->setEmailSender();
            $email->setName($this->tukarvoucherModel->getCustomerLastName(),$this->tukarvoucherModel->getCustomerFirstName());
            $email->setEmailReceiver($this->customer->getEmail());
            $email->setEmailTag("marketing_informa_anniv");
    
            $voucherExpired = date('j F Y', strtotime($parameters['expiration_date']));
            $companyCode = 'ODI';
            $templateID = \Helpers\GeneralHelper::getTemplateId(getenv("TUKARVOUCHER_EMAIL_TEMPLATE"), $companyCode);
            $email->setCompanyCode($companyCode);
            $email->setTemplateId($templateID);
            $email->setVoucherCode($voucherCode);
            $email->setVoucherExpired($voucherExpired);
            $email->setVoucherValue(number_format($customerDenom, 0, ',', '.'));
            $email->setMinimumTransaction(number_format($minimumTransaction, 0, ',', '.'));
            $email->send();
    
            $this->tukarvoucherModel->setVoucherCodeGenerated($voucherCode);
            $this->tukarvoucherModel->saveData('marketing_tukarvoucher');
    
            $this->data = array('customer_id' => $customerId);
        }else{
            $this->errorCode = 404;
            $this->errorMessages = "Customer sudah pernah mendapatkan voucher Informa Annivarsary";
        }
            
        return ;
    }
    
}