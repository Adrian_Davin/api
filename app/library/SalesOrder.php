<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Library;

use Exception;
use Helpers\LogHelper;
use \Helpers\CartHelper;
use \GuzzleHttp\Client as GuzzleClient;
use Phalcon\Db as Database;

class SalesOrder
{

    /**
     * @var \Models\SalesOrder
     */
    protected $salesOrder;

    /**
     * @var \Models\Cart
     */
    protected $shoppingCart;
    protected $shoppingCartData;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @return \Models\SalesOrder
     */
    public function getSalesOrder()
    {
        return $this->salesOrder;
    }

    public function __construct()
    {
    }

    /**
     * @param array $data_array
     */
    public function setFromShoppingCart($data_array = array())
    {
        if (empty($data_array['cart_id'])) {
            $this->errorCode = "RR202";
            $this->errorMessages[] = "Shopping cart not found";
            return;
        }

        $this->shoppingCart = new \Models\Cart();
        $this->shoppingCart->setCartId($data_array['cart_id']);
        $this->shoppingCartData = $cartData = $this->shoppingCart->loadCartData(true);

        $createOrderDelayStart = getenv('CREATE_ORDER_DELAY_START');
        $createOrderDelayEnd = getenv('CREATE_ORDER_DELAY_END');
        $nowDateTime = date('Y-m-d H:i:s');
        if (!empty($createOrderDelayStart) && !empty($createOrderDelayEnd)) {
            if ($nowDateTime >= $createOrderDelayStart && $nowDateTime <= $createOrderDelayEnd) {
                // set cart to hold_payment
                $this->shoppingCartData['status'] = "hold_payment";
                $this->shoppingCart->saveToDb($this->shoppingCartData);

                // set a delay between 1 to 5 seconds
                $randNumber = rand(1,5);
                sleep($randNumber);
            }
        }

        $salesOrderModel = new \Models\SalesOrder();
        $cartIDtemp = $cartData['cart_id'];
        $cartData['cart_id'] = $data_array['cart_id'];
        $isVendor = false;
        if (substr($cartData['reserved_order_no'], 0, 4) == "ODIS" || substr($cartData['reserved_order_no'], 0, 4) == "ODIT" || substr($cartData['reserved_order_no'], 0, 4) == "ODIK") {
            $cartData['cart_id'] = $cartIDtemp;
            $isVendor = true;
        }

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start $salesOrderModel->setFromShoppingCart library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

        $salesOrderModel->setFromShoppingCart($cartData);
        $salesOrderModel->setCartRaw($cartData);
        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End $salesOrderModel->setFromShoppingCart library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

        $this->salesOrder = $salesOrderModel;
        if (empty($salesOrderModel->getErrorCode())) {
            \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start $salesOrderModel->giftCardValidation library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

            $salesOrderModel->giftCardValidation();

            \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End $salesOrderModel->giftCardValidation library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

            \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start $salesOrderModel->addMissingDetail library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

            $salesOrderModel->addMissingDetail();

            \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End $salesOrderModel->addMissingDetail library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');
        }


        if (empty($salesOrderModel->getErrorCode())) {
            // update shopping cart status, prevent one shopping cart processed twice            
            if ($isVendor) {
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start shoppingCart->saveCartVendor library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

                $this->shoppingCart->saveCartVendor("processing");

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End shoppingCart->saveCartVendor library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');
            } else {
                $this->shoppingCartData['status'] = "processing";

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start $this->shoppingCart->saveToDb library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

                $this->shoppingCart->saveToDb($this->shoppingCartData);

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End $this->shoppingCart->saveToDb library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');
            }

            if (isset($cartData['refund_no'])) {
                if (!empty($cartData['refund_no'])) {
                    $creditMemoModel = new \Models\SalesCreditMemo();
                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start creditMemoModel->updateRefundStatus library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

                    $creditMemoModel->updateRefundStatus($cartData['refund_no'], "canceled");

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End creditMemoModel->updateRefundStatus library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start creditMemoModel->updateStatusItem library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');

                    $creditMemoModel->updateStatusItem($cartData['refund_no'], "reordered");

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End creditMemoModel->updateStatusItem library/SalesOrder.php Cart id: ' . $data_array['cart_id'], 'debug');
                }
            }
        } else {
            LogHelper::log("createOrderError", $salesOrderModel->getOrderNo() . " : " . json_encode($salesOrderModel->getErrorMessages()));
            // $this->cancelPayment();
        }
    }

    /**
     * @throws HTTPException
     */
    public function createSalesOrder($payloadData = array())
    {

        if (empty($this->salesOrder->getErrorMessages())) {
            $companyCode = $this->salesOrder->getCompanyCode();
            try {
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start salesOrder->saveTransaction library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                // save RRRID to table sales_order
                if (array_key_exists('rrr_id', $this->shoppingCartData['customer'])) {
                    $this->salesOrder->setRRRID($this->shoppingCartData['customer']['rrr_id']);
                }

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start salesOrder->saveTransaction library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');
                $this->salesOrder->setShoppingCartData($this->shoppingCartData);
                $resultProcess = $this->salesOrder->saveTransaction();

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End salesOrder->saveTransaction library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                if (!$resultProcess) {
                    throw new \Exception("create order failed");
                }

                // Redeem voucher to stamps via go-unify for voucher_type 10
                // voucher_type 10 = stamps
               // add condition for voucher type 10 to redeem voucher unify                                   
               $giftCards = json_decode($this->shoppingCartData['gift_cards'], true);               
               if (!empty($giftCards)){
                  foreach ($giftCards as $card) {                    
                      if (isset($card['voucher_type']) && $card['voucher_type'] == 10) {                          
                          $isStampsVoucherRedeemed = $this->redeemStampsVoucherUnify(
                              $this->shoppingCartData['customer']['customer_id'],
                              $this->shoppingCartData
                          );
                          
                          if (!$isStampsVoucherRedeemed) {                             
                              $orderNo = $this->salesOrder->getOrderNo();
                              $salesOrderID = $this->salesOrder->getSalesOrderId();
          
                              $redeemStampsVoucherErrMsg = "failed redeem stamps voucher";
                              $invoiceLib = new \Library\Invoice();
                              $invoiceLib->setStatusHold($salesOrderID, $redeemStampsVoucherErrMsg,  $orderNo);
          
                              // Slack Notif
                              $params_data = array(
                                  "message" => $redeemStampsVoucherErrMsg,
                                  "slack_channel" => $_ENV['OPS_ISSUE_SLACK_CHANNEL'],
                                  "slack_username" => $_ENV['OPS_ISSUE_SLACK_USERNAME']
                              );
                              $this->notificationError($params_data);
                          }                          
                          // ** Exit loop when match voucher type = 10 to redeemStampsVoucher
                          break;                          
                      }                  
                  }                     
               }   

                // Calculate MDR
                $mdrModel = new \Library\Finance($this->salesOrder);
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start mdrModel->calculateMDR library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                $mdrItems = $mdrModel->calculateMDR();

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End mdrModel->calculateMDR library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                if (count($mdrItems) > 0) {
                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start foreach ($mdrItems as $row) library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    foreach ($mdrItems as $row) {
                        $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
                        $salesOrderItemMdrModel->setFromArray($row);
                        $salesOrderItemMdrModel->save('sales_order_item_mdr');
                    }

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End foreach ($mdrItems as $row) library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');
                }

                $cart = new Cart();
                $params['cart_id'] = $this->salesOrder->cart_id;
                $params['payment'] = [
                    "has_changed_payment" => $this->salesOrder->getHasChangedPayment()
                ];
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start $cart->updatePayment library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                $cart->updatePayment($params);

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End $cart->updatePayment library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                // update reorder
                if (!empty($this->shoppingCartData['reference_invoice_no'])) {
                    $updateReorderData = array(
                        "invoice_no" => $this->shoppingCartData['reference_invoice_no'],
                        "sales_reorder" => array(
                            "status" => "completed",
                            "new_order_no" => $this->salesOrder->getOrderNo()
                        )
                    );
                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start this->updateReorder library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    $this->updateReorder($updateReorderData);

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End this->updateReorder library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');
                }

                /**
                 * @todo : Refactor it to proper code
                 */
                $send_email = false;
                if (($this->shoppingCartData['payment']['method'] == 'niceva' || $this->shoppingCartData['payment']['method'] == 'free_payment' || $this->shoppingCartData['payment']['method'] == 'manual_transfer' || $this->shoppingCartData['payment']['method'] == 'kredivo') && isset($this->shoppingCartData['send_email'])) {
                    $sendEmailFlag = $this->shoppingCartData['send_email'];
                    $send_email = ($sendEmailFlag == 'yes') ? true : false;
                }

                $customerId = $this->shoppingCartData['customer']['customer_id'];
                $customerFirstname = $this->shoppingCartData['customer']['customer_firstname'];

                // if($this->shoppingCartData['payment']['status'] == 'pending'){
                //     $this->pushnotifMobile($customerId,$customerFirstname);
                // }
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start $send_email library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');
                if ($send_email) {
                    if (!empty($this->shoppingCartData['reference_invoice_no'])){
                        $productOrderBuildParams = array();
                        $apiWrapper = new APIWrapper(getenv('REORDER_API'));
                        $apiWrapper->setEndPoint('reorder/invoice/parent?order_no=' . $this->salesOrder->getOrderNo());
                        $isGosendChange = false;
                        if ($apiWrapper->sendRequest("get")) {
                            $apiWrapper->formatResponse();
                            $parentInvoice = $apiWrapper->getData();
    
                            $carrierID = 0;
                            foreach ($this->shoppingCartData['items'] as $item) {
                                $carrierID = $item['shipping']['carrier_id'];
                            }
    
                            $gosendGroup = array(getenv('GOSEND_INSTANT_CARRIER_ID'), getenv('GOSEND_SAMEDAY_CARRIER_ID'));
                            if (in_array($parentInvoice['carrier_id'], $gosendGroup) && !in_array($carrierID, $gosendGroup)) {
                                $isGosendChange = true;
                            }
                            if (isset($parentInvoice["invoice_no"])){
                                $productOrderBuildParams["parent_invoice_no"] = $parentInvoice["invoice_no"];
                            }
                        }
                    }      
                    $email = new Email();
                    $email->setOrderObj($this->salesOrder);
                    $email->setEmailSender();
                    $email->setName();
                    $email->setEmailReceiver();                                  
                    $email->buildOrderEmailParam($productOrderBuildParams);
                    $email->generateAddress();
                    switch ($this->shoppingCartData['payment']['method']) {
                        case "niceva":
                            $email->setPaymentMethod("Transfer Virtual Account");
                            break;
                        case "kredivo":
                            $email->setPaymentMethod("Kredivo");
                            break;
                        case "manual_transfer":
                            $email->setPaymentMethod("Manual Transfer");
                            break;
                        default :     
                            $email->setPaymentMethod("");
                    }
                    $vaBank = "";
                    if (!empty($this->shoppingCartData['payment']['va_bank'])) {
                        $vaBank = strtolower($this->shoppingCartData['payment']['va_bank']);
                    }

                    if (!empty($this->salesOrder->getReferenceOrderNo())) {
                        $parentOrderNo = $this->salesOrder->getParentOrderNo();

                        if (!empty($parentOrderNo)) {
                            if (substr($parentOrderNo, 3, 1) == "A") {
                                $companyCode = 'AHI';
                            } else if (substr($parentOrderNo, 3, 1) == "H" || substr($parentOrderNo, 3, 1) == "I") {
                                $companyCode = 'HCI';
                            } else if (substr($parentOrderNo, 3, 1) == "L" || substr($parentOrderNo, 3, 1) == "M") {
                                $companyCode = 'SLM';
                            } else {
                                // query device from db
                                $parentInformation = new \Models\SalesOrder();
                                $parentData = $parentInformation->findFirst('order_no = "' . $parentOrderNo . '"');
                                if ($parentData) {
                                    $parentDataArr = $parentData->toArray();

                                    $device = $parentDataArr["device"];
                                    if ($device == 'ace-ios' || $device == 'ace-android') {
                                        $companyCode = 'AHI';
                                    } else if ($device == 'informa-ios' || $device == 'informa-android' || $device == 'informa-huawei') {
                                        $companyCode = 'HCI';
                                    } else if ($device == 'selma-ios' || $device == 'selma-android' || $device == 'selma-huawei') {
                                        $companyCode = 'SLM';
                                    }
                                }
                            }
                        }

                        if ($isGosendChange) {
                            $email->setEmailTag("reorder_change_regular");
                            $templateID = \Helpers\GeneralHelper::getTemplateId("reorder_change_regular", $companyCode);
                        } else {
                            $email->setEmailTag("reorder");
                            $templateID = \Helpers\GeneralHelper::getTemplateId("reorder", $companyCode);
                        }
                    } else {
                        if (!empty($this->salesOrder->getStoreCodeNewRetail()) && substr($this->salesOrder->getStoreCodeNewRetail(), 0, 1) == "A") {
                            $companyCode = 'AHI';
                        } else if (\Helpers\GeneralHelper::isStoreCodeSelma($this->salesOrder->getStoreCodeNewRetail())) {
                            $companyCode = 'SLM';
                        } else if (!empty($this->salesOrder->getStoreCodeNewRetail()) && substr($this->salesOrder->getStoreCodeNewRetail(), 0, 1) == "H") {
                            $companyCode = 'HCI';
                        } else if (!empty($this->salesOrder->getStoreCodeNewRetail()) && substr($this->salesOrder->getStoreCodeNewRetail(), 0, 1) == "J") {
                            $companyCode = 'HCI';
                        } else if ($this->salesOrder->getDevice() == 'ace-ios' || $this->salesOrder->getDevice() == 'ace-android') {
                            $companyCode = 'AHI';
                        } else if ($this->salesOrder->getDevice() == 'informa-ios' || $this->salesOrder->getDevice() == 'informa-android' || $this->salesOrder->getDevice() == 'informa-huawei') {
                            $companyCode = 'HCI';
                        } else if ($this->salesOrder->getDevice() == 'selma-ios' || $this->salesOrder->getDevice() == 'selma-android' || $this->salesOrder->getDevice() == 'selma-huawei') {
                            $companyCode = 'SLM';
                        }
                        switch ($vaBank) {
                            case "mandiri":
                                $email->setEmailTag("cust_checkout_bank_transfer_mandiri");
                                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_mandiri_new", $companyCode);
                                break;
                            case "bni":
                                $email->setEmailTag("cust_checkout_bank_transfer_bni");
                                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_bni_new", $companyCode);
                                break;
                            case "keb_hana":
                                $email->setEmailTag("cust_checkout_bank_transfer_kebhana");
                                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_keb_hana_new", $companyCode);
                                break;
                            case "bii":
                                $email->setEmailTag("cust_checkout_bank_transfer_maybank");
                                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_bii_new", $companyCode);
                                break;
                            case "cimb":
                                $email->setEmailTag("cust_checkout_bank_transfer_cimb");
                                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_cimb_new", $companyCode);
                                break;
                            case "bri":
                                $email->setEmailTag("cust_checkout_bank_transfer_bri");
                                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_bri_new", $companyCode);
                                break;
                            case "danamon":
                                $email->setEmailTag("cust_checkout_bank_transfer_danamon");
                                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_danamon_new", $companyCode);
                                break;
                        }
                    }
                    $creditNonCC = "";
                    if (!empty($this->shoppingCartData['payment']['method'])) {
                        $creditNonCC = strtolower($this->shoppingCartData['payment']['method']);
                    }
                    if (!empty($this->salesOrder->getReferenceOrderNo())) {
                        $email->setEmailTag("reorder");
                        $templateID = \Helpers\GeneralHelper::getTemplateId("reorder", $companyCode);
                    } else {
                        switch ($creditNonCC) {
                            case "kredivo":
                                $email->setEmailTag("cust_checkout_credit_non_cc_kredivo");
                                $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_kredivo_new", $companyCode);
                                break;
                            case "manual_transfer":
                                if (!empty($this->shoppingCartData['cart_type'])) {
                                    $cart_type = $this->shoppingCartData['cart_type'];
                                    if (strpos($cart_type, 'b2b') !== false) {
                                        $companyCode = "ODI";
                                        if ($cart_type == "informa_b2b") {
                                            $companyCode = "HCI";
                                            $email->setEmailCc(getenv('PIC_INFORMA_B2B_ORDER_EMAIL'));
                                        } else if ($cart_type == "ace_b2b") {
                                            $companyCode = "AHI";
                                            $email->setEmailCc(getenv('PIC_ACE_B2B_ORDER_EMAIL'));
                                        }

                                        $email->setEmailTag("cust_checkout_credit_non_cc_manual_transfer");
                                        $templateID = \Helpers\GeneralHelper::getTemplateId("payment_pending_manual_transfer_new", $companyCode);
                                    }
                                }
                                break;
                        }
                    }
                    if (isset($templateID)) {
                        if (!empty($templateID)) {
                            $email->setCompanyCode($companyCode);
                            $email->setTemplateId($templateID);
                        }
                    }

                    $email->setVaNumber();

                    if (!empty($this->salesOrder->getReferenceOrderNo())){
                        $newReorderEta = $email->getOrderEta() ?? 0;
                        $parentOrderEta = $email->getParentOrderEta() ?? 0;

                        $exactDiff = $newReorderEta - $parentOrderEta;
                        $daysDiff = floor($exactDiff / (60 * 60 * 24));

                        // if new reorder eta exists then it means delivery reorder
                        // else new reorder is pickup, hence no notification 

                        // if parent order eta does not exist (pickup parent) and reorder is delivery 
                        // then send email regardless
                        if (($newReorderEta && $parentOrderEta && $daysDiff >= 2) || ($newReorderEta && !$parentOrderEta)){
                            $email->send();

                            $newMaxEta = date('d-m-Y', $newReorderEta);
                            
                            // push notification 
                            $icon = getenv('PUSH_NOTIF_ICON_ODI');
                            $iconByCompanyCode = getenv(sprintf("PUSH_NOTIF_ICON_%s", strtoupper($companyCode)));
                            if (!empty($iconByCompanyCode)) {
                                $icon = $iconByCompanyCode;
                            }
                        
                            $pushNotifLib = new \Library\Notification();
                            $pushNotifLib->setTitle("Pesanan Mengalami Keterlambatan");
                            $pushNotifLib->setBody("Pesanan ".$parentInvoice["invoice_no"]." akan sampai paling lambat ".$newMaxEta.". Karena ketidaktersediaan stok di toko terdekat, pengiriman akan kami alihkan ke toko lain.");
                            $pushNotifLib->setKey("token");
                            $pushNotifLib->setValue("");
                            $pushNotifLib->setIcon($icon);
                            $pushNotifLib->setPageType("order-detail");
                            $pushNotifLib->setUrl(getenv("RUPARUPA_URL")."my-account/transaction/detail?".urlencode("tab=transaction&child_tab=detail&transaction_no=".$parentInvoice["invoice_no"]."&type=online&email=".$this->shoppingCartData['customer']['customer_email']));
                            $pushNotifLib->setUrlKey(sprintf('%s,%s,%s', $parentOrderNo, $email->getEmailReceiver(), $parentInvoice["invoice_no"]));
                            $pushNotifLib->setCustomerID($customerId);
                            $customerDeviceToken = new \Models\CustomerDeviceToken();
                            $customerDeviceToken = $customerDeviceToken->getCDTByCustIdForPushNotif($customerId);//
    
                            $pushNotifLib->PushNotifInbox();
    
                            $pushNotifList = explode(',', getenv('PUSH_NOTIF_BU_CODE'));
                            if (count($customerDeviceToken) > 0 && in_array($companyCode, $pushNotifList)) {
                                $filteredToken = $pushNotifLib->FilterTokenByHighestPriority($customerDeviceToken);
        
                                foreach($filteredToken as $filterToken){
                                    $pushNotifLib->setValue($filterToken["device_token"]);
                                    $pushNotifLib->PushNotif($companyCode);
                                }
                            }
                        }
                    } else {
                        $email->send();
                    }
                }

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End $send_email library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                if ($this->salesOrder->SalesOrderPayment->getStatus() == "paid" && $this->salesOrder->SalesOrderPayment->getType() != "term_of_payment") {
                    // Invoice for payment using TOP created from go-order-v2 to check status SAP first
                    $totalRow = 0;
                    $salesOrderTpModel = new \Models\SalesOrderTp();

                    $sql = "SELECT COUNT(odi) as totalOdi FROM sales_order_tp where odi ='" . $this->salesOrder->getOrderNo() . "' AND ((status = 'failed' AND retry_count < 3) OR status = 'process') AND source = 'Create TP'";
                    $result = $salesOrderTpModel->getDi()->getShared('dbMaster')->query($sql);

                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );

                    $totalRow = $result->fetchAll()[0]['totalOdi'];

                    // if no TP is processing
                    if($totalRow == 0){
                        $nsq = new \Library\Nsq();
                        $data = [
                            "data" => ["order_no" => $this->salesOrder->getOrderNo()],
                            "message" => "splitInvoice"
                        ];
                        $nsq->publishCluster('transaction', $data);
                    }
                }

                if ($this->shoppingCartData['device'] == "vendor") {
                    $orderNo = $this->salesOrder->getOrderNo();
                    if (substr($orderNo, 0, 4) == "ODIS") {
                        $params_data = array(
                            "message" => "Save order shopee success : " . $orderNo . " - " . $this->salesOrder->getSourceOrderNo(),
                            "slack_channel" => $_ENV['SHOPEE_ORDER_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['ORDER_SLACK_USERNAME']
                        );
                        $this->notificationError($params_data);
                    } else if (substr($orderNo, 0, 4) == "ODIT") {
                        $params_data = array(
                            "message" => "Save order tokopedia success : " . $orderNo . " - " . $this->salesOrder->getSourceOrderNo(),
                            "slack_channel" => $_ENV['TOKOPEDIA_ORDER_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['ORDER_SLACK_USERNAME']
                        );
                        $this->notificationError($params_data);
                    } else if (substr($orderNo, 0, 4) == "ODIK") {
                        $params_data = array(
                            "message" => "Save order tiktok success : " . $orderNo . " - " . $this->salesOrder->getSourceOrderNo(),
                            "slack_channel" => $_ENV['TIKTOK_ORDER_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['ORDER_SLACK_USERNAME']
                        );
                        $this->notificationError($params_data);
                    }
                }

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start this->salesOrder->saveRuleOrder library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                $resultSalesrule = $this->salesOrder->saveRuleOrder();

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End this->salesOrder->saveRuleOrder library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start process1021 library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                $result1021 = $this->salesOrder->process1021();
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End process1021 library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                if (!empty($this->shoppingCartData['payment']['cc_type'])) {
                    $resultSalesruleCreditCard = $this->salesOrder->saveSalesRuleCreditCard($this->shoppingCartData);

                    if (!$resultSalesruleCreditCard) {
                        $params_data = array(
                            "message" => "Failed create salesrule credit card : " . $this->salesOrder->getOrderNo(),
                            "slack_channel" => $_ENV['ORDER_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                        );
                        $this->notificationError($params_data);
                    }
                }

                $this->purchasePwpSku($this->shoppingCartData);
                $resultSalesruleOrderPackage = $this->salesOrder->saveSalesruleOrderPackage($this->shoppingCartData);

                if (!$resultSalesrule) {
                    $params_data = array(
                        "message" => "Failed create salesrule order : " . $this->salesOrder->getOrderNo(),
                        "slack_channel" => $_ENV['ORDER_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                    );
                    $this->notificationError($params_data);
                }
                if (!$result1021) {
                    $params_data = array(
                        "message" => "Failed create 1000DC listener : " . $this->salesOrder->getOrderNo(),
                        "slack_channel" => $_ENV['ORDER_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                    );
                    $this->notificationError($params_data);
                }

                if (!$resultSalesruleOrderPackage) {
                    $params_data = array(
                        "message" => "Failed create salesrule order package : " . $this->salesOrder->getOrderNo(),
                        "slack_channel" => $_ENV['ORDER_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                    );
                    $this->notificationError($params_data);
                }

                $this->slackLimitRequirement($this->salesOrder, $customerId);

                $skuOneQtyItems = array();
                $thermalSkuItems = array();
                $isInSkuOneQty = false;
                $isThermalSKu = false;
                $skuOneQtyList = explode(",", getenv('SKU_MASKER_LIST'));
                $thermalSku = explode(",", getenv('SKU_THERMAL'));
                foreach ($this->salesOrder->getItems() as $item) {
                    foreach ($skuOneQtyList as $skuOneQty) {
                        if ($item->sku == $skuOneQty) {
                            $isInSkuOneQty = true;
                            $skuOneQtyItems[] = $item->sku;
                            break;
                        }
                    }
                    if (in_array($item->sku, $thermalSku)) {
                        $isThermalSKu = true;
                        $thermalSkuItems[] = $item->sku;
                    }
                }
                if ($isInSkuOneQty) {
                    $skuOneQtyItemsString = implode(",", $skuOneQtyItems);
                    $params_data = array(
                        "message" => "Limited transaction for masker : " . $this->salesOrder->getOrderNo() . ", sku(" . $skuOneQtyItemsString . ")",
                        "slack_channel" => $_ENV['OPS_ISSUE_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['OPS_ISSUE_SLACK_USERNAME']
                    );
                    $this->notificationError($params_data);
                }

                if ($isThermalSKu) {
                    $thermalSkuString = implode(",", $thermalSkuItems);
                    $params_data = array(
                        "message" => "Pre order transaction for thermometer : " . $this->salesOrder->getOrderNo() . ", sku (" . $thermalSkuString . ")",
                        "slack_channel" => $_ENV['MERCHANDISE_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['MERCHANDISE_SLACK_CHANNEL']
                    );
                    $this->notificationError($params_data);
                }

                if (isset($this->shoppingCartData['analytics']) && isset($this->shoppingCartData['analytics']['gclid']) && !empty($this->shoppingCartData['analytics']['gclid']['identifier'])) {
                    $salesOrderTrackerLib = new \Library\SalesOrderTracker();

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start salesOrderTrackerLib->orderSaveTrackerProcess library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    $salesOrderTrackerLib->orderSaveTrackerProcess($this->salesOrder->getSalesOrderId(), $this->shoppingCartData['analytics']['gclid']);

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End salesOrderTrackerLib->orderSaveTrackerProcess library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');
                }

                if (
                    isset($this->shoppingCartData['analytics']) &&
                    isset($this->shoppingCartData['analytics']['fbclid']) &&
                    !empty($this->shoppingCartData['analytics']['fbclid']['identifier'])
                ) {
                    $salesOrderTrackerLib = new \Library\SalesOrderTracker();

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start salesOrderTrackerLib->orderSaveTrackerProcess library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    $salesOrderTrackerLib->orderSaveTrackerProcess($this->salesOrder->getSalesOrderId(), $this->shoppingCartData['analytics']['fbclid']);

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End salesOrderTrackerLib->orderSaveTrackerProcess library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');
                }

                if (
                    isset($this->shoppingCartData['analytics']) &&
                    isset($this->shoppingCartData['analytics']['ttclid']) &&
                    !empty($this->shoppingCartData['analytics']['ttclid']['identifier'])
                ) {
                    $salesOrderTrackerLib = new \Library\SalesOrderTracker();
                    $salesOrderTrackerLib->orderSaveTrackerProcess($this->salesOrder->getSalesOrderId(), $this->shoppingCartData['analytics']['ttclid']);
                }

                if (
                    isset($this->shoppingCartData['analytics']) &&
                    isset($this->shoppingCartData['analytics']['vue_ai']) &&
                    !empty($this->shoppingCartData['analytics']['vue_ai']['blox_uuid'])
                ) {
                    $salesOrderTrackerLib = new \Library\SalesOrderTracker();

                    // hit go-cart with cart_id to send analytics to vue.ai endpoint
                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start salesOrderTrackerLib->sendOrderTrackerVueAIAnalytics library/SalesOrderTracker.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');
                    $salesOrderTrackerLib->sendOrderTrackerVueAIAnalytics($this->salesOrder->cart_id);
                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End salesOrderTrackerLib->sendOrderTrackerVueAIAnalytics library/SalesOrderTracker.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                }                

            } catch (\Exception $e) {
                LogHelper::log("qris_invoice_error", "Error :" . $this->salesOrder->getOrderNo() . " : " . $e->getMessage());
                LogHelper::log("qris_invoice_error", "payment cart :" . json_encode($this->shoppingCartData['payment']));
                //revert back stock product                
                if ($this->shoppingCartData['device'] == "vendor") {

                    $vendor = "";
                    if (substr($this->salesOrder->getOrderNo(), 0, 4) == "ODIS") {
                        $vendor = "shopee";
                    } else {
                        $vendor = "tokopedia";
                    }

                    $salesOrderModel = new \Models\SalesOrder();

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', `==== Start Query SELECT shop_name FROM master_store_vendor msv WHERE msv.shop_id = ( SELECT shop_id FROM sales_order_vendor sov WHERE sov.order_no = '{$this->salesOrder->getOrderNo()}') AND msv.vendor = '{$vendor}' library/SalesOrder.php  Cart id: ` . $this->salesOrder->cart_id, 'debug');

                    $sql = "SELECT shop_name FROM master_store_vendor msv WHERE msv.shop_id = (
                        SELECT shop_id FROM sales_order_vendor sov WHERE sov.order_no = '{$this->salesOrder->getOrderNo()}'
                    ) AND msv.vendor = '{$vendor}'";

                    $salesOrderModel->useReadOnlyConnection();
                    $result = $salesOrderModel->getDi()->getShared($salesOrderModel->getConnection())->query($sql);
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );
                    $shopName = $result->fetch();

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', `==== End Query SELECT shop_name FROM master_store_vendor msv WHERE msv.shop_id = ( SELECT shop_id FROM sales_order_vendor sov WHERE sov.order_no = '{$this->salesOrder->getOrderNo()}') AND msv.vendor = '{$vendor}' library/SalesOrder.php  Cart id: ` . $this->salesOrder->cart_id, 'debug');

                    $param = $this->shoppingCartData['reserved_order_no'];
                    $this->revertOrderVendor($param);
                    $params_data = array(
                        "message" => "Order " . $this->salesOrder->getSourceOrderNo() . " dari toko " . $shopName["shop_name"] . " mengalami error pada proses create order",
                        "slack_channel" => $_ENV['SHOPEE_ERROR_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['ORDER_SLACK_USERNAME']
                    );

                    if (substr($param, 0, 4) == "ODIT") {
                        $params_data = array(
                            "message" => "Order " . $this->salesOrder->getSourceOrderNo() . " dari toko " . $shopName["shop_name"] . " mengalami error pada proses create order",
                            "slack_channel" => $_ENV['TOKOPEDIA_ERROR_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['ORDER_SLACK_USERNAME']
                        );
                    }

                    $this->notificationError($params_data);
                } else {
                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start foreach ($this->salesOrder->getItems()->reservedStock as $reservedStock) library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');
                    
                    $productStockModel = new \Library\ProductStock();
                    $stockRevertRequest = array();

                    foreach ($this->salesOrder->getItems()->reservedStock as $reservedStock) {
                        $stockRevertRequest[] = array(
                            "sku" => $reservedStock['sku'],
                            "store_code" => $reservedStock['store_code'],
                            "quantity" => (int)$reservedStock['qty']
                        );
                    }

                    $productStockModel->revertStockV2("payment", $this->salesOrder->getOrderNo(), $stockRevertRequest);

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End foreach ($this->salesOrder->getItems()->reservedStock as $reservedStock) library/SalesOrder.php Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    // revert back shopping cart status                    
                    $this->shoppingCartData['status'] = "hold_payment";

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start this->shoppingCart->validateCart library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    $this->shoppingCart->validateCart();

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End this->shoppingCart->validateCart library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start this->shoppingCart->saveToDb library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    $this->shoppingCart->saveToDb($this->shoppingCartData);

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End this->shoppingCart->saveToDb library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    // Revert penggunaan voucher dan marketing
                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start Revert penggunaan voucher dan marketing library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    $createOrderDelayStart = getenv('CREATE_ORDER_DELAY_START');
                    $createOrderDelayEnd = getenv('CREATE_ORDER_DELAY_END');
                    $nowDateTime = date('Y-m-d H:i:s');
                    $processRevertVoucher = true;
                    if (!empty($createOrderDelayStart) && !empty($createOrderDelayEnd)) {
                        if ($nowDateTime >= $createOrderDelayStart && $nowDateTime <= $createOrderDelayEnd) {
                            $processRevertVoucher = false;
                        }
                    }
                    if ($processRevertVoucher) {
                        $this->salesOrder->getVoucherCollection()->revertSaveData("sales_rule_voucher");
                    }
                    $this->salesOrder->getSalesRulesCustomerCollection()->revertSaveData("sales_rule_customer");

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End Revert penggunaan voucher dan marketing library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start this->cancelPayment library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                    // $this->cancelPayment();

                    \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End this->cancelPayment library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');
                }

                $params_data = array(
                    "message" => "Save order failed : " . $this->salesOrder->getOrderNo() . " | " . $e->getMessage(),
                    "slack_channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                    "slack_username" => $_ENV['MARKETING_SLACK_USERNAME']
                );

                $this->notificationError($params_data);

                if (isset($this->shoppingCartData['refund_no'])) {
                    if (!empty($this->shoppingCartData['refund_no'])) {
                        $creditMemoModel = new \Models\SalesCreditMemo();
                        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start creditMemoModel->updateRefundStatus library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                        $creditMemoModel->updateRefundStatus($this->shoppingCartData['refund_no'], "store_approved");

                        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End creditMemoModel->updateRefundStatus library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start creditMemoModel->updateStatusItem library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

                        $creditMemoModel->updateStatusItem($this->shoppingCartData['refund_no'], "complete");

                        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End creditMemoModel->updateStatusItem library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');
                    }
                }
                if (strpos($e->getMessage(), "untuk produk") !== false) {
                    throw new \Library\HTTPException(
                        $e->getMessage(),
                        500,
                        array($e->getMessage())
                    );
                } else if (strpos($e->getMessage(), "SQLSTATE[45000]") !== false) {
                    $logMsg = $this->salesOrder->getCartId() . " " . $this->salesOrder->getOrderNo();
                    if (!empty($this->salesOrder->getVoucherCollection())) {
                        $voucherListRevert = array();
                        foreach ($this->salesOrder->getVoucherCollection() as $voucher) {
                            $voucherListRevert[] = $voucher->voucher_code;
                        }
                        $logMsg .= " " . join(",", $voucherListRevert);
                    }
                    LogHelper::log("voucher_limit_exceeded", $logMsg);
                    
                    throw new \Library\HTTPException(
                        "Maaf, kuota kode voucher yang Anda gunakan sudah habis",
                        500,
                        array(
                            'msgTitle' => 'Internal Server Error',
                            'dev' => $e->getMessage()
                        )
                    );
                }
                throw new \Library\HTTPException(
                    "Save order failed, please contact ruparupa tech support",
                    500,
                    array(
                        'msgTitle' => 'Internal Server Error',
                        'dev' => $e->getMessage()
                    )
                );
            }
        } else {
            $this->errorCode = $this->salesOrder->getErrorCode();
            $this->errorMessages = $this->salesOrder->getErrorMessages();
        }

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start this->updateMinicart library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start this->updateMinicart library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

        // Warning: unmaintained code written by Ojan will be improved later because these logic is extremely dangerous
        // Minicart post checkout process, in here we want to remove an item from minicart that has relation with current cart
        $listCartTypeWithoutMinicart = ["gift", "instant", "installation", "automated_reorder", "automated_reorder_confirmation", "whatsapp_shopping", "whatsapp_shopping_ctwa"];
        $listCartDeviceWithoutMinicart = ["ace-android", "ace-ios"];
        if (!in_array($this->shoppingCartData['cart_type'], $listCartTypeWithoutMinicart) && !in_array($this->shoppingCartData['device'], $listCartDeviceWithoutMinicart) && $this->shoppingCartData['minicart_id'] != "") {
            $this->updateMinicart();
        }
        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End this->updateMinicart library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start this->insertInstallationDataItem library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

        $this->insertInstallationDataItem();

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End this->insertInstallationDataItem library/SalesOrder.php  Cart id: ' . $this->salesOrder->cart_id, 'debug');

        if (isset($this->shoppingCartData['cart_type'])) {
            if ($this->shoppingCartData['cart_type'] === 'additional_cost') {
                $detail[] = array(
                    "reference_key" => "sales_order_id",
                    "reference_key_value" => (int)$this->salesOrder->getSalesOrderId(),
                    "status" => "waiting_payment"
                );
        
                $param = array(
                    "pending_order_id" => (int)$this->shoppingCartData["b2b"]["pending_order_id"],
                    "pending_order_status" => "waiting_payment",
                    "pending_order_details" => $detail
                );

                $this->createPendingOrderDetail($param);

            } else if ($this->shoppingCartData['cart_type'] === 'installation' && 
                       isset($this->shoppingCartData['reference_installation_pending_order_no']) && 
                       $this->shoppingCartData['reference_installation_pending_order_no'] != "") {
                      
                $detail[] = array(
                    "reference_key" => "sales_installation_id",
                    "reference_key_value" => (int)$this->shoppingCartData['sales_installation_id'],
                    "status" => "waiting_payment"
                );
        
                $param = array(
                    "pending_order_no" => $this->shoppingCartData['reference_installation_pending_order_no'],
                    "pending_order_status" => "waiting_payment",
                    "pending_order_details" => $detail
                );

                $this->createPendingOrderDetail($param);

            } else if ($this->shoppingCartData['cart_type'] === 'informa_b2b' || $this->shoppingCartData['cart_type'] === 'ace_b2b') {
                $this->createOrderB2b($this->shoppingCartData, $this->salesOrder->getSalesOrderId());
            }
        }

        if ($this->shoppingCartData['payment']['method'] == 'term_of_payment') {
            $this->sentToKlSysGoOrderV2($this->salesOrder->getOrderNo());
        }
    }

    public function updateMinicart()
    {
        $miniCart = new \Models\MiniCart();
        $miniCart->minicart_id = $this->shoppingCartData['minicart_id'];
        $listCartSku = array();
        for ($i = 0; $i < count($this->shoppingCartData['items']); $i++) {
            array_push($listCartSku, $this->shoppingCartData['items'][$i]['sku']);
        }
        $miniCart = $miniCart->miniCartLoad();
        $data['items'] = $miniCart['items'];
        $items = array();
        for ($i = 0; $i < count($data['items']); $i++) {
            $itemObj = new \Models\SalesOrderItem();
            list($stockFound, $origin_stock_qty)  =  $itemObj->checkStockAvailableWithoutStoreCode($data['items'][$i]['sku'], $this->shoppingCartData['cart_type']);
            if ($data['items'][$i]['is_checked'] == false) {
                array_push($items, $data['items'][$i]);
            } else if ($data['items'][$i]['is_checked'] == true && (!$stockFound || $data['items'][$i]['status'] == 0) && !in_array($data['items'][$i]['sku'], $listCartSku)) {
                array_push($items, $data['items'][$i]);
            }
        }
        $dataMiniCart = array();
        $dataMiniCart['items'] = $items;
        $dataMiniCart['minicart_id'] = $this->shoppingCartData['minicart_id'];
        $cartLib = new \Library\Cart();
        $cartLib->updateMinicart($dataMiniCart);
    }

    public function notificationError($params_data = array())
    {
        $notificationText = $params_data["message"];
        $params = [
            "channel" => $params_data["slack_channel"],
            "username" => $params_data["slack_username"],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    public function revertOrderVendor($param = "")
    {

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "order_no" => $param,
            "message" => "revertOrderShopee"
        ];
        $nsq->publishCluster('shopee', $message);
    }

    public function cancelPayment()
    {
        /**
         * @todo : cancel payment VA
         */
        \Helpers\LogHelper::log("api_call", "cancel payment", "error");
        \Helpers\LogHelper::log("api_call", "payment type:" . $this->shoppingCartData['payment']['type'], "error");
        if (isset($this->shoppingCartData['payment']['type'])) {
            if ($this->shoppingCartData['payment']['type'] == 'credit_card' || $this->shoppingCartData['payment']['type'] == 'debit_card') {
                $paymentArray = $this->shoppingCartData['payment'];
                $paymentMethod = $paymentArray['method'];
                $addtitionalData = json_decode($paymentArray['additional_data'], true);
                $transaction_id = "";
                switch ($paymentMethod) {
                    case "vpayment":
                    case "vpaymentins":
                        $transaction_id = $addtitionalData['transaction_id'];
                }

                $data = [
                    "order_no" => $this->shoppingCartData['reserved_order_no'],
                    "payment_method" => $paymentMethod,
                    "transaction_id" => $transaction_id
                ];

                $nsq = new \Library\Nsq();
                $data = [
                    "data" => $data,
                    "message" => "cancelTransaction"
                ];
                $nsq->publishCluster('transaction', $data);
            }
        }
    }

    public function getAllOrder($params = array())
    {
        $salesOrderModel = new \Models\SalesOrder();
        $action = '';
        $formatInvoice = isset($params['format_invoice']) ? $params['format_invoice'] : 0;
        unset($params['format_invoice']);
        $resultSalesOrder = $salesOrderModel::query();
        $resultSalesOrder->join("Models\SalesCustomer", "Models\SalesCustomer.sales_order_id = Models\SalesOrder.sales_order_id");

        if (isset($params['sales_order_id'])) {
            $resultSalesOrder->andWhere("Models\SalesOrder.sales_order_id = " . $params['sales_order_id']);

            unset($params['sales_order_id']);
        }

        if (isset($params['order_type'])) {
            $resultSalesOrder->andWhere("Models\SalesOrder.order_type = '" . $params['order_type'] . "'");

            unset($params['order_type']);
        }

        if (isset($params['discount'])) {
            $resultSalesOrder->andWhere("Models\SalesOrder.discount_amount >= " . $params['discount']);

            unset($params['discount']);
        }

        if (isset($params['action'])) {
            $action = $params['action'];

            unset($params['action']);
        }

        foreach ($params as $key => $val) {
            if ($key == "flag_customer_info") {
                continue;
            }

            if ($key == "date_from") {
                $resultSalesOrder->andWhere("created_at >= '" . $val . "'");
            } else if ($key == "date_to") {
                $resultSalesOrder->andWhere("created_at  <= '" . $val . "'");
            } else if ($key != "order_by") {
                if ($key == 'customer_id') {
                    $key = 'Models\SalesCustomer.customer_id';
                }
                $resultSalesOrder->andWhere($key . " = '" . trim($val) . "'");
            }
        }

        if (!empty($params["order_by"])) {
            $resultSalesOrder->orderBy($params["order_by"]);
        } else {
            $resultSalesOrder->orderBy("created_at DESC");
        }

        $flagCustInfo = "no";
        if (isset($params['flag_customer_info'])) {
            if ($params['flag_customer_info'] == "yes") {
                $flagCustInfo = "yes";
            }
        }

        $result = $resultSalesOrder->execute();
        if (empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your orders";
            return array();
        } else {
            if ($flagCustInfo == "yes") {
                foreach ($result as $salesOrder) {
                    $salesOrderArray = $salesOrder->toArray([], true);

                    // For B2B order only (for B2B OMS dashboard reporting)
                    if ($salesOrder->SalesB2BInformation) {
                        $b2bInfoArray = $salesOrder->SalesB2BInformation->toArray();
                        $salesOrderArray['is_tax_invoice'] = (isset($b2bInfoArray['is_tax_invoice'])) ? (int)$b2bInfoArray['is_tax_invoice'] : 0;
                    }

                    if ($action == "download_order_b2b") {
                        if ($salesOrder->SalesCustomer) {
                            $salesOrderArray['customer'] = $salesOrder->SalesCustomer->toArray(["customer_id", "customer_firstname", "customer_lastname", "customer_email", "customer_phone"]);
                        }

                        if ($salesOrder->SalesOrderAddress) {
                            foreach ($salesOrder->SalesOrderAddress as $orderAddress) {
                                $orderAddressArray = $orderAddress->getDataArray([], true);
                                if ($orderAddressArray['address_type'] == 'shipping') {
                                    $salesOrderArray['shipping_address'][] = $orderAddressArray;
                                }
                            }
                        }

                        if ($salesOrder->SalesOrderPayment) {
                            $paymentArray = $salesOrder->SalesOrderPayment->toArray();
                            $salesOrderArray['payment'] = $salesOrder->SalesOrderPayment->getDataArray();
                        }
                    }

                    $allOrders[] = $salesOrderArray;
                }

                $allOrders['recordsTotal'] = count($allOrders);
                return $allOrders;
            }

            $allOrders = array();
            foreach ($result as $salesOrder) {
                $salesOrderArray = $salesOrder->toArray([], true);
                unset($salesOrderArray['customer_id']);

                $salesOrderArray['customer'] = array();
                if ($salesOrder->SalesCustomer) {
                    $salesOrderArray['customer'] = $salesOrder->SalesCustomer->toArray(["customer_id", "customer_firstname", "customer_lastname", "customer_email", "customer_phone", "customer_is_guest"]);
                }

                if ($salesOrder->SalesCustomer->Customer2Group) {
                    $salesOrderArray['customer']['customer_groups'] = $salesOrder->SalesCustomer->Customer2Group->toArray(["group_id", "member_number"]);
                }

                if ($salesOrder->SalesCustomerGroup) {
                    $salesOrderArray['sales_customer_group'] = $salesOrder->SalesCustomerGroup->toArray();
                }

                if ($salesOrder->customerGroup) {
                    $customer_group = new \Models\CustomerGroup();
                    $sql = "SELECT * FROM customer_group";

                    $customer_group->useReadOnlyConnection();
                    $result = $customer_group->getDi()->getShared($customer_group->getConnection())->query($sql);
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );
                    $salesOrderArray['master_customer_group'] = $result->fetchAll();
                }

                if ($salesOrder->RemarkHold) {
                    $remarkHold = $salesOrder->RemarkHold->toArray(['remark']);
                    if (!empty($remarkHold['remark'])) {
                        $salesOrderArray['remark_hold'] = $remarkHold['remark'];
                    }
                }

                if ($salesOrder->SalesOrderAddress) {
                    foreach ($salesOrder->SalesOrderAddress as $orderAddress) {
                        $orderAddressArray = $orderAddress->toArray();
                        $salesOrderAddress = new \Models\SalesOrderAddress();
                        $salesOrderAddress->setFromArray($orderAddressArray);
                        if ($orderAddress->getAddressType() == 'billing') {
                            $salesOrderArray['billing_address'] = $salesOrderAddress->getDataArray();
                        }
                    }
                }

                if ($salesOrder->SalesOrderPayment) {
                    $paymentArray = $salesOrder->SalesOrderPayment->toArray();
                    unset($paymentArray['sales_order_id']);
                    $salesOrderArray['payment'] = $salesOrder->SalesOrderPayment->getDataArray();

                    if ($salesOrder->SalesB2BInformation) {
                        $b2bInfoArray = $salesOrder->SalesB2BInformation->toArray();
                        $salesOrderArray['payment']['po_number'] = (isset($b2bInfoArray['po_number'])) ? $b2bInfoArray['po_number'] : '';
                        $salesOrderArray['payment']['po_file'] = (isset($b2bInfoArray['po_file'])) ? $b2bInfoArray['po_file'] : '';
                        $salesOrderArray['remark'] = (isset($b2bInfoArray['remark'])) ? $b2bInfoArray['remark'] : '';
                        $salesOrderArray['request_delivery_date'] = (isset($b2bInfoArray['request_delivery_date'])) ? $b2bInfoArray['request_delivery_date'] : '';
                        $salesOrderArray['is_tax_invoice'] = (isset($b2bInfoArray['is_tax_invoice'])) ? (int)$b2bInfoArray['is_tax_invoice'] : 0;
                        $salesOrderArray['payment']['so_sap_no'] = (isset($b2bInfoArray['so_sap_no'])) ? $b2bInfoArray['so_sap_no'] : '';
                        $salesOrderArray['payment']['so_sap_status'] = (isset($b2bInfoArray['so_sap_no'])) ? $b2bInfoArray['so_sap_status'] : '';
                    }
                }

                if ($salesOrder->SalesOrderItem) {
                    $pickup_order_address = [];
                    $handling_fee_adjust = 0;
                    foreach ($salesOrder->SalesOrderItem as $orderItem) {
                        $orderItemArray = $orderItem->getDataArray([], true);
                        if ($orderItemArray['delivery_method'] == 'pickup' && !empty($orderItem->PickupPoint) && !isset($pickup_order_address[$orderItemArray['pickup_code']])) {
                            $pickup_order_address[$orderItemArray['pickup_code']] = $orderItem->PickupPoint->toArray(['pickup_name', 'address_line_1', 'address_line_2']);
                        }

                        if ($orderItem->Supplier) {
                            if ($orderItemArray['shipping_info'] == 'delivery') {
                                $orderItemArray['shipping_by'] = "Dikirim oleh " . $orderItem->Supplier->getDikirimOleh();
                            } elseif ($orderItemArray['shipping_info'] == 'pickup_in_store') {
                                $pickupName = (isset($pickup_order_address[$orderItemArray['pickup_code']]['pickup_name'])) ? $pickup_order_address[$orderItemArray['pickup_code']]['pickup_name'] : '';
                                $addressLine1 = (isset($pickup_order_address[$orderItemArray['pickup_code']]['address_line_1'])) ? $pickup_order_address[$orderItemArray['pickup_code']]['address_line_1'] : '';
                                $addressLine2 = (isset($pickup_order_address[$orderItemArray['pickup_code']]['address_line_2'])) ? $pickup_order_address[$orderItemArray['pickup_code']]['address_line_2'] : '';

                                $orderItemArray['shipping_by'] = "Pengambilan di toko " . $pickupName . ' , ' . $addressLine1 . ' , ' . $addressLine2;
                            }
                        }
                        $handling_fee_adjust += $orderItem->getHandlingFeeAdjust() * $orderItem->getQtyOrdered();

                        $sql = "SELECT brand_name FROM sales_order_item_flat WHERE sales_order_item_id = '" . $orderItemArray["sales_order_item_id"] . "'";
                        $result = $salesOrderModel->getDi()->getShared('dbMaster')->query($sql);
                        $result->setFetchMode(
                            \Phalcon\Db::FETCH_ASSOC
                        );
                        $brand_name = $result->fetchAll()[0]["brand_name"];
                        $orderItemArray['brand_name'] = $brand_name;

                        $salesOrderArray['items'][] = $orderItemArray;
                    }

                    $salesOrderArray['pickup_address'] = $pickup_order_address;
                    $salesOrderArray['handling_fee_adjust'] = $handling_fee_adjust;
                }

                //change stock assignment store name for 1000DC into 1000DC store code detail
                $is1000DC = 0;
                $DC1000DetailStoreCode = "";
                if ($salesOrder->SalesStockAssignation) {
                    $isAlreadyRequested = 0;
                    foreach ($salesOrder->SalesStockAssignation as $key => $stockAssignment) {
                        $salesOrderArray['stock_assignment'][$key] = $stockAssignment->toArray(['sku', 'store_code', 'stock', 'qty_ordered', 'last_stock', 'stock_store_code', 'stock_store_code_qty','stock_store_code_remaining_qty']);
                        $salesOrderArray['stock_assignment'][$key] += array('store_name' => $stockAssignment->Store->getName());

                        // Set stock_store_code
                        $salesOrderArray['stock_assignment'][$key] += array('stock_store_code' => '');
                        $sql = "SELECT sales_order_item_id, stock_store_code FROM sales_order_item WHERE sales_order_id = " . $salesOrderArray['sales_order_id'] . " AND sku = '" . $salesOrderArray['stock_assignment'][$key]['sku'] . "' LIMIT 1";
                        $result = $salesOrderModel->getDi()->getShared('dbMaster')->query($sql);
                        $result->setFetchMode(
                            \Phalcon\Db::FETCH_ASSOC
                        );
                        $salesOrderItem = $result->fetchAll();
                        if (count($salesOrderItem) > 0) {
                            $stockStoreCode = $salesOrderItem[0]["stock_store_code"];
                            $salesOrderItemId = $salesOrderItem[0]["sales_order_item_id"];
                            if ($stockStoreCode) {
                                $salesOrderArray['stock_assignment'][$key]['stock_store_code'] = $stockStoreCode;
                            }
                        }
                       
                        // Set is_tarik_toko
                        $salesOrderArray['stock_assignment'][$key] += array('is_tarik_toko' => 0);
                        if ($stockStoreCode && $salesOrderItemId) {
                            $sql = "SELECT invoice_item_id FROM sales_invoice_item WHERE sales_order_item_id = " . $salesOrderItemId . " LIMIT 1";
                            $result = $salesOrderModel->getDi()->getShared('dbMaster')->query($sql);
                            $result->setFetchMode(
                                \Phalcon\Db::FETCH_ASSOC
                            );
                            $salesInvoiceItem = $result->fetchAll();

                            if (count($salesInvoiceItem) > 0) {
                                $invoiceItemId = $salesInvoiceItem[0]["invoice_item_id"];
                                if ($invoiceItemId) {
                                    $sql = "SELECT tarik_toko_status FROM sales_invoice_item_tarik_toko WHERE invoice_item_id = " . $invoiceItemId . " LIMIT 1";
                                    $result = $salesOrderModel->getDi()->getShared('dbMaster')->query($sql);
                                    $result->setFetchMode(
                                        \Phalcon\Db::FETCH_ASSOC
                                    );
                                    $salesInvoiceItemTarikToko = $result->fetchAll();
                                    if (count($salesInvoiceItemTarikToko) > 0) {
                                        $tarikTokoStatus = $salesInvoiceItemTarikToko[0]["tarik_toko_status"];
                                        if ($tarikTokoStatus) {
                                            $salesOrderArray['stock_assignment'][$key]['is_tarik_toko'] = 1;
                                        }
                                    }
                                }
                            }                          
                        }
                        
                        if (strpos($salesOrderArray['stock_assignment'][$key]['store_code'], '1000DC') !== false) {
                            if ($isAlreadyRequested == 0) {
                                $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                                // \Helpers\LogHelper::log("createListenerTPError", $item['_id'],"notice");
                                $endPoint = 'legacy/stock/'.$salesOrderArray['stock_assignment'][$key]['sku'] . $salesOrderArray['stock_assignment'][$key]['store_code'];
                                $apiWrapper->setEndPoint($endPoint);

                                if ($apiWrapper->send("get")) {
                                    $apiWrapper->formatResponse();
                                };
                                if (!empty($apiWrapper->getError())) {
                                    \Helpers\LogHelper::log("createListenerTPError", "Error Getting 1000 Store Code Detail from SKU : " . $salesOrderArray['stock_assignment'][$key]['sku'] . ", Error : " . json_encode($apiWrapper->getError()), "error");
                                }
                                $stock1000Detail = $apiWrapper->getData();
                                $stock1000Detail['store_code_detail'] = (isset($stock1000Detail['store_code_detail'])) ? $stock1000Detail['store_code_detail'] : '';
                                $salesOrderArray['stock_assignment'][$key]['store_name'] = $stock1000Detail['store_code_detail'];
                                $is1000DC = 1;
                                $DC1000DetailStoreCode = $stock1000Detail['store_code_detail'];;
                            } else {
                                $salesOrderArray['stock_assignment'][$key]['store_name'] = $DC1000DetailStoreCode;
                            }
                        }
                    }
                }

                if ($salesOrder->SalesInvoice) {
                    foreach ($salesOrder->SalesInvoice as $invoice) {
                        $invoiceModel = new \Models\SalesInvoice();
                        $invoiceModel->setFromArray($invoice->toArray([], true));
                        $salesOrderArray['invoices'][] = $invoiceModel->getDataArray(); // invoice kalau banyak ?
                    }

                    //change supplier alias for 1000DC into 1000DC store code detail
                    if (isset($salesOrderArray['invoices'])) {
                        foreach ($salesOrderArray['invoices'] as $keyInvoice => $invoice) {
                            if ($is1000DC == 1) {
                                foreach ($invoice['items'] as $key => $items) {

                                    if (substr($items['store_code'], 0, 2) === "DC" || $items['store_code'] == "exclude") {
                                        // $salesOrderArray['invoices']['items'][$key]['supplier_alias'] = $DC1000DetailStoreCode;
                                        $salesOrderArray['invoices'][$keyInvoice]['items'][$key]['supplier_alias'] = $DC1000DetailStoreCode;
                                    }
                                }
                            }
                        }
                    }

                    if ($formatInvoice == 1) {
                        foreach ($salesOrderArray['items'] as $key => $item) {
                            $productModel = new \Models\Product();
                            $esProduct = $productModel->searchProductFromElastic($item['sku']);
                            if ($esProduct) {
                                $salesOrderArray['items'][$key]['elastic_data'] = $esProduct;
                            }
                        }

                        if (isset($salesOrderArray['invoices'])) {
                            // we have to make group of invoices if flag applied
                            $tempInvArray = array();
                            foreach ($salesOrderArray['invoices'] as $key => $val) {
                                if ($val['delivery_method'] == 'delivery' || $val['delivery_method'] == 'store_fulfillment' || $val['delivery_method'] == 'ownfleet' || $val['delivery_method'] == 'marketplace') {
                                    if ($val['supplier']['supplier_code'] == 'MP') {
                                        $tempInvArray[$val['supplier']['name']][] = $val;
                                    } else {
                                        $tempInvArray['Ruparupa'][] = $val;
                                    }
                                } else if ($val['delivery_method'] === 'pickup') {
                                    $tempInvArray['Pickup ' . $val['store']['name']][] = $val;
                                }
                            }
                            $counter = 0;
                            $combineAllItem = array();
                            foreach ($tempInvArray as $key => $listOfInvoice) {
                                $realInvoice = array();
                                $groupingItem = array();

                                foreach ($listOfInvoice as $idx => $val) {
                                    if (empty($groupingItem)) {
                                        $val['sender'] = $key;
                                        array_push($groupingItem, $val);
                                    } else {
                                        // if not empty, we need to compare
                                        foreach ($groupingItem as $ctr => $diff) {
                                            $tempInvNo = array();
                                            $tempItems = array();
                                            if ($diff['pickup_code']['pickup_code'] === $val['pickup_code']['pickup_code']) {
                                                // now we have to merge invoice_no and items
                                                array_push($tempInvNo, $diff['invoice_no'], $val['invoice_no']);
                                                $tempItems = array_merge($diff['items'], $val['items']);
                                                $groupingItem[$ctr]['invoice_no'] = implode(',', $tempInvNo);
                                                $groupingItem[$ctr]['items'] = $tempItems;
                                            } else {
                                                // it means, if all of groupingItem passed and there is no match, we should push
                                                if (($ctr + 1) == count($groupingItem)) {
                                                    $val['sender'] = $key;
                                                    array_push($groupingItem, $val);
                                                }
                                            }
                                        }
                                    }
                                }

                                $combineAllItem = array_merge($combineAllItem, $groupingItem);
                            }

                            // looping through all items in invoice
                            // use combineAllItem not realInvoice - todo
                            foreach ($combineAllItem as $key => $value) {
                                foreach ($value['items'] as $ctx => $item) {
                                    $productModel = new \Models\Product();
                                    $esProduct = $productModel->searchProductFromElastic($item['sku']);
                                    if ($esProduct) {
                                        $combineAllItem[$key]['items'][$ctx]['elastic_data'] = $esProduct;
                                    }
                                }
                            }

                            $salesOrderArray['invoices'] = $combineAllItem;
                        }
                    }
                }

                if ($salesOrder->SalesRuleOrder) {
                    foreach ($salesOrder->SalesRuleOrder as $key => $salesRuleOrder) {
                        $salesOrderArray['voucher'][$key] = $salesRuleOrder->toArray(['rule_id', 'voucher_code', 'voucher_amount_used']);
                        if ($salesRuleOrder->SalesRuleVoucher)
                            $salesOrderArray['voucher'][$key]['type'] = $salesRuleOrder->SalesRuleVoucher->getType();
                    }
                }

                // cashback hold information
                $salesOrderTpModel = new \Models\SalesOrderTp();
                if (isset($params['order_no'])) {
                    $sql = "SELECT * FROM sales_cashback_hold WHERE order_no = '" . $params['order_no'] . "'";
                    $result = $salesOrderTpModel->getDi()->getShared('dbMaster')->query($sql);
                    if ($result->numRows() > 0) {
                        $result->setFetchMode(
                            Database::FETCH_ASSOC
                        );
                        $rowData = $result->fetchAll()[0];

                        if ($rowData['status'] == 1) {
                            $salesOrderArray['hold_cashback'] = "1";
                        }
                    }
                }


                // reorder information
                $reorderInformation = new \Models\SalesOrder();
                if (isset($params['order_no'])) {
                    $reorderData = $reorderInformation->find('reference_order_no = "' . $params['order_no'] . '"');
                    if ($reorderData) {
                        foreach ($reorderData as $reorder) {
                            $salesOrderItem = $reorder->SalesOrderItem;
                            if ($salesOrderItem) {
                                foreach ($salesOrderItem as $item) {
                                    $sku = $item->toArray()['sku'];
                                    $salesOrderArray['reorder'][$sku] = $reorder->toArray()['order_no'];
                                }
                            }
                        }
                    }
                }

                // installation item data
                $salesOrderItemInstallation = new \Models\SalesOrderItemInstallation();
                if (isset($salesOrderArray['sales_order_id'])) {
                    $rowDataArray = array();

                    $sql = "SELECT sales_order_id,sales_order_item_id,is_installed FROM sales_order_item_installation WHERE sales_order_id = '" . $salesOrderArray['sales_order_id'] . "'";
                    $result = $salesOrderItemInstallation->getDi()->getShared('dbMaster')->query($sql);
                    if ($result->numRows() > 0) {
                        $result->setFetchMode(
                            Database::FETCH_ASSOC
                        );
                        $rowDatas = $result->fetchAll();

                        foreach ($rowDatas as $data) {
                            $rowDataArray[$data['sales_order_item_id']] = true;
                        }
                    }

                    //salesorder items
                    foreach ($salesOrderArray['items'] as $key => $item) {
                        $salesOrderArray['items'][$key]['is_installed'] = false;

                        if (array_key_exists($item['sales_order_item_id'], $rowDataArray)) {
                            $salesOrderArray['items'][$key]['is_installed'] = true;
                        }
                    }

                    //invoices items

                    if (isset($salesOrderArray['invoices'])) {
                        foreach ($salesOrderArray['invoices'] as $keyInv => $itemInv) {
                            foreach ($itemInv['items'] as $key => $item) {
                                $salesOrderArray['invoices'][$keyInv]['items'][$key]['is_installed'] = false;

                                if (array_key_exists($item['sales_order_item_id'], $rowDataArray)) {
                                    $salesOrderArray['invoices'][$keyInv]['items'][$key]['is_installed'] = true;
                                }
                            }
                        }
                    }
                }

                $allOrders[] = $salesOrderArray;
            }

            $allOrders['recordsTotal'] = count($allOrders);
        }

        return $allOrders;
    }

    public function getAllOrderShipment($params = array())
    {
        $salesOrderModel = new \Models\SalesOrder();
        $resultSalesOrder = $salesOrderModel::query();
        $resultSalesOrder->andWhere("order_no = '" . trim($params['order_no']) . "'");
        $result = $resultSalesOrder->execute();
        $salesOrderArray = array();
        if (empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your orders";
            return array();
        } else {
            $orderModel = new \Models\SalesOrder();
            $product = new \Models\Product();

            $allOrders = array();
            foreach ($result as $salesOrder) {
                $salesOrderArray = $salesOrder->toArray([], true);
                if (isset($salesOrderArray["cart_rules"])) {
                    unset($salesOrderArray["cart_rules"]);
                }

                $salesOrderArray['refund'] = array();
                $query = "select refund_no, status, refund_type, created_at
                from sales_return_refund  srr
                where order_no = '{$salesOrderArray['order_no']}'
                UNION
                select retref_no, status, srd.type, created_at
                from sales_retref_dc srd 
                where order_no = '{$salesOrderArray['order_no']}'";

                $orderModel->useReadOnlyConnection();
                $result = $orderModel->getDi()->getShared($orderModel->getConnection())->query($query);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );
                $dataRefund = $result->fetchAll();
                if (count($dataRefund) > 0) {
                    for ($idx = 0; $idx < count($dataRefund); $idx++) {                      
                        $refund = $dataRefund[$idx];
                        $refundNo = $refund['refund_no'];

                        $query = "";

                        // check if refund starts with RFDC or RTDC
                        if (strpos($refundNo, 'RFDC') !== false || strpos($refundNo, 'RTDC') !== false) {
                            $query = "select
                                    srd.invoice_no,
                                    sv.voucher_code,
                                    sv.status,
                                    srdv.type = 'product' as is_voucher_product
                                from
                                    sales_retref_dc srd
                                inner join sales_retref_dc_voucher srdv on srd.retref_no = srdv.retref_no
                                inner join salesrule_voucher sv on srdv.voucher_id = sv.voucher_id
                                where
                                    srd.retref_no = '{$refundNo}'";
                            
                            $queryRefundItem = "select srri.name, srri.sku,srri.qty_retref as qty_refunded from sales_retref_dc_item srri
                                where srri.retref_no = '{$refundNo}'";
                        } else {
                            $query = "select
                                srr.invoice_no,
                                srv.voucher_code,
                                srv.status,
                                srv.voucher_id = srr.voucher_product_id is_voucher_product
                            from
                                salesrule_voucher srv
                            inner join sales_return_refund srr on srv.voucher_id = srr.voucher_product_id
                                or srv.voucher_id = srr.voucher_refund_bc_id
                            where
                                srr.refund_no = '{$refundNo}'";
                            
                            $queryRefundItem = "select srri.name, srri.sku,srri.qty_refunded from sales_return_refund_item srri
                                where srri.refund_no = '{$refundNo}'";
                        }

                        $result = $orderModel->getDi()->getShared($orderModel->getConnection())->query($query);
                        $vouchers = $result->fetchAll();

                        $resultItem = $orderModel->getDi()->getShared($orderModel->getConnection())->query($queryRefundItem);
                        $refundItem = $resultItem->fetchAll();

                        $voucherProduct = "";
                        $voucherBC = "";

                        $voucherProductStatus = 0;
                        $voucherBCStatus = 0;

                        foreach ($vouchers as $voucher) {
                            if ($voucher['is_voucher_product'] == "1") {
                                $voucherProduct = $voucher['voucher_code'];
                                $voucherProductStatus = $voucher['status'];
                            } else {
                                $voucherBC = $voucher['voucher_code'];
                                $voucherBCStatus = $voucher['status'];
                            }
                        }

                        for ($idxItem = 0; $idxItem < count($refundItem); $idxItem++) {
                            $paramSource = ["variants"];
                            $item = $refundItem[$idxItem];
                            $sku = $item['sku'];
                            $getProductElastics = $product->getDetailFromElastic($sku, $paramSource);
                            $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                            $refundItem[$idxItem]['product_image_url'] = $imageURL;
                        }

                        $dataRefund[$idx]['voucher_product'] = $voucherProduct;
                        $dataRefund[$idx]['voucher_refund_bc'] = $voucherBC;

                        $dataRefund[$idx]['voucher_product_status'] = $voucherProductStatus;
                        $dataRefund[$idx]['voucher_refund_bc_status'] = $voucherBCStatus;
                        $dataRefund[$idx]['item'] = $refundItem;
                        if (!empty($voucher['invoice_no'])){
                            $dataRefund[$idx]['invoice_no'] = $voucher['invoice_no'];
                        }
                    }

                    $salesOrderArray['refund'] = $dataRefund;
                }

                $salesOrderArray['tugu'] = array();
                $query = "select return_no, status, return_type, created_at
                from sales_return_tugu 
                where order_no = '{$salesOrderArray['order_no']}'
                UNION
                select tugu_no as return_no, status, std.type as return_type, created_at
                from sales_tugu_dc std 
                where order_no = '{$salesOrderArray['order_no']}'";

                $result = $orderModel->getDi()->getShared($orderModel->getConnection())->query($query);
                $result->setFetchMode(
                    Database::FETCH_ASSOC
                );
                $dataTugu = $result->fetchAll();
                if (count($dataTugu) > 0) {
                    for ($idx = 0; $idx < count($dataTugu); $idx++){
                        $tugu = $dataTugu[$idx];
                        $returnNo = $tugu['return_no'];
                        if (strpos($returnNo, 'TGDC') !== false ) {
                            $queryRefundItem = "select srri.name, srri.sku,srri.qty_tugu as qty_refunded from sales_tugu_dc_item srri
                                where srri.tugu_no = '{$returnNo}'";
                        } else { 
                            $queryRefundItem = "select srri.name, srri.sku,srri.qty_refunded from sales_return_refund_item srri
                                where srri.refund_no = '{$returnNo}'";
                        }
                        $resultItem = $orderModel->getDi()->getShared($orderModel->getConnection())->query($queryRefundItem);
                        $refundItem = $resultItem->fetchAll();

                        for ($idxItem = 0; $idxItem < count($refundItem); $idxItem++) {
                            $paramSource = ["variants"];
                            $item = $refundItem[$idxItem];
                            $sku = $item['sku'];
                            $getProductElastics = $product->getDetailFromElastic($sku, $paramSource);
                            $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                            $refundItem[$idxItem]['product_image_url'] = $imageURL;
                        }

                        $dataTugu[$idx]['item'] = $refundItem;
                    }
                    $salesOrderArray['tugu'] = $dataTugu;
                }

                if ($salesOrder->Customer && $salesOrder->SalesCustomer && !empty($params['email'])) {
                    $salesOrderArray['customer'] = $salesOrder->Customer->toArray();
                    if ((strtolower($salesOrderArray['customer']['email']) <> strtolower($params['email'])) && (strtolower($salesOrder->SalesCustomer->getCustomerEmail()) != strtolower($params['email']))) {
                        return array();
                    }

                    $baseQuery = 'select id from customer_refund_rekening where customer_id = ' . $salesOrderArray['customer']['customer_id'];
                    $query = "
                        select
                            ({$baseQuery} and is_active = 10 limit 1) is not null have_rekening,
                            ({$baseQuery} and is_active = 10 and is_default = 10 limit 1) is not null have_default_rekening";

                    $result = $orderModel->getDi()->getShared($orderModel->getConnection())->query($query);
                    $customerRekeningStatus = $result->fetchAll();

                    if (count($customerRekeningStatus) > 0) {
                        $salesOrderArray['customer']['is_already_have_rekening'] = $customerRekeningStatus[0]['have_rekening'] == "1";
                        $salesOrderArray['customer']['is_already_default_rekening'] = $customerRekeningStatus[0]['have_default_rekening'] == "1";
                    } else {
                        $salesOrderArray['customer']['is_already_have_rekening'] = false;
                        $salesOrderArray['customer']['is_already_default_rekening'] = false;
                    }
                }

                if ($salesOrder->Customer && $salesOrder->SalesCustomer && !empty($params['phone'])) {
                    $salesOrderArray['customer'] = $salesOrder->Customer->toArray();
                    if (($salesOrderArray['customer']['phone'] <> $params['phone']) && ($salesOrder->SalesCustomer->getCustomerPhone() != $params['phone'])) {
                        return array();
                    }
                }

                if ($salesOrder->SalesOrderAddress) {
                    foreach ($salesOrder->SalesOrderAddress as $orderAddress) {
                        $orderAddressArray = $orderAddress->getDataArray([], true);
                        if ($orderAddressArray['address_type'] == 'shipping') {
                            $salesOrderArray['shipping_address'][] = $orderAddressArray;
                        }
                    }
                }

                if ($salesOrder->SalesOrderPayment) {
                    $paymentArray = $salesOrder->SalesOrderPayment->toArray();
                    unset($paymentArray['sales_order_id']);
                    $salesOrderArray['payment'] = $salesOrder->SalesOrderPayment->getDataArray();

                    // Get Issue Bank - payment credit_card
                    $issueBank = "";
                    if (($salesOrderArray['payment']['type'] == "cc" || $salesOrderArray['payment']['type'] == "credit_card" || $salesOrderArray['payment']['type'] == "debit_card") && $salesOrderArray['payment']['cc_bin'] != "") {
                        $financeLib = new \Library\Finance();
                        $issueBank = $financeLib->GetBankNameCCBin($salesOrderArray['payment']['cc_bin']);
                    }
                    $salesOrderArray['payment']['issue_bank'] = $issueBank;
                }

                if ($salesOrder->SalesOrderItem) {
                    $pickup_order_address = [];
                    $handling_fee_adjust = 0;
                    foreach ($salesOrder->SalesOrderItem as $orderItem) {
                        $orderItemArray = $orderItem->getDataArray([], true);

                        if ($orderItemArray['delivery_method'] == 'pickup') {
                            $orderItemArray['shipping_address'] = array();
                        }

                        if (($orderItemArray['delivery_method'] == 'pickup' || $orderItemArray['delivery_method'] == 'store_fulfillment' || $orderItemArray['delivery_method'] == 'ownfleet') && !empty($orderItem->PickupPoint) && !isset($pickup_order_address[$orderItemArray['pickup_code']])) {
                            $pickup_order_address[$orderItemArray['pickup_code']] = $orderItem->PickupPoint->toArray(['pickup_name', 'address_line_1', 'address_line_2', 'geolocation']);
                        }

                        $geolocation = (isset($pickup_order_address[$orderItemArray['pickup_code']]['geolocation'])) ? $pickup_order_address[$orderItemArray['pickup_code']]['geolocation'] : '';

                        if ($orderItem->Supplier) {
                            if ($orderItemArray['shipping_info'] == 'delivery') {
                                if ($orderItemArray['delivery_method'] == 'store_fulfillment' || $orderItemArray['delivery_method'] == 'ownfleet') {
                                    $orderItemArray['shipping_by'] = $orderItem->Supplier->getDikirimOleh();

                                    $orderItemArray['shipping_from_store'] = "";
                                    if (isset($pickup_order_address[$orderItemArray['pickup_code']]['pickup_name'])) {
                                        $orderItemArray['shipping_from_store'] = trim($pickup_order_address[$orderItemArray['pickup_code']]['pickup_name']);
                                    }

                                    $orderItemArray['shipping_from_geolocation'] = trim($geolocation);
                                } else {
                                    $orderItemArray['shipping_by'] = $orderItem->Supplier->getDikirimOleh();
                                    $orderItemArray['shipping_from_store'] = "";
                                    $orderItemArray['shipping_from_geolocation'] = "";
                                }
                            } elseif ($orderItemArray['shipping_info'] == 'pickup_in_store') {
                                $orderItemArray['shipping_by'] = "Ruparupa";

                                $orderItemArray['shipping_from_store'] = "";
                                if (isset($pickup_order_address[$orderItemArray['pickup_code']]['pickup_name'])) {
                                    $orderItemArray['shipping_from_store'] = trim($pickup_order_address[$orderItemArray['pickup_code']]['pickup_name']);
                                }

                                $orderItemArray['shipping_from_geolocation'] = trim($geolocation);
                            }
                        }

                        $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
                        $salesOrderItemMdr = $salesOrderItemMdrModel->find("sales_order_item_id = " . $orderItemArray['sales_order_item_id']);
                        if ($salesOrderItemMdr != false) {
                            foreach ($salesOrderItemMdr->toArray() as $itemMdr) {
                                 $orderItemArray['mdr_customer'] = isset($itemMdr['value_customer']) ? $itemMdr['value_customer'] : 0;
                            }
                        }

                        $salesOrderArray['items'][] = $orderItemArray;

                        if ($params['page_type'] == "order_page" && $orderItemArray['is_free_item'] == 0) {
                            $salesInvoiceItem = $this->getInvoiceItemStatusFulfillment($orderItemArray['sales_order_item_id']);
                            if (!empty($salesInvoiceItem) && $salesInvoiceItem['status_fulfillment'] == "complete") {
                                $reviewInvoiceDatas[$orderItemArray['group_shipment']]['items'][] = $orderItemArray['sku'];
                            }
                        }
                    }
                }

                if ($salesOrder->SalesInvoice) {
                    foreach ($salesOrder->SalesInvoice as $invoice) {
                        $invoiceModel = new \Models\SalesInvoice();
                        $invoiceModel->setFromArray($invoice->toArray([], true));
                        $salesOrderArray['invoices'][] = $invoiceModel->getDataArray();

                        if ($params['page_type'] == "order_page") {
                            if ($invoice->SalesShipment) {
                                $shipmentArray = $invoice->SalesShipment->toArray();
                                $reviewInvoiceDatas[$shipmentArray['group_shipment']]['shipment_status'] = $shipmentArray['shipment_status'];
                            }

                            $invoiceArray = $invoice->toArray();
                            $reviewInvoiceDatas[$shipmentArray['group_shipment']]['invoice_no'] = $invoiceArray['invoice_no'];
                        }
                    }
                }

                if ($salesOrder->SalesRuleOrder) {
                    foreach ($salesOrder->SalesRuleOrder as $key => $salesRuleOrder) {
                        $salesOrderArray['voucher'][$key] = $salesRuleOrder->toArray(['rule_id', 'voucher_code', 'voucher_amount_used']);
                        if ($salesRuleOrder->SalesRuleVoucher)
                            $salesOrderArray['voucher'][$key]['type'] = $salesRuleOrder->SalesRuleVoucher->getType();
                    }
                }

                if ($params['page_type'] == "order_page") {
                    if (substr($salesOrderArray['order_no'], 0, 4) == "ODIR") { // New retail order can't review
                        $salesOrderArray['review_status'] = 0;
                    } else {
                        $salesOrderArray['review_status'] = $this->getReviewProductStatusShipment($reviewInvoiceDatas);
                    }
                }
            }

            $shipmentDetail = $this->formatOrderShipmentDetail($salesOrderArray);

            if ($params['page_type'] === "unify_apps") {
                $shipmentDetail['subtotal_unify_apps'] = 0;
                foreach ($shipmentDetail['shipment'] as $shipmentUnifyApps) {
                    $shipmentDetail['subtotal_unify_apps'] += floatval($shipmentUnifyApps['total']);
                }
                $shipmentDetail['subtotal_unify_apps'] += $shipmentDetail['shipping_discount_amount'];
            }

            if ($salesOrder->getOrderType() == "b2b_informa" || $salesOrder->getOrderType() == "b2b_ace") {
                if ($salesOrder->Customer) {
                    $customerB2bCompanyMdl = new \Models\CustomerB2bCompany();
                    $customerB2bCompany = $customerB2bCompanyMdl->findFirst("customer_id = " . $salesOrder->Customer->getCustomerId());
                    if ($customerB2bCompany) {
                        $customerCompanyDataMdl = new \Models\CustomerCompanyData();
                        $customerCompanyData = $customerCompanyDataMdl->findFirst("customer_company_data_id = " . $customerB2bCompany->getCustomerCompanyDataId());
                        if ($customerCompanyData) {
                            $shipmentDetail["company_name"] = $customerCompanyData->getCompanyName();
                        }
                    }

                }

                $invoiceSAP = $this->getInvoiceSap($salesOrder->getOrderNo());
                $shipmentDetail["invoice_sap"] = $invoiceSAP;
            }
        }



        return $shipmentDetail;
    }

    private function formatOrderShipmentDetail($salesOrderArray = array())
    {
        $formatedData = array();
        if (count($salesOrderArray) > 0) {
            $formatedData = $salesOrderArray;
            unset($formatedData['items']);
            unset($formatedData['invoices']);
        }
        $prefixOrderNo = substr($salesOrderArray['order_no'], 0, 4);

        $giftRecipient = "";
        if ($salesOrderArray['cart_type'] == "gift") {
            $cartModel = new \Models\Cart();
            $cartModel->setCartId($salesOrderArray['cart_id']);

            $headers = [
                'No-Refresh' => "1",
                'Cart-Type' => "gift"
            ];
            $cartData = $cartModel->getCartDetailV2($headers);
            if (!empty($cartModel->getErrorCode()) && !empty($cartModel->getErrorMessages()['message'])) {
                $this->errorCode = "RR302";
                $this->errorMessages = $cartModel->getErrorMessages()['message'];
                return array();
            }
            $giftRecipient = $cartData['gift_registry']['customer']['recipient'];
        }

        $formatedData['is_refundable'] = false;
        $formatedData['gift_recipient'] = $giftRecipient;
        

        if (isset($salesOrderArray['invoices'])) {
            $i = 0;
            foreach ($salesOrderArray['invoices'] as $row['invoice']) {
                $shipmentData[$i]['shipment_status'] = $row['invoice']['shipment']['shipment_status'];
                $shipmentData[$i]['invoice_no'] = $row['invoice']['invoice_no'];
                $shipmentData[$i]['receipt_id'] = $row['invoice']['receipt_id'];
                $shipmentData[$i]['customer_pin_code'] = $row['invoice']['customer_pin_code'];
                $shipmentData[$i]['status_fulfillment'] = $row['invoice']['status_fulfillment'];
                $shipmentData[$i]['invoice_created_at'] = $row['invoice']['created_at'];
                $shipmentData[$i]['track_number'] = $row['invoice']['shipment']['track_number'];
                $shipmentData[$i]['carrier'] = isset($row['invoice']['shipment']['carrier']) ? $row['invoice']['shipment']['carrier'] : array();
                $shipmentData[$i]['reorder'] = isset($row['invoice']['reorder']) ? $row['invoice']['reorder'] : array();
                $shipmentData[$i]['supplier_alias'] = isset($row['invoice']['supplier']) ? $row['invoice']['supplier']['supplier_alias'] : '';
                $shipmentData[$i]['eta'] = $row['invoice']['shipment']['eta'];
                $shipmentData[$i]['mdr_customer'] = 0;

                $salesShipmentLib = new \Library\SalesShipment();
                $params['shipment_id'] = $row['invoice']['shipment']['shipment_id'];
                $params['remark'] = 'tracking_awb';
                $resultTracking = $salesShipmentLib->getAllShipmentTracking($params);
                $shipmentData[$i]['shipment_tracking'] = array();
                if (count($resultTracking) > 0) {
                    if (!empty($resultTracking[0]['additional_info'])) {
                        $additionalInfo = json_decode($resultTracking[0]['additional_info']);
                        if (isset($additionalInfo->cnote_pod_receiver) && !empty($additionalInfo->cnote_pod_receiver)) {
                            $resultTracking[0]['receiver'] = $additionalInfo->cnote_pod_receiver;
                            $resultTracking[0]['receive_date'] = $additionalInfo->cnote_pod_date;
                        }
                    }

                    $shipmentData[$i]['shipment_tracking'] = $resultTracking[0];
                }

                $shippingFee = $handlingFee = $totalShipment = 0;
                $flagGroupShipment = false;
                foreach ($salesOrderArray['items'] as $rowItem) {
                    if (isset($rowItem["cart_rules"])) {
                        unset($rowItem["cart_rules"]);
                    }

                    if (isset($row['invoice']['shipment']['group_shipment'])) {
                        if ($rowItem['group_shipment'] == $row['invoice']['shipment']['group_shipment']) {
                            $flagGroupShipment = true;
                            $shipmentData[$i]['shipping_by'] = $rowItem['shipping_by'];
                            $shipmentData[$i]['shipping_from_store'] = $rowItem['shipping_from_store'];
                            $shipmentData[$i]['shipping_from_geolocation'] = $rowItem['shipping_from_geolocation'];

                            $salesInvoiceItemArray = $this->getInvoiceItem($rowItem['sales_order_item_id']);
                            $rowItem['status_fulfillment'] = $salesInvoiceItemArray['status_fulfillment'];
                            $shipmentData[$i]['details'][] = $rowItem;
                            $shippingFee += ($rowItem['shipping_amount'] - $rowItem['shipping_discount_amount']) * $rowItem['qty_ordered'];
                            $handlingFee += $rowItem['handling_fee_adjust'] * $rowItem['qty_ordered'];
                            $totalShipment += $rowItem['row_total'];
                            $shipmentData[$i]['mdr_customer'] +=  isset($rowItem['mdr_customer']) ? $rowItem['mdr_customer'] : 0;
                        }
                    }
                }

                if (!$flagGroupShipment) {
                    foreach ($row['invoice']['items'] as $rowItemInvoice) {
                        foreach ($salesOrderArray['items'] as $rowItemOrder) {
                            if ($rowItemInvoice['sku'] == $rowItemOrder['sku']) {
                                $shipmentData[$i]['shipping_by'] = (isset($rowItemOrder['shipping_by'])) ? $rowItemOrder['shipping_by'] : '';
                                $shipmentData[$i]['shipping_from_store'] = (isset($rowItemOrder['shipping_from_store'])) ? $rowItemOrder['shipping_from_store'] : '';
                                $shipmentData[$i]['shipping_from_geolocation'] = (isset($rowItemOrder['shipping_from_geolocation'])) ? $rowItemOrder['shipping_from_geolocation'] : '';

                                $rowItemOrder['status_fulfillment'] = $rowItemInvoice['status_fulfillment'];
                                $shipmentData[$i]['details'][] = $rowItemOrder;
                            }
                        }

                        $shippingFee += ($rowItemInvoice['shipping_amount'] - $rowItemInvoice['shipping_discount_amount']) * $rowItemInvoice['qty_ordered'];
                        $handlingFee += $rowItemInvoice['handling_fee_adjust'] * $rowItemInvoice['qty_ordered'];
                        $totalShipment += $rowItemInvoice['row_total'];
                    }
                }

                $shipmentData[$i]['biaya_kirim'] = $shippingFee;
                $shipmentData[$i]['biaya_layanan'] = $handlingFee;
                $shipmentData[$i]['total'] = $totalShipment + $shippingFee; // row total sudah sama handling fee                
                if (isset($formatedData['shipment']) && isset($shipmentData[$i]['reorder']['status']) && $shipmentData[$i]['reorder']['status'] == 'customer_confirmation') {
                    array_unshift($formatedData['shipment'], $shipmentData[$i]);
                } else {
                    $formatedData['shipment'][] = $shipmentData[$i];
                }

                if ($giftRecipient == "buyer" && $row['invoice']['invoice_status'] == "received" && $prefixOrderNo != "ODIS" && $prefixOrderNo != "ODIT" && $prefixOrderNo != "ODIK") {
                    $formatedData['is_refundable'] = true;
                }
                $i++;
            }
        } else {

            foreach ($salesOrderArray['items'] as $rowItem) {
                if (isset($rowItem["cart_rules"])) {
                    unset($rowItem["cart_rules"]);
                }

                if (empty($rowItem['group_shipment'])) {
                    $rowItem['group_shipment'] = 0;
                }

                $shipmentData[$rowItem['group_shipment']]['shipping_by'] = $rowItem['shipping_by'];
                $shipmentData[$rowItem['group_shipment']]['shipping_from_store'] = $rowItem['shipping_from_store'];
                $shipmentData[$rowItem['group_shipment']]['shipping_from_geolocation'] = $rowItem['shipping_from_geolocation'];
                if (isset($salesOrderArray['shipping_address'])) {
                    foreach ($salesOrderArray['shipping_address'] as $rowAddress) {
                        if (empty($rowAddress['group_shipment'])) {
                            $rowAddress['group_shipment'] = 0;
                        }

                        if ($rowAddress['group_shipment'] == $rowItem['group_shipment']) {
                            $shipmentData[$rowItem['group_shipment']]['shipping_address'] = $rowAddress;
                        }
                    }
                }

                $rowItem['status_fulfillment'] = null;
                $shipmentData[$rowItem['group_shipment']]['details'][] = $rowItem;

                if (!isset($shipmentData[$rowItem['group_shipment']]['biaya_kirim'])) {
                    $shipmentData[$rowItem['group_shipment']]['biaya_kirim'] = 0;
                }

                $shipmentData[$rowItem['group_shipment']]['biaya_kirim'] += ($rowItem['shipping_amount'] - $rowItem['shipping_discount_amount']) * $rowItem['qty_ordered'];

                if (!isset($shipmentData[$rowItem['group_shipment']]['biaya_layanan'])) {
                    $shipmentData[$rowItem['group_shipment']]['biaya_layanan'] = 0;
                }

                $shipmentData[$rowItem['group_shipment']]['biaya_layanan'] += $rowItem['handling_fee_adjust'] * $rowItem['qty_ordered'];

                if (!isset($shipmentData[$rowItem['group_shipment']]['total'])) {
                    $shipmentData[$rowItem['group_shipment']]['total'] = 0;
                }

                // row total sudah sama handling fee
                $shipmentData[$rowItem['group_shipment']]['total'] += ($rowItem['row_total'] + ($rowItem['shipping_amount'] - $rowItem['shipping_discount_amount']) * $rowItem['qty_ordered']);
            }

            foreach ($shipmentData as $rowShipment) {
                $formatedData['shipment'][] = $rowShipment;
            }
        }

        if (array_key_exists('review_status', $salesOrderArray)) {
            $formattedData['review_status'] = $salesOrderArray['review_status'];
        }

        if (array_key_exists('mdr_customer', $salesOrderArray)) {
            $formatedData['mdr_customer'] = (float) $salesOrderArray['mdr_customer'];
        }

        return $formatedData;
    }

    public function getOrderListDiscountAmount($params = array())
    {
        $salesOrderModel = new \Models\SalesOrder();

        $resultSalesOrder = $salesOrderModel::query();
        $resultSalesOrder->andWhere("Models\SalesOrder.discount_amount > 0");
        $resultSalesOrder->andWhere("Models\SalesOrder.status = 'processing' OR Models\SalesOrder.status = 'complete'");

        foreach ($params as $key => $val) {
            if ($key == "date_from") {
                $resultSalesOrder->andWhere("created_at >= '" . $val . "'");
            } else if ($key == "date_to") {
                $resultSalesOrder->andWhere("created_at  <= '" . $val . "'");
            } else if ($key != "order_by") {
                $resultSalesOrder->andWhere($key . " = '" . trim($val) . "'");
            }
        }

        if (!empty($params["order_by"])) {
            $resultSalesOrder->orderBy($params["order_by"]);
        } else {
            $resultSalesOrder->orderBy("created_at DESC");
        }

        $result = $resultSalesOrder->execute();

        if (empty($result->count())) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your orders";
            return array();
        } else {
            $allOrders = array();
            foreach ($result as $salesOrder) {
                $allOrders[] = $salesOrder;
            }
            $allOrders['recordsTotal'] = count($allOrders);
        }
        return $allOrders;
    }


    public function getAllOrderCustom($params = array())
    {
        $allOrders = array();

        if (isset($params['master_app_id'])) {
            if ($params['master_app_id'] == getenv('MASTER_APP_ID_OMS')) {
                unset($params['master_app_id']);

                $queryWhere = $queryLimit = $column = '';
                $queryHeader = "SELECT ";
                $queryFrom = " FROM sales_order so ";

                $columnCount = "COUNT(so.sales_order_id) AS total";
                $column = "so.sales_order_id, so.order_no, 
                    (
                        SELECT transaction_id FROM nicepay n 
                        WHERE n.order_id = so.order_no AND so.sales_order_id = (
                            SELECT sop.sales_order_id 
                            FROM sales_order_payment sop 
                            WHERE sop.type = 'credit_non_cc' AND sop.method = 'kredivo' AND sop.sales_order_id = so.sales_order_id 
                            LIMIT 1
                        )
                        order by n.nicepay_id desc LIMIT 1
                    ) AS kredivo_id,
                    so.device, so.created_at, so.reference_order_no,
                    (SELECT customer_email FROM sales_customer WHERE sales_customer.sales_order_id = so.sales_order_id LIMIT 1) AS email,
                    (SELECT first_name FROM sales_order_address WHERE sales_order_address.sales_order_id = so.sales_order_id AND address_type = 'shipping' LIMIT 1) AS shipping_firstname,
                    (SELECT last_name FROM sales_order_address WHERE sales_order_address.sales_order_id = so.sales_order_id AND address_type = 'shipping' LIMIT 1) AS shipping_lastname,
                    so.grand_total, so.status,
                    (SELECT type FROM sales_order_payment WHERE sales_order_payment.sales_order_id = so.sales_order_id LIMIT 1) AS payment_type,
                    (SELECT method FROM sales_order_payment WHERE sales_order_payment.sales_order_id = so.sales_order_id LIMIT 1) AS payment_method,
                    so.gift_cards, (SELECT customer_phone FROM sales_customer WHERE sales_customer.sales_order_id = so.sales_order_id LIMIT 1) AS phone_number ";

                if (isset($params['limit']) && isset($params['offset'])) {
                    $limit = $params['limit'];
                    $offset = $params['offset'];
                    unset($params['limit']);
                    unset($params['offset']);
                }

                $queryLimit = " LIMIT $limit OFFSET $offset";

                if (count($params) > 0) {
                    $flagOperator = 0;

                    if (isset($params['date_from']) && isset($params['date_to'])) {
                        $dateFrom = $params['date_from'] . " 00:00:00";
                        $dateTo   = $params['date_to'] . " 23:59:59";
                        $queryWhere .= "(so.created_at BETWEEN '$dateFrom' AND '$dateTo')";
                        $flagOperator++;
                    } elseif (isset($params['date_from'])) {
                        $queryWhere .= "so.created_at >= '" . $params['date_from'] . " 00:00:00'";
                        $flagOperator++;
                    } else if (isset($params['date_to'])) {
                        $queryWhere .= "so.created_at <= '" . $params['date_to'] . " 23:59:59'";
                        $flagOperator++;
                    }

                    if (isset($params['role_name'])) {
                        if ($params['role_name'] == 'super_admin_b2b') {
                            if ($flagOperator) $queryWhere .= " AND ";
                            $soColumn = "(
                                SELECT
                                    CONCAT
                                    (
                                        (
                                            IF(sbi.so_sap_status != '', sbi.so_sap_status, '-')
                                        ), 
                                            ',', 
                                        (
                                            IF(sbi.so_sap_no != '', sbi.so_sap_no ,'-')
                                        )
                                    )
                                FROM sales_b2b_information sbi WHERE sbi.sales_order_id = so.sales_order_id
                            ) AS so_sap, 
                            " . $column;
                            $column = $soColumn;
                            $queryWhere .= "(so.order_type = 'b2b_ace' OR so.order_type = 'b2b_informa')";
                            $flagOperator++;
                        }

                        if ($params['role_name'] == 'admin_b2b_informa') {
                            if ($flagOperator) $queryWhere .= " AND ";
                            $soColumn = "(
                                SELECT
                                    CONCAT
                                    (
                                        (
                                            IF(sbi.so_sap_status != '', sbi.so_sap_status, '-')
                                        ), 
                                            ',', 
                                        (
                                            IF(sbi.so_sap_no != '', sbi.so_sap_no ,'-')
                                        )
                                    )
                                FROM sales_b2b_information sbi WHERE sbi.sales_order_id = so.sales_order_id
                            ) AS so_sap, 
                            " . $column;
                            $column = $soColumn;
                            $queryWhere .= "(so.order_type = 'b2b_informa')";
                            $flagOperator++;
                        }

                        if ($params['role_name'] == 'admin_b2b_ace') {
                            if ($flagOperator) $queryWhere .= " AND ";
                            $soColumn = "(
                                SELECT
                                    CONCAT
                                    (
                                        (
                                            IF(sbi.so_sap_status != '', sbi.so_sap_status, '-')
                                        ), 
                                            ',', 
                                        (
                                            IF(sbi.so_sap_no != '', sbi.so_sap_no ,'-')
                                        )
                                    )
                                FROM sales_b2b_information sbi WHERE sbi.sales_order_id = so.sales_order_id
                            ) AS so_sap, 
                            " . $column;
                            $column = $soColumn;
                            $queryWhere .= "(so.order_type = 'b2b_ace')";
                            $flagOperator++;
                        }

                        if ($params['role_name'] == 'admin_rolka') {
                            if ($flagOperator) $queryWhere .= " AND ";

                            $departmentRolka = getenv("ROLKA_DEPARTMENT_NAME");

                            $queryFrom .= " JOIN
                                            sales_order_item soi ON so.sales_order_id = soi.sales_order_id
                                            JOIN
                                            sales_order_item_flat soif ON soif.sales_order_item_id = soi.sales_order_item_id
                                            ";

                            $column = "distinct(soi.sales_order_id) as soid, " . $column;

                            $columnCount = "COUNT(distinct(soi.sales_order_id)) AS total";

                            $queryWhere .= " soif.department_name IN ('{$departmentRolka}') AND";
                            $queryWhere .= " (so.order_type != 'b2b_ace' AND so.order_type != 'b2b_informa')";
                            $flagOperator++;

                            if (!empty($params['admin_user_id'])) {
                                $adminUserAttributeValueModel = new \Models\AdminUserAttributeValue();
                                $resultAttribute = $adminUserAttributeValueModel::findFirst("admin_user_id = " . $params['admin_user_id'] . " AND master_app_id = " . getenv('MASTER_APP_ID_OMS') . " AND admin_user_attribute_id = " . getenv('ADMIN_USER_PICKUP_POINT_ATTRIBUTE_ID'));
                                $adminUserAttributeValue = $resultAttribute->toArray();
                                if (strpos($adminUserAttributeValue['value'], 'all') === false){
                                    $pickupCodeList = explode(",", $adminUserAttributeValue['value']);
                                    $queryInPickupCode = implode("','", $pickupCodeList);

                                    if ($flagOperator) $queryWhere .= " AND ";                                
                                    $queryWhere .= " soi.pickup_code IN ('" . $queryInPickupCode . "')";
                                    $flagOperator++;
                                }
                            }
                        }
                    }

                    if (isset($params['status'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        if ($params['status'] == "1000DChold") {
                            $queryWhere .= "so.status = 'hold' AND ";
                            $queryWhere .= "so.sales_order_id IN(SELECT soi.sales_order_id FROM sales_order_item soi WHERE substr(soi.store_code,1,6) = '1000DC')";
                        } else {
                            $queryWhere .= "so.status = '" . $params['status'] . "'";
                        }
                        $flagOperator++;
                    }

                    if (isset($params['order_keyword'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $queryWhere .= "so.order_no = '" . strtoupper($params['order_keyword']) . "'";
                        $flagOperator++;
                    }

                    if (isset($params['order_type'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $queryWhere .= "so.order_type = '" . $params['order_type'] . "'";
                        $flagOperator++;
                    }

                    if (isset($params['grand_total_keyword'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $params['grand_total_keyword'] = strtoupper($params['grand_total_keyword']);
                        $queryWhere .= "so.grand_total = '" . $params['grand_total_keyword'] . "'";
                        $flagOperator++;
                    }

                    if (isset($params['giftcard_keyword'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $queryWhere .= "so.sales_order_id in (select sro.sales_order_id from salesrule_order sro where sro.voucher_code='" . strtoupper($params['giftcard_keyword']) . "')";
                        $flagOperator++;
                    }

                    if (isset($params['email_keyword'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $queryWhere .= "so.sales_order_id IN(SELECT sc.sales_order_id FROM sales_customer sc WHERE sc.customer_email = '" . $params['email_keyword'] . "')";
                        $flagOperator++;
                    }

                    if (isset($params['shipping_name_keyword'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $queryWhere .= "so.sales_order_id IN(SELECT soa.sales_order_id FROM sales_order_address soa 
                        WHERE concat(soa.first_name,' ',soa.last_name) LIKE '%" . $params['shipping_name_keyword'] . "%')";
                        $flagOperator++;
                    }

                    if (isset($params['phone_number_keyword'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $queryWhere .= "so.sales_order_id IN(SELECT sc.sales_order_id FROM sales_customer sc WHERE sc.customer_phone = '" . $params['phone_number_keyword'] . "')";
                        $flagOperator++;
                    }

                    if (isset($params['kredivo_id_keyword'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $queryWhere .= "so.order_no = (
                            SELECT n.order_id FROM nicepay n
                            LEFT JOIN sales_order so ON so.order_no = n.order_id 
                            LEFT JOIN sales_order_payment sop ON so.sales_order_id = sop.sales_order_id 
                            WHERE transaction_id = '" . $params['kredivo_id_keyword'] . "' AND sop.method = 'kredivo' AND sop.type = 'credit_non_cc'
                            LIMIT 1
                        )";
                        $flagOperator++;
                    }

                    if (!empty($params['created_at_keyword'])) {
                        if ($flagOperator) $queryWhere .= " AND ";

                        $dateFrom = $params['created_at_keyword'] . " 00:00:00";
                        $dateTo   = $params['created_at_keyword'] . " 23:59:59";
                        $queryWhere .= "(so.created_at BETWEEN '$dateFrom' AND '$dateTo')";
                    }

                    if ($flagOperator) {
                        $queryWhere = "WHERE " . $queryWhere;
                    }
                }

                try {
                    $orderModel = new \Models\SalesOrder();
                    $orderModel->useReadOnlyConnection();

                    $orderBy = ' ORDER BY sales_order_id DESC';
                    $queryStructure = $queryHeader . $column . $queryFrom . $queryWhere . $orderBy . $queryLimit;
                    $result = $orderModel->getDi()->getShared($orderModel->getConnection())->query($queryStructure);
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );
                    $resultArray = $result->fetchAll();
                    $allOrders['list'] = $resultArray;

                    // for datatables paging
                    $queryStructure = $queryHeader . $columnCount . $queryFrom . $queryWhere;
                    $result = $orderModel->getDi()->getShared($orderModel->getConnection())->query($queryStructure);
                    $result->setFetchMode(
                        Database::FETCH_ASSOC
                    );
                    $countArray = $result->fetch();
                    $allOrders['recordsTotal'] = $countArray['total'];
                } catch (\Exception $e) {
                    $allOrders['list'] = array();
                }
            }
        }

        return $allOrders;
    }

    public function getSalesOrderDetail($params = [])
    {
        if (empty($params['order_no'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Order no is required";
            return array();
        }

        /**
         * don't get sales order from redis for now
         */
        //$salesOrderData = \Library\Redis\SalesOrder::getSalesOrder($order_no);

        $salesOrderData = array();
        $salesOrderLib = new \Library\SalesOrder();
        $allOrders = $salesOrderLib->getAllOrder($params);
        if (!empty($allOrders)) {
            $salesOrderData = $allOrders[0];

            /**
             * Assign current data just for updating sales order only
             */
            $salesOrderModel = new \Models\SalesOrder();
            $salesOrderModel->assign($salesOrderData);
            $this->salesOrder = $salesOrderModel;
        } else {
            $this->errorCode = "202";
            $this->errorMessages = "Sorry, we can not found your orders";
        }

        return $salesOrderData;
    }

    public function getSalesOrderShipmentDetail($params = [])
    {
        if (empty($params['order_no'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Order no is required";
            return array();
        }

        $salesOrderData = array();
        $salesOrderLib = new \Library\SalesOrder();
        $allOrders = $salesOrderLib->getAllOrderShipment($params);
        if (!empty($allOrders)) {
            $salesOrderData = $allOrders;
        } else {
            $this->errorCode = "202";
            $this->errorMessages = "Sorry, we can not found your orders";
        }

        return $salesOrderData;
    }

    /**
     * Create sales order cache by order no
     * @param $params
     */
    public function createSaleOrderCache($params = "")
    {
        if (empty($params['order_no'])) {
            $this->errorCode = 'RR100';
            $this->errorMessages[] = 'Order no is empty';
            return;
        }

        $allOrder = $this->getAllOrder(["order_no" => $params['order_no']]);
        $orderDetail = $allOrder[0];

        $redisOrder = new \Library\Redis\SalesOrder();
        $status = $redisOrder->setSalesOrder($params['order_no'], $orderDetail);

        if (!$status) {
            $this->errorCode = "RR301";
            $this->errorMessages = "Failed to save sales order to redis";
        }
    }

    public function getOrderListBaseOnExternalOrderNo($params = array())
    {
        $salesOrderModel = new \Models\SalesOrder();
        $source_order_no = $params['source_order_no'];
        $query = "
                    SELECT
                        order_no, source_order_no
                    FROM
                        sales_order
                    WHERE
                        source_order_no IN ($source_order_no);
                    ";
        try {
            $param = array(
                "columns" => "order_no, source_order_no",
                "conditions" => "source_order_no IN ($source_order_no)"
            );
            $dataObj = $salesOrderModel->find($param);
            if (count($dataObj) > 0) {
                $allOrders['list'] = $dataObj->toArray();
            } else {
                $allOrders['list'] = array();
            }
            //$allOrders['list'] = $manager->executeQuery($query)->toArray();
        } catch (\Exception $e) {
            $allOrders['list'] = array();
        }

        return $allOrders;
    }

    public function getSalesOrderSummaryOnly($params)
    {
        $salesOrderSummary['list'] = array();
        $salesOrderSummary['count'] = 0;

        $salesOrderModel = new \Models\SalesOrder();
        $orders = $salesOrderModel->getOrderSummary($params);

        if (count($orders) <= 0) {
            $this->errorCode = "404";
            $this->errorMessages = "Order not found";
            return $salesOrderSummary;
        }

        $salesOrderSummary['list'] = $orders;
        $salesOrderSummary['count'] = count($orders);

        return $salesOrderSummary;
    }

    public function getSalesOrderSummary($params = array())
    {
        $salesOrderModel = new \Models\SalesOrder();
        $customer2GroupModel = new \Models\Customer2Group;

        $page_type = "";

        $resultSalesOrder = $salesOrderModel::query();
        $resultSalesOrder->join("Models\SalesCustomer", "Models\SalesCustomer.sales_order_id = Models\SalesOrder.sales_order_id");

        if (isset($params['page_type'])) {
            $page_type = $params['page_type'];
            unset($params['page_type']);
        }

        if (isset($params['sales_order_id'])) {
            $resultSalesOrder->andWhere("Models\SalesOrder.sales_order_id = " . $params['sales_order_id']);

            unset($params['sales_order_id']);
        }

        if ((isset($params["order_keywords"]) && !empty($params["order_keywords"]))) {
            $resultSalesOrder->join("Models\SalesOrderItem", "Models\SalesOrderItem.sales_order_id = Models\SalesOrder.sales_order_id");
        }

        if (isset($params["order_keywords"]) && !empty($params["order_keywords"])) {
            $keywords = substr($params["order_keywords"], 0, 3);

            if ($keywords == "ODI") {
                $params["order_no"] = $params["order_keywords"];

                unset($params["order_keywords"]);
            }
        } else if (isset($params["order_keywords"]) && empty($params["order_keywords"])) {
            unset($params["order_keywords"]);
        }

        if (isset($params['exclude_order']) && !empty($params['exclude_order'])) {
            $excludeOrderArr = explode(",", $params['exclude_order']);
            unset($params['exclude_order']);
            if (count($excludeOrderArr) > 0) {
                foreach ($excludeOrderArr as $val) {
                    $resultSalesOrder->andWhere("SUBSTRING(Models\SalesOrder.order_no, 1, 4) != '$val'");
                }
            }
        }

        foreach ($params as $key => $val) {
            if ($key == "date_from") {
                $resultSalesOrder->andWhere("DATE(created_at) >= " . "DATE('$val')");
            } else if ($key == "date_to") {
                $resultSalesOrder->andWhere("DATE(created_at) <= " . "DATE('$val')");
            } else if ($key == "gift_cards") {
                $resultSalesOrder->andWhere("gift_cards like '%\"voucher_code\":\"" . $val . "\"%'");
            } else if ($key == "order_no") {
                $resultSalesOrder->andWhere("order_no like '%" . $val . "%'");
            } else if ($key == "order_keywords") {
                $resultSalesOrder->andWhere("Models\SalesOrderItem.name LIKE '%" . $val . "%'");
            } else if ($key == 'customer_id') {
                $key = 'Models\SalesCustomer.customer_id';
                $resultSalesOrder->andWhere($key . " = '" . $val . "'");
            }
        }

        $resultSalesOrder->andWhere("Models\SalesCustomer.customer_is_guest = 0");

        if (!empty($params["order_by"])) {
            $resultSalesOrder->orderBy($params["order_by"]);
        } else {
            $resultSalesOrder->orderBy("Models\SalesOrder.created_at DESC");
        }

        if ($page_type == "unify_apps") {
            $resultSalesOrder->join("Models\SalesOrderPayment", "Models\SalesOrderPayment.sales_order_id = Models\SalesOrder.sales_order_id");
            $resultSalesOrder->andWhere("Models\SalesOrder.status in ('new')");
            $resultSalesOrder->andWhere("Models\SalesOrderPayment.status in ('pending')");
            $resultSalesOrder->andWhere("Models\SalesOrderPayment.method not in ('vpayment','vpaymentins')");
        } else if ($page_type == "b2b_informa" || $page_type == "b2b_ace") {
            $resultSalesOrder->join("Models\SalesOrderPayment", "Models\SalesOrderPayment.sales_order_id = Models\SalesOrder.sales_order_id");
        }

        $resultTotal = $resultSalesOrder->distinct(true)->execute();
        $resultProcessedSalesOrder = clone $resultSalesOrder;

        $resultProcessedSalesOrder->andWhere("Models\SalesOrder.status in ('processing','hold')");

        $resultProcessedTotal = $resultProcessedSalesOrder->execute();

        if ((isset($params["limit"]) && is_numeric($params['limit']) && $params['limit'] >= 0) &&
            (isset($params["offset"]) && is_numeric($params['offset']) && $params['offset'] >= 0)
        ) {
            $resultSalesOrder->limit($params["limit"], $params["offset"]);
        }

        $result = $resultSalesOrder->distinct(true)->execute();
        $salesOrderSummary['list'] = array();
        $salesOrderSummary['count'] = 0;
        if (empty($result->count())) {
            $this->errorCode = "404";
            $this->errorMessages = "Order not found";
            return $salesOrderSummary;
        } else {
            $allOrders = array();
            foreach ($result as $salesOrder) {
                $salesOrderArray = $salesOrder->toArray(['status', 'order_no', 'grand_total', 'created_at'], true);
                if ($page_type == "order_page") {
                    $reviewInvoiceDatas = array();
                }

                if ($salesOrder->SalesCustomer) {
                    $customerData = $salesOrder->SalesCustomer->toArray(["customer_firstname", "customer_lastname", "customer_email"], true);
                    $salesOrderArray = $salesOrderArray + $customerData;
                }

                if ($salesOrder->SalesOrderPayment) {
                    $paymentArray = $salesOrder->SalesOrderPayment->toArray();

                    if ($paymentArray['status'] == 'pending' || $paymentArray['status'] == 'pending_payment') {
                        $salesOrderArray['status'] = "Pending Payment";
                    } else if ($paymentArray['status'] == 'expire') {
                        $salesOrderArray['status'] = "expire";
                    }

                    if ($page_type == 'unify_apps') {
                        if ($paymentArray['method'] == "qris") {
                            $arrayPayment = json_decode($paymentArray['additional_data'], true);
                            if (isset($arrayPayment['actions'])) {
                                foreach ($arrayPayment['actions'] as $row) {
                                    if ($row['name'] == "generate-qr-code") {
                                        $paymentArray['generate-qr-code'] = $row['url'];
                                    }
                                }
                            }
                        }
                        $salesOrderArray['payment'] = $paymentArray;
                        $salesOrderArray['cart_id'] = $salesOrder->getCartId();
                        $salesOrderArray['has_change_payment'] = (int)$salesOrder->getHasChangedPayment();
                    } else if ($page_type == "b2b_informa" || $page_type == "b2b_ace") {
                        $paymentMethod = "";
                        switch ($paymentArray['method']) {
                            case 'term_of_payment':
                                $paymentMethod = "Term of Payment";
                                break;
                            case 'manual_transfer':
                                $paymentMethod = "Manual Transfer";
                                break;
                            case 'vpaymentva':
                            case 'niceva':
                                $paymentMethod = "Virtual Account";
                                break;
                            case 'vpayment':
                                $paymentMethod = "Credit / Debit Card";
                                break;
                            default:
                                $paymentMethod = $paymentArray['method'];
                                break;
                        }

                        $salesOrderArray['payment_method'] = $paymentMethod;
                    }
                }

                $salesOrderArray['shipment'] = array();
                if ($salesOrder->SalesInvoice) {
                    $invoiceArray = $salesOrder->SalesInvoice->toArray();
                    if (count($invoiceArray) > 0) {
                        $allInvoiceStatusList = $shipmentList = array();
                        $salesShipmentLib = new \Library\SalesShipment();

                        $reorderCount = 0;
                        $needCustConf = false;
                        foreach ($invoiceArray as $rowInvoice) {
                            $allInvoiceStatusList[] = $rowInvoice['invoice_status'];

                            $allShipment = $salesShipmentLib->getAllShipment(["invoice_id" => $rowInvoice['invoice_id']]);
                            $shipmentData = $allShipment[0];
                            if ($page_type == "order_page") {
                                $reviewInvoiceDatas[$shipmentData['group_shipment']]['invoice_no'] = $rowInvoice['invoice_no'];
                                $reviewInvoiceDatas[$shipmentData['group_shipment']]['shipment_status'] = $shipmentData['shipment_status'];
                            }

                            $shipmentList[] = array(
                                'invoice_no' => $rowInvoice['invoice_no'],
                                'delivery_method' => $rowInvoice['delivery_method'],
                                'shipment_status' => $shipmentData['shipment_status'],
                                'updated_at' => $shipmentData['updated_at']
                            );

                            if ($shipmentData['shipment_status'] == 'ready_to_pickup' && $rowInvoice['delivery_method'] == 'pickup' && $rowInvoice['invoice_status'] != 'full_refund') {
                                $salesOrderArray['status'] = 'ready_to_pickup_by_customer';
                            }

                            $salesReorderModel = new \Models\SalesReorder();
                            $salesReorderResult = $salesReorderModel->findFirst('invoice_id = ' . $rowInvoice['invoice_id']);

                            if ($salesReorderResult) {
                                $salesReorder = $salesReorderResult->toArray();
                                if ($salesReorder['status'] == 'completed' && isset($salesReorder['action']) && $salesReorder['action'] == 'reorder') {
                                    $reorderCount++;
                                } else if ($salesReorder['status'] == 'customer_confirmation') {
                                    $needCustConf = true;
                                }
                            }

                            if ( ($page_type == "b2b_informa" || $page_type == "b2b_ace") && $salesOrderArray['status'] == 'complete' && $salesOrderArray['payment_method'] == 'Term of Payment') {
                                $sap = new \Models\SalesInvoiceSap();
                                $sapResult = $sap->find('invoice_id = ' . $rowInvoice['invoice_id']);

                                if ($sapResult) {
                                    $invoiceSap = $sapResult->toArray();

                                    if (count($invoiceSap) > 0) {
                                        $countComplete = 0;

                                        foreach ($invoiceSap as $inv) {
                                            if ($inv['status'] == 'paid') {
                                                $countComplete++;
                                            }
                                        }

                                        if ($countComplete != count($invoiceSap)) {
                                            $salesOrderArray['status'] = 'Waiting for Payment';
                                        }
                                    } else {
                                        $salesOrderArray['status'] = 'Waiting for Payment';
                                    }
                                }
                            }
                        }

                        $uniqueInvoiceStatusList = array_unique($allInvoiceStatusList);
                        if ($needCustConf) {
                            $salesOrderArray['status'] = "perlu konfirmasi";
                        } else if ($reorderCount > 0) {
                            if (count($uniqueInvoiceStatusList) > 1) {
                                $salesOrderArray['status'] = "reorder sebagian";
                            } else if (in_array("partial_refund", $uniqueInvoiceStatusList)) {
                                $salesOrderArray['status'] = "reorder sebagian";
                            } else {
                                $salesOrderArray['status'] = "reorder";
                            }
                        } else if ($salesOrderArray['status'] != 'ready_to_pickup_by_customer') {
                            if (in_array("partial_refund", $uniqueInvoiceStatusList)) {
                                $salesOrderArray['status'] = "refund sebagian";
                            } else if (count($uniqueInvoiceStatusList) > 1 && in_array("full_refund", $uniqueInvoiceStatusList)) {
                                $salesOrderArray['status'] = "refund sebagian";
                            } else if (count($uniqueInvoiceStatusList) == 1 && in_array("full_refund", $uniqueInvoiceStatusList)) {
                                $salesOrderArray['status'] = "refund";
                            }
                        }

                        $salesOrderArray['shipment'] = $shipmentList;

                        // if ($page_type == "b2b_informa") {
                        //     $invoiceSap = new \Models\SalesInvoiceSap();
                        // }
                    }
                }

                $salesOrderArray['order_items'] = array();

                if ($page_type == "order_page") {
                    $orderItemArray = $this->getOrderItemWithStatusFulfillment($salesOrder->sales_order_id);
                    $product = new \Models\Product();
                    $paramSource = ["variants"];

                    // Only need get first ordered item because we need only to show one product per order on order list
                    $getProductElastics = $product->getDetailFromElastic($orderItemArray[0]['sku'], $paramSource);
                    $imageURL = (isset($getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'])) ? $getProductElastics[0]['_source']['variants'][0]['images'][0]['image_url'] : '';
                    $firstOrderItemParam = array(
                        'sku'       => $orderItemArray[0]['sku'],
                        'name'      => $orderItemArray[0]['name'],
                        'qty'       => $orderItemArray[0]['qty_ordered'],
                        'image_url' => $imageURL
                    );
                    $salesOrderArray['order_items'] = $firstOrderItemParam;
                    $salesOrderArray['total'] = sizeof($salesOrder->SalesOrderItem);

                    for ($i = 0; $i < sizeof($orderItemArray); $i++) {
                        if ($orderItemArray[$i]['is_free_item'] == 0 && $orderItemArray[$i]['status_fulfillment'] == "complete") {
                            $reviewInvoiceDatas[$orderItemArray[$i]['group_shipment']]['items'][] = $orderItemArray[$i]['sku'];
                        }
                    }
                }

                if ($salesOrderArray['status'] == 'received' || $salesOrderArray['status'] == 'complete') {
                    $refundOnProgressStatus = array('new', 'verified', 'approved', 'ship_from_customer', 'store_approved');
                    $refundStatusArray = $this->runReadSql("SELECT status FROM sales_return_refund WHERE order_no = '{$salesOrderArray['order_no']}'");
                    foreach ($refundStatusArray as $row) {
                        if (in_array($row['status'], $refundOnProgressStatus)) {
                            $salesOrderArray['status'] = 'on_process_refund';
                            break;
                        }
                    }

                    $refundOnProgressStatus = array('new', 'approved', 'ship_from_customer', 'dc_received', 'waiting_finance_billing', 'generated');
                    $refundStatusArray = $this->runReadSql("SELECT status FROM sales_retref_dc WHERE order_no = '{$salesOrderArray['order_no']}'");
                    foreach ($refundStatusArray as $row) {
                        if (in_array($row['status'], $refundOnProgressStatus)) {
                            $salesOrderArray['status'] = 'on_process_refund';
                            break;
                        }
                    }

                    $tuguOnProgressStatus = array('new', 'verified', 'approved', 'ship_from_customer', 'store_approved', 'stand_by', 'ready_to_pickup', 'picked', 'ship_from_store');
                    $tuguStatusArray = $this->runReadSql("SELECT status FROM sales_return_tugu WHERE order_no = '{$salesOrderArray['order_no']}'");
                    foreach ($tuguStatusArray as $row) {
                        if (in_array($row['status'], $tuguOnProgressStatus)) {
                            $salesOrderArray['status'] = 'on_process_tugu';
                            break;
                        }
                    }
                }

                if ($page_type == "order_page") {
                    if (substr($salesOrderArray['order_no'], 0, 4) == "ODIR") { // New retail order can't review
                        $salesOrderArray['review_status'] = 0;
                    } else {
                        $salesOrderArray['review_status'] = $this->getReviewProductStatusShipment($reviewInvoiceDatas);
                    }
                }

                $salesOrderArray['can_change_payment'] = !is_null($salesOrder->getHasChangedPayment()) && $salesOrder->getHasChangedPayment() >= 0 && $salesOrder->getHasChangedPayment() < 2 ? true : false;
                $salesOrderArray['can_cancel'] = false;

                if ($salesOrder->SalesOrderPayment->getCanCancel() == 10) {
                    $salesOrderArray['can_cancel'] = true;
                }


                $allOrders[] = $salesOrderArray;
            }

            $salesOrderSummary['list'] = $allOrders;
            $salesOrderSummary['count'] = $resultTotal->count();
            $salesOrderSummary['processedCount'] = $resultProcessedTotal->count();
        }

        return $salesOrderSummary;
    }

    public function getReviewProductStatusShipment($invoiceDatas)
    {
        $productCount = 0;
        $reviewCount = 0;
        $anyReceived = false;

        foreach ($invoiceDatas as $invoice) {
            if ($invoice['shipment_status'] == "received") {
                $anyReceived = true;
                $reviewData = $this->getReviewPerInvoice($invoice['invoice_no']);

                $productCount += sizeof($invoice['items']);
                $reviewCount += sizeof($reviewData);

                foreach ($reviewData as $review) {
                    if ($review['rating'] <= 3 && $review['rating_initial'] <= 3) {
                        return 2; // Reviewed, Editable
                    }
                }
            }
        }

        if ($anyReceived && ($reviewCount == 0 || $productCount > $reviewCount)) {
            return 1; // Unreviewed
        }

        return 0; // Nothing to review
    }

    public function getReviewPerInvoice($invoice_no)
    {
        $reviewProductModel = new \Models\ReviewProduct();
        $sql = "SELECT
                    rating, rating_initial 
                FROM 
                    review_product
                WHERE 
                    invoice_no = '" . $invoice_no . "'";
        $reviewProductModel->useReadOnlyConnection();
        $result = $reviewProductModel->getDi()->getShared('dbReader')->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $data = $result->fetchAll();

        if (!empty($reviewProductModel->getErrorCode())) {
            LogHelper::log("review_product", "There was an error while getting review product for invoice_no " . $invoice_no);
            $this->errorCode = $reviewProductModel->getErrorCode();
            $this->errorMessages = $reviewProductModel->getErrorMessages();
        }

        return $data;
    }

    public function lazadaUpdateOrderStatus($params_array = array())
    {
        $getPrefix = substr($params_array['order_no'], 0, 3);
        $getPrefix = "LZD";
        if (!empty($params_array['order_no']) and strtoupper($getPrefix) == "LZD") {
            $orderDetail = $this->getSalesOrderDetail(['order_no' => $params_array['order_no']]);

            if (!empty($orderDetail)) {
                if (!empty($params_array['status'])) {
                    $SalesOrder = new \Models\SalesOrder();
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $data['status'] = $params_array['status'];
                    $conditions = ' order_no = "' . $params_array['order_no'] . '"';

                    $SalesOrder->useWriteConnection();
                    $success = $SalesOrder->getDi()->getShared($SalesOrder->getConnection())->updateAsDict($SalesOrder->getSource(), $data, $conditions);
                    $result = ($success) ? $SalesOrder->getDi()->getShared($SalesOrder->getConnection())->affectedRows() : FALSE;
                }

                if (!empty($params_array['invoice_status']) and !empty($orderDetail['invoices']['0']['invoice_id'])) {
                    unset($data['status']);
                    $invoice = new \Models\SalesInvoice();
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $data['invoice_status'] = $params_array['invoice_status'];
                    $conditions = ' invoice_id = "' . $orderDetail['invoices']['0']['invoice_id'] . '"';

                    $invoice->useWriteConnection();
                    $success = $invoice->getDi()->getShared($invoice->getConnection())->updateAsDict($invoice->getSource(), $data, $conditions);
                    $result = ($success) ? $invoice->getDi()->getShared($invoice->getConnection())->affectedRows() : FALSE;
                }

                if (!empty($params_array['shipment_status']) and !empty($orderDetail['invoices']['0']['shipment']['shipment_id'])) {
                    unset($data['invoice_status']);
                    $shipment = new \Models\SalesShipment();
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $data['shipment_status'] = $params_array['shipment_status'];
                    $conditions = ' shipment_id = "' . $orderDetail['invoices']['0']['shipment']['shipment_id'] . '"';

                    $shipment->useWriteConnection();
                    $success = $shipment->getDi()->getShared($shipment->getConnection())->updateAsDict($shipment->getSource(), $data, $conditions);
                    $result = ($success) ? $shipment->getDi()->getShared($shipment->getConnection())->affectedRows() : FALSE;
                }
            } else {
                $this->errorCode = "RR404";
                $this->errorMessages = "Lazada Order Not Found";
            }
        }
    }

    public function updateOrderAddress($params = array())
    {
        if (empty($params['sales_order_address_id'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Order Address not found, can no update address";
            return;
        }

        $salesOrderAddressModel = new \Models\SalesOrderAddress();

        // Find if customer address is exist
        if (isset($params['sales_order_address_id'])) {
            $addressResult = $salesOrderAddressModel::findFirst("sales_order_address_id = '" . $params['sales_order_address_id'] . "'");

            if (!$addressResult) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Address not found";
                return false;
            }
        }

        if (empty($params['kelurahan_id'])) {
            $params['kelurahan_id'] = null;
        }
        $salesOrderAddressModel->setFromArray($params);
        $saveStatus = $salesOrderAddressModel->saveData("sales_order_address");

        if (!$saveStatus) {
            $this->errorCode = $salesOrderAddressModel->getErrorCode();
            $this->errorMessages[] = $salesOrderAddressModel->getErrorMessages();
            return false;
        }

        return true;
    }

    public function getOrderListPayment($params = array())
    {
        $salesOrderPaymentModel = new \Models\SalesOrderPayment();

        $resultSalesOrderPayment = $salesOrderPaymentModel::query();

        if (isset($params['from_hour']) && isset($params['to_hour'])) {
            $fromDate = date('Y-m-d H:i:s', strtotime('-' . $params['from_hour'] . ' hour'));
            $toDate = date('Y-m-d H:i:s', strtotime('-' . $params['to_hour'] . ' hour'));
            $resultSalesOrderPayment->betweenWhere("Models\SalesOrderPayment.created_at", $fromDate, $toDate);
            unset($params['from_hour_keyword'], $params['to_hour']);
        } else if (isset($params['to_hour'])) {
            $toDate = date('Y-m-d H:i:s', strtotime('-' . $params['to_hour'] . ' hour'));
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.created_at < '" . $toDate . "'");
            unset($params['to_hour']);
        } else if (isset($params['get_expired']) && $params['get_expired'] == 1) {
            $now = date('Y-m-d H:i:s');
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.expire_transaction <= '" . $now . "'");
        } else if (isset($params['reminder']) && $params['reminder'] == 1) {
            $expiredDate = date('Y-m-d H:i:s', strtotime('+2 hour'));
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.expire_transaction <= '" . $expiredDate . "'");
        } else if (isset($params['reminder']) && $params['reminder'] == 2) {
            $startDate = date('Y-m-d H:i:s', strtotime('+90 minute'));
            $endDate = date('Y-m-d H:i:s', strtotime('+150 minute'));
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.expire_transaction >= '" . $startDate . "'");
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.expire_transaction <= '" . $endDate . "'");
        } else if (isset($params['reminder']) && $params['reminder'] == 3) {
            $startDate = date('Y-m-d H:i:s', strtotime('+210 minute'));
            $endDate = date('Y-m-d H:i:s', strtotime('+220 minute'));
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.created_at >= '" . $startDate . "'");
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.created_at <= '" . $endDate . "'");
        }

        if (isset($params['status'])) {
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.status = '" . $params['status'] . "'");

            unset($params['status']);
        }

        if (isset($params['type'])) {
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.type = '" . $params['type'] . "'");

            unset($params['type']);
        }

        if (isset($params['method'])) {
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.method = '" . $params['method'] . "'");

            unset($params['method']);
        }

        if (isset($params['company_code'])) {
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.sales_order_id IN (select \Models\SalesOrder.sales_order_id from \Models\SalesOrder where \Models\SalesOrder.company_code = '{$params['company_code']}')");

            unset($params['company_code']);
        } else {
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.sales_order_id IN (select \Models\SalesOrder.sales_order_id from \Models\SalesOrder where \Models\SalesOrder.company_code = 'ODI')");
        }

        if (isset($params['limit_payment_time'])) {
            $resultSalesOrderPayment->andWhere("Models\SalesOrderPayment.sales_order_id IN (select \Models\SalesruleOrder.sales_order_id from \Models\SalesruleOrder where \Models\SalesruleOrder.rule_id in (select \Models\Salesrule.rule_id from \Models\Salesrule where \Models\Salesrule.limit_payment_time = {$params['limit_payment_time']}))");

            unset($params['limit_payment_time']);
        }

        $result = $resultSalesOrderPayment->execute();

        if (empty($result->count())) {
            $this->errorCode = "404";
            $this->errorMessages = "No Order Found";
            return array();
        } else {
            $allOrders = array();
            foreach ($result as $key => $salesOrderPayment) {
                $salesOrderPaymentArray = $salesOrderPayment->toArray(['payment_id', 'sales_order_id', 'method', 'additional_data', 'pg_transaction_id'], true);
                if ($salesOrderPaymentArray['method'] == "manual_transfer") {
                    $salesOrderPaymentArray['proof_of_payment'] = $salesOrderPayment->getProofOfPayment();
                }

                if ($salesOrderPayment->SalesOrder) {
                    $customerData = $salesOrderPayment->SalesOrder->toArray(["order_no"], true);
                    $salesOrderPaymentArray = $salesOrderPaymentArray + $customerData;
                }
                $allOrders[] = $salesOrderPaymentArray;
            }
        }

        return $allOrders;
    }

    public function prepareOrderUpdate($params = array())
    {
        $salesOrderModel = new \Models\SalesOrder();
        if (!array_key_exists('sales_order_id', $params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Order id can not empty";
            return null;
        }

        $salesOrderModel->setFromArray($params);
        $this->salesOrder = $salesOrderModel;
    }

    public function updateData()
    {
        try {
            $this->salesOrder->saveData('sales_order');
        } catch (\Exception $e) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Update sales order failed, please contact ruparupa tech support";
        }
    }

    public function updateDataPayment($params = array())
    {
        try {
            $salesOrderPaymentModel = new \Models\SalesOrderPayment();
            $getRowPayment = $salesOrderPaymentModel::findFirst("payment_id = '" . $params['payment_id'] . "'");
            if ($getRowPayment) {
                $params['sales_order_id'] = $getRowPayment->toArray()['sales_order_id'];
                $salesOrderPaymentModel->setFromArray($params);
                $salesOrderPaymentModel->saveData('sales_order_payment');
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages = "Update sales order payment failed, please contact ruparupa tech support";
            }
        } catch (\Exception $e) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Update sales order payment failed, please contact ruparupa tech support";
        }
    }

    /**
     * insert giftcard from sales_order to salesrule_order
     *
     * @param array $params
     * @return int
     */
    public function insertDataGiftCards($params = array())
    {
        $affectedRows = 0;
        try {
            if (count($params['data']) > 0) {
                foreach ($params['data'] as $row) {
                    unset($row['batch']);
                    $salesruleOrderModel = new \Models\SalesruleOrder();
                    $salesruleOrderModel->setFromArray($row);
                    if ($salesruleOrderModel->save('salesrule_order') === true) {
                        $affectedRows++;
                    }
                }
            } else {
                $affectedRows = 0;
            }
        } catch (\Exception $e) {
            return $affectedRows;
        }

        return $affectedRows;
    }

    public function getOrderItem($params = array())
    {
        if (empty($params['order_no'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Order no is required";
            return array();
        }

        $salesOrderModel = new \Models\SalesOrder();
        if (isset($params['status'])) {
            $salesOrder = $salesOrderModel->findFirst("order_no = '" . $params['order_no'] . "' and status = '" . $params['status'] . "'");
        } else {
            $salesOrder = $salesOrderModel->findFirst("order_no = '" . $params['order_no'] . "'");
        }

        if ($salesOrder) {
            $salesOrderArray = $salesOrder->toArray([], true);
            if ($salesOrder->SalesOrderItem) {
                foreach ($salesOrder->SalesOrderItem as $orderItem) {
                    $skuToHoldModel = new \Models\SkuToHold();
                    $orderItemArray = $orderItem->getDataArray([], true);
                    $skuToHoldResult = $skuToHoldModel->findFirst("sku = '" . $orderItemArray['sku'] . "'");
                    if ($skuToHoldResult) {
                        $salesOrderArray['items'][] = $orderItemArray;
                    }
                }
            }
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages = "Sorry, we can not found your orders";
            return array();
        }

        return $salesOrderArray;
    }

    public function getOrderListByPhoneAndGiftCards($phone, $voucher_code, $usesPerCustomer, $usesPerCustomerFrequence)
    {
        $now = date('Y-m-d');
        $salesOrderModel = new \Models\SalesOrder();
        // $query = "SELECT * FROM \Models\SalesOrder 
        //           WHERE \Models\SalesOrder.customer_id IN (
        //               select \Models\Customer.customer_id 
        //               from \Models\Customer 
        //               where \Models\Customer.phone = '".$phone."')                   
        //           AND \Models\SalesOrder.sales_order_id IN (
        //               select distinct \Models\SalesruleOrder.sales_order_id
        //               from \Models\SalesruleOrder 
        //               inner join \Models\SalesOrderAddress
        //               on \Models\SalesruleOrder.sales_order_id = \Models\SalesOrderAddress.sales_order_id
        //               where \Models\SalesruleOrder.voucher_code = '".$voucher_code."'
        //                     and \Models\SalesOrderAddress.phone = '".$phone."')";
        if ($usesPerCustomerFrequence > 0) {
            $query = "SELECT * FROM \Models\SalesOrder 
                    WHERE \Models\SalesOrder.sales_order_id IN (
                        select \Models\SalesOrder.sales_order_id
                        from \Models\SalesOrder 
                        inner join \Models\Customer
                        on \Models\SalesOrder.customer_id = \Models\Customer.customer_id
                        inner join \Models\SalesruleOrder
                        on \Models\SalesOrder.sales_order_id = \Models\SalesruleOrder.sales_order_id
                        where \Models\SalesruleOrder.voucher_code = '" . $voucher_code . "'
                            and \Models\SalesOrder.created_at between '" . $now . " 00:00:00' and '" . $now . " 23:59:59'
                            and \Models\Customer.phone = '" . $phone . "')                   
                    OR \Models\SalesOrder.sales_order_id IN (
                        select distinct \Models\SalesruleOrder.sales_order_id
                        from \Models\SalesruleOrder 
                        inner join \Models\SalesOrderAddress
                        on \Models\SalesruleOrder.sales_order_id = \Models\SalesOrderAddress.sales_order_id
                        where \Models\SalesruleOrder.voucher_code = '" . $voucher_code . "'
                            and \Models\SalesOrder.created_at between '" . $now . " 00:00:00' and '" . $now . " 23:59:59'
                            and \Models\SalesOrderAddress.phone = '" . $phone . "')";
        } else {
            $query = "SELECT * FROM \Models\SalesOrder 
                    WHERE \Models\SalesOrder.sales_order_id IN (
                        select \Models\SalesOrder.sales_order_id
                        from \Models\SalesOrder 
                        inner join \Models\Customer
                        on \Models\SalesOrder.customer_id = \Models\Customer.customer_id
                        inner join \Models\SalesruleOrder
                        on \Models\SalesOrder.sales_order_id = \Models\SalesruleOrder.sales_order_id
                        where \Models\SalesruleOrder.voucher_code = '" . $voucher_code . "'
                            and \Models\Customer.phone = '" . $phone . "')                   
                    OR \Models\SalesOrder.sales_order_id IN (
                        select distinct \Models\SalesruleOrder.sales_order_id
                        from \Models\SalesruleOrder 
                        inner join \Models\SalesOrderAddress
                        on \Models\SalesruleOrder.sales_order_id = \Models\SalesOrderAddress.sales_order_id
                        where \Models\SalesruleOrder.voucher_code = '" . $voucher_code . "'
                            and \Models\SalesOrderAddress.phone = '" . $phone . "')";
        }
        $salesOrderModel->useReadOnlyConnection();
        $manager = $salesOrderModel->getModelsManager();
        $salesOrder = $manager->executeQuery($query);
        if (sizeof($salesOrder) >= $usesPerCustomer) {
            return true;
        }
        return false;
    }

    // check rule and customer in salesorder per day
    public function getTotalUsedPerCustomerWithDays($rule_id, $customer_id)
    {

        $salesOrderModel = new \Models\SalesOrder();
        $now = date('Y-m-d');
        $query = "SELECT * FROM \Models\SalesOrder 
                    WHERE \Models\SalesOrder.sales_order_id IN (
                        select \Models\SalesOrder.sales_order_id
                        from \Models\SalesOrder 
                        inner join \Models\SalesruleOrder
                        on \Models\SalesOrder.sales_order_id = \Models\SalesruleOrder.sales_order_id
                        where \Models\SalesruleOrder.rule_id = '" . $rule_id . "'
                            and \Models\SalesOrder.customer_id = '" . $customer_id . "'
                            and \Models\SalesOrder.created_at between '" . $now . " 00:00:00' and '" . $now . " 23:59:59')";
        $salesOrderModel->useReadOnlyConnection();
        $manager = $salesOrderModel->getModelsManager();
        $salesOrder = $manager->executeQuery($query);
        return sizeof($salesOrder);
    }

    // check rule and customer in salesorder
    public function getTotalUsedPerCustomer($rule_id, $customer_id)
    {

        $salesOrderModel = new \Models\SalesOrder();
        $now = date('Y-m-d');
        $query = "SELECT * FROM \Models\SalesOrder 
                    WHERE \Models\SalesOrder.sales_order_id IN (
                        select \Models\SalesOrder.sales_order_id
                        from \Models\SalesOrder 
                        inner join \Models\SalesruleOrder
                        on \Models\SalesOrder.sales_order_id = \Models\SalesruleOrder.sales_order_id
                        where \Models\SalesruleOrder.rule_id = '" . $rule_id . "'
                            and \Models\SalesOrder.customer_id = '" . $customer_id . "')";
        $salesOrderModel->useReadOnlyConnection();
        $manager = $salesOrderModel->getModelsManager();
        $salesOrder = $manager->executeQuery($query);
        return sizeof($salesOrder);
    }

    // check rule in salesorder per period
    public function getTotalUsedPerRule($rule_id)
    {

        $salesOrderModel = new \Models\SalesOrder();

        $query = "SELECT * FROM \Models\SalesOrder 
                    WHERE \Models\SalesOrder.sales_order_id IN (
                        select \Models\SalesOrder.sales_order_id
                        from \Models\SalesOrder 
                        inner join \Models\SalesruleOrder
                        on \Models\SalesOrder.sales_order_id = \Models\SalesruleOrder.sales_order_id
                        where \Models\SalesruleOrder.rule_id = '" . $rule_id . "')";
        $salesOrderModel->useReadOnlyConnection();
        $manager = $salesOrderModel->getModelsManager();
        $salesOrder = $manager->executeQuery($query);
        return sizeof($salesOrder);
    }

    // check if order_no has been used or not
    public function checkOrderNoAvailability($orderno)
    {
        $salesOrderModel = new \Models\SalesOrder();

        $query = "SELECT * FROM \Models\SalesOrder 
                    WHERE \Models\SalesOrder.order_no = '" . $orderno . "'";
        $manager = $salesOrderModel->getModelsManager();
        $salesOrder = $manager->executeQuery($query);
        if (sizeof($salesOrder) > 0) {
            return false;
        }

        return true;
    }

    public function slackLimitRequirement($salesOrder, $customerId)
    {
        $now = date("Y-m-d");
        if ($now < '2020-05-01' || $now > '2020-05-31') {
            return;
        }

        $skuLimitQtyList = explode(",", getenv('SKU_LIMIT_QTY_LIST'));

        $skuLimitQtyItems = array();
        $isSkuLimitQty = false;
        $countItem = 0;
        $sendSlack = false;
        foreach ($salesOrder->getItems() as $item) {
            if (in_array($item->sku, $skuLimitQtyList)) {
                $isSkuLimitQty = true;
                $skuLimitQtyItems[] = $item->sku;

                $queryInsku = implode("','", $skuLimitQtyList);
                $sql = "SELECT COUNT(distinct sales_order.sales_order_id) as count_item
                        FROM sales_order_item as item 
                        LEFT JOIN sales_order ON item.sales_order_id = sales_order.sales_order_id 
                        WHERE sku IN ('" . $queryInsku . "')
                        AND sales_order.created_at BETWEEN '2020-05-01 00:00:00' AND '2020-06-01 00:00:00'
                        AND sales_order.status != 'canceled'
                        AND sales_order.customer_id = " . $customerId;

                $SalesOrderItem = new \Models\SalesOrderItem();
                $result = $SalesOrderItem->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );

                $countItem = $result->fetchAll()[0]['count_item'];

                if ($countItem > 2) {
                    $sendSlack = true;
                }
                break;
            }
        }

        if ($sendSlack) {
            $skuLimitQtyItemsString = implode(",", $skuLimitQtyItems);
            $params_data = array(
                "message" => "Transaction with limited qty : " . $salesOrder->getOrderNo(),
                "slack_channel" => $_ENV['OPS_ISSUE_SLACK_CHANNEL'],
                "slack_username" => $_ENV['OPS_ISSUE_SLACK_USERNAME']
            );
            $this->notificationError($params_data);
        }
    }

    public function updateReorder($params = array())
    {
        $apiWrapper = new APIWrapper(getenv('REORDER_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("reorder");
        $resultData = array();

        if ($apiWrapper->sendRequest("put")) {
            $apiWrapper->formatResponse();
            $resultData = $apiWrapper->getData();

            // \Helpers\LogHelper::log('reorder','[SUCCESS] ' . json_encode($resultData));
        } else {
            \Helpers\LogHelper::log('reorder', '[ERROR] when calling update reorder ' . json_encode($params));
        }

        return $resultData;
    }

    public function getSalesOrderDetailByID($params = array())
    {
        $salesOrderModel = new \Models\SalesOrder();

        $sql = "SELECT * FROM sales_order 
                    WHERE sales_order.sales_order_id = " . $params['sales_order_id'];

        $salesOrderModel->useReadOnlyConnection();
        $result = $salesOrderModel->getDi()->getShared($salesOrderModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $resultArray = $result->fetch();

        return $resultArray;
    }

    public function getSalesOrderItems($params = array())
    {
        try {
            $salesOrderModel = new \Models\SalesOrder();

            $sql = "SELECT sales_order_item_id, sku, name, store_code, qty_ordered, is_free_item, group_shipment FROM sales_order_item WHERE sales_order_id = {$params['sales_order_id']}";

            $salesOrderModel->useReadOnlyConnection();
            $result = $salesOrderModel->getDi()->getShared($salesOrderModel->getConnection())->query($sql);
            $result->setFetchMode(
                Database::FETCH_ASSOC
            );
            $resultArray = $result->fetchAll();
            return $resultArray;
        } catch (\Exception $e) {
            return array('error' => $e);
        }
    }

    public function getOrderItemWithStatusFulfillment($sales_order_id)
    {
        try {
            $salesOrderItemModel = new \Models\SalesOrderItem();

            $sql = "
                SELECT 
                    soi.sales_order_item_id, 
                    soi.sku, soi.name, 
                    soi.store_code, 
                    soi.qty_ordered, 
                    soi.is_free_item, 
                    soi.group_shipment, 
                    sii.status_fulfillment 
                FROM 
                    sales_order_item soi
                        LEFT JOIN 
                    sales_invoice_item sii ON soi.sales_order_item_id = sii.sales_order_item_id
                 WHERE soi.sales_order_id = " . $sales_order_id;

            $result = $salesOrderItemModel->getDi()->getShared('dbReader')->query($sql);
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $resultArray = $result->fetchAll();
            return $resultArray;
        } catch (\Exception $e) {
            return array('error' => $e);
        }
    }

    // public function pushnotifMobile($customerId, $customerName)
    // {

    //     // $client = new GuzzleClient();
    //     // $curlParam = [
    //     //     'timeout'=>'10.0',
    //     //     'headers' => ['Content-Type' => 'application/json'],
    //     //     'body' => json_encode(
    //     //         ['data' => [
    //     //             'customer_id' => $customerId,
    //     //             'title' => 'Menunggu Pembayaran',
    //     //             'body' => 'Hallo '.$customerName.', kamu belum membayar pesanan kamu nih. Yuk, segera selesaikan pembayaran sebelum terlambat'
    //     //         ]]
    //     //     )
    //     // ];

    //     // $responseApi = $client->request('POST',$_ENV['PUSH_NOTIF_API']."/push-device",$curlParam );
    //     // LogHelper::log("notification-mobile", "Response : '". $responseApi->getBody());

    //     // $nsq = new Nsq();
    //     $nsq = new \Library\Nsq();
    //     $message = [
    //         "data" => [
    //             'customer_id' => $customerId,
    //             'title' => 'Menunggu Pembayaran',
    //             'body' => 'Hi ' . $customerName . ', kamu belum membayar pesanan kamu nih. Yuk, segera selesaikan pembayaran sebelum terlambat'
    //         ],
    //         "message" => "pushNotif"
    //     ];
    //     $nsq->publishCluster('transaction', $message);
    // }

    private function getInvoiceItem($salesOrderItemID = 0)
    {
        $salesInvoiceItemArray = array();
        $salesInvoiceItemModel = new \Models\SalesInvoiceItem();
        $salesInvoiceItem = $salesInvoiceItemModel->findFirst("sales_order_item_id = $salesOrderItemID");
        if ($salesInvoiceItem) {
            $salesInvoiceItemArray = $salesInvoiceItem->toArray([], true);
        }

        return $salesInvoiceItemArray;
    }

    private function getInvoiceItemStatusFulfillment($salesOrderItemID = 0)
    {
        $salesOrderModel = new \Models\SalesOrder();

        $sql = "
            SELECT 
                sku, 
                status_fulfillment 
            FROM 
                sales_invoice_item 
            WHERE 
                sales_order_item_id = " . $salesOrderItemID;

        $salesOrderModel->useReadOnlyConnection();
        $result = $salesOrderModel->getDi()->getShared('dbReader')->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $resultArray = $result->fetch();

        return $resultArray;
    }

    private function runReadSql($sql = '')
    {
        $shipmentModel = new \Models\SalesShipment();
        $shipmentModel->useReadOnlyConnection();
        $result = $shipmentModel->getDi()->getShared($shipmentModel->getConnection())->query($sql);
        $result->setFetchMode(
            Database::FETCH_ASSOC
        );
        $resultArray = $result->fetchAll();

        return $resultArray;
    }

    public function insertInstallationDataItem()
    {
        $orderNo = $this->salesOrder->getOrderNo();
        if (substr($orderNo, 0, 4) == "ODIN") {
            if (!isset($this->shoppingCartData['sales_installation_id']) || $this->shoppingCartData['sales_installation_id'] == 0) {
                \Helpers\LogHelper::log("installation_order", "[ERROR] Sales installation id for cart_id " . $this->shoppingCartData['cart_id'] . " not found");
                $this->errorCode = 404;
                $this->errorMessages = "Sales installation ID not found on cart";
            }

            $salesInstallationMdl = new \Models\SalesInstallation;
            $salesInstallation = $salesInstallationMdl->findFirst("sales_installation_id = " . $this->shoppingCartData['sales_installation_id']);
            if (!$salesInstallation) {
                \Helpers\LogHelper::log("installation_order", "[ERROR] Sales installation data for sales_installation_id " . $this->shoppingCartData['sales_installation_id'] . " not found");
                $this->errorCode = 404;
                $this->errorMessages = "Sales installation data not found";
            }

            // B2b installation
            if ($this->shoppingCartData['reference_installation_pending_order_no'] != "") {
                $paramsUpdateInstallationStatus = array();
                $paramsUpdateInstallationStatus['sales_installation_id'] = (int)$salesInstallation->getSalesInstallationID();
                $paramsUpdateInstallationStatus['installation_order_no'] = $orderNo;
                $paramsUpdateInstallationStatus['status'] = "waiting_payment";
                $this->updateInstallationB2BStatus($paramsUpdateInstallationStatus);
            } else { // ODI installation
                $salesInstallation->setInstallationOrderNo($orderNo);
                $salesInstallation->setStatus("waiting_payment");
                $salesInstallation->setInstallationCartID($this->shoppingCartData['cart_id']);
                $result = $salesInstallation->update();
                if ($result === false) {
                    \Helpers\LogHelper::log("installation_order", "[ERROR] Failed to update sales installation data for sales_installation_id " . $this->shoppingCartData['sales_installation_id'] . ". Installation order no: " . $orderNo);
                    $this->errorCode = 500;
                    $this->errorMessages = "Failed to update sales installation data";
                }
            }
        } else {
            // Skip insert data into sales_order_item_installation if cart type informa_b2b/ace_b2b
            if ($this->shoppingCartData['cart_type'] == "ace_b2b" || $this->shoppingCartData['cart_type'] == "informa_b2b" || $this->shoppingCartData['cart_type'] === 'additional_cost') {
                return;
            }

            foreach ($this->salesOrder->getItems() as $item) {
                $isInstalled = 0;
                $isNeedInstallation = 0;

                if ($item->getIsInstalled() && $item->getIsInstallationEligible()) {
                    $isInstalled = 1;
                } else if ($item->getIsNeedInstallation() && $item->getIsNeedInstallationEligible()) {
                    $isNeedInstallation = 1;
                }

                // If contain any installation then save it to sales_order_item_installation table
                if ($isInstalled == 1 || $isNeedInstallation == 1) {
                    $installationModel = new \Models\SalesOrderItemInstallation();
                    $insertData = array(
                        'sales_order_id' => $item->getSalesOrderId(),
                        'sales_order_item_id' => $item->getSalesOrderItemId(),
                        'is_installed' => $isInstalled,
                        'is_need_installation' => $isNeedInstallation,
                    );
                    $installationModel->setFromArray($insertData);
                    $installationModel->saveData("sales_order_item_installation");
                }
            }
        }
    }

    private function createOrderB2b($cart = array(), $orderID)
    {
        $paramsB2b = array();
        $paramsUpdateQuotation = array();
        $paramsB2b['cart_id'] =  $cart['cart_id'];
        $paramsB2b['sales_order_id'] = (int)$orderID;
        $this->insertOrderB2b($paramsB2b);
        if (!empty($cart['b2b'])) {
            if ($cart['b2b']['quotation_id'] != 0) {
                $paramsUpdateQuotation['quotation_id'] = $cart['b2b']['quotation_id'];
                $paramsUpdateQuotation['status'] = "checkout";
                $paramsUpdateQuotation['customer_id'] = $cart['customer']['customer_id'];
                $this->updateStatusQuotation($paramsUpdateQuotation);
            }
        }
    }

    private function updateInstallationB2BStatus($param = array())
    {
        $apiWrapper = new APIWrapper(getenv('INSTALLATION_API'));
        $apiWrapper->setEndPoint("installation/b2b/status");
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("put")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to update your installation status, please try again";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("installation_order", "Failed to update b2b installation status. Response: " . json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
        return true;
    }

    public function getQuotaCreditMemo($order_no = "")
    {
        if ($order_no == "") {
            $order_no = $this->order->getOrderNo();
        }

        $apiWrapper = new APIWrapper(getenv('ORDER_API_V2'));
        $apiWrapper->setEndPoint("credit_memo/data/quota/{$order_no}");
        if ($apiWrapper->sendRequest("get")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to get quota credit memo for order no {$order_no}";
            return false;
        }

        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        return $data['data'];
    }

    public function transactUnifyReturn($payload)
    {
        $apiWrapper = new APIWrapper(getenv('ORDER_API_V2'));
        $apiWrapper->setEndPoint("sales/unify/add_transaction/refund");
        $apiWrapper->setParam($payload);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to transact point";
            return false;
        }

        $data['data'] = $apiWrapper->getData();
        $data['errors'] = $apiWrapper->getError();
        $data['message'] = $apiWrapper->getMessages();

        return $data['data'];
    }

    private function insertOrderB2b($param = array())
    {
        $apiWrapper = new APIWrapper(getenv('ORDER_B2B_API'));
        $apiWrapper->setEndPoint("order");
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed create Order B2b, please try again";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("CreateOrderB2b", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
        return true;
    }

    private function updateStatusQuotation($param = array())
    {

        $apiWrapper = new APIWrapper(getenv('QUOTATION_API'));
        $apiWrapper->setEndPoint("b2b/quotation/status");
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to update your Quotation status, please try again";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("CreateOrderB2b", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
        return true;
    }

    private function redeemStampsVoucherUnify($customerId, $shoppingCart)
    {
        if (!empty($shoppingCart['customer_cohesive']['member_id']) || !empty($shoppingCart['customer']['rrr_id'])) {            
            $cartId = $shoppingCart['cart_id'];
            // Hit stamps endpoint to redeem voucher
            $apiWrapper = new APIWrapper(getenv('GO_CUSTOMER_COHESIVE'));
            $apiWrapper->setEndPoint("redemption_approval/redeem-voucher");
            $apiWrapper->setParam(
                array(
                    'customer_id' => $customerId,
                    'cart_id' => $cartId
                )
            );

            LogHelper::log(
                'cohesive_redeem_voucher',
                '[SUCCESS] ' . json_encode($apiWrapper->getParam()),
                'info'
            );

            if ($apiWrapper->sendRequest("post")) {
                $apiWrapper->formatResponse();
                LogHelper::log(
                    'cohesive_redeem_voucher',
                    '[SUCCESS] ' . json_encode($apiWrapper->getData()),
                    'info'
                );
            } else {
                LogHelper::log(
                    'cohesive_redeem_voucher',
                    '[ERROR] When calling go-unify endpoint /redemption_approval/redeem-voucher API ' . json_encode($apiWrapper->getError()) .
                        ' [PAYLOAD] ' . json_encode($apiWrapper->getParam())
                );

                return false;
            }
            // End of redeem voucher in go-unify

        }

        return true;
    }

    public function sentToKlSysGoOrderV2($orderNo)
    {
        $param = array();
        $param["order_no"] = $orderNo;

        $apiWrapper = new APIWrapper(getenv("ORDER_API_V2"));
        $apiWrapper->setEndPoint("kawanlama/klsys/sent");
        $apiWrapper->setParam($param);

        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("create_so_sap", json_encode($apiWrapper->getError()));
        }

        return;
    }

    public function getInvoiceSap($orderNo)
    {
        try {
            $apiWrapper = new APIWrapper(getenv("ORDER_API_V2"));
            $apiWrapper->setEndPoint("sap/invoices/" . $orderNo);
            if ($apiWrapper->sendRequest("get")) {
                $apiWrapper->formatResponse();
            } else {
                $this->errorCode = "500";
                $this->errorMessages = "Failed get invoices sap";
                return null;
            }

            if (!empty($apiWrapper->getError())) {
                \Helpers\LogHelper::log("api_call", "get invoice sap failed", "error");
                \Helpers\LogHelper::log("api_call", json_encode($apiWrapper->getError()), "error");
                return null;
            } else {
                return $apiWrapper->getData();
            }
        } catch (\Throwable $th) {
            $this->errorCode = "500";
            $this->errorMessages = "Failed get invoices sap";
            return null;
        }
    }
    public function cancelRedeemStampsVoucherUnify($order_no)
    {
        if (empty($order_no)) {
            return false; // No order number provided, return false
        }

        try {
            // Hit stamps endpoint to cancel redeem voucher
            $apiWrapper = new APIWrapper(getenv('GO_CUSTOMER_COHESIVE'));
            $apiWrapper->setEndPoint("redemption_approval/cancel-redemption");
            $apiWrapper->setParam(
                array(
                    'order_no' => $order_no,
                    'source' => 'cancel_order',
                )
            );

            LogHelper::log(
                'cohesive_cancel_redeem_voucher',
                '[SUCCESS] ' . json_encode($apiWrapper->getParam()),
                'info'
            );

            if ($apiWrapper->sendRequestUnify("post")) {
                $apiWrapper->formatResponse();
                LogHelper::log(
                    'cohesive_redeem_voucher',
                    '[SUCCESS] ' . json_encode($apiWrapper->getData()),
                    'info'
                );
            } else {
                $errorResponse = $apiWrapper->getErrorBody();
                $this->errorCode = $errorResponse->code;
                $this->errorMessages = $errorResponse->messages;

                LogHelper::log(
                    'cohesive_cancel_redeem_voucher',
                    '[ERROR] When calling go-unify endpoint /redemption_approval/cancel-redemption API ' . json_encode($apiWrapper->getErrorBody()) .
                        ' [PAYLOAD] ' . json_encode($apiWrapper->getParam())
                );

                return false;
            }

            // End of redeem voucher in go-unify
            return true;
        } catch (Exception $e) {
            // Handle any other exceptions here, if needed
            LogHelper::log(
                'cohesive_cancel_redeem_voucher',
                '[ERROR] An exception occurred: ' . $e->getMessage()
            );
            return false;
        }
    }

    private function createPendingOrderDetail($param) {
        $apiWrapper = new APIWrapper(getenv('ORDER_B2B_API'));
        $apiWrapper->setEndPoint("pending_order_detail");
        $apiWrapper->setParam($param);
        if ($apiWrapper->sendRequest("post")) {
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed create Order B2b, please try again";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            \Helpers\LogHelper::log("create_informa_pending_order", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;
        }
    }

    public function purchasePwpSku($shoppingCartData) {
        try {
            foreach ($shoppingCartData['items'] as $item) {
                $salesrulePwpSkuModel = new \Models\SalesrulePwpSku();

                if ($item['pwp']['pwp_id'] != 0 && $item['qty_ordered'] != 0) {
                    $qty = json_decode($item['qty_ordered'],true);
                    $pwp = json_decode($item['pwp']['pwp_id'],true);
                    $salesrulePwpSkuModel->updateSkuBought($pwp, $qty);
                }
            }
        } catch (\Exception $e) {
            LogHelper::log("salesrule_pwp_sku", $this->shoppingCartData['cart_id'] . " : Update Purchase SKU PWP failed, Msg : " . $e->getMessage());
        }

        return true;
    }
}