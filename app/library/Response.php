<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/14/2016
 * Time: 9:43 AM
 */

namespace Library;

/**
 * Class Response
 * @package Library
 *
 * As for API respose, we using JSONAPI standard output (See : http://jsonapi.org/)
 * with some modified to fit our needs
 *
 * @TODO : Improve it to became our standart json output
 */
class Response extends \Phalcon\Http\Response
{
    protected $include;

    /**
     * Override default method, we need to make our own formating
     */
    public function setContent($content = array())
    {
        $responseArray = array();
        if (empty($content['errors'])) {
            $responseArray['errors'] = array();
        } else {
            /*if(empty($content['errors']['title'])) {
                unset($content['errors']['title']);
            }*/
            $responseArray['errors'] = $content['errors'];
        }

        if (empty($content)) {
            $responseArray['errors'] = array("code" => "204", "title " => $this->getResponseDescription(204),  "messages" => $this->getResponseDescription(204));
        }

        if (empty($content['data'])) {
            $responseArray['data'] = array();
        } else {
            $responseArray['data'] = $content['data'];
        }

        if (empty($content['count'])) {
            $responseArray['count'] = array();
        } else {
            $responseArray['count'] = $content['count'];
        }

        if (empty($content['messages'])) {
            $responseArray['messages'] = array();
        } else {
            $responseArray['messages'] = $content['messages'];
        }

        if (!empty($this->include)) {
            foreach ($this->include as $include) {
                $responseArray['include'][$include] = $content[$include];
            }
        }

        // we only sendoing JSON
        parent::setContent(json_encode($responseArray));
    }

    public function setInclude($include = array())
    {
        $this->include = $include;
    }

    public function setHeaderStatus($code = '')
    {
        $codeMessage = $this->getResponseDescription($code);
        parent::setStatusCode($code, $codeMessage)->sendHeaders();
    }

    public function getResponseDescription($code)
    {
        $codes = array(

            // Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',

            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',

            // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',  // 1.1
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            // 306 is deprecated but reserved
            307 => 'Temporary Redirect',

            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            422 => 'Unprocessable Entity response',

            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded',

            // Ruparupa internal error code


            // Parameter Error
            'RR001' => 'Parameter does not exist',
            'RR002' => 'General Error',

            // Validation error
            'RR100' => 'Validation Error',
            'RR101' => 'Fields is required',
            'RR102' => 'Data already exist',
            'RR103' => 'Data is not active',
            'RR104' => 'Data is deleted',
            'RR105' => 'Data is not active or deleted',
            'RR106' => 'Data not found',
            'RR107' => 'Wrong password',
            'RR108' => 'Member number not found',

            // Shopping cart error
            'RR200' => 'Shopping Cart General Error',
            'RR201' => 'Shopping cart save error',
            'RR202' => 'Shopping cart not found',
            'RR203' => 'Empty Stock',
            'RR204' => 'Items not found',
            'RR205' => 'Empty shipping destination',
            'RR206' => 'Qty Item not valid',
            'RR207' => 'Empty Stock Free Item',

            // DB Error
            'RR301' => 'Save data failed',
            'RR302' => 'Data not found',
            'RR303' => 'Update data failed',
            'RR304' => 'Delete data failed',

            // Transaction Process
            'RR400' => 'Transaction General Error',
            'RR401' => 'Validation Error',
            'RR402' => 'Shopping cart already in transaction process',
            'RR403' => 'Failed to process order',
            'RR404' => 'Sales order not valid'



        );

        $result = (isset($codes[$code])) ?
            $codes[$code]          :
            'Unknown Status Code';

        return $result;
    }
}
