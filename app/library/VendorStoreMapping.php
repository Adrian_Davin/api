<?php

namespace Library;

class VendorStoreMapping 
{
    private static $tokopediaMapping = array(
        "8231055" => array(
            "name" => "Tokopedia Informa",
            "shop_id" => "8231055",
            "sales_office" => "H300",
            "sales_group" => "H65",
            "sales_id" => "135152", // Justian Djuhana
        ),
        "6918815" => array(
            "name" => "Tokopedia Selma",
            "shop_id" => "6918815",
            "sales_office" => "H203",
            "sales_group" => "H65",
            "sales_id" => "170266", // Graciany
        ),
    );

    private static $shopeeMapping = array(
        "586863199" => array(
            "name" => "Shopee Informa",
            "shop_id" => "586863199",
            "sales_office" => "H300",
            "sales_group" => "H65",
            "sales_id" => "135152", // Justian Djuhana
        ),
        "272588101" => array(
            "name" => "Shopee Selma",
            "shop_id" => "272588101",
            "sales_office" => "H203",
            "sales_group" => "H65",
            "sales_id" => "170266", // Graciany
        ),
    );

    private static $tiktokMapping = array(
        // "1" => array(
        //     "name" => "Tiktok Informa",
        //     "shop_id" => "104",
        //     "sales_office" => "H300",
        //     "sales_group" => "H65",
        //     "sales_id" => "170266", // Justian Djuhana
        // ),
        "1" => array(
            "name" => "Tiktok Selma",
            "shop_id" => "1",
            "sales_office" => "H203",
            "sales_group" => "H65",
            "sales_id" => "170266", // Graciany
        ),
    );

    public static function GetTokopediaMapping($shop_id) {
        if (isset(VendorStoreMapping::$tokopediaMapping[$shop_id])) {
            return VendorStoreMapping::$tokopediaMapping[$shop_id];
        }

        return array();
    }

    public static function GetShopeeMapping($shop_id) {
        if (isset(VendorStoreMapping::$shopeeMapping[$shop_id])) {
            return VendorStoreMapping::$shopeeMapping[$shop_id];
        }

        return array();
    }

    public static function GetTiktokMapping($shop_id) {
        if (isset(VendorStoreMapping::$tiktokMapping[$shop_id])) {
            return VendorStoreMapping::$tiktokMapping[$shop_id];
        }

        return array();
    }
}
