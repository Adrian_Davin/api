<?php
/**
 * Library to send ruparupa order to SAP system
 */

namespace Library;

class SOB2B
{

    protected $paramWsdl = array();

    protected $order_no;
    protected $order_id;
    protected $order_date;
    protected $order_type;
    protected $cust_sold_no;
    protected $sales_id;
    protected $sales_office = "O300";
    protected $cust_ship_no;
    protected $cust_bill_no;
    protected $req_dlv_date;
    protected $ship_cost;
    protected $grand_price;
    protected $partial_ship = "";
    protected $remark;
    protected $source;
    protected $sales_grp;
    protected $shipping_point = "";
    protected $ship_condition = "Z6"; //land & sea E-Commerce
    protected $forwarder;
    protected $dp_value = "";
    protected $tax_class = "";
    protected $ship_name_1 = "";
    protected $ship_name_2 = "";
    protected $ship_address_1 = "";
    protected $ship_address_2 = "";
    protected $ship_address_3 = "";
    protected $ship_city = "";
    protected $ship_portal = "";
    protected $ship_phone = "";
    protected $ship_email = "";
    protected $bill_block = "";
    protected $dlv_block = "";

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    protected $invoiceItems;

    protected $cart;

    protected $parameter= array();

    public function __construct($invoice = null)
    {
        $this->paramWsdl = array('login' => $_ENV['B2B_WSDL_LOGIN'] , 'password' => $_ENV['B2B_WSDL_PASS'],"trace" => 1, "exception" => 1);
        $this->sales_id = $_ENV['B2B_SALES_ID'];

        $mongoDb = new Mongodb();
        $mongoDb->setCollection("SoB2B");
        $this->collection = $mongoDb->getCollection();

        if(!empty($invoice)) {
            $this->invoice = $invoice;

            // TODO: adjust with new cart
        //     $mongoDb = new Mongodb();
        //     $mongoDb->setCollection("cart");
        //     $this->collectionCart = $mongoDb->getCollection();
        //     $this->cart = $this->collectionCart->findOne(['reserved_order_no' => $this->invoice->SalesOrder->getOrderNo()]);
         }
    }

    /**
     * @param array $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    public function prepareDCParams()
    {
        $companyModel = New \Models\CompanyProfileDC();
        if(empty($this->invoice)) {
            return false;
        }

        /**
         * @var $currentOrder \Models\SalesOrder
         */
        $currentOrder = $this->invoice->SalesOrder;

        $shipToId = $billToId = $soldToId = $currentOrder->getCompanyId();

        // if($currentOrder->SalesOrderAddress) {
        //     foreach($currentOrder->SalesOrderAddress as $orderAddress) {

        //         if($orderAddress->getAddressType() == 'shipping') {
        //             $shipToId = $soldToId = $orderAddress->CompanyAddress->getAddressSapId();
        //         }

        //         if($orderAddress->getAddressType() == 'billing') {
        //             $billToId = $orderAddress->CompanyAddress->getAddressSapId();
        //         }
        //     }
        // }

        // $company2CustomerModel = new \Models\Company2Customer();
        // $resultCompany2Customer = $company2CustomerModel->findFirst('customer_id = '.$currentOrder->getCustomerId().' and status >= 0');

        // $companyAddress = new \Models\CompanyAddress();
        // $resultCompanyAddress = $companyAddress->findFirst('address_type = "sold" and company_id = '.$resultCompany2Customer->getCompanyId().' and status = 10');
        // if($resultCompanyAddress)
        //     $soldToId = $resultCompanyAddress->getAddressSapId();

        // $companyAddress = new \Models\CompanyAddress();
        // $resultCompanyAddress = $companyAddress->findFirst('address_type = "forwarding" and company_id = '.$resultCompany2Customer->getCompanyId().' and status = 10');
        // if($resultCompanyAddress)
        //     $forwardingToId = $resultCompanyAddress->getAddressSapId();
        // else
        //     $forwardingToId = "";
        $forwardingToId = "";
        $orderData = date_create($currentOrder->getCreatedAt());

        $this->order_no = $this->order_id = $currentOrder->getOrderNo();
        //if(isset($this->cart['po']['number']))
        //    $this->order_id = $currentOrder->getOrderNo().'|'.$this->cart['po']['number'];

        $this->order_date = date_format($orderData, "Ymd");
        //$this->order_type = $resultCompany2Customer->Company->getOrderType();
        $this->order_type = 'B2B_ODI';
        $this->cust_sold_no = $soldToId;
        $this->cust_ship_no = $shipToId;
        $this->cust_bill_no = $billToId;
        $this->forwarder = $forwardingToId;
        $this->req_dlv_date = date_format($orderData, "Ymd");
        $this->ship_cost = number_format( ($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount()) ,0, '.', '');
        $this->grand_price = number_format($this->invoice->getGrandTotal(),0, '.', '');

        /**
         * Sending remark to SAP
         */
        $this->remark = "";
        if(!empty($this->invoice->SalesShipment) && !empty($this->invoice->SalesShipment->getTrackNumber())) {
            $carrierCode = !empty($this->invoice->SalesShipment->ShippingCarrier->getCarrierCode()) ? $this->invoice->SalesShipment->ShippingCarrier->getCarrierCode() : "";
            $this->remark = $carrierCode . $this->invoice->SalesShipment->getTrackNumber();
        }

        // Genereate items
        $this->invoiceItems = $this->invoice->SalesInvoiceItem;

        $carrier_code = '';        

        //$this->source = $resultCompany2Customer->Company->getSourceFrom();
        //$this->sales_grp = $resultCompany2Customer->Company->getSalesGrp();
        
        $this->source = 'E_COMMERCE_ODI_B2B';
        $this->sales_grp = 'O01';        
        
        if (!empty($this->invoice->SalesShipment) && !empty($this->invoice->SalesShipment->SalesOrderAddress)) {
            $address = $this->invoice->SalesShipment->SalesOrderAddress;

            $address->setSalesOrderId($this->invoice->getSalesOrderId());
            $address->setAddressType('shipping');
            $address = $address->generateEntity();

            $ship_address = [];
            
            for ($i = 0; $i < 3; $i++) {
                $ship_address[$i] = substr($address['full_address'], $i*35, 35);
            }

            $customer = $this->invoice->SalesOrder->SalesCustomer;
            
            $sql = "SELECT carrier_code
                        from shipping_carrier 
                        where carrier_id = ".$carrier_id;
            $result = $companyModel->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(Database::FETCH_ASSOC);
            $carrierResult = $result->fetchAll()[0];
            $carrier_code = $carrierResult['carrier_code'];

            $this->ship_name_1 = substr($address['first_name'], 0, 35);
            $this->ship_name_2 = substr($address['last_name'], 0, 35);
            $this->ship_address_1 = $ship_address[0];
            $this->ship_address_2 = (!empty($ship_address[1])) ? $ship_address[1] : "";
            $this->ship_address_3 = (!empty($ship_address[2])) ? $ship_address[2] : "";
            $this->ship_city = substr($address['city_name'], 0, 35);
            $this->ship_portal = "";
            $this->ship_phone = $address['phone'];
            $this->ship_email = substr($customer->getCustomerEmail(), 0, 35);
            $this->bill_block = "";
            $this->dlv_block = "";
        }

        //cek prefix if lazada or shopee or tokopedia set ship_condition to z8. pickup E-Commerce
        if(substr($this->order_no,0,3) == "LZD" || substr($this->order_no,0,4) == "ODIS" || substr($this->order_no,0,4) == "ODIT" || substr($this->order_no,0,4) == "ODIK"){
            $this->ship_condition = "Z8";

            $salesOrderVendorModel = new \Models\SalesOrderVendor();
            $salesOrderVendor = $salesOrderVendorModel->findFirst('order_no = "'.$currentOrder->getOrderNo().'"');

            $remarkDelivId = $carrier_code;

            $overrideData = array();
            if (strpos(strtolower($remarkDelivId), 'ownfleet') !== false) {
                $remarkDelivId = "OWNFLEET";
            }

            if(substr($this->order_no,0,4) == "ODIS") {
                $this->remark = "TYP_ORD: SHOPEE\nDEL_ID: ".$remarkDelivId."\nSP-" . $this->invoice->getInvoiceNo();

                $overrideData = \Library\VendorStoreMapping::GetShopeeMapping($salesOrderVendor->shop_id);
            } else if(substr($this->order_no,0,4) == "ODIT") {
                $skuPromoTokpedDC = "";
                $additionalRemark = "";

                $skuPromoTokpedDC = getenv("SKU_PROMO_TOKPED_DC");
                
                if(!empty($skuPromoTokpedDC)){
                    $skuPromoTokpedDCArray = explode(",", $skuPromoTokpedDC);
                    if(!empty($this->invoiceItems)) {
                        foreach($this->invoiceItems as $item) {
                            if(in_array($item->getSku(), $skuPromoTokpedDCArray)){
                                $additionalRemark = getenv("REMARK_PROMO_TOKPED_DC");
                            }
                        }
                    }
                }
                $this->remark = "TYP_ORD: TOKOPEDIA\nDEL_ID: ".$remarkDelivId."\nTK-" . $this->invoice->getInvoiceNo()." ".$additionalRemark;

                $overrideData = \Library\VendorStoreMapping::GetTokopediaMapping($salesOrderVendor->shop_id);
            } else if (substr($this->order_no,0,4) == "ODIK") {
                // TODO: tiktok development, need confirm
                $this->remark = "TYP_ORD: SHOPEE\nDEL_ID: ".$remarkDelivId."\nTT-" . $this->invoice->getInvoiceNo();

                $overrideData = \Library\VendorStoreMapping::GetTiktokMapping($salesOrderVendor->shop_id);

            if (sizeof($overrideData) > 0) {
                $this->sales_id = $overrideData["sales_id"];
                $this->sales_office = $overrideData["sales_office"];
                $this->sales_grp = $overrideData["sales_group"];
            }
        }
    }

    public function generateDCParams()
    {
        $thisArray = get_object_vars($this);
        $b2b_order_detail = array();
        $underscoreParams = array('sales_grp');
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(is_scalar($val)) {
                if(in_array($key,$underscoreParams)){
                    $currentKey = strtoupper($key);
                }else{
                    $currentKey = strtoupper(str_replace("_", "", $key));
                }

                $b2b_order_detail[$currentKey] = $val;
            }
        }
        $productDetail = $this->generateDCProductDetail();

        $parameter = array(
            "HEADER" => $b2b_order_detail,
            "T_ITEM" => $productDetail,
            "company_code" => "ODI"
        );

        //save to mongodb
        $this->collection->insertOne($parameter);

        $this->parameter = $parameter;
    }

    public function generateDCProductDetail()
    {
        $productDetail = array();

        $idProductDetail = 0;
        if(!empty($this->invoiceItems)) {
            /**
             * @var $item \Models\SalesInvoiceItem
             */
            foreach($this->invoiceItems as $item) {
            //    $warehouseAttribute = $item->ProductVariant->Product->ProductAttributeInt->filter(function($attribute) {
            //        if ($attribute->getAttributeId() == 329) {
            //            return $attribute;
            //        }
            //     });

            //     $warehouseSelection = [];
            //      if(count($warehouseAttribute) > 0) {
            //         $warehouseSelection = $warehouseAttribute[0]->getDataArray();
            //     }

                $idProductDetail++;

                $finalUnitPrice = (int)$item->getSellingPrice() - (int)$item->getDiscountAmount();
                $unitPrice = ($finalUnitPrice <= 0) ? 1 : $finalUnitPrice;
                $salesPrice = $unitPrice * $item->getQtyOrdered();

                //$salesUom = \Helpers\ProductHelper::getSalesUom($item->getSku());
                $salesUom = 'EA';

                $poNumber = '';
                $poLevelItem = '';
                // if(isset($this->cart['po']['number'])){
                //     $poNumber = $this->cart['po']['number'];
                //     $poLevelItem = $this->cart['po']['items'][$item->getSku()];
                // }

                // Order Article Pre Order Shopee and Tokped : https://ruparupa.atlassian.net/browse/RRTICKET-6144
                $itemSku = "";
                preg_match_all('/([a-zA-Z0-9]+)-PRE/', strtoupper($item->getSku()), $match);
                if (count($match) >= 2 && isset($match[1][0])) {
                    $itemSku = $match[1][0];
                } else {
                    $itemSku = $item->getSku();
                }

                $productDetail[] = array(
                    "ID" => $idProductDetail,
                    "PRODUCTNO" => $itemSku,
                    "SALESPRICE" => intval($salesPrice),
                    "QUANTITY" => intval($item->getQtyOrdered()),
                    "UNIT" => $salesUom,
                    "CURRENCY" => "IDR", // For now we only using IDR for transaction
                    "STGE_LOC" => "1000",
                    //"SITE" => $warehouseSelection['value'] == 'H001' ? "O002" : "O001",
                    "SITE" => "O001",
                    "SITE_VENDOR" => "", //blank
                    "CONDTYPE" => "",
                    "INDENT" => "", //blank
                    "VENDOR" => "", //blank
                    "CONFIRMNO" => "", //blank
                    "PONUMBER" => $poNumber,
                    "POLEVELITEM" => $poLevelItem,
                    "SERIALNUMBER" => "" //blank

                );

                if(!empty((int)$this->invoice->SalesOrder->getGiftCardsAmount())) {
                    $unitPriceGc = $item->getGiftCardsAmount();
                    // zvo4 item price can't be higher than item unit price
                    if($unitPriceGc>$unitPrice){
                        $unitPriceGc = $unitPrice;
                    }
                    $salesProceGc = $unitPriceGc * $item->getQtyOrdered();
                    $productDetail[] = array(
                        "ID" => $idProductDetail,
                        "PRODUCTNO" => $itemSku,
                        "SALESPRICE" => intval($salesProceGc),
                        "QUANTITY" => $item->getQtyOrdered(),
                        "UNIT" => $salesUom,
                        "CURRENCY" => "IDR", // For now we only using IDR for transaction
                        "STGE_LOC" => "1000",
                        //"SITE" => $warehouseSelection['value'] == 'H001' ? "O002" : "O001",
                        "SITE" => "O001",
                        "SITE_VENDOR" => "", //blank
                        "CONDTYPE" => "ZV04",
                        "INDENT" => "", //blank
                        "VENDOR" => "", //blank
                        "CONFIRMNO" => "", //blank
                        "PONUMBER" => $poNumber,
                        "POLEVELITEM" => $poLevelItem,
                        "SERIALNUMBER" => "" //blank
                    );
                }
            }
        }
        return $productDetail;
    }

    public function getConsimentFlag($product_source = null)
    {
        $consimentFlag = "";
        if (!empty($product_source) && strpos($product_source, 'Konsinyasi') !== false ) {
            $consimentFlag = 1;
        }

        return $consimentFlag;
    }

    public function createSOB2B() {
        $nsq = new \Library\Nsq();
        $message = [
            "data" => json_encode($this->parameter),
            "message" => "createSOB2B"
        ];
        $nsq->publishCluster('transactionSap', $message);

        return true;
    }
}