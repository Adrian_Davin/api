<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 4/27/2017
 * Time: 2:00 PM
 */

namespace Library;


use GuzzleHttp\Client;
use Helpers\CartHelper;
use helpers\GeneralHelper;
use Helpers\ProductHelper;
use Helpers\RouteHelper;
use Models\Inspiration as InspirationModel;
use Models\ProductCategory;
use Phalcon\Db;

class Inspiration
{

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getAllInspiration($params = array())
    {
        $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';
        // search in redis first
        $redisKey = "inspiration_list_".$companyCode;
        $upperCaseType = '';

        if (isset($params['type']) && !empty(isset($params['type']))) {
            $upperCaseType = "INSPIRATION_".strtoupper($params['type'])."_".$companyCode;
            if (getenv($upperCaseType)) {
                $redisKey = "inspiration_".$params['type']."_".$companyCode;
            } else {
                $this->errorCode = "RR302";
                $this->errorMessages = "No inspiration has been found with type of ".$params['type'].".";
                return;
            }
        }

        $redis = new \Library\Redis();
        $redisCon = $redis::connect();
        $response = $redisCon->get($redisKey);

        if (!empty($response)) {
            $response = json_decode($response);

            return $this->filterScheduleCategory($response);
        } else {
            $insModel = new InspirationModel();
            $productCategoryModel = new ProductCategory();

            $newProduct = array();
            $inspirationDetails = array();
            $sku = array();
            $productDetails = array();
            $result = array();

           if (isset($params['type']) && !empty(isset($params['type']))) {
                // search inspirations in category
                $sql = "SELECT (SELECT title FROM product_category WHERE category_id = '" . getEnv($upperCaseType) . "') as 'parent_title',category_id,name,title,path,is_rule_based,filter_condition,rule_based_hero_products,thumbnail,thumbnail_mobile,position,include_in_menu,start_time,end_time,status,created_at,updated_at, meta_keywords FROM product_category WHERE path LIKE";
            
                // promotions inspiration
                $sql .= " '" . getEnv($upperCaseType) . "/%' AND";
            } else {
                if ($companyCode !== 'ODI') {
                     // search inspirations in category
                    $sql = "SELECT (SELECT title FROM product_category WHERE category_id = '" . getEnv('INSPIRATION_'.$companyCode) . "') as 'parent_title',category_id,name,title,path,is_rule_based,filter_condition,rule_based_hero_products,thumbnail,thumbnail_mobile,position,include_in_menu,start_time,end_time,status,created_at,updated_at, meta_keywords FROM product_category WHERE path LIKE";
                    
                    $sql .= " '" . getEnv('INSPIRATION_'.$companyCode) . "/%' AND";
                } else {
                     // search inspirations in category
                    $sql = "SELECT (SELECT title FROM product_category WHERE category_id = '2984') as 'parent_title',category_id,name,title,path,is_rule_based,filter_condition,rule_based_hero_products,thumbnail,thumbnail_mobile,position,include_in_menu,start_time,end_time,status,created_at,updated_at, meta_keywords FROM product_category WHERE path LIKE";
            
                    $sql .= " '2984/%' AND";
                }
            }
            $sql .= " STATUS = 10 AND include_in_menu IN (1, 2) AND level = 3 ORDER BY POSITION ASC ";
            if (!isset($params['no_limit'])) {
                $sql .= " LIMIT 6";
            }
            $productCategoryModel->useReadOnlyConnection();
            $result = $productCategoryModel->getDi()->getShared($productCategoryModel->getConnection())->query($sql);

            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $categoryIdList = $result->fetchAll();
            if (!empty($categoryIdList)) {
                // now each of category, search its products
                foreach ($categoryIdList as $key => $item) {
                    $inspirationDetails['inspiration_id'] = $item['category_id'];
                    $inspirationDetails['parent_title'] = $item['parent_title'];
                    $inspirationDetails['name'] = $item['name'];
                    $inspirationDetails['title'] = !empty($item['title']) ? $item['title'] : $item['name'];
                    $inspirationDetails['url_key'] = '';
                    $routeData = RouteHelper::getRouteData($item['category_id'],"category", "", $companyCode);

                    if(!empty($routeData)){
                        $inspirationDetails['url_key'] = $routeData['url_key'];
                    } else {
                        // check if it's custom category
                        $routeData = RouteHelper::getRouteData($item['category_id'],"custom_category", "", $companyCode);
                        if (!empty($routeData)) {
                            $inspirationDetails['url_key'] = $routeData['url_key'];
                        }
                    }

                    $inspirationDetails['small_banner'] = (empty($item['thumbnail']))?'':$item['thumbnail'];
                    $inspirationDetails['small_banner_mobile'] = (empty($item['thumbnail_mobile']))?'':$item['thumbnail_mobile'];
                    $inspirationDetails['products'] = "";
                    $inspirationDetails['priority'] = $item['position'];
                    if($item['include_in_menu'] == '1'){
                        $item['include_in_menu'] = '10';
                    }

                    $inspirationDetails['is_rule_based'] = intVal(isset($item['is_rule_based']) ? $item['is_rule_based'] : 0);
                    $inspirationDetails['path'] = $item['path'];
                    $inspirationDetails['filter_condition'] = $item['filter_condition'];
                    $inspirationDetails['rule_based_hero_products'] = $item['rule_based_hero_products'];
                    $inspirationDetails['show_home_page'] = $item['include_in_menu'];
                    $inspirationDetails['show_home_page_start_time'] = (empty($item['start_time'])) ? "" : $item['start_time'];
                    $inspirationDetails['show_home_page_end_time'] = (empty($item['end_time'])) ? "" : $item['end_time'];
                    $inspirationDetails['status'] = $item['status'];
                    $inspirationDetails['created_at'] = $item['created_at'];
                    $inspirationDetails['updated_at'] = $item['updated_at'];
                    $inspirationDetails['meta_keywords'] = $item['meta_keywords'];
                    $inspirationListTmp[] = $inspirationDetails;
                }

                $redisCon->set($redisKey,json_encode($inspirationListTmp));

                return $this->filterScheduleCategory($inspirationListTmp);
            }
            else{
                $this->errorCode = "RR302";
                $this->errorMessages = "No inspiration has been found";
                return;
            }
        }
    }

    public function filterScheduleCategory($categoryList = array()) 
    {
        $filteredCategories = array();
        foreach ($categoryList as $key => $item) {
            $item = (array)$item;
            if ($item['show_home_page'] == '10') {
                unset($item['show_home_page_start_time']);
                unset($item['show_home_page_end_time']);
                $filteredCategories[] = $item;
                continue;
            }

            // print_r($item);
            if ($item['show_home_page'] == '2' && isset($item['show_home_page_start_time']) && isset($item['show_home_page_end_time'])) {
                $nowDate = strtotime(date("Y-m-d H:i:s"));
                $startTime = strtotime($item['show_home_page_start_time']);
                $endTime = strtotime($item['show_home_page_end_time']);

                if (($nowDate >= $startTime) && ($nowDate <= $endTime)) {
                    unset($item['show_home_page_start_time']);
                    unset($item['show_home_page_end_time']);
                    $item['show_home_page'] = '10';
                    $filteredCategories[] = $item;
                }
            }
        }

        return array_values($filteredCategories);
    }

    // New Inspiration - sorted by created_at DESC
    public function getAllNewInspiration($params = array())
    {
        $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';
        // search in redis first
        $redisKey = "new_inspiration_list_" . $companyCode;
        $redis = new \Library\Redis();
        $redisCon = $redis::connect();

        $response = $redisCon->get($redisKey);

        if (!empty($response)) {
            $response = json_decode($response);
            return $response;
        } else {
            $insModel = new InspirationModel();
            $productCategoryModel = new ProductCategory();
            $productVariantModel = new \Models\ProductVariant();

            $newProduct = array();
            $inspirationDetails = array();
            $sku = array();
            $productDetails = array();
            $result = array();

            // search inspirations in category
            $sql = "SELECT category_id,name,path,is_rule_based,filter_condition,rule_based_hero_products,thumbnail,thumbnail_mobile,position,include_in_menu,status,created_at,updated_at, meta_keywords FROM product_category WHERE path LIKE";
            if ($companyCode !== 'ODI') {
              $sql .= " '" . getEnv('INSPIRATION_'.$companyCode) . "/%' AND";
            } else {
              $sql .= " '2984/%' AND";
            }
            $sql .= " STATUS = 10 AND include_in_menu = 1 AND level = 3 ORDER BY position ASC LIMIT 6 OFFSET 6";
            $productCategoryModel->useReadOnlyConnection();
            $result = $productCategoryModel->getDi()->getShared($productCategoryModel->getConnection())->query($sql);

            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $categoryIdList = $result->fetchAll();
            $currentCategory = ($companyCode == 'ODI') ? 'categories' : 'categories_' . $companyCode;
            if (!empty($categoryIdList)) {
                // now each of category, search its products
                foreach ($categoryIdList as $key => $item) {
                    $inspirationDetails['inspiration_id'] = $item['category_id'];
                    $inspirationDetails['title'] = $item['name'];
                    $routeData = RouteHelper::getRouteData($item['category_id'],"category", "", $companyCode);

                    if(!empty($routeData)){
                        $inspirationDetails['url_key'] = $routeData['url_key'];
                    } else {
                        // check if it's custom category
                        $routeData = RouteHelper::getRouteData($item['category_id'],"custom_category", "", $companyCode);
                        if (!empty($routeData)) {
                            $inspirationDetails['url_key'] = $routeData['url_key'];
                        }
                    }

                    $inspirationDetails['small_banner'] = (empty($item['thumbnail']))?'':$item['thumbnail'];
                    $inspirationDetails['small_banner_mobile'] = (empty($item['thumbnail_mobile']))?'':$item['thumbnail_mobile'];
                    $inspirationDetails['products'] = "";
                    $inspirationDetails['priority'] = $item['position'];
                    if($item['include_in_menu'] == '1'){
                        $item['include_in_menu'] = '10';
                    }

                    $inspirationDetails['is_rule_based'] = intVal(isset($item['is_rule_based']) ? $item['is_rule_based'] : 0);
                    $inspirationDetails['path'] = $item['path'];
                    $inspirationDetails['filter_condition'] = $item['filter_condition'];
                    $inspirationDetails['rule_based_hero_products'] = $item['rule_based_hero_products'];
                    $inspirationDetails['show_home_page'] = $item['include_in_menu'];
                    $inspirationDetails['status'] = $item['status'];
                    $inspirationDetails['created_at'] = $item['created_at'];
                    $inspirationDetails['updated_at'] = $item['updated_at'];
                    $inspirationDetails['meta_keywords'] = $item['meta_keywords'];

                    $inspirationList[] = $inspirationDetails;
                }

                $redisCon->set($redisKey,json_encode($inspirationList));
                return $inspirationList;
            }
            else{
                $this->errorCode = "RR302";
                $this->errorMessages = "No inspiration has been found";
                return;
            }
        }
    }

    public function getInspirationDetail($params = array())
    {   // not use anymore
        $insModel = new InspirationModel();
        $productCategoryModel = new ProductCategory();
        $productVariantModel = new \Models\ProductVariant();

        $newProduct = array();
        $inspirationDetails = array();
        $routeData = array();
        $sku = array();

        if(empty($params['inspiration_id'])) {
            $this->errorCode = "RR001";
            $this->errorMessages = "Empty inspiration_id";
            return;
        }

        // search inspirations in category
        $sql  = "SELECT name,meta_title,meta_keywords,meta_description,header_description,footer_description,description,thumbnail,position,include_in_menu,status,created_at,updated_at FROM product_category WHERE category_id=";
        $sql .= $params['inspiration_id'];
        $sql .=" AND STATUS = 10";
        $productCategoryModel->useReadOnlyConnection();
        $result = $productCategoryModel->getDi()->getShared($productCategoryModel->getConnection())->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $categoryIdList = $result->fetchAll();

        if(!empty($categoryIdList)) {
            // now query to collect sku(s)
            $sql  = "SELECT sku FROM product_variant WHERE product_id in (";
            $sql .= "SELECT product_id FROM ( SELECT product_id FROM product_2_category WHERE category_id=".$params['inspiration_id'];
            $sql .= " ORDER BY priority ASC) X)";
            $productVariantModel->useReadOnlyConnection();
            $result = $productVariantModel->getDi()->getShared($productVariantModel->getConnection())->query($sql);

            $result->setFetchMode(
                Db::FETCH_ASSOC
            );

            $productsSku = $result->fetchAll();

            $products = array();

            foreach ($productsSku as $id => $value){
                $product = ProductHelper::getFlatProductData($value['sku']);
                if(!empty($product)){
                    $products[] = ProductHelper::buildStructure($product);
                }
            }

            $inspirationDetails['inspiration_id'] = $params['inspiration_id'];
            $inspirationDetails['title'] = $categoryIdList[0]['name'];
            $inspirationDetails['meta_title'] = (empty($categoryIdList[0]['meta_title']))?'':$categoryIdList[0]['meta_title'];
            $inspirationDetails['meta_keywords'] = (empty($categoryIdList[0]['meta_keywords']))?'':$categoryIdList[0]['meta_keywords'];
            $inspirationDetails['meta_description'] = (empty($categoryIdList[0]['meta_description']))?'':$categoryIdList[0]['meta_description'];
            $inspirationDetails['header_description'] = (empty($categoryIdList[0]['header_description']))?'':$categoryIdList[0]['header_description'];
            $inspirationDetails['footer_description'] = (empty($categoryIdList[0]['footer_description']))?'':$categoryIdList[0]['footer_description'];
            $inspirationDetails['description'] = (empty($categoryIdList[0]['description']))?'':$categoryIdList[0]['description'];

            $routeData = RouteHelper::getRouteData($params['inspiration_id'],"category");

            if(!empty($routeData)){
                $inspirationDetails['url_key'] = $routeData['url_key'];
                $inspirationDetails['additional_header'] = (empty($routeData['additional_header']))?'':$routeData['additional_header'];
                $inspirationDetails['inline_script'] = (empty($routeData['inline_script']))?'':$routeData['inline_script'];
            }
            $inspirationDetails['small_banner'] = (empty($categoryIdList[0]['thumbnail']))?'':$categoryIdList[0]['thumbnail'];
            $inspirationDetails['products'] = $products;
            $inspirationDetails['priority'] = $categoryIdList[0]['position'];
            if($categoryIdList[0]['include_in_menu'] == '1'){
                $categoryIdList[0]['include_in_menu'] = '10';
            }
            $inspirationDetails['show_home_page'] = $categoryIdList[0]['include_in_menu'];
            $inspirationDetails['status'] = $categoryIdList[0]['status'];
            $inspirationDetails['created_at'] = $categoryIdList[0]['created_at'];
            $inspirationDetails['updated_at'] = $categoryIdList[0]['updated_at'];

            return $inspirationDetails;
        }
        else {
            $this->errorCode = "RR302";
            $this->errorMessages = "No inspiration has been found";
            return;
        }

    }

    public function deleteInspirationRedis($companyCode){
        /*
        * Clean up inspiration_list cache
        */
        $redis = new Redis();
        $redisCon = $redis::connect();
        $redisKey = "inspiration_list_" . $companyCode;
        $redisKeyNew = "new_inspiration_list_" . $companyCode;
        $response = $redisCon->del($redisKey);
        $responseNew = $redisCon->del($redisKeyNew);
    }

    public function deletePromoInspirationRedis($key, $companyCode){
        /*
        * Clean up inspiration_list cache
        */
        $redis = new Redis();
        $redisCon = $redis::connect();
        $redisKey = $key.$companyCode;
        $response = $redisCon->del($redisKey);
    }
}