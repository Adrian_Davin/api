<?php
namespace Library;

use Phalcon\Db;

class SalesOrderTp
{
    public function __construct()
    {
        
    }

    public function createSlackNotifCreditMemo($invoiceNo = '',$orderNo = '',$listSku = ''){
        $notificationText = "Alert : Terjadi refund invoice 1000DC dengan no invoice : ".$invoiceNo.", order no : ".$orderNo.", dan list sku : ".$listSku."";
        $params = [ 
            "channel" => $_ENV['1000DCREFUND_SLACK_CHANNEL'],
            "username" => $_ENV['OPS_SLACK_USERNAME'],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];
    
        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    public function CheckSalesOrderTpForSlackNotif($invoiceId = ''){
        $sql = "SELECT COUNT(odi) as totalOdi
                FROM sales_order_tp
                WHERE odi in (
                    SELECT order_no
                    from sales_order
                    where sales_order_id in (
                        SELECT sales_order_id
                        FROM sales_invoice
                        where invoice_id = $invoiceId
                    )
                )
                AND status = 'success'";
        $salesOrderTpModel = new \Models\SalesOrderTp();
        $result = $salesOrderTpModel->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            Db::FETCH_ASSOC
        );
        $totalRow = $result->fetchAll()[0]['totalOdi'];
        
        return $totalRow;
    }
}


