<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Library;

use Helpers\LogHelper;
use Helpers\ProductHelper,
    Phalcon\Db;

class InformaWebAPI {

  protected $errorCode;
  protected $errorMessages;

  /**
   * @return mixed
   */
  public function getErrorCode()
  {
      return $this->errorCode;
  }

  /**
   * @return mixed
   */
  public function getErrorMessages()
  {
      return $this->errorMessages;
  }

  public function __construct()
  {
      $this->customer = new \Models\Customer();
  }

  public function informaWebAPIEncrypt($payload = array()) {
      $apiWrapper = new APIWrapper(getenv('INFORMA_API'));
      $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/doencrypt");
      $apiWrapper->setParam($payload);

      if($apiWrapper->sendRequest("post")) {
        $apiWrapper->formatResponse();
      } else {
        $this->errorCode = 400;
        $this->errorMessages = "Error saat melakukan enkripsi payload";
        return false;
      }

      // if failed to register, return error
      if (!empty($apiWrapper->getError())) {
        $this->errorCode = 400;
        $this->errorMessages = $apiWrapper->getError();
        return false;
      }

      $response = $apiWrapper->getMessages();
      return $response;
  }

  public function informaRegisterCustomer($encryptedPayload = "") {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/registernewapp");
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
        $this->errorCode = 400;
        $this->errorMessages = "Error saat mengirim data customer";
        return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getMessage();
    return $response;
  }

  public function loginCustomer($encryptedPayload = "", $loginType = "member") {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/loginnewapp");
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
        $this->errorCode = 400;
        $this->errorMessages = "Error saat login customer";
        return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $responseRows = $apiWrapper->getData();

    return $responseRows;
  }

  public function forgetPasskey($encryptedPayload) {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/forgotpasswd");
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error saat forget passkey";
      return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function getCustomerData($data = array())
  {

    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/myprofile");
    $apiWrapper->setHeaders(array("MFSESSID" => $data['uid'].";".$data['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
        $this->errorCode = 400;
        $this->errorMessages = "Error saat get data customer";
        return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }
    
    $responseRows = $apiWrapper->getData();

    return $responseRows;
  }

  public function getMemberData($encryptedPayload = "")
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/showmembershipprofile");
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
        $this->errorCode = 400;
        $this->errorMessages = "Error when getMemberData";
        return false;
    }

    // if failed to register, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }
    
    $responseRows = $apiWrapper->getData();

    return $responseRows;
  }

  public function changePasskey($encryptedPayload = "",$auth = array())
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/changesetting");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
        $this->errorCode = 400;
        $this->errorMessages = "Error when change passkey";
        return false;
    }

    // if failed to changePasskey, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function editProfile($encryptedPayload = "", $auth = array())
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/changeprofile");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
        $this->errorCode = 400;
        $this->errorMessages = "Error when edit profile";
        return false;
    }

    // if failed to editProfile, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }
    
    $response = $apiWrapper->getMessage();
    return $response;
  }
  
  public function editMemberData($encryptedPayload = "")
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/updatemembershipprofile");
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
        $this->errorCode = 400;
        $this->errorMessages = "Error when editMemberData";
        return false;
    }

    // if failed to editMemberData, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }
    
    $response = $apiWrapper->getMessage();
    return $response;
  }

  public function getMemberVoucher($auth = array()) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/uservoucher");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get voucher";
      return false;
    }

    // if failed to getMemberVoucher, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();
    $response["totamount"] = count($response["arrvoucher"]);
    return $response;
  }

  public function getMemberListNotif($auth = array(), $page) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/listnotif/$page");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get list notif";
      return false;
    }

    // if failed to getMemberListNotif, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();

    if ($response) {
      $response["is_last_page"] = $response["nexturl"] == "";
      unset($response["nexturl"]);
    }

    return $response;
  }

  public function getTransactionHist($auth = array()) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/transactionhist");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get tx hist";
      return false;
    }

    // if failed to getTransactionHist, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if (empty($apiWrapper->getData())) {
      $this->errorCode = 404;
      $this->errorMessages = "Belum ada riwayat transaksi";
      return false;
    }

    $response = $apiWrapper->getData();
    return $response;
  }

  public function getTiering($auth = array()) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/showmembershipnewapp");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get tiering";
      return false;
    }

    // if failed to getTiering, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();
    if ($response) {
      $responseSplit = explode(" ", $response["point"]);
      if (count($responseSplit) > 0) {
        $response["point"] = intval($responseSplit[0]);
        $response["point_amount"] = $response["point"] * 2500;
      }
    }

    return $response;
  }

  public function getDigitalReceipt($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/receiptimage");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    $apiWrapper->setParam($encryptedPayload);


    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get digital receipt";
      return false;
    }

    // if failed to getTiering, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();
    if (isset($response[0]["CONTENT"])) {
      $response[0]["CONTENT"] = str_replace("\r","\r\n",$response[0]["CONTENT"]);
      $response[0]["CONTENT"] = str_replace("\r\n\n","\r\n",$response[0]["CONTENT"]);
      $response[0]["CONTENT"] = str_replace("\u0000","",$response[0]["CONTENT"]);
    }
    return $response;
  }


  public function logoutUser($auth = array()) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/logout");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get logout";
      return false;
    }

    // if failed to logoutUser, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function transactionDeliveryList($auth = array(), $receiptNo) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/listtrx_delivery/$receiptNo");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get transaction detail";
      return false;
    }

    // if failed to get transactionDeliveryList, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // if failed to get transactionDeliveryList, return error
    if (empty($apiWrapper->getData())) {
      $this->errorCode = 404;
      $this->errorMessages = "Pengiriman tidak ditemukan";
      return false;
    } 

    $response = $apiWrapper->getData();
    return $response;
  }

  public function deliveryDetail($auth = array(), $noTrans, $jobId, $siteId) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/detaildelivery/$noTrans/$jobId/$siteId");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get delivery detail";
      return false;
    }

    // if failed to logoutUser, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();
    return $response;
  }

  public function postSatisfactionRate($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/installrate");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post satisfaction rate";
      return false;
    }

    // if failed to postSatisfactionRate, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function verifyEmail($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/sendemailmembershipverification");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post verify email";
      return false;
    }

    // if failed to verifyEmail, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function informaSendChangePhoneOTP($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/sendotp_changehpmembership");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post send change phone otp";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function informaSubmitChangePhoneOTP($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/submitotp_changehpmembership");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post submit change phone otp";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];
    
    return $response;
  }
  
  public function verifyPhone($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/sendotp_verifyhpmembership");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post verify phone";
      return false;
    }

    // if failed to verifyEmail, return error
    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }
    
    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
      "otp" => $apiWrapper->getOTP(),
    ];

    return $response;
  }

  public function informaSendChangeEmailOTP($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/sendotp_changeemailmembership");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post send change email otp";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }
    
    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
      "otp" => $apiWrapper->getOTP(),
    ];

    return $response;
  }

  public function informaSubmitChangeEmailOTP($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/submitotp_changeemailmembership");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post submit change email otp";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];
    
    return $response;
  }

  public function informaVerifyPhoneSubmitOTP($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/submitotp_verifyhpmembership");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post submit verify phone otp";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    
    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function informaRedeemVoucherList($auth = array()) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/listvoucher");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get redeem voucher list";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();
    return $response;
  }

  public function informaRedeemVoucherSendOTP($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/sendOTP_exchangepoint");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post redeem voucher send otp";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }
    
    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
      "otp" => $apiWrapper->getOTP(),
    ];

    return $response;
  }

  public function informaRedeemVoucherSubmitOTP($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/redeempointOTP");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post redeem voucher submit otp";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function informaChangeProfileEmail($auth = array(), $payload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/updateemailprofile");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($payload);

    if(!$apiWrapper->sendInforma("postRaw")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post change profile email";
      return false;
    }

    if ($apiWrapper->getResult() != "True") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessage();
      return false;
    }

    $response = $apiWrapper->getMessage();
    return $response;
  }

  public function installationList($auth = array(), $receiptNo) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/listtrx_install/$receiptNo");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get installation list";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    // if failed to get installationList, return error
    if (empty($apiWrapper->getData())) {
      $this->errorCode = 404;
      $this->errorMessages = "Instalasi tidak ditemukan";
      return false;
    } 

    $response = $apiWrapper->getData();
    return $response;
  }

  public function installationDetail($auth = array(), $noTrans, $jobId) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/detailinstallation/$noTrans/$jobId");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get installation detail";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();
    return $response;
  }

  public function informaPayWithPoint($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/insertotp");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post informaPayWithPoint";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = [
      "result" => $apiWrapper->getResult(),
      "message" => $apiWrapper->getMessage(),
    ];

    return $response;
  }

  public function informaGetReceiptList($auth = array(), $page) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/listreceipt/$page");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get informaGetReceiptList";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();

    if ($response) {
      $response["is_last_page"] = $response["nexturl"] == "";
      unset($response["nexturl"]);

      if ($response["rating"]["israte"] == 1 && $response["rating"]["haveInstallation"]) {
        $response["rating"]["can_review"] = 1;
      } else {
        $response["rating"]["can_review"] = 0;
      }
    }

    return $response;
  }

  public function informaGetPointHistory($payload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("/api/check_point_history");
    $apiWrapper->setHeaders(array("token" => getenv('INFORMA_MEMBERSHIP_API_TOKEN')));
    $apiWrapper->setParam($payload);

    if(!$apiWrapper->sendInforma("postRawJson")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get informaGetPointHistory";
      return false;
    }

    $response = $apiWrapper->getData();

    if (!isset($response["is_ok"]) || $response["is_ok"] != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessage();
      return false;
    }

    if ($response) {
      $response["is_last_page"] = is_null($response["next_page"]);
    }

    return $response;
  }

  public function informaGetMembershipData($payload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("/api/get_cust_data");
    $apiWrapper->setHeaders(array("token" => getenv('INFORMA_MEMBERSHIP_API_TOKEN')));
    $apiWrapper->setParam($payload);

    if(!$apiWrapper->sendInforma("postRawJson")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get informaGetMembershipData";
      return false;
    }

    $response = $apiWrapper->getData();
    
    if (!isset($response["is_ok"]) || $response["is_ok"] != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessage();
      return false;
    }

    return $response;
  }

  public function informaGetCheckEmail($payload) {
    $apiWrapper = new APIWrapperInforma(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("/api/Check_Existing_Email");
    $apiWrapper->setHeaders(array("token" => getenv('INFORMA_MEMBERSHIP_API_TOKEN')));
    $apiWrapper->setParam($payload);

    if(!$apiWrapper->sendInforma("postRawJson")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get informaGetCheckEmail";
      return false;
    }

    $response = $apiWrapper->getData();
    
    if (!isset($response["rows"]) || count($response["rows"]) < 1) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessage();
      return false;
    }

    return $response["rows"][0];
  }

  public function informaPostConnectMember($auth, $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/checkmembershipnewapp");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post informaPostConnectMember";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();

    return $response;
  }

  public function informaCheckReceiptNo($auth = array(), $encryptedPayload) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/checkreceiptno");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));
    $apiWrapper->setParam($encryptedPayload);

    if(!$apiWrapper->sendInforma("post")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when post informaCheckReceiptNo";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();

    return $response;
  }

  public function informaInstallationRoute($auth = array(), $notrans, $jobid) 
  {
    $apiWrapper = new APIWrapperInforma(getenv('INFORMA_API'));
    $apiWrapper->setEndPoint("/mainapp/api/customerapi_v2/installroute/$notrans/$jobid");
    $apiWrapper->setHeaders(array("MFSESSID" => $auth['uid'].";".$auth['sessionid']));

    if(!$apiWrapper->sendInforma("get")) {
      $this->errorCode = 400;
      $this->errorMessages = "Error when get informaInstallationRoute";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    $response = $apiWrapper->getData();

    return $response;
  }

  public function informaPwpSendOTP($encryptedPayload) 
  {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("/api/send_wa");
    $apiWrapper->setParam(array("p_encrypt" => $encryptedPayload));

    if (!$apiWrapper->sendRequest("post")) {
        $this->errorCode = 400;
        $this->errorMessages = "Error occurs when trying to send whatsapp";

        return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();

      return false;
    }

    // check is_ok
    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages();

      return false;
    }

    return true;
  }

  public function insertPointInforma($payload = array()) {
    $apiWrapper = new APIWrapper(getenv('MEMBERSHIP_API_KL'));
    $apiWrapper->setEndPoint("/api/insert_point");
    $apiWrapper->setParam($payload);

    if (!$apiWrapper->sendRequest("post")) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Terjadi kesalahan saat menambahkan/memotong poin";
      return false;
    }

    if (!empty($apiWrapper->getError())) {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getError();
      return false;
    }

    if ($apiWrapper->getIsOk() != "true") {
      $this->errorCode = 400;
      $this->errorMessages = $apiWrapper->getMessages() ? $apiWrapper->getMessages() : "Terjadi kesalahan saat menambahkan/memotong poin";
      return false;
    }

    return true;
  }

}
