<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 3/17/2017
 * Time: 2:36 PM
 */

namespace Library;

use \Helpers\CartHelper;
use \Helpers\LogHelper;
use \Library\SalesOrder as SalesOrderLib;


class Marketing
{
    protected $voucher_list = array();
    protected $sku_bundling = array();

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @return array
     */
    public function getVoucherList()
    {
        return $this->voucher_list;
    }

    /**
     * @return array
     */
    public function getSkuBundling()
    {
        return $this->sku_bundling;
    }

    /**
     * @param array $sku_bundling
     */
    public function setSkuBundling($sku_bundling)
    {
        $this->sku_bundling = $sku_bundling;
    }

    /**
     * @return int
     */
    public function getTotalQtyItem()
    {
        return $this->total_qty_item;
    }

    /**
     * @param int $total_qty_item
     */
    public function setTotalQtyItem($total_qty_item)
    {
        $this->total_qty_item = $total_qty_item;
    }

    public function generateVoucher($parameters = array())
    {
        set_time_limit(10800);

        // Temp
        \Helpers\LogHelper::log("generate_voucher", "Request Payload: " .json_encode($parameters));

        if(empty($parameters['suffix'])) {
            $parameters['suffix'] = "";
        }

        if(empty($parameters['prefix'])) {
            $parameters['prefix'] = "";
        }


        if($parameters['length'] <= 0){

            $myString = $parameters['prefix'].$parameters['suffix'];
            $this->voucher_list[] = $myString;
            return true;

        }
    

        switch ($parameters['format']) {
            case "alphanum" : $this->generateAlphaNumVoucher($parameters['qty'],$parameters['length'],$parameters['prefix'],$parameters['suffix']);break;
            case "alpha" : $this->generateAlphaVoucher($parameters['qty'],$parameters['length'],$parameters['prefix'],$parameters['suffix']);break;
            case "num" : $this->generateNumVoucher($parameters['qty'],$parameters['length'],$parameters['prefix'],$parameters['suffix']);break;
            case "order" : $this->generateOrderVoucher($parameters['qty'],$parameters['prefix']);break;
            default :
                $this->errorCode = "RR101";
                $this->errorMessages[] = "Voucher format not found";
                return false;
                break;
        }

        return true;
    }

    public function generateAlphaNumVoucher($qty = 0, $length = 10, $prefix = "", $suffix = "")
    {
        $totalPossible = pow(36, $length);
        if ($qty/$totalPossible >= 0.6) {
            $this->errorCode = "RR101";
            $this->errorMessages[] = "Silahkan menambahkan code length";
            return;
        }
        $i = 0;
        do {
            $myRandom = strtoupper(\Phalcon\Text::random(\Phalcon\Text::RANDOM_ALNUM , $length));

            // Validation string can not contain Zero and O to eliminate confusion of user
            if(strpos($myRandom, "0") !== false || strpos($myRandom, "O") !== false) {
                continue;
            }

            $myString = $prefix.$myRandom.$suffix;

            // Make sure our string don't have duplicate entry
            if(!in_array($myString,$this->voucher_list)) {
                $this->voucher_list[] = $myString;
                $i++;
            }
        } while ($i < $qty);
       

        return;
    }

    public function generateAlphaVoucher($qty = 0, $length = 10, $prefix = "", $suffix = "")
    {
        $i = 0;
        do {
            $myRandom = strtoupper(\Phalcon\Text::random(\Phalcon\Text::RANDOM_ALPHA , $length));

            // Validation string can not contain Zero and O to eliminate confusion of user
            if(strpos($myRandom, "0") !== false) {
                continue;
            }

            $myString = $prefix.$myRandom.$suffix;

            // Make sure our string don't have duplicate entry
            if(!in_array($myString,$this->voucher_list)) {
                $this->voucher_list[] = $myString;
                $i++;
            }
        } while ($i < $qty);

        return;
    }

    public function generateNumVoucher($qty = 0, $length = 10, $prefix = "", $suffix = "")
    {
        $i = 0;
        do {
            $myRandom = strtoupper(\Phalcon\Text::random(\Phalcon\Text::RANDOM_NOZERO , $length));
            $myString = $prefix.$myRandom.$suffix;

            // Make sure our string don't have duplicate entry
            if(!in_array($myString,$this->voucher_list)) {
                $this->voucher_list[] = $myString;
                $i++;
            }
        } while ($i < $qty);

        return;
    }

    public function generateOrderVoucher($qty = 0, $prefix = ""){
        $voucherPrefix = substr($prefix,0,12);

        for ($i = 1 ; $i <= $qty ; $i++) {
            if ($i < 10){
                $myString = $voucherPrefix."0".$i;
            }else {
                $myString = $voucherPrefix.$i;
            }
            if(!in_array($myString,$this->voucher_list)) {
                $this->voucher_list[] = $myString;                
            }
        }

        return;
    }

    /**

     * @param array $parameters
     * @return bool
     */
    public function validateGeneratedVoucher($parameters = array())
    {
        if(!empty($this->errorCode)) {
           return false;
        }

        if(empty($this->voucher_list)) {
            $this->errorCode = "RR101";
            $this->errorMessages[] = "Voucher not found";
            return false;
        }

        $salesRuleVoucherModel = new \Models\SalesruleVoucher();

        // Temp
        // Makesure there are no duplicate entry in database
        // set variable loop for circuit breaker
        $loop = 0;
        $qtyRequest = $parameters['qty'];
        do {
            $query = $salesRuleVoucherModel::query();
            $query->inWhere("voucher_code", $this->voucher_list);
            // $query->andWhere("company_code = '" . $parameters['company_code'] . "'");
            // $query->andWhere("status > -1");
            $result = $query->execute();
            if($result->count() > 0 && ($parameters['length'] > 0)) {
                $tempArray = array_flip($this->voucher_list);
                
                foreach ($result as $voucher) {
                    unset($tempArray[$voucher->voucher_code]);
                }
                $this->voucher_list = array_flip($tempArray);
                $parameters['qty'] = $result->count();
                $this->generateVoucher($parameters);
            }else{
                break;
            }
            $loop++;
        } while($result->count() > 0 && $loop < 3);
        
        /**
         * Just a safety net we not process duplicate data
         */
        
        if((($result->count() > 0) && ($parameters['length'] > 0)) || $loop == 3) {
            
            $this->errorCode = "RR102";
            $this->errorMessages[] = "Terdapat voucher yang sama, silahkan menambahkan code length";
            return false;
        }

        return true;
    }

    /**
     * @todo : improve, sending email report
     */
    public function saveGeneratedVoucher($params = array())
    {
        if(!empty($this->errorCode)) {
            return false;
        }

        $salesRuleVoucherModel = new \Models\SalesruleVoucher();
        $salesRuleVoucherModel->setFromArray($params);
        $saveStatus = $salesRuleVoucherModel->bulkInsert($this->voucher_list);
        if(!$saveStatus) {
            $this->errorCode = $salesRuleVoucherModel->getErrorCode();
            $this->errorMessages[] = $salesRuleVoucherModel->getErrorMessages();
            return false;
        }

        return true;
    }

    public function getMarketingPromotion($promotion_type = array())
    {
        $MarketingPromotionCache = \Library\Redis\MarketingPromotion::getPromotionCache(date('Ymd'), $promotion_type);

        return $MarketingPromotionCache;
    }

    /**
     * @param string $voucher_code
     * @param string $company_code
     * @return array|bool
     */
    public function getVoucherDetail($voucher_code = "", $company_code = "ODI")
    {
        if(empty($voucher_code)) {
            $this->errorCode = "RR101";
            $this->errorMessages[] = "Voucher Code tidak boleh kosong";
            return false;
        }

        // Check if voucher code is exist
        $salesRuleVoucherModel = new \Models\SalesruleVoucher();
        $voucherResult = $salesRuleVoucherModel->findFirst([
            'voucher_code = :voucher_code: AND status >= :status:',
            'bind' => [
                'voucher_code' => $voucher_code,
                'status' => 10
            ]
        ]);

        if(!$voucherResult) {
            $this->errorCode = "RR100";
            $this->errorMessages['type'] = "global";
            $this->errorMessages['value'] = "";
            $this->errorMessages['message'] = "Voucher tidak diketemukan atau sudah berakhir.";
            return false;
        }

        // Check if voucher is active or not (not expired)
        if(strtotime($voucherResult->expiration_date) < time()) {
            $this->errorCode = "RR100";
            $this->errorMessages['type'] = "global";
            $this->errorMessages['value'] = "";
            $this->errorMessages['message'] = "Maaf, Promo voucher ini sudah berakhir.";
            return false;
        }

        if($voucherResult->status < 0) {
            $this->errorCode = "RR100";
            $this->errorMessages['type'] = "global";
            $this->errorMessages['value'] = "";
            $this->errorMessages['message'] = "Voucher tidak diketemukan atau sudah berakhir.";
            return false;
        }

        // if($voucherResult->limit_used > 0 && $voucherResult->times_used >= $voucherResult->limit_used) {
        //     $this->errorCode = "RR100";
        //     $this->errorMessages['type'] = "global";
        //     $this->errorMessages['value'] = "";
        //     $this->errorMessages['message'] = "Maaf, limit voucher sudah terpakai semua.";
        //     return false;
        // }

        // if everything is ok send back to front end
        return $voucherResult->getDataArray();
    }

    public function maxUsageValidation($rules = [], $customer_id = 0, $voucher = [])
    {
        if(empty($rules)) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Rule not found";
            return false;
        }

        $voucherUsageModel = new \Models\SalesruleCustomer();
        $param = [
            "conditions" => "rule_id = '". $rules['rule_id'] ."'"
        ];

        if(!empty($voucher)) {
            $param['conditions'] = $param['conditions']. " AND voucher_code = '". $voucher['voucher_code']."'";
        }

        if(!empty($customer_id)) {
            $param['conditions'] = $param['conditions']. " AND customer_id = '" . $customer_id."'";
        }

        $voucherUsageResult = $voucherUsageModel::findFirst($param);

        if(!empty($rules['uses_per_customer']) && $voucherUsageResult && $voucherUsageResult->times_used >= $rules['uses_per_customer'] && $rules['uses_per_customer_frequence'] == 0) {
            $this->errorCode = "RR100";

            if(empty($voucher)) {
                $this->errorMessages[] = "Exceed max usage of promotion";
            } else {
                $this->errorMessages[] = "Exceed max usage of voucher : " . $voucher['voucher_code'];
            }

            /*$this->errorMessages['type'] = "global";
            $this->errorMessages['value'] = "";
            if(empty($voucher)) {
                $this->errorMessages['message'] = "Exceed max usage of promotion";
            } else {
                $this->errorMessages['message'] = "Exceed max usage of voucher : " . $voucher['voucher_code'];
            }*/

            return false;
        }else if($rules['uses_per_customer_frequence'] > 0) {
            $salesOrderLib = new SalesOrderLib;
            $totalFrequence = $salesOrderLib->getTotalUsedPerCustomerWithDays($rules['rule_id'],$customer_id);

            if($rules['uses_per_customer']>$totalFrequence){
                return [];
            }else{
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Exceed max usage of promotion per day";
                return false;
            }
        }else if(empty($voucherUsageResult)) {
            return [];
        }

        return $voucherUsageResult->toArray();
    }

    public function maxLimitUsedValidation($rules = [])
    {
        if(empty($rules)) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Rule not found";
            return false;
        }

        // Calculate total limit_used
        // $ruleUsageModel = new \Models\SalesOrder();
        // $param = [
        //     'cart_rules like \'%"rule_id":"'.$rules['rule_id'].'"%\''
        // ];

        // $ruleUsageResult = $ruleUsageModel::find($param);
        // if($ruleUsageResult){
        //     $totalUsed = count($ruleUsageResult->toArray());
        
        //     if(!empty($rules['limit_used']) && $ruleUsageResult && $totalUsed >= $rules['limit_used']) {
        //         $this->errorCode = "RR100";
        //         $this->errorMessages[] = "Exceed max usage of promotion";
        //         return false;
        //     } else {
        //         return true;
        //     }
        // }else{
        //     return true;
        // }

        $salesOrderLib = new SalesOrderLib;
        $totalFrequence = $salesOrderLib->getTotalUsedPerRule($rules['rule_id']);

        if($rules['limit_used']>$totalFrequence){
            return true;
        }else{
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Exceed max usage of promotion per day";
            return false;
        }
        
    }

    public function maxUsedFrequenceValidation($rules = [], $customer_id)
    {
        if(empty($rules)) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Rule not found";
            return false;
        }

        // Calculate total limit_used
        // $now = date('Y-m-d');
        // $ruleUsageModel = new \Models\SalesOrder();
        // $ruleUsageResult = $ruleUsageModel::find(
        //     '( cart_rules like \'%"rule_id":"'.$rules['rule_id'].'"%\' or gift_cards like \'%"rule_id":"'.$rules['rule_id'].'"%\' ) and customer_id = '.$customer_id.' and ( created_at between "'.$now.' 00:00:00" and "'.$now.' 23:59:59")'
        // );

        // if($ruleUsageResult){
        //     $totalUsed = count($ruleUsageResult->toArray());
        
        //     if(!empty($rules['uses_per_customer']) && $ruleUsageResult && $totalUsed >= $rules['uses_per_customer']) {
        //         $this->errorCode = "RR100";
        //         $this->errorMessages[] = "Exceed max usage of promotion per day";
        //         return false;
        //     } else {
        //         return true;
        //     }
                
        // }else{
        //     return true;
        // }

        $salesOrderLib = new SalesOrderLib;
        $totalFrequence = $salesOrderLib->getTotalUsedPerCustomerWithDays($rules['rule_id'],$customer_id);

        if($rules['uses_per_customer']>$totalFrequence){
            return true;
        }else{
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Exceed max usage of promotion per day";
            return false;
        }
        
    }

    private function _checkConditionStatus($operator,$value,$dataValue)
    {
        $value = trim($value); // sometime we got space on value, trim it

        // Let's make everything lower case, we not looking for some case sensitive comparation
        $value = strtolower($value);
        $dataValue = strtolower($dataValue);

        $matchCondition = false;
        if($operator == '()') {
            $findIn = explode(",",$value);
            if(in_array($dataValue,$findIn)) {
                $matchCondition = true;
            }
        } else if($operator == '!()') {
            $findIn = explode(",",$value);
            if(!in_array($dataValue,$findIn)) {
                $matchCondition = true;
            }
        } else if($operator == '<=' && $dataValue <= $value) {
            $matchCondition = true;
        } else if($operator == '<' && $dataValue < $value) {
            $matchCondition = true;
        } else if($operator == '>=' && $dataValue >= $value) {
            $matchCondition = true;
        } else if($operator == '>' && $dataValue > $value) {
            $matchCondition = true;
        } else if( $operator == '==' &&  $dataValue == $value) {
            $matchCondition = true;
        } else if( $operator == '!=' &&  $dataValue != $value) {
            $matchCondition = true;
        } else if( $operator == '!=' &&  $dataValue == $value) {
            $matchCondition = false;
        } else if( $operator == '%' && strpos(strtolower($dataValue),strtolower($value)) !== false && empty($this->sku_bundling)) {
            $matchCondition = true;
        } else if( $operator == '==' && !empty($this->sku_bundling)){ // SKU Bundling porpose
            if(in_array($dataValue,$this->sku_bundling)) {
                $matchCondition = true;
            }
        } else if($operator == '^') {
            if (strpos($dataValue, $value) === 0) {
                $matchCondition = true;
            }
        } else if($operator == '^()') {
            $findIn = explode(",",$value);
            foreach($findIn as $haystack) {
                if (strpos($haystack, $value) === 0) {
                    $matchCondition = true;
                    break;
                }   
            }
        }

        return $matchCondition;
    }

    /**
     * @param $operator Condition operator
     * @param $value Value of Operator
     * @param $dataValue Data value to check
     * @param $level
     * @return bool
     */
    private function _checkArrayConditionStatus($operator,$value,$dataValue,$level = 0)
    {
        $level++;
        $matchCondition = false;

        if(empty($dataValue)) {
            $matchCondition = $this->_checkConditionStatus($operator,$value,"");
        } else {
            foreach ($dataValue as $currentValue) {
                if(empty($currentValue)) {
                    $matchCondition = $this->_checkConditionStatus($operator,$value,"");
                } else {
                    if(is_array($currentValue)) {
                        $matchCondition = $this->_checkArrayConditionStatus($operator,$value,$currentValue,$level);
                    } else {
                        $matchCondition = $this->_checkConditionStatus($operator,$value,$currentValue);
                    }
                }

                if($matchCondition == true && $operator != "!=" && $operator != "!()") {
                    break;
                } else if($matchCondition == false && ($operator == "!=" || $operator == "!()")) {
                    break;
                }
            }
        }

        return $matchCondition;
    }

    /**
     * Checking for sales rule can be applied or not
     *
     * return data have 2 data, 1st data is key, 2nd is aggregator
     * @param array $conditions
     * @param array $data (cart_data)
     * @return array|mixed
     */
    public function marketingConditionCheck($conditions = [], $data = [])
    {
        if(empty($conditions)) {
            return [true,''];
        }

        // Set SKU Bundling values
        foreach($conditions as $condition){
            if($condition['attribute'] == "sku_bundling"){
                $this->sku_bundling = explode(',',str_replace(' ','',strtolower($condition['value'])));
                break;
            }
        }        
        
        $globalConditionsStatus = [];
        foreach ($conditions as $condition) {
            if(empty($condition)) {
                continue;
            }

            // exclude sku bundling condition
            if($condition['attribute'] == 'sku_bundling'){
                continue;
            }

            $localConditionCheck = [];
            $conditionAtribute = explode(".",$condition['attribute']);         

            /**
             * Check marketing condition on first level
             */
            $resultValue = $this->_getDataValue($conditionAtribute, $data);
            if(count($conditionAtribute) > 1) {
                if(is_array($resultValue)) {
                    $isValidCondition = $this->_checkArrayConditionStatus($condition['operator'],$condition['value'],$resultValue);
                } else {
                    $isValidCondition = $this->_checkConditionStatus($condition['operator'],$condition['value'],$resultValue);
                }
            } else {
                \Helpers\LogHelper::log('debug_625', '/app/library/Marketing.php line 626 '.$data['reserved_order_no'], 'debug');
                $isValidCondition = $this->_checkConditionStatus($condition['operator'],$condition['value'],$resultValue);
            }

            if(isset($condition['conditions'])) {
                if($condition['attribute'] == 'sku'){
                    if($data['sku'] == $condition['value']){
                        \Helpers\LogHelper::log('debug_625', '/app/library/Marketing.php line 633', 'debug');
                        $localConditionCheck = $this->marketingConditionCheck($condition['conditions'], $data);
                    }
                }else{
                    \Helpers\LogHelper::log('debug_625', '/app/library/Marketing.php line 637', 'debug');
                    $localConditionCheck = $this->marketingConditionCheck($condition['conditions'], $data);
                }
            }

            $aggregator = (isset($condition['aggregator']) && count($conditions) > 1) ? $condition['aggregator'] : '';
            if($aggregator != '') {
                $aggregator = (strtolower($aggregator) == 'or') ? '||' : '&&';
            }

            if(!empty($localConditionCheck)) {
                $thisStatus = ($isValidCondition == true) ? 1 : 0;
                $localConditon = ($localConditionCheck[0] == true) ? 1 : 0;
                $string = 'if(';
                $string .= $thisStatus . ' && ' . $localConditon;
                $string .= ') { return true; } else {';
                $string .= ' return false; }';
                $isValidCondition = eval($string);
            }

            $globalConditionsStatus[] =  [$isValidCondition,$aggregator];
        }

        $totalConditons = count($globalConditionsStatus);
        if($totalConditons > 1) {
            $string = 'if(';
            for($i=0;$i<$totalConditons;$i++) {
                $operator = isset($globalConditionsStatus[$i+1]) ? $globalConditionsStatus[$i+1][1] : '';
                $thisStatus = ($globalConditionsStatus[$i][0] == true) ? 1 : 0;
                $string .= $thisStatus . ' ' . $operator. ' ';
            }
            $string .= ') { return true; } else {';
            $string .= ' return false; }';
            $status = eval($string);
            return [$status,''];
        } else if(!empty($globalConditionsStatus)) {
            return $globalConditionsStatus[0];
        } else {
            return [false,''];
        }
    }

    private function _isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    private function _getDataValue($conditionAtribute = [], $data = [], $index = 0)
    {
        $attribute = $conditionAtribute[$index];

        if($attribute == "supplier_alias") {
            $attribute = $attribute."_original";
        }

        if($attribute == 'cart_rules') {
            $value = $this->_getApplidRulesIds($data);
            return $value;
        }

        if(!isset($data[$attribute])) {
            return null;
        }

        $isJson = \Helpers\ParserHelper::isJSON($data[$attribute]);
        if($isJson) {
            $data[$attribute] = json_decode($data[$attribute],true);
        }

        $isAssoc = false;
        if(is_array($data[$attribute])) {
            $isAssoc = $this->_isAssoc($data[$attribute]);
        }

        if($index < count($conditionAtribute) - 1 && (!is_array($data[$attribute]) || $isAssoc)) {
            $index++;
            $value = $this->_getDataValue($conditionAtribute,$data[$attribute],$index);

        } else if(!$isAssoc && is_array($data[$attribute])) {
            $index++;
            $batchData = [];

            foreach ($data[$attribute] as $currentData) {
                if (is_array($currentData)) {
                    $batchData[] = $this->_getDataValue($conditionAtribute,$currentData,$index);
                } else {
                    $batchData[] = $currentData;
                }
            }

            $value = [];
            if( (count($conditionAtribute) - 1) == $index) {
                $value = $batchData;
            } else if(!empty($batchData)){
                $value = $batchData;
            }
        } else {
            $value = $data[$attribute];
        }

        return $value;
    }

    private function _getApplidRulesIds($data = [])
    {
        $rules_ids = [];

        /**
         * Check global cart_rules
         */
        if(!empty($data['cart_rules'])) {
            $cartRules = json_decode($data['cart_rules'],true);
            if(!empty($cartRules)) {
                foreach ($cartRules as $cartRule) {
                    if(!in_array($cartRule['rule_id'],$rules_ids)) {
                        $rules_ids[] = $cartRule['rule_id'];
                    }
                }
            }
        }

        /**
         * Check for item applied cart rules
         */
        if(!empty($data['items'])) {
            foreach ($data['items'] as $item) {
                $itemRules = json_decode($item['cart_rules'],true);
                if(!empty($itemRules)) {
                    foreach ($itemRules as $itemRule) {
                        if(!in_array($itemRule['rule_id'],$rules_ids)) {
                            $rules_ids[] = $itemRule['rule_id'];
                        }
                    }
                }
            }
        }
        if(!empty($data['gift_cards'])) {
            $giftCards = json_decode($data['gift_cards'],true);
            foreach ($giftCards as $giftCard) {
                if(!in_array($giftCard['rule_id'],$rules_ids)) {
                    $rules_ids[] = $giftCard['rule_id'];
                }
            }
        }

        return $rules_ids;

    }

    /**
     * Checking if there are rule in condition to restrict certain rule_id
     * @param $conditions
     * @return array
     */
    private function __getSalesRulesInCondition($conditions)
    {
        $canAppliedSalesRule = [];
        foreach ($conditions as $condition) {
            $attribute = explode(".",$condition['attribute']);
            if($attribute[count($attribute) - 1] == 'rule_id') {
                $cart_ids = explode(",",str_replace(" ","",$condition['value']));
                $canAppliedSalesRule = array_merge($canAppliedSalesRule, $cart_ids);
            }

            if(isset($condition['conditions'])) {
                $cart_ids = $this->__getSalesRulesInCondition($condition['conditions']);
                $canAppliedSalesRule = array_merge($canAppliedSalesRule, $cart_ids);
            }
        }

        return $canAppliedSalesRule;
    }

    public function canCombineCheck($data = [], $rule = [])
    {
        if(empty($data)) {
            return false;
        }

        $canAppliedSalesRule = $this->__getSalesRulesInCondition($rule['conditions']);

        $isJson = \Helpers\ParserHelper::isJSON($data['gift_cards']);
        $gift_cards = [];
        if($isJson) {
            $gift_cards = json_decode($data['gift_cards'], true);
        }

        $isJson = \Helpers\ParserHelper::isJSON($data['cart_rules']);
        $cart_rules = [];
        if($isJson) {
            $cart_rules = json_decode($data['cart_rules'],true);
        }

        // By default we set all rule globaly can be combine
        $can_combine = true;
        $ruleIds = [];
        if(empty($rule['global_can_combine'])) {
            $can_combine = true;
            
            /**
             * Check can combine giftcard
             */
            if(!empty($gift_cards) && ($rule['global_can_combine']=="0")) {
                foreach ($gift_cards as $gift_card) {
                    
                    /**
                     * If current gift card is same with current applied rule, it's can be combine
                     */
                    if(in_array($gift_card['rule_id'],$canAppliedSalesRule) || $gift_card['global_can_combine'] == 1) {
                        continue;
                    }else if (($gift_card['can_combine'] == 1 && $rule['can_combine'] == 1)  && ($gift_card['rule_id'] == $rule['rule_id'])){
                        continue;
                    }else {
                        $can_combine = false;
                        break ;
                    }
                    $ruleIds[] = $gift_card['rule_id'];
                }
            }

            /**
             * Check can combine promotion rule other than gift card
             */
            if($can_combine) {

                if(!empty($cart_rules)) {
                    foreach ($cart_rules as $cart_rule) {
                        if($cart_rule['can_combine'] == 0 && $cart_rule['rule_id'] != $rule['rule_id']) {
                            $can_combine = false;
                        }
                    }
                }

                if(isset($data['items'])) {
                    foreach ($data['items'] as $item) {
                        $isJson = \Helpers\ParserHelper::isJSON($item['cart_rules']);
                        if($isJson) {
                            $cart_rules = json_decode($item['cart_rules'],true);
                            if(!empty($cart_rules)) {
                                foreach ($cart_rules as $cart_rule) {
                                    if($cart_rule['can_combine'] == 0 && $cart_rule['rule_id'] != $rule['rule_id']) {
                                        $can_combine = false;
                                    }
                                    $ruleIds[] = $cart_rule['rule_id'];

                                }
                            }
                        }
                    }
                }
            }

        }

        // Gosend Rule Combine
        $gosendRuleId = explode(',',getenv('GOSEND_RULE_ID'));
        if($can_combine == false){
            if(in_array($rule['rule_id'],$gosendRuleId)){
                $can_combine = true;
            }

            $ruleIds = array_unique($ruleIds);
            foreach($ruleIds as $rowRuleId){
                if(!in_array($rowRuleId, $gosendRuleId)){
                    $can_combine = false;        
                }        
            }
        }

        return $can_combine;
    }

    public function isValidCompany($cartData = [], $rules = [])
    {
        // Check if this rules can be applied for this company or not
        $companyCodeList = explode(",",$rules['company_code']);
        if (!in_array($cartData['company_code'], $companyCodeList)) {
            return false;
        }

        return true;
    }


    public function prerequisiteConditionCheck($cartData = [], $rules = [])
    {
        $isValidCompany = $this->isValidCompany($cartData,$rules);
        if(!$isValidCompany) {
            return false;
        }

        $isActiveTime = \Helpers\GeneralHelper::checkActiveBetweenDate($rules['from_date'],$rules['to_date']);

        // if this is not the time to get this promotion skip it
        if(!$isActiveTime) {
            return false;
        }

        // Check if a rule already aplied or not in this shopping cart
        $isRulesApplied = $this->isRulesApplied($cartData,$rules);
        if(!$isRulesApplied) {
            return false;
        }

        // Check if this rules can combined or not
        $canCombine = $this->canCombineCheck($cartData,$rules);
        if(!$canCombine) {
            return false;
        }

        // Check used per customer
        if (isset($rules['uses_per_customer'])) {
            if($rules['uses_per_customer'] > 0) {
                if($cartData['customer']['customer_is_guest'] == 1) {
                    return false;
                }
    
                if((int)$rules['uses_per_customer_frequence'] == 0) {
                    $isValidUsage = $this->maxUsageValidation($rules, $cartData['customer']['customer_id']);
                }else{
                    $isValidUsage = $this->maxUsedFrequenceValidation($rules, $cartData['customer']['customer_id']);
                }

                if($isValidUsage === false) {
                    return false;
                }
            }
        }

        // limit used check
        if (isset($rules['limit_used'])) {
            if($rules['limit_used'] > 0){
                $isValidUsage = $this->maxLimitUsedValidation($rules);
                if($isValidUsage === false) {
                    return false;
                }
            }
        }

        $isAllowedGroup = $this->isAllowedGroup($cartData,$rules);
        if(!$isAllowedGroup) {
            return false;
        }

        return true;
    }

    public function isRulesApplied($cartData = [], $rules = [])
    {
        $cartRules = json_decode($cartData['cart_rules'],true);
        if(!empty($cartRules)) {
            $foundRule = false;
            foreach ($cartRules as $appliedRule) {
                if($appliedRule['rule_id'] == $rules['rule_id']) {
                    $foundRule = true;
                    break;
                }
            }

            if($foundRule) {
                return false;
            }

        }

        return true;
    }

    public function isAllowedGroup($cartData = [], $rules = [])
    {
        if(isset($rules['customer_group_id']) && $rules['customer_group_id'] != "") {
            $allowedGroup = explode(",",$rules['customer_group_id']);
            if (isset($cartData['customer']['customer_group']) && count($cartData['customer']['customer_group']) > 0) {
                foreach ($cartData['customer']['customer_group'] as $customer_group) {
                    if(in_array($customer_group,$allowedGroup)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function countingDiscountGet($sales_rules = [], $amount_to_discount = 0, $voucher_amount_percentage = 0)
    {
        $discountAmount = 0;
        $originDiscountAmount = isset($sales_rules['action']['discount_amount']) ? $sales_rules['action']['discount_amount'] : 0;
        if(!isset($sales_rules['action']['discount_type']) || $sales_rules['action']['discount_type'] == 'fix') {
            $discountAmount =  CartHelper::countDiscountAmountUsed($amount_to_discount, $originDiscountAmount);
        } else if ($sales_rules['action']['discount_type'] == 'percent') {
            if(!empty($voucher_amount_percentage)) {
                $discountAmountGet = $originDiscountAmount;
            } else {
                $discountAmountGet = round(($originDiscountAmount / 100) * $amount_to_discount);
            }
            if(isset($sales_rules['action']['max_redemption'])) {
                if(!empty((int)$sales_rules['action']['max_redemption']) && $discountAmountGet > $sales_rules['action']['max_redemption']) {
                    $discountAmountGet = $sales_rules['action']['max_redemption'];
                }
            }

            $originDiscountAmount = $discountAmount =  CartHelper::countDiscountAmountUsed($amount_to_discount,$discountAmountGet);
        } else if ($sales_rules['action']['discount_type'] == 'final_amount') {
            $originDiscountAmount = $amount_to_discount - $sales_rules['action']['final_amount'];
            $discountAmount = CartHelper::countDiscountAmountUsed($amount_to_discount,$originDiscountAmount);
        }

        return [$discountAmount,$originDiscountAmount];
    }

    public function incrementPromotionUssage($rule = [], $cart_rule = '', $cart_gift_card = '')
    {
        $promotionStack = 1;
        if(!empty($rule['promotion_stack'])) {
            $promotionStack = $rule['promotion_stack'];
        }

        $attributeToIncrement = ['subtotal','total_qty_item','items.qty_ordered','shipping_amount','grand_total_exclude_shipping','items.grand_total_exclude_shipping','grand_total', 'items.subtotal'];

        if (!empty($cart_rule)) {
            $appliedRules = json_decode($cart_rule,true);
            foreach ($appliedRules as $appliedRule) {
                if ($appliedRule['rule_id'] == $rule['rule_id'] && ($rule['global_can_combine'] == 1 || $rule['can_combine'] == 1)) {
                    $promotionStack = $appliedRule['promotion_stack']++;
                }
            }
        }

        if (!empty($cart_gift_card)) {
            $appliedGiftCards = json_decode($cart_gift_card,true);
            foreach ($appliedGiftCards as $appliedGiftCard) {
                if ($appliedGiftCard['rule_id'] == $rule['rule_id'] && ($rule['global_can_combine'] == 1 || $rule['can_combine'] == 1)) {
                    $promotionStack++;
                }
            }
        }

        foreach ($rule['conditions'] as $key => $val) {
            if (in_array($val['attribute'],$attributeToIncrement)) {
                $rule['conditions'][$key]['value'] = $promotionStack * $val['value'];
            }
        }

        $rule['promotion_stack'] = $promotionStack;

        return $rule;
    }

    public function limitGroupRule($rule_id, $customer_id){
        $salesOrderLib = new SalesOrderLib;
        $totalFrequence = $salesOrderLib->getTotalUsedPerCustomer($rule_id,$customer_id);

        if($totalFrequence>=1){
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Exceed max usage of promotion per day";
            return false;
        }else{
            return true;
        }


        // Terlalu canggih. akan didevelop kembali ketika fungsi 0 @ 0 digunakan pada Rule Head Group
        // $promotionType[] = 'shopping_cart';
        // $marketingPromo = $this->getMarketingPromotion($promotionType);

        // if(!empty($marketingPromo)) {
        //     foreach ($promotionType as $key => $type) {
        //         if(empty($marketingPromo[$type])){
        //             continue;
        //         }

        //         $promoList = json_decode($marketingPromo[$type],true);
        //         foreach ($promoList as $rule) {
        //             if($rule_id == $rule['rule_id']){

        //             }
        //         }
        //     }
        // }
    }

    public function redeemRefundCheck($giftCards) {
        $valueRedeemRefund = 0;
        $giftcard = json_decode($giftCards,true);
        $voucherType = ["2","5"];        
        foreach ($giftcard as $voucherApplied){
            if(in_array($voucherApplied['voucher_type'],$voucherType)){
                $valueRedeemRefund += $voucherApplied['voucher_amount_used'];
            }
        }
        return $valueRedeemRefund;
    }
}