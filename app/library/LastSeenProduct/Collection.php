<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Library\LastSeenProduct;


class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Library\LastSeenProduct
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->getDataArray();
            $view[] = $itemArray;

            $this->next();
        }
        return $view;
    }

    public function updateItem($sku = "")
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();

            if($sku == $item->getSku()) {
                $item->setUpdatedAt(time());
                break;
            }

            $this->next();
        }

        return true;
    }


}