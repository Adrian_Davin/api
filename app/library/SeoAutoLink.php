<?php
/**
 * Created by PhpStorm.
 * User: iwan
 * Date: 30/08/18
 * Time: 9:55
 */

namespace Library;


class SeoAutoLink
{
    /**
     * @var $seoAutoLink \Models\SeoAutoLink
     */
    protected $seoAutoLink;
    protected $errorCode;
    protected $errorMessages;

    public function __construct()
    {
        $this->seoAutoLink = new \Models\SeoAutoLink();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getSeoAutoLink($params = array()){
        $seoAutoLinkModel = new \Models\SeoAutoLink();

        $conditions = array();

        if (isset($params['seo_link'])) { // no index
            // $resultSeoAutoLink->andWhere("Models\SeoAutoLink.seo_link = '". $params['seo_link']."'");
            array_push($conditions,"seo_link = '".$params['seo_link']."'");
        }

        if (isset($params['keyword'])) {
            $params['keyword'] = addslashes($params['keyword']);
            // $resultSeoAutoLink->andWhere("Models\SeoAutoLink.seo_link like \"%". $params['keyword']."%\"");
            array_push($conditions,"seo_link like \"%".$params['keyword']."%\"");
        }

        if (isset($params['company_code'])) { // no index
            // $resultSeoAutoLink->andWhere("Models\SeoAutoLink.company_code = '". $params['company_code']."'");
            array_push($conditions,"company_code = '".$params['company_code']."'");
        }

        if (isset($params['seo_auto_link_id'])) {
            // $resultSeoAutoLink->andWhere("Models\SeoAutoLink.seo_auto_link_id = '". $params['seo_auto_link_id']."'");
            array_push($conditions,"seo_auto_link_id = '".$params['seo_auto_link_id']."'");
        }

        if (isset($params['flag'])) {
            // $resultSeoAutoLink->andWhere("Models\SeoAutoLink.flag = '". $params['flag']."'");
            array_push($conditions,"flag = '".$params['flag']."'");
        }

        if (isset($params['typo'])) {
            // $resultSeoAutoLink->andWhere("Models\SeoAutoLink.typo = '". $params['typo']."'");
            array_push($conditions,"typo = '".$params['typo']."'");
        }

        $finalConditions = implode(" AND ", $conditions);

        // $resultSeoAutoLink->orderBy("seo_auto_link_id DESC");

        $finalQuery = array(
            "conditions" => $finalConditions
        );

        if (isset($params['master_app_id'])) {
            $resultTotal = $seoAutoLinkModel->find($finalQuery);
            $recordsTotal = $resultTotal->count();
        }

        $finalQuery = array_merge($finalQuery, array(
            "order" => "seo_auto_link_id DESC",
            "limit" => 1
        ));

        if (isset($params['limit']) && isset($params['offset'])) {
            $finalQuery['limit'] = $params['limit'];
            $finalQuery['offset'] = $params['offset'];
        }

        $resultSeoAutoLink = $seoAutoLinkModel->find($finalQuery);

        if($resultSeoAutoLink->count() <= 0) {
            $this->errorCode = "404";
            $this->errorMessages = "No Seo Link Found";
            return array();
        } else {
            $allSeoAutoLink['list'] = $resultSeoAutoLink->toArray();
            $allSeoAutoLink['recordsTotal'] = isset($params['master_app_id']) ? $recordsTotal : $resultSeoAutoLink->count();
        }

        return $allSeoAutoLink;
    }

    public function customList($params = array()) {
        $company_code = isset($params['company_code']) ? $params['company_code'] : "ODI";
        $limit = isset($params['limit']) ? $params['limit'] : "30";
        $data = [];
        $flag = 0;
        foreach (range('a', 'z') as $letter) {
            $seoAutoLinkModel = new \Models\SeoAutoLink();

            $resultSeoAutoLink = $seoAutoLinkModel::query();
            $resultSeoAutoLink->columns('seo_link');
            $resultSeoAutoLink->andWhere("Models\SeoAutoLink.seo_link like '". $letter."%'");
            $resultSeoAutoLink->andWhere("Models\SeoAutoLink.company_code = '".$company_code."'");
            if (isset($params['status'])) {
                $resultSeoAutoLink->andWhere("Models\SeoAutoLink.status = '".$params['status']."'");
            }
            if (isset($params['typo'])) {
                $resultSeoAutoLink->andWhere("Models\SeoAutoLink.typo = '".$params['typo']."'");
            }
            $resultSeoAutoLink->orderBy("is_default DESC, created_at DESC");
            $resultSeoAutoLink->limit($limit,0);
            $result = $resultSeoAutoLink->execute();
            if(!empty($result->count())) {
                $data[$flag]['key'] = strtoupper($letter);
                $data[$flag]['items'] = $result->toArray(); 
                $flag++;
            }
        }

        return $data;
    }

    public function customAlphabet($params = array()) {
        $letter = isset($params['letter']) ? $params['letter'] : "A";
        $company_code = isset($params['company_code']) ? $params['company_code'] : "ODI";
        $offset = isset($params['offset']) ? $params['offset'] : "0";
        $limit = isset($params['limit']) ? $params['limit'] : "20";
        $data = [];
        $seoAutoLinkModel = new \Models\SeoAutoLink();

        $resultSeoAutoLink = $seoAutoLinkModel::query();
        $resultSeoAutoLink->columns('seo_link');
        $resultSeoAutoLink->andWhere("Models\SeoAutoLink.seo_link like '". $letter."%'");
        $resultSeoAutoLink->andWhere("Models\SeoAutoLink.company_code = '".$company_code."'");
        if (isset($params['status'])) {
            $resultSeoAutoLink->andWhere("Models\SeoAutoLink.status = '".$params['status']."'");
        }
        if (isset($params['typo'])) {
            $resultSeoAutoLink->andWhere("Models\SeoAutoLink.typo = '".$params['typo']."'");
        }
        $resultSeoAutoLink->orderBy("is_default DESC, created_at DESC");
        $resultTotal = $resultSeoAutoLink->execute();
        $resultSeoAutoLink->limit($limit,$offset);
        $result = $resultSeoAutoLink->execute();
        if(!empty($result->count())) {
            $data['key'] = strtoupper($letter);
            $data['items'] = $result->toArray(); 
            $data['total'] = $resultTotal->count();
        }

        return $data;
    }
}