<?php

namespace Library;

use \GuzzleHttp\Client as GuzzleClient;
use \Helpers\LogHelper;
use \Helpers\SimpleEncrypt;

class APIWrapper
{
    protected $client;
    protected $headers;
    protected $param;
    protected $response;
    protected $data;
    protected $rawData;
    protected $error;
    protected $errorBody;
    protected $messages;
    protected $endPoint;
    protected $count;
    protected $responseArray;
    protected $base_uri;

    public function __construct($base_uri = "", $stream = false)
    {
        if (empty($base_uri)) {
            $base_uri = !(empty($_ENV['API_URL'])) ? $_ENV['API_URL'] : '';
        }
        $this->client = new GuzzleClient(
            [
                'base_uri' => $base_uri,
                'verify' => false,
                'stream' => $stream
            ]
        );

        $this->param = array();
        $this->headers = array();
        $this->base_uri = $base_uri;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * @param mixed $param
     */
    public function setParam($param)
    {
        $this->param = $param;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return mixed
     */
    public function getErrorBody()
    {
        return $this->errorBody;
    }

    /**
     * @param mixed $error
     */
    public function setErrorBody($error)
    {
        $this->errorBody = $error;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getResponseArray()
    {
        return $this->responseArray;
    }

    /**
     * @param mixed $response
     */
    public function setResponseArray($responseArray)
    {
        $this->response = $responseArray;
    }

    /**
     * @return mixed
     */
    public function getEndPoint()
    {
        return $this->endPoint;
    }

    /**
     * @param mixed $endPoint
     */
    public function setEndPoint($endPoint)
    {
        if (getenv('ENVIRONMENT') != "production" && $this->base_uri == "http://10.110.100.23") {
            $endPoint = "/membership_api" . $endPoint;
        }
        $this->endPoint = $endPoint;
    }

    /**
     * @return mixed
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param mixed $rawData
     */
    public function setRawData($rawData)
    {
        $this->rawData = $rawData;
    }

    /**
     * @return mixed
     */
    public function getIsOk()
    {
        return $this->is_ok;
    }

    /**
     * @param mixed $rawData
     */
    public function setIsOk($is_ok)
    {
        $this->is_ok = $is_ok;
    }

    /**
     * @return mixed
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @param mixed $rawData
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $rawData
     */
    public function setRows($rows)
    {
        $this->rows = $rows;
    }

    private function _validate()
    {
        if (empty($this->endPoint)) {
            LogHelper::log("api_call", "End point '" . $this->endPoint . "'not found");
            return false;
        }

        /*if(empty($this->param)) {
            LogHelper::log("api_call", "Parameter is empty");
            return false;
        }*/

        return true;
    }

    public function send($method = "", $timeout = "60.0")
    {
        $valid = $this->_validate();
        if (!$valid) {
            return false;
        }

        $payload_data = [
            "errors" => array(),
            "data" => $this->param,
            "messages" => array()
        ];

        // add default content type
        $this->headers['Content-Type'] =  'application/json';
        // add gzip support
        $this->headers['Accept-Encoding'] =  'gzip';

        $addtionalParam = [];
        if (!empty(getenv('ENVIRONMENT')) && getenv('ENVIRONMENT') == 'development' && !empty(getenv('HTTP_PROXY'))) {
            $addtionalParam['proxy'] = 'http://192.168.1.37:22';
        }

        try {
            switch ($method) {
                case "get":
                    $params = ['timeout' => $timeout, 'headers' => $this->headers];
                    $params = array_merge($params, $addtionalParam);
                    $this->response = $this->client->get($this->endPoint, $params);
                    break;
                case "put":
                    $params = ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers];
                    $params = array_merge($params, $addtionalParam);
                    $this->response = $this->client->put($this->endPoint, $params);
                    break;
                case "delete":
                    $params = ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers];
                    $params = array_merge($params, $addtionalParam);
                    $this->response = $this->client->delete($this->endPoint, $params);
                    break;
                case "post":
                default:
                    $params = ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers];
                    $params = array_merge($params, $addtionalParam);
                    $this->response = $this->client->post($this->endPoint, $params);
                    break;
            }
        } catch (\Exception $e) {
            LogHelper::log("api_call", "Error getting response : " . $this->endPoint . " : " . json_encode($e->getMessage()) . " REQUEST : " . json_encode($this->headers) . " - " . json_encode($payload_data));
            return false;
        }

        $this->_mapResponse();
        return true;
    }

    public function sendRequest($method = "", $timeout = "60.0", $useNewMapper = false)
    {
        $valid = $this->_validate();
        if (!$valid) {
            return false;
        }


        $payload_data = $this->param;

        // add default content type
        $this->headers['Content-Type'] =  'application/json';
        // add gzip support
        $this->headers['Accept-Encoding'] =  'gzip';

        try {
            switch ($method) {
                case "get":
                    $this->response = $this->client->get($this->endPoint, ['timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "put":
                    $this->response = $this->client->put($this->endPoint, ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "delete":
                    $this->response = $this->client->delete($this->endPoint, ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "post":
                default:
                    $this->response = $this->client->post($this->endPoint, ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers]);
                    break;
            }
        } catch (\Exception $e) {
            $responseBody = $e->getResponse()->getBody(true);            
            if (!empty($responseBody)) {
                $errBody = json_decode($responseBody);                
                if (isset($errBody->error)){
                    $this->setErrorBody($errBody->error);                    
                }                
            }
            $this->error = $e->getMessage();
            \Helpers\LogHelper::log("api_call", "Error getting response : " . $this->endPoint . " : " . json_encode($e->getMessage()));
            return false;
        }

        if ($useNewMapper) {
            $this->_mapResponseNew();
        } else {
            $this->_mapResponse();
        }

        return true;
    }

    private function _mapResponse()
    {
        
        if (empty($this->response)) {
            LogHelper::log("api_call", "Get empty response : " . $this->endPoint . " : " . json_encode($this->error));
            return;
        }

        $responseArray = json_decode($this->response->getBody()->getContents(), true);

        if (isset($responseArray) && !empty($responseArray)) {
            $this->responseArray = $responseArray;
        }

        if (!empty($responseArray['data'])) {
            $this->rawData = $responseArray['data'];
        }

        if (isset($responseArray['Data'])) {
            if (!empty($responseArray['Data'])) {
                $this->rawData = $responseArray['Data'];
            }
        }

        if (!empty($responseArray['errors'])) {
            $this->error = $responseArray['errors'];
            LogHelper::log("api_call", "Error when calling api : " . json_encode($responseArray['errors']));
        }

        if (!empty($responseArray['messages'])) {
            $this->messages = $responseArray['messages'];
        }

        if (!empty($responseArray['message'])) {
            $this->messages = $responseArray['message'];
        }

        if (isset($responseArray['is_ok'])) {
            $this->is_ok = $responseArray['is_ok'];
        }

        if (!empty($responseArray['rows'])) {
            $this->rows = $responseArray['rows'];
        }

        if (!empty($responseArray['count'])) {
            $this->count = $responseArray['count'];
        }

        if ($this->response->getStatusCode() < 200 || $this->response->getStatusCode() >= 300) {
            LogHelper::log("api_call", "Error getting response : " . $this->endPoint . " : " . json_encode($this->error));
        }
    }

    private function _mapResponseNew()
    {
        
        if (empty($this->response)) {
            LogHelper::log("api_call", "Get empty response : " . $this->endPoint . " : " . json_encode($this->error));
            return;
        }

        $responseArray = json_decode($this->response->getBody()->getContents(), true);

        if (isset($responseArray) && !empty($responseArray)) {
            $this->responseArray = $responseArray;
        }

        if (!empty($responseArray['data'])) {
            $this->rawData = $responseArray['data'];
        }

        if (isset($responseArray['Data'])) {
            if (!empty($responseArray['Data'])) {
                $this->rawData = $responseArray['Data'];
            }
        }

        if (!empty($responseArray['error'])) {
            $this->error = $responseArray['error'];
            LogHelper::log("api_call", "Error when calling api : " . json_encode($responseArray['error']));
        }

        if (!empty($responseArray['messages'])) {
            $this->messages = $responseArray['messages'];
        }

        if (!empty($responseArray['message'])) {
            $this->messages = $responseArray['message'];
        }

        if (isset($responseArray['is_ok'])) {
            $this->is_ok = $responseArray['is_ok'];
        }

        if (!empty($responseArray['rows'])) {
            $this->rows = $responseArray['rows'];
        }

        if (!empty($responseArray['count'])) {
            $this->count = $responseArray['count'];
        }

        if ($this->response->getStatusCode() < 200 || $this->response->getStatusCode() >= 300) {
            LogHelper::log("api_call", "Error getting response : " . $this->endPoint . " : " . json_encode($this->error));
        }
    }

    public function sendRequestStripes($method = "", $timeout = "60.0")
    {
        $valid = $this->_validate();
        if (!$valid) {
            return false;
        }


        $payload_data = $this->param;

        // add default content type
        $this->headers['Content-Type'] =  'application/json';
        // add gzip support
        $this->headers['Accept-Encoding'] =  'gzip';

        try {
            switch ($method) {
                case "get":
                    $this->response = $this->client->get($this->endPoint, ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "put":
                    $this->response = $this->client->put($this->endPoint, ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "delete":
                    $this->response = $this->client->delete($this->endPoint, ['timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "post":
                default:
                    $this->response = $this->client->post($this->endPoint, ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers]);
                    break;
            }
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            \Helpers\LogHelper::log("api_call", "Error getting response : " . $this->endPoint . " : " . json_encode($e->getMessage()));
            return false;
        }

        $this->_mapResponseStripes();

        return true;
    }

    private function _mapResponseStripes()
    {
        if (empty($this->response)) {
            LogHelper::log("api_call", "Get empty response : " . $this->endPoint . " : " . json_encode($this->error));
            return;
        }

        $responseArray = json_decode($this->response->getBody()->getContents(), true);
        if (!empty($responseArray)) {
            $this->rawData = $responseArray;
        }

        if (!empty($responseArray['errors'])) {
            $this->error = $responseArray['errors'];
            LogHelper::log("api_call", "Error when calling api : " . json_encode($responseArray['errors']));
        }

        if ($this->response->getStatusCode() < 200 || $this->response->getStatusCode() >= 300) {
            LogHelper::log("api_call", "Error getting response : " . $this->endPoint . " : " . json_encode($this->error));
        }
    }

    public function formatResponse($format = array(), $raw_data = array(), $need_to_encrypt = array())
    {
        if (empty($format)) {
            $this->data = $this->rawData;
            return;
        }

        if (empty($raw_data)) {
            $raw_data = $this->rawData;
        }

        $formatData = $format;
        $view = $this->_formatThisData($formatData, $raw_data, $need_to_encrypt);

        // send back not found key to front end as empty data
        if (!empty($formatData)) {
            foreach ($formatData  as $not_found) {
                $view[$not_found] =  "";
            }
        }

        $this->data = $view;
    }

    private function _formatThisData(&$format = array(), $raw_data = array(), $need_to_encrypt = array(), &$view_data = array(), $key_parent = "")
    {
        foreach ($raw_data as $keyData => $valData) {
            if (is_array($valData)) {
                $thisKey = $keyData;
                if (!empty($key_parent)) {
                    $thisKey = $key_parent . "." . $keyData;
                }
                $this->_formatThisData($format, $valData, $need_to_encrypt, $view_data, $thisKey);
            } else {
                if (!empty($key_parent)) {
                    $thisKey = $key_parent . "." . $keyData;
                } else {
                    $thisKey = $keyData;
                }

                if (in_array($thisKey, $format)) {
                    $keyArray = array_search($thisKey, $format);
                    if (in_array($thisKey, $need_to_encrypt)) {
                        $view_data[$keyData] = SimpleEncrypt::encrypt($valData);
                    } else {
                        $view_data[$keyData] = $valData;
                    }
                    unset($format[$keyArray]);
                } else if (isset($format[$thisKey])) {
                    if (in_array($format[$thisKey], $need_to_encrypt)) {
                        $view_data[$format[$thisKey]] = SimpleEncrypt::encrypt($valData);
                    } else {
                        $view_data[$format[$thisKey]] = $valData;
                    }
                    unset($format[$thisKey]);
                }
            }
        }

        return $view_data;
    }

    public function sendRequestUnify($method = "", $timeout = "60.0")
    {
        $valid = $this->_validate();
        if (!$valid) {
            return false;
        }


        $payload_data = $this->param;

        // add default content type
        $this->headers['Content-Type'] =  'application/json';
        // add gzip support
        $this->headers['Accept-Encoding'] =  'gzip';

        try {
            switch ($method) {
                case "get":
                    $this->response = $this->client->get($this->endPoint, ['timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "put":
                    $this->response = $this->client->put($this->endPoint, ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "delete":
                    $this->response = $this->client->delete($this->endPoint, ['timeout' => $timeout, 'headers' => $this->headers]);
                    break;
                case "post":
                default:
                    $this->response = $this->client->post($this->endPoint, ['json' => $payload_data, 'timeout' => $timeout, 'headers' => $this->headers]);
                    break;
            }
        } catch (\Exception $e) {
            $responseBody = $e->getResponse()->getBody(true);            
            if (!empty($responseBody)) {
                $errBody = json_decode($responseBody);            
                if (isset($errBody->errors)){
                    $this->setErrorBody($errBody->errors);                
                }                
            }
            $this->error = $e->getMessage();
            \Helpers\LogHelper::log("api_call", "Error getting response : " . $this->endPoint . " : " . json_encode($e->getMessage()));
            return false;
        }

        $this->_mapResponse();

        return true;
    }
}

