<?php

namespace Library;

class POSChatime
{
    protected $P_LIST_ITEM;
    protected $P_STORE_ID;
    protected $P_MEMBER_ID;
    protected $P_TOTAL_QTY;
    protected $P_RECEIVER_NAME;
    protected $P_SUB_TOTAL;
    protected $P_GRAND_TOTAL;

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    public function __construct($invoice = null)
    {
        if (!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function prepareParams()
    {
        // {
        //     "P_LIST_ITEM": [
        //         {
        //             "P_PRODUCT_ID": "10414198",
        //             "P_QTY_PER_ITEM": 2
        //         }
        //     ],
        //     "P_MEMBER_ID": 1580,
        //     "P_STORE_ID": "F353",
        //     "P_TOTAL_QTY": 2,
        //     "P_RECEIVER_NAME": "test",
        //     "P_SUB_TOTAL": 0,
        //     "P_GRAND_TOTAL": 0
        // }
        $invoiceData = $this->invoice->getDataArray([], true);
        // $salesCustomerModel = new \Models\SalesCustomer;
        // $queryCondition = array(
        //     "conditions" => "sales_order_id = '" . $invoiceData['sales_order_id'] . "'"
        // );
        // $salesCustomerResult = $salesCustomerModel->findFirst($queryCondition);
        $this->P_LIST_ITEM = array();
        if (!empty($invoiceData) && count($invoiceData["items"]) > 0) {
            $items = $invoiceData["items"];
            // $this->P_RECEIVER_NAME = $salesCustomerResult->getCustomerFirstname();
            $this->P_RECEIVER_NAME = $invoiceData["invoice_no"];
            foreach ($items as $item) {
                $data = array();
                $data["P_PRODUCT_ID"] = $item["sku"];
                $data["P_QTY_PER_ITEM"] = is_string($item["qty_ordered"]) ? (int) $item["qty_ordered"] : $item["qty_ordered"];
                array_push($this->P_LIST_ITEM, $data);
            }
        }
        $this->P_STORE_ID = $this->invoice->getStoreCode();
        $this->P_MEMBER_ID = (int) $_ENV['CHATIME_MEMBER_ID'];
        $this->P_TOTAL_QTY = $this->invoice->getQtyOrdered();
        $this->P_SUB_TOTAL = 0;
        $this->P_GRAND_TOTAL = 0;
    }

    public function getParams()
    {
        $thisArray = get_object_vars($this);
        $parameter = array();
        foreach ($thisArray as $key => $val) {
            if ($key !== "invoice") {
                $parameter[$key] = $val;
            }
        }

        return $parameter;
    }

    public function createPOSChatime()
    {
        $parameter = $this->getParams();
        $nsq = new \Library\Nsq();
        $message = [
            "data" => json_encode($parameter),
            "message" => "createPosChatime",
            "invoice_no" => $this->invoice->getInvoiceNo()
        ];
        $nsq->publishCluster('transactionSap', $message);
        return true;
    }
}
