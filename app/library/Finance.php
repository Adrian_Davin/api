<?php

/**
 * Created by PhpStorm.
 * User: Rusmin
 * Date: 28/3/2021
 * Time: 9:56 AM
 */

namespace Library;

use Phalcon\Db as Database;

class Finance
{
    /**
     * @var \Models\SalesOrder
     */
    protected $salesOrder;

    public function __construct($sales_order = null)
    {
        if (!empty($sales_order)) {
            $this->salesOrder = $sales_order;
        }
    }

    /**
     * @param \Models\SalesOrder $sales_order
     */
    public function setSalesOrder($sales_order)
    {
        $this->salesOrder = $sales_order;
    }

    public function GetBankNameCCBin($bin = ""){
        $getModel = new \Models\SalesOrderItemMdr();
        $getModel->useReadOnlyConnection();
        $sql  = "select bank_name
                from list_bin_cc
                where bin = '".$bin."'";
        $result = $getModel->getDi()->getShared($getModel->getConnection())->query($sql);
        $result->setFetchMode(Database::FETCH_ASSOC);
        $resultData = $result->fetch();
        if(count($resultData) > 0){
            return $resultData['bank_name'];
        }
        return "";
    }

    public function GetMDRMappingBank($bank = ""){
        $getModel = new \Models\SalesOrderItemMdr();
        $getModel->useReadOnlyConnection();
        $sql  = "select mdr_bank_code
                    from mdr_mapping_bank
                    where bank_name = '".$bank."'";
        $result = $getModel->getDi()->getShared($getModel->getConnection())->query($sql);
        $result->setFetchMode(Database::FETCH_ASSOC);
        $resultData = $result->fetch();
        if(count($resultData) > 0){
            return $resultData['mdr_bank_code'];
        }
        return "";
    }

    public function GetMDRPayment($bank = "", $methodType = "", $tenorTemp = ""){
        $resultData = [];
        $getModel = new \Models\SalesOrderItemMdr();
        $getModel->useReadOnlyConnection();
        $sql  = "select type, rate
                from mdr_payment
                where bank = '".$bank."' and method = '".$methodType."' and installment_term = '".$tenorTemp."'";
        $result = $getModel->getDi()->getShared($getModel->getConnection())->query($sql);
        $result->setFetchMode(Database::FETCH_ASSOC);
        $resultData = $result->fetch();
        if(count($resultData) > 0){
            return $resultData;
        }
        return $resultData;
    }

    public function GetMDRValue($item, $mp = [], $totalItems = 1, $mdrCustomer = 0){
        $value = 0;
        if ($mp['type'] == "percent") {
            if ($mp['rate'] == 0) {
                $value = 0;
            } else {
                $sellingPlusHandling = $item->getSellingPrice() + $item->getHandlingFeeAdjust()+ $mdrCustomer;
                $subTotalItem = \Helpers\Transaction::countTotal($sellingPlusHandling, $item->getDiscountAmount(), $item->getShippingAmount(),$item->getShippingDiscountAmount(), $item->getGiftCardsAmount());
                $rowTotalItem = \Helpers\Transaction::countTotalItem($subTotalItem, $item->getQtyOrdered());
                $value = $rowTotalItem * $mp['rate'] / 100;
            }
        } else {
            $value = $mp['rate'] / $totalItems;
        }
        return $value;
    }

    public function calculateMDR()
    {
        $return = array();
        $items = $this->salesOrder->items;
        $method = $this->salesOrder->payment->method;
        $ccNumber = $this->salesOrder->payment->cc_number;
        $ccType = $this->salesOrder->payment->cc_type;
        $vaBank = strtolower($this->salesOrder->payment->va_bank);
        $type = $this->salesOrder->payment->type;
        $installmentTenor = $this->salesOrder->payment->installment_tenor;
        $device = $this->salesOrder->device;

        $bank = "";
        $methodType = "";
        $tenor = "0";
        $bankCC = "";

        $fixRate = 0;
        
        // exclude free items
        // $finalItems = [];
        // if(count($items) > 0){
        //     foreach($items as $rowItem){
        //         if($rowItem->getIsFreeItem() == false){
        //             $finalItems[] = $rowItem;
        //         }
        //     }
        // }
        $finalItems = $items;      
        if($method != ""){
            switch ($method){
                case "vpaymentva" : 
                    $bank = "bca";
                    if ($vaBank == "permata"){
                        $bank = "permata";
                    }elseif ($vaBank == "bni"){
                        $bank = "bni";
                    }elseif ($vaBank == "bri"){
                        $bank = "bri";
                    }
                    $methodType = "va";
                    break;
                case "niceva":
                    $bank = "nicepay";
                    $methodType = "va";
                    break;
                case ($method == "vpaymentins" || $method == "vpayment") :
                    $methodType = "cc";
                    break;
                case ($method == "ovo" || $method == "gopay") :
                    $methodType = "e_money";
                    $bank = $method;
                    if ($bank == "gopay" && $device == "desktop") {
                        $bank = "qris";
                    }
                    break;
                case "kredivo":
                    $methodType = "credit_non_cc";
                    $bank = $method;
                    break;
                case "qris":
                    $methodType = "e_money";
                    $bank = $method;
                    break;
                default:
                    if ($type != "") {
                        $methodType = $type;
                    }
                    $bank = $method;
            }
        }

        if($bank == "free_payment"){
            return array();
        }

        if ($method == "manual_transfer" || $method == "term_of_payment") {
            return array();
        }

        if($methodType == "vendor"){
            return array();
        }

        if ($methodType == "cc") {  
            if ($ccType != "") {
                switch ($ccType) {
                case ($ccType == "mandiri" || $ccType == "bca" || $ccType == "cimb" ) :
                    $bank = $ccType;                    
                    break;
                case "bni":
                    $bank = "bni";
                    if ($ccNumber != "") {
                        if (substr($ccNumber,0,1) == "3") {
                            $bank = "bni_jcb";
                        }
                    }
                    break;
                case "bri":
                    $bank = "bri";
                    if ($ccNumber != "") {
                        $bin = substr($ccNumber,0,6);
                        $bankTemp = $this->GetBankNameCCBin($bin);
                        $bankTemp = $this->GetMDRMappingBank($bankTemp);
                        $bankCC = $bankTemp; 
                        }

                    if ($bankCC != $bank) {
                        if($bankCC == 'cimb'){
                            $bank = $bankCC;
                        }else{                                                        
                            $bank = "bri_off_us";                                                  
                                
                        }
                        
                    } else {
                        $bankCC = "";
                    }
                }
            }
            
            if ($installmentTenor == "") {
                $installmentTenor = "0";
            }
        
        }

        if ($bankTemp = $this->GetBankNameCCBin(substr($ccNumber,0,8)) == "KOINWORKS NEO" ){            
            $bank = "bca_off_us";
                  
        }

        if ($bank == "bri_off_us" || $bank == "bni_off_us") {
            $installmentTenor = "0";
        }

  

        if ($bank == "bca" && $methodType == "cc") {
            if ($ccNumber != "") {
                $bin = substr($ccNumber, 0, 6);
                $bankTemp = $this->GetBankNameCCBin($bin);
                $bankTemp = $this->GetMDRMappingBank($bankTemp);
                $bankCC = $bankTemp;
            }

            if ($bankCC != $bank) {
                if ($bankCC == 'cimb') {
                    $bank = $bankCC;
                } else {
                    $bank = "bca_off_us";
                    $installmentTenor = "0";
                }
            } else {
                $bankCC = "";
            }
        }


        if($methodType == "credit_non_cc" && $bank == 'kredivo'){
            $mpy = $this->GetMDRPayment($bank, 'nicepay_incentive', $installmentTenor);
            $fixRate = $this->GetMDRValue(array(), $mpy, count($finalItems));

            $totalMpyValue = count($finalItems) * floor($fixRate);
        }
        if ($bank == "bca" && $ccNumber != "") {
            $ccNumber = substr($ccNumber, 0, 6);
            $regexresult = preg_match("/^3[47]\d+$/", $ccNumber);
            if ($regexresult == 1) {
                $brands = $this->getBrandByBin($ccNumber);
                if (empty($brands['binData']['brand'])) {                    
                    $bank = "bca_amex";
                } else {
                    $isCardAmex = false;                    
                        if ($brands['binData']['brand']== "AMERICAN EXPRESS") {
                            $isCardAmex = true;
                        }
                    $bank = $isCardAmex === true ? "bca_amex" : "bca";
                }
            }            
        }

        $mpx = $this->GetMDRPayment($bank, $methodType, $installmentTenor);
        if (count($mpx) == 0) {
            return $return;
        }
        $mp = $mpx;

        $paymentType = $bank . " " . $methodType;
        if ($bankCC != "") {
            $paymentType = $bank . "-" . $bankCC . " " . $methodType;
        }

        $totalValue = 0;
        $rawCartData = $this->salesOrder->getCartRaw();
        foreach($finalItems as $row){
            $salesOrderItemId = $row->getSalesOrderItemId();
            $id = $row->getId();

            $mdrCustomer = 0;
            $tieringId = 0;
            if ($methodType == "cc" && $this->salesOrder->payment->installment_tenor > 0) {               
                    foreach ($rawCartData['items'] as $cartItem ) {                                                           
                        if ($cartItem['id'] == $row->getId()) {                        
                            $mdrCustomer = $cartItem['mdr_customer'];                        
                            $tieringId = $cartItem['tiering_id'];                        
                                                 
                        }
                    }
                    
                
            }

            $value = $this->GetMDRValue($row, $mp, count($finalItems),$mdrCustomer);            

            $value = $value + $fixRate;
            $itemReturn = [
                'sales_order_item_id' => $row->getSalesOrderItemId(),
                'sales_order_id' => $row->getSalesOrderId(),
                'mdr_type' => $mp['type'],
                'rate' => $mp['rate'],
                'installment_term' => "",
                'payment_type' => $paymentType,
                'value' => $value,
                'id' => $row->getId(),
                'parent_id' => $row->getParentId(),
                'is_free_item' => $row->getParentId()
            ];

            if ($mdrCustomer > 0) {
                $itemReturn['value_customer'] = $mdrCustomer;
                $itemReturn['tiering_id'] = $tieringId;
            }


            $return[$id] = $itemReturn;
        }

        // Exclude Free Items
        $finalReturn = [];
        foreach($return as $row){
            if($row['is_free_item']){
                $return[$row['parent_id']]['value'] += $row['value'];
                $return[$row['parent_id']]['value_customer'] += $row['value_customer'];
                unset($return[$row['id']]);
            }
        }
        
       

       

      
        
        

       

        foreach($return as $row){
            $roundValue = floor($row['value']);
            $itemReturn = [
                'sales_order_item_id' => $row['sales_order_item_id'],
                'sales_order_id' => $row['sales_order_id'],
                'mdr_type' => $row['mdr_type'],
                'rate' => $row['rate'],
                'installment_term' => $row['installment_term'],
                'payment_type' => $row['payment_type'],
                'value' => $roundValue,                
                'value_customer' => isset($row['value_customer']) ? $row['value_customer'] : null,
                'tiering_id' => isset($row['tiering_id']) ? $row['tiering_id'] : null

            ];

            $finalReturn[$row['sales_order_item_id']] = $itemReturn;
            $totalValue += $roundValue;
        }
        
        ksort($finalReturn);

        // Rounding, put selisih on lowest sales_order_item_id
        if($mp['type'] == "fix"){
            $selisih = $mp['rate'] - $totalValue;
            if($selisih > 0){
                $finalReturn[key($finalReturn)]['value'] += $selisih; 
            }
        }

        // Rounding fix rate, put selisih on lowest sales_order_item_id
        if (!empty($mpy)){
            if($mpy['type'] == "fix"){
                $selisih = $mpy['rate'] - $totalMpyValue;
                if($selisih > 0){
                    $finalReturn[key($finalReturn)]['value'] += $selisih; 
                }
            }
        }

        return $finalReturn;
        
    }

    public function getBrandByBin($bin)
    {
               
        $apiWrapper = new APIWrapper(getenv('PAYMENT_PHP_API'));

        // Now we deduct stock for this item
        
        $apiWrapper->setEndPoint("bin?bin=".$bin);        
        
        
        if($apiWrapper->sendRequest("get")) {
            
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = 404;
            $this->errorMessages[] = "Failed to connect to Payment API";
            return false;
        }

        if (!empty($apiWrapper->getError())) {
            $error = $apiWrapper->getError();
            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            \Helpers\LogHelper::log("brand_bin_error", json_encode($apiWrapper->getError()));
            return false;
        }

        return $apiWrapper->getRawData();
    }
}