<?php
/**
 * Library to send ruparupa order to SAP system
 */

namespace Library;

use Phalcon\Db as Database;
class CONSB2B
{

    protected $paramWsdl = array();
 
    protected $order_id;
    protected $order_date;
    protected $order_type;
    protected $cust_sold_no;
    protected $cust_ship_no;
    protected $cust_bill_no;
    protected $req_dlv_date;
    protected $ship_cost;
    protected $grand_price;
    protected $original_price;
    protected $partial_ship = "";
    protected $remark;
    protected $source;
    protected $sales_id;
    protected $sales_office = "O300";
    protected $sales_grp;
    protected $shipping_point = "";
    protected $ship_condition = "Z6"; //land & sea E-Commerce
    protected $forwarder;
    protected $dp_value = "";
    protected $tax_class = "";
    protected $ship_name_1 = "";
    protected $ship_name_2 = "";
    protected $ship_address_1 = "";
    protected $ship_address_2 = "";
    protected $ship_address_3 = "";
    protected $ship_city = "";
    protected $ship_portal = "";
    protected $ship_phone = "";
    protected $ship_email = "";
    protected $bill_block = "";
    protected $dlv_block = "";
    protected $company_code = "";
    protected $site = "";
    protected $is_refund = 0;

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    protected $invoiceItems;

    protected $creditMemo = array();
    protected $creditMemoInvoice = array();

    protected $cart;

    protected $parameter= array();

    public function __construct($invoice = null, $creditMemo = null)
    {
        if(!empty($invoice)) {
            $this->invoice = $invoice;
            
            if(!empty($creditMemo)) {
                $this->creditMemo = $creditMemo;
                $this->is_refund = 1;
    
                $invoiceNo = $invoice->getInvoiceNo();
                
                if (!empty($creditMemo['invoices'])) {
                    foreach ($creditMemo['invoices'] as $crmInvoice) {
                        if ($crmInvoice['invoice_no'] == $invoiceNo) {
                            $this->creditMemoInvoice = $crmInvoice;
                        }
                    }
                }
            }
        }
    }

    private function findCRMItem($sku) {
        if (!empty($this->creditMemoInvoice['credit_memo_items'])) {
            foreach ($this->creditMemoInvoice['credit_memo_items'] as $crmItem) {
                if ($crmItem['sku'] == $sku) {
                    return $crmItem;
                }
            }
        }

        return null;
    }

    /**
     * @param array $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    public function getOrderType()
    {
        return $this->order_type;
    }

    public function prepareDCParamsCONSB2B()
    {
        $companyModel = New \Models\CompanyProfileDC();
        if(empty($this->invoice)) {
            return false;
        }

        /**
         * @var $currentOrder \Models\SalesOrder
         */
        $currentOrder = $this->invoice->SalesOrder;

        $shipToId = $billToId = $soldToId = $currentOrder->getCompanyId();

        $forwardingToId = "";
        $orderData = date_create($currentOrder->getCreatedAt());

       
        $this->order_id = $currentOrder->getOrderNo();
        $this->order_date = date_format($orderData, "Ymd");
        
        $this->cust_sold_no = $soldToId;
        $this->cust_ship_no = $shipToId;
        $this->cust_bill_no = $billToId;
        $this->req_dlv_date = date_format($orderData, "Ymd");        
        $this->ship_cost = number_format( ($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount()) ,0, '.', '');

        $grandPrice = $this->invoice->getGiftCardsAmount() + $this->invoice->getGrandTotal();
        $this->grand_price = number_format($grandPrice,0, '.', '');
        $this->partial_ship = "";
        $this->forwarder = $forwardingToId;
        /**
         * Sending remark to SAP
         */
        $this->remark = "";
        //cek prefix if lazada or shopee or tokopedia set ship_condition to z6. pickup E-Commerce
        if(substr($this->order_id,0,3) == "LZD" || substr($this->order_id,0,4) == "ODIS" || substr($this->order_id,0,4) == "ODIT" || substr($this->order_id,0,4) == "ODIK"){
            $this->ship_condition = "Z6";
            if(substr($this->order_id,0,4) == "ODIS") {
                $this->remark = "SP-" . $this->invoice->getInvoiceNo();
            } else if(substr($this->order_id,0,4) == "ODIT") {
                $skuPromoTokpedDC = "";
                $additionalRemark = "";

                $skuPromoTokpedDC = getenv("SKU_PROMO_TOKPED_DC");
                
                if(!empty($skuPromoTokpedDC)){
                    $skuPromoTokpedDCArray = explode(",", $skuPromoTokpedDC);
                    if(!empty($this->invoiceItems)) {
                        foreach($this->invoiceItems as $item) {
                            if(in_array($item->getSku(), $skuPromoTokpedDCArray)){
                                $additionalRemark = getenv("REMARK_PROMO_TOKPED_DC");
                            }
                        }
                    }
                    
                }
                $this->remark = "TK-" . $this->invoice->getInvoiceNo()." ".$additionalRemark;
            } else if(substr($this->order_id,0,4) == "ODIK") {
                $this->remark = "TT-" . $this->invoice->getInvoiceNo();
            }
        }
       
        $this->shipping_point = '';
        $this->forwarder = '';
        $this->dp_value = '';
        $this->tax_class = '';

        // Genereate items
        $this->invoiceItems = $this->invoice->SalesInvoiceItem;        
        
        if (!empty($this->invoice->SalesShipment) && !empty($this->invoice->SalesShipment->SalesOrderAddress)) {
            $address = $this->invoice->SalesShipment->SalesOrderAddress;

            $address->setSalesOrderId($this->invoice->getSalesOrderId());
            $address->setAddressType('shipping');
            $address = $address->generateEntity();

            $ship_address = [];
            
            for ($i = 0; $i < 3; $i++) {
                $ship_address[$i] = substr($address['full_address'], $i*35, 35);
            }

            $carrier_id = $this->invoice->SalesShipment->getCarrierId();
            
            $sql = "SELECT carrier_name
                        from shipping_carrier 
                        where carrier_id = ".$carrier_id;
            $result = $companyModel->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(Database::FETCH_ASSOC);
            $carrierResult = $result->fetchAll()[0];
            $carrier_name = $carrierResult['carrier_name'];  

            $customer = $this->invoice->SalesOrder->SalesCustomer;
            // for cust_name1 = add carrier name
            $this->ship_name_1 = substr($address['first_name'], 0, 35);
            $this->ship_name_2 = substr($address['last_name'], 0, 35)." - ".$carrier_name;
            $this->ship_address_1 = $ship_address[0];
            $this->ship_address_2 = (!empty($ship_address[1])) ? $ship_address[1] : "";
            $this->ship_address_3 = (!empty($ship_address[2])) ? $ship_address[2] : "";
            $this->ship_city = substr($address['city_name'], 0, 35);
            $this->ship_portal = "";
            $this->ship_phone = $address['phone'];
            $this->ship_email = substr($customer->getCustomerEmail(), 0, 35);
            $this->bill_block = "";
            $this->dlv_block = "";
        }

        $store_code = $this->invoice->getStoreCode();
        if (substr($store_code, 0, 4) == "1000") {
            $store_code = substr($store_code,4);
        }
        // get the company profile by store_code
        $companyProfile = $companyModel->getCompanyByStoreCode($store_code);

        
        if (substr($store_code,0,2) == 'DC' && strlen($store_code) > 2) {
            $store_code = substr($store_code,2);
        }
        $this->site = $store_code;


        
        if(!empty($companyProfile)){
            $this->order_type = 'B2B_'.$companyProfile["company_code"];
            $this->company_code = $companyProfile["company_code"];
            $this->source = "E_COMMERCE_".$companyProfile["company_code"]."_B2B";
            $this->sales_id = $companyProfile["salesman_code"];
            $this->sales_office = $companyProfile["sales_office"];
            $this->sales_grp = $companyProfile["sales_group"];
        }else{
            $this->order_type = "B2B_ODI";
            $this->sales_id = "";
            $this->sales_office = "";
            $this->sales_grp = "";
            $this->source = "E_COMMERCE_ODI_B2B";
            $this->company_code = "";
        }

        if (!empty($this->creditMemo)) {
            $this->order_id .= "_".$this->creditMemo['credit_memo_count'];
        }
    }

    public function generateDCParamsCONSB2B()
    {
        $productDetail = $this->generateDCProductDetailCONSB2B();
        $thisArray = get_object_vars($this);
        $b2b_order_detail = array();
        $underscoreParams = array('sales_grp');

        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if($key == "company_code") {
                continue;
            }
            if(is_scalar($val)) {
                if(in_array($key,$underscoreParams)){
                    $currentKey = strtoupper($key);
                }else{
                    $currentKey = strtoupper(str_replace("_", "", $key));
                }

                $b2b_order_detail[$currentKey] = $val;
            }  
        }

        $parameter = array(
            "HEADER" => $b2b_order_detail,
            "T_ITEM" => $productDetail,
            "company_code" => $this->company_code
        );

        $this->parameter = $parameter;
    }

    public function generateDCProductDetailCONSB2B()
    {
        $productDetail = array();

        $idProductDetail = 0;

        $totalOriginalPrice = 0;
        $totalPrice = 0;
        if(!empty($this->invoiceItems)) {
            /**
             * @var $item \Models\SalesInvoiceItem
             */
            foreach($this->invoiceItems as $item) {
                $idProductDetail++;

                $crmItem = array();
                $itemSku = $item->getSku();

                if (!empty($this->creditMemo)) {
                    $crmItem = $this->findCRMItem($itemSku);

                    if (empty($crmItem)) {
                        continue;
                    }
                }

                $finalUnitPrice = (int)$item->getSellingPrice() - (int)$item->getDiscountAmount();
                $unitPrice = ($finalUnitPrice <= 0) ? 1 : $finalUnitPrice;

                $quantity = intval(((empty($crmItem)) ? $item->getQtyOrdered() : $crmItem['qty_refunded']));
                $salesPrice = $unitPrice * $quantity;
                $totalPrice += $salesPrice;

                $salesOrderItem = $item->SalesOrderItem;
                $originalUnitPrice = (int)$salesOrderItem->getNormalPrice();
                $originalPrice = $originalUnitPrice  * $quantity;
                $totalOriginalPrice += $originalPrice;

                $discount = round((((float)$originalUnitPrice - (float)$unitPrice) / (float)$originalUnitPrice) * 100.0, 0);

                $store_code = $item->getStoreCode();
                $productSite = $store_code;
                if (substr($store_code, 0, 4) == "1000") {
                    $store_code = substr($store_code,4);
                }
                if (substr($store_code,0,2) == 'DC' && strlen($store_code) > 2) {
                    $productSite = substr($store_code,2); 
                }

                preg_match_all('/([a-zA-Z0-9]+)-PRE/', strtoupper($item->getSku()), $match);
                if (count($match) >= 2 && isset($match[1][0])) {
                    $itemSku = $match[1][0];
                }

                $salesUom = \Helpers\ProductHelper::getSalesUom($itemSku);

                $poNumber = '';
                $poLevelItem = '';

                $productDetail[] = array(
                    "ID" => $idProductDetail,
                    "PRODUCTNO" => $itemSku,
                    "SALESPRICE" => intval($unitPrice),
                    "QUANTITY" => (empty($crmItem)) ? $quantity : -1 * $quantity,
                    "UNIT" => $salesUom,
                    "CURRENCY" => "IDR", // For now we only using IDR for transaction
                    "STGE_LOC" => "1021",
                    "SITE" => $productSite,
                    "SITE_VENDOR" => "",
                    "CONDTYPE" => "",
                    "INDENT" => "",
                    "VENDOR" => "", 
                    "CONFIRMNO" => "", 
                    "PONUMBER" => $poNumber,
                    "POLEVELITEM" => $poLevelItem,
                    "SERIALNUMBER" => "",
                    "ORIGINALPRICE" => $originalUnitPrice,
                    "DISCOUNT" => number_format($discount,0, '.', ''),
                );
            }
        }

        $this->original_price = number_format($totalOriginalPrice,0, '.', '');

        if (!empty($this->creditMemo)) {
            $shippingAmount = 0.0;

            $this->ship_cost = number_format($shippingAmount, 0, '.', '');

            $grandPrice = $totalPrice + $shippingAmount;
            $this->grand_price = number_format($grandPrice,0, '.', '');
        }

        return $productDetail;
    }

    public function createCONSB2B() {
        $nsq = new \Library\Nsq();
        $orderInfo = $this->parameter["HEADER"]["ORDERID"];
        $message = [
            "data" => json_encode($this->parameter),
            "message" => "calculateCONSB2B",
            "invoice_no" => $this->invoice->getInvoiceNo(),
            "order_no" => explode('_', $orderInfo)[0],
            "order_id" => $orderInfo
        ];
        $nsq->publishCluster('transactionSap', $message);
    }
}