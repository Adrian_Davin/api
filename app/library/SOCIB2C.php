<?php
/**
 * Library to send ruparupa order to SAP system
 */

namespace Library;

use Phalcon\Db as Database;
use Helpers\LogHelper;

class SOCIB2C
{
    protected $order_id;
    protected $order_date;
    protected $order_type = "B2C";
    protected $cust_name1;
    protected $cust_name2;
    protected $email;
    protected $phone;
    protected $address1;
    protected $address2;
    protected $address3;
    protected $city;
    protected $sales_id = "";
    protected $sales_office = "";
    protected $sales_grp = "";
    protected $kode_jalur;
    protected $country;
    protected $postal_code;
    protected $ship_cost;
    protected $grand_price;
    protected $partial_ship;
    protected $remark;    
    protected $cust_no = "";
    protected $source = "E_COMMERCE_MAGENTO_B2C";
    protected $req_dlv_date;
    protected $ship_condition;
    protected $ship_cost_gc;
    protected $dp_value;
    protected $payer_name1 = "";
    protected $payer_name2 = "";
    protected $payer_addr1 = "";
    protected $payer_addr2 = "";
    protected $payer_addr3 = "";
    protected $payer_post_code = "";
    protected $payer_city = "";
    protected $payer_province = "";
    protected $payer_npwp = "";
    protected $payer_email = "";
    protected $bill_block;
    protected $dlv_block;    
    protected $employee_reff;
    protected $invoice_id;
    protected $site;

    protected $company_code = 'ODI';

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    protected $parameter= array();

    public function __construct($invoice = null)
    {
        if(!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }

    /**
     * @param array $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    public function getOrderType()
    {
        return $this->order_type;
    }

    public function prepareDCParamsSOCIB2C()
    {
        $companyModel = New \Models\CompanyProfileDC();
        if(empty($this->invoice)) {
            return false;
        }

        /**
         * @var $currentOrder \Models\SalesOrder
         */
        $currentOrder = $this->invoice->SalesOrder;
        $orderDate = date_create($currentOrder->getCreatedAt());

        /**
         * @var $shippingAddress \Models\SalesOrderAddress
         */
        $shippingAddress = null;
        $carrier_id = 0;
        $carrier_code = "";

        if(!empty($this->invoice->SalesShipment)) {
            $shippingAddress = $this->invoice->SalesShipment->SalesOrderAddress->getDataArray();
            $carrier_id = $this->invoice->SalesShipment->getCarrierId();
            
            $sql = "SELECT carrier_name, carrier_code
                        from shipping_carrier 
                        where carrier_id = ".$carrier_id;
            $result = $companyModel->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(Database::FETCH_ASSOC);
            $carrierResult = $result->fetchAll()[0];
            $carrier_name = $carrierResult['carrier_name'];
            $carrier_code = $carrierResult['carrier_code'];
        }

        $remarkDelivId = $carrier_code;
        if (strpos(strtolower($remarkDelivId), 'ownfleet') !== false) {
            $remarkDelivId = "OWNFLEET";
        }

        $interval = new \DateInterval('P3D');
        $interval->invert = 0;

        $typOrd = "RUPARUPA";
        if($currentOrder->getOrderType() === "b2b_informa" ){       
            $interval = new \DateInterval('P2D');
            $interval->invert = 0;
            $typOrd = "B2B INFORMA";
        } else if ($currentOrder->getOrderType() === "b2b_ace"){       
            $interval = new \DateInterval('P2D');
            $interval->invert = 0;
            $typOrd = "B2B ACE";
        }

        $delivDateTime = clone $orderDate;
        if ($carrier_id !== 0) {
            foreach (explode(",", getenv('INSTANT_SAMEDAY_CARRIER_ID')) as $carrier_instant_id) {
                if (strval($carrier_id) == $carrier_instant_id) {
                    $interval = new \DateInterval('PT23H59M');
                    $interval->invert = 0;
                    break;
                }
            }
        }
        $delivDateTime = $delivDateTime->add($interval);
        
        $this->req_dlv_date = date_format($orderDate, "Ymd");

        $delivDateStr = date_format($delivDateTime, "d.m.Y");
        $delivTimeStr = date_format($delivDateTime, "H:i:s");

        /*
         * Preorder if item not preorder remove prefix pre from order no
         */
        $invoiceDetail = !empty($this->invoice->toArray())? $this->invoice->toArray() : array();
        if(substr( $invoiceDetail['invoice_no'], 0, 3 ) === "PRE"){
            $orderId = $currentOrder->getOrderNo();
        }else{
            $orderId = str_replace("PRE","",$currentOrder->getOrderNo());
        }

        // appending invoiceId to maintain uniqueness in case of 1 order multiple DC
        // switch odi and invoice no, to handle multiple DC 
        // for cust_name1 = add carrier name
        $this->order_id =  $orderId . '_' .  substr($this->invoice->getInvoiceNo(),strlen($this->invoice->getInvoiceNo())-3, 3);
        $this->order_date = date('Ymd',strtotime($this->invoice->getCreatedAt()));
        
        $this->cust_name1 =  $shippingAddress['first_name'];
        $this->cust_name2 =  $shippingAddress['last_name']." - ".$carrier_name;
        $this->email = $currentOrder->SalesCustomer->getCustomerEmail();
        $this->phone = !empty($shippingAddress['phone']) ? $shippingAddress['phone'] : "";
        
        // https://ruparupa.atlassian.net/browse/DCD-163
        // Address1 : max 60 char
        // Address2 : max 40 char
        // Address3 : max 40 char
        // City : max 40 char
        $kecamatanName = "";
        if(isset($shippingAddress['kecamatan']['kecamatan_name'])){
            $kecamatanName = ", ".$shippingAddress['kecamatan']['kecamatan_name'];
        }

        $cityName = "";
        if(isset($shippingAddress['city']['city_name'])){
            $cityName = $shippingAddress['city']['city_name'];
        }

        $addressXml = $shippingAddress['full_address'] . $kecamatanName;
        $this->address1 = mb_substr($addressXml,0,60, "utf-8");
        $this->address2 = ($addr2 = mb_substr($addressXml,60,40, "utf-8")) != false ? $addr2 : "";
        $this->address3 = ($addr3 = mb_substr($addressXml,100,40, "utf-8")) != false ? $addr3 : "";
        $this->city = $cityName;
        
        $this->kode_jalur = (isset($shippingAddress['kecamatan']['kode_jalur'])) ? $shippingAddress['kecamatan']['kode_jalur'] : '';
        $this->country  = $shippingAddress['country']['country_code'];
        $this->postal_code  = !empty($shippingAddress['post_code']) ? $shippingAddress['post_code'] : "00000";

        $this->ship_cost = number_format( ($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount()) ,0, '.', '');
        
        $grandPrice = $this->invoice->getGiftCardsAmount() + $this->invoice->getGrandTotal();
        $this->grand_price = number_format($grandPrice,0, '.', '');
        $this->partial_ship = '';

        /**
         * Send AWB as Shipping Remark to SAP
         */
        $this->remark = "TYP_ORD : ".$typOrd."\nDEL_ID : ".$remarkDelivId."\nDEL_TIME : ".$delivTimeStr."\nDEL_DATE : ".$delivDateStr;
        if(!empty($this->invoice->SalesShipment)) {
            //foreach($this->invoice->SalesShipment as $rowObjShipment) {
            //    $carrierCode = !empty($rowObjShipment->ShippingCarrier->getCarrierCode()) ? $rowObjShipment->ShippingCarrier->getCarrierCode() : "";
            //    $this->remark = $carrierCode . $rowObjShipment->getTrackNumber();
            //}
            $carrierCode = !empty($this->invoice->SalesShipment->ShippingCarrier->getCarrierCode()) ? $this->invoice->SalesShipment->ShippingCarrier->getCarrierCode() : "";
            $this->remark = $this->remark."\n".$carrierCode . $this->invoice->SalesShipment->getTrackNumber();
        }
        $this->cust_no = '';
        

        // set request delivery date different per company
        // disable by ticket https://ruparupa.atlassian.net/browse/DCD-129
        // $additionalRdd = getenv('ADDITIONAL_RDD_' . $currentOrder->getCompanyCode() ? $currentOrder->getCompanyCode() : 'ODI');
        // if (intval($additionalRdd) > 0) {
        //     $this->req_dlv_date = date('Ymd', strtotime('+'.intval($additionalRdd).' days', strtotime($this->invoice->getCreatedAt())));
        // } else {
        //     $this->req_dlv_date = date('Ymd', strtotime($this->invoice->getCreatedAt()));
        // }

        $this->ship_condition = "Z6";
        $this->ship_cost_gc = '';
        $this->dp_value = '';   

        /**
         * @todo : Implement payer here
         */
        $this->payer_name1 = "";
        $this->payer_name2 = "";
        $this->payer_addr1 = "";
        $this->payer_addr2 = "";
        $this->payer_addr3 = "";
        $this->payer_post_code = "";
        $this->payer_city = "";
        $this->payer_province = "";
        $this->payer_npwp = "";
        $this->payer_email = "";
        $this->bill_block = "";
        $this->dlv_block = "";
        $this->employee_reff = "";

        // Genereate items
        $this->invoiceItems = $this->invoice->SalesInvoiceItem;
                
        $this->invoice_id = $this->invoice->getInvoiceNo();

        $store_code = $this->invoice->getStoreCode();
        if (substr($store_code, 0, 4) == "1000") {
            $store_code = substr($store_code,4);
        }
        
        // get the company profile by store_code
        $companyProfile = $companyModel->getCompanyByStoreCode($store_code);

        if (substr($store_code,0,2) == 'DC' && strlen($store_code) > 2) {
            $store_code = substr($store_code,2);
        }
        $this->site = $store_code;
        
        if (!empty($companyProfile) ){
            $this->order_type = "B2C_".$companyProfile["company_code"];
            $this->sales_id = $companyProfile["salesman_code"];
            $this->sales_office = $companyProfile["sales_office"];
            $this->sales_grp = $companyProfile["sales_group"];
            $this->source = "E_COMMERCE_".$companyProfile["company_code"]."_B2C";
            $this->company_code = $companyProfile["company_code"];            
        }else {
            $this->order_type = "B2C_ODI";
            $this->sales_id = "";
            $this->sales_office = "";
            $this->sales_grp = "";
            $this->source = "E_COMMERCE_ODI_B2C";
            $this->company_code = "";
        }

        if($currentOrder->getOrderType() === "b2b_informa" ){       
            $this->sales_id = "022424";
            $this->sales_grp = "H64";
            $this->sales_office = "H300";
            $this->order_type = "B2C_HCI";
            $this->source = "E_COMMERCE_HCI_B2C";
        } else if ($currentOrder->getOrderType() === "b2b_ace") {
            $this->sales_id = "168836";
            $this->sales_grp = "A00";
            $this->sales_office = "A300";
            $this->order_type = "B2C_AHI";
            $this->source = "E_COMMERCE_AHI_B2C";
        } else if ($this->invoice->getStoreCode() == "DC") {
            $this->sales_id = getenv('DC_SALES_ID');
            $this->sales_grp = "O00";
            $this->sales_office = "O300";
        }

        $salesOrderItemFlatModel = new \Models\SalesOrderItemFlat;

        foreach ($this->invoiceItems as $item) {
            $soItemFlatData = $salesOrderItemFlatModel->findFirst("sales_order_item_id = " . $item->sales_order_item_id);
            if ($soItemFlatData && $soItemFlatData->department_name === getenv("ROLKA_DEPARTMENT_NAME")) {
                $this->order_type = "B2C_HCI";
                $this->sales_id = getenv("ROLKA_DEPARTMENT_SALES_ID");
                $this->sales_office = "H300";
                $this->sales_grp = "H65";
                $this->source = "E_COMMERCE_HCI_B2C";
                $this->company_code = "HCI";
                
                if ($carrierCode == "ownfleet_informa") {
                    $carrierCode = "OWNFLEET";
                }

                $orderNo = $currentOrder->getOrderNo();
                $typeOrder = 'RUPARUPA';
                if (substr($orderNo, 0, 4) == "ODIT") {
                    $typeOrder = "TOKOPEDIA";
                } else if (substr($orderNo, 0, 4) == "ODIS") {
                    $typeOrder = "SHOPEE";
                } else if (substr($orderNo, 0, 4) == "ODIK") {
                    $typeOrder = "TIKTOK";
                }
                
                break;
            }
        }
    }

    public function generateDCParamsSOCIB2C()
    {
        $thisArray = get_object_vars($this);
        $b2c_order_detail = array();
        $underscoreParams = array('sales_grp','employee_reff');
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if($key == "company_code") {
                continue;
            }
            if(is_scalar($val)) {
                if(in_array($key,$underscoreParams)){
                    $currentKey = strtoupper($key);
                }else{
                    $currentKey = strtoupper(str_replace("_", "", $key));
                }                
                $b2c_order_detail[$currentKey] = $val;
            }
        }

        $productDetail = $this->generateDCProductDetailSOCIB2C();

        $parameter = array(
            "b2c_order_detail" => $b2c_order_detail,
            "b2c_product_detail" => $productDetail,
            'company_code' => $this->company_code
        );

        $parameter = array("Order_Data_B2C" => $parameter);
        $this->parameter = $parameter;
    }

    public function generateDCProductDetailSOCIB2C()
    {
        $productDetail = array();
        $mdrSkuArr = array();

        $idProductDetail = 0;
        $invoiceModel = new \Models\SalesInvoice();
        if(!empty($this->invoiceItems)) {
            /**
             * @var $item \Models\SalesInvoiceItem
             */
            foreach($this->invoiceItems as $item) {

                preg_match_all('/([a-zA-Z0-9]+)SET([a-zA-Z0-9]+)/', $item->getSku(), $match);
                if (count($match) >= 3 && isset($match[1][0]) && isset($match[2][0])) {
                    $itemSku = $match[1][0];
                    $qtySet = $match[2][0];
                } else {
                    preg_match_all('/([a-zA-Z0-9]+)-PRE/', strtoupper($item->getSku()), $match);
                    if (count($match) >= 2 && isset($match[1][0])) {
                        $itemSku = $match[1][0];
                    } else {
                        $itemSku = $item->getSku();
                    }
                    $qtySet = 1;
                }
                $qtyOrdered = $item->getQtyOrdered() * $qtySet;                

                $store_code = $item->getStoreCode();
                $productSite = $store_code;

                if (substr($store_code, 0, 4) == "1000") {
                    $store_code = substr($store_code,4);
                }
                if (substr($store_code,0,2) == 'DC' && strlen($store_code) > 2) {
                    $productSite = substr($store_code,2);
                }

                $sql = "SELECT coalesce(sum(voucher_amount_difference),0) as selisih, 
                               coalesce(sum(split_voucher_amount_difference),0) as selisih_split_voucher,
                               coalesce(sum(split_discount_amount_difference),0) as selisih_split_discount
                        from sales_order_item_giftcards 
                        where sales_order_item_id = ".$item->getSalesOrderItemId();
                $result = $invoiceModel->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(Database::FETCH_ASSOC);                
                $selisihResult = $result->fetchAll()[0];
                $selisih = $selisihResult['selisih'];
                $selisihSplitVoucher = $selisihResult['selisih_split_voucher'];
                $selisihSplitDiscount = $selisihResult['selisih_split_discount'];

                $idProductDetail++;

                $finalUnitPrice = (int)$item->getSellingPrice() - (int)$item->getDiscountAmount();
               
                $orderType = $item->SalesOrderItem->SalesOrder->getOrderType();
                // Include price zone for b2b_ace/b2b_informa order type
                if ($orderType == "b2b_ace" || $orderType == "b2b_informa") {
                    $finalUnitPrice += (int)$item->getHandlingFeeAdjust();
                }
               
                $unitPrice = (($finalUnitPrice <= 0) ? 1 : $finalUnitPrice) / $qtySet;
                $salesPrice = ($unitPrice * $qtyOrdered) + $selisihSplitDiscount ;                

                $salesUom = \Helpers\ProductHelper::getSalesUom($itemSku);
                $salesOrderItemFlatModel = new \Models\SalesOrderItemFlat;
                $soItemFlatData = $salesOrderItemFlatModel->findFirst("sales_order_item_id = " . $item->sales_order_item_id);

                $stgeLoc = "1021";
                if ($item->getStoreCode() == 'DC') {
                    $stgeLoc = "1000";
                    $productSite = "O001";
                }

                $productDetail[] = array(
                    "ID" => $idProductDetail,
                    "PRODUCTNO" => $itemSku,                    
                    "SALESPRICE" => intval($salesPrice),
                    "QUANTITY" => $qtyOrdered,
                    "UNIT" => $salesUom,
                    "CURRENCY" => "IDR", // For now we only using IDR for transaction
                    "STGE_LOC" => $stgeLoc,
                    "SITE" => $productSite,
                    "SITE_VENDOR" => "",
                    "CONDTYPE" => "",
                    "INDENT" => "",
                    "VENDOR" => "",
                    "CONFIRMNO" => "",
                    "SERIALNUMBER" => ""                    
                );

                // if(!empty((int)$this->invoice->SalesOrder->getGiftCardsAmount())) {
                //     $unitPriceGc = $item->getGiftCardsAmount() / $qtySet;

                //     // zvo4 item price can't be higher than item unit price
                //     if($unitPriceGc>$unitPrice){
                //         $unitPriceGc = $unitPrice;
                //     }
                //     $salesProceGc = ($unitPriceGc * $qtyOrdered) + $selisih + $selisihSplitVoucher;

                //     $productDetail[] = array(
                //         "ID" => $idProductDetail,
                //         "PRODUCTNO" => $itemSku,                    
                //         "SALESPRICE" => intval($salesProceGc),
                //         "QUANTITY" => $qtyOrdered,
                //         "UNIT" => $salesUom,
                //         "CURRENCY" => "IDR", // For now we only using IDR for transaction
                //         "STGE_LOC" => "1021",
                //         "SITE" => $productSite,
                //         "SITE_VENDOR" => "",
                //         "CONDTYPE" => "ZV04",
                //         "INDENT" => "",
                //         "VENDOR" => "",
                //         "CONFIRMNO" => "",
                //         "SERIALNUMBER" => ""  
                //     );

                // }

                // Check if current sku needs to apply MDR on customer
                $isApplyMdrOnCust = \Helpers\ProductHelper::isApplyMdrOnCust($itemSku);

                // If it needs to apply MDR, check if it has MDR fee on customer
                $salesOrderItemMdrArr = array();
                if ($isApplyMdrOnCust) {
                    $salesOrderItemMdrModel = new \Models\SalesOrderItemMdr();
                    $salesOrderItemMdr = $salesOrderItemMdrModel->findFirst("sales_order_item_id = " . $item->getSalesOrderItemId());
                    if($salesOrderItemMdr){
                        $salesOrderItemMdrArr = $salesOrderItemMdr->toArray();
                    }
                }

                // If it has MDR fee on customer, get MDR sku based on MDR fee, then prepare the parameter to append to product detail
                if (!empty($salesOrderItemMdrArr) && !empty($salesOrderItemMdrArr['value_customer'] && !empty($salesOrderItemMdrArr['tiering_id']))) {
                    $mdrPaymentTieringModel = new \Models\MdrPaymentTiering();
                    $mdrPaymentTiering = $mdrPaymentTieringModel->findFirst("tiering_id = " . $salesOrderItemMdrArr['tiering_id']);
                    if($mdrPaymentTiering){
                        $mdrPaymentTieringArr = $mdrPaymentTiering->toArray();
                    }

                    $mdrSku = "";
                    if (!empty($mdrPaymentTieringArr)) {
                        $mdrSku = $mdrPaymentTieringArr['sku'];

                        $mdrSkuArr[] = array(
                            "sku" => $mdrSku,
                            "price" => $salesOrderItemMdrArr["value_customer"],
                            "stge_loc" => $stgeLoc,
                            "site" => $productSite
                        );
                    }
                }
            }

            // Append MDR sku to product detail
            if (!empty($mdrSkuArr)) {
                foreach($mdrSkuArr as $mdrSku) {
                    $idProductDetail++;

                    $productDetail[] = array(
                        "ID" => $idProductDetail,
                        "PRODUCTNO" => $mdrSku["sku"],
                        "SALESPRICE" => intval($mdrSku["price"]),
                        "QUANTITY" => 1,
                        "UNIT" => "",
                        "CURRENCY" => "IDR", 
                        "STGE_LOC" => $mdrSku["stge_loc"],
                        "SITE" => $mdrSku["site"],
                        "SITE_VENDOR" => "",
                        "CONDTYPE" => "",
                        "INDENT" => "",
                        "VENDOR" => "", 
                        "CONFIRMNO" => "", 
                        "SERIALNUMBER" => "" 
                    );
                }
            } 
        }

        return $productDetail;
    }

    public function createSOCIB2C()
    {
        // Get sales invoice for this invoice
        $invoiceStoreCode = $this->invoice->getStoreCode();

        $data_param = json_encode($this->parameter);

        if ($data_param == false) {
            LogHelper::log('create_soci_b2c', print_r($this->parameter, true), 'ENCODE PARAM');
        }

        // Only create SOB2C if invoice store code is DC
        if(substr($invoiceStoreCode, 0, 2 ) === "DC"){
            $nsq = new \Library\Nsq();
            $message = [
                "data" => $data_param,
                "message" => "createSOCIB2C",
                "invoice_no" => $this->invoice->getInvoiceNo()
            ];
            $nsq->publishCluster('transactionSap', $message);    
        }
        
        return true;
    }
}