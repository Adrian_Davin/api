<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/10/2017
 * Time: 2:07 PM
 */

namespace Library;

use Helpers\LogHelper;
use Helpers\GeneralHelper;
use Models\CustomerSelmaOtpTemp;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class SelmaApp
{
    protected $invoice_no;
    protected $order_no;
    protected $credit_memo_no;
    protected $amount;

    protected $errorCode;
    protected $errorMessages;
    protected $customerNewModel;
    protected $appVersion;
    protected $errorData = [];

    public $voucherId;

    /**
     * @return mixed
     */

    protected $customer;

    protected $customerSelmaDetails;

    protected $marketingLib;

    protected $kawanLamaSystemLib;

    public function __construct()
    {
        $this->customer = new \Models\Customer();
        $this->customerSelmaDetails = new \Models\CustomerSelmaDetails();
        $this->marketingLib = new Marketing();
        $this->kawanLamaSystemLib = new KawanLamaSystem();
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getErrorData() 
    {
        return $this->errorData;
    }

    public function setFromArray($data_array = array()) 
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function setAppVersion($params)
    {
        $this->appVersion = isset($params["x-app-version"]) ? $params["x-app-version"] : "";
    }


    public function customerLogin($data_array = array())
    {
        $logFile = "selma/selma_customer_login";
        $logFileError = "selma/selma_customer_login_error";
        // TODO: add login attempt validation when wrong password more than 5 times
        $validate = $this->validateRequired($data_array, ["email", "password"]);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        $password = $data_array["password"];
        $email = $data_array["email"];

        $requestPayload["email"] = $email;
        $requestPayload["passwd"] = $password;
        $requestPayload["P_BU"] = "SELMA";

        $customer = false;
        $customer = $this->getCustomerData("email = '" . $requestPayload["email"] . "'");
        if(!$customer && $this->getErrorCode() != 404) {
            $this->logReqRes($logFileError, "getCustomerData", $requestPayload, $customer, "", $this->errorMessages);
            return false;
        }
        if ($customer["status"] == 1) {
            $this->errorCode = 400;
            $this->errorMessages = "Akun sudah dihapus dan tidak dapat digunakan kembali.";
            $this->logReqRes($logFileError, "selmaLoginDeletedAccount", $requestPayload, $customer, "", $this->errorMessages);
            return false;
        }

        $this->errorCode = null;
        $this->errorMessages = null;

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $requestPayload, $encryptedPayload, "", $this->errorMessages);
            return false;
        }

        $selmaLogin = $selmaWebAPI->loginCustomer($encryptedPayload, "");
        $this->logReqRes($logFile, "loginCustomer", $requestPayload, $selmaLogin);
        if (!$selmaLogin) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages() !== "" ? $selmaWebAPI->getErrorMessages() : "Login Information Tidak Valid";
            $this->logReqRes($logFileError, "loginCustomer", $requestPayload, $selmaLogin, "", $this->errorMessages);
            return false;
        }

        $customerData = $this->getCustomerDataSelma($selmaLogin);
        if (!empty($this->errorCode)) {
            return false;
        }
        $useMemberPhone = false;
        $custPhone = $customerData['notelp'];
        $name = $this->getFirstAndLastName($customerData["fname"]);

        $customerEntities = [
            "first_name" => $name["first_name"],
            "last_name" => $name["last_name"],
            "email" => $customerData["email"],
            "password" => $password,
            "company_code" => "SLM",
            "status" => 10,
            "last_login" => date("Y-m-d H:i:s"),
        ];

        $customerSelmaDetailsEntities = [
            "selma_uid" => $customerData["uid"],
            "session_id" => $selmaLogin["sessionid"],
        ];

        $memberData = array();
        $memberData["P_CUST_ID"] = "";
        $memberData["P_CARD_ID"] = "";
        $selmaCardKey = "selma_customer_member_no";
        $isPaidMember = false;

        if ($customerData['is_member'] != 0) {
            $memberData = $this->getMemberDataSelma($selmaLogin["uid"]);
            if (!empty($this->errorCode)) {
                return false;
            }
            if (isset($memberData["P_JENIS_KELAMIN"])) {
                $customerEntities["gender"] = $this->getSexString($memberData["P_JENIS_KELAMIN"]);
            }

            if (isset($memberData["P_TANGGAL_LAHIR"])) {
                $dateTime = date_create_from_format('d/m/Y', $memberData["P_TANGGAL_LAHIR"]);
                $customerEntities["birth_date"] = date_format($dateTime, 'Y-m-d');
            }

            if ($memberData["P_NO_KTP"]) {
                $customerSelmaDetailsEntities["ktp"] = $memberData["P_NO_KTP"];
            }

            if ($memberData["P_STATUS_PERKAWINAN"]) {
                $customerSelmaDetailsEntities["marital_status"] = $memberData["P_STATUS_PERKAWINAN"];
            }

            if (substr($memberData["P_CARD_ID"], 0, 3) == "TIM") {
                $selmaCardKey = "selma_customer_tmp_member_no";
            } else {
                $isPaidMember = true;
            }

            if (($custPhone == "" || $custPhone == "-") && ($memberData["P_NO_HP"] != "0000")) {
                $custPhone = $memberData["P_NO_HP"];
                $useMemberPhone = true;
            }

            $nowDate = new \DateTime("now");
            $expDate = new \DateTime($memberData["P_EXPIRED_DATE"]);
            $customerEntities["group"] = [
                "SLM" => $memberData["P_CARD_ID"],
                "SLM_expiration_date" => $expDate->format('Y-m-d 23:59:59'),
                "SLM_is_verified" => $nowDate <= $expDate ? 10 : 5,
            ];
        }

        if ($custPhone == "" || $custPhone == "-") {
            $isPhoneValid = false;
        } else {
            $phone = $this->ruparupaFormatPhone($custPhone);
            $customerEntities["phone"] = $phone;
            $isPhoneValid = true;
        }
        $customerSelmaDetailsEntities["selma_customer_id"] = $memberData["P_CUST_ID"];
        $customerSelmaDetailsEntities[$selmaCardKey] = $memberData["P_CARD_ID"];

        $customerNewModel = new \Models\Customer();
        $customer = false;
        $isMigrated = false;

        $customer = $this->getCustomerData("email = '" . $customerData["email"] . "' AND status = 10 AND company_code = 'SLM'");
        if (!$customer) {
            if ($this->getErrorCode() != 404) {
                return false;
            }
            $customerSelmaDetailsEntities["is_migrated"] = 10;
            $isMigrated = true;

            $customerNewModel->setRegisteredBy("selma (mobile apps)");
        } else {
            $customerEntities["customer_id"] = $customer["customer_id"];
            $customerSelmaDetail = $this->getCustomerDetail("customer_id = '" . $customer["customer_id"] . "'");
            if (!$customerSelmaDetail) {
                if ($this->getErrorCode() != 404) {
                    return false;
                }
            } else {
                $customerSelmaDetailsEntities["customer_selma_details_id"] = $customerSelmaDetail["customer_selma_details_id"];
            }
        }

        $customerNewModel->setFromArray($customerEntities);
        if (!$customerNewModel->saveData("register_selma_from_login")) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal menyimpan Customer Selma";
            $this->logReqRes($logFileError, "saveData", $customerEntities, false, "", $this->errorMessages);
            return false;
        }

        $customerSelmaDetailsEntities["customer_id"] = $customerNewModel->getCustomerId();

        $this->customerSelmaDetails->setFromArray($customerSelmaDetailsEntities);
        if (!$this->customerSelmaDetails->saveData("register_selma_from_login")) {
            $this->errorCode = $this->customerSelmaDetails->getErrorCode();
            $this->errorMessages = $this->customerSelmaDetails->getErrorMessages();
            $this->logReqRes($logFileError, "saveData", $customerSelmaDetailsEntities, false, "", $this->errorMessages);
            return false;
        }

        $customerSelmaData = $this->getCustomerData("customer_id = '" . $customerNewModel->getCustomerId() . "'");
        if (!$customerSelmaData) {
            if ($this->getErrorCode() != 404)
                return false;
        } else {
            if (isset($customerSelmaData["gender"])) {
                $customerData["sex"] =  $this->getSexId($customerSelmaData["gender"]);
            }
            if (isset($customerSelmaData["birth_date"])) {
                $customerData["birth_date"] =  $customerSelmaData["birth_date"];
            }
        }

        $customerSelmaDetailData = $this->getCustomerDetail("customer_id = '" . $customerNewModel->getCustomerId() . "'");
        if (!$customerSelmaDetailData) {
            if ($this->getErrorCode() != 404)
                return false;
        } else {
            if (isset($customerSelmaDetailData["ktp"])) {
                $customerData["ktp"] = $customerSelmaDetailData["ktp"];
            }

            if (isset($customerSelmaDetailData["marital_status"])) {
                $customerData["marital_status"] = $customerSelmaDetailData["marital_status"];
            }
        }

        if ($isMigrated) {
            // Update last login to now on migrate
            $customerUpdateModel = new \Models\Customer();
            $customerEntities["customer_id"] = $customerNewModel->getCustomerId();
            $customerEntities["last_login"] = date("Y-m-d H:i:s");
            $customerUpdateModel->setFromArray($customerEntities);
            if (!$customerUpdateModel->saveData("update_selma_from_login")) {
                $this->errorCode = 400;
                $this->errorMessages = "Gagal menyimpan Customer Selma";
                $this->logReqRes($logFileError, "saveData", $customerEntities, false, "", $this->errorMessages);
                return false;
            }
        }

        $currentCartId = empty($data_array['cart_id']) ? "" : $data_array['cart_id'];
        $cartLib = new \Library\Cart();
        $storeCodeNewRetail = isset($data_array['store_code_new_retail']) ? $data_array['store_code_new_retail'] : '';
        $respMergeCart = $cartLib->getCustomerCartId($customerNewModel->getCustomerId(), $currentCartId, "ODI", $storeCodeNewRetail);
        $shoppingCartId = !empty($respMergeCart['data']['cart_id']) ? $respMergeCart['data']['cart_id'] : "";
        $respMergeMinicart = $cartLib->getCustomerMiniCartId($customerNewModel->getCustomerId(), $currentCartId, "ODI", $storeCodeNewRetail);
        $shoppingMiniCartId = !empty($respMergeMinicart['data']['cart_id']) ? $respMergeMinicart['data']['cart_id'] : "";

        if ($useMemberPhone) {
            $successUpdatePhone = $this->changeProfilePhoneCustomerSelma($customerData["uid"], $selmaLogin["sessionid"], $customerNewModel->getCustomerId(), $customerEntities["phone"]);
            if (!$successUpdatePhone) {
                $this->logReqRes($logFileError, "changeProfilePhoneCustomerSelma", $customerSelmaDetailsEntities, false, "", $this->errorMessages);
                return false;
            }
        }

        $customerData["member"] = $memberData;
        $responseData = $this->mappingCustomerData($customerData);
        $responseData["customer_id"] = $customerNewModel->getCustomerId();
        $responseData["details"] = $customerSelmaDetailsEntities;
        $responseData["cart_id"] = $shoppingCartId;
        $responseData["minicart_id"] = $shoppingMiniCartId;
        $responseData["is_phone_valid"] = $isPhoneValid;
        $responseData["use_member_phone"] = $useMemberPhone;
        $responseData["is_paid_member"] = $isPaidMember;

        if ($isPhoneValid) {
            // In here we need to pass the value of email, phone and company_code to queue-v2 for linking customer
            $customerHelper = new \Helpers\CustomerHelper();
            $customerHelper->linkingCustomerUsingNsq($customerEntities["email"], $customerEntities["phone"], "SLM");
        }

        return $responseData;
    }

    public function registerCustomerSelma($sending_welcome_email = true, $params = array())
    {
        $logFile = "selma/selma_register_customer";
        $logFileError = "selma/selma_register_customer_error";
        $storeCodeNewRetail = isset($params['store_code_new_retail']) && !empty($params['store_code_new_retail']) ? $params['store_code_new_retail'] : "";

        $requiredKey = ["name", "phone", "email", "password", "address"];
        $validate = $this->validateRequired($params, $requiredKey);
        if ($validate) {
            $this->errorCode = 400;
            $this->errorMessages = $validate;
            return false;
        }

        if (!filter_var($params["email"], FILTER_VALIDATE_EMAIL)) {
            $this->errorCode = 400;
            $this->errorMessages = "Format e-mail tidak valid";
            return false;
        }

        $formattedPhone = $this->formatPhoneNumber($params["phone"]);
        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format Nomor Handphone tidak valid";
            return false;
        }

        $customerDetailTmp = $this->getCustomerData("email = '" . $params["email"] . "'");
        if (!$customerDetailTmp && $this->getErrorCode() != 404) {
            $this->logReqRes($logFileError, "getCustomerData", $params, $customerDetailTmp, "", $this->errorMessages);
            return false;
        }
        if($customerDetailTmp["status"] == 1){
            $this->errorCode = 400;
            $this->errorMessages = "Akun sudah dihapus dan tidak dapat digunakan kembali.";
            $this->logReqRes($logFileError, "selmaRegisterDeletedAccount", $params, $customerDetailTmp, "", $this->errorMessages);
            return false;
        }
        if ($customerDetailTmp) {
            $this->errorCode = 303;
            $this->errorMessages = "E-mail sudah terdaftar. Silakan gunakan yang lain.";
            $this->logReqRes($logFileError, "getCustomerData", $params, $customerDetailTmp, "", $this->errorMessages);
            return false;
        }

        $rupaRupaPhone = $this->ruparupaFormatPhone($params["phone"]);
        $customerDetailTmp = $this->getCustomerData("phone = '" . $rupaRupaPhone . "'");
        if (!$customerDetailTmp && $this->getErrorCode() != 404) {
            return false;
        }
        if($customerDetailTmp["status"] == 1){
            $this->errorCode = 303;
            $this->errorMessages = "Akun sudah dihapus dan tidak dapat digunakan kembali.";
            $this->logReqRes($logFileError, "selmaRegisterDeletedAccount", $params, $customerDetailTmp, $this->errorMessages);
            return false;
        }
        if ($customerDetailTmp) {
            $this->errorCode = 303;
            $this->errorMessages = "Nomor HP sudah terdaftar. Silakan gunakan yang lain.";
            $this->logReqRes($logFileError, "getCustomerData", $params, $customerDetailTmp, "", $this->errorMessages);
            return false;
        }

        $genderString = null;
        if (!empty($params["gender"])){
            $formattedGender = $this->formatGender($params["gender"]);
            $genderString = $this->getSexString($formattedGender);
            if (empty($genderString)) {
                $this->errorCode = 400;
                $this->errorMessages = "Format Gender tidak valid";
                return false;
            }
        }


        $this->errorCode = null;
        $this->errorMessages = null;

        $name = $this->getFirstAndLastName($params["name"]);
        $customerEntities = [
            "first_name" => $name["first_name"],
            "last_name" => $name["last_name"],
            "phone" => $params["phone"],
            "email" => $params["email"],
            "password" => $params["password"],
            "company_code" => "SLM",
            "status" => 10,
            "gender" => $genderString,
            "is_phone_verified" => 10,
            "is_email_verified" => 0,
            "last_login" => date("Y-m-d H:i:s"),
        ];
        if (!empty($params["birth_date"])) {
            $customerEntities["birth_date"] = $params["birth_date"];
        }

        $customerNewModel = new \Models\Customer();
        $customerNewModel->setFromArray($customerEntities);
        $customerNewModel->setRegisteredBy("selma (mobile apps)");

        // Transaction
        $manager = new TxManager();
        $manager->setDbService($customerNewModel->getConnection());
        $transaction = $manager->get();

        try {
            if (!$customerNewModel->saveData("register_selma_from_login", "", $transaction)) {
                $this->errorCode = 400;
                $this->errorMessages = "Gagal menyimpan Customer Selma";
                $this->logReqRes($logFileError, "saveData", $customerEntities, false, "", $this->errorMessages);
                $transaction->rollback($this->errorMessages);
                return false;
            }

            // Format Phone
            $hoPhone = $this->formatPhoneNumberHO($params["phone"]);

            $selmaWebAPI = new \Library\SelmaWebAPI();
            $registerRequestPayload = [
                "email" => $params["email"],
                "fname" => $params["name"],
                "passwd" => $params["password"],
                "nohp" => $hoPhone,
                "address" => $params["address"],
                "provider" => "app",
                "P_BU" => "SELMA",
            ];

            $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($registerRequestPayload);
            if (!$encryptedPayload) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $registerRequestPayload, $encryptedPayload, "", $this->errorMessages);
                $transaction->rollback($this->errorMessages);
                return false;
            }

            $selmaRegisterCustomer = $selmaWebAPI->selmaRegisterCustomer($encryptedPayload);
            $this->logReqRes($logFile, "selmaRegisterCustomer", $registerRequestPayload, $selmaRegisterCustomer, "");
            if (!$selmaRegisterCustomer) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "selmaRegisterCustomer", $registerRequestPayload, $selmaRegisterCustomer, "", $this->errorMessages);
                $transaction->rollback($this->errorMessages);
                return false;
            }
        } catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        // call myprofile API for get is_member

        $selmaPCustID = "";
        $selmaPCardID = "";
        $selmaKTP = null;
        $selmaMaritalStatus = null;
        $selmaCardKey = "selma_customer_member_no";

        $customerSelmaDetailsEntities = [
            "selma_customer_id" => $selmaPCustID,
            $selmaCardKey => $selmaPCardID,
            "customer_id" => $customerNewModel->getCustomerId(),
            "ktp" => $selmaKTP,
            "marital_status" => $selmaMaritalStatus,
            "is_migrated" => 0,
        ];

        $this->customerSelmaDetails->setFromArray($customerSelmaDetailsEntities);
        if (!$this->customerSelmaDetails->saveData("register_selma_from_login")) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal menyimpan Customer Selma";
            $this->logReqRes($logFileError, "saveData", $customerSelmaDetailsEntities, false, "", $this->errorMessages);
            return false;
        }

        // In here we need to pass the value of email, phone and company_code to queue-v2 for linking customer
        $customerHelper = new \Helpers\CustomerHelper();
        $customerHelper->linkingCustomerUsingNsq($params["email"], $params["phone"], "SLM");

        $response = [
            "success" => true,
            "customer_id" => $customerNewModel->getCustomerId(),
        ];
        return $response;
    }


    public function getCustomerDetail($condition = "")
    {
        $logFileError = "selma/selma_get_customer_detail_error";
        try {
            $customer = new \Models\CustomerSelmaDetails();
            $param['conditions'] = $condition == "" ? $condition : "(" . $condition . ")";
            $result = $customer->findFirst($param);
        } catch (\Exception $e) {
            return false;
        }

        if (!empty($result)) {
            $this->errorCode = null;
            return $result->toArray();
        }
        $this->errorCode = 404;
        $this->errorMessages = "Customer detail tidak ditemukan";

        $this->logReqRes($logFileError, "EmptyData", $condition, false, "", $this->errorMessages);
        return false;
    }

    public function getCustomerData($condition = "", $companyCode = "SLM")
    {
        $logFileError = "selma/selma_get_customer_data_error";
        try {
            $customer = new \Models\Customer();
            $param['conditions'] = $condition == "" ? $condition : "(" . $condition . " AND company_code = '" . $companyCode . "')";
            $result = $customer->findFirst($param);
        } catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage() ? $e->getMessage() : "Error saat mendapatkan customer data";
        }

        if (!empty($result)) {
            $this->errorCode = null;
            return $result->toArray();
        }

        $this->errorCode = 404;
        $this->errorMessages = "Customer data tidak ditemukan";
        $this->logReqRes($logFileError, "EmptyData", $condition, false, "", $this->errorMessages);
        return false;
    }

    public function forgetPasskey($email, $companyId = "SLM")
    {
        $logFileError = "selma/selma_forget_passkey_error";
        $requestPayload["email"] = $email != false ? $email : "";
        $requestPayload["P_BU"] = "SELMA";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $requestPayload, false, "", $this->errorMessages);
            return false;
        }

        $forgetPassKey = $selmaWebAPI->forgetPasskey($encryptedPayload);
        if (!$forgetPassKey) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "forgetPasskey", $requestPayload, false, "", $this->errorMessages);

            return false;
        }

        return $forgetPassKey;
    }

    Public function changePasskey($customerId, $email, $oldPass, $newPass, $uid, $sessionId)
    {   
        $logFile = "selma/selma_change_password";
        $logFileError = "selma/selma_change_password_error";
        $newPassTmp = $newPass;
        $newPass = hash('sha256', $newPass);
        $dateNow = date("Y-m-d H:i:s");

        try {
            $customer = new \Models\Customer();
            $manager = new TxManager();
            $manager->setDbService($customer->getConnection());
            $transaction = $manager->get();

            $sql = "UPDATE customer SET password = '".$newPass."', updated_at = '" . $dateNow . "' WHERE customer_id = " . $customerId;
            $customer->useWriteConnection();
            $customer->getDi()->getShared($customer->getConnection())->query($sql);

            $customer->setTransaction($transaction);

            $changePassKeySelma = $this->changePasskeySelma($customerId, $email, $oldPass, $newPassTmp, $uid, $sessionId);
            if(! $changePassKeySelma) {
                $transaction->rollback($this->errorMessages);
                return false;
            }

            $transaction->commit();
            return $changePassKeySelma;
        }catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage();

            $this->logReqRes($logFileError, "changePasskey", $email, false, $customerId, $this->errorMessages);
        }

        return false;

    }

    private function changePasskeySelma($customerId, $email, $oldPassword, $newPassword, $uid, $sessionId) {
        $logFile = "selma/selma_change_pass_key";
        $logFileError = "selma/selma_change_pass_key_error";

        $requestPayload["email"] = $email;
        $requestPayload["oldpasswd"] = $oldPassword;
        $requestPayload["newpasswd"] = $newPassword;
        $requestPayload["P_BU"] = "SELMA";

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($requestPayload);
        if (! $encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $requestPayload, false, $customerId, $this->errorMessages);
            return false;
        }

        $changePassKey = $selmaWebAPI->changePasskey($encryptedPayload, $auth);
        if(! $changePassKey) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();

            $this->logReqRes($logFileError, "changePasskey", $requestPayload, false, $customerId, $this->errorMessages);
            return false;
        }

        return $changePassKey;
    }

    public function getCustomerDataSelma($data = array()) 
    {
        $logFile = "selma/selma_get_customer_data_slm";
        $logFileError = "selma/selma_get_customer_data_slm_error";

        //uid;seassonID
        $selmaWebAPI = new \Library\SelmaWebAPI();
        $customerNewModel = new \Models\Customer();

        $customer = $selmaWebAPI->getCustomerData($data);
        $this->logReqRes($logFile, "getCustomerData", $data, $customer);
        if (!$customer) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getCustomerData", $data, $customer, $data["uid"], $this->errorMessages);
            return false;
        }

        if (empty($customer)) {
            $this->errorCode = 404;
            $this->errorMessages = ["Customer tidak ditemukan"];
            $this->logReqRes($logFileError, "getCustomerData", $data, false, $data["uid"], $this->errorMessages);
            return false;
        }

        return $customer;
    }

    public function isPhoneAvailable($phone, $customerId) {

        $rupaRupaPhone = $this->ruparupaFormatPhone($phone);

        try {
            $customer = new \Models\Customer();
            $param['conditions'] = "phone = '".$rupaRupaPhone."' AND status = 10 AND company_code = 'SLM' AND customer_id != " . $customerId;
            $result = $customer->findFirst($param);    

        }catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage() ? $e->getMessage() : "Error saat mendapatkan customer";
        }

        return empty($result); 
    }

    public function getMemberDataSelma($uid)
    {
        $logFile = "selma/selma_get_member_data";
        $logFileError = "selma/selma_get_member_data_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $requestPayload["P_UID"] = $uid;
        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($requestPayload);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $requestPayload, false, "", $this->errorMessages);
            return false;
        }

        $memberDetail = $selmaWebAPI->getMemberData($encryptedPayload);
        $this->logReqRes($logFile, "getMemberData", $requestPayload, $memberDetail);
        if (!$memberDetail) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getMemberData", $requestPayload, false, "", $this->errorMessages);
            return false;
        }

        if (empty($memberDetail)) {
            $this->errorCode = 404;
            $this->errorMessages = ["Customer tidak ditemukan"];
            $this->logReqRes($logFileError, "getMemberData", $requestPayload, false, "", $this->errorMessages);
            return false;
        }
        return $memberDetail;
    }

    private function formatPhoneNumber($phone)
    {
        if (substr($phone, 0, 2) == "08") {
            return substr_replace($phone, "+628", 0, 2);
        }

        return false;
    }

    private function formatNormalPhoneNumber($phone)
    {
        if (substr($phone, 0, 4) == "+628") {
            return substr_replace($phone, "08", 0, 4);
        }

        return false;
    }

    private function formatPhoneNumberHO($phone)
    {
        if (substr($phone, 0, 2) == "08") {
            return substr_replace($phone, "628", 0, 2);
        }
        if (substr($phone, 0, 4) == "+628") {
            return substr_replace($phone, "628", 0, 4);
        }

        return $phone;
    }

    private function ruparupaFormatPhone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);

        if (substr($phone, 0, 2) == "62") {
            return substr_replace($phone, "0", 0, 2);
        }

        return $phone;
    }

    private function getFirstAndLastName($fullName)
    {
        $fullNameArr = explode(" ", $fullName);
        $firstName = $fullNameArr[0];
        $lastName = "";
        if (count($fullNameArr) > 1) {
            array_shift($fullNameArr);

            $lastName = implode(" ", $fullNameArr);
        }

        return [
            "first_name" => $firstName,
            "last_name" => $lastName,
        ];
    }


    public function validateRequired(array $data, array $fields, $checkValueEmpty = true)
    {
        $response = [];

        foreach ($fields as $field) {
            if (!array_key_exists($field, $data) || ($checkValueEmpty && empty($data[$field]))) {
                $field = str_replace("_", " ", $field);
                $message = ucwords($field) . " Diperlukan";
                $response[] = $message;
            }
        }

        return count($response) > 0 ? $response : false;
    }

    public function mappingCustomerData(array $customer)
    {

        $member = $customer["member"];
        $name = $this->getFirstAndLastName($customer["fname"]);
        $phone = $this->ruparupaFormatPhone($customer["notelp"]);

        $customerMap = [];
        $memberDetail = [];
        if ($member["P_CUST_ID"] != "") {
            $memberDetail["id_card"] = $member["P_CARD_ID"];
            $memberDetail["member_no"] = $member["P_CUST_ID"];
            $memberDetail["member_name"] = $member["P_MEMBER_NAME"];
            $memberDetail["member_exp_date"] = $member["P_EXPIRED_DATE"];
            $memberDetail["member_email"] = strtolower($member["P_EMAIL"]);
            $memberDetail["member_phone"] = $member["P_NO_HP"];
            $memberDetail["birth_place"] = $member["P_TEMPAT_LAHIR"];
            $memberDetail['is_email_verified'] = $member["P_MAIL_STATUS"];
            $memberDetail['is_phone_verified'] = $member["P_HP_STATUS"];
        }

        $customerMap["first_name"] = $name["first_name"];
        $customerMap["last_name"] = $name["last_name"];
        $customerMap["email"] = $customer["email"];
        $customerMap["phone"] = $phone;
        $customerMap["is_member"] = $customer["is_member"];
        $customerMap["address"] = $customer["addr"];
        $customerMap["city"] = $customer["city"];
        $customerMap["sex"] = -1;
        $customerMap["birth_date"] = "";
        $customerMap["ktp"] = "";
        $customerMap["marital_status"] = -1;

        if (isset($customer["sex"])) {
            $customerMap["sex"] = $customer["sex"];
        } else if (isset($member["P_JENIS_KELAMIN"])) {
            $customerMap["sex"] = $member["P_JENIS_KELAMIN"];
        }

        if (isset($customer["birth_date"])) {
            $customerMap["birth_date"] = $customer["birth_date"];
        } else if (isset($member["P_TANGGAL_LAHIR"])) {
            $dateTime = date_create_from_format('d/m/Y', $member["P_TANGGAL_LAHIR"]);
            $customerMap["birth_date"] = date_format($dateTime, 'Y-m-d');
        }

        if (isset($customer["ktp"])) {
            $customerMap["ktp"] = $customer["ktp"];
        } else if (isset($member["P_NO_KTP"])) {
            $customerMap["ktp"] = $member["P_NO_KTP"];
        }

        if (isset($customer["marital_status"])) {
            $customerMap["marital_status"] = $customer["marital_status"];
        } else if (isset($member["P_STATUS_PERKAWINAN"])) {
            $customerMap["marital_status"] = $member["P_STATUS_PERKAWINAN"];
        }

        $customerMap["member_detail"] = $memberDetail;

        return $customerMap;
    }

    public function getUserVoucherCustomerSelma($uid, $sessionId) 
    {
        $logFileError = "selma/selma_get_customer_voucher_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $voucherInfo = $selmaWebAPI->getMemberVoucher($auth);
        if (! $voucherInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getMemberVoucher", $auth, $voucherInfo, $uid, $this->errorMessages);

            return false;
        }

        return $voucherInfo;
    }

    public function getListNotifCustomerSelma($uid, $sessionId, $page) 
    {
        $logFileError = "selma/selma_get_list_notif_customer_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $response = $selmaWebAPI->getMemberListNotif($auth, $page);
        if (! $response) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();

            $this->logReqRes($logFileError, "getMemberListNotif", $auth, $response, $uid, $this->errorMessages);

            return false;
        }

        return $response;
    }

    public function getTransactionHistCustomerSelma($uid, $sessionId) 
    {
        $logFileError = "selma/selma_get_transaction_hist_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $result = $selmaWebAPI->getTransactionHist($auth);
        if (! $result) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getTransactionHist", $auth, $result, $uid, $this->errorMessages);

            return false;
        }

        return $result;
    }

    public function getTieringCustomerSelma($uid, $sessionId) 
    {
        $logFileError = "selma/selma_get_tiering_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $result = $selmaWebAPI->getTiering($auth);
        if (! $result) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getTiering", $auth, $result, $uid, $this->errorMessages);
            return false;
        }

        return $result;
    }

    public function getDigitalReceiptCustomerSelma($uid, $sessionId, $receiptNo) 
    {
        $logFileError = "selma/selma_get_digital_receipt_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();

        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $payload["receipt_no"] = $receiptNo;
        $payload["company_code"] = "HCI";    

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $payload, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $receiptInfo = $selmaWebAPI->getDigitalReceipt($auth, $encryptedPayload);
        if (! $receiptInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getDigitalReceipt", $payload, $receiptInfo, $uid, $this->errorMessages);
            return false;
        }

        return $receiptInfo;
    }

    public function checkReceiptNoCustomerSelma($auth, $receiptNo) {
        $logFileError = "selma/selma_check_receipt_no";
        $selmaWebAPI = new \Library\SelmaWebAPI();

        $payload = [
            "receiptno" => $receiptNo,
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $payload, $encryptedPayload, $auth["uid"], $this->errorMessages);
            return false;
        }

        $checkResult = $selmaWebAPI->selmaCheckReceiptNo($auth, $encryptedPayload);
        if (!$checkResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaCheckReceiptNo", $payload, $checkResult, $auth["uid"], $this->errorMessages);
            return false;
        }

        return $checkResult;
    }

    public function transactionDeliveryListCustomerSelma($uid, $sessionId, $receiptNo) {
        $logFileError = "selma/selma_transaction_delivery_list_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $checkResult = $this->checkReceiptNoCustomerSelma($auth, $receiptNo);
        if (!$checkResult) {
            return false;
        }

        $txDetailInfo = $selmaWebAPI->transactionDeliveryList($auth, $receiptNo);
        if (! $txDetailInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "transactionDeliveryList", $receiptNo, $txDetailInfo, $uid, $this->errorMessages);
            return false;
        }
   
        if (count($txDetailInfo) < 1) {
            $this->errorCode = 404;
            $this->errorMessages = "No Data Found";
            $this->logReqRes($logFileError, "transactionDeliveryList", $receiptNo, $txDetailInfo, $uid, $this->errorMessages);
            return false;
        }

        foreach($txDetailInfo as $index => $item) {
            if (isset($item["dlv_status"])) {
                $txDetailInfo[$index]["dlv_status_desc"] = $this->getDeliveryInfo($item["dlv_status"]);
            }
        }

        return $txDetailInfo;
    }

    public function transactionDeliveryDetailCustomerSelma($uid, $sessionId, $noTrans, $jobId, $siteId) {
        $logFileError = "selma/selma_transaction_delivery_detail_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $logReq = [
            "noTrans" => $noTrans,
            "jobId" => $jobId,
            "siteId" => $siteId,
        ];

        $deliveryInfo = $selmaWebAPI->deliveryDetail($auth, $noTrans, $jobId, $siteId);
        if (! $deliveryInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "deliveryDetail", $logReq, $deliveryInfo, $uid, $this->errorMessages);
            return false;
        }

        if (isset($deliveryInfo["dlv_status"])) {
            $deliveryInfo["dlv_status_desc"] = $this->getDeliveryInfo($deliveryInfo["dlv_status"]);
        }

        return $deliveryInfo;
    }

    public function getDeliveryInfo($deliveryStatus) {
        switch ($deliveryStatus) {
            case "0":
                $deliveryInfoDesc = "Menunggu";break;
            case "1":
                $deliveryInfoDesc = "Siap Kirim";break;
            case "2":
                $deliveryInfoDesc = "Dalam Perjalanan";break;
            case "3":
                $deliveryInfoDesc = "Terkirim";break;
            case "4":
                $deliveryInfoDesc = "Dibatalkan";break;
            case "5":
                $deliveryInfoDesc = "Ditunda";break;
            case "6":
            case "99":
                $deliveryInfoDesc = "Dikirimkan oleh Ekspedisi";break;
            default:
                $deliveryInfoDesc = "";
        }
        return $deliveryInfoDesc;
    }

    public function satisfactionRateCustomerSelma($uid, $sessionId, $requestParams = array()) {
        $logFileError = "selma/selma_satisfaction_rate_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($requestParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $requestParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $csatInfo = $selmaWebAPI->postSatisfactionRate($auth, $encryptedPayload);
        if (! $csatInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "postSatisfactionRate", $requestParams, $csatInfo, $uid, $this->errorMessages);
            return false;
        }

        return $csatInfo;
    }

    public function verifyEmailCustomerSelma($uid, $sessionId) {
        $logFileError = "selma/selma_verify_email_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataSelma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataSelma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        $verifyParams = [
            "P_MEMBER_NAME" => $memberData["P_MEMBER_NAME"],
            "P_EMAIL" => $memberData["P_EMAIL"],
            "P_CUST_ID" => $memberData["P_CUST_ID"],
            "P_BU" => "SELMA",
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $verifyEmailInfo = $selmaWebAPI->verifyEmail($auth, $encryptedPayload);
        if (! $verifyEmailInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "verifyEmail", $verifyParams, $verifyEmailInfo, $uid, $this->errorMessages);
            return false;
        }

        return $verifyEmailInfo;
    }

    public function sendChangePhoneOTPCustomerSelma($uid, $sessionId, $newPhone) {
        $logFile = "selma/selma_send_change_phone_otp";
        $logFileError = "selma/selma_send_change_phone_otp_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataSelma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataSelma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        // Format Phone
        $hoPhone = $this->formatPhoneNumberHO($newPhone);

        $verifyParams = [
            "P_NEW_HP" => $hoPhone,
            "P_EMAIL" => $memberData["P_EMAIL"],
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_BU" => "SELMA",
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $changePhoneOTPResult = $selmaWebAPI->selmaSendChangePhoneOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "selmaSendChangePhoneOTP", $verifyParams, $changePhoneOTPResult, $uid);
        if (! $changePhoneOTPResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaSendChangePhoneOTP", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        return $changePhoneOTPResult;
    }

    public function submitChangePhoneOTPCustomerSelma($uid, $sessionId, $newPhone, $otp) {
        $logFile = "selma/selma_submit_change_phone_otp";
        $logFileError = "selma/selma_submit_change_phone_otp_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataSelma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataSelma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        // Format Phone
        $hoPhone = $this->formatPhoneNumberHO($newPhone);
        
        $verifyParams = [
            "P_NEW_HP" => $hoPhone,
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_ID" => $memberData["P_CUST_ID"],
            "P_CUST_OTP" => $otp,
        ];

        $memberPhone = $this->ruparupaFormatPhone($memberData["P_NO_HP"]);
        if (strtolower($newPhone) == strtolower($memberPhone)) {
            $this->logReqRes($logFile, "selmaSkipSubmitChangePhoneOTP", $verifyParams, $memberData, $uid);
            return true;
        }

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $changePhoneOTPResult = $selmaWebAPI->selmaSubmitChangePhoneOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "selmaSubmitChangePhoneOTP", $verifyParams, $changePhoneOTPResult, $uid);
        if (! $changePhoneOTPResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaSubmitChangePhoneOTP", $verifyParams, $changePhoneOTPResult, $uid, $this->errorMessages);
            return false;
        }

        return $changePhoneOTPResult;
    }

    public function sendChangeEmailOTPCustomerSelma($uid, $sessionId, $newEmail) {
        $logFile = "selma/selma_send_change_email_otp";
        $logFileError = "selma/selma_send_change_email_otp_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataSelma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataSelma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        $verifyParams = [
            "P_NEW_EMAIL" => $newEmail,
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_HP" => $memberData["P_NO_HP"],
            "P_BU" => "SELMA",
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $sendChangeEmailResult = $selmaWebAPI->selmaSendChangeEmailOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "selmaSendChangeEmailOTP", $verifyParams, $sendChangeEmailResult, $uid);
        if (! $sendChangeEmailResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();

            $this->logReqRes($logFileError, "selmaSendChangeEmailOTP", $verifyParams, $sendChangeEmailResult, $uid, $this->errorMessages);
            return false;
        }

        return $sendChangeEmailResult;
    }

    public function submitChangeEmailOTPCustomerSelma($uid, $sessionId, $newEmail, $otp) {
        $logFile = "selma/selma_submit_change_email_otp";
        $logFileError = "selma/selma_submit_change_email_otp_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataSelma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataSelma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }
    
        $verifyParams = [
            "P_NEW_EMAIL" => $newEmail,
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_ID" => $memberData["P_CUST_ID"],
            "P_CUST_OTP" => $otp,
        ];

        if (strtolower($newEmail) == strtolower($memberData["P_EMAIL"])) {
            $this->logReqRes($logFile, "selmaSkipSubmitChangeEmailOTP", $verifyParams, $memberData, $uid);
            return true;
        }

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "getMemberDataSelma", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $changeEmailOTPResult = $selmaWebAPI->selmaSubmitChangeEmailOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "selmaSubmitChangeEmailOTP", $verifyParams, $changeEmailOTPResult, $uid);
        if (! $changeEmailOTPResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaSubmitChangeEmailOTP", $verifyParams, $changeEmailOTPResult, $uid, $this->errorMessages);
            return false;
        }

        return $changeEmailOTPResult;
    }

    public function getRedeemVoucherListCustomerSelma($uid, $sessionId) {
        $logFileError = "selma/selma_get_redeem_voucher_list_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $selmaResult = $selmaWebAPI->selmaRedeemVoucherList($auth);
        if (! $selmaResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaRedeemVoucherList", $auth, $selmaResult, $uid, $this->errorMessages);
            return false;
        }

        return $selmaResult;
    }

    public function redeemVoucherSendOTPCustomerSelma($uid, $sessionId) {
        $logFile = "selma/selma_redeem_voucher_send_otp";
        $logFileError = "selma/selma_redeem_voucher_send_otp_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $verifyParams = [
            "P_UID" => $uid,
            "P_REQUEST_TYPE" => "1",
            "P_BU" => "SELMA",
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $selmaResult = $selmaWebAPI->selmaRedeemVoucherSendOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "selmaRedeemVoucherSendOTP", $verifyParams, $selmaResult, $uid);
        if (! $selmaResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaRedeemVoucherSendOTP", $verifyParams, $selmaResult, $uid, $this->errorMessages);
            return false;
        }

        return $selmaResult;
    }

    public function redeemVoucherSubmitOTPCustomerSelma($uid, $sessionId, $voucherCode, $otp) {
        $logFile = "selma/selma_redeem_voucher_submit_otp";
        $logFileError = "selma/selma_redeem_voucher_submit_otp_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $verifyParams = [
            "voucherid" => $voucherCode,
            "P_CUST_OTP" => $otp,
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $selmaResult = $selmaWebAPI->selmaRedeemVoucherSubmitOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "selmaRedeemVoucherSubmitOTP", $verifyParams, $selmaResult, $uid);
        if (! $selmaResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaRedeemVoucherSubmitOTP", $verifyParams, $selmaResult, $uid, $this->errorMessages);
            return false;
        }

        return $selmaResult;
    }

    public function changeProfileEmailCustomerSelma($uid, $sessionId, $customerId, $oldEmail, $newEmail) {
        $logFile = "selma/selma_change_profile_email";
        $logFileError = "selma/selma_change_profile_email_error";

        if (strtolower($oldEmail) == strtolower($newEmail)) {
            $this->logReqRes($logFile, "selmaSkipChangeProfileEmailCustomerSelma", $oldEmail, $newEmail, $uid);
            return true;
        }

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $customerModel = new \Models\Customer();
        $manager = new TxManager();
        $manager->setDbService($customerModel->getConnection());
        $transaction = $manager->get();
        
        try {
            // save data to Customer
            $customerModel = $customerModel->findFirst("customer_id = '" . $customerId . "'");
            $customerModel->email = $newEmail;
            $customerModel->updated_at = date("Y-m-d H:i:s");
            $customerModel->save();
            $customerModel->setTransaction($transaction);

            // Change selma data
            $payload = [
                [
                    'name'=>'p_uid',
                    'contents'=>$uid,
                    'Content-type' => 'multipart/form-data',    
                ],
                [
                    'name'=>'p_old_email',
                    'contents'=>$oldEmail,
                    'Content-type' => 'multipart/form-data',    
                ],
                [
                    'name'=>'p_new_email',
                    'contents'=>$newEmail,
                    'Content-type' => 'multipart/form-data',    
                ],
            ];

            $selmaResult = $selmaWebAPI->selmaChangeProfileEmail($auth, $payload);
            if (! $selmaResult) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "selmaChangeProfileEmail", $payload, $selmaResult, $uid, $this->errorMessages);
                $transaction->rollback($this->errorMessages);    
                return false;
            }

        }   catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        return true;
    }

    public function changeProfilePhoneCustomerSelma($uid, $sessionId, $customerId, $newPhone)
    {
        $logFileError = "selma/selma_change_profile_email_error";

        $phone = $this->ruparupaFormatPhone($newPhone);
        $formattedPhone = $this->formatPhoneNumber($phone);

        if (!$formattedPhone) {
            $this->errorCode = 400;
            $this->errorMessages = "Format nomor handphone tidak valid";
            return false;
        }

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $customerModel = new \Models\Customer();
        $manager = new TxManager();
        $manager->setDbService($customerModel->getConnection());
        $transaction = $manager->get();

        try {
            // save data to Customer
            $customerModel = $customerModel->findFirst("customer_id = '" . $customerId . "'");
            $customerModel->phone = $phone;
            $customerModel->updated_at = date("Y-m-d H:i:s");
            $customerModel->save();
            $customerModel->setTransaction($transaction);

            // Format Phone
            $hoPhone = $this->formatPhoneNumberHO($phone);

            $profilePayload = [
                'nohp' => $hoPhone,
                'city' => '',
                'address' => '',
                'fname' => '',
                'P_BU' => 'SELMA',
            ];

            $encryptedProfilePayload = $selmaWebAPI->selmaWebAPIEncrypt($profilePayload);
            if (!$encryptedProfilePayload) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $profilePayload, $encryptedProfilePayload, $uid, $this->errorMessages);
                $transaction->rollback($this->errorMessages);

                return false;
            }

            $successUpdateProfile = $selmaWebAPI->editProfile($encryptedProfilePayload, $auth);
            if (!$successUpdateProfile) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "editProfile", $profilePayload, $successUpdateProfile, $uid, $this->errorMessages);
                $transaction->rollback($this->errorMessages);

                return false;
            }
        } catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        return true;
    }

    public function transactionInstallationListCustomerSelma($uid, $sessionId, $receiptNo) {
        $logFileError = "selma/selma_transaction_installation_list_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $checkResult = $this->checkReceiptNoCustomerSelma($auth, $receiptNo);
        if (!$checkResult) {
            return false;
        }

        $txDetailInfo = $selmaWebAPI->installationList($auth, $receiptNo);
        if (! $txDetailInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "installationList", $receiptNo, $txDetailInfo, $uid, $this->errorMessages);

            return false;
        }

        if (count($txDetailInfo) < 1) {
            $this->errorCode = 404;
            $this->errorMessages = "No Data Found";
            $this->logReqRes($logFileError, "installationList", $receiptNo, $txDetailInfo, $uid, $this->errorMessages);

            return false;
        }

        foreach($txDetailInfo as $index => $item) {
            if (isset($item["ins_status"])) {
                $txDetailInfo[$index]["ins_status_desc"] = $this->getDeliveryInfo($item["ins_status"]);
            }
        }

        return $txDetailInfo;
    }

    public function transactionInstallationDetailCustomerSelma($uid, $sessionId, $noTrans, $jobId) {
        $logFileError = "selma/selma_transaction_installation_detail_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $logReq = [
            "noTrans" => $noTrans,
            "jobId" => $jobId,
        ];

        $installationInfo = $selmaWebAPI->installationDetail($auth, $noTrans, $jobId);
        if (! $installationInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "installationDetail", $logReq, $installationInfo, $uid, $this->errorMessages);

            return false;
        }

        if (isset($installationInfo["ins_status"])) {
            $installationInfo["ins_status_desc"] = $this->getInstallationInfo($installationInfo["ins_status"]);
        }

        return $installationInfo;
    }

    public function getInstallationInfo($installationStatus) {
        switch ($installationStatus) {
            case "0":
                $installationInfo = "Menunggu";break;
            case "1":
                $installationInfo = "Siap Berangkat";break;
            case "2":
                $installationInfo = "Dalam Perjalanan";break;
            case "3":
                $installationInfo = "Terpasang";break;
            case "4":
                $installationInfo = "Dibatalkan";break;
            case "5":
                $installationInfo = "Ditunda";break;
            case "6":
                $installationInfo = "Terpasang - Belum Lengkap";break;
            default:
                $installationInfo = "";
        }

        return $installationInfo;
    }

    public function payWithPointCustomerSelma($uid, $sessionId, $otp) {
        $logFile = "selma/selma_pay_with_point";
        $logFileError = "selma/selma_pay_with_point_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $payload = [
            'P_UID' => $uid,
            'OTP' => $otp,
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $payload, $encryptedPayload, $uid, $this->errorMessages);
          
            return false;
        }
       
        $result = $selmaWebAPI->selmaPayWithPoint($auth, $encryptedPayload);
        $this->logReqRes($logFile, "selmaPayWithPoint", $payload, $result, $uid);
        if (! $result) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaPayWithPoint", $payload, $result, $uid, $this->errorMessages);

            return false;
        }

       return $result;
    }

    public function formatGender($gender){
        $result = -1;
        switch ($gender) {
            case "wanita":
                $result = 0;
                break;
            case "pria":
                $result = 1;
                break;
            default:
                break;
        }

        return $result;
    }

    public function getReceiptListCustomerSelma($uid, $sessionId, $page) {
        $logFileError = "selma/selma_get_receipt_list_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $result = $selmaWebAPI->selmaGetReceiptList($auth, $page);
        if (! $result) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaGetReceiptList", $page, $result, $uid, $this->errorMessages);

            return false;
        }

       return $result;
    }

    public function getSexString($sexId)
    {
        $result = "";
        switch ($sexId) {
            case 0:
                $result = "female";
                break;
            case 1:
                $result = "male";
                break;
            default:
                break;
        }

        return $result;
    }

    public function getSexId($sexString)
    {
        $result = -1;
        switch ($sexString) {
            case "female":
                $result = 0;
                break;
            case "male":
                $result = 1;
                break;
            default:
                break;
        }

        return $result;
    }

    public function getPointHistoryCustomerSelma($customerId, $page) {
        $logFileError = "selma/selma_get_point_history_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();

        $endDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime($endDate . ' +1 day'));

        $payload = [
            "P_BU_Code" => "HCI",
            "P_Cust_Id" => $customerId,
            "P_Page_No" => $page,
            "P_Page_Size" => "10",
            "P_Date_From" => "1900-01-01",
            "P_Date_To" => $endDate
        ];

        $result = $selmaWebAPI->selmaGetPointHistory($payload);
        if (! $result) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaGetPointHistory", $payload, $result, $customerId, $this->errorMessages);

            return false;
        }

       return $result;

    }

    public function isEmailAvailable($email, $customerId) {

        try {
            $customer = new \Models\Customer();
            $param['conditions'] = "email = '".$email."' AND status = 10 AND company_code = 'SLM' AND customer_id != " . $customerId;
            $result = $customer->findFirst($param);    

        }catch (\Exception $e) {
            $this->errorCode = 400;
            $this->errorMessages = $e->getMessage() ? $e->getMessage() : "Error saat mendapatkan customer";
        }

        return empty($result); 
    }

    public function transactionInstallationRouteCustomerSelma($auth, $notrans, $jobid) {
        $logFileError = "selma/selma_get_installation_route_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();

        $logReq = [
            "notrans" => $notrans,
            "jobid" => $jobid,
        ];

        $result = $selmaWebAPI->selmaInstallationRoute($auth, $notrans, $jobid);
        if (! $result) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaInstallationRoute", $logReq, $result, $auth["uid"], $this->errorMessages);

            return false;
        }

       return $result;
    }

    function logReqRes($logfile, $prefix = "", $req = false, $res = false, $custId = "", $errMsg = "")
    {
        $doLog = true;
        $logType = "info";
        $excludedErrMsg = [];

        if (substr($logfile, -6) == "_error") {
            $logType = "error";
        }

        if (isset($req["data"])) {
            $req["data"] = "***";
        }

        if (isset($req["sessionid"])) {
            $req["sessionid"] = "***";
        }

        if (isset($req["password"])) {
            $req["password"] = "***";
        }

        if (isset($req["passwd"])) {
            $req["passwd"] = "***";
        }

        if (isset($req["oldpasswd"])) {
            $req["oldpasswd"] = "***";
        }

        if (isset($req["newpasswd"])) {
            $req["newpasswd"] = "***";
        }

        if (isset($res["password"])) {
            $res["password"] = "***";
        }

        $log = $prefix . " -- Request: " . json_encode($req) . " -- Response: " . json_encode($res);
        if ($custId != "") {
            $log = $log . " -- Customer ID: " . $custId;
        }
        if ($errMsg != "") {
            $log = $log . " -- ErrMessage: " . $errMsg;
            if (in_array($errMsg, $excludedErrMsg)) {
                $doLog = false;
            }
        }
        if (!empty($this->appVersion)) {
            $log = $log . " -- AppVersion: " . $this->appVersion;
        }

        if ($doLog) {
            LogHelper::log($logfile, $log, $logType);
            if ($logType == "error" && getenv('SLACK_ERROR_SELMA') == "true") {
                $formattedLog = $logfile . " -- " . $log;

                $this->sendSlackNotif($formattedLog, getenv('SLACK_ERROR_SELMA_CHANNEL'), getenv('OPS_ISSUE_SLACK_USERNAME'));
                $this->sendDiscordNotif(getenv('DISCORD_HOOK_SELMA'), $formattedLog, getenv('DISCORD_HOOK_SELMA_USERNAME'));
            }
        }
    }

    public function sendSlackNotif($message, $channel, $username)
    {
        $notificationText = $message;
        $params = [
            "channel" => $channel,
            "username" => $username,
            "text" => $message,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    public function sendDiscordNotif($webhookUrl, $message, $username)
    {
        $notificationText = $message;
        $params = [
            "username" => $username,
            "content" => $message,
        ];

        // call queue for webhook discord
        $nsq = new \Library\Nsq();
        $message = [
            "webhook_url" => $webhookUrl,
            "data" => $params,
            "message" => "discordNotif"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    public function logoutCustomerSelma($uid, $sessionId)
    {
        $logFileError = "selma/selma_logout";
        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $logoutInfo = $selmaWebAPI->logoutUser($auth);
        if (!$logoutInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "logoutUser", $auth, $logoutInfo, $uid, $this->errorMessages);
            return false;
        }

        return $logoutInfo;
    }

    public function getMembershipDataSelma($selmaCustomerID) {
        $logFileError = "selma/selma_get_membership_data_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();

        $payload = [
            "P_Cust_Id" => $selmaCustomerID,
        ];

        $result = $selmaWebAPI->selmaGetMembershipData($payload);
        if (! $result) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaGetMembershipData", $payload, $result, $selmaCustomerID, $this->errorMessages);

            return false;
        }

        $resultFiltered = [];
        if (count($result["rows"]) > 0) {
            $resultFiltered = [
                "Exp_Date" => $result["rows"][0]["Exp_Date"],
                "Cust_Add" => $result["rows"][0]["Cust_Add"],
                "AutoRen" => $result["rows"][0]["AutoRen"],
                "Card_Ref" => $result["rows"][0]["Card_Ref"],
                "Ref_Date" => $result["rows"][0]["Ref_Date"],
                "Reg_Date" => $result["rows"][0]["Reg_Date"],
                "cust_poscode" => $result["rows"][0]["cust_poscode"],
                "Jml_Point" => $result["rows"][0]["Jml_Point"],
                "Point_exp" => $result["rows"][0]["Point_exp"],
                "point_exp_date" => $result["rows"][0]["point_exp_date"],
                "member_type" => $result["rows"][0]["member_type"],
                "description" => $result["rows"][0]["description"],
                "tier_exp_date" => $result["rows"][0]["tier_exp_date"],
                "total_trans_period" => $result["rows"][0]["total_trans_period"],
                "Flag_ultimate" => $result["rows"][0]["Flag_ultimate"],
            ];
        }

       return $resultFiltered;
    }

    public function updateCustomerData($customer = array(), $customerSelmaDetail = array(), $updateData = array(), $isMember = false)
    {
        $logFile = "selma/selma_update_customer";
        $logFileError = "selma/selma_update_customer_error";

        $fullName = $updateData["full_name"];
        $name = $this->getFirstAndLastName($fullName);    

        $profilePayload["city"] = $updateData["city"]; 
        $profilePayload["fname"] = $updateData["full_name"]; 
        $profilePayload["nohp"] = ""; // Phone and email cannot be updated thru this endpoint 
        $profilePayload["address"] = $updateData["address"]; 
        $profilePayload["P_BU"] = "SELMA"; 

        $customerModel = new \Models\Customer();
        $customerDetailModel = new \Models\CustomerSelmaDetails();

        $manager = new TxManager();
        $manager->setDbService($customerModel->getConnection());
        $transaction = $manager->get();

        $successUpdateMember = false;

        try {
            // save data to Customer
            $customerModel = $customerModel->findFirst("customer_id = '" . $customer["customer_id"] . "'");
            if (!empty($fullName)) {
                $customerModel->first_name = $name["first_name"];
                $customerModel->last_name = $name["last_name"];
            }
            if (!empty($updateData["birth_date"]))
                $customerModel->birth_date = $updateData["birth_date"];
   
            if (isset($updateData["marital_status"]))
                $customerModel->gender = $this->getSexString($updateData["sex"]);
    
            
            $customerModel->updated_at = date("Y-m-d H:i:s");
            $customerModel->save();
            $customerModel->setTransaction($transaction);
            

            $customerDetailModel = $customerDetailModel->findFirst("customer_id = '" . $customer["customer_id"] . "'");
            
            if (isset($updateData["ktp"]))
                $customerDetailModel->ktp = $updateData["ktp"];

            if (isset($updateData["marital_status"]))
                $customerDetailModel->marital_status = $updateData["marital_status"];


            $customerDetailModel->updated_at = date("Y-m-d H:i:s");
            $customerDetailModel->save();
            $customerDetailModel->setTransaction($transaction);
            
            # change profile data
            $selmaWebAPI = new \Library\SelmaWebAPI();
            $encryptedProfilePayload = $selmaWebAPI->selmaWebAPIEncrypt($profilePayload);
            if (!$encryptedProfilePayload) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $profilePayload, $encryptedProfilePayload, $customer["customer_id"], $this->errorMessages);
                $transaction->rollback($this->errorMessages);
              
                return false;
            }

            $auth["uid"] = $customerSelmaDetail["selma_uid"];
            $auth["sessionid"] = $customerSelmaDetail["session_id"];
            $successUpdateProfile = $selmaWebAPI->editProfile($encryptedProfilePayload, $auth);
            $this->logReqRes($logFile, "editProfile", $profilePayload, $successUpdateProfile);
            if (! $successUpdateProfile) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "editProfile", $profilePayload, $successUpdateProfile, $customer["customer_id"], $this->errorMessages);
                $transaction->rollback($this->errorMessages);

                return false;
            }

            if ($isMember) {

                $memberPayload["P_CUST_ID"] = $customerSelmaDetail["selma_customer_id"];
                $memberPayload["P_NO_KTP"] = $updateData["ktp"];
                $memberPayload["P_TANGGAL_LAHIR"] = $updateData["birth_date"];
                $memberPayload["P_TEMPAT_LAHIR"] = $updateData["birth_place"];
                $memberPayload["P_STATUS_PERKAWINAN"] = $updateData["marital_status"];
                $memberPayload["P_JENIS_KELAMIN"] = $updateData["sex"];
        
                # change member data
                $encryptedMemberPayload = $selmaWebAPI->selmaWebAPIEncrypt($memberPayload);
                if (!$encryptedMemberPayload) {
                    $this->errorCode = $selmaWebAPI->getErrorCode();
                    $this->errorMessages = $selmaWebAPI->getErrorMessages();
                    $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $memberPayload, $encryptedMemberPayload, $customer["customer_id"], $this->errorMessages);
                    $transaction->rollback($this->errorMessages);
                  
                    return false;
                }
                $successUpdateMember = $selmaWebAPI->editMemberData($encryptedMemberPayload);
                $this->logReqRes($logFile, "editMemberData", $memberPayload, $successUpdateMember, $customer["customer_id"]);
                if (! $successUpdateMember) {
                    $this->errorCode = $selmaWebAPI->getErrorCode();
                    $this->errorMessages = $selmaWebAPI->getErrorMessages();
                    $this->logReqRes($logFileError, "editMemberData", $memberPayload, $successUpdateMember, $customer["customer_id"], $this->errorMessages);
                    $transaction->rollback($this->errorMessages);
    
                    return false;
                }
            }

        }catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();
        return true;

    }

    private function formatPhoneNumberInternational($phone) {
        if (substr($phone, 0, 2) == "08") {
            $phone = substr_replace($phone, "+628", 0, 2);
        } else if (substr($phone, 0, 2) == "62") {
            $phone = substr_replace($phone, "+62", 0, 2);
        }

        return $phone;
    }

    public function checkCustomerWithUpdateSelmaMemberSKU($customerId)
    {
        $salesOrderModel = new \Models\SalesOrder();
        
        $query = "SELECT \Models\SalesOrderItem.sku FROM \Models\SalesOrder
                    INNER JOIN \Models\SalesOrderItem
                WHERE \Models\SalesOrder.customer_id = $customerId 
                AND \Models\SalesOrder.status != 'canceled'
                AND \Models\SalesOrderItem.sku = '".getenv("MEMBERSHIP_SLM_SKU")."' LIMIT 1";
        
        $manager = $salesOrderModel->getModelsManager();
        $salesOrder = $manager->executeQuery($query);
        if (sizeof($salesOrder) === 0) {
            return true;
        }

        return false;
    }

    public function isCurrentDate($date) {
        $dateNow = date("Y-m-d");
        return $date == $dateNow;
    }

    public function getCustomerVoucherDeals($customerId) {
        $params = [
            "customer_id" => $customerId,
            "include_type" => getenv('VOUCHER_TYPE_OFFLINE_DEALS'),
        ];
        $customerLib = new \Library\Customer();
        $customerLib->setCustomerFromArray($params);
        $customerLib->setConditionsFromArray($params);
        $voucherList = $customerLib->getVoucherList();

        return $voucherList;
    }

    public function appendDealsVoucher($storeVoucher, $dealsVouchers) {
        $result = [
            "totamount" => isset($storeVoucher["totamount"]) ? $storeVoucher["totamount"] : 0,
            "arrvoucher" => isset($storeVoucher["arrvoucher"]) ? $storeVoucher["arrvoucher"] : [],
        ];

        foreach ($dealsVouchers as $dealsVoucher) {
            if ($dealsVoucher["status"] == "Sudah Digunakan" || $dealsVoucher["status"] == "Kadaluarsa") {
                continue;
            }
            $spliced = false;
            $amt = (int)$dealsVoucher["voucher_amount"];
            $formattedDeals = [
                "data" => [
                    "vouchercode" => $dealsVoucher["voucher_code"],
                    "amount" => (string)$amt,
                    "voucher_category" => "store_voucher_deals",
                    "type" => $dealsVoucher["type"],
                    "expired_date" => $dealsVoucher["expiration_datetime"],
                    "tnc" => $dealsVoucher["tnc"],
                    "deals_data" => $dealsVoucher["deals_data"],
                ]
            ];

            foreach ($result["arrvoucher"] as $index => $voucher) {
                if ($formattedDeals["data"]["expired_date"] >= $voucher["expired_date"]) {
                    array_splice($result["arrvoucher"], $index, 0, $formattedDeals);
                    $result["totamount"]++;
                    $spliced = true;
                    break;
                }
            }

            if (!$spliced) {
                array_push($result["arrvoucher"], $formattedDeals["data"]);
                $result["totamount"]++;
            }
        }
        
        return $result;
    }

    public function getFullName($firstName, $lastName) {
        $fullName = $firstName;
        if (!empty($lastName) && !is_null($lastName) && isset($lastName)) {
            $fullName = $firstName . " " . $lastName;
        }

        return $fullName;
    }

    public function postRegisterMemberCustomerSelma($customerData) {
        $logFile = "selma/selma_register_member";
        $logFileError = "selma/selma_register_member_error";
        $aceWebAPI = new \Library\AceWebAPI();

        $fullName = $this->getFullName($customerData["first_name"], isset($customerData["last_name"]) ? $customerData["last_name"] : "");

        $sex = "0";
        if (isset($customerData["gender"])) {
            $sex = $this->getSexId($customerData["gender"]);
        }

        $customerDataSelma = $this->getCustomerDetail("customer_id = '" . $customerData["customer_id"] . "'");
        if(! $customerDataSelma) {
            return false;
        }

        $ktp = "";
        if (isset($customerDataSelma["ktp"])) {
            $ktp = $customerDataSelma["ktp"];
        }

        $statusMarital = "";
        if (isset($customerDataSelma["marital_status"])) {
            $statusMarital = $customerDataSelma["marital_status"];
        }

        $payload = [
            "P_Company" => "SELMA",
            "P_Cust_Name" => $fullName,
            "P_Cust_Add" => "",
            "P_Cust_Hp" => isset($customerData["phone"]) ? $this->formatPhoneNumberHO($customerData["phone"]) : "",
            "P_Cust_Mail" => $customerData["email"],
            "P_Cust_Birth_Date" => isset($customerData["birth_date"]) ? $customerData["birth_date"] : "",
            "P_Cust_Birth_Place" => "",
            "P_Cust_Sex" => strval($sex),
            "P_Cust_City" => "",
            "P_Cust_PosCode" => "",
            "P_Cust_Add_Ktp" => "",
            "P_Cust_Phone" => "",
            "P_Cust_IdCard" => $ktp,
            "P_Cust_Religion" => "",
            "P_Cust_Stat_Marital" => $statusMarital,
            "P_Cust_citizenship" => "",
            "P_Cust_Employ" => "",
            "P_Cust_Purpose" => "",
            "P_Cust_AutoRen" => "",
            "P_Card_Ref" => "",
            "P_Reference_Date" => "",
            "P_Company_Code" => "XAQ",
            "P_Cust_Ref" => "APP",
            "P_Cust_FreeF" => "0"
        ];

        // Encrypted Payload
        $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $aceWebAPI->getErrorCode();
            $this->errorMessages = $aceWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "aceWebAPIEncrypt", $payload, $encryptedPayload, $customerData["customer_id"], $this->errorMessages);
            return false;
        }

        // Transaction
        $customerDetailModel = new \Models\CustomerSelmaDetails();
        $manager = new TxManager();
        $manager->setDbService($customerDetailModel->getConnection());
        $transaction = $manager->get();
        $result = [];

        try {
            // save data to Customer
            $customerDetailModel = $customerDetailModel->findFirst("customer_id = '" . $customerData["customer_id"] . "'");
            $customerDetailModel->setTransaction($transaction);

            $result = $aceWebAPI->aceRegisterCustomer($encryptedPayload);
            $this->logReqRes($logFile, "aceRegisterCustomer", $payload, $result, $customerData["customer_id"]);
            if (!$result) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "aceRegisterCustomer", $payload, $result, $customerData["customer_id"], $this->errorMessages);
                $transaction->rollback();
                return false;
            }

            $customerDetailModel->selma_customer_tmp_member_no = isset($result[0]["Card"]) ? $result[0]["Card"] : "";
            $customerDetailModel->selma_customer_id = isset($result[0]["Cust_Id"]) ? $result[0]["Cust_Id"] : "";

            $customerDetailModel->updated_at = date("Y-m-d H:i:s");
            $customerDetailModel->save();
            $customerDetailModel->setTransaction($transaction);

        }catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

       return $result;
    }

    public function getCheckEmailCustomerSelma($email) {
        $logFile = "selma/selma_get_check_email";
        $logFileError = "selma/selma_get_check_email_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();

        $payload = [
            "P_Cust_Mail" => $email,
            "P_Company_Type" => "I",
            "P_BU_Code" => "SELMA",
        ];

        $result = $selmaWebAPI->selmaGetCheckEmail($payload);
        $this->logReqRes($logFile, "selmaGetCheckEmail", $payload, $result);
        if (! $result) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaGetCheckEmail", $payload, $result, "", $this->errorMessages);

            return false;
        }

        return $result;
    }

    public function updateTemporaryMemberCustomerSelma($custDetailID, $checkEmailData) {
        $logFileError = "selma/selma_update_tmp_member_error";

        $customerSelmaDetailsEntities = [
            "customer_selma_details_id" => $custDetailID,
            "selma_customer_id" => $checkEmailData["cust_id"],
            "selma_customer_tmp_member_no" => $checkEmailData["card_id"],
            "updated_at" => date("Y-m-d H:i:s"),
        ];

        $this->customerSelmaDetails->setFromArray($customerSelmaDetailsEntities);
        if (!$this->customerSelmaDetails->saveData("selma/selma_update_tmp_member")) {
            $this->errorCode = $this->customerSelmaDetails->getErrorCode();
            $this->errorMessages = $this->customerSelmaDetails->getErrorMessages();
            $this->logReqRes($logFileError, "saveData", $customerSelmaDetailsEntities, false, "", $this->errorMessages);
            return false;
        }

        return true;
    }

    public function setCustomer2Group($customerId, $memberNo, $expiredDate) {
        $logFileError = "selma/selma_set_customer_2_group_error";
        $nowDate = new \DateTime("now");
        $expDate = new \DateTime($expiredDate);    
        $customerEntities = [
            "customer_id" => $customerId,
            "group" => [
                "SLM" => $memberNo,
                "SLM_expiration_date" => $expDate->format('Y-m-d 23:59:59'),
                "SLM_is_verified" => $nowDate <= $expDate ? 10 : 5,
            ]
        ];
        $customerNewModel = new \Models\Customer();

        $customerNewModel->setFromArray($customerEntities);
        if (!$customerNewModel->saveData("selma/selma_set_customer_2_group")) {
            $this->errorCode = 400;
            $this->errorMessages = "Gagal memperbarui Customer Selma";
            $this->logReqRes($logFileError, "saveData", $customerEntities, false, $customerId, $this->errorMessages);
            return false;
        }

        return true;
    }

    public function postConnectMemberCustomerSelma($customerId, $auth, $fullName, $email, $irNumber, $custId) {
        $logFile = "selma/selma_post_connect_member";
        $logFileError = "selma/selma_post_connect_member_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();

        $payload = [
            "cardid" => $irNumber,
            "fname" => $fullName,
            "email" => $email,
            "ismilist" => "0",
            "P_BU" => "SELMA",
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($payload);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $payload, $encryptedPayload, $customerId, $this->errorMessages);
            return false;
        }

        // Transaction
        $customerDetailModel = new \Models\CustomerSelmaDetails();
        $manager = new TxManager();
        $manager->setDbService($customerDetailModel->getConnection());
        $transaction = $manager->get();        
        $result = [];

        try {
            $customerDetailModel = $customerDetailModel->findFirst("customer_id = '" . $customerId . "'");
            
            $customerDetailModel->updated_at = date("Y-m-d H:i:s");
            $customerDetailModel->setTransaction($transaction);

            // TODO: try catch logging
            $result = $selmaWebAPI->selmaPostConnectMember($auth, $encryptedPayload);
            $this->logReqRes($logFile, "selmaPostConnectMember", $payload, $result, $customerId);
            if (! $result) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "selmaPostConnectMember", $payload, $result, $customerId, $this->errorMessages);
                $transaction->rollback();
                return false;
            }

            $customerDetailModel->selma_customer_id = $custId;

            if (substr($irNumber, 0, 3) == "TIM") {
                $customerDetailModel->selma_customer_tmp_member_no = isset($result["cardid"]) ? $result["cardid"] : "";
            } else {
                $customerDetailModel->selma_customer_member_no = isset($result["cardid"]) ? $result["cardid"] : "";
            }

            $customerDetailModel->save();
            $customerDetailModel->setTransaction($transaction);

        }catch (\Exception $e) {
            return false;
        }

        $transaction->commit();

       return $result;
    }

    public function verifyPhoneCustomerSelma($uid, $sessionId) {
        $logFile = "selma/selma_verify_phone";
        $logFileError = "selma/selma_verify_phone_error";
        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataSelma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataSelma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }

        $verifyParams = [
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_HP" => $memberData["P_NO_HP"],
            "P_BU" => "SELMA",
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $verifyPhoneInfo = $selmaWebAPI->verifyPhone($auth, $encryptedPayload);
        $this->logReqRes($logFile, "verifyPhone", $verifyParams, $verifyPhoneInfo, $uid);
        if (! $verifyPhoneInfo) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "verifyPhone", $verifyParams, $verifyPhoneInfo, $uid, $this->errorMessages);
            return false;
        }

        return $verifyPhoneInfo;
    }

    public function verifyPhoneSubmitOTPCustomerSelma($uid, $sessionId, $otp) {
        $logFile = "selma/selma_submit_verify_phone_otp";
        $logFileError = "selma/selma_submit_verify_phone_otp_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $auth["uid"] = $uid;
        $auth["sessionid"] = $sessionId;

        $memberData = $this->getMemberDataSelma($uid);
        if (!empty($this->errorCode)) {
            $this->logReqRes($logFileError, "getMemberDataSelma", $uid, $memberData, $uid, $this->errorMessages);
            return false;
        }
    
        $verifyParams = [
            "P_CARD_ID" => $memberData["P_CARD_ID"],
            "P_CUST_ID" => $memberData["P_CUST_ID"],
            "P_CUST_OTP" => $otp,
        ];

        $encryptedPayload = $selmaWebAPI->selmaWebAPIEncrypt($verifyParams);
        if (!$encryptedPayload) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaWebAPIEncrypt", $verifyParams, $encryptedPayload, $uid, $this->errorMessages);
            return false;
        }

        $changeEmailOTPResult = $selmaWebAPI->selmaVerifyPhoneSubmitOTP($auth, $encryptedPayload);
        $this->logReqRes($logFile, "selmaVerifyPhoneSubmitOTP", $verifyParams, $changeEmailOTPResult, $uid);
        if (! $changeEmailOTPResult) {
            $this->errorCode = $selmaWebAPI->getErrorCode();
            $this->errorMessages = $selmaWebAPI->getErrorMessages();
            $this->logReqRes($logFileError, "selmaVerifyPhoneSubmitOTP", $verifyParams, $changeEmailOTPResult, $uid, $this->errorMessages);
            return false;
        }

        return $changeEmailOTPResult;
    }

    public function postPwpSendOTPSelma($selmaAuth, $customerId, $phoneNo, $cardId, $redeemPoint, $cartId, $skipCart) {
        $logFile = "selma/selma_pwp_send_otp";
        $logFileError = "selma/selma_pwp_send_otp_error";
        
        $validate = $this->validatePwpOTPSelma($selmaAuth, $redeemPoint, $cartId, $skipCart);
        if (!$validate) {
            $this->logReqRes($logFileError, "validatePwpOTPSelma", $selmaAuth, $validate, $customerId, $this->errorMessages);
            return false;
        }

        $selmaWebAPI = new \Library\SelmaWebAPI();
        $aceWebAPI = new \Library\AceWebAPI();
        $selmaOtpTempModel = new \Models\CustomerSelmaOtpTemp();

        $formattedPhone = $this->formatPhoneNumberInternational($phoneNo);
        $otp = GeneralHelper::generateRandomNumber(6);

        $manager = new TxManager();
        $manager->setDbService($selmaOtpTempModel->getConnection());
        $transaction = $manager->get();

        $otpType = "pay_with_point";
        if ($skipCart) {
            $otpType = "redeem_voucher_online";
        }
        
        try {
            $validate = $selmaOtpTempModel->validateSelmaSendOTP($customerId, "phone", $formattedPhone, $otp, $otpType, $transaction, $validateCount);
            if (getenv("SELMA_ENABLE_OTP_LIMIT_DAILY")) {
                if ($skipCart) {
                    $remainingSubmitOTP = getenv("SELMA_REDEEM_ONLINE_VOUCHER_OTP_LIMIT_DAILY") - $validateCount;
                } else {
                    $remainingSubmitOTP = getenv("SELMA_PWP_OTP_LIMIT_DAILY") - $validateCount;
                }
            } else {
                $remainingSubmitOTP = 1;
            }

            if (!$validate) {
                $this->errorCode = $selmaOtpTempModel->getErrorCode();
                $this->errorMessages = $selmaOtpTempModel->getErrorMessages();
                $this->errorData = [
                    "phone" => $formattedPhone,
                    "cooldown_sec" => $selmaOtpTempModel->getCountdownSec(),
                    "remaining_submit_otp" => $remainingSubmitOTP,
                ];
                
                $this->logReqRes($logFileError, "validateSelmaSendOTP", $phoneNo, $validate, $customerId, $this->errorMessages);
                return false;
            }
            $request = [
                "P_BU_CODE" => "SELMA",
                "P_NO_HP" =>  $formattedPhone,
                "P_TEMPLATE_TYPE" => "pay_with_point_offline",
                "P_FROM" => "apps_selma",
                "P_CARD_ID" => $cardId,
                "P_OTP" => $otp,
                "P_PASSKEY" => ""
            ];

            // Encrypted Payload
            $encryptedPayload = $aceWebAPI->aceWebAPIEncrypt($request);
            if (!$encryptedPayload) {
                $this->errorCode = $aceWebAPI->getErrorCode();
                $this->errorMessages = $aceWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "aceWebAPIEncrypt", $request, $encryptedPayload, $customerId, $this->errorMessages);
                $transaction->rollback();
                return false;
            }
    
            $result = $selmaWebAPI->selmaPwpSendOTP($encryptedPayload);
            $this->logReqRes($logFile, "selmaPwpSendOTP", $request, $result, $customerId);
            if (!$result) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "selmaPwpSendOTP", $request, $result, $customerId, $this->errorMessages);
                $transaction->rollback();
                return false;
            }
        } catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        $otpResp = "";
        if (getenv('ENVIRONMENT') != 'production') {
            $otpResp = $otp;
        }

        $response = [
            "success" => $result,
            "phone" => $formattedPhone,
            "otp" => $otpResp,
            "remaining_submit_otp" => $remainingSubmitOTP,
        ];

        return $response;
    }

    public function validatePwpOTPSelma($selmaAuth, $redeemPoint, $cartId, $skipCart) {
        if (!$skipCart) {
            $cartAPI = new \Library\Cart();
    
            $cartData = $cartAPI->getCartByCartId($cartId);
            if (!$cartData) {
                $this->errorCode = $cartAPI->getErrorCode();
                $this->errorMessages = json_encode($cartAPI->getErrorMessages());
    
                return false;
            }
   
            $voucherAmount = (int)$redeemPoint * (int)getenv('POINT_VOUCHER_WORTH_SLM'); // n * 2500

            $calcTotal = (int)$cartData['subtotal'] - (int)$cartData['discount_amount'] - (int)$cartData['gift_cards_amount'];
            if ((int)$voucherAmount > (int)$calcTotal) {
                $this->errorCode = 400;
                $this->errorMessages = "Penukaran point tidak boleh melebihi harga yang harus dibayarkan";
    
                return false;
            }
        }

        $tieringData = $this->getTieringCustomerSelma($selmaAuth["uid"], $selmaAuth["sessionid"]);
        if(! $tieringData)
            return $this->sendErrorResponse($this->selmaAppLib->getErrorCode(), $this->selmaAppLib->getErrorMessages());

        $customerPoints = 0;
        $customerPoints = (int) $tieringData["point"];
        if ((int) $redeemPoint > $customerPoints) {
            $this->errorCode = 400;
            $this->errorMessages = "Point yang ditukarkan tidak boleh lebih dari poin yang dimiliki";

            return false;
        }        

        return true;
    }

    public function postPwpSubmitOTPSelma($selmaAuth, $customerId, $phoneNo, $cardId, $selmaCustId, $otp, $redeemPoint, $cartId, $skipCart) {
        $logFile = "selma/selma_pwp_submit_otp";
        $logFileError = "selma/selma_pwp_submit_otp_error";

        $selmaWebAPI = new \Library\SelmaWebAPI();

        $validate = $this->validatePwpOTPSelma($selmaAuth, $redeemPoint, $cartId, $skipCart);
        if (!$validate) {
            $this->logReqRes($logFileError, "validatePwpOTPSelma", $selmaAuth, $validate, $customerId, $this->errorMessages);
            return false;
        }

        $selmaOtpTempModel = new \Models\CustomerSelmaOtpTemp();
        $customerPointRedeemModel = new \Models\CustomerPointRedeem();
        $salesRuleVoucherModel = new \Models\SalesruleVoucher();
        $customerPointRedeemModel->useWriteConnection();
        $salesRuleVoucherModel->useWriteConnection();

        $manager = new TxManager();
        $manager->setDbService($selmaOtpTempModel->getConnection());
        $manager->setDbService($customerPointRedeemModel->getConnection());
        $manager->setDbService($salesRuleVoucherModel->getConnection());
        $transaction = $manager->get();

        $otpType = "pay_with_point";
        $ruleId = getenv('REDEEM_SALES_RULE_SLM');
        if ($skipCart) {
            $otpType = "redeem_voucher_online";
            $ruleId = getenv('REDEEM_ONLINE_VOUCHER_SALES_RULE_SLM');
        }
        
        // validate otp
        $formattedPhone = $this->formatPhoneNumberInternational($phoneNo);
        try {
            $validate = $selmaOtpTempModel->validateSelmaSubmitOTP($customerId, "phone", $formattedPhone, $otp, $otpType, $transaction);
            if (!$validate) {
                $this->errorCode = $selmaOtpTempModel->getErrorCode();
                $this->errorMessages = $selmaOtpTempModel->getErrorMessages();
                $logData = [
                    "cart_id" => $cartId,
                    "phone" => $formattedPhone,
                    "otp" => $otp,
                    "otp_type" => $otpType,
                ];
                $this->logReqRes($logFileError, "validateSelmaSubmitOTP", $logData, $validate, $customerId, $this->errorMessages);
                return false;
            }

            if (getenv("SELMA_ENABLE_OTP_LIMIT_DAILY")) {
                if ($skipCart) {
                    $remainingSubmitOTP = getenv("SELMA_REDEEM_ONLINE_VOUCHER_OTP_LIMIT_DAILY") - $validate;
                } else {
                    $remainingSubmitOTP = getenv("SELMA_PWP_OTP_LIMIT_DAILY") - $validate;
                }
            } else {
                $remainingSubmitOTP = 1;
            }

            $voucherGeneratorParams = [
                'rule_id' => $ruleId,
                'customer_id' => $customerId,
                'company_code' => 'ODI',
                'type' => 5, // Type = redeem
                'expiration_date' => date("Y-m-d 23:59:59", strtotime("+6 month", time())),
                'qty' => 1,
                'voucher_amount' => (int)$redeemPoint * (int)getenv('POINT_VOUCHER_WORTH_SLM'),
                'length' => 7,
                'format' => 'alphanum',
                'prefix' => getenv('POINT_REDEEM_PREFIX_HO'),
            ];
            $this->marketingLib->generateVoucher($voucherGeneratorParams);
            $this->marketingLib->validateGeneratedVoucher($voucherGeneratorParams);
    
            $voucherGeneratorParams['voucher_code'] = $this->marketingLib->getVoucherList()[0];

            // Save to table salesrule_voucher
            $salesRuleVoucherModel->assign($voucherGeneratorParams);
            if (!$salesRuleVoucherModel->save()) {
                $this->errorCode = 400;
                $this->errorMessages = implode('|', $salesRuleVoucherModel->getMessages());
                $this->logReqRes($logFileError, "saveSalesRuleVoucherModel", $voucherGeneratorParams, false, $customerId, $this->errorMessages);
                return false;
            }
            $salesRuleVoucherModel->setTransaction($transaction);

            $redeemModelParams = [
                'customer_id' => $customerId,
                'point_redeemed' => $redeemPoint,
                'sales_rule_voucher_id' => $salesRuleVoucherModel->getVoucherId(),
                'company_code' => 'SLM',
                'created_at' => date('Y-m-d')
            ];

            // Save to table customer_point_redeem
            $customerPointRedeemModel->assign($redeemModelParams);
            if (!$customerPointRedeemModel->save()) {
                $this->errorCode = 400;
                $this->errorMessages = implode('|', $customerPointRedeemModel->getMessages());
                $this->logReqRes($logFileError, "saveCustomerPointRedeemModel", $redeemModelParams, false, $customerId, $this->errorMessages);
                return false;
            }
            $customerPointRedeemModel->setTransaction($transaction);

            $now = new \DateTime("now");
            $pointRedeemed = (int)$redeemPoint * -1;
    
            // Adding '-' after CP
            $voucherAsParam = substr_replace($voucherGeneratorParams['voucher_code'], '-', 2, 0);

            $req = array(
                "P_company_type" => "I",
                "P_cust_id" => $selmaCustId,
                "P_card_id" => $cardId,
                "P_receive_no" => $voucherAsParam, // ex: CP-AF789C78
                "P_jmlkenapoint" => "0", // Default when redeem point
                "P_factor" => getenv('POINT_VOUCHER_WORTH_SLM'), // Factor is how much money converted for every point used
                "P_reward" => "0", // Because customer doesn't get reward from this transaction
                "P_jmlpoint" => (string)$pointRedeemed,
                "P_createdate" => $now->format('Y-m-d H:i:s')
            );
      
            $pointRedeemed = $selmaWebAPI->insertPointSelma($req);
            $this->logReqRes($logFile, "insertPointSelma", $req, $pointRedeemed, $customerId);
            if (!$pointRedeemed) {
                $this->errorCode = $selmaWebAPI->getErrorCode();
                $this->errorMessages = $selmaWebAPI->getErrorMessages();
                $this->logReqRes($logFileError, "insertPointSelma", $req, $pointRedeemed, $customerId, $this->errorMessages);
                return false;
            }
        }  catch (\Exception $exception) {
            return false;
        }

        $transaction->commit();

        // Posting Journal 3601
        $this->kawanLamaSystemLib->postingJournal($salesRuleVoucherModel, "SLM");
        $this->logReqRes($logFile, "postingJournal", $salesRuleVoucherModel->toArray(), true, $customerId);

        if (!$skipCart) {
            $cartAPI = new \Library\Cart();
    
            $cartData = $cartAPI->postCartRedeemVoucher($cartId, $voucherGeneratorParams['voucher_code']);
            if (!$cartData) {
                $this->errorCode = $cartAPI->getErrorCode();
                $this->errorMessages = $cartAPI->getErrorMessages();
                $this->logReqRes($logFileError, "postCartRedeemVoucher", $cartId, $cartData, $customerId, $this->errorMessages);
    
                return false;
            }
        }

        $response = [
            "success" => true,
            "remaining_submit_otp" => $remainingSubmitOTP,
        ];

        return $response;
    }
}
