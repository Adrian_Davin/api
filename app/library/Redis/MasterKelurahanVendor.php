<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class MasterKelurahanVendor extends \Library\Redis
{
    private static $prefix  = 'masterKelurahanVendor';
    private static $prefix_list = 'masterKelurahanVendor:list';
    protected static $redisCon;
    public static $fields = array(
        'id',
        'kelurahan_id',
        'vendor_name',
        'vendor_kelurahan_id',
        'created_at',
        'updated_at'
    );

    public static function getKelurahanVendor($key, $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $cacheKey = sprintf("%s:%s", self::$prefix, $key);
        if(self::$redisCon->exists($cacheKey)) {
            return self::$redisCon->hMGet($cacheKey, $fields);
        }

        return array();
    }

    public static function getListKelurahanVendor($fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $listKelurahanVendor = array();
            $kelurahanVendorKeys = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($kelurahanVendorKeys as $kelurahanVendorKey) {
                $listKelurahanVendor[] = self::getKelurahanVendor($kelurahanVendorKey, $fields);
            }

            return $listKelurahanVendor;
        }

        return array();
    }

    public static function setListKelurahanVendor($listKelurahanVendor = array())
    {
        if(empty($listKelurahanVendor)) {
            return false;
        }

        self::$redisCon = parent::connect();
        foreach($listKelurahanVendor as $kelurahanVendorKey => $kelurahanVendorData)
        {
            self::$redisCon->rPush(self::$prefix_list, $kelurahanVendorKey);
            self::setKelurahanVendor($kelurahanVendorKey, $kelurahanVendorData);
        }

        return true;
    }

    public static function setKelurahanVendor($key, $data = array())
    {
        if(empty($key)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $cacheKey = sprintf("%s:%s", self::$prefix, $key);
        self::$redisCon->hMSet($cacheKey, $data);

        return true;
    }

    public static function deleteListKelurahanVendor()
    {
        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $kelurahanVendorKeys = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($kelurahanVendorKeys as $kelurahanVendorKey) {
                self::deleteKelurahanVendor($kelurahanVendorKey);
            }
            self::$redisCon->delete(self::$prefix_list);
        }
    }

    public static function deleteKelurahanVendor($key = "")
    {
        if(empty($key)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $cacheKey = sprintf("%s:%s", self::$prefix, $key);
        self::$redisCon->delete($cacheKey);
    }
}
