<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 1/11/2017
 * Time: 11:08 AM
 */

namespace Library\Redis;

class ShippingCourierCredential extends \Library\Redis
{
    private static $prefix  = 'ShippingCourierCredential:';

    public static $fields = array(
        'courier_account',
        'company_code',
        'username',
        'password',
        'company_id',
        'allocation_code'
    );

    protected static $redisCon;

    public static function getCourier($assignation = "", $fields = array())
    {
        $courierData = array();

        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if(self::$redisCon->exists(self::$prefix.$assignation)) {
            $courierData = self::$redisCon->hMGet(self::$prefix.$assignation, $fields);
        }

        return $courierData;

    }

    public static function setCourier($assignation = "", $courierData = array())
    {
        if(empty($assignation)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->hMSet(self::$prefix.$assignation, $courierData);

        return true;
    }

    public static function deleteCourier($assignation = "")
    {
        if(empty($assignation)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix.$assignation);
    }

}
