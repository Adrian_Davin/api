<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class MasterKecamatanVendor extends \Library\Redis
{
    private static $prefix  = 'masterKecamatanVendor';
    private static $prefix_list = 'masterKecamatanVendor:list';
    protected static $redisCon;
    public static $fields = array(
        'id',
        'kecamatan_id',
        'vendor_name',
        'vendor_kecamatan_id',
        'created_at',
        'updated_at'
    );

    public static function getKecamatanVendor($key, $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $cacheKey = sprintf("%s:%s", self::$prefix, $key);
        if(self::$redisCon->exists($cacheKey)) {
            return self::$redisCon->hMGet($cacheKey, $fields);
        }

        return array();
    }

    public static function getListKecamatanVendor($fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $listKecamatanVendor = array();
            $kecamatanVendorKeys = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($kecamatanVendorKeys as $kecamatanVendorKey) {
                $listKecamatanVendor[] = self::getKecamatanVendor($kecamatanVendorKey, $fields);
            }

            return $listKecamatanVendor;
        }

        return array();
    }

    public static function setListKecamatanVendor($listKecamatanVendor = array())
    {
        if(empty($listKecamatanVendor)) {
            return false;
        }

        self::$redisCon = parent::connect();
        foreach($listKecamatanVendor as $kecamatanVendorKey => $kecamatanVendorData)
        {
            self::$redisCon->rPush(self::$prefix_list, $kecamatanVendorKey);
            self::setKecamatanVendor($kecamatanVendorKey, $kecamatanVendorData);
        }

        return true;
    }

    public static function setKecamatanVendor($key, $data = array())
    {
        if(empty($key)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $cacheKey = sprintf("%s:%s", self::$prefix, $key);
        self::$redisCon->hMSet($cacheKey, $data);

        return true;
    }

    public static function deleteListKecamatanVendor()
    {
        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $kecamatanVendorKeys = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($kecamatanVendorKeys as $kecamatanVendorKey) {
                self::deleteKecamatanVendor($kecamatanVendorKey);
            }
            self::$redisCon->delete(self::$prefix_list);
        }
    }

    public static function deleteKecamatanVendor($key = "")
    {
        if(empty($key)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $cacheKey = sprintf("%s:%s", self::$prefix, $key);
        self::$redisCon->delete($cacheKey);
    }
}
