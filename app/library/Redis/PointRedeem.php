<?php

namespace Library\Redis;

class PointRedeem extends \Library\Redis
{
    private static $prefix  = 'pointRedeem:';

    protected static $redisCon;

    public static function getPointRedeemProgress($customer_id)
    {
        $inProgress = "-1";

        if(empty($customer_id)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if(self::$redisCon->exists(self::$prefix.$customer_id)) {
            $inProgress = self::$redisCon->Get(self::$prefix.$customer_id);
        }

        return $inProgress;
    }

    public static function setPointRedeemProgress($customer_id)
    {
        $isInProgress = "1";
        $expirationTime = 60; // 1 minute

        if(empty($customer_id)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->Set(self::$prefix.$customer_id, $isInProgress);
        self::$redisCon->expire(self::$prefix.$customer_id, $expirationTime);

        return true;
    }

    public static function deletePointRedeemProgress($customer_id)
    {
        if(empty($customer_id)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix.$customer_id);

        return true;
    }

}
