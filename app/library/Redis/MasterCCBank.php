<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class MasterCCBank extends \Library\Redis
{
    private static $prefix  = 'masterCCBank:';

    public static $fields = array(
        'bin',
        'card_type',
        'country_code',
        'bank_name'
    );

    protected static $redisCon;

    public static function getBank($bin = "", $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        /**
         * remove all format and we only need 6 first digit
         */
        $bin = str_replace(" ", "", $bin);
        $bin = substr($bin,0,6);

        if(self::$redisCon->exists(self::$prefix.$bin)) {
            $bankData = self::$redisCon->hMGet(self::$prefix.$bin, $fields);
            return $bankData;
        }

        return array();

    }

    public static function setBank($bin = "", $cityData = array())
    {
        if(empty($bin)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->hMSet(self::$prefix.$bin,$cityData);

        return true;
    }

    public static function deleteBank($bin = "")
    {
        if(empty($bin)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix.$bin);
    }



}
