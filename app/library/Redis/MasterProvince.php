<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class MasterProvince extends \Library\Redis
{
    private static $prefix  = 'masterProvince:';
    private static $prefix_list = 'masterProvince:list';
    private static $prefix_by_country = 'masterProvince:byCountry';

    public static $fields = array(
        'province_id',
        'country_id',
        'province_code',
        'province_name',
        'timezone',
        'city'
    );

    protected static $redisCon;

    public static function getProvinceData($province_code = "", $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if(self::$redisCon->exists(self::$prefix.$province_code)) {

            $provinceData = self::$redisCon->hMGet(self::$prefix.$province_code, $fields);
            $provinceData['city'] = json_decode($provinceData['city'],true);
            return $provinceData;
        }

        return array();

    }

    public static function getProvinceListByCountry($country_id = "", $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        self::$redisCon = parent::connect();

        if(self::$redisCon->exists(self::$prefix_by_country.$country_id)) {
            $provinceData = self::$redisCon->hMGet(self::$prefix_by_country.$country_id, $fields);
            $provinceData['city'] = json_decode($provinceData['city'],true);
            return $provinceData;
        }

        return array();
    }

    public static function getProvinceList($fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $provinces = array();
            $provinceKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($provinceKeyList as $keyProvince) {
                $provinces[] = self::getProvinceData($keyProvince, $fields);
            }

            return $provinces;
        }

        return array();
    }

    public static function setProvinces($provinces = array())
    {
        if(empty($provinces)) {
            return false;
        }

        self::$redisCon = parent::connect();
        foreach($provinces as $provinceId => $province)
        {
            self::$redisCon->rPush(self::$prefix_list, $provinceId);
            self::setProvince($provinceId, $province);
        }

        return true;
    }

    public static function setProvince($provinceId = "", $provinceData = array())
    {
        if(empty($provinceId)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $provinceData['city'] = json_encode($provinceData['city']);
        self::$redisCon->hMSet(self::$prefix.$provinceId,$provinceData);

        return true;
    }

    public static function setProvinceByCountry($countryId = "", $provinces = array())
    {
        if(empty($provinces)) {
            return false;
        }

        self::$redisCon = parent::connect();
        foreach($provinces as $countryId => $province)
        {
            self::$redisCon->rPush(self::$prefix_by_country, $countryId);
            self::setProvince($countryId, $province);
        }

        return true;
    }

    public static function deleteProvinces()
    {
        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $provinceKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($provinceKeyList as $keyProvince) {
                self::deleteProvince($keyProvince);
            }
            self::$redisCon->delete(self::$prefix_list);
        }
    }

    public static function deleteProvince($keyProvince = "")
    {
        if(empty($keyProvince)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix.$keyProvince);
    }

    public static function deleteProvinceByCountry($keyCountry = "")
    {
        if(empty($keyCountry)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix_by_country.$keyCountry);
    }
}
