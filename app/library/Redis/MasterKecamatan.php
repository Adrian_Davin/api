<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class MasterKecamatan extends \Library\Redis
{
    private static $prefix  = 'masterKecamatan:';
    private static $prefix_list = 'masterKecamatan:list';

    public static $fields = array(
        'kecamatan_id',
        'city_id',
        'kecamatan_name',
        'coding',
        'kecamatan_code',
        'kode_jalur',
        'sap_exp_code',
        'kelurahan',
        'same_day',
        'instant_delivery',
        'geolocation'
    );

    protected static $redisCon;

    public static function getKecamatanData($kecamatanId = "", $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if(self::$redisCon->exists(self::$prefix.$kecamatanId)) {

            $kecamatanData = self::$redisCon->hMGet(self::$prefix.$kecamatanId, $fields);
            $kecamatanData['kelurahan'] = json_decode($kecamatanData['kelurahan'],true);
            return $kecamatanData;
        }

        return array();

    }

    public static function getKecamatanList($fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $kecamatans = array();
            $kecamatanKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($kecamatanKeyList as $keyKecamatan) {
                $kecamatans[] = self::getKecamatanData($keyKecamatan, $fields);
            }

            return $kecamatans;
        }

        return array();
    }

    public static function setKecamatans($kecamatans = array())
    {
        if(empty($kecamatans)) {
            return false;
        }

        self::$redisCon = parent::connect();
        foreach($kecamatans as $kecamatanId => $kecamatan)
        {
            self::$redisCon->rPush(self::$prefix_list, $kecamatanId);
            self::setKecamatan($kecamatanId, $kecamatan);
        }

        return true;
    }

    public static function setKecamatan($kecamatanId = "", $kecamatanData = array())
    {
        if(empty($kecamatanId)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $kecamatanData['kelurahan'] = json_encode($kecamatanData['kelurahan']);
        self::$redisCon->hMSet(self::$prefix.$kecamatanId,$kecamatanData);

        return true;
    }

    public static function deleteKecamatans()
    {
        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $kecamatanKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($kecamatanKeyList as $keyKecamatan) {
                self::deleteKecamatan($keyKecamatan);
            }
            self::$redisCon->delete(self::$prefix_list);
        }
    }

    public static function deleteKecamatan($keyKecamatan = "")
    {
        if(empty($keyKecamatan)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix.$keyKecamatan);
    }



}
