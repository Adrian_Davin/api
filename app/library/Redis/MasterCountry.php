<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class MasterCountry extends \Library\Redis
{
    private static $prefix  = 'masterCountry:';
    private static $prefix_list = 'masterCountry:list';

    public static $fields = array(
        'country_id',
        'country_code',
        'country_name',
        'province'
    );

    protected static $redisCon;

    public static function getCountryData($countryId = "", $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if(self::$redisCon->exists(self::$prefix.$countryId)) {

            $countryData = self::$redisCon->hMGet(self::$prefix.$countryId, $fields);
            $countryData['province'] = json_decode($countryData['province'],true);
            return $countryData;
        }

        return array();

    }

    public static function getCountryList($fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }
        
        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $countries = array();
            $countryKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($countryKeyList as $keyCountry) {
                $countries[] = self::getCountryData($keyCountry, $fields);
            }

            return $countries;
        }

        return array();
    }

    public static function setCountries($countries = array())
    {
        if(empty($countries)) {
            return false;
        }

        self::$redisCon = parent::connect();
        foreach($countries as $countryId => $country)
        {
            self::$redisCon->rPush(self::$prefix_list, $countryId);
            self::setCountry($countryId, $country);
        }

        return true;
    }

    public static function setCountry($countryId = "", $countryData = array())
    {
        if(empty($countryId)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $countryData['province'] = json_encode($countryData['province']);
        self::$redisCon->hMSet(self::$prefix.$countryId,$countryData);

        return true;
    }

    public static function deleteCountries()
    {
        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $countryKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($countryKeyList as $keyCountry) {
                self::deleteCountry($keyCountry);
            }
            self::$redisCon->delete(self::$prefix_list);
        }
    }

    public static function deleteCountry($keyCountry = "")
    {
        if(empty($keyCountry)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix.$keyCountry);
    }



}
