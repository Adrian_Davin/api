<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class CategoryTree extends \Library\Redis
{
    private static $prefix  = 'categoryTree';
    private static $prefixCms  = 'categoryTreeCms';
    private static $postfixMobileApp  = 'mobileApp';
    protected static $redisCon;
    protected static $redisConCategory;

    public static function getCategoryTree($accessType = "", $companyCode = 'ODI', $parentCategoryID = 0, $userAgent = "")
    {
        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if($accessType == "cms"){
            if(self::$redisCon->exists(self::$prefixCms.'_'.$parentCategoryID.'_'.$companyCode))
            {
                $categoryTreeJson = self::$redisCon->get(self::$prefixCms.'_'.$parentCategoryID.'_'.$companyCode);
                $categoryTree = json_decode($categoryTreeJson,true);
                return $categoryTree;
            }
        } else {
            // Now it also can handle access type other than cms
            $categoryCacheKey = self::buildCategoryCacheKey($accessType, $companyCode, $parentCategoryID, $userAgent);

            // Check on redis & retrieve category data if exist
            if (self::$redisCon->exists($categoryCacheKey)) {
                $categoryTreeJson = self::$redisCon->get($categoryCacheKey);
                $categoryTree = json_decode($categoryTreeJson,true);
                return $categoryTree;
            }
        }

        return array();

    }

    public static function saveCategoryTree($category_tree = array(), $accessType = "", $companyCode = 'ODI', $parentCategoryID = 0, $userAgent = '')
    {
        if(empty($category_tree)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if($accessType == "cms"){
            self::$redisCon->set(self::$prefixCms.'_'.$parentCategoryID.'_'.$companyCode, json_encode($category_tree));
        } else {
            $categoryCacheKey = self::buildCategoryCacheKey($accessType, $companyCode, $parentCategoryID, $userAgent);
            self::$redisCon->set($categoryCacheKey, json_encode($category_tree));
        }

        return true;
    }


    public static function deleteCategoryTree($accessType = "", $companyCode = 'ODI', $parentCategoryID = 0)
    {
        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if ($accessType == "cms") {
            $iterator = null;
            do {
                $result = self::$redisCon->scan($iterator,self::$prefixCms.'_*_'.$companyCode);

                if(!empty($result)) {
                    foreach ($result as $key) {
                        self::$redisCon->del($key);
                    }
                }
            } while ($iterator > 0);

            return true;
        } else {
            if(self::$redisCon->exists(self::$prefix)){
                self::$redisCon->del(self::$prefix);
            }

            return true;
        }

    }

    // This function will be use to create category cache key other than categoryTreeCms
    private static function buildCategoryCacheKey($accessType = "", $companyCode = 'ODI', $parentCategoryID = 0, $userAgent = "") {
        $categoryCacheKey = self::$prefix;
        if ($parentCategoryID > 0) {
            $categoryCacheKey = sprintf("%s_%s", $categoryCacheKey, strval($parentCategoryID));
        }

        if (!empty($companyCode)) {
            $categoryCacheKey = sprintf("%s_%s", $categoryCacheKey, $companyCode);
        }

        $mobileAppsUserAgent = ["ios", "android", "RuparupaApps"];
        if (in_array($userAgent, $mobileAppsUserAgent)) {
            // Added postfix to mobile_app
            $categoryCacheKey = sprintf("%s_%s", $categoryCacheKey, self::$postfixMobileApp);
        }

        if (!empty($accessType)) {
            $categoryCacheKey = sprintf("%s_%s", $categoryCacheKey, strtolower($accessType));
        }

        return $categoryCacheKey;
    }

    public static function deleteCategoryCache()
    {
        if(empty(self::$redisConCategory)) {
            self::$redisConCategory = parent::connect($_ENV['REDIS_HOST'], $_ENV['REDIS_PORT'], $_ENV['REDIS_DB_PERSONALIZED']);
        }

        $cachedKeyPersonalize = "product:product_category:personalized";

        if(self::$redisConCategory->exists($cachedKeyPersonalize)){
            self::$redisConCategory->del($cachedKeyPersonalize);
        }

        return true;

    }
}
