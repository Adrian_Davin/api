<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class MasterCity extends \Library\Redis
{
    private static $prefix  = 'masterCity:';
    private static $prefix_list = 'masterCity:list';

    public static $fields = array(
        'city_id',
        'province_id',
        'city_name',
        'kecamatan'
    );

    protected static $redisCon;

    public static function getCityData($city_id = "", $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if(self::$redisCon->exists(self::$prefix.$city_id)) {

            $cityData = self::$redisCon->hMGet(self::$prefix.$city_id, $fields);
            $cityData['kecamatan'] = json_decode($cityData['kecamatan'],true);
            return $cityData;
        }

        return array();

    }

    public static function getCityList($fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $countries = array();
            $cityKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($cityKeyList as $keyCity) {
                $countries[] = self::getCityData($keyCity, $fields);
            }

            return $countries;
        }

        return array();
    }

    public static function setCities($countries = array())
    {
        if(empty($countries)) {
            return false;
        }

        self::$redisCon = parent::connect();
        foreach($countries as $cityId => $city)
        {
            self::$redisCon->rPush(self::$prefix_list, $cityId);
            self::setCity($cityId, $city);
        }

        return true;
    }

    public static function setCity($cityId = "", $cityData = array())
    {
        if(empty($cityId)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $cityData['kecamatan'] = json_encode($cityData['kecamatan']);
        self::$redisCon->hMSet(self::$prefix.$cityId,$cityData);

        return true;
    }

    public static function deleteCities()
    {
        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $cityKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($cityKeyList as $keyCity) {
                self::deleteCity($keyCity);
            }
            self::$redisCon->delete(self::$prefix_list);
        }
    }

    public static function deleteCity($keyCity = "")
    {
        if(empty($keyCity)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix.$keyCity);
    }



}
