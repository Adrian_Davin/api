<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/18/2017
 * Time: 2:23 PM
 */

namespace Library\Redis;


class SalesOrder extends \Library\Redis
{
    private static $prefix  = 'salesOrder:';

    protected static $redisCon;

    public static function setSalesOrder($sales_order_no = "", $sales_order_data = array())
    {
        if(empty($sales_order_data)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        $expired = time(NULL)+172800;

        // Redis only support string, all array need to be convert into string
        foreach($sales_order_data as $key => $val) {
            if(!is_scalar($val)) {
                $sales_order_data[$key] = json_encode($val);
            }
        }

        self::$redisCon->hMSet(self::$prefix.$sales_order_no,$sales_order_data);
        self::$redisCon->expireAt(self::$prefix.$sales_order_no, $expired);

        return true;
    }

    public static function getSalesOrder($sales_order_no = "")
    {
        if(empty($salesOrderData)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if(self::$redisCon->exists(self::$prefix.$sales_order_no)) {
            $salesOrderData = self::$redisCon->hMGet(self::$prefix.$sales_order_no);
            foreach($salesOrderData as $key => $val) {
                if(\Helpers\ParserHelper::isJSON($val)) {
                    $salesOrderData[$key] = json_decode($val,true);
                }
            }
            return $salesOrderData;
        }

        return array();
    }


}