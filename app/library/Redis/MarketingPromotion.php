<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/18/2017
 * Time: 2:23 PM
 */

namespace Library\Redis;


class MarketingPromotion extends \Library\Redis
{
    private static $prefix  = 'marketing:promotion:';

    protected static $redisCon;

    public static $fields = array(
        'shopping_cart',
        'shipping',
        'payment',
        'voucher',
        'invoice_created',
        'exchange_voucher'
    );

    public static function setPromotionCache($date = null, $promotion_data = array())
    {
        if(empty($promotion_data)) {
            return false;
        }

        if(empty($date)) {
            $date = date('Ymd');
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connectRedisMarketingMaster();
        }

        self::$redisCon->hMSet(self::$prefix.$date,$promotion_data);

        return true;
    }

    public static function getPromotionCache($date = null, $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty($date)) {
            $date = date('Ymd');
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connectRedisMarketingReader();
        }

        if(self::$redisCon->exists(self::$prefix.$date)) {
            $promotionData = self::$redisCon->hMGet(self::$prefix.$date, $fields);
            return $promotionData;
        }

        return array();
    }

    public static function deletePromotionCache($date = "")
    {
        if(empty($date)) {
            $date = date('Ymd');
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connectRedisMarketingMaster();
        }

        self::$redisCon->delete(self::$prefix.$date);
    }


}