<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/8/2017
 * Time: 2:38 PM
 */

namespace Library\Redis;


class MasterKelurahan extends \Library\Redis
{
    private static $prefix  = 'masterKelurahan:';
    private static $prefix_list = 'masterKelurahan:list';

    public static $fields = array(
        'kelurahan_id',
        'kecamatan_id',
        'kelurahan_name',
        'status',
        'post_code',
        'geolocation',
        'created_at',
        'updated_at'
    );

    protected static $redisCon;

    public static function getKelurahanData($kelurahanId = "", $fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        if(self::$redisCon->exists(self::$prefix.$kelurahanId)) {

            $kelurahanData = self::$redisCon->hMGet(self::$prefix.$kelurahanId, $fields);
            return $kelurahanData;
        }

        return array();

    }

    public static function getKelurahanList($fields = array())
    {
        if(empty($fields)) {
            $fields = self::$fields;
        }

        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $kelurahans = array();
            $kelurahanKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($kelurahanKeyList as $keyKelurahan) {
                $kelurahans[] = self::getKelurahanData($keyKelurahan, $fields);
            }

            return $kelurahans;
        }

        return array();
    }

    public static function setKelurahans($kelurahans = array())
    {
        if(empty($kelurahans)) {
            return false;
        }

        self::$redisCon = parent::connect();
        foreach($kelurahans as $kelurahanId => $kelurahan)
        {
            self::$redisCon->rPush(self::$prefix_list, $kelurahanId);
            self::setKelurahan($kelurahanId, $kelurahan);
        }

        return true;
    }

    public static function setKelurahan($kelurahanId = "", $kelurahanData = array())
    {
        if(empty($kelurahanId)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->hMSet(self::$prefix.$kelurahanId,$kelurahanData);

        return true;
    }

    public static function deleteKelurahans()
    {
        self::$redisCon = parent::connect();
        if(self::$redisCon->exists(self::$prefix_list)) {
            $kelurahanKeyList = self::$redisCon->lRange(self::$prefix_list,0,-1);
            foreach($kelurahanKeyList as $keyKelurahan) {
                self::deleteKelurahan($keyKelurahan);
            }
            self::$redisCon->delete(self::$prefix_list);
        }
    }

    public static function deleteKelurahan($keyKelurahan = "")
    {
        if(empty($keyKelurahan)) {
            return false;
        }

        if(empty(self::$redisCon)) {
            self::$redisCon = parent::connect();
        }

        self::$redisCon->delete(self::$prefix.$keyKelurahan);
    }



}
