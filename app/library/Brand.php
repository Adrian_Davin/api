<?php

namespace Library;

class Brand
{
    protected $brand;
    protected $errorCode;
    protected $errorMessages;

    public function __construct()
    {
        $this->searchHeroProduct = new \Models\SearchHeroProduct();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function deleteBrand($params = array())
    {
        $brandModel = new \Models\MasterBrand();


        $alertResult = $brandModel::findFirst("brand_id = '". $params['brand_id'] ."'");

        if(!$alertResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Brand not found";
            return false;
        }

        $brandModel->assign($alertResult->toArray());
        $brandModel->setStatus(-1);
        $saveStatus = $brandModel->saveData("delete_brand");;

        if(!$saveStatus) {
            $this->errorCode = $brandModel->getErrorCode();
            $this->errorMessages[] = $brandModel->getErrorMessages();
            return false;
        }

        return true;
    }

    public function getUrlKey($params = array())
    {
        $companyCode = isset($params['company_code'])? $params['company_code'] : 'ODI';
        $redisKey = "brand_url_".$params['brand_id']."_".$companyCode;

        $redis = new \Library\Redis();
        $redisCon = $redis::connect();
        $response = $redisCon->get($redisKey);

        if (!empty($response)) {
            $response = json_decode($response);
            return $response;
        } else {
            $brandModel = new \Models\MasterBrand();
            $sql = "SELECT url_key FROM master_url_key muk JOIN product_category_mapping_brand pcmb ON muk.reference_id = pcmb.category_id
            WHERE section = 'custom_category' AND status = '10' AND pcmb.brand_id = '".$params['brand_id']."' AND pcmb.company_code = '".$companyCode."' ORDER BY created_at DESC LIMIT 1";
            $result = $brandModel->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            
            $sqlResult = $result->fetchAll();

            $finalResult = array();
            if(!empty($sqlResult)) {
                foreach($sqlResult as $res){
                    $res['url_key'] .= ".html";
                    $finalResult = array($res);
                }
                if(!empty($finalResult)){
                    $redisCon->set($redisKey,json_encode($finalResult));
                }
            }
            return $finalResult;
        }
    }
}
