<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/15/2017
 * Time: 3:00 PM
 * User: roesmien_ecomm
 * Date: 5/16/2017
 * Time: 9:33 AM
 */

namespace Library;
use Helpers\RouteHelper;
use Models\MasterUrlKey;
use Models\Product2Category;
use Models\ProductCategory;
use Phalcon\Db as Database;
use Phalcon\Mvc\Model;

class MasterAttributes
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function getListMasterAttributes($params = array())
    {
        unset($params['master_app_id']); // Event Purpose
        unset($params['admin_user_id']); // Event Purpose

        $optionOrderBy = isset($params['option_order_by']) ? $params['option_order_by'] : true;
        $limit = isset($params['limit']) ? $params['limit'] : "";
        $offset = isset($params['offset']) ? $params['offset'] : "";
        $detail = isset($params['detail']) ? $params['detail'] : "true";
        unset($params['option_order_by'], $params['limit'], $params['offset'], $params['detail']);

        $masterAttributesModel = new \Models\MasterAttributes();

        $query = $masterAttributesModel::query();

        foreach($params as $key => $val)
        {
            if ($key === 'not_attribute_type') {
                $query->andWhere("attribute_type != '" . $val ."'");
            } else if ($key === 'attribute_id') {
                $arrAttributeId = explode(',',$val);
                $attributeId = implode(',',$arrAttributeId);
                $query->andWhere("attribute_id in (" . $attributeId .")");
            } else {
                $query->andWhere($key ." = '" . $val ."'");
            }
        }

        $query->orderBy('attribute_label');

        if (!empty($limit)) {
            $query->limit($limit,$offset);
        }
        $attribute = $query->execute();

        $attributeList = $attribute->toArray();

        if(!empty($attributeList)){
            if ($detail == "true") {
                $attributeList = $this->detailAttribute($attributeList, $optionOrderBy);
            }
            return $attributeList;
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "Master Attribute not found";
            return;
        }
    }


    public function getListMasterAttributesOnSet($params = array())
    {
        unset($params['master_app_id']); // Event Purpose
        unset($params['admin_user_id']); // Event Purpose

        $masterAttributeModel = new \Models\MasterAttributes();
        $attributeList = $masterAttributeModel->attributeSet2Attribute($params['attribute_set_id']);

        if(!empty($attributeList)){
            $attributeList = $this->detailAttribute($attributeList);
            return $attributeList;
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "Master Attribute not found";
            return;
        }
    }

    public function getListMasterAttributesOnVariantSet($params = array())
    {
        unset($params['master_app_id']); // Event Purpose
        unset($params['admin_user_id']); // Event Purpose

        $masterAttributeModel = new \Models\MasterAttributes();
        $attributeList = $masterAttributeModel->attributeVariantSet2Attribute($params['attribute_variant_set_id']);

        if(!empty($attributeList)){

            $attributeList = $this->detailAttribute($attributeList);
            return $attributeList;
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "Master Attribute not found";
            return;
        }
    }

    private function detailAttribute($attributeList = array(), $optionOrderBy = true){
        $index = 0;

        foreach($attributeList as $row){
            // Set Option List
            $attributeList[$index]['option_list'] = array();
            if($row['attribute_type'] == 'option'){
                if($row['is_variant']){
                    $options = \Models\AttributeVariantOptions::find('status > 0 and attribute_id = '.$row['attribute_id']);
                    $optionsList = $options->toArray();
                }else{
                    $query = \Models\AttributeOptionValue::query();
                    $query->andWhere("status > 0");
                    $query->andWhere("attribute_id = ".$row['attribute_id']);
                    if ($optionOrderBy) {
                        $query->orderBy('value');
                    }
                    $options = $query->execute();
                    $optionsList = $options->toArray();

                    // because before we use table attributeOption, and field is different so, we readjust the field name
                    foreach($optionsList as $key => $list) {
                        $optionsList[$key]['option_value'] = $list['value'];
                        unset($optionsList[$key]['value']);
                    }
                }
                
                $attributeList[$index]['option_list'] = $optionsList;
            }

            // Set Unit Option List
            $attributeList[$index]['attribute_unit_list'] = array();
            if($row['attribute_unit_id'] != 0){
                $optionsUnit = \Models\AttributeUnitOptions::find('status > 0 and attribute_unit_id = '.$row['attribute_unit_id']);
                $optionsUnitList = $optionsUnit->toArray();
                $attributeList[$index]['attribute_unit_list'] = $optionsUnitList;
            }
            unset($attributeList[$index]['attribute_unit_id']);

            $index ++;
        }
        return $attributeList;
    }

    public function getVariantOption($params = array())
    {
        $attributeVariantOptionsModel = new \Models\AttributeVariantOptions();
        $result = $attributeVariantOptionsModel->find('status > 0 and option_id = '.$params['option_id']);
        if($result){
            return $result->toArray();
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "Master Attribute not found";
            return;
        }
    }

    public function saveMasterAttributes($params = array())
    {
        // only accept article hierarchy lv 4
        if(strlen($params['article_hierarchy']) != 12) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Article Hierarchy must lv 4";
            return false;
        }

        // Check in product_category using article hierarcy that attribute_set_id not zero
        $productCategoryModel = new \Models\ProductCategory();
        $productCategoryResult = $productCategoryModel->findFirst("article_hierarchy = '". $params['article_hierarchy'] ."'");

        if (!empty($productCategoryResult)) {
            $attributeSetId = $productCategoryResult->toArray()['attribute_set_id'];
            if (empty($attributeSetId)) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Attribute Id can't be zero";
                return false;    
            }
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages = "Article Hierarchy not found";
            return false;
        }

        // Find if attribute_name already exists
        $masterAttributesModel = new \Models\MasterAttributes();
        $checkAttributeName = $masterAttributesModel->findFirst("attribute_name = '". $params['attribute_name'] ."'");

        if (!empty($checkAttributeName)){
            $attributeId = $checkAttributeName->toArray()['attribute_id'];
            /**
             * check if name already exists
             * if not exists we insert new attribute set id to attribute set 2 attribute
             * and because master attribute already exists we don't care about attribute option & unit_option
             */
            $attributeSet2AttributeModel = new \Models\AttributeSet2Attribute();
            $checkAttributeSet2AttributeResult = $attributeSet2AttributeModel->findFirst("attribute_set_id = ".$attributeSetId." and attribute_id = ".$attributeId);

            if (!empty($checkAttributeSet2AttributeResult)) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Attribute Name and Attribute Set Id already exists";
                return false;
            } else {
                $dataAttributeSet2Attribute = [
                    'attribute_set_id' => $attributeSetId,
                    'attribute_id' => $attributeId,
                    'status' => 10
                ];
                // insert attribute set id and attribute id to attribute set 2 attribute
                $attributeSet2AttributeModel = new \Models\AttributeSet2Attribute();
                $attributeSet2AttributeModel->assign($dataAttributeSet2Attribute);
                $saveAttributeSet2Attribute = $attributeSet2AttributeModel->saveData("attributeset2attribute");

                if(!$saveAttributeSet2Attribute) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "Failed to insert attribute 2 attribute";
                    return false;
                }
                return;
            }
        }

        if (!empty($params['attribute_unit_id'])) {
            $attributeModel = new \Models\AttributeUnitOptions();
            $checkMasterAttributeResult = $attributeModel->findFirst("attribute_unit_id = '". $params['attribute_unit_id'] ."'");
            if (empty($checkMasterAttributeResult)) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Attribute Unit Id not found";
                return false;
            }

            if ($params['attribute_unit_value'] != '') {
                $exAttributeUnitValue = explode(',', $params['attribute_unit_value']);
                foreach ($exAttributeUnitValue as $ex) {
                    $checkAttributeUnitModel = new \Models\AttributeUnitOptions();
                    $checkAttributeUnitResult = $checkAttributeUnitModel->findFirst("unit_option_value = '". trim($ex) ."' and attribute_unit_id = ".$params['attribute_unit_id']);
                    if (!empty($checkAttributeUnitResult)) {
                        $this->errorCode = "RR302";
                        $this->errorMessages = "Attribute Unit Value Already Exists";
                        return false;
                    }
                }
            }
        }
        
        // insert to master attribute and retrieve attribute_id
        $masterAttributesModel = new \Models\MasterAttributes();
        $masterAttributesModel->assign($params);
        $saveStatus = $masterAttributesModel->saveData('Attribute');

        if(!$saveStatus) {
            $this->errorCode = "RR302";
                    $this->errorMessages = "Failed to insert master attribute";
            return false;
        } else {
                // get last attribute id
            $lastId = $masterAttributesModel->findFirst(['order' => 'attribute_id desc']);
            $attributeId = !empty($lastId) ? $lastId->toArray()['attribute_id'] : 0;

            if (!empty($attributeId)) {
                $dataAttributeSet2Attribute = [
                    'attribute_set_id' => $attributeSetId,
                    'attribute_id' => $attributeId,
                    'status' => 10
                ];
                // insert attribute set id and attribute id to attribute set 2 attribute
                $attributeSet2AttributeModel = new \Models\AttributeSet2Attribute();
                $attributeSet2AttributeModel->assign($dataAttributeSet2Attribute);
                $saveAttributeSet2Attribute = $attributeSet2AttributeModel->saveData("attributeset2attribute");

                if(!$saveAttributeSet2Attribute) {
                    $this->errorCode = "RR302";
                    $this->errorMessages = "Failed to insert attribute 2 attribute";
                    return false;
                }

                // Check attribute Type if option we need to insert to attribute_option_value
                if ($params['attribute_type'] == 'option') {
                    $exAttributeUnitValue = explode('|', $params['attribute_option_value']);
                    foreach ($exAttributeUnitValue as $ex) {
                        // insert attribute_unit_value to attribute_unit_options
                        $dataAttributeOptionValue = [
                            'attribute_id' => $attributeId,
                            'value' => trim($ex),
                            'status' => 10
                        ];
                        $attributeOptionValueModel = new \Models\AttributeOptionValue();
                        $attributeOptionValueModel->assign($dataAttributeOptionValue);
                        $saveAttributeOptionValueModel = $attributeOptionValueModel->saveData("attribute_option_value");

                        if(!$saveAttributeOptionValueModel) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Failed to insert attribute option value";
                            return false;
                        }
                    }
                }

                if (!empty($params['attribute_unit_id']) && $params['attribute_unit_value'] != '') {
                    $exAttributeUnitValue = explode('|', $params['attribute_unit_value']);
                    foreach ($exAttributeUnitValue as $ex) {
                        // insert attribute_unit_value to attribute_unit_options
                        $dataAttributeUnitOptions = [
                            'attribute_unit_id' => $params['attribute_unit_id'],
                            'unit_option_value' => trim($ex),
                            'status' => 10
                        ];
                        $attributeUnitOptionsModel = new \Models\AttributeUnitOptions();
                        $attributeUnitOptionsModel->assign($dataAttributeUnitOptions);
                        $saveAttributeUnitOptionsModel = $attributeUnitOptionsModel->saveData("attribute_unit_options");

                        if(!$saveAttributeUnitOptionsModel) {
                            $this->errorCode = "RR302";
                            $this->errorMessages = "Failed to insert attribute unit value";
                            return false;
                        }
                    }
                }
            } else {
                $this->errorCode = "RR302";
                $this->errorMessages = "Failed to insert master attribute";
                return false;
            }
            return;
        }
    }

    public function addMasterAttributes($params = array())
    {
        // only accept article hierarchy lv 4
        if(strlen($params['article_hierarchy']) != 12) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Article Hierarchy must lv 4";
            return false;
        }

        // Check in product_category using article hierarcy that attribute_set_id not zero
        $productCategoryModel = new \Models\ProductCategory();
        $productCategoryResult = $productCategoryModel->findFirst("article_hierarchy = '". $params['article_hierarchy'] ."'");

        if (!empty($productCategoryResult)) {
            $attributeSetId = $productCategoryResult->toArray()['attribute_set_id'];
            if (empty($attributeSetId)) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Attribute Id can't be zero";
                return false;    
            }
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages = "Article Hierarchy not found";
            return false;
        }

        // Find if attribute_name already exists
        $masterAttributesModel = new \Models\MasterAttributes();
        $checkAttributeName = $masterAttributesModel->findFirst("attribute_name = '". $params['attribute_name'] ."'");

        if (!empty($checkAttributeName)){
            $this->errorCode = "RR302";
            $this->errorMessages = "Master attribute already exists";
            return false;
        }

        $masterAttributesModel = new \Models\MasterAttributes();
        $masterAttributesModel->assign($params);
        $saveStatus = $masterAttributesModel->saveData('Attribute');

        if(!$saveStatus) {
            $this->errorCode = "RR302";
                    $this->errorMessages = "Failed to insert master attribute";
            return false;
        } else {
            // get last attribute id
            $lastId = $masterAttributesModel->findFirst(['order' => 'attribute_id desc']);
            $attributeId = !empty($lastId) ? $lastId->toArray()['attribute_id'] : 0;

            $dataAttributeSet2Attribute = [
                'attribute_set_id' => $attributeSetId,
                'attribute_id' => $attributeId,
                'status' => $params['status']
            ];
            // insert attribute set id and attribute id to attribute set 2 attribute
            $attributeSet2AttributeModel = new \Models\AttributeSet2Attribute();
            $attributeSet2AttributeModel->assign($dataAttributeSet2Attribute);
            $saveAttributeSet2Attribute = $attributeSet2AttributeModel->saveData("attributeset2attribute");

            if(!$saveAttributeSet2Attribute) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Failed to insert attribute 2 attribute";
                return false;
            }

            // Check attribute Type if option we need to insert to attribute_option_value
            if ($params['attribute_type'] == 'option' && $params['attribute_option_value'] != '') {

                $exAttributeUnitValue = explode('|', rtrim($params['attribute_option_value'], '|'));
                foreach ($exAttributeUnitValue as $ex) {
                    // insert attribute_unit_value to attribute_unit_options

                    if ($params['is_group_attribute'] == 10 && $params['is_variant'] == 1) {
                        $attributeOptionValueModel = new \Models\AttributeVariantOptions();
                        $attributeOptionValueModel->assign([
                            'attribute_id' => $attributeId,
                            'option_value' => trim($ex),
                            'status' => $params['status']
                        ]);
                    } else {
                        $attributeOptionValueModel = new \Models\AttributeOptionValue();
                        $attributeOptionValueModel->assign([
                            'attribute_id' => $attributeId,
                            'value' => trim($ex),
                            'status' => $params['status']
                        ]);
                    }

                    $saveAttributeOptionValueModel = $attributeOptionValueModel->saveData("attribute_option_value");

                    if(!$saveAttributeOptionValueModel) {
                        $this->errorCode = "RR302";
                        $this->errorMessages = "Failed to insert attribute option value";
                        return false;
                    }
                }
            }

            return;
        }
    }
}