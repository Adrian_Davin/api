<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Library;

use Helpers\ProductHelper,
    Phalcon\Db;
use Models\CustomerAceDetails;
use Models\CustomerAddress;
use mysql_xdevapi\Exception;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

use Helpers\LogHelper;
use Models\Cart;

class Customer
{

    /**
     * @var $customer \Models\Customer
     */
    protected $customer;

    protected $customerOtp;

    /**
     * @var $customerToken \Models\CustomerToken
     */
    protected $customerToken;

    /**
     * @var $customerToken \Models\CustomerAlert
     */
    protected $customerAlert;

    /**
     * @var $wishlist \Models\Wishlist
     */
    protected $wishlist;

    // ACE
    protected $member_no;

    protected $errorCode;
    protected $errorMessages;
    protected $excludeType;
    protected $includeType;
    protected $isDuplicateDevice;
    protected $isReferralNotFound;

    public function __construct()
    {
        $this->customer = new \Models\Customer();
        $this->customerAceDetails = new \Models\CustomerAceDetails();
        $this->customerOtp = new \Models\CustomerOtp();
        $this->customerToken = new \Models\CustomerToken();
        $this->customerAlert = new \Models\CustomerAlert();
        $this->wishlist = new \Models\Wishlist();
        $this->isDuplicateDevice = false;
        $this->isReferralNotFound = false;
        $this->disposableEmailDomain = new \Models\DisposableEmailDomain();
        $this->affiliate = new \Models\Affiliate();
    }

    /**
     * @return bool
     */
    public function getIsDuplicateDevice()
    {
        return $this->isDuplicateDevice;
    }

    public function setIsDuplicateDevice($value)
    {
        $this->isDuplicateDevice = $value;
    }

        /**
     * @return bool
     */
    public function getIsReferralNotFound()
    {
        return $this->isReferralNotFound;
    }

    public function setIsReferralNotFound($value)
    {
        $this->isReferralNotFound = $value;
    }


    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @return \Models\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param \Models\Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    public function setCustomerFromArray($data_array = array())
    {
        $this->customer->setFromArray($data_array);

        if (!empty($data_array['token'])) {
            $this->customerToken->setFromArray($data_array);
        }
    }

    public function setConditionsFromArray($data_array = array())
    {
        if (!empty($data_array['exclude_type'])) {
            $this->excludeType = $data_array["exclude_type"];
        }
        if (!empty($data_array['include_type'])) {
            $this->includeType = $data_array["include_type"];
        }
    }

    public function customerLogout()
    {
        if (empty($this->customerToken->token)) {
            $this->errorMessages[] = 'Token is required';
            $this->errorCode = "RR100";
            return array();
        }

        $queryConditions = array(
            "conditions" => "token = '" . $this->customerToken->token . "'"
        );
        $customerTokenResult = $this->customerToken->findFirst($queryConditions);

        if (empty($customerTokenResult->count())) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Invalid Email or Password.";
            return;
        } else {
            $customerTokenResult->delete();
        }

        return array();
    }

    private function mergeFingerprint($params = array())
    {
        if (!isset($params['fingerprint'])) {
            return;
        } else {
            $fingerprint = $params['fingerprint'];
            $customerData = $this->customer->findFirst('email="' . $params['email'] . '" AND company_code = "ODI"');
            $newFingerprint = $fingerprint;
            if ($customerData) {
                $oldFingerprint = '';
                if (isset($customerData->toArray()['fingerprint'])) {
                    $oldFingerprint = $customerData->toArray()['fingerprint'];
                }
                if (!empty($oldFingerprint)) {
                    // we need to explode, and only save max 3 device id
                    $exFingerprints = explode(',', $oldFingerprint);
                    $flag = 1;
                    foreach ($exFingerprints as $exFingerprint) {
                        if ($fingerprint !== $exFingerprint && $flag < 3) {
                            $newFingerprint .= ',' . $exFingerprint;
                            $flag++;
                        }
                    }
                }
            }
            $this->customer->setFingerprint($newFingerprint);
        }
    }

    public function customerLogin($data_array = array())
    {
        $use_token = false;
        $this->errorMessages = [];
        $this->errorCode = "";

        $nowDate = date('Y-m-d H:i:s');
        $logParams = $data_array;
        $logParams["date"] = $nowDate;
        $logId = hash("md5", json_encode($logParams));
        
        LogHelper::log("customer_otp_login", "[Log ID: ".$logId."] Data for Login, Params: ".json_encode($data_array), "info");

        if (empty($this->customer->getEmail()) && empty($this->customer->getPhone())) {
            $this->errorMessages[] = 'Email or phone is required';
        }

        if (!empty($this->customerToken->token)) {
            $use_token = true;
            $this->errorMessages = []; // Reset error message, we login using token
        }

        if (!$use_token) {
            if (empty($this->customer->getPassword())) {
                $this->errorMessages[] = 'Password is required';
            }
        }

        if (isset($data_array['secure_key'])) {
            date_default_timezone_set("Asia/Jakarta");
            $mobileTokenValidation = md5(date("Y-m-d") . "_mobilerupa328");
            if ($data_array['secure_key'] <> $mobileTokenValidation) {
                $this->errorMessages= [];
                $this->errorMessages[] = 'Token not valid';
            } else {
                // For OTP
                $customerResult = array();
                if (isset($data_array['phone']) && !empty($data_array['phone'])) {
                    // check phone exist
                    $sosmedLib = new \Library\Sosmed();
                    $customerResult = $sosmedLib->getCustomerByPhone($data_array['phone']);

                    $use_token = true;
                    $this->errorMessages = [];
                } else if (isset($data_array['email']) && !empty($data_array['email'])) {
                    // check email exist
                    $sosmedLib = new \Library\Sosmed();
                    $customerResult = $sosmedLib->getCustomerByEmail($data_array['email']);

                    $use_token = true;
                    $this->errorMessages = [];
                } else {
                    $this->errorMessages[] = 'Email or phone is required';
                }

                if (count($customerResult) == 0 && count($this->errorMessages) == 0) {
                    $this->registerCustomer(true, $data_array);
                    if (!empty($this->getErrorCode())) {
                        $this->errorMessages[] = "Register customer failed";
                    } else {
                        // Automatic login and get voucher list for this customer
                        $customerData = $this->customerLogin($data_array);
                        $customerData['voucher_list'] = $this->getVoucherList();

                        return $customerData;
                    }
                }
            }
        }

        if (!empty($this->errorMessages)) {
            $this->errorCode = "RR100";
            return array();
        }

        $queryConditions = array();
        if ($use_token && isset($data_array['secure_key'])) {
            // in here we need to know if using phone or email.
            if (isset($data_array['phone']) && !empty($data_array['phone'])) {
                $queryConditions['conditions'] = "phone = '" . $this->customer->getPhone() . "' and company_code = '" . $this->customer->getCompanyCode() . "'";
            } else if (isset($data_array['email']) && !empty($data_array['email'])) {
                $queryConditions['conditions'] = "email = '" . $this->customer->getEmail() . "' and company_code = '" . $this->customer->getCompanyCode() . "'";
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Email or phone is required";
                return;
            }
        } else if ($use_token) {
            // Login using token
            $queryConditions = array(
                "conditions" => "token = '" . $this->customerToken->token . "'"
            );

            $customerTokenResult = $this->customerToken->findFirst($queryConditions);

            if (empty($customerTokenResult->count())) {
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Invalid Email or Password.";
                return;
            } else {
                $queryConditions = array(
                    "conditions" => "customer_id = '" . $customerTokenResult->customer_id . "'"
                );
            }
        } else {
            // ini sama persis sih, how ?
            if (isset($data_array['phone']) && !empty($data_array['phone'])) {
                $queryConditions['conditions'] = "phone = '" . $this->customer->getPhone() . "' and company_code = '" . $this->customer->getCompanyCode() . "'";
            } else if (isset($data_array['email']) && !empty($data_array['email'])) {
                $queryConditions['conditions'] = "email = '" . $this->customer->getEmail() . "' and company_code = '" . $this->customer->getCompanyCode() . "'";
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Email or phone is required";
                return;
            }
        }

        $queryConditions['conditions'] .= " AND status = 10";
        $queryConditions['order'] = "last_login DESC";
        $queryConditions['limit'] = 1;

        $customerResult = $this->customer->findFirst($queryConditions);
        if ($customerResult) {
            $customerData = $customerResult->toArray(array('customer_id', 'first_name', 'last_name', 'email', 'phone', 'is_phone_verified', 'is_email_verified', 'birth_date', 'gender', 'last_login', 'status', 'is_new', 'password', 'registered_by'), true);
            LogHelper::log("customer_otp_login", "[Log ID: ".$logId."] Customer found, Data: ".json_encode($customerData), "info");
            $this->customer->setRegisteredBy($customerData['registered_by']);
            // First of all, check phone verification by customer_otp table
            $mutatedPhoneNumber = $customerData['phone'];
            $customerOtpModel = new \Models\CustomerOtp();

            $queryCondition = array(
                "conditions" => "customer_id = '" . $customerData['customer_id'] . "'"
            );

            $customerOtpResult = $customerOtpModel->findFirst($queryCondition);
            $this->customer->setIsPhoneVerified(0);
            $this->customer->setIsEmailVerified(0);
            //
            if ($customerOtpResult) {
                $expirationDate = $customerOtpResult->getExpiredOn();
                $otpIsPhoneVerified = $customerOtpResult->getIsPhoneVerified();
                $otpIsEmailVerified = $customerOtpResult->getIsEmailVerified();
                if ($otpIsPhoneVerified <= 0 && $otpIsEmailVerified <= 0) {
                    // return error unverified
                    $this->errorCode = 401;
                    $this->errorMessages[] = "Unauthorized. OTP not verified yet.";
                    return array();
                } else {
                    // renew phone or email verified
                    $otpIsPhoneVerified = $customerOtpResult->getIsPhoneVerified();
                    $otpIsEmailVerified = $customerOtpResult->getIsEmailVerified();
                    $this->customer->setIsPhoneVerified($otpIsPhoneVerified);
                    $this->customer->setIsEmailVerified($otpIsEmailVerified);
                }
            } else {
                $this->errorCode = 401;
                // $this->errorMessages[] = "Unauthorized. OTP not exist.";
                $this->errorMessages[] = "Update aplikasi Anda sekarang untuk dapat melakukan verifikasi akun.";
                return array();
            }

            $sosmedArray = array('facebook', 'google', 'apple');
            $loginUsingSosmed = false;

            if (isset($data_array['login_by'])) {
                if (in_array($data_array['login_by'], $sosmedArray)) {
                    $loginUsingSosmed = true;
                }
            }

            $validatePassword = false;
            if (!$use_token) {
                $hashArr = explode(':', $customerData['password']);
                if ($loginUsingSosmed) {
                    $validatePassword = md5($customerData['email'].$data_array['login_by']) === $data_array['password'];
                } else {
                    switch (count($hashArr)) {
                        case 1:
                            $validatePassword = (hash('sha256', $data_array['password']) === $customerData['password']);
                            break;
                        case 2:
                            $validatePassword = (hash('sha256', $hashArr[1] . $data_array['password']) === $hashArr[0]);
                            break;
                        default:
                            $validatePassword = false;
                    }
                }
            } else if ($use_token) {
                $validatePassword = true;
            }

            // Bypass password validation for mobile apps social login
            $listMobileApps = ["android", "ios", "ace-android", "ace-ios", "informa-android", "informa-ios", "informa-huawei", "selma-android", "selma-ios", "selma-huawei"];
            $excludeMiniCartUserAgent = ["informa-ios", "informa-android", "ace-ios", "ace-android","informa-huawei", "selma-android", "selma-ios", "selma-huawei"];
            if ($loginUsingSosmed && !empty($data_array["user_agent"]) && in_array($data_array["user_agent"], $listMobileApps)) {
                $validatePassword = true;
            }

            if ($validatePassword) {
                if ($customerData['status'] < 0) {
                    $this->errorCode = "RR100";
                    $this->errorMessages[] = "Customer is no longer active";
                } else if ($customerData['status'] == 5) {
                    $this->errorCode = "RR100";
                    $this->errorMessages[] = "Email or phone is not registered";
                } else {
                    unset($customerData['password']);
                    unset($customerData['status']);

                    if (!$this->customer->getRegisteredBy()) {
                        switch ($this->customer->getCompanyCode()) {
                            case 'ODI':
                                $registered_by = 'ruparupa';
                                break;
                            case 'AHI':
                                $registered_by = 'ace hardware indonesia';
                                break;
                            case 'HCI':
                                $registered_by = 'home center indonesia';
                                break;
                            case 'TGI':
                                $registered_by = 'toys kingdom indonesia';
                                break;
                            default:
                                $registered_by = 'ruparupa';
                                break;
                        }
                        $this->customer->setRegisteredBy($registered_by);
                    }

                    // set fingerprint when login
                    $this->mergeFingerprint($data_array);
                    $this->customer->setCustomerId($customerData['customer_id']);
                    $this->customer->setLastLogin(date("Y-m-d H:i:s"));
                    if (!empty($customerData['email'])){
                        $this->customer->setEmail($customerData['email']);
                    }
                    
                    $this->customer->setIsEmailVerified(0);
                    $this->customer->setSkipAttributeOnUpdate(['password', 'first_name', 'last_name', 'is_email_verified', 'phone', 'birth_date', 'gender', 'status', 'is_new', 'created_at']);
                    $this->customer->unSetSkipAttributeOnUpdate(['last_login', 'is_phone_verified']);
                    if ($loginUsingSosmed) {
                        $this->customer->setIsEmailVerified(10);
                        $this->customer->setSkipAttributeOnUpdate(['password', 'first_name', 'last_name', 'phone', 'birth_date', 'gender', 'status', 'is_new', 'created_at']);
                        $this->customer->unSetSkipAttributeOnUpdate(['last_login', 'is_phone_verified', 'is_email_verified']);
                    }
                    $this->customer->saveData();

                    // if login using email and password re-generate the token
                    if (!$use_token) {
                        $myHash = hash_hmac('sha512', $this->customer->getEmail(), $this->customer->getPassword());
                        $token = substr($myHash, 0, -64);

                        $this->customerToken->customer_id = $customerData['customer_id'];
                        $this->customerToken->token = $token;
                        $this->customerToken->created_at = date("Y-m-d H:i:s");
                        $this->customerToken->saveData("customer");
                    } else {
                        if (!isset($data_array['secure_key'])) {
                            $token = $this->customer->customerToken->token;
                        } else {
                            $token = $data_array['secure_key'];
                        }
                    }

                    $customerData['token'] = $token;
              
                    // Check for shopping cart data for this customer
                    $current_cart_id = empty($data_array['cart_id']) ? "" : $data_array['cart_id'];
                    $cartLib = new \Library\Cart();
                    $storeCodeNewRetail = isset($data_array['store_code_new_retail']) ? $data_array['store_code_new_retail'] : '';
                    if (in_array($data_array["user_agent"], $excludeMiniCartUserAgent) ) {
                        $respMergeCart = $cartLib->getCustomerCartId($customerData['customer_id'], $current_cart_id, $data_array['company_code'], $storeCodeNewRetail);
                        $shoppingCartId = !empty($respMergeCart['data']['cart_id']) ? $respMergeCart['data']['cart_id'] : "";
                    } else {
                        $respMergeMinicart = $cartLib->getCustomerMiniCartId($customerData['customer_id'], $data_array["minicart_id"],$data_array["cart_id"], $data_array['company_code'], $storeCodeNewRetail);
                        $shoppingCartId = !empty($respMergeMinicart['data']['cart_id']) ? $respMergeMinicart['data']['cart_id'] : "";
                        if($data_array["fromPayment"] == true) {
                            // possible user agent from web base(not contain prefix 'aa2') & rr
                            $isUseAA2 = $data_array["user_agent"] != "android" && $data_array["user_agent"] != "ios";
                            $respMergeCart = $cartLib->getCustomerCartId($customerData['customer_id'], $data_array["cart_id"], $data_array['company_code'], $storeCodeNewRetail,$isUseAA2);
                            $shoppingCartId = !empty($respMergeCart['data']['cart_id']) ? $respMergeCart['data']['cart_id'] : "";
                        }
                    }
                    $customerData['cart_id'] = $shoppingCartId;

                    if ($loginUsingSosmed) {
                        // update is_email_verified at otp -- only sosmed
                        $dateNow = date("Y-m-d H:i:s");
                        try {
                            $customerOtpModel = new \Models\CustomerOtp();
                            $sql = "UPDATE customer_otp SET email = '".$customerData['email']."', is_email_verified = 10, updated_at = '" . $dateNow . "' WHERE customer_id = " . $customerData['customer_id'];
                            $customerOtpModel->useWriteConnection();
                            $customerOtpModel->getDi()->getShared($customerOtpModel->getConnection())->query($sql);
                        } catch (\Exception $e) {
                            $this->errorCode = "500";
                            $this->errorMessages = "Terjadi kendala pada saat memperbaharui data otp";
                            return false;
                        }
                    }

                    // In here we need to pass the value of email, phone and company_code to queue-v2 for linking customer
                    $customerHelper = new \Helpers\CustomerHelper();
                    $customerHelper->linkingCustomerUsingNsq($customerData["email"], $customerData["phone"], "ODI");

                    $affiliateData = (object)[];
                    $customerData['affiliate'] = $affiliateData;

                    $affiliateData = $this->affiliate->findFirst('customer_id = ' . $customerData['customer_id']);
                    if ($affiliateData) {
                        $customerData['affiliate'] = $affiliateData->getDataArray(array(),true);
                        $sql = "SELECT account_number AS bank_number FROM affiliate_rekening WHERE affiliate_id = '".$affiliateData->getAffiliateID()."' limit 1;";
                        $this->affiliate->useReadOnlyConnection();
                        $result = $this->affiliate->getDi()->getShared($this->affiliate->getConnection())->query($sql);
                        if($result->numRows() > 0) {
                            $result->setFetchMode(Db::FETCH_ASSOC);
                            $affiliateRekening = $result->fetch();
                            $customerData['affiliate']['bank_number'] = $affiliateRekening['bank_number'];
                        } else {
                            $customerData['affiliate']['bank_number'] = '';
                        }
                    }           
                    return $customerData;
                }
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Invalid Email or Password.";
            }
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Invalid Email or Password.";
        }

        return array();
    }

    public function registerCustomer($sending_welcome_email = true, $params = array())
    {
        /*
         * check if consumer sending customer ID, if sent we just need to updated it
         */
        $customerResult = null;
        $guestResultArray = null;
        $customerOtpResult = null;
        $isPhoneVerified = -1;
        $salesOrderModel = new \Models\SalesOrder();

        $nowDate = date('Y-m-d H:i:s');
        $logParams = $params;
        $logParams["date"] = $nowDate;
        $logId = hash("md5", json_encode($logParams));

        LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Data for register, Params: ".json_encode($params), "info");

        if (empty($this->customer->getCustomerId())) { // Create Customer
            $param = array();
            if (!empty($this->customer->getEmail()) && !empty($this->customer->getPhone())) {
                // TODO:
                // First of all, check phone verification by customer_otp table
                $mutatedPhoneNumber = $this->customer->getPhone();
                $customerOtpModel = new \Models\CustomerOtp();
                $customerOtpParam = array(
                    "conditions" => "phone = '" . $mutatedPhoneNumber . "'"
                );

                $customerOtpResult = $customerOtpModel->findFirst($customerOtpParam);
                // $this->customer->setIsPhoneVerified(0);
                $isPhoneVerified = 0;

                if ($customerOtpResult) {
                    // This is the flow where we need to check from customer_otp table, if the user has otp-ed
                    $isPhoneVerified = $customerOtpResult->getIsPhoneVerified();
                    if ($isPhoneVerified > 0) {
                        // $this->customer->setIsPhoneVerified(10);
                        $isPhoneVerified = 10;
                    } else {
                        // return error unverified
                        LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Old apps, is phone verified = 0", "info");
                        $this->errorCode = 401;
                        $this->errorMessages[] = "Unauthorized. OTP not verified yet.";
                        return false;
                    }
                } else {
                    LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Old Apps, customer otp not found", "info");
                    $this->errorCode = 401;
                    // $this->errorMessages[] = "Unauthorized. OTP not exist.";
                    $this->errorMessages[] = "Update aplikasi Anda sekarang untuk dapat melakukan verifikasi akun.";
                    return false;
                }
            } else {
                // throw error, email and phone must be filled
                $this->errorCode = 404;
                $this->errorMessages[] = "Email and phone are required";
                return false;
            }

            // check if email is using disposable domain
            $domain = explode("@", $this->customer->getEmail())[1];
            $param["conditions"] = "domain_name = '" . $domain . "' AND status = 1";
            $emailDomainResult = $this->disposableEmailDomain->findFirst($param);
            if ($emailDomainResult) {
                LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Data to be registered is using disposable email domain", "info");
                $errMessageUsingDisposableDomain = "Email tidak valid. Silakan gunakan email lain";
                $this->errorCode = 400;
                $this->errorMessages[] = $errMessageUsingDisposableDomain;
                return false;
            }

            // in here we need to check email and password
            // First we need to check if email already exist or not
            $param["conditions"] = "(email = '" . $this->customer->getEmail() . "' OR phone = '" . $this->customer->getPhone() . "') AND status IN (1,10) AND company_code = 'ODI'";
            $customerResult = $this->customer->findFirst($param);
            if ($customerResult) {
                // throw error
                LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Data to be registered is exist", "info");

                // Check whether status 10 / 1
                $errMessageCustExist = "Alamat email atau nomor telepon Anda sudah terdaftar";
                if ($customerResult->getStatus() == 1) {
                    $errMessageCustExist = "Akun sudah dihapus dan tidak dapat digunakan kembali";
                }

                $this->errorCode = 404;
                $this->errorMessages[] = $errMessageCustExist;
                return false;
            } else {
                // TODO: Any customer with status 5, it should be checked first if there is a record using this email and phone. If found, you need to update that specific record.  If not, just use OR.
                $param["conditions"] = "(email = '" . $this->customer->getEmail() . "' AND phone = '" . $this->customer->getPhone() . "') AND status = 5 AND company_code = 'ODI'";
                $param["order"] = "created_at DESC";
                $guestResult = $this->customer->findFirst($param);
                if ($guestResult) {
                    $this->customer->setFromArray($guestResult->toArray());
                    $customerResult = $guestResult;
                }
                
                // for searching candidates or delete unused records
                $unsetIndex = -1;
                $param["conditions"] = "(email = '" . $this->customer->getEmail() . "' OR phone = '" . $this->customer->getPhone() . "') AND status = 5 AND company_code = 'ODI'";
                if (!empty($customerResult)) { // it means, this want to delete unused records
                    $param["conditions"] .= " AND customer_id != ".$customerResult->getCustomerId();
                }
                $param["order"] = "created_at DESC";
                $guestResults = $this->customer->find($param);
                if ($guestResults->count() > 0) {
                    if(empty($customerResult)) { // it means, searching for candidates
                        // if there is no result yet
                        foreach ($guestResults as $key => $val) {
                            $guestRegisteredBy = $val->getRegisteredBy();
                            // in here, this still can be used but we must check this customer_id to sales_order. If there is/are order(s) created using this customer_id, don't reuse it!
                            $orderResult = $salesOrderModel->findFirst(array(
                                'conditions' => "customer_id = ".$val->getCustomerId()
                            ));

                            if (empty($orderResult) || $guestRegisteredBy == "shopee" || $guestRegisteredBy == "tokopedia") {
                                // this record can be use
                                $this->customer->setFromArray($val->toArray());
                                $customerResult = $val;
                                $unsetIndex = $key;
                                break;
                            }
                        }
                    }
                }

                // this is for deletion or to make status = 0
                $guestResultArray = $guestResults->toArray();
                if ($unsetIndex > -1) {
                    unset($guestResultArray[$unsetIndex]);
                    $guestResultArray = array_values($guestResultArray);
                }
            }
        } else {
            if (isset($params['change_form'])) {
                $param = array(
                    "conditions" => "customer_id = " . $this->customer->getCustomerId()
                );
                $customerResultChange = $this->customer->findFirst($param);
                if ($customerResultChange) {
                    $registeredBy = $customerResultChange->toArray()['registered_by'];
                    if (substr($registeredBy, 0, 14) == "facebook_leads") {
                        $registered_by = 'changes-' . $registeredBy;
                        $this->customer->setRegisteredBy($registered_by);
                        $this->customer->unSetSkipAttributeOnUpdate(array('registered_by'));
                    }
                }
            }
        }

        $registeredBy = strtoupper(trim($this->customer->getRegisteredBy()));
        $registered_by = "ruparupa";

        $registeredByShopedLandingPage = false;

        $isRegisteredByExist = isset($params['registered_by']);
        if ($isRegisteredByExist){
            if( $params['registered_by'] == 'Shoped Tq Voucher'){
                $registeredByShopedLandingPage = true;
            }
        }

        if ($registeredBy) {
            if (strtolower(substr($registeredBy, 0, 14)) == "facebook_leads") {
                $sending_welcome_email = false;
                $registered_by = strtolower($registeredBy);
            } else if (strtolower(substr($registeredBy, 0, 6)) == "shopee" || strtolower(substr($registeredBy, 0, 9)) == "tokopedia") {
                $registered_by = strtolower($registeredBy);
            } else {
                switch ($registeredBy) {
                    case 'ODI':
                        $registered_by = 'ruparupa';
                        break;
                    case 'AHI':
                        $registered_by = 'ace hardware indonesia';
                        break;
                    case 'HCI':
                        $registered_by = 'home center indonesia';
                        break;
                    case 'TGI':
                        $registered_by = 'toys kingdom indonesia';
                        break;
                    case 'GOOGLE':
                        $registered_by = 'google';
                        break;
                    case 'FACEBOOK':
                        $registered_by = 'facebook';
                        break;
                    case 'GAW':
                        $registered_by = 'give away';
                        break;
                    case 'PROMO':
                        $registered_by = 'promo';
                        break;
                    case 'SHOPBACK': 
                        $registered_by = 'aggregator';
                        break;
                    // Promo New Member
                    case 'PNM': 
                    $registered_by = 'newCampaignRegSem';
                        break;
                    case 'TRAVELOKA': 
                        $registered_by = 'traveloka';
                        break;
                    case 'APPLE':
                        $registered_by = 'apple';
                        break;
                    case 'SHOPED TQ VOUCHER':
                        $registered_by = 'Shoped Tq Voucher';
                        break;
                    case 'LANDINGPAGEWELCOME':
                        $registered_by = 'landingpagewelcome';
                        break;
                    case 'STOREMODE':
                        $registered_by = 'storemode';
                        break;    
                    case 'GOOGLE ACE':
                        $registered_by = 'google ace';
                        break;
                    case 'GOOGLE INFORMA':
                        $registered_by = 'google informa';
                        break;
                    case 'GOOGLE TOYS':
                        $registered_by = 'google toys';
                        break;
                    default:
                        $registered_by = 'ruparupa';
                        break;
                }
            }
        }

        if ($registeredByShopedLandingPage){
            $this->customer->setRegisteredBy('Shoped Tq Voucher');
        }else{
            $this->customer->setRegisteredBy($registered_by);            
        }
        $this->customer->unSetSkipAttributeOnUpdate(array('registered_by'));            

        // If exist we need to check status of customer
        if ($customerResult) {
            // If a customer already have a transaction with us, upgrade it's status to registered customer
            LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Before check if customer is guest, data: ".json_encode($customerResult->toArray()), "info");
            if ($customerResult->getStatus() == 5) {
                $this->customer->setCustomerId($customerResult->getCustomerId());
                $this->customer->setUpdatedAt(date("Y-m-d H:i:s"));
                $this->customer->setStatus(10);
                // we need to set phone and email - by using params, not the model because someone has override it
                // we have to choose which one will be the one we update because too many garbage data.
                $this->customer->setEmail($params['email']);
                $this->customer->setPhone($params['phone']);
                // Set the password using param if exist bcs if user register using guest data it'll cause an error
                if(!empty($params['password'])){
                    $this->customer->setPassword(hash('sha256', $params['password']));
                }

                $this->customer->setIsNew($customerResult->getIsNew());
                // if (($registered_by == "shopee" || $registered_by == "tokopedia") || $registeredByShopedLandingPage) {
                //     $this->customer->setIsNew(0);
                // }
    
                $this->customer->unSetSkipAttributeOnUpdate(['customer_id', 'email', 'phone', 'status', 'is_new']);
            } else if (strtolower(substr($registeredBy, 0, 14)) != "facebook_leads") {
                $this->errorCode = "RR102";
                $this->errorMessages = "Email or phone already registered";
                return false;
            }
        } else {
            // if customer fill member_number, check validity of every member_number using prefix
            $cek = $this->checkMemberNumber($params);
            if($cek){
                
                if(!empty($this->customer->getCustomerId())) { // edit condition
                    $param = array(
                        "conditions" => "customer_id = ".$this->customer->getCustomerId()
                    );
                    $customerResultUpdate = $this->customer->findFirst($param);
                    if($customerResultUpdate){
                        $this->customer->setStatus($customerResultUpdate->getStatus());
                        $this->customer->unSetSkipAttributeOnUpdate(['status']);
                        // If update profile, we use current registered by
                        $this->customer->setRegisteredBy($customerResultUpdate->getRegisteredBy());
                    }
                }else{ // register condition
                    $this->customer->setStatus(10);
                    $this->customer->setIsNew(10);
                    $this->customer->unSetSkipAttributeOnUpdate(['status', 'is_new']);
                }
            }else{
                return false;
            }
        }

        // set fingerprint to detect customer device
        $this->mergeFingerprint($params);

        // set is_phone_verified
        if ($isPhoneVerified >= 0) {
            $this->customer->setIsPhoneVerified($isPhoneVerified);
        }

        // set first_name and last_name if exist
        if (isset($params['first_name']) && $params['first_name'] != "") {
            $this->customer->setFirstName($params['first_name']);
            $this->customer->unSetSkipAttributeOnUpdate(['first_name']);
        }

        if (isset($params['last_name'])) {
            $this->customer->setLastName($params['last_name']);
            $this->customer->unSetSkipAttributeOnUpdate(['last_name']);
        }

        // set is_email_verified to 10 too
        if (strpos($registered_by,'google') !== false || $registered_by == 'facebook' || $registered_by == 'apple') {
            $this->customer->setIsEmailVerified(10);
        }

        if (is_null($this->customer->getPassword())) {
            $this->customer->setSkipAttributeOnUpdate(['password']);
        } else {
            $this->customer->unSetSkipAttributeOnUpdate(['password']);
        }

        if ($this->customer->getCustomerId() == "") {
            $this->customer->setSkipAttributeOnUpdate(['customer_id']);
        }

        //LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Customer data before call saveData, Data: ".json_encode($this->customer->getDataArray([]), true), "info");

        if (!$this->customer->saveData()) {
            LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Error after saveData, Error: ".json_encode($this->customer->getMessages()), "info");
            $this->errorCode = $this->customer->getErrorCode();
            $this->errorCode = $this->customer->getMessages();
            return false;
        } else if ($sending_welcome_email && ($registeredBy == 'ODI' || strtolower($registeredBy) == 'ruparupa' || strtolower($registeredBy) == 'google' || strtolower($registeredBy) == 'facebook' || strtolower($registeredBy) == 'apple')) {
            
            $nsqParams = [
                "company_code" => "ODI",
                "customer_id" => $this->customer->getCustomerId()
            ];

            $nsq = new \Library\Nsq();
            $message = [
                "data" => $nsqParams,
                "message" => "customerRegistrationVoucher"
            ];
            $nsq->publishCluster('customer', $message);

        } else if ($sending_welcome_email && ($registeredBy == 'AHI' || strtolower($registeredBy) == 'ace hardware indonesia' || strtolower($registeredBy) == "google ace" )) {
            
            $nsqParams = [
                "company_code" => "AHI",
                "customer_id" => $this->customer->getCustomerId()
            ];

            $nsq = new \Library\Nsq();
            $message = [
                "data" => $nsqParams,
                "message" => "customerRegistrationVoucher"
            ];
            $nsq->publishCluster('customer', $message);

        } else if($sending_welcome_email && ($registeredBy == 'TGI' || strtolower($registeredBy) == 'toys kingdom indonesia' || strtolower($registeredBy) == "google toys" )){

            $nsqParams = [
                "company_code" => "TGI",
                "customer_id" => $this->customer->getCustomerId()
            ];

            $nsq = new \Library\Nsq();
            $message = [
                "data" => $nsqParams,
                "message" => "customerRegistrationVoucher"
            ];
            $nsq->publishCluster('customer', $message);

        }

        // Have to sub string from character 0 because registered_by changes-facebook_leads no need to get voucher
        if (strtolower(substr($registeredBy, 0, 14)) == "facebook_leads") {
            // Create Voucher Facebook_leads and link to the customer
            if ($registeredBy == "facebook_leads_informa") {
                $prefix = "FBI";
                $ruleId = getenv('RULE_ID_FB_LEADS_INFORMA');
            } else if ($registeredBy == "facebook_leads_ace") {
                $prefix = "FBA";
                $ruleId = getenv('RULE_ID_FB_LEADS_ACE');
            } else {
                $prefix = "FBO";
                $ruleId = getenv('RULE_ID_FB_LEADS_ODI');
            }

            // Make sure customer not get double voucher
            $customerId = empty($this->customer->getCustomerId()) ? 0 : $this->customer->getCustomerId();
            $salesRuleVoucherModel = new \Models\SalesruleVoucher();
            $customerDataVoucher = $salesRuleVoucherModel->findFirst('customer_id = ' . $customerId . ' and rule_id = ' . $ruleId);
            if (!$customerDataVoucher) {
                // Generate Voucher
                $parameters = [
                    "rule_id" => $ruleId,
                    "type" => 3,
                    "created_by" => 0,
                    "expiration_date" => date('Y-m-d H:i:s', strtotime('+30 days')),
                    "qty" => 1,
                    "voucher_amount" => getenv('FB_LEADS_VOUCHER_AMOUNT'),
                    "length" => 7,
                    "prefix" => $prefix,
                    "format" => "alphanum"
                ];
                $marketingLib = new \Library\Marketing();
                $marketingLib->generateVoucher($parameters);
                $voucherList = $marketingLib->getVoucherList();

                if (!empty($voucherList)) {
                    foreach ($voucherList as $voucherCode) {
                        $parameters['voucher_code'] = $voucherCode;
                        $parameters['limit_used'] = 1;
                        $parameters['max_redemption'] = 0;
                        $parameters['status'] = 10;
                        $parameters['customer_id'] = $customerId;
                        $salesRuleVoucherModel = new \Models\SalesruleVoucher();
                        $salesRuleVoucherModel->setFromArray($parameters);
                        $salesRuleVoucherModel->saveData("voucher");
                    }
                }
            }
        }

        // in here dont forget to update another garbage guest data
        if ($guestResultArray) {
            foreach ($guestResultArray as $idx => $guestData) {
                // we need to update here using clean CustomerModel
                $customerModel = new \Models\Customer();
                $customerModel->setCustomerId($guestData['customer_id']);
                $customerModel->setStatus(0);
                $customerModel->saveData();
            }
        }

        $referralLib = new \Library\CustomerReferral();

        // Turn off Refer & Earn features
        // Referral program start point
        // if (isset($params['referral_code']) && strlen($params['referral_code']) > 0 ) {
        //     // Get Referral ID by referral code
        //     $referralCustID = $referralLib->getReferralCustomerIdByReferralCode($params['referral_code']);
        //     if ($referralCustID) {
        //         $params['referral_id'] = $referralCustID;
        //         $params['type'] = $referralCust["type"];
        //     } else {
        //         LogHelper::log("referral_not_found", "[getReferralCustomerIdByReferralCode] Params: ".json_encode($params), "info");
        //         $this->setIsReferralNotFound(true);
        //     }
        // }

        // if (isset($params['referral_id']) && !empty($params['referral_id'])) {
        //     $referralparams['referral_id'] = $params['referral_id'];
        //     $referralparams['user_agent'] = $params['user_agent'];
        //     $referralparams['customer_id'] = empty($this->customer->getCustomerId()) ? 0 : $this->customer->getCustomerId();
            
        //     if ($params["type"] == 'tactical'){
        //         $referralparams['type'] = 'tactical_register';
        //     } else {
        //         $referralparams['type'] = 'referral_register';
        //     }
            
        //     if (isset($params['unique_id'])) {
        //         $referralparams['unique_id'] = $params['unique_id'];
        //     }
        //     $referralLib->referralCustomerRegistration($referralparams);
        //     $this->setIsDuplicateDevice($referralLib->getIsDuplicateDevice());

        // update customer_id to customer_otp table
        $dateNow = date("Y-m-d H:i:s");
        $date = new \DateTime($dateNow);
        $date->add(new \DateInterval('P90D'));
        $addedDate = $date->format('Y-m-d H:i:s');
        if ($customerOtpResult) {
            try {
                $customerOtpModel = new \Models\CustomerOtp();
                LogHelper::log("customer_otp_register", "[Log ID: ".$logId."] Update customer id on customer otp, Data: ".json_encode($customerOtpResult->toArray()), "info");
                $sql = "UPDATE customer_otp SET customer_id = ".$this->customer->getCustomerId().", updated_at = '" . $dateNow . "' WHERE customer_otp_id = " . $customerOtpResult->getCustomerOtpId();
                $customerOtpModel->useWriteConnection();
                $updateResult = $customerOtpModel->getDi()->getShared($customerOtpModel->getConnection())->query($sql);
            } catch (\Exception $e) {
                $this->errorCode = "500";
                $this->errorMessages = "Terjadi kendala pada saat memperbaharui data otp";
                return false;
            }
        }

        return true;
    }

    public function saveGuestCustomer($params = array())
    {
        // in here, not just saving the data, but we need to validate if email or phone has already existed.
        // If existed and status = 5 and has no order, this record can be reuse

        // Find Customer
        $customerModel = new \Models\Customer();
        $result = $customerModel->findFirst(array(
            "conditions" => "(email = '".$params['email']. "' OR phone = '".$params['phone']."') AND status > 5 AND company_code = 'ODI'"
        ));

        if (!empty($result)) {
            // registered, we should break
            $this->errorCode = "400";
            $this->errorMessages = "Alamat e-mail atau nomor telepon telah terdaftar";
            return false;
        } else {
            $customerModel = new \Models\Customer();
            $result = $customerModel->findFirst(array(
                "conditions" => "(email = '".$params['email']. "' AND phone = '".$params['phone']."') AND status = 5 AND company_code = 'ODI'"
            ));

            // found, then we should validate
            $resultArray = array();
            $existingGuestData = array();
            if (!empty($result)) {
                // we can probably use this, but we need to check if this record has orders
                $resultArray = array($result); // this is because findfirst returns object
            } else {
                // if we don't find it, we should check for candidates
                $customerModel = new \Models\Customer();
                $result = $customerModel->findFirst(array(
                    "conditions" => "(email = '".$params['email']. "' OR phone = '".$params['phone']."') AND status = 5 AND company_code = 'ODI'",
                    "order" => "created_at DESC"
                ));

                if (!empty($result)) {
                    $resultArray = array($result);
                }
            }

            if (!empty($resultArray)) {
                // proceed checking
                // foreach($resultArray as $val) { 
                //     // We have to check if these records have orders
                //     $salesOrderModel = new \Models\SalesOrder();
                //     $orderResult = $salesOrderModel->findFirst(array(
                //         "conditions" => "customer_id = ".$val->getCustomerId()
                //     ));

                //     if (empty($orderResult)) {
                //         // if empty, we can use this record
                //         $existingGuestData = $val;
                //         break;
                //     }
                // }

                $this->customer->setFromArray($resultArray[0]->toArray());
                // keep the first registered_by value if exist
                if (!empty($this->customer->getRegisteredBy())){
                    $params['registered_by'] = $this->customer->getRegisteredBy();
                }
            }
            
            $this->customer->setFromArray($params);
        }

        if (!$this->customer->saveData()) {
            $this->errorCode = $this->customer->getErrorCode();
            $this->errorMessages = $this->customer->getMessages();
            return false;
        }

        return true;
    }

    public function checkMemberNumber($params = array())
    {
        // if customer fill member_number, check validity of every member_number using prefix
        if (isset($params['group'])) {
            foreach ($params['group'] as $key => $val) {
                if (!empty($val)) {
                    if ($key == 'AHI') {
                        if (substr($val, 0, 2) != 'AR' && substr($val, 0, 3) != 'TAM' && substr($val, 0, 3) != 'ARE' && substr($val, 0, 2) != 'AV') {
                            $this->errorCode = "RR108";
                            $this->errorMessages = "Member Number " . $val . " not valid";
                            return false;
                        }
                    } else if ($key == 'HCI') {
                        if (substr($val, 0, 2) != 'IR' && substr($val, 0, 3) != 'TIM') {
                            $this->errorCode = "RR108";
                            $this->errorMessages = "Member Number " . $val . " not valid";
                            return false;
                        }
                    } else if ($key == 'TGI') {
                        if (substr($val, 0, 2) != 'SC' && substr($val, 0, 3) != 'TCM') {
                            $this->errorCode = "RR108";
                            $this->errorMessages = "Member Number " . $val . " not valid";
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return true;
    }

    // TODO: Dryed this function below. Remove Redundance
    public function getAllCustomer($params = array())
    {
        // this purpose is for check customer on payment guest checkout. I know this is not yet good.
        $emailAndPhone = "";
        if (isset($params["email_and_phone"])) {
            $emailAndPhone = strtolower($params["email_and_phone"]);
            unset($params['email_and_phone']);
        }
        $arrayEmailPhone = !empty($emailAndPhone) ? explode(",", $emailAndPhone) : array();

        // Set member if exist
        $member = "";
        if (isset($params["member_number"])) {
            $member = $params["member_number"];
            unset($params["member_number"]);
        }

        $customerModel = new \Models\Customer();
        $resultCustomer = $customerModel::query();
        $from = isset($params["from"]) ? $params["from"] : '';
        unset($params['from']);
        if (empty($params['master_app_id']) || $params['master_app_id'] !== $_ENV['MASTER_APP_ID_OMS']) {
            $resultCustomer->where("status >= 10");
        }
        unset($params['master_app_id']);

        $companyCode = 'ODI';
        if (isset($params['company_code'])) {
            $companyCode = $params['company_code'];
            unset($params['company_code']);
        }

        $resultCustomer->andWhere("company_code = '$companyCode'");

        foreach ($params as $key => $val) {
            if ($key == "date_from") {
                $resultCustomer->andWhere("created_at >= " . $val);
            } else if ($key == "date_to") {
                $resultCustomer->andWhere("created_at  <= " . $val);
            } else {
                $resultCustomer->andWhere($key . " = '" . $val . "'");
            }
        }

        // Additional logic for handler guest checkout check customer
        if (!empty($arrayEmailPhone) && count($arrayEmailPhone) > 1) {
            // email always array[0], phone array[1]
            $resultCustomer->andWhere("( email " . " = '" . $arrayEmailPhone[0] . "' OR phone = " . "'" . $arrayEmailPhone[1]. "')");
        }

        $resultCustomer->orderBy("created_at DESC");
        $resultCustomer->limit(1);
        $result = $resultCustomer->execute();

        if (empty($result->count())) {
            if (!empty($member)) {
                // Determine group id
                $groupId = $this->checkGroupId($member);
                if ($groupId == 0) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Member yang Anda masukkan tidak valid";
                    return array();
                }

                $cohesiveBu = array_map('intval', explode(',', getenv('COHESIVE_GROUP_ID')));
                if (!in_array($groupId, $cohesiveBu)) {
                    // Check new member that is supplied by customer
                    $memberData = $this->checkMemberStatus($member);
                    $isVerifiedNum = 0;

                    if (empty($memberData)) { 
                        $this->errorCode = 400;
                        $this->errorMessages = "Gagal mendapatkan informasi member Anda";
                        return;
                    }

                    // Check member status
                    /**
                     * isVerifiedNum
                     * 10 => valid and not expired
                     * 5 => valid and expired
                     * 0 => not valid and expired
                     */
                    if ($memberData["cust_id"] != ""){
                        // Get is verified value 
                        $isVerifiedNum = $memberData["is_verified"];
                    } 
                    
                    if ($isVerifiedNum <= 0) {
                        $this->errorCode = 400;
                        $this->errorMessages = "Member yang Anda masukkan tidak valid";
                        return;
                    }
    
                    // Still return error if expired member is not an ace member
                    if ($isVerifiedNum == 5 && $groupId != 4) {
                        $this->errorCode = 400;
                        $this->errorMessages = "Member yang Anda masukkan telah expired";
                        return;
                    }
                }
            }
            return array();
        }

        // if (empty($result->count())) {
        //     // in here we need to check, if the email owner with status 5 has ordered something. If so, unpermit reusing data
        //     $salesOrderModel = new \Models\SalesOrder();
        //     $resultCustomer = $customerModel::query();
        //     $resultCustomer->where("status = 5");
        //     $companyCode = 'ODI';
        //     if (isset($params['company_code'])) {
        //         $companyCode = $params['company_code'];
        //         unset($params['company_code']);
        //     }

        //     $resultCustomer->andWhere("company_code = '$companyCode'");

        //     foreach ($params as $key => $val) {
        //         if ($key == "date_from") {
        //             $resultCustomer->andWhere("created_at >= " . $val);
        //         } else if ($key == "date_to") {
        //             $resultCustomer->andWhere("created_at  <= " . $val);
        //         } else {
        //             $resultCustomer->andWhere($key . " = '" . $val . "'");
        //         }
        //     }

        //     // Additional logic for handler guest checkout check customer
        //     if (!empty($arrayEmailPhone) && count($arrayEmailPhone) > 1) {
        //         // email always array[0], phone array[1]
        //         $resultCustomer->andWhere("( email " . " = '" . $arrayEmailPhone[0] . "' OR phone = " . "'" . $arrayEmailPhone[1]. "')");
        //     }

        //     // Should this check phone number too?
        //     $resultCustomer->orderBy("created_at DESC");
        //     $result = $resultCustomer->execute();
        //     if (!empty($result->count())) {
        //         //TODO:
        //         // loop through all candidates, and verify is there is a candidate that record can be use. Check to sales
        //         foreach($result as $val) {
        //             // check to sales_order if this customer has order something
        //             $records = $salesOrderModel->findFirst(array(
        //                 "conditions" => "customer_id = ".$val->getCustomerId()
        //             ));
                    
        //             if (empty($records)) {
        //                 return array();
        //             }
        //         }
        //     } else {
        //         return array();
        //     }
        // }

        $allCustomer = array();

        foreach ($result as $key => $customer) {
            $allCustomer[$key] = $customer->toArray(array(), true);

            if (!empty($customer->Customer2Group)) {
                $groups = array();
                foreach ($customer->Customer2Group as $group) {
                    $group_name = $group->CustomerGroup->getGroupName();
                    $groups[$group_name] = $group->getMemberNumber();
                    $verifiedMember = ['AHI', 'HCI', 'TGI', 'SLM'];
                    if (in_array($group_name, $verifiedMember)) {
                        $groups[$group_name.'_is_verified'] = $group->getIsVerified();
                        $groups[$group_name.'_expiration_date'] = $group->getExpirationDate();
                        $groups[$group_name.'_update_timestamp'] = $group->getUpdateTimestamp();
                    }
                }

                // Temporary code
                // This is for old client apps, so it won't crash because we change group name SELMA to SLM
                // Add member SELMA if SLM member exist on customer2group 
                $selmaGroupName = "SLM";
                if (!empty($groups[$selmaGroupName]) && !empty($groups[sprintf("%s_is_verified", $selmaGroupName)])) {
                    $groups["SELMA"] = $groups[$selmaGroupName];
                    $groups["SELMA_is_verified"] = (int) $groups[sprintf("%s_is_verified", $selmaGroupName)];
                    $groups["SELMA_expiration_date"] = $groups[sprintf("%s_expiration_date", $selmaGroupName)];
                    $groups["SELMA_update_timestamp"] = $groups[sprintf("%s_update_timestamp", $selmaGroupName)];
                }

                $allCustomer[$key]['group'] = $groups;
            }

            // This code for feature affiliate program 
            $canRegistAffiliate = $this->whitelistAffiliate($customer->getCustomerId());
            $allCustomer[$key]['allowed_register_affiliate'] = $canRegistAffiliate;

            $affiliateData = (object)[];
            $allCustomer[$key]['affiliate'] = $affiliateData;

            $affiliateData = $this->affiliate->findFirst('customer_id = ' . $customer->getCustomerId());
            if ($affiliateData) {
                $allCustomer[$key]['affiliate'] = $affiliateData->getDataArray(array(),true);
                $sql = "SELECT account_number AS bank_number FROM affiliate_rekening WHERE affiliate_id = '".$affiliateData->getAffiliateID()."' limit 1;";
                        $this->affiliate->useReadOnlyConnection();
                        $result = $this->affiliate->getDi()->getShared($this->affiliate->getConnection())->query($sql);
                        if($result->numRows() > 0) {
                            $result->setFetchMode(Db::FETCH_ASSOC);
                            $affiliateRekening = $result->fetch();
                            $allCustomer[$key]['affiliate']['bank_number'] = $affiliateRekening['bank_number'];
                        } else {
                            $allCustomer[$key]['affiliate']['bank_number'] = '';
                        }
            }

            if ($from != "" && $from == "wapi") {
                // directly return, because profile based dont need it
                break;
            }

            // Get using function, because we need to limit how much data is retrive
            $customerAddress = $this->getCustomerAddress(["customer_id" => $customer->getCustomerId()]);
            $shipping_address = array();
            $billing_address = array();
            foreach ($customerAddress as $address) {
                if ($address['address_type'] == 'shipping') {
                    $shipping_address[] = $address;
                } else if ($address['address_type'] == 'billing') {
                    $billing_address[] = $address;
                }
            }

            if (!empty($billing_address)) {
                $allCustomer[$key]['billing_address'] = $billing_address;
            }

            if (!empty($shipping_address)) {
                $allCustomer[$key]['shipping_address'] = $shipping_address;
            }

            $customerAbuser = $this->getCustomerAbuser(["customer_id" => $customer->getCustomerId()]);
            if (!empty($customerAbuser)) {
                $allCustomer[$key]['group_customer_abuser'] = $customerAbuser;
            }
        }

        return $allCustomer;
    }

    public function getAllCustomerCustom($params = array())
    {
        $customerModel = new \Models\Customer();

        if (isset($params['limit']) && isset($params['offset'])) {
            $limit = $params['limit'];
            $offset = $params['offset'];
            unset($params['limit']);
            unset($params['offset']);
        }

        $query_count = "
                SELECT
                    count(customer_id) as total
                FROM
                    \Models\Customer";

        $columns = "*";
        $where_clause = '';
        if (isset($params['master_app_id'])) {
            if ($params['master_app_id'] == $_ENV['MASTER_APP_ID_OMS']) {
                unset($params['master_app_id']);

                $columns = "customer_id, first_name, last_name, email, phone, created_at, company_code";
            }
        }

        $where_clause .= " WHERE \Models\Customer.status >=5";

        if (isset($params['email_keyword'])) {
            $params['email_keyword'] = strtolower($params['email_keyword']);
            $where_clause .= " AND LOWER(\Models\Customer.email) LIKE '%" . strtolower($params['email_keyword']) . "%'";
        }

        if (isset($params['name_keyword'])) {
            $params['name_keyword'] = strtolower($params['name_keyword']);
            $where_clause .= " AND LOWER(CONCAT(\Models\Customer.first_name, ' ', \Models\Customer.last_name)) LIKE '%" . strtolower($params['name_keyword']) . "%'";
        }

        if (isset($params['phone_keyword'])) {
            $where_clause .= " AND \Models\Customer.phone LIKE '%" . $params['phone_keyword'] . "%'";
        }

        
        if (isset($params['customer_id_keyword'])) {
            $customerIds = explode(",", $params['customer_id_keyword']);
            if (count($customerIds) > 1) {
                $where_clause .= " AND \Models\Customer.customer_id IN (" . $params['customer_id_keyword']. ")";
            } else if (count($customerIds) == 1) {
                $where_clause .= " AND \Models\Customer.customer_id = '" . $params['customer_id_keyword']. "'";
            }
        }

        

        $query_count .= $where_clause;

        if (isset($limit) && isset($offset)) {
            $query = "SELECT " . $columns . " FROM \Models\Customer " . $where_clause . " ORDER BY customer_id DESC LIMIT $limit OFFSET $offset";
        } else {
            $query = "SELECT " . $columns . " FROM \Models\Customer " . $where_clause . " ORDER BY customer_id DESC";
        }

        try {
            $manager = $customerModel->getModelsManager();

            $allCustomer['list'] = $manager->executeQuery($query)->toArray();

            $total = $manager->executeQuery($query_count)->toArray();
            $allCustomer['recordsTotal'] = $total[0]['total'];
        } catch (\Exception $e) {
            $allCustomer['list'] = array();
        }

        return $allCustomer;
    }

    public function getCustomerAddress($params = array())
    {
        $customerAddressModel = new \Models\CustomerAddress();

        $resultAddress = $customerAddressModel::query();
        foreach ($params as $key => $val) {
            if ($key == "encrypt") {
                continue;
            }
            $resultAddress->andWhere($key . " = '" . $val . "'");
        }
        $resultAddress->andWhere("status = 10");
        $resultAddress->andWhere("is_default != 10");
        $resultAddress->andWhere("address_name != 'vendor_address'");
        $resultAddress->orderBy("is_default DESC, address_id DESC");
        $resultAddress->limit(100);

        $result = $resultAddress->execute();

        $allAddress = array();
        foreach ($result as $address) {
            $customerAddressModel = new \Models\CustomerAddress();
            $customerAddressModel->setFromArray($address->toArray());
            $allAddress[] = $customerAddressModel->getDataArray();
        }

        if (isset($params['encrypt']) && $params['encrypt'] == 'yes') {
            foreach ($allAddress as $key => $val) {
                $allAddress[$key]['address_id'] = \Helpers\SimpleEncrypt::encrypt($val['address_id']);
            }
        }

        return $allAddress;
    }

    public function getCustomerCompany($params = array())
    {
        $customerCompanyModel = new \Models\CustomerCompany();
       
        $resultCompany = $customerCompanyModel::query();
        foreach($params as $key => $val)
        {
            if($key == "encrypt") {
                continue;
            }
            $resultCompany->andWhere($key ." = '" . $val ."'");
        }
        $resultCompany->andWhere("status > 0");
        $resultCompany->orderBy("is_default DESC, customer_company_id DESC");
        $resultCompany->limit(100);

        $result = $resultCompany->execute();

        $allCompany = array();
        foreach($result as $company) {
            $customerCompanyModel = new \Models\CustomerCompany();
            $customerCompanyModel->setFromArray($company->toArray());
            $allCompany[] = $customerCompanyModel->getDataArray();
        }

        return $allCompany;
    }

    public function deleteCustomerAddress($params = array())
    {
        $customerAddressModel = new \Models\CustomerAddress();

        if (isset($params['decrypt']) && $params['decrypt'] == 'yes' && isset($params['address_id'])) {
            $params['address_id'] = \Helpers\SimpleEncrypt::decrypt($params['address_id']);
        }

        // Find if customer address is exist
        $addressResult = $customerAddressModel::findFirst("address_id = '" . $params['address_id'] . "'");

        if (!$addressResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Address not found";
            return false;
        }

        // if customer id is send, we check if that address id is own by that customer id.
        if (isset($params['customer_id'])) {
            $checkResult = $customerAddressModel::findFirst("address_id = '" . $params['address_id'] . "' and customer_id = '" . $params['customer_id'] . "'");
            if (!$checkResult) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Address not found";
                return false;
            }
        }

        $customerAddressModel->assign($addressResult->toArray());
        $customerAddressModel->setStatus(-1);;
        $saveStatus = $customerAddressModel->saveData("customer_address");;

        if (!$saveStatus) {
            $this->errorCode = $customerAddressModel->getErrorCode();
            $this->errorMessages[] = $customerAddressModel->getErrorMessages();
            return false;
        }

        return true;
    }

    public function updateCustomerCompany($params = array())
    {
        if(empty($params['customer_company_id'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Customer Company not found, can not edit company";
            return;
        }

        $customerCompanyModel = new \Models\CustomerCompany();

        $customerCompanyModel->setFromArray($params);
        $saveStatus = $customerCompanyModel->saveData("customer_company");

        if(!$saveStatus) {
            $this->errorCode = $customerCompanyModel->getErrorCode();
            $this->errorMessages[] = $customerCompanyModel->getErrorMessages();
            return false;
        }

        return true;
    }

    public function saveCustomerCompany($params = array())
    {
        if(empty($params['customer_id'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Customer not found, can not add company";
            return;
        }

        //validate is_npwp_address
        if($params['is_npwp_address'] == "yes"){
            $params['company_address'] = "";
        }

        $customerCompanyModel = new \Models\CustomerCompany();

        $customerCompanyModel->setFromArray($params);
        $saveStatus = $customerCompanyModel->saveData("customer_company");

        if(!$saveStatus) {
            $this->errorCode = $customerCompanyModel->getErrorCode();
            $this->errorMessages[] = $customerCompanyModel->getErrorMessages();
            return false;
        }

        return true;
    }

    public function saveData($params = array())
    {
        if (empty($params['customer_id'])) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Customer not found, can not add address";
            return;
        }

        // Check if geolocation has undefined value
        if(!empty($params['geolocation'])){
            if(stripos($params['geolocation'], "undefined") !== false) {
                $this->errorCode = 400;
                $this->errorMessages[] = "Geolocation tidak valid";
                return;
            }
            $params['is_geolocation_valid'] = 10;
        }
        
        if (isset($params['decrypt']) && $params['decrypt'] == 'yes' && isset($params['address_id'])) {
            $params['address_id'] = \Helpers\SimpleEncrypt::decrypt($params['address_id']);
        }
        
        $customerAddressModel = new \Models\CustomerAddress();

        // check if that address id is own by that customer id.  
        $customer_id = $params['customer_id'];
        if (isset($params['address_id'])) {
            $addressResult = $customerAddressModel::findFirst("address_id = '" . $params['address_id'] . "' and customer_id = '" . $params['customer_id'] . "'");
            if (!$addressResult) {
                $this->errorCode = "RR302";
                $this->errorMessages = "Address not found";
                return false;
            }
            $addressResultArray  = $addressResult->toArray();
        } else {
            $addressResultArray['address_type'] = "shipping";
        }
        
        // If new data is default, update others
        if (isset($params['is_default']) && $params['is_default'] == 1) {
            $addressType = isset($params['address_type']) ? $params['address_type'] : $addressResultArray['address_type'];


            $query = "UPDATE customer_address SET ";
            $query .= "is_default = 0";
            $query .= " WHERE customer_id = " . $params['customer_id'] . " AND address_type='shipping'";

            $customerAddressModel->useWriteConnection();
            $result = $customerAddressModel->getDi()->getShared($customerAddressModel->getConnection())->query($query);
        }
        
        // If the kecamatan_id is provided but the kelurahan_id isn't, we need to verify the current kelurahan_id
        if(!empty($params['kecamatan']['kecamatan_id']) && empty($params['kelurahan']['kelurahan_id']) && !empty($addressResultArray['kelurahan_id'])){
            $masterKelurahanModel = new \Models\MasterKelurahan();
            $masterKelurahan = $masterKelurahanModel::findFirst("kecamatan_id = '" . $params['kecamatan']['kecamatan_id'] . "' and kelurahan_id = '" . $addressResultArray['kelurahan_id'] . "'");
            if (!$masterKelurahan) {
                $params['is_kelurahan_invalid'] =  true;
            }
        }

        $customerAddressModel->setFromArray($params);
        $saveStatus = $customerAddressModel->saveData("customer_address");

        if (!$saveStatus) {
            $this->errorCode = $customerAddressModel->getErrorCode();
            $this->errorMessages[] = $customerAddressModel->getErrorMessages();
            return false;
        }

        return array('address_id' => $customerAddressModel->getAddressId());
    }

    public function generateToken($params = array())
    {
        // for otp forgot password, we need to check is it phone or email
        if (empty($params['email'])) {
            $this->errorCode = 'RR100';
            $this->errorMessages = 'Email is required';
            return false;
        }

        if (!isset($params['company_code'])) {
            $params['company_code'] = "ODI";
        }

        $param = array(
            "conditions" => "email = '" . $params['email'] . "' AND status >= 10 AND company_code = '" . $params['company_code'] . "'"
        );

        $customerResult = $this->customer->findFirst($param);

        if ($customerResult) {
            $customerData = $customerResult->toArray();
            $token = \Helpers\SimpleEncrypt::encrypt(strtotime("+6 hours"));
            $customerData['password_token'] = $token;

            unset($customerData['password']);
            $this->customer->setFromArray($customerData);
            try {
                $this->customer->saveData();
            } catch (\Exception $e) {
                $this->errorCode = "500";
                $this->errorMessages[] = "Failed to generate token";
                return;
            }

            // decide url forgot password
            $urlForgotPassword = getenv('BASE_URL').getenv('URL_FORGOT_PASSWORD')."?token=".$token;
            if (isset($params['cart_id'])) {
                if (!empty($params['cart_id'])) {
                    $urlForgotPassword = 'https://payment.ruparupa.com/reset-password/checkout?token=' . $params['cart_id'] . '&ptoken='.$token;
                }
            } else if (isset($params['company_code'])) {
                $companyCode = strtolower($params['company_code']);
                switch ($companyCode) {
                    case 'ahi' :
                        $urlForgotPassword = getenv('BASE_URL').'acestore/'.getenv('URL_FORGOT_PASSWORD')."?token=".$token;
                        break;
                    case 'hci' : 
                        $urlForgotPassword = getenv('BASE_URL').'informastore/'.getenv('URL_FORGOT_PASSWORD')."?token=".$token;                     
                }
            }

            // Sending email
            $email = new \Library\Email();
            $email->setEmailSender();
            $email->setName($customerData['last_name'], $customerData['first_name']);
            $email->setEmailReceiver($customerData['email']);
            $email->setEmailTag("cust_forgot_password");

            $templateID = \Helpers\GeneralHelper::getTemplateId("customer_forgot", $params['company_code']);
            $email->setTemplateId($templateID);
            $email->setCompanyCode($params['company_code']);
            $email->setUrlForgotPassword("$urlForgotPassword");
            $email->send();

            return true;
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages[] = "Email found";
        }

        return false;
    }

    public function generateTokenOtp($params = array())
    {
        // for otp forgot password, we need to check is it phone or email
        /**
         * {
         *  "data": {
         *      "email": "",
         *      "phone": "", // format have to be "08"
         *      "company_code": ""
         *  }
         * }
         * / */

        if (!empty($params['email']) || !empty($params['phone'])) {
            $queryCondition = "";

            if (!isset($params['company_code'])) {
                $params['company_code'] = "ODI";
            }

            if (!empty($params['email'])) {
                // using email
                $queryCondition = "email = '" . $params['email'] . "' AND status >= 10 AND company_code = '" . $params['company_code'] . "'";
            } else if (!empty($params['phone'])) {
                // using email
                $queryCondition = "phone = '" . $params['phone'] . "' AND status >= 10 AND company_code = '" . $params['company_code'] . "'";
            }

            $queryParam = array(
                "conditions" => $queryCondition,
                "order" => "last_login DESC"
            );

            $customerResult = $this->customer->findFirst($queryParam);

            if ($customerResult) {
                $token = \Helpers\SimpleEncrypt::encrypt(strtotime("+6 hours"));
                $customerUpdatedData = [
                    'customer_id' => $customerResult->getCustomerId(),
                    'password_token' => $token
                ];

                $this->customer->setFromArray($customerUpdatedData);
                try {
                    $this->customer->saveData();
                } catch (\Exception $e) {
                    $this->errorCode = "500";
                    $this->errorMessages[] = "Failed to generate token";
                    return;
                }

                return $token;
            } else {
                $this->errorCode = "RR302";
                $this->errorMessages[] = "Customer not found";
            }
        } else {
            $this->errorCode = "RR302";
            $this->errorMessages[] = "Email/Phone not found";
        }

        return false;
    }

    public function tokenCheck($token = "")
    {
        if (empty($token)) {
            $this->errorCode = 'RR100';
            $this->errorMessages = 'token is required';
            return false;
        }

        $param = array(
            "conditions" => "password_token = '" . $token . "' AND status >= 10"
        );
        $customerResult = $this->customer->findFirst($param);

        if ($customerResult) {
            $token = \Helpers\SimpleEncrypt::decrypt($token);

            if (time() < $token) {
                $this->customer->assign($customerResult->toArray(['customer_id', 'first_name', 'last_name', 'email']));
                return true;
            } else {
                $this->errorCode = "RR100";
                $this->errorMessages[] = "Invalid token";
            }
        } else {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "Invalid token";
        }

        return false;
    }

    public function checkCustomer($params = array())
    {
        // Social register flag => states that we're trying to authenticate using social account that hasn't been registered on customer table
        $isSocialReg = $params['action'] == "social-login" && !empty($params['email']) && !empty($params['phone']);

        // Prepare query to get customer result
        $queryParam = array(
            "order" => "last_login DESC",
            "conditions" => ""
        );

        if ($params["action"] == "register" || $isSocialReg || $params["action"] == "change-email") {
            if (!empty($params["email"])) {
                // check if email is using disposable domain
                $domain = explode("@", $params["email"])[1];
                $param["conditions"] = "domain_name = '" . $domain . "' AND status = 1";
                $emailDomainResult = $this->disposableEmailDomain->findFirst($param);
                if ($emailDomainResult) {
                    $this->errorCode = 400;
                    $this->errorMessages = "Email tidak valid. Silakan gunakan email lain";
                    return;
                }
            }
        }
       
        // Check whether email or phone are registered
        if ($params['action'] == "register" || $isSocialReg) {
            $queryParam["conditions"] = sprintf("(phone = '%s' OR email = '%s')", $params['phone'], $params['email']);
        } else {
            if (!empty($params["phone"])) {
                $queryParam["conditions"] = sprintf("phone = '%s'", $params['phone']);
            } else {
                $queryParam["conditions"] = sprintf("email = '%s'", $params['email']);
            }
        }

        $queryParam["conditions"] .= " AND company_code = 'ODI' AND status > 0";
        $queryParam["order"] = "status DESC, updated_at DESC";
        $returnResult = array(
            "customer_id" => 0,
            "phone" => "",
            "email" => "",
            "is_used" => false,
            "is_expired" => true,
            "force_update" => false,
            "empty_data" => "",
            "guest_equality" => "",
            "is_employee" => false,
        );
        $chosenCustomerResult = array();
        $customerResults = $this->customer->find($queryParam);

        // Customer not found
        if ($customerResults->count() <= 0) {
            if (in_array($params['action'], ['login', 'forgot-password'])) {
                $this->errorCode = 404;
                $this->errorMessages = "Informasi tidak valid";
                return;
            }

            // Customer's going to register through social account, then we have to force them to verify phone
            if ($params['action'] == "social-login") {
                $returnResult["force_update"] = true;
            }
            //Check Customer to table customer stamps kawan lama for Go-Unify
            if ($params['action'] == "register"){        
            $customerModel = new \Models\CustomerStampsKawanLama();
            $paramEmail['email'] = $params['email'];
            $paramPhone['phone'] = $params['phone'];

            $hasil = $customerModel->getCustomerIdByEmail($paramEmail);
            if (count($hasil) > 0) {
                $this->errorCode = 400;
                $this->errorMessages = "Alamat email Anda sudah terdaftar";
                return;
            }

            $hasil = $customerModel->getCustomerIdByPhone($paramPhone);
            if (count($hasil) > 0) {
                $this->errorCode = 400;
                $this->errorMessages = "Nomor telepon Anda sudah terdaftar";
                return;
            }
            }
            return $returnResult;
        }

        // Check customerResults based on its status
        if (in_array($customerResults[0]->getStatus(), [10, 1])) {
            $errorMsgAction = "";
            if ($customerResults[0]->getStatus() == 1) {
                // Customer found with status 1
                switch (true) {
                    case $params['action'] == 'change-phone':
                        $errorMsgAction = "Nomor telepon Anda sudah terdaftar";
                        break;
                    case $params['action'] == 'change-email':
                        $errorMsgAction = "Alamat email Anda sudah terdaftar";
                        break;
                    case in_array($params['action'], ['register', 'login', 'social-login', 'forgot-password']):
                        $errorMsgAction = "Akun sudah dihapus dan tidak dapat digunakan kembali.";
                        break;
                }
            } else {
                // Customer found with status 10
                switch ($params['action']) {
                    case 'change-phone':
                        $errorMsgAction = "Nomor telepon Anda sudah terdaftar";
                        break;
                    case 'change-email':
                        $errorMsgAction = "Alamat email Anda sudah terdaftar";
                        break;
                    case 'register':
                        $errorMsgAction = "Alamat email atau nomor telepon Anda sudah terdaftar";
                        break;
                }
            }

            if (!empty($errorMsgAction)) {
                $this->errorCode = 400;
                $this->errorMessages = $errorMsgAction;
                return;
            }

            // Set some return result
            $chosenCustomerResult = $customerResults[0];
            $returnResult["customer_id"] = (int) $chosenCustomerResult->getCustomerId();
            $returnResult["phone"] = !empty($chosenCustomerResult->getPhone()) ? $chosenCustomerResult->getPhone() : "";
            $returnResult["email"] = !empty($chosenCustomerResult->getEmail()) ? $chosenCustomerResult->getEmail() : "";
            $returnResult["is_used"] = true;

            // Check password & empty data for login
            if ($params['action'] == "login") {
                // Check whether customer data exist on customer_otp
                $customerOtpQuery = [
                    "conditions" => sprintf("customer_id = %s", $chosenCustomerResult->getCustomerId()),
                    "order" => "updated_at DESC"
                ];
                $customerOtpResult = $this->customerOtp->findFirst($customerOtpQuery);

                // Potential anomaly, whereas password is empty but customer_otp data is exist, then we'll return then same error like when customer's password is wrong
                // So they can do forgot password and back to normal flow

                // Check password if needed
                if (!empty($chosenCustomerResult->getPassword()) || (empty($chosenCustomerResult->getPassword()) && !empty($customerOtpResult))) {
                    $hashArr = explode(":", $chosenCustomerResult->getPassword());
                    $validatePassword = false;
                    switch (count($hashArr)) {
                        case 1:
                            $validatePassword = (hash("sha256", $params["password"]) === $chosenCustomerResult->getPassword());
                            break;
                        case 2:
                            $validatePassword = (hash("sha256", $hashArr[1] . $params["password"]) === $hashArr[0]);
                            break;
                        default:
                            $validatePassword = false;
                    }

                    if (!$validatePassword) {
                        $this->errorCode = 400;
                        $this->errorMessages = "Alamat email atau nomor telepon dan password Anda salah";
                        return;
                    }
                }

                // Check existence of password, email, phone data on customer
                switch (true) {
                    case empty($chosenCustomerResult->getPassword()):
                        $returnResult["empty_data"] = "password";
                        break;
                    
                    case empty($chosenCustomerResult->getPhone()):
                        $returnResult["empty_data"] = "phone";
                        break;
                    
                    case empty($chosenCustomerResult->getEmail()):
                        $returnResult["empty_data"] = "email";
                        break;
                }
            }

            // Check is_employee
            $customer2GroupModel = new \Models\Customer2Group();
            $customer2GroupParam["conditions"] = sprintf("customer_id = %d AND group_id = 7", (int) $chosenCustomerResult->getCustomerId());
            $customer2GroupData = $customer2GroupModel->findFirst($customer2GroupParam);
            if (!empty($customer2GroupData)) {
                $returnResult["is_employee"] = true;
            }

            // Check whether phone exist on $chosenCustomerResult
            if (empty($chosenCustomerResult->getPhone())) {
                $returnResult['force_update'] = true;
                return $returnResult;
            }

            // Phone exist, then we should check customer_otp record with that phone
            $customerOtpParam = array(
                "conditions" => sprintf("phone = '%s'", $chosenCustomerResult->getPhone()),
                "order" => "updated_at DESC"
            );

            $customerOtpResult = $this->customerOtp->findFirst($customerOtpParam);
            if (!$customerOtpResult) {
                $returnResult['force_update'] = true;
                return $returnResult;
            }

            // Means it's not a valid customer_otp record, then we have to force user to verify phone
            if ($customerOtpResult->getCustomerId() != $chosenCustomerResult->getCustomerId()) {
                $returnResult['force_update'] = true;
                return $returnResult;
            }

            // Check expired_on on customer_otp
            $parsedDateNow = strtotime(date("Y-m-d H:i:s"));
            $parsedExpirationDate = strtotime($customerOtpResult->getExpiredOn());
            if ($parsedDateNow < $parsedExpirationDate) {
                $returnResult['is_expired'] = false;
            }
        } else {
            // Customer found with status 5
            // Means that customer register using social account, then we have to force customer to verify phone
            if ($params['action'] == "social-login") {
                $returnResult["force_update"] = true;
            }

            if ($params['action'] == "register" || $isSocialReg) {
                $salesOrderModel = new \Models\SalesOrder();
                $guestCandidate = new \Models\Customer();

                // Flags for guest candidate (will be updated after each loop of $customerResults)
                // Determine whether $guestCandidate exist
                $isGuestCandidateExist = false;
                // Determine whether phone / email match on the current $guestCandidate
                $guestCandidateEq = "";

                // Check customer results
                foreach ($customerResults as $customerResult) {
                    // Check whether there is a customerResult match with email and phone from params, 
                    // If there is, then it means the real guest customer wants to register, so we proceed with registration
                    if (strtolower($customerResult->getEmail()) == $params['email'] && $customerResult->getPhone() == $params['phone']) {
                        $chosenCustomerResult = $customerResult;
                        break;
                    } else {
                        /**
                         * Search guest candidate
                         * If the customerResult doesn't not match with both email & phone from params, 
                         * We need to check whether current customerResult is a good candidate, criteria:
                         * - Has order
                         * - Same phone occur on customerResults will be the first priority, with condition:
                         *   => Same phone occur on more than one on customerResults, then we use the first one
                         * - Same phone not found on all the records on customerResults, then we use the first record with same email
                         */
                        $orderResult = $salesOrderModel->findFirst(array(
                            'conditions' => sprintf("customer_id = %s", $customerResult->getCustomerId())
                        ));
                        
                        if (!empty($orderResult)) {
                            // Check whether phone / email match on the current customerResult
                            $currCustResEq = $customerResult->getPhone() == $params['phone'] ? 'phone' : 'email';
                            if ($currCustResEq == 'phone') {
                                $guestCandidate = !$isGuestCandidateExist || ($isGuestCandidateExist && $guestCandidateEq == 'email') ? $customerResult : $guestCandidate;
                            } else {
                                $guestCandidate = !$isGuestCandidateExist ? $customerResult : $guestCandidate;
                            }
                        }

                        // Renew $isGuestCandidateExist 
                        $isGuestCandidateExist = !empty($guestCandidate->getCustomerId());

                        // Renew $guestCandidateEq
                        if ($isGuestCandidateExist) {
                            $guestCandidateEq = $guestCandidate->getPhone() == $params['phone'] ? 'phone' : 'email';
                        }
                    }
                }

                if (empty($chosenCustomerResult) && $isGuestCandidateExist) {
                    // Check whether customer registered_by is shopee or tokped then we let customer use the account
                    // This condition appear because shoped account normally only have phone, so there will be no email & phone match at the same time
                    $listRegisteredByFromVendor = ["shopee", "tokopedia"];
                    if (in_array($guestCandidate->getRegisteredBy(), $listRegisteredByFromVendor)) {
                        return $returnResult;
                    }

                    // Check whether phone / email match on guestCandidate
                    $isDataInfoSent = false;

                    // Set guest equality flag
                    $returnResult["guest_equality"] = $guestCandidateEq;
                
                    // Send mismatch data to customer based on guestCandidateEq
                    if ($guestCandidateEq == 'phone') {
                        $messageAPILib = new \Library\MessageAPI();
                        $censoredEmail = \Helpers\GeneralHelper::censorEmail($guestCandidate->getEmail());
                        $messagePayload = [
                            "recipient" => $guestCandidate->getPhone(),
                            "channel" => "chat",
                            "content" => [
                                "template_name" => "email_regis_salah",
                                "placeholders" => ["Ruppers", $censoredEmail]
                            ]
                        ];
                        $messages = [$messagePayload];
                        $messageAPILib->setMessages($messages);
                        $messageAPILib->setAppSender("api");
                        $sendMessageResp = $messageAPILib->sendSingleGeneralMessage();
                        // Check status and rate limit flag
                        if (!empty($sendMessageResp["message_receipts"])) {
                            if ($sendMessageResp["message_receipts"][0]["status"] == "error") {
                                $this->errorCode = $sendMessageResp["message_receipts"][0]["is_limit_reached"] ? 400 : 500;
                                $this->errorMessages = $sendMessageResp["message_receipts"][0]["error_message"];
                                return;
                            }
                            // Status not error means data is sent
                            $isDataInfoSent = true;
                        }
                    } else {
                        $emailLib = new \Library\Email();
                        $templateID = \Helpers\GeneralHelper::getTemplateId("register_guest_acc_invalid_phone", "ODI");
                        $emailLib->setTemplateId($templateID);
                        $emailLib->setEmailReceiver($guestCandidate->getEmail());
                        $emailLib->setEmailSender();
                        $censoredPhone = \Helpers\GeneralHelper::censorPhone($guestCandidate->getPhone());
                        $emailLib->setTemplateModel([ 
                            "phone" => $censoredPhone
                        ]);
                        $emailLib->setCompanyCode("ODI");
                        $emailLib->setEmailTag("register_guest_acc_invalid_phone");
                        $isDataInfoSent = $emailLib->send();
                    }

                    // Check whether send email / wa succeed
                    if (!$isDataInfoSent) {
                        $this->errorCode = 500;
                        $this->errorMessages = "Terjadi kesalahan silakan ulangi beberapa saat lagi";
                        return;
                    }
                }
            }
        }

        return $returnResult;
    }

    /**
     * Get voucher list with pretty view based on customer id
     * @return array|bool
     */
    public function getVoucherList()
    {
        if (empty($this->customer->getCustomerId())) {
            $this->errorCode = 'RR100';
            $this->errorMessages = 'Cusstomer id is required';
            return false;
        }

        $customer_id = $this->customer->getCustomerId();
        $voucherModel = new \Models\SalesruleVoucher();
        $params = [
            'conditions' => "customer_id = " . $customer_id ,  
            'order' => 'expiration_date ASC'
        ];

        if (!empty($this->excludeType)) {
            $params["conditions"] = $params["conditions"] . " AND type NOT IN (" . $this->excludeType . ")";
        }

        if (!empty($this->includeType)) {
            $params["conditions"] = $params["conditions"] . " AND type IN (" . $this->includeType . ")";
        }

        $voucherList = $voucherModel::find($params);

        $finalVoucher = array();
        setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
        foreach ($voucherList as $voucher) {
            // it must be dispute voucher, skip it
            if ($voucher->getStatus() == "0" && intval($voucher->getTimesUsed()) == 0) {
                continue;
            }

            // only show voucher refund for status = 0
            if ($voucher->getStatus() == "0" && $voucher->getRuleId() <> getenv("SALESRULE_VOUCHER_PRODUCT")) {
                continue;
            }

            $voucherExpirationDate = $voucher->getExpirationDate();
            $thisVoucher['voucher_code'] = $voucher->getVoucherCode();
            $thisVoucher['voucher_amount'] = $voucher->getVoucherAmount();
            $thisVoucher['expiration_date'] = strftime("%d %B %Y", strtotime($voucherExpirationDate));
            $thisVoucher['expiration_datetime'] = $voucherExpirationDate;
            $thisVoucher['rule_id'] = $voucher->getRuleId();
            $thisVoucher['total_stamp'] = $voucher->getTotalStamp();
          

            // tnc set
            $salesRuleModel = new \Models\Salesrule();
            $params = [
                'conditions' => "rule_id = " . $thisVoucher['rule_id']  
            ];
            $salesRule = $salesRuleModel::findFirst($params);
            $thisVoucher['tnc'] = $salesRule->getTnc();

            // Exclude Voucher Reorder
            $salesReorderModel = new \Models\SalesReorder();
            $sql = "
                    SELECT sales_order_id FROM
                        sales_reorder
                    WHERE
                        sales_order_id IN ( SELECT sales_order_id FROM sales_order
                            WHERE
                                order_no IN ( SELECT order_no FROM sales_credit_memo
                                    WHERE voucher_id = ".$voucher->getVoucherId()."
                                )
                        )
            ";
            $resultReorder = $salesReorderModel->getDi()->getShared($salesReorderModel->getConnection())->query($sql);
            if ($resultReorder->numRows() > 0) {
                continue;                 
            }
           
            // for voucher refund needs
            $thisVoucher['refund_status'] = "";
            $invoiceModel = new \Models\SalesInvoice();
            $sql = "select srr.status as refund_status, coalesce(srrp.customer_rekening_id, 0) as customer_rekening_id 
            from sales_return_refund srr left join sales_return_refund_payment srrp on srrp.refund_no = srr.refund_no 
            where voucher_product_id = " . $voucher->getVoucherId();
            $result = $invoiceModel->getDi()->getShared($invoiceModel->getConnection())->query($sql);
            $result->setFetchMode(Db::FETCH_ASSOC);
            $dataRefund = $result->fetch();
            if (isset($dataRefund['refund_status']) && isset($dataRefund['customer_rekening_id'])) {
                $thisVoucher['refund_status'] = $dataRefund['refund_status'];
                $thisVoucher['customer_rekening_id'] = $dataRefund['customer_rekening_id'];
            }

            if (intval($voucher->getLimitUsed()) - intval($voucher->getTimesUsed()) > 0) {
                if (strtotime($voucherExpirationDate) < strtotime(date('Y-m-d H:i:s'))) {
                    // expired
                    $thisVoucher['status'] = "Kadaluarsa";                
                } else if ($voucher->getStatus() == "6" && $thisVoucher['refund_status'] == "recheck") {
                    $thisVoucher['status'] = "Aktif";    
                } else if ($voucher->getStatus() == "6") {
                    $thisVoucher['status'] = "Sedang diproses";    
                } else {
                    $thisVoucher['status'] = "Aktif";
                }
            } else {
                $thisVoucher['status'] = "Sudah Digunakan";
            }

            if ($voucher->getRuleId() == getenv("SALESRULE_VOUCHER_PRODUCT")) {
                if ($voucher->getStatus() == "0" && intval($voucher->getTimesUsed()) > 0) {
                    $thisVoucher['status'] = "Sudah Dicairkan";
                }
            }

            $isDeals = false;
            switch ($voucher->getType()) {
                case 1:
                    $thisVoucher['type'] =  "Registration";
                    break;
                case 2:
                    $thisVoucher['type'] =  "Refund";
                    break;
                case 3:
                    $thisVoucher['type'] =  "General";
                    break;
                case 4:
                    $thisVoucher['type'] =  "Cashback";
                    break;
                case 5:
                    $thisVoucher['type'] =  "Redeem Point";
                    break;
                case getenv('VOUCHER_TYPE_ONLINE_DEALS'):
                    $isDeals = true;
                    $thisVoucher['type'] =  "Online Deals";
                    break;    
                case getenv('VOUCHER_TYPE_OFFLINE_DEALS'):
                    $isDeals = true;
                    $thisVoucher['type'] =  "Store Voucher Deals";
                    break;
            }            

            if ($isDeals && !isset($dealsRuleData[$voucher->getRuleId()])) {
                $voucherDealsModel = new \Models\VoucherDeals();
                $params = [
                    'conditions' => "rule_id = " . $voucher->getRuleId() 
                ];
                $resultVoucherDeals = $voucherDealsModel::findFirst($params);
                if ($resultVoucherDeals) {
                    $dealsRuleData[$voucher->getRuleId()] = [
                        "title" => $resultVoucherDeals->getTitle(),
                        "description" => $resultVoucherDeals->getDescription(),
                        "discount_type" => $resultVoucherDeals->getDiscountType(),
                        "banner_image_small_url" => $resultVoucherDeals->getBannerImageSmallURL(),
                        "banner_image_small_title" => $resultVoucherDeals->getBannerImageSmallTitle(),
                        "banner_image_large_url" => $resultVoucherDeals->getBannerImageLargeURL(),
                        "banner_image_large_title" => $resultVoucherDeals->getBannerImageLargeTitle(),
                    ];
                }
            }

            $thisVoucher['times_used'] = intval($voucher->getTimesUsed());
            $thisVoucher['deals_data'] = isset($dealsRuleData[$voucher->getRuleId()]) ? $dealsRuleData[$voucher->getRuleId()] : [];
            
            $finalVoucher[] = $thisVoucher;
        }

        return $finalVoucher;
    }

    public function changePassword() // ini ga digunakan lagi sepertinya ?
    {
        // Check if customer is exist
        if (!empty($this->customer->getEmail())) {
            $param = array(
                "conditions" => "email = '" . $this->customer->getEmail() . "' AND company_code = 'ODI'"
            );
        } else if (!empty($this->customer->getCustomerId())) {
            $param = array(
                "conditions" => "customer_id = '" . $this->customer->getCustomerId() . "'"
            );
        } else if (empty($this->customer->getPassword())) {
            $this->errorCode = "RR101";
            $this->errorMessages = "Password is required";
            return false;
        } else {
            $this->errorCode = "RR101";
            $this->errorMessages = "Email is required";
            return false;
        }

        $customerResult = $this->customer->findFirst($param);

        // If exist we need to check status of customer
        if ($customerResult) {
            if ($customerResult->getStatus() >= 10) {
                $validatePassword = false;
                $hashArr = explode(':', $customerResult->getPassword());

                switch (count($hashArr)) {
                    case 1:
                        $validatePassword = (hash('sha256', $this->customer->getOldPassword()) === $customerResult->getPassword());
                        break;
                    case 2:
                        $validatePassword = (hash('sha256', $hashArr[1] . $this->customer->getOldPassword()) === $hashArr[0]);
                        break;
                    default:
                        $validatePassword = false;
                }

                if (!$validatePassword) {
                    $this->errorCode = "RR100";
                    $this->errorMessages = "Password anda salah";
                    return false;
                } else {
                    // Insert customer group, it'll be deleted when save customer data we need to add it back
                    if (!empty($customerResult->Customer2Group->count())) {
                        $customerGroups = $customerResult->Customer2Group->toArray();
                        if (!empty($customerGroup)) {
                            $thisCustomerGroups = [];
                            foreach ($customerGroups as $customerGroup) {
                                $customer_2_group = new \Models\Customer2Group();
                                $customer_2_group->setFromArray($customerGroup);
                                $thisCustomerGroups[] = $customer_2_group;
                            }

                            $customerGroupCollection = new \Models\Customer\Group\Collection($thisCustomerGroups);
                            $this->customer->setGroups($customerGroupCollection);
                        }
                    }
                }
            } else {
                $this->errorCode = "RR102";
                $this->errorMessages = "Customer data not found";
                return false;
            }
        }

        if (!$this->customer->saveData()) {
            $this->errorCode = $this->customer->getErrorCode();
            $this->errorCode = $this->customer->getMessages();
            return false;
        }

        return true;
    }

    public function getAlertList($params = array())
    {
        $customerAlertModel = new \Models\CustomerAlert();

        $resultCustomerAlert = $customerAlertModel::query();
        $resultCustomerAlert->andWhere("Models\CustomerAlert.status = 10");

        if (isset($params['customer_alert_id_keyword'])) {
            $resultCustomerAlert->andWhere("Models\CustomerAlert.customer_alert_id = " . $params['customer_alert_id_keyword']);
        }

        if (isset($params['email_keyword'])) {
            $resultCustomerAlert->andWhere("Models\CustomerAlert.email like '%" . $params['email_keyword'] . "%'");
        }

        if (isset($params['phone_keyword'])) {
            $resultCustomerAlert->andWhere("Models\CustomerAlert.phone like '%" . $params['phone_keyword'] . "%'");
        }

        if (isset($params['remark_keyword'])) {
            $resultCustomerAlert->andWhere("Models\CustomerAlert.remark like '%" . $params['remark_keyword'] . "%'");
        }

        if (isset($params['fingerprint_keyword'])) {
            $resultCustomerAlert->andWhere("Models\CustomerAlert.fingerprint like '%" . $params['fingerprint_keyword'] . "%'");
        }

        $resultCustomerAlert->orderBy("customer_alert_id DESC");

        $resultTotal = $resultCustomerAlert->execute();

        if (isset($params['limit']) && isset($params['offset'])) {
            $resultCustomerAlert->limit($params['limit'], $params['offset']);
        }

        $result = $resultCustomerAlert->execute();

        if (empty($result->count())) {
            $this->errorCode = "404";
            $this->errorMessages = "No Order Found";
            return array();
        } else {
            $allCustomerAlert['list'] = $result->toArray();
            $allCustomerAlert['recordsTotal'] = $resultTotal->count();
        }

        return $allCustomerAlert;
    }

    public function deleteCustomerAlert($params = array())
    {
        $customerAlertModel = new \Models\CustomerAlert();

        // Find if customer alert is exist
        $alertResult = $customerAlertModel::findFirst("customer_alert_id = '" . $params['customer_alert_id'] . "'");

        if (!$alertResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Customer alert not found";
            return false;
        }

        $customerAlertModel->assign($alertResult->toArray());
        $customerAlertModel->setStatus(0);;
        $saveStatus = $customerAlertModel->saveData("customer_alert");;

        if (!$saveStatus) {
            $this->errorCode = $customerAlertModel->getErrorCode();
            $this->errorMessages[] = $customerAlertModel->getErrorMessages();
            return false;
        }

        return true;
    }

    function checkCustomerAlert($orderNo, $salesOrderId)
    {
        $salesOrderModel = new \Models\SalesOrder();
        $salesOrder = $salesOrderModel::findFirst("sales_order_id = '" . $salesOrderId . "'");
        $email = $salesOrder->SalesCustomer->getCustomerEmail();
        $phone = $salesOrder->SalesOrderAddress->toArray()[0]['phone'];

        $customerAlertModel = new \Models\CustomerAlert();

        $resultCustomerAlert = $customerAlertModel::query();
        $resultCustomerAlert->where("Models\CustomerAlert.status = 10");

        if (!empty($email) && !empty($phone))
            $resultCustomerAlert->andWhere("Models\CustomerAlert.email = '" . $email . "' OR Models\CustomerAlert.phone = '" . $phone . "'");
        else if (!empty($email))
            $resultCustomerAlert->andWhere("Models\CustomerAlert.email = '" . $email . "'");
        else if (!empty($phone))
            $resultCustomerAlert->andWhere("Models\CustomerAlert.phone = '" . $phone . "'");
        $resultCustomerAlert->limit(1);
        $result = $resultCustomerAlert->execute();

        if (empty($result->count())) {
            //cek if customer using marketing gift voucher , and total is same with voucher value
            $subTotalGiftCards = 0;
            $giftCards = \GuzzleHttp\json_decode($salesOrder->getGiftCards());
            foreach ($giftCards as $gift) {
                if ($gift->voucher_type == 3)
                    $subTotalGiftCards += $gift->voucher_amount_used;
            }

            $remark = '';
            if (!empty($subTotalGiftCards) && $salesOrder->getSubtotal() == $subTotalGiftCards)
                $remark = 'Subtotal & Voucher amount sama';
            else if ($salesOrder->getGrandTotal() == $salesOrder->getShippingAmount() && empty($salesOrder->getReferenceOrderNo()))
                $remark = 'Grandtotal & Shipping amount sama';
            //compare with subtotal order
            if (!empty($remark)) {
                $params = [
                    "channel" => $_ENV['CA_SLACK_CHANNEL'],
                    "username" => $_ENV['CA_SLACK_USERNAME'],
                    "text" => 'order alert ' . $orderNo . ' email:' . $email . ' phone: ' . $phone . ' remark: ' . $remark,
                    "icon_emoji" => ':robot_face:'
                ];

                // call queue for webhook slack
                $nsq = new Nsq();
                $message = [
                    "data" => $params,
                    "message" => "customerAlert"
                ];
                $nsq->publishCluster('transaction', $message);
            }
        } else {

            $resultArray = $result->toArray();
            $remark = $resultArray[0]['remark'];

            $params = [
                "channel" => $_ENV['CA_SLACK_CHANNEL'],
                "username" => $_ENV['CA_SLACK_USERNAME'],
                "text" => 'order alert ' . $orderNo . ' email:' . $email . ' phone: ' . $phone . ' remark:' . $remark,
                "icon_emoji" => ':robot_face:'
            ];

            // call queue for webhook slack
            $nsq = new Nsq();
            $message = [
                "data" => $params,
                "message" => "customerAlert"
            ];
            $nsq->publishCluster('transaction', $message);
        }
    }

    public function getWishlistData($params = array())
    {
        $data = array();
        if (count($params) > 0) {
            // TODO: We need cache!!!
            $redisKey = "wishlist_";

            // Build keys
            if (isset($params["customer_id"]) && !empty($params["customer_id"])) {
              $redisKey .= "customer_id:" . $params["customer_id"];
            }

            foreach ($params as $key => $val) {
                if ($key == "customer_id") {
                  continue;
                }
                $redisKey .= $key . ":" . $val;
            }

            $redis = new \Library\Redis();
            $redisCon = $redis::connect();
            $response = $redisCon->get($redisKey);

            if (!empty($response)) {
                // directly return
                $response = json_decode($response);
                return $response;
            } else {
                // normal
                if (isset($params['customer_id'])) {
                    $wishlistModel = new \Models\Wishlist();
                    $email = $wishlistModel->getEmailByCustomerId($params['customer_id']);
                    $params['email'] = $email;
                }

                if (!empty($email)) {
                    $storeCodeNewRetail = isset($params['store_code_new_retail']) && $params['store_code_new_retail'] != "" ? $params['store_code_new_retail'] : "";
                    $result = $this->wishlist->getWishlistData($params);
                    if (count($result) > 0) {
                        $i = 0;
                        $elasticLib = new \Library\Elastic();
                        $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX') ? getenv('ELASTIC_PRODUCT_INDEX') : "products";
                        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE') ? getenv('ELASTIC_PRODUCT_TYPE') : "product";
                        $wishList = array();
                        $listOfSKU = [];
        
                        foreach ($result as $row) {
                            array_push($listOfSKU, $row['items']['sku']);
                        }
        
                        $resp = $this->getBySkuFromElastic($listOfSKU);
                        if (count($resp['hits']['hits']) > 0) {
                            foreach ($resp['hits']['hits'] as $id => $value) {
                                $products = ProductHelper::buildStructure(array_merge($value['_source'], array('_id' => $value['_id']), array('store_code_new_retail' => $storeCodeNewRetail)));
                                if ($products['is_extended'] == 10){
                                    $wishList['items'][$i] = $products;
                                    $wishList['items'][$i]['variants'][0]['is_product_scanned'] = isset($result[$id]['items']['is_product_scanned']) ? $result[$id]['items']['is_product_scanned'] : 0;
                                    $wishList['items'][$i]['store_code_new_retail'] = isset($result[$id]['items']['store_code_new_retail']) ? $result[$id]['items']['store_code_new_retail'] : "";
                                    $wishList['items'][$i]['added_date'] = $result[$id]['items']['added_date'];
                                    $wishList['items'][$i]['review'] = isset($result[$id]['items']['review']) ? $result[$id]['items']['review'] : [];
                                    $i++;
                                }
                            }
                            $wishList['wishlist_id'] = $result[$id]['wishlist_id'];
                            $wishList['email'] = $result[$id]['email'];
                            $wishList['total'] = $result[$id]['total'];
                        }
        
                        $data = $wishList;
                        $redisCon->set($redisKey,json_encode($data)); // persist until invalidated
                    }
                }
            }
        }

        return $data;
    }

    public function getWishlistList($params = array())
    {
        $data = array();
        if (isset($params['limit']) && isset($params['offset'])) {
            $result = $this->wishlist->getWishlistList($params);
            if (count($result) > 0) {
                $wishList = array();
                $indexParent = 0;
                $customerModel = new \Models\Customer();
                foreach ($result['data'] as $row) {
                    $custID = $customerModel->getCustomerIdByExactEmail($row['email']);
                    if (empty($custID)) {
                        continue;
                    }

                    $wishList[$indexParent]['customer_id'] = $custID;
                    $wishList[$indexParent]['email'] = $row['email'];
                    $wishList[$indexParent]['total_item'] = count($row['items']);

                    $indexParent++;
                }

                $data['data'] = $wishList;
                $data['recordsTotal'] =  $result['recordsTotal'];
            }
        }

        return $data;
    }

    private function getBySkuFromElastic($sku = [], $companyCode = 'ODI')
    {
        $functions = [];
        $totalSKU = count($sku);
        $joinedSKU = '["' . implode('","',$sku) . '"]';

        foreach ($sku as $key => $val) {
            // $b = $totalSKU - $key;
            array_push($functions, array(
                "filter" => array(
                    "nested" => array(
                        "path" => "variants",
                        "query" => array(
                            "term" => array(
                                "variants.sku.faceted" => $val
                            )
                        )
                    )
                ),
                "weight" => ($totalSKU - $key)
            ) );
        }

        $mustArray = [
            '{
                "nested": {
                  "path": "variants",
                  "query": {
                    "terms": {
                      "variants.sku.faceted": ' . $joinedSKU . '
                    }
                  }
                }
              }'
        ];

        $concatMustArray = implode(',', $mustArray);

        $body = '{
            "size": "'.count($sku).'",
            "_source": [
              "name",
              "url_key",
              "is_in_stock",
              "variants",
              "supplier",
              "is_extended",
              "review",
              "status_retail",
              "brand.name",
              "minimum_order",
              "is_apply_multiple"
            ],
            "query": {
                "function_score": {
                    "query": {
                        "bool": {
                          "must":[
                            ' . $concatMustArray . '
                          ]
                        }
                      },
                    "functions": '.json_encode($functions).',
                    "score_mode": "max",
                    "boost_mode": "sum",
                    "min_score": 1
                }
            }
          }';

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/_search';
        if (!empty(getenv('HTTP_PROXY'))) {
            $client = new \GuzzleHttp\Client(['proxy' => getenv('HTTP_PROXY')]);
        } else {
            $client = new \GuzzleHttp\Client();
        }

        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);
        $result = \GuzzleHttp\json_decode($response->getBody(), true);

        return $result;
    }

    public function saveWishlist($params = array())
    {
        $flagComplete = true;
        $fields = array('customer_id', 'sku');
        foreach ($fields as $field) {
            if (!isset($params[$field])) {
                $flagComplete = false;
            }
        }

        if ($flagComplete) {
            $params['added_date'] = date("Y-m-d H:i:s");

            if (!isset($params['is_product_scanned'])) {
                $params['is_product_scanned'] = 0;
            }

            if (!isset($params['store_code_new_retail'])) {
                $params['store_code_new_retail'] = "";
            }

            $result = $this->getBySkuFromElastic(array($params['sku']) );
            if (count($result['hits']['hits']) > 0) {
                $headData = $result['hits']['hits'][0]['_source'];
                $variantData = $headData['variants'][0];
                $params['product_name'] = $headData['name'];
                $params['is_in_stock'] = $headData['is_in_stock'];

                $params['minimum_order'] = !empty($headData['minimum_order']) ? $headData['minimum_order'] : 1;
                if ($params['minimum_order'] == 0) {
                    $params['minimum_order'] = 1;
                }
                $params['is_apply_multiple'] = !empty($headData['is_apply_multiple']) ? $headData['is_apply_multiple'] : 0;
                

                $prices = $variantData['prices'][0];
                date_default_timezone_set("Asia/Jakarta");
                $skrg = date("Y-m-d H:i:s");
                $special_from_date = date("Y-m-d H:i:s", strtotime($prices['special_from_date']));
                $special_to_date = date("Y-m-d H:i:s", strtotime($prices['special_to_date']));
                $flagActive = false;
                if (($skrg > $special_from_date) && ($skrg < $special_to_date)) {
                    $flagActive = true;
                }

                $params['selling_price'] = $prices['price'];
                if ($flagActive) {
                    if ($prices['special_price'] > $prices['price']) {
                        $params['selling_price'] = $prices['price'];
                    } else {
                        $params['selling_price'] = $prices['special_price'];
                    }
                }

                $params['review'] = isset($headData['review']) ? $headData['review'] : [];

                $wishlistModel = new \Models\Wishlist();
                $params['email'] = $wishlistModel->getEmailByCustomerId($params['customer_id']);

                // Validate if email is empty. This is from miss ace usecase where cs05 can delete membership. Thus, on our database, the email
                // will be emptied. For more information, please ask Hendro or other MISSACE PM
                if (!empty($params['email'])) {
                    $resultIsExist = $this->wishlist->getWishlistDataByEmail(array('email' => $params['email']));
                    if (count($resultIsExist) == 0) {
                        // if not exist insert the product
                        $params['is_exist'] = 0;
                        $this->wishlist->setFromArray($params);
                        $result = $this->wishlist->saveWishlist();
                    } else {
                        // if exist update the items list
                        $params['is_exist'] = 1;
                        $this->wishlist->setFromArray($params);
                        $result = $this->wishlist->saveWishlist($params);
                    }
    
                    // Invalidate Redis Cache
                    $wishlistModel->invalidateCustomerWishlistCache($params["customer_id"]);
                } else {
                    $result['errors'] = "Email is empty";
                    return $result;
                }

            } else {
                $result['errors'] = "Cannot get data from elastic";
                return $result;
            }
        } else {
            $result['errors'] = "Data must complete";
        }

        return $result;
    }

    public function deleteWishlist($params = array())
    {
        $flagComplete = true;
        $fields = array('customer_id', 'sku');
        foreach ($fields as $field) {
            if (!isset($params[$field])) {
                $flagComplete = false;
            }
        }

        if ($flagComplete) {
            // is email exist
            $wishlistModel = new \Models\Wishlist();
            $params['email'] = $wishlistModel->getEmailByCustomerId($params['customer_id']);
            $resultIsExist = $this->wishlist->getWishlistDataByEmail(array('email' => $params['email']));
            if (count($resultIsExist) > 0) {
                $this->wishlist->setFromArray($params);
                $result = $this->wishlist->deleteWishlist();
                // Invalidate Redis Cache
                $wishlistModel->invalidateCustomerWishlistCache($params['customer_id']);
            } else {
                $result['errors'] = "Data not found";
            }
        } else {
            $result['errors'] = "Data must complete";
        }

        return $result;
    }

    public function saveMemberChances($params = array())
    {
        if (empty($params)) {
            $this->errorMessages[] = 'Parameter is empty.';
            $this->errorCode = '400';
            return false;
        }

        // No need model - little feature
        if (!empty($params['customer_email']) && !empty($params['order_no']) && !empty($params['is_subscribed'])) {
            $customerModel = new \Models\Customer();
            $sql = "INSERT INTO member_chances (customer_email, order_no, is_subscribed) VALUES('" . $params['customer_email'] . "','" . $params['order_no'] . "', '" . $params['is_subscribed'] . "')"; // customeR_email, invoice_number, is_subscribed
            $customerModel->useWriteConnection();
            $result = $customerModel->getDi()->getShared($customerModel->getConnection())->query($sql);
            return $result->numRows();
        } else {
            $this->errorMessages[] = 'Parameters are not complete';
            $this->errorCode = '400';
            return false;
        }
    }

    public function verifyEmail()
    {
        $customerId = $this->customer->getCustomerId();
        if (empty($customerId)) {
            $this->errorMessages[] = 'Parameters are not complete';
            $this->errorCode = '400';
            return false;
        }

        // check from customer otp table
        $result = $this->customerOtp->findFirst(
            array(
                "conditions" => "customer_id = '" . $customerId . "'",
                "limit" => 1
            )
        );

        if ($result) {
            $otpIsEmailVerified = $result->getIsEmailVerified();
            if ($otpIsEmailVerified > 0) {
                // update
                $customerModel = new \Models\Customer();
                $dateNow = date("Y-m-d H:i:s");
                $sql = "UPDATE customer SET is_email_verified = 10, updated_at = '" . $dateNow . "' WHERE customer_id = " . $customerId;
                $customerModel->useWriteConnection();
                $updateResult = $customerModel->getDi()->getShared($customerModel->getConnection())->query($sql);
                return $updateResult->numRows();
            } else {
                // say its not verified yet at otp table
                $this->errorMessages[] = 'Email belum terverifikasi';
                $this->errorCode = '404';
                return false;
            }
        } else {
            $this->errorMessages[] = 'Data tidak ditemukan';
            $this->errorCode = '404';
            return false;
        }
    }

    public function resetPassword () {
        // this is for forgot password action
        // first we need verify password and customer_id are present
        if (empty($this->customer->getCustomerId()) || empty($this->customer->getPassword())) {
            $this->errorMessages[] = 'Payload is not valid';
            $this->errorCode = '400';
            return false;
        }

        // skip attr when update
        $this->customer->setSkipAttributeOnUpdate(
            ['first_name', 'last_name', 'email', 'is_email_verified', 'is_phone_verified', 'phone', 'birth_date', 'gender', 'status', 'is_new', 'created_at', 'password_token', 'registered_by', 'company_code', 'fingerprint']
        );

        // then we just need to update password by saving data
        // do we need to check token again ?
        if (!$this->customer->saveData()) {
            $this->errorMessages[] = 'Save Failed';
            $this->errorCode = '400';
            return false;
        }

        return;
    }

    public function getAbuserList($params = array())
    {
        $data = array();
        if (isset($params['limit']) && isset($params['offset'])) {            
            $queryCustomerId = false;
            $queryEmail = false;
            $queryPhone = false;
            $queryName = false;
            $queryCustomerId = false;
            $queryGroup = false;

            $emailQuery = '';
            $phoneQuery = '';
            $nameQuery = '';
            $customerIdQuery = '';
            $groupQuery = '';

            if(isset($params['customer_id_keyword'])){
                if($params['customer_id_keyword'] <> '') {
                    $queryCustomerId = true;
                    $customerIdQuery = $params['customer_id_keyword'];
                }
            }

            if(isset($params['group_keyword'])){
                if($params['group_keyword'] <> '') {
                    $queryGroup = true;
                    $groupQuery = $params['group_keyword'];
                }
            }

            if(isset($params['email_keyword'])){
                if($params['email_keyword'] <> '') {
                    $queryEmail = true;
                    $emailQuery = $params['email_keyword'];
                }
            }

            if(isset($params['phone_keyword'])){
                if($params['phone_keyword'] <> '') {
                    $queryPhone = true;
                    $phoneQuery = $params['phone_keyword'];
                }
            }

            if(isset($params['name_keyword'])){
                if($params['name_keyword'] <> '') {
                    $queryName = true;
                    $nameQuery = $params['name_keyword'];
                }
            }

            

            $abusers = array();
            $abuserModel = new \Models\CustomerAbuser();
            if(!$queryEmail && !$queryPhone && !$queryName && !$queryCustomerId && !$queryGroup) {                
                $abusers = $abuserModel->getAllListAbusers();
            } else {
                $abusers = $abuserModel->getListAbusersByKeyword($emailQuery,$phoneQuery,$nameQuery, $customerIdQuery, $groupQuery);
            }
            

            $data['recordsTotal'] = count($abusers);
            $data['data'] = array_slice($abusers, $params['offset'], $params['limit'], false);
        }

        return $data;
    }

    public function getCustomerAbuser($params = array())
    {
        $customer_id = $params['customer_id'];

        $abuserModel = new \Models\CustomerAbuser();
        $abusers = $abuserModel->getGroupAbuser($customer_id);

        if (count($abusers) > 0) {
            return $abusers[0]['group'];
        } else {
            return '';
        }

    }

    public function loginStoreMode($params = array()) {
        // Payload
        // {
        //     "phone": "088374723434",
        //     "company_code : "ODI",
        //     "store_code_new_retail :"A333"
        //     "member_number": "AR02896000" - may empty
        //     "cart_id": "b532af5084907e315f344144ec37c6857aa698cc"
        //     "minicart_id": "12da3fe6d72e1376457ef9866180d47929cf3c40"
        //     "action": "check" | "login"
        //     "user_agent": "aa2-sm-ios"||"aa2-sm-android"||"ios"||"android"
        //     "is_use_aa2": true
        // }

        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return;
        }

        $requiredParam = ["phone", "company_code", "store_code_new_retail", "action"];
        foreach ($requiredParam as $value) {
            if (empty($params[$value])) {
                $this->errorCode = 400;
                $this->errorMessages = "Invalid payload";
                return;
            }
        }

        if ((!empty($params["member_number"]) && $params["company_code"] != "ODI") || ($params["action"] == "login" && (empty($params["cart_id"]) || empty($params["minicart_id"])))) {
            $this->errorCode = 400;
            $this->errorMessages = "Invalid payload";
            return;
        }

        // Init Library
        $cartLib = new \Library\Cart;

        // Init Model
        $customerModel = new \Models\Customer;
        $customer2GroupModel = new \Models\Customer2Group;
        $responseResult = array();

        // Get from customer table
        $customerParam["conditions"] = sprintf("company_code = '%s' AND phone = '%s' AND status = 10", $params['company_code'], $params['phone']);
        $customerParam["order"] = "last_login DESC";
        $customerFound = $customerModel->findFirst($customerParam);
        if (!$customerFound) {
            // customer isn't registered yet
            switch ($params["company_code"]) {
                case 'ODI':
                    $companyName = "ruparupa";
                    break;
                case 'AHI':
                    $companyName = "ace";
                    break;
                case 'HCI':
                    $companyName = "informa";
                    break;
                default:
                    $companyName = "kami";
            }
            $this->errorCode = 400;
            $this->errorMessages = "Data Anda belum terdaftar sebagai account ".$companyName;
            return;
        }
        $customerModel->setFromArray($customerFound->toArray());

        // Check user email because it may be empty
        if (empty($customerModel->getEmail())) {
            $this->errorCode = 400;
            $this->errorMessages = "Account Anda belum memiliki email";
            return;
        }
        
        $responseResult["customer_id"] = (int)$customerModel->getCustomerId();
        $responseResult["first_name"] = $customerModel->getFirstName();
        $responseResult["email"] = $customerModel->getEmail();
        $responseResult["phone"] = $customerModel->getPhone();
        $responseResult["member_number"] = "";
        
        // Is customer already have member
        // Determine group id
        if (!empty($params["member_number"])) {
            $groupId = $this->checkGroupId($params["member_number"]);
        } else {
            switch ($params["company_code"]) {
                case 'ODI':
                    $groupId = $this->checkGroupIdByStoreCode($params["store_code_new_retail"]);
                    break;
                case 'AHI':
                    $groupId = 4;
                    break;
                case 'HCI':
                    $groupId = 5;
                    break;
                case 'TGI':
                    $groupId = 6;
                    break;
                case 'SLM':
                    $groupId = 9;
                    break;
                default:
                    $groupId = 0;
            }
        }
        if ($groupId == 0) {
            $this->errorCode = 400;
            $this->errorMessages = "Data member tidak ditemukan";
            return;
        }

        // Check if the group_id is cohesive or not
        $cohesiveBu = array_map('intval', explode(',', getenv('COHESIVE_GROUP_ID')));
        if (!in_array($groupId, $cohesiveBu)) {
            // We also check for expired member if it's an ace member
            $isVerifiedValues = [10];
            if ($groupId == 4) {
                array_push($isVerifiedValues, 5);
            }
    
            $groupParam["conditions"] = "customer_id = ". $customerModel->getCustomerId() ." AND is_verified IN (". implode(",", $isVerifiedValues) .") AND group_id = ".$groupId;
            $customerMember = $customer2GroupModel->findFirst($groupParam);
            if (!$customerMember) {
                if (!empty($params["member_number"])){
                    // Check new member that is supplied by customer
                    $memberData = $this->checkMemberStatus($params["member_number"]);
                    $isVerifiedNum = 0;
    
                    if (empty($memberData)) { 
                        $this->errorCode = 400;
                        $this->errorMessages = "Gagal mendapatkan informasi member Anda";
                        return;
                    }

                    // Check member status
                    /**
                     * isVerifiedNum
                     * 10 => valid and not expired
                     * 5 => valid and expired
                     * 0 => not valid and expired
                     */
                    if ($memberData["cust_id"] != ""){
                        // Get is verified value 
                        $isVerifiedNum = $memberData["is_verified"];
                    } 
                    
                    if ($isVerifiedNum <= 0) {
                        $this->errorCode = 400;
                        $this->errorMessages = "Member yang Anda masukkan tidak valid";
                        return;
                    }
    
                    // Still return error if expired member is not an ace member
                    if ($isVerifiedNum == 5 && $groupId != 4) {
                        $this->errorCode = 400;
                        $this->errorMessages = "Member yang Anda masukkan telah expired";
                        return;
                    }
    
                    // Insert member number to customer_2_group
                    // Only insert when action is login
                    if ($params["action"] == "login") {
                        $verifiedDate = date_create($memberData["exp_date"]);
                        $newCustomerMember = array(
                            "customer_id" => $customerModel->getCustomerId(),
                            "group_id" => $groupId,
                            "member_number" => $params["member_number"],
                            "update_timestamp" => date('Y-m-d H:i:s'),
                            "is_verified" => $isVerifiedNum,
                            "expiration_date" => date_format($verifiedDate, "Y-m-d H:i:s")
                        );
                        $customer2GroupModel->setFromArray($newCustomerMember);
                        $customer2GroupModel->saveData();
                    }
                    $responseResult["member_number"] = $params["member_number"];
                }
            } else {
                $customer2GroupModel->setFromArray($customerMember->toArray());
                if (!empty($params["member_number"]) && $customer2GroupModel->getMemberNumber() != $params["member_number"]){
                    $this->errorCode = 400;
                    $this->errorMessages = "Member yang Anda masukkan tidak sesuai";
                    return;
                } 
                $responseResult["member_number"] = $customer2GroupModel->getMemberNumber();
            }
       } else {
            // Retrieve the unify member number if the phone is not empty and the group_id is cohesive
            if (!empty($params["phone"])){
                $memberData =$this->getStoremodeMemberDetail($params["phone"]);
                if (!empty($memberData["rrr_id"])){
                    $responseResult["member_number"] = $memberData["rrr_id"];
                }
            }
       }

        // When action login we have to merge login for minicart and cart
        if ($params["action"] == "login") {
            $isUseAA2 = $params["is_use_aa2"] || ($params["user_agent"] != "android" && $params["user_agent"] != "ios");

            // Note that the function call order matters here, we must merge login for minicart first then we can merge login for cart
            $respMergeMinicart = $cartLib->getCustomerMiniCartId($customerModel->getCustomerId(), $params["minicart_id"], $params["cart_id"], "ODI");
            if (!empty($respMergeMinicart['error']) || empty($respMergeMinicart['data']['cart_id']) ) {
                $this->errorCode = !empty($respMergeMinicart['error']['code']) ? $respMergeMinicart['error']['code'] : 500;
                $this->errorMessages = !empty($respMergeMinicart['error']['message']) ? $respMergeMinicart['error']['message'] : "Terjadi kesalahan silakan ulangi beberapa saat lagi";
                return;
            }
            $respMergeCart = $cartLib->getCustomerCartId($customerModel->getCustomerId(), $params["cart_id"], "ODI",'',$isUseAA2, true);
            if (!empty($respMergeCart['error']) || empty($respMergeCart['data']['cart_id']) ) {
                $this->errorCode = !empty($respMergeCart['error']['code']) ? $respMergeCart['error']['code'] : 500;
                $this->errorMessages = !empty($respMergeCart['error']['message']) ? $respMergeCart['error']['message'] : "Terjadi kesalahan silakan ulangi beberapa saat lagi";
                return;
            }
            $responseResult["cart_id"] = $respMergeCart['data']['cart_id'];
        }
        
        return $responseResult;
    }

    public function getMemberData($member) {
        // ex: member = AR00000000 | IR00000000 | SC00000000
        $dataResp = array();
        if (preg_match("/^(AR|ARE|TAM|AV|IR|TIM|SC|TCM)[0-9]+/", $member)) {
            $checkPayload = array();
            $checkPayload["P_card_id"] = $member;

            // Determine company type for payload
            switch (true) {
                case strtolower(substr($member, 0, 2)) == "ar" || strtolower(substr($member, 0, 3)) == "are" || strtolower(substr($member, 0, 3)) == "tam" || strtolower(substr($member, 0, 2)) == "av":
                    $checkPayload["P_company_type"] = "A";
                    break;
                case strtolower(substr($member, 0, 2)) == "ir" || strtolower(substr($member, 0, 3)) == "tim":
                    $checkPayload["P_company_type"] = "I";
                    break;
                case strtolower(substr($member, 0, 2)) == "sc" || strtolower(substr($member, 0, 3)) == "tcm":
                    $checkPayload["P_company_type"] = "T";
                    break;
            }

            $klLib = new \Library\KawanLamaSystem;
            $checkMemberResp = $klLib->checkStatusMember($checkPayload);
            $dataResp = json_decode($checkMemberResp, true);        
        }
        return $dataResp;
    }

    public function checkGroupId($member){
        // ex: member = AR00000000 | ARE0000000 | TAM0000000 | IR00000000 | SC00000000
        $groupId = 0;
        if (!empty($member) && is_string($member)) {
            switch (true) {
                case strtolower(substr($member, 0, 2)) == "ar" || strtolower(substr($member, 0, 3)) == "are" || strtolower(substr($member, 0, 3)) == "tam":
                    $groupId = 4;
                    break;
                case strtolower(substr($member, 0, 2)) == "ir":
                    if ((strtolower(substr($member, 0, 3)) == "ir7") || (strtolower(substr($member, 0, 3)) == "ir8")){
                        $groupId = 9;
                    }else{
                        $groupId = 5;
                    }
                    break;
                case strtolower(substr($member, 0, 2)) == "sc":
                    $groupId = 6;
                    break;
                case strtolower(substr($member, 0, 3)) == "rrr":
                    $groupId = 1;
                    break;
            }
        }
        return $groupId;
    }

    public function checkGroupIdByStoreCode($storeCode){
        // ex: storecode = Axxx | Hxxx | Txxx 
        $groupId = 0;
        if (!empty($storeCode) && is_string($storeCode)) {
            switch (strtolower(substr($storeCode, 0, 1))) {
                case "a": 
                    $groupId = 4;
                    break;
                case strtolower(substr($storeCode, 0, 2)) == "h7":
                    $groupId = 9;
                    break;    
                case strtolower(substr($storeCode, 0, 2)) == "j7":
                    $groupId = 9;
                    break;    
                case strtolower(substr($storeCode, 0, 1)) == "h":
                case "h":
                    $groupId = 5;
                    break;
                case "j":
                    $groupId = 5;
                    break;
                case "t":
                    $groupId = 6;
                    break;
            }
        }
        return $groupId;
    }

    public function linkingCustomerBetweenCompany($params = array()) {
        // Payload
        // {
        //     "email": "email@email.com",
        //     "phone": "088374723434",
        //     "company_code": "ODI/AHI/HCI"
        // }

        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return false;
        }

        // Init Model
        $customerModel = new \Models\Customer;
        $customer2GroupModel = new \Models\Customer2Customer;
        $responseResult = array();
        
        if (!empty($params["email"]) && !empty($params["phone"])) {
            $customerParam["conditions"] = "(email = '". $params["email"] ."' OR phone = '". $params["phone"] ."') AND status = 10 AND email != '' AND phone != '' AND email != '-' AND phone != '-' AND company_code IN ('ODI','AHI','HCI')";
            $customerParam["order"] = "last_login DESC";
            $customerParam["columns"] = "customer_id,company_code";
            $customerFound = $customerModel->find($customerParam);

            if (empty($customerFound)) {
                // customer isn't registered yet
                $this->errorCode = 400;
                $this->errorMessages = "Data tidak ditemukan";
                return false;
            }

            $customer2customerEntities = [];

            $customerAHI = [];
            $customerODI = [];
            $customerHCI = [];
            foreach ($customerFound as $key => $val) {
                if (!empty($customerAHI) && !empty($customerODI) && !empty($customerHCI)) {
                    break;
                }

                $companyCode = $val["company_code"];
                if (empty($customerODI)) {
                    if ($companyCode == "ODI") {
                        $customerODI = $val;
                        $customer2customerEntities["customer_id_odi"] = $customerODI["customer_id"];
                        continue;
                    }
                }
                
                if (empty($customerAHI)) {
                    if ($companyCode == "AHI") {
                        $customerAHI = $val;
                        $customer2customerEntities["customer_id_ahi"] = $customerAHI["customer_id"];
                        continue;
                    }   
                }

                if (empty($customerHCI)) {
                    if ($companyCode == "HCI") {
                        $customerHCI = $val;
                        $customer2customerEntities["customer_id_hci"] = $customerHCI["customer_id"];
                        continue;
                    }   
                }

            }

            if (!empty($customerODI) && (!empty($customerHCI) || !empty($customerAHI))) {
        
                // Check if the customerAHI or customerODI has already present on table customer_2_customer
                $customer2CustomerModel = new \Models\Customer2Customer();
                $searchParam["conditions"] = "customer_id_odi = ".$customerODI["customer_id"];
                $customer2CustomerResult = $customer2CustomerModel->findFirst($searchParam);
                if ($customer2CustomerResult) {
                    $customer2customerEntities["customer_2_customer_id"] = $customer2CustomerResult->getCustomer2customerId();
                }

                // Preparing to insert
                $customer2CustomerModel->setFromArray($customer2customerEntities);

                if ($customer2CustomerModel->saveData("save_customer2customer")) {
                    // success
                    return true;
                } else {
                    $this->errorCode = 503;
                    $this->errorMessages = "Failed to save data";
                    return false;
                }
            } else {
                $this->errorCode = 404;
                $this->errorMessages = "Link candidate for customer ".$params["company_code"]." is not found";
                return false;
            }
        }
        
        $this->errorCode = 400;
        $this->errorMessages = "Invalid payload";
        return false;
    }

    public function getStoreModeCustomer($params = array()) {
        // Payload
        // {
        //     "member_number": "TAM0196040",
        //     "phone": "085782667339",
        //     "email": "ruparupadev@gmail.com"
        // }

        if (empty($params)) {
            $this->errorCode = "RR100";
            $this->errorMessages = "Parameter not specified";
            return false;
        }

        if (empty($params["phone"]) && empty($params["member_no"]) && empty($params["email"])){
            $this->errorCode = "RR100";
            $this->errorMessages = "Need phone number, member number or email";
            return false;
        }

        // Init Model
        $customerModel = new \Models\Customer;
        $Customer2GroupModel = new \Models\Customer2Group();
        $customerFound = array();

        if(!empty($params["phone"])){
            $customerParam["conditions"] = " phone = '". $params["phone"] ."' AND status = 10 AND company_code IN ('ODI','AHI','HCI')";
            $customerParam["order"] = "updated_at DESC";
            $customerParam["columns"] = "customer_id";
            $customerParam['limit'] = 1;
            $results = $customerModel->find($customerParam);
            foreach ($results as $key => $val) {
                $customerFound["customer_id"] = $val["customer_id"];
            }
        }else if (!empty($params['email'])){
            $customerParam["conditions"] = " email = '". $params["email"] ."' AND status = 10 AND company_code IN ('ODI','AHI','HCI')";
            $customerParam["order"] = "updated_at DESC";
            $customerParam["columns"] = "customer_id";
            $customerParam['limit'] = 1;
            $results = $customerModel->find($customerParam);
            foreach ($results as $key => $val) {
                $customerFound["customer_id"] = $val["customer_id"];
            }
        }else if (!empty($params["member_no"])){
            $sql = "SELECT c.customer_id FROM customer_2_group cg 
            INNER JOIN customer c ON cg.customer_id = c.customer_id 
            WHERE c.status  = '10' AND cg.member_number = '".$params["member_no"] ."'
            ORDER BY c.updated_at DESC LIMIT 1";
            $result = $Customer2GroupModel->getDi()->getShared($Customer2GroupModel->getConnection())->query($sql);
            $result->setFetchMode(Db::FETCH_ASSOC);
            $customerFound = $result->fetch();
        }

        if (empty($customerFound)) {
            // customer isn't registered yet
            $this->errorCode = 400;
            $this->errorMessages = "Data customer tidak ditemukan";
            return false;
        }
        return $customerFound;
    }

    public function whitelistAffiliate($customerId) {
        $isAllowed = true;
        $whitelist = getenv("WHITELIST_CUSTOMER_ID_AFFILIATE");
        
        if ($whitelist) {
            $allowedIds = explode(',', $whitelist);
            
            if (!in_array($customerId, $allowedIds)) {
                $isAllowed = false;
            }
        }
    
        return $isAllowed;
    }

    public function checkMemberStatus($params){
        // ex: member = AR00000000 | IR00000000 | SC00000000
        $resultData = array();
        if (preg_match("/^(AR|ARE|TAM|AV|IR|TIM|SC|TCM)[0-9]+/", $params)) {
            $checkPayload = array();
            $checkPayload["member_number"] = $params;

            // Determine company type for payload
            switch (true) {
                case strtolower(substr($params, 0, 2)) == "ar" || strtolower(substr($params, 0, 3)) == "are" || strtolower(substr($params, 0, 3)) == "tam" || strtolower(substr($params, 0, 2)) == "av":
                    $checkPayload["company_code"] = "AHI";
                    break;
                case strtolower(substr($params, 0, 2)) == "ir" || strtolower(substr($params, 0, 3)) == "tim":
                    $checkPayload["company_code"] = "HCI";
                    break;
                case strtolower(substr($params, 0, 2)) == "sc" || strtolower(substr($params, 0, 3)) == "tcm":
                    $checkPayload["company_code"] = "TGI";
                    break;
            }

            $apiWrapper = new APIWrapper(getenv('CUSTOMER_API'));
            $apiWrapper->setEndPoint("customer/group/check-status");
            $apiWrapper->setParam($checkPayload);
            if ($apiWrapper->sendRequest("post")) {
                $apiWrapper->formatResponse();
                LogHelper::log('check_member_status','[SUCCESS] ' . json_encode($apiWrapper->getData()));
            } else {
                LogHelper::log('check_member_status','[ERROR] when calling customer API ' . json_encode($apiWrapper->getError()));
            }

            $resultData = $apiWrapper->getData();
        }

        return $resultData;
    }

    public function getStoremodeMemberDetail($params){
    
        $resultData = array();
        $apiWrapper = new APIWrapper(getenv('GO_CUSTOMER_COHESIVE'));
        $apiWrapper->setEndPoint(sprintf("storemode/auth/check?identifier=%s", $params));
        if ($apiWrapper->sendRequest("get")) {
            $apiWrapper->formatResponse();
            LogHelper::log('get_unify_member','[SUCCESS] ' . json_encode($apiWrapper->getData()));
        } else {
            LogHelper::log('get_unify_member','[ERROR] when calling customer cohesive API ' . json_encode($apiWrapper->getError()));
        }

        $resultData = $apiWrapper->getData();

        return $resultData;
    }

}