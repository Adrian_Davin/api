<?php
namespace Library;

use Helpers\LogHelper;

class SalesOrderTracker
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function __construct()
    {
        $this->salesOrderTracker = new \Models\SalesOrderTracker();
    }

    public function orderSaveTrackerProcess($sales_order_id = null, $gclidData = array()) {
        if (empty($sales_order_id)) {
            LogHelper::log("sales_order_tracker", "[SAVE TRACKER][FAIL] Failed to save sales order tracker. Error: Missing sales_order_id param.");
            return;
        }

        if (empty($gclidData['platform']) || empty($gclidData['valid_at']) || empty($gclidData['identifier']) || empty($gclidData['expired_at'])) {
            return;
        }

        $orderTrackerParams = array(
            "sales_order_id" => $sales_order_id,
            "platform"       => $gclidData['platform'],
            "valid_at"       => $gclidData['valid_at'],
            "identifier"     => $gclidData['identifier'],
            "expired_at"     => $gclidData['expired_at']
        );

        $salesOrderTrackerModel = new \Models\SalesOrderTracker();
        $today = new \DateTime("now");
        $now = $today->format('Y-m-d H:i:s');
        
        if ($now <= $gclidData['expired_at']) {
            $salesOrderTrackerModel->setFromArray($orderTrackerParams);

            if ($gclidData['valid_at'] == "order_created") {
                $salesOrderTrackerModel->setStatus(10);
            } else {
                $salesOrderTrackerModel->setStatus(0);
            }

            $salesOrderTrackerModel->saveData("sales_order_tracker");
        }
    }

    public function invoiceSaveTrackerProcess($sales_order_id = null){
        if (empty($sales_order_id)) {
            LogHelper::log("sales_order_tracker", "[SAVE TRACKER][FAIL] Failed to save sales order tracker. Error: Missing sales_order_id param.");
            return;
        }

        $salesOrderTrackerModel = new \Models\SalesOrderTracker();
        $salesOrderTracker = $salesOrderTrackerModel->findAll("sales_order_id = '" . $sales_order_id . "' AND valid_at = 'invoice_created'");
        
        if ($salesOrderTracker->count() > 0) {
            $dateNow = date("Y-m-d H:i:s");
            $sql = "UPDATE sales_order_tracker SET status = 10, updated_at = '" . $dateNow ."' WHERE sales_order_id = " . $sales_order_id;

            $salesOrderTrackerModel->useWriteConnection();
            $salesOrderTrackerModel->getDi()->getShared($salesOrderTrackerModel->getConnection())->query($sql);
        } else {
            LogHelper::log("sales_order_tracker", "[UPDATE TRACKER][FAIL] No order tracker found for sales_order_id " . $sales_order_id);
            return;
        }

    }

    public function sendOrderTrackerVueAIAnalytics($cart_id = null){
        if (!$cart_id) {
            return false;
        }

        $apiWrapper = new APIWrapper(getenv("CART_API"));
        $apiWrapper->setEndPoint("/cart/analytics/order");
        $apiWrapper->setParam(
            array(
                'cart_id' => $cart_id
            )
        );

        if ($apiWrapper->sendRequest("post")) {    
            $apiWrapper->formatResponse();
        } else {
            $this->errorCode = "500";
            $this->errorMessages = "Failed to send vue.ai order tracker analytics";
            return false;
        }

        if (!empty($apiWrapper->getError())){
            \Helpers\LogHelper::log("api", json_encode($apiWrapper->getError()));
            $error = $apiWrapper->getError();

            $this->errorCode = $error['code'];
            $this->errorMessages = $error['messages'];
            return false;            
        }

        return true;
    }
}


