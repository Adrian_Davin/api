<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 3/3/2017
 * Time: 10:15 AM
 */

namespace Library;


class JournalSAPItem
{
    protected $posnr;
    protected $zuonr;
    protected $wrbtr;
    protected $waers = "IDR";
    protected $kostl;
    protected $kunnr;
    protected $lifnr;

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    public function __construct($invoice = null)
    {
        if(!empty($invoice)) {
            $this->invoice = $invoice;
        }
    }

    public function setFromArray($data_array = array())
    {
        foreach($data_array as $key => $val) {
            $this->{$key} = $val;
        }
    }

    public function prepareItemParam()
    {
        $thisArray = get_object_vars($this);
        $parameter = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if($key=='POSNR')
            $this->posnr = $val;
            else if($key=='WRBTR')
            $this->wrbtr = $val;

            if(is_scalar($val)) {
                $parameter[strtoupper($key)] = $val;
            }
        }
        //\Helpers\LogHelper::log("create_journal","POSNR " . $this->posnr . " = " . $this->wrbtr);

        return $parameter;
    }
}