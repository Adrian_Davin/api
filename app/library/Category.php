<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 5/15/2017
 * Time: 3:00 PM
 * User: roesmien_ecomm
 * Date: 5/16/2017
 * Time: 9:33 AM
 */

namespace Library;


use Helpers\RouteHelper;
use Models\MasterUrlKey;
use Models\Product2Category;
use Models\ProductCategory;
use Models\ProductCategoryVm;
use Phalcon\Db as Database;

class Category
{
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    // one time only - AHI
    public function getAllCategories($params = array()) {
        if (empty($params)) {
            return false;
        }

        $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';

        // we need to query - get category_id, path and category_key categories AHI
        $categoryModel = new ProductCategory();
        $result = $categoryModel->find([
            "columns" => "category_id, name, path, category_key, company_code",
            "conditions" => "company_code = '".$companyCode."' AND status > 0"
        ]);

        if (!empty($result)) {
            return $result->toArray();
        }

        return false;
    }

    public function getPairingProductCategory() {
        // $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';
        $categoryModel = new ProductCategory();

        // we need to query - get category_id, path and category_key categories AHI
        $sql  = "SELECT * FROM temp_product_multicompany_del";
        $categoryModel->useReadOnlyConnection();
        $result = $categoryModel->getDi()->getShared($categoryModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $final = $result->fetchAll();

        if (!empty($final)) {
            return $final;
        }

        return false;
    }

    public function getGeneratedURl($params = array()) {
        if (empty($params)) {
            return false;
        }

        $path = isset($params['path']) && !empty($params['path']) ? $params['path'] : 'ODI';
        $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';

        // we need to query - get category_id, path and category_key categories AHI
        $categoryModel = new ProductCategory();
        $result = $categoryModel->find([
            "columns" => "category_id, name, category_key, company_code",
            "conditions" => "category_id IN ".$path." AND company_code = '".$companyCode."' AND status > 0"
        ]);

        if (!empty($result)) {
            $generatedPath = '';
            foreach ($result as $key => $val) {
                if (empty($generatedPath)) {
                    $generatedPath = $val['category_key'];
                } else {
                    $generatedPath = $generatedPath.'/'.$val['category_key'];
                }
            }
            return array('generated_path' => $generatedPath);
        }

        return false;
    }

    public function getCategoryList($params = array())
    {
        $access_type = isset($params['access_type'])?$params['access_type']:'';
        $master_app_id = isset($params['master_app_id'])?$params['master_app_id']:'';
        $categoryId = isset($params['category_id'])?$params['category_id']:'';
        $companyCode = isset($params['company_code'])?$params['company_code']:'ODI';
        $notUsingCompanyCode = isset($params['not_using_company_code'])?true:false;

        if($categoryId == '0'){
            return false;
        }
        unset($params['access_type']);
        unset($params['master_app_id']);
        unset($params['category_id']);
        unset($params['company_code']);
        unset($params['not_using_company_code']);

        // declare default section
        $section = 'category';

        $categoryModel = new \Models\ProductCategory();
        $query = $categoryModel::query();

        if(isset($params['url_key'])){

            $flagUrlKey = TRUE;
            $urlKey = explode('.',$params['url_key']);
            unset($params['url_key']);

            // check if section is custom_category
            $masterUrlKeyModel = new \Models\MasterUrlKey();
            $resultCategory = $masterUrlKeyModel->findFirst(' url_key = "'.$urlKey[0].'" AND section = "custom_category" AND company_code = "'.$companyCode.'" ');

            $categoryId = 0;
            if($resultCategory){
                $categoryId = $resultCategory->toArray()['reference_id'];
                $section = 'custom_category';
            } else {
                $masterUrlKeyModel = new \Models\MasterUrlKey();
                $resultCategory = $masterUrlKeyModel->findFirst(' url_key = "'.$urlKey[0].'" AND section = "'.$section.'" AND company_code = "'.$companyCode.'" ');

                if($resultCategory){
                    $categoryId = $resultCategory->toArray()['reference_id'];
                }
            }

            $query->where("category_id in (".$categoryId.") AND company_code = '".$companyCode."'");
        }else{
            // check if section is custom_category
            $categoryModelSection = new \Models\ProductCategory();
            if(!empty($categoryId)) {
                $result = $categoryModelSection->findFirst('category_id IN (' . $categoryId . ') and is_rule_based = 1');
                if ($result) {
                    $section = 'custom_category';
                }
            }else{
                $result = $categoryModelSection->findFirst('is_rule_based = 1');
                if ($result) {
                    $section = 'custom_category';
                }
            }

            $flagUrlKey = FALSE;
            if($access_type == "cms"){
                if($master_app_id == $_ENV['MASTER_APP_ID_SELLER']){
                    $query->where("status = 10 AND include_in_menu = 1 AND article_hierarchy != ''");

                    if (isset($params["level"]) && $params["level"] == 5) {
                        $query->where("status = 10 AND article_hierarchy != ''");
                    }
                }else{
                    $query->where("status >= -1");
                }
            }
            else{
                $query->where("status = 10");
            }

            if(!empty($categoryId)){
                $query->andWhere("category_id in (".$categoryId.")");
            }

            foreach($params as $key => $val)
            {
                $query->andWhere($key ." = '" . $val ."'");
                
            }

            if (!$notUsingCompanyCode) {
                $query->andWhere("company_code = '".$companyCode."'");
            }

            $query->orderBy('position');
        }
        $categoryList = $query->execute();

        // We need to find url_key for tcategory
        $categoryFinal = [];
        if(!empty($categoryList->toArray())){

            foreach ($categoryList as $key => $category) {
                $categoryArray = $category->toArray();
                $categoryArray['title'] = !empty($categoryArray['title']) ? $categoryArray['title'] : $categoryArray['name'];

                if($master_app_id != $_ENV['MASTER_APP_ID_SELLER']) {

                    $urlRoute = \Helpers\RouteHelper::getRouteData($categoryArray['category_id'], $section,
                        $access_type, $companyCode);

                    if (!empty($urlRoute)) {
                        if ($categoryArray['parent_id'] != '0') {
                            $routeData = \Helpers\RouteHelper::getRouteData($categoryArray['parent_id'], $section,
                                $access_type, $companyCode);

                            if (!empty($routeData)) {
                                //unset($categoryArray['url_path']);
                                $categoryArray['url_key'] = $urlRoute['url_key'];
                                $categoryArray['parent_url_path'] = $routeData['url_key'];
                                $categoryArray['additional_header'] = $urlRoute['additional_header'];
                                $categoryArray['inline_script'] = $urlRoute['inline_script'];
                            } else {
                                $section = 'category';
                                $routeData = \Helpers\RouteHelper::getRouteData($categoryArray['parent_id'], $section,
                                $access_type, $companyCode);
                                if (!empty($routeData)) {
                                    //unset($categoryArray['url_path']);
                                    $categoryArray['url_key'] = $urlRoute['url_key'];
                                    $categoryArray['parent_url_path'] = $routeData['url_key'];
                                    $categoryArray['additional_header'] = $urlRoute['additional_header'];
                                    $categoryArray['inline_script'] = $urlRoute['inline_script'];
                                } else {
                                    $categoryArray['url_key'] = "";
                                    $categoryArray['parent_url_path'] = "";
                                    $categoryArray['additional_header'] = "";
                                    $categoryArray['inline_script'] = "";
                                }
                            }
                        } else {
                            $categoryArray['url_key'] = $urlRoute['url_key'];
                            $categoryArray['parent_url_path'] = "";
                            $categoryArray['additional_header'] = $urlRoute['additional_header'];
                            $categoryArray['inline_script'] = $urlRoute['inline_script'];
                        }
                    } else {
                        $categoryArray['url_key'] = "";
                        $categoryArray['parent_url_path'] = "";
                        $categoryArray['additional_header'] = "";
                        $categoryArray['inline_script'] = "";
                    }

                    // if($access_type != 'cms' && isset($categoryArray['best_seller'])){
                    //     $bestSellerSKU = explode(',', $categoryArray['best_seller']);
                    //     $categoryArray['best_seller'] = array();

                    //     foreach ($bestSellerSKU as $sku) {
                    //         $product = new \Models\Product();
                    //         $best_seller = $product->searchProductFromElastic($sku);
                    //         if (!empty($best_seller)) {
                    //             $categoryArray['best_seller'][] = $best_seller;
                    //         }
                    //     }
                    // } else {
                    //     $categoryArray['best_seller'] = [];
                    // }
                    $categoryArray['best_seller'] = [];
                }
                if(empty($categoryArray['rule_based_hero_products'])) {
                    $categoryArray['rule_based_hero_products'] = "";
                }
                $categoryFinal[$key] = $categoryArray;

            }
            if($flagUrlKey){
                return $categoryFinal[0];
            }else{
                return $categoryFinal;
            }


        }
        else{
            $this->errorCode = "404";
            $this->errorMessages = "Category not found";
            return;
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getCategoryDetail($params = array())
    {
        $categoryList = $this->getCategoryList($params);

        return $categoryList[0];
    }

    /**
     * @param array $params
     * @return array|mixed
     */
    public function getCategoryTree($params = array())
    {
        $categoryTreeCacheLib = new \Library\Redis\CategoryTree();

        $accessType = isset($params['access_type'])?$params['access_type']:'';
        $userAgent = isset($params['user_agent']) ? $params['user_agent']: '';
        $companyCode = (isset($params['company_code']) && !empty($params['company_code'])) ? $params['company_code'] : 'ODI';
        $parentCategoryID = isset($params['parent_category_id']) ? $params['parent_category_id'] : 0;

        $categoryTree = $categoryTreeCacheLib::getCategoryTree($accessType, $companyCode, $parentCategoryID, $userAgent);

        if(empty($categoryTree)) {
            $categoryTree = $this->generateCategoryTree($accessType, $companyCode, $parentCategoryID, $userAgent);
        }

        if ($accessType == "cms") {
            return $categoryTree;
        }

        // reconstruct include in menu bcs its using cache
        foreach ($categoryTree as $key => $category) {
            if ($category["include_in_menu"] == 2 && isset($category["include_in_menu_start_time"]) && isset($category["include_in_menu_end_time"])) {
                $nowDate = strtotime(date("Y-m-d H:i:s"));
                $startTime = strtotime($category["include_in_menu_start_time"]);
                $endTime = strtotime($category["include_in_menu_end_time"]);

                if (($nowDate >= $startTime) && ($nowDate <= $endTime)) {
                    $categoryTree[$key]["include_in_menu"] = "1";
                } else {
                    unset($categoryTree[$key]);
                    continue;
                }
            }
            unset($categoryTree[$key]["include_in_menu_start_time"]);
            unset($categoryTree[$key]["include_in_menu_end_time"]);
            
            $categoryTree[$key]["children"] = $this->checkChildrenIncludeInMenu($categoryTree[$key]["children"]);
        }

        $categoryTree = array_values($categoryTree);
        return $categoryTree;
    }

    function checkChildrenIncludeInMenu($childrens = array())
    {
        foreach ($childrens as $key => $children) {
            if ($children["include_in_menu"] == 2 && isset($children["include_in_menu_start_time"]) && isset($children["include_in_menu_end_time"])) {
                $nowDate = strtotime(date("Y-m-d H:i:s"));
                $startTime = strtotime($children["include_in_menu_start_time"]);
                $endTime = strtotime($children["include_in_menu_end_time"]);

                if (($nowDate >= $startTime) && ($nowDate <= $endTime)) {
                    $childrens[$key]["include_in_menu"] = "1";
                } else {
                    unset($childrens[$key]);
                    continue;
                }
            }

            unset($childrens[$key]["include_in_menu_start_time"]);
            unset($childrens[$key]["include_in_menu_end_time"]);

            $childrens[$key]["children"] = $this->checkChildrenIncludeInMenu($childrens[$key]["children"]);
        }

        $childrens = array_values($childrens);
        return $childrens;
    }

    /**
     * @return bool
     */
    public function deleteCategoryTree($accessType = "", $companyCode = "ODI", $parentCategoryID = 0)
    {
        $deleteStatus = \Library\Redis\CategoryTree::deleteCategoryTree($accessType, $companyCode, $parentCategoryID);

        return $deleteStatus;
    }

     /**
     * @return bool
     */
    public function deleteCategoryCache()
    {
        $deleteStatus = \Library\Redis\CategoryTree::deleteCategoryCache();

        return $deleteStatus;
    }

    /**
     * @param string $accessType
     * @return array
     */
    public function generateCategoryTree($accessType = "", $companyCode = 'ODI', $parentCategoryID = 0, $userAgent = '')
    {
        $categoryTree = $this->_buildTree(2, $parentCategoryID, null,null, $accessType, $companyCode, $userAgent);
        
        // Delete existing sub gideon tree (greedy)
        if ($accessType == "cms") {
            $this->deleteCategoryTree($accessType, $companyCode, $parentCategoryID);
        }

        // Connect to redis to store data
        $categoryTreeCacheLib = new \Library\Redis\CategoryTree();
        $categoryTreeCacheLib::saveCategoryTree($categoryTree, $accessType, $companyCode, $parentCategoryID, $userAgent);

        return $categoryTree;
    }

    /**
     NOTE :
     * For level and parent_id, somehow we start it at lavel 2
     * @param int $level
     * @param int $parent_id
     * @param null $category_model
     * @param null $master_url_model
     * @param string $accessType
     * @return array
     */
    private function _buildTree($level = 2, $parent_id = 0, $category_model = null, $master_url_model = null, $accessType = "", $companyCode = 'ODI', $userAgent = '')
    {
        // level for categoryTree frontend, parent_id for categoryTreeCMS
        $category_tree = array();

        if(empty($category_model)) {
            $category_model = new \Models\ProductCategory();
        }

        if(empty($master_url_model)) {
            $master_url_model = new \Models\MasterUrlKey();
        }

        $queryParams = [];
        if($accessType == "cms"){
            if ($parent_id <= 0) {
                $queryParams = [
                    "conditions" => "parent_id = " . $parent_id ." AND level = ". $level ." AND company_code = '".$companyCode."' AND status > -1",
                    "order" => "position ASC"
                ];
            } else {
                $queryParams = [
                    "conditions" => "parent_id = " . $parent_id ." AND company_code = '".$companyCode."' AND status > -1",
                    "order" => "position ASC"
                ];
            }
        }
        else{
            // Now it also can handle access type other than cms
            // Build query condition
            $queryParams = [
                "conditions" => sprintf("company_code = '%s' AND status > 0", $companyCode),
                "order" => "position ASC"
            ];
            
            $mobileAppsUserAgent = ["ios", "android", "RuparupaApps"];
            $filterMap = [
                "parent_id" => $parent_id > 0,
                "show_on_app" => in_array($userAgent, $mobileAppsUserAgent),
                "include_in_menu" => true,
            ];

            // Adjust query filter based on certain access type
            switch ($accessType) {
                case "gift":
                    $filterMap["show_on_app"] = false;
                    $filterMap["include_in_menu"] = false;
                    break;
                // Another access type in the future
            }

            // Add query based on filter map
            if ($filterMap["parent_id"]) {
                $queryParams["conditions"] = sprintf("%s AND parent_id = %s", $queryParams["conditions"], $parent_id);
            } else {
                $queryParams["conditions"] = sprintf("%s AND level = %s", $queryParams["conditions"], $level);
            }

            if ($filterMap["show_on_app"]) {
                $queryParams["conditions"] = sprintf("%s AND show_on_app > 0", $queryParams["conditions"]);
            }

            if ($filterMap["include_in_menu"]) {
                $queryParams["conditions"] = sprintf("%s AND include_in_menu IN (1, 2)", $queryParams["conditions"]);
            }
        }

        $categoryList = $category_model::find($queryParams);

        /**
         * @var $category \Models\ProductCategory
         */
        foreach ($categoryList as $category)
        {
            $isRuleBased = false;
            if($category->getisRuleBased() == 1) {
                $isRuleBased = true;
                $urlRoute = RouteHelper::getRouteData($category->getCategoryId(), "custom_category", "", $companyCode);
            } else {
                $urlRoute = RouteHelper::getRouteData($category->getCategoryId(), "category", "", $companyCode);
            }

            if($accessType == "cms"){
                $category_tree[] = [
                    "category_id" => $category->getCategoryId(),
                    "parent_id" => $category->getParentId(),
                    "article_hierarchy" => $category->getArticleHierarchy(),
                    "name" => $category->getName(),
                    "path" => $category->getPath(),
                    "level" => $category->getLevel(),
                    "mini_category_banner" => $category->getMiniCategoryBanner(),
                    // "children" => $this->_buildTree($category->getLevel() + 1, $category->getCategoryId(), $category_model, null, $accessType, $companyCode),
                    "is_rule_based" => $isRuleBased
                ];
            }
            else{
                $category_tree[] = [
                    "category_id" => $category->getCategoryId(),
                    "name" => $category->getName(),
                    "image" => $category->getImage(),
                    "url_path_image" => $category->getUrlPathImage(),
                    "url_key" => $urlRoute['url_key'],
                    "icon" => $category->getIcon(),
                    "include_in_menu" => $category->getIncludeInMenu(),
                    "include_in_menu_start_time" => $category->getStartTime(),
                    "include_in_menu_end_time" => $category->getEndTime(),
                    "mini_category_banner" => $category->getMiniCategoryBanner(),
                    "children" => $this->_buildTree($category->getLevel() + 1,  $category->getCategoryId(), $category_model, null, $accessType, $companyCode, $userAgent),
                    "is_rule_based" => $isRuleBased
                ];
            }
        }

        return $category_tree;

    }

    public function categoryPath($article_hierarchy = FALSE){

        $categoryPath = array();

        $categoryModel = new \Models\ProductCategory();
        $categoryDetail = $categoryModel->findFirst(
            array(
                'conditions' => ' article_hierarchy = "'.$article_hierarchy.'"',
                'columns' => 'path'
            )
        );

        /**
         * If no any AH MC ( Level 4 ), then go to AH Class ( Level 3 )
         */
        if(!$categoryDetail) {
            $categoryDetail = $categoryModel->findFirst(
                array(
                    'conditions' => ' article_hierarchy = "'.substr($article_hierarchy,0,10).'"',
                    'columns' => 'path'
                )
            );
        }

        if($categoryDetail){
            $path = explode('/',$categoryDetail->toArray()['path']);

            /**
             * Set third level being default
             */
            $finalPath = array();
            $index = 1;
            foreach($path as $key => $row){
                if($key == 2){
                    $finalPath[0] = $row;
                }else{
                    $finalPath[$index] = $row;
                    $index++;
                }

            }
            ksort($finalPath);

            foreach($finalPath as $row){
                $categoryPathChild = array();
                $categoryDetail = $categoryModel->findFirst(
                    array(
                        'conditions' => ' category_id = "'.$row.'"',
                        'columns' => 'path'
                    )
                );

                if($categoryDetail){
                    $path = explode('/',$categoryDetail->toArray()['path']);
                    foreach($path as $row){
                        $categoryPathChild[]['category_id'] = $row;
                    }
                    array_push($categoryPath,$categoryPathChild);
                }

            }
        }

        return $categoryPath;
    }

    public function populateBestDeal($productId = "", $specialPriceStatus = "active"){
        if(empty($productId)){
            return false;
        }

        $product2CatModel = new Product2Category();

        $sql  = "DELETE FROM product_2_category WHERE product_id=".$productId;
        $sql .= " AND category_id=2983";
        $product2CatModel->useWriteConnection();
        $result = $product2CatModel->getDi()->getShared($product2CatModel->getConnection())->query($sql);

        if($specialPriceStatus == "active"){
            $sql  = "INSERT INTO product_2_category VALUES (".$productId.",2983,0,10000)";
            $product2CatModel->useWriteConnection();
            $result = $product2CatModel->getDi()->getShared($product2CatModel->getConnection())->query($sql);
        }

        return $result->numRows();

    }

    public function insertCategoryBatch($params = array()){
        $ctr = array();

        if(empty($params)){
            return false;
        }

        $products = "'".str_replace(',',"','",$params['products'])."'";

        // step 1 : if parent id is not number, we should search for actual id
        if (!is_numeric($params['parent_id'])) {
            $conditions = "name='" . $params['parent_id'] . "'";
        } else {
            $conditions = "category_id='" . $params['parent_id'] . "'";
        }

        $categoryModel = new ProductCategory();

        $sql = "SELECT category_id,url_path,path FROM product_category WHERE " . $conditions;
        $categoryModel->useReadOnlyConnection();
        $result = $categoryModel->getDi()->getShared($categoryModel->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $categoryResult = $result->fetchAll()[0];

        $params['parent_id'] = $categoryResult['category_id'];
        $params['parent_path_'] = $categoryResult['path'];
        $params['parent_url_path_'] = $categoryResult['url_path'];

        $params['category_key'] = preg_replace('#[^0-9a-z]+#i', '-', $params['name']);
        $params['category_key'] = strtolower($params['category_key']);

        $params['url_path'] = $params['parent_url_path_'] . '/' . $params['category_key'];
        $params['page_layout'] = 'one_column';

        $categoryModel->setFromArray($params);
        $saved = $categoryModel->saveData();

        if($saved){
            $ctr['saved_category'] = 1;

            $newCategoryId = $categoryModel->getCategoryId();

            $newPath = $params['parent_path_'] . '/' . $newCategoryId;

            $sql = "UPDATE product_category SET path='".$newPath."' WHERE category_id=".$newCategoryId;
            $categoryModel->useWriteConnection();
            $result = $categoryModel->getDi()->getShared($categoryModel->getConnection())->query($sql);

            // next insert to master_url_key
            $urlModel = new \Models\MasterUrlKey();

            $sql  = "INSERT INTO master_url_key (url_key, section, document_title, reference_id) ";
            $sql .= "VALUES (
                        '".$params['url_path']."',
                        'category',
                        'Jual ".$params['name']." | Ruparupa',
                        '".$newCategoryId."'
                        )";

            $urlModel->useWriteConnection();
            $result = $urlModel->getDi()->getShared($urlModel->getConnection())->query($sql);

            $ctr['inserted_master_url_key'] = $result->numRows();

            // insert product to product_2_category
            $productVariantModel = new \Models\ProductVariant();
            $variantResult = $productVariantModel->find(
                [
                    "columns" => "product_id",
                    "conditions" => "sku IN (".$products.")"
                ]
            );

            $productResult = $variantResult->toArray();

            foreach ($productResult as $key => $val){
                $values[] = '('.$val['product_id'].','.$newCategoryId.',0,10000)';
            }
            $sql  = "INSERT INTO product_2_category VALUES ".implode(',',$values);
            $productVariantModel->useWriteConnection();
            $result = $productVariantModel->getDi()->getShared($productVariantModel->getConnection())->query($sql);
            $ctr['inserted_product_2_category'] = $result->numRows();

            return $ctr;
        }
        else{
            return false;
        }
    }

    public function updateCategoryBatch($params = array()){
        $ctr = array();

        if(empty($params) || empty($params['category_id'])){
            return false;
        }

        $categoryModel = new ProductCategory();

        $sql = "UPDATE product_category SET
                image = '".$params['image']."',
                thumbnail = '".$params['thumbnail']."',
                url_path_image = '".$params['url_path_image']."',
                header_description = '".$params['header_description']."',
                header_description_mobile = '".$params['header_description_mobile']."',
                page_layout = ".intval($params['page_layout'])."
                WHERE category_id = '".$params['category_id']."'";

        $categoryModel->useWriteConnection();
        $result = $categoryModel->getDi()->getShared($categoryModel->getConnection())->query($sql);

        $categoryResult = $result->numRows();

        return $categoryResult;
    }

    public function getAutoHeroProduct() {
        $productCategoryVmModel = new \Models\ProductCategoryVm();
        $categoryVmResult = $productCategoryVmModel->find();
        if($categoryVmResult){
            $result = $categoryVmResult->toArray();
            foreach ($result as $key=>$row ) {
                $productCategoryModel = new \Models\ProductCategory();
                $productCategoryResult = $productCategoryModel->findFirst('category_id = '.$row['category_id']);
                if($productCategoryResult){
                    $result[$key] = array_merge($result[$key], $productCategoryResult->getDataArray(['company_code','vm_most_sales','vm_most_click','vm_add_to_cart','vm_slow_moving','vm_new_arrival','vm_special_price','name'], true));
                }
            }
            return $result;
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "No data has been found.";
        }
    }


    public function customCategoryDetail($params = array()) {
        if (empty($params)) {
            return false;
        }

        $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';
        $keyword = isset($params['keyword']) && !empty($params['keyword']) ? $params['keyword'] : '';

        $categoryModel = new ProductCategory();
        $sql = "SELECT pc.category_id, pc.url_path, pc.name, pc.is_rule_based, pc.company_code 
                FROM product_category pc 
                WHERE pc.company_code = '".$companyCode."' AND pc.status > 0 AND concat(',', pc.tags, ',') like '%,".$keyword."%,'
                ORDER BY pc.updated_at desc";
        $result = $categoryModel->getDi()->getShared('dbReader')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $categoryList = $result->fetchAll();

        if (!empty($categoryList)) {
            return $categoryList;
        } else {
            return false;
        }
    }

    public function deleteCustomCategoryRedis() {
        $redisField = 'category_id,url_path,name,is_rule_based,company_code';
        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisField."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }
}