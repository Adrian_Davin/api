<?php
/**
 * Library to send ruparupa order to SAP system
 */

namespace Library;

use Phalcon\Db as Database;

class CONSB2C
{
    protected $order_id;
    protected $order_date;
    protected $order_type = "B2C";
    protected $cust_name1;
    protected $cust_name2;
    protected $email;
    protected $phone;
    protected $address1;
    protected $address2;
    protected $address3;
    protected $city;
    protected $sales_id = "";
    protected $sales_office = "";
    protected $sales_grp = "";
    protected $kode_jalur;
    protected $country;
    protected $postal_code;
    protected $ship_cost;
    protected $grand_price;
    protected $original_price;
    protected $partial_ship;
    protected $remark;    
    protected $cust_no = "";
    protected $source = "E_COMMERCE_MAGENTO_B2C";
    protected $req_dlv_date;
    protected $ship_condition;
    protected $ship_cost_gc;
    protected $dp_value;
    protected $payer_name1 = "";
    protected $payer_name2 = "";
    protected $payer_addr1 = "";
    protected $payer_addr2 = "";
    protected $payer_addr3 = "";
    protected $payer_post_code = "";
    protected $payer_city = "";
    protected $payer_province = "";
    protected $payer_npwp = "";
    protected $payer_email = "";
    protected $bill_block;
    protected $dlv_block;    
    protected $employee_reff;
    protected $invoice_id;
    protected $site;
    protected $is_refund = 0;

    protected $company_code = 'ODI';

    /**
     * @var \Models\SalesInvoice
     */
    protected $invoice;

    protected $creditMemo = array();
    protected $creditMemoInvoice = array();

    protected $parameter= array();

    public function __construct($invoice = null, $creditMemo = null)
    {
        if(!empty($invoice)) {
            $this->invoice = $invoice;
            
            if(!empty($creditMemo)) {
                $this->creditMemo = $creditMemo;
                $this->is_refund = 1;
    
                $invoiceNo = $invoice->getInvoiceNo();
                
                if (!empty($creditMemo['invoices'])) {
                    foreach ($creditMemo['invoices'] as $crmInvoice) {
                        if ($crmInvoice['invoice_no'] == $invoiceNo) {
                            $this->creditMemoInvoice = $crmInvoice;
                        }
                    }
                }
            }
        }
    }

    private function findCRMItem($sku) {
        if (!empty($this->creditMemoInvoice['credit_memo_items'])) {
            foreach ($this->creditMemoInvoice['credit_memo_items'] as $crmItem) {
                if ($crmItem['sku'] == $sku) {
                    return $crmItem;
                }
            }
        }

        return null;
    }

    /**
     * @param array $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    public function getOrderType()
    {
        return $this->order_type;
    }

    public function prepareDCParamsCONSB2C()
    {
        $companyModel = New \Models\CompanyProfileDC();

        if(empty($this->invoice)) {
            return false;
        }

        /**
         * @var $currentOrder \Models\SalesOrder
         */
        $currentOrder = $this->invoice->SalesOrder;

        /**
         * @var $shippingAddress \Models\SalesOrderAddress
         */
        $shippingAddress = null;

       
       
        

        if(!empty($this->invoice->SalesShipment)) {
            $shippingAddress = $this->invoice->SalesShipment->SalesOrderAddress->getDataArray();
            $carrier_id = $this->invoice->SalesShipment->getCarrierId();
            
            $sql = "SELECT carrier_name
                        from shipping_carrier 
                        where carrier_id = ".$carrier_id;
            $result = $companyModel->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(Database::FETCH_ASSOC);
            $carrierResult = $result->fetchAll()[0];
            $carrier_name = $carrierResult['carrier_name'];            
        }

        /*
         * Preorder if item not preorder remove prefix pre from order no
         */
        $invoiceDetail = !empty($this->invoice->toArray())? $this->invoice->toArray() : array();
        if(substr( $invoiceDetail['invoice_no'], 0, 3 ) === "PRE"){
            $orderId = $currentOrder->getOrderNo();
        }else{
            $orderId = str_replace("PRE","",$currentOrder->getOrderNo());
        }

        // appending invoiceId to maintain uniqueness in case of 1 order multiple DC
        // switch odi and invoice no, to handle multiple DC 
        // for cust_name1 = add carrier name
        $this->order_id =  $orderId . '_' .  substr($this->invoice->getInvoiceNo(),strlen($this->invoice->getInvoiceNo())-3, 3);
        $this->order_date = date('Ymd',strtotime($this->invoice->getCreatedAt()));
    
        $this->cust_name1 =  $shippingAddress['first_name'];
        $this->cust_name2 =  $shippingAddress['last_name']." - ".$carrier_name;
        $this->email = $currentOrder->SalesCustomer->getCustomerEmail();
        $this->phone = !empty($shippingAddress['phone']) ? $shippingAddress['phone'] : "";
        $this->address1 = substr($shippingAddress['full_address'],0,35);
        $this->address2 = ($addr2 = substr($shippingAddress['full_address'],35,35)) != false ? $addr2 : "";
        $this->address3 = ($addr3 = substr($shippingAddress['full_address'],70,35)) != false ? $addr3 : "";
        $this->city = $shippingAddress['kecamatan']['kecamatan_code'];
        
        $this->kode_jalur = (isset($shippingAddress['kecamatan']['kode_jalur'])) ? $shippingAddress['kecamatan']['kode_jalur'] : '';
        $this->country  = $shippingAddress['country']['country_code'];
        $this->postal_code  = !empty($shippingAddress['post_code']) ? $shippingAddress['post_code'] : "00000";

        $this->ship_cost = number_format( ($this->invoice->getShippingAmount() - $this->invoice->getShippingDiscountAmount()) ,0, '.', '');
        
        $grandPrice = $this->invoice->getGiftCardsAmount() + $this->invoice->getGrandTotal();
        $this->grand_price = number_format($grandPrice,0, '.', '');
        $this->partial_ship = '';

        /**
         * Send AWB as Shipping Remark to SAP
         */
        $this->remark = '';
        if(!empty($this->invoice->SalesShipment)) {
            //foreach($this->invoice->SalesShipment as $rowObjShipment) {
            //    $carrierCode = !empty($rowObjShipment->ShippingCarrier->getCarrierCode()) ? $rowObjShipment->ShippingCarrier->getCarrierCode() : "";
            //    $this->remark = $carrierCode . $rowObjShipment->getTrackNumber();
            //}
            $carrierCode = !empty($this->invoice->SalesShipment->ShippingCarrier->getCarrierCode()) ? $this->invoice->SalesShipment->ShippingCarrier->getCarrierCode() : "";
            $this->remark = $carrierCode . $this->invoice->SalesShipment->getTrackNumber();
        }
        $this->cust_no = '';
        

        // set request delivery date different per company
        $additionalRdd = getenv('ADDITIONAL_RDD_' . $currentOrder->getCompanyCode() ? $currentOrder->getCompanyCode() : 'ODI');
        if (intval($additionalRdd) > 0) {
            $this->req_dlv_date = date('Ymd', strtotime('+'.intval($additionalRdd).' days', strtotime($this->invoice->getCreatedAt())));
        } else {
            $this->req_dlv_date = date('Ymd', strtotime($this->invoice->getCreatedAt()));
        }

        $this->ship_condition = "Z6";
        $this->ship_cost_gc = '';
        $this->dp_value = '';   

        /**
         * @todo : Implement payer here
         */
        $this->payer_name1 = "";
        $this->payer_name2 = "";
        $this->payer_addr1 = "";
        $this->payer_addr2 = "";
        $this->payer_addr3 = "";
        $this->payer_post_code = "";
        $this->payer_city = "";
        $this->payer_province = "";
        $this->payer_npwp = "";
        $this->payer_email = "";
        $this->bill_block = "";
        $this->dlv_block = "";
        $this->employee_reff = "";

        // Genereate items
        $this->invoiceItems = $this->invoice->SalesInvoiceItem;
        
        
        $this->invoice_id = $this->invoice->getInvoiceNo();


        $store_code = $this->invoice->getStoreCode();
        if (substr($store_code, 0, 4) == "1000") {
            $store_code = substr($store_code,4);
        }
        // get the company profile by store_code
        $companyProfile = $companyModel->getCompanyByStoreCode($store_code);
        
        if (substr($store_code,0,2) == 'DC' && strlen($store_code) > 2) {
            $store_code = substr($store_code,2);
        }
        $this->site = $store_code;

        
        if(!empty($companyProfile)){
        $this->source = "E_COMMERCE_".$companyProfile["company_code"]."_B2C";
        $this->order_type = "B2C_".$companyProfile["company_code"];
        $this->sales_id = $companyProfile["salesman_code"];
        $this->sales_office = $companyProfile["sales_office"];
        $this->sales_grp = $companyProfile["sales_group"];
        $this->company_code = $companyProfile["company_code"];
        } else {
            $this->order_type = "B2C_ODI";
            $this->sales_id = "";
            $this->sales_office = "";
            $this->sales_grp = "";
            $this->source = "E_COMMERCE_ODI_B2C";
            $this->company_code ="";
        }

        if (!empty($this->creditMemo)) {
            $this->order_id .= "_".$this->creditMemo['credit_memo_count'];
        }
    }

    public function generateDCParamsCONSB2C()
    {
        $productDetail = $this->generateDCProductDetailCONSB2C();
        $thisArray = get_object_vars($this);
        $b2c_order_detail = array();
        $underscoreParams = array('sales_grp','employee_reff');

        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if($key == "company_code") {
                continue;
            }
            if(is_scalar($val)) {
                if(in_array($key,$underscoreParams)){
                    $currentKey = strtoupper($key);
                }else{
                    $currentKey = strtoupper(str_replace("_", "", $key));
                }                
                $b2c_order_detail[$currentKey] = $val;
            }
        }

        $parameter = array(
            "b2c_order_detail" => $b2c_order_detail,
            "b2c_product_detail" => $productDetail,
            'company_code' => $this->company_code
        );

        $parameter = array("Order_Data_B2C" => $parameter);
        $this->parameter = $parameter;
    }

    public function generateDCProductDetailCONSB2C()
    {
        $productDetail = array();

        $totalOriginalPrice = 0;
        $totalPrice = 0;

        $idProductDetail = 0;
        $invoiceModel = new \Models\SalesInvoice();
        if(!empty($this->invoiceItems)) {
            /**
             * @var $item \Models\SalesInvoiceItem
             */
            foreach($this->invoiceItems as $item) {
                preg_match_all('/([a-zA-Z0-9]+)SET([a-zA-Z0-9]+)/', $item->getSku(), $match);
                if (count($match) >= 3 && isset($match[1][0]) && isset($match[2][0])) {
                    $itemSku = $match[1][0];
                    $qtySet = $match[2][0];
                } else {
                    preg_match_all('/([a-zA-Z0-9]+)-PRE/', strtoupper($item->getSku()), $match);
                    if (count($match) >= 2 && isset($match[1][0])) {
                        $itemSku = $match[1][0];
                    } else {
                        $itemSku = $item->getSku();
                    }
                    $qtySet = 1;
                }
                
                $crmItem = array();
                
                if (!empty($this->creditMemo)) {
                    $crmItem = $this->findCRMItem($itemSku);
                    
                    if (empty($crmItem)) {
                        continue;
                    }
                }
                
                $quantity = intval(((empty($crmItem)) ? $item->getQtyOrdered() : $crmItem['qty_refunded']));

                $qtyOrdered = $quantity * $qtySet;   
                
                $store_code = $item->getStoreCode();
                $productSite = $store_code;
                if (substr($store_code, 0, 4) == "1000") {
                    $store_code = substr($store_code,4);
                }
                if (substr($store_code,0,2) == 'DC' && strlen($store_code) > 2) {
                    $productSite = substr($store_code,2);
                }

                $sql = "SELECT coalesce(sum(voucher_amount_difference),0) as selisih, 
                               coalesce(sum(split_voucher_amount_difference),0) as selisih_split_voucher,
                               coalesce(sum(split_discount_amount_difference),0) as selisih_split_discount
                        from sales_order_item_giftcards 
                        where sales_order_item_id = ".$item->getSalesOrderItemId();
                $result = $invoiceModel->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(Database::FETCH_ASSOC);                
                $selisihResult = $result->fetchAll()[0];
                $selisih = $selisihResult['selisih'];
                $selisihSplitVoucher = $selisihResult['selisih_split_voucher'];
                $selisihSplitDiscount = $selisihResult['selisih_split_discount'];
                $selisihSplitDiscountByQty = $selisihSplitDiscount / $qtyOrdered;

                $idProductDetail++;

                $finalUnitPrice = (int)$item->getSellingPrice() - (int)$item->getDiscountAmount();
                $unitPrice = (($finalUnitPrice <= 0) ? 1 : $finalUnitPrice) / $qtySet;
                $salesPrice = ($unitPrice * $qtyOrdered) + $selisihSplitDiscount;
                $totalPrice += $salesPrice;
                
                $salesOrderItem = $item->SalesOrderItem;
                $originalUnitPrice = (int)$salesOrderItem->getNormalPrice();
                $originalPrice = $originalUnitPrice  * $quantity;
                $totalOriginalPrice += $originalPrice;

                $discount = round((((float)$originalPrice - (float)$salesPrice) / (float)$originalPrice) * 100.0, 0);

                $salesUom = \Helpers\ProductHelper::getSalesUom($itemSku);
                $productDetail[] = array(
                    "ID" => $idProductDetail,
                    "PRODUCTNO" => $itemSku,                    
                    "SALESPRICE" => intval($unitPrice + $selisihSplitDiscountByQty),
                    "QUANTITY" => (empty($crmItem)) ? $qtyOrdered : -1 * $qtyOrdered,
                    "UNIT" => $salesUom,
                    "CURRENCY" => "IDR", // For now we only using IDR for transaction
                    "STGE_LOC" => "1021",
                    "SITE" => $productSite,
                    "SITE_VENDOR" => "",
                    "CONDTYPE" => "",
                    "INDENT" => "",
                    "VENDOR" => "",
                    "CONFIRMNO" => "",
                    "SERIALNUMBER" => "",
                    "ORIGINALPRICE" => $originalUnitPrice,
                    "DISCOUNT" => number_format($discount,0, '.', ''),
                );

                // if(!empty((int)$this->invoice->SalesOrder->getGiftCardsAmount())) {
                //     $unitPriceGc = $item->getGiftCardsAmount() / $qtySet;

                //     // zvo4 item price can't be higher than item unit price
                //     if($unitPriceGc>$unitPrice){
                //         $unitPriceGc = $unitPrice;
                //     }
                //     $salesProceGc = ($unitPriceGc * $qtyOrdered) + $selisih + $selisihSplitVoucher;

                //     $productDetail[] = array(
                //         "ID" => $idProductDetail,
                //         "PRODUCTNO" => $itemSku,                    
                //         "SALESPRICE" => intval($salesProceGc),
                //         "QUANTITY" => $qtyOrdered,
                //         "UNIT" => $salesUom,
                //         "CURRENCY" => "IDR", // For now we only using IDR for transaction
                //         "STGE_LOC" => "1021",
                //         "SITE" => $productSite,
                //         "SITE_VENDOR" => "",
                //         "CONDTYPE" => "ZV04",
                //         "INDENT" => "",
                //         "VENDOR" => "",
                //         "CONFIRMNO" => "",
                //         "SERIALNUMBER" => ""  
                //     );

                // }
            }
        }

        $this->original_price = number_format($totalOriginalPrice,0, '.', '');

        if (!empty($this->creditMemo)) {
            $shippingAmount = 0.0;

            $this->ship_cost = number_format($shippingAmount, 0, '.', '');

            $grandPrice = $totalPrice + $shippingAmount;
            $this->grand_price = number_format($grandPrice,0, '.', '');
        }

        return $productDetail;
    }

    public function createCONSB2C()
    {
        $nsq = new \Library\Nsq();
        $orderInfo = $this->parameter["Order_Data_B2C"]["b2c_order_detail"]["ORDERID"];
        $message = [
            "data" => json_encode($this->parameter),
            "message" => "calculateCONSB2C",
            "invoice_no" => $this->invoice->getInvoiceNo(),
            "order_no" => explode('_', $orderInfo)[0],
            "order_id" => $orderInfo
        ];
        $nsq->publishCluster('transactionSap', $message);

        return true;
    }
}