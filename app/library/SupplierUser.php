<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Library;

class SupplierUser
{

    /**
     * @var $customer \Models\Customer
     */
    protected $supplierUser;

    /**
     * @var $customerToken \Models\CustomerToken
     */
    protected $supplierUserToken;

    protected $errorCode;
    protected $errorMessages;

    protected $supplier_user_id;

    public function __construct()
    {
        $this->supplierUser = new \Models\SupplierUser();
        //$this->supplierUserToken = new \Models\CustomerToken();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @return \Models\Customer
     */
    public function getSupplierUser()
    {
        return $this->supplierUser;
    }

    /**
     * @param \Models\Customer $customer
     */
    public function setSupplierUser($supplierUser)
    {
        $this->supplierUser = $supplierUser;
    }

    public function setSupplierUserId($supplier_user_id){
        $this->supplier_user_id = $supplier_user_id;
    }

    public function getSupplierUserId(){
        return $this->supplier_user_id;
    }

    public function setSupplierUserFromArray($data_array = array())
    {
        $this->supplierUser->setFromArray($data_array);

        if(!empty($data_array['token'])) {
            $this->customerToken->setFromArray($data_array);
        }
    }

    public function saveData($sending_welcome_email = true)
    {
        /*
         * check if consumer sending customer ID, if sent we just need to updated it
         */
        $customerResult = null;

        if(empty($this->supplierUser->getFirstName())){
            $this->errorMessages[] = "First Name required";
        }

        if(empty($this->supplierUser->getSupplierId())){
            $this->errorMessages[] = "Supplier required";
        }

        if(empty($this->supplierUser->getEmail())){
            $this->errorMessages[] = "Email required";

        }

        if(empty($this->supplierUser->getSupplierUserId())) {
            // First we need to check if email already exist or not
            $this->supplierUser->setPassword(md5($this->supplierUser->getPassword()));
            $param = array(
                "conditions" => "email = '".$this->supplierUser->getEmail()."'"
            );
            $supplierUserResult = $this->supplierUser->findFirst($param);
            if($supplierUserResult){
                $this->errorMessages[] = "Email already Exist";
            }
        }else{
            $param = array(
                "conditions" => "supplier_user_id = '".$this->supplierUser->getSupplierUserId()."'"
            );
            $supplierUserResult = $this->supplierUser->findFirst($param)->toArray();
            if(md5($this->supplierUser->getPassword()) == $supplierUserResult['password'] || $this->supplierUser->getPassword() == ''){
                $this->supplierUser->setPassword($supplierUserResult['password']);
                $this->supplierUser->setIsNeedVerify($supplierUserResult['is_need_verify']);
            }else{
                $this->supplierUser->setPassword(md5($this->supplierUser->getPassword()));
                $this->supplierUser->setLastChangePassword(date('Y-m-d H:i:s'));
            }

            $this->supplierUser->setCreatedAt($supplierUserResult['created_at']);
        }

        if(count($this->errorMessages)==0) {
            if (!$this->supplierUser->saveUser()) {
                $this->errorCode = $this->supplierUser->getErrorCode();
                $this->errorCode = $this->supplierUser->getMessages();
                return false;
            } else if ($sending_welcome_email) {
                $this->setSupplierUserId($this->supplierUser->getSupplierUserId());
                // Sending welcome email
                /*
                $email = new \Library\Email();
                $email->setEmailSender();
                $email->setName($this->customer->getLastName(),$this->customer->getFirstName());
                $email->setEmailReceiver($this->customer->getEmail());
                $email->setEmailTag("customer_register");
                Done use env use table master_email_template $email->setTemplateId($_ENV['TEMPLATE_CUSTOMER_REGISTER']);
                $email->send();
                */
            }
        }else{

            $this->errorCode = "RR100";
            return array();
        }

        return true;
    }
}