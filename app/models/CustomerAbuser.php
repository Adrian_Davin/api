<?php

namespace Models;

// use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
// use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
// use Phalcon\Db as Database;

class CustomerAbuser extends \Models\BaseModel
{
    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    public function getAllListAbusers(){
        $sql = "
            SELECT DISTINCT c.customer_id, concat(c.first_name,' ',c.last_name) as name, c.email, c.phone, ca.group, c.company_code, 		      		
                    (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=4) as member_ahi,
                    (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=5) as member_hci,
                    (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=6) as member_tgi
            FROM customer c
            INNER JOIN customer_2_group cg ON c.customer_id = cg.customer_id
            INNER JOIN customer_abuser_group ca ON c.customer_id = ca.customer_id
            ORDER BY ca.group ASC;
        ";

        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $abusers = $result->fetchAll();

        return $abusers;
    }

    public function getListAbusersByKeyword($email = "", $phone = "", $name="", $customerId = 0, $group = 0){

        if ($customerId > 0) {
            return $this->getSingleAbuser($customerId);
        }

        
        $sql = 'SELECT ca.* from customer_abuser_group ca 
                INNER JOIN customer c ON c.customer_id = ca.customer_id';

        if($email <> "" || $phone <> "" || $name <> "" ||  $group > 0){
            // $sql = '
            //     SELECT ca.* from customer_abuser_group ca 
            //     INNER JOIN customer c ON c.customer_id = ca.customer_id
            //     WHERE c.email like "'.$email.'";
            // ';

            $sql .= ' WHERE ';
            $hasCondition = false;


            if ($group > 0) {
                $sql .= $hasCondition ? ' AND ' : '';
                $sql .= 'ca.group = ' . $group;
                $hasCondition = true;
            }

            if ($email <> "") {
                $sql .= $hasCondition ? ' AND ' : '';
                $sql .= 'upper(c.email) like upper("%'.$email.'%")';
                $hasCondition = true;
            } 

            if ($phone <> "") {
                $sql .= $hasCondition ? ' AND ' : '';
                $sql .= 'c.phone like "%'.$phone.'%"';
                $hasCondition = true;
            }

            if ($name <> "") {
                $sql .= $hasCondition ? ' AND ' : '';
                $sql .= 'upper(concat(c.first_name, " ", c.last_name)) like upper("%'.$name.'%")';
                $hasCondition = true;
            }
            

        }
        $sql .= ';';


        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $group = $result->fetchAll();


        $sql = "
            SELECT DISTINCT c.customer_id, concat(c.first_name,' ',c.last_name) as name, c.email, c.phone, ca.group, ca.reason, c.company_code,
                    (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=4) as member_ahi,
                    (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=5) as member_hci,
                    (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=6) as member_tgi
            FROM customer c
            INNER JOIN customer_2_group cg ON c.customer_id = cg.customer_id
            INNER JOIN customer_abuser_group ca ON c.customer_id = ca.customer_id
            WHERE ca.group = ".$group[0]['group']."
            ORDER BY ca.group ASC;
        ";

        
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $abusers = $result->fetchAll();

        return $abusers;
    }

    public function getSingleAbuser($customerId = 0) {
        $sql = "
                SELECT c.customer_id, concat(c.first_name,' ',c.last_name) as name, c.email, c.phone, ca.group, ca.reason, c.company_code,
                        (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=4) as member_ahi,
                        (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=5) as member_hci,
                        (SELECT member_number FROM customer_2_group WHERE customer_id=cg.customer_id AND group_id=6) as member_tgi
                FROM customer c
                INNER JOIN customer_2_group cg ON c.customer_id = cg.customer_id
                INNER JOIN customer_abuser_group ca ON c.customer_id = ca.customer_id
                WHERE ca.customer_id = ".$customerId."
                limit 1;
            ";
        
            $result = $this->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $abusers = $result->fetchAll();
    
            return $abusers;
    }

    public function getGroupAbuser($customerId = ''){
        $sql = "
            SELECT ca.customer_id, ca.group
            FROM customer_abuser_group ca
            WHERE ca.customer_id = ".$customerId.";
        ";

        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $abusers = $result->fetchAll();

        return $abusers;
    } 
}