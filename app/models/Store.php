<?php

namespace Models;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

use Phalcon\Db;

class Store extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $store_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $pickup_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $supplier_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $supplier_address_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $brand_store_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $name;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $type;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected $store_code;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=true)
     */
    protected $email_manager;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=true)
     */
    protected $email_admin;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $phone;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $zone_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $default_carrier_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $fulfillment_center;

    protected $pickup_center;

    protected $virtual_pos;
    protected $order_auto_received;
    protected $inactive_carrier_id;
    protected $request_stock;
    protected $check_item;

    protected $is_able_gift;

    protected $open_time;
    protected $close_time;

    protected $storeCache;

    /**
     * Get the value of fulfillment_center
     */
    public function getFulfillment_center()
    {
        return $this->fulfillment_center;
    }

    /**
     * Set the value of fulfillment_center
     *
     * @return  self
     */
    public function setFulfillment_center($fulfillment_center)
    {
        $this->fulfillment_center = $fulfillment_center;

        return $this;
    }

    /**
     * Get the value of pickup_center
     */
    public function getPickup_center()
    {
        return $this->pickup_center;
    }

    /**
     * Set the value of pickup_center
     *
     * @return  self
     */
    public function setPickup_center($pickup_center)
    {
        $this->pickup_center = $pickup_center;

        return $this;
    }

    /**
     * Get the value of virtual_pos
     */
    public function getVirtualPos()
    {
        return $this->virtual_pos;
    }

    /**
     * Set the value of virtual_pos
     *
     * @return  self
     */
    public function setVirtualPos($virtual_pos)
    {
        $this->virtual_pos = $virtual_pos;
    }

    /**
     * Get the value of order_auto_received
     */
    public function getOrderAutoReceived()
    {
        return $this->order_auto_received;
    }

    /**
     * Set the value of order_auto_received
     *
     * @return  self
     */
    public function setOrderAutoReceived($order_auto_received)
    {
        $this->order_auto_received = $order_auto_received;
    }

    /**
     * Get the value of inactive_carrier_id
     */
    public function getInactiveCarrierId()
    {
        return $this->inactive_carrier_id;
    }

    /**
     * Set the value of inactive_carrier_id
     *
     * @return  self
     */
    public function setInactiveCarrierId($inactive_carrier_id)
    {
        $this->inactive_carrier_id = $inactive_carrier_id;
    }


    /**
     * Get the value of request_stock
     */
    public function getRequestStock()
    {
        return $this->request_stock;
    }

    /**
     * @param int $request_stock
     */
    public function setRequestStock($request_stock)
    {
        $this->request_stock = $request_stock;
    }

        /**
     * Get the value of check_item
     */
    public function getCheckItem()
    {
        return $this->check_item;
    }

    /**
     * @param int $check_item
     */
    public function setCheckItem($check_item)
    {
        $this->check_item = $check_item;
    }

    /**
     * @return int
     */
    public function getIsAbleGift()
    {
        return $this->is_able_gift;
    }

    /**
     * @param int $is_able_gift
     */
    public function setIsAbleGift($is_able_gift)
    {
        $this->is_able_gift = $is_able_gift;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('store_id', 'Models\ProductStock', 'store_id', array('alias' => 'ProductStock'));
        $this->belongsTo('pickup_id', 'Models\PickupPoint', 'pickup_id', array('alias' => 'PickupPoint', "reusable" => true));
        $this->belongsTo('supplier_id', 'Models\Supplier', 'supplier_id', array('alias' => 'Supplier', "reusable" => true));
        $this->belongsTo('supplier_address_id', 'Models\SupplierAddress', 'supplier_address_id', array('alias' => 'SupplierAddress', "reusable" => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'store';
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->store_id;
    }

    /**
     * @param int $store_id
     */
    public function setStoreId($store_id)
    {
        $this->store_id = $store_id;
    }

    /**
     * @return int
     */
    public function getPickupId()
    {
        return $this->pickup_id;
    }

    /**
     * @param int $pickup_id
     */
    public function setPickupId($pickup_id)
    {
        $this->pickup_id = $pickup_id;
    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param int $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return int
     */
    public function getSupplierAddressId()
    {
        return $this->supplier_address_id;
    }

    /**
     * @return int
     */
    public function getBrandStoreId()
    {
        return $this->brand_store_id;
    }

    /**
     * @param int $brand_store_id
     */
    public function setBrandStoreId($brand_store_id)
    {
        $this->brand_store_id = $brand_store_id;
    }

    /**
     * @param int $supplier_address_id
     */
    public function setSupplierAddressId($supplier_address_id)
    {
        $this->supplier_address_id = $supplier_address_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->store_code;
    }

    /**
     * @param string $store_code
     */
    public function setStoreCode($store_code)
    {
        $this->store_code = $store_code;
    }

    /**
     * @return string
     */
    public function getEmailManager()
    {
        return $this->email_manager;
    }

    /**
     * @param string $email_manager
     */
    public function setEmailManager($email_manager)
    {
        $this->email_manager = $email_manager;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getZoneId()
    {
        return $this->zone_id;
    }

    /**
     * @param int $zone_id
     */
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
    }

    /**
     * @return int
     */
    public function getDefaultCarrierId()
    {
        return $this->default_carrier_id;
    }

    /**
     * @param int $default_carrier_id
     */
    public function setDefaultCarrierId($default_carrier_id)
    {
        $this->default_carrier_id = $default_carrier_id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


    /**
     * Get the value of email_admin
     *
     * @return  string
     */
    public function getEmail_admin()
    {
        return $this->email_admin;
    }

    /**
     * Set the value of email_admin
     *
     * @param  string  $email_admin
     *
     * @return  self
     */
    public function setEmail_admin(string $email_admin)
    {
        $this->email_admin = $email_admin;

        return $this;
    }

    public function getOpenTime()
    {
        return $this->open_time;
    }

    public function setOpenTime($open_time)
    {
        $this->open_time = $open_time;

        return $this;
    }

    public function getCloseTime()
    {
        return $this->close_time;
    }

    public function setCloseTime($close_time)
    {
        $this->close_time = $close_time;

        return $this;
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getNearestStore($params = array()){
        $latitude = $params['latitude'];
        $longitude = $params['longitude'];

        $sql = " SELECT * FROM ( SELECT `pickup_name`, `address_line_2`, `address_line_1`, `email_admin`, SUBSTRING_INDEX(`geolocation`, ',', 1) as `latitude`, SUBSTRING_INDEX(`geolocation`, ',', -1) as `longitude`, `phone`, 6371 * ACOS(COS(RADIANS('".$latitude."')) ";
        $sql .= " * COS(RADIANS(SUBSTRING_INDEX(`geolocation`, ',', 1))) * COS(RADIANS('".$longitude."') - RADIANS(SUBSTRING_INDEX(`geolocation`, ',', -1))) ";
        $sql .= " + SIN(RADIANS('".$latitude."')) * SIN(RADIANS(SUBSTRING_INDEX(`geolocation`, ',', 1)))) AS `distance` FROM `store` inner join `pickup_point` on ";
        $sql .= " `store`.`pickup_id` = `pickup_point`.`pickup_id` and `store`.`status` = 10 WHERE SUBSTRING_INDEX(`geolocation`, ',', 1) BETWEEN '".$latitude."' - (20 / 69) AND '".$latitude."' + (20 / 69) ";
        $sql .= " AND SUBSTRING_INDEX(`geolocation`, ',', -1) BETWEEN '".$longitude."' - (20 / (69 * COS(RADIANS('".$latitude."')))) AND '".$longitude."' + (20 / (69* COS ";
        $sql .= " (RADIANS('".$latitude."')))))as store WHERE `distance` < 20 ORDER BY `distance` ASC limit 5";

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        $result->setFetchMode(
            Db::FETCH_ASSOC
        );
        $result = $result->fetchAll();

        return $result;
    }

    public function saveStore(){
        try {

            $checkStore = parent::findFirst(array(
                "conditions" => "supplier_id = ".$this->supplier_id,
            ));

            if(!empty($checkStore->store_id)) {
                $this->store_id = $checkStore->store_id;
            }

            $saveStatus = $this->saveData("store");
            $action = $this->action;
            if(empty(trim($this->store_id))){
                $lastSupplier = parent::findFirst(array(
                    "columns" => "store_id",
                    "order"=> "store_id DESC"
                ));

                $this->store_id   = $lastSupplier->store_id;
            }

            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("Supplier", "Supplier Account save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Save/Update Supplier Account failed";

                $transaction->rollback(
                    "Save/Update Supplier Account failed"
                );
                return false;
            }
            $transaction->commit();
            return $this->store_id;
        } catch (TxFailed $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::find($parameters);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function findX($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

    /*
     * Override base model function to set from cache
     */
    public function afterUpdate()
    {
        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);
        $redisKey = "Store";
        $this->scanIterator($redisCon, $redisKey);

        // golang can't read phalcon redis, and shipping assignation using golang want to use same redis
        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['REDIS_HOST'],$_ENV['REDIS_PORT'],$_ENV['REDIS_DB']);
        if (!empty($this->getStoreCode()) && !empty($this->getStoreCached())) {
            $redisKey = sprintf("store:%s", $this->getStoreCode());
            $redisCon->set($redisKey, json_encode($this->getStoreCached()));
        }
    }

    private function scanIterator ($redisCon, $redisKey) {
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");
            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }

    public function getStoreCache($params = array())
    {
        // Ensure store_code is provided
        if (empty($params['store_code'])) {
            return null;
        }
        $data = null;
            $sql = "
            SELECT
                s.store_id,
                s.store_code,
                s.pickup_id,
                pp.pickup_name,
                pp.pickup_code,
                pp.address_line_1,
                pp.address_line_2,
                s.brand_store_id,
                mbs.logo,
                s.name,
                s.email_manager,
                s.email_admin,
                pp.email_pic,
                s.phone,
                s.supplier_id,
                s.supplier_address_id,
                su.supplier_alias,
                s.status,
                mk.kecamatan_id,
                pp.kecamatan_code,
                mk.kecamatan_name,
                pp.city_id,
                mc.city_name,
                mp.province_id,
                pp.province_code,
                mp.province_name,
                s.type,
                s.zone_id,
                s.default_carrier_id,
                s.fulfillment_center,
                s.pickup_center,
                pp.is_ownfleet,
                pp.is_express_courier,
                su.mp_flag_shipment,
                pp.geolocation,
                s.virtual_pos,
                s.inactive_carrier_id,
                s.return_any_store,
                s.is_able_gift,
                pp.kelurahan_id,
                mbs.brand_store_name,
                mp.island_name,
                s.order_auto_received
            FROM
                store s
            LEFT JOIN master_brand_store mbs ON
                s.brand_store_id = mbs.brand_store_id
            JOIN supplier su ON
                s.supplier_id = su.supplier_id
            JOIN pickup_point pp ON
                pp.pickup_id = s.pickup_id
            JOIN master_kecamatan mk ON
                mk.kecamatan_id = pp.kecamatan_code
            JOIN master_city mc ON
                mc.city_id = mk.city_id
            JOIN master_province mp ON
                mp.province_id = mc.province_id
            WHERE
                s.store_code = '{$params['store_code']}';";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Db::FETCH_ASSOC);
                $data = $result->fetch();                
            }

        $data['store_id'] = (int)$data['store_id'];
        $data['pickup_id'] = (int)$data['pickup_id'];
        $data['brand_store_id'] = (int)$data['brand_store_id'];
        $data['supplier_id'] = (int)$data['supplier_id'];
        $data['supplier_address_id'] = (int)$data['supplier_address_id'];
        $data['fulfillment_center'] = (int)$data['fulfillment_center'];
        $data['pickup_center'] = (int)$data['pickup_center'];
        $data['virtual_pos'] = (int)$data['virtual_pos'];
        $data['status'] = (int)$data['status'];
        $data['kecamatan_id'] = (int)$data['kecamatan_id'];
        $data['kecamatan_code'] = (int)$data['kecamatan_code'];
        $data['city_id'] = (int)$data['city_id'];
        $data['province_id'] = (int)$data['province_id'];
        $data['type'] = (int)$data['type'];
        $data['zone_id'] = (int)$data['zone_id'];
        $data['default_carrier_id'] = (int)$data['default_carrier_id'];
        $data['is_ownfleet'] = (int)$data['is_ownfleet'];
        $data['is_express_courier'] = (int)$data['is_express_courier'];
        $data['return_any_store'] = (int)$data['return_any_store'];
        $data['kelurahan_id'] = (int)$data['kelurahan_id'];
        $data['is_able_gift'] = (int)$data['is_able_gift'];
        
        return $data;
    }

    /**
     * Get the value of store_cache
     */
    public function getStoreCached()
    {
        return $this->storeCache;
    }

    /**
     * Set the value of store_cache
     *
     * @return  self
     */
    public function setStoreCached($storeCache)
    {
        $this->storeCache = $storeCache;
    }
}
