<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Items\CreditMemo;

class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\SalesCreditMemoItem
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @param array $columns
     * @param bool $showEmpty
     * @return array
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->getDataArray($columns, $showEmpty);
            $view[$idx] = $itemArray;
            $this->next();
        }

        return $view;
    }

    public function setSalesCreditMemoId($credit_memo_id = 0)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setCreditMemoId($credit_memo_id);
            $this->next();
        }
        return $view;
    }

    /**
     * @param null $transaction \Phalcon\Mvc\Model\Transaction Object
     * @return bool
     * @throws \Exception
     */
    public function saveData($transaction = null)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();

            if($transaction) {
                $item->setTransaction($transaction);
            }

            try {
                $item->saveData("credit_memo");
            } catch (\Library\HTTPException $e) {
                throw new \Exception($e->devMessage);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            $this->next();
        }
        return true;
    }

}