<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Items\SalesOrder;

use \Helpers\LogHelper;
use Library\ProductStock;

class Collection extends \ArrayIterator
{
    public $reservedStock = array();
    public $order_no = "";
    public $storeCodeNewRetail = "";
    public $reference_order_no = "";
    public $cart_type = "";
    public $skipDeductStock = false;
    public $reorder_customer_confirmation = false;
    public $cart_id = "";
    public $items = array();
    public $itemsShoppingCartData="";

    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\SalesOrderItem
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @param array $columns
     * @param bool $showEmpty
     * @return array
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->getDataArray($columns, $showEmpty);
            $view[$idx] = $itemArray;
            $this->next();
        }

        return $view;
    }

    public function getProducts()
    {
        $this->rewind();
        $products = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            // $product = $item->getProduct()->getDataArray();
            $product = $item->getDataArray();
            $products[$idx] = $product;

            $this->next();
        }
        return $products;
    }

    /**
     * @param int $sales_order_id
     * @return array
     */
    public function createData($sales_order_id = 0)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setSalesOrderId($sales_order_id);
            $itemArray = $item->createData();
            $view[$idx] = $itemArray;

            $this->next();
        }
        return $view;
    }

    public function setSalesOrderId($order_id = 0)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setSalesOrderId($order_id);
            $this->next();
        }
        return $view;
    }

    /**
     * @param null $transaction
     * @param string $company_code
     * @return bool
     * @throws \Exception
     */
    public function saveData($transaction = null, $company_code = 'ODI')
    {
        
        // creating group bogo
        $this->BogoGroup();

        $this->rewind();
        $sku = array();
        $storeCode = "";
        $productStock = new ProductStock();

        $skudeducts = array();
        
        
        
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();    
            $selectedItem = array();        
            // for order vendor
            if ($storeCode == ""){
                $storeCode = $item->getStoreCode();
            }            
            if($transaction) {
                $item->setTransaction($transaction);
            }

            foreach($this->itemsShoppingCartData as $itemShoppingCartData){
                if ($itemShoppingCartData['sku'] == $item->getSku() ){
                    $selectedItem = $itemShoppingCartData;
                }
            }

            try {                          
                $item->setCart_id($this->cart_id);
                $item->saveData("transaction");
                $item->deductAndAssignItemStock($this->order_no,$company_code, $this->storeCodeNewRetail, $this->reference_order_no, $this->cart_type, $selectedItem);
            } catch (\Library\HTTPException $e) {
             
                LogHelper::log("createOrderError",$this->order_no . " : Save item failed, SKU item : " . $item->getSku() . ", Msg : " .$e->devMessage);
                if($company_code == "ODIT" || $company_code == "ODIS" || $company_code == "ODIK") {
                    array_push($sku, $item->getSku());                   
                }else {
                    throw new \Exception($e->devMessage);                
                }                
            } catch (\Exception $e) {
                LogHelper::log("createOrderError",$this->order_no . " : Save item failed, SKU item : " . $item->getSku() . ", Msg : " .$e->getMessage());
                if($company_code == "ODIT" || $company_code == "ODIS" || $company_code == "ODIK") {
                    array_push($sku, $item->getSku());
                }else {
                    
                   throw new \Exception($e->getMessage());
                } 
                
            }

            if(count($sku) == 0){
                try {
                    if (!empty($item->getEmployees())) {
                        $item->getEmployees()->order_no = $this->order_no;
                        $item->getEmployees()->setSalesOrderItemId($item->getSalesOrderItemId());
                        $item->getEmployees()->saveData($transaction);
                    }
                } catch (\Exception $e) {
                    $transaction->rollback(
                        "saveEmployeeItem : " . $e->getMessage()
                    );
                    return false;
                }
    
                try {
                    if (!empty($item->getFlat())) {
                        $item->getFlat()->order_no = $this->order_no;
                        $item->getFlat()->setSalesOrderItemId($item->getSalesOrderItemId());
                        $item->getFlat()->saveData($transaction);
                    }
                } catch (\Exception $e) {
                    $transaction->rollback(
                        "saveItemFlat : " . $e->getMessage()
                    );
                    return false;
                }
            }
            

            try {
                if (!empty($item->getGiftcards())) {
                    $item->getGiftcards()->setSalesOrderItemId($item->getSalesOrderItemId());
                    $item->getGiftcards()->saveData($transaction);
                }
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveItemGiftcards : " . $e->getMessage()
                );
                return false;
            }            

            $isStoreCode1000DC = false;
            
            if (!$this->skipDeductStock && ($company_code == "ODI" || $company_code == "ODIT" || $company_code == "ODIS" || $company_code == "ODIK")) {
                if (!$this->reorder_customer_confirmation) {
                    if (strpos($item->getStoreCode(), '1000DC') !== false) {
                        $isStoreCode1000DC = true;
                    }
                    if (!empty($item->getStockStoreCode())) {
                        $storeCodeDeduct = $item->getStockStoreCode();
                    }
                    $on_hand_only = 0;
                    if ($this->storeCodeNewRetail != "" || $this->reference_order_no != "") {
                        $on_hand_only = 1;
                    }

                    $installationInformaB2BSku = getenv('INSTALLATION_INFORMAB2B_SKU');
                    $installationAceB2BSku = getenv('INSTALLATION_ACEB2B_SKU');

                    // Skip deduct item for installation sku informa/ace b2b because it doesn't need any stock validation/deduction
                    $skuItem = $item->getSku();
                    if ($skuItem != $installationAceB2BSku && $skuItem != $installationInformaB2BSku) {
                        $qty = $item->getQtyOrdered();
                        $found = false;

                        for ($i = 0; $i < count($skudeducts); $i++) {
                            if ($skudeducts[$i]['sku'] === $skuItem) {
                                $skudeducts[$i]['qty'] += $qty;
                                $found = true;
                                break;
                            }
                        }

                        if (!$found) {
                            $item = array(
                                "sku" => $item->getSku(),
                                "store_code" => !empty($storeCodeDeduct) ? $storeCodeDeduct : $item->getStoreCode(),
                                "qty" => $item->getQtyOrdered(),
                                "company_code" => $company_code,
                                "on_hand_only" => $on_hand_only,
                                "process" => "payment"
                            );
                            array_push($skudeducts, $item);
                        }
                    }
                }
            }

            $this->next();
        }  

        

        if(count($sku) > 0){
            $msg = join(", ",$sku);
            $msg = "Sku : ".$msg." on ".$storeCode." not valid";
            throw new \Exception($msg);
        }

        // if company code ODI
        // or company code ODIT or ODIS BUT store code 1000DC
        if (!$this->skipDeductStock && ($company_code == "ODI" || (($company_code == "ODIT" || $company_code == "ODIS" || $company_code == "ODIK") && $isStoreCode1000DC))) {
            if (!$this->reorder_customer_confirmation) {
                $stockDeductRequest = array();

                foreach ($skudeducts as $skudeduct) {
                    $stockDeductRequest[] = array(
                        "sku" => $skudeduct['sku'],
                        "store_code" => $skudeduct['store_code'],
                        "quantity" => (int)$skudeduct['qty']
                    );
                }

                $shopId = 0;
                $channel = '';
                try {
                    if ($company_code == "ODIT" || $company_code == "ODIS" || $company_code == "ODIK"){
                        $salesOrderVendorModel = new \Models\SalesOrderVendor();
                        $salesOrderVendor = $salesOrderVendorModel->findFirst('order_no = "' . $this->order_no . '"');
                        $shopId = intval($salesOrderVendor->toArray()['shop_id']);
                    }
    
                    if ($company_code == "ODIT"){
                        $channel = 'tokopedia';
                    } elseif ($company_code == "ODIS"){
                        $channel = 'shopee';
                    } elseif ($company_code == "ODIK"){
                        $channel = 'tiktok';
                    }
                } catch (\Exception $e) {
                    LogHelper::log("get_data_shoped", $e->getMessage(), "error");
                };

                $responseDeductStock = $productStock->deductStockV2("payment", $this->order_no, $stockDeductRequest, $shopId, $channel);

                if ($responseDeductStock) {
                    foreach ($skudeducts as $skudeduct) {
                        $this->reservedStock[] = array(
                            'sku' => $skudeduct['sku'],
                            'qty_ordered' => (int)$skudeduct['qty'],
                            'store_code' => $skudeduct['store_code']
                        );
                    }
                }
            }
        }



        // if($company_code == 'ODI'){
        //     $item->deductAndAssignItemStockV2($this->order_no,$company_code, $this->storeCodeNewRetail, $this->reference_order_no, $this->cart_type,$this->items);                
        // }

        return true;
    }

    public function BogoGroup() {        
        $promo = array();
        $countPromo=0;
        // set group bogo in level child
        $this->rewind();
        while ($this->valid()) {            
            $item = $this->current();
            $itemArray = $item->getDataArray();
            $id =$itemArray['id'];            
            $cart_rules = json_decode($itemArray['cart_rules'], true);
            if(count($cart_rules)==0 && $itemArray['parent_id']== 0){
                $this->next();
                continue;
            }        
            // $rule_id = $cart_rules[0]['rule_id'];            
            for($i=0;$i < count($cart_rules);$i++){
                if($cart_rules[$i]["action"]["applied_action"] == "get_item_fixed" || $cart_rules[$i]["action"]["applied_action"] == "get_item_discount"){
                    $rule_id = $cart_rules[$i]['rule_id'];                    

                    if(!isset($promo[$id])){
                        $countPromo +=1;                        
                        $promo[$id]['rule_id'] = $rule_id;
                        $promo[$id]['group_promo_id'] = "promo ".$countPromo;
                        $item->setRuleId($promo[$id]['rule_id']);
                        $item->setGroupPromoId($promo[$id]['group_promo_id']);
                    }
                }
            }

            if($itemArray['parent_id']!=0){
                $parentId = $itemArray['parent_id'];                 
                $item->setRuleId($promo[$parentId]['rule_id']);
                $item->setGroupPromoId($promo[$parentId]['group_promo_id']);               
            }

            $this->next();
        } 

        // $this->rewind();
        // while ($this->valid()) {
        //     $idx = $this->key();
        //     $item = $this->current();
        //     $itemArray = $item->getDataArray();
        //     $id = $itemArray['id'];
        //     if($itemArray['parent_id']==0){                
        //         if(isset($promo[$id])){
        //             $item->setRuleId();
        //             $item->setGroupPromoId($promo[$id]['group_promo_id']);
        //         }
        //     }

        //     $this->next();
        // }
    }

}