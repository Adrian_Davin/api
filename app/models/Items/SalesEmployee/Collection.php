<?php

namespace Models\Items\SalesEmployee;

use \Helpers\LogHelper;

class Collection extends \ArrayIterator
{
    public $order_no = "";

    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\SalesEmployeeItem
     */
    public function current()
    {
        return parent::current();
    }

    public function setSalesOrderItemId($order_item_id = 0)
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $item->setSalesOrderItemId($order_item_id);
            $this->next();
        }
    }

    public function saveData($transaction = null, $company_code = 'ODI')
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();

            if($transaction) {
                $item->setTransaction($transaction);
            }

            try {
                $item->setStatus(10);
                $item->setCreatedAt(date("Y-m-d H:i:s"));
                $item->saveData("transaction");
            } catch (\Library\HTTPException $e) {
                LogHelper::log("createOrderError",$this->order_no . " : Save employee item failed, Sales order item id : " . $item->getSalesOrderItemId() . ", Msg : " .$e->devMessage);
                throw new \Exception($e->devMessage);
            } catch (\Exception $e) {
                LogHelper::log("createOrderError",$this->order_no . " : Save employee item failed, Sales order item id : " . $item->getSalesOrderItemId() . ", Msg : " .$e->getMessage());
                throw new \Exception($e->getMessage());
            }

            $this->next();
        }
        return true;
    }

}