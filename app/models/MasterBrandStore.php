<?php
namespace Models;


class MasterBrandStore extends \Models\BaseModel
{
    public $brand_store_id;

    public $brand_store_name;

    public $status;

    protected $errors;

    protected $messages;

    /**
     * @return mixed
     */
    public function getBrandStoreId()
    {
        return $this->brand_store_id;
    }

    /**
     * @param mixed $brand_store_id
     */
    public function setBrandId($brand_store_id)
    {
        $this->brand_store_id = $brand_store_id;
    }

    /**
     * @return mixed
     */
    public function getBrandStoreName()
    {
        return $this->brand_store_name;
    }

    /**
     * @param mixed $name
     */
    public function setBrandStoreName($brand_store_name)
    {
        $this->brand_store_name = $brand_store_name;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->whiteList = array();
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_brand_store';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

}