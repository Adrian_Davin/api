<?php

namespace Models;

use Helpers\GeneralHelper as GeneralHelper;
use Library\Nsq;
use \Phalcon\Db as Database;

class SupplierOwnfleetTemplate extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $ownfleet_template_id;
    protected $supplier_id;
    protected $name;
    protected $status;

    protected $created_at;
    protected $updated_at;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getOwnfleetTemplateId()
    {
        return $this->ownfleet_template_id;
    }

    /**
     * @param int $ownfleet_template_id
     */
    public function setOwnfleetTemplateId($ownfleet_template_id)
    {
        $this->ownfleet_template_id = $ownfleet_template_id;
    }

    /**
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param mixed $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('ownfleet_template_id', 'Models\SupplierOwnfleetRate', 'ownfleet_template_id', array('alias' => 'SupplierOwnfleetRate'));
        $this->belongsTo('supplier_id', 'Models\Supplier', 'supplier_id', array('alias' => 'Supplier'));
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return 'supplier_ownfleet_template';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
            if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function uploadTemplateToProduct($params = array()){
        $sku = array();
        $arr = array();
        $productIds = array();

        foreach ($params['result'] as $key => $val){
            $sku[] = "'".$val['sku']."'";
        }

        $stringSku = implode(",", $sku);
        $stringSku = '('.$stringSku.')';

        $sql  = "SELECT product_id, sku FROM product_variant WHERE sku IN ".$stringSku." ";

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        $result->setFetchMode(
            Database::FETCH_ASSOC
        );

        $productData = $result->fetchAll();

        foreach ($productData as $key => $val){
            $found = GeneralHelper::in_array_r($val['sku'],$params['result']);

            if(!empty($found)){
                $arr[] = "WHEN ".$val['product_id']." THEN ".$found['ownfleet_template_id'];
                $arrUpdatedAt[] = "WHEN ".$val['product_id']." THEN '".date('Y-m-d H:i:s')."'";
                $productIds[] = "'".$val['product_id']."'";
            }
        }

        $stringCase = implode(" ", $arr);
        $stringUpdatedAt = implode(" ", $arrUpdatedAt);
        $stringProductIds = '('.implode(",", $productIds).')';

        $sql = "UPDATE product SET ownfleet_template_id = ( CASE product_id ".$stringCase." END), updated_at = ( CASE product_id ".$stringUpdatedAt." END)";
        $sql .= " WHERE product_id IN ".$stringProductIds;
        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        $count = $result->numRows();

        if($count > 0){
            foreach ($productData as $key => $val){
                $nsq = new Nsq();
                $message = [
                    "data" => ["sku" => $val['sku']],
                    "message" => "createCache"
                ];
                $nsq->publishCluster('product', $message);
            }

        }
        return $count ;

    }

}
