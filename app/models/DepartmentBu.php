<?php
/**
 * Created by PhpStorm.
 * User: roesmien_ecomm
 * Date: 2/8/2017
 * Time: 9:49 AM
 */

namespace Models;


class DepartmentBu extends \Models\BaseModel
{
    public $id_department_bu;

    public $name;

    public $supplier_id;

    public $sister_company;

    public $status;

    public $create_date;

    protected $errors;

    protected $messages;

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

     /**
     * @return mixed
     */
    public function getDepartementBUId()
    {
        return $this->id_department_bu;
    }

    /**
     * @param mixed $id_department_bu
     */
    public function setDepartementBUId($id_department_bu)
    {
        $this->id_department_bu = $id_department_bu;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param mixed $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    public function getSisterCompany()
    {
        return $this->sister_company;
    }

    /**
     * @param mixed $sister_company
     */
    public function setSisterCompany($sister_company)
    {
        $this->sister_company = $sister_company;
    }

    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id_department_bu', 'AttributeSap', 'department_bu', array('alias' => 'AttributeSap'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'department_bu';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

}