<?php

namespace Models;

class SellerNotif extends \Models\BaseModel
{
    public $id;
    public $sku;
    public $is_buyer_approved;
    public $is_content_approved;
    public $is_rejected;
    public $created_at;
    public $updated_at;
    public $supplier_id;
    public $product_name;
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=true)
     */


    public function getSource()
    {
        return 'seller_notif';
    }

    public function initialize(){
        
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return seller_notif[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return seller_notif
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function updateDataSellerNotif($sku = FALSE, $is_buyer_approved = FALSE, $is_content_approved = FALSE, $is_rejected = FALSE, $created_at = FALSE, $product_name = FALSE){
        try{
            $response = array();
            
            $parameters = [
                'columns' => 'sku, is_buyer_approved, is_content_approved, is_rejected',
                'conditions' => 'sku = "'.$sku.'"'
            ];
            $data = $this->findFirst($parameters);
            if(empty($data)){
                $response['errors'] = array(
                    "code" => "R100",
                    "title" => "Error Message",
                    "messages" => "Failed to save data seller, please contact ruparupa tech support"
                );
                
                return $response;
            }
            
            if ($is_buyer_approved){
                if($data->is_content_approved){
                    $phql = "UPDATE seller_notif SET is_buyer_approved = '".$is_buyer_approved."', is_rejected = 0,  updated_at = '".$created_at."', product_name = '".$product_name."' WHERE sku = '".$sku."'";
                }else {
                    $phql = "UPDATE seller_notif SET is_buyer_approved = '".$is_buyer_approved."', updated_at = '".$created_at."', product_name = '".$product_name."' WHERE sku = '".$sku."'";
                }
            }else if ($is_content_approved){
                if($data->is_buyer_approved){
                    $phql = "UPDATE seller_notif SET is_content_approved = '".$is_content_approved."', is_rejected = 0,  updated_at = '".$created_at."', product_name = '".$product_name."' WHERE sku = '".$sku."'";
                }else {
                    $phql = "UPDATE seller_notif SET is_content_approved = '".$is_content_approved."', updated_at = '".$created_at."', product_name = '".$product_name."' WHERE sku = '".$sku."'";
                }
            }else if ($is_rejected){
                    $phql = "UPDATE seller_notif SET is_content_approved = 0,  is_rejected = '".$is_rejected."', updated_at = '".$created_at."', product_name = '".$product_name."' WHERE sku = '".$sku."'";
            } else {
                $phql = "UPDATE seller_notif SET is_content_approved = 0,  is_rejected = '".$is_rejected."', updated_at = '".$created_at."' WHERE sku = '".$sku."'";
            }
            $result = $this->getDi()->getShared('dbMaster')->execute($phql);
            $affected_row = $this->getDi()->getShared('dbMaster')->affectedRows();
           
            $response = $this->setMessages(array('Successfully Update Seller data'));
            
            
    }   catch (\Exception $e){
            
            $response['errors'] = array(
                "code" => "R100",
                "title" => "Error Message",
                "messages" => "Failed to save data seller, please contact ruparupa tech support"
            );
            return $response;
        }
    }

}