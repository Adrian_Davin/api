<?php

namespace Models;

use Phalcon\Db;

class SpecialPrice extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sku;
    protected $start_date;
    protected $end_date;
    protected $special_price;
    protected $file_csv;
    protected $created_user;
    protected $created_at;
    protected $label;
    protected $additional_specification;

    protected $string_value;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getStringValue()
    {
        return $this->string_value;
    }

    /**
     * @param mixed $string_value
     */
    public function setStringValue($string_value)
    {
        $this->string_value = $string_value;
    }

    /**
     * @return int
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param int $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return mixed
     */
    public function getCreatedUser()
    {
        return $this->created_user;
    }

    /**
     * @param mixed $created_user
     */
    public function setCreatedUser($created_user)
    {
        $this->created_user = $created_user;
    }

    /**
     * @return mixed
     */
    public function getSpecialPrice()
    {
        return $this->special_price;
    }

    /**
     * @param mixed $special_price
     */
    public function setSpecialPrice($special_price)
    {
        $this->special_price = $special_price;
    }

    /**
     * @return mixed
     */
    public function getFileCsv()
    {
        return $this->file_csv;
    }

    /**
     * @param mixed $file_csv
     */
    public function setFileCsv($file_csv)
    {
        $this->file_csv = $file_csv;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getAdditionalSpecification()
    {
        return $this->additional_specification;
    }

    /**
     * @param mixed $additional_specification
     */
    public function setAdditionalSpecification($additional_specification)
    {
        $this->additional_specification = $additional_specification;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'special_price';
    }


    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setPriceFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {

                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function insertPriceToDb(){
        try {
            $sql  = "INSERT INTO special_price(sku,special_price,start_date,end_date,file_csv,created_user,created_at,label,additional_specification) ";
            $sql .= "VALUES".$this->string_value;

            $this->useWriteConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);

            if (!$result) {
                $this->errorCode = "RR301";
                $this->errorMessages[] = $e->getMessage();
            }
        }  catch (\Exception $e) {
            $this->errorCode = "RR301";
            $this->errorMessages[] = $e->getMessage();
        }
    }

    public function deleteSpecialPrice(){
        $date = date('Y-m-d H:i:s');

        $sql  = "DELETE FROM special_price WHERE end_date < '".$date."'";

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        $final = array(
            'deleted' => $result->numRows()
        );
        return $final;

    }

    public function getAllActiveSpecialPrice(){
        $date = date('Y-m-d H:i:s');

        $result = $this->find(
            [
                "columns" => "special_price_id, sku, special_price, start_date, end_date, created_at",
                "conditions" => "start_date <='".$date."' AND end_date >='".$date."'",
                "order" => "sku ASC"
            ]
        );

        $prices = $result->toArray();

        $data = array();

        foreach($prices as $key => $val){
            if(empty($data)){ // belom di isi apa2
                // masukin data sku pertama
                $data[$key]['sku'] = $val['sku'];
                $data[$key]['special_price'] = $val['special_price'];
                $data[$key]['start_date'] = $val['start_date'];
                $data[$key]['end_date'] = $val['end_date'];
                $data[$key]['created_at'] = $val['created_at'];
            }
            else { // kalau tidak kosong, maka dia mulai cek
                $last_idx = $key-1;
                //echo $last_idx;
                if(($val['sku']) == $data[$last_idx]['sku']){ // kalau sku double
                    if(strtotime($data[$last_idx]['start_date']) < strtotime($val['start_date'])){ // yang ditampung sku nya sama, dan start_date lebih jauh drpd yang sekarang
                        $data[$key]['sku'] = $val['sku'];
                        $data[$key]['special_price'] = $val['special_price'];
                        $data[$key]['start_date'] = $val['start_date'];
                        $data[$key]['end_date'] = $val['end_date'];
                        $data[$key]['created_at'] = $val['created_at'];

                        $data[$last_idx]['sku'] = "";
                        $data[$last_idx]['special_price'] = "";
                        $data[$last_idx]['start_date'] = "";
                        $data[$last_idx]['end_date'] = "";
                        $data[$last_idx]['created_at'] = "";

                    }
                    else if(strtotime($data[$last_idx]['start_date']) > strtotime($val['start_date'])) { //
                        $data[$key]['sku'] = $data[$last_idx]['sku'];
                        $data[$key]['special_price'] = $data[$last_idx]['special_price'];
                        $data[$key]['start_date'] = $data[$last_idx]['start_date'];
                        $data[$key]['end_date'] = $data[$last_idx]['end_date'];
                        $data[$key]['created_at'] = $data[$last_idx]['created_at'];

                        $data[$last_idx]['sku'] = "";
                        $data[$last_idx]['special_price'] = "";
                        $data[$last_idx]['start_date'] = "";
                        $data[$last_idx]['end_date'] = "";
                        $data[$last_idx]['created_at'] = "";

                    }
                    else if(strtotime($data[$last_idx]['start_date']) == strtotime($val['start_date'])){ // kalau sku sama, start sama, end sama, liat created at
                        if(strtotime($data[$last_idx]['end_date']) > strtotime($val['end_date'])){ // kalau statement 1 fail, berarti start_date sama (double), cari end_date
                            $data[$key]['sku'] = $val['sku'];
                            $data[$key]['special_price'] = $val['special_price'];
                            $data[$key]['start_date'] = $val['start_date'];
                            $data[$key]['end_date'] = $val['end_date'];
                            $data[$key]['created_at'] = $val['created_at'];

                            $data[$last_idx]['sku'] = "";
                            $data[$last_idx]['special_price'] = "";
                            $data[$last_idx]['start_date'] = "";
                            $data[$last_idx]['end_date'] = "";
                            $data[$last_idx]['created_at'] = "";
                        }
                        else if(strtotime($data[$last_idx]['end_date'] ) < strtotime($val['end_date'])){ // kalau statement 1 fail, berarti start_date sama (double), cari end_date
                            $data[$key]['sku'] = $data[$last_idx]['sku'];
                            $data[$key]['special_price'] = $data[$last_idx]['special_price'];
                            $data[$key]['start_date'] = $data[$last_idx]['start_date'];
                            $data[$key]['end_date'] = $data[$last_idx]['end_date'];
                            $data[$key]['created_at'] = $data[$last_idx]['created_at'];

                            $data[$last_idx]['sku'] = "";
                            $data[$last_idx]['special_price'] = "";
                            $data[$last_idx]['start_date'] = "";
                            $data[$last_idx]['end_date'] = "";
                            $data[$last_idx]['created_at'] = "";
                        }
                        else if(strtotime($data[$last_idx]['end_date'] ) == strtotime($val['end_date'])){
                            // random take or unset this
                            $data[$key]['sku'] = $data[$last_idx]['sku'];
                            $data[$key]['special_price'] = $data[$last_idx]['special_price'];
                            $data[$key]['start_date'] = $data[$last_idx]['start_date'];
                            $data[$key]['end_date'] = $data[$last_idx]['end_date'];
                            $data[$key]['created_at'] = $data[$last_idx]['created_at'];

                            $data[$last_idx]['sku'] = "";
                            $data[$last_idx]['special_price'] = "";
                            $data[$last_idx]['start_date'] = "";
                            $data[$last_idx]['end_date'] = "";
                            $data[$last_idx]['created_at'] = "";
                        }
                    }
                }
                else {
                    $data[$key]['sku'] = $val['sku'];
                    $data[$key]['special_price'] = $val['special_price'];
                    $data[$key]['start_date'] = $val['start_date'];
                    $data[$key]['end_date'] = $val['end_date'];
                    $data[$key]['created_at'] = $val['created_at'];
                }
            }
        }

        return array_values(array_filter(array_map('array_filter',$data)));
    }

}
