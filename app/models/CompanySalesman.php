<?php

namespace Models;

class CompanySalesman extends \Models\BaseModel
{    
    protected $company_salesman_id;
    protected $sales_group;
    protected $sales_group_description;
    protected $salesman_code;
    protected $salesman;

     /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }
    

       /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CompanySalesman
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    /**
     * Get the value of company_salesman_id
     */ 
    public function getCompany_salesman_id()
    {
        return $this->company_salesman_id;
    }

    /**
     * Set the value of company_salesman_id
     *
     * @return  self
     */ 
    public function setCompany_salesman_id($company_salesman_id)
    {
        $this->company_salesman_id = $company_salesman_id;

        return $this;
    }

    /**
     * Get the value of sales_group
     */ 
    public function getSales_group()
    {
        return $this->sales_group;
    }

    /**
     * Set the value of sales_group
     *
     * @return  self
     */ 
    public function setSales_group($sales_group)
    {
        $this->sales_group = $sales_group;

        return $this;
    }

    /**
     * Get the value of sales_group_description
     */ 
    public function getSales_group_description()
    {
        return $this->sales_group_description;
    }

    /**
     * Set the value of sales_group_description
     *
     * @return  self
     */ 
    public function setSales_group_description($sales_group_description)
    {
        $this->sales_group_description = $sales_group_description;

        return $this;
    }

    /**
     * Get the value of salesman_code
     */ 
    public function getSalesman_code()
    {
        return $this->salesman_code;
    }

    /**
     * Set the value of salesman_code
     *
     * @return  self
     */ 
    public function setSalesman_code($salesman_code)
    {
        $this->salesman_code = $salesman_code;

        return $this;
    }

    /**
     * Get the value of salesman
     */ 
    public function getSalesman()
    {
        return $this->salesman;
    }

    /**
     * Set the value of salesman
     *
     * @return  self
     */ 
    public function setSalesman($salesman)
    {
        $this->salesman = $salesman;

        return $this;
    }
}
