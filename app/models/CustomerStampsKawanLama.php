<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerStampsKawanLama extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $customer_stamps_kawan_lama_id;

    /**
     *
     * @var string
     * @Column(type="string", length=128, nullable=true)
     */
    protected $email;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $phone;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $member_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $merchant_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_stamps_kawan_lama';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerStampsKawanLama[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return int
     */
    public function getCustomerStampsKawanLamaId()
    {
        return $this->customer_stamps_kawan_lama_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_stamps_kawan_lama_id)
    {
        $this->customer_stamps_kawan_lama_id = $customer_stamps_kawan_lama_id;
    }


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMemberID()
    {
        return $this->member_id;
    }

    /**
     * @param string $member_id
     */
    public function setMemberID($member_id)
    {
        $this->member_id = $member_id;
    }


    /**
     * @return int
     */
    public function getMerchantID()
    {
        return $this->merchant_id;
    }

    /**
     * @param int $merchant_id
     */
    public function setMerchantID($merchant_id)
    {
        $this->merchant_id = $merchant_id;
    }

    public function getCustomerIdByEmail($paramsEmail)
    {
        $custIdArray = array();
        if (!empty($paramsEmail['email'])) {
            $sql = "SELECT customer_stamps_kawan_lama_id FROM customer_stamps_kawan_lama WHERE email = '{$paramsEmail['email']}';";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $custIdArray = $result->fetchAll();
            }
        }
        return $custIdArray;
    }

    public function getCustomerIdByPhone($paramsPhone)
    {
        $custIdArray = array();
        if (!empty($paramsPhone['phone'])) {
            $sql = "SELECT customer_stamps_kawan_lama_id FROM customer_stamps_kawan_lama WHERE phone = '{$paramsPhone['phone']}';";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $custIdArray = $result->fetchAll();
            }
        }
        return $custIdArray;
    }
}
