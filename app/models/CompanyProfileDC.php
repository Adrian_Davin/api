<?php

namespace Models;

use Phalcon\Db as Database;

class CompanyProfileDC extends \Models\BaseModel
{
    protected $company_id;
    protected $sales_office;
    protected $sales_office_description;
    protected $sales_group;
    protected $sales_group_description;
    protected $salesman;
    protected $salesman_code;
    protected $company_code;
    protected $store_code;
    
    public function getCompany_id(){
		return $this->company_id;
	}

	public function setCompany_id($company_id){
		$this->company_id = $company_id;
	}

	public function getSales_office(){
		return $this->sales_office;
	}

	public function setSales_office($sales_office){
		$this->sales_office = $sales_office;
	}

	public function getSales_office_description(){
		return $this->sales_office_description;
	}

	public function setSales_office_description($sales_office_description){
		$this->sales_office_description = $sales_office_description;
	}

	public function getSales_group(){
		return $this->sales_group;
	}

	public function setSales_group($sales_group){
		$this->sales_group = $sales_group;
	}

	public function getSales_group_description(){
		return $this->sales_group_description;
	}

	public function setSales_group_description($sales_group_description){
		$this->sales_group_description = $sales_group_description;
	}

	public function getSalesman(){
		return $this->salesman;
	}

	public function setSalesman($salesman){
		$this->salesman = $salesman;
	}

	public function getSalesman_code(){
		return $this->salesman_code;
	}

	public function setSalesman_code($salesman_code){
		$this->salesman_code = $salesman_code;
	}

	public function getCompany_code(){
		return $this->company_code;
	}

	public function setCompany_code($company_code){
		$this->company_code = $company_code;
	}

	public function getStore_code(){
		return $this->store_code;
	}

	public function setStore_code($store_code){
		$this->store_code = $store_code;
	}

	/**
     * Initialize method for model.
     */
    public function initialize()
    {

    }
    
    public function getCompanyByStoreCode($store_code)
    {
        $companyProfile = array();
        if (!empty($store_code)) {
            $sql = "SELECT * FROM company_profile_dc WHERE store_code = '$store_code';";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $companyProfile = $result->fetchAll()[0];
            }
        }

        return $companyProfile;
    }
}

    