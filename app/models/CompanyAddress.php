<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 2:06 PM
 */


namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class CompanyAddress extends \Models\BaseModel
{
    protected $address_id;
    protected $company_id;
    protected $address_sap_id;
    protected $country_id;
    protected $province_id;
    protected $city_id;
    protected $kecamatan_id;
    protected $first_name;
    protected $last_name;
    protected $address_name;
    protected $full_address;
    protected $address_type;
    protected $phone;
    protected $post_code;
    protected $fax;
    protected $is_default;
    protected $status;
    protected $created_at;
    protected $updated_at;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * @return mixed
     */
    public function getAddressSapId()
    {
        return $this->address_sap_id;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * @return mixed
     */
    public function getProvinceId()
    {
        return $this->province_id;
    }

    /**
     * @return mixed
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * @return mixed
     */
    public function getKecamatanId()
    {
        return $this->kecamatan_id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getFullAddress()
    {
        return $this->full_address;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->post_code;
    }

    public function initialize()
    {
        $this->belongsTo('company_id', 'Models\Company', 'company_id', array('alias' => 'Company'));
        $this->belongsTo('country_id', 'Models\MasterCountry', 'country_id', array('alias' => 'MasterCountry', "reusable" => true));
        $this->belongsTo('city_id', 'Models\MasterCity', 'city_id', array('alias' => 'MasterCity', "reusable" => true));
        $this->belongsTo('province_id', 'Models\MasterProvince', 'province_id', array('alias' => 'MasterProvince', "reusable" => true));
        $this->belongsTo('kecamatan_id', 'Models\MasterKecamatan', 'kecamatan_id', array('alias' => 'MasterKecamatan', "reusable" => true));
        $this->belongsTo('company_id', 'Models\Company', 'company_id', array('alias' => 'Company'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'company_address';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }

            if($key == 'country') {
                $data_array['country_id'] = $data_array['country']['country_id'];
                $this->country_id = $data_array['country']['country_id'];
            }

            if($key == 'province') {
                $data_array['province_id'] = $data_array['province']['province_id'];
                $this->province_id = $data_array['province']['province_id'];
            }

            if($key == 'city') {
                $data_array['city_id'] = $data_array['city']['city_id'];
                $this->city_id = $data_array['city']['city_id'];
            }

            if($key == 'kecamatan') {
                $data_array['kecamatan_id'] = $data_array['kecamatan']['kecamatan_id'];
                $this->kecamatan_id = $data_array['kecamatan']['kecamatan_id'];
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        unset($view['country_id']);
        unset($view['province_id']);
        unset($view['city_id']);
        unset($view['kecamatan_id']);

        if(!empty($this->country_id)) {
            $view['country'] = $this->MasterCountry->toArray();
        }

        if(!empty($this->province_id)) {
            $view['province'] = $this->MasterProvince->toArray(['province_id','province_code','province_name']);
        }

        if(!empty($this->city_id)) {
            $view['city'] = $this->MasterCity->toArray(['city_id','city_name']);
        }

        if(!empty($this->kecamatan_id)) {
            $view['kecamatan'] = $this->MasterKecamatan->toArray(['kecamatan_id','kecamatan_name']);
        }

        return $view;
    }
}