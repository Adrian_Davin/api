<?php
namespace Models;

use Phalcon\Db as Database;

class AdminUserAttributeValue extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $admin_user_attribute_value_id;

    /**
     *
     * @var integer
     */
    protected $admin_user_id;

    /**
     *
     * @var integer
     */
    protected $master_app_id;

    /**
     *
     * @var integer
     */
    protected $admin_user_attribute_id;

    /**
     *
     * @var string
     */
    protected $value;

    /**
     * Method to set the value of field admin_user_attribute_value_id
     *
     * @param integer $admin_user_attribute_value_id
     * @return $this
     */
    public function setAdminUserAttributeValueId($admin_user_attribute_value_id)
    {
        $this->admin_user_attribute_value_id = $admin_user_attribute_value_id;

        return $this;
    }

    /**
     * Method to set the value of field admin_user_id
     *
     * @param integer $admin_user_id
     * @return $this
     */
    public function setAdminUserId($admin_user_id)
    {
        $this->admin_user_id = $admin_user_id;

        return $this;
    }

    /**
     * Method to set the value of field master_app_id
     *
     * @param integer $master_app_id
     * @return $this
     */
    public function setMasterAppId($master_app_id)
    {
        $this->master_app_id = $master_app_id;

        return $this;
    }

    /**
     * Method to set the value of field admin_user_attribute_id
     *
     * @param integer $admin_user_attribute_id
     * @return $this
     */
    public function setAdminUserAttributeId($admin_user_attribute_id)
    {
        $this->admin_user_attribute_id = $admin_user_attribute_id;

        return $this;
    }

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Returns the value of field admin_user_attribute_value_id
     *
     * @return integer
     */
    public function getAdminUserAttributeValueId()
    {
        return $this->admin_user_attribute_value_id;
    }

    /**
     * Returns the value of field admin_user_id
     *
     * @return integer
     */
    public function getAdminUserId()
    {
        return $this->admin_user_id;
    }

    /**
     * Returns the value of field master_app_id
     *
     * @return integer
     */
    public function getMasterAppId()
    {
        return $this->master_app_id;
    }

    /**
     * Returns the value of field admin_user_attribute_id
     *
     * @return integer
     */
    public function getAdminUserAttributeId()
    {
        return $this->admin_user_attribute_id;
    }

    /**
     * Returns the value of field value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('admin_user_id', 'AdminUser', 'admin_user_id', array('alias' => 'AdminUser'));
        $this->belongsTo('admin_user_attribute_id', 'AdminUserAttribute', 'admin_user_attribute_id', array('alias' => 'AdminUserAttribute'));
        $this->belongsTo('master_app_id', 'MasterApp', 'master_app_id', array('alias' => 'MasterApp'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'admin_user_attribute_value';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminUserAttributeValue[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminUserAttributeValue
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function setFromArrayCustomize($dataArray = array())
    {
        foreach($dataArray as $key => $val) {
            if($key == "admin_user_attribute_name") {

                $adminUserAttributeModel = new \Models\AdminUserAttribute();
                $result = $adminUserAttributeModel->findFirst(' admin_user_attribute_name = "'.$val.'" ');

                if($result){
                    $this->admin_user_attribute_id = $result->toArray()['admin_user_attribute_id'];
                }else{
                    \Helpers\LogHelper::log("admin_user_attribute_value", "attribute not found with name : " . $val, "error");
                }

            }else{
                if(property_exists($this, $key)) {
                    $this->{$key} = $val;
                }
            }

        }

    }

    public function getAdminListUserAttributeValue($admin_user_id, $master_app_id){
        $sql =  "
        SELECT auav.admin_user_attribute_value_id, auav.value, ma.master_app_id, ma.master_app_name, aua.admin_user_attribute_id, aua.admin_user_attribute_name, aua.description
        FROM admin_user_attribute_value auav
        INNER JOIN master_app ma ON auav.master_app_id = ma.master_app_id
        INNER JOIN admin_user_attribute aua ON auav.admin_user_attribute_id = aua.admin_user_attribute_id
        WHERE auav.admin_user_id = '$admin_user_id' 
        AND auav.master_app_id = '$master_app_id';
        ";

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            return $result->fetchAll();
        }
        return array();
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }

}
