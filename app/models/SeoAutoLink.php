<?php
/**
 * Created by PhpStorm.
 * User: iwan
 * Date: 30/08/18
 * Time: 9:57
 */

namespace Models;


class SeoAutoLink extends \Models\BaseModel
{
    /**
     * @var $seo_auto_link_id
     */
    protected $seo_auto_link_id;
    protected $seo_header;
    protected $seo_footer;
    protected $seo_link;
    protected $company_code;
    protected $hero_product;
    protected $status;
    protected $is_default;
    protected $created_at;
    protected $updated_at;
    protected $flag;
    protected $typo;

    public function onConstruct()
    {
        parent::onConstruct();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'seo_auto_link';
    }

    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Get the value of seo_auto_link_id
     *
     * @return  $seo_auto_link_id
     */ 
    public function getSeo_auto_link_id()
    {
        return $this->seo_auto_link_id;
    }

    /**
     * Set the value of seo_auto_link_id
     *
     * @param  $seo_auto_link_id
     *
     * @return  self
     */ 
    public function setSeo_auto_link_id($seo_auto_link_id)
    {
        $this->seo_auto_link_id = $seo_auto_link_id;

        return $this;
    }

    /**
     * Get the value of seo_header
     */ 
    public function getSeo_header()
    {
        return $this->seo_header;
    }

    /**
     * Set the value of seo_header
     *
     * @return  self
     */ 
    public function setSeo_header($seo_header)
    {
        $this->seo_header = $seo_header;

        return $this;
    }

    /**
     * Get the value of seo_footer
     */ 
    public function getSeo_footer()
    {
        return $this->seo_footer;
    }

    /**
     * Set the value of seo_footer
     *
     * @return  self
     */ 
    public function setSeo_footer($seo_footer)
    {
        $this->seo_footer = $seo_footer;

        return $this;
    }

    /**
     * Get the value of seo_link
     */ 
    public function getSeo_link()
    {
        return $this->seo_link;
    }

    /**
     * Set the value of seo_link
     *
     * @return  self
     */ 
    public function setSeo_link($seo_link)
    {
        $this->seo_link = $seo_link;

        return $this;
    }

    /**
     * Get the value of company_code
     */ 
    public function getCompany_code()
    {
        return $this->company_code;
    }

    /**
     * Set the value of company_code
     *
     * @return  self
     */ 
    public function setCompany_code($company_code)
    {
        $this->company_code = $company_code;

        return $this;
    }

    /**
     * Get the value of hero_product
     */ 
    public function getHero_product()
    {
        return $this->hero_product;
    }

    /**
     * Set the value of hero_product
     *
     * @return  self
     */ 
    public function setHero_product($hero_product)
    {
        $this->hero_product = $hero_product;

        return $this;
    }

    /**
     * Get the value of created_at
     */ 
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */ 
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of updated_at
     */ 
    public function getUpdated_at()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of updated_at
     *
     * @return  self
     */ 
    public function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
            if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of is_default
     */ 
    public function getIs_default()
    {
        return $this->is_default;
    }

    /**
     * Set the value of is_default
     *
     * @return  self
     */ 
    public function setIs_default($is_default)
    {
        $this->is_default = $is_default;

        return $this;
    }

    /**
     * Get the value of flag
     */ 
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set the value of flag
     *
     * @return  self
     */ 
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get the value of typo
     */ 
    public function getTypo()
    {
        return $this->typo;
    }

    /**
     * Set the value of typo
     *
     * @return  self
     */ 
    public function setTypo($typo)
    {
        $this->typo = $typo;

        return $this;
    }
}