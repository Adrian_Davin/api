<?php
namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class SupplierAccount extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $supplier_account_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $supplier_id;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    public $account_name;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    public $account_number;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    public $bank;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $bank_code;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $iban;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $swift;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    public $is_default;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('supplier_id', 'Supplier', 'supplier_id', array('alias' => 'Supplier'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'supplier_account';
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SupplierAccount[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SupplierAccount
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function saveAccount(){
        try {

            $saveStatus = $this->saveData("supplier_account");
            $action = $this->action;
            if(empty(trim($this->supplier_account_id))){
                $lastSupplier = $this::findFirst(array(
                    "columns" => "supplier_account_id",
                ));

                $this->supplier_account_id      = $lastSupplier['supplier_account_id'];
            }else{
                $this->supplier_account_id   = $this->supplier_account_id;
            }

            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("Supplier", "Supplier Account save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Save/Update Supplier Account failed";

                $transaction->rollback(
                    "Save/Update Supplier Account failed"
                );
                return false;
            }
            $transaction->commit();
        } catch (TxFailed $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }

}
