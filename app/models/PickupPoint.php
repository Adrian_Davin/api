<?php

namespace Models;

use Phalcon\Db;

class PickupPoint extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $pickup_id;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $pickup_code;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $pickup_name;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=true)
     */
    protected $email_pic;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $geolocation;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=false)
     */
    protected $post_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $is_express_courier;

    protected $city_id;
    protected $province_code;
    protected $kecamatan_code;
    protected $kelurahan_id;
    protected $address_line_1;
    protected $address_line_2;
    protected $is_ownfleet;
    protected $information;
    protected $pickupPointCache;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('pickup_id', 'Models\Store', 'pickup_id', array('alias' => 'Store'));
        $this->belongsTo('city_id', 'Models\MasterCity', 'city_id', array('alias' => 'MasterCity', "reusable" => true));
        $this->belongsTo('kecamatan_code', 'Models\MasterKecamatan', 'kecamatan_code', array('alias' => 'MasterKecamatan', "reusable" => true));
        $this->belongsTo('province_code', 'Models\MasterProvince', 'province_code', array('alias' => 'MasterProvince', "reusable" => true));
        $this->belongsTo('kelurahan_id', 'Models\MasterKelurahan', 'kelurahan_id', array('alias' => 'MasterKelurahan', "reusable" => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pickup_point';
    }

    /**
     * @return int
     */
    public function getPickupId()
    {
        return $this->pickup_id;
    }

    /**
     * @param int $pickup_id
     */
    public function setPickupId($pickup_id)
    {
        $this->pickup_id = $pickup_id;
    }

    /**
     * @return string
     */
    public function getPickupCode()
    {
        return $this->pickup_code;
    }

    /**
     * @param string $pickup_code
     */
    public function setPickupCode($pickup_code)
    {
        $this->pickup_code = $pickup_code;
    }

    /**
     * @return string
     */
    public function getPickupName()
    {
        return $this->pickup_name;
    }

    /**
     * @param string $pickup_name
     */
    public function setPickupName($pickup_name)
    {
        $this->pickup_name = $pickup_name;
    }

    /**
     * @return string
     */
    public function getEmailPic()
    {
        return $this->email_pic;
    }

    /**
     * @param string $email_pic
     */
    public function setEmailPic($email_pic)
    {
        $this->email_pic = $email_pic;
    }

    /**
     * @return string
     */
    public function getGeolocation()
    {
        return $this->geolocation;
    }

    /**
     * @param string $geolocation
     */
    public function setGeolocation($geolocation)
    {
        $this->geolocation = $geolocation;
    }

    /**
     * @return string
     */
    public function getPostCode()
    {
        return $this->post_code;
    }

    /**
     * @param string $post_code
     */
    public function setPostCode($post_code)
    {
        $this->post_code = $post_code;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getIsExpressCourier()
    {
        return $this->is_express_courier;
    }

    /**
     * @param mixed $is_express_courier
     */
    public function setIsExpressCourier($is_express_courier)
    {
        $this->is_express_courier = $is_express_courier;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * @param mixed $city_id
     */
    public function setCityId($city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @return mixed
     */
    public function getProvinceCode()
    {
        return $this->province_code;
    }

    /**
     * @param mixed $province_code
     */
    public function setProvinceCode($province_code)
    {
        $this->province_code = $province_code;
    }

    /**
     * @return mixed
     */
    public function getKecamatanCode()
    {
        return $this->kecamatan_code;
    }

    /**
     * @param mixed $kecamatan_code
     */
    public function setKecamatanCode($kecamatan_code)
    {
        $this->kecamatan_code = $kecamatan_code;
    }

        /**
     * @return mixed
     */
    public function getKelurahanID()
    {
        return $this->kelurahan_id;
    }

    /**
     * @param mixed $kelurahan_id
     */
    public function setKelurahanID($kelurahan_id)
    {
        $this->kelurahan_id = $kelurahan_id;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Get the value of address_line_1
     */ 
    public function getAddress_line_1()
    {
        return $this->address_line_1;
    }

    /**
     * Set the value of address_line_1
     *
     * @return  self
     */ 
    public function setAddress_line_1($address_line_1)
    {
        $this->address_line_1 = $address_line_1;

        return $this;
    }

    /**
     * Get the value of address_line_2
     */ 
    public function getAddress_line_2()
    {
        return $this->address_line_2;
    }

    /**
     * Set the value of address_line_2
     *
     * @return  self
     */ 
    public function setAddress_line_2($address_line_2)
    {
        $this->address_line_2 = $address_line_2;

        return $this;
    }

    /**
     * Get the value of is_ownfleet
     */ 
    public function getIs_ownfleet()
    {
        return $this->is_ownfleet;
    }

    /**
     * Set the value of is_ownfleet
     *
     * @return  self
     */ 
    public function setIs_ownfleet($is_ownfleet)
    {
        $this->is_ownfleet = $is_ownfleet;

        return $this;
    }
    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {

        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];

        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        
        $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
        if (isset($view['address_line_2'])) {
            $view['address'] = $view['address_line_1'] . " " . $view['address_line_2'];
        } else {
            $view['address'] = $view['address_line_1'];
        }
        unset($view['city_id']);
        unset($view['province_code']);
        unset($view['kecamatan_code']);
        unset($view['kelurahan_id']);

        if(!empty($this->city_id)) {
            $view['city'] = $this->MasterCity->toArray(['city_id','city_name']);
        }

        if(!empty($this->province_code)) {
            $view['province'] = $this->MasterProvince->toArray(['province_id','province_code','province_name']);
        }

        if(!empty($this->kecamatan_code)) {
            $view['kecamatan'] = $this->MasterKecamatan->toArray(['kecamatan_id','kecamatan_name', 'coding', 'kecamatan_code','kode_jalur','sap_exp_code']);
        }

        if(!empty($this->kelurahan_id)) {
            $view['kelurahan'] = $this->MasterKelurahan->toArray(['kelurahan_id', 'kelurahan_name', 'post_code', 'geolocation']);
        }

        unset($view['created_at']);
        unset($view['updated_at']);

        return $view;
    }
    
    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    /*
     * Override base model function to set from cache
     */
    public function afterUpdate()
    {

        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);
        $redisKey = "PickupPoint";
        $this->scanIterator($redisCon, $redisKey);

        // golang can't read phalcon redis, and shipping assignation using golang want to use same redis
        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['REDIS_HOST'],$_ENV['REDIS_PORT'],$_ENV['REDIS_DB']);
        if (!empty($this->getPickupId()) && !empty($this->getPickupPointCached())) {
            $redisKey = sprintf("pickupPoint:%s", $this->getPickupId());
            $redisCon->set($redisKey, json_encode($this->getPickupPointCached()));
        }
    }

    private function scanIterator ($redisCon, $redisKey) {
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }

    /**
     * Get the value of information
     */ 
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * Set the value of information
     *
     * @return  self
     */ 
    public function setInformation($information)
    {
        $this->information = $information;

        return $this;
    }

    public function getPickupPointCache($params = array())
    {
        // Ensure pickup_id is provided
        if (empty($params['pickup_id'])) {
            return null;
        }
        $data = null;
            $sql = "
            SELECT
                s.pickup_id,
                pp.pickup_code,
                pp.pickup_name,
                pp.province_code,
                pp.city_id,
                pp.kecamatan_code,
                pp.address_line_1,
                pp.address_line_2,
                pp.email_pic,
                pp.geolocation,
                pp.is_express_courier,
                pp.is_ownfleet,
                s.status,
                pp.kelurahan_id
            FROM
                store s
            JOIN pickup_point pp ON
                pp.pickup_id = s.pickup_id
            WHERE
                s.pickup_id = '{$params['pickup_id']}';";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Db::FETCH_ASSOC);
                $data = $result->fetch();
            }

        $data['pickup_id'] = (int)$data['pickup_id'];
        $data['city_id'] = (int)$data['city_id'];
        $data['is_express_courier'] = (int)$data['is_express_courier'];
        $data['is_ownfleet'] = (int)$data['is_ownfleet'];
        $data['status'] = (int)$data['status'];
        $data['kelurahan_id'] = (int)$data['kelurahan_id'];

        return $data;
    }

    /**
     * Get the value of pickup_point_cache
     */
    public function getPickupPointCached()
    {
        return $this->pickupPointCache;
    }

    /**
     * Set the value of pickup_point_cache
     *
     * @return  self
     */
    public function setPickupPointCached($pickupPointCache)
    {
        $this->pickupPointCache = $pickupPointCache;
    }
}
