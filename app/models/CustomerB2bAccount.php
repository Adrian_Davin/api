<?php

namespace Models;

class CustomerB2bAccount extends \Models\BaseModel
{
    protected $customer_b2b_account_id;
    protected $email;
    protected $sap_no;
    protected $company_name;
    protected $status;

     /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }
    

    /**
     * Get the value of customer_b2b_account_id
     */ 
    public function getCustomer_b2b_account_id()
    {
        return $this->customer_b2b_account_id;
    }

    /**
     * Set the value of customer_b2b_account_id
     *
     * @return  self
     */ 
    public function setCustomer_b2b_account_id($customer_b2b_account_id)
    {
        $this->customer_b2b_account_id = $customer_b2b_account_id;

        return $this;
    }


    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

      /**
     * Get the value of sap_no
     */ 
    public function getSap_no()
    {
        return $this->sap_no;
    }

    /**
     * Set the value of sap_no
     *
     * @return  self
     */ 
    public function setSap_no($sap_no)
    {
        $this->sap_no = $sap_no;

        return $this;
    }

        /**
     * Get the value of company_name
     */ 
    public function getCompany_name()
    {
        return $this->company_name;
    }

    /**
     * Set the value of company_name
     *
     * @return  self
     */ 
    public function setCompany_name($company_name)
    {
        $this->company_name = $company_name;

        return $this;
    }

       /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerB2bAccount
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
