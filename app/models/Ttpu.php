<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class Ttpu extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $ttpu_id;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $ttpu_no;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $store_code;


    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $company_code;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $page;


    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_by;

    public function getSource()
    {
        return 'ttpu_header';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->getSource();
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterColor[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterColor
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function saveTtpuHeader(){
        try {

            $saveStatus = $this->saveData("ttpu_header");
            $action = $this->action;
            if(empty(trim($this->ttpu_id))){
                $lastTtpu = $this::findFirst(array(
                    "columns" => "ttpu_id",
                ));

                $this->ttpu_id   = $lastTtpu['ttpu_id'];
            }

            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("Ttpu", "Ttpu save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Save/Update Ttpu failed";

                $transaction->rollback(
                    "Save/Update Ttpu failed"
                );
                return false;
            }
            $transaction->commit();
        } catch (TxFailed $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return $this->ttpu_id;
    }

}
