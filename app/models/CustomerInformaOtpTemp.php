<?php

namespace Models;

use Phalcon\Db as Database;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class CustomerInformaOtpTemp extends \Models\BaseModel
{
    protected $customer_informa_otp_temp_id;
    protected $email;
    protected $phone;
    protected $access_code;
    protected $validate_count;
    protected $request_count;
    protected $customer_id;
    protected $type;
    protected $created_at;
    protected $updated_at;

    protected $countdownSec;

    public function initialize()
    {
        //
    }
    /**
     * @return mixed
     */
    public function getCustomerInformaOtpTempId()
    {
        return $this->customer_informa_otp_temp_id;
    }

    /**
     * @param mixed $customer_informa_otp_temp_id
     */
    public function setCustomerInformaOtpTempId($customer_informa_otp_temp_id)
    {
        $this->customer_informa_otp_temp_id = $customer_informa_otp_temp_id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAccessCode()
    {
        return $this->access_code;
    }

    /**
     * @param mixed $access_code
     */
    public function setAccessCode($access_code)
    {
        $this->access_code = $access_code;
    }

    /**
     * @return mixed
     */
    public function getValidateCount()
    {
        return $this->validate_count;
    }

    /**
     * @param mixed $validate_count
     */
    public function setValidateCount($validate_count)
    {
        $this->validate_count = $validate_count;
    }

    /**
     * @return mixed
     */
    public function getRequestCount()
    {
        return $this->request_count;
    }

    /**
     * @param mixed $request_count
     */
    public function setRequestCount($request_count)
    {
        $this->request_count = $request_count;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param mixed $customer_id
     */
    public function setCustomerId($customer_id): void
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function getCountdownSec()
    {
        return $this->countdownSec;
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        return $this->toArray($columns, $showEmpty);
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    private function validateOtpAccessKey($accessKey) {
        if (($accessKey == "email") || ($accessKey == "phone")) {
            return true;
        }

        $this->errorCode = 400;
        $this->errorMessages = "Tipe request tidak valid: " . $accessKey;
        return false;
    }

    public function validateInformaSendOTP($customerId, $accessKey, $accessValue, $accessCode, $type, $tx = false, &$validateCount) {
        if (!$this->validateOtpAccessKey($accessKey)) {
            return false;
        }

        try {
            if (!$tx) {
                $manager = new TxManager();
                $manager->setDbService($this->getConnection());
                $transaction = $manager->get();
            } else {
                $transaction = $tx;
            }

            $customerInformaOtpTemp = $this->findFirst($accessKey . " = '" . $accessValue . "' AND type = '". $type ."' AND customer_id ='" . $customerId . "'");

            if (!$customerInformaOtpTemp) {
                $sql = "INSERT INTO customer_informa_otp_temp (". $accessKey .", request_count, access_code, customer_id, type) ";
                $sql .= "VALUES('" . $accessValue . "', '1', '" . $accessCode . "', '" . $customerId . "','". $type ."')";
            } else {
                $validateCount = $customerInformaOtpTemp->getValidateCount();

                $isResetOTP = false;
                
                if ($type == "pay_with_point" || $type == "redeem_voucher_online") {
                    $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                    $enableOtpLimitDaily = getenv("INFORMA_ENABLE_OTP_LIMIT_DAILY");
                    if ($type == "pay_with_point") {
                        $cooldownSec = getenv("INFORMA_PWP_OTP_COOLDOWN_SEC");
                        $otpResetSec = getenv("INFORMA_PWP_OTP_FAILURE_LIMIT_RESET_COOLDOWN_SEC");
                        $failureLimit = getenv("INFORMA_PWP_OTP_FAILURE_LIMIT");
                        $otpSubmitLimitDaily = getenv("INFORMA_PWP_OTP_LIMIT_DAILY");
                    } else if ($type == "redeem_voucher_online") {
                        $cooldownSec = getenv("INFORMA_REDEEM_ONLINE_VOUCHER_OTP_COOLDOWN_SEC");
                        $otpResetSec = getenv("INFORMA_REDEEM_ONLINE_VOUCHER_OTP_FAILURE_LIMIT_RESET_COOLDOWN_SEC");
                        $failureLimit = getenv("INFORMA_REDEEM_ONLINE_VOUCHER_OTP_FAILURE_LIMIT");
                        $otpSubmitLimitDaily = getenv("INFORMA_REDEEM_ONLINE_VOUCHER_OTP_LIMIT_DAILY");
                    }
                    
                    $dateTime = date_create_from_format('Y-m-d H:i:s', $customerInformaOtpTemp->getUpdatedAt());
                    $sameDay = date("Y-m-d") == date_format($dateTime, "Y-m-d");

                    // count limit otp 10 times per day
                    if ($sameDay) {
                        if ($enableOtpLimitDaily && ($customerInformaOtpTemp->getValidateCount() >= $otpSubmitLimitDaily)) {
                            $this->errorCode = 400;
                            $this->errorMessages = "Anda sudah mencapai batas maximum redeem point per hari";
                            return false;
                        } else if ($customerInformaOtpTemp->getRequestCount() - $customerInformaOtpTemp->getValidateCount() >= $failureLimit) {
                            $diffSec = $kawanLamaSystemLib->getDiffDateInSecond($customerInformaOtpTemp->getUpdatedAt(), date("Y-m-d H:i:s"));
                            if ($diffSec < $otpResetSec) {
                                $this->errorCode = 400;
                                $this->errorMessages = "Anda telah melampaui batas pengiriman OTP sebanyak " . $failureLimit . " kali.";
                                return false;
                            } else {
                                $isResetOTP = true;
                            }    
                        }
                    } else {
                        $isResetOTP = true;
                    }

                    $diffSec = $kawanLamaSystemLib->getDiffDateInSecond($customerInformaOtpTemp->getUpdatedAt(), date("Y-m-d H:i:s"));
                    if ($diffSec < $cooldownSec) {
                        $countdownSec = $cooldownSec - $diffSec;
                        $this->errorCode = 400;
                        $this->errorMessages = "Mohon tunggu " . $countdownSec . " detik sebelum melakukan pengiriman OTP ulang";
                        $this->countdownSec =  $countdownSec;
                        return false;
                    }
                }

                $requestCount = $customerInformaOtpTemp->getRequestCount() + 1;
                $sql = "UPDATE customer_informa_otp_temp SET request_count = '" . $requestCount . "', access_code = '" . $accessCode . "' WHERE customer_informa_otp_temp_id = '" . $customerInformaOtpTemp->getCustomerInformaOtpTempId() . "'";

                if ($isResetOTP) {
                    $validateCount = 0;
                    $sql = "UPDATE customer_informa_otp_temp SET request_count = '1', validate_count = '0', access_code = '" . $accessCode . "' WHERE customer_informa_otp_temp_id = '" . $customerInformaOtpTemp->getCustomerInformaOtpTempId() . "'";
                }
            }

            $this->useWriteConnection();
            $this->getDi()->getShared($this->getConnection())->query($sql);
            $this->setTransaction($transaction);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal insert/update customer Informa OTP temp";
            return false;
        }

        if (!$tx) {
            $transaction->commit();
        } else {
            $tx = $transaction;
        }

        return true;
    }

    public function validateInformaSubmitOTP($customerId, $accessKey, $accessValue, $accessCode, $type, $tx = false) {
        if (!$this->validateOtpAccessKey($accessKey)) {
            return false;
        }

        try {
            if (!$tx) {
                $manager = new TxManager();
                $manager->setDbService($this->getConnection());
                $transaction = $manager->get();
            } else {
                $transaction = $tx;
            }

            $customerInformaOtpTemp = $this->findFirst($accessKey . " = '" . $accessValue . "' AND type = '". $type ."' AND customer_id ='" . $customerId . "'");

            if (!$customerInformaOtpTemp) {
                $this->errorCode = 400;
                $this->errorMessages = "Tidak mendapatkan OTP untuk " . $accessValue;
                return false;
            } 

            if ($customerInformaOtpTemp->getAccessCode() != $accessCode) {
                $this->errorCode = 400;
                $this->errorMessages = "OTP yang Anda masukkan salah. Harap masukkan OTP yang benar";
                return false;
            }
            
            $kawanLamaSystemLib = new \Library\KawanLamaSystem();
            if (
                $kawanLamaSystemLib->getDiffDateInSecond(
                    $customerInformaOtpTemp->getUpdatedAt(),
                    date("Y-m-d H:i:s")
                ) > 300
            ) {
                $this->errorCode = 400;
                $this->errorMessages = "OTP anda telah expired, mohon melakukan pengiriman ulang.";
                return false;
            }
    
            $validateCount = $customerInformaOtpTemp->getValidateCount() + 1;
            $sql = "UPDATE customer_informa_otp_temp SET validate_count = '" . $validateCount . "', access_code = '' WHERE customer_informa_otp_temp_id = '" . $customerInformaOtpTemp->getCustomerInformaOtpTempId() . "'";

            $this->useWriteConnection();
            $this->getDi()->getShared($this->getConnection())->query($sql);
            $this->setTransaction($transaction);
        } catch (\Exception $exception) {
            $this->errorCode = 400;
            $this->errorMessages = $exception->getMessage() ? $exception->getMessage() : "Gagal insert/update customer Informa OTP temp";
            LogHelper::log("validatePwpSendOTP", "MESSAGE: " . $this->errorMessages);
            return false;
        }

        if (!$tx) {
            $transaction->commit();
        } else {
            $tx = $transaction;
        }

        return $validateCount;

    }


}