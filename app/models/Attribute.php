<?php

namespace Models;

class Attribute extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $attribute_id;

    protected $attribute_unit_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $code;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $label;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected $backend_type;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_global;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_visible;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_searchable;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_filterable;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_comparable;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_visible_on_front;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_html_allowed_on_front;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_used_for_price_rules;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_filterable_in_search;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $used_in_product_listing;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_visible_in_advanced_search;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $position;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $is_used_for_promo_rules;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $search_weight;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('attribute_id', 'Models\Attribute2Set', 'attribute_id', array('alias' => 'Attribute2Set'));
        $this->hasMany('attribute_id', 'Models\AttributeOptionValue', 'attribute_id', array('alias' => 'AttributeOptionValue', "reusable" => true));
        $this->hasMany('attribute_id', 'Models\ProductAttributeInt', 'attribute_id', array('alias' => 'ProductAttributeInt'));
        $this->hasMany('attribute_id', 'Models\ProductAttributeText', 'attribute_id', array('alias' => 'ProductAttributeText'));
        $this->hasMany('attribute_id', 'Models\ProductAttributeVarchar', 'attribute_id', array('alias' => 'ProductAttributeVarchar'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'attribute';
    }

    /**
     * @return int
     */
    public function getAttributeId()
    {
        return $this->attribute_id;
    }

    /**
     * @param int $attribute_id
     */
    public function setAttributeId($attribute_id)
    {
        $this->attribute_id = $attribute_id;
    }

    /**
     * @return mixed
     */
    public function getAttributeUnitId()
    {
        return $this->attribute_unit_id;
    }

    /**
     * @param mixed $attribute_unit_id
     */
    public function setAttributeUnitId($attribute_unit_id)
    {
        $this->attribute_unit_id = $attribute_unit_id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getBackendType()
    {
        return $this->backend_type;
    }

    /**
     * @param string $backend_type
     */
    public function setBackendType($backend_type)
    {
        $this->backend_type = $backend_type;
    }

    /**
     * @return int
     */
    public function getIsGlobal()
    {
        return $this->is_global;
    }

    /**
     * @param int $is_global
     */
    public function setIsGlobal($is_global)
    {
        $this->is_global = $is_global;
    }

    /**
     * @return int
     */
    public function getIsVisible()
    {
        return $this->is_visible;
    }

    /**
     * @param int $is_visible
     */
    public function setIsVisible($is_visible)
    {
        $this->is_visible = $is_visible;
    }

    /**
     * @return int
     */
    public function getIsSearchable()
    {
        return $this->is_searchable;
    }

    /**
     * @param int $is_searchable
     */
    public function setIsSearchable($is_searchable)
    {
        $this->is_searchable = $is_searchable;
    }

    /**
     * @return int
     */
    public function getIsFilterable()
    {
        return $this->is_filterable;
    }

    /**
     * @param int $is_filterable
     */
    public function setIsFilterable($is_filterable)
    {
        $this->is_filterable = $is_filterable;
    }

    /**
     * @return int
     */
    public function getIsComparable()
    {
        return $this->is_comparable;
    }

    /**
     * @param int $is_comparable
     */
    public function setIsComparable($is_comparable)
    {
        $this->is_comparable = $is_comparable;
    }

    /**
     * @return int
     */
    public function getIsVisibleOnFront()
    {
        return $this->is_visible_on_front;
    }

    /**
     * @param int $is_visible_on_front
     */
    public function setIsVisibleOnFront($is_visible_on_front)
    {
        $this->is_visible_on_front = $is_visible_on_front;
    }

    /**
     * @return int
     */
    public function getIsHtmlAllowedOnFront()
    {
        return $this->is_html_allowed_on_front;
    }

    /**
     * @param int $is_html_allowed_on_front
     */
    public function setIsHtmlAllowedOnFront($is_html_allowed_on_front)
    {
        $this->is_html_allowed_on_front = $is_html_allowed_on_front;
    }

    /**
     * @return int
     */
    public function getIsUsedForPriceRules()
    {
        return $this->is_used_for_price_rules;
    }

    /**
     * @param int $is_used_for_price_rules
     */
    public function setIsUsedForPriceRules($is_used_for_price_rules)
    {
        $this->is_used_for_price_rules = $is_used_for_price_rules;
    }

    /**
     * @return int
     */
    public function getIsFilterableInSearch()
    {
        return $this->is_filterable_in_search;
    }

    /**
     * @param int $is_filterable_in_search
     */
    public function setIsFilterableInSearch($is_filterable_in_search)
    {
        $this->is_filterable_in_search = $is_filterable_in_search;
    }

    /**
     * @return int
     */
    public function getUsedInProductListing()
    {
        return $this->used_in_product_listing;
    }

    /**
     * @param int $used_in_product_listing
     */
    public function setUsedInProductListing($used_in_product_listing)
    {
        $this->used_in_product_listing = $used_in_product_listing;
    }

    /**
     * @return int
     */
    public function getIsVisibleInAdvancedSearch()
    {
        return $this->is_visible_in_advanced_search;
    }

    /**
     * @param int $is_visible_in_advanced_search
     */
    public function setIsVisibleInAdvancedSearch($is_visible_in_advanced_search)
    {
        $this->is_visible_in_advanced_search = $is_visible_in_advanced_search;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getIsUsedForPromoRules()
    {
        return $this->is_used_for_promo_rules;
    }

    /**
     * @param int $is_used_for_promo_rules
     */
    public function setIsUsedForPromoRules($is_used_for_promo_rules)
    {
        $this->is_used_for_promo_rules = $is_used_for_promo_rules;
    }

    /**
     * @return int
     */
    public function getSearchWeight()
    {
        return $this->search_weight;
    }

    /**
     * @param int $search_weight
     */
    public function setSearchWeight($search_weight)
    {
        $this->search_weight = $search_weight;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Attribute[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Attribute
     */
    public static function findFirst($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}
