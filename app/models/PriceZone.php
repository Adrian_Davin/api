<?php

namespace Models;

class PriceZone extends \Models\BaseModel
{

    protected $data;

    protected $errors;

    protected $messages;


    /**
     *
     * @var string
     * @Primary
     * @Column(type="string", length=45, nullable=false)
     */
    public $sku;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=4, nullable=false)
     */
    public $zone_id;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    public $sale_price;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $special_from_date;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }



    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $special_to_date;

    public function initialize()
    {
        $this->belongsTo('sku', 'Models\ProductVariant', 'sku', array('alias' => 'ProductVariant', "reusable" => true));
        $this->belongsTo('zone_id', 'Models\Store', 'zone_id', array('alias' => 'Store', "reusable" => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'price_zone';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PriceZone[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PriceZone
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function importBatch($data = array()){
        try {

            $sql = "insert into price_zone (zone_id, sale_price, special_from_date, special_to_date) VALUES ".$data['values'];
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            if(!$result){
                \Helpers\LogHelper::log("price zone", 'There have something error in update data');
                $this->setErrors(array('code' => 'RR303','title' => 'price zone','message' => 'There have something error in update data'));
                $this->setData(array('affected_row' => 0));
            }else{
                $this->setMessages(array('Successfully Import Batch'));
                $this->setData(array('affected_row' => $this->getDi()->getShared('dbMaster')->affectedRows()));
                $this->setErrors(array());
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("price zone", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'price zone','message' => 'There have something error, in query import batch'));
            $this->setData(array('affected_row' => 0));
        }
    }

    public function updateBatch($params = array()){
        try {
            $arr_sku = array();
            foreach($params['data'] as $row){
                $arr_sku[] = "'".$row['sku']."'";
            }

            $result_exist = $this->find(
                array(
                    'conditions' => " sku in (".implode(',',$arr_sku).") and zone_id = ".$params['zone_id']
                )
            );

            $arr_sku = array();
            if($result_exist){
                foreach($result_exist->toArray() as $row){
                    $arr_sku[] = $row['sku'];
                }
            }


            $sql_insert = " insert into price_zone (zone_id, sku, sale_price, special_from_date, special_to_date) values ";
            $arr_values = array();


            $sql_update = "UPDATE price_zone set ";
            $sql_sale_price = " sale_price = CASE sku ";
            $sql_special_from_date = " special_from_date = CASE sku ";
            $sql_special_to_date = " special_to_date = CASE sku ";
            $arr_sku1 = array();
            foreach($params['data'] as $row){
                if(in_array($row['sku'], $arr_sku)){ // Update

                    $sql_sale_price .= " WHEN '".$row['sku']."' THEN ".(($row['special_price'] < 5000)? $row['price']:$row['special_price']);
                    $sql_special_from_date .= " WHEN '".$row['sku']."' THEN ".((empty($row['special_from_date']))? 'NULL' : "'".$row['special_from_date']."'")." ";
                    $sql_special_to_date .= " WHEN '".$row['sku']."' THEN ".((empty($row['special_to_date']))? 'NULL' : "'".$row['special_to_date']."'")." ";
                    $arr_sku1[] = "'".$row['sku']."'";
                }else{ // Insert
                    $arr_values[] = " (".$params['zone_id'].",'".$row['sku']."',".(($row['special_price'] < 5000)? $row['price']:$row['special_price']).",".((empty($row['special_from_date']))? 'NULL' : "'".$row['special_from_date']."'").",
                                        ".((empty($row['special_to_date']))? 'NULL' : "'".$row['special_to_date']."'").")";
                }
            }
            $sku = implode(',',$arr_sku1);
            $sql_update = $sql_update .
                $sql_sale_price . " ELSE sale_price END," .
                $sql_special_from_date . " ELSE special_from_date END," .
                $sql_special_to_date . " ELSE special_to_date END " .
                " WHERE sku in (".$sku.") and zone_id = ".$params['zone_id'];

            $sql_insert = $sql_insert . implode(',',$arr_values);

            $affected_row_update = 0;
            $affected_row_insert = 0;
            if($result_exist){
                if(!empty($result_exist->toArray())){
                    $result_update = $this->getDi()->getShared('dbMaster')->execute($sql_update);
                    $affected_row_update = $this->getDi()->getShared('dbMaster')->affectedRows();
                }
            }

            if(count($arr_sku1) < count($params['data'])){
                $result_insert = $this->getDi()->getShared('dbMaster')->execute($sql_insert);
                $affected_row_insert = $this->getDi()->getShared('dbMaster')->affectedRows();
            }

            $this->setMessages(array('Successfully Update Batch'));
            $this->setData(array('affected_row_insert' => $affected_row_insert, 'affected_row_update' => $affected_row_update));
            $this->setErrors(array());

        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("price zone", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'price zone','message' => 'There have something error, in query import batch'));
            $this->setData(array('affected_row_insert' => 0,'affected_row_update' => 0));
        }
    }

}
