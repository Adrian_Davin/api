<?php

namespace Models;

use Helpers\LogHelper;
class SalesPendingOrder extends \Models\BaseModel
{
 
    protected $pending_order_id;
    protected $parent_order_no;

    public function getSource()
    {
        return 'sales_pending_order';
    }
    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {        

    }  

    public function setFromArray($dataArray = array())
    {
        foreach($dataArray as $key => $val) {
            $this->{$key} = $val;
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getPendingOrderID()
    {
        return $this->pending_order_id;
    }

    public function getParentOrderNo()
    {
        return $this->parent_order_no;
    }

    public function setPendingOrderID($pending_order_id)
    {
        $this->pending_order_id = $pending_order_id;
    }

    public function setParentOrderNo($parent_order_no)
    {
        $this->parent_order_no = $parent_order_no;
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}