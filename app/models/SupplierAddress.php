<?php

namespace Models;

#use Library\Redis\MasterKecamatan;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class SupplierAddress extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $supplier_address_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $supplier_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $city_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $province_id;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $province_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $kecamatan_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $kecamatan_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $kelurahan_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $supplier_address_type;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=true)
     */
    protected $address_point;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $address;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $phone;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $fax;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $instant_courier_status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('supplier_address_id', 'Models\Store', 'supplier_address_id', array('alias' => 'Store'));
        $this->belongsTo('supplier_id', 'Models\Supplier', 'supplier_id', array('alias' => 'Supplier'));

        $this->belongsTo('city_id', 'Models\MasterCity', 'city_id', array('alias' => 'MasterCity', "reusable" => true));
        $this->belongsTo('province_code', 'Models\MasterProvince', 'province_code', array('alias' => 'MasterProvince', "reusable" => true));
        $this->belongsTo('kecamatan_code', 'Models\MasterKecamatan', 'kecamatan_code', array('alias' => 'MasterKecamatan', "reusable" => true));
        $this->belongsTo('kelurahan_id', 'Models\MasterKelurahan', 'kelurahan_id', array('alias' => 'MasterKelurahan', "reusable" => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'supplier_address';
    }

    /**
     * @return int
     */
    public function getSupplierAddressId()
    {
        return $this->supplier_address_id;
    }

    /**
     * @param int $supplier_address_id
     */
    public function setSupplierAddressId($supplier_address_id)
    {
        $this->supplier_address_id = $supplier_address_id;
    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param int $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * @param int $city_id
     */
    public function setCityId($city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @return string
     */
    public function getProvinceCode()
    {
        return $this->province_code;
    }

    /**
     * @param string $province_code
     */
    public function setProvinceCode($province_code)
    {
        $this->province_code = $province_code;
    }

    /**
     * @return string
     */
    public function getKecamatanCode()
    {
        return $this->kecamatan_code;
    }

    /**
     * @param string $kecamatan_code
     */
    public function setKecamatanCode($kecamatan_code)
    {
        $this->kecamatan_code = $kecamatan_code;
    }

    /**
     * @return int
     */
    public function getKelurahanID()
    {
        return $this->kelurahan_id;
    }

    /**
     * @param int $kelurahan_id
     */
    public function setKelurahanID($kelurahan_id)
    {
        $this->kelurahan_id = $kelurahan_id;
    }

    /**
     * @return int
     */
    public function getSupplierAddressType()
    {
        return $this->supplier_address_type;
    }

    /**
     * @param int $supplier_address_type
     */
    public function setSupplierAddressType($supplier_address_type)
    {
        $this->supplier_address_type = $supplier_address_type;
    }

    /**
     * @return string
     */
    public function getAddressPoint()
    {
        return $this->address_point;
    }

    /**
     * @param string $address_point
     */
    public function setAddressPoint($address_point)
    {
        $this->address_point = $address_point;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getInstantCourierStatus()
    {
        return $this->instant_courier_status;
    }

    /**
     * @param int $instant_courier_status
     */
    public function setInstantCourierStatus($instant_courier_status)
    {
        $this->instant_courier_status = $instant_courier_status;
    }
    

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {

            $this->{$key} = $val;

            if (property_exists($this, $key)) {
                if($key=="province_id") {
                    $provinceParams = array(
                        "province_id" => $val
                    );
                    $province = new MasterProvince();
                    $dataProvince = $province->getData($provinceParams);
                    $this->province_code = $dataProvince['province_code'];

                }elseif($key=="kecamatan_id"){
                    $kecamatanParams = array(
                        "kecamatan_id" => $val
                    );
                    $kecamatan = new \Models\MasterKecamatan();
                    $dataKecamatan = $kecamatan->getData($kecamatanParams);
                    $this->kecamatan_code = $dataKecamatan['kecamatan_code'];
                }
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {

        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        unset($view['city_id']);
        unset($view['province_code']);
        unset($view['kecamatan_code']);
        unset($view['kelurahan_id']);

        if(!empty($this->city_id)) {
            $view['city'] = $this->MasterCity->toArray(['city_id','city_name']);
        }

        if(!empty($this->province_code)) {
            $view['province'] = $this->MasterProvince->toArray(['province_id','province_code','province_name']);
        }

        if(!empty($this->kecamatan_code)) {
            $view['kecamatan'] = $this->MasterKecamatan->toArray(['kecamatan_id','kecamatan_name', 'coding', 'kecamatan_code','kode_jalur','sap_exp_code']);
        }

        if(!empty($this->kelurahan_id)) {
            $view['kelurahan'] = $this->MasterKelurahan->toArray(['kelurahan_id', 'kelurahan_name', 'post_code', 'geolocation']);
        }

        unset($view['created_at']);
        unset($view['updated_at']);

        return $view;
    }

    public function saveAddress(){
        try {

            $saveStatus = $this->saveData("supplier_address");
            $action = $this->action;

            if(empty($this->supplier_id)) {
                $lastSupplier = parent::findFirst(array(
                    "columns" => "supplier_address_id",
                    "order" => "supplier_address_id DESC"
                ));

                $this->supplier_address_id   = $lastSupplier['supplier_address_id'];
            }else{
                $this->supplier_address_id   = $this->supplier_address_id;
            }

            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("Supplier", "Supplier Account save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Save/Update Supplier Account failed";

                $transaction->rollback(
                    "Save/Update Supplier Account failed"
                );
                return false;
            }
            $transaction->commit();
            return $this->supplier_address_id;
        } catch (TxFailed $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }

}
