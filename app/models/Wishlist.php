<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 19/09/2017
 * Time: 08:30 AM
 */

namespace Models;

use \Library\Mongodb;

/**
 * Class Wishlist
 * @package Models
 */

class Wishlist
{
    protected $wishlist_id;
    protected $email;
    protected $sku;
    protected $product_name;
    protected $selling_price;
    protected $is_in_stock;
    protected $added_date;
    protected $employees;
    protected $is_exist;
    protected $is_product_scanned;
    protected $store_code_new_retail;
    protected $review;

    protected $minimum_order;

    protected $is_apply_multiple;

    /**
     * @return mixed
     */
    public function getIsExist()
    {
        return $this->is_exist;
    }

    /**
     * @param mixed $is_exist
     */
    public function setIsExist($is_exist)
    {
        $this->is_exist = $is_exist;
    }

    /**
     * @return mixed
     */
    public function getWishlistId()
    {
        return $this->wishlist_id;
    }

    /**
     * @param mixed $wishlist_id
     */
    public function setWishlistId($wishlist_id)
    {
        $this->wishlist_id = $wishlist_id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->product_name;
    }

    /**
     * @param mixed $product_name
     */
    public function setProductName($product_name)
    {
        $this->product_name = $product_name;
    }

    /**
     * @return mixed
     */
    public function getSellingPrice()
    {
        return $this->selling_price;
    }

    /**
     * @param mixed $selling_price
     */
    public function setSellingPrice($selling_price)
    {
        $this->selling_price = $selling_price;
    }

    /**
     * @return mixed
     */
    public function getIsInStock()
    {
        return $this->is_in_stock;
    }

    /**
     * @param mixed $is_in_stock
     */
    public function setIsInStock($is_in_stock)
    {
        $this->is_in_stock = $is_in_stock;
    }

    /**
     * @return mixed
     */
    public function getAddedDate()
    {
        return $this->added_date;
    }

    /**
     * @param mixed $added_date
     */
    public function setAddedDate($added_date)
    {
        $this->added_date = $added_date;
    }

    public function getReview(){
        return $this->review;
    }

    public function setReview($review)
    {
        $this->review = $review;
    }

    /**
     * @return mixed
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployees($employees)
    {
        $this->employee = $employees;
    }


    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->error_messages;
    }

    public function getMinimumOrder()
    {
        return $this->minimum_order;
    }

    /**
     * @param mixed $minimum_order
     */
    public function setMinimumOrder($minimum_order)
    {
        $this->minimum_order = $minimum_order;
    }

     public function getIsApplyMultiple()
    {
        return $this->is_apply_multiple;
    }

    /**
     * @param mixed $is_apply_multiple
     */
    public function setIsApplyMultiple($is_apply_multiple)
    {
        $this->is_apply_multiple = $is_apply_multiple;
    }



    /**
     * mongo db Collections
     * @var $collection
     */
    protected $collection;
    protected $error_code;
    protected $error_messages = [];

    public function __construct()
    {
        $mongoDb = new Mongodb();
        $mongoDb->setCollection("wishlist");
        $this->collection = $mongoDb->getCollection();
    }

    public function getWishlistData($params = array())
    {
        $data = array();
        $cursor = array();
        $storeCodeNewRetail = isset($params['store_code_new_retail']) && !empty($params["store_code_new_retail"]) ? $params['store_code_new_retail'] : "";

        if (isset($params['email'])) {
            $email = (string)$params['email'];
            $matchQuery = array(
                'email' => $email
            );
    
            if ($storeCodeNewRetail !== "") {
                $matchQuery['items.store_code_new_retail'] = $storeCodeNewRetail;
            }

            if (isset($params['email']) && isset($params['product_name']) && isset($params['limit']) && isset($params['offset'])) {
                $params['product_name'] = strtoupper($params['product_name']);
                $condition = array(
                    array('$unwind' => '$items'),
                    array(
                        '$match' => array(
                            'email' => $email,
                            'items.product_name' => new \MongoDB\BSON\Regex($params['product_name'])
                        )
                    ),
                    array('$unwind' => '$items'),
                    array('$skip' => (int)$params['offset']),
                    array('$limit' => (int)$params['limit'])
                );
                $cursor = $this->collection->aggregate($condition);
            } else if (isset($params['email']) && isset($params['limit']) && isset($params['offset'])) {
                // by Aaron: to provide pagination based on user email without product's name
                // HERE

                $condition = array(
                    array('$unwind' => '$items'),
                    array(
                        '$match' => $matchQuery
                    ),
                    array('$unwind' => '$items'),
                    array(
                        '$sort' => array(
                            'items.added_date' => -1
                        )
                    ),
                    array('$skip' => (int)$params['offset']),
                    array('$limit' => (int)$params['limit'])
                );
                $cursor = $this->collection->aggregate($condition);
            } else if (isset($params['email']) && isset($params['product_name'])) {
                $params['product_name'] = strtoupper($params['product_name']);
                $condition = array(
                    array('$unwind' => '$items'),
                    array(
                        '$match' => array(
                            'email' => $email,
                            'items.product_name' => new \MongoDB\BSON\Regex($params['product_name'])
                        )
                    ),
                    array('$unwind' => '$items')
                );
                $cursor = $this->collection->aggregate($condition);
            } else {
                // HERE
                $condition = array(
                    array('$unwind' => '$items'),
                    array(
                        '$match' => $matchQuery
                    ),
                    array('$unwind' => '$items'),
                    array(
                        '$sort' => array(
                            'items.added_date' => -1
                        )
                    )
                );
                $cursor = $this->collection->aggregate($condition);
            }
        } else {
            if (isset($params['product_name']) && isset($params['limit']) && isset($params['offset'])) {
                $params['product_name'] = strtoupper($params['product_name']);
                $condition = array(
                    array('$unwind' => '$items'),
                    array(
                        '$match' => array(
                            'items.product_name' => new \MongoDB\BSON\Regex($params['product_name'])
                        )
                    ),
                    array('$unwind' => '$items'),
                    array('$skip' => (int)$params['offset']),
                    array('$limit' => (int)$params['limit'])
                );
                $cursor = $this->collection->aggregate($condition);
            } else if (isset($params['product_name'])) {
                $params['product_name'] = strtoupper($params['product_name']);
                $condition = array(
                    array('$unwind' => '$items'),
                    array(
                        '$match' => array(
                            'items.product_name' => new \MongoDB\BSON\Regex($params['product_name'])
                        )
                    ),
                    array('$unwind' => '$items')
                );
                $cursor = $this->collection->aggregate($condition);
            } else {
                $condition = array(
                    array('$unwind' => '$items')
                );
                $cursor = $this->collection->aggregate($condition);
            }
        }

        // total count - temp
        $totalCondition = array(
            array('$unwind' => '$items'),
            array(
                '$match' => $matchQuery
            ),
            array(
                '$count' => "sku"
            )
        );

        $totalCursor = $this->collection->aggregate($totalCondition);
        $totalData = 0;

        foreach ($totalCursor as $row) {
            $totalData = $row['sku'];
        }
        // end here
        foreach ($cursor as $row2) {
            if (count($row2) > 0) {
                $wishlist_oid = (string)$row2['_id'];
                unset($row2['_id']);
                $row2 = array("wishlist_id" => $wishlist_oid, "total" => $totalData) + $row2;
                $data[] = $row2;
            }
        }

        return $data;
    }

    public function getWishlistList($params = array())
    {
        $data = array('data' => array(),'recordsTotal' => 0 );
        if (isset($params['limit']) && isset($params['offset'])) {
            $condition = array(
                array('$skip' => (int)$params['offset']),
                array('$limit' => (int)$params['limit'])
            );

            if (isset($params['email_keyword'])) {
                if (!empty($params['email_keyword'])) {
                    $condition = array(
                        array('$match' => array('email' => array('$regex' => ".*" . $params['email_keyword'] . ".*"))),
                        array('$skip' => (int)$params['offset']),
                        array('$limit' => (int)$params['limit'])
                    );

                    $conditionCount = array(
                        array('$match' => array('email' => array('$regex' => ".*" . $params['email_keyword'] . ".*"))),
                    );
                    $countResult = count($this->collection->aggregate($conditionCount)->toArray());
                }
            }

            $result = $this->collection->aggregate($condition)->toArray();

            $countAllData = (isset($countResult)) ? $countResult : $this->collection->count();
            if (count($result) > 0) {
                $data['data'] = $result;
                $data['recordsTotal'] = $countAllData;
            }
        }

        return $data;
    }

    /**
     * I make this function because if items is empty,
     * even if the email is exist it always return empty array
     *
     * @param array $params
     * @return array
     */
    public function getWishlistDataByEmail($params = array())
    {
        $data = array();
        if(isset($params['email']) && !empty($params['email'])) {
            $email = (string)$params['email'];
            $condition = array(
                array(
                    '$match' => array(
                        'email' => $email
                    )
                ),
            );
            $cursor = $this->collection->aggregate($condition);

            foreach ($cursor as $row) {
                if (count($row) > 0) {
                    $wishlist_oid = (string)$row['_id'];
                    unset($row['_id']);
                    $row = array("wishlist_id" => $wishlist_oid) + $row;
                    $data[] = $row;
                }
            }
        }

        return $data;
    }

    /**
     * Set object properties from $data_array
     * @param array $data_array array containing data to be set to object properties
     */
    public function setFromArray($params = array())
    {
        foreach($params as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);

        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) == 'object') {
                if($key == 'collection') continue;
                $view[$key] = $thisArray[$key]->getDataArray(); // get everything
            } else if(strpos($key, "_") !== 0) {
                $view[$key] = $val;
            }
        }

        unset($view['error_messages']);
        unset($view['error_code']);

        return $view;
    }

    public function saveWishlist()
    {
        if (!$this->is_exist) {
            // email not found, insert new data
            $insertParams['email'] = $this->email;
            $insertParams['items'][0]['sku'] = $this->sku;
            $insertParams['items'][0]['product_name'] = $this->product_name;
            $insertParams['items'][0]['selling_price'] = $this->selling_price;
            $insertParams['items'][0]['is_product_scanned'] = $this->is_product_scanned;
            $insertParams['items'][0]['store_code_new_retail'] = $this->store_code_new_retail;
            $insertParams['items'][0]['is_in_stock'] = $this->is_in_stock;
            $insertParams['items'][0]['added_date'] = $this->added_date;
            $insertParams['items'][0]['review'] = $this->review;
            $insertParams['items'][0]['employees'] = !empty($this->employees) ? $this->employees : array();
            $insertParams['items'][0]['minimum_order'] = $this->minimum_order;
            $insertParams['items'][0]['is_apply_multiple'] = $this->is_apply_multiple;
            
            $saveWishlist = $this->collection->insertOne($insertParams);
        } else {
            // email found
            // Check if sku already exist
            $email = (string)$this->email;
            $condition = [
                ['$unwind' => '$items'],
                ['$match' => ['email' => $email, 'items.sku' => $this->sku]],
                ['$project' => ['_id' => 1, 'email' => 1, 'detail' => '$items']],
                ['$limit' => 1]
            ];
            $cursor = $this->collection->aggregate($condition);
            $existingItem = [];
            foreach ($cursor as $row) {
                if (count($row) > 0) {
                    $row["wishlist_id"] = (string) $row['_id'];
                    $flattenItem = array_merge($row, $row['detail']);
                    unset($flattenItem['_id'], $flattenItem['detail']);
                    $existingItem = $flattenItem;
                }
            }

            if (!empty($existingItem)) {
                // sku already exist, delete it first
                $delete = array('$pull' => array('items' => array('sku' => $this->sku)));
                $filter = array("email" => $this->email);
                $deleteWishlist = $this->collection->updateOne($filter, $delete);
                if (!$deleteWishlist) {
                    $result['errors'] = "Failed to save data";
                    return $result;
                }
            }

            // insert the item
            $insertParams['sku'] = $this->sku;
            $insertParams['product_name'] = $this->product_name;
            $insertParams['is_product_scanned'] = $this->is_product_scanned;
            $insertParams['store_code_new_retail'] = $this->store_code_new_retail;
            $insertParams['selling_price'] = $this->selling_price;
            $insertParams['is_in_stock'] = $this->is_in_stock;
            $insertParams['minimum_order'] = $this->minimum_order;
            $insertParams['is_apply_multiple'] = $this->is_apply_multiple;
            $insertParams['added_date'] = $this->added_date;
            $insertParams['review'] = $this->review;
            $existingItemEmployees = !empty($existingItem['employees']) ? $existingItem['employees'] : [];
            $insertParams['employees'] = empty($existingItemEmployees) && !empty($this->employees) ? $this->employees : $existingItemEmployees;

            $update = array('$push' => array('items' => $insertParams));
            $filter = array("email" => $this->email);
            $saveWishlist = $this->collection->updateOne($filter, $update);
        }

        if (!$saveWishlist) {
            $result['errors'] = "Failed to save data";
        } else {
            $result['message'] = array("success");
        }

        return $result;
    }

    public function deleteWishlist()
    {
        $delete = array('$pull' => array('items' => array('sku' => $this->sku)));
        $filter = array("email" => $this->email);
        $deleteWishlist = $this->collection->updateOne($filter, $delete);
        if (!$deleteWishlist) {
            $result['errors'] = "Failed to delete data";
        } else {
            $result['message'] = array("success");
        }

        return $result;
    }

    public function getEmailByCustomerId($customerId = FALSE)
    {
        $email = '';
        if ($customerId) {
            $customerModel = new \Models\Customer();
            $custArray = $customerModel->getCustomerByCustomerId($customerId);
            if (count($custArray) > 0) {
                $email = $custArray[0]['email'];
            }
        }

        return $email;
    }
    
    public function invalidateCustomerWishlistCache($customer_id)
    {
        //init models and library
        $redis = new \Library\Redis();
        $redisCon = $redis::connect();

        $redisKey = sprintf("wishlist_customer_id:%s", $customer_id);
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator, $redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
        return $result;
    }
}