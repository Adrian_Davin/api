<?php
namespace Models;

use Phalcon\Db as Database;

class MarketingAdminRole extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $role_id;

    /**
     *
     * @var string
     */
    protected $role_name;

    /**
     *
     * @var string
     */
    protected $module_access;

    /**
     *
     * @var integer
     */
    protected $status;

    /**
     * Method to set the value of field role_id
     *
     * @param integer $role_id
     * @return $this
     */
    public function setRoleId($role_id)
    {
        $this->role_id = $role_id;

        return $this;
    }

    /**
     * Method to set the value of field role_name
     *
     * @param string $role_name
     * @return $this
     */
    public function setRoleName($role_name)
    {
        $this->role_name = $role_name;

        return $this;
    }

    /**
     * Method to set the value of field module_access
     *
     * @param string $module_access
     * @return $this
     */
    public function setModuleAccess($module_access)
    {
        $this->module_access = $module_access;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field role_id
     *
     * @return integer
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * Returns the value of field role_name
     *
     * @return string
     */
    public function getRoleName()
    {
        return $this->role_name;
    }

    /**
     * Returns the value of field module_access
     *
     * @return string
     */
    public function getModuleAccess()
    {
        return $this->module_access;
    }

    /**
     * Returns the value of field status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function initialize()
    {
        
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'marketing_admin_role';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MarketingAdminRole[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MarketingAdminRole
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getRoleAttribute($params)
    {
        $response = array();
        try {
            $sql = "SELECT 
                    role_id, role_name, module_access, status
                FROM marketing_admin_role
                WHERE role_id = '{$params['role_id']}'";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $getArr = $result->fetchAll();
                foreach ($getArr as $key => $item) {
                    $data[$key] = $item;
                }

                $response['data'] = $data[0];
                $response['messages'] = array("success");
            }
        }  catch (\Exception $e) {
            $response['error'] = $e->getMessage();

            \Helpers\LogHelper::log("login", "Get marketing user data failed, error : " . $e->getMessage());

            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return $response;
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }
}
