<?php
/**
 * Created by PhpStorm.
 * User: Aaron Putra
 * Date: 04/05/2017
 * Time: 15.57
 */

namespace Models;


class AdminUserLog extends \Models\BaseModel
{
    protected $id_user_log;
    protected $master_app_id;
    protected $admin_user_id;
    protected $action_group;
    protected $action;
    protected $ip_address;
    protected $result;
    protected $full_action_name;
    protected $short_details;
    protected $full_details;
    protected $created_at;

    /**
     * @return mixed
     */
    public function getMasterAppId()
    {
        return $this->master_app_id;
    }

    /**
     * @param mixed $master_app_id
     */
    public function setMasterAppId($master_app_id)
    {
        $this->master_app_id = $master_app_id;
    }

    /**
     * @return mixed
     */
    public function getAdminUserId()
    {
        return $this->admin_user_id;
    }

    /**
     * @param mixed $admin_user_id
     */
    public function setAdminUserId($admin_user_id)
    {
        $this->admin_user_id = $admin_user_id;
    }

    /**
     * @return mixed
     */
    public function getActionGroup()
    {
        return $this->action_group;
    }

    /**
     * @param mixed $action_group
     */
    public function setActionGroup($action_group)
    {
        $this->action_group = $action_group;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * @param mixed $ip_address
     */
    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getFullActionName()
    {
        return $this->full_action_name;
    }

    /**
     * @param mixed $full_action_name
     */
    public function setFullActionName($full_action_name)
    {
        $this->full_action_name = $full_action_name;
    }

    /**
     * @return mixed
     */
    public function getShortDetails()
    {
        return $this->short_details;
    }

    /**
     * @param mixed $short_details
     */
    public function setShortDetails($short_details)
    {
        $this->short_details = $short_details;
    }

    /**
     * @return mixed
     */
    public function getFullDetails()
    {
        return $this->full_details;
    }

    /**
     * @param mixed $full_details
     */
    public function setFullDetails($full_details)
    {
        $this->full_details = $full_details;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    public function getSource()
    {
        return 'admin_user_log';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setUserLogFromArray($params = array()){

        $temp = array();

        foreach ($params as $event => $val){


            if (property_exists($this, $event)) {
                if($event == "full_details"){
                    $this->full_details = json_encode($val);
                }
                else {
                    $this->{$event} = $val;
                }
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }


}