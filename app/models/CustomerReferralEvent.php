<?php

namespace Models;

// use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
// use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerReferralEvent extends \Models\BaseModel
{
  /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $customer_referral_event_id;

    protected $referral_customer_id;

    protected $invited_customer_id;
    
    protected $event_name;

    protected $order_no;

    protected $reward_status;

    protected $voucher_id;

    protected $created_at;

    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_referral_event';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return int 
     */
    public function getCustomerReferralEventId()
    {
        return $this->customer_referral_event_id;
    }

    /**
     * @param int $customer_referral_id
     */
    public function setCustomerReferralEventId($customer_referral_event_id)
    {
        $this->customer_referral_event_id = $customer_referral_event_id;
    }

    /**
     * @return int
     */
    public function getReferralCustomerId() 
    {
        return $this->referral_customer_id;
    } 

    /**
     * @param int $referral_customer_Id
     */
    public function setReferralCustomerId($referral_customer_Id)
    {
        $this->referral_customer_Id = $referral_customer_Id;
    }

     /**
     * @return int
     */
    public function getInvitedCustomerId() 
    {
        return $this->invited_customer_id;
    } 

    /**
     * @param int $invited_customer_id
     */
    public function setInvitedCustomerId($invited_customer_id)
    {
        $this->invited_customer_id = $invited_customer_id;
    }

    public function getEventName()
    {
        return $this->event_name;
    }

    public function setEventName($event_name)
    {
        $this->event_name = $event_name;
    }

    public function getOrderNo()
    {
        return $this->order_no;
    }

    public function setOrderNo($order_no)
    {
        $this->order_no = $order_no;
    }

     /**
     * @return int
     */
    public function getRewardStatus()
    {
        return $this->reward_status;
    }

     /**
     * @param int $reward_status
     */
    public function setRewardStatus($reward_status)
    {
        $this->reward_status = $reward_status;
    }

    /**
     * @return int
     */
    public function getVoucherId() 
    {
        return $this->voucher_id;
    } 

    /**
     * @param int $voucher_id
     */
    public function setVoucherId($voucher_id)
    {
        $this->voucher_id = $voucher_id;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
          if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
  }

  public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}