<?php

namespace Models;

class MasterKelurahan extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $kelurahan_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $kecamatan_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $kelurahan_name;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $kelurahan_code;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('kelurahan_id', 'Models\CustomerAddress', 'kelurahan_id', array('alias' => 'CustomerAddress'));
        $this->hasMany('kelurahan_id', 'Models\SalesOrderAddress', 'kelurahan_id', array('alias' => 'SalesOrderAddress'));
        $this->belongsTo('kecamatan_id', 'Models\MasterKecamatan', 'kecamatan_id', array('alias' => 'MasterKecamatan'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_kelurahan';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
    }

    public function getDataList($params = array())
    {
        $countries = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterKelurahan::deleteKelurahans();
        } else {
            $countries = \Library\Redis\MasterKelurahan::getKelurahanList();
        }

        if(empty($countries)) {
            $result = $this->find();

            if($result) {
                $countriesRedis = array();
                $countries = array();
                foreach($result as $Kelurahan) {
                    $KelurahanArray = $Kelurahan->toArray();
                    $countriesRedis[$Kelurahan->kelurahan_id] = $KelurahanArray;
                    $countries[] = $KelurahanArray;
                }

                // save to redis
                \Library\Redis\MasterKelurahan::setKelurahans($countriesRedis);
            }
        }


        return $countries;
    }

    public function getData($params = array())
    {
        $Kelurahan = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterKelurahan::deleteKelurahan($params['kelurahan_id']);
        } else {
            $Kelurahan = \Library\Redis\MasterKelurahan::getKelurahanData($params['kelurahan_id']);
        }

        if(empty($Kelurahan)) {
            $KelurahanData = $this->findFirst("kelurahan_id = '".$params['kelurahan_id']."'");

            if($KelurahanData) {
                $Kelurahan = $KelurahanData->toArray();
                \Library\Redis\MasterKelurahan::setKelurahan($params['kelurahan_id'],$Kelurahan);
            }
        }

        return $Kelurahan;
    }
}
