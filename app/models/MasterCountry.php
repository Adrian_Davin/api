<?php

namespace Models;

use Phalcon\Mvc\Model;

/**
 * @property \Phalcon\Mvc\Model\Resultset|Model assignation
 */
class MasterCountry extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $country_id;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $country_code;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $country_name;



    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('country_id', 'Models\CustomerAddress', 'country_id', array('alias' => 'CustomerAddress'));
        $this->hasMany('country_id', 'Models\MasterProvince', 'country_id', array('alias' => 'MasterProvince'));
        $this->hasMany('country_id', 'Models\SalesOrderAddress', 'country_id', array('alias' => 'SalesOrderAddress'));
    }

    public function onConstruct()
    {
        parent::onConstruct();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_country';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {

        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
    }

    public function getDataList($params = array())
    {
        $countries = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterCountry::deleteCountries();
        } else {
            $countries = \Library\Redis\MasterCountry::getCountryList();
        }

        if(empty($countries)) {
            $result = $this->find();

            if($result) {
                $countriesRedis = array();
                $countries = array();
                foreach($result as $country) {
                    $countryArray = $country->toArray();
                    $countryArray['province'] = $country->MasterProvince->toArray();
                    $countriesRedis[$country->country_id] = $countryArray;
                    $countries[] = $countryArray;
                }

                // save to redis
               \Library\Redis\MasterCountry::setCountries($countriesRedis);
            }
        }


        return $countries;
    }

    public function getData($params = array())
    {
        $country = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterCountry::deleteCountry($params['country_id']);
        } else {
            $country = \Library\Redis\MasterCountry::getCountryData($params['country_id']);
        }

        if(empty($country)) {
            $countryData = $this->findFirst("country_id = '".$params['country_id']."'");

            if($countryData) {
                $country = $countryData->toArray();
                $country['province'] = $countryData->MasterProvince->toArray();
                \Library\Redis\MasterCountry::setCountry($params['country_id'],$country);
            }
        }

        return $country;
    }

}
