<?php

namespace Models;

use Phalcon\Db;

class PriceZoneGideon extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sku;
    protected $zone_id;
    protected $start_date;
    protected $end_date;
    protected $sale_price;
    protected $file_csv;
    protected $created_user;
    protected $created_at;

    protected $string_value;

    protected $errorCode;
    protected $errorMessages;
    protected $data;

    /**
     * @return mixed
     */
    public function getStringValue()
    {
        return $this->string_value;
    }

    /**
     * @param mixed $string_value
     */
    public function setStringValue($string_value)
    {
        $this->string_value = $string_value;
    }

    /**
     * @return int
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param int $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return int
     */
    public function getZoneId()
    {
        return $this->zone_id;
    }

    /**
     * @param int $zone_id
     */
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return mixed
     */
    public function getCreatedUser()
    {
        return $this->created_user;
    }

    /**
     * @param mixed $created_user
     */
    public function setCreatedUser($created_user)
    {
        $this->created_user = $created_user;
    }

    /**
     * @return mixed
     */
    public function getSalePrice()
    {
        return $this->sale_price;
    }

    /**
     * @param mixed $sale_price
     */
    public function setSalePrice($sale_price)
    {
        $this->sale_price = $sale_price;
    }

    /**
     * @return mixed
     */
    public function getFileCsv()
    {
        return $this->file_csv;
    }

    /**
     * @param mixed $file_csv
     */
    public function setFileCsv($file_csv)
    {
        $this->file_csv = $file_csv;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'price_zone_gideon';
    }


    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setParamsFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {

                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function insertPriceZoneToDb(){
        try {
            $sql  = "INSERT INTO price_zone_gideon(sku,zone_id,sale_price,start_date,end_date,file_csv,created_user,created_at) ";
            $sql .= "VALUES".$this->string_value;

            $this->useWriteConnection();
            $result = $this->getDi()->getShared($this->getConnection())->execute($sql);
            
            if(!$result){
                $this->errorCode = "RR301";
                $this->errorMessages[] = $e->getMessage();
            }else{
                $this->data = array('affected_row' => $this->getDi()->getShared($this->getConnection())->affectedRows());
            }
        }  catch (\Exception $e) {
            $this->errorCode = "RR301";
            $this->errorMessages[] = $e->getMessage();
        }
    }
}
