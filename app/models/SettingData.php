<?php
/**
 * Created by PhpStorm.
 * User: Aaron Putra
 * Date: 04/05/2017
 * Time: 10.35
 */

namespace Models;


class SettingData extends \Models\BaseModel
{
    protected $setting_data_id;
    protected $setting;
    protected $value;
    protected $updated_at;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getSettingDataId()
    {
        return $this->setting_data_id;
    }

    /**
     * @param mixed $setting_data_id
     */
    public function setSettingDataId($setting_data_id)
    {
        $this->setting_data_id = $setting_data_id;
    }

    /**
     * @return mixed
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * @param mixed $setting
     */
    public function setSetting($setting)
    {
        $this->setting = $setting;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function initialize()
    {

    }

    public function getSource()
    {
        return 'setting_data';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setSettingFormArray($params = array()){

        foreach ($params as $event => $val){
            if (property_exists($this, $event)) {

                $this->{$event} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function getSettingDetail(){
        $settings = $this->find([
            "columns" => "setting_data_id, setting, value",
        ]);

        $settingArr = array();

        if(!empty($settings)){
            foreach ($settings as $setting => $val){
                $settingArr = $val->toArray();
            }

            return $settingArr;
        }
        else{
            $this->errorCode = "RR302";
            $this->errorMessages = "No data has been found";
            return;
        }
    }


}