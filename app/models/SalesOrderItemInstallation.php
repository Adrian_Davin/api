<?php

namespace Models;

use Helpers\LogHelper;
class SalesOrderItemInstallation extends \Models\BaseModel
{
    protected $sales_order_item_installation_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_item_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_id;

    /**
     *
     * @var bool
     *
     */
    protected $is_installed;

    protected $is_need_installation;

    public function getSource()
    {
        return 'sales_order_item_installation';
    }
    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {        
        $this->setSource("sales_order_item_installation");
        $this->hasOne('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
    }  

    public function setFromArray($dataArray = array())
    {
        foreach($dataArray as $key => $val) {
            $this->{$key} = $val;
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getSalesOrderItemInstallationID()
    {
        return $this->sales_order_item_installation_id;
    }

    public function setSalesOrderItemInstallationID($sales_order_item_installation_id)
    {
        $this->sales_order_item_installation_id = $sales_order_item_installation_id;

        return $this;
    }

    public function getSalesOrderItemId()
    {
        return $this->sales_order_item_id;
    }

    public function setSalesOrderItemId(string $sales_order_item_id)
    {
        $this->sales_order_item_id = $sales_order_item_id;

        return $this;
    }

    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    public function setSalesOrderId(string $sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;

        return $this;
    }
    
    public function getIsInstalled()
    {
        return $this->is_installed;
    }

    public function setIsInstalled(string $is_installed)
    {
        $this->is_installed = $is_installed;

        return $this;
    }

    public function getIsNeedInstallation()
    {
        return $this->is_need_installation;
    }

    public function setIsNeedInstallation($is_need_installation)
    {
        $this->is_need_installation = $is_need_installation;

        return $this;
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
