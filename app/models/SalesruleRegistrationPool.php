<?php

namespace Models;

use Phalcon\Db as Database;

class SalesruleRegistrationPool extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $salesrule_registration_pool_id;
    protected $title;
    protected $company_code;
    protected $voucher_rules;
    protected $first_reminder;
    protected $second_reminder;
    protected $third_reminder;
    protected $fourth_reminder;
    protected $second_reminder_template_id;
    protected $fourth_reminder_template_id;
    protected $status;
    protected $created_at;
    protected $updated_at;

        /**
     * Get the value of salesrule_registration_pool_id
     *
     * @return  integer
     */ 
    public function getSalesrule_registration_pool_id()
    {
        return $this->salesrule_registration_pool_id;
    }

    /**
     * Set the value of salesrule_registration_pool_id
     *
     * @param  integer  $salesrule_registration_pool_id
     *
     * @return  self
     */ 
    public function setSalesrule_registration_pool_id($salesrule_registration_pool_id)
    {
        $this->salesrule_registration_pool_id = $salesrule_registration_pool_id;

        return $this;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of company_code
     */ 
    public function getCompany_code()
    {
        return $this->company_code;
    }

    /**
     * Set the value of company_code
     *
     * @return  self
     */ 
    public function setCompany_code($company_code)
    {
        $this->company_code = $company_code;

        return $this;
    }

    /**
     * Get the value of voucher_rules
     */ 
    public function getVoucher_rules()
    {
        return $this->voucher_rules;
    }

    /**
     * Set the value of voucher_rules
     *
     * @return  self
     */ 
    public function setVoucher_rules($voucher_rules)
    {
        $this->voucher_rules = $voucher_rules;

        return $this;
    }

    /**
     * Get the value of first_reminder
     */ 
    public function getFirst_reminder()
    {
        return $this->first_reminder;
    }

    /**
     * Set the value of first_reminder
     *
     * @return  self
     */ 
    public function setFirst_reminder($first_reminder)
    {
        $this->first_reminder = $first_reminder;

        return $this;
    }

    /**
     * Get the value of second_reminder
     */ 
    public function getSecond_reminder()
    {
        return $this->second_reminder;
    }

    /**
     * Set the value of second_reminder
     *
     * @return  self
     */ 
    public function setSecond_reminder($second_reminder)
    {
        $this->second_reminder = $second_reminder;

        return $this;
    }

    /**
     * Get the value of third_reminder
     */ 
    public function getThird_reminder()
    {
        return $this->third_reminder;
    }

    /**
     * Set the value of third_reminder
     *
     * @return  self
     */ 
    public function setThird_reminder($third_reminder)
    {
        $this->third_reminder = $third_reminder;

        return $this;
    }

    /**
     * Get the value of fourth_reminder
     */ 
    public function getFourth_reminder()
    {
        return $this->fourth_reminder;
    }

    /**
     * Set the value of fourth_reminder
     *
     * @return  self
     */ 
    public function setFourth_reminder($fourth_reminder)
    {
        $this->fourth_reminder = $fourth_reminder;

        return $this;
    }

    /**
     * Get the value of second_reminder_template_id
     */ 
    public function getSecond_reminder_template_id()
    {
        return $this->second_reminder_template_id;
    }

    /**
     * Set the value of second_reminder_template_id
     *
     * @return  self
     */ 
    public function setSecond_reminder_template_id($second_reminder_template_id)
    {
        $this->second_reminder_template_id = $second_reminder_template_id;

        return $this;
    }

    /**
     * Get the value of fourth_reminder_template_id
     */ 
    public function getFourth_reminder_template_id()
    {
        return $this->fourth_reminder_template_id;
    }

    /**
     * Set the value of fourth_reminder_template_id
     *
     * @return  self
     */ 
    public function setFourth_reminder_template_id($fourth_reminder_template_id)
    {
        $this->fourth_reminder_template_id = $fourth_reminder_template_id;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of created_at
     */ 
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */ 
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of updated_at
     */ 
    public function getUpdated_at()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of updated_at
     *
     * @return  self
     */ 
    public function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    protected $errors;
    protected $messages;
    protected $data;

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of errors
     *
     * @return  self
     */ 
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Get the value of messages
     */ 
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set the value of messages
     *
     * @return  self
     */ 
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * Get the value of data
     */ 
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */ 
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

        /**
     * Initialize method for model.
     */
    public function initialize()
    {
    }

        /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_registration_pool';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleRegistrationPool[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleRegistrationPool
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function saveSalesruleRegistrationPool($log_file_name = "error"){

        $resultData = array(
            'return' => false,
            'rule_id' => 0
        );
        
        $result = $this->saveData($log_file_name);

        if ($result){
            $resultData['return'] = true;
            $resultData['salesrule_registration_pool_id'] = $this->salesrule_registration_pool_id;
        }
        
        return $resultData;
    }

    public function setFromArray($params = array()){

        foreach ($params as $key => $val){
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }
}
