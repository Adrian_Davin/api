<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Salesrule\CustomerGroup;


class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\SalesruleCustomerGroup
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $price = $this->current();
            $priceArray = $price->getDataArray($columns, $showEmpty);
            $view[$idx] = $priceArray;

            $this->next();
        }
        return $view;
    }


    public function saveData($log_file_name = "salesrule")
    {

        $error = array();
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $response = $item->saveData($log_file_name);

            if(!$response) {
                $error[] = "Failed to save Salesrule";
            }

            $this->next();
        }

        return $error;
    }



}