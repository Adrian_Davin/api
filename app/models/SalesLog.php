<?php
namespace Models;

class SalesLog extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $sales_log_id;

    /**
     *
     * @var integer
     */
    protected $sales_order_id;

    /**
     *
     * @var integer
     */
    protected $invoice_id;

    /**
     *
     * @var integer
     */
    protected $shipment_id;

    /**
     *
     * @var integer
     */
    protected $admin_user_id;

    /**
     *
     * @var integer
     */
    protected $supplier_user_id;

    /**
     *
     * @var string
     */
    protected $actions;

        /**
     *
     * @var string
     */
    protected $affected_field;

        /**
     *
     * @var string
     */
    protected $value;

    /**
     *
     * @var string
     */
    protected $log_time;

    /**
     *
     * @var string
     */
    protected $flag;

    /**
     * Method to set the value of field sales_log_id
     *
     * @param integer $sales_log_id
     * @return $this
     */
    public function setSalesLogId($sales_log_id)
    {
        $this->sales_log_id = $sales_log_id;

        return $this;
    }

    /**
     * Method to set the value of field sales_order_id
     *
     * @param integer $sales_order_id
     * @return $this
     */
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;

        return $this;
    }

    /**
     * Method to set the value of field invoice_id
     *
     * @param integer $invoice_id
     * @return $this
     */
    public function setInvoiceId($invoice_id)
    {
        $this->invoice_id = $invoice_id;

        return $this;
    }

    /**
     * Method to set the value of field shipment_id
     *
     * @param integer $shipment_id
     * @return $this
     */
    public function setShipmentId($shipment_id)
    {
        $this->shipment_id = $shipment_id;

        return $this;
    }

    /**
     * Method to set the value of field admin_user_id
     *
     * @param integer $admin_user_id
     * @return $this
     */
    public function setAdminUserId($admin_user_id)
    {
        $this->admin_user_id = $admin_user_id;

        return $this;
    }

    /**
     * Method to set the value of field actions
     *
     * @param string $actions
     * @return $this
     */
    public function setActions($actions)
    {
        $this->actions = $actions;

        return $this;
    }

    /**
     * Method to set the value of field affected_field
     *
     * @param string $affected_field
     * @return $this
     */
    public function setAffectedField($affected_field)
    {
        $this->affected_field = $affected_field;

        return $this;
    }

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $value
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
    /**
     * Method to set the value of field log_time
     *
     * @param string $log_time
     * @return $this
     */
    public function setLogTime($log_time)
    {
        $this->log_time = $log_time;

        return $this;
    }

    /**
     * Method to set the value of field flag
     *
     * @param string $flag
     * @return $this
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Returns the value of field sales_log_id
     *
     * @return integer
     */
    public function getSalesLogId()
    {
        return $this->sales_log_id;
    }

    /**
     * Returns the value of field sales_order_id
     *
     * @return integer
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * Returns the value of field invoice_id
     *
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->invoice_id;
    }

    /**
     * Returns the value of field shipment_id
     *
     * @return integer
     */
    public function getShipmentId()
    {
        return $this->shipment_id;
    }

    /**
     * Returns the value of field admin_user_id
     *
     * @return integer
     */
    public function getAdminUserId()
    {
        return $this->admin_user_id;
    }

    /**
     * Returns the value of field actions
     *
     * @return string
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Returns the value of field affected_field
     *
     * @return string
     */
    public function getAffectedField()
    {
        return $this->affected_field;
    }

    /**
     * Returns the value of field value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * Returns the value of field log_time
     *
     * @return string
     */
    public function getLogTime()
    {
        return $this->log_time;
    }

    /**
     * Returns the value of field flag
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_log';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesLog[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesLog
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }
}