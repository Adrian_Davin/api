<?php

namespace Models;

use Phalcon\Db as Database;
use \Helpers\CartHelper;
use Library\APIWrapper;

class SalesShipment extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $shipment_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $invoice_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $shipping_address_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $shipment_status;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $total_weight;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $real_shipping_amount;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $shipped_date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $delivered_date;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $carrier_id;

    /** 
     * 
     * @var integer 
     * @Column(type="integer", length=4, nullable=true) 
     */ 
    protected $group_shipment; 

    /** 
     * 
     * @var integer 
     * @Column(type="integer", length=4, nullable=true) 
     */ 
    protected $express_courier_state; 

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $eta;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $shipment_type;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $insert_to_3pl;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $pod_image;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $do_image;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $track_number;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $company_code = 'ODI';

    protected $lead_time_min;
    protected $lead_time_max;
    protected $lead_time_unit;
    protected $insurance_fee;
    protected $must_use_insurance;
    protected $insurance_applied;
    protected $initial_lead_time_min;
    protected $initial_lead_time_max;
    protected $initial_lead_time_unit;

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    protected $errors;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('invoice_id', 'Models\SalesInvoice', 'invoice_id', array('alias' => 'SalesInvoice','reusable' => true));
        $this->belongsTo('shipping_address_id', 'Models\SalesOrderAddress', 'sales_order_address_id', array('alias' => 'SalesOrderAddress', 'reusable' => true));
        $this->belongsTo('carrier_id', 'Models\ShippingCarrier', 'carrier_id', array('alias' => 'ShippingCarrier', 'reusable' => true));
        $this->hasMany('shipment_id', 'Models\SalesShipmentTracking', 'shipment_id', array('alias' => 'ShipmentTracking'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_shipment';
    }

    /**
     * @return int
     */
    public function getShipmentId()
    {
        return $this->shipment_id;
    }

    /**
     * @param int $shipment_id
     */
    public function setShipmentId($shipment_id)
    {
        $this->shipment_id = $shipment_id;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoice_id;
    }

    /**
     * @param int $invoice_id
     */
    public function setInvoiceId($invoice_id)
    {
        $this->invoice_id = $invoice_id;
    }

    /**
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param string $company_code
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * @return int
     */
    public function getShippingAddressId()
    {
        return $this->shipping_address_id;
    }

    /**
     * @param int $shipping_address_id
     */
    public function setShippingAddressId($shipping_address_id)
    {
        $this->shipping_address_id = $shipping_address_id;
    }

    /**
     * @return string
     */
    public function getShipmentStatus()
    {
        return $this->shipment_status;
    }

    /**
     * @param string $shipment_status
     */
    public function setShipmentStatus($shipment_status)
    {
        $this->shipment_status = $shipment_status;
    }

    /**
     * @return float
     */
    public function getTotalWeight()
    {
        return $this->total_weight;
    }

    /**
     * @param float $total_weight
     */
    public function setTotalWeight($total_weight)
    {
        $this->total_weight = $total_weight;
    }

    /**
     * @return float
     */
    public function getRealShippingAmount()
    {
        return $this->real_shipping_amount;
    }

    /**
     * @param float $real_shipping_amount
     */
    public function setRealShippingAmount($real_shipping_amount)
    {
        $this->real_shipping_amount = $real_shipping_amount;
    }

    /**
     * @return string
     */
    public function getShippedDate()
    {
        return $this->shipped_date;
    }

    /**
     * @param string $shipped_date
     */
    public function setShippedDate($shipped_date)
    {
        $this->shipped_date = $shipped_date;
    }

    /**
     * @return string
     */
    public function getDeliveredDate()
    {
        return $this->delivered_date;
    }

    /**
     * @param string $delivered_date
     */
    public function setDeliveredDate($delivered_date)
    {
        $this->delivered_date = $delivered_date;
    }

    /**
     * @return string
     */
    public function getCarrierCode()
    {
        return $this->carrier_code;
    }

    /**
     * @param string $carrier_code
     */
    public function setCarrierCode($carrier_code)
    {
        $this->carrier_code = $carrier_code;
    }

    /**
     * @return int
     */
    public function getCarrierId()
    {
        return $this->carrier_id;
    }

    /**
     * @param int $carrier_id
     */
    public function setCarrierId($carrier_id)
    {
        $this->carrier_id = $carrier_id;
    }

    /**
     * @return string
     */
    public function getShipmentType()
    {
        return $this->shipment_type;
    }

    /**
     * @param string $shipment_type
     */
    public function setShipmentType($shipment_type)
    {
        $this->shipment_type = $shipment_type;
    }

    /**
     * @return string
     */
    public function getInsertTo3pl()
    {
        return $this->insert_to_3pl;
    }

    /**
     * @param string $insert_to_3pl
     */
    public function setInsertTo3pl($insert_to_3pl)
    {
        $this->insert_to_3pl = $insert_to_3pl;
    }

    /**
     * @return string
     */
    public function getCarrierName()
    {
        return $this->carrier_name;
    }

    /**
     * @param string $carrier_name
     */
    public function setCarrierName($carrier_name)
    {
        $this->carrier_name = $carrier_name;
    }

    /**
     * @return string
     */
    public function getTrackNumber()
    {
        return $this->track_number;
    }

    /** 
     * @param int $group_shipment 
     */ 
    public function setGroupShipment($group_shipment) 
    { 
        $this->group_shipment = $group_shipment; 
    } 
     
     /** 
     * @return mixed 
     */ 
    public function getGroupShipment() 
    { 
        return $this->group_shipment; 
    }     
    
    /** 
     * @param int $express_courier_state 
     */ 
    public function setExpressCourierState($express_courier_state) 
    { 
        $this->express_courier_state = $express_courier_state; 
    } 
     
     /** 
     * @return mixed 
     */ 
    public function getExpressCourierState() 
    { 
        return $this->express_courier_state; 
    }     

    /**
     * @return string
     */
    public function getEta()
    {
        return $this->eta;
    }

    /**
     * @param string $eta
     */
    public function setEta($eta)
    {
        $this->eta = $eta;
    }
    
    /**
     * @param string $track_number
     */
    public function setTrackNumber($track_number)
    {
        $this->track_number = $track_number;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesShipment[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesShipment
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_shipment", "TRIGGERED");
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = parent::toArray($columns,$showEmpty);

        unset($view['invoice_id']);
        unset($view['carrier_id']);
        if(!empty($this->carrier_id)) {
            $view['carrier'] = $this->ShippingCarrier->toArray(['carrier_id', 'carrier_name', 'carrier_code', 'logo_url']);
        }

        unset($view['shipping_address_id']);
        if(!empty($this->SalesOrderAddress)) {
            $shippingAddress = new \Models\SalesOrderAddress();
            $shippingAddress->setFromArray($this->SalesOrderAddress->toArray());
            $view['shipping_address'] = $shippingAddress->getDataArray();
        }

        return $view;
    }

    /**
     * Function that check Shipping Null data, if found then will set is in stock to 0
     * and send email to merchandising@ruparupa.com
     */
    public function updateShippingNull($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $resultList = $params;

            if (count($resultList['productList']) > 0) {
                $affectedRow = 0;
                $skuChain = array();
                foreach ($resultList['productList'] as $row) {
                    if (!empty($row['sku'])) {
                        $skuChain[] = $row['sku'];
                    }
                }

                $chunksSku = array_chunk($skuChain, 500);
                $productStockLib = new \Library\ProductStock();
                foreach ($chunksSku as $rowArrayS) {
                    $paramStock = array();
                    foreach($rowArrayS as $skuCompare){
                        $dataCompare = array(
                            'sku' => $skuCompare,
                            'store_code' => 'DC',
                            'status' => 0,
                            'company_code' => 'ODI'
                        );
                        $paramStock[] = $dataCompare;
                    }
                    $affectedRow += $productStockLib->updateStock($paramStock);
                }

                $response['messages'] = array('success');
                $response['data']['affected_row'] = $affectedRow;
            } else {
                $response['messages'] = array('success');
                $response['data']['affected_row'] = 0;
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Check Shipping Null failed, please contact ruparupa tech support ('.$e->getMessage() . ")");
        }

        return $response;
    }

    public function getShippingNullData()
    {
        $response['data'] = array();

        try {
            $sql = "
            SELECT
                (SELECT product_variant_id from product_variant where product_id = a.product_id limit 1) as product_variant_id,
                (SELECT sku from product_variant where product_id = a.product_id limit 1) as sku,	
                name,
                (SELECT price from product_price where product_variant_id = (SELECT product_variant_id from product_variant where product_id = a.product_id limit 1)) as price
            FROM product a
            WHERE ((packaging_height * packaging_length * packaging_width) = 0) AND (weight = 0)
            AND (SELECT product_variant_id from product_variant where product_id = a.product_id limit 1) is not NULL;";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $data = $result->fetchAll();
            }

            $response['messages'] = array('success');
            $response['data'] = $data;
        } catch (\Exception $e) {
            $response['errors'] = array('Check Shipping Null failed, please contact ruparupa tech support ('.$e->getMessage() . ")");
        }

        return $response;
    }

    public function sendShippingNullEmail($params = array())
    {
        $emailHelper = new \Library\Email();

        $templateID = \Helpers\GeneralHelper::getTemplateId("shipping_null", "ODI");
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode("ODI");
        $emailHelper->setEmailSender();
        $emailHelper->setEmailReceiver($params['to']);
        
        if (isset($params['cc'])) {
            $emailHelper->setEmailCc($params['cc']);
        }

        $emailHelper->setSubject($params['subject']);
        $emailHelper->setEmailTag("team_batch_processor_shipping_null");
        $emailHelper->setLinkDownload($params['link_download']);
        if ($emailHelper->send() == false) {
            $response['errors'] = array('Send email shipping null Failed');
        } else {
            $response['messages'] = array('Send email shipping null Success');
        }

        return $response;
    }

    public function sendEmailReminderPendingOrder($params = array())
    {
        if (isset($params['type'])) {
            $emailHelper = new \Library\Email();

            $companyCode = "ODI";
            if (isset($params['company_code'])) {
                $companyCode = $params['company_code'];
            }

            if ($params['type'] == 'store') {
                return $response['errors'] = array('No need send email reminder pending order store');

                // $templateID = \Helpers\GeneralHelper::getTemplateId("reminder_store", $companyCode);
                // $emailHelper->setCompanyCode($companyCode);
                // $emailHelper->setTemplateId($templateID);
                // $emailHelper->setStoreName($params['data_email']['store_name']);
                // $emailHelper->setEmailTag("pic_stops_reminder_order_incomplete");
            } elseif ($params['type'] == 'dc') {
                $templateID = \Helpers\GeneralHelper::getTemplateId("reminder_dc", $companyCode);
                $emailHelper->setCompanyCode($companyCode);
                $emailHelper->setTemplateId($templateID);
                $emailHelper->setEmailTag("dc_reminder_pending_order");
            } else {
                return $response['errors'] = array('Send email reminder pending order Failed');
            }

            $emailHelper->setEmailSender();
            $emailHelper->setEmailReceiver($params['to']);
            $emailHelper->setEmailCc($params['cc']);
            $emailHelper->setPendingOrderList($params['data_email']['pending_order_list']);
            if ($emailHelper->send() == false) {
                $response['errors'] = array('Send email reminder pending order Failed');
            } else {
                $response['messages'] = array('Send email reminder pending order Success');
            }
        } else {
            $response['errors'] = array('Send email reminder pending order Failed');
        }

        return $response;
    }

    public function getOriginByKecamatanID($params = array())
    {
        $response['data'] = array();
        $response['messages'] = array('error');
        if (!isset($params['kecamatan_id']) || !isset($params['sku']) || !isset($params['qty_ordered']) || !isset($params['carrier_id'])) {
            $response['errors'] = array('kecamatan_id / sku / qty_ordered / carrier_id needed');
            return $response;
        }

        if (empty($params['kecamatan_id']) || empty($params['sku']) || empty($params['qty_ordered']) || empty($params['carrier_id'])) {
            $response['errors'] = array('kecamatan_id / sku / qty_ordered / carrier_id must filled');
            return $response;
        }

        // Get kecamatan code
        $kecamatanParams = array("kecamatan_id" => $params['kecamatan_id']);
        $kecamatanModel = new \Models\MasterKecamatan();
        $dataKecamatan = $kecamatanModel->getData($kecamatanParams);
        $kecamatanCode = "";
        if (isset($dataKecamatan['kecamatan_code'])) {
            $kecamatanCode = $dataKecamatan['kecamatan_code'];
            if (empty($kecamatanCode)) {
                $response['errors'] = array('Lokasi tidak disupport Gosend');
                return $response;
            }
        } else {
            $response['errors'] = array('Lokasi tidak disupport Gosend');
            return $response;
        }

        $cartItemModel = new \Models\CartItem();
        $paramsItem['sku'] = $params['sku'];
        $paramsItem['qty_ordered'] = $params['qty_ordered'];
        $paramsItem['kecamatan_code'] = $kecamatanCode;
        $cartItemModel->setFromArray($paramsItem);
        $stockLocationList = $cartItemModel->checkStockAndAssignation($kecamatanCode, 'ODI', null, $params['carrier_id']);
        if (!empty($cartItemModel->getErrorCode())) {
            $response['errors'] = array($cartItemModel->getErrorMessages());
            return $response;
        }

        $cartItemData = $cartItemModel->getDataArray();
        if ($cartItemData['max_qty'] == 0) {
            $response['errors'] = array('Jumlah barang tidak mencukupi');
            return $response;
        }

        if (CartHelper::isGosend($params['carrier_id'])) {
            // weight validation
            $totalWeightMax = CartHelper::maxWeightGosend($params['carrier_id'], $params['qty_ordered']);
            $totalWeight = $cartItemData['weight'] * $params['qty_ordered'];
            if ($totalWeight > $totalWeightMax) {
                $response['errors'] = array('Berat barang tidak disupport Gosend');
                return $response;
            }

            // volumetric validation
            $totalVolumeMax = CartHelper::maxVolumeGosend($params['carrier_id'], $params['qty_ordered']);
            if (strtolower($cartItemData['packaging_uom']) == 'cm') { 
                $totalVolumetric = ($cartItemData['packaging_height'] * $cartItemData['packaging_width'] * $cartItemData['packaging_length']) * $params['qty_ordered'];
            } elseif (strtolower($cartItemData['packaging_uom']) == 'mm') {
                $totalVolumetric = (($cartItemData['packaging_height'] / 10) * ($cartItemData['packaging_width'] / 10) * ($cartItemData['packaging_length'] / 10)) * $params['qty_ordered'];
            } elseif (strtolower($cartItemData['packaging_uom']) == 'm') {
                $totalVolumetric = (($cartItemData['packaging_height'] * 100) * ($cartItemData['packaging_width'] * 100) * ($cartItemData['packaging_length'] * 100)) * $params['qty_ordered'];
            }

            if (isset($totalVolumetric)) {
                if ($totalVolumetric > $totalVolumeMax) {
                    $response['errors'] = array('Volume barang tidak disupport Gosend');
                    return $response;
                }
            } else {
                $response['errors'] = array('Volume barang tidak disupport Gosend');
                return $response;
            }


            if (empty($cartItemData['pickup_code'])) {
                $response['errors'] = array('Lokasi tidak disupport Gosend');
                return $response;
            }

            // validation by gosend mapping
            $gosendMappingModel = new \Models\GosendMapping();			
            $gosendMappingResult = $gosendMappingModel->find("pickup_code = '" . $cartItemData['pickup_code'] . "'");
            if($gosendMappingResult){
                $kecamatanCodeList = array();
                foreach ($gosendMappingResult as $rowGosendMapping) {
                    $kecamatanCodeList[$rowGosendMapping->toArray()['kecamatan_code']] = 1;
                }    

                if (!isset($kecamatanCodeList[$kecamatanCode])) {
                    $response['errors'] = array('Lokasi tidak disupport Gosend');
                    return $response;
                }
            }

            // get geolocation by pickup_code and check the is_express_courier
            $cartLib = new \Library\Cart();
            $dataPickupPoint = $cartLib->getPickupData($cartItemData['pickup_code']);
            if (count($dataPickupPoint) == 0) {
                $response['errors'] = array('Data not found');
                return $response;
            }

            if ($dataPickupPoint['is_express_courier'] <> 1 || empty($dataPickupPoint['geolocation'])) {
                $response['errors'] = array('Lokasi tidak disupport Gosend');
                return $response;
            }

            $response['data'] = array(
                'pickup_code' => $dataPickupPoint['pickup_code'],
                "pickup_name" => $dataPickupPoint['pickup_name'],
                'pickup_address' => $dataPickupPoint['pickup_address'],
                'geolocation' => $dataPickupPoint['geolocation']
            );

            $response['messages'] = array('success');
        } else {
            // For other carrier_id
            if ($cartItemData['delivery_method'] == 'store_fulfillment') {
                if (empty($cartItemData['pickup_code'])) {
                    $response['errors'] = array('Data not found');
                    return $response;
                }

                $cartLib = new \Library\Cart();
                $dataPickupPoint = $cartLib->getPickupData($cartItemData['pickup_code']);
                if (count($dataPickupPoint) == 0) {
                    $response['errors'] = array('Data not found');
                    return $response;
                }

                $response['data'] = array(
                    'pickup_code' => $dataPickupPoint['pickup_code'],
                    "pickup_name" => $dataPickupPoint['pickup_name'],
                    'pickup_address' => $dataPickupPoint['pickup_address'],
                    'geolocation' => $dataPickupPoint['geolocation']
                );
    
                $response['messages'] = array('success');
            }
        }
        
        return $response;
    }
    
    /**
     * This function is used by generate awb function to get Qty order based on pickup code
     * @param array $params contain sales_order_id and pickup_code
     * @return int
     */
    public function getQtyOrderedStoreFulfillment($params = array())
    {
        try {
            $sql = "SELECT COALESCE(sum(qty_ordered),1) as qty from sales_invoice where sales_order_id = {$params['sales_order_id']} and pickup_code = '{$params['pickup_code']}';";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $getQty = $result->fetchAll();

                foreach ($getQty as $key => $value){
                    $qty = (int)$value['qty'];
                }
            } else {
                $qty = 1;
            }
        } catch (\Exception $e) {
            $qty = 1;
        }

        return $qty;
    }

    /**
     * This function is used by generate awb function to get Weight order based on pickup code
     * @param array $params contain sales_order_id and pickup_code
     * @return int
     */
    public function getWeightOrderedStoreFulfillment($params = array())
    {
        try {
            $sql = "SELECT COALESCE(sum(total_weight),1.00) as weight from sales_shipment where invoice_id in (
                SELECT invoice_id from sales_invoice where sales_order_id = {$params['sales_order_id']} and pickup_code = '{$params['pickup_code']}'
            );";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $getWeight = $result->fetchAll();

                foreach ($getWeight as $key => $value){
                    $weight = $value['weight'];
                }
            } else {
                $weight = 1.00;
            }
        } catch (\Exception $e) {
            $weight = 1.00;
        }

        return $weight;
    }

    /**
     * This function is used by generate awb function to get Customer Phone
     * @param array $params contain only sales_order_id
     * @return int
     */
    public function getRecipientHandphone($params = array())
    {
        try {
            // use our cs phone number if customer phone not found
            $sql = "SELECT COALESCE(phone, '021 - 582 9191') as phone FROM customer WHERE customer_id =
                    (SELECT customer_id FROM sales_order WHERE sales_order_id = {$params['sales_order_id']});";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $getPhone = $result->fetchAll();

                foreach ($getPhone as $key => $value){
                    $phone = $value['phone'];
                }
            } else {
                $phone = '021 - 582 9191';
            }
        } catch (\Exception $e) {
            $phone = '021 - 582 9191';
        }

        return $phone;
    }

    public function getPendingOrder($params = array())
    {
        $response['data'] = array();

        try {
            if (isset($params['type'])) {
                if ($params['type'] == 'store') {
                    $sql = "
                    SELECT
                        si.pickup_code,
                        so.order_no,
                        si.invoice_no,
                        si.created_at as invoice_date,
                        CONCAT(soa.first_name,' ',soa.last_name) as customer_name,
                        c.email as customer_email,	
                        pp.email_pic,
                        pp.pickup_name,
                        sii.sku,
                        sii.`name`,
                        sii.qty_ordered
                    FROM
                        sales_shipment ss LEFT JOIN 
                        sales_invoice si ON si.invoice_id = ss.invoice_id LEFT JOIN
                        sales_order so ON so.sales_order_id = si.sales_order_id LEFT JOIN
                        sales_order_address soa ON soa.sales_order_id = so.sales_order_id LEFT JOIN
                        pickup_point pp ON pp.pickup_code = si.pickup_code LEFT JOIN
                        customer c ON c.customer_id = so.customer_id LEFT JOIN
                        sales_invoice_item sii ON sii.invoice_id = si.invoice_id
                    WHERE
                        ss.shipment_type IN (
                                'pickup',
                                'store_fulfillment'
                        )
                        AND ss.shipment_status = 'new' 
                        AND soa.address_type = 'shipping'
                        AND sii.sku NOT IN ('RR00001','RR00002')
                        ORDER BY si.pickup_code, so.created_at, so.order_no, si.invoice_no;";
                } elseif($params['type'] == 'dc') {
                    $sql = "
                    SELECT
                        so.order_no,
                        si.invoice_no,
                        si.created_at as invoice_date,
                        CONCAT(soa.first_name,' ',soa.last_name) as customer_name,
                        c.email as customer_email,	
                        sii.sku,
                        sii.`name`,
                        sii.qty_ordered
                    FROM
                        sales_shipment ss LEFT JOIN 
                        sales_invoice si ON si.invoice_id = ss.invoice_id LEFT JOIN
                        sales_order so ON so.sales_order_id = si.sales_order_id LEFT JOIN
                        sales_order_address soa ON soa.sales_order_id = so.sales_order_id LEFT JOIN
                        customer c ON c.customer_id = so.customer_id LEFT JOIN
                        sales_invoice_item sii ON sii.invoice_id = si.invoice_id
                    WHERE
                        si.delivery_method = 'delivery'
                        AND substr(si.store_code,1,2) = 'DC'
                        AND ss.shipment_status IN ('new','ready_for_pickup_lazada')
                        AND soa.address_type = 'shipping'
                        AND sii.sku NOT IN ('RR00001','RR00002')
                        ORDER BY so.created_at, so.order_no, si.invoice_no;";
                }

                if (isset($sql)) {
                    $this->useReadOnlyConnection();
                    $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                    if ($result->numRows() > 0) {
                        $result->setFetchMode(Database::FETCH_ASSOC);
                        $data = $result->fetchAll();
                    }

                    $response['messages'] = array('success');
                    $response['data'] = $data;
                }
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Get Store Pending Order failed, please contact ruparupa tech support ('.$e->getMessage() . ")");
        }

        return $response;
    }

    /**
     *
     * check for previously exist similar shipment by invoice_id, shipping_address_id and carrier_id
     * prevent double shipment to be created
     *
     * @return bool
     */
    public function checkExists () {
        $result = $this->findFirst(
            [
                'columns' => 'shipment_id',
                'conditions' => 'invoice_id=?1 AND shipping_address_id=?2 AND carrier_id=?3',
                'bind' => [
                    1 => $this->invoice_id,
                    2 => $this->shipping_address_id,
                    3 => $this->carrier_id
                ]
            ]);
        if($result) {
            return true;
        } else {
            return false;
        }
    }

    public function gosendBooking($invoice_item_id = null)
    {
        if ($invoice_item_id) {
            // found the item's courier ID
            $sql = "
            SELECT
                soi.sales_order_id,
                si.invoice_no,
                soi.group_shipment,
                ss.carrier_id,
                ss.express_courier_state,
                ss.shipment_id
            FROM
                sales_invoice_item sii LEFT JOIN sales_order_item soi ON soi.sales_order_item_id = sii.sales_order_item_id
                LEFT JOIN sales_invoice si ON si.invoice_id = sii.invoice_id
                LEFT JOIN sales_shipment ss ON ss.invoice_id = si.invoice_id
            WHERE
                sii.invoice_item_id = $invoice_item_id;";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            $data = array(); 
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $data = $result->fetchAll();
                $salesOrderID = $data[0]['sales_order_id'];
                $invoiceNo = $data[0]['invoice_no'];
                $groupShipment = $data[0]['group_shipment'];
                $carrierID = $data[0]['carrier_id'];
                $expressCourierState = $data[0]['express_courier_state'];
                $shipmentID = $data[0]['shipment_id'];
                if (CartHelper::isGosend($carrierID) && $expressCourierState == 0) {
                    // cek available gosend time in store
                    $startTime = date('Y-m-d 10:00:00');
                    if ($carrierID == getenv('GOSEND_INSTANT_CARRIER_ID')) {
                        $endTime = date('Y-m-d 18:00:00');
                    } else if ($carrierID == getenv('GOSEND_SAMEDAY_CARRIER_ID')) {
                        $endTime = date('Y-m-d 15:00:00');
                    }

                    // check is all item in the group shipment already complete?
                    $sql = "SELECT status_fulfillment
                    FROM sales_invoice_item
                    WHERE sales_order_item_id IN (SELECT sales_order_item_id FROM sales_order_item WHERE sales_order_id = $salesOrderID AND group_shipment = $groupShipment);";

                    $flagComplete = 1;
                    $resultFulfillment = $this->getDi()->getShared($this->getConnection())->query($sql);
                    $dataFulfillment = array(); 
                    if ($resultFulfillment->numRows() > 0) {
                        $resultFulfillment->setFetchMode(Database::FETCH_ASSOC);
                        $dataFulfillment = $resultFulfillment->fetchAll();
                        foreach($dataFulfillment as $rowItem) {
                            if ($rowItem['status_fulfillment'] <> 'complete') {
                                $flagComplete = 0;
                            }
                        }
                    }

                    $isActiveTime = \Helpers\GeneralHelper::iscurrentTimeActive($startTime,$endTime);
                    if ($isActiveTime) {
                        if ($flagComplete) {
                            $this->generateBookingGosend(['invoice_no' => $invoiceNo]);
                        }
                    } else {
                        if ($flagComplete) {
                           // change shipment status to pending_booking
                           $params['shipment_id'] = $shipmentID;
                           $params['shipment_status'] = "pending_booking";
                           $this->setFromArray($params);
                           $this->saveData("shipment");
                        }
                    }
                }
            }
        }
    }

    public function generateBookingGosend($params = array()) 
    {
        $apiWrapper = new APIWrapper(getenv('SHIPMENT_API'));
        $apiWrapper->setParam($params);
        $apiWrapper->setEndPoint("gosend/booking/create");
        try {
            if($apiWrapper->send("post")) {
                $apiWrapper->formatResponse();
            }

            $responseContent['data'] = $apiWrapper->getData();
            $responseContent['errors'] = $apiWrapper->getError();
            $responseContent['messages'] = $apiWrapper->getMessages();
            if (isset($responseContent['messages'][0]) && $responseContent['messages'][0] == 'success') { 
                \Helpers\LogHelper::log('shipment_create_gosend','[SUCCESS] to call generate booking gosend' . json_encode($resultData));
            } elseif (isset($responseContent['errors']['messages'][0])) {
                \Helpers\LogHelper::log('shipment_create_gosend','[ERROR] when calling shipment API :' . $responseContent['errors']['messages'][0] . ' => ' .json_encode($params));
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log('shipment_create_gosend','[ERROR] when calling shipment API: ' . $e->getMessage());
        }
    }

    public function updateStatusBatch($params = array())
    {
        $response = array(
            'messages' => 'failed',
            'data' => array(),
            'errors' => array()
        );

        try {
            // looping and get shipment id
            if (isset($params['lists']) && count($params['lists']) > 0) {
                $resultList = array();
                $flagDataUpdated = false;
                foreach($params['lists'] as $row) {
                    $resultData = $this->getShipmentDataByInvoiceNo($row['invoice_no']);
                    $shipmentID = $resultData['shipment_id'];
                    $currentShipmentStatus = $resultData['shipment_status'];
                    if ($shipmentID == 0 || $currentShipmentStatus == '') {
                        $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => "failed (data not found)");
                        continue;
                    }

                    if ($currentShipmentStatus == 'received' && $row['shipment_status'] == 'shipped') {
                        $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => "failed (backward status not allowed)");
                        continue;
                    }

                    // the minimum status that can be updated are stand by (store) / shipped (marketplace)
                    // remove new according to this ticket RRTICKET-8838
                    $notAllowedStatus = array('pending_shipment','confirmed','processing','hold','canceled');
                    if (in_array($currentShipmentStatus, $notAllowedStatus) && 
                    strpos($resultData['store_code'], 'DC') == false &&
                    ($row['shipment_status'] == 'shipped' || $row['shipment_status'] == 'received')) {
                        $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => "failed ('$currentShipmentStatus' cant be updated to '{$row['shipment_status']}')");
                        continue;
                    }

                    $allowedStatus = array('shipped', 'received');
                    if (!in_array($row['shipment_status'], $allowedStatus)) {
                        $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => "failed (we only accept status shipped and received)");
                        continue;
                    }

                    // build the params
                    $paramsUpdate = array();
                    $paramsUpdate['shipment_id'] = $shipmentID;
                    $paramsUpdate['send_email'] = 'no';
                    if (isset($row['shipment_status']) && !empty($row['shipment_status']) && $currentShipmentStatus <> $row['shipment_status']) {
                        $paramsUpdate['shipment_status'] = $row['shipment_status'];
                    }

                    if (isset($row['track_number']) && !empty($row['track_number'])) {
                        $paramsUpdate['track_number'] = $row['track_number'];
                    }

                    if (isset($row['carrier_id']) && !empty($row['carrier_id'])) {
                        $paramsUpdate['carrier_id'] = $row['carrier_id'];
                    }
        
                    $updateShipmentStatuses = array();
                    if (!empty($paramsUpdate['shipment_status'])){
                        // Determine which order of status taken
                        // Pickup: stand_by => ready_to_pickup => received
                        // Delivery, storefulfill, ownfleet: stand_by => ready_to_pickup => picked => shipped => received
                        $shipmentStatusSteps = array();
                        $resultData['delivery_method'] == "pickup" ? array_push($shipmentStatusSteps,"stand_by", "ready_to_pickup","received") : array_push($shipmentStatusSteps,"stand_by", "ready_to_pickup","picked","shipped","received");
                        $currentStatusIndex = array_search($currentShipmentStatus, $shipmentStatusSteps);
                        $updateStatusIndex = array_search($paramsUpdate['shipment_status'], $shipmentStatusSteps);
                      
                        // add delivery status until intended update status
                        foreach(array_keys($shipmentStatusSteps) as $key) {
                            if ($key > $currentStatusIndex && $key <= $updateStatusIndex){
                                array_push($updateShipmentStatuses,$shipmentStatusSteps[$key]);
                            }
                        }
                    }

                    $salesShipmentLib = new \Library\SalesShipment();
                    $paramsUpdate['admin_user_id'] = !empty($row['admin_user_id']) ? $row['admin_user_id'] : 1;
                    $paramsUpdate['request_by'] = !empty($row['request_by']) ? $row['request_by'] : '-';
                    if (count($updateShipmentStatuses) > 0 ){
                        // Make track_number and carrier_id empty so only on last status will update them
                        $paramsUpdateTemp = $paramsUpdate;
                        if (!empty($paramsUpdate['track_number'])){
                            unset($paramsUpdateTemp['track_number']);
                        }
                        if (!empty($paramsUpdate['carrier_id'])){
                            unset($paramsUpdateTemp['carrier_id']);
                        }

                        foreach($updateShipmentStatuses as $status) {
                            if (end($updateShipmentStatuses) == $status){
                                if (!empty($paramsUpdate['track_number'])){
                                    $paramsUpdateTemp['track_number'] = $paramsUpdate['track_number'];
                                }
                                if (!empty($paramsUpdate['carrier_id'])){
                                    $paramsUpdateTemp['carrier_id'] = $paramsUpdate['carrier_id'];
                                }
                            }

                            $paramsUpdateTemp['shipment_status'] = $status;
                            $salesShipmentLib->prepareSalesShipment($paramsUpdateTemp);
                            if (!empty($salesShipmentLib->getErrorCode())) {
                                $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => "failed (" . $salesShipmentLib->getErrorMessages() . ")");
                                continue;
                            }
        
                            $salesShipmentLib->updateData($paramsUpdateTemp);
                            if (!empty($salesShipmentLib->getErrorCode())) {
                                $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => "failed (" . $salesShipmentLib->getErrorMessages() . ")");
                                continue;
                            }
                        }
                    }else{
                        $salesShipmentLib->prepareSalesShipment($paramsUpdate);
                        if (!empty($salesShipmentLib->getErrorCode())) {
                            $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => "failed (" . $salesShipmentLib->getErrorMessages() . ")");
                            continue;
                        }
    
                        $salesShipmentLib->updateData($paramsUpdate);
                        if (!empty($salesShipmentLib->getErrorCode())) {
                            $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => "failed (" . $salesShipmentLib->getErrorMessages() . ")");
                            continue;
                        }
                    }

                    $resultList[] = array('invoice_no' => $row['invoice_no'], 'result' => 'success');
                    $flagDataUpdated = true;
                }

                $response['data'] = $resultList;
                if ($flagDataUpdated) {
                    $response['messages'] = 'success';
                } else {
                    $response['errors'][] = "No data updated";
                }
            } else {
                $response['errors'][] = "Data not found";
            }
        } catch (\Exception $e) {
            $response['errors'][] = "Update shipment status failed: " . $e->getMessage();
        }

        return $response;
    }

    public function createSalesLog($params = array())
    {
        if (isset($params['invoice_no']) && !empty($params['invoice_no'])) {
            $shipmentStatus = $trackNumber = $carrierID = '';
            if (isset($params['shipment_status']) && !empty($params['shipment_status'])) {
                $shipmentStatus = $params['shipment_status'];
            }

            if (isset($params['track_number']) && !empty($params['track_number'])) {
                $trackNumber = $params['track_number'];
            }

            if (isset($params['carrier_id']) && !empty($params['carrier_id'])) {
                $carrierID = $params['carrier_id'];
            }

            if (!empty($shipmentStatus) || !empty($trackNumber) || !empty($carrierID)) {
                $salesInvoice = new \Models\SalesInvoice();
                $salesInvoiceResult = $salesInvoice->findFirst("invoice_no = '{$params['invoice_no']}'");
                if ($salesInvoiceResult) {
                    $salesInvoiceArray = $salesInvoiceResult->toArray();
                    $paramLog['sales_order_id'] = $salesInvoiceArray['sales_order_id'];
                    $paramLog['invoice_id'] = $salesInvoiceArray['invoice_id'];
                    $paramLog['shipment_id'] = $params['shipment_id'];
                    $paramLog['admin_user_id'] = (isset($params['admin_user_id'])) ? $params['admin_user_id'] : 1;
                    $paramLog['log_time'] = date("Y-m-d H:i:s");
                    $paramLog['flag'] = ((substr($salesInvoiceArray['store_code'], 0, 2) == "DC")? "dc_dashboard" : "store_dashboard");
                    $requestBy = (isset($params['request_by'])) ? $params['request_by'] : '-';
                    if (!empty($shipmentStatus)) {
                        $paramLog['actions'] = "update shipment status #{$salesInvoiceArray['invoice_no']} to {$params['shipment_status']}, request by: $requestBy";
                        // if (!$this->checkLogByActions($paramLog['actions'],$params['shipment_id'],$paramLog['admin_user_id'])) {
                            $paramLog['affected_field'] = "shipment_status";
                            $paramLog['value'] = $params['shipment_status'];
                            $salesLogModel = new \Models\SalesLog();    
                            $salesLogModel->setFromArray($paramLog);                            
                            $salesLogModel->saveData("sales_log");
                        // }                        

                        $paramLog['actions'] = "update invoice status #{$salesInvoiceArray['invoice_no']} to {$params['shipment_status']}, request by: $requestBy";
                        // if (!$this->checkLogByActions($paramLog['actions'],$params['shipment_id'],$paramLog['admin_user_id'])) {
                            $currentInvoiceStatus = $salesInvoiceArray['invoice_status'];
                            $forbiddenStatus = array('partial_refund', 'full_refund');
                            $allowedStatus = array('processing', 'stand_by', 'ready_to_pickup', 'picked', 'shipped', 'received', 'canceled');
                            if (!in_array($currentInvoiceStatus, $forbiddenStatus)) {
                                if (in_array($params['shipment_status'], $allowedStatus)) {
                                    $paramLog['affected_field'] = "invoice_status";
                                    $paramLog['value'] = $params['shipment_status'];                            
                                    $salesLogModel = new \Models\SalesLog();
                                    $salesLogModel->setFromArray($paramLog);
                                    $salesLogModel->saveData("sales_log");
                                }
                            }
                        // }
                    }

                    if (!empty($trackNumber)) {
                        $paramLog['actions'] = "update track number #{$salesInvoiceArray['invoice_no']} to $trackNumber, request by: $requestBy";
                        // if (!$this->checkLogByActions($paramLog['actions'],$params['shipment_id'],$paramLog['admin_user_id'])) {
                            $paramLog['affected_field'] = "track_number";
                            $paramLog['value'] = $trackNumber;
                            $salesLogModel = new \Models\SalesLog();    
                            $salesLogModel->setFromArray($paramLog);                            
                            $salesLogModel->saveData("sales_log");
                        // }
                    }

                    if (!empty($carrierID)) {
                        $paramLog['actions'] = "update carrier ID #{$salesInvoiceArray['invoice_no']} to $carrierID, request by: $requestBy";
                        // if (!$this->checkLogByActions($paramLog['actions'],$params['shipment_id'],$paramLog['admin_user_id'])) {
                            $paramLog['affected_field'] = "carrier_id";
                            $paramLog['value'] = $carrierID;
                            $salesLogModel = new \Models\SalesLog();    
                            $salesLogModel->setFromArray($paramLog);                            
                            $salesLogModel->saveData("sales_log");
                        // }
                    }
                }
            }
        }

        return;
    }

    public function checkLogByActions($actions = '', $shipmentID = 0, $adminUserID = 0)
    {
        $isExist = false;
        $sql = "SELECT count(sales_log_id) as count_it FROM sales_log WHERE shipment_id = $shipmentID and admin_user_id = $adminUserID and actions = '$actions' limit 1";
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $resultArray = $result->fetch();
            $countIt = $resultArray['count_it'];
            if ($countIt > 0) {
                $isExist = true;
            }
        }

        return $isExist;
    }

    public function getShipmentDataByInvoiceNo($invoiceNo = '')
    {

        $respond = array('shipment_id' => 0, 'shipment_status' => '');
        if (!empty($invoiceNo)) {
            $sql = "SELECT ss.shipment_id, ss.shipment_status,si.invoice_no,si.store_code, si.delivery_method,
            ss.lead_time_min, ss.lead_time_max, ss.lead_time_unit
            FROM sales_shipment ss left join sales_invoice si on si.invoice_id = ss.invoice_id
            WHERE si.invoice_no = '$invoiceNo' limit 1;";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $shipmentArray = $result->fetch();
                $respond['shipment_id'] = $shipmentArray['shipment_id'];
                $respond['shipment_status'] = $shipmentArray['shipment_status'];
                $respond['store_code'] = $shipmentArray['store_code'];
                $respond['delivery_method'] = $shipmentArray['delivery_method'];
                $respond['lead_time_min'] = $shipmentArray['lead_time_min'];
                $respond['lead_time_max'] = $shipmentArray['lead_time_max'];
                $respond['lead_time_unit'] = $shipmentArray['lead_time_unit'];            
            }
        }

        return $respond;
    }

}
