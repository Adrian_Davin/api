<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Db as Database;

class SalesruleVoucher extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=10, nullable=false)
     */
    public $voucher_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    public $rule_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $voucher_code;

    /**
     *
     * @var decimal
     * @Column(type="decimal", length=10,2, nullable=true)
     */
    public $voucher_amount;

    /**
     * @var $percentage
     * @Column(type="decimal", length=10,2, nullable=true)
     */
    public $percentage;

    /**
     * @var decimal
     * @Column(type="decimal", length=10,2, nullable=true)
     */
    public $max_redemption;

    public $final_amount;

    public $note;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    public $times_used;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    public $limit_used;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $expiration_date;

    /**
     *
     * @var int
     * @Column(type="int", nullable=true)
     */
    public $customer_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var int
     * @Column(type="int", nullable=true)
     */
    public $created_by;

    /**
     * @var
     */
    public $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=6, nullable=false)
     */
    public $type;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $company_code;

    public $member_id;

    public $total_stamp;


    /**
     * @return string
     */
    public function getVoucherCode()
    {
        return $this->voucher_code;
    }

    /**
     * @param string $voucher_code
     */
    public function setVoucherCode($voucher_code)
    {
        $this->voucher_code = $voucher_code;
    }

    /**
     * @return int
     */
    public function getVoucherId()
    {
        return $this->voucher_id;
    }

    /**
     * @param int $voucher_id
     */
    public function setVoucherId($voucher_id)
    {
        $this->voucher_id = $voucher_id;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }
    /**
     * @return int
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * @return decimal
     */
    public function getVoucherAmount()
    {
        return $this->voucher_amount;
    }

    /**
     * @return mixed
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param mixed $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return decimal
     */
    public function getMaxRedemtion()
    {
        return $this->max_redemtion;
    }

    /**
     * @param decimal $max_redemption
     */
    public function setMaxRedemption($max_redemption)
    {
        $this->max_redemption = $max_redemption;
    }

    /**
     * @return int
     */
    public function getTimesUsed()
    {
        return $this->times_used;
    }

    /**
     * @return int
     */
    public function getLimitUsed()
    {
        return $this->limit_used;
    }

    /**
     * @param int $limit_used
     */
    public function setLimitUsed($limit_used)
    {
        $this->limit_used = $limit_used;
    }

    /**
     * Get the value of note
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set the value of note
     *
     * @return  self
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return string
     */
    public function getExpirationDate()
    {
        return $this->expiration_date;
    }

    /**
     * @return string
     */
    public function setExpirationDate($expiration_date)
    {
        $this->expiration_date = $expiration_date;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param string $company_code
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * @return string
     */
    public function getMemberId()
    {
        return $this->member_id;
    }

    /**
     * @param string $member_id
     */
    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }

    /**
     * @return int
     */
    public function getTotalStamp()
    {
        return $this->total_stamp;
    }

    public function setTotalStamp($total_stamp)
    {
        $this->total_stamp = $total_stamp;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('voucher_id', 'Models\SalesruleVoucherUsage', 'voucher_id', array('alias' => 'SalesruleVoucherUsage'));
        $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer', 'reusable' => true));
        $this->belongsTo('rule_id', 'Models\Salesrule', 'rule_id', array('alias' => 'Salesrule', 'reusable' => true));
        $this->belongsTo('created_by', 'Models\AdminUser', 'admin_user_id', array('alias' => 'AdminUser', 'reusable' => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_voucher';
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray, $dataArray));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleVoucher[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleVoucher
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_salesrule_voucher", "TRIGGERED: ".$this->voucher_code);
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        // Get rule this voucher
        $MarketingPromotionCache = \Library\Redis\MarketingPromotion::getPromotionCache(date('Ymd'), ['voucher', 'shopping_cart', 'invoice_created', 'payment', 'shipping', 'exchange_voucher']);
        $voucherList = array();
        $view['rules'] = array();

        if ($MarketingPromotionCache) {
            foreach ($MarketingPromotionCache as $promoType => $rules) {
                if (!empty($rules)) {
                    $salesRule = json_decode($rules, true);
                    foreach ($salesRule as $rule) {
                        if ($rule['rule_id'] == $this->rule_id) {
                            $view['rules'] = $rule;
                            $view['rules']['promo_type'] = $promoType;
                            break;
                        }
                    }
                }
            }
        } else {
            /*
             * @todo : trigger to call it manually
             */
        }

        return $view;
    }

    public function updateSalesruleVoucher()
    {
        $sql = "DELETE FROM salesrule_customer_group";
        $sql .= " WHERE rule_id = " . $this->rule_id;
        $this->useWriteConnection();
        $this->getDi()->getShared($this->getConnection())->query($sql);
    }

    public function updateStatusVoucher($param)
    {
        $response = array();
        try {
            $sql = "UPDATE salesrule_voucher SET status=" . $param['status'];
            $sql .= " WHERE voucher_id = " . $param['voucher_id'];
            $this->useWriteConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            $response['messages'] = "success";
        } catch (\Exception $e) {
            $response['errors'] = "failed";
            $response['messages'] = "";
        }

        return $response;
    }

    public function getDetailVoucher($params)
    {

        $voucher = null;
        $sql = " SELECT sv.voucher_id, sv.rule_id, sv.voucher_code, sv.times_used, sv.limit_used, sv.voucher_amount, sv.percentage, sv.max_redemption, sv.final_amount, sv.expiration_date, sv.customer_id, sv.total_stamp, sv.company_code,
                 sv.created_at, sv.created_by, sv.status, sv.note, sv.type, sr.to_date FROM salesrule_voucher sv LEFT JOIN salesrule sr ON sv.rule_id = sr.rule_id ";

        if (isset($params['voucher_id'])) {
            $sql .= " where sv.voucher_id = " . $params['voucher_id'] . " and sv.status > -1 limit 1 ";
        } elseif (isset($params['voucher_code'])) {
            $sql .= " where sv.voucher_code = '" . $params['voucher_code'] . "' and sv.status > -1 limit 1 ";
        }

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $voucher = $result->fetch();
        }

        return $voucher;
    }

    public function deleteSalesruleCoupon($params)
    {
        $response = array();
        try {
            /**
             * @todo:
             * @Roesmien not found class SalesruleCoupon
             */
            $salesruleCoupon = new \Models\SalesruleCoupon();
            $findDatas = $salesruleCoupon::find("coupon_id = " . $params['coupon_id']);
            if ($findDatas) {
                $this->useWriteConnection();
                $manager = new TxManager();
                $manager->setDbService($this->getConnection());
                $transaction = $manager->get();
                $this->setTransaction($transaction);

                foreach ($findDatas as $findData) {
                    $findData->setTransaction($transaction);
                    if ($findData->delete() === false) {
                        $response['messages'] = array('failed');
                        $response['errors'] = array('failed');

                        $messages = $findData->getMessages();
                        $errMsg = array();
                        foreach ($messages as $message) {
                            $errMsg[] = $message->getMessage();
                        }
                        \Helpers\LogHelper::log('sales_rule_coupon', "Delete data failed, error : " . json_encode($errMsg));

                        $transaction->rollback(
                            "Error when try to delete salesrule coupon"
                        );
                    } else {
                        $response['messages'] = array("success");
                    }
                }

                $transaction->commit();
            }
        } catch (\Exception $e) {
            $response['messages'] = array('failed');
            $response['errors'] = array('failed');

            \Helpers\LogHelper::log("sales_rule_coupon", "save data failed, error : " . $e->getMessage());

            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return $response;
    }

    public function bulkInsert($voucher_code_list = array())
    {
        $customerID = 0;
        if ($this->customer_id) {
            $customerID = $this->customer_id;
        }

        $voucherStatus = 10;
        if (isset($this->status) && intval($this->status) >= 0) {
            $voucherStatus = $this->status;
        }

        $query = "INSERT INTO salesrule_voucher (rule_id, voucher_code, company_code, times_used,limit_used, expiration_date, voucher_amount,percentage,max_redemption, final_amount, created_at, created_by, type,note, customer_id, status) VALUES ";
        foreach ($voucher_code_list as $voucher_code) {
            $query .= "(";
            $query .= $this->rule_id . ", ";
            $query .= "'" . $voucher_code . "', ";
            $query .= "'" . $this->company_code . "', ";
            $query .= "0, ";
            $query .= $this->limit_used . ", ";
            $query .= "'" . $this->expiration_date . "', ";
            $query .= "" . $this->voucher_amount . ", ";
            $query .= "" . (!empty($this->percentage) ? $this->percentage : '0') . ", ";
            $query .= "" . (!empty($this->max_redemption) ? $this->max_redemption : '0') . ", ";
            $query .= "" . (!empty($this->final_amount) ? $this->final_amount : '0') . ", ";
            $query .= "'" . date("Y-m-d H:i:s") . "', ";
            $query .= "'" . $this->created_by . "', ";
            $query .= $this->type . ", ";
            $query .= "'" . $this->note . "', ";
            $query .= $customerID . ", ";
            $query .= $voucherStatus;
            $query .= "),";
        }

        $query = substr($query, 0, -1);

        try {
            $this->useWriteConnection();
            $connection = $this->getConnection();
            $di         = \Phalcon\DI::getDefault();
            $db         = $di[$connection];
            $result     = $db->query($query);

            return true;
        } catch (\Exception $e) {
            $this->errorCode = "RR301";
            $this->errorMessages = "Failed when try to save data";

            \Helpers\LogHelper::log("generate_voucher", "Fail when insert voucher, error : " . $e->getMessage());
            return false;
        }
    }

    public function updateVoucherCustomerID($param = array())
    {
        $response = array();
        try {
            $sql = "UPDATE salesrule_voucher SET status = 10, customer_id = " . $param['customer_id'];
            $sql .= " WHERE voucher_code = '" . $param['voucher_code'] . "'";
            $this->useWriteConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            $response['messages'] = "success";
        } catch (\Exception $e) {
            $response['errors'] = "failed";
            $response['messages'] = "";
        }

        return $response;
    }

    public function getVoucherUsageAndLimit($voucher_code) {
                $this->useReadOnlyConnection();
        $sql = "select".
        "    srv.limit_used as limit_used,".
        "    srv.times_used as times_used,".
        "    srv.max_redemption as max_redemption,".
        "    srv.rule_id as rule_id".
        " from".
        "    salesrule_voucher srv".
        " where".
        " srv.voucher_code = '" . $voucher_code . "';";
        
        $voucher = null;
        $queryResult = $this->getDi()->getShared($this->getConnection())->query($sql);

        $queryResult->setFetchMode(Database::FETCH_ASSOC);
        $voucher = $queryResult->fetch();

        return $voucher;
    }

    public function getVoucherUsageAndLimitByRuleIDCustomerIDAndDate(
        $customer_id,
        $rule_id,
        $start_date,
        $end_date
    ) {
        $this->useReadOnlyConnection();

        $sql = "select" .
            "    srv.voucher_code as voucher_code," .
            "    srv.limit_used as limit_used," .
            "    srv.times_used as times_used," .
            "    srv.max_redemption as max_redemption," .
            "    srv.note as note," .
            "    DATE(srv.created_at) as created_at," .
            "    DATE(so.created_at) as voucher_usage_created_at" .
            " from" .
            "    salesrule_voucher srv" .
            " left join" .
            "    salesrule_order sro" .
            " on" .
            "    srv.voucher_code = sro.voucher_code" .
            " left join" .
            "    sales_order so" .
            " on" .
            "    sro.sales_order_id = so.sales_order_id" .
            " where" .
            " srv.customer_id = '" . $customer_id . "' AND" .
            " srv.rule_id = '" . $rule_id . "' AND" .
            " (srv.created_at >= DATE('" . $start_date . "') AND srv.created_at <= DATE('" . $end_date . "'))" .
            " ORDER BY srv.created_at DESC";

        $voucher = null;
        $queryResult = $this->getDi()->getShared($this->getConnection())->query($sql);

        $queryResult->setFetchMode(Database::FETCH_ASSOC);
        $voucher = $queryResult->fetchAll();

        return $voucher;
    }

    public function checkVoucherGenerateQuantity($rule_id) {
        $response = array(
            'return' => false,
            'check' => false
        );

        try {
            $sql = 'SELECT count(voucher_code) as totalVoucher from salesrule_voucher WHERE rule_id = ' .$rule_id;
            $result = $this->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $totalVoucher = $result->fetchAll()[0]['totalVoucher'];
            if ($totalVoucher >= 1) {
                //ceck publish description
                $sql = 'SELECT publish_description from salesrule WHERE rule_id = ' .$rule_id;
                $result = $this->getDi()->getShared('dbMaster')->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $publishDesc = $result->fetchAll()[0]['publish_description'];

                if ($publishDesc !== '') {
                    $response['check'] = true;
                }
            }
            $response['return'] = true;

        } catch (\Exception $e) {
            \Helpers\LogHelper::log("check_voucher_salesrule", "Failed to check voucher at salesrule_voucher on generate voucher, error : " . $e->getMessage());
        }

        return $response;
    }
}
