<?php

namespace Models;

use Helpers\LogHelper;
class ProductCategoryMappingBrand extends \Models\BaseModel{
    protected $product_category_mapping_brand_id;
    protected $category_id;
    protected $brand_id;
    protected $company_code;

    public function initialize(){
        $this->setSource("product_category_mapping_brand");
    }

    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false){
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}