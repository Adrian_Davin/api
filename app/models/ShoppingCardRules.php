<?php

class ShoppingCardRules extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $card_rule_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $card_rule_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $date_start;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $date_end;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'shopping_card_rules';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ShoppingCardRules[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ShoppingCardRules
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
