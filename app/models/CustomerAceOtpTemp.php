<?php

namespace Models;

use Phalcon\Db as Database;

class CustomerAceOtpTemp extends \Models\BaseModel
{
    protected $customer_ace_otp_temp_id;
    protected $email;
    protected $phone;
    protected $access_code;
    protected $validate_count;
    protected $request_count;
    protected $customer_id;
    protected $type;
    protected $created_at;
    protected $updated_at;

    public function initialize()
    {
        //
    }
    /**
     * @return mixed
     */
    public function getCustomerAceOtpTempId()
    {
        return $this->customer_ace_otp_temp_id;
    }

    /**
     * @param mixed $customer_ace_otp_temp_id
     */
    public function setCustomerAceOtpTempId($customer_ace_otp_temp_id)
    {
        $this->customer_ace_otp_temp_id = $customer_ace_otp_temp_id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAccessCode()
    {
        return $this->access_code;
    }

    /**
     * @param mixed $access_code
     */
    public function setAccessCode($access_code)
    {
        $this->access_code = $access_code;
    }

    /**
     * @return mixed
     */
    public function getValidateCount()
    {
        return $this->validate_count;
    }

    /**
     * @param mixed $validate_count
     */
    public function setValidateCount($validate_count)
    {
        $this->validate_count = $validate_count;
    }

    /**
     * @return mixed
     */
    public function getRequestCount()
    {
        return $this->request_count;
    }

    /**
     * @param mixed $request_count
     */
    public function setRequestCount($request_count)
    {
        $this->request_count = $request_count;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param mixed $customer_id
     */
    public function setCustomerId($customer_id): void
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        return $this->toArray($columns, $showEmpty);
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}