<?php

namespace Models;

class MasterCcBank extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $cc_bank_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=6, nullable=true)
     */
    protected $bin_start;

    /**
     *
     * @var integer
     * @Column(type="integer", length=6, nullable=true)
     */
    protected $bin_end;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $card_type;

    /**
     *
     * @var string
     * @Column(type="string", length=5, nullable=true)
     */
    protected $country_code;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $bank_name;

    /**
     * Method to set the value of field cc_bank_id
     *
     * @param integer $cc_bank_id
     * @return $this
     */
    public function setCcBankId($cc_bank_id)
    {
        $this->cc_bank_id = $cc_bank_id;

        return $this;
    }

    /**
     * Method to set the value of field bin_start
     *
     * @param integer $bin_start
     * @return $this
     */
    public function setBinStart($bin_start)
    {
        $this->bin_start = $bin_start;

        return $this;
    }

    /**
     * Method to set the value of field bin_end
     *
     * @param integer $bin_end
     * @return $this
     */
    public function setBinEnd($bin_end)
    {
        $this->bin_end = $bin_end;

        return $this;
    }

    /**
     * Method to set the value of field card_type
     *
     * @param string $card_type
     * @return $this
     */
    public function setCardType($card_type)
    {
        $this->card_type = $card_type;

        return $this;
    }

    /**
     * Method to set the value of field country_code
     *
     * @param string $country_code
     * @return $this
     */
    public function setCountryCode($country_code)
    {
        $this->country_code = $country_code;

        return $this;
    }

    /**
     * Method to set the value of field bank_name
     *
     * @param string $bank_name
     * @return $this
     */
    public function setBankName($bank_name)
    {
        $this->bank_name = $bank_name;

        return $this;
    }

    /**
     * Returns the value of field cc_bank_id
     *
     * @return integer
     */
    public function getCcBankId()
    {
        return $this->cc_bank_id;
    }

    /**
     * Returns the value of field bin_start
     *
     * @return integer
     */
    public function getBinStart()
    {
        return $this->bin_start;
    }

    /**
     * Returns the value of field bin_end
     *
     * @return integer
     */
    public function getBinEnd()
    {
        return $this->bin_end;
    }

    /**
     * Returns the value of field card_type
     *
     * @return string
     */
    public function getCardType()
    {
        return $this->card_type;
    }

    /**
     * Returns the value of field country_code
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }

    /**
     * Returns the value of field bank_name
     *
     * @return string
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_cc_bank';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCcBank[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCcBank
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function saveToCache($params = array())
    {

        $bankData = self::find();

        $bankCache = [];
        foreach ($bankData as $bank) {
            if(!empty($bank->getBinEnd())) {
                for($i=$bank->getBinStart();$i<=$bank->getBinEnd();$i++) {
                    $bankCache[] = [
                        'bin' => $i,
                        'card_type' => $bank->getCardType(),
                        'country_code' => $bank->getCountryCode(),
                        'bank_name' => $bank->getBankName()
                    ];
                }
            } else {
                $bankCache[] = [
                    'bin' => $bank->getBinStart(),
                    'card_type' => $bank->getCardType(),
                    'country_code' => $bank->getCountryCode(),
                    'bank_name' => $bank->getBankName()
                ];
            }
        }

        if(!empty($bankCache)) {
            foreach ($bankCache as $cache) {
                \Library\Redis\MasterCCBank::setBank($cache['bin'],$cache);
            }
        }

        return true;
    }

    public function deleteFromCache($params = array())
    {
        \Library\Redis\MasterCCBank::deleteBank($params['bin']);

        return;
    }

}
