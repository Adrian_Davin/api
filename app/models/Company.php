<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 1:58 PM
 */

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class Company extends \Models\BaseModel
{

    protected $company_id;
    protected $vendor_sap_id;
    protected $name;
    protected $source_from;
    protected $order_type;
    protected $sales_grp;
    protected $status;
    protected $created_at;
    protected $updated_at;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getOrderType()
    {
        return $this->order_type;
    }

    /**
     * @return mixed
     */
    public function getSalesGrp()
    {
        return $this->sales_grp;
    }

    /**
     * @return mixed
     */
    public function getVendorSapId()
    {
        return $this->vendor_sap_id;
    }

    /**
     * @return mixed
     */
    public function getSourceFrom()
    {
        return $this->source_from;
    }

    public function initialize()
    {
        $this->hasMany('company_id', '\Models\CompanyAddress', 'company_id', array('alias' => 'CompanyAddress'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'company';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }
}