<?php

namespace Models;

class FakturPajakInvoice extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_order_id;
    protected $company_name;
    protected $company_npwp;
    protected $company_address;
    protected $recipient_email;
    protected $npwp_image;
    protected $created_at;
    protected $status;
    protected $invoice_id;
    protected $invoice_no;
    protected $customer_company_id;
    protected $is_npwp_address;
    protected $nitku;
    protected $tipe_wajib_pajak;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'faktur_pajak_invoice';
    }
    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {        
        $this->setSource("faktur_pajak_invoice");
        // $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
    }  

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return FakturPajakInvoice[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return FakturPajakInvoice
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);
        // get not send data but have in this parameter       
        $thisArray = get_class_vars(get_class($this));        
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
        return $view;
    }    

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param $errors mixed
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Get the value of sales_order_id
     *
     * @return  integer
     */ 
    public function getSales_order_id()
    {
        return $this->sales_order_id;
    }

    /**
     * Set the value of sales_order_id
     *
     * @param  integer  $sales_order_id
     *
     * @return  self
     */ 
    public function setSales_order_id($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;

        return $this;
    }

      /**
     * Get the value of customer_company_id
     */ 
    public function getCustomer_company_id()
    {
        return $this->customer_company_id;
    }

    /**
     * Set the value of customer_company_id
     *
     * @return  self
     */ 
    public function setCustomer_company_id($customer_company_id)
    {
        $this->customer_company_id = $customer_company_id;

        return $this;
    }

    /**
     * Get the value of company_name
     */ 
    public function getCompany_name()
    {
        return $this->company_name;
    }

    /**
     * Set the value of company_name
     *
     * @return  self
     */ 
    public function setCompany_name($company_name)
    {
        $this->company_name = $company_name;

        return $this;
    }

    /**
     * Get the value of company_npwp
     */ 
    public function getCompany_npwp()
    {
        return $this->company_npwp;
    }

    /**
     * Set the value of company_npwp
     *
     * @return  self
     */ 
    public function setCompany_npwp($company_npwp)
    {
        $this->company_npwp = $company_npwp;

        return $this;
    }

    /**
     * Get the value of company_address
     */ 
    public function getCompany_address()
    {
        return $this->company_address;
    }

    /**
     * Set the value of company_address
     *
     * @return  self
     */ 
    public function setCompany_address($company_address)
    {
        $this->company_address = $company_address;

        return $this;
    }

    /**
     * Get the value of recipient_email
     */ 
    public function getRecipient_email()
    {
        return $this->recipient_email;
    }

    /**
     * Set the value of recipient_email
     *
     * @return  self
     */ 
    public function setRecipient_email($recipient_email)
    {
        $this->recipient_email = $recipient_email;

        return $this;
    }

    /**
     * Get the value of npwp_image
     */ 
    public function getNpwp_image()
    {
        return $this->npwp_image;
    }

    /**
     * Set the value of npwp_image
     *
     * @return  self
     */ 
    public function setNpwp_image($npwp_image)
    {
        $this->npwp_image = $npwp_image;

        return $this;
    }

    /**
      * Get the value of is_npwp_address
      */ 

      public function getIsNpwpAddress()
      {
            return $this->is_npwp_address;
      }
  
      /**
        * Set the value of is_npwp_address
        *
        * @return  self
      */ 
      public function setIsNpwpAddress($is_npwp_address)
      {
           $this->is_npwp_address = $is_npwp_address;
   
           return $this;
      }

      /**
      * Get the value of nitku
      */ 

      public function getNitku()
      {
            return $this->nitku;
      }
  
      /**
        * Set the value of nitku
        *
        * @return  self
      */ 
      public function setNitku($nitku)
      {
           $this->nitku = $nitku;
   
           return $this;
      }

      /**
      * Get the value of tipe_wajib_pajak
      */ 

      public function getTipeWajibPajak()
      {
            return $this->tipe_wajib_pajak;
      }
  
      /**
        * Set the value of tipe_wajib_pajak
        *
        * @return  self
      */ 
      public function setTipeWajibPajak($tipe_wajib_pajak)
      {
           $this->tipe_wajib_pajak = $tipe_wajib_pajak;
   
           return $this;
      }

    /**
     * Get the value of created_at
     */ 
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */ 
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

}
