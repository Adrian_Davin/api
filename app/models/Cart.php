<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 10:36 AM
 */

namespace Models;

use \Library\Mongodb;
use \Library\Marketing as MarketingLib;
use \Library\APIWrapper;
use \Library\SalesOrder as SalesOrderLib;
use \Library\Cart as CartLibrary;
use MongoDB\InsertOneResult;
use \Phalcon\Text as PhalconText;
use \Models\CartItem\Collection as ItemsCollection;
use \Helpers\SimpleEncrypt;
use \Helpers\Transaction as TransactionHelper;
use \Helpers\LogHelper;
use \Helpers\CartHelper;
use \MongoDB\BSON\ObjectID as MongoDbObjectId;
use \Phalcon\Db;

/**
 * Class Cart
 * @package Models
 *
 * @todo : add redirect_url --> where to go from payment
 */

class Cart
{
    protected $cart_id;
    protected $minicart_id;
    protected $token;
    protected $remote_ip;
    protected $refund_no;
    protected $device;
    protected $status;
    protected $cart_type;
    protected $total_qty_item;
    protected $reserved_order_no;
    protected $reference_order_no;
    protected $subtotal;
    protected $discount_amount;
    protected $company_code = 'ODI';
    protected $store_code = '';
    protected $handling_fee_adjust = 0;
    protected $shipping_amount;
    protected $shipping_discount_amount;
    protected $grand_total_exclude_shipping;
    protected $grand_total;
    protected $cart_rules = '[]';
    protected $gift_cards_amount = 0;
    protected $gift_cards = '[]';
    protected $customer_note;
    protected $split_cost = '[]';
    protected $created_at;
    protected $updated_at;

    protected $redirect_url = "";
    protected $send_email = "yes";

    protected $source_order;
    protected $source_order_id;
    protected $source_order_no;

    protected $limit_payment_time = 0;
    protected $po;
    protected $utm_parameter = '';
    protected $affiliate_id = '';
    protected $customer_is_new;

    /**
     * @var $customer \Models\SalesCustomer
     */
    protected $customer;

    /**
     * @var $shipping \Models\SalesOrderAddress
     */
    protected $shipping_address;

    /**
     * @var $shipping \Models\SalesOrderAddress
     */
    protected $billing_address;

    /**
     * @var $shipping \Models\SalesOrderPayment
     */
    protected $payment;

    /**
     * @var $payer
     */
    protected $payer;
    /**
     * @var $items \Models\CartItem\Collection
     */
    protected $items;

    /**
     * @var $tracker \Models\SalesOrderTracker
     */
    protected $tracker;

    /**
     * mongo db Collections
     * @var $collection
     */
    protected $collection;
    protected $error_code;
    protected $error_messages = [];

    private $_current_discount = [];

    public function __construct()
    {
        $mongoDb = new Mongodb();
        $mongoDb->setCollection(getenv("CART_COLLECTION"));
        $this->collection = $mongoDb->getCollection();

        $this->subtotal = 0;
        $this->handling_fee_adjust = 0;
        $this->discount_amount = 0;
        $this->shipping_amount = 0;
        $this->shipping_discount_amount = 0;
        $this->grand_total = 0;
        $this->total_qty_item = 0;
        $this->status = "pending";
        $this->redirect_url = $_ENV['BASE_URL'];
    }

    public function initialize($cartId = "")
    {
        if(!empty($this->cart_id)) {
            $cartId = $this->cart_id;
        }

        if(empty($cartId)) {
            /** @var InsertOneResult $insertOneResult */
            // $insertOneResult = $this->collection->insertOne([]);

            // if($insertOneResult->getInsertedCount() > 0)
            // {
            //     $lastInsertId = (string)$insertOneResult->getInsertedId();

            //     $this->created_at = date("Y-m-d H:i:s");
            //     $this->cart_id = $lastInsertId;

            //     // Generate tokens
            //     $token = $lastInsertId.strtolower(PhalconText::random(PhalconText::RANDOM_ALNUM , 3));
            //     $this->token = SimpleEncrypt::encrypt($token);
            // } else {
            //     $this->error_code = "501";
            //     $this->error_messages['type'] = "global";
            //     $this->error_messages['value'] = "";
            //     $this->error_messages['message'] = "Failed to initiate shopping cart";
            //     return false;
            // }

            $this->error_code = "400";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "cart_id not found";
            return false;
        } else {
            $this->cart_id = $cartId;

            $cartDataArray = $this->loadCartData(true);
            if($cartDataArray) {
                foreach ($cartDataArray as $key => $val) {
                    if (property_exists($this, $key)) {
                        $this->{$key} = $val;
                    }

                    if (!empty($val)) {
                        if ($key == 'items') {
                            $items = array();
                            foreach ($val as $item) {
                                $itemObj = new CartItem();
                                $itemObj->initialize($item);
                                $items[] = $itemObj;
                            }

                            $this->items = new ItemsCollection($items);
                        }

                        if ($key == 'customer') {
                            $customer = new SalesCustomer();
                            $customer->setFromArray($val);
                            $customer->checkCustomerType($customer->getCustomerId());
                            $this->customer = $customer;
                        }

                        if ($key == 'billing_address') {
                            $billingAddress = new SalesOrderAddress();
                            $val['address_type'] = "billing";
                            $billingAddress->setFromArray($val);
                            $this->billing_address = $billingAddress;
                        }

                        if ($key == 'payment') {
                            $payment = new SalesOrderPayment();

                            if (empty($val['expire_transaction'])) {
                                $custom_expiry = 24;
                                if(!empty($this->limit_payment_time)) {
                                    $custom_expiry = $this->limit_payment_time;
                                } else if(!empty($_ENV['BANK_TRANSFER_EXPIRED'])) {
                                    $custom_expiry = $_ENV['BANK_TRANSFER_EXPIRED'];
                                }

                                $val['expire_transaction'] = date("Y-m-d H:i:s", strtotime('+'.$custom_expiry.' hours'));
                            }

                            $payment->setFromArray($val);
                            $this->payment = $payment;
                        }
                        /**
                         * @todo : Uncomment if we already have payer table
                         */
                        /*
                        if(!empty($key == 'payer')) {
                            $payer = new SalesOrderPayer();
                            $payer->setFromArray($data_array['payer']);
                            $this->payer = $payer;
                        }
                        */
                    }
                }
            }
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getCartId()
    {
        return $this->cart_id;
    }

    /**
     * @param mixed $cart_id
     */
    public function setCartId($cart_id)
    {
        $this->cart_id = $cart_id;
    }
    public function setMiniCartId($minicart_id)
    {
        $this->minicart_id = $minicart_id;
    }
    public function getMiniCartId($minicart_id)
    {
        $this->minicart_id = $minicart_id;
    }

    /**
     * @return mixed
     */
    public function getReservedOrderNo()
    {
        return $this->reserved_order_no;
    }

    /**
     * @param mixed $reserved_order_no
     */
    public function setReservedOrderNo($reserved_order_no)
    {
        $this->reserved_order_no = $reserved_order_no;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return SalesOrderAddress
     */
    public function getShippingAddress()
    {
        return $this->shipping_address;
    }

    /**
     * @param SalesOrderAddress $shipping_address
     */
    public function setShippingAddress($shipping_address)
    {
        $this->shipping_address = $shipping_address;
    }

    /**
     * @param int $shipping_amount
     */
    public function setShippingAmount($shipping_amount)
    {
        $this->shipping_amount = $shipping_amount;
    }

    /**
     * @return int
     */
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

    /**
     * @return string
     */
    public function getGiftCards()
    {
        return $this->gift_cards;
    }

    /**
     * @param string $gift_cards
     */
    public function setGiftCards($gift_cards)
    {
        $this->gift_cards = $gift_cards;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->error_messages;
    }

    /**
     * @return int
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * @return array
     */
    public function getCurrentDiscount()
    {
        return $this->_current_discount;
    }

    /**
     * @return int
     */
    public function getShippingDiscountAmount()
    {
        return $this->shipping_discount_amount;
    }

    /**
     * @param int $shipping_discount_amount
     */
    public function setShippingDiscountAmount($shipping_discount_amount)
    {
        $this->shipping_discount_amount = $shipping_discount_amount;
    }

    /**
     * @return int
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param int $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return int
     */
    public function getGrandTotal()
    {
        return $this->grand_total;
    }

    /**
     * @param int $grand_total
     */
    public function setGrandTotal($grand_total)
    {
        $this->grand_total = $grand_total;
    }

    /**
     * @return string
     */
    public function getCartType()
    {
        return $this->cart_type;
    }

    /**
     * @param string $grand_total
     */
    public function setCartType($cartType)
    {
        $this->cart_type = $cartType;
    }

    /**
     * Get the value of affiliate_id
     */ 
    public function getAffiliate_id()
    {
        return $this->affiliate_id;
    }

    /**
     * Set the value of affiliate_id
     *
     * @return  self
     */ 
    public function setAffiliate_id($affiliate_id)
    {
        $this->affiliate_id = $affiliate_id;

        return $this;
    }

    public function getCustomerIsNew()
    {
        return $this->customer_is_new;
    }

    /**
     * Set the value of customer_is_new
     *
     * @return  self
     */ 
    public function setCustomerIsNew($customer_is_new)
    {
        $this->customer_is_new = $customer_is_new;

        return $this;
    }

    /**
     * @param array $payment_data
     * payment_data passed from paymentapi
     */
    public function setPayment($payment_data = array())
    {
        if (empty($payment_data['expire_transaction'])) {
            $custom_expiry = 24;
            if(!empty($this->limit_payment_time)) {
                $custom_expiry = $this->limit_payment_time;
            } else if(!empty($_ENV['BANK_TRANSFER_EXPIRED'])) {
                $custom_expiry = $_ENV['BANK_TRANSFER_EXPIRED'];
            }

            $payment_data['expire_transaction'] = date("Y-m-d H:i:s", strtotime('+'.$custom_expiry.' hours'));
        }

        if(empty($this->payment)) {
            $payment = new SalesOrderPayment();
            $payment->setFromArray($payment_data);

            $this->payment = $payment;
        } else {
            $this->payment->setFromArray($payment_data);
        }
    }

    public function setPoNumber($po_data = array())
    {
        unset($po_data['cart_id']);
        $this->po = $po_data;
    }

    /**
     * @return mixed
     */
    public function getSourceOrder()
    {
        return $this->source_order;
    }

    public function resetShippingRate()
    {
        $this->shipping_amount = 0;
        $this->shipping_discount_amount = 0;
        $this->split_cost = "";
        $this->handling_fee_adjust = 0;

        if(!empty($this->items)) {
            $this->items->resetShippingRate();
        }

        // Reset promotion for shipping
        $cartRules = json_decode($this->cart_rules,true);
        if(!empty($cartRules)) {
            foreach ($cartRules as $key => $appliedRule) {
                if(isset($appliedRule['action']['attributes'])){
                    if($appliedRule['action']['attributes'] == 'shipping_amount') {
                        unset($cartRules[$key]);
                    }
                }
                
            }
        }

        $this->cart_rules = json_encode($cartRules);
    }

    /**
     * Set object properties from $data_array
     * @param array $data_array array containing data to be set to object properties
     */
    public function setFromArray($data_array = array())
    {
        foreach($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }

            if(!empty($val)) {
                if($key == 'shipping_address' ) {
                    $shippingAddress = new SalesOrderAddress();
                    $val['address_type'] = "shipping";
                    $shippingAddress->setFromArray($val);
                    $this->shipping_address = $shippingAddress;
                }

                if($key == 'items') {
                    $items = array();

                    $kecamatan_code = getenv('DEFAULT_SHIPPING_KECAMATAN_CODE');
                    if(!empty($this->shipping_address)) {
                        $shippingAddress = $this->shipping_address->getDataArray();
                        if(!empty($shippingAddress['kecamatan']['kecamatan_code'])) {
                            $kecamatan_code = $shippingAddress['kecamatan']['kecamatan_code'];
                        }
                    }

                    $shippingAddress = new SalesOrderAddress();
                    foreach($val as $item) {
                        $item['source_order'] = $this->source_order;
                        $itemObj = new CartItem();

                        if (isset($item['delivery_method'])) {
                            $deliveryGroup = array('delivery','store_fulfillment','ownfleet');
                            if (in_array($item['delivery_method'], $deliveryGroup) && !isset($item['shipping_address'])) {
                                // set default address to kembangan
                                $defaultShippingLocation = [
                                    "address_type" => 'shipping', 
                                    "country_id" => getenv("DEFAULT_SHIPPING_COUNTRY_ID"),
                                    "province_id" => getenv("DEFAULT_SHIPPING_PROVINCE_ID"),
                                    "city_id" => getenv("DEFAULT_SHIPPING_CITY_ID"),
                                    "kecamatan_id" => getenv("DEFAULT_SHIPPING_KECAMATAN_ID"),
                                    "geolocation" => getenv("DEFAULT_GEOLOCATION")
                                ];

                                $shippingAddress->setFromArray($defaultShippingLocation);
                                $item['shipping_address'] = $shippingAddress->getDataArray();
                                //\Helpers\LogHelper::log("bug_cart","/models/Cart.php Line 517, data: " . json_encode($data_array),"debug");
                            }
                        }

                        if (!isset($item['is_free_item']) && !isset($item['parent_id'])) {
                            $item['free_items'] = array();
                        } else {
                            if (!empty($item['is_free_item']) && !empty($item['parent_id'])) {
                                $found = array_search($item['parent_id'], array_column($val, 'id'));
                                if ($found === '0' || $found === 0 || $found) {
                                    $val[$found]['shipping_amount'] += $item['shipping_amount'];
                                    $val[$found]['shipping_discount_amount'] += $item['shipping_discount_amount'];
                                    $freeItem['sku'] = $item['sku'];
                                    $freeItem['name'] = $item['name'];
                                    $freeItem['qty_ordered'] = $item['qty_ordered'];
                                    $freeItem['primary_image_url'] = $item['primary_image_url'];
                                    $freeItem['selling_price'] = $item['selling_price'];
                                    $freeItem['url_key'] = $item['url_key'];
                                    $val[$found]['free_items'][] = $freeItem;
                                }
                            }
                        }

                        $itemObj->setFromArray($item);
                        if($itemObj->getErrorCode()) {
                            $this->error_code = $itemObj->getErrorCode();
                            $this->error_messages['type'] = "sku"; 
                            $this->error_messages['value'] = $itemObj->getSku(); 
                            $this->error_messages['message'] = $itemObj->getErrorMessages(); 
                        }

                        $items[] = $itemObj;
                    }

                    $this->items = new ItemsCollection($items);
                    $this->items->kecamatan_code = $kecamatan_code;
                }

                if($key == 'customer') {
                    $customer = new SalesCustomer();
                    if(isset($val['first_name'])) {
                        $val['customer_firstname'] = $val['first_name'];
                        unset($val['first_name']);
                    }

                    if(isset($val['last_name'])) {
                        $val['customer_lastname'] = $val['last_name'];
                        unset($val['last_name']);
                    }

                    if(isset($val['email'])) {
                        $val['customer_email'] = $val['email'];
                        unset($val['email']);
                    }

                    if(isset($val['phone'])) {
                        $val['customer_phone'] = $val['phone'];
                        unset($val['phone']);
                    }

                    $customer->setFromArray($val);
                    $customer->checkCustomerType($customer->getCustomerId());
                    $this->customer = $customer;
                }

                if($key == 'billing_address' ) {
                    $billingAddress = new SalesOrderAddress();
                    $val['address_type'] = "billing";
                    $billingAddress->setFromArray($val);
                    $this->billing_address = $billingAddress;
                }

                if($key == 'payment' ) {
                    $payment = new SalesOrderPayment();

                    if (empty($val['expire_transaction'])) {
                        $custom_expiry = 24;
                        if(!empty($this->limit_payment_time)) {
                            $custom_expiry = $this->limit_payment_time;
                        } else if(!empty($_ENV['BANK_TRANSFER_EXPIRED'])) {
                            $custom_expiry = $_ENV['BANK_TRANSFER_EXPIRED'];
                        }

                        $val['expire_transaction'] = date("Y-m-d H:i:s", strtotime('+'.$custom_expiry.' hours'));
                    }

                    $payment->setFromArray($val);
                    $this->payment = $payment;
                }
                
                /**
                 * @todo : Uncomment if we already have payer table
                 */
                /*
                if(!empty($key == 'payer')) {
                    $payer = new SalesOrderPayer();
                    $payer->setFromArray($data_array['payer']);
                    $this->payer = $payer;
                }
                */
            }
        }
    }

    /**
     * generate a new reserved order number (if empty)
     * check for preorder item as well for "PRE" prefix
     */
    public function initiateReserveOrderNo()
    {
        if(empty($this->reserved_order_no)) {
            if($this->checkPreorderItem() == true){
                $this->reserved_order_no = TransactionHelper::generatePreOrderNo($this->company_code);
            }else if(strtolower($this->source_order) == "lazada") {
                $this->reserved_order_no = TransactionHelper::generateLazadaOrderNo($this->source_order_id);
            }else{
                $orderNoSafe = false;
                $changed = false;
                while(!$orderNoSafe){
                    $this->reserved_order_no = TransactionHelper::generateOrderNo($this->company_code);
                    $changed = true;
                    // must check if reserved_order_no has been used or not
                    $salesOrderLib =  new SalesOrderLib;
                    $orderNoSafe = $salesOrderLib->checkOrderNoAvailability($this->reserved_order_no); //checkOrderNoAvailbility($this->reserved_order_no);
                }
                if($changed){
                    $data = array();
                    $data['cart_id'] = $this->cart_id;
                    $data['reserved_order_no'] = $this->reserved_order_no;
                    $cart = new CartLibrary;
                    $cart->updateCartWithoutMarketing($data);
                } 
            }
        }else{
            if($this->checkPreorderItem() == true AND substr($this->reserved_order_no, 0, 3) != 'PRE'){
                $this->reserved_order_no = "PRE".$this->reserved_order_no;
            }elseif($this->checkPreorderItem() == false AND substr($this->reserved_order_no, 0, 3) == 'PRE' ){
                $this->reserved_order_no = str_replace("PRE","",$this->reserved_order_no);
            }
        }
    }

    public function checkStockAndAssignation()
    {
        if(!empty($this->items)) {
            $this->items->checkStockAndAssignation($this->company_code, $this->customer);

            if(!empty($this->items->getErrorCode())) {
                $this->error_code = $this->items->getErrorCode();
                $this->error_messages['type'] = "sku"; 
                $this->error_messages['value'] = $this->items->getSku(); 
                $this->error_messages['message'] = $this->items->getErrorMessages(); 
            }
        }
    }

    public function checkRestrictedEvent()
    {
        if(!empty($this->items) && !empty($this->customer->getCustomerId())) {
            $this->items->checkRestrictedEvent($this->customer->getCustomerId());
        }
    }

    public function checkPreorderItem(){

        if(!empty($this->items)) {
            $items = !empty($this->items->getDataArray()) ? $this->items->getDataArray() : array();
            foreach ($items as $item) {
                if ($item['status_preorder'] == true) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * All lazada item must be assign to DC and we no need to return any error
     */
    public function assignLazadaItem()
    {
        if(!empty($this->items)) {
            $this->items->assignLazadaItem();
        }
    }

    public function countSubtotalAndQtyItems()
    {
        if(!empty($this->items)) {
            $itemTotalSummary = $this->items->countTotal();

            $this->total_qty_item = $itemTotalSummary[0];
            $this->subtotal = $itemTotalSummary[2];
        }
    }

    public function completeCustomerInformation()
    {
        $customerModel = new Customer();
        $param = "";

        /**
         * if customer object is empty, set as guest
         */
        if(empty($this->customer)) {
            $this->customer = new SalesCustomer();
            $this->customer->setCustomerIsGuest(1);
            $this->customer->setCustomerType("B2C");
            $this->customer->setCustomerGroup([0]);
        } else if(!empty($this->customer->getCustomerEmail()) || !empty($this->customer->getCustomerId())) {
            if ($this->customer->getCustomerIsGuest() != 0) {
                // Assume this customer is a guest
                $this->customer->setCustomerIsGuest(1);
            }

            if(!empty($this->customer->getCustomerId())) {
                $param = "customer_id = '" . $this->customer->getCustomerId() ."'";
            } else if(!empty($this->customer->getCustomerEmail())) {
                $param = "email = '" . $this->customer->getCustomerEmail() ."'";
            }

            $param .= " AND company_code = '". $this->company_code ."'";

            $customerResult = $customerModel->findFirst($param);

            // If customer is found then we need to set needed customer data to cart
            if($customerResult) {
                // if data exist from payload then replace it
                $custArray = $this->customer->toArray();
                $customerFirstname = (isset($custArray['customer_firstname']) && !empty($custArray['customer_firstname'])) ? $custArray['customer_firstname'] : $customerResult->getFirstName();
                $customerLastname = (isset($custArray['customer_lastname']) && !empty($custArray['customer_lastname'])) ? $custArray['customer_lastname'] : $customerResult->getLastName();
                $customerPhone = (isset($custArray['customer_phone']) && !empty($custArray['customer_phone'])) ? $custArray['customer_phone'] : $customerResult->getPhone();

                $this->customer->setCustomerId($customerResult->getCustomerId());
                $this->customer->setCustomerEmail($customerResult->getEmail());
                $this->customer->setCustomerFirstname($customerFirstname);
                $this->customer->setCustomerLastname($customerLastname);
                $this->customer->setCustomerPhone($customerPhone);
                $this->customer->setRegisteredBy($customerResult->getRegisteredBy());
                $this->customer->setCustomerIsNew(intval($customerResult->getIsNew()));
                $this->customer->checkCustomerType($this->customer->getCustomerId());

                if (!isset($custArray['customer_is_guest'])) {
                    if ($customerResult->getStatus() >= 10) {
                        $this->customer->setCustomerIsGuest(0);
                    } else {
                        $this->customer->setCustomerIsGuest(1);
                    }
                } else {
                    $this->customer->setCustomerIsGuest($custArray['customer_is_guest']);
                }

                if(!empty($customerResult->customer2Group)) {
                    if (!empty($customerResult->customer2Group->count())) {
                        $groups = [];
                        foreach ($customerResult->customer2Group as $group) {
                            $groups[] = $group->getGroupId();
                        }
                        $this->customer->setCustomerGroup($groups);
                    }
                }
            } else if (empty($customerResult) && !empty($this->customer->getCustomerEmail())) {
                // If customer not found and we have his email, registered this customer as guest
                $customerData = [
                    "first_name" => $this->customer->getCustomerFirstname(),
                    "last_name" => $this->customer->getCustomerLastname(),
                    "phone" => $this->customer->getCustomerPhone(),
                    "email" => $this->customer->getCustomerEmail(),
                    "status" => 5,
                    'company_code' => $this->company_code
                ];
                $customerModel->setFromArray($customerData);
                $customerModel->saveData();

                $this->customer->setCustomerId($customerModel->getCustomerId());
                $this->customer->setCustomerIsNew(10);
                $this->customer->checkCustomerType($customerModel->getCustomerId());
                $this->customer->setCustomerGroup([0]);
            }
        }

        if ($this->customer->getCustomerIsGuest() == 1 && !empty($this->shipping_address)) {
            /**
             * For guest customer, they input they name only in shipping information
             * grab that information and put it as customer name
             */
            if(!empty($this->shipping_address->getFirstName()) || !empty($this->shipping_address->getLastName())) {
                $firstName = $this->shipping_address->getFirstName();
                $lastName = $this->shipping_address->getLastName();
                $this->customer->setCustomerFirstname($firstName);
                $this->customer->setCustomerLastname($lastName);
            }
        }
    }

    public function completeShippingInformation()
    {
        if ($this->items) {
            $this->items->completeShippingAddress();
            $itemDataArray = $this->items->getDataArray();
            if (empty($this->billing_address)) {
                foreach($itemDataArray as $rowItem) {
                    if (count($rowItem['shipping_address']) > 0 && empty($this->billing_address)) {
                        $this->billing_address = $rowItem['shipping_address'];
                        break;
                    }
                }
            } else {
                $billingData = $this->billing_address->getDataArray();
                if (empty($billingData['first_name'])) {
                    foreach($itemDataArray as $rowItem) {
                        if (count($rowItem['shipping_address']) > 0 && !empty($rowItem['shipping_address']['first_name'])) {
                            $this->billing_address = $rowItem['shipping_address'];
                            break;
                        }
                    }
                }
            }
        }
    }

    public function countGrandTotal()
    {
        // count grant total
        $subtotalWithHandling = $this->subtotal + $this->handling_fee_adjust;
        $this->grand_total = TransactionHelper::countTotal($subtotalWithHandling, $this->discount_amount, $this->shipping_amount,$this->shipping_discount_amount, $this->gift_cards_amount);

        $this->countGrandTotalExcludeShipping();
    }

    public function countGrandTotalExcludeShipping()
    {
        $subtotalWithHandling = $this->subtotal + $this->handling_fee_adjust;
        $grandTotalExcludeShipping = $subtotalWithHandling - ($this->discount_amount + $this->gift_cards_amount);
        if($grandTotalExcludeShipping <= 0) {
            $this->grand_total_exclude_shipping = 0;
        } else {
            $this->grand_total_exclude_shipping = $grandTotalExcludeShipping;
        }
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);
        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) == 'object') {
                if($key == 'collection') {
                    continue;
                }

                $view[$key] = $thisArray[$key]->getDataArray(); // get everything
                // if ($key == 'items') {
                //     $iterateItem = 0;
                //     foreach($view[$key] as $rowItem) {
                //         $deliveryMethod3PL = array('delivery','store_fulfillment');
                //         if (in_array($rowItem['delivery_method'], $deliveryMethod3PL)) {
                //             // For add to cart only
                //             if (!isset($view[$key][$iterateItem]['shipping']['shipping_address'])) {
                //                 // So dont remove shipping_address from cart model [important]
                //                 $shippingAddress = $thisArray['shipping']['shipping_address']->getDataArray();
                //                 $shippingAddress['geolocation'] = getenv('DEFAULT_GEOLOCATION');
                //                 $view[$key][$iterateItem]['shipping_address'] = $shippingAddress;
                                
                //                 // set default cart carrier ID
                //                 $view[$key][$iterateItem]['carrier_id'] = getenv("JNE_CARRIER_ID");
                //             }
                //         } else if ($rowItem['delivery_method'] == 'ownfleet') {
                //             if (!isset($view[$key][$iterateItem]['shipping_address'])) {
                //                 $shippingAddress = $thisArray['shipping_address']->getDataArray();
                //                 $shippingAddress['geolocation'] = getenv('DEFAULT_GEOLOCATION');
                //                 $view[$key][$iterateItem]['shipping_address'] = $shippingAddress;
                //             }

                //             $view[$key][$iterateItem]['carrier_id'] = (isset($rowItem['carrier_id'])) ? $rowItem['carrier_id'] : '';
                //         } else {
                //             $view[$key][$iterateItem]['shipping_address'] = (isset($rowItem['shipping_address'])) ? $rowItem['shipping_address'] : array();
                //             $view[$key][$iterateItem]['carrier_id'] = (isset($rowItem['carrier_id'])) ? $rowItem['carrier_id'] : '';
                //         }

                //         $iterateItem++;
                //     } 
                // }
            } else if(strpos($key, "_") !== 0) {
                $view[$key] = $val;
            }
        }

        // TODO: perlu di recount lg gak?
        // recount grand total
        $subtotalWithHandling = $this->subtotal + $this->handling_fee_adjust;
        $view['grand_total'] = TransactionHelper::countTotal($subtotalWithHandling, $this->discount_amount, $this->shipping_amount,$this->shipping_discount_amount, $this->gift_cards_amount);

        unset($view['shipping_address']);
        unset($view['error_messages']);
        unset($view['error_code']);
        
        return $view;
    }

    /**
     * @param array $data - data from front-end
     *
     */
    public function setCartData($data = array())
    {
        // first checking
        $newCartData = false;

        if (!empty($data['cart_id'])) {
            $this->cart_id = $data['cart_id'];
            $newCartData = $this->loadCartData();
            if ($newCartData === false) {
                $this->error_code = "";
                $this->error_messages['type'] = "";
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = "";
                unset($data['cart_id']);
                $this->cart_id = "";
            }
        }

        // shopping cart already exist in mongodb
        if($newCartData) {
            $this->cart_id = $data['cart_id'];
            /**
             * Data get from mongo
             * @var $newCartData
             */
            // $newCartData = $this->loadCartData();

            /**
             * If cart is not 'pending', meaning it's being processed (as order)
             */
            if($newCartData && (empty($newCartData) || (isset($newCartData['status']) && $newCartData['status'] != 'pending'))) {
                if (isset($newCartData['status']) && $newCartData['status'] != 'pending') {
                    $this->cart_id = '';
                }

                unset($data['cart_id']);
                $this->error_code = '';
                $this->error_messages = [];
                $this->setFromArray($data);

                // Before we continue, we need to have cart_id
                $this->saveCart();
                return;
            }

            // Never update company code
            unset($data['company_code']);

            // group shipping address payload by customer_address
            $groupShippingAddress = array();
            if (isset($data['items'])) {
                foreach($data['items'] as $rowItem) {
                    if (isset($rowItem['shipping_address']['customer_address'])) {
                        $groupShippingAddress[$rowItem['shipping_address']['customer_address']] = $rowItem['shipping_address'];
                    }
                }
            }

            // Update old data with new one, $data => from payload, $newCartData => from cart
            foreach($data as $keyData => $newValue) {
                /**
                 * if we are updating non-array ShoppingCart properties
                 * Remove gift_cards and gift_cards_amount change data for gift cards must be handle only when
                 * customer redeem/delete gift card
                 */
                if(!is_array($newValue) && $keyData != "gift_cards" && $keyData != "gift_cards_amount" && $keyData != "shipping_address" && $keyData != "billing_address") {
                    $newCartData[$keyData] = $newValue;
                } else if(!empty($newValue)) { // making sure we are not processing empty array
                    /**
                     * Processing items
                     */
                    if($keyData == 'items' ) {
                        $itemToUpdate = array(); // variable that hold cart data grouping
                        $keyFreeItem = array(); // free item list
                        if(!empty($newCartData['items'])) {
                            foreach($newCartData['items'] as $keyItem => $item) {
                                if($item['is_free_item'] == 1) { // skip free item
                                    $keyFreeItem[] = $keyItem;
                                    continue;
                                }

                                // group cart data from mongo
                                if(!empty($item['sku'])) {
                                    // update address to the newest address if exist
                                    if (isset($item['shipping_address']['customer_address']) && $item['delivery_method'] <> 'pickup') {
                                        if (isset($groupShippingAddress[$item['shipping_address']['customer_address']])) {
                                            $item['shipping_address'] = $groupShippingAddress[$item['shipping_address']['customer_address']];
                                        }
                                    }
                                    
                                    $itemToUpdate[$item['sku']] = $item;
                                }
                            }
                        }

                        /**
                         * Loop through items (from payload)
                         */
                        foreach($data['items'] as $keyItem => $item) {
                            if(isset($itemToUpdate[$item['sku']]) && !in_array($keyItem,$keyFreeItem)) {
                                /**
                                 * Updating all item
                                 * NOTE : All marketing promo using voucher keep intact in item
                                 */
                                foreach($item as $keyDataItem => $valItem) {
                                    if($keyDataItem == 'shipping_address') {  
                                        // $valItem => array type
                                        // complete addresss information
                                        $shippingAddress = new SalesOrderAddress();
                                        $valItem['address_type'] = "shipping";

                                        // make company_address_id same with customer_address_id if customer_type B2B
                                        $customerType = isset($newValue['customer_type']) ? $newValue['customer_type'] : 'B2C';
                                        if($customerType=='B2B') {
                                            $valItem['company_address_id'] = $valItem['customer_address'];
                                        } else if($customerType=='B2C') {
                                            $valItem['company_address_id'] = 0;
                                        }

                                        if (!isset($valItem['geolocation'])) {
                                            // check existing geolocation, if exist take it
                                            if (isset($item['sku'])) {
                                                foreach($newCartData['items'] as $keyItem => $itemDataDB) {
                                                    if ($item['sku'] == $itemDataDB['sku']) {
                                                        if (isset($itemDataDB['shipping_address']['geolocation'])) {
                                                            $valItem['geolocation'] = $itemDataDB['shipping_address']['geolocation'];
                                                        }
                                                        
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        $shippingAddress->setFromArray($valItem);
                                        $itemToUpdate[$item['sku']][$keyDataItem] = $shippingAddress;
                                    } else if ($keyDataItem == 'delivery_method') {
                                        $deliveryGroup = array('delivery','store_fulfillment','ownfleet');
                                        if (in_array($valItem, $deliveryGroup) && (!isset($item['shipping_address']) || empty($item['shipping_address']))) {
                                            // set default address to kembangan
                                            $defaultShippingLocation = [
                                                "address_type" => 'shipping', 
                                                "country_id" => getenv("DEFAULT_SHIPPING_COUNTRY_ID"),
                                                "province_id" => getenv("DEFAULT_SHIPPING_PROVINCE_ID"),
                                                "city_id" => getenv("DEFAULT_SHIPPING_CITY_ID"),
                                                "kecamatan_id" => getenv("DEFAULT_SHIPPING_KECAMATAN_ID"),
                                                "geolocation" => getenv("DEFAULT_GEOLOCATION")
                                            ];
                                            $shippingAddress = new SalesOrderAddress();
                                            $shippingAddress->setFromArray($defaultShippingLocation);
                                            $itemToUpdate[$item['sku']]['shipping_address'] = $shippingAddress->getDataArray();

                                            //\Helpers\LogHelper::log("bug_cart","/models/Cart.php Line 1056, data: " . json_encode($data),"debug");
                                        } else if (in_array($valItem, $deliveryGroup) && !isset($item['carrier_id'])) {
                                            // set carrier id if not exist
                                            $itemToUpdate[$item['sku']]['carrier_id'] = '';
                                        }

                                        $itemToUpdate[$item['sku']][$keyDataItem] = $valItem;
                                    } else {
                                        $itemToUpdate[$item['sku']][$keyDataItem] = $valItem;
                                    }     
                                }                     
                                
                                /**
                                 * Making sure that if we are updating to delivery_method 'delivery',
                                 * we remove 'pickup_code'
                                 */
                                if(isset($itemToUpdate[$item['sku']]['delivery_method'])
                                    && $itemToUpdate[$item['sku']]['delivery_method'] == 'delivery') {
                                    //$itemToUpdate[$item['sku']]['pickup_code'] = '';
                                }
                            } else if(!in_array($keyItem,$keyFreeItem)) {
                                /**
                                 * New item added to shopping cart
                                 */

                                // complete with default address if shipping_address not exist
                                foreach($item as $keyDataItem => $valItem) {
                                    if ($keyDataItem == 'delivery_method') {
                                        $deliveryGroup = array('delivery','store_fulfillment','ownfleet');
                                        if (in_array($valItem, $deliveryGroup) && !isset($item['shipping_address'])) {
                                            // set default address to kembangan
                                            $defaultShippingLocation = [
                                                "address_type" => 'shipping', 
                                                "country_id" => getenv("DEFAULT_SHIPPING_COUNTRY_ID"),
                                                "province_id" => getenv("DEFAULT_SHIPPING_PROVINCE_ID"),
                                                "city_id" => getenv("DEFAULT_SHIPPING_CITY_ID"),
                                                "kecamatan_id" => getenv("DEFAULT_SHIPPING_KECAMATAN_ID"),
                                                "geolocation" => getenv("DEFAULT_GEOLOCATION")
                                            ];
                                            $shippingAddress = new SalesOrderAddress();
                                            $shippingAddress->setFromArray($defaultShippingLocation);
                                            $itemToUpdate[$item['sku']]['shipping_address'] = $shippingAddress->getDataArray();

                                            //\Helpers\LogHelper::log("bug_cart","/models/Cart.php Line 1099, data: " . json_encode($data),"debug");
                                        } else {
                                            if (isset($item['shipping_address'])) {
                                                $itemToUpdate[$item['sku']]['shipping_address'] = $item['shipping_address'];
                                            } else {
                                                $itemToUpdate[$item['sku']]['shipping_address'] = array();
                                            }
                                        }
                                    }
                                    
                                    $itemToUpdate[$item['sku']][$keyDataItem] = $valItem;
                                }
                            }
                        }
                        
                        /**
                         * Reset all marketing rule applied
                         */
                        foreach ($itemToUpdate as $keyUpdateItem => $UpdateItem) {
                            $itemToUpdate[$keyUpdateItem]['cart_rules'] = "";
                            $itemToUpdate[$keyUpdateItem]['discount_amount'] = 0;
                            $itemToUpdate[$keyUpdateItem]['shipping_amount'] = 0;
                            $itemToUpdate[$keyUpdateItem]['shipping_discount_amount'] = 0;
                            $itemToUpdate[$keyUpdateItem]['handling_fee_adjust'] = 0;
                        }
                        
                        $newCartData['items'] = array(); // make sure to reset everything
                        $newCartData['items'] = $itemToUpdate;
                    }
                    
                    /**
                     * Updating 'customer' section of the ShoppingCart
                     */
                    if($keyData == 'customer') {
                        // If somehow FE change it's status to guest believe it
                        if(isset($newValue['customer_is_guest']) && $newValue['customer_is_guest'] == 1) {
                            $newCartData['customer'] = $newValue;
                        } else {
                            foreach($newValue as $keyNewValue => $newValueData) {
                                $newCartData['customer'][$keyNewValue] = $newValueData;
                            }
                        }
                    }                    

                    /**
                     * Updating 'billing_address'
                     */
                    if($keyData == 'billing_address') {
                        foreach($newValue as $keyNewValue => $newValueData) {
                            $newCartData['billing_address'][$keyNewValue] = $newValueData;
                            if($keyNewValue=='customer_address')
                                $newCartData['billing_address']['company_address_id'] = $newValueData;
                        }
                    }

                    /**
                     * Updating 'payment'
                     */
                    if($keyData == 'payment') {
                        foreach($newValue as $keyNewValue => $newValueData) {
                            $newCartData['payment'][$keyNewValue] = $newValueData;
                        }
                    }
                    /**
                     * @todo : Uncomment if we already have payer table
                     */
                    /*
                    if(!empty($key == 'payer')) {
                        foreach($newValue as $key => $newValueData) {
                            $newCartData['items']['payer'][$key] = $newValueData;
                        }
                    }
                    */
                }
            }

            /**
             * Free item will be add later when we checking for marketing module, make sure our data no contain free item
             */
            if(!empty($newCartData['items'])) {
                foreach ($newCartData['items'] as $keyItem => $item) {
                    if(isset($item['is_free_item']) && $item['is_free_item'] == 1) {
                        unset($newCartData['items'][$keyItem]);
                    } else if(!empty($item['cart_rules'])) {
                        $newCartData['items'][$keyItem]['cart_rules'] = '[]';
                    }
                }
            }

            $newCartData = $this->resetMarketingData($newCartData);
            $this->setFromArray($newCartData);
        } else {
            /**
             * no cart_id supplied, create new
             */
            $this->setFromArray($data);
        }

        $this->updated_at = date("Y-m-d H:i:s");
    }    

    public function defineGroupShipment()
    {
        $cartData = $this->getDataArray();
        if (count($cartData['items']) > 0) {
            // define group_shipment for every sku then call the item collection to assign it
            $cartLib = new \Library\Cart();
            $cartCustom = array();
            foreach ($cartData['items'] as $row){
                $sender = $cartLib->getSender($row);
                $storeCode = $row['store_code'];
                $courierPickup = ($row['delivery_method'] == 'pickup') ? $row['store_code'] : $row['carrier_id'];
                $geolocation = (isset($row['shipping_address']['geolocation']) && !empty($row['shipping_address']['geolocation'])) ? $row['shipping_address']['geolocation'] : 'geolocation_empty';
                $cartCustom[$sender][$storeCode][$row['delivery_method']][$courierPickup][$geolocation][] = $row; 
            }

            // convert the group to ordinary array
            $tempItem = array();
            foreach($cartCustom as $keySender => $valueSender) {
                foreach($valueSender as $keyStoreCode => $valueStoreCode) {
                    foreach($valueStoreCode as $keyDeliveryMethod => $valueDeliveryMethod) {
                        foreach($valueDeliveryMethod as $keyCarrierID => $valueCarrierID) {
                            foreach($valueCarrierID as $keyGeoLocation => $valueGeoLocation) {
                                $tempItem[] = $valueGeoLocation; 
                            }   
                        }          
                    }
                }
            }

            $counterGroupShipment = 1;
            $groupShipmentPerSKU = array();
            foreach($tempItem as $rowShipment) {
                foreach($rowShipment as $rowItem) {
                    $groupShipmentPerSKU[$rowItem['sku']] = $counterGroupShipment;
                }

                $counterGroupShipment++;
            }

            $this->items->defineGroupShipment($groupShipmentPerSKU);
        }
    }

    /**
     * Get shopping cart own by a customer
     * @param int $customer_id
     * @param string $company_code
     * @return array|string
     */
    public function getCustomerCartData($customer_id = 0, $company_code = 'ODI')
    {
        if(empty($customer_id)) {
            return "";
        }

        // @todo add param reserved order_no = ""
        $search_param = ['customer.customer_id' => $customer_id, 'company_code' => $company_code, 'status' => 'pending', 'reference_order_no' => null];
        //$search_param = ['customer.customer_id' => $customer_id, 'company_code' => $company_code, 'status' => 'pending'];
        // $shoppingCartResult = $this->collection->find($search_param,['sort' => ['$natural'=>-1]]);
        $shoppingCartResult = $this->collection->find($search_param,['sort' => ['created_at' => -1]]); // testing

        $shoppingCartArray = [];
        foreach ($shoppingCartResult as $result) {
            $shoppingCartArray[] = $result;
        }

        return $shoppingCartArray;
    }

    public function mergeShoppingCartItem($shopping_cart_array = [],$current_cart_id = "", $company_code = 'ODI')
    {
        $itemList = [];
        $customerInfo = [];
        foreach ($shopping_cart_array as $cart) {
            foreach ($cart['items'] as $item) {
                if(!isset($itemList[$item['sku']])) {
                    $itemList[$item['sku']] = $item;
                }
            }

            if($cart['cart_id'] != $current_cart_id) {
                $cart['status'] = "expired";
                $this->cart_id = $cart['cart_id'];
                $this->saveToDb($cart);
            }

            $customerInfo = $cart['customer'];
        }

        $this->cart_id = $current_cart_id;
        $cartData = $this->loadCartData();

        foreach ($cartData['items'] as $item) {
            if(!isset($itemList[$item['sku']])) {
                $itemList[$item['sku']] = $item;
            }
        }

        $itemList = array_values($itemList);
        $cartData['items'] = $itemList;
        $cartData['customer'] = $customerInfo;
        $cartData['company_code'] = $company_code;
        $this->cart_id = $cartData['cart_id'];

        $cartData['do_count_shipping'] = true;

        $cart = new \Library\Cart();
        $cart->setFromParam($cartData);

        if(empty($this->params['source_order'])){
            \Helpers\LogHelper::log('debug_625', '/app/models/Cart.php line 1392', 'debug');
            $cart->checkMarketingPromotion();
            \Helpers\LogHelper::log('debug_625', '/app/models/Cart.php line 1394', 'debug');
            $cart->checkGiftcartApplied();
        }

        $cart->saveCart();
    }

    /**
     * Load cart data by cart_id
     * @param bool $get_raw_data
     * @return mixed
     */
    public function loadCartData($get_raw_data = false)
    {

        if(!empty($this->cart_id)) {
            $param = [];

            if (!empty($this->reserved_order_no)) {
                $param = ['reserved_order_no' => $this->reserved_order_no];
            } else if (!empty($this->cart_id)) {
                // $id = new MongoDbObjectId($this->cart_id);
                $param = ['cart_id' => $this->cart_id];
            }

            // $result = [];
            // if (!empty($param)) {
            //     $result = $this->collection->findOne($param);
            // }
            $result = $this->getCartDetail();
            if(count($result) > 0) {
                // unset($result['_id']);
                // if(!$get_raw_data) {
                //     /**
                //      * Reset previous marketing rule evaluation of the ShoppingCart
                //      * No need to unset gift card, since it's handled separately in 'redeem'
                //      */
                //     unset($result['subtotal']);
                //     unset($result['discount_amount']);
                //     unset($result['shipping_amount']);
                //     unset($result['shipping_discount_amount']);
                //     unset($result['split_cost']); // TODO: split_cost?
                //     unset($result['handling_fee_adjust']);
                //     unset($result['grand_total']);
                //     unset($result['total_qty_item']);
                // }

                return $result;
            } else {
                $this->error_code = "RR202";
                $this->error_messages['type'] = "global";
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = "shopping cart not found";
                return $result;
            }
        } else {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "no shopping cart id passed for retrieve";
            return array();
        }
    }

    public function getCartDetail($check_for_status = true)
    {
        $cartData = array();

        if(empty($this->cart_id)) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty or not found";
            return $cartData;
        }

        // Load shopping card data
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setHeaders(['No-Refresh' => "1"]);
        $apiWrapper->setEndPoint("cart/".$this->cart_id);
        
        if(strlen($this->cart_id) < 35 && (substr($this->cart_id,0,4) == "ODIS") || substr($this->cart_id,0,4) == "ODIT" || substr($this->cart_id,0,4) == "ODIK"){
            $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_API'));            
            $apiWrapper->setEndPoint("cart/".$this->cart_id);            
        }        

        // if (!empty($this->reserved_order_no)) {
        //     $apiWrapper->setHeaders(['rr-reserved-order-no' => $this->reserved_order_no]);
        // }

        if($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
        } else {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty or not found";
            return $cartData;
        }

        // maybe cart is found, but data is empty
        $cartData = $apiWrapper->getData();
        if(empty($cartData)) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty";
            return $cartData;
        }

        if($cartData['status'] != 'pending' && $check_for_status) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart already processing";
            return $cartData;
        }

        if (isset($cartData['errors']['code'])) {
            if ($cartData['errors']['code'] != 0) {
                $this->error_code = $cartData['errors']['code'];
                $this->error_messages['type'] = $cartData['errors']['data']['type'];
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = $cartData['errors']['message'];
                return $cartData;
            }
        }

        // If have no item then redirect to home page
        if(empty($cartData['items'])) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty";
            return $cartData;
        }

        return $cartData;
    }

    public function getCartDetailV2($headers)
    {
        $cartData = array();

        if(empty($this->cart_id)) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty or not found";
            return $cartData;
        }

        // Load shopping card data
        $apiWrapper = new APIWrapper(getenv('CART_API'));
        $apiWrapper->setHeaders($headers);
        $apiWrapper->setEndPoint("v2/cart/".$this->cart_id);

        if($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
        } else {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty or not found";
            return $cartData;
        }

        // maybe cart is found, but data is empty
        $cartData = $apiWrapper->getData();
        if(empty($cartData)) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty";
            return $cartData;
        }

        if (isset($cartData['errors']['code'])) {
            if ($cartData['errors']['code'] != 0) {
                $this->error_code = $cartData['errors']['code'];
                $this->error_messages['type'] = $cartData['errors']['data']['type'];
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = $cartData['errors']['message'];
                return $cartData;
            }
        }

        return $cartData;
    }

    public function saveCartVendor($status = ""){
        if(empty($this->cart_id)) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty or not found";
            return false;
        }

        $apiWrapper = new APIWrapper(getenv('ORDER_VENDOR_API'));            
        $apiWrapper->setEndPoint("cart/status/".$this->cart_id."?status=".$status);
        
        if($apiWrapper->send("put")) {
            $apiWrapper->formatResponse();
        } else {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart is empty or not found";
            return false;
        }
    }

    /**
     * Save shopping cart, this function will populate cart data and save it
     */
    public function saveCart()
    {
        if(empty($this->cart_id)) {
            // initialize new shopping cart
            $this->initialize();
        }
        
        $cartData = $this->getDataArray();
        $cartData['updated_at'] = date("Y-m-d H:i:s");
        
        if(empty($cartData['token']) || (isset($cartData['token']) && $cartData['token'] == 'null')) {
            $cartData['token'] = SimpleEncrypt::encrypt($this->cart_id.strtolower(PhalconText::random(PhalconText::RANDOM_ALNUM , 3)));
        }
        
        $this->saveToDb($cartData);
    }

    /**
     * Save carta data to database (mongo db)
     * @param array $cartData
     */
    public function saveToDb($cartData = [])
    {
        if(empty($this->cart_id)) {
            /** @var InsertOneResult $insertOneResult */
            $insertOneResult = $this->collection->insertOne([]);

            if($insertOneResult->getInsertedCount() > 0)
            {
                $lastInsertId = (string)$insertOneResult->getInsertedId();

                $this->created_at = date("Y-m-d H:i:s");
                $this->cart_id = $lastInsertId;

                // Generate tokens
                $token = $lastInsertId.strtolower(PhalconText::random(PhalconText::RANDOM_ALNUM , 3));
                $this->token = SimpleEncrypt::encrypt($token);
            } else {
                $this->error_code = "501";
                $this->error_messages['type'] = "global";
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = "Failed to initiate shopping cart";
                return false;
            }
        }
        // $id = new MongoDbObjectId($this->cart_id);
        $this->collection->findOneAndReplace(['cart_id' => $this->cart_id], $cartData);
    }

    public function checkMarketingPromotion($promotion_type = [])
    {        
        // If we have no item no need to apply any promotion
        if(empty($this->items)) {
            return;
        }

        $marketingLib = new MarketingLib();
        if(empty($promotion_type)) {
            $promotionType[] = 'shopping_cart';
            if(!empty($this->shipping_amount)) {
                $promotionType[] = 'shipping';
            }

            if(!empty($this->payment)) {
                $promotionType[] = 'payment';
            }
        } else {
            $promotionType = $promotion_type;
        }
        
        $marketingPromo = $marketingLib->getMarketingPromotion($promotionType);   
        if(!empty($marketingPromo)) {
            foreach ($promotionType as $key => $type) {
                if(empty($marketingPromo[$type])){
                    continue;
                }

                $promoList = json_decode($marketingPromo[$type],true);
                foreach ($promoList as $rule) {

                    $isActiveTime = \Helpers\GeneralHelper::checkActiveBetweenDate($rule['from_date'],$rule['to_date']);
                    if (!$isActiveTime) {
                        continue;
                    }
                    
                    $cartData = $this->getDataArray();
                    $canContinue = $marketingLib->prerequisiteConditionCheck($cartData,$rule);
                    if(!$canContinue) {
                        continue;
                    }
                                        
                    /**
                     * If using voucher, check if voucher already redeem or not
                     */
                    $voucherCode = "";
                    $applyUsingVoucher = false;
                    if(isset($rule['use_voucher']) && $rule['use_voucher'] == 1) {
                        $giftCardInfo = json_decode($this->gift_cards,true);
                        $foundInGiftcard = false;
                        foreach ($giftCardInfo as $gcId => $gcVal) {
                            if($gcVal['rule_id'] == $rule['rule_id'] && empty($gcVal['voucher_amount_used'])) {
                                unset($giftCardInfo[$gcId]);

                                $voucherCode = $gcVal['voucher_code'];
                                $foundInGiftcard = true;

                                if($rule['action']['applied_action'] == 'use_voucher') {
                                    $applyUsingVoucher = true;
                                }
                            }
                        }

                        if($foundInGiftcard) {
                            $this->gift_cards = json_encode($giftCardInfo);
                            $this->saveCart();
                        } else {
                            continue;
                        }
                    }

                    $conditions = $rule['conditions'];
                    \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 1680', 'debug');
                    $isMatchRule = $this->marketingConditionCheck($conditions,$cartData);
                    if($isMatchRule) {
                        $rule = $marketingLib->incrementPromotionUssage($rule,$this->cart_rules, $this->gift_cards);
                        if($applyUsingVoucher) {
                            $this->redeemVoucher($voucherCode, true,false,true);
                        } else {
                            //$rule['action']['max_redemption'] = 0;
                            if(isset($rule['action']['max_redemption'])){
                                $rule['action']['max_redemption'] = (int)$rule['action']['max_redemption'];
                                if (empty($rule['action']['max_redemption'])) {
                                    $rule['action']['max_redemption'] = 0;
                                }
                            }

                            $this->applyMarketingAction($rule);
                        }

                        if($rule['stop_rules_processing'] == true) {
                            break;
                        }
                    }
                }
            }

            $cartData = $this->getDataArray();

            $this->saveToDb($cartData);
        }
    }

    /**
     * Reset previous marketing rule evaluation of the ShoppingCart
     * No need to reset grand_total or other data that always called in process
     * @param array $cartData
     * @return array $cartDta
     */
    public function resetMarketingData($cartData = [])
    {
        if(empty($cartData)) {
            return;
        }

        unset($cartData['discount_amount']);
        unset($cartData['shipping_discount_amount']);
        unset($cartData['handling_fee_adjust']);
        unset($cartData['split_cost']);
        unset($cartData['cart_rules']);

        return $cartData;
    }

    public function countShippingAndHandlingFee($virtualInvoice = [])
    {
        if(empty($this->items)) {
            return;
        }

        // we need to define the carrier_id first
        $this->items->defineCourierID();
        
        // Generate virtual invoice
        if(empty($virtualInvoice)) {
            $virtualInvoice = $this->items->generateVirtualInvoice();
        }
        
        // Reset Everything, we recalculate latter
        if(!empty($virtualInvoice['invoice'])) {
            $this->handling_fee_adjust = 0;
            $this->shipping_amount = 0;
            $this->shipping_discount_amount= 0;
        }
        
        if(empty($virtualInvoice['origin_shipping_region'])) { 
            /* 
            * assume all items is picked up by customer
            * count handling fee
            */
            foreach($virtualInvoice['invoice'] as $supplierAlias => $invoiceByStoreCode) {
                foreach($invoiceByStoreCode as $storeCode => $invoiceByDeliveryMethod) {
                    foreach($invoiceByDeliveryMethod as $deliveryMethod => $invoiceByCourierID) {
                        foreach($invoiceByCourierID as $courierID => $invoiceByGeolocation) {
                            foreach($invoiceByGeolocation as $geoLocation => $invoiceItems) {
                                $nonDeliveryItems = $invoiceItems;
                                unset($nonDeliveryItems['origin_shipping_region']);
                                $handlingFeeAdjust = $this->_countHandlingFee($nonDeliveryItems);
                                $this->handling_fee_adjust += $handlingFeeAdjust;
                                $this->shipping_amount += $this->_countShippingAmount($invoiceItems);
                            }
                        }
                    }
                }
            }

            return;
        } else {
            // Get shipping cost 
            $itemsArray = $this->items->getDataArray();
            foreach($itemsArray as $rowItem) {
                if ($rowItem['delivery_method'] <> 'pickup') {
                    // set to default kecamatan code if empty
                    $rowItem['shipping_address']['kecamatan']['kecamatan_code'] = (empty($rowItem['shipping_address']['kecamatan']['kecamatan_code'])) ? getenv('DEFAULT_SHIPPING_KECAMATAN_CODE') : $rowItem['shipping_address']['kecamatan']['kecamatan_code'];
                } else {
                    continue;
                }

                if (isset($rowItem['shipping_address']['kecamatan']['kecamatan_code']) && 
                !empty($rowItem['shipping_address']['kecamatan']['kecamatan_code']) && !CartHelper::isGosend($rowItem['carrier_id'])) {
                    // $virtualInvoice['origin_shipping_district'] is list of origins kecamatan
                    $originDistrict = "'".implode("','", $virtualInvoice['origin_shipping_district'])."'";
                    $shippingDestination = $rowItem['shipping_address']['kecamatan']['kecamatan_code'];

                    // By Default we need to find shipping cost from jakarta
                    if(strpos($originDistrict, getenv('DEFAULT_ORIGIN_SHIPPING_DISTRICT')) === false) {
                        $originDistrict .= ",'".getenv('DEFAULT_ORIGIN_SHIPPING_DISTRICT')."'";
                    }
                } else if (CartHelper::isGosend($rowItem['carrier_id'])) {
                    $shippingDestination = $rowItem['shipping_address']['kecamatan']['kecamatan_code'];
                }
            }
        }

        $totalShippingAmount = $totalHandlingFee = 0;
        $splitCost = json_decode($this->split_cost,true);
        foreach($virtualInvoice['invoice'] as $supplierAlias => $invoiceByStoreCode) {
            foreach($invoiceByStoreCode as $storeCode => $invoiceByDeliveryMethod) {
                foreach($invoiceByDeliveryMethod as $deliveryMethod => $invoiceByCourierID) {
                    foreach($invoiceByCourierID as $courierID => $invoiceByGeolocation) {
                        foreach($invoiceByGeolocation as $geoLocation => $invoiceItems) {
                            $shippingAmount = $handlingFeeAdjust = $shipping_discount_amount = 0; // Reset for each invoice

                            $originShippingDistrict = empty($invoiceItems['origin_shipping_district']) ? getenv('DEFAULT_ORIGIN_SHIPPING_DISTRICT') : $invoiceItems['origin_shipping_district'];
                            unset($invoiceItems['origin_shipping_region']);
                            unset($invoiceItems['origin_shipping_district']);
                            if(substr($supplierAlias,0,2) != 'MP') {
                                if ($storeCode <> 'DC' && $deliveryMethod == 'delivery') {
                                    $deliveryMethod = 'store_fulfillment';
                                }

                                // logic for RR and sisco
                                if($deliveryMethod == 'delivery') {
                                    $shippingDestination = '';
                                    foreach($invoiceItems as $key => $rowItem) {
                                        $rowParams['carrier_id'] = $rowItem['carrier_id'];
                                        $rowParams['sku'] = $rowItem['sku'];
                                        $this->items->assignShippingAmountToItem($rowParams); // assign only carrier_id
                                        $this->defineGroupShipment();
                                        $invoiceItems[$key]['group_shipment'] = $this->items->getGroupShipmentBySKU($rowItem['sku']);
                                        
                                        if (empty($shippingDestination)) {
                                            if (isset($rowItem['shipping_address']['kecamatan']['kecamatan_id'])) {
                                                $kecamatanCode = $this->getKecamatanCode($rowItem['shipping_address']['kecamatan']['kecamatan_id']);
                                                if (empty($kecamatanCode)) {
                                                    //$this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan","count_shipping", "Kecamatan code empty (1)","shipment", "{$rowItem['group_shipment']}");
                                                    return;
                                                }
                                            } else {
                                                //$this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan","count_shipping", "Kecamatan code empty (2)","shipment", "{$rowItem['group_shipment']}");
                                                return;
                                            }

                                            $shippingDestination = $kecamatanCode;
                                        }
                                    }

                                    $shippingAmount = $this->_countDeliveryCost($invoiceItems);
                                } else if($deliveryMethod == 'store_fulfillment') {
                                    // pisahkan invoice item gosend dan bukan gosend
                                    $invoiceItemNonGosend = array();
                                    $invoiceItemGosend = array();
                                    $shippingDestination = $groupShipment  = '';
                                    foreach($invoiceItems as $rowItem) {
                                        $rowParams['carrier_id'] = $rowItem['carrier_id']; // assign carrier id
                                        $rowParams['sku'] = $rowItem['sku'];
                                        $this->items->assignShippingAmountToItem($rowParams);
                                        $this->defineGroupShipment();
                                        $rowItem['group_shipment'] = $this->items->getGroupShipmentBySKU($rowItem['sku']);
                                        if (CartHelper::isGosend($rowItem['carrier_id'])) {
                                            $invoiceItemGosend[] = $rowItem;
                                        } else {
                                            $groupShipment = $rowItem['group_shipment'];

                                            if (empty($shippingDestination)) {
                                                if (isset($rowItem['shipping_address']['kecamatan']['kecamatan_id'])) {
                                                    $kecamatanCode = $this->getKecamatanCode($rowItem['shipping_address']['kecamatan']['kecamatan_id']);
                                                    if (empty($kecamatanCode)) {
                                                        //$this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan","count_shipping", "Kecamatan code empty (3)","shipment", "{$rowItem['group_shipment']}");
                                                        return;
                                                    }
                                                } else {
                                                    //$this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan1","count_shipping", "Kecamatan code empty (4)","shipment", "{$rowItem['group_shipment']}");
                                                    return;
                                                }

                                                $shippingDestination = $kecamatanCode;
                                            }

                                            $invoiceItemNonGosend[] = $rowItem;
                                        }
                                    }

                                    if (count($invoiceItemGosend) > 0) { 
                                        $shippingAmount = $this->_countGosendCost($invoiceItemGosend);
                                        if (is_null($shippingAmount)) {
                                            return;
                                        }

                                        $shipping_discount_amount = $this->items->countShippingDiscountAmount();
                                        $this->shipping_discount_amount = intval(ceil($shipping_discount_amount));

                                        $handlingFeeAdjust = $this->_countHandlingFee($invoiceItemGosend);
                                    }

                                    if (count($invoiceItemNonGosend) > 0) {
                                        if (empty($shippingDestination)) {
                                            $this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan","count_shipping", "Kecamatan code empty (5)", "shipment","$groupShipment");
                                            return;
                                        }

                                        $shippingAmount += $this->_countDeliveryCost($invoiceItemNonGosend);
                                        $handlingFeeAdjust += $this->_countHandlingFee($invoiceItemNonGosend);
                                    }   
                                } else if($deliveryMethod == 'pickup') {
                                    $handlingFeeAdjust = $this->_countHandlingFee($invoiceItems);
                                    
                                    // reset array pointer to first value (just to be save)
                                    reset($invoiceItems);
                                }
                            } else {
                                // Marketplace
                                
                                /**
                                 * @todo  ada pengecekan kalau supplier ini support 3pl atau tidak
                                 */
                                // This is marketplace product
                                if($deliveryMethod == 'delivery') {
                                    $shippingDestination = '';
                                    foreach($invoiceItems as $key => $rowItem) {
                                        $rowParams['carrier_id'] = $rowItem['carrier_id'];
                                        $rowParams['sku'] = $rowItem['sku'];
                                        $this->items->assignShippingAmountToItem($rowParams); // assign only carrier_id
                                        $this->defineGroupShipment();
                                        $invoiceItems[$key]['group_shipment'] = $this->items->getGroupShipmentBySKU($rowItem['sku']);

                                        if (empty($shippingDestination)) {
                                            if (isset($rowItem['shipping_address']['kecamatan']['kecamatan_id'])) {
                                                $kecamatanCode = $this->getKecamatanCode($rowItem['shipping_address']['kecamatan']['kecamatan_id']);
                                                if (empty($kecamatanCode)) {
                                                    $this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan","count_shipping", "Kecamatan code empty (5)","shipment", "{$rowItem['group_shipment']}");
                                                    return;
                                                }
                                            } else {
                                                $this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan","count_shipping", "Kecamatan code empty (6)","shipment", "{$rowItem['group_shipment']}");
                                                return;
                                            }

                                            $shippingDestination = $kecamatanCode;
                                        }
                                    }
                                    
                                    $shippingAmount = $this->_countDeliveryCostMarketplace($invoiceItems,$supplierAlias,$originShippingDistrict, $shippingDestination, $deliveryMethod);
                                } else if($deliveryMethod == 'ownfleet') {
                                    /**
                                     * @todo:
                                     * @Roesmien: count by ownfleet rate
                                     */
                                    foreach ($invoiceItems as $item) {
                                        $this->shipping_amount += $item['shipping_amount'];
                                    }

                                    $shippingAmount = $this->shipping_amount;
                                    // Distribute shipping_amount to item
                                    if($shippingAmount > 0) {
                                        $this->items->distributeShippingAmount($shippingAmount, $invoiceItems);
                                    }
                                }
                            }

                            // no shipping amount for reorder
                            if(!empty($this->reference_order_no) AND !empty($this->gift_cards)) {
                                $shipping_discount_amount = $shippingAmount;
                                $this->shipping_discount_amount = $shippingAmount;
                            }

                            // @todo : do we still need this thing? yes, I need the information
                            $flagFound = 0;
                            foreach($splitCost as $rowSplitCost) {
                                if ($rowSplitCost['store_code'] == $storeCode && $rowSplitCost['delivery_method'] == $deliveryMethod
                                && $rowSplitCost['shipping_amount'] == $shippingAmount) {
                                    $flagFound = 1;
                                }
                            }
                            
                            if (!$flagFound) {
                                $splitCost[] = array(
                                    "store_code" => $storeCode,
                                    "delivery_method" => $deliveryMethod,
                                    "shipping_amount" => $shippingAmount,
                                    "shipping_discount_amount" => $this->shipping_discount_amount
                                );
                            }                    

                            $totalShippingAmount += $shippingAmount;
                            $totalHandlingFee += $handlingFeeAdjust;
                        }
                    }
                }
            }
        }

        if($this->shipping_discount_amount > $this->shipping_amount) {
            $this->shipping_discount_amount = $this->shipping_amount;
        }        
        
        $this->shipping_amount = $totalShippingAmount;
        $this->handling_fee_adjust = $totalHandlingFee;
        $this->split_cost = json_encode($splitCost);
        
        if(!empty($this->reference_order_no)){
            $this->shipping_amount = 0;
            $this->shipping_discount_amount = 0;
            $this->items->setShippingReorder();
        }

        return $totalShippingAmount;
    }

    public function getKecamatanCode($kecamatanID = NULL)
    {
        $kecamatanCode = '';
        if ($kecamatanID) {
            $kecamatanModel = new \Models\MasterKecamatan();
            $result = $kecamatanModel->findFirst('kecamatan_id = ' . $kecamatanID);
            $dataKecamatan = $result->toArray();
            if (isset($dataKecamatan['kecamatan_code'])) {
                $kecamatanCode = $dataKecamatan['kecamatan_code'];
            }
        }

        return $kecamatanCode;
    }

    public function getDataProvince($provinceID = NULL)
    {
        $dataProvince = array();
        if ($provinceID) {
            $provinceModel = new \Models\MasterProvince();
            $result = $provinceModel->findFirst('province_id = ' . $provinceID);
            $dataProvince = $result->toArray();
        }

        return $dataProvince;
    }

    public function getDataCity($cityID = NULL)
    {
        $dataCity = array();
        if ($cityID) {
            $cityModel = new \Models\MasterCity();
            $result = $cityModel->findFirst('city_id = ' . $cityID);
            $dataCity = $result->toArray();
        }

        return $dataCity;
    }

    public function getDataKecamatan($kecamatanID = NULL)
    {
        $dataKecamatan = array();
        if ($kecamatanID) {
            $kecamatanModel = new \Models\MasterKecamatan();
            $result = $kecamatanModel->findFirst('kecamatan_id = ' . $kecamatanID);
            $dataKecamatan = $result->toArray();
        }

        return $dataKecamatan;
    }

    private function _countDeliveryCost($items = array())
    {
        $shippingAmount = $this->_applyShippingRateToItems($items);

        $shipping_discount_amount = $this->items->countShippingDiscountAmount();
        $this->shipping_amount += $shippingAmount;
        $this->shipping_discount_amount = intval(ceil($shipping_discount_amount));

        return $shippingAmount;
    }

    private function _countDeliveryCostMarketplace($items = array(),$supplier_alias = "",  $origin_shipping_district = "", $shipping_destination = "", $deliveryMethod = "")
    {
        $shippingRate = $this->_getShippingRateAndCarrier($supplier_alias, $origin_shipping_district, $shipping_destination, $deliveryMethod);
        $shippingAmount = $this->_applyShippingRateToItemsFromParams($items, $shippingRate['shipping_rate'], $shippingRate['carrier_id']);

        $shipping_discount_amount = $this->items->countShippingDiscountAmount();
        $this->shipping_amount += $shippingAmount;
        $this->shipping_discount_amount = intval(ceil($shipping_discount_amount));

        return $shippingAmount;
    }

    private function _countGosendCost($invoiceItem = array(), $flagValidateCart = false)
    {
        $cartLib = new \Library\Cart(); 
        $groupShipment = array();        
        $shippingAmount = 0;
        foreach ($invoiceItem as $rowItem) {
            $groupShipment[$rowItem['shipping_address']['geolocation']][$rowItem['carrier_id']][] = $rowItem;
        }

        // Validation weight and volume
        foreach ($groupShipment as $keyGeoloation => $valArrayByGeolocation) {
            foreach ($valArrayByGeolocation as $keyCarrier => $valArrayByCarrier) {
                $carrierID = $keyCarrier;
                $carrierName = "GO-SEND Courier";
                if ($carrierID == getenv('GOSEND_INSTANT_CARRIER_ID')) {
                    $carrierName = "GO-SEND Instant Courier";
                } elseif ($carrierID == getenv('GOSEND_SAMEDAY_CARRIER_ID')) {
                    $carrierName = "GO-SEND Same Day Service";
                }

                $totalWeight = $totalVolumetric = $qtyOrder = 0;
                foreach ($valArrayByCarrier as $rowItem) {
                    $totalWeight += $rowItem['weight'] * $rowItem['qty_ordered'];
                    $qtyOrder += $rowItem['qty_ordered'];
                    if (strtolower($rowItem['packaging_uom']) == 'cm') { 
                        $totalVolumetric += ($rowItem['packaging_height'] * $rowItem['packaging_width'] * $rowItem['packaging_length']) * $rowItem['qty_ordered'];
                    } elseif (strtolower($rowItem['packaging_uom']) == 'mm') {
                        $totalVolumetric += (($rowItem['packaging_height'] / 10) * ($rowItem['packaging_width'] / 10) * ($rowItem['packaging_length'] / 10)) * $rowItem['qty_ordered'];
                    } elseif (strtolower($rowItem['packaging_uom']) == 'm') {
                        $totalVolumetric += (($rowItem['packaging_height'] * 100) * ($rowItem['packaging_width'] * 100) * ($rowItem['packaging_length'] * 100)) * $rowItem['qty_ordered'];
                    }
                }

                // By weight   
                $totalWeightMax = CartHelper::maxWeightGosend($carrierID, $qtyOrder);             
                if ($totalWeight > $totalWeightMax) { 
                    $this->setErrorAndLog("RR200","Berat barang tidak disupport $carrierName","count_shipping", "[gosend]Berat barang tidak disupport Gosend","shipment","{$valArrayByCarrier[0]['group_shipment']}");
                    return; 
                }

                // By volumetric
                $totalVolumeMax = CartHelper::maxVolumeGosend($carrierID, $qtyOrder);    
                if ($totalVolumetric > $totalVolumeMax) { 
                    $this->setErrorAndLog("RR200","Volume barang tidak disupport $carrierName","count_shipping", "[gosend]Volume barang tidak disupport Gosend","shipment","{$valArrayByCarrier[0]['group_shipment']}");
                    return; 
                }

                // get geolocation by pickup_code and check the is_express_courier 
                $dataPickupPoint = $cartLib->getPickupData($valArrayByCarrier[0]['pickup_code']);
                if (count($dataPickupPoint) == 0) { 
                    $this->setErrorAndLog("RR200","Data tidak ditemukan","count_shipping", "[gosend]Data pickup point tidak ditemukan","shipment","{$valArrayByCarrier[0]['group_shipment']}");
                    return;
                } 
    
                if ($dataPickupPoint['is_express_courier'] <> 1 || empty($dataPickupPoint['geolocation'])) { 
                    $this->setErrorAndLog("RR200","Lokasi tidak disupport $carrierName","count_shipping", "[gosend]Lokasi pickup point tidak disupport Gosend","shipment","{$valArrayByCarrier[0]['group_shipment']}");
                    return;
                }
                    
                // call shipment gosend to get shipping amount
                $paramGosend['origin'] = $dataPickupPoint['geolocation'];
                $paramGosend['destination'] = $valArrayByCarrier[0]['shipping_address']['geolocation'];
                $paramGosend['payment_type'] = "3";
                $groupShipmentID = $valArrayByCarrier[0]['group_shipment'];
                $shippingAmountPerCarrier = $this->_getShippingCostGosend($paramGosend, $carrierID, $carrierName, $groupShipmentID);
                if ($shippingAmountPerCarrier) {
                    $shippingAmount += $shippingAmountPerCarrier; 

                    // Remove Item Bogo
                    $indexCheckBogo = 0;
                    foreach ($valArrayByCarrier as $rowItem) { 
                        if($rowItem['is_free_item'] == 1){
                            $rowParams['shipping_amount'] = 0;
                            $rowParams['carrier_id'] = $carrierID;
                            $rowParams['sku'] = $rowItem['sku'];
                            $this->items->assignShippingAmountToItem($rowParams, $rowItem['is_free_item']);
                            unset($valArrayByCarrier[$indexCheckBogo]);
                        }
                        $indexCheckBogo++;
                    }

                    // spread shipping amount
                    foreach ($valArrayByCarrier as $rowItem) { 
                        $rowParams['shipping_amount'] = $shippingAmountPerCarrier / count($valArrayByCarrier);
                        $rowParams['carrier_id'] = $carrierID;
                        $rowParams['sku'] = $rowItem['sku'];
                        $this->items->assignShippingAmountToItem($rowParams);
                    }
                } else {
                    return;
                }                    
            }
        }
        
        $this->items->countShippingAmount();

        if (!$flagValidateCart) {
            $this->shipping_amount += $shippingAmount; 
        }
 
        return $shippingAmount; 
    }

    private function _getShippingCostGosend($params = array(), $carrierID = "", $carrierName = "", $groupShipmentID = "")
    {
        $apiWrapper = new APIWrapper(getenv('SHIPMENT_API'));
        $apiWrapper->setEndPoint("gosend/validate");
        $apiWrapper->setParam($params);
        $shippingAmount = 0;
        if($apiWrapper->send("put")) {
            $apiWrapper->formatResponse();
            $resultData = $apiWrapper->getData();
            if (empty($resultData)) {
                $this->setErrorAndLog("RR200","Cannot connect to gosend","count_shipping", "[gosend]Cannot connect to gosend","shipment","$groupShipmentID");
                return;    
            } else {
                $gosendService = ($carrierID == getenv('GOSEND_INSTANT_CARRIER_ID')) ? 'Instant' : 'Same Day';
                foreach($resultData as $rowService) {
                    if ($rowService['DeliveryMethod'] == $gosendService) {
                        $shippingAmount = $rowService['Details']['price']['total_price'];
                    }
                }
            }
        } else {
            $this->setErrorAndLog("RR200","Pengiriman dengan $carrierName ke alamat Anda tidak tersedia","count_shipping", "[gosend]Cannot connect to gosend","shipment","$groupShipmentID");
            return;
        }
        
        if ($shippingAmount < 1) {
            $this->setErrorAndLog("RR200","Pengiriman dengan $carrierName ke alamat Anda tidak tersedia","count_shipping", "[gosend]Shipping amount tidak ditemukan","shipment","$groupShipmentID");
            return;
        }

        return $shippingAmount;
    }

    /**
     * @param $items
     * @return int
     */
    private function _applyShippingRateToItems ($items) {

        $totalWeight = CartHelper::countWeight($items);
        $shippingAmount = 0;
        foreach ($items as $rowItem) {
            $itemArrayList[0] = $rowItem;
            $totalWeightItem = CartHelper::countWeight($itemArrayList);
            $shippingAmount = $totalWeight['shipping'] * $rowItem['shipping_assignation_rate'];
            $shippingCostItem = ($totalWeightItem['shipping_no_ceil'] / $totalWeight['shipping_no_ceil']) * $shippingAmount;
            $rowParams['shipping_amount'] = round($shippingCostItem, 2);
            $rowParams['carrier_id'] = $rowItem['carrier_id'];
            $rowParams['sku'] = $rowItem['sku'];
            $this->items->assignShippingAmountToItem($rowParams, $rowItem['is_free_item']);
            
        }
        return $shippingAmount;
    }

    /**
     * @param $items
     * @param $shippingRate
     * @param $carrierId
     * @return int
     */
    private function _applyShippingRateToItemsFromParams ($items, $shippingRate, $carrierId) {

        $totalWeight = CartHelper::countWeight($items);
        $shippingAmount = $totalWeight['shipping'] * $shippingRate;
        foreach ($items as $rowItem) {
            $itemArrayList[0] = $rowItem;
            $totalWeightItem = CartHelper::countWeight($itemArrayList);
            $shippingCostItem = ($totalWeightItem['shipping_no_ceil'] / $totalWeight['shipping_no_ceil']) * $shippingAmount;
            $rowParams['shipping_amount'] = round($shippingCostItem, 2);
            $rowParams['carrier_id'] = $carrierId;
            $rowParams['sku'] = $rowItem['sku'];
            $this->items->assignShippingAmountToItem($rowParams, $rowItem['is_free_item']);
        }
        return $shippingAmount;
    }

    /**
     * Get Shipping cost (rates) @todo: shipment_API candidate
     * @param string $supplier_alias
     * Supplier alias of the items
     * @param array $shipping_price_list
     * @param string $origin_shipping_district
     * @param string $shipping_destination
     * @param string $deliveryMethod
     * @return array
     */
    private function _getShippingRateAndCarrier($supplier_alias = "", $origin_shipping_district = "", $shipping_destination = "", $deliveryMethod = "")
    {
        $courierId = $this->getCourierId($supplier_alias, $shipping_destination, $deliveryMethod);
        if (empty($courierId)) {
            $this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan (carrier not found)",
                "error_getShippingCost", "Shipping courier not found: " . print_r([$supplier_alias, $shipping_destination, $deliveryMethod], true),
                "shipment","");
            return [];
        }

        $shippingRateData = $this->getShippingRate($origin_shipping_district, $shipping_destination, $courierId);
        if (empty($shippingRateData)) {
            $this->setErrorAndLog("RR200","Biaya pengiriman ke alamat anda tidak ditemukan (invalid shipping rate)",
                "error_getShippingRate", "Shipping rate not valid, origin_kecamatan_id : ". $origin_shipping_district .", destination_kecamatan_id : " . $shipping_destination . ", courier_id: " . $courierId,
                "shipment","");
            return [];
        } else {
            $shippingRate = $shippingRateData['price'];
        }

        return [
            'shipping_rate' => $shippingRate,
            'carrier_id' => $courierId
        ];
    }

    public function getShippingRate($origin_kecamatan_id = "", $destination_kecamatan_id = "", $carrier_id = "")
    {
        $resultData = array();
        $apiWrapper = new APIWrapper(getenv('SHIPMENT_API'));
        $apiWrapper->setEndPoint("shipping/rate?origin_kecamatan_id=" . $origin_kecamatan_id . "&destination_kecamatan_id=" . $destination_kecamatan_id . "&carrier_id=" . $carrier_id);
        if($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
            $resultData = $apiWrapper->getData();
        } else {
            $this->errorCode = "RR203";
            $this->errorMessages = "Get shipping rate failed";
            return;
        }
        return $resultData;
    }

    private function _countHandlingFee($items = array())
    {
        $handlingFee = 0;
        foreach($items as $item) {
            $handlingFeeAdjust = empty($item['handling_fee_adjust']) ? 0 : $item['handling_fee_adjust'];

            if(!empty(intval($handlingFeeAdjust))) {
                $handlingFee += TransactionHelper::countHandlingFee($handlingFeeAdjust,$item['qty_ordered']);
            }
        }

        return $handlingFee;
    }

    private function _countShippingAmount($items = array())
    {
        $shipping_amount = 0;
        foreach($items as $item) {
            $shippingAmount = empty($item['shipping_amount']) ? 0 : $item['shipping_amount'];
            $shippingDiscountAmount = empty($item['shipping_discount_amount']) ? 0 : $item['shipping_discount_amount'];

            if(!empty(intval($shippingAmount))) {
                $shipping_amount += (($shippingAmount - $shippingDiscountAmount) * $item['qty_ordered']);
            }

        }

        return $shipping_amount;
    }

    public function stockCheck()
    {
        if(empty($this->cart_id)) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart not found";
            return;
        }

        $shoppingCartData = $this->loadCartData();
        $shoppingCartitem = $shoppingCartData['items'];

        if(empty($shoppingCartitem)) {
            $this->error_code = "RR204";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Shopping cart item not found";
            return;
        }

        foreach($shoppingCartitem as $item) {
            $item['source_order'] = $this->source_order;
            $itemObj= new CartItem();
            $itemObj->setFromArray($item);
            $itemObj->checkStockItem($shoppingCartData['company_code']);

            if($errorCode = $itemObj->getErrorCode()) {
                $this->error_code[] = $errorCode;
                $this->error_messages['type'] = "sku";
                $this->error_messages['value'] = $itemObj->getSku();
                $this->error_messages['message'] = $itemObj->getErrorMessages();
            }
        }
    }

    /**
     * @param string $voucher_code
     * @param bool $force_redeem
     * @param bool $inital_cart
     * @param bool $update_cart
     * @param string $company_code
     * @return bool
     *
     * @todo: create more meaningful error messages
     */
    public function redeemVoucher($voucher_code = '', $force_redeem = false, $inital_cart = true, $update_cart = true, $company_code = 'ODI')
    {
        $marketingLib = new MarketingLib();
        $voucherDetail = $marketingLib->getVoucherDetail(trim($voucher_code), $company_code);

        if(!empty($marketingLib->getErrorCode())) {
            $this->error_code[] = $marketingLib->getErrorCode();
            $this->error_messages = array_merge($this->error_messages, $marketingLib->getErrorMessages());
            return false;
        }
        
        // If we don't get conditions return false as code not found
        if(empty($voucherDetail['rules'])) {
            $this->error_code[] = "RR201";
            $this->error_messages[] = "Voucher tidak ditemukan atau sudah berakhir (n/a rules).";
            return false;
        }
        
        // Load cart data
        $cartData = $this->loadCartData(true);
        
        // Before doing action do some rules validation
        $rules = $marketingLib->incrementPromotionUssage($voucherDetail['rules'],$cartData['cart_rules'],$cartData['gift_cards']);
        
        // Check if this rules can be applied for this company or not
        $isValidCompany = $marketingLib->isValidCompany($cartData,$rules);
        
        // if this is not the time to get this promotion skip it
        if(!$isValidCompany) {
            $this->error_code[] = "RR201";
            $this->error_messages[] = "Voucher tidak ditemukan atau sudah berakhir (company).";
            return false;
        }
        
        $isActiveTime = \Helpers\GeneralHelper::checkActiveBetweenDate($rules['from_date'],$rules['to_date']);
        
        // if this is not the time to get this promotion skip it
        if(!$isActiveTime) {
            $this->error_code[] = "RR201";
            $this->error_messages[] = "Voucher tidak ditemukan atau sudah berakhir (time limit).";
            return false;
        }
        
        if($rules['uses_per_customer'] > 0) {
            if($cartData['customer']['customer_is_guest'] == 1) {
                $this->error_code[] = "400";
                $this->error_messages[] = "Login untuk dapat menggunakan voucher.";
                return false;
            }
            
            if($rules['uses_per_customer_frequence'] == 0) {
                $isValidUsage = $marketingLib->maxUsageValidation($rules, $cartData['customer']['customer_id'],$voucherDetail);
            }else{
                $isValidUsage = $marketingLib->maxUsedFrequenceValidation($rules, $cartData['customer']['customer_id']);
            }

            if($isValidUsage === false) {
                $this->error_code[] = "400";
                $this->error_messages[] = "Maaf, penggunaan voucher sudah melebihi batas.";
                return false;
            }
        }

        if($rules['limit_used'] > 0) {
            $isValidUsage = $marketingLib->maxLimitUsedValidation($rules);
            if($isValidUsage === false) {
                $this->error_code = "400";
                $this->error_messages['type'] = "global";
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = "Maaf, penggunaan voucher sudah melebihi batas.";
                return false;
            }
        }

        if(!empty($rules['rule_group'])){
            $isValidGroup = $marketingLib->limitGroupRule($rules['rule_group'],$cartData['customer']['customer_id']);
            if($isValidGroup === false) {
                $this->error_code = "400";
                $this->error_messages['type'] = "global";
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = "Maaf, Anda sudah mengikuti promo ini.";
                return false;
            }
        }

        $can_combine = $marketingLib->canCombineCheck($cartData,$rules);

        if(!$can_combine) {
            $this->setErrorAndLog("RR100", "Voucher tidak bisa digabungkan dengan promo lain.");
            return false;
        }

        // just a little bit check if we know what to apply in action
        if(empty($rules['action']) || !isset($rules['action']['applied_action'])) {
            $this->setErrorAndLog("RR200", "Maaf, penggunaan voucher tidak sesuai dengan syarat dan ketentuan yang berlaku (no action).");
            return false;
        }

        if(empty($rules['use_voucher'])) {
            $this->error_code[] = "RR201";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Voucher tidak ditemukan atau sudah berakhir (n/a rule).";
            return false;
        }

        // Voucher condition
        $conditions = $rules['conditions'];

        /**
         * Customer redeem voucher in cart page, so we don't have payment information
         * For now we remove payment codition before we apply it
         */
        $conditions = CartHelper::removeConditionBasedOnAttribute($conditions,"payment");
        \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 2517', 'debug');
        $isMatchCondition = $this->marketingConditionCheck($conditions);
        if($isMatchCondition) {
            if($inital_cart) {
                $this->initialize();
            }

            if(!empty($voucherDetail['customer_id']) && $voucherDetail['customer_id'] != $this->customer->getCustomerId()) {
                $this->setErrorAndLog("RR200", "Voucher tidak bisa digunakan untuk akun anda.");
                return false;
            }

            $is_allow_group = false;
            if(isset($rules['customer_group_id']) && $rules['customer_group_id'] != "") {
                $allowedGroup = explode(",",$rules['customer_group_id']);

                // refactoring, should have break,once getting true
                foreach ($this->customer->getCustomerGroup() as $customer_group) {
                    if(in_array($customer_group,$allowedGroup)) {
                        $is_allow_group = true;
                        break;
                    }
                }
            }

            // If this group not allow to get this promotion, skip it
            if(!$is_allow_group) {
                $this->setErrorAndLog("RR200", "Maaf, customer group Anda tidak memenuhi syarat untuk promo ini.");
                return false;
            }

            // Check if voucher code already been used in this transaction or not
            if(!empty($this->gift_cards)) {
                $giftCardInfo = json_decode($this->gift_cards);
                $duplicateType = 0;
                foreach ($giftCardInfo as $card_info) {
                    if( ($card_info->voucher_code == $voucher_code) || ($card_info->rule_id == $voucherDetail['rule_id'] && ($card_info->can_combine == 0 && $card_info->global_can_combine == 0))) {
                        $this->setErrorAndLog("RR100", "Voucher atau Promo yang sama sudah diaplikasikan di shopping cart ini.");
                        return false;
                    }
                }
            }

            // Check if this customer's phone number already been used in this transaction or not
            if(!empty($cartData['customer']['customer_phone'] && $rules['uses_per_customer'] > 0)){

                // check if the rule whitelist from used many times
                $refundVoucher = explode(',', getenv('SKIP_ABUSE_RULE_ID'));                
                if(!in_array($rules['rule_id'],$refundVoucher)){
                    $salesOrderLib =  new SalesOrderLib;
                    $isDetectFraud = $salesOrderLib->getOrderListByPhoneAndGiftCards($cartData['customer']['customer_phone'], $voucher_code, $rules['uses_per_customer'], $rules['uses_per_customer_frequence']);
                    if ($isDetectFraud){
                        $this->setErrorAndLog("RR100", "Voucher tidak bisa digunakan untuk akun anda.");
                        return false;
                    } 
                } 

                
            }
            
            if($rules['promo_type'] != 'voucher' && $force_redeem == false) {
                $giftCardInfo[] = [
                    "rule_id" => $rules['rule_id'],
                    "voucher_code" => $voucherDetail['voucher_code'],
                    "voucher_amount" => 0,
                    "voucher_expired" => $voucherDetail['expiration_date'],
                    "voucher_amount_used" => 0,
                    "voucher_type" => $voucherDetail['type'],
                    "can_combine" => $rules['can_combine'],
                    "global_can_combine" => $rules['global_can_combine'],
                    "promo_type" => $rules['promo_type'],
                    "voucher_affected" => "global"
                ];

                $currentGiftCards = json_decode($this->gift_cards, true);
                $this->gift_cards = json_encode(array_merge($currentGiftCards, $giftCardInfo));
            } else {
                if(isset($rules['action']['applied_action']) && $rules['action']['applied_action'] == 'use_voucher') {
                    /**
                     * Mapping voucher so it's generic between redeem voucher and common marketing sales rule
                     */
                    $rules['action']['discount_amount'] = $voucherDetail['voucher_amount'];
                    if(!empty($voucherDetail['percentage']) && (int)$voucherDetail['percentage'] > 0) {
                        $rules['action']['discount_type'] = "percent";
                    } else if (!empty($voucherDetail['final_amount']) && (int)$voucherDetail['final_amount'] > 0){
                        $rules['action']['final_amount'] = $voucherDetail['final_amount'];
                        $rules['action']['discount_type'] = "final_amount";
                    } else {
                        $rules['action']['discount_type'] = "fix";
                    }
                    $rules['action']['max_redemption'] = (empty($voucherDetail['max_redemption'])) ? 0 : $voucherDetail['max_redemption'];
                } else {
                    $rules['action']['max_redemption'] = intval($rules['action']['max_redemption']);
                }

                $this->applyMarketingAction($rules, $voucherDetail);
            }

            if($update_cart) {
                $this->saveCart();
            }

        } else {
            $this->setErrorAndLog("RR200","Maaf, penggunaan voucher tidak sesuai dengan syarat dan ketentuan yang berlaku (general TnC).");
            return false;
        }

        return true;
    }

    public function deleteRedeemVoucher($voucher_code = "")
    {
        // Load cart data
        $this->initialize($this->cart_id);

        // Check if voucher code is used or not
        $voucher_used = false;
        $used_gift_card = [];
        $giftCardInfo = [];
        $rule_id = 0;
        if(!empty($this->gift_cards)) {
            $giftCardInfo = json_decode($this->gift_cards,true);
            foreach ($giftCardInfo as $key => $card_info) {
                if(strtolower($card_info['voucher_code']) == strtolower($voucher_code)) {
                    $rule_id = $card_info['rule_id'];
                    unset($giftCardInfo[$key]);
                    $used_gift_card = $card_info;
                    $voucher_used = true;
                    break;
                }
            }
        }

        if(!$voucher_used) {
            $this->setErrorAndLog("RR100","Voucher tidak ditemukan dalam transaksi anda.");
            return false;
        }

        if($used_gift_card['voucher_affected'] == 'shipping') {
            $this->shipping_discount_amount -= $used_gift_card['voucher_amount_used'];
        } else {
            $this->items->removePromotions(0,$voucher_code);
            $this->gift_cards_amount = $this->items->countGiftCardAmount();
        }

        /**
         * Make sure if we don't have gift card info
         * Gift Card Amount must not be set
         */
        if (empty($giftCardInfo) || $this->gift_cards_amount < 0) {
            $this->gift_cards_amount = 0;
        }

        $this->gift_cards = json_encode($giftCardInfo);
        $shoppingCart = $this->getDataArray();
        $this->saveToDb($shoppingCart);

        return true;
    }

    /**
     * check marketing conditions
     * here we using search engine in mongodb, if condition met it's will return shopping cart data
     *
     * @param array $conditions
     * @return mixed
     */
    public function marketingConditionCheck($conditions = array(), $cartData = [])
    { 
        if(empty($cartData)) {
            $cartData = $this->loadCartData(true);
        }

        // First we check globaly for given conditions
        $conditionsGlobal = CartHelper::removeConditionBasedOnAttribute($conditions);

        // informa conditions
        if(!empty($conditionsGlobal)){
            $grandTotal = 0;
            foreach($conditionsGlobal as $rowConditions){
                if($rowConditions['attribute'] == "device" && $rowConditions['value'] == "99999"){
                    foreach($cartData['items'] as $rowItems){
                        if(substr($rowItems['store_code'],0,1) == "H" || substr($rowItems['store_code'],0,1) == "J"){
                            $grandTotal += $rowItems['row_total'] - $rowItems['gift_cards_amount'];
                        }
                    }

                    $cartData['grand_total_exclude_shipping'] = $grandTotal;
                }
            }
        }

        $marketingLib = new \Library\Marketing();
        \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 2710 '.$cartData['reserved_order_no'], 'debug');
        $conditonCheckingStatus = $marketingLib->marketingConditionCheck($conditionsGlobal,$cartData);
        $isMatchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;
        if(!$isMatchCondition) {
            return $isMatchCondition;
        }

        /**
         * And now we check each item for given conditions
         * Remove other condition except for items
         * return only condition that have items condition only
         */
        $conditions = CartHelper::keepAttributeConditions($conditions);
        if(empty($conditions)) {
            return $isMatchCondition;
        }

        // Remove "items" from attribute, so attribute like items.xxx will be xxx
        $conditions = CartHelper::removeAtributeCondition($conditions);
             \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 2729', 'debug');
        $isMatchCondition = $this->marketingItemsConditionCheck($conditions, $cartData['items']);

        // Checking Global Sku Bundling
        if($isMatchCondition){
            $skuBundling = array();
            foreach($conditions as $condition){
                if($condition['attribute'] == 'sku_bundling'){
                    $skuBundling = explode(',', $condition['value']);
                }
            }

            if(!empty($skuBundling)){
                $cartItemsSku = array();
                foreach($cartData['items'] as $item){
                    $cartItemsSku[] = $item['sku'];
                }
                if(!empty($cartItemsSku)){
                    $count = 0;
                    foreach($cartItemsSku as $sku){
                        if(in_array($sku, $skuBundling)){
                            $count ++;
                        }
                    }
                    if($count < count($skuBundling)){
                        $isMatchCondition = false;
                    }
                }

            }

        }

        return $isMatchCondition;
    }

    public function marketingItemsConditionCheck($conditions = array(), $itemsData = [])
    {
        if(empty($itemsData)) {
            $cartData = $this->loadCartData(true);
            $itemsData = $cartData['items'];
        }
        
        if(empty($itemsData)) {
            $this->error_code = 404;
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Item is empty";
            return;
        }
        
        $marketingLib = new \Library\Marketing();
        $matchCondition = false;  
        
        // handle grand_total_exclude_shipping
        $promo1212 = false;
        foreach($conditions as $key => $value) {
            if ($value['attribute'] == 'grand_total_exclude_shipping') {
                $grandTotalExcludeShipping = 0;
                $operator = $value['operator'];
                $amountValue = $value['value'];
                unset($conditions[$key]);
            }elseif($value['attribute'] == 'subtotal'){
                $subtotal = 0;
                $operator = $value['operator'];
                $amountValue = $value['value'];
                unset($conditions[$key]);
            }elseif($value['attribute'] == 'product_source' && $value['value'] == "99999"){
                $promo1212 = true;
                unset($conditions[$key]);
            }
        }

        $conditions = array_values($conditions);
        
        $flagTrue = false;
        foreach ($itemsData as $item) {
            \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 2805', 'debug');
            $conditonCheckingStatus = $marketingLib->marketingConditionCheck($conditions,$item);
            $matchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;            
            if ($matchCondition) {
                $flagTrue = true;
            }
            // hotfix, harus check lebih perdalam lagi mengenai $flagtrue
            $eligibleItem = false;
            if ($matchCondition) {
                $eligibleItem = true;
            }

            if (isset($grandTotalExcludeShipping)) {
                $valueRedeemRefund = $marketingLib->redeemRefundCheck($item['gift_cards']);
                if ($matchCondition) {
                    if($promo1212){
                        $grandTotalExcludeShipping += (($item['selling_price'] + $item['handling_fee_adjust']) * $item['qty_ordered']) - $item['gift_cards_amount'];
                        continue;
                    } else if ($valueRedeemRefund > 0){
                        $grandTotalExcludeShipping += $item['row_total'] - $item['gift_cards_amount'] + $valueRedeemRefund;
                        continue;
                    } else{
                        $grandTotalExcludeShipping += $item['row_total'] - $item['gift_cards_amount'];
                        continue;
                    }    
                    
                }
            } elseif(isset($subtotal) && $eligibleItem){
                $subtotal += $item['row_total'];
            }
        }

        if (isset($grandTotalExcludeShipping)) {
            if ($operator == '>=') {
                if (!($grandTotalExcludeShipping >= $amountValue && $flagTrue)) {
                    $flagTrue = false;
                }
            }
        } elseif(isset($subtotal)){
            if ($operator == '>=') {
                if (!($subtotal >= $amountValue && $flagTrue)) {                   
                    return false;
                }
            }
        }

        /* 
        ** return true jika hanya 1 saja yang true, 
        ** pengecekan ulang condition dilakukan di $this->items->applyMarketingPromotion
        */
        return $flagTrue;
    }

    public function applyMarketingAction($rules = array(), $voucher_detail = array())
    {
        $marketingLib = new MarketingLib();
        $save_rule = false;

        if(empty($this->gift_cards_amount) && empty($this->gift_cards)) {
            $this->gift_cards_amount = 0; // make sure it's integer
            $this->gift_cards = "[]";
        }

        // just a little bit check if we know what to apply in action
        if(empty($rules['action']) || !isset($rules['action']['applied_action'])) {
            $this->error_code = "RR202";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "Can not apply action";
            return false;
        }

        // Check if this customer limit usage per voucher
        if(isset($rules['uses_per_customer']) && $rules['uses_per_customer'] > 0 && !empty($this->customer->getCustomerId())) {
            
            if($rules['uses_per_customer_frequence'] == 0) {
                $validationStatus = $marketingLib->maxUsageValidation($rules, $this->customer->getCustomerId(), $voucher_detail);
            }else{
                $validationStatus = $marketingLib->maxUsedFrequenceValidation($rules, $this->customer->getCustomerId());
            }
            
            if($validationStatus === false) {
                $this->error_code = $marketingLib->getErrorCode();
                $this->error_messages['type'] = "global";
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = $marketingLib->getErrorMessages();
                return false;
            }             
        }

        // Check if this customer limit usage
        if(isset($rules['limit_used']) && $rules['limit_used'] > 0) {
            $validationStatus = $marketingLib->maxLimitUsedValidation($rules);

            if($validationStatus === false) {
                $this->error_code = $marketingLib->getErrorCode();
                $this->error_messages['type'] = "global";
                $this->error_messages['value'] = "";
                $this->error_messages['message'] = $marketingLib->getErrorMessages();
                return false;
            }
        }

        if(empty($rules['action']['attributes'])) {
            $discountCartAttribute = ['items','price'];
        } else {
            $discountCartAttribute = explode(".",$rules['action']['attributes']);
        }

        // If action of conditions related to item, we need to assign it to correct item
        if($discountCartAttribute[0] == "items") {
            // Make sure we have item if we want to apply it
            if(!empty($this->items)) {
                 \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 2920', 'debug');
                list($gift_card_amount, $gift_cards, $currentDiscount, $discountAmount) = $this->items->applyMarketingPromotion($rules, $voucher_detail, $this->company_code);
                
                // re-assign split_cost
                $splitCost = array();
                $virtualInvoice = $this->items->generateVirtualInvoice();
                foreach($virtualInvoice['invoice'] as $supplierAlias => $invoiceByCompany) {
                    foreach($invoiceByCompany as $storeCode => $invoiceByDelivery) {
                        $shippingAmount = 0;
                        $shipping_discount_amount = 0;
                        foreach($invoiceByDelivery as $deliveryMethod => $invoiceByCourierID) {
                            foreach($invoiceByCourierID as $courierID => $invoiceByGeolocation) {
                                foreach($invoiceByGeolocation as $geoLocation => $invoiceItems) {
                                    if ($deliveryMethod != 'pickup') {
                                        foreach($invoiceItems as $rowDeliveryItem) {
                                            if (isset($rowDeliveryItem['shipping_amount'])) {
                                                $shippingAmount += $rowDeliveryItem['shipping_amount'];
                                                $shipping_discount_amount += $rowDeliveryItem['shipping_discount_amount'];
                                            }
                                        }
                                    }

                                    $flagFound = 0;
                                    foreach($splitCost as $rowSplitCost) {
                                        if ($rowSplitCost['store_code'] == $storeCode && $rowSplitCost['delivery_method'] == $deliveryMethod
                                        && $rowSplitCost['shipping_amount'] == $shippingAmount) {
                                            $flagFound = 1;
                                        }
                                    }

                                    if (!$flagFound) {
                                        $splitCost[] = array(
                                            "store_code" => $storeCode,
                                            "delivery_method" => $deliveryMethod,
                                            "shipping_amount" => $shippingAmount,
                                            "shipping_discount_amount" => $shipping_discount_amount
                                        );
                                    }   
                                }
                            }
                        }
                    }
                }

                $this->split_cost = json_encode($splitCost);

                if(!empty($gift_card_amount) || ($gift_cards != "[]" && !empty($gift_cards))) {
                    // Gift card from items can be duplicate, we need merge it
                    $mergedGiftcard = [];
                    $idGiftCardsApplied = [];
                    foreach ($gift_cards as $gc_used) {
                        if(!isset($mergedGiftcard[$gc_used['rule_id']])) {
                            $mergedGiftcard[$gc_used['rule_id']] = $gc_used;
                        } else {
                            $oldAmount = $mergedGiftcard[$gc_used['rule_id']]['voucher_amount'];
                            $oldAmountUsed = $mergedGiftcard[$gc_used['rule_id']]['voucher_amount_used'];

                            $newAmount = $gc_used['voucher_amount'] + $oldAmount;
                            $newAmountUsed = $gc_used['voucher_amount_used'] + $oldAmountUsed;

                            // Validation Max Redemption
                            if($rules['action']['discount_type'] == 'percent'){
                                if($newAmountUsed > (int)$rules['action']['max_redemption']){ 
                                    $newAmountUsed = (int)$rules['action']['max_redemption']; 
                                }
                                if($newAmount > (int)$rules['action']['max_redemption']){ 
                                    $newAmount = (int)$rules['action']['max_redemption']; 
                                }
                            }elseif($rules['action']['discount_type'] == 'fix'){
                                if($newAmountUsed > (int)$rules['action']['discount_amount']){ 
                                    $newAmountUsed = (int)$rules['action']['discount_amount']; 
                                }
                                if($newAmount > (int)$rules['action']['discount_amount']){ 
                                    $newAmount = (int)$rules['action']['discount_amount']; 
                                }
                            }

                            $mergedGiftcard[$gc_used['rule_id']]['voucher_amount'] = $newAmount;
                            $mergedGiftcard[$gc_used['rule_id']]['voucher_amount_used'] = $newAmountUsed;
                        }
                        $idGiftCardsApplied[] = $gc_used['rule_id'];
                    }

                    $currentGiftCards = json_decode($this->gift_cards, true);
                    if(!empty($currentGiftCards)){
                        foreach($currentGiftCards as $keyGiftCards => $rowGiftCards){
                            if(in_array($rowGiftCards['rule_id'],$idGiftCardsApplied) && substr($mergedGiftcard[$rowGiftCards['rule_id']]['voucher_code'],0,3) != "ODI"){
                                unset($currentGiftCards[$keyGiftCards]);
                            }
                        }
                    }
                    
                    $this->gift_cards = json_encode(array_merge($currentGiftCards, $mergedGiftcard));
                    
                    if($rules['action']['discount_type'] == 'percent'){
                        if($gift_card_amount > (int)$rules['action']['max_redemption']){ 
                            $gift_card_amount = (int)$rules['action']['max_redemption']; 
                        }
                        $voucher_detail['voucher_amount'] = strval($gift_card_amount);
                    }elseif($rules['action']['discount_type'] == 'fix'){
                        if($gift_card_amount > (int)$rules['action']['discount_amount']){ 
                            $gift_card_amount = (int)$rules['action']['discount_amount'];
                        }
                    }
                    $this->items->prorateFixer($voucher_detail);
                    $this->gift_cards_amount += $gift_card_amount;
                }

                // count again for subtotal and total row from items
                $totalItemData = $this->items->countTotal();

                //list($totalQtyItem, $rowTotal, $subTotalWithoutHandling)
                $this->total_qty_item = $totalItemData[0];
                //$this->subtotal = $totalItemData[2];
                
                $discountAmount = 0;
                if(!empty($currentDiscount)){
                    foreach($currentDiscount as $rowDiscount){
                        $thisDiscount = intval($rowDiscount['discount_amount']);
                        $discountAmount += $thisDiscount;
                    }
                    if($rules['action']['discount_type'] == 'percent'){
                        if($discountAmount > intval($rules['action']['max_redemption'])){
                            $discountAmount = intval($rules['action']['max_redemption']);
                        }
                    }elseif($rules['action']['discount_type'] == 'fix'){
                        if($discountAmount > intval($rules['action']['discount_amount'])){
                            $discountAmount = intval($rules['action']['discount_amount']);
                        }
                    }
                    $this->discount_amount += $discountAmount;
                }         
                                
                $this->_current_discount[] = [
                    'name' => $rules['name'],
                    'discount_amount' => $discountAmount
                ];
                
                // Recalculate shipping and handling
                $this->countShippingAndHandlingFee();
                 
                if($rules['use_voucher'] == 0){ 
                    $save_rule = true; 
                } 

            }
        } else {
            $discountAttribute = $discountCartAttribute[count($discountCartAttribute) - 1];

            if(substr($discountAttribute,0,8) == 'shipping') {
                $is_shipping_promo = true;
            } else {
                $is_shipping_promo = false;
            }

            $save_rule = true;
            if(!$is_shipping_promo) {
                if(property_exists($this,$discountAttribute)) {
                    $discountAttributAmount = $this->{$discountAttribute};
                } else if($discountAttribute == "subtotal_bundling"){
                    $discountAttributAmount = 0;
                    $skuBundling = array();
                    $conditions = $rules['conditions'];
                    foreach($conditions as $condition){
                        if($condition['attribute'] == "items.sku_bundling"){
                            $skuBundling = explode(',', str_replace(' ','',strtolower($condition['value'])));
                            break;
                        }
                    }

                    if(!empty($skuBundling)){
                        // load cart data
                        $cartData = $this->loadCartData(true);
                        foreach($cartData['items'] as $rowItem){
                            if(in_array(strtolower($rowItem['sku']), $skuBundling)){
                                $discountAttributAmount += $rowItem['subtotal'] - $rowItem['handling_fee_adjust'];
                            }
                        }

                    }else{
                        $this->error_code = "";
                        $this->error_messages['type'] = "global";
                        $this->error_messages['value'] = "";
                        $this->error_messages['message'] = "SKU Bundling tidak ditemukan";
                        return false;
                    }
                }else {
                    return;
                }

                if($discountAttribute == 'subtotal') {
                    $discountAttributAmount -= $this->gift_cards_amount;
                }

                // Implement max percentage for voucher ussage
                $voucherAmountPercentage = 0;
                if(!empty($voucher_detail['percentage']) && $voucher_detail['percentage'] > 0) {
                    $voucherAmountPercentage = round(($voucher_detail['percentage'] / 100) * $discountAttributAmount);

                    if(empty((int)$rules['action']['discount_amount'])) {
                        $rules['action']['discount_amount'] = $voucherAmountPercentage;
                    } else if(!empty((int)$rules['action']['discount_amount']) && $voucherAmountPercentage > $rules['action']['discount_amount']) {
                        $rules['action']['discount_amount'] = $rules['action']['discount_amount'];
                    }
                }

                list($discountAmount,$originDiscountAmount) = $marketingLib->countingDiscountGet($rules,$discountAttributAmount,$voucherAmountPercentage);

                if($voucher_detail) {
                    $giftCardInfo[] = [
                        "rule_id" => $rules['rule_id'],
                        "voucher_code" => $voucher_detail['voucher_code'],
                        "voucher_amount" => $originDiscountAmount,
                        "voucher_expired" => $voucher_detail['expiration_date'],
                        "voucher_amount_used" => $discountAmount,
                        "can_combine" => $rules['can_combine'],
                        "global_can_combine" => $rules['global_can_combine'],
                        "voucher_type" => $voucher_detail['type'],
                        "voucher_affected" => "global"
                    ];

                    /**
                     * Prorate giftcard amount to eligible item
                     */
                    $this->proparateGiftCard($giftCardInfo[0],$rules,$discountAttribute);

                    /**
                     * Maybe attribute used for discount (ex: grand_total or subtotal) not all item is eligible for giftcard
                     * After we prorate gift card amount, we re-calculate again to check whether the amount still same with old one
                     * if amount not same use the amount after we prorate gift card
                     */
                    $currentGiftCardsAmount = $this->items->countGiftCardAmount();

                    // Now we callucate final gift card amunt used for this giftcard
                    $giftCardInfo[0]['voucher_amount_used'] = $this->items->countItemGiftCard($voucher_detail['voucher_code']);;


                    $this->gift_cards_amount = $currentGiftCardsAmount;
                    $currentGiftCards = json_decode($this->gift_cards, true);
                    $this->gift_cards = json_encode(array_merge($currentGiftCards, $giftCardInfo));


                } else {
                    $this->_current_discount[] = [
                        'name' => $rules['name'],
                        'discount_amount' => $originDiscountAmount
                    ];
                    $this->discount_amount += $discountAmount;
                }
            } else {
                if(!empty($this->shipping_amount)) {
                    $subtotal = $this->subtotal - $this->discount_amount;
                    $discountShippingAmount = 0;
                    // $is_match_condition = true;
                    // foreach ($rules['conditions'] as $condition) {
                    //     if($condition['attribute'] == 'subtotal' &&  $subtotal < $condition['value']) {
                    //         $is_match_condition = false;
                    //         break;
                    //     }
                    // }

                    // if(!$is_match_condition) {
                    //     return false;
                    // }

                    $itemCondition = CartHelper::keepAttributeConditions($rules['conditions']);
                    if(empty($itemCondition)) {
                        $shippingAmount = round($this->shipping_amount - $this->shipping_discount_amount);

                        // Implement max percentage for voucher ussage
                        if(!empty($voucher_detail['percentage']) && $voucher_detail['percentage'] > 0) {
                            $voucherAmountPercentage = round(($voucher_detail['percentage'] / 100) * $shippingAmount);

                            if(empty((int)$rules['action']['discount_amount'])) {
                                $rules['action']['discount_amount'] = $voucherAmountPercentage;
                            } 
                        }

                        if($rules['action']['discount_type'] == 'percent') {
                            $discountAmountGet = round($shippingAmount * ($rules['action']['discount_amount'] / 100));
                            if(!empty((int)$rules['action']['max_redemption']) && $discountAmountGet > $rules['action']['max_redemption']) {
                                $discountAmountGet = $rules['action']['max_redemption'];
                            }

                            $originDiscountAmount = $discountShippingAmount =  CartHelper::countDiscountAmountUsed($shippingAmount,$discountAmountGet);

                        } else if ($rules['action']['discount_type'] == 'final_amount') {
                            $originDiscountAmount = $shippingAmount - $rules['action']['final_amount'];
                            $discountShippingAmount = CartHelper::countDiscountAmountUsed($shippingAmount,$originDiscountAmount);
                        } else {
                            $originDiscountAmount = $rules['action']['discount_amount'];
                            $discountShippingAmount =  CartHelper::countDiscountAmountUsed($shippingAmount, $rules['action']['discount_amount'] );
                        }

                        $this->shipping_discount_amount = round($this->shipping_discount_amount) + $discountShippingAmount;

                        if($this->shipping_discount_amount > $this->shipping_amount) {
                            $this->shipping_discount_amount = $this->shipping_amount;
                        }

                        if($voucher_detail) {
                            $giftCardInfo[] = [
                                "rule_id" => $rules['rule_id'],
                                "voucher_code" => $voucher_detail['voucher_code'],
                                "voucher_amount" => $originDiscountAmount,
                                "voucher_expired" => $voucher_detail['expiration_date'],
                                "voucher_amount_used" => $discountShippingAmount,
                                "can_combine" => $rules['can_combine'],
                                "global_can_combine" => $rules['global_can_combine'],
                                "voucher_type" => $voucher_detail['type'],
                                "voucher_affected" => "shipping"
                            ];

                            $currentGiftCards = json_decode($this->gift_cards, true);
                            $this->gift_cards = json_encode(array_merge($currentGiftCards, $giftCardInfo));
                        }

                        if(intval($this->shipping_discount_amount - $this->shipping_amount) == 0) {
                            $this->items->distributeFullShippingDiscountAmount();
                        } else if (!empty(intval($this->shipping_discount_amount))) {
                            $this->items->distributeShippingDiscountAmount($this->shipping_discount_amount, $this->items->getDataArray());
                        }

                    } else {
                        // Get shipping amount from item that eligible for free shiping
                        \Helpers\LogHelper::log('debug_625', '/app/Models/CartItem/Collection.php line 3247', 'debug');
                        $splitInvoice = $this->items->splitInvoiceShippingPromotion($itemCondition);

                        // Now we need to recalculate shipping amount for new "Invoice"
                        $eligibleInvoice = [
                            "invoice" => $splitInvoice['freeShippingInvoice'],
                            "origin_shipping_region" => $splitInvoice['origin_shipping_region'],
                        ];
                        $this->countShippingAndHandlingFee($eligibleInvoice);

                        if(!empty($splitInvoice['nonFreeShippingInvoice'])) {
                            $nonEligibleInvoice = [
                                "invoice" => $splitInvoice['nonFreeShippingInvoice'],
                                "origin_shipping_region" => $splitInvoice['origin_shipping_region'],
                            ];
                            $this->countShippingAndHandlingFee($nonEligibleInvoice);
                        }
                           \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 3263', 'debug');
                        $this->items->applyMarketingPromotion($rules, [], $this->company_code);

                        $this->shipping_discount_amount = $this->items->countShippingDiscountAmount();
                        $this->shipping_amount = $this->items->countShippingAmount();
                    }
                    $this->_current_discount[] = [
                        'name' => $rules['name'],
                        'discount_amount' => $discountShippingAmount
                    ];

                }
            }

            // if using voucher don't save the rules
            if($voucher_detail) {
                $save_rule = false;
                // $rules['voucher'] = $voucher_detail;
            }

        }

        // Validate if current rule already add in global rule
        if($save_rule) {
            $ruleList = json_decode($this->cart_rules, true);
            if(empty($ruleList)) {
                $is_rule_exist = false;
            } else {
                $is_rule_exist = CartHelper::checkRuleIsExist($ruleList,$rules);
            }

            if(!$is_rule_exist) {
                if(!empty($ruleList)) {
                    $currentRule[] = $rules;
                    $rule_list = array_merge($ruleList,$currentRule);
                } else {
                    $rule_list[] = $rules;
                }
                $this->cart_rules = json_encode($rule_list);
            }
        }

        if ( !empty($rules['limit_payment_time']) ) {
            if (empty($this->limit_payment_time) || $this->limit_payment_time > $rules['limit_payment_time']) {
                $this->limit_payment_time = (int)$rules['limit_payment_time'];
            }
        }

        // count again grant total
        $subtotalWithHandling = $this->subtotal + $this->handling_fee_adjust;
        $this->grand_total = TransactionHelper::countTotal($subtotalWithHandling, $this->discount_amount, $this->shipping_amount,$this->shipping_discount_amount, $this->gift_cards_amount);
        $this->countGrandTotalExcludeShipping();

        return true;
    }

    public function checkGiftcartApplied($filterGiftCards = array())
    {
        if(is_array($this->gift_cards)) {
            $giftCards = $this->gift_cards;
        } else {
            $giftCards = json_decode($this->gift_cards,true);
        }

        if(empty($giftCards) || empty($this->items)) {
            return;
        }

        // Clean up current gift card data
        $this->gift_cards = '[]';
        $this->gift_cards_amount = 0;
        
        $this->items->clearGiftCard();
        
        // recount grand total
        $this->countGrandTotal();
        
        $marketingLib = new MarketingLib();
        foreach ($giftCards as $giftCard)
        {
            $cartData = $this->getDataArray();
            if(isset($giftCard['promo_type']) && $giftCard['promo_type'] != "voucher") {
                $currentGiftcard = json_decode($this->gift_cards,true);
                $this->gift_cards = json_encode($currentGiftcard + [$giftCard]);
                continue;
            }

            // Remove promotion item related to giftcard
            $this->items->removePromotions($giftCard['rule_id'], $giftCard['voucher_code']);

            $voucherDetail = $marketingLib->getVoucherDetail(trim($giftCard['voucher_code']), $this->company_code);
            if(!empty($marketingLib->getErrorCode())) {
                continue;
            }
            
            $isValidCompany = $marketingLib->isValidCompany($cartData,$voucherDetail['rules']);
            if (!$isValidCompany) {
                continue;
            }
            
            $isActiveTime = \Helpers\GeneralHelper::checkActiveBetweenDate($voucherDetail['rules']['from_date'],$voucherDetail['rules']['to_date']);
            if (!$isActiveTime) {
                continue;
            }

            // Voucher condition
            $conditions = $voucherDetail['rules']['conditions'];
            if(!empty($filterGiftCards)){
                if($filterGiftCards[0] == 'exclude_payment'){
                    $conditions = CartHelper::removeConditionBasedOnAttribute($conditions,"payment");
                }
            }
            \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 3372', 'debug');
            $isMatchCondition = $this->marketingConditionCheck($conditions,$cartData);
            if($isMatchCondition) {
                $rules = $voucherDetail['rules'];
                $rules = $marketingLib->incrementPromotionUssage($rules,$this->cart_rules, $this->gift_cards);
                if(isset($rules['action']['applied_action']) && $rules['action']['applied_action'] == 'use_voucher') {
                    // Mapping voucher so it's generic between redeem voucher and common marketing sales rule
                    $rules['action']['discount_amount'] = $voucherDetail['voucher_amount'];
                    if(!empty($voucherDetail['percentage']) && (int)$voucherDetail['percentage'] > 0) {
                        $rules['action']['discount_type'] = "percent";
                    } else if (!empty($voucherDetail['final_amount']) && (int)$voucherDetail['final_amount'] > 0){
                        $rules['action']['final_amount'] = $voucherDetail['final_amount'];
                        $rules['action']['discount_type'] = "final_amount";
                    } else {
                        $rules['action']['discount_type'] = "fix";
                    }
                    $rules['action']['max_redemption'] = (empty($voucherDetail['max_redemption'])) ? 0 : $voucherDetail['max_redemption'];
                } else {
                    $rules['action']['max_redemption'] = intval($rules['action']['max_redemption']);
                }


                $this->applyMarketingAction($rules, $voucherDetail);
            }
        }

        /**
         * Make sure if we don't have gift card info
         * Gift Card Amount must not be set
         */
        if(is_array($this->gift_cards)) {
            $giftCards = $this->gift_cards;
        } else {
            $giftCards = json_decode($this->gift_cards,true);
        }

        if (empty($giftCards) || $this->gift_cards_amount < 0) {
            $this->gift_cards_amount = 0;
        }

        // recount grand total after we re applied GC
        $this->countGrandTotal();
    }

    public function proparateGiftCard($gift_card_info = [], $rules = [],$discount_attribute = 'subtotal')
    {
        \Helpers\LogHelper::log('debug_625', '/app/Models/Cart.php line 3421', 'debug');
        $this->items->prorateGiftCard($gift_card_info, $this->getDataArray(), $rules, $discount_attribute);
    }

    private function setErrorAndLog($error_code = "", $error_message = "", $log_filename = "", $log_message = "", $type = "", $value = "")
    {
        $this->error_code = $error_code;
        $this->error_messages['type'] = (!empty($type)) ? $type : "global"; 
        $this->error_messages['value'] = (!empty($value)) ? $value : ""; 
        $this->error_messages['message'] = $error_message;

        if (!empty($log_filename)) {
            LogHelper::log($log_filename, $log_message);
        }
    }

    public function getCourierId($supplierAlias = '', $shippingDestination = '', $deliveryMethod = '')
    {
        $courierId = '';
        // Marketplace
        if(substr($supplierAlias,0,2) == 'MP') {
            $courierId = 6;
        }

        return $courierId;
    }

    /**
     * Cart validation and conistency check
     */
    public function validateCart()
    {
        /**
         * Here we validating gift cart ussage, Amount of gift card used must have information abount gift card
         * otherwise, if we have information but amount is null we also need to reset it for consistancy
         */

        $giftCartInfo = json_decode($this->gift_cards,true);

        if ($this->gift_cards_amount > 0 || count($giftCartInfo) > 0 ) {
            if (empty($giftCartInfo)) {
                $this->gift_cards_amount = 0;
            }

            $itemGiftCartAmount = $this->items->countGiftCardAmount();
            if (empty($giftCartInfo) && !empty($itemGiftCartAmount)) {
                $this->items->clearGiftCard();
            }
        } else {
            $this->gift_cards_amount = 0;
            $this->gift_cards = '[]';
        }

        if ($this->grand_total < 0) {
            $this->items->markCanNotBuy();
        }
        
        // Validate item
        if ($this->items) {
            $this->items->itemsValidation($this->customer->getCustomerId(),$this->company_code);
            
            if (!empty($this->items->getErrorMessages())) {
                $this->error_messages = $this->items->getErrorMessages();
            }
        }
    }

    public function stockAssignationMixSku()
    {
        if (empty($this->items)) {
            return;
        }

        $skuList = $this->items->getBOGOMixSku();
        if (!empty($skuList)) {
            foreach ($skuList as $items) {
                $stockData = [];
                $skus = [];

                // Warning, IF SKU X = SKU Y, BOGO
                $flagItems = array();
                foreach ($items as $itemX) {
                    if(isset($flagItems[$itemX['sku']])){
                        $flagItems[$itemX['sku']]['qty'] += $itemX['qty'];
                    }else{
                        $flagItems[$itemX['sku']]['qty'] = $itemX['qty'];
                    }
                }    

                $items = array();
                foreach($flagItems as $keyFlag => $valFlag){
                    $items[$keyFlag]['sku'] = $keyFlag;
                    $items[$keyFlag]['qty'] = $valFlag['qty'];
                }

                foreach ($items as $item) {
                    $skus[] = $item['sku'];

                    // Load shopping card data
                    $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                    $apiWrapper->setEndPoint("legacy/stock/check_available/".$item['sku']."?qty=".$item['qty']."&company_code=".strtolower($this->company_code));

                    if($apiWrapper->send("get")) {
                        $apiWrapper->formatResponse();
                    }

                    if (empty($apiWrapper->getError())) {
                        // maybe cart is found, but data is empty
                        $productStockData = $apiWrapper->getData();

                        // We don't need any data, just what to assign this all item to a store code
                        $simpleData = [];
                        foreach ($productStockData['stock_list'] as $stock) {
                            $simpleData[]  = $stock['store_code'];
                        }

                        if (empty($stockData)) {
                            $stockData = $simpleData;
                        } else {
                            $stockData = array_intersect($stockData, $simpleData);
                        }
                    }else{

                        // Set Ketika free item stock tidak available, maka set is in stock product menjadi 0
                        $responseError = $apiWrapper->getError();
                        $responseData = $apiWrapper->getData();
                        if($responseError['code'] == 202 && $responseData['max_qty'] == 0){
                            
                            $isInStockParam['sku'] = '"'.$item['sku'].'"';
                            $isInStockParam['is_in_stock'] = 0;
                            $isInStockParam['company_code'] = 'ODI';
                            $productHelper = new \Helpers\ProductHelper();

                            $productHelper->updateIsInStockVariant($isInStockParam);
                            $productHelper->updateIsInStockProduct($isInStockParam);

                            // Delete items in cart
                            $cartLib = new \Library\Cart();
                            $params = array(
                                'cart_id' => $this->cart_id,
                                'items' => array($item['sku'])
                            );
                            $cartLib->deleteCartItems($params);
                        }
                    }
                }

                // Now we have it where we can assign this item to, now let's put assignment on this
                if (!empty($stockData)) {
                    $storeData = \Helpers\ProductHelper::getStoreInformation($stockData);

                    $cartLib = new \Library\Cart();
                    $billingAddress = (is_object($this->billing_address)) ? $this->billing_address->getDataArray() : $this->billing_address;
                    $cartData['shipping_address']['kecamatan']['kecamatan_code'] = (isset($billingAddress['kecamatan']['kecamatan_code'])) ? $billingAddress['kecamatan']['kecamatan_code'] : getenv("DEFAULT_SHIPPING_KECAMATAN_CODE");
                    $assignation = $cartLib->shippingAssignation($cartData, $this->company_code);
                    $this->items->assignBOGOMixSku($skus, $storeData, $this->company_code, $assignation);
                } else {
                    $this->items->markCanNotBuy($skus, 'bogo');
                }
            }

            if (!empty($this->items->getErrorCode())) {
                $this->error_code = $this->items->getErrorCode();
                $this->error_messages["type"] = "sku";  
                $this->error_messages["value"] = $this->items->getSku();  
                $this->error_messages["message"] = $this->items->getErrorMessages();  
            }
        }
    }

    public function courierValidation()
    {
        if (empty($this->items)) {
            return;
        }

        // group per shipment
        $cartData = $this->getDataArray();
        $cartLib = new \Library\Cart();
        if(!empty($cartData) && !empty($cartData['items'])){
            // group the cart item by sender, storeCode, deliveryMethod, carrierID and geoLocation
            $cartCustom = array();
            foreach ($cartData['items'] as $row){
                $sender = $cartLib->getSender($row);
                $storeCode = $row['store_code'];
                $courierPickup = ($row['delivery_method'] == 'pickup') ? $row['store_code'] : $row['carrier_id'];
                $geolocation = (isset($row['shipping_address']['geolocation']) && !empty($row['shipping_address']['geolocation'])) ? $row['shipping_address']['geolocation'] : 'geolocation_empty';
                $cartCustom[$sender][$storeCode][$row['delivery_method']][$courierPickup][$geolocation][] = $row; 
            }

            // convert the group to ordinary array
            $tempItem = array();
            foreach($cartCustom as $keySender => $valueSender) {
                foreach($valueSender as $keyStoreCode => $valueStoreCode) {
                    foreach($valueStoreCode as $keyDeliveryMethod => $valueDeliveryMethod) {
                        foreach($valueDeliveryMethod as $keyCarrierID => $valueCarrierID) {
                            foreach($valueCarrierID as $keyGeoLocation => $valueGeoLocation) {
                                $tempItem[] = $valueGeoLocation; 
                            }   
                        }          
                    }
                }
            }

            // complete $tempItem header
            $modifiedItem = array();
            $parentCount = 0;
            foreach($tempItem as $rowHeader) {
                $tempShippingFee = 0;
                foreach($rowHeader as $rowItemChild) {
                    $modifiedItem[$parentCount]['delivery_method'] = $rowItemChild['delivery_method'];
                    if (isset($rowItemChild['carrier_id']) && !empty($rowItemChild['carrier_id'])) {
                        $modifiedItem[$parentCount]['carrier'] = $cartLib->getCarrierData($rowItemChild['carrier_id']);
                    } else {
                        $modifiedItem[$parentCount]['carrier'] = array();
                    }
                    
                    if ($rowItemChild['delivery_method'] <> 'pickup') {
                        if (!isset($rowItemChild['shipping_address']['geolocation'])) {
                            $rowItemChild['shipping_address']['geolocation'] = '';
                        }
                    }
                    
                    $modifiedItem[$parentCount]['details'][] = $rowItemChild;
                }

                $parentCount++;
            }
            
            foreach($modifiedItem as $rowShipment) {
                if ($rowShipment['delivery_method'] == 'store_fulfillment') {
                    $carrierID = "";
                    if (count($rowShipment['carrier']) > 0) {
                        $carrierID = $rowShipment['carrier']['carrier_id'];
                    }

                    if (CartHelper::isGosend($carrierID)) {
                        $this->_countGosendCost($rowShipment['details'], true);
                    }
                }

                if ($rowShipment['delivery_method'] <> 'pickup') {
                    if (!isset($rowShipment['details'][0]['shipping_address']['first_name'])) {
                        $this->error_code = "RR200";
                        $this->error_messages['type'] = "shipment";
                        $this->error_messages['value'] = $rowShipment['details'][0]['group_shipment'];
                        $this->error_messages['message'] = "Shipping address not valid";
                        return;
                    }
                }
            }
        } else {
            $this->error_code = "RR200";
            $this->error_messages['type'] = "global";
            $this->error_messages['value'] = "";
            $this->error_messages['message'] = "We cannot proceed your order";
        }
    }
}