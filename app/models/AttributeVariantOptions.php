<?php

namespace Models;

class AttributeVariantOptions extends \Models\BaseModel
{
    protected $option_id;

    protected $attribute_id;

    protected $option_value;

    protected $extra_information;

    protected $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('attribute_id', 'Models\MasterAttributes', 'attribute_id', array('alias' => 'Attribute'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'attribute_variant_options';
    }

    /**
     * @return mixed
     */
    public function getOptionId()
    {
        return $this->option_id;
    }

    /**
     * @param mixed $option_id
     */
    public function setOptionId($option_id)
    {
        $this->option_id = $option_id;
    }

    /**
     * @return mixed
     */
    public function getAttributeId()
    {
        return $this->attribute_id;
    }

    /**
     * @param mixed $attribute_id
     */
    public function setAttributeId($attribute_id)
    {
        $this->attribute_id = $attribute_id;
    }

    /**
     * @return mixed
     */
    public function getOptionValue()
    {
        return $this->option_value;
    }

    /**
     * @param mixed $option_value
     */
    public function setOptionValue($option_value)
    {
        $this->option_value = $option_value;
    }

    /**
     * @return mixed
     */
    public function getExtraInformation()
    {
        return $this->extra_information;
    }

    /**
     * @param mixed $extra_information
     */
    public function setExtraInformation($extra_information)
    {
        $this->extra_information = $extra_information;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeOptionValue[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeOptionValue
     */
    public static function findFirst($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

     /*
     * Override base model function to delete from cache
     */
    public function afterUpdate()
    {

        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);

        $redisKey = "AttributeVariantOptions";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }
}
