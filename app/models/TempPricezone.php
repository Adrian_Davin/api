<?php
/**
 * Created by PhpStorm.
 * User: roesmien_ecomm
 * Date: 2/8/2017
 * Time: 9:49 AM
 */

namespace Models;
use Phalcon\Db as Database;

class TempPricezone extends \Models\BaseModel
{

    protected $errors;

    protected $messages;

    protected $data;

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'temp_pricezone';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    function _group_by($array, $key) {
        $return = array();
        foreach($array as $val) {
            $return[$val[$key]][] = $val;
        }
        return $return;
    }

    public function updateBatch($params = array()){
        try {
            if($params['method'] == 'truncate_price'){
                $sql = "TRUNCATE TABLE temp_pricezone";
            }elseif($params['method'] == 'truncate_batch_pricezone'){
                $sql = "TRUNCATE TABLE temp_batch_pricezone";
            }elseif($params['method'] == 'delete_not_exist'){
                $sql = "delete from temp_pricezone where sku not in (
                            select sku from product_price
                        )";
            }elseif($params['method'] == 'exchange_sku'){
                $sql = "update temp_pricezone set sku = skuold";
            }elseif($params['method'] == 'delete_temp_less_special'){
                $sql = "delete from temp_pricezone where price < special_price";
            }elseif($params['method'] == 'delete_temp_less_10'){
                $sql = "delete from temp_pricezone where price < 10 or special_price < 10";
            }elseif($params['method'] == 'delete_override_sap'){
                $sql = "delete from  temp_pricezone where sku in (
                            select sku from price_override_sap where now() between from_date and to_date
                        )";
            }

            if($params['method'] == 'move_to_batch_pricezone'){

                /**
                 * Insert SKU, Having = 1
                 */
                $sql = "insert into temp_batch_pricezone (sku, price, special_price, special_from_date, special_to_date, zone_id, sisco)  
                        select 
                            sku, price, special_price, special_from_date, special_to_date, zone_id, '".$params['sisco']."' as sisco
                        from  
                            temp_pricezone a
                        where sku in (
                            SELECT sku FROM temp_pricezone group by sku having count(sku) = 1
                        ) 
                        ";
                $this->getDi()->getShared('dbMaster')->execute($sql);
                $affectedRowInsert = $this->getDi()->getShared('dbMaster')->affectedRows();

                /**
                 * Get All SKU, having count > 1
                 */

                $sqlAllTempPriceZone = " select 
                                            sku, price, special_price, special_from_date, special_to_date, zone_id, '" . $params['sisco'] . "' as sisco
                                        from  
                                            temp_pricezone a 
                                        where sku in (
                                            SELECT sku FROM temp_pricezone group by sku having count(sku) > 1
                                        )     
                                ";

                $this->useReadOnlyConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($sqlAllTempPriceZone);
                $arrValue = array();
                if ($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $allTempPriceZone = $result->fetchAll();

                    $sql = 'insert into temp_batch_pricezone (sku, price, special_price, special_from_date, special_to_date, zone_id, sisco) values ';
                    $skuGroup = $this->_group_by($allTempPriceZone,'sku');
                    foreach($skuGroup as $sku => $rowMulti){
                        $uniqueSku = array_reduce($rowMulti, function ($a, $b) {
                            return @$a['special_from_date'] > $b['special_from_date'] ? $a : $b ;
                        });
                        $arrValue[] = "(
                                        '".$uniqueSku['sku']."',
                                        ".$uniqueSku['price'].",
                                        ".(empty($uniqueSku['special_price'])? 'null': $uniqueSku['special_price']).",
                                        ".(empty($uniqueSku['special_from_date'])? 'null': "'".$uniqueSku['special_from_date']."'").",
                                        ".(empty($uniqueSku['special_to_date'])? 'null': "'".$uniqueSku['special_to_date']."'").",
                                        ".$uniqueSku['zone_id'].",".$uniqueSku['sisco']."
                                    )";
                    }

                    $sql .= implode(',',$arrValue);
                    $this->getDi()->getShared('dbMaster')->execute($sql);
                }

                $this->setMessages(array('Successfully Query Batch'));
                $affectedRowInsertMoreThan1 = count($arrValue);
                $this->setData(array('affected_row' => $affectedRowInsertMoreThan1 + $affectedRowInsert));
                $this->setErrors(array());

            }else{
                $result = $this->getDi()->getShared('dbMaster')->execute($sql);

                if(!$result){
                    \Helpers\LogHelper::log("productVariant ".$params['method'], 'There have something error in update data');
                    $this->setErrors(array('code' => 'RR303','title' => 'productVariant Query Batch','message' => 'There have something error in update data'));
                    $this->setData(array('affected_row' => 0));
                }else{
                    $this->setMessages(array('Successfully Query Batch'));
                    $this->setData(array('affected_row' => $this->getDi()->getShared('dbMaster')->affectedRows()));
                    $this->setErrors(array());
                }
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("productVariant_queryBatch", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'productVariant_queryBatch','message' => 'There have something error, in query batch'));
            $this->setData(array('affected_row' => 0));
        }
    }

    public function queryBatch($params = false)
    {
        try {
            $sql = "

                    select 
                        sku, price, special_price as special_price, special_from_date, special_to_date
                    from temp_batch_pricezone 
                    where sku in (
                        select 
                            sku
                        from temp_batch_pricezone 
                        where zone_id = " . $params['zone_id'] . "
                        group by sku
                        having count(sku) " . $params['operator'] . "
                    ) and zone_id = " . $params['zone_id'] . "
                    
                    ";


            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                return $result->fetchAll();
            } else {
                \Helpers\LogHelper::log("productVariant", 'Theres no data');
                $this->setErrors(array('code' => 'RR301', 'title' => 'productVariant', 'message' => 'Theres no data'));
                return false;
            }

        } catch (\Exception $e) {
            \Helpers\LogHelper::log("productVariant", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301', 'title' => 'productVariant', 'message' => 'There have something error, in query getListProductCustom'));
            return false;
        }
    }

    public function importBatch($data = array()){
        try {

            $sql = "insert into ".$data['model']." (".$data['fields'].") VALUES ".$data['values'];
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            if(!$result){
                \Helpers\LogHelper::log("temp price zone", 'There have something error in update data');
                $this->setErrors(array('code' => 'RR303','title' => 'temp price zone','message' => 'There have something error in update data'));
                $this->setData(array('affected_row' => 0));
            }else{
                $this->setMessages(array('Successfully Import Batch'));
                $this->setData(array('affected_row' => $this->getDi()->getShared('dbMaster')->affectedRows()));
                $this->setErrors(array());
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("temp price zone", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'temp price zone','message' => 'There have something error, in query import batch'));
            $this->setData(array('affected_row' => 0));
        }
    }

}