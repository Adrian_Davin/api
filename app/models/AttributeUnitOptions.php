<?php

namespace Models;

class AttributeUnitOptions extends \Models\BaseModel
{

    protected $unit_option_id;

    protected $attribute_unit_id;

    protected $unit_option_value;

    protected $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'attribute_unit_options';
    }

    /**
     * @return mixed
     */
    public function getUnitOptionId()
    {
        return $this->unit_option_id;
    }

    /**
     * @param mixed $unit_option_id
     */
    public function setUnitOptionId($unit_option_id)
    {
        $this->unit_option_id = $unit_option_id;
    }

    /**
     * @return mixed
     */
    public function getAttributeUnitId()
    {
        return $this->attribute_unit_id;
    }

    /**
     * @param mixed $attribute_unit_id
     */
    public function setAttributeUnitId($attribute_unit_id)
    {
        $this->attribute_unit_id = $attribute_unit_id;
    }

    /**
     * @return mixed
     */
    public function getUnitOptionValue()
    {
        return $this->unit_option_value;
    }

    /**
     * @param mixed $unit_option_value
     */
    public function setUnitOptionValue($unit_option_value)
    {
        $this->unit_option_value = $unit_option_value;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Attribute[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Attribute
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}
