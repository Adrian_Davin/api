<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerAceDetails extends \Models\BaseModel {

  protected $customer_ace_details_id;
  protected $customer_id;
  protected $ace_customer_id;
  protected $ace_customer_member_no;
  protected $customer_token;
  protected $login_attempt;
  protected $status;
  protected $expired_date;
  protected $created_at;
  protected $updated_at;

  public function setCustomerAceDetailsId($customer_ace_details_id) {
    $this->customer_ace_details_id = $customer_ace_details_id;
  }

  public function getCustomerAceDetailsId()
  {
      return $this->customer_ace_details_id;
  }

  public function getAceCustomerMemberNo()
  {
      return $this->ace_customer_member_no;
  }

  public function getCustomerToken()
  {
      return $this->customer_token;
  }

  public function setCustomerToken($customer_token) {
    $this->customer_token = $customer_token;
  }

  public function setLoginAttempt($login_attempt) {
    $this->login_attempt = $login_attempt;
  }

  public function getLoginAttempt()
  {
      return $this->login_attempt;
  }

  public function getStatus()
  {
      return $this->status;
  }

  public function setExpiredDate($expired_date) {
    $this->expired_date = $expired_date;
  }
  
  public function getExpiredDate() {
      return $this->expired_date;
  }

  public function initialize()
  {
    $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
  }

  /**
   * Returns table name mapped in the model.
   *
   * @return string
   */
  public function getSource()
  {
      return 'customer_ace_details';
  }

  /**
   * Allows to query a set of records that match the specified conditions
   *
   * @param mixed $parameters
   * @return Customer[]
   */
  public static function find($parameters = null)
  {
      return parent::find($parameters);
  }

  /**
   * Allows to query the first record that match the specified conditions
   *
   * @param mixed $parameters
   * @return Customer
   */
  public static function findFirst($parameters = null)
  {
      return parent::findFirst($parameters);
  }

  public function setFromArray($data_array = array())
  {
      $this->assign($data_array);

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray, $data_array));
  }

  public function afterUpdate() {
    // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
    // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
    \Helpers\LogHelper::log("after_update_customer_ace", "TRIGGERED: " .json_encode($this->toArray()));
  }

  // public function update
}

?>