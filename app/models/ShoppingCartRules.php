<?php

class ShoppingCartRules extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $cart_rule_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $cart_rule_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $date_start;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $date_end;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'shopping_cart_rules';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ShoppingCartRules[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ShoppingCartRules
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
