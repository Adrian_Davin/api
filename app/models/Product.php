<?php

namespace Models;

use GuzzleHttp\Client;
use Helpers\ProductHelper;
use Phalcon\Db as Database;

class Product extends \Models\BaseModel
{

    const PRODUCT_STATUS_ACTIVE=10;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $attribute_set_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $tag_search;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $name;

    protected $supplier_id;
    protected $brand_id;

    protected $is_in_stock;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $meta_title;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $meta_keyword;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $meta_description;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $package_content;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $warranty_code;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $warranty_unit;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $warranty_part;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $warranty_services;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $warranty_contributor;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $packaging_uom;

    /**
     *
     * @var double
     * @Column(type="double", length=5, nullable=true)
     */
    protected $packaging_height;

    /**
     *
     * @var double
     * @Column(type="double", length=5, nullable=true)
     */
    protected $packaging_length;

    /**
     *
     * @var double
     * @Column(type="double", length=5, nullable=true)
     */
    protected $packaging_width;

    /**
     *
     * @var double
     * @Column(type="double", length=5, nullable=true)
     */
    protected $product_height;

    /**
     *
     * @var double
     * @Column(type="double", length=5, nullable=true)
     */
    protected $product_width;

    /**
     *
     * @var double
     * @Column(type="double", length=5, nullable=true)
     */
    protected $product_length;

    /**
     *
     * @var double
     * @Column(type="double", length=5, nullable=true)
     */
    protected $weight;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $description;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $how_to_use;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $tips_trick;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $specification;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $video_link;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $url_key;

    protected $wilayah_template_id;

    protected $courier_id;

    protected $shared_commision;

    protected $increment_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * @var $productAttribute \Models\Product\Attribute\Collection
     */
    protected $productAttribute;

    /**
     * @var $productVariant \Models\Product\ProductVariant\Collection
     */
    protected $productVariant;

    /**
     * @var $supplier \Models\Supplier
     */
    protected $supplier;

    /**
     * @var
     */
    protected $default_sort;

    /**
     * @var
     */
    protected $popularity;

    /**
     * @var $productCategory \Models\Product\ProductCategoryCollection
     */
    protected $productCategory;

    /**
     * @var $product2Category \Models\Product\Product2CategoryCollection
     */
    protected $product2Category;

    /**
     * @var $brand \Models\MasterBrand
     */
    protected $brand;

    protected $errors;
    protected $messages;
    protected $data;

    protected $whiteList;
    protected $related_product;
    protected $is_mp;

    protected $additional_header;
    protected $inline_script;
    protected $inline_script_seo;
    protected $inline_script_seo_mobile;
    protected $old_url_key;
    protected $visible;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('product_id', 'Models\Product2Category', 'product_id', array('alias' => 'Product2Category'));
        $this->hasMany('product_id', 'Models\ProductAttributeInt', 'product_id', array('alias' => 'ProductAttributeInt'));
        $this->hasMany('product_id', 'Models\ProductAttributeText', 'product_id', array('alias' => 'ProductAttributeText'));
        $this->hasMany('product_id', 'Models\ProductAttributeVarchar', 'product_id', array('alias' => 'ProductAttributeVarchar'));
        $this->hasMany('product_id', 'Models\ProductVariant', 'product_id', array('alias' => 'ProductVariant'));
        $this->belongsTo('attribute_set_id', 'Models\AttributeSet', 'attribute_set_id', array('alias' => 'AttributeSet', "reusable" => true));
        $this->belongsTo('brand_id', 'Models\MasterBrand', 'brand_id', array('alias' => 'MasterBrand', "reusable" => true));
        $this->belongsTo('supplier_id', 'Models\Supplier', 'supplier_id', array('alias' => 'Supplier', "reusable" => true));

    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->whiteList = array();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product';
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getAttributeSetId()
    {
        return $this->attribute_set_id;
    }

    /**
     * @param int $attribute_set_id
     */
    public function setAttributeSetId($attribute_set_id)
    {
        $this->attribute_set_id = $attribute_set_id;
    }


    /**
     * @return string
     */
    public function getTagSearch()
    {
        return $this->tag_search;
    }

    /**
     * @param string $tag_search
     */
    public function setTagSearch($tag_search)
    {
        $this->tag_search = $tag_search;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getIsInStock()
    {
        return $this->is_in_stock;
    }

    /**
     * @param mixed $is_in_stock
     */
    public function setIsInStock($is_in_stock)
    {
        $this->is_in_stock = $is_in_stock;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * @param string $meta_title
     */
    public function setMetaTitle($meta_title)
    {
        $this->meta_title = $meta_title;
    }

    /**
     * @return string
     */
    public function getMetaKeyword()
    {
        return $this->meta_keyword;
    }

    /**
     * @param string $meta_keyword
     */
    public function setMetaKeyword($meta_keyword)
    {
        $this->meta_keyword = $meta_keyword;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * @param string $meta_description
     */
    public function setMetaDescription($meta_description)
    {
        $this->meta_description = $meta_description;
    }

    /**
     * @return string
     */
    public function getPackageContent()
    {
        return $this->package_content;
    }

    /**
     * @param string $package_content
     */
    public function setPackageContent($package_content)
    {
        $this->package_content = $package_content;
    }

    /**
     * @return string
     */
    public function getWarrantyCode()
    {
        return $this->warranty_code;
    }

    /**
     * @param string $warranty_code
     */
    public function setWarrantyCode($warranty_code)
    {
        $this->warranty_code = $warranty_code;
    }

    /**
     * @return string
     */
    public function getWarrantyUnit()
    {
        return $this->warranty_unit;
    }

    /**
     * @return mixed
     */
    public function getRelatedProduct()
    {
        return $this->related_product;
    }

    /**
     * @param mixed $related_product
     */
    public function setRelatedProduct($related_product)
    {
        $this->related_product = $related_product;
    }

    /**
     * @param string $warranty_unit
     */
    public function setWarrantyUnit($warranty_unit)
    {
        $this->warranty_unit = $warranty_unit;
    }

    /**
     * @return string
     */
    public function getWarrantyPart()
    {
        return $this->warranty_part;
    }

    /**
     * @param string $warranty_part
     */
    public function setWarrantyPart($warranty_part)
    {
        $this->warranty_part = $warranty_part;
    }

    /**
     * @return string
     */
    public function getWarrantyServices()
    {
        return $this->warranty_services;
    }

    /**
     * @param string $warranty_services
     */
    public function setWarrantyServices($warranty_services)
    {
        $this->warranty_services = $warranty_services;
    }

    /**
     * @return string
     */
    public function getWarrantyContributor()
    {
        return $this->warranty_contributor;
    }

    /**
     * @param string $warranty_contributor
     */
    public function setWarrantyContributor($warranty_contributor)
    {
        $this->warranty_contributor = $warranty_contributor;
    }

    /**
     * @return string
     */
    public function getPackagingUom()
    {
        return $this->packaging_uom;
    }

    /**
     * @param string $packaging_uom
     */
    public function setPackagingUom($packaging_uom)
    {
        $this->packaging_uom = $packaging_uom;
    }

    /**
     * @return float
     */
    public function getPackagingHeight()
    {
        return $this->packaging_height;
    }

    /**
     * @param float $packaging_height
     */
    public function setPackagingHeight($packaging_height)
    {
        $this->packaging_height = $packaging_height;
    }

    /**
     * @return float
     */
    public function getPackagingLength()
    {
        return $this->packaging_length;
    }

    /**
     * @param float $packaging_length
     */
    public function setPackagingLength($packaging_length)
    {
        $this->packaging_length = $packaging_length;
    }

    /**
     * @return float
     */
    public function getPackagingWidth()
    {
        return $this->packaging_width;
    }

    /**
     * @param float $packaging_width
     */
    public function setPackagingWidth($packaging_width)
    {
        $this->packaging_width = $packaging_width;
    }

    /**
     * @return float
     */
    public function getProductHeight()
    {
        return $this->product_height;
    }

    /**
     * @param float $product_height
     */
    public function setProductHeight($product_height)
    {
        $this->product_height = $product_height;
    }

    /**
     * @return float
     */
    public function getProductWidth()
    {
        return $this->product_width;
    }

    /**
     * @param float $product_width
     */
    public function setProductWidth($product_width)
    {
        $this->product_width = $product_width;
    }

    /**
     * @return float
     */
    public function getProductLength()
    {
        return $this->product_length;
    }

    /**
     * @param float $product_length
     */
    public function setProductLength($product_length)
    {
        $this->product_length = $product_length;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getHowToUse()
    {
        return $this->how_to_use;
    }

    /**
     * @param string $how_to_use
     */
    public function setHowToUse($how_to_use)
    {
        $this->how_to_use = $how_to_use;
    }

    /**
     * @return string
     */
    public function getTipsTrick()
    {
        return $this->tips_trick;
    }

    /**
     * @param string $tips_trick
     */
    public function setTipsTrick($tips_trick)
    {
        $this->tips_trick = $tips_trick;
    }

    /**
     * @return string
     */
    public function getSpecification()
    {
        return $this->specification;
    }

    /**
     * @param string $specification
     */
    public function setSpecification($specification)
    {
        $this->specification = $specification;
    }

    /**
     * @return string
     */
    public function getVideoLink()
    {
        return $this->video_link;
    }

    /**
     * @param string $video_link
     */
    public function setVideoLink($video_link)
    {
        $this->video_link = $video_link;
    }

    /**
     * @return string
     */
    public function getUrlKey()
    {
        if(isset($this->url_key)) {
            return $this->url_key;
        }
        return "";
    }

    /**
     * @param string $url_key
     */
    public function setUrlKey($url_key)
    {
        $this->url_key = $url_key;
    }

    /**
     * @return mixed
     */
    public function getAdditionalHeader()
    {
        return $this->additional_header;
    }

    /**
     * @param mixed $additional_header
     */
    public function setAdditionalHeader($additional_header)
    {
        $this->additional_header = $additional_header;
    }

    /**
     * @return mixed
     */
    public function getInlineScript()
    {
        return $this->inline_script;
    }

    /**
     * @param mixed $inline_script
     */
    public function setInlineScript($inline_script)
    {
        $this->inline_script = $inline_script;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptSeo()
    {
        return $this->inline_script_seo;
    }

    /**
     * @param mixed $inline_script_seo
     */
    public function setInlineScriptSeo($inline_script_seo)
    {
        $this->inline_script_seo = $inline_script_seo;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptSeoMobile()
    {
        return $this->inline_script_seo_mobile;
    }

    /**
     * @param mixed $inline_script_seo_mobile
     */
    public function setInlineScriptSeoMobile($inline_script_seo_mobile)
    {
        $this->inline_script_seo_mobile = $inline_script_seo_mobile;
    }

    /**
     * @return mixed
     */
    public function getOldUrlKey()
    {
        return $this->old_url_key;
    }

    /**
     * @param mixed $old_url_key
     */
    public function setOldUrlKey($old_url_key)
    {
        $this->old_url_key = $old_url_key;
    }

    /**
     * @return mixed
     */
    public function getWilayahTemplateId()
    {
        return $this->wilayah_template_id;
    }

    /**
     * @param mixed $wilayah_template_id
     */
    public function setWilayahTemplateId($wilayah_template_id)
    {
        $this->wilayah_template_id = $wilayah_template_id;
    }

    /**
     * @return mixed
     */
    public function getCourierId()
    {
        return $this->courier_id;
    }

    /**
     * @param mixed $courier_id
     */
    public function setCourierId($courier_id)
    {
        $this->courier_id = $courier_id;
    }

    /**
     * @return mixed
     */
    public function getSharedCommision()
    {
        return $this->shared_commision;
    }

    /**
     * @param mixed $shared_commision
     */
    public function setSharedCommision($shared_commision)
    {
        $this->shared_commision = $shared_commision;
    }

    /**
     * @return mixed
     */
    public function getIncrementId()
    {
        return $this->increment_id;
    }

    /**
     * @param mixed $increment_id
     */
    public function setIncrementId($increment_id)
    {
        $this->increment_id = $increment_id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param mixed $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getDefaultSort()
    {
        return $this->default_sort;
    }

    /**
     * @param mixed $default_sort
     */
    public function setDefaultSort($default_sort)
    {
        $this->default_sort = $default_sort;
    }

    /**
     * @return mixed
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * @param mixed $popularity
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Product[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Product
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @todo : Refactoring, product, product_variant, etc that move to elastic, used elastic not DB
     * @param array $dataArray
     */
    public function setFromArray($dataArray = array())
    {
        foreach($dataArray as $key => $val) {

            if(is_string($val) == true){
                $val = str_replace("'","&apos;",$val);
            }

            if(property_exists($this, $key)) {
                $this->{$key} = $val;
                // input is send but value is empty, we still need to update that fields
                if($val === 0 || $val == "") {
                    $this->whiteList[] = $key;
                }
            }

            if($key == "supplier") {
                $supplier = new \Models\Supplier();
                $supplier->setFromArray($val);

                $this->supplier = $supplier;
                $this->supplier_id = $val['supplier_id'];
            }

            if($key == "brand") {
                $brand = new \Models\MasterBrand();
                $brand->setFromArray($val);

                $this->brand = $brand;
                $this->brand_id = $val['brand_id'];
            }

            if($key == "variant"){
                $variants = array();
                foreach ($dataArray['variant'] as $key => $val){
                    $sku = $key;

                    $variantArray = $val;
                    $variantArray['sku'] = $sku;
                    if(!empty($dataArray['product_id'])) {
                        $variantArray['product_id'] = $dataArray['product_id'];
                    }
                    $variants[] = $variantArray;
                }
                $this->productVariant = $variants;
            }

            if($key == "attribute") {
                $attributes = new \Models\Product\Attribute\Collection();
                if(!empty($dataArray['product_id'])) {
                    $attributes->setProductId($dataArray['product_id']);
                }
                $attributes->setFromArray($val);
                $this->productAttribute = $attributes;
            }

            if($key == "category") {
                $allCategories = array();
                $product2Categories = array();
                foreach($dataArray['category'] as $keyCat => $categoryList) {
                    $categories = array();
                    $category_id = 0;
                    foreach($categoryList as $category) {
                        $category_id = $category['category_id'];
                        $categoryObj = new \Models\ProductCategory();
                        $categoryObj->setFromArray($category);
                        $categories[] = $categoryObj;
                    }

                    $allCategories[] = new \Models\Product\Category\Collection($categories);

                    $isDefault = ($keyCat === (count($dataArray['category'])-1)) ? 1 : 0;
                    $product_id = empty($dataArray['product_id'])  ? 0 : $dataArray['product_id'];
                    $dataProduct2Category = array(
                        "product_id" => $product_id,
                        "category_id" => $category_id,
                        "is_default" => $isDefault
                    );
                    $product2CategoryObj = new \Models\Product2Category();
                    $product2CategoryObj->setFromArray($dataProduct2Category);
                    $product2Categories[] = $product2CategoryObj;
                }

                $this->productCategory = new \Models\Product\ProductCategoryCollection($allCategories);
                $this->product2Category = new \Models\Product\Product2CategoryCollection($product2Categories);

            }
        }
    }

    /**
     * @todo : Refactoring, product, product_variant, etc that move to elastic, used elastic not DB
     * @param array $columns
     * @param bool $showEmpty
     * @return array
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        if(!empty($view['tag_search']) && \Helpers\ParserHelper::isJSON($view['tag_search'])) {
            $view['tag_search'] = json_decode($view['tag_search']);
        }

        if(!empty($this->supplier)) {
            $view['supplier'] = $this->supplier->getDataArray();
        }

        if(!empty($this->brand)) {
            $view['brand'] = $this->brand->getDataArray();
        }

        if(!empty($this->productVariant)) {
            $view['variant'] = $this->productVariant->getDataArray();
        }

        if(!empty($this->productAttribute)) {
            $view['attribute'] = $this->productAttribute->getDataArray();
            //unset manufacture_no
            $found = \Helpers\GeneralHelper::in_array_r('mfr_no',$view['attribute'],false,true);
            unset($view['attribute'][$found['key']]);
            $view['attribute'] = array_values($view['attribute']);
        }

        if(!empty($this->productCategory)) {
            $view['category'] = $this->productCategory->getDataArray();
        } else if (!empty($this->product2Category)) {

            /**
             * Impact Refactoring Product 2 Category
             */
            $categorySorting = [];
            $categoryIsInStock = [];
            $breadCrumb = [];
            $category = [];

            $product2catModel = new \Models\Product2Category();
            $product2catData = $product2catModel->find([
                'conditions' => ' product_id = '.$this->product_id,
                'order' => ' is_default desc '
            ]);

            if($product2catData){

                $flagBreadCrumb = FALSE;
                foreach($product2catData->toArray() as $cat) {

                    $productCatModel = new \Models\ProductCategory();
                    $productCatData = $productCatModel->findFirst(' category_id = '.$cat['category_id'].' and status > 0 ');
                    if($productCatData){
                        if($cat['is_default'] == 1) {
                            $catId = str_replace('/',"','",$productCatData->toArray()['path']);

                            $categoryFound = $product2catModel->find(
                                [
                                    "columns" => "category_id",
                                    "conditions" => "category_id IN ('$catId') AND product_id =".$this->product_id
                                ]
                            );

                            if(!empty($categoryFound->toArray())){
                                $catId = implode("','",array_map('current',$categoryFound->toArray()));
                            }

                            $productCatExtractData = $productCatModel->find("category_id in ('$catId') and status > 0");

                            // check and unchecked product
                            if($productCatExtractData && !$flagBreadCrumb){
                                // TODO : no need to loop, get the first is_default only
                                $flagBreadCrumb = TRUE;
                                foreach($productCatExtractData->toArray() as $rowExtract){
                                    $routeData = \Helpers\RouteHelper::getRouteData($rowExtract['category_id'], "category");
                                    $breadCrumb[] = array(
                                        'category_id' => $rowExtract['category_id'],
                                        'name' => $rowExtract['name'],
                                        'url_key' => !empty($routeData['url_key'])? $routeData['url_key'] : ""
                                    );
                                }
                            }
                        }

                        $routeDataCategory = \Helpers\RouteHelper::getRouteData($cat['category_id'], "category");
                        if(!empty($routeDataCategory)){
                            $categorySorting[] = array(
                                'priority' => $cat['priority'],
                                'url_key' => $routeDataCategory['url_key']
                            );

                            $categoryIsInStock[] = array(
                                'category_id' => $cat['category_id'],
                                'url_key' => $routeDataCategory['url_key'],
                                'is_in_stock' => 1
                            );

                            $category[] = array(
                                'category_id' => $cat['category_id'],
                                'name' => $productCatData->toArray()['name'],
                                'url_key' => $routeDataCategory['url_key']
                            );
                        }
                        else{
                            $categorySorting[] = array(
                                'priority' => $cat['priority'],
                                'url_key' => ""
                            );

                            $categoryIsInStock[] = array(
                                'category_id' => $cat['category_id'],
                                'url_key' => "",
                                'is_in_stock' => 1
                            );

                            $category[] = array(
                                'category_id' => $cat['category_id'],
                                'name' => $productCatData->toArray()['name'],
                                'url_key' => ""
                            );
                        }

                    }

                }

            }
            $view['breadcrumb'] = $breadCrumb;
            $view['category_sorting'] = $categorySorting;
            $view['category_is_in_stock'] = $categoryIsInStock;
            $view['category'][0] = $category;

        }

        unset($view["created_at"]);
        unset($view["updated_at"]);
        unset($view["brand_id"]);
        unset($view["supplier_id"]);

        return $view;
    }

    public function getSimpleProductDataArray()
    {
        $view = $this->toArray(array('sku', 'name', 'url_key'), true);

        if(!empty($this->prices)) {
            $price = $this->prices->offsetGet(0);
            $view['price'] = $price->getPrice();
            $view['special_price'] = $price->getSpecialPrice();
        } else {
            return array();
        }

        if(!empty($this->images)) {
            $view['image_url'] = $this->images->offsetGet(0)->getImageUrl();
        }

        return $view;
    }

    /**
     * Get data from cache, for product cache will be using elastic
     */
    public function getProductDataFromCache()
    {
        $elasticLib = new \Library\Elastic();
        $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX') ? getenv('ELASTIC_PRODUCT_INDEX') : "products";
        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE') ? getenv('ELASTIC_PRODUCT_TYPE') : "product";

        $query = [
            'query' => [
                'query_string' => [
                    'query' => $this->url_key,
                    'fields' => ["url_key"]
                ]
            ]
        ];
        $elasticLib->body = $query;

        $elasticLib->search();
        $elasticLib->getFormatedResult();

        if($elasticLib->getTotalData() > 0) {
            $this->setFromArray($elasticLib->getFormatedResult());
        } else {
            \Helpers\LogHelper::log("product", "product not found with url-key : " . $this->url_key, "error");
            $this->errors[] = "Product not found";
        }

        return $elasticLib->getFormatedResult();

    }

    /**
     * Search all product data with inserted term by customer
     * @param string $term
     * @return mixed
     * @throws \Library\HTTPException
     */
    public function searchProduct($term = "", $query = "")
    {
        /**
         * in future we need to save the search term
         */

        $elasticLib = new \Library\Elastic();
        $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX') ? getenv('ELASTIC_PRODUCT_INDEX') : "productsvariant";
        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE') ? getenv('ELASTIC_PRODUCT_TYPE') : "product";

        if(empty($term) && empty($query)) {
            // Get all data with max data is 50 and random all product to show
            $query = [
                'from' => 0,
                'size' => 50,
                "query" => [
                    "query_string" => [
                        "query" => "*.*"
                    ]
                ]
            ];
        } else {
            if (empty($query)) {
                $query = [
                    'query' => [
                        'match_phrase' => $term

                    ]
                ];
            }
        }

        $elasticLib->body = $query;

        $elasticLib->search();

        if($elasticLib->getTotalData() == 0) {
            //\Helpers\LogHelper::log("product", "product not found with term : " . json_encode($term), "error");
            $this->errors[] = "Product not found";
        }

        if(!empty($elasticLib->getFormatedResult())) {
            return $elasticLib->getFormatedResult();
        } else {
            return array();
        }
    }


    public function searchProductFromElastic($sku = "")
    {
        /**
         * This is for multivariant if we wanted to get specific variant from an array of variant
         */

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/_search?pretty';
        // use only in local proxy

        $products = array();
        $client = new Client();

        // now query to collect sku(s) -> query via elasticsearch
        $body = '{
                      "_source": ["name","url_key","is_in_stock","variants","categories_vendor"],
                      "query": {
                        "nested": {
                          "path": "variants",
                          "query": {
                            "term": {
                                "variants.sku.faceted": "'.$sku.'"
                              }
                          }
                        }
                      }
                    }';

        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);

        if (!empty($result['hits']['hits'])) {
            foreach ($result['hits']['hits'] as $id => $value) {
                $products = ProductHelper::buildStructure(array_merge($value['_source'], array('_id' => $value['_id'])));
            }
        }

        return $products;
    }

    public function deleteFromCache($sku = null)
    {
        if(empty($sku)) {
            $this->errorCode = "RR100";
            $this->errorMessages[] = "SKU is required";
            return false;
        }

        $query = [
            'from' => 0,
            'size' => 50,
            "query" => [
                "bool" => [
                    "must" => [
                        "nested" => [
                            "path" => "variants",
                            "query" => [
                                "match" => [
                                    "variants.sku" => $sku
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $result = $this->searchProduct("",$query);
        if(!empty($result)){
            if (isset($result[0])) {
                $result = $result[0];
            }

            $elasticLib = new \Library\Elastic();
            $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX') ? getenv('ELASTIC_PRODUCT_INDEX') : "products";
            $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE') ? getenv('ELASTIC_PRODUCT_TYPE') : "product";
            $deleteStatus = $elasticLib->delete($result['product_id']);

            if(!$deleteStatus) {
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Failed when try saving data to cache";
                return false;
            }
        }


        return true;
    }

    /**
     * Get list of product by category from elastic
     *
     * @param array $category_url
     * @return mixed
     * @throws \Library\HTTPException
     */
    public function getProductList($category_url = array())
    {
        if(empty($category_url)) {
            $this->errors[] = "Please provide category url_key";
            return array();
        }

        $elasticLib = new \Library\Elastic();
        $elasticLib->index = getenv('ELASTIC_PRODUCT_INDEX') ? getenv('ELASTIC_PRODUCT_INDEX') : "products";
        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE') ? getenv('ELASTIC_PRODUCT_TYPE') : "product";

        $elasticParam = array();
        foreach($category_url as $url) {
            $elasticParam[] = ["match" => ["category.data.url_key" => $url]];
        }
        $query = [
            'query' => [
                'bool' => [
                    'must' => [
                        $elasticParam
                    ]
                ]
            ]
        ];

        $elasticLib->search($query);

        if($elasticLib->getTotalData() == 0) {
            \Helpers\LogHelper::log("product", "product not found with url-key : " . $this->url_key, "error");
            $this->errors[] = "Product not found";
        }

        return $elasticLib->getFormatedResult();
    }

    /**
     * This function is use for generate remarketing data
     * @return mixed
     * @throws \Library\HTTPException
     */
    public function getDataRemarketing()
    {
        $elasticLib = new \Library\Elastic();
        $elasticLib->index = getenv('ELASTIC_PRODUCT_VARIANT_INDEX') ? getenv('ELASTIC_PRODUCT_VARIANT_INDEX') : "productsvariants";
        $elasticLib->type = getenv('ELASTIC_PRODUCT_TYPE') ? getenv('ELASTIC_PRODUCT_TYPE') : "product";

        $query = [
            "_source" => [
                "variants",
                "name",
                "url_key",
                "categories.category_id",
                "categories.url_key",
                "brand.name",
                "description"
            ],
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "nested" => [
                                "path" => "variants",
                                "query" => [
                                    "match" => [
                                        "variants.status" => "10"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $elasticLib->body = $query;
        $parameter = [
            "scroll" => "30s",
            "size" => 5000
        ];
        $elasticLib->search($parameter);

        $productDataChunk = array();
        $response = $elasticLib->getRawResult();

        $i = 0;
        while (isset($response['hits']['hits']) && count($response['hits']['hits']) > 0) {
            if ($i == 0) {
                $productDataChunk[] = $elasticLib->getFormatedResult();
                $i++;
            }

            $scroll_id = $response['_scroll_id'];
            $elasticLib->scroll([
                    "scroll_id" => $scroll_id,  //...using our previously obtained _scroll_id
                    "scroll" => "30s"           // and the same timeout window
                ]
            );

            $response = $elasticLib->getRawResult(); // used for next looping

            $productDataChunk[] = $elasticLib->getFormatedResult();
        }

        $skuCollection = array();
        if ($productDataChunk > 0) {
            foreach ($productDataChunk as $subChunk) {
                foreach ($subChunk as $row) {
                    if (count($row['variants']) > 0) {
                        foreach ($row['variants'] as $rowVariant) {
                            $skuCollection[] = $rowVariant['sku'];
                        }
                    }
                }
            }
        }

        $in_string = implode("','", $skuCollection);
        $in_string = "'" . $in_string . "'";

        // Get stock collection by sku
        $sql = "SELECT sku, sum(qty) as total_qty from product_stock where sku in (".$in_string.") GROUP BY sku";
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        $newDataStock = array();
        if($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $dataStock = $result->fetchAll();

            foreach ($dataStock as $rowStock) {
                $newDataStock[$rowStock['sku']] = $rowStock['total_qty'];
            }
        }

        $productData = array();
        if (count($productDataChunk) > 0 && count($newDataStock) > 0) {
            $index = 0;
            $currentDate = date('Y-m-d H:i:s');
            $currentDate = date('Y-m-d H:i:s', strtotime($currentDate));

            foreach ($productDataChunk as $subChunk) {
                foreach ($subChunk as $row) {
                    foreach ($row['variants'] as $rowVariant) {
                        $sku = $rowVariant['sku'];
                        $productData[$index]['id'] = $rowVariant['sku'];
                        $productData[$index]['item_title'] = $row['name'];
                        $productData[$index]['final_url'] = $row['url_key'];
                        $productData[$index]['image_url'] = (isset($rowVariant['images'][0]['image_url'])) ? $rowVariant['images'][0]['image_url'] : '-';
                        $productData[$index]['price'] = $rowVariant['prices'][0]['price'];

                        $productData[$index]['sale_price'] = $rowVariant['prices'][0]['price'];
                        if (!empty($rowVariant['prices'][0]['special_from_date']) && !empty($rowVariant['prices'][0]['special_to_date'])) {
                            $fromDate = date('Y-m-d H:i:s', strtotime($rowVariant['prices'][0]['special_from_date']));
                            $toDate = date('Y-m-d H:i:s', strtotime($rowVariant['prices'][0]['special_to_date']));
                            if (($currentDate > $fromDate) && ($currentDate < $toDate)) {
                                $productData[$index]['sale_price'] = $rowVariant['prices'][0]['special_price'];
                            }
                        }

                        $productData[$index]['category_name'] = (isset($row['categories'][0]['url_key'])) ? $row['categories'][0]['url_key'] : '-';
                        $productData[$index]['category_id'] = (isset($row['categories'][0]['category_id'])) ? $row['categories'][0]['category_id'] : '-';

                        $productData[$index]['total_qty'] = 0;
                        if (isset($newDataStock[$sku])) {
                            $productData[$index]['total_qty'] = $newDataStock[$sku];
                        }

                        $productData[$index]['item_group_id'] = $row["_id"];
                        $productData[$index]['brand'] = (isset($row['brand']['name'])) ? $row['brand']['name'] : "-";
                        $productData[$index]['description'] = $row['description'];

                        $index++;
                    }
                }
            }
        } else {
            $response['data'] = array();
        }

        if (count($productData) > 0) {
            $response['data'] = $productData;
        } else {
            $response['data'] = array();
        }

        $response['messages'] = array("success");

        return $response;
    }

    /**
     * Check the sku by order_no, is the sku exist in sku_to_hold table?
     *
     * @param array $params Must contain order_no
     * @return string return the sku is in blacklist or not
     */
    public function isBlacklistSku($params = array())
    {
        try {
            $flagBlacklist = 'no';
            if (!empty($params['order_no'])) {
                $sql = "SELECT count(*) AS jml FROM sales_order_item
                        WHERE sales_order_id IN (SELECT sales_order_id FROM sales_order WHERE order_no = '{$params['order_no']}')
                        AND sku IN (SELECT sku FROM sku_to_hold WHERE now() BETWEEN from_date and to_date);";
                $this->useReadOnlyConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $hasil = $result->fetchAll();
                    if ($hasil[0]['jml'] > 0) {
                        $flagBlacklist = 'yes';
                    }
                }
            }
        } catch (\Exception $e) {
            $flagBlacklist = 'no';
        }

        return $flagBlacklist;
    }

    public function getDetailFromElastic($sku = FALSE, $paramSource = array())
    {

        if(empty($paramSource)){
            $this->errors[] = "Params source required";
            return false;
        }

        try {

            $elasticUrl = getenv('ELASTIC_HOST').':'.getenv('ELASTIC_PORT').
                '/'.getenv('ELASTIC_PRODUCT_INDEX').
                '/'.getenv('ELASTIC_PRODUCT_TYPE').
                '/_search';

            $paramsSourceArr = array();
            foreach($paramSource as $partialSource) {
                $paramsSourceArr[] = '"'.$partialSource.'"';
            }
            $paramsSourceJson = implode(",",$paramsSourceArr);

            $body = '
            {
                "_source":[
                            '.$paramsSourceJson.'
                          ],
                "query": {
                    "nested" : {
                        "path" : "variants",
                        "score_mode" : "avg",
                        "query" : {
                            "bool" : {
                                "must" :
                                    {
                                      "match" : {"variants.sku" : "'.$sku.'"}
                                    }
                            }
                        }
                    }
                }
            }
            ';

            // use only in local proxy
            //$client = new \GuzzleHttp\Client(['proxy' => 'http://192.168.1.37:22']);
            $client = new \GuzzleHttp\Client();
            $response = $client->post($elasticUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);

            $result = \GuzzleHttp\json_decode($response->getBody(), true);

            if($result['hits']['total'] > 0){
                return $result['hits']['hits'];
            }else{
                return FALSE;
            }

        }  catch (\Exception $e) {
            return false;
        }
    }
}
