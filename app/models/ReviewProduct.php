<?php

namespace Models;
use Phalcon\Db as Database;

class ReviewProduct extends \Models\BaseModel
{
    protected $review_product_id;
    protected $sku;
    protected $invoice_no;
    protected $admin_user_id;
    protected $rating;
    protected $description;
    protected $review_as;
    protected $customer_name;
    protected $is_follow_up;
    protected $is_blacklist;
    protected $is_new;
    protected $is_replied;
    protected $rating_initial;
    protected $status;

    public function initialize()
    {
        
    }
}