<?php

namespace Models;

class ScSellerReport extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id_report;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $file_name;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $report_type;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $supplier_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $start_periode;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $end_periode;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sc_seller_report';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ScSellerReport[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ScSellerReport
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
