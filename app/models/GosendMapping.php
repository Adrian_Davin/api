<?php
/**
 * Created by PhpStorm.
 * User: iwan
 * Date: 19/07/18
 * Time: 15:00
 */

namespace Models;


class GosendMapping extends \Models\BaseModel
{
    public $gosend_mapping_id;
    public $kecamatan_code;
    public $pickup_code;
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('pickup_code', 'Models\PickupPoint', 'pickup_code', array('alias' => 'PickupPoint', "reusable" => true));
    }

    public function getSource()
    {
        return 'gosend_mapping';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterColor[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterColor
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}