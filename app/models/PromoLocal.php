<?php

namespace Models;

class PromoLocal extends \Models\BaseModel
{

    /**
     *
     * @var string
     */
    protected $sku;

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku(string $sku)
    {
        $this->sku = $sku;
    }

    /**
     *
     * @var integer
     */
    protected $status_event;

    /**
     * @return int
     */
    public function getStatusEvent(): int
    {
        return $this->status_event;
    }

    /**
     * @param int $status_event
     */
    public function setStatusEvent(int $status_event)
    {
        $this->status_event = $status_event;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'promo_local';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminUserAttribute[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminUserAttribute
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function importProduct($params = array()){
        if(empty($params)){
            $this->errorCode = "RR001";
            $this->errorMessages = "Params cannot be empty.";
            return;
        }
        else{
            if(isset($params['sku'])){
                try{
                    $sql  = "TRUNCATE TABLE promo_local; INSERT INTO promo_local (sku) VALUES ";
                    $sql .= $params['sku'];
                    $this->useWriteConnection();
                    $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                }catch (\Exception $e){
                    $this->errorCode = "RR301";
                    $this->errorMessages = "Insert data failed. Please contact ruparupa tech team.";
                    return;
                }

            }
        }
    }

}
