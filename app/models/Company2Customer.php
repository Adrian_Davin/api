<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 2:06 PM
 */


namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class Company2Customer extends \Models\BaseModel
{
    protected $customer_id;
    protected $company_id;
    protected $status;
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }


    public function initialize()
    {
        $this->belongsTo('company_id', 'Models\Company', 'company_id', array('alias' => 'Company'));
        $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'company_2_customer';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        if(!empty($this->customer_id)) {
            $result = $this->Customer->toArray();
            $view['first_name'] = $result['first_name'];
            $view['last_name'] = $result['last_name'];
            $view['name'] = $result['first_name'].' '.$result['last_name'];
            $view['email'] = $result['email'];
        }

        return $view;
    }

    public function saveData($log_file_name = "error", $state = "")
    {
        try {
            if ($this->save() === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Failed when try to save data";

                if($transaction = $this->get()) {
                    $transaction->rollback(
                        "Cannot save data, rollback"
                    );

                    throw new \Exception("Rollback transaction");
                }

                return false;
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }
}