<?php

namespace Models;

class AceSendVoucher extends \Models\BaseModel
{

    /**
     *
     * @var int
     */
    protected $ace_send_voucher_id;
    /**
     *
     * @var int
     */
    protected $customer_id_sender;

    /**
     *
     * @var string
     */
    protected $email_receiver;

    /**
     *
     * @var string
     */
    protected $voucher_code;

    /**
     *
     * @var int
     */
    protected $voucher_type;

    /**
     *
     * @var int
     */
    protected $is_claim;

    /**
     *
     * @var datetime
     */
    protected $created_at;

    /**
     *
     * @var datetime
     */
    protected $updated_at;
    /**
     * Method to set the value of field template_code
     *
     * @param string $template_code
     * @return $this
     */
    public function setTemplateCode($template_code)
    {
        $this->template_code = $template_code;

        return $this;
    }

    /**
     * Method to set the value of field template_id
     *
     * @param string $template_id
     * @return $this
     */
    public function setTemplateId($template_id)
    {
        $this->template_id = $template_id;

        return $this;
    }

    /**
     * Method to set the value of field company_code
     *
     * @param string $company_code
     * @return $this
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;

        return $this;
    }

    /**
     * Returns the value of field template_code
     *
     * @return string
     */
    public function getTemplateCode()
    {
        return $this->template_code;
    }

    /**
     * Returns the value of field template_id
     *
     * @return string
     */
    public function getTemplateId()
    {
        return intVal($this->template_id);
    }

    /**
     * Returns the value of field company_code
     *
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ace_send_voucher';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterEmailTemplate[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterEmailTemplate
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function updateIsClaim($voucherCode = "", $emailReceiver = "")
    {
        $dateNow = date("Y-m-d H:i:s");
        $sql = "UPDATE ace_send_voucher SET is_claim = 10 , updated_at = '" . $dateNow . "' WHERE voucher_code = '" . $voucherCode . "' AND email_receiver = '" . $emailReceiver . "'";
        $result = $this->getDi()->getShared('dbMaster')->execute($sql);

        if (!$result) {
            \Helpers\LogHelper::log("ace_send_voucher", "cannot update is_claim #" . $voucherCode . " to " . $emailReceiver);

            return false;
        }

        return true;
    }
}
