<?php

namespace Models;

use Phalcon\Db as Database;

class CategoryStockMapping extends \Models\BaseModel
{

    protected $pickup_id;

    protected $category_id;

    protected $sku;

    protected $qty;

    protected $errorCode;

    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param mixed $qty
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }


    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('pickup_id', 'Models\Store', 'pickup_id', array('alias' => 'Store'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'category_stock_mapping';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CategoryStockMapping[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CategoryStockMapping
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function categoryStockMapping($params = array()){

        if(isset($params['sku'])){
            $result = $this->find("sku = '{$params['sku']}'");
        }
        else{
            $result = $this->find();
        }

        if(count($result) > 0){
            try {
                $sqlUpdateStock = 'UPDATE category_stock_mapping SET qty = CASE';
                $arr_sku = array();
                $arr_pickup_id = array();
                foreach ($result as $row) {
                    $qty = 0;
                    $categoryStockMappingResult = $row->toArray();
                    $stores = $row->Store;
                    foreach($stores as $store){
                        $storeResult = $store->toArray();
                        $productStockModel = new \Models\ProductStock();
                        $productStockResult = $productStockModel->find('sku = "'.$categoryStockMappingResult['sku'].'" and store_code = "'.$storeResult['store_code'].'"');
                        foreach($productStockResult as $productStock){
                            $productStockData = $productStock->toArray();
                            $qty += $productStockData['qty'];
                        }
                    }
                    $sqlUpdateStock .= " WHEN (pickup_id = '{$categoryStockMappingResult['pickup_id']}' AND sku = '{$categoryStockMappingResult['sku']}') THEN {$qty}";
                    if (!in_array("'{$categoryStockMappingResult['sku']}'", $arr_sku)){
                        $arr_sku[] = "'{$categoryStockMappingResult['sku']}'";
                    }
                    if (!in_array("'{$categoryStockMappingResult['pickup_id']}'", $arr_pickup_id)){
                        $arr_pickup_id[] = "'{$categoryStockMappingResult['pickup_id']}'";
                    }
                }
                $strSku = implode(',',$arr_sku);
                $strPickupId = implode(',',$arr_pickup_id);
                $sqlUpdateStock .= " END WHERE pickup_id in ($strPickupId) AND sku in ($strSku) ";

                $result = $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
                if (!$result) {
                    $this->errorCode = "RR301";
                    $this->errorMessages = 'Update category stock mapping failed, please contact ruparupa tech support'. $e->getMessage();
                }
            } catch (\Exception $e) {
                $this->errorCode = "RR301";
                $this->errorMessages = 'Update category stock mapping failed, please contact ruparupa tech support'. $e->getMessage();
            }
        }
    }
}
