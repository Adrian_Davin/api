<?php

namespace Models;

class SalesEmployeeItem extends \Models\BaseModel
{
    protected  $sales_employee_item_id;
    protected  $nip;
    protected  $name;
    protected  $store_code;
    protected  $sales_order_item_id;
    protected  $status;
    protected  $app_source;
    protected  $type;
    protected  $created_at;
    protected  $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sales_order_item_id', 'Models\SalesOrderItem', 'sales_order_item_id', array('alias' => 'SalesOrderItem'));   
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function setSalesOrderItemId($id) {
        $this->sales_order_item_id = $id;
    }

    public function getSalesOrderItemId() {
        return $this->sales_order_item_id;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getAppSource() {
        return $this->app_source;
    }

    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }

    public function setNip($nip) {
        $this->nip = $nip;
    }

    public function getNip(){
        return $this->nip;
    }

    public function setStoreCode($store_code){
        $this->store_code = $store_code;
    }

    public function getStoreCode(){
        return $this->store_code;
    }
}
