<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 2/9/2017
 * Time: 1:58 PM
 */

namespace Models;

use \MongoDB\BSON\ObjectID as MongoDbObjectId;
use \Helpers\Transaction as TransactionHelper;
use \Helpers\ProductHelper;
use \Helpers\CartHelper;
use \Library\APIWrapper;
use \Library\Cart as CartLib;

/**
 * Class CartItem
 * @package Library
 *
 */

class CartItem
{
    protected $id;
    protected $parent_id = 0;
    protected $sku;
    protected $name;
    protected $weight;
    protected $packaging_height;
    protected $packaging_width;
    protected $packaging_length;
    protected $packaging_uom;
    protected $qty_ordered = 1;
    protected $max_qty = 0;
    protected $normal_price = 0;
    protected $selling_price = 0;
    protected $commission = 0;
    protected $net_sales = 0;
    protected $price_modified;
    protected $modified_description;
    protected $discount_amount = 0;
    protected $shipping_assignation_rate = 0;
    protected $shipping_amount = 0;
    protected $shipping_discount_amount = 0;
    protected $carrier_id = 0;
    protected $group_shipment = 0;
    protected $handling_fee_adjust = 0;
    protected $zone_id;
    protected $gift_cards = '[]';
    protected $gift_cards_amount = 0;
    protected $subtotal = 0;
    protected $discount_subtotal = 0;
    protected $row_total = 0;
    protected $discount_row_total = 0;
    protected $delivery_method;
    protected $store_code;
    protected $supplier_alias;
    protected $supplier_alias_original;
    protected $pickup_code;
    protected $origin_shipping_region;
    protected $origin_shipping_district;
    protected $product_source;
    protected $primary_image_url;
    protected $url_key;
    protected $cart_rules;
    protected $gift_warping = 0;
    protected $gift_message = "";
    protected $service_date;
    protected $category = [];
    protected $event  = [];
    protected $is_have_child = false;
    protected $utm_parameter;
    protected $first_utm_parameter;

    protected $can_delivery = true;
    protected $support_express_courier = false;
    protected $can_delivery_gosend = false;
    protected $pickup_location_list = array();
    protected $is_free_item = 0;
    protected $is_gift_warp = false;
    protected $is_gift_message = false;
    protected $is_service_date = false;
    protected $status_preorder = false;
    protected $is_restricted_event = false;

    const CUSTOMSKU = 'RR00001';

    protected $attributes = [];
    protected $shipping_address = [];

    /**
     * @return SalesOrderAddress
     */
    public function getShippingAddress()
    {
        return $this->shipping_address;
    }

    /**
     * @param SalesOrderAddress $shipping_address
     */
    public function setShippingAddress($shipping_address)
    {
        $this->shipping_address = $shipping_address;
    }

    /**
     * @var \Models\Product
     */
    protected $product;

    /**
     * @var \Models\ProductVariant
     */
    protected $product_variant = false;

    protected $ownfleet_template_id = false;

    protected $errorCode;
    protected $errorMessages;
    protected $brand;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param int $parent_id
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getQtyOrdered()
    {
        return $this->qty_ordered;
    }

    /**
     * @param mixed $qty_ordered
     */
    public function setQtyOrdered($qty_ordered)
    {
        $this->qty_ordered = $qty_ordered;
    }

    /**
     * @return mixed
     */
    public function getGroupShipment()
    {
        return $this->group_shipment;
    }

    /**
     * @param mixed $group_shipment
     */
    public function setGroupShipment($group_shipment)
    {
        $this->group_shipment = $group_shipment;
    }

    /**
     * @return mixed
     */
    public function getNormalPrice()
    {
        return $this->normal_price;
    }

    /**
     * @param mixed $normal_price
     */
    public function setNormalPrice($normal_price)
    {
        $this->normal_price = $normal_price;
    }

    /**
     * @return mixed
     */
    public function getSellingPrice()
    {
        return $this->selling_price;
    }

    /**
     * @param mixed $selling_price
     */
    public function setSellingPrice($selling_price)
    {
        $this->selling_price = $selling_price;
    }

    /**
     * @return int
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * @param int $discount_amount
     */
    public function setDiscountAmount($discount_amount)
    {
        $this->discount_amount = $discount_amount;
    }


    /**
     * @return string
     */
    public function getPriceModifed()
    {
        return $this->price_modified;
    }

    /**
     * @param string $price_modified
     */
    public function setPriceModifed($price_modified)
    {
        $this->price_modified = $price_modified;
    }


    /**
     * @return string
     */
    public function getModifiedDescription()
    {
        return $this->modified_description;
    }

    /**
     * @param string $modified_description
     */
    public function setModifiedDescription($modified_description)
    {
        $this->modified_description = $modified_description;
    }

    /**
     * @return string
     */
    public function getGiftCards()
    {
        return $this->gift_cards;
    }

    /**
     * @param string $gift_cards
     */
    public function setGiftCards($gift_cards)
    {
        $this->gift_cards = $gift_cards;
    }

    /**
     * @return int
     */
    public function getGiftCardsAmount()
    {
        return $this->gift_cards_amount;
    }

    /**
     * @param int $gift_cards_amount
     */
    public function setGiftCardsAmount($gift_cards_amount)
    {
        $this->gift_cards_amount = $gift_cards_amount;
    }

    /**
     * @return int
     */
    public function getDiscountSubtotal()
    {
        return $this->discount_subtotal;
    }

    /**
     * @param int $discount_subtotal
     */
    public function setDiscountSubtotal($discount_subtotal)
    {
        $this->discount_subtotal = $discount_subtotal;
    }

    /**
     * @return int
     */
    public function getDiscountRowTotal()
    {
        return $this->discount_row_total;
    }

    /**
     * @param int $discount_row_total
     */
    public function setDiscountRowTotal($discount_row_total)
    {
        $this->discount_row_total = $discount_row_total;
    }

    /**
     * @return int
     */
    public function getShippingAssignationRate()
    {
        return $this->shipping_assignation_rate;
    }

    /**
     * @param int $shipping_assignation_rate
     */
    public function setShippingAssignationRate($shipping_assignation_rate)
    {
        $this->shipping_assignation_rate = $shipping_assignation_rate;
    }

    /**
     * @return int
     */
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

    /**
     * @param int $shipping_amount
     */
    public function setShippingAmount($shipping_amount)
    {
        $this->shipping_amount = $shipping_amount;
    }

    /**
     * @return int
     */
    public function getCarrierId()
    {
        return $this->carrier_id;
    }

    /**
     * @param int $carrier_id
     */
    public function setCarrierId($carrier_id)
    {
        $this->carrier_id = $carrier_id;
    }

    /**
     * @return int
     */
    public function getShippingDiscountAmount()
    {
        return $this->shipping_discount_amount;
    }

    /**
     * @param int $shipping_discount_amount
     */
    public function setShippingDiscountAmount($shipping_discount_amount)
    {
        $this->shipping_discount_amount = $shipping_discount_amount;
    }

    /**
     * @return int
     */
    public function getHandlingFeeAdjust()
    {
        return $this->handling_fee_adjust;
    }

    /**
     * @param int $handling_fee_adjust
     */
    public function setHandlingFeeAdjust($handling_fee_adjust)
    {
        $this->handling_fee_adjust = $handling_fee_adjust;
    }

    /**
     * @return mixed
     */
    public function getZoneId()
    {
        return $this->zone_id;
    }

    /**
     * @param mixed $zone_id
     */
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
    }

    /**
     * @return mixed
     */
    public function getStoreCode()
    {
        return $this->store_code;
    }

    /**
     * @param mixed $store_code
     */
    public function setStoreCode($store_code)
    {
        $this->store_code = $store_code;
    }

    /**
     * @return mixed
     */
    public function getSupplierAliasOriginal()
    {
        return $this->supplier_alias_original;
    }

    /**
     * @param mixed $supplier_alias_original
     */
    public function setSupplierAliasOriginal($supplier_alias_original)
    {
        $this->supplier_alias_original = $supplier_alias_original;
    }

    /**
     * @return mixed
     */
    public function getPickupCode()
    {
        return $this->pickup_code;
    }

    /**
     * @param mixed $pickup_code
     */
    public function setPickupCode($pickup_code)
    {
        $this->pickup_code = $pickup_code;
    }

    /**
     * @return array
     */
    public function getPickupLocationList()
    {
        return $this->pickup_location_list;
    }

    /**
     * @param array $pickup_location_list
     */
    public function setPickupLocationList($pickup_location_list)
    {
        $this->pickup_location_list = $pickup_location_list;
    }

    /**
     * @return bool
     */
    public function getIsFreeItem()
    {
        return $this->is_free_item;
    }

    /**
     * @param bool $is_free_item
     */
    public function setIsFreeItem($is_free_item)
    {
        $this->is_free_item = $is_free_item;
    }

    /**
     * @return int
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param int $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return int
     */
    public function getRowTotal()
    {
        return $this->row_total;
    }

    /**
     * @param int $row_total
     */
    public function setRowTotal($row_total)
    {
        $this->row_total = $row_total;
    }

    /**
     * @return mixed
     */
    public function getSupplierAlias()
    {
        return $this->supplier_alias;
    }

    /**
     * @param mixed $supplier_alias
     */
    public function setSupplierAlias($supplier_alias)
    {
        $this->supplier_alias = $supplier_alias;
    }

    /**
     * @return mixed
     */
    public function getOriginShippingRegion()
    {
        return $this->origin_shipping_region;
    }

    /**
     * @param mixed $origin_shipping_region
     */
    public function setOriginShippingRegion($origin_shipping_region)
    {
        $this->origin_shipping_region = $origin_shipping_region;
    }

    /**
     * @return mixed
     */
    public function getOriginShippingDistrict()
    {
        return $this->origin_shipping_district;
    }

    /**
     * @param mixed $origin_shipping_district
     */
    public function setOriginShippingDistrict($origin_shipping_district)
    {
        $this->origin_shipping_district = $origin_shipping_district;
    }

    /**
     * @return mixed
     */
    public function getProductSource()
    {
        return $this->product_source;
    }

    /**
     * @param mixed $product_source
     */
    public function setProductSource($product_source)
    {
        $this->product_source = $product_source;
    }

    /**
     * @return mixed
     */
    public function getDeliveryMethod()
    {
        return $this->delivery_method;
    }

    /**
     * @param mixed $delivery_method
     */
    public function setDeliveryMethod($delivery_method)
    {
        $this->delivery_method = $delivery_method;
    }

    /**
     * @return bool
     */
    public function isIsHaveChild()
    {
        return $this->is_have_child;
    }

    /**
     * @param bool $is_have_child
     */
    public function setIsHaveChild(bool $is_have_child)
    {
        $this->is_have_child = $is_have_child;
    }

    /**
     * @return mixed
     */
    public function getCartRules()
    {
        if(empty($this->cart_rules)) {
            return array();
        } else {
            if(is_array($this->cart_rules)) {
                return $this->cart_rules;
            } else {
                return json_decode($this->cart_rules,true);
            }
        }
    }

    /**
     * @param mixed $cart_rules
     */
    public function setCartRules($cart_rules = array())
    {
        $this->cart_rules = json_encode($cart_rules);
    }

    /**
     * @param bool $can_delivery
     */
    public function setCanDelivery($can_delivery)
    {
        $this->can_delivery = $can_delivery;
    }

    /**
     * @return bool
     */
    public function isCanDeliveryGosend()
    {
        return $this->can_delivery_gosend;
    }

    /**
     * @param bool $can_delivery_gosend
     */
    public function setCanDeliveryGosend($can_delivery_gosend)
    {
        $this->can_delivery_gosend = $can_delivery_gosend;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getPackagingHeight()
    {
        return $this->packaging_height;
    }

    /**
     * @param mixed $packaging_height
     */
    public function setPackagingHeight($packaging_height)
    {
        $this->packaging_height = $packaging_height;
    }

    /**
     * @return mixed
     */
    public function getPackagingWidth()
    {
        return $this->packaging_width;
    }

    /**
     * @param mixed $packaging_width
     */
    public function setPackagingWidth($packaging_width)
    {
        $this->packaging_width = $packaging_width;
    }

    /**
     * @return mixed
     */
    public function getPackagingLength()
    {
        return $this->packaging_length;
    }

    /**
     * @param mixed $packaging_length
     */
    public function setPackagingLength($packaging_length)
    {
        $this->packaging_length = $packaging_length;
    }

    /**
     * @return mixed
     */
    public function getPackagingUom()
    {
        return $this->packaging_uom;
    }

    /**
     * @param mixed $packaging_uom
     */
    public function setPackagingUom($packaging_uom)
    {
        $this->packaging_uom = $packaging_uom;
    }

    /**
     * @return boolean
     */
    public function isOwnfleetTemplateId()
    {
        return $this->ownfleet_template_id;
    }

    /**
     * @param boolean $ownfleet_template_id
     */
    public function setOwnfleetTemplateId($ownfleet_template_id)
    {
        $this->ownfleet_template_id = $ownfleet_template_id;
    }

    /**
     * @return bool
     */
    public function isCanDelivery()
    {
        return $this->can_delivery;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getValue($key = "")
    {
        if(isset($this->{$key})) {
            return $this->{$key};
        } else {
            return "";
        }
    }

    public function setValue($key = "", $value = "")
    {
        if(property_exists($this,$key)) {
            $this->{$key} = $value;
        }
    }

    /**
     * @return bool
     */
    public function isRestrictedEvent()
    {
        return $this->is_restricted_event;
    }

    /**
     * @param bool $is_restricted_event
     */
    public function setIsRestrictedEvent($is_restricted_event)
    {
        $this->is_restricted_event = $is_restricted_event;
    }

    public function isBelongToCategory($category_id = 0)
    {
        if(empty($this->category)) {
            return false;
        }

        foreach ($this->category as $category) {
            if($category['category_id'] == $category_id) {
                return true;
            }
        }
    }

    public function isBelongToEvent($event_id = 0)
    {
        if(empty($this->event)) {
            return false;
        }

        foreach ($this->event as $event) {
            if($event['event_id'] == $event_id) {
                return true;
            }
        }
    }

    public function initialize($data_array = [])
    {
        $this->assignAllValue($data_array);
    }

    public function assignAllValue($data_array) { 
        foreach($data_array as $key => $value) { 
            if (is_array($value)) { 
                $this->assignAllValue($value); 
                 
            } else if(property_exists($this, $key)) { 
                $this->{$key} = $value; 
            } 
        } 
    } 

    public function setFromArray($data_array = array())
    {
        $sourceOrder = !empty($data_array['source_order'])? $data_array['source_order'] : "";
        unset($data_array['source_order']);

        foreach($data_array as $key => $val) {
            if($key == "qty_ordered" && $val <= 0) {
                $this->errorCode = "RR206";
                $this->sku = $data_array['sku'];
                $this->errorMessages = "Jumlah Kuantiti harus lebih besar dari 0";
                return false;
            } else if($key == "qty_ordered") {
                $this->qty_ordered = (int)$val; // Force as integer
            } else {
                $this->{$key} = $val;
            }
        }


        /*
         * get commission
         */
        $now = time();
        $productCommissionModel = new \Models\ProductCommission();
        $dataFlagCommission = $productCommissionModel->findFirst(
            array(
                'conditions' => ' sku = "'.$this->sku.'" '
            )
        );
        $dataCommission = array();

        $this->commission = 0;
        if(!empty($dataFlagCommission)) {
            $dataCommission = $dataFlagCommission->toArray();
            if(strtotime($dataCommission['special_from_date']) <= $now && strtotime($dataCommission['special_to_date'])>= $now){
                $this->commission = $dataCommission['special_commission'];
            }else{
                $this->commission = $dataCommission['commission'];
            }
        }


        /*
         * get net sales
         */
        $now = time();
        $productNetSalesModel = new \Models\ProductNetSales();
        $dataFlagNetSales = $productNetSalesModel->findFirst(
            array(
                'conditions' => ' sku = "'.$this->sku.'" '
            )
        );
        $dataNetSales = array();
        $this->net_sales = 0;
        if(!empty($dataFlagNetSales)) {
            $dataNetSales = $dataFlagNetSales->toArray();
            if(strtotime($dataNetSales['special_from_date']) <= $now && strtotime($dataNetSales['special_to_date'])>= $now){
                $this->net_sales = $dataNetSales['special_net_sales'];
            }else{
                $this->net_sales = $dataNetSales['net_sales'];
            }
        }

        $this->sku = trim($this->sku); // make sure we have no space in sku

        /**
         * Add additional product detail information (i.e. packaging weight, width, etc.)
         */
        $this->addProductDetail();

        /**
         * get product price from db, and set to CartItem price properties
         */
        if($sourceOrder=='lazada') {
            $this->getProductPriceLazada();
            // @todo: improve this dirty code
        } else if ($this->sku != self::CUSTOMSKU) {
                $this->getProductPrice();
        }


        if(!empty($this->errorMessages)) {
            return false;
        }

        /**
         * If this product is have an event, reset qty to 1
         * @todo : Need to do it in proper way
         */
        if(!empty($this->event)) {
            $this->qty_ordered = 1;
        }

        // Reset this item, we will calculate it later
        if($this->is_free_item == 0) {
            // If free item leave it
            $this->discount_amount = 0;
        }

        if($sourceOrder != 'lazada') {
            $this->shipping_amount = 0;
            $this->shipping_discount_amount = 0;
        }

        /**
         * Create a cart_id
         */
        if(empty($this->id)) {
            $mongoObjectId = new MongoDbObjectId();
            $this->id = (string)$mongoObjectId;
        }

        if($this->is_free_item == 1) {
            $this->handling_fee_adjust = 0;
        }

        /** final item price subtotal */
        $this->subtotal = TransactionHelper::countSubTotalItem($this->selling_price,$this->handling_fee_adjust,$this->discount_amount);
        /** final row total (qty * item price) row_total */
        $this->row_total = TransactionHelper::countTotalItem($this->subtotal, $this->qty_ordered);

        // Set First UTM Parameter
        if(empty($this->first_utm_parameter) || $this->first_utm_parameter == "" || $this->first_utm_parameter == null){
            $this->first_utm_parameter = str_replace('\\','',$this->utm_parameter);
        }
        $this->utm_parameter = str_replace('\\','',$this->utm_parameter);

        return true;
    }

    public function addProductDetail()
    {
        /** @var array $productDetail */
        // get product detail
        $productDetail = ProductHelper::getFlatProductData($this->sku,true,true);

        /**
         * Cleanup Event, we just need to know what event this product belong to
         */

        $eventSorted = [];
        if(!empty($productDetail['event'])) {
            foreach ($productDetail['event'] as $event) {
                $eventSorted[$event['event_id']] = $event;
            }

            $productDetail['event'] = array_values($eventSorted);
        } else {
            $productDetail['event'] = $eventSorted;
        }

        // assign product detail to CartItem properties
        if(!empty($productDetail)) {
            unset($productDetail['qty_ordered']);
            if ($this->sku != self::CUSTOMSKU) {
                unset($productDetail['price']);
                unset($productDetail['special_price']);
            }
            foreach($productDetail as $key => $val) {
                if (in_array($key,['price', 'special_price', 'normal_price', 'selling_price', 'name'])
                    && $this->sku == self::CUSTOMSKU) {
                    continue;
                } else {
                    $this->{$key} = $val;
                }

            }
        }

        $this->supplier_alias_original = $this->supplier_alias;

        // Check for gift_wraping, service_date, etc
        // @todo: Ask @Iman Suhardiman vtr
        $skuCampaignModel = new VtrSkuCampaign();
        $campaignResult = $skuCampaignModel->find(['sku' => $this->sku]);

        if(!empty($campaignResult->count())) {
            foreach ($campaignResult as $campaign) {
                switch ($campaign->getCampaignName()) {
                    case 'cleanandcare' : $this->is_service_date = true;break;
                    case 'giftwrapping' : $this->is_gift_warp = true;break;
                    case 'giftmessage' : $this->is_gift_message = true;break;
                }
            }
        }

        $preOrder = new \Models\ProductVariant();
        $this->status_preorder = $preOrder->preorderStatus($this->sku);

    }

    public function countSubTotalWithoutHandling()
    {
        $subtotal = ($this->selling_price - $this->discount_amount);
        return $subtotal;
    }

    public function countRowTotalWithoutHandling()
    {
        return ($this->countSubTotalWithoutHandling() * $this->qty_ordered);
    }

    public function countSubTotal()
    {
        $this->subtotal = TransactionHelper::countSubTotalItem($this->selling_price,$this->handling_fee_adjust,$this->discount_amount);
    }

    public function countRowTotal()
    {
        $this->row_total = TransactionHelper::countTotalItem($this->subtotal, $this->qty_ordered);
    }

    public function getDataArray()
    {
        $thisArray = get_object_vars($this);
        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {

            if(gettype($thisArray[$key]) == 'object' && $key != 'product' && $key != 'product_variant') {
                $view[$key] = $thisArray[$key]->getDataArray();
            } else if(is_scalar($val) || is_array($val) || $val === 0 || $val == "") {
                $view[$key] = $val;
            }
        }

        unset($view['errorCode']);
        unset($view['errorMessages']);

        return $view;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function getProductPriceLazada(){
        $productVariantModel = new ProductVariant();
        $productVariant = $productVariantModel->findFirst("sku = '" . $this->sku ."'");
        if($productVariant) {
            /**
             * @todo: refactor this
             * @AndyAntonius
             */
            $this->product_variant = $productVariant;
        } else {
            $this->errorCode = "RR206";
            $this->errorMessages = "Stok tidak cukup atau sedang kosong";
        }
    }

    /**
     * @todo: refactor function to directly query to product price by sku
     */
    public function getProductPrice()
    {
        $productPriceModel = new ProductPrice();
        $productPrice = $productPriceModel->find("sku = '" . trim($this->sku) ."'");
        if($productPrice) {
            $productPrices = $productPrice->toArray();
            if ($productPrices) {
                $productPrice = $productPrices[0];

                $this->normal_price = $this->selling_price = (int)$productPrice['price'];
                $this->price_modified = $productPrice['last_modified'];
                $this->modified_description = $productPrice['modified_description'];
                $today = date("Y-m-d H:i:s");
                if( $productPrice['special_from_date'] < $today &&
                    $today < $productPrice['special_to_date'] &&
                    (int)$productPrice['special_price'] > 0 &&
                    (int)$productPrice['special_price'] <= (int)$productPrice['price']
                ) {
                    $this->selling_price = (int)$productPrice['special_price'];
                }
            } else {
                $this->errorCode = "RR206";
                $this->errorMessages = "Stok tidak cukup atau sedang kosong (price issue)";
            }
        } else {
            $this->errorCode = "RR206";
            $this->errorMessages = "Stok tidak cukup atau sedang kosong (price issue)";
        }
    }

    public function checkRestrictedEvent($customer_id = 0, $company_code = "ODI")
    {
        if (!empty($this->event) && !empty($customer_id)) {
            foreach ($this->event as $event) {
                $modelOrderItem = new \Models\SalesOrderItem();
                $sql = "                                       
                    SELECT * FROM sales_order_item as item LEFT JOIN sales_order ON item.sales_order_id = sales_order.sales_order_id 
                    WHERE sku = '".$this->sku."' AND item.created_at >= '".$event['start_date']."'
                    AND item.created_at <= '".$event['end_date']."'  AND sales_order.customer_id = ".$customer_id."  
                    AND sales_order.company_code = '". $company_code ."'          
                ";

                $modelOrderItem->useReadOnlyConnection();
                $result = $modelOrderItem->getDi()->getShared($modelOrderItem->getConnection())->query($sql);

                if ($result->numRows() > 0) {
                    $this->is_restricted_event = true;
                    $this->can_delivery = false;
                    $this->pickup_location_list = [];
                } else {
                    $this->is_restricted_event = false;
                }
            }
        }

    }

    /**
     * This function trigger to checking stock and quote item assignation
     * making quote item as MP or store fullfillment if stock in DC is empty
     *
     * @param string $kecamatan_code
     * @param string $company_code
     * @return bool
     */
    public function checkStockAndAssignation($kecamatan_code = "", $company_code = 'ODI', $customer = null, $carrier_id = null)
    {
        // get available stock according to sku
        $stockLocationList = $this->getStoreWithStock($company_code);
        
        // by default we send the products from dc
        if(empty($this->delivery_method)){
            $this->delivery_method = "delivery";
        }
        
        // If this product have stock then ...
        if(count($stockLocationList) > 0) {
            if($this->delivery_method == "delivery" || $this->delivery_method == "ownfleet" || $this->delivery_method == 'store_fulfillment') {
                if(!empty($kecamatan_code)) {
                    // if it's not marketplace product
                    if(strtolower($this->product_source) != "marketplace") {
                        $this->shippingAssignation($kecamatan_code,$stockLocationList, $company_code, $carrier_id);
                    } else {
                        $this->marketplaceShippingAssignation($kecamatan_code,$stockLocationList, $customer);
                    }
                } else {
                    $this->can_delivery = true;
                }
            } else if( $this->delivery_method == "pickup" ) {
                $this->pickupAssignation($stockLocationList, $kecamatan_code);
            }


            $this->max_qty = 0;
            foreach ($stockLocationList as $stockList) {
                $storeInformation = $stockList[0];

                if($this->max_qty < $storeInformation['qty']) {
                    $this->max_qty = (int)$storeInformation['qty'];
                }
            }

            // Setup pickup location list
            if(strtolower($this->product_source) == "marketplace") {
                $this->pickup_location_list = array();
            } else {
                $this->preparePickupLocationList($stockLocationList);
            }

            // Check gosend can delivery
            if ($this->can_delivery) {
                $this->canDeliveryGosend($kecamatan_code, $stockLocationList);
            }

            return true;
        } else {
            $this->can_delivery = false;
            $this->can_delivery_gosend = false;
            $this->pickup_location_list = [];
            $this->delivery_method = "delivery";
            $this->pickup_code = "";
            $this->store_code = "";
            $this->origin_shipping_region = "";
            $this->origin_shipping_district = "";

            /**
             * Stock for particular CartItem is not found anywhere in the DC / store / marketplace
             */
            $this->errorCode = "RR203";
            $this->errorMessages = "Stok tidak cukup atau sedang kosong";
            return false;
        }
    }

    /**
     * This function is to validate the stock according to its store_code
     *
     * @param string $company_code
     * @return bool
     */
    public function checkStockValidate($company_code = 'ODI')
    {
        // get available stock according to sku and store_code
        $stockLocationList = $this->getStoreWithStock($company_code, $this->store_code);

        // If this product doesn't have stock on its store then ...
        if(count($stockLocationList) == 0) {
            $this->can_delivery = false;
            $this->can_delivery_gosend = false;
            $this->pickup_location_list = [];
            $this->delivery_method = "delivery";
            $this->pickup_code = "";
            $this->store_code = "";
            $this->origin_shipping_region = "";
            $this->origin_shipping_district = "";

            /**
             * Stock for particular CartItem is not found on it's store_code
             */
            $this->errorCode = "RR203";
            $this->errorMessages = "Stok tidak cukup atau sedang kosong";
            return false;
        }
    }

    /*
     * get all pickup_code that support gosend shipment based on kecamatan, get storecode from it
     * using storecode that we get, compare with stocklocationlist that we know all that list is have available stock
     * if one of them is match we assume that we can send it with gosend
     * */
    public function canDeliveryGosend ($kecamatan_code, $stockLocationList) {
        // we store storecode to make easy check for available stock
        $temp_store_code = array();
        foreach($stockLocationList as $stockLocation) {
            foreach($stockLocation as $stock) {
                if ($stock['qty'] > 0) {
                    $temp_store_code[] = $stock['store_code'];
                }
            }
        }

        $gosendMappingModelSection = new \Models\GosendMapping();
        $gosendMappingData = $gosendMappingModelSection->find('kecamatan_code = "'.$kecamatan_code.'"');
        foreach($gosendMappingData as $gosendData) {
            if (!$this->can_delivery_gosend) {
                $gosendMappingModel = new \Models\GosendMapping();
                $gosendMapping = $gosendMappingModel->findFirst('gosend_mapping_id = ' . $gosendData->toArray()['gosend_mapping_id']);
                if ($gosendMapping && $gosendMapping->PickupPoint) {
                    $pickupPoint = $gosendMapping->PickupPoint;
                    if ($pickupPoint && $pickupPoint->Store) {
                        $storeData = $pickupPoint->Store->toArray();
                        foreach ($storeData as $store) {
                            $storeCode = $store['store_code'];
                            if (in_array($storeCode, $temp_store_code)) {
                                $this->can_delivery_gosend = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public function pickupAssignation($stockLocationList = [],$kecamatan_code = "",$company_code = 'ODI')
    {
        // Check if in this location have stock
        $is_have_stock = false;

        if(!empty($this->store_code)) {
            $storeModel = new Store();
            $storeData = $storeModel::findFirst(["store_code = '".$this->store_code."'"]);
            if ($storeData) {
                $this->pickup_code = $storeData->PickupPoint->pickup_code;
            }
        }

        if(!empty($this->pickup_code)) {
            if(!empty($stockLocationList[$this->pickup_code])) {
                foreach ($stockLocationList[$this->pickup_code] as $storeData) {
                    // for custom order that have stock in more than 1 store in 1 pickup point
                    if ($this->store_code) {
                        if ($storeData['store_code'] == $this->store_code) {
                            $is_have_stock = true;
                            $this->store_code = $storeData['store_code'];
                            $this->supplier_alias = $storeData['supplier_alias'];
                            $this->zone_id = $storeData['zone_id'];
                            $this->origin_shipping_region = $storeData['origin_shipping_region'];
                            break;
                        }
                    }

                    /**
                     * Get store data only for store choose by customer
                     */
                    $is_have_stock = true;
                    $this->store_code = $storeData['store_code'];
                    $this->supplier_alias = $storeData['supplier_alias'];
                    $this->zone_id = $storeData['zone_id'];
                    $this->origin_shipping_region = $storeData['origin_shipping_region'];
                }
                
                if ($this->zone_id > 1 && $this->is_free_item == 0) {
                    $this->countHandlingFeeAdjust();
                }
            }
        } else if (!empty($stockLocationList)){
            /**
             * If pickup code is empty, customer have not yet choose his pickup location
             * and if we have stock (not empty stock location list) then we have stock
             * by default we set it as delivery
             */
            $is_have_stock = true;
            //$this->can_delivery = true;
            //$this->delivery_method = "delivery";
        }

        /**
         * failed to assign stock for pickup
         */
        if(!$is_have_stock) {
            $this->errorCode = "RR203";
            $this->errorMessages = "Stok tidak tersedia pada lokasi pickup";
            $this->can_delivery = false;
        }

        // Check if can deliver from this pickup_point
        $shippingAssignationList = $this->getShippingAssignationList($kecamatan_code, $company_code);
        foreach($shippingAssignationList as $shippingAssignation) {
            if(isset($stockLocationList[$shippingAssignation['pickup_code']]) && $shippingAssignation['status'] == 10) {
                $this->can_delivery = true;
                break;
            } else {
                $this->can_delivery = false;
            }
        }
    }

    public function preparePickupLocationList($stockLocationList = [])
    {
        $this->pickup_location_list = array();
        foreach ($stockLocationList as $pickup_code => $storesInformation) {
            // Remove DC from pickup_point
            if (strtoupper(substr($pickup_code,0,2)) == 'DC') {
                unset($stockLocationList[$pickup_code]);
                continue;
            }

            foreach ($storesInformation as $storeInformation) {
                $storeInformation['city']['city_name'] = isset($storeInformation['city']['city_name']) ? $storeInformation['city']['city_name'] : "";
                $storeInformation['pickup_data'] = isset($storeInformation['pickup_data']) ? $storeInformation['pickup_data'] : array();
                $pickup = [
                    "pickup_code" => $pickup_code,
                    "pickup_location" => empty($storeInformation['city']['city_name']) ? '' : $storeInformation['city']['city_name'],
                    "store_code" => $storeInformation['store_code']
                ];

                if (isset($storeInformation['pickup_data']['is_express_courier']) && !empty($storeInformation['pickup_data']['is_express_courier'])) {
                    if ($storeInformation['pickup_data']['is_express_courier'] == '1') {
                        $this->support_express_courier = true;
                    }
                }

                $this->pickup_location_list[] = array_merge($pickup, $storeInformation['pickup_data']);
            }
        }
    }

    public function assignLazadaItem()
    {
        $this->supplier_alias = "RR";
        $this->store_code = "DC";
        $this->supplier_alias = "RR";
        $this->delivery_method = "delivery";
        $this->pickup_code = "";
        $this->origin_shipping_region = "ID-JK";
        $this->origin_shipping_district = "BKI10100CIKARANG";
        $this->zone_id = 1;
    }

    /**
     * Get list of stores which has available stock of particular SKU (CartItem->sku)
     * @param string $company_code
     * @return array
     */
    public function getStoreWithStock($company_code = 'ODI', $store_code = null, $on_hand_only = null, $business_unit, $source = null)
    {
        $storeWithStockData= [];
        
        // Load shopping card data
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));

        $queryParams = "&company_code=".strtolower($company_code);
        if ($store_code) {
            $queryParams .= "&store_code=".$store_code;
        }
        if ($on_hand_only) {
            $queryParams .= "&on_hand_only=".$on_hand_only;
        }
        if ($business_unit) {
            $queryParams .= "&business_unit=".$business_unit;
        }
        if (!empty($source)) {
            $queryParams .= "&source=".$source;
        }

        $apiWrapper->setEndPoint("legacy/stock/check_available/".$this->sku."?qty=".(int)$this->qty_ordered. $queryParams);
        
        if($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
        } else {
           return $storeWithStockData;
        }

        if (!empty($apiWrapper->getError())) {
            return $storeWithStockData;
        }

        // maybe cart is found, but data is empty
        $productStockData = $apiWrapper->getData();
        if (!empty($productStockData['max_qty']) && $productStockData['max_qty'] > 0) {
            /**
             * Check qty and get pickup location store information (DC & store)
             */
            $storeWithStockData = ProductHelper::completeStoreInformation($productStockData['stock_list']);
        }

        return $storeWithStockData;
    }

    /**
     * check stock for this item in selected store
     */
    public function checkStockItem($company_code = "ODI")
    {
        if(empty($this->sku) || empty($this->store_code)) {
            $this->errorCode = "RR204";
            $this->errorMessages = "Item not found.";
            return;
        }

        $stockInfo = ProductHelper::checkStockItem($this->sku,$this->store_code,$this->qty_ordered,$company_code);
        if($stockInfo[0] == false) {
            $this->errorCode = "RR203";
            $this->errorMessages = "Stok tidak cukup atau sedang kosong";
        }
    }

    public function getShippingAssignationList($kecamatan_code, $company_code = 'ODI') {
        $shippingAssignationList = array();
        $apiWrapper = new APIWrapper(getenv('SHIPMENT_API'));
        $apiWrapper->setEndPoint("shipping/assignation?destination_kecamatan_id=" . $kecamatan_code . "&company_code=" . $company_code);
        if($apiWrapper->send("get")) {
            $apiWrapper->formatResponse();
            $resultData = $apiWrapper->getData();
            $shippingAssignationList = $resultData['assignations'];
        } else {
            $this->errorCode = "RR203";
            $this->errorMessages = "Get shipping assignation failed";
            return;
        }
        return $shippingAssignationList;
    }

    /**
     * @param string $kecamatan_code
     * @param array $stockLocationList
     * $stockLocationList array map format : $stockLocationList[pickup_location][store_list]
     */
    public function shippingAssignation($kecamatan_code = "", $stockLocationList = array(), $company_code = 'ODI', $carrier_id = null)
    {
        if(empty($kecamatan_code)) {
            $this->errorCode = "RR203";
            $this->errorMessages = "Produk tidak bisa dikirim ke alamat anda, silahkan pilih pickup point";
            return;
        }

        $shippingAssignationList = $this->getShippingAssignationList($kecamatan_code, $company_code);

        $stockFound = false;
        foreach($shippingAssignationList as $shippingAssignation) {
            if(isset($stockLocationList[$shippingAssignation['pickup_code']]) && $shippingAssignation['status'] == 10) {
                $stockLocations = $stockLocationList[$shippingAssignation['pickup_code']];
                foreach($stockLocations as $stockLocation) {
                    if ($stockLocation['store_code'] == $shippingAssignation['store_code']) {
                        $storeInformation = $stockLocation;

                        // move to next store if DC assign to gosend courier
                        if (isset($storeInformation['store_code'])) { 
                            if (CartHelper::isGosend($carrier_id) && $storeInformation['store_code'] == 'DC') { 
                                continue; 
                            } 
                        }

                        if (strtoupper(substr($shippingAssignation['pickup_code'], 0, 2)) == "DC") {
                            $delivery_method = "delivery";
                        } else {
                            $delivery_method = "store_fulfillment";
                        }
                        
                        // if gosend cek geolocation and is_express_courier
                        if (CartHelper::isGosend($carrier_id)) { 
                            if ($shippingAssignation['is_express_courier'] <> 1 || empty($shippingAssignation['geolocation'])) {
                                continue;
                            }
                        }

                        $this->supplier_alias = $storeInformation['supplier_alias'];
                        $this->store_code = $storeInformation['store_code'];
                        $this->delivery_method = $delivery_method;
                        $this->zone_id = $storeInformation['zone_id'];
                        if (CartHelper::isGosend($carrier_id)) { 
                            $this->shipping_assignation_rate = 0;
                        } else {
                            $this->carrier_id = $shippingAssignation['carrier_id'];
                            $this->shipping_assignation_rate = floatval($shippingAssignation['shipping_rate']);
                        }
                        $this->pickup_code = $shippingAssignation['pickup_code'];
                        $this->origin_shipping_region = $storeInformation['origin_shipping_region'];
                        $this->origin_shipping_district = $storeInformation['origin_shipping_district'];
                        
                        // need to count handling fee for prize zone
                        if ($this->zone_id > 1 && $this->is_free_item == 0) {
                            $this->countHandlingFeeAdjust();
                        }

                        $stockFound = true;
                        $this->can_delivery = true;
                    }

                    if($stockFound) {
                        break 2;
                    }
                }
            }
        }
        
        /**
         * @todo: also check pickup location list
         * case nya di PDP muncul product hanya bisa di ambil di: Warehouse DC
         */
        if(!$stockFound && !empty($stockLocationList) && !CartHelper::isGosend($carrier_id)) {
            $this->can_delivery = false;
            $this->delivery_method = "pickup";
            $this->pickup_code = "";
            $this->store_code = "";
            $this->carrier_id = "";

            $stockFound =  true;
        } elseif(!$stockFound && CartHelper::isGosend($carrier_id)) {
            // if not found pickup point that can complish the gosend order then we recalculate again

            $storeFound = false;
            if($this->delivery_method == "delivery" || $this->delivery_method == "store_fulfillment") {
                foreach($shippingAssignationList as $shippingAssignation) {
                    if ($this->pickup_code == $shippingAssignation['pickup_code'] && $shippingAssignation['status'] == 10) {
                        $stockLocations = $stockLocationList[$this->pickup_code];
                        foreach($stockLocations as $stockLocation) {
                            if ($stockLocation['store_code'] == $this->store_code) {
                                $storeInformation = $stockLocation;
                                $shippingInformation = $shippingAssignation;
                                $storeFound = true;
                                break 2;
                            }
                        }
                    }
                }
            }

            if (!$storeFound) {
                foreach($shippingAssignationList as $shippingAssignation) {
                    if(isset($stockLocationList[$shippingAssignation['pickup_code']]) && $shippingAssignation['status'] == 10) {
                        $stockLocations = $stockLocationList[$shippingAssignation['pickup_code']];
                        foreach($stockLocations as $stockLocation) {
                            if ($stockLocation['store_code'] == $shippingAssignation['store_code']) {
                                $storeInformation = $stockLocation;
                                $shippingInformation = $shippingAssignation;
                                $storeFound = true;
                                break 2;
                            }
                        }
                    }
                }
            }

            if ($storeFound) {
                $shippingAssignation = $shippingInformation;
                if (strtoupper(substr($shippingAssignation['pickup_code'], 0, 2)) == "DC") {
                    $delivery_method = "delivery";
                } else {
                    $delivery_method = "store_fulfillment";
                }
                
                $this->supplier_alias = $storeInformation['supplier_alias'];
                $this->store_code = $storeInformation['store_code'];
                $this->delivery_method = $delivery_method;
                $this->zone_id = $storeInformation['zone_id'];
                $this->carrier_id = $shippingAssignation['carrier_id'];
                $this->shipping_assignation_rate = $shippingAssignation['shipping_rate'];
                $this->pickup_code = $shippingAssignation['pickup_code'];
                $this->origin_shipping_region = $storeInformation['origin_shipping_region'];
                $this->origin_shipping_district = $storeInformation['origin_shipping_district'];
                
                // need to count handling fee for prize zone
                if ($this->zone_id > 1 && $this->is_free_item == 0) {
                    $this->countHandlingFeeAdjust();
                }

                $stockFound = true;
                $this->can_delivery = true;
            }

            if(!$stockFound && !empty($stockLocationList)) {
                $this->can_delivery = false;
                $this->delivery_method = "pickup";
                $this->pickup_code = "";
                $this->store_code = "";
                $this->carrier_id = "";
    
                $stockFound =  true;
            }
        }

        if(empty($this->delivery_method)) {
            $this->delivery_method = "delivery";
        }

        if(!$stockFound ) {
            $this->errorCode = "RR203";
            $this->errorMessages = "Stok tidak cukup atau sedang kosong";
        }
    }

    public function countHandlingFeeAdjust()
    {
        $priceZoneModel = new PriceZone();
        $parameters = [
            "conditions" => "sku = ?1 AND zone_id = ?2",
            "bind" => ["1" => $this->sku, "2" => $this->zone_id]
        ];
        $priceZone = $priceZoneModel->findFirst($parameters);

        $this->handling_fee_adjust = 0;
        if($priceZone) {
            $handlingFee = $priceZone->sale_price - $this->selling_price;
            if($handlingFee > 0 ) {
                $this->handling_fee_adjust = $priceZone->sale_price - $this->selling_price;

                $this->countSubTotal();
                $this->countRowTotal();
            }
        }
    }

    /**
     * @param string $kecamatan_code
     * @param array $stockLocationList
     *
     * @todo: [PENTING] Refactor this function only to determine marketplace item shipping assignation
     * @Roesmien
     * Ini hanya untuk menjawab product X, apakah bisa dikirim ke $kecamatan_code K, dan jika bisa origin nya lewat mana ? -> set this->shipping_origin
     * Jika tidak bisa error
     */
    public function marketplaceShippingAssignation($kecamatan_code = "", $stockLocationList = array(), $customer = null)
    {
        if(empty($kecamatan_code)) {
            $this->errorCode = "RR203";
            $this->errorMessages = "Produk tidak bisa dikirim ke alamat anda";
            return;
        }

        /**
         * Get store information where goods is send from
         *
         * for now, we only get the first store in data for now until we implement multiple store for MP
         *
         * @todo : change this logic if we already implement multipler store (storage) for MP
         */

        foreach ($stockLocationList as $storeCode => $storesData) {
            $storeData = $storesData[0];
            $this->store_code = $storeData['store_code'];
            $this->origin_shipping_region = $storeData['origin_shipping_region'];
            $this->origin_shipping_district = $storeData['origin_shipping_district'];
            break;
        }

        $supplierModel = new Supplier();

        /**
         * @var $supplierResult \Models\Supplier
         */

        $supplierResult = $supplierModel->findFirst("supplier_alias = '" . $this->supplier_alias ."' and status > 0 ");
        if(!$supplierResult) {
            $this->errorCode = "RR302";
            $this->errorMessages = "Supplier not found, please try again";
            return;
        }

        /**
         * Get information for shipment mathod support by this supplier
         */
        $flagShipment = $supplierResult->getMpFlagShipment();

        /**
         * if this supplier can not support ownfleet no need to check for region sendding support by this supplier
         */
        $arrFlagShipment = explode(',',$flagShipment);

        if(!in_array('ownfleet',$arrFlagShipment)) {

            /**
             * @todo: if tidak bisa ownfleet, treat / hitung sebagai JNE
             */
            $this->delivery_method = "delivery";
            return;
        }

        /**
         * Get product ownfleet template id
         */
        $ownfleetTemplateId = FALSE;
        if( $this->product_variant )  {
            $this->product = $this->product_variant->Product;
            $ownfleetTemplateId = $this->product->getOwnfleetTemplateId();
        } else if ($this->ownfleet_template_id){
            $ownfleetTemplateId = $this->ownfleet_template_id;
        }else {
            /*
             * If supplier flag shipment having 3pl and no ownfleet template id in sku, then used 3pl / Ruparupa Rate
             */
            if(in_array('3pl',$arrFlagShipment) || in_array('ruparupa',$arrFlagShipment)) {
                $this->delivery_method = "delivery";
                return;
            }else{
                $this->errorCode = "RR203";
                $this->errorMessages = "Produk tidak bisa dikirim ke alamat anda";
                return;
            }
        }

        /**
         * @todo: simplify wilayah_mapping
         * @Roesmien
         * product X dikirim ke $kecamatan_code , di support oleh `origin_region` apa ? dan fk_wilayah_template apa ?
         * kalau tidak ada `origin_region` yang support, maka ***tidak dapat dikirim****
         * kalau ada masuk ke $this->_getOwnFleetShippingCost($kecamatan_code);
         * "select * from sc_wilayah_mapping where kecamatan_code=$kecamatan_code and fk_wilayah_template IN ($this->product->getWilayahTemplateId()) LIMIT 0,1";
         */

        $ownfleetRateModel = new SupplierOwnfleetRate();
        $params = ["conditions" => "ownfleet_template_id = '". $ownfleetTemplateId ."' AND dest_district_code = '". $kecamatan_code ."'"];
        $ownfleetRateResult =  $ownfleetRateModel->findFirst($params);
        if($ownfleetRateResult){
            /*
             * "exclude" means not supported area
             */
            if($ownfleetRateResult->getIncludeExclude() == 'exclude'){
                $this->errorCode = "RR203";
                $this->errorMessages = "Produk tidak bisa dikirim ke alamat anda";
                return;
            }else{
                /**
                 * Checking ownfleet but using regular carrier rate
                 */
                if($ownfleetRateResult->getCarrierCode() != "ownfleet"){
                    $this->delivery_method = "delivery";
                }elseif($ownfleetRateResult->getRate() == 0){
                    $this->delivery_method = "delivery";
                }else{
                    $this->delivery_method = "ownfleet";
                    if($ownfleetRateResult->getIsFlat() == 1) {
                        $this->shipping_amount = $ownfleetRateResult->getRate();
                    } else {
                        $this->shipping_amount = ceil($this->weight) * $ownfleetRateResult->getRate();
                    }

                    // assign shipping address according to kecamatan code
                    if ($kecamatan_code == getenv('DEFAULT_SHIPPING_KECAMATAN_CODE')) {
                        if (!empty($customer)) {
                            $customerData = $customer->toArray();
                        }
                        
                        $firstName = (isset($customerData['customer_firstname'])) ? $customerData['customer_firstname'] : '';
                        $lastName = (isset($customerData['customer_lastname'])) ? $customerData['customer_lastname'] : '';
                        $defaultShippingLocation = [
                            "first_name" => $firstName, 
                            "last_name" => $lastName, 
                            "address_type" => 'shipping', 
                            "country_id" => getenv("DEFAULT_SHIPPING_COUNTRY_ID"),
                            "province_id" => getenv("DEFAULT_SHIPPING_PROVINCE_ID"),
                            "city_id" => getenv("DEFAULT_SHIPPING_CITY_ID"),
                            "kecamatan_id" => getenv("DEFAULT_SHIPPING_KECAMATAN_ID")
                        ];
                        $shippingAddress = new SalesOrderAddress();
                        $shippingAddress->setFromArray($defaultShippingLocation);
                        $this->shipping_address = $shippingAddress;

                        //\Helpers\LogHelper::log("bug_cart","/models/CartItem.php Line 1860, data: $kecamatan_code","debug");
                    }                    
                }
            }
        }else{
            /*
             * default rule if ownfleet not found use jne rate
             * if you wont to disable order if ownfleet not found enable comment err
             */
            //$this->errorCode = "RR203";
            //$this->errorMessages[$this->sku][] = "Produk tidak bisa dikirim ke alamat anda Ownfleet rate tidak tersedia";
            $this->delivery_method = "delivery";
        }

        /**
         * Refactoring Impact from simplify wilayah_mapping
         */
//        $listWilayah = explode(",", str_replace(" ","",$this->product->getWilayahTemplateId()));


//        foreach ($listWilayah as $wilayahTemplateId) {
//            $wilayahMappingModel = new ScWilayahMapping();
//            $params = ["conditions" => "wilayah_template_id = '". $wilayahTemplateId ."' AND dest_kecamatan_code = '". $kecamatan_code ."'"];
//            $wilayahResult =  $wilayahMappingModel->findFirst($params);
//
//            if($wilayahResult) {
//                $this->delivery_method = "ownfleet";
//                $this->origin_shipping_region = $wilayahResult->getOriginRegion();
//                $this->_getOwnFleetShippingCost($kecamatan_code);
//            }
//        }
    }

    /**
     * Determine shipping rate & courier when sending --> $this->origin_shipping_region to $kecamatan_code
     * Determine whether to use is_flat = 1 or is_flat = 0
     * @param string $kecamatan_code
     */

    /**
     * Refactoring Impact from simplify wilayah_mapping
     */

//    private function _getOwnFleetShippingCost($kecamatan_code = "")
//    {
//        /**
//         * @todo: determine only 1 row
//         * @Roesmien
//         * Kirim dari Jakarta ke Papua, (1) pake tarif flat (is_flat=1) atau (2) tarif kiloan (is_flat=0) (3) atau ga di support sama sekali
//         *
//         * is_flat = f(origin_shipping_region, kecamatan_code, ownfleet_template_id)
//         * rate = f(origin_shipping_region, kecamatan_code, ownfleet_template_id)
//         *
//         *
//         * @todo:
//         * @Roesmien
//         * - change database structure sc_ownfleet_rate to use *product_id* instead of ownfleet_template_id
//         * - change database product to remove *ownfleet_template_id*
//         */
//        $listOwnFleetFareTemplate = explode(",", str_replace(" ","",$this->product->getOwnfleetTemplateId()));
//
//        foreach ($listOwnFleetFareTemplate as $ownFleetTemplateId) {
//            $scOwnFleetPrice = new ScOwnfleetRate();
//            $params = ["conditions" => "ownfleet_template_id = '". $ownFleetTemplateId ."' AND origin_region = '". $this->origin_shipping_region ."' AND dest_kecamatan_code = '". $kecamatan_code ."'"];
//            $ownFleetPriceResult = $scOwnFleetPrice->findFirst($params);
//            if($ownFleetPriceResult) {
//                // count shipping ammount
//                if($ownFleetPriceResult->getIsFlat() == 1) {
//                    $this->shipping_amount = $ownFleetPriceResult->getRate();
//                } else {
//                    $this->shipping_amount = ceil($this->weight) * $ownFleetPriceResult->getRate();
//                }
//                break;
//            } else {
//                $this->errorCode = "RR203";
//                $this->errorMessages[$this->sku][] = "Produk tidak bisa dikirim ke alamat anda";
//                break;
//            }
//        }
//
//    }


    public function itemValidation($company_code = 'ODI')
    {
        /**
         * Checking consistancy data and validation of fulfillment item
         */
        if ($this->delivery_method == 'pickup' && strtoupper(substr($this->store_code,0,2)) == 'DC') {
            $this->store_code = "";
            $this->pickup_code = "";
        }
        
        if ($this->delivery_method == 'pickup' && ($this->store_code == "" || $this->store_code == "DC") && $this->pickup_code == "") {
            $this->errorCode = "RR203";
            $this->errorMessages['type'] = "sku";  
            $this->errorMessages['value'] = $this->sku;  
            $this->errorMessages['message'] =  "Silahkan pilih toko tempat anda ingin mengambil barang.";
        }
        
        if ($this->delivery_method == "delivery") {
            $this->pickup_code = "";
        }

        /**
         * Checking stock avaibility of an item
         */
        if (empty($this->errorMessages)) {
            $this->checkStockValidate($company_code);
        }
    }    
}