<?php 
 
namespace Models; 
use Phalcon\Db as Database; 
 
 
class MarketingTukarvoucher extends \Models\BaseModel 
{ 
 
    protected $tukarvoucher_id; 
 
    protected $customer_first_name; 

    protected $customer_last_name; 

    protected $customer_phone; 
 
    protected $customer_email; 

    protected $customer_id;
 
    protected $customer_password; 
 
    protected $customer_merchant; 
 
    protected $voucher_code_generated;

    protected $customer_denom;
 
    protected $created_at; 
 
    protected $updated_at; 
 
    /** 
     * Initialize method for model. 
     */ 
    public function initialize() 
    { 
 
    } 
 
    public function onConstruct() 
    { 
        parent::onConstruct(); 
        $this->whiteList = array(); 
    } 
 
    /** 
     * Returns table name mapped in the model. 
     * 
     * @return string 
     */ 
    public function getSource() 
    { 
        return 'marketing_tukarvoucher'; 
    } 
 
    /** 
     * Allows to query a set of records that match the specified conditions 
     * 
     * @param mixed $parameters 
     * @return ProductVariant[] 
     */ 
    public static function find($parameters = null) 
    { 
        return parent::find($parameters); 
    } 
 
    /** 
     * Allows to query the first record that match the specified conditions 
     * 
     * @param mixed $parameters 
     * @return ProductVariant 
     */ 
    public static function findFirst($parameters = null) 
    { 
        return parent::findFirst($parameters); 
    } 
 
    public function setFromArray($dataArray = array()) 
    { 
        $variantAttributes = array(); 
        foreach ($dataArray as $key => $val) { 
            if (property_exists($this, $key)) { 
                $this->{$key} = $val; 
            } 
        } 
 
        // get not send data but have in this parameter 
        $thisArray = get_class_vars(get_class($this)); 
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray)); 
    } 
 
    public function getDataArray($columns = array(), $showEmpty = false) 
    { 
        $view = $this->toArray($columns, $showEmpty); 
 
        return $view; 
    } 
 

    /**
     * Get the value of customer_email
     */ 
    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    /**
     * Set the value of customer_email
     *
     * @return  self
     */ 
    public function setCustomerEmail($customer_email)
    {
        $this->customer_email = $customer_email;

        return $this;
    }

    /**
     * Get the value of customer_first_name
     */ 
    public function getCustomerFirstName()
    {
        return $this->customer_first_name;
    }

    /**
     * Set the value of customer_first_name
     *
     * @return  self
     */ 
    public function setCustomerFirstName($customer_first_name)
    {
        $this->customer_first_name = $customer_first_name;

        return $this;
    }

    /**
     * Get the value of customer_last_name
     */ 
    public function getCustomerLastName()
    {
        return $this->customer_last_name;
    }

    /**
     * Set the value of customer_last_name
     *
     * @return  self
     */ 
    public function setCustomerLastName($customer_last_name)
    {
        $this->customer_last_name = $customer_last_name;

        return $this;
    }

    /**
     * Get the value of customer_phone
     */ 
    public function getCustomerPhone()
    {
        return $this->customer_phone;
    }

    /**
     * Set the value of customer_phone
     *
     * @return  self
     */ 
    public function setCustomerPhone($customer_phone)
    {
        $this->customer_phone = $customer_phone;

        return $this;
    }

    /**
     * Get the value of customer_password
     */ 
    public function getCustomerPassword()
    {
        return $this->customer_password;
    }

    /**
     * Set the value of customer_password
     *
     * @return  self
     */ 
    public function setCustomerPassword($customer_password)
    {
        $this->customer_password = $customer_password;

        return $this;
    }

    /**
     * Get the value of customer_denom
     */ 
    public function getCustomerDenom()
    {
        return $this->customer_denom;
    }

    /**
     * Set the value of customer_denom
     *
     * @return  self
     */ 
    public function setCustomerDenom($customer_denom)
    {
        $this->customer_denom = $customer_denom;

        return $this;
    }

    /**
     * Get the value of voucher_code_generated
     */ 
    public function getVoucherCodeGenerated()
    {
        return $this->voucher_code_generated;
    }

    /**
     * Set the value of voucher_code_generated
     *
     * @return  self
     */ 
    public function setVoucherCodeGenerated($voucher_code_generated)
    {
        $this->voucher_code_generated = $voucher_code_generated;

        return $this;
    }

    /**
     * Get the value of customer_id
     */ 
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Set the value of customer_id
     *
     * @return  self
     */ 
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;

        return $this;
    }
} 