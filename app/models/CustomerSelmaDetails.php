<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerSelmaDetails extends \Models\BaseModel {
    protected $customer_selma_details_id;
    protected $customer_id;
    protected $selma_uid;
    protected $selma_customer_id;
    protected $selma_customer_member_no;
    protected $selma_customer_tmp_member_no;
    protected $session_id;
    protected $login_attempt;
    protected $ktp;
    protected $marital_status;
    protected $is_migrated;
    protected $created_at;
    protected $updated_at;

    public function setCustomerSelmaDetailsId($customer_selma_details_id) {
        $this->customer_selma_details_id = $customer_selma_details_id;
    }
    
    public function getCustomerSelmaDetailsId()
    {
        return $this->customer_selma_details_id;
    }
    
    public function setSelmaCustomerId($selma_customer_id)
    {
        $this->selma_customer_id = $selma_customer_id;
    }
    
    public function getSelmaCustomerMemberNo()
    {
        return $this->selma_customer_member_no;
    }
    
    public function setSelmaCustomerMemberNo($selma_customer_member_no)
    {
        $this->selma_customer_member_no = $selma_customer_member_no;
    }
    
    public function getCustomerToken()
    {
        return $this->customer_token;
    }
    
    public function setCustomerToken($customer_token) {
      $this->customer_token = $customer_token;
    }
    
    public function setLoginAttempt($login_attempt) {
      $this->login_attempt = $login_attempt;
    }
    
    public function getLoginAttempt()
    {
        return $this->login_attempt;
    }
        
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function getKtp()
    {
        return $this->ktp;
    }

    /**
     * @param string $ktp
     */
    public function setKtp($ktp)
    {
        $this->ktp = $ktp;
    }

    public function getMaritalStatus()
    {
        return $this->marital_status;
    }
   /**
     * @param string $marital_status
     */
    public function setMaritalStatus($marital_status)
    {
        $this->marital_status = $marital_status;
    }

    public function getSelmaCustomerTmpMemberNo()
    {
        return $this->selma_customer_tmp_member_no;
    }
        /**
     * @param string $selma_customer_tmp_member_no
     */
    public function setSelmaCustomerTmpMemberNo($selma_customer_tmp_member_no)
    {
        $this->selma_customer_tmp_member_no = $selma_customer_tmp_member_no;
    }

    public function getIsMigrated()
    {
        return $this->is_migrated;
    }
        /**
     * @param string $is_migrated
     */
    public function setIsMigrated($is_migrated)
    {
        $this->is_migrated = $is_migrated;
    }

    public function initialize()
    {
        $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_selma_details';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_customer_selma", "TRIGGERED: " .json_encode($this->toArray()));
    }

}
