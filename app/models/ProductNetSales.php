<?php

namespace Models;

use Helpers\GeneralHelper;
use Helpers\ProductHelper;
use Phalcon\Db;
use Phalcon\Db as Database;

class ProductNetSales extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $net_sales_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_variant_id;

    protected $sku;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $net_sales;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $special_net_sales;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $special_from_date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $special_to_date;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $data;

    protected $errors;

    protected $messages;


    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }



    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('product_variant_id', 'ProductVariant', 'product_variant_id', array('alias' => 'ProductVariant'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_net_sales';
    }

    /**
     * @return int
     */
    public function getNetSalesId()
    {
        return $this->net_sales_id;
    }

    /**
     * @param int $price_id
     */
    public function setNetSalesId($net_sales_id)
    {
        $this->price_id = $net_sales_id;
    }

    /**
     * @return int
     */
    public function getProductVariantId()
    {
        return $this->product_variant_id;
    }

    /**
     * @param int $product_variant_id
     */
    public function setProductVariantId($product_variant_id)
    {
        $this->product_variant_id = $product_variant_id;
    }

    /**
     * @return float
     */
    public function getNetSales()
    {
        return $this->net_sales;
    }

    /**
     * @param float $price
     */
    public function setNetSales($net_sales)
    {
        $this->net_sales = $net_sales;
    }

    /**
     * @return float
     */
    public function getSpecialPrice()
    {
        return $this->special_net_sales;
    }

    /**
     * @param float $special_price
     */
    public function setSpecialNetSales($special_net_sales)
    {
        $this->special_net_sales = $special_net_sales;
    }

    /**
     * @return string
     */
    public function getSpecialFromDate()
    {
        return $this->special_from_date;
    }

    /**
     * @param string $special_from_date
     */
    public function setSpecialFromDate($special_from_date)
    {
        $this->special_from_date = $special_from_date;
    }

    /**
     * @return string
     */
    public function getSpecialToDate()
    {
        return $this->special_to_date;
    }

    /**
     * @param string $special_to_date
     */
    public function setSpecialToDate($special_to_date)
    {
        $this->special_to_date = $special_to_date;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductPrice[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductPrice
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                if($key == 'special_from_date'){
                    if(\Helpers\ProductHelper::checkIsAValidDate($val)){
                        $this->{$key} = $val;
                    }else{
                        $this->{$key} = null;
                    }
                }
                elseif($key == 'special_to_date'){
                    if(\Helpers\ProductHelper::checkIsAValidDate($val)){
                        $this->{$key} = $val;
                    }else{
                        $this->{$key} = null;
                    }
                }else{
                    $this->{$key} = $val;
                }
            }
        }
        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public function beforeCreate()
    {
        parent::beforeCreate();

        if(empty($this->status)) {
            $this->status = 10;
        }
    }
}
