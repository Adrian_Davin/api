<?php

namespace Models;

class AdminUserAttribute extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $admin_user_attribute_id;

    /**
     *
     * @var string
     */
    protected $admin_user_attribute_name;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     * Method to set the value of field admin_user_attribute_id
     *
     * @param integer $admin_user_attribute_id
     * @return $this
     */
    public function setAdminUserAttributeId($admin_user_attribute_id)
    {
        $this->admin_user_attribute_id = $admin_user_attribute_id;

        return $this;
    }

    /**
     * Method to set the value of field admin_user_attribute_name
     *
     * @param string $admin_user_attribute_name
     * @return $this
     */
    public function setAdminUserAttributeName($admin_user_attribute_name)
    {
        $this->admin_user_attribute_name = $admin_user_attribute_name;

        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field admin_user_attribute_id
     *
     * @return integer
     */
    public function getAdminUserAttributeId()
    {
        return $this->admin_user_attribute_id;
    }

    /**
     * Returns the value of field admin_user_attribute_name
     *
     * @return string
     */
    public function getAdminUserAttributeName()
    {
        return $this->admin_user_attribute_name;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('admin_user_attribute_id', 'Models\AdminUserAttributeValue', 'admin_user_attribute_id', array('alias' => 'AdminUserAttributeValue'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'admin_user_attribute';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminUserAttribute[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminUserAttribute
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
