<?php

namespace Models;

use Helpers\LogHelper;

class SalesOrderPayment extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $payment_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $sales_order_id;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $method;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $type;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $total_amount_ordered;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $total_amount_paid;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $total_amount_refunded;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $cc_type;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $cc_number;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $cc_owner;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $installment_bank;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $installment_tenor;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $va_bank;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $va_number;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $additional_data;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $status;

    protected $expire_transaction;

    protected $proof_of_payment;

    protected $pg_transaction_id;

    protected $action_url;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $issuer;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $acquirer;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $can_cancel;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_order_payment';
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * @param int $sales_order_id
     */
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }

    /**
     * @return int
     */
    public function getPaymentId()
    {
        return $this->payment_id;
    }

    /**
     * @param int $payment_id
     */
    public function setPaymentId($payment_id)
    {
        $this->payment_id = $payment_id;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getTotalAmountOrdered()
    {
        return $this->total_amount_ordered;
    }

    /**
     * @param float $total_amount_ordered
     */
    public function setTotalAmountOrdered($total_amount_ordered)
    {
        $this->total_amount_ordered = $total_amount_ordered;
    }

    /**
     * @return float
     */
    public function getTotalAmountPaid()
    {
        return $this->total_amount_paid;
    }

    /**
     * @param float $total_amount_paid
     */
    public function setTotalAmountPaid($total_amount_paid)
    {
        $this->total_amount_paid = $total_amount_paid;
    }

    /**
     * @return float
     */
    public function getTotalAmountRefunded()
    {
        return $this->total_amount_refunded;
    }

    /**
     * @param float $total_amount_refunded
     */
    public function setTotalAmountRefunded($total_amount_refunded)
    {
        $this->total_amount_refunded = $total_amount_refunded;
    }

    /**
     * @return string
     */
    public function getCcType()
    {
        return $this->cc_type;
    }

    /**
     * @param string $cc_type
     */
    public function setCcType($cc_type)
    {
        $this->cc_type = $cc_type;
    }

    /**
     * @return string
     */
    public function getCcNumber()
    {
        return $this->cc_number;
    }

    /**
     * @param string $cc_number
     */
    public function setCcNumber($cc_number)
    {
        $this->cc_number = $cc_number;
    }

    /**
     * @return string
     */
    public function getCcOwner()
    {
        return $this->cc_owner;
    }

    /**
     * @param string $cc_owner
     */
    public function setCcOwner($cc_owner)
    {
        $this->cc_owner = $cc_owner;
    }

    /**
     * @return string
     */
    public function getInstallmentBank()
    {
        return $this->installment_bank;
    }

    /**
     * @param string $installment_bank
     */
    public function setInstallmentBank($installment_bank)
    {
        $this->installment_bank = $installment_bank;
    }

    /**
     * @return string
     */
    public function getInstallmentTenor()
    {
        return $this->installment_tenor;
    }

    /**
     * @param string $installment_tenor
     */
    public function setInstallmentTenor($installment_tenor)
    {
        $this->installment_tenor = $installment_tenor;
    }

    /**
     * @return string
     */
    public function getVaBank()
    {
        return $this->va_bank;
    }

    /**
     * @param string $va_bank
     */
    public function setVaBank($va_bank)
    {
        $this->va_bank = $va_bank;
    }

    /**
     * @return string
     */
    public function getVaNumber()
    {
        return $this->va_number;
    }

    /**
     * @param string $va_number
     */
    public function setVaNumber($va_number)
    {
        $this->va_number = $va_number;
    }

    /**
     * @return string
     */
    public function getAdditionalData()
    {
        return $this->additional_data;
    }

    /**
     * @param string $additional_data
     */
    public function setAdditionalData($additional_data)
    {
        $this->additional_data = $additional_data;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getExpireTransaction()
    {
        return $this->expire_transaction;
    }

    /**
     * @param mixed $expire_transaction
     */
    public function setExpireTransaction($expire_transaction): void
    {
        $this->expire_transaction = $expire_transaction;
    }

    public function setProofOfPayment($proof_of_payment) {
        $this->proof_of_payment = $proof_of_payment;
    }

    public function getProofOfPayment() {
        return $this->proof_of_payment;
    }

    /**
     * @return int
     */
    public function getCanCancel()
    {
        return $this->can_cancel;
    }

    /**
     * @param int $can_cancel
     */
    public function setCanCancel($can_cancel)
    {
        $this->can_cancel = $can_cancel;
    }

    /**
     * @return string
     */
    public function getPgTransactionId() 
    {
        return $this->pg_transaction_id;
    }

    /**
     * @param string $pg_transaction_id
     */
    public function setPgTransactionId($pg_transaction_id)
    {
        $this->pg_transaction_id = $pg_transaction_id;
    }

    /**
     * @return string
     */
    public function getActionUrl() 
    {
        return $this->action_url;
    }

    /**
     * @param string $$action_url
     */
    public function setActionUrl($action_url)
    {
        $this->action_url = $action_url;
    }

    /**
     * @return string
     */
    public function getIssuer() {
        return $this->issuer;
    }

    /**
     * @param string $issuer
     */
    public function setIssuer($issuer) {
        $this->issuer = $issuer;
    }

    /**
     * @return string
     */
    public function getAcquirer() {
        return $this->issuer;
    }

    /**
     * @param string $acquirer
     */
    public function setAcquirer($acquirer) {
        $this->acquirer = $acquirer;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderPayment[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderPayment
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_order_payment", "TRIGGERED");
    }

    /**
     * @param array $dataArray
     */
    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            $this->{$key} = $val;
        }

        if (empty($this->status)) {
            $this->status = 'pending';
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray, $dataArray));
    }

    /**
     * @param array $columns
     * @param bool $showEmpty
     * @return mixed
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        if (!empty($this->cc_number)) {
            $cardNo = substr(str_replace(' ', '', $this->cc_number), 0, 6);
            $view['cc_bin'] = $cardNo;
        }

        return $view;
    }

    /**
     * @return array
     * @throws \Library\HTTPException
     */
    public function createData()
    {
        try {
            $this->useWriteConnection();
            $this->created_at = date('Y-m-d H:i:s');
            if ($this->create() === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                LogHelper::log("sales_order_payment", "Sales Order Payment save failed, error : " . json_encode($errMsg));
            }
        } catch (\Exception $e) {
            LogHelper::log("sales_order_payment", "Sales Order Payment update failed, error : " . $e->getMessage());
            $this->errors[] = "Update failed, please try again";
        }

        return;
    }

    public function generateEntity()
    {
        $this->useReadOnlyConnection();
        $result = $this->findFirst(
            array(
                "conditions" => "sales_order_id = " . $this->sales_order_id,
                "columns" => "method,type,cc_type,cc_number,cc_owner,installment_bank,installment_tenor,va_bank,va_number,additional_data"
            )
        );

        $this->setFromArray($result);
        return $this->getDataArray(array(), true);
    }
}
