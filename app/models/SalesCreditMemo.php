<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class SalesCreditMemo extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $credit_memo_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $order_no;

    protected $credit_memo_no;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $subtotal;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $shipping_amount;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $customer_penalty;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $refund_as_gift_card;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $refund_cash;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    protected $voucher_id = 0;

    /**
     * @var \Models\Items\CreditMemo\Collection
     */
    protected $credit_memo_items;

    /**
     * @var \Models\Invoice\Collection
     */
    protected $invoices;

    /**
     * @var \Models\Shipment\Collection
     */
    protected $shipment;

    protected $created_by;

    /**
     * Get the value of created_by
     */ 
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set the value of created_by
     *
     * @return  self
     */ 
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('order_no', 'Models\SalesOrder', 'order_no', array('alias' => 'SalesOrder'));
        $this->hasOne('voucher_id', 'Models\Sales', 'voucher_id', array('alias' => 'SalesRuleVoucher'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_credit_memo';
    }

    /**
     * @return mixed
     */
    public function getCreditMemoNo()
    {
        return $this->credit_memo_no;
    }

    /**
     * @param mixed $credit_memo_no
     */
    public function setCreditMemoNo($credit_memo_no)
    {
        $this->credit_memo_no = $credit_memo_no;
    }

    /**
     * @return int
     */
    public function getCreditMemoId()
    {
        return $this->credit_memo_id;
    }

    /**
     * @return mixed
     */
    public function getVoucherId()
    {
        return $this->voucher_id;
    }

    /**
     * @param mixed $voucher_id
     */
    public function setVoucherId($voucher_id)
    {
        $this->voucher_id = $voucher_id;
    }

    /**
     * @return string
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * @param string $order_no
     */
    public function setOrderNo($order_no)
    {
        $this->order_no = $order_no;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCreditMemo[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCreditMemo
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        foreach($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }

            if($key == 'invoices')
            {
                $creditMemoItems = array();
                $invoices = $shipment = array();
                foreach($val as $invoice) {
                    $item = array();
                    foreach($invoice['credit_memo_items'] as $sales_order_id => $item) {
                        $item['invoice_no'] = $invoice['invoice_no'];
                        $creditMemoItem = new \Models\SalesCreditMemoItem();
                        $creditMemoItem->setFromArray($item);
                        $creditMemoItems[] = $creditMemoItem;
                    }

                    $invoiceModel = new \Models\SalesInvoice();
                    $invoiceResult = $invoiceModel->findFirst("invoice_no = '". $invoice['invoice_no'] ."'");
                    $invoiceModel->assign($invoiceResult->toArray());
                    $invoiceModel->setInvoiceStatus($invoice['refund_status']);

                    if ($invoice['refund_status'] == 'full_refund') {
                        $invoiceId = $invoiceResult->toArray()['invoice_id'];
                        $shipmentModel = new \Models\SalesShipment();
                        $shipmentResult = $shipmentModel->findFirst("invoice_id = $invoiceId");
                        $currentShipmentStatus = $shipmentResult->toArray()['shipment_status'];
                        if ($currentShipmentStatus == 'new') {
                            $shipmentModel->assign($shipmentResult->toArray());
                            $shipmentModel->setShipmentStatus("canceled");
                            $shipment[] = $shipmentModel;
                        }
                    }

                    $invoices[] = $invoiceModel;
                }

                if (!empty($shipment)) $this->shipment = new \Models\Shipment\Collection($shipment);
                $this->invoices = new \Models\Invoice\Collection($invoices);
                $this->credit_memo_items = new \Models\Items\CreditMemo\Collection($creditMemoItems);
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = parent::toArray($columns,true);

        if(!empty($this->credit_memo_items)) {
            $view['items'] = $this->credit_memo_items->getDataArray();
        }

        return $view;
    }

    public function beforeCreate()
    {
        parent::beforeCreate();

        $result = $this->findFirst("credit_memo_no = '" . $this->credit_memo_no ."'");
        if($result) {
            do {
                $result = $this->findFirst("credit_memo_no = '" . $this->credit_memo_no ."'");
                $this->credit_memo_no = \Helpers\Transaction::generateCreditMemoNo();
            } while(!empty($result));
        }
    }

    public function saveData($log_file_name = "credit_memo", $state = "")
    {
        $this->created_at = date("Y-m-d H:i:s");

        try {
            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);

            if ($this->create() === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log($log_file_name, "Credit memo save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Save Credit Memo failed";

                $transaction->rollback(
                    "Save Credit memo failed"
                );
                return false;
            }

            try {
                $this->credit_memo_items->setSalesCreditMemoId($this->credit_memo_id);
                $this->credit_memo_items->saveData($transaction);
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveItem : " . $e->getMessage()
                );
            }

            try {
                $this->invoices->saveData($transaction);
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveInvoice : " . $e->getMessage()
                );
            }

            if (!empty($this->shipment)) {
                try {
                    $this->shipment->saveData($transaction);
                } catch (\Exception $e) {
                    $transaction->rollback(
                        "saveShipment : " . $e->getMessage()
                    );
                }
            }

            $transaction->commit();
        } catch (TxFailed $e) {
            throw new \Exception($e->getMessage());
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return true;
    }

    public function updateCreditMemo()
    {
        $this->useWriteConnection();
        if ($this->update() === false) {
            $messages = $this->getMessages();

            $errMsg = array();
            foreach ($messages as $message) {
                $errMsg[] = $message->getMessage();
            }

            \Helpers\LogHelper::log("credit_memo", "Credit memo save failed, error : " . json_encode($errMsg));
            $this->errorCode = "RR301";
            $this->errorMessages[] = "Save Credit Memo failed";

            return false;
        }
    }

    public function getCreditMemoList($params = array())
    {
        if (isset($params['limit']) && isset($params['offset'])) {
            $limit = $params['limit'];
            $offset = $params['offset'];
            unset($params['limit']);
            unset($params['offset']);
        }

        $query_count = "
        SELECT
            count(credit_memo_id) as total
        FROM
            sales_credit_memo scm";

        $columns = "credit_memo_id,
                    credit_memo_no,
                    order_no,
                    (
                        SELECT
                            created_at
                        FROM
                            sales_order
                        WHERE
                            order_no = scm.order_no
                        LIMIT 1
                    ) AS order_created_at,
                    (
                        SELECT
                            concat(first_name, ' ', last_name)
                        FROM
                            sales_order_address
                        WHERE
                            sales_order_id = (
                                SELECT
                                    sales_order_id
                                FROM
                                    sales_order
                                WHERE
                                    order_no = scm.order_no
                                LIMIT 1
                            )
                        AND address_type = 'billing'
                    ) AS customer_name,
                    subtotal,
                    type,
                    scm.created_at,
                    (
                        SELECT n.transaction_id FROM nicepay n 
                        WHERE n.order_id = (
                            SELECT so.order_no FROM sales_order so 
                            LEFT JOIN sales_order_payment sop ON sop.sales_order_id = so.sales_order_id 
                            WHERE sop.method = 'kredivo' AND sop.type = 'credit_non_cc' AND so.order_no = scm.order_no 
                            LIMIT 1
                        )
                        LIMIT 1
                    ) AS kredivo_id";

        $where_clause = "";
        if (isset($params['cmr_no_keyword']) || isset($params['kredivo_id_keyword']) || isset($params['order_no_keyword']) || isset($params['order_date_keyword']) || isset($params['bill_to_name_keyword']) || isset($params['created_at_keyword']) || isset($params['company_code'])) {
            $where_clause .= " WHERE ";
        }

        $flagAnd = 0;
        if (isset($params['cmr_no_keyword'])) {
            $where_clause .= "(LOWER(credit_memo_no) LIKE '%" . strtolower($params['cmr_no_keyword']) . "%')";

            $flagAnd++;
        }

        if (isset($params['order_no_keyword'])) {
            if ($flagAnd > 0) {
                $where_clause .= " AND ";
            }

            if (is_array($params['order_no_keyword'])) {
                $where_clause .= "(order_no IN ('" . implode("','", $params['order_no_keyword']) . "'))";
            } else {
                $where_clause .= "(LOWER(order_no) LIKE '%" . strtolower($params['order_no_keyword']) . "%')";
            }
            $flagAnd++;
        }

        if (isset($params['order_date_keyword'])) {
            if ($flagAnd > 0) {
                $where_clause .= " AND ";
            }

            $dateFrom = $params['order_date_keyword']." 00:00:00";
            $dateTo   = $params['order_date_keyword']." 23:59:59";
            $where_clause .= " order_no in (SELECT order_no FROM sales_order WHERE order_no = scm.order_no and created_at BETWEEN '".$dateFrom."' AND '".$dateTo."')";
            $flagAnd++;
        }

        if (isset($params['bill_to_name_keyword'])) {
            if ($flagAnd > 0) {
                $where_clause .= " AND ";
            }

            $where_clause .= " order_no in (select order_no from sales_order where sales_order_id in(select sales_order_id FROM sales_order_address WHERE sales_order_id = ( SELECT sales_order_id FROM sales_order WHERE order_no = scm.order_no LIMIT 1 ) AND address_type = 'billing' AND (first_name like '%".$params['bill_to_name_keyword']."%' or last_name like '%".$params['bill_to_name_keyword']."%')))";
            $flagAnd++;
        }

        if (isset($params['company_code'])) {
            if ($flagAnd > 0) $where_clause .= " AND ";

            $companyCodeArray = explode(",", strtoupper($params['company_code']));
            if (in_array("ODI", $companyCodeArray) && in_array("AHI", $companyCodeArray)) {
                $where_clause .= "(company_code = 'ODI' OR company_code = 'AHI')";
            } else if (in_array("ODI", $companyCodeArray)) {
                $where_clause .= "company_code = 'ODI'";
            } else if (in_array("AHI", $companyCodeArray)) {
                $where_clause .= "company_code = 'AHI'";
            }

            $flagAnd++;
        }

        if (isset($params['created_at_keyword'])) {
            if ($flagAnd > 0) {
                $where_clause .= " AND ";
            }

            $dateFrom = $params['created_at_keyword']." 00:00:00";
            $dateTo   = $params['created_at_keyword']." 23:59:59";
            $where_clause .= " created_at BETWEEN '".$dateFrom."' AND '".$dateTo."'";
        }

        $query_count .= $where_clause;
        if (isset($limit) && isset($offset)) {
            $query = "SELECT " . $columns . " FROM sales_credit_memo scm " . $where_clause . " ORDER BY created_at DESC  LIMIT $limit OFFSET $offset";
        } else {
            $query = "SELECT " . $columns . " FROM sales_credit_memo scm " . $where_clause . " ORDER BY created_at DESC ";
        }

        $allCreditMemo['list'] = array();
        try {
            if($params['type'] != "count_only") {
                $result = $this->getDi()->getShared($this->getConnection())->query($query);
                if ($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $allCreditMemo['list'] = $result->fetchAll();
                }
            }

            $result = $this->getDi()->getShared($this->getConnection())->query($query_count);
            $result->setFetchMode(Database::FETCH_ASSOC);
            $total = $result->fetchAll();
            $allCreditMemo['recordsTotal'] = $total[0]['total'];
        } catch (\Exception $e) {
            $allCreditMemo['list'] = array();
        }

        return $allCreditMemo;
    }

    public function updateRefundStatus($refundNo = "", $status ="")
    {
        $sql = "update sales_return_refund set status = '" . $status . "' where refund_no = '" . $refundNo . "'";
        $result = $this->getDi()->getShared('dbMaster')->execute($sql);

        if(!$result){
            \Helpers\LogHelper::log("return_refund", "cannot update status #". $refundNo . " to " . $status);
        }
    }

    public function updateStatusItem($refundNo = "", $status ="")
    {
        // get invoice_id by refund_no
        $sql = "select invoice_id
                from sales_invoice 
                where invoice_no = (select invoice_no from sales_return_refund where refund_no = '".$refundNo."' limit 1)";
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $invArray = $result->fetch();
            $invoiceID = $invArray['invoice_id'];

            // get invoice item by invoice_id
            $sql = "select sales_order_item_id, qty_ordered from sales_invoice_item where invoice_id = " . $invoiceID;
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $invItem = $result->fetchAll();

                $sql = "select sales_order_item_id, qty_refunded from sales_return_refund_item where refund_no = '" . $refundNo . "'";
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if ($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $retrefItem = $result->fetchAll();

                    foreach ($invItem as $rowInvItem) {
                        $flagExact = false;
                        foreach ($retrefItem as $rowRetrefItem) {
                            if (($rowInvItem['sales_order_item_id'] == $rowRetrefItem['sales_order_item_id']) && ($rowInvItem['qty_ordered'] == $rowRetrefItem['qty_refunded'])) {
                                $flagExact = true;
                            }
                        }

                        if ($flagExact) {
                            $sql = "update sales_invoice_item set status_fulfillment = '" . $status . "' where sales_order_item_id = " . $rowInvItem['sales_order_item_id'];
                            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
                        }
                    }
                }
            }
        }
    }
}
