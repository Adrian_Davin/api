<?php

namespace Models;

use Phalcon\Db as Database;
use Phalcon\Mvc\Model;

class MasterAttributes extends \Models\BaseModel
{
    protected $attribute_id;

    protected $attribute_unit_id;

    protected $attribute_name;

    protected $attribute_label;

    protected $attribute_type;

    protected $is_variant;

    protected $is_visible_on_frontend;

    protected $is_filtering;

    protected $is_basic;

    protected $form_validation;

    protected $status;

    protected $created_at;

    protected $updated_at;

    protected $attribute_hint;

    protected $mandatory_attribute;

    protected $priority;

    protected $show_value;

    /**
     * @return mixed
     */
    public function getAttributeId()
    {
        return $this->attribute_id;
    }

    /**
     * @param mixed $attribute_id
     */
    public function setAttributeId($attribute_id)
    {
        $this->attribute_id = $attribute_id;
    }

    /**
     * @return mixed
     */
    public function getAttributeUnitId()
    {
        return $this->attribute_unit_id;
    }

    /**
     * @param mixed $attribute_unit_id
     */
    public function setAttributeUnitId($attribute_unit_id)
    {
        $this->attribute_unit_id = $attribute_unit_id;
    }

    /**
     * @return mixed
     */
    public function getAttributeName()
    {
        return $this->attribute_name;
    }

    /**
     * @param mixed $attribute_name
     */
    public function setAttributeName($attribute_name)
    {
        $this->attribute_name = $attribute_name;
    }

    /**
     * @return mixed
     */
    public function getAttributeLabel()
    {
        return $this->attribute_label;
    }

    /**
     * @param mixed $attribute_label
     */
    public function setAttributeLabel($attribute_label)
    {
        $this->attribute_label = $attribute_label;
    }

    /**
     * @return mixed
     */
    public function getAttributeType()
    {
        return $this->attribute_type;
    }

    /**
     * @param mixed $attribute_type
     */
    public function setAttributeType($attribute_type)
    {
        $this->attribute_type = $attribute_type;
    }

    /**
     * @return mixed
     */
    public function getisVariant()
    {
        return $this->is_variant;
    }

    /**
     * @param mixed $is_variant
     */
    public function setIsVariant($is_variant)
    {
        $this->is_variant = $is_variant;
    }

    /**
     * @return mixed
     */
    public function getisVisibleOnFrontend()
    {
        return $this->is_visible_on_frontend;
    }

    /**
     * @param mixed $is_visible_on_frontend
     */
    public function setIsVisibleOnFrontend($is_visible_on_frontend)
    {
        $this->is_visible_on_frontend = $is_visible_on_frontend;
    }

    /**
     * @return mixed
     */
    public function getisFiltering()
    {
        return $this->is_filtering;
    }

    /**
     * @param mixed $is_filtering
     */
    public function setIsFiltering($is_filtering)
    {
        $this->is_filtering = $is_filtering;
    }

    /**
     * @return mixed
     */
    public function getisBasic()
    {
        return $this->is_basic;
    }

    /**
     * @param mixed $is_basic
     */
    public function setIsBasic($is_basic)
    {
        $this->is_basic = $is_basic;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->$created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->$updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getAttributeHint()
    {
        return $this->attribute_hint;
    }

    /**
     * @param mixed $attribute_hint
     */
    public function setAttributeHint($attribute_hint)
    {
        $this->attribute_hint = $attribute_hint;
    }

    /**
     * @return mixed
     */
    public function getmandatoryAttribute()
    {
        return $this->mandatory_attribute;
    }

    /**
     * @param mixed $mandatory_attribute
     */
    public function setmandatoryAttribute($mandatory_attribute)
    {
        $this->mandatory_attribute = $mandatory_attribute;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->$priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getShowValue()
    {
        return $this->$show_value;
    }

    /**
     * @param mixed $show_value
     */
    public function setShowValue($show_value)
    {
        $this->show_value = $show_value;
    }

    /**
     * @return mixed
     */
    public function getFormValidation()
    {
        return $this->form_validation;
    }

    /**
     * @param mixed $form_validation
     */
    public function setFormValidation($form_validation)
    {
        $this->form_validation = $form_validation;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('attribute_id', 'Models\AttributeSet2Attribute', 'attribute_id', array('alias' => 'AttributeSet2Attribute'));
    }

    public function onConstruct()
    {
        parent::onConstruct();

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_attributes';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterSize[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterSize
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }
    
    public function attributeSet2Attribute($attributeSetId = 0){
        $sql = "
                SELECT 
                    *
                FROM
                    master_attributes
                WHERE
                    status > 0
                    AND attribute_id IN (
                        SELECT 
                            attribute_id
                        FROM
                            attribute_set_2_attribute
                        WHERE
                            attribute_set_id = ".$attributeSetId."
                            and status = 10
                    )
                ORDER BY is_basic DESC , attribute_label
        ";

        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if($result->numRows() > 0){
            $result->setFetchMode(Database::FETCH_ASSOC);
            $attributeList = $result->fetchAll();
            return $attributeList;
        }else{
            return array();
        }
    }

    public function attributeVariantSet2Attribute($attributeVariantSetId = 0){
        $sql = "
                SELECT 
                    *
                FROM
                    master_attributes
                WHERE
                    status > 0
                    AND attribute_id IN (
                        SELECT 
                            attribute_id
                        FROM
                            attribute_variant_set_2_attribute
                        WHERE
                            attribute_variant_set_id = ".$attributeVariantSetId."
                    )
                ORDER BY is_basic DESC , attribute_label
        ";

        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if($result->numRows() > 0){
            $result->setFetchMode(Database::FETCH_ASSOC);
            $attributeList = $result->fetchAll();
            return $attributeList;
        }else{
            return array();
        }
    }

    public function saveData($log_file_name = "error", $state = "")
    {
        try {
            if ($this->save() === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Failed when try to save data";

                if($transaction = $this->get()) {
                    $transaction->rollback(
                        "Cannot save data, rollback"
                    );

                    throw new \Exception("Rollback transaction");
                }

                return false;
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }

}
