<?php

namespace Models;

class SalesruleOrderPackage extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $salesrule_order_package_id;


    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $rule_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $sales_order_id;


     /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $salesrule_package_id;

     /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $package_qty;
  

    /**
     * @return int
     */
    public function getSalesruleOrderPackageID()
    {
        return $this->salesrule_order_package_id;
    }

    /**
     * @param int $salesrule_order_package_id
     */
    public function setSalesruleOrderPackageID($salesrule_order_package_id)
    {
        $this->salesrule_order_package_id = $salesrule_order_package_id;
    }


    /**
     * @return int
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * @param int $rule_id
     */
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;
    }


    /**
     * @return int
     */
    public function getSalesOrderID()
    {
        return $this->sales_order_id;
    }

     /**
     * @param int $sales_order_id
     */
    public function setSalesOrderID($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }


      /**
     * @return int
     */
    public function getSalesrulePackageID()
    {
        return $this->salesrule_package_id;
    }

     /**
     * @param int $salesrule_package_id
     */
    public function setSalesrulePackageID($salesrule_package_id)
    {
        $this->salesrule_package_id = $salesrule_package_id;
    }

      /**
     * @return int
     */
    public function getPackageQty()
    {
        return $this->package_qty;
    }

    /**
     * @param int $package_qty
     */
    public function setPackageQty($package_qty)
    {
        $this->package_qty = $package_qty;
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_order_package';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
       
    }

    public function onConstruct()
    {
        parent::onConstruct();

    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleOrderPackage[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleOrderPackage
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

     public function saveData($log_file_name = "error", $state = "")
    {
        try {
            if ($this->save() === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Failed when try to save data";

                if($transaction = $this->get()) {
                    $transaction->rollback(
                        "Cannot save data, rollback"
                    );

                    throw new \Exception("Rollback transaction");
                }

                return false;
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }
}
