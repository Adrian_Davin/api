<?php

namespace Models;

// use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
// use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
// use Phalcon\Db as Database;

class TokopediaOrder extends \Models\BaseModel
{
    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }
    
    public function getOrderTokopedia($params = array())
    {
        $columnCount = "count(sov.order_no_vendor) as total";
        $columnData = "sov.order_no, coalesce(si.invoice_no,'') as invoice_no, sov.order_no_vendor, 
        sov.order_status, sov.paid_at as created_at, sov.ship_at as auto_cancel_at, ms.shop_name";

        $from = "sales_order_vendor sov
        inner join master_store_vendor ms on sov.shop_id = ms.shop_id
        inner join sales_order so on sov.order_no = so.order_no
        left join sales_invoice si on si.sales_order_id = so.sales_order_id";

        $whereClause = "vendor_name = 'Tokopedia' and sov.order_no != ''";
        if(isset($params['orderno'])){
            $whereClause .=" and sov.order_no like '%".$params['orderno']."%'";
        }
        
        if(isset($params['invoiceno'])){
            $whereClause .=" and si.invoice_no like '%".$params['invoiceno']."%'";
        }
        
        if(isset($params['company_code'])){
            $companyCodeArray = explode(",", strtoupper($params['company_code']));
            if (!in_array("ODI", $companyCodeArray)) {
                if (count($companyCodeArray) > 1) {
                    $whereClause .= " and (";
                    $loopLike = '';
                    foreach($companyCodeArray as $valCompanyCode) {
                        $loopLike .= "ms.company_code = '{$valCompanyCode}' OR ";
                    }
                    $loopLike = rtrim($loopLike," OR ");
                    $whereClause .= $loopLike.")";
                } else {
                    $whereClause .= "and ms.company_code = '{$companyCodeArray[0]}'";
                }
            }
        }

        if(isset($params['ordernovendor'])){
            $whereClause .=" and sov.order_no_vendor like '%".$params['ordernovendor']."%'";
        }
        
        if(isset($params['orderstatus'])){
            $whereClause .=" and sov.order_status like '%".$params['orderstatus']."%'";
        }
        
        if(isset($params['createdat'])){
            $whereClause .=" and sov.paid_at like '%".$params['createdat']."%'";
        }
        
        if(isset($params['autocancelat'])){
            $whereClause .=" and sov.ship_at like '%".$params['autocancelat']."%'";
        } 
        
        if(isset($params['shopname'])){
            $whereClause .=" and ms.shop_name like '%".$params['shopname']."%'";
        }

        if($params['status'] == "waiting") {
            $whereClause .= " and order_status in ('READY_TO_SHIP', 'VERIFIED_ORDER')";
        } else if($params['status'] == "processing") {
            $whereClause .= " and order_status in ('SHIPPED','RECEIVED','REQUESTED_CANCEL_BY_CUSTOMER')";
        } else if($params['status'] == "done") {
            $whereClause .= " and order_status in ('COMPLETE','CANCEL_BY_TOKOPEDIA','INCOMPLETE')";
        }                

        $sortBy = "sov.ship_at asc";

        $queryData = "SELECT $columnData FROM $from WHERE $whereClause ORDER BY $sortBy LIMIT {$params['limit']} OFFSET {$params['offset']}";
        $result = $this->getDi()->getShared('dbMaster')->query($queryData);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $orders['data'] = $result->fetchAll();

        $queryCount = "SELECT $columnCount FROM $from WHERE $whereClause";
        $result = $this->getDi()->getShared('dbMaster')->query($queryCount);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $countArray = $result->fetch();
        $orders['count'] = $countArray['total'];

        return $orders;
    }
          
    public function getDataEscrowTokopedia($params = array()){

        $sql = "
            select so.order_no as 'Order No', so.order_no_vendor as 'Order Tokopedia No', so.total_amount as 'Total Amount', 
                    coalesce(se.voucher,0) as 'Voucher', 
                    coalesce(se.voucher_seller,0) as 'Voucher Seller', 
                    coalesce(se.seller_rebate,0) as 'Seller Rebate', so.shipping_fee as 'Actual Shipping Cost', 
                    coalesce(se.shipping_fee_rebate,0) as 'Shipping Fee Rebate', 
                    coalesce(se.commision_fee,0) as 'Commission Fee', 
                    coalesce(so.escrow_amount,0) as 'Escrow Amount', so.updated_at as 'Order Date', 
                    coalesce(se.voucher_name,'') as 'Voucher Name', 
                    coalesce(se.voucher_code,'') as 'Voucher Code', 
                    coalesce(se.coin,0) as Coin
            from sales_order_vendor so  
            left join sales_order_vendor_escrow se
            on se.ordersn = so.order_no_vendor
            where so.created_at between '".$params['startdate']."' AND '".$params['enddate']."' and so.vendor_name = 'Tokopedia' and so.order_status= 'COMPLETED' order by so.updated_at
        ";
            
        $result = $this->getDi()->getShared('dbMaster')->query($sql);
        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );
        $orders = $result->fetchAll();

        return $orders;
    }
}