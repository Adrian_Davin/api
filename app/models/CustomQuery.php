<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomQuery extends \Models\BaseModel
{


    public function initialize()
    {

    }

    public function marketingRuleStatus()
    {

        try {
            $now = date("Y-m-d");
            $this->useReadOnlyConnection();

            $delete = "DELETE FROM batch_sales_rule_enable_disable WHERE to_date > '" . $now . "'";
            $result = $this->getDi()->getShared($this->getConnection())->query($delete);

            $getBatchStatusList = "SELECT * FROM batch_sales_rule_enable_disable WHERE from_date >= '" . $now . "' AND to_date <= '" . $now . "'";
            $resultGBSL = $this->getDi()->getShared($this->getConnection())->query($getBatchStatusList);

            if ($resultGBSL->numRows() > 0) {
                $resultGBSL->setFetchMode(Database::FETCH_ASSOC);
                $hasilGBSL = $resultGBSL->fetchAll();

                foreach ($hasilGBSL as $gbsl) {
                    $updateSalesRule = "UPDATE salesrule SET status = '" . $gbsl['status'] . "' WHERE rule_id IN (" . $gbsl['rule_ids'] . ")";
                    $result = $this->getDi()->getShared($this->getConnection())->query($updateSalesRule);
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return $result;

    }

    public function customInventoryAfterBatchStock(){
        try {

            //Stock jabodetabek and zone 1 only  19 Mar 2018 - 31 Des 2025
            $now = date("Ymd");
            if($now >= 20180319 AND $now <= 20251231){
                $stockJabodetabeckOnly =
                    "UPDATE product_stock
                        SET qty = 0
                        WHERE sku IN ('10041956','10041957')
                        AND store_code NOT IN (
                          'J347','J365','J354','J430','J445','J436','H322','H394','J364','J383','H333','H301','H302','J350','J349','H324','J315','J317','J384','H343','J432','J460','J311','J409','J406','H369','H370','J444','H372','J378','H376','J454','H978','H979','J414','J321','H318','H323','J346','H330','H365'
                        )";
                $this->getDi()->getShared($this->getConnection())->query($stockJabodetabeckOnly);
            }

            /*
             * Promo Early Bird berikut 29 maret 2018 sampe 30 april 2018
             */
            if($now >= 20180329 AND $now <= 20180430) {
                $query = "UPDATE product_stock SET qty = '10'
                          WHERE store_code = 'J347'
                          AND sku IN (
                            'EABR0002','EABR0003','EABR0004','EABR0005','EABR0006','EABR0007','EABR0008','EABR0009','EABR0010','EABR0011','EABR0012','EABR0013','EABR0014','EABR0015','EABR0016','EABR0017','10082347-pre','X182918-pre','10137241-pre'
                          )";
                $this->getDi()->getShared($this->getConnection())->query($query);

                $query = "UPDATE product_stock SET qty = '20'
                          WHERE store_code = 'J347'
                          AND sku IN (
                            '10125611-pre','10125612-pre','10125613-pre','10125614-pre','10125615-pre','10125616-pre','10082348-pre','X182916-pre','X182917-pre','X182909-pre','X182912-pre','X182915-pre','X122911-pre','10035348-pre','10035349-pre','10035350-pre','10035351-pre','X180209-pre','X180213-pre','10119431-pre','10080960-pre','10080961-pre','10119430-pre'
                          )";
                $this->getDi()->getShared($this->getConnection())->query($query);

                $query = "UPDATE product_stock SET qty = '100'
                          WHERE store_code = 'J347'
                          AND sku IN (
                              '10076811-pre','10076882-pre','10090425-pre','EABR0001'
                          )";
                $this->getDi()->getShared($this->getConnection())->query($query);
            }

            /*
             * Stock Manual Olympus 180412 sampai 12 Apr � 31 Des 2018
             */
            if($now >= 20180412 AND $now <= 20181231) {
                $query = "UPDATE product_stock SET qty = '100'
                          WHERE store_code = 'J347'
                          AND sku IN (
                              'HCIOLY12'
                          )";
                $this->getDi()->getShared($this->getConnection())->query($query);
            }

            /*
             * Update stock service 999 everyday
             */
            $query = "UPDATE product_stock SET qty = '999' WHERE sku IN (
                    '70000788','70000789','70000790','70000791','70000792','70000826','70000827','70000828','70000829','70000830','70000831','70000832','70002374','70002375','70002611','70002612','70002613','70002615'
                ) AND store_code = 'DC'";
            $this->getDi()->getShared($this->getConnection())->query($query);


        } catch (\Exception $e) {

            return false;
        }

        return true;

    }

}
