<?php

namespace Models;

class ProductLabels extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_label_id;
    protected $name;
    protected $url_cloudinary;
    protected $sku;
    protected $old_sku;
    protected $additional_specification;
    protected $priority;
    protected $company_code;
    protected $start_date;
    protected $end_date;
    protected $sync_marketing;
    protected $status;
    protected $created_at;
    protected $updated_at;
    protected $rule_id;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getProductLabelId()
    {
        return $this->product_label_id;

    }

    /**
     * @param int $product_label_id
     */
    public function setProductLabelId($product_label_id)
    {
        $this->product_label_id = $product_label_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUrlCloudinary()
    {
        return $this->url_cloudinary;
    }

    /**
     * @param mixed $url_cloudinary
     */
    public function setUrlCloudinary($url_cloudinary)
    {
        $this->url_cloudinary = $url_cloudinary;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getOldSku()
    {
        return $this->old_sku;
    }

    /**
     * @param mixed $old_sku
     */
    public function setOldSku($old_sku)
    {
        $this->old_sku = $old_sku;
    }
    /**
     * @return mixed
     */
    public function getAdditionalSpecification()
    {
        return $this->additional_specification;
    }

    /**
     * @param mixed $additional_specification
     */
    public function setAdditionalSpecification($additional_specification)
    {
        $this->additional_specification = $additional_specification;
    }
    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param mixed $company_code
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getSyncMarketing()
    {
        return $this->sync_marketing;
    }

    /**
     * @param mixed $sync_marketing
     */
    public function setSyncMarketing($sync_marketing)
    {
        $this->sync_marketing = $sync_marketing;
    }

       /**
     * @return mixed
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * @param mixed $rule_id
     */
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_labels';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setLabelFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function checkLabelForPost(){
        /**
         * TODO : Kalau suatu saat add lagi nama yang sudah di delete. Bagaimana ?
         */

        $labelName = $this->findFirst(
            [
                "conditions" => "name ='".$this->name."' AND status >-1",
            ]
        );

        if(!empty($labelName)){
            $this->errorCode = "RR001";
            $this->errorMessages ="Label's name has already existed";
            return;
        }

    }

    public function checkLabelForUpdate(){

        $labelCondition = $this->findFirst(
            [
                "conditions" => "product_label_id ='" . $this->product_label_id . "'",
            ]
        );

        $labelExist = $this->findFirst(
            [
                "columns" => "name",
                "conditions" => "name='".$this->name."' AND product_label_id!='".$this->product_label_id."' AND status >-1",
            ]
        );

        if(!empty($labelCondition)){
            if(!empty($labelExist)){
                $this->errorCode = "RR001";
                $this->errorMessages ="Label name has already existed";
                return;
            }
        }
        else {
            $this->errorCode = "RR001";
            $this->errorMessages = "label_id is not exist";
            return;
        }

    }

}
