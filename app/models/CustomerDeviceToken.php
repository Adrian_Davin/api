<?php

namespace Models;


use phalcon\Db as Database;

class CustomerDeviceToken extends BaseModel
{

    protected $id;
    protected $customer_id;
    protected $device_token;
    protected $os;
    protected $unique_id;
    protected $city;
    protected $last_visit;
    protected $created_at;
    protected $updated_at;
    protected $is_exist;
    protected $ODI;
    protected $AHI;
    protected $HCI;

    public function initialize()
    {
    }

    // GET
    public function getCustomerDeviceTokenByCustomerId($customerId = FALSE)
    {
        $custDTArray = array();
        if ($customerId) {
            $sql = "SELECT * FROM customer_device_token WHERE customer_id = $customerId AND bu_code = 'ODI';";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $custDTArray = $result->fetchAll();
            }
        }

        return $custDTArray;
    }

    public function getCDTByCustIdForPushNotif($customerId = FALSE)
    {
        $custDTArray = array();
        if ($customerId) {
            $sql = "SELECT * FROM customer_device_token WHERE customer_id = $customerId AND is_exist = 10 AND os != 'mac-safari';";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $custDTArray = $result->fetchAll();
            }
        }

        return $custDTArray;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCustomerId()
    {
        return $this->customer_id;
    }

    public function getDeviceToken()
    {
        return $this->device_token;
    }

    public function getOs()
    {
        return $this->os;
    }

    public function getUniqueId()
    {
        return $this->unique_id;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getLastVisit()
    {
        return $this->last_visit;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getIsExist()
    {
        return $this->is_exist;
    }

    public function getODI()
    {
        return $this->ODI;
    }

    public function getAHI()
    {
        return $this->AHI;
    }

    public function getHCI()
    {
        return $this->HCI;
    }

    // SET
    public function setId($id)
    {
        return $this->id = $id;
    }

    public function setCustomerId($customer_id)
    {
        return $this->customer_id = $customer_id;
    }

    public function setDeviceToken($device_token)
    {
        return $this->device_token = $device_token;
    }

    public function setOs($os)
    {
        return $this->os = $os;
    }

    public function setUniqueId($unique_id)
    {
        return $this->unique_id = $unique_id;
    }

    public function setCity($city)
    {
        return $this->city = $city;
    }

    public function setLastVisit($last_visit)
    {
        return $this->last_visit = $last_visit;
    }

    public function setCreatedAt($created_at)
    {
        return $this->created_at = $created_at;
    }

    public function setUpdatedAt($updated_at)
    {
        return $this->updated_at = $updated_at;
    }

    public function setIsExist($is_exist)
    {
        return $this->is_exist = $is_exist;
    }

    public function setODI($ODI)
    {
        return $this->ODI = $ODI;
    }

    public function setAHI($AHI)
    {
        return $this->AHI = $AHI;
    }

    public function setHCI($HCI)
    {
        return $this->HCI = $HCI;
    }
}
