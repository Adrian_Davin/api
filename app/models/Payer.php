<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/13/2017
 * Time: 2:38 PM
 */

namespace Models;


class Payer
{
    protected $first_name;
    protected $last_name;
    protected $full_address;
    protected $postcode;
    protected $city;
    protected $province;
    protected $npwp;
    protected $email;

    public function initialize()
    {
        //$this->setSource("sales_order_payer");
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if(property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public function toArray($columns = array(), $showEmpty = false)
    {
        $thisArray = get_object_vars($this);

        $view = array();
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) != 'object') {
                $view[$key] = $val;
            }
        }

        if(!$showEmpty) {
            foreach($view as $key => $val) {
                if(empty($val)) {
                    unset($view[$key]);
                }
            }
        }

        return $view;
    }

}