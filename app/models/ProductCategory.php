<?php

namespace Models;

use Helpers\RouteHelper;
use Library\Category;
use Library\Nsq;
use Library\Redis;
use Library\Redis\CategoryTree;
use Phalcon\Db;

class ProductCategory extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $category_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $parent_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $name;

    protected $title;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $meta_title;
    protected $article_hierarchy;
    protected $terminology;
    
    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $meta_keywords;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $meta_description;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $category_key;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $url_key;
    protected $old_url_key;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $page_layout;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $is_anchor;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $include_in_menu;

        /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $start_time;

        /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $end_time;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $image;

    protected $company_code;

    protected $hexa_code;

    protected $vm_most_sales;
    protected $vm_most_click;
    protected $vm_add_to_cart;
    protected $vm_slow_moving;
    protected $vm_new_arrival;
    protected $vm_special_price;
    protected $attribute_set_id;
    protected $attribute_variant_set_id;
    protected $tags;

    protected $brand_id;

    protected $index_setup;

    protected $canonical_url;


    /**
     * Get the value of hexa_code
     */ 
    public function getHexaCode()
    {
        return $this->hexa_code;
    }

    /**
     * Set the value of hexa_code
     *
     * @return  self
     */ 
    public function setHexaCode($hexa_code)
    {
        $this->hexa_code = $hexa_code;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $icon;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $display_mode;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $description;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $children;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $all_children;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $children_count;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $level;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $position;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    protected $path;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $best_seller;
    protected $status;
    protected $additional_header;
    protected $header_description;
    protected $header_description_mobile;
    protected $footer_description;
    protected $show_thumbnail;
    protected $thumbnail;
    protected $thumbnail_mobile;
    protected $url_path_image;
    protected $old_parent_path;
    protected $parent_path;
    protected $inline_script;
    protected $inline_script_seo;
    protected $inline_script_seo_mobile;
    protected $is_rule_based;
    protected $filter_condition;
    protected $rule_based_hero_products;
    protected $mini_category_banner;
    protected $exclude_category;
    protected $exclude_sku;
    protected $show_on_app;


    public function getShowOnApp()
    {
        return $this->show_on_app;
    }

    public function setShowOnApp($show_on_app)
    {
        $this->show_on_app = $show_on_app;

        return $this;
    }
    
        /**
     * Get the value of tags
     */ 
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set the value of tags
     *
     * @return  self
     */ 
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }
    
    /**
     * @return int
     */
    public function getBestSeller()
    {
        return $this->best_seller;
    }

    /**
     * @param int $best_seller
     */
    public function setBestSeller($best_seller)
    {
        $this->best_seller = $best_seller;
    }

    /**
     * @return mixed
     */
    public function getHeaderDescriptionMobile()
    {
        return $this->header_description_mobile;
    }

    /**
     * @param mixed $header_description_mobile
     */
    public function setHeaderDescriptionMobile($header_description_mobile)
    {
        $this->header_description_mobile = $header_description_mobile;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptSeo()
    {
        return $this->inline_script_seo;
    }

    /**
     * @param mixed $inline_script_seo
     */
    public function setInlineScriptSeo($inline_script_seo)
    {
        $this->inline_script_seo = $inline_script_seo;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptSeoMobile()
    {
        return $this->inline_script_seo_mobile;
    }

    /**
     * @param mixed $inline_script_seo_mobile
     */
    public function setInlineScriptSeoMobile($inline_script_seo_mobile)
    {
        $this->inline_script_seo_mobile = $inline_script_seo_mobile;
    }

    /**
     * @return mixed
     */
    public function getExcludeCategory()
    {
        return $this->exclude_category;
    }

    /**
     * @param mixed $exclude_category
     */
    public function setExcludeCategory($exclude_category)
    {
        $this->exclude_category = $exclude_category;
    }

    /**
     * @return mixed
     */
    public function getExcludeSku()
    {
        return $this->exclude_sku;
    }

    /**
     * @param mixed $exclude_sku
     */
    public function setExcludeSku($exclude_sku)
    {
        $this->exclude_sku = $exclude_sku;
    }

    protected $url_path;


    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $errors;
    protected $whiteList;
    /**
     * @return mixed
     */
    public function getHeaderDescription()
    {
        return $this->header_description;
    }

    /**
     * @param mixed $header_description
     * @return ProductCategory
     */
    public function setHeaderDescription($header_description)
    {
        $this->header_description = $header_description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFooterDescription()
    {
        return $this->footer_description;
    }

    /**
     * @param mixed $footer_description
     * @return ProductCategory
     */
    public function setFooterDescription($footer_description)
    {
        $this->footer_description = $footer_description;

        return $this;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('category_id', 'Product2Category', 'category_id', array('alias' => 'Product2Category'));
    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->whiteList = array();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_category';
    }

    /**
     * @return int
     */
    public function getIncludeInMenu()
    {
        return $this->include_in_menu;
    }

    /**
     * @param int $include_in_menu
     */
    public function setIncludeInMenu($include_in_menu)
    {
        $this->include_in_menu = $include_in_menu;
    }

    /**
     * @return int
     */
    public function getStartTime()
    {
        return $this->start_time;
    }

    /**
     * @param int $start_time
     */
    public function setStartTime($start_time)
    {
        $this->start_time = $start_time;
    }

    /**
     * @return int
     */
    public function getEndTime()
    {
        return $this->end_time;
    }

    /**
     * @param int $end_time
     */
    public function setEndTime($end_time)
    {
        $this->end_time = $end_time;
    }

    /**
     * @return mixed
     */
    public function getArticleHierarchy()
    {
        return $this->article_hierarchy;
    }

    /**
     * @param mixed $article_hierarchy
     */
    public function setArticleHierarchy($article_hierarchy)
    {
        $this->article_hierarchy = $article_hierarchy;
    }


    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUrlPathImage()
    {
        return $this->url_path_image;
    }

    /**
     * @param mixed $url_path_image
     */
    public function setUrlPathImage($url_path_image)
    {
        $this->url_path_image = $url_path_image;
    }


    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return ProductCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getisRuleBased()
    {
        return $this->is_rule_based;
    }

    /**
     * @param mixed $is_rule_based
     */
    public function setIsRuleBased($is_rule_based)
    {
        $this->is_rule_based = $is_rule_based;
    }

    /**
     * @return mixed
     */
    public function getRuleBasedHeroProducts()
    {
        return $this->rule_based_hero_products;
    }

    /**
     * @param mixed $rule_based_hero_products
     */
    public function setRuleBasedHeroProducts($rule_based_hero_products)
    {
        $this->rule_based_hero_products = $rule_based_hero_products;
    }

    /**
     * @return mixed
     */
    public function getFilterCondition()
    {
        return $this->filter_condition;
    }

    /**
     * @param mixed $filter_condition
     */
    public function setFilterCondition($filter_condition)
    {
        $this->filter_condition = $filter_condition;
    }

    /**
     * Get the value of company_code
     */ 
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * Set the value of company_code
     *
     * @return  self
     */ 
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

        /**
     * Get the value of mini_category_banner
     */ 
    public function getMiniCategoryBanner()
    {
        return $this->mini_category_banner;
    }

    /**
     * Set the value of mini_category_banner
     *
     * @return  self
     */ 
    public function setMiniCategoryBanner($mini_category_banner)
    {
        $this->mini_category_banner = $mini_category_banner;
    }

    /**
     * Get the value of index_setup
     */ 
    public function getIndexSetup()
    {
        return $this->index_setup;
    }

    /**
     * Set the value of index_setup
     *
     * @return  self
     */ 
    public function setIndexSetup($index_setup)
    {
        $this->index_setup = $index_setup;
    }
    
    /**
     * Get the value of canonical_url
     * 
     * @return  self
     */ 
    public function getCanonicalURL()
    {
        return $this->canonical_url;
    }

    /**
     * Set the value of canonical_url
     *
     * @return  self
     */ 
    public function setCanonicalURL($canonical_url)
    {
        $this->canonical_url = $canonical_url;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductCategory[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductCategory
     */
    public static function findFirst($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        foreach($data_array as $key => $val) {
            if(property_exists($this, $key)) {
                $this->{$key} = $val;
                // input is send but value is empty, we still need to update that fields
                if($val === 0 || $val == "") {
                    $this->whiteList[] = $key;
                }
            }
        }
    }

    public function setFilterToJson($data_array = array())
    {
        $filter = [];
        foreach($data_array as $key => $val) {
            if(substr( $key, 0, 9 ) === "filter_by") {
                $filter[$key] = $val;
            }
        }
        
        $this->filter_condition =  json_encode($filter);
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        $master_url_model = new MasterUrlKey();
        $parameter = [
            "conditions" => "reference_id = " . $this->category_id ." AND section = 'category'",
        ];
        $urlKey = $master_url_model::findFirst($parameter);
        $thisUrlKey = "";
        if($urlKey) {
            $thisUrlKey = $urlKey->getUrlKey().".html";
        }
        $view['url_key'] = $thisUrlKey;

        return $view;
    }

    public function getCategory($data_array = array())
    {
        $query_param = "";
        $bind_param = array();

        foreach($data_array as $keyData => $valData)
        {
            $query_param .= $keyData ." = :".$keyData.": AND ";
            $bind_param[$keyData] = $valData;
        }

        // Remove AND statement at the end of string
        $query_param = substr(trim($query_param), 0,-3);

        $param = array(
            $query_param,
            "bind" => $bind_param
        );

        try {
            $this->useReadOnlyConnection();
            $result = self::find($param);
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support.",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }
        return $result->toArray();
    }

    public function buildUpdateParam()
    {
        $thisArray = get_object_vars($this);

        unset($thisArray['category_id']);

        $db_params = "";
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) != 'object' && (!empty($val) || $val == '0' || in_array($key,$this->whiteList) ) && $key[0] !== "_" && $key!="whiteList") {
                $val = (is_array($val)) ? json_encode($val) : $val;
                $db_params .= $key . " = \"". str_replace('"','\"',$val) . "\", ";
            }
        }

        $db_params = substr($db_params,0,-2);

        return $db_params;

    }

    public function updateData()
    {
        $date = date('Y-m-d H:i:s');
        $companyCode = (!empty($this->company_code)) ? $this->company_code : 'ODI';
        //root parent for inspiration only
        $root = explode('/',$this->path);
        $root = $root[0];
        $section = 'category';
        if ($this->is_rule_based == 1) {
           $section = 'custom_category';
        }

        if(empty($this->category_id)) { // save
            $this->created_at = $date;

            if($this->parent_id == '0'){ // insert new root
                // check again if url key exist
                $routeHelper = new RouteHelper();
                $message = $routeHelper->checkUrlKey("","category",$this->url_key,$companyCode);
                if($message['url_key_status'] == 0){
                    $this->errors[] = "Save category failed, Please check url key.";
                    return array("Save category failed, Please check url key");
                }
            }
            else { // if sub-category
                // check again if url key exist
                $routeHelper = new RouteHelper();
                $message = $routeHelper->checkUrlKey("","category",$this->url_path,$companyCode);
                if($message['url_key_status'] == 0){
                    $this->errors[] = "Save category failed, Please check url key.";
                    return array("Save category failed, Please check url key");
                }
            }
           
            if(!empty($this->brand_id)){
                $sql = "SELECT * FROM product_category_mapping_brand pcmb JOIN product_category pc ON pcmb.category_id = pc.category_id 
                WHERE brand_id = ".$this->brand_id." AND pcmb.company_code = '".$this->company_code."' AND parent_id != 5808 AND parent_id != 8906 AND pc.status = 10";
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $validateBrand = $result->fetchAll();
                if (!empty($validateBrand)) {
                    $this->errors[] = "Save category failed, Brand ID has been used.";
                    return array("Save category failed, Brand has been used");
                }
            }

            if ($this->create() === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("product_catalog", "Product category save failed, error : " . json_encode($errMsg));
                $this->errors[] = "Save category data failed, please try agian";
            } else {

                if(!empty($this->brand_id)) {
                    $categoryMappingModel = new ProductCategoryMappingBrand();
                    $categoryMappingData = array(
                        'brand_id' => $this->brand_id,
                        'category_id' => $this->category_id,
                        'company_code' => $this->company_code
                    );
                    $categoryMappingModel->setFromArray($categoryMappingData);
                    $categoryMappingModel->saveData();

                    $redisKey = "brand_url_".$this->brand_id."_".$companyCode;
                    $redis = new \Library\Redis();
                    $redisCon = $redis::connect();
                    $redisResponse = $redisCon->get($redisKey);
    
                    if (!empty($redisResponse)) {
                        $redisCon->delete($redisKey);
                    }
                }

                if($this->parent_id=='0'){
                    $urlData = array(
                        'url_key' => $this->url_key,
                        'section' => $section,
                        'inline_script' => $this->inline_script,
                        'inline_script_seo' => $this->inline_script_seo,
                        'inline_script_seo_mobile' => $this->inline_script_seo_mobile,
                        'additional_header' => $this->additional_header,
                        'document_title' => "Jual ".$this->name." | Ruparupa",
                        'reference_id' => $this->category_id,
                        'company_code' => $companyCode
                    );

                    $urlModel = new MasterUrlKey();

                    $urlModel->setFromArray($urlData);
                    $urlModel->saveUrlKey("masterUrlKey");

                    $query = "UPDATE \Models\ProductCategory SET ";
                    $query .= "path = '".$this->category_id."'";
                    $query .= " WHERE category_id = " . $this->category_id;
                    $query .= " AND company_code = '".$companyCode."'";
                    $manager = $this->getModelsManager();
                    $manager->executeQuery($query);
                }
                else{
                    $urlData = array(
                        'url_key' => $this->url_path,
                        'section' => $section,
                        'inline_script' => $this->inline_script,
                        'inline_script_seo' => $this->inline_script_seo,
                        'inline_script_seo_mobile' => $this->inline_script_seo_mobile,
                        'additional_header' => $this->additional_header,
                        'document_title' => "Jual ".$this->name." | Ruparupa",
                        'reference_id' => $this->category_id,
                        'company_code' => $companyCode // here
                    );

                    $urlModel = new MasterUrlKey();

                    $urlModel->setFromArray($urlData);
                    $urlModel->saveUrlKey("masterUrlKey");

                    $query = "UPDATE \Models\ProductCategory SET ";
                    $query .= "path = CONCAT(path,'/".$this->category_id."')";
                    $query .= " WHERE category_id = " . $this->category_id;
                    $query .= " AND company_code = '".$companyCode."'";
                    $manager = $this->getModelsManager();
                    $manager->executeQuery($query);

                    // update parent children count
                    $updateChildCount = $this->updateParentChildrenCount(array('category_id' => $this->category_id));
                }

                if($root == '2984' || $root == getEnv('INSPIRATION_AHI') || $root == getEnv('INSPIRATION_HCI') || $root == getEnv('INSPIRATION_TGI')){
                    //meaning it's inspiration
                    $inspirationLib = new \Library\Inspiration();
                    $inspirationLib->deleteInspirationRedis($companyCode);

                    $categoryLib = new \Library\Category();
                    $categoryLib->deleteCustomCategoryRedis();                    
                } else {
                    if ($root == getEnv('INSPIRATION_PROMO_BRAND_ODI') || $root == getEnv('INSPIRATION_PROMO_BRAND_AHI') || $root == getEnv('INSPIRATION_PROMO_BRAND_HCI')|| $root == getEnv('INSPIRATION_PROMO_BRAND_TGI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_brand_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_GET_MORE_SALE_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_get_more_sale_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PAYDAY_SURPRISE_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_payday_surprise_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_ALL_OFFICE_NEEDS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_all_office_needs_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_FURNITURE_FESTIVAL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_furniture_festival_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_ALL_OFFICE_NEEDS_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_all_office_needs_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_EARTH_DAY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_earth_day_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_TOYS_KINGDOM_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_toys_kingdom_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_HARI_RAYA_SERU_DI_RUMAH_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_hari_raya_seru_di_rumah_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_LEBARAN_PUASA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_lebaran_puasa_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_CORONA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_corona_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_COVID_19_SURVIVAL_KIT_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_covid_19_survival_kit_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_HEALTHY_LIVING_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_healthy_living_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_FURNIFEST_FESTIVAL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_furnifest_festival_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_HEALTHY_LIVING_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_healthy_living_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_FURNIFEST_FESTIVAL_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_furnifest_festival_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_INSPIRATION_FURNIFEST_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_inspiration_furnifest_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_RAK_DAN_PENYIMPANAN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_rak_dan_penyimpanan_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRAND_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brand_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_TRAVELING_HOLIDAY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_traveling_holiday_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_ALL_ABOUT_HOBBIES_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_all_about_hobbies_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_GET_MORE_SALE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_get_more_sale_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMOTIONS_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promotions_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_BRAND_MICROSITE_DETAIL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_brand_microsite_detail_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MICROSITE_DETAIL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_microsite_detail_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRAND_LACASSA_DETAIL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brand_lacassa_detail_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRANDS_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brands_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRANDS_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brands_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_NATAL_DAN_AKHIR_TAHUN_DI_RUMAH_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_natal_dan_akhir_tahun_di_rumah_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PILIH_NUANSA_NATAL_2020_DI_RUMAHMU_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_pilih_nuansa_natal_2020_di_rumahmu_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_BRAND_MICROSITE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_brand_microsite_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BOOM_SALE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_boom_sale_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MAKAN_DAN_HIDUP_SEHAT_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_makan_dan_hidup_sehat_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_PERFECT_WORKSPACE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_perfect_workspace_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_CHOOSE_THE_LOOK_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_choose_the_look_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_COMPLETE_YOUR_KITCHEN_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_complete_your_kitchen_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_DECOR_STORAGE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_decor_storage_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_FESTIVE_MOMENT_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_festive_moment_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_COFFEE_AND_BAKE_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_coffee_and_bake_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_FURNITURE_AND_DECORATION_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_furniture_and_decoration_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_COOKING_MICROSITE_KITCHEN_FAIR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_cooking_microsite_kitchen_fair_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_YOUR_COMFORT_ZONE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_your_comfort_zone_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_SOLUSI_RUMAH_RAPI_DAN_NYAMAN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_solusi_rumah_rapi_dan_nyaman_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_BARANG_WAJIB_UNTUK_SETIAP_RUANGAN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_barang_wajib_untuk_setiap_ruangan_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_WOW_SALE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_wow_sale_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_HOME_AND_LIVING_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_home_and_living_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_EVERYDAY_PROBLEMS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_everyday_problems_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_HOBBIES_LIFESTYLE_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_hobbies_lifestyle_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_JOYFUL_MOMENTS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_joyful_moments_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_YOUR_BUSINESS_COMPANION_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_your_business_companion_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_5TH_ANNIVERSARY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_5th_anniversary_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_WOW_SALE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_wow_sale_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_INSPIRASI_MICROSITE_TEKSTIL_UNTUK_HUNIAN_ANDA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_inspirasi_microsite_tekstil_untuk_hunian_anda_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_HOME_LIVING_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_home_living_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_KITCHEN_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_kitchen_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_HOBBIES_LIFESTYLE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_hobbies_lifestyle_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_JOYOUS_MOMENT_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_joyous_moment_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_AUTOMOTIVE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_automotive_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_TETAP_SERU_DI_RUMAH_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_tetap_seru_di_rumah_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_SUSEN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_susen_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_INFORMA_17TH_ANNIVERSARY_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_informa_17th_anniversary_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_NEW_TRENDS_17TH_ANNIVERSARY_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_new_trends_17th_anniversary_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_SHOP_IN_SHOP_FURNITUR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_shop_in_shop_furnitur_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_ATARU_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_ataru_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_RAWAT_DAN_LENGKAPI_KENDARAAN_ANDA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_rawat_dan_lengkapi_kendaraan_anda_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_HOBBY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_hobby_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_SHOP_IN_SHOP_RUMAH_TANGGA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_shop_in_shop_rumah_tangga_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_HOT_CLEARANCE_ACE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_hot_clearance_ace_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_ASHLEY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_ashley_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_INSPIRATION_PROMO_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_inspiration_promo_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_INSPO_PROMO_AKHIR_TAHUN_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_inspo_promo_akhir_tahun_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_PROMO_AKHIR_TAHUN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_promo_akhir_tahun_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_KOREAN_HOME_DECOR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_korean_home_decor_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_SHOP_IN_SHOP_PET_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_shop_in_shop_pet_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_GIFT_IDEAS_1_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_gift_ideas_1_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_GIFT_IDEAS_2_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_gift_ideas_2_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_GIFT_IDEAS_3_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_gift_ideas_3_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_BEST_GIFT_IDEAS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_best_gift_ideas_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MAJOR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MAJOR_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MAJOR_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MAJOR_TGI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_OFFICIAL_STORE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_official_store_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_OFFICIAL_STORE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_official_store_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRAND_FAVORITE_TGI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brand_favorite_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_SHOP_BY_STYLE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_shop_by_style_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PRODUCT_RECOMMENDATION_B2B_DETAIL_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_product_recommendation_b2b_detail_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_RUPARUPA_REWARDS_PROMO_MAJOR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_ruparupa_rewards_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_HARGA_SPESIAL_RUPARUPA_REWARDS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_harga_spesial_ruparupa_rewards_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_RUPARUPA_BISNIS_DETAIL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_ruparupa_bisnis_detail_', $companyCode);
                    }
                    
                }

                // Repolpulate again
                $nsq = new Nsq();
                $message = [
                    "message" => "createTree",
                    "company_code" => $companyCode
                ];
                $nsq->publishCluster('category', $message);

                return array("Save data success",$this->category_id);
            }

        } else if (!empty($this->category_id)) { // update
            $parent_id = "";
            $this->updated_at = date("Y-m-d H:i:s");
            //update parent id
            if(strrpos($this->parent_path, '/')){
                $parent_id = substr($this->parent_path,strrpos($this->parent_path,'/')+1);
            }
            else{
                $parent_id = $this->parent_path;
            }

            $this->parent_id = $parent_id;

            if(!empty($this->brand_id)){
                $sql = "SELECT * FROM product_category_mapping_brand pcmb JOIN product_category pc ON pcmb.category_id = pc.category_id 
                WHERE brand_id = ".$this->brand_id." AND pcmb.company_code = '".$this->company_code."' AND pcmb.category_id != ".$this->category_id." AND parent_id != 5808 AND parent_id != 8906 AND pc.status = 10";
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $validateBrand = $result->fetchAll();
                if (!empty($validateBrand)) {
                    $this->errors[] = "Update failed! Brand ID has been used.";
                    return array("Update failed! Brand has been used");
                }
            }

            if($this->url_key != $this->old_url_key) {
                // for now, it'll be the same
                // check first if the url_key used to be category's url_key
                $routeHelper = new RouteHelper();
                $message = $routeHelper->checkUrlKey($this->category_id,"category",$this->url_key, $companyCode);

                if($message['url_key_status'] == 0){ // url_key existed
                    $error = "Url key existed";
                    $messages = "Update failed! Url Key existed.";
                    $this->errors[] = $error;
                    return ;
                }
                else if($message['url_key_status'] == 1){// url_key used to be this url_key
                    $urlData = array(
                        'url_key' => $this->url_key,
                        'section' => $section,
                        'inline_script' => $this->inline_script,
                        'inline_script_seo' => $this->inline_script_seo,
                        'inline_script_seo_mobile' => $this->inline_script_seo_mobile,
                        'additional_header' => $this->additional_header,
                        'document_title' => "Jual " . $this->name . " | Ruparupa",
                        'reference_id' => $this->category_id,
                        'company_code' => $companyCode,
                        'created_at' => $date
                    );

                    $urlModel = new MasterUrlKey();
                    $urlModel->setFromArray($urlData);
                    $urlModel->updateUrlKey();
                    $urlModel->afterUpdate();
                }
                else{ // completely new
                    $urlData = array(
                        'url_key' => $this->url_key,
                        'section' => $section,
                        'inline_script' => $this->inline_script,
                        'inline_script_seo' => $this->inline_script_seo,
                        'inline_script_seo_mobile' => $this->inline_script_seo_mobile,
                        'additional_header' => $this->additional_header,
                        'document_title' => "Jual " . $this->name . " | Ruparupa",
                        'reference_id' => $this->category_id,
                        'company_code' => $companyCode
                    );

                    $urlModel = new MasterUrlKey();
                    $urlModel->setFromArray($urlData);
                    $urlModel->saveUrlKey();
                }

                // implement update related path
                $this->updateChildrenUrlKey($companyCode, $section);
            } else {
                //if url_key is same, update all beside url_key
                $urlData = array(
                    'url_key' => $this->url_key,
                    'section' => $section,
                    'inline_script' => $this->inline_script,
                    'inline_script_seo' => $this->inline_script_seo,
                    'inline_script_seo_mobile' => $this->inline_script_seo_mobile,
                    'additional_header' => $this->additional_header,
                    'document_title' => "Jual " . $this->name . " | Ruparupa",
                    'reference_id' => $this->category_id,
                    'company_code' => $companyCode,
                    'created_at' => $date
                );

                $urlModel = new MasterUrlKey();
                $urlModel->setFromArray($urlData);
                $urlModel->updateUrlKey();
            }

            if(!empty($this->brand_id)) {
                $categoryMappingModel = new ProductCategoryMappingBrand();

                $sql = "SELECT * FROM product_category_mapping_brand pcmb WHERE pcmb.category_id = ".$this->category_id;
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                $result->setFetchMode(
                    \Phalcon\Db::FETCH_ASSOC
                );
                $brandData = $result->fetchAll();
                if (!empty($brandData)) {
                    //update data
                    $sql = "UPDATE product_category_mapping_brand SET brand_id = '".$this->brand_id."' WHERE category_id = '".$this->category_id."'";
                    $categoryMappingModel->useWriteConnection();
                    $categoryMappingModel->getDi()->getShared($this->getConnection())->query($sql);
                    $categoryMappingModel->afterUpdate();
                } else {
                    //insert data
                    $categoryMappingData = array(
                        'brand_id' => $this->brand_id,
                        'category_id' => $this->category_id,
                        'company_code' => $this->company_code
                    );
                    $categoryMappingModel->setFromArray($categoryMappingData);
                    $categoryMappingModel->saveData();
                }

                $redisKey = "brand_url_".$this->brand_id."_".$companyCode;
                $redis = new \Library\Redis();
                $redisCon = $redis::connect();
                $redisResponse = $redisCon->get($redisKey);
    
                if (!empty($redisResponse)) {
                    $redisCon->delete($redisKey);
                }
            }

            //unset all for buildparam
            unset($this->url_key);
            unset($this->old_url_key);
            unset($this->additional_header);
            unset($this->inline_script);
            unset($this->inline_script_seo);
            unset($this->inline_script_seo_mobile);

            unset($this->old_parent_path);
            unset($this->parent_path);
            unset($this->brand_id);

            $updateParam = $this->buildUpdateParam();

            $query = "UPDATE product_category SET ";
            $query .= $updateParam;
            $query .= " WHERE category_id = " . $this->category_id . " AND company_code = '".$companyCode."'";

            try {
                $this->useWriteConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($query);

                // delete PHCR redis
                $this->afterUpdate();

                if($root == '2984' || $root == getEnv('INSPIRATION_AHI') || $root == getEnv('INSPIRATION_HCI') || $root == getEnv('INSPIRATION_TGI')){
                    //meaning it's inspiration
                    $inspirationLib = new \Library\Inspiration();
                    $inspirationLib->deleteInspirationRedis($companyCode);

                    $categoryLib = new \Library\Category();
                    $categoryLib->deleteCustomCategoryRedis();
                } else {
                    if ($root == getEnv('INSPIRATION_PROMO_BRAND_ODI') || $root == getEnv('INSPIRATION_PROMO_BRAND_AHI') || $root == getEnv('INSPIRATION_PROMO_BRAND_HCI') || $root == getEnv('INSPIRATION_PROMO_BRAND_TGI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_brand_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_GET_MORE_SALE_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_get_more_sale_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PAYDAY_SURPRISE_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_payday_surprise_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_ALL_OFFICE_NEEDS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_all_office_needs_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_FURNITURE_FESTIVAL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_furniture_festival_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_ALL_OFFICE_NEEDS_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_all_office_needs_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_EARTH_DAY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_earth_day_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_TOYS_KINGDOM_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_toys_kingdom_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_HARI_RAYA_SERU_DI_RUMAH_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_hari_raya_seru_di_rumah_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_LEBARAN_PUASA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_lebaran_puasa_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_CORONA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_corona_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_COVID_19_SURVIVAL_KIT_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_covid_19_survival_kit_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_HEALTHY_LIVING_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_healthy_living_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_FURNIFEST_FESTIVAL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_furnifest_festival_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_HEALTHY_LIVING_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_healthy_living_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_FURNIFEST_FESTIVAL_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_furnifest_festival_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_INSPIRATION_FURNIFEST_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_inspiration_furnifest_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_RAK_DAN_PENYIMPANAN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_rak_dan_penyimpanan_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRAND_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brand_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_TRAVELING_HOLIDAY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_traveling_holiday_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_ALL_ABOUT_HOBBIES_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_all_about_hobbies_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_GET_MORE_SALE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_get_more_sale_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMOTIONS_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promotions_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_BRAND_MICROSITE_DETAIL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_brand_microsite_detail_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MICROSITE_DETAIL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_microsite_detail_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRAND_LACASSA_DETAIL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brand_lacassa_detail_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRANDS_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brands_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRANDS_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brands_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_NATAL_DAN_AKHIR_TAHUN_DI_RUMAH_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_natal_dan_akhir_tahun_di_rumah_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PILIH_NUANSA_NATAL_2020_DI_RUMAHMU_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_pilih_nuansa_natal_2020_di_rumahmu_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_BRAND_MICROSITE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_brand_microsite_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BOOM_SALE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_boom_sale_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MAKAN_DAN_HIDUP_SEHAT_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_makan_dan_hidup_sehat_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_PERFECT_WORKSPACE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_perfect_workspace_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_CHOOSE_THE_LOOK_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_choose_the_look_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_COMPLETE_YOUR_KITCHEN_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_complete_your_kitchen_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_DECOR_STORAGE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_decor_storage_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_FESTIVE_MOMENT_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_festive_moment_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_COFFEE_AND_BAKE_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_coffee_and_bake_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_FURNITURE_AND_DECORATION_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_furniture_and_decoration_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_COOKING_MICROSITE_KITCHEN_FAIR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_cooking_microsite_kitchen_fair_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_YOUR_COMFORT_ZONE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_your_comfort_zone_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_SOLUSI_RUMAH_RAPI_DAN_NYAMAN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_solusi_rumah_rapi_dan_nyaman_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_BARANG_WAJIB_UNTUK_SETIAP_RUANGAN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_barang_wajib_untuk_setiap_ruangan_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_WOW_SALE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_wow_sale_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_HOME_AND_LIVING_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_home_and_living_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_EVERYDAY_PROBLEMS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_everyday_problems_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_HOBBIES_LIFESTYLE_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_hobbies_lifestyle_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_JOYFUL_MOMENTS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_joyful_moments_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_YOUR_BUSINESS_COMPANION_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_your_business_companion_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_5TH_ANNIVERSARY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_5th_anniversary_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_WOW_SALE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_wow_sale_', $companyCode);
                    }else if ($root == getEnv('INSPIRATION_INSPIRASI_MICROSITE_TEKSTIL_UNTUK_HUNIAN_ANDA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_inspirasi_microsite_tekstil_untuk_hunian_anda_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_HOME_LIVING_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_home_living_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_KITCHEN_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_kitchen_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_HOBBIES_LIFESTYLE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_hobbies_lifestyle_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_JOYOUS_MOMENT_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_joyous_moment_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_INSPIRATIONS_AUTOMOTIVE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_inspirations_automotive_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_TETAP_SERU_DI_RUMAH_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_tetap_seru_di_rumah_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_SUSEN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_susen_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_INFORMA_17TH_ANNIVERSARY_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_informa_17th_anniversary_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_NEW_TRENDS_17TH_ANNIVERSARY_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_new_trends_17th_anniversary_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_SHOP_IN_SHOP_FURNITUR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_shop_in_shop_furnitur_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_ATARU_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_ataru_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_RAWAT_DAN_LENGKAPI_KENDARAAN_ANDA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_rawat_dan_lengkapi_kendaraan_anda_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_HOBBY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_hobby_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_SHOP_IN_SHOP_RUMAH_TANGGA_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_shop_in_shop_rumah_tangga_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_HOT_CLEARANCE_ACE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_hot_clearance_ace_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_ASHLEY_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_ashley_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_INSPIRATION_PROMO_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_inspiration_promo_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_INSPO_PROMO_AKHIR_TAHUN_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_inspo_promo_akhir_tahun_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_PROMO_AKHIR_TAHUN_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_promo_akhir_tahun_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_KOREAN_HOME_DECOR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_korean_home_decor_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_SHOP_IN_SHOP_PET_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_shop_in_shop_pet_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_GIFT_IDEAS_1_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_gift_ideas_1_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_GIFT_IDEAS_2_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_gift_ideas_2_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_GIFT_IDEAS_3_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_gift_ideas_3_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_MICROSITE_BEST_GIFT_IDEAS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_microsite_best_gift_ideas_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MAJOR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MAJOR_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MAJOR_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PROMO_MAJOR_TGI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_OFFICIAL_STORE_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_official_store_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_OFFICIAL_STORE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_official_store_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_BRAND_FAVORITE_TGI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_brand_favorite_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_SHOP_BY_STYLE_HCI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_shop_by_style_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_PRODUCT_RECOMMENDATION_B2B_DETAIL_AHI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_product_recommendation_b2b_detail_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_RUPARUPA_REWARDS_PROMO_MAJOR_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_ruparupa_rewards_promo_major_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_HARGA_SPESIAL_RUPARUPA_REWARDS_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_harga_spesial_ruparupa_rewards_', $companyCode);
                    } else if ($root == getEnv('INSPIRATION_RUPARUPA_BISNIS_DETAIL_ODI')) {
                        $inspirationLib = new \Library\Inspiration();
                        $inspirationLib->deletePromoInspirationRedis('inspiration_ruparupa_bisnis_detail_', $companyCode);
                    }
                    
                }

                // Repolpulate again
                $nsq = new Nsq();
                $message = [
                    "message" => "createTree",
                    "company_code" => $companyCode
                    // "current_category_id" => $this->category_id,
                    // "parent_category_id" => $this->parent_id
                ];

                $nsq->publishCluster('category', $message);

                return array("0" => "Affected rows: " . $result->numRows());

            } catch (\Exception $e) {
                \Helpers\LogHelper::log("product_catalog", "Product category update failed, error : " . $e->getMessage());
                $this->errors[] = "Update failed, please try again";
            }

        } else {
            $this->errors[] = "Category Id is required";
        }

        return true;
    }

    public function afterUpdate()
    {
        /*
         * Also Clean up this url key from redis (if query from url key)
         */
        $redis = new Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);
        $redisKey = "category_id";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->del($key);
                }
            }
        } while ($iterator > 0);

        // delete base on article hierarcy
        $redisKey = "article_hierarchy";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->del($key);
                }
            }
        } while ($iterator > 0);

        // delete base on level too,flush all
        $redisKey = "level";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->del($key);
                }
            }
        } while ($iterator > 0);

        // delete based on parent_id
        $redisKey = "parent_id";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->del($key);
                }
            }
        } while ($iterator > 0);
    }

    public function updateChildrenUrlKey($companyCode = 'ODI', $section = 'category'){
        // Update all url_key that contains old_url_key to new_url_key
        // Note : If url_key has changed
        // TODO: We have to change all of the children url_key in elasticsearch too
        $date = date('Y-m-d H:i:s');
        $search_old = $this->old_url_key.'/%';
        $search_new = $this->url_key.'/%';
        $arrData = array();
        $arrCase = array();

        // search if the related-paths are already exist
        $sql  = "SELECT url_key, document_title, reference_id ";
        $sql .= "FROM master_url_key WHERE url_key LIKE '".$search_new."' AND company_code = '".$companyCode."'";

        $this->useReadOnlyConnection();
        $relatedPath_old = $this->getDi()->getShared($this->getConnection())->query($sql);

        $relatedPath_old->setFetchMode(
            Db::FETCH_ASSOC
        );

        $relatedPath_old = $relatedPath_old->fetchAll();

        if(!empty($relatedPath_old)) {
            //search additional header, inline script from current category
            $sql  = "SELECT additional_header, inline_script, inline_script_seo, inline_script_seo_mobile ";
            $sql .= "FROM master_url_key WHERE url_key LIKE '".$search_old."' AND company_code = '".$companyCode."'";
            $this->useReadOnlyConnection();
            $newDataUrlKey = $this->getDi()->getShared($this->getConnection())->query($sql);

            $newDataUrlKey->setFetchMode(
                Db::FETCH_ASSOC
            );

            $newDataUrlKey = $newDataUrlKey->fetchAll();

            foreach ($relatedPath_old as $key => $value) {
                // find the url_key for updating bulk
                $arrData[] = "'" . $value['url_key'] . "'";
                $arrCase[] = "WHEN '" . $value['url_key'] . "' THEN '" . $date . "'";
                $arrSection[] = "WHEN '".$value['url_key']."' THEN 'category'";
                $arrAddHeader[] = "WHEN '".$value['url_key']."' THEN '".$newDataUrlKey[$key]['additional_header']."'";
                $arrInlineScript[] = "WHEN '".$value['url_key']."' THEN '".$newDataUrlKey[$key]['inline_script']."'";
                $arrInlineScriptSeo[] = "WHEN '".$value['url_key']."' THEN '".$newDataUrlKey[$key]['inline_script_seo']."'";
                $arrInlineScriptSeoMobile[] = "WHEN '".$value['url_key']."' THEN '".$newDataUrlKey[$key]['inline_script_seo_mobile']."'";

                // delete redis first
                $urlModel3 = new MasterUrlKey();
                $urlModel3->setFromArray(array(
                    'reference_id' => $value['reference_id'],
                    'section' => $section
                ));
                $urlModel3->afterUpdate();
            }

            $urlList = "(" . implode(',', $arrData) . ")";
            $caseBulk = implode(' ', $arrCase);
            $sectionBulk = implode(' ', $arrSection);
            $headerBulk = implode(' ', $arrAddHeader);
            $scriptBulk = implode(' ', $arrInlineScript);
            $scriptSeoBulk = implode(' ', $arrInlineScriptSeo);
            $scriptSeoMobileBulk = implode(' ', $arrInlineScriptSeoMobile);

            $sql = "UPDATE master_url_key SET additional_header = ";
            $sql .= "(CASE url_key ".$headerBulk." END), inline_script = ";
            $sql .= "(CASE url_key ".$scriptBulk." END), inline_script_seo = ";
            $sql .= "(CASE url_key ".$scriptBulk." END), inline_script_seo_mobile = ";
            $sql .= "(CASE url_key ".$scriptSeoMobileBulk." END), section = ";
            $sql .= "(CASE url_key ".$sectionBulk." END), created_at = ";
            $sql .= "(CASE url_key " . $caseBulk . " END)";
            $sql .= " WHERE url_key IN " . $urlList . " AND company_code = '".$companyCode."'";

            $this->useWriteConnection();
            $this->getDi()->getShared($this->getConnection())->query($sql);

            foreach ($relatedPath_old as $key => $val){
                // delete redis again
                $urlModel3 = new MasterUrlKey();
                $urlModel3->setFromArray(array(
                    'reference_id' => $value['reference_id'],
                    'section' => $section
                ));
                $urlModel3->afterUpdate();
            }
        }
        else{
            // if not exists, then insert new path with new url_path
            $sql  = "SELECT url_key, section, additional_header, inline_script, inline_script_seo, inline_script_seo_mobile, document_title, reference_id ";
            $sql .= "FROM master_url_key WHERE url_key LIKE '".$search_old."' AND company_code = '".$companyCode."'";
            $this->useReadOnlyConnection();
            $relatedPath_new = $this->getDi()->getShared($this->getConnection())->query($sql);

            $relatedPath_new->setFetchMode(
                Db::FETCH_ASSOC
            );

            $relatedPath_new = $relatedPath_new->fetchAll();

            foreach ($relatedPath_new as $key => $value){
                $relatedPath_new[$key]['url_key'] = str_replace($this->old_url_key, $this->url_key, $value['url_key']);

                // delete redis first
                $urlModel3 = new MasterUrlKey();
                $urlModel3->setFromArray(array(
                    'reference_id' => $value['reference_id'],
                    'section' => $section
                ));
                $urlModel3->afterUpdate();

//              insert new url_key to master_url_key
                $urlModelNew = new MasterUrlKey();
                $urlModelNew->setFromArray($relatedPath_new[$key]);
                $urlModelNew->saveUrlKey();
            }
        }
    }

    public function updateChildren($categoryId = array(), $successCounter = 0, $errorCounter = 0){
        $childrenPath = array();
        $childrenPathNew = array();
        $childrenLevelNew = array();
        $childrenId = array();
        $success = $successCounter;
        $errors = $errorCounter;

        if(empty($categoryId)){
            return false;
        }

        foreach ($categoryId as $id => $value) {

            $sql = "SELECT path,level FROM product_category WHERE category_id=";
            $sql .= $value;
            $this->useReadOnlyConnection();
            $currentCategoryRes = $this->getDi()->getShared($this->getConnection())->query($sql);

            $currentCategoryRes->setFetchMode(
                DB::FETCH_ASSOC
            );

            $categoryPath = $currentCategoryRes->fetchAll();

            // we want to update current category's children, update path

            $sql = "SELECT category_id FROM product_category WHERE parent_id=";
            $sql .= $value;
            $this->useReadOnlyConnection();
            $childrenResult = $this->getDi()->getShared($this->getConnection())->query($sql);

            $childrenResult->setFetchMode(
                DB::FETCH_ASSOC
            );

            $children = $childrenResult->fetchAll();

            if (!empty($children)) {
                // looping update level and path
                foreach ($children as $key => $val) {
                    $sql = "SELECT path FROM product_category WHERE category_id=";
                    $sql .= $val['category_id'];

                    $this->useReadOnlyConnection();
                    $path = $this->getDi()->getShared($this->getConnection())->query($sql);

                    $path->setFetchMode(
                        DB::FETCH_ASSOC
                    );

                    $childrenPath = $path->fetchAll();

                    if (!empty($childrenPath)) {
                        if(!empty($childrenPath[0]['path'])){
                            // new path
                            $childParent = substr($childrenPath[0]['path'], 0, strrpos($childrenPath[0]['path'], '/'));
                            $childrenPathNew[] = "WHEN " . $val['category_id'] . " THEN '" . str_replace($childParent, $categoryPath[0]['path'], $childrenPath[0]['path']) . "'";

                            // new level
                            $childrenLevelNew[] = "WHEN " . $val['category_id'] . " THEN " . ($categoryPath[0]['level'] + 1) . "";

                            //child
                            $childrenId[] = $val['category_id'];
                        }
                    }
                }

                // let's update bulk
                $stringChildrenPathNew = implode(' ', $childrenPathNew);
                $stringChildrenLevelNew = implode(' ', $childrenLevelNew);
                $stringChildrenId = implode(',', $childrenId);

                $query = "UPDATE product_category SET path = ( CASE category_id ";
                $query .= $stringChildrenPathNew;
                $query .= " END), level = ( CASE category_id ";
                $query .= $stringChildrenLevelNew;
                $query .= " END) WHERE category_id IN (" . $stringChildrenId . ")";

                $this->useWriteConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($query);

                if($result->numRows() > 0){
                    $success += $result->numRows();
                }
                else{
                    $errors += 1;
                }

            } else {
                return array("success_total" => $success, "errors_total" => $errors);
            }
        }
        // recursive
        $this->updateChildren($childrenId,$success,$errors);

    }

    public function updateParentChildrenCount($currentCategoryData = array()){
        if(empty($currentCategoryData)){
            return false;
        }

        $parentData = $this->findFirst(
            [
                "columns" => "parent_id",
                "conditions" => "category_id=".$currentCategoryData['category_id']
            ]
        );

        if($parentData){
            $parentData = $parentData->toArray();
            if($parentData['parent_id'] != 0){
                // search parent children_count
                $query  = "UPDATE product_category SET ";
                $query .= "children_count = (children_count+1) ";
                $query .= "WHERE category_id=".$parentData['parent_id'];
                $this->useWriteConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($query);

                if($result->numRows() <=0 ){
                    return false;
                }
                else{
                    // ambil parent id untuk update atasnya lagi
                    $this->updateParentChildrenCount(array('category_id' => $parentData['parent_id']));
                }
            }
            return true;
        }
    }

    public function generateUrlPath(){
        $urlPath = "";
        $parent_path = "";
        $res =array();
        $parent_id = explode('/', $this->parent_path);
        foreach ($parent_id as $id => $val){
            //find parent url_path
            $result = $this->findFirst(
                [
                    "columns" => "category_key",
                    "conditions" => "category_id='".$val."'"
                ]
            );
            if(empty($result)){
                return array(
                    'new_url_path' => '',
                    'error' => '1'
                );
            }
            $res[] =$result->toArray();
        }

        foreach ($res as $key => $val){
            $res[$key] = $val['category_key'];
        }

        $parent_path = implode('/', $res);

        //find current category key
        $category_key = $this->findFirst(
            [
                "columns" => "category_key",
                "conditions" => "category_id='".$this->category_id."'"
            ]
        );

        $category_key = $category_key->toArray();

        // concat them
        $urlPath = $parent_path.'/'.$category_key['category_key'];

        return array(
            'new_url_path' => $urlPath,
            'error' => '0'
        );
    }

    
    // bikin model baru sesuai nama table nya
    // getDataArray dari model ini -> panggilnya di model baru itu
}

