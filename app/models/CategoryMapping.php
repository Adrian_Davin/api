<?php

namespace Models;

class MasterProvince extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $province_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $country_id;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $province_code;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $province_name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('province_id', 'Models\CustomerAddress', 'province_id', array('alias' => 'CustomerAddress'));
        $this->hasMany('province_id', 'Models\MasterCity', 'province_id', array('alias' => 'MasterCity'));
        $this->hasMany('province_id', 'Models\ShippingRate', 'origin_province_id', array('alias' => 'ShippingRate'));
        $this->belongsTo('country_id', 'Models\MasterCountry', 'country_id', array('alias' => 'MasterCountry'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_province';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {

        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
    }

    public function getDataList($params = array())
    {
        $provinces = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterProvince::deleteProvince();
        } else {
            $provinces = \Library\Redis\MasterProvince::getProvinceList();
        }

        if(empty($provinces)) {
            $result = $this->find();

            if($result) {
                $provincesRedis = array();
                $provinces = array();
                foreach($result as $province) {
                    $provinceArray = $province->toArray();
                    $provinceArray['city'] = $province->MasterCity->toArray();
                    $provincesRedis[$province->province_id] = $provinceArray;
                    $provinces[] = $provinceArray;
                }

                // save to redis
                \Library\Redis\MasterProvince::setProvinces($provincesRedis);
            }
        }


        return $provinces;
    }

    public function getData($params = array())
    {
        $province = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterProvince::deleteProvince($params['province_id']);
        } else {
            $province = \Library\Redis\MasterProvince::getProvinceData($params['province_id']);
        }

        if(empty($province)) {
            $provinceData = $this->findFirst("province_id = '".$params['province_id']."'");

            if($provinceData) {
                $province = $provinceData->toArray();
                $province['city'] = $provinceData->MasterCity->toArray();
                \Library\Redis\MasterProvince::setProvince($params['province_id'], $province);
            }
        }

        return $province;
    }
}
