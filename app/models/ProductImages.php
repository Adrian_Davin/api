<?php

namespace Models;

class ProductImages extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $image_id;


    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $product_variant_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $image_url;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $image_beta_url;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    public $angle;

    public $image_info;
    public $image_tag;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    protected $data;

    protected $errors;

    protected $messages;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $messages
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }


    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('product_id', 'Models\Product', 'product_id', array('alias' => 'Product'));
        $this->belongsTo('product_variant_id', 'Models\ProductVariant', 'product_variant_id', array('alias' => 'ProductVariant'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_images';
    }

    /**
     * @return int
     */
    public function getImageId()
    {
        return $this->image_id;
    }

    /**
     * @param int $image_id
     */
    public function setImageId($image_id)
    {
        $this->image_id = $image_id;
    }

    /**
     * @return int
     */
    public function getProductVariantId()
    {
        return $this->product_variant_id;
    }

    /**
     * @param int $product_variant_id
     */
    public function setProductVariantId($product_variant_id)
    {
        $this->product_variant_id = $product_variant_id;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @param string $image_url
     */
    public function setImageUrl($image_url)
    {
        $this->image_url = $image_url;
    }

    /**
     * @return string
     */
    public function getImageBetaUrl()
    {
        return $this->image_beta_url;
    }

    /**
     * @param string $image_beta_url
     */
    public function setImageBetaUrl($image_beta_url)
    {
        $this->image_beta_url = $image_beta_url;
    }

    /**
     * @return int
     */
    public function getAngle()
    {
        return $this->angle;
    }

    /**
     * @param int $angle
     */
    public function setAngle($angle)
    {
        $this->angle = $angle;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductImages[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductImages
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public function beforeCreate()
    {
        parent::beforeCreate();

        if(empty($this->status)) {
            $this->status = 10;
        }
    }

    public function updateImages($params = array()){
        try {

            $sql = " update product_images set 
                          updated_at = '".date('Y-m-d H:i:s')."',
                          image_info = '".$params['image_info']."',
                          image_url = '".$params['image_url']."',
                          angle = ".$params['angle']."
                      
 
 ";

            $result_insert = $this->getDi()->getShared('dbMaster')->execute($sql);
            $affected_row = $this->getDi()->getShared('dbMaster')->affectedRows();

            $this->setMessages(array('Successfully Update Product Images'));
            $this->setData(array('affected_row_update' => $affected_row));
            $this->setErrors(array());

        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("product images", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'product images','message' => 'There have something error, in query update Product Images'));
            $this->setData(array('affected_row_update' => 0));
        }
    }

    public function updateImagesChangeSupplier($params = array()){
        try {
            if(!empty($params['image_id'])) {
                $sql = " update product_images set
                          updated_at = '" . date('Y-m-d H:i:s') . "',
                          image_info = '" . $params['image_info'] . "',
                          image_url = '" . $params['image_url'] . "',
                          image_beta_url = '" . $params['image_beta_url'] . "',
                          angle = " . $params['angle'] . "
                          WHERE image_id = " . $params['image_id'] . "
                    ";

                $result_insert = $this->getDi()->getShared('dbMaster')->execute($sql);
                $affected_row = $this->getDi()->getShared('dbMaster')->affectedRows();

                $this->setMessages(array('Successfully Update Product Images'));
                $this->setData(array('affected_row_update' => $affected_row));
                $this->setErrors(array());
            }

        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("product images", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'product images','message' => 'There have something error, in query update Product Images'));
            $this->setData(array('affected_row_update' => 0));
        }
    }
}
