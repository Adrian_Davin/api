<?php

namespace Models;

class SalesrulePwpSku extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $salesrule_pwp_sku_id;


    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $rule_id;


     /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $sku;

     /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $sku_bought;
  

    /**
     * @return int
     */
    public function getSalesrulePwpSkuID()
    {
        return $this->salesrule_pwp_sku_id;
    }

    /**
     * @param int $salesrule_pwp_sku_id
     */
    public function setSalesrulePwpSkuID($salesrule_pwp_sku_id)
    {
        $this->salesrule_pwp_sku_id = $salesrule_pwp_sku_id;
    }


    /**
     * @return int
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * @param int $rule_id
     */
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;
    }

      /**
     * @return int
     */
    public function getSkuBought()
    {
        return $this->sku_bought;
    }

    /**
     * @param int $sku_bought
     */
    public function setSkuBought($sku_bought)
    {
        $this->sku_bought = $sku_bought;
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_pwp_sku';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
       
    }

    public function onConstruct()
    {
        parent::onConstruct();

    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesrulePwpSku[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesrulePwpSku
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

     public function saveData($log_file_name = "error", $state = "")
    {
        try {
            if ($this->save() === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Failed when try to save data";

                if($transaction = $this->get()) {
                    $transaction->rollback(
                        "Cannot save data, rollback"
                    );

                    throw new \Exception("Rollback transaction");
                }

                return false;
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }

    public function updateSkuBought($pwp, $qty)
    {
        $sql = "UPDATE salesrule_pwp_sku SET sku_bought = sku_bought + '" . $qty . "'  WHERE salesrule_pwp_sku_id = '" . $pwp . "'";
        $result = $this->getDi()->getShared('dbMaster')->execute($sql);
        if (!$result) {
            \Helpers\LogHelper::log("salesrule_pwp_sku", "cannot update salesrule_pwp_sku = '" . $pwp . "'");
            return false;
        }

        return true;
    }
}
