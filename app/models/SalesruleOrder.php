<?php

namespace Models;

use Phalcon\Db as Database;

class SalesruleOrder extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $salesrule_order_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $sales_order_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $rule_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $voucher_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $group_abuser;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $voucher_amount_used;

    /**
     * @return float
     */
    public function getVoucherAmountUsed()
    {
        return $this->voucher_amount_used;
    }

    /**
     * @param float $voucher_amount_used
     */
    public function setVoucherAmountUsed($voucher_amount_used)
    {
        $this->voucher_amount_used = $voucher_amount_used;
    }

    /**
     * @return int
     */
    public function getSalesruleOrderId()
    {
        return $this->salesrule_order_id;
    }

    /**
     * @param int $salesrule_order_id
     */
    public function setSalesruleOrderId($salesrule_order_id)
    {
        $this->salesrule_order_id = $salesrule_order_id;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * @param int $sales_order_id
     */
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }

    /**
     * @return int
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * @param int $rule_id
     */
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;
    }

    /**
     * @return string
     */
    public function getVoucherCode()
    {
        return $this->voucher_code;
    }

    /**
     * @param string $voucher_code
     */
    public function setVoucherCode($voucher_code)
    {
        $this->voucher_code = $voucher_code;
    }

    /**
     * @return int
     */
    public function getGroupAbuser()
    {
        return $this->group_abuser;
    }

    /**
     * @param int $group_abuser
     */
    public function setGroupAbuser($group_abuser)
    {
        $this->group_abuser = $group_abuser;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_order';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasOne('rule_id', 'Models\SalesruleVoucher', 'rule_id', array('alias' => 'SalesRuleVoucher'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleOrder[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleOrder
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }

    public function getDataGiftCards()
    {
        try {
            $sql = "SELECT sales_order_id, gift_cards FROM sales_order
                    WHERE sales_order_id NOT IN (SELECT sales_order_id FROM salesrule_order) AND
                    gift_cards NOT IN ('[]','a:0:{}','null') AND gift_cards IS NOT NULL;";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $data = $result->fetchAll();
            } else {
                $data = array();
            }
        }  catch (\Exception $e) {
            $data = array();
        }

        return $data;
    }

}
