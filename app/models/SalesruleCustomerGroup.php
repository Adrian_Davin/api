<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class SalesruleCustomerGroup extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $rule_id;

    /**
     *
     * @var integer
     */
    protected $customer_group_id;

    protected $errors;

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Method to set the value of field rule_id
     *
     * @param integer $rule_id
     * @return $this
     */
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;

        return $this;
    }

    /**
     * Method to set the value of field customer_group_id
     *
     * @param integer $customer_group_id
     * @return $this
     */
    public function setCustomerGroupId($customer_group_id)
    {
        $this->customer_group_id = $customer_group_id;

        return $this;
    }

    /**
     * Returns the value of field rule_id
     *
     * @return integer
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * Returns the value of field customer_group_id
     *
     * @return integer
     */
    public function getCustomerGroupId()
    {
        return $this->customer_group_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_customer_group';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleCustomerGroup[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleCustomerGroup
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

}
