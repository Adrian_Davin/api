<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Invoice;

use \Helpers\LogHelper;

class Collection extends \ArrayIterator
{
    
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\SalesInvoice
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $invoice = $this->current();
            $invoiceArray = $invoice->getDataArray();
            $view[$idx] = $invoiceArray;

            $this->next();
        }
        return $view;
    }

    public function createInvoice($transaction = null)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();

            $invoice = $this->current();

            if ($transaction) {
                $invoice->setTransaction($transaction);
            }

            try {
                 \Helpers\LogHelper::log('debug_625', '/app/Controller/InvoiceController.php line 61', 'debug');
                $invoice->createInvoice("invoice", $invoice->getInvoiceNo() . "-saveInvoice");
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            $this->next();
        }
        return true;
    }

    public function saveData($transaction = null)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $invoice = $this->current();

            if ($transaction) {
                $invoice->setTransaction($transaction);
            }

            try {
                $invoice->saveData("invoice");
            } catch (\Library\HTTPException $e) {
                throw new \Exception($e->devMessage);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            $this->next();
        }
        return true;
    }


    public function sendToKLSys()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $invoice = $this->current();

            //Check invoice store code                      
            $invoiceStoreCode = $invoice->getStoreCode();            
            // exclude all invoice with exclude store code
            if ($invoiceStoreCode == "exclude" || substr($invoiceStoreCode, 0, 4) == "1000") {
                $this->next();
                continue;
            }
            
            $orderNo = $invoice->SalesOrder->getOrderNo();

            // Send to KL System for product not MP
            if (substr($invoice->getSupplierAlias(), 0, 2) != "MP") {
                $firstInvItemSku = "";
                $invoiceItems = $invoice->getInvoiceItems();
                $firstInvItemSku = $invoiceItems[0]->getSku();

                $skipCreateSOB2B = $this->checkSkipCreateSOB2B($firstInvItemSku);
                
                // Create SO to all item from DC
                if ($invoice->getDeliveryMethod() == "delivery" || 
                   ($invoice->getDeliveryMethod() == "ownfleet" && substr( $invoiceStoreCode, 0, 2 ) === "DC") || 
                    !$skipCreateSOB2B) {  
                    $store_code = $invoice->getStoreCode();          
                    $companyModel = New \Models\CompanyProfileDC();
                    $companyProfile = $companyModel->getCompanyByStoreCode($store_code);
                    $companyCode = !empty($companyProfile) ? $companyProfile["company_code"] : "" ;
                    //cek if order type b2b or b2c
                    if ($invoice->SalesOrder->getOrderType() == 'B2B' || !$skipCreateSOB2B) {
                        if ($companyCode == 'TGI' && $invoice->checkIfConsignment()) {
                            $consB2B = new \Library\CONSB2B($invoice);
                            $consB2B->prepareDCParamsCONSB2B();
                            $consB2B->generateDCParamsCONSB2B();
                            $consB2B->createCONSB2B();
                        } else {
                            $soB2B = new \Library\SOCIB2B($invoice);
                            $soB2B->prepareDCParamsSOCIB2B();
                            $soB2B->generateDCParamsSOCIB2B();
                            $soB2B->createSOCIB2B();
                        }     
                    } else {
                        if ($companyCode == 'TGI' && $invoice->checkIfConsignment()) {
                            $consB2C = new \Library\CONSB2C($invoice);
                            $consB2C->prepareDCParamsCONSB2C();
                            $consB2C->generateDCParamsCONSB2C();
                            $consB2C->createCONSB2C();
                        } else {
                            $soB2C = new \Library\SOCIB2C($invoice);
                            $soB2C->prepareDCParamsSOCIB2C();
                            $soB2C->generateDCParamsSOCIB2C();
                            $soB2C->createSOCIB2C();
                        }
                    }
                } else if ($invoice->getDeliveryMethod() == "pickup" || $invoice->getDeliveryMethod() == "store_fulfillment" || ($invoice->getDeliveryMethod() == "ownfleet" && substr( $invoiceStoreCode, 0, 2 ) !== "DC")) {
                    if ($invoiceStoreCode == "A300" || $invoiceStoreCode == "H300" ||  $invoiceStoreCode == "T300" || $invoiceStoreCode == "H203" || $invoiceStoreCode == "S702") { // make sure store code choosen later
                        
                    // if ($invoice->getDeliveryMethod() == "pickup") {
                        $SalesRegisterMember = new \Library\SalesRegisterMember($invoice);
                        $SalesRegisterMember->prepareSalesRegisterMemberParams();
                        $SalesRegisterMember->generateSalesRegisterMemberParams();
                        $SalesRegisterMember->createSalesRegisterMember();

                        if ($invoiceStoreCode != "S702") {
                            $UpgradeMember = new \Library\UpgradeMember($invoice);
                            $UpgradeMember->prepareUpgradeMemberParams($invoice->SalesOrder->getCustomerId());
                            $UpgradeMember->createUpgradeMember();
                        }

                        // Send Journal Sales 3602
                        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                        $dataInvoice['invoice_no'] = $invoice->getInvoiceNo();
                        $kawanLamaSystemLib->setFromArray($dataInvoice);
                        $kawanLamaSystemLib->createJournal('pickup'); 
                    }
                    // Chatime Phase 1 : create journal pickup directly
                    else if (substr(trim($invoiceStoreCode), 0, 1) == "F" && strtoupper($invoiceStoreCode) == "F353") {
                        // Process POS Chatime
                        $POSChatime = new \Library\POSChatime($invoice);
                        $POSChatime->prepareParams();
                        $POSChatime->createPOSChatime();

                        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                        $dataInvoice['invoice_no'] = $invoice->getInvoiceNo();
                        $kawanLamaSystemLib->setFromArray($dataInvoice);
                        $kawanLamaSystemLib->createJournal('pickup'); 
                        $kawanLamaSystemLib->createJournal('incoming'); 
                        $kawanLamaSystemLib->createJournal('mdr'); 
                    } else if (substr(trim($invoiceStoreCode), 0, 1) == "C" || substr(trim($invoiceStoreCode), 0, 1) == "F") {
                        $kawanLamaSystemLib = new \Library\KawanLamaSystem();
                        $dataInvoice['invoice_no'] = $invoice->getInvoiceNo();
                        $kawanLamaSystemLib->setFromArray($dataInvoice);
                        $kawanLamaSystemLib->createJournal('pickup'); 
                        $kawanLamaSystemLib->createJournal('incoming'); 
                        $kawanLamaSystemLib->createJournal('mdr'); 
                    } 
                    else {
                        $installationInformaB2BSku = getenv('INSTALLATION_INFORMAB2B_SKU');
                        $installationAceB2BSku = getenv('INSTALLATION_ACEB2B_SKU');

                        if (substr($orderNo, 0, 4) == "ODIN"){
                            $salesInstallationMdl = new \Models\SalesInstallation;
                            $salesInstallation = $salesInstallationMdl->findFirst("installation_order_no = '" . $orderNo . "'");
                            if (!$salesInstallation) {
                                $this->next();
                                continue;
                            }

                            // For installation RR with company code AHI, if installation order is free, we need to create pickup voucher with Rp1
                            if ($salesInstallation->getCompanyCode() != "AHI" && $salesInstallation->getInstallationType() != "ODI") {
                                if ($invoice->getGrandTotal() == 0 || ($firstInvItemSku == $installationInformaB2BSku || $firstInvItemSku == $installationAceB2BSku)) {
                                    $this->next();
                                    continue;
                                } 
                            } 
                        }

                        $this->createPickupVoucher($invoice);
                    }
                }
            } else if (substr($invoice->getSupplierAlias(),0,2) == "MP"){
                $journalSAP = new \Library\JournalSAP($invoice);
                $headerStatus = $journalSAP->prepareHeader();
                $journalSAP->preparePickupVoucherJournal();
                $journalSAP->createJournal("pickup");
            }

            $this->next();
        }
        return true;
    }

    public function isNewMop($invoice, $ffResponse) {
        if ($ffResponse['data']['is_all_bu']){
            return true;
        }

        $storeModel = new \Models\Store();
        $storeParams["conditions"] = sprintf("store_code = '%s'", $invoice->getStoreCode());
        $storeResult = $storeModel->findFirst($storeParams);
        if (!$storeResult) {
            $errMessage = sprintf("Failed create pickup voucher. Store Code %s not valid", $invoice->getStoreCode());
            LogHelper::log("pickup_voucher", $errMessage);
            return false;
        }

        $brandStoreModel = new \Models\MasterBrandStore();
        $brandStoreParams["conditions"] = sprintf("brand_store_id = '%d'", $storeResult->brand_store_id);
        $brandStoreResult = $brandStoreModel->findFirst($brandStoreParams);
        if (!$brandStoreResult) {
            $errMessage = sprintf("Failed create pickup voucher. Brand Store %s not valid", $invoice->getStoreCode());
            LogHelper::log("pickup_voucher", $errMessage);
            return false;
        }

        $bu = \Helpers\GeneralHelper::getMemberOrder($storeResult->Supplier->getSupplierAlias(), $brandStoreResult->getBrandStoreName());

        foreach ($ffResponse['data']['list_active_store'] as $activeStore) {
            if ($bu == $activeStore['business_unit']) {
                if (count($activeStore['list_store_code']) == 0) {
                    return true;
                }

                foreach ($activeStore['list_store_code'] as $store){
                    if ($invoice->getStoreCode() == $store['store_code']) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function createPickupVoucher($invoice)
    {
        $ffAPI = new \Library\FeatureFlagAPI();
        $ffResponse = $ffAPI->getFeatureFlag(["key" => "new_mop"]);
        if (!$ffResponse) {
            return;
        }

        if ($this->isNewMop($invoice, $ffResponse)){
            $payload = ["invoice_no" => $invoice->getInvoiceNo()];
            $posVoucher = new \Library\POSVoucher();
            $posVoucher->publishPickupVoucher($payload);

            if ($posVoucher->getErrorMessage() != "") {
                $errMessage = sprintf("Failed create pickup voucher. Invoice %s not publish to go-order-store. Error: %s", $invoice->getInvoiceNo(), $posVoucher->getErrorMessage());
                LogHelper::log("pickup_voucher", $errMessage);
            }
            return;
        }

        $POSVoucher = new \Library\POSVoucher($invoice);
        $POSVoucher->prepareParams();
        $POSVoucher->createPOSVoucher();
    }
    
    private function checkSkipCreateSOB2B($firstInvItemSku) {
        $invoice = $this->current();

        $installationInformaB2BSku = getenv('INSTALLATION_INFORMAB2B_SKU');
        $installationAceB2BSku = getenv('INSTALLATION_ACEB2B_SKU');

        $orderType = $invoice->SalesOrder->getOrderType();

        // Validate order type. Possible order type for B2B order are b2b_informa, b2b_ace, installation, additional_cost
        if ($orderType != 'b2b_informa' && $orderType != 'b2b_ace' && $orderType != 'installation' && $orderType != 'additional_cost') {
            // Skip create SO B2B
            return true;
        }

        // Flag for if current invoice is for B2B order
        $orderB2B = false;

        // Flag for if current invoice is for installation
        $installationB2BWithOrder = false;

        // Flag for if current invoice is for not free installation
        $notFreeInstallationB2B = false;

        switch($orderType) {
            case 'b2b_ace' :
            case 'b2b_informa':
                $orderB2B = true;
                if ($firstInvItemSku == $installationInformaB2BSku || $firstInvItemSku == $installationAceB2BSku) { // Validate if current invoice is for installation
                    $installationB2BWithOrder = true;
                }
                break;

            case 'installation':
                // Validate not free installation & make sure this installation order type is for B2B parent order
                if (($firstInvItemSku == $installationInformaB2BSku || $firstInvItemSku == $installationAceB2BSku) && $invoice->getGrandTotal() > 0) {
                    $notFreeInstallationB2B = true;
                }
                break;

            // If order type is additional cost, no need to check anything, just create the SO B2B (don't skip create SO B2B)
            case 'additional_cost':
                // Don't skip create SO B2B
                return false;
                break;

        }

        // If order b2b/installation with order b2b/not free installation for B2B order parent
        if ($orderB2B || $installationB2BWithOrder || $notFreeInstallationB2B) {
            // Create SO B2B/don't skip create SO B2B
            return false;
        }

        return true;
    }

    public function sendToSitory()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) 
        {
            $idx = $this->key();
            $invoice = $this->current();

            //Check invoice store code
            $invoiceStoreCode = $invoice->getStoreCode();
            // exclude all invoice with exclude store code
            if ($invoiceStoreCode == "exclude")
            {
                $this->next();
            }

            $sitoryStoreCodeStr = !empty(getenv("SITORY_STORE_CODE")) ? getenv("SITORY_STORE_CODE") : "";
            $sitoryStoreCodeArr = explode(",", $sitoryStoreCodeStr);

            if (substr(trim($invoiceStoreCode), 0, 1) == "A" && in_array($invoiceStoreCode, $sitoryStoreCodeArr)){
             if($invoice->getDeliveryMethod() == "pickup" || $invoice->getDeliveryMethod() == "store_fulfillment" || $invoice->getDeliveryMethod() == "ownfleet" )
                {
                $POSSitory = new \Library\POSSitory($invoice);
                $POSSitory->prepareParams();
                $POSSitory->createPOSSitory();
                }
            }
                $this->next();
         }
            return true;         
    }

}
