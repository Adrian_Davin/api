<?php

namespace Models;

class MdrPaymentTiering extends \Models\BaseModel
{
    protected $tiering_id;

    protected $tiering_name;

    protected $bottom_value;

    protected $top_value;

    protected $rate;

    protected $sku;

    /**
     * @return mixed
     */
    public function getTieringId()
    {
        return $this->tiering_id;
    }

    /**
     * @param mixed $tiering_id
     */
    public function setTieringId($tiering_id)
    {
        $this->tiering_id = $tiering_id;
    }

    /**
     * @return mixed
     */
    public function getTieringName()
    {
        return $this->tiering_name;
    }

    /**
     * @param mixed $tiering_name
     */
    public function setTieringName($tiering_name)
    {
        $this->tiering_name = $tiering_name;
    }

    /**
     * @return mixed
     */
    public function getBottomValue()
    {
        return $this->bottom_value;
    }

    /**
     * @param mixed $bottom_value
     */
    public function setBottomValue($bottom_value)
    {
        $this->bottom_value = $bottom_value;
    }

    /**
     * @return mixed
     */
    public function getTopValue()
    {
        return $this->top_value;
    }

    /**
     * @param mixed $top_value
     */
    public function setTopValue($top_value)
    {
        $this->top_value = $top_value;
    }

      /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'mdr_payment_tiering';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterSize[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterSize
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
