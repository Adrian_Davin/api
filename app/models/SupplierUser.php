<?php

namespace Models;

use Phalcon\Mvc\Model\Validator\Email as Email;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class SupplierUser extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $supplier_id;

    protected $supplier_user_id;

    protected $last_login;

    protected $role_id;

    protected $email;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $first_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $last_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $password;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $is_need_verify;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $last_change_password;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $ip_address;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $errors;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $messages;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('supplier_id', 'Models\Supplier', 'supplier_id', array('alias' => 'Supplier'));
        //$this->getSource();
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function setSupplierUserId($supplier_user_id){
        $this->supplier_user_id = $supplier_user_id;
    }

    public function getSupplierUserId(){
        return $this->supplier_user_id;
    }

    public function setSupplierId($supplier_id){
        $this->supplier_id = $supplier_id;
    }

    public function getSupplierId(){
        return $this->supplier_id;
    }

    public function setIsNeedVerify($is_need_verify){
        $this->is_need_verify = $is_need_verify;
    }

    public function getIsNeedVerify(){
        return $this->is_need_verify;
    }

    public function setFirstName($first_name){
        $this->first_name = $first_name;
    }

    public function getFirstName(){
        return $this->first_name;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setLastLogin($last_login){
        $this->last_login = $last_login;
    }

    public function getLastLogin(){
        return $this->last_login;
    }

    public function setIpAddresss($ip_address){
        $this->ip_address = $ip_address;
    }

    public function getIpAddresss(){
        return $this->ip_address;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $messages
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getLastChangePassword()
    {
        return $this->last_change_password;
    }

    /**
     * @param mixed $messages
     */
    public function setLastChangePassword($last_change_password)
    {
        $this->last_change_password = $last_change_password;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'supplier_user';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SupplierUser[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SupplierUser
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function saveUser(){
        try {
            $saveStatus = $this->saveData("supplier_user");
            $action = $this->action;

            if($saveStatus) {
                if ($action == "create" || empty($this->supplier_user_id)) {
                    $lastSupplier = $this::findFirst(array(
                        "columns" => "supplier_user_id",
                    ));

                    $this->supplier_user_id = $lastSupplier['supplier_user_id'];
                } else {
                    $this->supplier_user_id = $this->supplier_user_id;
                }

                $this->setSupplierUserId($this->supplier_user_id);

            }
            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("Supplier", "Supplier save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Save/Update Supplier failed";

                $transaction->rollback(
                    "Save/Update Supplier failed"
                );
                return false;
            }
            $transaction->commit();
        } catch (TxFailed $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }

    public function getSupplierUserList(){
        if($this->supplier_id == 'mp'){
            $supplierModel = new Supplier();
            $supplierData = $supplierModel->find(
                array(
                    "conditions" => "supplier_alias LIKE '%MP%'"
                )
            );

            $supplierIdArr = array();
            if(!empty($supplierData)) {
                foreach ($supplierData->toArray() as $supplierIdIn) {
                    $supplierIdArr[] = "'" . $supplierIdIn['supplier_id'] . "'";
                }
            }
            $data_supplier_ids = implode(",",$supplierIdArr);
            $conditions = " supplier_id IN (".$data_supplier_ids.")";


        }else{
            $conditions = " supplier_id = '".$this->supplier_id."'";
        }

        if(!empty($this->supplier_id)){
            $condition = array(
                'conditions' => $conditions
            );
        }
        return $this::find($condition);
    }

    public function getSupplierUserDetail(){
        $condition = "";
        if(!empty($this->supplier_user_id)){
            $condition = array(
                'conditions' => ' supplier_user_id = "'.$this->supplier_user_id.'"'
            );
        }
        return $this::findFirst($condition);
    }

    public function logout($params) {
        $response = array();
        try {
            if (isset($params['supplier_user_id'])) {
                $sql = "update supplier_user set ip_address = '' where supplier_user_id = {$params['supplier_user_id']}";
                $result = $this->getDi()->getShared('dbMaster')->execute($sql);
                if(!$result){
                    $response['errors'] = 'Logout process failed';
                }
            } else {
                $response['errors'] = 'Logout process failed';
            }            
        }  catch (\Exception $e) {
            $response['errors'] = 'Logout process failed';
            \Helpers\LogHelper::log("user_logout", "Logout process failed, error : " . $e->getMessage());
        }

        return $response;
    }
    

    /**
     * Get the value of role_id
     */ 
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * Set the value of role_id
     *
     * @return  self
     */ 
    public function setRoleId($role_id)
    {
        $this->role_id = $role_id;

        return $this;
    }
}
