<?php

namespace Models;

class ShippingAssignation extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $shipping_assignation_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $province;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $city;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $kecamatan;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=true)
     */
    public $kecamatan_code;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    public $kilometer;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $is_armada;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $is_3pl;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $assignation;

    /**
     *
     * @var integer
     * @Column(type="integer", length=2, nullable=true)
     */
    public $courier_assignation_dc;

    /**
     *
     * @var integer
     * @Column(type="integer", length=2, nullable=true)
     */
    public $courier_assignation_store;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('kecamatan_code', 'Models\MasterKecamatan', 'kecamatan_code', array('alias' => 'MasterKecamatan', "reusable" => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'shipping_assignation';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {

        $redisKey = self::_createKey($parameters);

        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

}
