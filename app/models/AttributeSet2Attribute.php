<?php

namespace Models;

class AttributeSet2Attribute extends \Models\BaseModel
{

    protected $attribute_set_2_attribute_id;
    protected $attribute_set_id;
    protected $attribute_id;
    protected $status;
    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'attribute_set_2_attribute';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Get the value of attribute_set_2_attribute_id
     */ 
    public function getAttribute_set_2_attribute_id()
    {
        return $this->attribute_set_2_attribute_id;
    }

    /**
     * Set the value of attribute_set_2_attribute_id
     *
     * @return  self
     */ 
    public function setAttribute_set_2_attribute_id($attribute_set_2_attribute_id)
    {
        $this->attribute_set_2_attribute_id = $attribute_set_2_attribute_id;

        return $this;
    }

    /**
     * Get the value of attribute_set_id
     */ 
    public function getAttribute_set_id()
    {
        return $this->attribute_set_id;
    }

    /**
     * Set the value of attribute_set_id
     *
     * @return  self
     */ 
    public function setAttribute_set_id($attribute_set_id)
    {
        $this->attribute_set_id = $attribute_set_id;

        return $this;
    }

    /**
     * Get the value of attribute_id
     */ 
    public function getAttribute_id()
    {
        return $this->attribute_id;
    }

    /**
     * Set the value of attribute_id
     *
     * @return  self
     */ 
    public function setAttribute_id($attribute_id)
    {
        $this->attribute_id = $attribute_id;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }
  }