<?php

namespace Models;

use \Library\Mongodb;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use \MongoDB\BSON\ObjectID as MongoDbObjectId;

class ProductReview
{

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $review_id;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $url_key;


    protected $customer_id;

    protected $customer_email;

    protected $customer_first_name;

    protected $customer_last_name;

    protected $customer_alias;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $order_no;
    protected $invoice_no;

    /**
     *
     * @var integer
     * @Column(type="boolean", true, false)
     */
    protected $datagroup = false;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $product_quality;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $product_accuration;

    protected $sku;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $review;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $show_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $errors;
    protected $errorCode;

    protected $messages;

    protected $action;

    public function __construct()
    {
        $mongoDb = new Mongodb();
        $mongoDb->setCollection("product_review");
        $this->collection = $mongoDb->getCollection();
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_review';
    }

    /**
     * @return int
     */
    public function getReviewId()
    {
        return $this->review_id;
    }

    /**
     * @param int $review_id
     */
    public function setReviewId($review_id)
    {
        $this->review_id = $review_id;
    }

    /**
     * @return int
     */
    public function getUrlKey()
    {
        return $this->url_key;
    }

    /**
     * @param int $url_key
     */
    public function setUrlKey($url_key)
    {
        $this->url_key = $url_key;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }


    /**
     * @return string
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * @return mixed
     */
    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    /**
     * @param mixed $customer_email
     */
    public function setCustomerEmail($customer_email)
    {
        $this->customer_email = $customer_email;
    }

    /**
     * @return mixed
     */
    public function getCustomerFirstName()
    {
        return $this->customer_first_name;
    }

    /**
     * @param mixed $customer_first_name
     */
    public function setCustomerFirstName($customer_first_name)
    {
        $this->customer_first_name = $customer_first_name;
    }

    /**
     * @return mixed
     */
    public function getCustomerLastName()
    {
        return $this->customer_last_name;
    }

    /**
     * @param mixed $customer_last_name
     */
    public function setCustomerLastName($customer_last_name)
    {
        $this->customer_last_name = $customer_last_name;
    }

    /**
     * @return mixed
     */
    public function getCustomerAlias()
    {
        return $this->customer_alias;
    }

    /**
     * @param mixed $customer_alias
     */
    public function setCustomerAlias($customer_alias)
    {
        $this->customer_alias = $customer_alias;
    }

    /**
     * @param string $order_no
     */
    public function setOrderNo($order_no)
    {
        $this->order_no = $order_no;
    }

    /**
     * @return int
     */
    public function getProductQuality()
    {
        return $this->product_quality;
    }

    /**
     * @param int $product_quality
     */
    public function setProductQuality($product_quality)
    {
        $this->product_quality = $product_quality;
    }

    /**
     * @return int
     */
    public function getProductAccuration()
    {
        return $this->product_accuration;
    }

    /**
     * @param int $product_accuration
     */
    public function setProductAccuration($product_accuration)
    {
        $this->product_accuration = $product_accuration;
    }

    /**
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param string $review
     */
    public function setReview($review)
    {
        $this->review = $review;
    }

    /**
     * @return int
     */
    public function getShowName()
    {
        return $this->show_name;
    }

    /**
     * @param int $show_name
     */
    public function setShowName($show_name)
    {
        $this->show_name = $show_name;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errors
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function dataValidation($params){
        $validateAttribute = array("url_key","sku","customer_email","customer_first_name","customer_last_name","order_no","product_quality","product_accuration");

        $errors = array();
        foreach($params as $keyParam =>$param){
            if (in_array($keyParam, $validateAttribute)){
                if(empty($param)){
                    $errors[] = $keyParam." required";
                }
            }
        }
        if(count($errors)>0){
            $this->setErrors($errors);
            return false;
        }else{
            return true;
        }
    }

    public function reviewList(){

        $search_param = array();
        $reviewData = array();
        if(!empty($this->review_id)){
            $id = new MongoDbObjectId($this->review_id);
            $search_param['_id'] = $id;
        }

        if(!empty($this->product_id)){
            $productId = $this->product_id;
            $search_param['url_key'] = (int)$productId;
        }

        if(!empty($this->invoice_no)){
            $invoiceNo = $this->invoice_no;
            $search_param['invoice_no'] = (string)$invoiceNo;
        }

        if(!empty($this->sku)){
            $sku = $this->sku;
            $search_param['sku'] = (string)$sku;
        }

        if(!empty($this->customer_email)){
            $customerEmail = $this->customer_email;
            $search_param['customer_email'] = (string)$customerEmail;
        }

        if(!empty($this->customer_id)){
            $customer_id = $this->customer_id;
            $search_param['customer_id'] = (int)$customer_id;
        }

        if(!empty($this->status)){
            $status = (int)$this->status;
            $search_param['status'] = $status;
        }


        if (empty($search_param)) {
            $reviewList = $this->collection->find();
        } else {
            $reviewList = $this->collection->find($search_param);

        }

        foreach ($reviewList as $review) {
            $reviewOid = (array)$review['_id'];
            $reviewId = $reviewOid['oid'];
            unset($review['_id']);
            $reviewData[] = array("review_id" => $reviewId) + $review;
        }

        $recordsTotal = count($reviewData);
        $data['list'] = $reviewData;
        $data['recordsTotal'] = $recordsTotal;
        return $data;
    }


    public function average(){

        $search_param = array();
        $reviewData = array();
        if(!empty($this->review_id)){
            $id = new MongoDbObjectId($this->review_id);
            $search_param['_id'] = $id;
        }

        if(!empty($this->invoice_no)){
            $invoice_no = (string)$this->invoice_no;
            $filter[] = array(
                '$match' => array("invoice_no" => "$invoice_no")
            );
        }

        if(!empty($this->sku)){
            $sku = (string)$this->sku;
            $filter[] = array(
                '$match' => array("sku" => "$sku")
            );
        }

        if(!empty($this->status)){
            $status = (int)$this->status;
            $filter[] = array(
                '$match' => array("status" => $status)
            );
        }

        $filter[] =
            array(
                '$group' => array(
                    "_id" => array("sku" => '$sku',"status" => '$status'),
                    'average_product_quality' => array('$avg' => '$product_quality'),
                    'average_product_accuration' => array('$avg' => '$product_accuration'),
                    "total_data" => array('$sum' => 1)
                )

        );

        $avarage_total = 0;
        $uniqueLists = $this->collection->aggregate($filter);
        foreach($uniqueLists as $sku){
            if(!empty($sku['_id']['sku'])) {
                $data['sku'] = $sku['_id']['sku'];
                $data['status'] = $sku['_id']['status'];
                $avarage_total = number_format(($sku['average_product_quality']+$sku['average_product_accuration'])/2,2);
                $total_all[$data['sku']][] = $avarage_total;
                $quality[$data['sku']][] = $sku['average_product_quality'];
                $accuration[$data['sku']][] = $sku['average_product_accuration'];
                unset($sku['_id']);
                $sku['average_product_quality']    = number_format($sku['average_product_quality'],2);
                $sku['average_product_accuration'] = number_format($sku['average_product_quality'],2);
                if ($data['status'] == 1) {
                    $reviewData[$data['sku']]['approved'] = $data + $sku + array("avarage_total"=>$avarage_total);
                } elseif ($data['status'] == 2) {
                    $reviewData[$data['sku']]['not_approved'] = $data + $sku + array("avarage_total"=>$avarage_total);
                } else {
                    $reviewData[$data['sku']]['new'] = $data + $sku + array("avarage_total"=>$avarage_total);
                }

                $reviewData[$data['sku']]['all'] = $data + $sku + array("avarage_total"=>number_format(array_sum($total_all[$data['sku']])/(count($total_all[$data['sku']])),2));
                $reviewData[$data['sku']]['all']['status'] = "all";
                $reviewData[$data['sku']]['all']['average_product_quality'] = number_format(array_sum($quality[$data['sku']])/(count($quality[$data['sku']])),2);
                $reviewData[$data['sku']]['all']['average_product_accuration'] = number_format(array_sum($accuration[$data['sku']])/(count($accuration[$data['sku']])),2);
                ksort($reviewData[$data['sku']]);
            }
        }

        $recordsTotal = count($reviewData);

        $data['list'] = $reviewData;
        $data['recordsTotal'] = $recordsTotal;
        return $data;
    }


    public function saveReview($params){
        try {

            $validate = $this->dataValidation($params);
            $params['product_quality'] = isset($params['product_quality'])? (int)$params['product_quality']: 0 ;
            $params['product_accuration'] = isset($params['product_accuration'])? (int)$params['product_accuration']: 0 ;
            $params['status']           = isset($params['status'])? (int)$params['status'] : 0 ;
            $params['supplier_id']      = isset($params['supplier_id'])? (int)$params['supplier_id']: 0 ;
            $params['customer_id']      = isset($params['customer_id'])? (int)$params['customer_id']: 0 ;

            $now = date("Y-m-d H:i:s");
            if($validate == true) {
                if (!empty($params['review_id'])) {

                    $this->setReviewId($params['review_id']);
                    $id = new MongoDbObjectId($params['review_id']);
                    unset($params['review_id']);

                    $review = $this->reviewList()['list'];
                    if(!empty($review[0])) {
                        $params = $params + $review[0];
                    }else{
                        $params = $params;
                    }
                    $params['created_at'] = $review[0]['created_at'];
                    $params['updated_at'] = $now;
                    $saveReview = $this->collection->findOneAndReplace(['_id' => $id], $params);
                    if ($saveReview) {
                        $this->setMessages("Update succes");
                    } else {

                    }
                } else {
                    $params['created_at'] = $now;
                    $params['updated_at'] = "";
                    $params['status'] = 0;
                    $saveReview = $this->collection->insertOne($params);
                    if ($saveReview) {
                        $this->setMessages("Insert succes");
                    }
                }
            }else{
                return;
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

    }

}
