<?php

namespace Models;

use \Library\APIWrapper;
use Phalcon\Db as Database;
class SalesOrderItemGiftcards extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_order_item_giftcards_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_item_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $rule_id;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $voucher_code;

    protected $voucher_amount;

    protected $coin_amount;

    protected $voucher_expired;

    protected $voucher_amount_used;

    protected $voucher_amount_difference;

    protected $split_voucher_amount_difference;

    protected $split_discount_amount_difference;

    protected $can_combine;

    protected $global_can_combine;

    protected $voucher_type;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sales_order_item_id', 'Models\SalesOrderItem', 'sales_order_item_id', array('alias' => 'SalesOrderItem'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_order_item_giftcards';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderItemFlat[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderItemFlat
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param array $data_array
     */
    public function setFromArray($data_array = array()) 
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function assignAllValue($val) {
        foreach($val as $keyVal => $value) {
            if (is_array($value)) {
                $this->assignAllValue($value);
                
            } else if(property_exists($this, $keyVal)) {
                $this->{$keyVal} = $value;
            }
        }
    }

    /**
     * @param array $columns
     * @param bool $showEmpty
     * @return mixed
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    /**
     * Get the value of sales_order_item_giftcards_id
     *
     * @return  integer
     */ 
    public function getSalesOrderItemGiftcardsId()
    {
        return $this->sales_order_item_giftcards_id;
    }

    /**
     * Set the value of sales_order_item_giftcards_id
     *
     * @param  integer  $sales_order_item_giftcards_id
     *
     * @return  self
     */ 
    public function setSalesOrderItemGiftcardsId($sales_order_item_giftcards_id)
    {
        $this->sales_order_item_giftcards_id = $sales_order_item_giftcards_id;

        return $this;
    }

    /**
     * Get the value of sales_order_item_id
     *
     * @return  integer
     */ 
    public function getSalesOrderItemId()
    {
        return $this->sales_order_item_id;
    }

    /**
     * Set the value of sales_order_item_id
     *
     * @param  integer  $sales_order_item_id
     *
     * @return  self
     */ 
    public function setSalesOrderItemId($sales_order_item_id)
    {
        $this->sales_order_item_id = $sales_order_item_id;

        return $this;
    }

    /**
     * Get the value of rule_id
     *
     * @return  integer
     */ 
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * Set the value of rule_id
     *
     * @param  integer  $rule_id
     *
     * @return  self
     */ 
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;

        return $this;
    }

    /**
     * Get the value of voucher_code
     *
     * @return  string
     */ 
    public function getVoucherCode()
    {
        return $this->voucher_code;
    }

    /**
     * Set the value of voucher_code
     *
     * @param  string  $voucher_code
     *
     * @return  self
     */ 
    public function setVoucherCode(string $voucher_code)
    {
        $this->voucher_code = $voucher_code;

        return $this;
    }

    /**
     * Get the value of voucher_amount
     */ 
    public function getVoucherAmount()
    {
        return $this->voucher_amount;
    }

    /**
     * Set the value of voucher_amount
     *
     * @return  self
     */ 
    public function setVoucherAmount($voucher_amount)
    {
        $this->voucher_amount = $voucher_amount;

        return $this;
    }

    /**
     * Get the value of coin_amount
     */ 
    public function getCoinAmount()
    {
        return $this->coin_amount;
    }

    /**
     * Set the value of coin_amount
     *
     * @return  self
     */ 
    public function setCoinAmount($coin_amount)
    {
        $this->coin_amount = $coin_amount;

        return $this;
    }

    /**
     * Get the value of voucher_expired
     */ 
    public function getVoucherExpired()
    {
        return $this->voucher_expired;
    }

    /**
     * Set the value of voucher_expired
     *
     * @return  self
     */ 
    public function setVoucherExpired($voucher_expired)
    {
        $this->voucher_expired = $voucher_expired;

        return $this;
    }

    /**
     * Get the value of voucher_amount_used
     */ 
    public function getVoucherAmountUsed()
    {
        return $this->voucher_amount_used;
    }

    /**
     * Set the value of voucher_amount_used
     *
     * @return  self
     */ 
    public function setVoucherAmountUsed($voucher_amount_used)
    {
        $this->voucher_amount_used = $voucher_amount_used;

        return $this;
    }

    /**
     * Get the value of voucher_amount_difference
     */ 
    public function getVoucherAmountDifference()
    {
        return $this->voucher_amount_difference;
    }

    /**
     * Set the value of voucher_amount_difference
     *
     * @return  self
     */ 
    public function setVoucherAmountDifference($voucher_amount_difference)
    {
        $this->voucher_amount_difference = $voucher_amount_difference;

        return $this;
    }

      /**
     * Get the value of split_voucher_amount_difference
     */ 
    public function getSplitVoucherAmountDifference()
    {
        return $this->split_voucher_amount_difference;
    }

    /**
     * Set the value of split_voucher_amount_difference
     *
     * @return  self
     */ 
    public function setSplitVoucherAmountDifference($split_voucher_amount_difference)
    {
        $this->split_voucher_amount_difference = $split_voucher_amount_difference;

        return $this;
    }

      /**
     * Get the value of split_discount_amount_difference
     */ 
    public function getSplitDiscountAmountDifference()
    {
        return $this->split_discount_amount_difference;
    }

    /**
     * Set the value of split_discount_amount_difference
     *
     * @return  self
     */ 
    public function setSplitDiscountAmountDifference($split_discount_amount_difference)
    {
        $this->split_discount_amount_difference = $split_discount_amount_difference;

        return $this;
    }

    /**
     * Get the value of can_combine
     */ 
    public function getCanCombine()
    {
        return $this->can_combine;
    }

    /**
     * Set the value of can_combine
     *
     * @return  self
     */ 
    public function setCanCombine($can_combine)
    {
        $this->can_combine = $can_combine;

        return $this;
    }

    /**
     * Get the value of global_can_combine
     */ 
    public function getGlobalCanCombine()
    {
        return $this->global_can_combine;
    }

    /**
     * Set the value of global_can_combine
     *
     * @return  self
     */ 
    public function setGlobalCanCombine($global_can_combine)
    {
        $this->global_can_combine = $global_can_combine;

        return $this;
    }

    /**
     * Get the value of voucher_type
     */ 
    public function getVoucherType()
    {
        return $this->voucher_type;
    }

    /**
     * Set the value of voucher_type
     *
     * @return  self
     */ 
    public function setVoucherType($voucher_type)
    {
        $this->voucher_type = $voucher_type;

        return $this;
    }
}
