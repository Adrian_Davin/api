<?php

namespace Models;

use Library\Store;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class ProductToLazada extends \Models\BaseModel
{
        /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $sku;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $xml;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $response;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $image_response;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $qty;

   /*
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $errors;

    protected $messages;

    protected $action;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_to_lazada';
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * @param string $xml
     */
    public function setXml($xml)
    {
        $this->xml = $xml;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param string $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function getImageRespnse()
    {
        return $this->image_response;
    }

    /**
     * @param string $image_response
     */
    public function setImageResponse($image_response)
    {
        $this->image_response = $image_response;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        unset($view["created_at"]);
        unset($view["updated_at"]);

        return $view;
    }

    public function getSupplierDetail(){
        $supplierParam = array(
            "conditions" => "supplier_id = ".$this->supplier_id
        );
        $supplier = parent::findFirst($supplierParam)->toArray();

        $account = new SupplierAccount();
        $supplier['account'] = $account::findFirst($supplierParam);
        if(!empty($supplier['account'])){
            $supplier['account']->toArray();
        }

        $address = new SupplierAddress();
        $supplier['address'] = $address::find($supplierParam)->toArray();
        return $supplier;

    }

    public function saveSupplierProfile($log_file_name = "")
    {
        try {
            unset($this->created_at);
            $saveStatus = $this->saveData("supplier");
            $action = $this->action;
            if($saveStatus) {
                if (empty($this->supplier_id)) {
                    $lastSupplier = parent::findFirst(array(
                        "columns" => "supplier_id",
                        "order" => "supplier_id DESC"
                    ));
                    $this->supplier_alias = "MP" . $lastSupplier['supplier_id'];
                    $this->supplier_id = $lastSupplier['supplier_id'];
                } else {
                    $this->supplier_alias = "MP" . $this->supplier_id;
                }

                $this->account['supplier_id'] = $this->address['supplier_id'] = $this->supplier_id;
                $this->account['action'] = $this->address['action'] = $this->action;

                $account = new SupplierAccount();
                $account->setFromArray($this->account);
                $account->saveAccount();

                $address = new SupplierAddress();
                $address->setFromArray($this->address);
                $lastAddress = $address->saveAddress();


                //create store warehouse
                $storeAddress['supplier_id'] = $this->supplier_id;
                $storeAddress['supplier_address_id'] = $lastAddress;
                $storeAddress['name'] = $this->name."(Warehouse MP)";
                $storeAddress['type'] = 2;
                $storeAddress['zone_id'] = 1;
                $storeAddress['status'] = 10;
                $storeAddress['store_code'] = 'M'.$lastAddress;
                $store = new \Models\Store();
                $store->setFromArray($storeAddress);
                $store->saveStore();


                $phql = "UPDATE supplier SET supplier_alias = '$this->supplier_alias' WHERE supplier_id = $this->supplier_id";
                $this->getDi()->getShared('dbMaster')->execute($phql);
                return $this->setSupplierId($this->supplier_id);
            }

            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("Supplier", "Supplier save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Save/Update Supplier failed";

                $transaction->rollback(
                    "Save/Update Supplier failed"
                );
                return false;
            }
        } catch (TxFailed $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;

    }
}
