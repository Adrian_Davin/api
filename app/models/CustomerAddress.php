<?php

namespace Models;

class CustomerAddress extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $address_id = 0;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $customer_id = 0;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $country_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $province_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $city_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $kecamatan_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $kelurahan_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $geolocation = '';

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $first_name = '';

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $last_name = '';

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $phone = '';

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $full_address = '';

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $post_code = '';

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $address_type;

    /**
     *
     * @var integer
     * @Column(type="integer", length=3, nullable=true)
     */
    protected $is_default;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    protected $address_name;

    /**
     *
     * @var integer
     * @Column(type="integer", length=3, nullable=true)
     */
    protected $is_geolocation_valid;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer', "reusable" => true));
        $this->belongsTo('city_id', 'Models\MasterCity', 'city_id', array('alias' => 'MasterCity', "reusable" => true));
        $this->belongsTo('country_id', 'Models\MasterCountry', 'country_id', array('alias' => 'MasterCountry', "reusable" => true));
        $this->belongsTo('kecamatan_id', 'Models\MasterKecamatan', 'kecamatan_id', array('alias' => 'MasterKecamatan', "reusable" => true));
        $this->belongsTo('kelurahan_id', 'Models\MasterKelurahan', 'kelurahan_id', array('alias' => 'MasterKelurahan', "reusable" => true));
        $this->belongsTo('province_id', 'Models\MasterProvince', 'province_id', array('alias' => 'MasterProvince', "reusable" => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_address';
    }

    /**
     * @return int
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * @param int $address_id
     */
    public function setAddressId($address_id)
    {
        $this->address_id = $address_id;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return int
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * @param int $country_id
     */
    public function setCountryId($country_id)
    {
        $this->country_id = $country_id;
    }

    /**
     * @return int
     */
    public function getProvinceId()
    {
        return $this->province_id;
    }

    /**
     * @param int $province_id
     */
    public function setProvinceId($province_id)
    {
        $this->province_id = $province_id;
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * @param int $city_id
     */
    public function setCityId($city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @return int
     */
    public function getKecamatanId()
    {
        return $this->kecamatan_id;
    }

    /**
     * @param int $kecamatan_id
     */
    public function setKecamatanId($kecamatan_id)
    {
        $this->kecamatan_id = $kecamatan_id;
    }

    /**
     * @return int
     */
    public function getKelurahanId()
    {
        return $this->kelurahan_id;
    }

    /**
     * @param int $kelurahan_id
     */
    public function setKelurahanId($kelurahan_id)
    {
        $this->kelurahan_id = $kelurahan_id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }


    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        return $this->full_address;
    }

    /**
     * @return string
     */
    public function getGeolocation()
    {
        return $this->geolocation;
    }

    /**
     * @param string $full_address
     */
    public function setFullAddress($full_address)
    {
        $this->full_address = $full_address;
    }

    /**
     * @param string $geolocation
     */
    public function setGeolocation($geolocation)
    {
        $this->geolocation = $geolocation;
    }

    /**
     * @return string
     */
    public function getPostCode()
    {
        return $this->post_code;
    }

    /**
     * @param string $post_code
     */
    public function setPostCode($post_code)
    {
        $this->post_code = $post_code;
    }

    /**
     * @return string
     */
    public function getAddressType()
    {
        return $this->address_type;
    }

    /**
     * @param string $address_type
     */
    public function setAddressType($address_type)
    {
        $this->address_type = $address_type;
    }

    /**
     * @return int
     */
    public function getIsDefault()
    {
        return $this->is_default;
    }

    /**
     * @param int $is_default
     */
    public function setIsDefault($is_default)
    {
        $this->is_default = $is_default;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return int
     */
    public function getIsGeolocationValid()
    {
        return $this->is_geolocation_valid;
    }

    /**
     * @param int $is_geolocation_valid
     */
    public function setIsGeolocationValid($is_geolocation_valid)
    {
        $this->is_geolocation_valid = $is_geolocation_valid;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_customer_address", "TRIGGERED: " .json_encode($this->toArray()));
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }

            if($key == 'country') {
                $data_array['country_id'] = $data_array['country']['country_id'];
                $this->country_id = $data_array['country']['country_id'];
            }

            if($key == 'province') {
                $data_array['province_id'] = $data_array['province']['province_id'];
                $this->province_id = $data_array['province']['province_id'];
            }

            if($key == 'city') {
                $data_array['city_id'] = $data_array['city']['city_id'];
                $this->city_id = $data_array['city']['city_id'];
            }

            if($key == 'kecamatan') {
                $data_array['kecamatan_id'] = $data_array['kecamatan']['kecamatan_id'];
                $this->kecamatan_id = $data_array['kecamatan']['kecamatan_id'];
            }

            if($key == 'kelurahan') {
                $data_array['kelurahan_id'] = $data_array['kelurahan']['kelurahan_id'];
                $this->kelurahan_id = $data_array['kelurahan']['kelurahan_id'];
            }
        }

        if ((isset($data_array['is_kelurahan_invalid']))) {
            $data_array['kelurahan_id'] = null;
            array_push($this->forceUpdateFields, 'kelurahan_id');
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        unset($view['country_id']);
        unset($view['province_id']);
        unset($view['city_id']);
        unset($view['kecamatan_id']);
        unset($view['kelurahan_id']);

        if(!empty($this->country_id)) {
            $view['country'] = $this->MasterCountry->toArray();
        }

        if(!empty($this->province_id)) {
            $view['province'] = $this->MasterProvince->toArray(['province_id','province_code','province_name']);
        }

        if(!empty($this->city_id)) {
            $view['city'] = $this->MasterCity->toArray(['city_id','city_name']);
        }

        if(!empty($this->kecamatan_id)) {
            $view['kecamatan'] = $this->MasterKecamatan->toArray(['kecamatan_id','kecamatan_name','kecamatan_code']);
        }

        if(!empty($this->kelurahan_id)) {
            $view['kelurahan'] = $this->MasterKelurahan->toArray(['kelurahan_id', 'kelurahan_name', 'post_code', 'geolocation']);
        }


        return $view;
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }
    /**
     * @param mixed $address_name
     */
    public function setAddressName($address_name)
    {
        $this->address_name = $address_name;
    }

    public function setForceUpdateFields($fields = array())
    {
        $this->forceUpdateFields = array_merge($this->forceUpdateFields, $fields);
    }

}
