<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 11/9/2016
 * Time: 9:31 AM
 */

namespace Models;


class MarketplaceSellerAddress extends \Models\BaseModel
{
    public function initialize()
    {
        $this->setSource("supplier_address");
    }

    public function getMarketPlaceAddressDb()// get marketplace data using database
    {
        $sql = "SELECT
                    (
                        SELECT
                          province_name
                        FROM
                          master_province
                        WHERE
                          province_id = (
                                    SELECT
                                        province_id
                                    FROM
                                        master_city WHERE city_id = (SELECT city_id FROM master_kecamatan WHERE kecamatan_id = ".$this->setSource.".kecamatan_id)
                                    )
                    ) as province,
                    (
                        SELECT
                            kecamatan
                        FROM
                            master_kecamatan
                        WHERE
                            ".$this->setSource.".kecamatan_id=master_kecamatan.kecamatan_id
                    ) as kecamatan,
                    (
                        SELECT
                            city_name
                        FROM
                            master_city WHERE city_id = (SELECT city_id FROM master_kecamatan WHERE kecamatan_id = ".$this->setSource.".kecamatan_id)
                    ) as city
                FROM
                    ".$this->setSource."
                WHERE
                    supplier_address_type='2' AND
                    supplier_id = ''
                ";

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $hasil = $result->fetchAll();
            return $hasil;
        } else {
            return FALSE;
        }
    }

}