<?php

namespace Models;

// use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
// use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerOtp extends \Models\BaseModel
{
  /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $customer_otp_id;

    protected $customer_id;

    protected $phone;
    
    protected $email;

    protected $is_phone_verified;
    
    protected $is_email_verified;

    protected $expired_on;

    protected $created_at;

    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_otp';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return int
     */
    public function getCustomerOtpId()
    {
        return $this->customer_otp_id;
    }

    /**
     * @param int $customer_otp_id
     */
    public function setCustomerOtpId($customer_otp_id)
    {
        $this->customer_otp_id = $customer_otp_id;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getExpiredOn()
    {
        return $this->expired_on;
    }

    public function setExpiredOn($expired_on)
    {
        $this->expired_on = $expired_on;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getIsPhoneVerified()
    {
        return $this->is_phone_verified;
    }

    /**
     * @param mixed $is_phone_verified
     */
    public function setIsPhoneVerified($is_phone_verified)
    {
        $this->is_phone_verified = $is_phone_verified;
    }

    /**
     * @return mixed
     */
    public function getIsEmailVerified()
    {
        return $this->is_email_verified;
    }

    /**
     * @param mixed $is_email_verified
     */
    public function setIsEmailVerified($is_email_verified)
    {
        $this->is_email_verified = $is_email_verified;
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
          if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
  }

  public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}