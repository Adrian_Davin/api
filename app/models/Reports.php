<?php

namespace Models;
use Phalcon\Db as Database;

class Reports extends \Models\BaseModel
{

    protected $errors;
    protected $messages;

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }



    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'supplier_report';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Event[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Event
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setEventFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {

                    $this->{$event} = $val;
                }
            }
    }

    private function _custom_RDBM_SQL($array = FALSE){
        $result = array();
        foreach($array as $key => $value){
            $result['key'][] = $key;
            $result['value'][] = $value;
        }

        return $result;
    }

    public function update_($data, $table, $conditions)
    {
        $success = $this->getDi()->getShared($this->getConnection())->updateAsDict($table,$data,$conditions);
        return ($success)? $this->getDi()->getShared($this->getConnection())->affectedRows() : FALSE;

    }

    public function insert($data, $table)
    {
        $formatted_data = $this->_custom_RDBM_SQL($data);

        try{
            $success = $this->getDi()->getShared($this->getConnection())->insert($table,$formatted_data['value'],$formatted_data['key']);
        }catch (\Exception $e){
            return false;
        }

        return ($success)? $this->getDi()->getShared($this->getConnection())->lastInsertId($table) : FALSE;

    }

}
