<?php

namespace Models;

class ProductSynonym extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $synonym_id;
    protected $query_text;
    protected $synonym_for;
    protected $status;
    protected $created_at;
    protected $updated_at;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getSynonymId()
    {
        return $this->synonym_id;
    }

    /**
     * @param int $synonym_id
     */
    public function setSynonymId($synonym_id)
    {
        $this->synonym_id = $synonym_id;
    }

    /**
     * @return mixed
     */
    public function getQueryText()
    {
        return $this->query_text;
    }

    /**
     * @param mixed $query_text
     */
    public function setQueryText($query_text)
    {
        $this->query_text = $query_text;
    }

    /**
     * @return mixed
     */
    public function getSynonymFor()
    {
        return $this->synonym_for;
    }

    /**
     * @param mixed $synonym_for
     */
    public function setSynonymFor($synonym_for)
    {
        $this->synonym_for = $synonym_for;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_synonym';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

}
