<?php

namespace Models;

class ProductAttributeVarchar extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $attribute_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $value;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('attribute_id', 'Models\Attribute', 'attribute_id', array('alias' => 'Attribute'));
        $this->belongsTo('product_id', 'Models\Product', 'product_id', array('alias' => 'Product'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_attribute_varchar';
    }

    /**
     * @return int
     */
    public function getAttributeId()
    {
        return $this->attribute_id;
    }

    /**
     * @param int $attribute_id
     */
    public function setAttributeId($attribute_id)
    {
        $this->attribute_id = $attribute_id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductAttributeVarchar[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductAttributeVarchar
     */
    public static function findFirst($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = array();
        $attributeArray = $this->Attribute->toArray();
        $view['attribute_id'] = $this->attribute_id;
        $view['name'] = $attributeArray['code'];
        $view['label'] = $attributeArray['label'];
        if (isset($attributeArray['attribute_unit_id'])) {
            $view['attribute_unit_value'] = \Models\AttributeUnitValue::find(array('columns' => 'value','conditions' => ' attribute_unit_id = '.$attributeArray['attribute_unit_id']))->toArray();
        }

        $view['value'] = $this->value;

        return $view;
    }

    public function saveData($log_file_name = "", $state = "")
    {
        if(empty($this->product_id)) {
            \Helpers\LogHelper::log("product", "Product variant attribute varchar save failed, error : product_id is empty");
            return "Save product variant attribute need product_variant_id";
        }

        if(empty($this->attribute_id)) {
            \Helpers\LogHelper::log("product", "Product variant attribute varchar save failed, error : attribute_id is empty");
            return "Save product variant attribute_id is empty";
        }

        if ($this->save() === false) {

            $messages = $this->getMessages();

            $errMsg = array();
            foreach ($messages as $message) {
                $errMsg[] = $message->getMessage();
            }

            \Helpers\LogHelper::log("product", "Product attribute varchar save failed, error : " . json_encode($errMsg));
            return "Save product 2 category failed";
        }

        return;
    }

    public function updateBatch($params = array()){
        try {
            $sql = "
                update product_attribute_varchar set ".$params['stringSet']." where attribute_id = ".$params['attributeId']." and product_id in (
                    select product_id from product_variant where sku = '".$params['sku']."'
                )
                
            ";
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            if($result){
                $affectedRows = $this->getDi()->getShared('dbMaster')->affectedRows();

                return $affectedRows;

            }else{
                return false;
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("Product Variant Batch", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'Product Variant Batch','message' => 'There have something error, in query batch'));
            return false;
        }
    }

}
