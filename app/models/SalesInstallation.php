<?php

namespace Models;

use Helpers\LogHelper;
class SalesInstallation extends \Models\BaseModel
{
 
    protected $sales_installation_id;
    protected $reference_pending_order_id;
    protected $installation_order_no;
    protected $installation_invoice_id;
    protected $installation_cart_id;
    protected $status;
    protected $company_code;
    protected $installation_type;

    public function getSource()
    {
        return 'sales_installation';
    }
    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {        

    }  

    public function setFromArray($dataArray = array())
    {
        foreach($dataArray as $key => $val) {
            $this->{$key} = $val;
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getSalesInstallationID()
    {
        return $this->sales_installation_id;
    }

    public function getReferencePendingOrderID()
    {
        return $this->reference_pending_order_id;
    }

    public function getInstallationCartID() 
    {
        return $this->installation_cart_id;
    }

    public function getInstallationOrderNo() 
    {
        return $this->installation_order_no;
    }

    public function getInstallationInvoiceID()
    {
        return $this->installation_invoice_id;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCompanyCode(){
        return $this->company_code;
    }

    public function setSalesInstallationID($sales_installation_id)
    {
        $this->sales_installation_id = $sales_installation_id;
    }

    public function setInstallationCartID($installation_cart_id)
    {
        $this->installation_cart_id = $installation_cart_id;
    }

    public function setInstallationOrderNo($installation_order_no)
    {
        $this->installation_order_no = $installation_order_no;
    }

    public function setInstallationInvoiceID($installation_invoice_id)
    {
        $this->installation_invoice_id = $installation_invoice_id;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setInstallationType($installation_type)
    {
        $this->installation_type = $installation_type;
    }

    public function getInstallationType() {
        return $this->installation_type;
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}