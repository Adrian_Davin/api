<?php

namespace Models;

class MasterKecamatan extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $kecamatan_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $city_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $kecamatan_name;

    /**
     *
     * @var string
     * @Column(type="string", length=128, nullable=true)
     */
    public $coding;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $kecamatan_code;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $kode_jalur;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $sap_exp_code;


    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $geolocation;
    

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('kecamatan_id', 'Models\CustomerAddress', 'kecamatan_id', array('alias' => 'CustomerAddress'));
        $this->hasMany('kecamatan_id', 'Models\MasterKelurahan', 'kecamatan_id', array('alias' => 'MasterKelurahan'));
        $this->hasMany('kecamatan_id', 'Models\SalesOrderAddress', 'kecamatan_id', array('alias' => 'SalesOrderAddress'));
        $this->hasMany('kecamatan_id', 'Models\ShippingRate', 'destination_kecamatan_id', array('alias' => 'ShippingRate'));
        $this->hasMany('kecamatan_id', 'Models\SupplierAddress', 'kecamatan_id', array('alias' => 'SupplierAddress'));
        $this->belongsTo('city_id', 'Models\MasterCity', 'city_id', array('alias' => 'MasterCity'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_kecamatan';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getDataList($params = array())
    {
        $kecamatans = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterKecamatan::deleteKecamatans();
        } else {
            $kecamatans = \Library\Redis\MasterKecamatan::getKecamatanList();
        }

        if(empty($kecamatans)) {
            $result = $this->find("status = 1");
            //$result = $this->find();

            if($result) {
                $kecamatansRedis = array();
                $kecamatans = array();
                foreach($result as $kecamatan) {
                    $kecamatanArray = $kecamatan->toArray(array(), true);
                    $kecamatanArray['kelurahan'] = $kecamatan->MasterKelurahan->toArray();
                    $kecamatansRedis[$kecamatan->kecamatan_id] = $kecamatanArray;
                    $kecamatans[] = $kecamatanArray;
                }

                // save to redis
                \Library\Redis\MasterKecamatan::setKecamatans($kecamatansRedis);
            }
        }


        return $kecamatans;
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
    }

    public function getData($params = array())
    {
        $kecamatan = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterKecamatan::deleteKecamatan($params['kecamatan_id']);
        } else {
            $kecamatan = \Library\Redis\MasterKecamatan::getKecamatanData($params['kecamatan_id']);
        }

        if(empty($kecamatan)) {
            $kecamatanData = $this->findFirst(sprintf("kecamatan_id = %s AND status = 1", $params['kecamatan_id']));

            if($kecamatanData) {
                $kecamatan = $kecamatanData->toArray();
                $kecamatan['kelurahan'] = $kecamatanData->MasterKelurahan->toArray();
                \Library\Redis\MasterKecamatan::setKecamatan($params['kecamatan_id'],$kecamatan);
            }
        }

        return $kecamatan;
    }
}
