<?php

namespace Models;

use Phalcon\Db as Database;

class Event extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $event_id;
    protected $title;
    protected $term_conditions;
    protected $status;
    protected $start_date;
    protected $end_date;
    protected $big_banner;
    protected $small_banner;
    protected $is_active_small_banner;
    protected $old_url_key;
    protected $url_key;
    protected $products;
    protected $old_products;
    protected $created_at;
    protected $updated_at;
    protected $inline_script;
    protected $additional_header;
    protected $identifier;
    protected $company_code;
    protected $lock_qty;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getSmallBanner()
    {
        return $this->small_banner;
    }

    /**
     * @param mixed $small_banner
     */
    public function setSmallBanner($small_banner)
    {
        $this->small_banner = $small_banner;
    }

    /**
     * @return mixed
     */
    public function getIsActiveSmallBanner()
    {
        return $this->is_active_small_banner;
    }

    /**
     * @param mixed $is_active_small_banner
     */
    public function setIsActiveSmallBanner($is_active_small_banner)
    {
        $this->is_active_small_banner = $is_active_small_banner;
    }

    /**
     * @return mixed
     */
    public function getOldUrlKey()
    {
        return $this->old_url_key;
    }

    /**
     * @param mixed $old_url_key
     */
    public function setOldUrlKey($old_url_key)
    {
        $this->old_url_key = $old_url_key;
    }
    /**
     * @return mixed
     */
    public function getInlineScript()
    {
        return $this->inline_script;
    }

    /**
     * @param mixed $inline_script
     */
    public function setInlineScript($inline_script)
    {
        $this->inline_script = $inline_script;
    }

    /**
     * @return mixed
     */
    public function getAdditionalHeader()
    {
        return $this->additional_header;
    }

    /**
     * @param mixed $additional_header
     */
    public function setAdditionalHeader($additional_header)
    {
        $this->additional_header = $additional_header;
    }
    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }


    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTermConditions()
    {
        return $this->term_conditions;
    }

    /**
     * @param mixed $term_conditions
     */
    public function setTermConditions($term_conditions)
    {
        $this->term_conditions = $term_conditions;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return mixed
     */
    public function getBigBanner()
    {
        return $this->big_banner;
    }

    /**
     * @param mixed $big_banner
     */
    public function setBigBanner($big_banner)
    {
        $this->big_banner = $big_banner;
    }

    /**
     * @return mixed
     */
    public function getUrlKey()
    {
        return $this->url_key;
    }

    /**
     * @param mixed $url_key
     */
    public function setUrlKey($url_key)
    {
        $this->url_key = $url_key;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getOldProducts()
    {
        return $this->old_products;
    }

    /**
     * @param mixed $old_products
     */
    public function setOldProducts($old_products)
    {
        $this->old_products = $old_products;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @param int $event_id
     */
    public function setEventId($event_id)
    {
        $this->event_id = $event_id;
    }


    /**
     * Get the value of identifier
     */ 
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set the value of identifier
     *
     * @return  self
     */ 
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

        /**
     * Get the value of company_code
     */ 
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * Set the value of company_code
     *
     * @return  self
     */ 
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;

        return $this;
    }

    /**
     * Get the value of lock_qty
     */ 
    public function getLockQty()
    {
        return $this->lock_qty;
    }

    /**
     * Set the value of lock_qty
     *
     * @return  self
     */ 
    public function setLockQty($lock_qty)
    {
        $this->lock_qty = $lock_qty;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('event_id', 'SalesOrder', 'event_id', array('alias' => 'SalesOrder'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'event';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Event[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Event
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setEventFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {

                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function deleteEventProducts($eventID = FALSE) {
        if ($eventID) {
            $sql = "DELETE FROM event_products where event_id = $eventID";
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public function insertEventProducts($sku = FALSE, $storeCode = FALSE, $eventID = FALSE) {
        if ($sku && $storeCode && $eventID) {
            $sql = "INSERT INTO event_products (event_id, sku, store_code) VALUES ($eventID, '$sku', '$storeCode')";
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public function checkOverrideStatus($sku = FALSE, $companyCode = FALSE, $startDate = FALSE, $endDate = FALSE) {
        if ($sku && $companyCode && $startDate && $endDate) {
            $sql = "SELECT count(sku) as jml
                  FROM product_stock_override_status 
                  WHERE sku = '$sku' AND company_code = '$companyCode' AND 
                  start_date = '$startDate' AND end_date = '$endDate'";

            $eventModel = new \Models\Event();
            $eventModel->useReadOnlyConnection();
            $result = $eventModel->getDi()->getShared($eventModel->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $resultData = $result->fetchAll();
                if ($resultData[0]['jml'] > 0) {
                    return true;
                }
            }
        }

        return false;
    }

    public function insertOverrideStatus($sku = FALSE, $storeCode = FALSE, $status = FALSE, $companyCode = FALSE, $startDate = FALSE, $endDate = FALSE) {
        if ($sku && $storeCode && $companyCode && $startDate && $endDate) {
            $sql = "INSERT INTO product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) 
              VALUES ('$sku', '$storeCode', $status, '$companyCode', '$startDate', '$endDate')";
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public function deleteOverrideStatus($sku = FALSE, $companyCode = FALSE, $startDate = FALSE, $endDate = FALSE) {
        if ($sku && $companyCode && $startDate && $endDate) {
            $sql = "DELETE FROM product_stock_override_status WHERE 
            sku = '$sku' AND company_code = '$companyCode' AND 
            start_date = '$startDate' AND end_date = '$endDate'";

            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }
}
