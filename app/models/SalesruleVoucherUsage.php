<?php

namespace Models;

class SalesruleVoucherUsage extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=10, nullable=false)
     */
    public $voucher_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $customer_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    public $times_used;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
        $this->belongsTo('voucher_id', 'Models\SalesruleVoucher', 'voucher_id', array('alias' => 'SalesruleVoucher'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_voucher_usage';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleVoucherUsage[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleVoucherUsage
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
