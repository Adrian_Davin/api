<?php
/**
 * Created by PhpStorm.
 * User: roesmien_ecomm
 * Date: 2/8/2017
 * Time: 9:49 AM
 */

namespace Models;


class MasterBrand extends \Models\BaseModel
{
    public $brand_id;

    public $extra_information;

    public $name;

    public $name_old;

    public $status;

    public $created_at;

    public $updated_at;

    protected $errors;

    protected $messages;

    /**
     * @return mixed
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * @param mixed $brand_id
     */
    public function setBrandId($brand_id)
    {
        $this->brand_id = $brand_id;
    }

    /**
     * @return mixed
     */
    public function getExtraInformation()
    {
        return $this->extra_information;
    }

    /**
     * @param mixed $extra_information
     */
    public function setExtraInformation($extra_information)
    {
        $this->extra_information = $extra_information;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNameOld()
    {
        return $this->name_old;
    }

    /**
     * @param mixed $name_old
     */
    public function setNameOld($name_old)
    {
        $this->name_old = $name_old;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->whiteList = array();
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_brand';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {
//        $redisKey = self::_createKey($parameters);
//        $cache = [
//            "cache" => [
//                "key" => $redisKey,
//                "lifetime" => 7200,
//            ],
//        ];
//        if(!is_array($parameters)) {
//            $oldParam =  $parameters;
//            $parameters = array();
//            $parameters['conditions'] = $oldParam;
//        }
//        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
//        $redisKey = self::_createKey($parameters);
//        $cache = [
//            "cache" => [
//                "key" => $redisKey,
//                "lifetime" => 7200,
//            ],
//        ];
//        if(!is_array($parameters)) {
//            $oldParam =  $parameters;
//            $parameters = array();
//            $parameters['conditions'] = $oldParam;
//        }
//        $parameters = array_merge($parameters,$cache);

        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

}