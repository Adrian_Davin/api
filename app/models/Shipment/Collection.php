<?php
/**
 * Created by PhpStorm.
 * User: iman.suhardiman
 * Date: 28/11/2017
 * Time: 17:41 AM
 */

namespace Models\Shipment;


class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\SalesShipment
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $shipment = $this->current();
            $shipmentArray = $shipment->getDataArray();
            $view[$idx] = $shipmentArray;

            $this->next();
        }

        return $view;
    }

    public function setInvoiceId($invoice_id = 0)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $item = $this->current();
            $item->setInvoiceId($invoice_id);
            $this->next();
        }
        return $view;
    }

    public function setRealShippingAmount($real_shipping_amount = null)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $item = $this->current();
            $item->setRealShippingAmount($real_shipping_amount);
            $this->next();
        }
        return $view;
    }

    public function saveData($transaction = null)
    {
        $this->rewind();
        $shipmentModel = new \Models\SalesShipment();
        while ($this->valid()) {
            $shipment = $this->current();

            // check if exist, next
            if ($shipmentModel->checkExists()) {
                $this->next();
            }

            if($transaction) {
                $shipment->setTransaction($transaction);
            }

            try {
                $shipment->saveData("shipment");
            } catch (\Library\HTTPException $e) {
                throw new \Exception($e->devMessage);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            $this->next();
        }

        return true;
    }

}