<?php

namespace Models;

use \Library\APIWrapper;
use Phalcon\Db as Database;
class SalesOrderItemFlat extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_order_item_flat_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_item_id;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $sku;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $sales_source;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $supplier_company;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $department_name;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $consignment_control;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $brand_name;

    /**
     *
     * @var string
     * @Column(type="datetime", nullable=true)
     */
    protected $pre_order_date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $label_odi;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $conversion_source;

    /**
     * 
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $brand_store_name;
    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $device_source;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sales_order_item_id', 'Models\SalesOrderItem', 'sales_order_item_id', array('alias' => 'SalesOrderItem'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_order_item_flat';
    }

    /**
     * @return int
     */
    public function getSalesOrderItemFlatId()
    {
        return $this->sales_order_item_flat_id;
    }

    /**
     * @param int $sales_order_item_flat_id
     */
    public function setSalesOrderItemFlatId($sales_order_item_flat_id)
    {
        $this->sales_order_item_flat_id = $sales_order_item_flat_id;
    }

    /**
     * @return int
     */
    public function getSalesOrderItemId(): int
    {
        return $this->sales_order_item_id;
    }

    /**
     * @param int $sales_order_item_id
     */
    public function setSalesOrderItemId(int $sales_order_item_id)
    {
        $this->sales_order_item_id = $sales_order_item_id;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getSalesSource()
    {
        return $this->sales_source;
    }

    /**
     * @param string $sales_source
     */
    public function setSalesSource($sales_source)
    {
        $this->sales_source = $sales_source;
    }

    /**
     * @return string
     */ 
    public function getSupplierCompany()
    {
        return $this->supplier_company;
    }

    /**
     * @param  string $supplier_company
     */ 
    public function setSupplierCompany($supplier_company)
    {
        $this->supplier_company = $supplier_company;
    }

    /**
     * @return  string
     */ 
    public function getDepartmentName()
    {
        return $this->department_name;
    }

    /**
     * @param  string $department_name
     */ 
    public function setDepartmentName($department_name)
    {
        $this->department_name = $department_name;
    }

    /**
     * @return  string
     */ 
    public function getConsignmentControl()
    {
        return $this->consignment_control;
    }

    /**
     * @param  string $department_name
     */ 
    public function setConsignmentControl($consignment_control)
    {
        $this->consignment_control = $consignment_control;
    }

    /**
     * @return  string
     */ 
    public function getBrandName()
    {
        return $this->brand_name;
    }

    /**
     * @param  string $brand_name
     */ 
    public function setBrandName($brand_name)
    {
        $this->brand_name = $brand_name;
    }

    /**
     * @return  datetime
     */ 
    public function getPreOrderDate()
    {
        return $this->pre_order_date;
    }

    /**
     * @param  datetime $pre_order_date
     */ 
    public function setPreOrderDate($pre_order_date)
    {
        $this->pre_order_date = $pre_order_date;
    }

    /**
     * @return  string
     */ 
    public function getLabelOdi()
    {
        return $this->label_odi;
    }

    /**
     * @param  string $label_odi
     */ 
    public function setLabelOdi($label_odi)
    {
        $this->label_odi = $label_odi;
    }

    /**
     * @return  string
     */ 
    public function getConversionSource()
    {
        return $this->conversion_source;
    }

    /**
     * @param  string $conversion_source
     */ 
    public function setConversionSource($conversion_source)
    {
        $this->conversion_source = $conversion_source;
    }

    /**
     * @param string $brand_store_name
     */
    public function setBrandStoreName($brand_store_name) {
        $this->brand_store_name = $brand_store_name;
    }

    /**
     * @return string
     */
    public function getBrandStoreName() {
        return $this->brand_store_name;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderItemFlat[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderItemFlat
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param array $data_array
     */
    public function setFromArray($data_array = array()) 
    {
        $this->assign($data_array);

        $store_code = $data_array['shipping']['store_code'];
        $device = $data_array['device'];

        // Generate Sales Source
        $sales_source = $this->generateSalesSource($data_array['utm_parameter'], $data_array['reserved_order_no'], $data_array['store_code_new_retail'], $device, $data_array['insentif_ace'], $data_array['insentif_informa'], $data_array['insentif_selma'], $data_array['order_type']);

        $supplier_alias = "";
        $supplier_data = $this->getSupplierByStoreCode($store_code);

        if (count($supplier_data) > 0) {
            $supplier_alias = $supplier_data[0]['supplier_alias'];
        }
        // if (strpos($store_code, 'DC') !== false || substr($store_code, 0, 1) == 'M') {
        //     $supplier_alias = $data_array['supplier_alias'];
        if(substr($store_code, 0, 1) == 'J'){
            $supplier_alias = 'HCIR';
        }

        $brand_store_name = "";

        if (strpos($store_code, "DC") !== false) {
            $supplier_id = 0;
            if (count($supplier_data) > 0) {
                $supplier_id = $supplier_data[0]['supplier_id'];
            }

            if ($supplier_id == 60) {
                // Supplier_id == 60 is informa. For dc informa, using shop_id from sales_order_vendor to get brand_store_name is Informa / Rolka / Selma
                if ($device == "vendor") {
                    $query = "SELECT 
                        mbs.brand_store_name 
                    FROM 
                        sales_order_vendor sov 
                    INNER JOIN master_store_vendor msv ON msv.shop_id = sov.shop_id 
                    INNER JOIN master_brand_store mbs ON mbs.brand_store_id = msv.brand_store_id 
                    WHERE sov.order_no = '".$data_array['reserved_order_no']."'";
    
                    $result = $this->getDi()->getShared('dbReader')->query($query);
                    $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
                    $data = $result->fetchAll();
    
                    if (count($data) > 0) {
                        $brand_store_name = $data[0]['brand_store_name'];
                    }
                } elseif ($data_array["order_type"] == "b2b_informa") {
                    $brand_store_name = "Informa";
                } elseif ($data_array["order_type"] == "b2b_ace") {
                    $brand_store_name = "Ace";
                } elseif ($data_array["department_name"] == $_ENV["ROLKA_DEPARTMENT_NAME"]) {
                    $brand_store_name = "Rolka";
                } 
            } else {
                $brand_store_name = $this->getStoreNameByStoreCode($data_array['shipping']['store_code']);
            }
        } else {
            $brand_store_name = $this->getStoreNameByStoreCode($data_array['shipping']['store_code']);
        }
        
        $this->brand_store_name = $brand_store_name;
        $this->sales_source = $sales_source;
        $this->supplier_company = $supplier_alias;
        $this->department_name = $data_array['department_name'];
        $this->consignment_control = $data_array['consignment_control'];
        $this->brand_name = $data_array['brand'];
        $this->label_odi = $data_array['label']['ODI'];
        $this->conversion_source = !empty($data_array['conversion_source']) ? $data_array['conversion_source']: null;
        $this->device_source = !empty($data_array['device_source']) ? $data_array['device_source']: null;

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function assignAllValue($val) {
        foreach($val as $keyVal => $value) {
            if (is_array($value)) {
                $this->assignAllValue($value);
                
            } else if(property_exists($this, $keyVal)) {
                $this->{$keyVal} = $value;
            }
        }
    }

    /**
     * @param array $columns
     * @param bool $showEmpty
     * @return mixed
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public function generateSalesSource($utmParameter, $orderNo, $storeCodeNewRetail, $device, $insentifAce, $insentifInforma, $insentifSelma, $orderType) {
        if (strpos($utmParameter, "/ace/") !== false || strpos($utmParameter, "/acestore/") !== false){
            return "Ace Online";
        } else if (strpos($utmParameter, "/informa/") !== false || strpos($utmParameter, "/informastore/") !== false){
            return "Informa Online";
        } else if (strpos($utmParameter, "/selma/") !== false || strpos($utmParameter, "/selmastore/") !== false){
            return "Selma Online";
        } else if (strpos($utmParameter, "/toyskingdomonline/") !== false){
            return "Toys Kingdom Online";
        } else if (substr($orderNo, 0, 4) == "ODIT") {
            return "Tokopedia";
        } else if (substr($orderNo, 0, 4) == "ODIS") {
            return "Shopee";
        } else if (substr($orderNo, 0, 4) == "ODIK") {
            return "Tiktok";
        } else if (substr($orderNo, 0, 4) == "ODIB" || substr($orderNo, 0, 4) == "ODIC") {
            return "B2B";
        } else if (in_array($device, array("ace-android", "ace-ios"))) {
            if (!empty($storeCodeNewRetail)) {
                if ($insentifAce) {
                    return "Miss Ace Assisted";
                } else {
                    return "Miss Ace Independent";
                } 
            }
            return "Miss Ace Homeshopping";
        } else if (in_array($device, array("informa-android", "informa-ios", "informa-huawei"))) {
            if (!empty($storeCodeNewRetail)) {
                if ($insentifInforma) {
                    return "Informa Apps Assisted";
                } else {
                    return "Informa Apps Independent";
                }
            }
            return "Informa Apps Homeshopping";
        } else if (in_array($device, array("selma-android", "selma-ios", "selma-huawei"))) {
            if (!empty($storeCodeNewRetail)) {
                if ($insentifSelma) {
                    return "Selma Apps Assisted";
                } else {
                    return "Selma Apps Independent";
                }
            }
            return "Selma Apps Homeshopping";
        } else if (!empty($storeCodeNewRetail) && (substr($orderNo, 0, 4) == "ODIR")) {
            return "Storemode";
        }  else if ($orderType == "wa_shopping") {
            return "Whatsapp";
        } else if ($orderType == "ctwa_shopping") {
            return "Whatsapp CTWA";
        } else if (in_array($device, array("android", "ios","rr-android","rr-ios"))) {
            return "RR MApps";
        } else if (!in_array($device, array("android", "ios","rr-android","rr-ios"))) {
            return "RR Webs";
        }

        return "Undefined";
    }

    public function getStoreNameByStoreCode(string $store_code) {
        $brand_store_name = "";

        $query = "SELECT 
            mbs.brand_store_name 
        FROM store s 
        INNER JOIN master_brand_store mbs ON mbs.brand_store_id = s.brand_store_id 
        WHERE s.store_code = '".$store_code."'";

        $result = $this->getDi()->getShared('dbReader')->query($query);
        $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
        $data = $result->fetchAll();

        if (count($data) > 0) {
            $brand_store_name = $data[0]['brand_store_name'];
        }

        return $brand_store_name;
    }

    public function getSupplierByStoreCode(string $store_code) {
        $query = "SELECT su.supplier_alias, s.supplier_id
            FROM store s
            JOIN supplier su ON su.supplier_id = s.supplier_id
            WHERE s.store_code = '". $store_code ."'";
        $result = $this->getDi()->getShared('dbReader')->query($query);
        $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
        $supplier = $result->fetchAll();
        
        return $supplier;
    }
}
