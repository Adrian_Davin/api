<?php

namespace Models;
use Phalcon\Db as Database;


class GideonAdminRole extends \Models\BaseModel
{

    protected $role_id;

    protected $name;

    protected $module_access;

    protected $function_access;

    protected $status;

    protected $created_at;

    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->whiteList = array();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'gideon_admin_role';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductVariant[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductVariant
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getAdminRole($master_app_id, $admin_user_attribute_id, $admin_user_id){
        $sql = "
        SELECT role_id, name, module_access, status, created_at, updated_at 
        FROM gideon_admin_role 
        WHERE role_id = (
            SELECT value FROM admin_user_attribute_value 
            WHERE master_app_id = '$master_app_id' 
            AND admin_user_attribute_id = '$admin_user_attribute_id'
            AND admin_user_id = '$admin_user_id'
            LIMIT 1
        )";

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            return $result->fetch();
        }
        return array();
    }

    public function setFromArray($dataArray = array())
    {
        $variantAttributes = array();
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public function saveRole($log_file_name = "gideonAdminRole")
    {
        $result = $this->saveData($log_file_name);

        return true;
    }

}
