<?php

namespace Models;

class StaticPage extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $static_page_id;
    protected $title;
    protected $body_html;
    protected $body_html_mobile;
    protected $inline_script;
    protected $inline_script_mobile;
    protected $inline_script_seo;
    protected $inline_script_seo_mobile;
    protected $company_code;
    protected $status;
    protected $url_key;
    protected $old_url_key;
    protected $additional_header;
    protected $additional_header_mobile;
    protected $created_at;
    protected $updated_at;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return mixed
     */
    public function getUrlKey()
    {
        return $this->url_key;
    }

    /**
     * @param mixed $url_key
     */
    public function setUrlKey($url_key)
    {
        $this->url_key = $url_key;
    }

    /**
     * @return int
     */
    public function getStaticPageId()
    {
        return $this->static_page_id;
    }

    /**
     * @return mixed
     */
    public function getOldUrlKey()
    {
        return $this->old_url_key;
    }

    /**
     * @param mixed $old_url_key
     */
    public function setOldUrlKey($old_url_key)
    {
        $this->old_url_key = $old_url_key;
    }
    /**
     * @param int $static_page_id
     */
    public function setStaticPageId($static_page_id)
    {
        $this->static_page_id = $static_page_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getBodyHtml()
    {
        return $this->body_html;
    }

    /**
     * @param mixed $body_html
     */
    public function setBodyHtml($body_html)
    {
        $this->body_html = $body_html;
    }

    /**
     * @return mixed
     */
    public function getInlineScript()
    {
        return $this->inline_script;
    }

    /**
     * @param mixed $inline_script
     */
    public function setInlineScript($inline_script)
    {
        $this->inline_script = $inline_script;
    }

    /**
     * Get the value of company_code
     */ 
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * Set the value of company_code
     *
     * @return  self
     */ 
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * @return mixed
     */
    public function getBodyHtmlMobile()
    {
        return $this->body_html_mobile;
    }

    /**
     * @param mixed $body_html_mobile
     */
    public function setBodyHtmlMobile($body_html_mobile)
    {
        $this->body_html_mobile = $body_html_mobile;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptMobile()
    {
        return $this->inline_script_mobile;
    }

    /**
     * @param mixed $inline_script_mobile
     */
    public function setInlineScriptMobile($inline_script_mobile)
    {
        $this->inline_script_mobile = $inline_script_mobile;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptSeo()
    {
        return $this->inline_script_seo;
    }

    /**
     * @param mixed $inline_script_seo
     */
    public function setInlineScriptSeo($inline_script_seo)
    {
        $this->inline_script_seo = $inline_script_seo;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptSeoMobile()
    {
        return $this->inline_script_seo_mobile;
    }

    /**
     * @param mixed $inline_script_seo_mobile
     */
    public function setInlineScriptSeoMobile($inline_script_seo_mobile)
    {
        $this->inline_script_seo_mobile = $inline_script_seo_mobile;
    }

    /**
     * @return mixed
     */
    public function getAdditionalHeaderMobile()
    {
        return $this->additional_header_mobile;
    }

    /**
     * @param mixed $additional_header_mobile
     */
    public function setAdditionalHeaderMobile($additional_header_mobile)
    {
        $this->additional_header_mobile = $additional_header_mobile;
    }


    public function getAdditionalHeader()
    {
        return $this->additional_header;
    }

    /**
     * @param mixed $additional_header
     */
    public function setAdditionalHeader($additional_header)
    {
        $this->additional_header = $additional_header;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'static_page';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setPageFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }
}
