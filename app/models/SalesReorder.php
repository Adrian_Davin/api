<?php

namespace Models;

use Phalcon\Db as Database;
use Helpers\LogHelper;
use Library\HTTPException;

class SalesReorder extends BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_reorder_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $invoice_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $status;

     /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $store_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $parent_invoice_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=2, nullable=true)
     */
    protected $reorder_increment;

    /**
     *
     * @var integer
     * @Column(type="integer", length=2, nullable=true)
     */
    protected $customer_confirmation;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $assigned_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $cart_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $new_order_no;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $action;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $refunded_by;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $admin_user_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $assigned_date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * @var $admin_user \Models\AdminUser
     */
    protected $admin_user;

    protected $errors;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        // $this->belongsTo('admin_user_id', 'Models\AdminUser', 'admin_user_id', array('alias' => 'AdminUser',"reusable" => true));
        $this->belongsTo('invoice_id', 'Models\SalesInvoice', 'invoice_id', array('alias' => 'SalesInvoice',"reusable" => true));
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder',"reusable" => true));
    }

    public function onConstruct()
    {
        parent::onConstruct();
        // $this->admin_user = new AdminUser();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_reorder';
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param $errors mixed
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesReorder[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesReorder
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function saveData($log_file_name = "", $state = "")
    {
        $this->created_at = date("Y-m-d H:i:s");
        $this->skipAttributesOnUpdate($this->skip_attribute_on_update);

        try {
            if ($this->save() === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                LogHelper::log("sales_reorder", "Sales reorder save failed, error : " . json_encode($errMsg));
                $this->errors[] = "Save Sales reorder failed";
            }
        } catch (\Exception $e) {
            throw new HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

    }
}
