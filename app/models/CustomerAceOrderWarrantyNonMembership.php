<?php


namespace Models;

class CustomerAceOrderWarrantyNonMembership extends \Models\BaseModel

{

    protected $customer_ace_order_warranty_non_membership_id;
    protected $ace_customer_id;
    protected $receipt_id_and_sku;
    protected $warranty_code;
    protected $ace_warranty_id;
    protected $serial_number;
    protected $warranty_number;
    protected $new_item_replacement_warranty_date;
    protected $spare_part_warranty_date;
    protected $service_warranty_date;
    protected $spare_parts_detail_warranty;
    protected $status;
    protected $created_at;

    public function initialize()

    {

        //

    }

    /**

     * @return mixed

     */

    public function getCustomerAceOrderWarrantyNonMembershipId()

    {

        return $this->customer_ace_order_warranty_non_membership_id;
    }


    /**

     * @param mixed $customer_ace_otp_temp_id

     */

    public function setCustomerAceOrderWarrantyNonMembershipId($customer_ace_order_warranty_non_membership_id)

    {

        $this->customer_ace_order_warranty_non_membership_id = $customer_ace_order_warranty_non_membership_id;
    }


    /**

     * @return mixed

     */

    public function getAceCustomerId()

    {

        return $this->ace_customer_id;
    }


    /**

     * @param mixed

     */

    public function setAceCustomerId($ace_customer_id)

    {

        $this->ace_customer_id = $ace_customer_id;
    }


    /**

     * @return mixed

     */

    public function getReceiptIdAndSku()

    {

        return $this->receipt_id_and_sku;
    }


    /**

     * @param mixed $phone

     */

    public function setReceiptIdAndSku($receipt_id_and_sku)

    {

        $this->receipt_id_and_sku = $receipt_id_and_sku;
    }


    /**

     * @return mixed

     */

    public function getWarrantyCode()

    {

        return $this->warranty_code;
    }


    /**

     * @param mixed

     */

    public function setWarrantyCode($warranty_code)

    {

        $this->warranty_code = $warranty_code;
    }


    /**

     * @return mixed

     */

    public function getAceWarrantyId()

    {

        return $this->ace_warranty_id;
    }


    /**

     * @param mixed

     */

    public function setAceWarrantyId($ace_warranty_id)

    {

        $this->ace_warranty_id = $ace_warranty_id;
    }


    /**

     * @return mixed

     */

    public function getSerialNumber()

    {

        return $this->serial_number;
    }


    /**

     * @param mixed

     */

    public function setSerialNumber($serial_number)

    {

        $this->serial_number = $serial_number;
    }


    /**

     * @return mixed

     */

    public function getWarrantyNumber()

    {

        return $this->warranty_number;
    }


    /**

     * @param mixed

     */

    public function setWarrantyNumber($warranty_number)

    {

        $this->warranty_number = $warranty_number;
    }

    /**

     * @return mixed

     */

    public function getNewItemReplacementWarrantyDate()

    {

        return $this->new_item_replacement_warranty_date;
    }


    /**

     * @param mixed

     */

    public function setNewItemReplacementWarrantyDate($new_item_replacement_warranty_date)

    {

        $this->new_item_replacement_warranty_date = $new_item_replacement_warranty_date;
    }

    /**

     * @return mixed

     */

    public function getSparePartWarrantyDate()

    {

        return $this->spare_part_warranty_date;
    }


    /**

     * @param mixed

     */

    public function setSparePartWarrantyDate($spare_part_warranty_date)

    {

        $this->spare_part_warranty_date = $spare_part_warranty_date;
    }

    /**

     * @return mixed

     */

    public function getServiceWarrantyDate()

    {

        return $this->service_warranty_date;
    }


    /**

     * @param mixed

     */

    public function setServiceWarrantyDate($service_warranty_date)

    {

        $this->service_warranty_date = $service_warranty_date;
    }

    /**

     * @return mixed

     */

    public function getSparePartsDetailWarranty()

    {

        return $this->spare_parts_detail_warranty;
    }


    /**

     * @param mixed

     */

    public function setSparePartsDetailWarranty($spare_parts_detail_warranty)

    {

        $this->spare_parts_detail_warranty = $spare_parts_detail_warranty;
    }

    /**

     * @return mixed

     */

    public function getStatus()

    {

        return $this->status;
    }


    /**

     * @param mixed

     */

    public function setStatus($status)

    {

        $this->status = $status;
    }

    /**

     * @return mixed

     */

    public function getCreatedAt()

    {

        return $this->created_at;
    }


    /**

     * @param mixed

     */

    public function setCreatedAt($created_at)

    {

        $this->created_at = $created_at;
    }


    public function getDataArray($columns = array(), $showEmpty = false)

    {

        return $this->toArray($columns, $showEmpty);
    }


    public static function find($parameters = null)

    {

        return parent::find($parameters);
    }


    public static function findFirst($parameters = null)

    {

        return parent::findFirst($parameters);
    }
}
