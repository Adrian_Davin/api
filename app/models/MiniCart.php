<?php

/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 3/8/2017
 * Time: 2:47 PM
 */

namespace Models;
use \Library\Mongodb;
class MiniCart
{
    public $minicart_id;    
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function miniCartLoad($check_for_status = true)
    {
        if (empty($this->minicart_id)) {
            $this->errorCode = "RR202";
            $this->errorMessages = "Shopping cart is empty or not found";
            return false;
        }   

        // Load shopping card data
        $mongoDb = new \Library\Mongodb();
        $mongoDb->setCollection(getenv('MINI_CART_COLLECTION'));
        $collection = $mongoDb->getCollection();
        $minicart = $collection->findOne(['minicart_id' => $this->minicart_id]);
        unset($minicart['_id']);
        
        return $minicart;           
    }

}
