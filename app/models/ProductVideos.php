<?php

namespace Models;

class ProductVideos extends \Models\BaseModel
{
    protected $product_video_id;
    protected $product_variant_id;
    protected $video_url;
    protected $status;
    protected $priority;

    protected $created_at;
    protected $updated_at;

    /**
     * @return mixed
     */
    public function getProductVideoId()
    {
        return $this->product_video_id;
    }

    /**
     * @param mixed $product_video_id
     */
    public function setProductVideoId($product_video_id)
    {
        $this->product_video_id = $product_video_id;
    }

    /**
     * @return mixed
     */
    public function getProductVariantId()
    {
        return $this->product_variant_id;
    }

    /**
     * @param mixed $product_variant_id
     */
    public function setProductVariantId($product_variant_id)
    {
        $this->product_variant_id = $product_variant_id;
    }

    /**
     * @return mixed
     */
    public function getVideoUrl()
    {
        return $this->video_url;
    }

    /**
     * @param mixed $video_url
     */
    public function setVideoUrl($video_url)
    {
        $this->video_url = $video_url;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('product_id', 'Models\Product', 'product_id', array('alias' => 'Product'));
        $this->belongsTo('product_variant_id', 'Models\ProductVariant', 'product_variant_id', array('alias' => 'ProductVariant'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_videos';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductImages[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductImages
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

}
