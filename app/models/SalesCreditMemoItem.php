<?php

namespace Models;

class SalesCreditMemoItem extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $credit_memo_item_id;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $credit_memo_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $invoice_no;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $sku;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $price;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $weight;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $shipping_amount;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $gift_cards_amount;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=true)
     */
    protected $qty_refunded;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('invoice_no', 'Models\SalesOrder', 'invoice_no', array('alias' => 'SalesInvoice'));
        $this->belongsTo('credit_memo_id', 'Models\SalesCreditMemo', 'credit_memo_id', array('alias' => 'SalesCreditMemo'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_credit_memo_item';
    }

    /**
     * @return int
     */
    public function getCreditMemoId()
    {
        return $this->credit_memo_id;
    }

    /**
     * @param int $credit_memo_id
     */
    public function setCreditMemoId($credit_memo_id)
    {
        $this->credit_memo_id = $credit_memo_id;
    }

    /**
     * @return string
     */
    public function getInvoiceNo()
    {
        return $this->invoice_no;
    }

    /**
     * @param string $invoice_no
     */
    public function setInvoiceNo($invoice_no)
    {
        $this->invoice_no = $invoice_no;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCreditMemoItem[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCreditMemoItem
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = parent::toArray($columns,true);

        unset($view['sales_credit_memo_id']);
        return $view;
    }

}
