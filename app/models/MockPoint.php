<?php

namespace Models;

// use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
// use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class MockPoint extends \Models\BaseModel
{
  /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $mock_point_id;

    protected $member_id;

    protected $point;
    

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'mock_point';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return int 
     */
    public function getMockPointId()
    {
        return $this->mock_point_id;
    }

    /**
     * @param int $mock_point_id
     */
    public function setMockPointId($mock_point_id)
    {
        $this->mock_point_id = $mock_point_id;
    }

    /**
     * @return string
     */
    public function getMemberId() 
    {
        return $this->member_id;
    } 

    /**
     * @param string $member_id
     */
    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }

     /**
     * @return int
     */
    public function getPoint() 
    {
        return $this->point;
    } 

    /**
     * @param int $point
     */
    public function setPoint($point)
    {
        $this->point = $point;
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
          if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
  }

  public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}