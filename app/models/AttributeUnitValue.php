<?php

namespace Models;

class AttributeUnitValue extends \Models\BaseModel
{

    protected $attribute_unit_value_id;

    protected $attribute_unit_id;

    protected $value;

    protected $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'attribute_unit_value';
    }

    /**
     * @return mixed
     */
    public function getAttributeUnitValueId()
    {
        return $this->attribute_unit_value_id;
    }

    /**
     * @param mixed $attribute_unit_value_id
     */
    public function setAttributeUnitValueId($attribute_unit_value_id)
    {
        $this->attribute_unit_value_id = $attribute_unit_value_id;
    }

    /**
     * @return mixed
     */
    public function getAttributeUnitId()
    {
        return $this->attribute_unit_id;
    }

    /**
     * @param mixed $attribute_unit_id
     */
    public function setAttributeUnitId($attribute_unit_id)
    {
        $this->attribute_unit_id = $attribute_unit_id;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Attribute[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Attribute
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}
