<?php

namespace Models;
use Phalcon\Db as Database;

class Salesrule1212 extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $id;


    /**
     *
     * @var integer
     */
    protected $final_amount;


    /**
     *
     * @var string
     */
    protected $sku;

    /**
     * @var integer
     */
    protected $discount_amount;

    /**
     *
     * @var integer
     */
    protected $gift_cards_amount;

    /**
     *
     * @var string
     */
    protected $voucher_code;

     /**
     *
     * @var string
     */
    protected $time_start;

    /**
     *
     * @var integer
     */
    protected $rule_id;

    
 

    /**
     * Get the value of id
     *
     * @return  integer
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  integer  $id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of sku
     *
     * @return  string
     */ 
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set the value of sku
     *
     * @param  string  $sku
     *
     * @return  self
     */ 
    public function setSku(string $sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get the value of discount_amount
     *
     * @return  integer
     */ 
    public function getDiscount_amount()
    {
        return $this->discount_amount;
    }

    /**
     * Set the value of discount_amount
     *
     * @param  integer  $discount_amount
     *
     * @return  self
     */ 
    public function setDiscount_amount($discount_amount)
    {
        $this->discount_amount = $discount_amount;

        return $this;
    }

    /**
     * Get the value of gift_cards_amount
     *
     * @return  integer
     */ 
    public function getGift_cards_amount()
    {
        return $this->gift_cards_amount;
    }

    /**
     * Set the value of gift_cards_amount
     *
     * @param  integer  $gift_cards_amount
     *
     * @return  self
     */ 
    public function setGift_cards_amount($gift_cards_amount)
    {
        $this->gift_cards_amount = $gift_cards_amount;

        return $this;
    }

    /**
     * Get the value of voucher_code
     *
     * @return  string
     */ 
    public function getVoucher_code()
    {
        return $this->voucher_code;
    }

    /**
     * Set the value of voucher_code
     *
     * @param  string  $voucher_code
     *
     * @return  self
     */ 
    public function setVoucher_code(string $voucher_code)
    {
        $this->voucher_code = $voucher_code;

        return $this;
    }

    /**
     * Get the value of rule_id
     *
     * @return  integer
     */ 
    public function getRule_id()
    {
        return $this->rule_id;
    }

    /**
     * Set the value of rule_id
     *
     * @param  integer  $rule_id
     *
     * @return  self
     */ 
    public function setRule_id($rule_id)
    {
        $this->rule_id = $rule_id;

        return $this;
    }

    public function initialize()
    {
        
    }

    /**
     * Get the value of final_amount
     *
     * @return  integer
     */ 
    public function getFinal_amount()
    {
        return $this->final_amount;
    }

    /**
     * Set the value of final_amount
     *
     * @param  integer  $final_amount
     *
     * @return  self
     */ 
    public function setFinal_amount($final_amount)
    {
        $this->final_amount = $final_amount;

        return $this;
    }

    /**
     * Get the value of time_start
     *
     * @return  string
     */ 
    public function getTime_start()
    {
        return $this->time_start;
    }

    /**
     * Set the value of time_start
     *
     * @param  string  $time_start
     *
     * @return  self
     */ 
    public function setTime_start(string $time_start)
    {
        $this->time_start = $time_start;

        return $this;
    }
}
