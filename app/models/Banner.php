<?php

namespace Models;

use Phalcon\Db;

class Banner extends \Models\BaseModel
{
    protected $bannerid;    // banner_id
    protected $description; // title
    protected $bannertext;  // banner_text
    protected $url;         // banner_url
    protected $imageurl;    // banner_image
    protected $width;       // width
    protected $height;      // height

    protected $created_at;
    protected $updated_at;

    protected $errorCode;
    protected $errorMessages;



    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rv_banners';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function getBannerList($params = array()){
        $campaign_name = $params['campaign_name'];

        // $mobileCampaign = getenv('MOBILE_MAIN_BANNER');
        // $desktopCampaign = getenv('DESKTOP_MAIN_BANNER');

        $sql  = "SELECT c.campaignname, b.bannerid AS banner_id,b.url AS banner_url, b.imageurl AS banner_image, b.description AS title, b.alt AS vocal_point, b.bannertext AS banner_text, b.height, b.width";
        $sql .= ",b.weight, b.comments AS html_left_right, b.keyword AS hexa_code, b.statustext AS scheduling FROM rv_banners b, rv_campaigns c WHERE b.status=0 AND b.campaignid = c.campaignid ";
        $sql .= " AND c.campaignname = '".$campaign_name."'";
        $sql .= " ORDER BY b.weight DESC";
        $this->useReviveConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $result = $result->fetchAll();

        foreach ($result as $key => $value){
            if(!empty($value['scheduling'])){
                // format : start_date,end_date -> if banner will be shown in specific time
                $comment = explode(',',$value['scheduling']);
                if(!empty($comment)){
                    $date = strtotime(date('Y-m-d H:i:s'));
                    if($date < strtotime($comment[0]) || $date > strtotime($comment[1])){
                        unset($result[$key]);
                    }
                }
            }
            // if schedule is empty, banner will be provided forever
        }

        // result still sorted by weight desc
        if(!empty($result)){
            if(!strpos($campaign_name, 'desktop_brand_campaign') && strpos($campaign_name, 'desktop_brand_campaign') != 0){
                if(count($result) > 5){
                    // we need to pop last (n-5) data
                    $idx = count($result) - 5;

                    while($idx > 0){
                        array_pop($result);
                        $idx--;
                    }
                }
            }
            return array_values($result);
        }
        else{
            $this->errorCode="RR301";
            $this->errorMessages="No banner found.";
        }

        return;
    }
}
