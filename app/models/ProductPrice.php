<?php

namespace Models;

use Helpers\GeneralHelper;
use Helpers\ProductHelper;
use Phalcon\Db;
use Phalcon\Db as Database;

class ProductPrice extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $price_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_variant_id;

    protected $sku;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $price;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $special_price;

    protected $price_pir;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $special_from_date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $special_to_date;

     /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $last_modified;

     /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $modified_description;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $data;

    protected $errors;

    protected $messages;


    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }



    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('product_variant_id', 'ProductVariant', 'product_variant_id', array('alias' => 'ProductVariant'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_price';
    }

    /**
     * @return int
     */
    public function getPriceId()
    {
        return $this->price_id;
    }

    /**
     * @param int $price_id
     */
    public function setPriceId($price_id)
    {
        $this->price_id = $price_id;
    }

    /**
     * @return int
     */
    public function getProductVariantId()
    {
        return $this->product_variant_id;
    }

    /**
     * @param int $product_variant_id
     */
    public function setProductVariantId($product_variant_id)
    {
        $this->product_variant_id = $product_variant_id;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getSpecialPrice()
    {
        return $this->special_price;
    }

    /**
     * @param float $special_price
     */
    public function setSpecialPrice($special_price)
    {
        $this->special_price = $special_price;
    }

    /**
     * @return mixed
     */
    public function getPricePir()
    {
        return $this->price_pir;
    }

    /**
     * @param mixed $price_pir
     */
    public function setPricePir($price_pir)
    {
        $this->price_pir = $price_pir;
    }

    /**
     * @return string
     */
    public function getSpecialFromDate()
    {
        return $this->special_from_date;
    }

    /**
     * @param string $special_from_date
     */
    public function setSpecialFromDate($special_from_date)
    {
        $this->special_from_date = $special_from_date;
    }

    /**
     * @return string
     */
    public function getSpecialToDate()
    {
        return $this->special_to_date;
    }

    /**
     * @param string $special_to_date
     */
    public function setSpecialToDate($special_to_date)
    {
        $this->special_to_date = $special_to_date;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductPrice[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductPrice
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                if($key == 'special_from_date'){
                    if(\Helpers\ProductHelper::checkIsAValidDate($val)){
                        $this->{$key} = $val;
                    }else{
                        $this->{$key} = null;
                    }
                }
                elseif($key == 'special_to_date'){
                    if(\Helpers\ProductHelper::checkIsAValidDate($val)){
                        $this->{$key} = $val;
                    }else{
                        $this->{$key} = null;
                    }
                }else{
                    $this->{$key} = $val;
                }
            }
        }
        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public function beforeCreate()
    {
        parent::beforeCreate();

        if(empty($this->status)) {
            $this->status = 10;
        }
    }

    public function updateBatchPrice($params = array()){
        try {
            $sql = "TRUNCATE TABLE product_price_sap";
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            $sql = "insert into product_price_sap (sku, price) values ";
            $arrSqlPrice = [];
            foreach($params as $row){
                $arrSqlPrice[] = "('".$row['sku']."',".$row['price'].")";
            }
            $sqlPrice = implode(',',$arrSqlPrice);
            $sql = $sql.$sqlPrice;    

            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            if(!$result){
                \Helpers\LogHelper::log("Product Price ".$params['method'], 'There have something error in update data');
                $this->setErrors(array('code' => 'RR303','title' => 'productVariant Query Batch','message' => 'There have something error in update data'));
                $this->setData(array('affected_row' => 0));
            }else{
                $this->setMessages(array('Successfully Query Batch'));
                $this->setData(array('affected_row' => $this->getDi()->getShared('dbMaster')->affectedRows()));
                $this->setErrors(array());
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("Product Price", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'Product Price','message' => 'There have something error, in query batch'));
            $this->setData(array('affected_row' => 0));
        }
    }

    /**
     * @todo : Refactor not using product variant again
     * @param array $params
     */
    public function updateBatch($params = array()){
        try {

            $sql = "UPDATE product_price set ";
            $sql_price = " price = CASE sku ";
            $sql_special_price = " special_price = CASE sku ";
            $sql_special_from_date = " special_from_date = CASE sku ";
            $sql_special_to_date = " special_to_date = CASE sku ";
            $sql_updated_at = " updated_at = CASE sku ";
            $arr_sku = array();
            $update_date = date('Y-m-d H:i:s');
            foreach($params['data'] as $row){
                if($row['price']<$row['special_price']){
                    $row['special_price'] = $row['price'];
                }
                $sql_price .= " WHEN '".$row['sku']."' THEN ".$row['price'];
                $sql_special_price .= " WHEN '".$row['sku']."' THEN ".((empty($row['special_price']))? 'NULL' : $row['special_price']);
                $sql_special_from_date .= " WHEN '".$row['sku']."' THEN ".(empty($row['special_from_date'])? 'NULL' : "'".$row['special_from_date']."'")." ";
                $sql_special_to_date .= " WHEN '".$row['sku']."' THEN ".(empty($row['special_to_date'])? 'NULL' : "'".$row['special_to_date']."'")." ";
                $sql_updated_at .= " WHEN '".$row['sku']."' THEN '".$update_date."' ";
                $arr_sku[] = "'".$row['sku']."'";
            }
            $sku = implode(',',$arr_sku);
            $sql = $sql .
                $sql_price . " ELSE price END," .
                $sql_special_price . " ELSE special_price END," .
                $sql_special_from_date . " ELSE special_from_date END," .
                $sql_special_to_date . " ELSE special_to_date END, " .
                $sql_updated_at . " ELSE updated_at END " .
                " WHERE sku in (".$sku.") ";

            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            if(!$result){
                \Helpers\LogHelper::log("Product Price ".$params['method'], 'There have something error in update data');
                $this->setErrors(array('code' => 'RR303','title' => 'productVariant Query Batch','message' => 'There have something error in update data'));
                $this->setData(array('affected_row' => 0));
            }else{
                $this->setMessages(array('Successfully Query Batch'));
                $this->setData(array('affected_row' => $this->getDi()->getShared('dbMaster')->affectedRows()));
                $this->setErrors(array());
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("Product Price", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'Product Price','message' => 'There have something error in update data'));
            $this->setData(array('affected_row' => 0));
        }
    }

    /**
     * @todo : Refactor not using product variant again
     * @param array $params
     * @return array|string
     */
    public function generatePrice($params = array()){
        $reindex_array = array_values($params['price_value']);
        $arr_to_insert = array();

        $arr_price = array();
        $arr_from_date = array();
        $arr_to_date = array();

        $arr = array();

        foreach($reindex_array as $index => $val){
            $arr_to_insert[$index]['sku'] = $val['sku'];
            $arr_to_insert[$index]['special_price'] = $val['special_price'];
            $arr_to_insert[$index]['start_date'] = $val['start_date'];
            $arr_to_insert[$index]['end_date'] = $val['end_date'];
            $arr_to_insert[$index]['updated_at'] = date('Y-m-d H:i:s');
            $arr[] = "'".$val['sku']."'";
        }

        $string_sku = implode(',', $arr);

        /**
         * BUILD THE UPDATE PARAM
         */

        // get special price
        foreach ($arr_to_insert as $res => $val){
            $arr_price[]= "WHEN '".$val['sku']."' THEN '".$val['special_price']."'";
            $arr_from_date[]= "WHEN '".$val['sku']."' THEN '".$val['start_date']."'";
            $arr_to_date[]= "WHEN '".$val['sku']."' THEN '".$val['end_date']."'";
            $arr_updated_at[]= "WHEN '".$val['sku']."' THEN '".$val['updated_at']."'";
        }

        $string_price = implode(' ', $arr_price);

        $string_from_date = implode(' ', $arr_from_date);

        $string_to_date = implode(' ', $arr_to_date);

        $string_updated_at = implode(' ', $arr_updated_at);

        /**
         * END HERE
         */

        // query to update special price
        $sql  = "UPDATE product_price SET special_price = ( ";
        $sql .= "CASE sku ";
        $sql .= $string_price;
        $sql .= " END ), special_from_date = ( CASE sku ";
        $sql .= $string_from_date;
        $sql .= " END ), special_to_date = ( CASE sku ";
        $sql .= $string_to_date;
        $sql .= " END ), updated_at = ( CASE sku ";
        $sql .= $string_updated_at;
        $sql .= " END ) WHERE sku IN ( $string_sku )";

        $this->useWriteConnection();
        $resultSpecialPrice = $this->getDi()->getShared($this->getConnection())->query($sql);

        if($resultSpecialPrice->numRows() > 0) {
            $final = array(
                'inserted' => $resultSpecialPrice->numRows(),
                'total' => count($reindex_array)
            );
            return $final;
        }
        else{
            return "error";
        }

    }

    /**
     * @todo : Refactor not using product variant again
     * @param array $params
     * @return mixed
     */
    public function getChunckedData($params = array()){
        $sku = $params['sku'];

        $string_sku = '('.implode(',',$sku).')';

        $sql = "SELECT product_variant_id, sku FROM product_variant WHERE sku IN ";
        $sql .= $string_sku;
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $products = $result->fetchAll();

        return $products;
    }

    /**
     * @todo : Refactor not using product variant again
     * @param array $params
     * @return array
     */
    public function insertPir($params = array()){
        $string_id = $params['string_id'];
        $string_update = $params['string_update'];
        $string_updated_at = $params['string_updated_at'];

        $sql  = "UPDATE product_price SET price_pir = ( CASE sku ";
        $sql .= $string_update;
        $sql .= " END ), updated_at = ( CASE sku ";
        $sql .= $string_updated_at;
        $sql .= " END ) WHERE sku IN (";
        $sql .= $string_id;
        $sql .= ")";
        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        return array('affected_rows' => $result->numRows());
    }

    public function cleanExpiredProductStockOverrideStatus(){
        $sql = "DELETE FROM product_stock_override_status WHERE end_date < now()";
        $this->getDi()->getShared('dbMaster')->execute($sql);
        return TRUE;
    }
    
    public function comparePir(){
        $date = date('Y-m-d H:i:s');
        $data_compare = array();
        $update_sku = array();
        $sku = array();
        $affectedRows = 0;
        $string_sku = "";
        $sku_to_update = "";

        $sql  = " SELECT pp.sku, pp.price, pp.special_price, pp.price_pir, pp.special_from_date, pp.special_to_date ";
        $sql .= " FROM product_price pp";
        $sql .= " WHERE pp.status > 0 and pp.price_pir is not null";
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        $result->setFetchMode(
            Db::FETCH_ASSOC
        );

        $arr_data = $result->fetchAll();
        foreach (array_chunk($arr_data,10000) as $key => $val){
            foreach ($val as $data => $value){
                // check current active price (price / special_price)
                if(!empty($value['special_from_date']) && !empty($value['special_to_date'])){
                    if($value['special_from_date'] <= $date && $value['special_to_date'] >= $date){
                        $currentActivePrice = $value['special_price'];
                    }
                    else{
                        $currentActivePrice = $value['price'];
                    }
                }
                else{
                    $currentActivePrice = $value['price'];
                }

                if($value['price_pir'] > $currentActivePrice){
                    $data_compare[] = $value['sku'];
                }
            }
        }
       
        $affectedRows = 0;
        $data = array_chunk($data_compare, 500);
        foreach( $data as $key => $val){
            //Store Code = DC
            $sql = "insert into product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) values ";
            $values = "";
            $valuesArray = array();
            foreach($val as $skuCompare){
                $valueString = "('".$skuCompare."','DC',0,'odi','".date('Y-m-d 00:00:00')."','".date('Y-m-d 00:00:00',strtotime('+1 day'))."')";
                array_push($valuesArray, $valueString);
            }
            $values = implode(',',$valuesArray);
            $sql = $sql . $values;

            $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();

            //Store Code = 1000DC
            $sql = "insert into product_stock_override_status (sku, store_code, status, company_code, start_date, end_date) values ";
            $values = "";
            $valuesArray = array();
            foreach($val as $skuCompare){
                $valueString = "('".$skuCompare."','1000DC',0,'odi','".date('Y-m-d 00:00:00')."','".date('Y-m-d 00:00:00',strtotime('+1 day'))."')";
                array_push($valuesArray, $valueString);
            }
            $values = implode(',',$valuesArray);
            $sql = $sql . $values;

            $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();
        }
        
        $final = array(
            'updated_rows' => $affectedRows,
        );

        return $final;
    }

    public function getCountProductPrice()
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $sqlSku = "SELECT COUNT(*) as jml FROM product_price;";
            $resultSku = $this->getDi()->getShared($this->getConnection())->query($sqlSku);
            $jml = 0;
            if ($resultSku->numRows() > 0) {
                $resultSku->setFetchMode(Database::FETCH_ASSOC);
                $resultArray = $resultSku->fetch();
                $jml = $resultArray['jml'];
            }

            $response['messages'] = array('success');
            $response['data']['count'] = $jml;
        } catch (\Exception $e) {
            $response['errors'] = array('Get count SKU for Update price to elastic failed (' . $e->getMessage() . ')');
        }

        return $response;
    }

    public function getProductPrice($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $sqlPrice = "
                SELECT
                    sku,
                    price,
                    COALESCE (special_price, 0) as special_price,
                    special_from_date,
                    special_to_date
                FROM product_price
                where status = 10 limit 1000 offset " . $params['offset'];
            $resultPrice = $this->getDi()->getShared($this->getConnection())->query($sqlPrice);
            $data = array();
            if ($resultPrice->numRows() > 0) {
                $resultPrice->setFetchMode(Database::FETCH_ASSOC);
                $resultArray = $resultPrice->fetchAll();
                $i=0;
                foreach ($resultArray as $rowPrice) {
                    $data[$i]['sku'] = $rowPrice['sku'];
                    $data[$i]['price'] = $rowPrice['price'];
                    $data[$i]['special_price'] = $rowPrice['special_price'];
                    $data[$i]['special_from_date'] = $rowPrice['special_from_date'];
                    $data[$i]['special_to_date'] = $rowPrice['special_to_date'];
                    $i++;
                }
            }

            $response['messages'] = array('success');
            $response['data'] = $data;
        } catch (\Exception $e) {
            $response['errors'] = array('Get SKU for Update price to elastic failed (' . $e->getMessage() . ')');
        }

        return $response;
    }

    public function getProductPriceBySKU($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $sqlPrice = "
                SELECT
                    sku,
                    price,
                    COALESCE (special_price, 0) as special_price,
                    special_from_date,
                    special_to_date,
                    last_modified,
                    modified_description
                FROM product_price
                where sku = '" . $params['sku']."'";
            $resultPrice = $this->getDi()->getShared($this->getConnection())->query($sqlPrice);
            $data = array();
            if ($resultPrice->numRows() > 0) {
                $resultPrice->setFetchMode(Database::FETCH_ASSOC);
                $resultArray = $resultPrice->fetchAll();
                $i=0;
                foreach ($resultArray as $rowPrice) {
                    $data[$i]['sku'] = $rowPrice['sku'];
                    $data[$i]['price'] = $rowPrice['price'];
                    $data[$i]['special_price'] = $rowPrice['special_price'];
                    $data[$i]['special_from_date'] = $rowPrice['special_from_date'];
                    $data[$i]['special_to_date'] = $rowPrice['special_to_date'];
                    $data[$i]['last_modified'] = $rowPrice['last_modified'];
                    $data[$i]['modified_description'] = $rowPrice['modified_description'];
                    $i++;
                }
            }

            $response['messages'] = array('success');
            $response['data'] = $data;
        } catch (\Exception $e) {
            $response['errors'] = array('Get SKU for Update price to elastic failed (' . $e->getMessage() . ')');
        }

        return $response;
    }

    public function getSellingPriceBySKU($sku)
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $sqlPrice = "
                SELECT 
                    price,
                    price_pir,
                    (
                        CASE WHEN NOW() BETWEEN special_from_date AND special_to_date THEN special_price ELSE price END
                    ) as selling_price
                FROM product_price
                where sku = '". $sku ."'
                and status = 10";
            $resultPrice = $this->getDi()->getShared($this->getConnection())->query($sqlPrice);
            $data = array();
            if ($resultPrice->numRows() > 0) {
                $resultPrice->setFetchMode(Database::FETCH_ASSOC);
                $resultArray = $resultPrice->fetchAll();
                
                foreach ($resultArray as $rowPrice) {
                    $data['price'] = $rowPrice['price'];
                    $data['price_pir'] = $rowPrice['price_pir'];
                    $data['selling_price'] = $rowPrice['selling_price'];
                }
            }

            $response['messages'] = array('success');
            $response['data'] = $data;
        } catch (\Exception $e) {
            $response['errors'] = array('Get product price failed (' . $e->getMessage() . ')');
        }

        return $response;
    }
}
