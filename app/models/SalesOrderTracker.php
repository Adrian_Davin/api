<?php

namespace Models;

class SalesOrderTracker extends \Models\BaseModel
{
    protected $sales_order_tracker_id;
    protected $sales_order_id;
    protected $platform;
    protected $identifier;
    protected $valid_at;
    protected $status;
    protected $expired_at;
    protected $created_at;

    public function initialize()
    {

    }

    public function getSalesOrderTrackerId()
    {
        return $this->sales_order_tracker_id;
    }

    public function setSalesOrderTrackerId($sales_order_tracker_id)
    {
        $this->sales_order_tracker_id = $sales_order_tracker_id;
    }

    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }

    public function getPlatform()
    {
        return $this->platform;
    }

    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    public function getValidAt()
    {
        return $this->getValidAt;
    }

    public function setValidAt($valid_at)
    {
        $this->valid_at = $valid_at;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getExpiredAt()
    {
        return $this->expired_at;
    }

    public function setExpiredAt($expired_at)
    {
        $this->expired_at = $expired_at;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function findAll($parameters = null)
    {
        return parent::find($parameters);
    }
}