<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 1/13/2017
 * Time: 2:38 PM
 */

namespace Models;

class Payment extends \Models\BaseModel
{
    protected $method;
    protected $type;
    protected $cc_type;
    protected $cc_number;
    protected $instalement_bank;
    protected $instalement_tenor;
    protected $va_bank;
    protected $va_number;

    public function initialize()
    {
        $this->setSource("sales_order_payment");
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if(property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}