<?php

namespace Models;

class CustomerB2bCompany extends \Models\BaseModel
{
    protected $customer_b2b_company_id;
    protected $customer_company_data_id;
    protected $customer_id;
    protected $company_salesman_id;
    protected $tax_number_type;
    protected $nitku_no;
    protected $company_code;

    public function initialize()
    {
        $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
        $this->belongsTo('customer_id', 'Models\SalesCustomer', 'customer_id', array('alias' => 'SalesCustomer'));
        $this->hasOne('customer_company_data_id', 'Models\CustomerCompanyData', 'customer_company_data_id', array('alias' => 'CustomerCompanyData'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_b2b_company';
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerB2bCompany
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
        return $view;
    }


    public function getCustomerB2bCompanyId() {
        return $this->customer_b2b_company_id;
    }

    public function setCustomerB2bCompanyId($customer_b2b_company_id) {
        $this->customer_b2b_company_id = $customer_b2b_company_id;
    }

    public function getCustomerCompanyDataId() {
        return $this->customer_company_data_id;
    }

    public function setCustomerCompanyDataId($customer_company_data_id) {
        $this->customer_company_data_id = $customer_company_data_id;
    }

    public function getCompanySalesmanId() {
        return $this->company_salesman_id;
    }

    public function setCompanySalesmanId($company_salesman_id) {
        $this->company_salesman_id = $company_salesman_id;
    }

    public function getCustomerId() {
        return $this->customer_id;
    }

    public function setCustomerId($customer_id) {
        $this->customer_id = $customer_id;
    }

    public function getTaxNumberType() {
        return $this->tax_number_type;
    }  

    public function setTaxNumberType($tax_number_type) {
        $this->tax_number_type = $tax_number_type;
    }

    public function getNitkuNo() {
        return $this->nitku_no;
    }

    public function setNitkuNo($nitku_no) {
        $this->nitku_no = $nitku_no;
    }

    public function getCompanyCode() {
        return $this->company_code;
    }

    public function setCompanyCode($company_code) {
        $this->company_code = $company_code;
    }

}