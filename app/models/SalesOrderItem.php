<?php

namespace Models;

use \Library\APIWrapper;
use \Library\ProductStock;
use Phalcon\Db as Database;
class SalesOrderItem extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    protected  $sales_order_item_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected  $sales_order_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected  $sku;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected  $name;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $weight;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $packaging_height;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $packaging_width;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $packaging_length;


    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $packaging_weight;


    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected  $packaging_uom;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=true)
     */
    protected  $qty_ordered;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $normal_price;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $selling_price;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $discount_amount;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected  $handling_fee_adjust;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $row_total;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $subtotal;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected  $delivery_method;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=false)
     */
    protected $group_shipment;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected  $store_code;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected  $supplier_alias;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected  $pickup_code;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected  $product_source;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected  $zone_id;

    

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected  $gift_message;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $cart_rules;

    /**
     *
     * @var string
     * @Column(type="string")
     */
    protected $preorder_date;
    protected $is_free_item = 0;
    protected $gift_cards_amount = 0;
    protected $first_utm_parameter = "";
    protected $utm_parameter = "";
    protected $commission;
    protected $net_sales;
        
    protected $site_source = "";

    protected $price_modified;
    protected $modified_description;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected  $service_date;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected  $gift_warping;
    
    protected $shipping_amount = 0;
    protected $shipping_discount_amount = 0;
    
    protected $attributes = [];

    // for refund purpose
    protected $group_promo_id;
    protected $rule_id;
    protected $parent_id;
    protected $id;

    protected $is_product_scanned;
    protected $is_within_radius;
    protected $is_extended;
    protected $is_dc_only = 0;
    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected  $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected  $updated_at;

    /**
     * @var $employees \Models\Items\SalesEmployee\Collection
     */
    protected $employees;

    /**
     * @var $flat \Models\Items\SalesFlat\Collection
     */
    protected $flat;

    /**
     * @var $flat \Models\Items\SalesGiftcards\Collection
     */
    protected $giftcards;
    
    /*
    field for installation per item
    */
    protected $is_installed;
    protected $is_installation_eligible;
    protected $is_need_installation_eligible;
    protected $is_need_installation;

    /*
    field for store note from minicart
    */
    protected $customer_note;

    protected $cart_id;
    protected $package_id;

    protected $lead_time_min;
    protected $lead_time_max;
    protected $lead_time_unit;
    protected $final_weight;
    protected $insurance_applied;
    protected $stock_store_code;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('sales_order_item_id', 'Models\SalesInvoiceItem', 'sales_order_item_id', array('alias' => 'SalesInvoiceItem'));
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
        $this->belongsTo('sku', 'Models\ProductVariant', 'sku', array('alias' => 'ProductVariant'));
        $this->hasOne('pickup_code', 'Models\PickupPoint', 'pickup_code', array('alias' => 'PickupPoint',"reusable", true));
        $this->hasOne('supplier_alias', 'Models\Supplier', 'supplier_alias', array('alias' => 'Supplier',"reusable", true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_order_item';
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->store_code;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * @param int $sales_order_id
     */
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }

    /**
     * @return int
     */
    public function getSalesOrderItemId(): int
    {
        return $this->sales_order_item_id;
    }

    /**
     * @param int $sales_order_item_id
     */
    public function setSalesOrderItemId(int $sales_order_item_id)
    {
        $this->sales_order_item_id = $sales_order_item_id;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     * Get the value of group_promo_id
     */ 
    public function getGroupPromoId()
    {
        return $this->group_promo_id;
    }

    /**
     * Set the value of group_promo_id
     *
     * @param  string $group_promo_id
     */ 
    public function setGroupPromoId($group_promo_id)
    {
        $this->group_promo_id = $group_promo_id;
    }

    /**
     * @return  int
     * Get the value of rule_id
     */ 
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * Set the value of rule_id
     *
     * @param  int $rule_id
     */ 
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;
    }

    /**
     * @return string
     * Get the value of parent_id
     */ 
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Set the value of parent_id
     *
     * @param  string $parent_id
     */ 
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return string
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  string $id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getPackagingUom()
    {
        return $this->packaging_uom;
    }

    /**
     * @param string $packaging_uom
     */
    public function setPackagingUom($packaging_uom)
    {
        $this->packaging_uom = $packaging_uom;
    }

    /**
     * @return int
     */
    public function getQtyOrdered()
    {
        return $this->qty_ordered;
    }

    /**
     * @param int $qty_ordered
     */
    public function setQtyOrdered($qty_ordered)
    {
        $this->qty_ordered = $qty_ordered;
    }

    /**
     * @return float
     */
    public function getNormalPrice()
    {
        return $this->normal_price;
    }

    /**
     * @param float $normal_price
     */
    public function setNormalPrice($normal_price)
    {
        $this->normal_price = $normal_price;
    }

    /**
     * @return float
     */
    public function getSellingPrice()
    {
        return $this->selling_price;
    }

    /**
     * @param float $selling_price
     */
    public function setSellingPrice($selling_price)
    {
        $this->selling_price = $selling_price;
    }

    /**
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * @param float $discount_amount
     */
    public function setDiscountAmount($discount_amount)
    {
        $this->discount_amount = $discount_amount;
    }

    /**
     * @return float
     */
    public function getHandlingFeeAdjust()
    {
        return $this->handling_fee_adjust;
    }

    /**
     * @param float $handling_fee_adjust
     */
    public function setHandlingFeeAdjust($handling_fee_adjust)
    {
        $this->handling_fee_adjust = $handling_fee_adjust;
    }

    /**
     * @return int
     */
    public function getGroupShipment()
    {
        return $this->group_shipment;
    }

    /**
     * @param int $group_shipment
     */
    public function setGroupShipment($group_shipment)
    {
        $this->group_shipment = $group_shipment;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return \Helpers\Transaction::countSubTotalItem($this->selling_price,$this->handling_fee_adjust,$this->discount_amount);
    }


    /**
     * @param float $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return float
     */
    public function getRowTotal()
    {
        if(empty($this->subtotal)) {
            $this->subtotal = $this->getSubtotal();
        }

        return \Helpers\Transaction::countTotalItem($this->subtotal,$this->qty_ordered);
    }

    /**
     * @return int
     */
    public function getIsProductScanned()
    {
        return $this->is_product_scanned;
    }

    /**
     * @param int $is_product_scanned
     */
    public function setIsProductScanned($is_product_scanned)
    {
        $this->is_product_scanned = $is_product_scanned;
    }

    /**
     * @return int
     */
    public function getIsWithinRadius()
    {
        return $this->is_within_radius;
    }

    /**
     * @param int $isWithinRadius
     */
    public function setIsWithinRadius($isWithinRadius)
    {
        $this->is_within_radius = $isWithinRadius;
    }

    /**
     * @return int
     */
    public function getIsExtended()
    {
        return $this->is_extended;
    }

    /**
     * @param int $is_extended
     */
    public function setIsExtended($is_extended)
    {
        $this->is_extended = $is_extended;
    }    
    
    /**
     * @return int
     */
    public function getIsDcOnly()
    {
        return $this->is_dc_only;
    }

    /**
     * @param int $is_dc_only
     */ 
    public function setIsDcOnly($is_dc_only)
    {
        $this->is_dc_only = $is_dc_only;
    }

    /**
     * @return string
     */
    public function getPreorderDate()
    {
        return $this->preorder_date;
    }

    /**
     * @param string $preorder_date
     */
    public function setPreorderDate($preorder_date)
    {
        $this->preorder_date = $preorder_date;

        return $this;
    }

    /**
     * Get the value of utm_parameter
     */ 
    public function getUTMParameter()
    {
        return $this->utm_parameter;
    }

    /**
     * Set the value of utm_parameter
     *
     * @return  self
     */ 
    public function setUTMParameter($utm_parameter)
    {
        $this->utm_parameter = $utm_parameter;

        return $this;
    }

    /**
     * Get the value of first_utm_parameter
     */ 
    public function getFirstUTMParameter()
    {
        return $this->first_utm_parameter;
    }

    /**
     * Set the value of first_utm_parameter
     *
     * @return  self
     */ 
    public function setFirstUTMParameter($first_utm_parameter)
    {
        $this->first_utm_parameter = $first_utm_parameter;

        return $this;
    }

    /**
     * Get the value of site_source
     */ 
    public function getSiteSource()
    {
        return $this->site_source;
    }

    /**
     * Set the value of site_source
     *
     * @return  self
     */ 
    public function setSiteSource($site_source)
    {
        $this->site_source = $site_source;

        return $this;
    }

    /**
     * @return Items\SalesEmployee\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @return Items\SalesFlat\Collection
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * @return Items\SalesGiftcards\Collection
     */
    public function getGiftcards()
    {
        return $this->giftcards;
    }
    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderItem[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderItem
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param array $data_array
     */
    public function setFromArray($data_array = array()) 
    {
        /**
         * We need to store attributes in json format, so we need to encode it
         * and remove it from array so it'll not re-set wrong format
         */
        if (!is_null($data_array['attributes']) && is_array($data_array['attributes'])) {
            $this->attributes =  json_encode($data_array['attributes']);
            unset($data_array['attributes']);
        }

        // check product_attribute
        if (isset($data_array['product_attributes']) && !empty($data_array['product_attributes'])) {
            foreach ($data_array["product_attributes"] as $productAttribute) {
                if (isset($productAttribute["attribute_name"]) && isset($productAttribute["attribute_value"]) && $productAttribute["attribute_name"] == "pengiriman_pre_order_mulai_tanggal") {
                    $dateTime = date_create_from_format('m/d/Y', $productAttribute["attribute_value"]);  
                    $data_array['pre_order_date'] = date_format($dateTime, 'Y-m-d');
                }
            }
            unset($data_array['product_attributes']);
        }

        $this->assign($data_array);

        // $this->weight = $data_array['shipping']['weight'];
        // $this->packaging_height = $data_array['shipping']['packaging_height'];
        // $this->packaging_width = $data_array['shipping']['packaging_width'];
        // $this->packaging_length = $data_array['shipping']['packaging_length'];
        // $this->packaging_uom = $data_array['shipping']['packaging_uom'];
        // $this->normal_price = $data_array['prices']['normal_price'];
        // $this->

        $this->assignAllValue($data_array);
        $this->name = $data_array['name'];

        $this->site_source = "ruparupa";

        if (strpos($data_array['utm_parameter'], "/ace/") !== false || strpos($data_array['utm_parameter'], "/acestore/") !== false) {
            $this->site_source = "ace_online";
        } else if (strpos($data_array['utm_parameter'], "/informa/") !== false || strpos($data_array['utm_parameter'], "/informastore/") !== false) {
            $this->site_source = "informa_online";
        }

        // foreach($data_array as $key => $val) {
        //     // if($key == "shipping") {
        //         if (is_array($val)) {
        //             foreach($val as $keyVal => $value) {
        //                 if(property_exists($this, $keyVal)) {
        //                     $this->{$keyVal} = $value;
        //                 }
        //             }
        //         }
                
        //     // }
        // }

        $data_array['insentif_ace'] = false;
        $data_array['insentif_informa'] = false;
        $data_array['insentif_selma'] = false;

        if (isset($data_array["employees"])) {
            if (is_array($data_array["employees"])) {
                $listEmployees = array();
                if (count($data_array["employees"]) > 0) {
                    foreach ($data_array["employees"] as $emp) {
                        $employee = new \Models\SalesEmployeeItem();
                        $employee->setFromArray($emp);
                        $listEmployees[] = $employee;

                        if($employee->getAppSource() == "missace" || $employee->getAppSource() == "missace-oms") {
                            $data_array['insentif_ace'] = true;
                        }
                        if($employee->getAppSource() == "informa" || $employee->getAppSource() == "informa-oms") {
                            $data_array['insentif_informa'] = true;
                        }
                        if($employee->getAppSource() == "selma" || $employee->getAppSource() == "selma-oms") {
                            $data_array['insentif_selma'] = true;
                        }
                    }
                }
                $this->employees = new \Models\Items\SalesEmployee\Collection($listEmployees);
            }
            // unset employees from data_array, because we'll insert employee data on the single item level
            // or if employees length is 0 we'll also ignore it
            unset($data_array["employees"]);
        }

        // sales order item giftcards        
        if($data_array['marketing']['gift_cards'] != "[]"){
            $itemGiftcardsList = array();
            $giftCards = json_decode($data_array['marketing']['gift_cards'],true);
            foreach($giftCards as $rowGift){
                if($rowGift['voucher_affected'] != "shipping") {
                    $itemGiftcards = new \Models\SalesOrderItemGiftcards();
                    if($rowGift['voucher_amount_used'] > $rowGift['voucher_amount']){
                        $rowGift['voucher_amount_used'] = $rowGift['voucher_amount'];
                    }

                    $validSources = ["coin-exchange-to-product"];
                    if (in_array($rowGift['voucher_source'], $validSources)) {
                        $rowGift['coin_amount'] = json_encode($data_array['marketing']['coin_amount']);
                    }

                    $itemGiftcards->setFromArray($rowGift);
                    $itemGiftcardsList[] = $itemGiftcards;
                }
            }

            if(count($itemGiftcardsList)>0){
                $this->giftcards = new \Models\Items\SalesGiftcards\Collection($itemGiftcardsList);
            }            
        }
        

        $itemFlat = new \Models\SalesOrderItemFlat();
        $itemFlat->setFromArray($data_array);
        $itemFlatList[] = $itemFlat;        

        $this->flat = new \Models\Items\SalesFlat\Collection($itemFlatList);

        if (isset($data_array["shipping"]["lead_time"]) && strtolower($data_array["shipping"]["delivery_method"]) != "pickup") {
            $this->setLeadTimeMin($data_array["shipping"]["lead_time"]["min"]);
            $this->setLeadTimeMax($data_array["shipping"]["lead_time"]["max"]);
            $this->setLeadTimeUnit($data_array["shipping"]["lead_time"]["unit"]);
        }

        if (isset($data_array["shipping"]["final_weight"])) {
            $this->setFinalWeight($data_array["shipping"]["final_weight"]);
        }

        if (isset($data_array["shipping"]["insurance_applied"])) {
            $this->setInsuranceApplied($data_array["shipping"]["insurance_applied"]);
        }

        if (isset($data_array["shipping"]["stock_source"])) {
            if (!empty($data_array["shipping"]["stock_source"]["store_code"])) {
                $this->setStockStoreCode($data_array["shipping"]["stock_source"]["store_code"]);
            }
        }
        
        // Set package data
        if (isset($data_array["package"])) {
            $this->setPackageID($data_array["package"]["package_id"]);
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function assignAllValue($val) {
        foreach($val as $keyVal => $value) {
            if ($keyVal != "tracking_assignation" && $keyVal != "express_courier" && $keyVal != "package" && $keyVal != "stock_source" && $keyVal != "pwp") {
                if (is_array($value)) {
                    $this->assignAllValue($value);
                    
                } else if(property_exists($this, $keyVal)) {
                    $this->{$keyVal} = $value;
                }
            }
        }
    }

    /**
     * @param array $columns
     * @param bool $showEmpty
     * @return mixed
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
        if(empty($this->subtotal)) {
            $view['subtotal'] = \Helpers\Transaction::countSubTotalItem($this->selling_price,$this->handling_fee_adjust,$this->discount_amount);
        }

        if(empty($this->row_total)) {
            $view['row_total'] = \Helpers\Transaction::countTotalItem($view['subtotal'],$this->qty_ordered);
        }

        $view['primary_image_url'] = '';
        $flatProductData = \Helpers\ProductHelper::getFlatProductData($this->sku);
        if (!empty($flatProductData) && isset($flatProductData['primary_image_url'])) {
            $view['primary_image_url'] = $flatProductData['primary_image_url'];
        }

        if($view['delivery_method'] == 'pickup') {
            $view['shipping_info'] = "pickup_in_store";
        } else {
            $view['shipping_info'] = "delivery";
        }

        if(empty($view['attributes'])) {
            $view['attributes'] = '[]';
        }

        $productData = \Helpers\ProductHelper::getFlatProductData($view['sku']);
        $view['url_key'] = !empty($productData['url_key']) ? $productData['url_key'] : '';

        // Gosend - Shipping Address
        $orderAddressModel = new \Models\SalesOrderAddress();
        $orderAddressData = $orderAddressModel->find([
            'address_type = :address_type: AND sales_order_id = :sales_order_id: and group_shipment = :group_shipment:',
            'bind' => [
                'address_type' => 'shipping',
                'sales_order_id' => $view['sales_order_id'],
                'group_shipment' => $view['group_shipment']
            ]
        ]);
        if($orderAddressData->count()){
            $orderAddressModel->setFromArray($orderAddressData->toArray());
            $view['shipping_address'] = $orderAddressModel->getDataArray();
        }else{
            $orderAddressData = $orderAddressModel->find([
                'address_type = :address_type: AND sales_order_id = :sales_order_id:',
                'bind' => [
                    'address_type' => 'shipping',
                    'sales_order_id' => $view['sales_order_id']
                ]
            ]);
            
            $orderAddressModel->setFromArray($orderAddressData->toArray());
            $view['shipping_address'] = $orderAddressModel->getDataArray(); 
        }
        
        if(isset($view['sales_order_item_id']) && !empty($view['sales_order_item_id'])) { 
            $sql = "
                SELECT
                (
                    (
                        select coalesce(sum(srdi.qty_retref),0) 
                        from sales_retref_dc_item srdi 
                        where srdi.sales_order_item_id = {$view['sales_order_item_id']} and 
                        (select status from sales_retref_dc srd where srd.retref_no = srdi.retref_no) <> 'rejected'
                    ) 
                    + 
                    (
                        select coalesce(sum(scmi.qty_refunded),0) 
                        from sales_credit_memo_item scmi left join sales_credit_memo scm on scm.credit_memo_id = scmi.credit_memo_id 
                        where scmi.sales_order_item_id = {$view['sales_order_item_id']}
                    )
                    +
                    (
                        select coalesce(sum(srri.qty_refunded),0) 
                        from sales_return_refund_item srri
                        where srri.sales_order_item_id = {$view['sales_order_item_id']} and 
                        (select status from sales_return_refund srr where srr.refund_no = srri.refund_no) <> 'rejected'
                    )
                ) as qty_refunded
                FROM
                dual
            ";
            $result = $this->getDi()->getShared('dbReader')->query($sql);
            $result->setFetchMode( Database::FETCH_ASSOC );
            $resultData = $result->fetchAll()[0];

            $view['qty_refunded'] = (int)$resultData['qty_refunded'];
        }

        unset($view['sales_order_id']);

        return $view;
    }

    public function checkStockItem()
    {
        if(empty($this->sku) || empty($this->store_code)) {            
            $this->errorCode = "RR204";
            $this->errorMessages[] = "Item not found.";
            return;
        }

        list($stockFound,$origin_stock_qty) = \Helpers\ProductHelper::checkStockItem($this->sku,$this->store_code,$this->qty_ordered);
        if(!$stockFound) {
            $this->errorCode = "RR203";
            $this->errorMessages[] = "Mohon maaf, stock product " . $this->name ." saat ini sedang kosong.";
        }
    }

    /**
     * call stock API to deduct and assign item stock to particular store_code
     * @todo: rethink the entire strategy to anticipate race condition
     * @param string $order_no
     * @param string $company_code
     * @throws \Exception
     */
    public function deductAndAssignItemStock($order_no = '', $company_code = 'ODI', $store_code_new_retail = '', $reference_order_no = '', $cart_type = '', $selectedItem = '')
    {
        if(empty($this->sku) || empty($this->store_code)) {            
            $this->errorCode = "RR204";
            $this->errorMessages[] = "Item not found.";            
            throw new \Exception($this->getSku()." || Mohon maaf, pesanan kamu harus dibatalkan karena keterbatasan stok. dan saldo anda tidak terpotong");
            //throw new \Exception("Item not found");
        }
        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start deductAndAssignItemStock models/SalesOrderItem.php Cart id: '.$this->cart_id. " SKU:".$this->sku, 'debug');
        if($company_code == "ODI"){
            $currentStock = !empty($selectedItem['shipping']['max_stock_store']) ? $selectedItem['shipping']['max_stock_store'] : 0;
            $stockStoreCode = !empty($selectedItem['shipping']['stock_source']['store_code']) ? $selectedItem['shipping']['stock_source']['store_code'] : null;
            $stockStoreCodeQty = !empty($selectedItem['shipping']['stock_source']['store_code']) ? $selectedItem['shipping']['stock_source']['qty'] : 0;
            
            $lastStock = !empty($selectedItem['shipping']['stock_source']['store_code']) ? $currentStock : $currentStock - $this->qty_ordered;
            if($lastStock < 0){
                $lastStock = 0;
            }

            $stockStoreCodeRemainingQty = !empty($selectedItem['shipping']['stock_source']['store_code']) ? $selectedItem['shipping']['stock_source']['qty'] - $this->qty_ordered : 0;
            if ($stockStoreCodeRemainingQty < 0){
                $stockStoreCodeRemainingQty = 0;
            }

            
            $data = [
                "sku" => $this->sku,
                "sales_order_id" => $this->sales_order_id,
                "sales_order_item_id" => $this->sales_order_item_id,
                "store_code" => $this->store_code,
                "stock" => $currentStock,
                "qty_ordered" => $this->qty_ordered,
                "last_stock" => $lastStock,
                "stock_store_code" => $stockStoreCode,
                "stock_store_code_qty" => $stockStoreCodeQty,
                "stock_store_code_remaining_qty" => $stockStoreCodeRemainingQty
            ];

            $productAssignation = new \Models\SalesStockAssignation();
            $productAssignation->assign($data);
            if ($productAssignation->create() === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }
    
                \Helpers\LogHelper::log("stock_assignation", "When save sales stock assignation : sku : " .$this->sku." - sales_order_id :" .$this->sales_order_id . " - error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Failed when try to save data";
    
                throw new \Exception("Failed to assign item");
            }
        } else if ($company_code == "ODIS" || $company_code == "ODIT" || $company_code == "ODIK") {

            // shopee & tokopedia order deduct stock at vendor service
            $lastStock = 0;
            $stockInfo = \Helpers\ProductHelper::checkStockItem($this->sku,$this->store_code,0,"ODI"); 
            
            if($stockInfo[0] == true) {
                $lastStock = $stockInfo[1];
            }

            $currentQty = $lastStock + $this->qty_ordered;

            $productAssignation = new \Models\SalesStockAssignation();
            $data = [
                "sku" => $this->sku,
                "sales_order_id" => $this->sales_order_id,
                "sales_order_item_id" => $this->sales_order_item_id,
                "store_code" => $this->store_code,
                "stock" => $currentQty,
                "qty_ordered" => $this->qty_ordered,
                "last_stock" => $lastStock,
                "stock_store_code" => null,
                "stock_store_code_qty" => 0,
                "stock_store_code_remaining_qty" => 0
            ];
            $productAssignation->assign($data);
    
            if ($productAssignation->create() === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }
    
                \Helpers\LogHelper::log("stock_assignation", "Save data assign product : sku : " .$this->sku." - sales_order_id :" .$this->sales_order_id . " - error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Failed when try to save data";
    
                throw new \Exception("Failed to assign item");
            }
        }

        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End deductAndAssignItemStock models/SalesOrderItem.php Cart id: '.$this->cart_id. " SKU:".$this->sku, 'debug');
    }
    
    public static function checkStockAvailableWithoutStoreCode($sku = "", $cart_type = "", $company_code = "ODI")
    {
    
        if(empty($sku)) {
            return array(false , 0);
        }

        $strQueryParams = "";
        if ($cart_type == "informa_b2b" || $cart_type == "ace_b2b") {
            $strQueryParams .= "&business_unit=". $cart_type;
        }

        // Load shopping card data
        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
        $apiWrapper->setEndPoint("legacy/stock/check_available/".$sku."?&company_code=".strtolower($company_code) .$strQueryParams);  
        if($apiWrapper->send("get")) {              
            $apiWrapper->formatResponse();
        } else {            
            return array(false, 0);
        }
        
        if (!empty($apiWrapper->getError())) {            
            return array(false, 0);
        }        
        // maybe cart is found, but data is empty
        $productStockData = $apiWrapper->getData();              
        
        if (!empty($productStockData['max_qty']) && $productStockData['max_qty'] > 0 ) {
            return array(true, $productStockData['max_qty']);
        } else {
            return array(false, 0);
        }
    }

    /**
     * Get the value of shipping_amount
     */ 
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

    /**
     * Set the value of shipping_amount
     *
     * @return  self
     */ 
    public function setShippingAmount($shipping_amount)
    {
        $this->shipping_amount = $shipping_amount;

        return $this;
    }

    /**
     * Get the value of shipping_discount_amount
     */ 
    public function getShippingDiscountAmount()
    {
        return $this->shipping_discount_amount;
    }

    /**
     * Set the value of shipping_discount_amount
     *
     * @return  self
     */ 
    public function setShippingDiscountAmount($shipping_discount_amount)
    {
        $this->shipping_discount_amount = $shipping_discount_amount;

        return $this;
    }

    /**
     * Get the value of gift_card_amount
     */ 
    public function getGiftCardsAmount()
    {
        return $this->gift_cards_amount;
    }

    /**
     * Set the value of gift_card_amount
     *
     * @return  self
     */ 
    public function setGiftCardsAmount($gift_cards_amount)
    {
        $this->gift_cards_amount = $gift_cards_amount;

        return $this;
    }

    /**
     * Get the value of is_free_item
     */ 
    public function getIsFreeItem()
    {
        return $this->is_free_item;
    }

    /**
     * Set the value of is_free_item
     *
     * @return  self
     */ 
    public function setIsFreeItem($is_free_item)
    {
        $this->is_free_item = $is_free_item;

        return $this;
    }

    public function getIsInstalled()
    {
        return $this->is_installed;
    }

    public function setIsInstalled($is_installed)
    {
        $this->is_installed = $is_installed;

        return $this;
    }

    public function getIsInstallationEligible()
    {
        return $this->is_installation_eligible;
    }

    public function setIsInstallationEligible($is_installation_eligible)
    {
        $this->is_installation_eligible = $is_installation_eligible;

        return $this;
    }

    public function getIsNeedInstallationEligible()
    {
        return $this->is_need_installation_eligible;
    }

    public function setIsNeedInstallationEligible($is_need_installation_eligible)
    {
        $this->is_need_installation_eligible = $is_need_installation_eligible;

        return $this;
    }

    public function getIsNeedInstallation()
    {
        return $this->is_need_installation;
    }

    public function setIsNeedInstallation($is_need_installation)
    {
        $this->is_need_installation = $is_need_installation;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerNote()
    {
        return $this->customer_note;
    }

    /**
     * @param string $name
     */
    public function setCustomerNote($customer_note)
    {
        $this->customer_note = $customer_note;
    }

    /**
     * Get the value of cart_id
     */ 
    public function getCart_id()
    {
        return $this->cart_id;
    }

    /**
     * Set the value of cart_id
     *
     * @return  self
     */ 
    public function setCart_id($cart_id)
    {
        $this->cart_id = $cart_id;

        return $this;
    }

    public function getLeadTimeMin()
    {
        return $this->lead_time_min;
    }

    public function setLeadTimeMin($leadTimeMin)
    {
        $this->lead_time_min = $leadTimeMin;
    }

    public function getLeadTimeMax()
    {
        return $this->lead_time_max;
    }

    public function setLeadTimeMax($leadTimeMax)
    {
        $this->lead_time_max = $leadTimeMax;
    }

    public function getLeadTimeUnit()
    {
        return $this->lead_time_unit;
    }

    public function setLeadTimeUnit($leadTimeUnit)
    {
        $this->lead_time_unit = $leadTimeUnit;
    }

    public function setFinalWeight($finalWeight)
    {
        $this->final_weight = $finalWeight;
    }

    public function setInsuranceApplied($insuranceApplied)
    {
        $this->insurance_applied = $insuranceApplied;
    }

    public function setStockStoreCode($stockStoreCode)
    {
        $this->stock_store_code = $stockStoreCode;
    }
    
    public function getStockStoreCode()
    {
        return $this->stock_store_code;
    }

    /**
     * @return int
     */
    public function getPackageID()
    {
        return $this->package_id;
    }

    /**
     * @param int $package_id
     */
    public function setPackageID($package_id)
    {
        $this->package_id = $package_id;
    }
}
