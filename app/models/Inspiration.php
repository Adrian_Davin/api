<?php
/**
 * Created by PhpStorm.
 * User: Aaron Putra
 * Date: 04/05/2017
 * Time: 10.35
 */

namespace Models;


class Inspiration extends \Models\BaseModel
{
    protected $inspiration_id;
    protected $title;
    protected $small_banner;
    protected $big_banner;
    protected $priority;
    protected $show_home_page;
    protected $status;
    protected $products;
    protected $url_key;
    protected $old_url_key;
    protected $url_path_small_banner;
    protected $url_path_big_banner;
    protected $inline_script;
    protected $additional_header;

    protected $created_at;
    protected $updated_at;

    protected $errorCode;
    protected $errorMessages;

    protected $old_products;

    /**
     * @return mixed
     */
    public function getAdditionalHeader()
    {
        return $this->additional_header;
    }

    /**
     * @param mixed $additional_header
     */
    public function setAdditionalHeader($additional_header)
    {
        $this->additional_header = $additional_header;
    }
    /**
     * @return mixed
     */
    public function getOldUrlKey()
    {
        return $this->old_url_key;
    }

    /**
     * @param mixed $old_url_key
     */
    public function setOldUrlKey($old_url_key)
    {
        $this->old_url_key = $old_url_key;
    }
    /**
     * @return mixed
     */
    public function getInlineScript()
    {
        return $this->inline_script;
    }

    /**
     * @param mixed $inline_script
     */
    public function setInlineScript($inline_script)
    {
        $this->inline_script = $inline_script;
    }
    /**
     * @return mixed
     */
    public function getInspirationId()
    {
        return $this->inspiration_id;
    }

    /**
     * @param mixed $inspiration_id
     */
    public function setInspirationId($inspiration_id)
    {
        $this->inspiration_id = $inspiration_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSmallBanner()
    {
        return $this->small_banner;
    }

    /**
     * @param mixed $small_banner
     */
    public function setSmallBanner($small_banner)
    {
        $this->small_banner = $small_banner;
    }

    /**
     * @return mixed
     */
    public function getBigBanner()
    {
        return $this->big_banner;
    }

    /**
     * @param mixed $big_banner
     */
    public function setBigBanner($big_banner)
    {
        $this->big_banner = $big_banner;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getShowHomePage()
    {
        return $this->show_home_page;
    }

    /**
     * @return mixed
     */
    public function getUrlKey()
    {
        return $this->url_key;
    }

    /**
     * @param mixed $url_key
     */
    public function setUrlKey($url_key)
    {
        $this->url_key = $url_key;
    }

    /**
     * @param mixed $show_home_page
     */
    public function setShowHomePage($show_home_page)
    {
        $this->show_home_page = $show_home_page;
    }

    /**
     * @return mixed
     */
    public function getUrlPathSmallBanner()
    {
        return $this->url_path_small_banner;
    }

    /**
     * @param mixed $url_path_small_banner
     */
    public function setUrlPathSmallBanner($url_path_small_banner)
    {
        $this->url_path_small_banner = $url_path_small_banner;
    }

    /**
     * @return mixed
     */
    public function getUrlPathBigBanner()
    {
        return $this->url_path_big_banner;
    }

    /**
     * @param mixed $url_path_big_banner
     */
    public function setUrlPathBigBanner($url_path_big_banner)
    {
        $this->url_path_big_banner = $url_path_big_banner;
    }

    /**
     * @return mixed
     */
    public function getOldProducts()
    {
        return $this->old_products;
    }

    /**
     * @param mixed $old_products
     */
    public function setOldProducts($old_products)
    {
        $this->old_products = $old_products;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function initialize()
    {

    }

    public function getSource()
    {
        return 'product_inspiration';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Event
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setInspirationFromArray($params = array()){

        foreach ($params as $event => $val){
            if (property_exists($this, $event)) {

                $this->{$event} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }
}