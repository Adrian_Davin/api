<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 8/22/2017
 * Time: 3:24 PM
 */

namespace Models;

class CustomerAlert extends \Models\BaseModel
{
    /**
     * @var $customer_alert_id
     */
    protected $customer_alert_id;

    protected $customer_id;
    /**
     * @var $email
     */
    protected $email;

    /**
     * @var $phone
     */
    protected $phone;

    /**
     * @var $remark
     */
    protected $remark;

    /**
     * @var $status
     */
    protected $status;

    protected $Messages;

    protected $fingerprint;

    public function onConstruct()
    {
        parent::onConstruct();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_alert';
    }

    /**
     * @return int
     */
    public function getCustomerAlertId()
    {
        return $this->customer_alert_id;
    }

    /**
     * @param int $customer_alert_id
     */
    public function setCustomerAlertId($customer_alert_id)
    {
        $this->customer_alert_id = $customer_alert_id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->Messages;
    }

    /**
     * @param mixed $Messages
     */
    public function setMessages($Messages)
    {
        $this->Messages = $Messages;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param mixed $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        if(!empty($data_array['email'])) {  
            $this->email = $data_array['email'];
        }

        if(!empty($data_array['phone'])) {
            $this->phone = $data_array['phone'];
        }

        if(!empty($data_array['remark'])) {
            $this->remark = $data_array['remark'];
        }

        if(!empty($data_array['customer_id'])) {
            $this->customer_id = $data_array['customer_id'];
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    /**
     * Get the value of fingerprint
     */ 
    public function getFingerprint()
    {
        return $this->fingerprint;
    }

    /**
     * Set the value of fingerprint
     *
     * @return  self
     */ 
    public function setFingerprint($fingerprint)
    {
        $this->fingerprint = $fingerprint;

        return $this;
    }
}