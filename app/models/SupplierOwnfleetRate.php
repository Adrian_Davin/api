<?php

namespace Models;

class SupplierOwnfleetRate extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $ownfleet_template_id;
    protected $sku;
    protected $origin_region;
    protected $dest_district_code;
    protected $rate;
    protected $include_exclude;
    protected $is_flat;
    protected $carrier_code;

    protected $string_value;

    protected $created_at;
    protected $updated_at;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getOwnfleetTemplateId()
    {
        return $this->ownfleet_template_id;
    }

    /**
     * @param int $ownfleet_template_id
     */
    public function setOwnfleetTemplateId($ownfleet_template_id)
    {
        $this->ownfleet_template_id = $ownfleet_template_id;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getOriginRegion()
    {
        return $this->origin_region;
    }

    /**
     * @param mixed $origin_region
     */
    public function setOriginRegion($origin_region)
    {
        $this->origin_region = $origin_region;
    }

    /**
     * @return mixed
     */
    public function getDestDistrictCode()
    {
        return $this->dest_district_code;
    }

    /**
     * @param mixed $dest_district_code
     */
    public function setDestDistrictCode($dest_district_code)
    {
        $this->dest_district_code = $dest_district_code;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getIncludeExclude()
    {
        return $this->include_exclude;
    }

    /**
     * @param mixed $include_exclude
     */
    public function setIncludeExclude($include_exclude)
    {
        $this->include_exclude = $include_exclude;
    }

    /**
     * @return mixed
     */
    public function getIsFlat()
    {
        return $this->is_flat;
    }

    /**
     * @param mixed $is_flat
     */
    public function setIsFlat($is_flat)
    {
        $this->is_flat = $is_flat;
    }

    /**
     * @return mixed
     */
    public function getCarrierCode()
    {
        return $this->carrier_code;
    }

    /**
     * @param mixed $carrier_code
     */
    public function setCarrierCode($carrier_code)
    {
        $this->carrier_code = $carrier_code;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('ownfleet_template_id', 'Models\SupplierOwnfleetTemplate', 'ownfleet_template_id', array('alias' => 'SupplierOwnfleetTemplate'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'supplier_ownfleet_rate';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function insertTemplateToRate(){

        $sql  = "INSERT INTO supplier_ownfleet_rate(ownfleet_template_id,origin_region,dest_district_code,rate,is_flat,carrier_code,include_exclude,created_at) ";
        $sql .= "VALUES".$this->string_value;

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        if($result->numRows() > 0){
            return $msg = "Insert template to rate success!";
        }
        else {
            return $msg = "error";
        }
    }

    public function clearShippingRate($id){
        $sql  = "DELETE FROM supplier_ownfleet_rate ";
        $sql .= "WHERE ownfleet_template_id=".$id;

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        if($result->numRows() > 0){
            return $msg = "success";
        }
        else {
            return $msg = "error";
        }
    }

}
