<?php

namespace Models;
class ColoursMapping extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $value_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $colours_Ruparupa;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $colours_lazada;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->getSource();
    }

    public function getSource()
    {
        return 'colours_mapping';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterColor[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterColor
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
