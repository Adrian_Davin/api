<?php

namespace Models;

use \Library\APIWrapper;
use Phalcon\Db as Database;
use Helpers\LogHelper;

class SalesInvoiceItem extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $invoice_item_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $invoice_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $sales_order_item_id;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $selling_price;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $discount_amount;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $qty_ordered;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $gift_cards_amount;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $row_total;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $subtotal;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $sku;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $delivery_method;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $store_code;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $supplier_alias;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $pickup_code;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected  $product_source;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $handling_fee_adjust;

    /**
     *
     * @var string     
     */
    protected $preorder_date;

    protected $price_modified;
    protected $modified_description;
    protected $commission;
    protected $net_sales;

    protected $gift_warping = 0;
    protected $gift_message = "";
    protected $service_date;

    protected $is_free_item = 0;
    protected $shipping_amount = 0;
    protected $shipping_discount_amount = 0;

    protected $available_stock;
    protected $status_fulfillment;
    protected $reason;
    protected $note;
    protected $created_at;
    protected $updated_at;

    protected $attributes = [];

    protected $errors;

    // for refund purpose
    protected $group_promo_id;
    protected $rule_id;
    protected $package_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('invoice_id', 'Models\SalesInvoice', 'invoice_id', array('alias' => 'SalesInvoice','reusable' => true));
        $this->belongsTo('sales_order_item_id', 'Models\SalesOrderItem', 'sales_order_item_id', array('alias' => 'SalesOrderItem','reusable' => true));
        $this->belongsTo('sku', 'Models\ProductVariant', 'sku', array('alias' => 'ProductVariant', 'reusable' => true));
        $this->belongsTo('sales_order_item_id', 'Models\SalesOrderItemFlat', 'sales_order_item_id', array('alias' => 'SalesOrderItemFlat',"reusable", true));
        $this->belongsTo('sales_order_item_id', 'Models\SalesOrderItemMdr', 'sales_order_item_id', array('alias' => 'SalesOrderItemMdr',"reusable", true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_invoice_item';
    }

    /**
     * @return int
     */
    public function getInvoiceItemId()
    {
        return $this->invoice_item_id;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoice_id;
    }

    /**
     * @param int $invoice_id
     */
    public function setInvoiceId($invoice_id)
    {
        $this->invoice_id = $invoice_id;
    }

    /**
     * @return int
     */
    public function getSalesOrderItemId()
    {
        return $this->sales_order_item_id;
    }

    /**
     * @param int $sales_order_item_id
     */
    public function setSalesOrderItemId($sales_order_item_id)
    {
        $this->sales_order_item_id = $sales_order_item_id;
    }

    /**
     * @return float
     */
    public function getSellingPrice()
    {
        return $this->selling_price;
    }

    /**
     * @param float $selling_price
     */
    public function setSellingPrice($selling_price)
    {
        $this->selling_price = $selling_price;
    }

    /**
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * @param float $discount_amount
     */
    public function setDiscountAmount($discount_amount)
    {
        $this->discount_amount = $discount_amount;
    }

    /**
     * @return int
     */
    public function getQtyOrdered()
    {
        return $this->qty_ordered;
    }

    /**
     * @param int $qty_ordered
     */
    public function setQtyOrdered($qty_ordered)
    {
        $this->qty_ordered = $qty_ordered;
    }

    /**
     * @return float
     */
    public function getGiftCardsAmount()
    {
        return $this->gift_cards_amount;
    }

    /**
     * @param float $gift_cards_amount
     */
    public function setGiftCardsAmount($gift_cards_amount)
    {
        $this->gift_cards_amount = $gift_cards_amount;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return \Helpers\Transaction::countSubTotalItem($this->selling_price,$this->handling_fee_adjust,$this->discount_amount);
    }

    /**
     * @param float $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return float
     */
    public function getRowTotal()
    {
        if(empty($this->subtotal)) {
            $this->subtotal = $this->getSubtotal();
        }

        return \Helpers\Transaction::countTotalItem($this->subtotal,$this->qty_ordered);
    }

    /**
     * @param float $row_total
     */
    public function setRowTotal($row_total)
    {
        $this->row_total = $row_total;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDeliveryMethod()
    {
        return $this->delivery_method;
    }

    /**
     * @param string $delivery_method
     */
    public function setDeliveryMethod($delivery_method)
    {
        $this->delivery_method = $delivery_method;
    }

     /**
     * @return string
     * Get the value of group_promo_id
     */ 
    public function getGroupPromoId()
    {
        return $this->group_promo_id;
    }

    /**
     * Set the value of group_promo_id
     *
     * @param  string $group_promo_id
     */ 
    public function setGroupPromoId($group_promo_id)
    {
        $this->group_promo_id = $group_promo_id;
    }

    /**
     * @return  int
     * Get the value of rule_id
     */ 
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * Set the value of rule_id
     *
     * @param  int $rule_id
     */ 
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->store_code;
    }

    /**
     * @param string $store_code
     */
    public function setStoreCode($store_code)
    {
        $this->store_code = $store_code;
    }

    /**
     * @return string
     */
    public function getSupplierAlias()
    {
        return $this->supplier_alias;
    }

    /**
     * @param string $supplier_alias
     */
    public function setSupplierAlias($supplier_alias)
    {
        $this->supplier_alias = $supplier_alias;
    }

    /**
     * @return string
     */
    public function getPickupCode()
    {
        return $this->pickup_code;
    }

    /**
     * @param string $pickup_code
     */
    public function setPickupCode($pickup_code)
    {
        $this->pickup_code = $pickup_code;
    }

    /**
     * @return string
     */
    public function getProductSource()
    {
        return $this->product_source;
    }

    /**
     * @param string $product_source
     */
    public function setProductSource($product_source)
    {
        $this->product_source = $product_source;
    }

    /**
     * @return float
     */
    public function getHandlingFeeAdjust()
    {
        return $this->handling_fee_adjust;
    }

    /**
     * @param float $handling_fee_adjust
     */
    public function setHandlingFeeAdjust($handling_fee_adjust)
    {
        $this->handling_fee_adjust = $handling_fee_adjust;
    }

    /**
     * @return int
     */
    public function getGiftWarping()
    {
        return $this->gift_warping;
    }

    /**
     * @param int $gift_warping
     */
    public function setGiftWarping($gift_warping)
    {
        $this->gift_warping = $gift_warping;
    }

    /**
     * @return string
     */
    public function getGiftMessage()
    {
        return $this->gift_message;
    }

    /**
     * @param string $gift_message
     */
    public function setGiftMessage($gift_message)
    {
        $this->gift_message = $gift_message;
    }

    /**
     * @return string
     */
    public function getServiceDate()
    {
        return $this->service_date;
    }

    /**
     * @param string $service_date
     */
    public function setServiceDate($service_date)
    {
        $this->service_date = $service_date;
    }

    /**
     * @return int
     */
    public function getisFreeItem()
    {
        return $this->is_free_item;
    }

    /**
     * @param int $is_free_item
     */
    public function setIsFreeItem($is_free_item)
    {
        $this->is_free_item = $is_free_item;
    }

    /**
     * @return int
     */
    public function getAvailableStock()
    {
        return $this->available_stock;
    }

    /**
     * @param int $available_stock
     */
    public function setAvailableStock($available_stock)
    {
        $this->available_stock = $available_stock;
    }

    /**
     * @return string
     */
    public function getStatusFulfillment()
    {
        return $this->status_fulfillment;
    }

    /**
     * @param string $status_fulfillment
     */
    public function setStatusFulfillment($status_fulfillment)
    {
        $this->status_fulfillment = $status_fulfillment;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getPreorderDate()
    {
        return $this->preorder_date;
    }

    /**
     * @param string $preorder_date
     */
    public function setPreorderDate($preorder_date)
    {
        $this->preorder_date = $preorder_date;

        return $this;
    }

    /**
     * @return float
     */
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

     /**
     * @return float
     */
    public function getShippingDiscountAmount()
    {
        return $this->shipping_discount_amount;
    }


    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param $errors mixed
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesInvoiceItem[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesInvoiceItem
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_invoice_item", "TRIGGERED");
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        if(!empty($data_array['preorder_date'])){
            $this->preorder_date = $data_array['preorder_date'];
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = parent::toArray($columns,true);

        if(empty($this->subtotal)) {
            $view['subtotal'] = \Helpers\Transaction::countSubTotalItem($this->selling_price,$this->handling_fee_adjust,$this->discount_amount);
        }

        if(empty($this->row_total)) {
            $view['row_total'] = \Helpers\Transaction::countTotalItem($view['subtotal'],$this->qty_ordered);
        }

        $flatProductData = \Helpers\ProductHelper::getFlatProductData($this->sku);
        if (isset($flatProductData['primary_image_url'])) {
            $view['primary_image_url'] = $flatProductData['primary_image_url'];
        }

        $sql = "SELECT COALESCE(qty_refunded,0) as qty_refunded FROM sales_credit_memo_item WHERE sales_order_item_id = " . $this->SalesOrderItem->SalesOrderItemId;
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        $qty_refunded = 0;
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $hasil = $result->fetchAll();
            $qty_refunded = $hasil[0]['qty_refunded'];
        }
        $view['qty_refunded'] = $qty_refunded;

        if(empty($view['attributes'])) {
            $view['attributes'] = '[]';
        }

        return $view;
    }

    public function getInvoiceItem($params = array())
    {
        $response['data'] = $response['messages'] = $response['errors'] = array();
        if (count($params) > 0) {
            $sql = "
                    SELECT 
                        sii.invoice_item_id,
                        so.sales_order_id,
                        so.order_no,
                        sii.invoice_id,
                        si.invoice_no,
                        sii.sales_order_item_id,
                        sii.sku,
                        si.store_code,
                        sii.name,
                        sii.qty_ordered,	
                        sii.available_stock,
                        COALESCE(si.sap_so_number, '') AS so_number,
                        COALESCE(sii.status_fulfillment, '') AS status_fulfillment,
                        sii.reason,
                        sii.note,
                        si.company_code,
                        ss.shipment_status,
                        ss.carrier_id,
                        ss.track_number,
                        COALESCE(si.total_koli,0) as total_koli	
                    FROM
                        sales_invoice_item sii LEFT JOIN
                        sales_invoice si ON si.invoice_id = sii.invoice_id LEFT JOIN
                        sales_order so ON so.sales_order_id = si.sales_order_id LEFT JOIN
                        sales_shipment ss ON ss.invoice_id = sii.invoice_id";

            $countWhere = 0;
            if (isset($params['invoice_id'])) {
                if ($countWhere == 0) $sql .= ' WHERE ';

                $sql .= "sii.invoice_id = {$params['invoice_id']}";
                $countWhere++;
            }

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $hasil = $result->fetchAll();

                $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));

                $i = 0;
                foreach ($hasil as $row) {
                    if (empty($row['available_stock'])) {
                        $availableStock = 0;

                        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                        $apiWrapper->setEndPoint("legacy/stock/check_available/".$row['sku']."?store_code=".$row['store_code']."&company_code=".strtolower($row['company_code']));
                        if($apiWrapper->send("get")) {
                            $apiWrapper->formatResponse();
                            $productStockData = $apiWrapper->getData();
                            if (count($productStockData['stock_list']) > 0) {
                                foreach ($productStockData['stock_list'] as $rowStock) {
                                    if ($rowStock['store_code'] == $row['store_code']) {
                                        $availableStock = $rowStock['qty'];
                                    }
                                }
                            }
                        }

                        $hasil[$i]['available_stock'] = $availableStock;
                    }

                    $i++;
                }

                $response['data'] = $hasil;
                $response['messages'] = array('success');
            }
        } else {
            $response['errors'] = array('You must send valid parameter to get data');
        }

        return $response;
    }

    public function saveData($log_file_name = "error", $state = "")
    {
        $action = $this->getAction();
        try {
            if($action == "create") {
                $this->created_at = date("Y-m-d H:i:s");
                $saveStatus = $this->create();
            } else {
                $saveStatus = $this->update();
            }

            // $saveStatus = false;
            if ($saveStatus === false) {
                $messages = $this->getMessages();
                if(!empty($messages)) {
                    $errMsg = array();
                    foreach ($messages as $message) {
                        $errMsg[] = $message->getMessage();
                    }

                    LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                    $this->errors[] = "Save Sales invoice item failed";

                    if($state !== "") {
                        $params_data = array(
                            "message" => "Failed at $state : Caused by: ".json_encode($errMsg),
                            "slack_channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                        );
                        $this->notificationError($params_data);
                    }
                } else {
                    LogHelper::log($log_file_name, "Save data failed, error : unknown error");
                    $this->errors[] = "Save data failed, error : unknown error";

                    if($state !== "") {
                        $params_data = array(
                            "message" => "Failed at $state : Caused by: unknown error",
                            "slack_channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                        );
                        $this->notificationError($params_data);
                    }
                }

                $transactionManager = new \Phalcon\Mvc\Model\Transaction\Manager();
                $transactionManager->setDbService($this->getConnection());
                $is_have_transaction = $transactionManager->has();
                if($is_have_transaction) {
                    throw new \Exception("Rollback transaction");
                }

                return false;
            }
        } catch (\Exception $e) {
            $this->errors[] = "Save Sales invoice item failed (" . $e->getMessage() . ")";
        }

        return true;
    }

    public function getInvoiceItemByOrderNo($orderNo = "")
    {
        $resultArray = array();
        try {
            if (!empty($orderNo)) {
                $sql = "SELECT * from sales_invoice_item where invoice_id in (
                    SELECT invoice_id from sales_invoice where sales_order_id IN (SELECT sales_order_id FROM sales_order WHERE order_no = '$orderNo')
                );";

                $this->useReadOnlyConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $resultArray = $result->fetchAll();
                }
            }
        } catch (\Exception $e) {
            return $resultArray;
        }

        return $resultArray;
    }

    public function updateStatusFulfillmentInvoiceItem($salesOrderItemID = 0)
    {
        $result = true;
        if ($salesOrderItemID > 0) {
            try {
                $sql = "UPDATE sales_invoice_item SET status_fulfillment = 'refunded' WHERE sales_order_item_id = ".$salesOrderItemID;
                $result = $this->getDi()->getShared('dbMaster')->execute($sql);
                if(!$result){
                    $result = false;
                }
            }  catch (\Exception $e) {
                $result = false;
            }
        }

        return $result;
    }

    public function notificationError($params_data = array()){
        $notificationText = $params_data["message"];
        $params = [ 
            "channel" => $params_data["slack_channel"],
            "username" => $params_data["slack_username"],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }
}
