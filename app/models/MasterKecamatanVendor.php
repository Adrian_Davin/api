<?php

namespace Models;

use \Library\Redis\MasterKecamatanVendor as MasterKecamatanVendorRedis;

class MasterKecamatanVendor extends \Models\BaseModel
{

    public $id;
    public $kecamatan_id;
    public $vendor_name;
    public $vendor_kecamatan_id;
    public $created_at;
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasOne('kecamatan_id', 'Models\MasterKecamatan', 'kecamatan_id', array('alias' => 'MasterKecamatan'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_kecamatan_vendor';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterKecamatanVendor[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataList($params = array())
    {
        $listKecamatanVendor = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            MasterKecamatanVendorRedis::deleteListKecamatanVendor();
        } else {
            $listKecamatanVendor = MasterKecamatanVendorRedis::getListKecamatanVendor();
        }

        if(empty($listKecamatanVendor)) {
            $result = $this->find();

            if($result) {
                $listKecamatanVendorRedis = array();
                $listKecamatanVendor = array();
                foreach($result as $kecamatanVendor) {
                    $kecamatanVendorArr = $kecamatanVendor->toArray();
                    $listKecamatanVendorRedis[sprintf("%s:%s", $kecamatanVendorArr["kecamatan_id"], $kecamatanVendorArr["vendor_name"])] = $kecamatanVendorArr;
                    $listKecamatanVendor[] = $kecamatanVendorArr;
                }

                // save to redis
                MasterKecamatanVendorRedis::setListKecamatanVendor($listKecamatanVendorRedis);
            }
        }


        return $listKecamatanVendor;
    }

    public function getData($params = array())
    {
        $kecamatanVendor = array();
        $cacheID = sprintf("%s:%s", $params['kecamatan_id'], $params['vendor_name']);
        if(isset($params['reload']) && $params['reload'] == true) {
            MasterKecamatanVendorRedis::deleteKecamatanVendor($cacheID);
        } else {
            $kecamatanVendor = MasterKecamatanVendorRedis::getKecamatanVendor($cacheID);
        }

        if(empty($kecamatanVendor)) {
            $kecamatanVendorDB = $this->findFirst(sprintf("kecamatan_id = '%s' AND vendor_name = '%s'", $params['kecamatan_id'], $params['vendor_name']));

            if($kecamatanVendorDB) {
                $kecamatanVendor = $kecamatanVendorDB->toArray();
                MasterKecamatanVendorRedis::setKecamatanVendor($cacheID, $kecamatanVendor);
            }
        }

        return $kecamatanVendor;
    }
}
