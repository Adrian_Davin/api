<?php

namespace Models;

class ProductChangeLog extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_id;
    protected $supplier_id;
    protected $supplier_user_id;
    protected $description;
    protected $how_to_use;
    protected $tips_trick;
    protected $specification;
    protected $updated_at;

    protected $errorCode;
    protected $errorMessages;
    protected $errors;
    protected $data;
    protected $messages;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_change_log';
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param mixed $supplier_id
     */
    public function setSupplierUserId($supplier_user_id)
    {
        $this->supplier_user_id = $supplier_user_id;
    }

    /**
     * @return mixed
     */
    public function getSupplierUserId()
    {
        return $this->supplier_user_id;
    }

    /**
     * @param mixed $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $description
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getHowToUse()
    {
        return $this->how_to_use;
    }

    /**
     * @param string $how_to_use
     */
    public function setHowToUse($how_to_use)
    {
        $this->how_to_use = $how_to_use;
    }

    /**
     * @return string
     */
    public function getTipsTrick()
    {
        return $this->tips_trick;
    }

    /**
     * @param string $tips_trick
     */
    public function setTipsTrick($tips_trick)
    {
        $this->tips_trick = $tips_trick;
    }

    /**
     * @return string
     */
    public function getSpecification()
    {
        return $this->specification;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @param string $specification
     */
    public function setSpecification($specification)
    {
        $this->specification = $specification;
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function buildUpdateParam()
    {
        $thisArray = get_object_vars($this);

        unset($thisArray['product_id']);

        $db_params = "";
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) != 'object' && (!empty($val) || $val==='0') && $key != "_dirtyState" ) {
                $val = (is_array($val)) ? json_encode($val) : $val;
                $db_params .= $key . " = '" . $val . "', ";
            }
        }

        $db_params = substr($db_params,0,-2);

        return $db_params;

    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        //$thisArray = get_class_vars(get_class($this));
        //$this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function saveData($log_file_name = "", $state = ""){

        $this->updated_at = date("Y-m-d H:i:s");
        $checkIfLogExist = $this->findFirst(
            array(
                "conditions" => "product_id = ".$this->product_id
            )
        );

        if(empty($checkIfLogExist->toArray())) { // save

            if(is_array($this->tag_search)) {
                $this->tag_search = json_encode($this->tag_search);
            }

            if ($this->create() === false) {

                $messages = $this->getMessages();
                $errMsg = array();
                if(count($messages)>0) {
                    foreach ($messages as $message) {
                        $errMsg[] = $message->getMessage();
                    }
                }

                \Helpers\LogHelper::log("product", "Product save failed, error : " . json_encode($errMsg));
                $this->errors[] = "Save product data failed, please try again";
                return;
            }
        } else if(!empty($this->product_id)) {//update

            $updateParam = $this->buildUpdateParam();
            $query = "UPDATE Models\ProductChangeLog SET ";
            $query .= $updateParam;
            $query .= " WHERE product_id = " . $this->product_id;

            try {
                $manager = $this->getModelsManager();
                $manager->executeQuery($query);

                $this->messages[] = "Product save affected rows: " . $this->getWriteConnection()->affectedRows();
                $this->data['affected_row'] = $this->getWriteConnection()->affectedRows();
            } catch (\Exception $e) {
                \Helpers\LogHelper::log("product", "Product update failed, error : " . $e->getMessage());
                $this->errors[] = "Update failed, please try again";
                return;
            }
        }
    }

    public function productChangeLogDetail()
    {
        try {
            $changeLogDetail = $this->findFirst(
                array(
                    "conditions" => "product_id = " . $this->product_id
                )
            );

            $this->data = $changeLogDetail;
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("product", "Product detail failed, error : " . $e->getMessage());
            $this->errors[] = "Product detail failed, please try again";
            return;
        }

    }

}
