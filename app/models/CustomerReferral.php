<?php

namespace Models;

// use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
// use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerReferral extends \Models\BaseModel
{
  /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $customer_referral_id;

    protected $referral_customer_id;

    protected $invited_customer_id;
    
    protected $device;

    protected $reward_status;

    protected $voucher_id;

    protected $first_purchase_status;

    protected $created_at;

    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_referral';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return int 
     */
    public function getCustomerReferralId()
    {
        return $this->customer_referral_id;
    }

    /**
     * @param int $customer_referral_id
     */
    public function setCustomerReferralId($customer_referral_id)
    {
        $this->customer_referral_id = $customer_referral_id;
    }

    /**
     * @return int
     */
    public function getReferralCustomerId() 
    {
        return $this->referral_customer_id;
    } 

    /**
     * @param int $customer_id
     */
    public function setReferralCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    public function getDevice()
    {
        return $this->device;
    }

    public function setDevice($device)
    {
        $this->device = $device;
    }

    public function getRewardStatus()
    {
        return $this->device;
    }

    public function setRewardStatus($reward_status)
    {
        $this->reward_status = $reward_status;
    }

    public function getVoucherId()
    {
        return $this->voucher_id;
    }

    public function setVoucherId($voucher_id)
    {
        $this->voucher_id = $voucher_id;
    }

    public function getFirstPurchaseStatus()
    {
        return $this->first_purchase_status;
    }

    public function setFirstPurchaseStatus($first_purchase_status)
    {
        $this->first_purchase_status = $first_purchase_status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
          if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
  }

  public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}