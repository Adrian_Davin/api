<?php

namespace Models;

use Phalcon\Db as Database;

class SalesShipmentTracking extends \Models\BaseModel
{
    /**
     *
     * @var integer
     */
    protected $shipment_tracking_id;

    /**
     *
     * @var integer
     */
    protected $shipment_id;

    /**
     *
     * @var string
     */
    protected $status;

        /**
     *
     * @var string
     */
    protected $additional_info;

    /**
     *
     * @var string
     */
    protected $created_at;

    /**
     *
     * @var string
     */
    protected $updated_at;

    /**
     *
     * @var string
     */
    protected $updated_by;

    /**
     *
     * @var string
     */
    protected $remark;

    /**
     * Method to set the value of field shipment_tracking_id
     *
     * @param integer $shipment_tracking_id
     * @return $this
     */
    public function setShipmentTrackingId($shipment_tracking_id)
    {
        $this->shipment_tracking_id = $shipment_tracking_id;

        return $this;
    }

    /**
     * Method to set the value of field shipment_id
     *
     * @param integer $shipment_id
     * @return $this
     */
    public function setShipmentId($shipment_id)
    {
        $this->shipment_id = $shipment_id;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field additional_info
     *
     * @param string $additional_info
     * @return $this
     */
    public function setAdditionalInfo($additional_info)
    {
        $this->additional_info = $additional_info;

        return $this;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param string $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Method to set the value of field updated_at
     *
     * @param string $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Method to set the value of field updated_by
     *
     * @param string $updated_by
     * @return $this
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * Method to set the value of field remark
     *
     * @param string $remark
     * @return $this
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Returns the value of field shipment_tracking_id
     *
     * @return integer
     */
    public function getShipmentTrackingId()
    {
        return $this->shipment_tracking_id;
    }

    /**
     * Returns the value of field shipment_id
     *
     * @return integer
     */
    public function getShipmentId()
    {
        return $this->shipment_id;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field additional_info
     *
     * @return string
     */
    public function getAdditionalInfo()
    {
        return $this->additional_info;
    }

    /**
     * Returns the value of field created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the value of field updated_at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Returns the value of field updated_by
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Returns the value of field remark
     *
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('shipment_id', 'SalesShipment', 'shipment_id', array('alias' => 'SalesShipment'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_shipment_tracking';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesShipmentTracking[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesShipmentTracking
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_shipment_tracking", "TRIGGERED");
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public function getAwbTracking($params = array())
    {
        $response['data'] = $response['errors'] = $data = array();
        $sql = '';
        try {
            if (isset($params['awb_no'])) {
                $sql = "
                    SELECT
                        sst.status,
                        sst.created_at,
                        ss.track_number AS 'no_awb',
                        coalesce(sc.carrier_name, 'unknown') AS carrier_name
                    FROM
                        sales_shipment_tracking sst LEFT JOIN
                        sales_shipment ss ON ss.shipment_id = sst.shipment_id LEFT JOIN
                        sales_order_address soa ON soa.sales_order_address_id = ss.shipping_address_id LEFT JOIN
                        shipping_carrier sc ON sc.carrier_id = ss.carrier_id
                    WHERE
                        ss.track_number = '{$params['awb_no']}' AND
                        sst.remark = 'tracking_awb'
                    ORDER BY sst.created_at ASC;";
            } elseif (isset($params['invoice_no'])) {
                $sql = "
                    SELECT
                        sst.status,
                        sst.created_at,
                        ss.track_number AS 'no_awb',
                        COALESCE(CONCAT(soa.first_name,' ',soa.last_name), '') as shipping_to,
                        coalesce(sc.carrier_name, 'unknown') AS carrier_name
                    FROM
                        sales_shipment_tracking sst LEFT JOIN
                        sales_shipment ss ON ss.shipment_id = sst.shipment_id LEFT JOIN
                        sales_invoice si ON si.invoice_id = ss.invoice_id LEFT JOIN
                        sales_order_address soa ON soa.sales_order_address_id = ss.shipping_address_id LEFT JOIN
                        shipping_carrier sc ON sc.carrier_id = ss.carrier_id
                    WHERE
                        si.invoice_no = '{$params['invoice_no']}' AND
                        sst.remark = 'tracking_awb'
                    ORDER BY sst.created_at ASC;";
            }

            if (!empty($sql)) {
                $this->useReadOnlyConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if ($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $data = $result->fetchAll();
                }

                $response['messages'] = array('success');
                $response['data'] = $data;
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Shipment tracking data not found (' . $e->getMessage() . ')');
        }

        return $response;
    }
}
