<?php

namespace Models;

use Phalcon\Db as Database;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class SalesruleCreditCard extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $salesrule_credit_card_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $customer_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $rule_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $cc_type;


     /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $cc_number;

     /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;
  

    /**
     * @return int
     */
    public function getSalesruleCreditCardID()
    {
        return $this->salesrule_credit_card_id;
    }

    /**
     * @param int $salesrule_credit_card_id
     */
    public function setSalesruleCreditCardID($salesrule_credit_card_id)
    {
        $this->salesrule_credit_card_id = $salesrule_credit_card_id;
    }


    /**
     * @return int
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * @param int $rule_id
     */
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;
    }


    /**
     * @return string
     */
    public function getCCType()
    {
        return $this->cc_type;
    }

     /**
     * @param string $cc_type
     */
    public function setCCType($cc_type)
    {
        $this->cc_type = $cc_type;
    }


      /**
     * @return string
     */
    public function getCCNumber()
    {
        return $this->cc_number;
    }

     /**
     * @param string $cc_number
     */
    public function setCCNumber($cc_number)
    {
        $this->cc_number = $cc_number;
    }

     /**
     * @return int
     */
    public function getCustomerID()
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerID($customer_id)
    {
        $this->customer_id = $customer_id;
    }

      /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_credit_card';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
       
    }

    public function onConstruct()
    {
        parent::onConstruct();

    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleCreditCard[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleCreditCard
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

     public function saveData($log_file_name = "error", $state = "")
    {
        try {
            if ($this->save() === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Failed when try to save data";

                if($transaction = $this->get()) {
                    $transaction->rollback(
                        "Cannot save data, rollback"
                    );

                    throw new \Exception("Rollback transaction");
                }

                return false;
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }
}
