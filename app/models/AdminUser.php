<?php

namespace Models;

use Phalcon\Mvc\Model\Validator\Email as Email;
use Phalcon\Db as Database;

class AdminUser extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $admin_user_id;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     *
     * @var string
     */
    protected $firstname;

    /**
     *
     * @var string
     */
    protected $lastname;

    /**
     *
     * @var string
     */
    protected $token;


    /**
     * Method to set the value of field admin_user_id
     *
     * @param integer $admin_user_id
     * @return $this
     */
    public function setAdminUserId($admin_user_id)
    {
        $this->admin_user_id = $admin_user_id;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = md5($password);

        return $this;
    }

    /**
     * Method to set the value of field firstname
     *
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Method to set the value of field lastname
     *
     * @param string $lastname
     * @return $this
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Returns the value of field admin_user_id
     *
     * @return integer
     */
    public function getAdminUserId()
    {
        return $this->admin_user_id;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the value of field firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Returns the value of field lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('admin_user_id', 'Models\AdminUserAttributeValue', 'admin_user_id', array('alias' => 'AdminUserAttributeValue'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'admin_user';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminUser[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AdminUser
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getDataCustomize($params = array())
    {

        $andWhere = "";
        foreach ($params as $key => $value) {
            if ($key != 'master_app_id') {
                $andWhere .= " and " . $key . " = '" . $value . "' ";
            }
        }

        $sql = "
                select 
                  admin_user_id as adminUserId,
                  email,
                  firstname as firstName, 
                  lastname as lastName,
                  (
					  select count(*) from admin_user_attribute_value 
					  where master_app_id = " . $params['master_app_id'] . " and admin_user_id = a.admin_user_id
					  ) as existInApp
                from admin_user a where email is not null " . $andWhere;
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            return $result->fetch();
        }
        return false;
    }

    public function getListAdminUser($params = array())
    {

        $whereFlag = false;

        $sql = "SELECT * FROM admin_user as au";

        foreach ($params as $key => $val) {
            if ($key == "admin_user_id") {
                $whereFlag = true;
                $sql = $sql . " WHERE admin_user_id = '$val'";
            }
            if ($key == "email") {
                if ($whereFlag) {
                    $sql = $sql . " AND email = '$val'";
                } else {
                    $sql = $sql . " WHERE email = '$val'";
                }
                $whereFlag = true;
            }
            if ($key == "firstname") {
                if ($whereFlag) {
                    $sql = $sql . " AND firstname = '$val'";
                } else {
                    $sql = $sql . " WHERE firstname = '$val'";
                }
                $whereFlag = true;
            }
            if ($key == "email_keyword" && $val != '') {
                if ($whereFlag) {
                    $sql = $sql . " AND email LIKE %$val%";
                } else {
                    $sql = $sql . " WHERE email LIKE \"%$val%\"";
                }
                $whereFlag = true;
            }
            if ($key == "fullname_keyword" && $val != '') {
                if ($whereFlag) {
                    $sql = $sql . " AND CONCAT(firstname, \" \", lastname) LIKE \"%$val%\"";
                } else {
                    $sql = $sql . " WHERE CONCAT(firstname, \" \", lastname) LIKE \"%$val%\"";
                }
                $whereFlag = true;
            }

            if ($key == "company_code_keyword" && $val != '') {

                $subsql = 'SELECT admin_user_id FROM admin_user_attribute_value WHERE UPPER(value) LIKE UPPER("%' . $val . '%") AND admin_user_id = au.admin_user_id AND admin_user_attribute_id = 15';

                if ($whereFlag) {
                    $sql = $sql . " AND admin_user_id in ($subsql)";
                } else {
                    $sql = $sql . " WHERE admin_user_id in ($subsql)";
                }
                $whereFlag = true;
            }

            if ($key == "role_keyword" && $val != '') {

                $subsql = 'SELECT count(role_id) FROM gideon_admin_role WHERE role_id in (SELECT value FROM admin_user_attribute_value WHERE admin_user_id = au.admin_user_id AND admin_user_attribute_id = 2 ) AND UPPER(name) LIKE UPPER("%' . $val . '%")';

                if ($whereFlag) {
                    $sql = $sql . " AND ($subsql) > 0";
                } else {
                    $sql = $sql . " WHERE ($subsql) > 0";
                }
                $whereFlag = true;
            }
        }

        if (isset($params['master_app_id'])) {
            if (!empty($params['master_app_id'])) {
                if ($whereFlag) {
                    $sql = $sql . " AND admin_user_id in (SELECT admin_user_id FROM admin_user_attribute_value WHERE master_app_id = {$params['master_app_id']})";
                } else {
                    $sql = $sql . " WHERE admin_user_id in (SELECT admin_user_id FROM admin_user_attribute_value WHERE master_app_id = {$params['master_app_id']})";
                }
            }
        }

        if (!empty($params["order_by"])) {
            $sql = $sql . " ORDER BY " . $params["order_by"];
        } else {
            $sql = $sql . " ORDER BY firstname ASC";
        }

        if (!empty($params["limit"]) && (!empty($params["offset"]) || $params["offset"] == 0 || $params["offset"] == '0')) {
            $limit = intval($params["limit"]);
            $offset = intval($params["offset"]);
        } else {
            $limit = 10;
            $offset = 0;
        }

        $sql = $sql . " LIMIT " . $limit . " OFFSET " . $offset;

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            return $result->fetchAll();
        }
        return array();
    }

    public function getSumListAdminUser($params = array())
    {
        $whereFlag = false;

        $sql = "SELECT COUNT(admin_user_id) FROM admin_user as au";

        foreach ($params as $key => $val) {
            if ($key == "admin_user_id") {
                $whereFlag = true;
                $sql = $sql . " WHERE admin_user_id = '$val'";
            }
            if ($key == "email") {
                if ($whereFlag) {
                    $sql = $sql . " AND email = '$val'";
                } else {
                    $sql = $sql . " WHERE email = '$val'";
                }
                $whereFlag = true;
            }
            if ($key == "firstname") {
                if ($whereFlag) {
                    $sql = $sql . " AND firstname = '$val'";
                } else {
                    $sql = $sql . " WHERE firstname = '$val'";
                }
                $whereFlag = true;
            }
            if ($key == "email_keyword" && $val != '') {
                if ($whereFlag) {
                    $sql = $sql . " AND email LIKE %$val%";
                } else {
                    $sql = $sql . " WHERE email LIKE \"%$val%\"";
                }
                $whereFlag = true;
            }
            if ($key == "fullname_keyword" && $val != '') {
                if ($whereFlag) {
                    $sql = $sql . " AND CONCAT(firstname, \" \", lastname) LIKE \"%$val%\"";
                } else {
                    $sql = $sql . " WHERE CONCAT(firstname, \" \", lastname) LIKE \"%$val%\"";
                }
                $whereFlag = true;
            }

            if ($key == "company_code_keyword" && $val != '') {

                $subsql = 'SELECT admin_user_id FROM admin_user_attribute_value WHERE UPPER(value) LIKE UPPER("%' . $val . '%") AND admin_user_id = au.admin_user_id AND admin_user_attribute_id = 15';

                if ($whereFlag) {
                    $sql = $sql . " AND admin_user_id in ($subsql)";
                } else {
                    $sql = $sql . " WHERE admin_user_id in ($subsql)";
                }
                $whereFlag = true;
            }

            if ($key == "role_keyword" && $val != '') {

                $subsql = 'SELECT count(role_id) FROM gideon_admin_role WHERE role_id in (SELECT value FROM admin_user_attribute_value WHERE admin_user_id = au.admin_user_id AND admin_user_attribute_id = 2 ) AND UPPER(name) LIKE UPPER("%' . $val . '%")';

                if ($whereFlag) {
                    $sql = $sql . " AND ($subsql) > 0";
                } else {
                    $sql = $sql . " WHERE ($subsql) > 0";
                }
                $whereFlag = true;
            }
        }

        if ($whereFlag) {
            $sql = $sql . " AND admin_user_id in (SELECT admin_user_id FROM admin_user_attribute_value WHERE master_app_id = 2)";
        } else {
            $sql = $sql . " WHERE admin_user_id in (SELECT admin_user_id FROM admin_user_attribute_value WHERE master_app_id = 2)";
        }

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            return $result->fetch()['COUNT(admin_user_id)'];
        }
        return 0;
    }

    public function login($params)
    {
        $response = array();
        try {
            $this->useReadOnlyConnection();

            $params['password'] = md5($params['password']);
            $sql = "SELECT 
                    admin_user_id,
                    email,
                    password,
                    firstname,
                    lastname,
                    token
                FROM admin_user
                WHERE email = '{$params['email']}'";

            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $data = $result->fetchAll();

                $sql = "select
                        admin_user_attribute_value_id,
                        admin_user_attribute_id,
                        value
                    from admin_user_attribute_value
                    where admin_user_id = (SELECT admin_user_id from admin_user where email = '{$params['email']}' limit 1)
                    and master_app_id = {$params['master_app_id']}";

                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if ($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $getAtt = $result->fetchAll();

                    foreach ($getAtt as $valueAtt) {
                        foreach ($valueAtt as $keyAtt => $itemAtt) {
                            if ($valueAtt['admin_user_attribute_id'] == '1') {
                                $userArr['lognum'] = $valueAtt['value'];
                                $userArr['id_lognum'] = $valueAtt['admin_user_attribute_value_id'];
                            }

                            // if ($valueAtt['admin_user_attribute_id'] == '3') {
                            //     $userArr['failures_num'] = $valueAtt['value'];
                            //     $userArr['id_failures_num'] = $valueAtt['admin_user_attribute_value_id'];
                            // }

                            if ($valueAtt['admin_user_attribute_id'] == '4') {
                                $userArr['last_login'] = $valueAtt['value'];
                                $userArr['id_last_login'] = $valueAtt['admin_user_attribute_value_id'];
                            }

                            if ($valueAtt['admin_user_attribute_id'] == '5') {
                                $userArr['status'] = $valueAtt['value'];
                                $userArr['id_status'] = $valueAtt['admin_user_attribute_value_id'];
                            }

                            if ($valueAtt['admin_user_attribute_id'] == '16') {
                                $userArr['ip_address'] = $valueAtt['value'];
                                $userArr['id_ip_address'] = $valueAtt['admin_user_attribute_value_id'];
                            }
                        }
                    }

                    if ($userArr['status'] == "0") {
                        $response['messages'] = 'Your account is no longer active';
                    } elseif ($userArr['status'] == "5") {
                        $response['messages'] = 'Your account has been locked due to repeated failed sign in attempts.';
                    } elseif ($userArr['status'] == "10") {
                        $paramsAtt['admin_user_id'] = $data[0]['admin_user_id'];
                        $paramsAtt['master_app_id'] = $params['master_app_id'];

                        if ($data[0]['password'] == $params['password']) {
                            // update ip_address
                            if (isset($userArr['ip_address'])) $paramsAtt['admin_user_attribute_value_id'] = $userArr['id_ip_address'];
                            $paramsAtt['admin_user_attribute_id'] = 16;
                            $paramsAtt['value'] = (isset($params['ip_address'])) ? $params['ip_address'] : '';
                            $this->updateAttributeUserByQuery($paramsAtt);

                            // update lognum
                            if (isset($userArr['id_lognum'])) $paramsAtt['admin_user_attribute_value_id'] = $userArr['id_lognum'];
                            $paramsAtt['admin_user_attribute_id'] = 1;
                            $paramsAtt['value'] = (isset($userArr['lognum'])) ? $userArr['lognum'] + 1 : "1";
                            $this->updateAttributeUser($paramsAtt);

                            // update failures_num to 0
                            // if (isset($userArr['id_failures_num'])) $paramsAtt['admin_user_attribute_value_id'] = $userArr['id_failures_num'];
                            // $paramsAtt['admin_user_attribute_id'] = 3;
                            // $paramsAtt['value'] = "0";
                            // $this->updateAttributeUser($paramsAtt);

                            // last login
                            $last_login = date('Y-m-d H:i:s');
                            if (isset($userArr['id_last_login'])) $paramsAtt['admin_user_attribute_value_id'] = $userArr['id_last_login'];
                            $paramsAtt['admin_user_attribute_id'] = 4;
                            $paramsAtt['value'] = $last_login;
                            $this->updateAttributeUserByQuery($paramsAtt);

                            // insert to table user_daily_login
                            $paramsDailyLogin = array(
                                'admin_user_id' => $paramsAtt['admin_user_id'],
                                'master_app_id' => $paramsAtt['master_app_id'],
                                'login_time' => $last_login
                            );
                            $this->insertUserDailyLogin($paramsDailyLogin);

                            $sql = "select 
                                master_app_id, 
                                (select master_app_name from master_app where master_app_id = auav.master_app_id) AS master_app_name,
                                admin_user_attribute_value_id,
                                auav.admin_user_attribute_id, 
                                (SELECT admin_user_attribute_name from admin_user_attribute where admin_user_attribute_id = auav.admin_user_attribute_id limit 1) as admin_user_attribute_name,
                                (SELECT description from admin_user_attribute where admin_user_attribute_id = auav.admin_user_attribute_id limit 1) as description,
                                value
                            from admin_user_attribute_value as auav
                            where admin_user_id = (SELECT admin_user_id from admin_user where email = '{$params['email']}' limit 1)
                            and master_app_id = {$params['master_app_id']}";

                            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                            if ($result->numRows() > 0) {
                                $result->setFetchMode(Database::FETCH_ASSOC);
                                $data[0]['attributes'] = $result->fetchAll();
                            }

                            $response['messages'] = 'success';

                            // remove password data
                            unset($data[0]['password']);
                            $response['data'] = $data;
                        } else {
                            // update failures_num
                            // $userArr['failures_num'] += 1;
                            // if (isset($userArr['id_failures_num'])) $paramsAtt['admin_user_attribute_value_id'] = $userArr['id_failures_num'];
                            // $paramsAtt['admin_user_attribute_id'] = 3;
                            // $paramsAtt['value'] = "{$userArr['failures_num']}";
                            // $this->updateAttributeUser($paramsAtt);

                            // if ($userArr['failures_num'] > 10) {
                            //     // set status to 5 (locked)
                            //     if (isset($userArr['id_status'])) $paramsAtt['admin_user_attribute_value_id'] = $userArr['id_status'];
                            //     $paramsAtt['admin_user_attribute_id'] = "5"; 
                            //     $paramsAtt['value'] = "5";
                            //     $this->updateAttributeUser($paramsAtt);

                            //     $response['messages'] = "Your account has been locked due to repeated failed sign in attempts.";
                            // } else if ($userArr['failures_num'] > 7) {
                            //     $attempsVal = 10 - $userArr['failures_num'];
                            //     $response['messages'] = "You only have $attempsVal attemp(s) left, please contact our support.";    
                            // } else {
                            //     $response['messages'] = "You entered wrong password";
                            // }
                            $response['messages'] = "Login Failed. Please try again with right email and password";
                        }
                    } else {
                        $response['messages'] = "Wrong login status, Please contact administrator";
                    }
                } else {
                    $response['messages'] = 'You dont have access to this application';
                }
            } else {
                $response['messages'] = 'The Email not recognized.';
            }
        } catch (\Exception $e) {
            $response['errors'] = $response['messages'] = 'failed';

            \Helpers\LogHelper::log("login", "Get user data failed, error : " . $e->getMessage());

            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return $response;
    }

    public function logout($params)
    {
        $response = array();
        try {
            if (isset($params['admin_user_id']) && isset($params['master_app_id'])) {
                $sql = "update admin_user_attribute_value set value = '' where admin_user_id = {$params['admin_user_id']} and admin_user_attribute_id = 16 and master_app_id = {$params['master_app_id']}";
                $result = $this->getDi()->getShared('dbMaster')->execute($sql);
                if (!$result) {
                    $response['errors'] = 'Logout process failed';
                }
            } else {
                $response['errors'] = 'Logout process failed';
            }
        } catch (\Exception $e) {
            $response['errors'] = 'Logout process failed';
            \Helpers\LogHelper::log("user_logout", "Logout process failed, error : " . $e->getMessage());
        }

        return $response;
    }

    private function updateAttributeUser($params)
    {
        $userAttributeModel = new \Models\AdminUserAttributeValue();
        if (isset($params['admin_user_attribute_value_id'])) {
            $dataUpdate['admin_user_attribute_value_id'] = $params['admin_user_attribute_value_id'];
        }

        $dataUpdate['admin_user_id'] = $params['admin_user_id'];
        $dataUpdate['master_app_id'] = $params['master_app_id'];
        $dataUpdate['admin_user_attribute_id'] = $params['admin_user_attribute_id'];
        $dataUpdate['value'] = $params['value'];
        $userAttributeModel->setFromArray($dataUpdate);
        $userAttributeModel->saveData("user_attribute");
    }

    private function insertUserDailyLogin($params)
    {
        try {
            $sql = "insert into user_daily_login (admin_user_id, master_app_id, login_time) 
            values ({$params['admin_user_id']}, {$params['master_app_id']}, '{$params['login_time']}')";
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            if (!$result) {
                \Helpers\LogHelper::log("login", "Insert data daily login error");
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("login", "Failed to insert data daily login, error : " . $e->getMessage());
        }
    }

    private function updateAttributeUserByQuery($params)
    {
        try {
            $userAttributeModel = new \Models\AdminUserAttributeValue();

            $where = '';
            $dataUpdate['value'] = $params['value'];
            if (isset($params['admin_user_attribute_value_id'])) {
                $dataUpdate['admin_user_attribute_value_id'] = $params['admin_user_attribute_value_id'];

                $sql = "
                    update 
                        admin_user_attribute_value 
                    set 
                        value = '" . $dataUpdate['value'] . "' 
                    where  admin_user_attribute_value_id = " . $dataUpdate['admin_user_attribute_value_id'];
                $result = $this->getDi()->getShared('dbMaster')->execute($sql);
                if (!$result) {
                    \Helpers\LogHelper::log("login", "Update admin user attribute value error");
                }
            } else {
                $dataUpdate['admin_user_id'] = $params['admin_user_id'];
                $dataUpdate['master_app_id'] = $params['master_app_id'];
                $dataUpdate['admin_user_attribute_id'] = $params['admin_user_attribute_id'];
                $sql = "insert into admin_user_attribute_value (admin_user_id, master_app_id, admin_user_attribute_id, value) 
                        values (" . $dataUpdate['admin_user_id'] . "," . $dataUpdate['master_app_id'] . "," . $dataUpdate['admin_user_attribute_id'] . ",'" . $dataUpdate['value'] . "')
                ";
                $result = $this->getDi()->getShared('dbMaster')->execute($sql);
                if (!$result) {
                    \Helpers\LogHelper::log("login", "Insert admin user attribute value error");
                }
            }

            $userAttributeModel->setFromArray($dataUpdate);
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("login", "Login process failed, error : " . $e->getMessage());
        }
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray, $data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }
}
