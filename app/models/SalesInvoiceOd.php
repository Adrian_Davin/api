<?php

namespace Models;

class SalesInvoiceOd extends \Models\BaseModel
{

     /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('invoice_no', 'Models\SalesInvoice', 'invoice_no', array('alias' => 'InvoiceNo'));

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_invoice_od';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
