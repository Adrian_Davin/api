<?php

namespace Models;

class ShippingRate extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $origin_province_id;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $origin_province_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $destination_kecamatan_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $destination_kecamatan_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $carrier_id;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    public $price;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $type;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('destination_kecamatan_id', 'Models\MasterKecamatan', 'kecamatan_id', array('alias' => 'MasterKecamatan'));
        $this->belongsTo('origin_province_id', 'Models\MasterProvince', 'province_id', array('alias' => 'MasterProvince'));
        $this->belongsTo('carrier_id', 'Models\ShippingCarrier', 'carrier_id', array('alias' => 'ShippingCarrier'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'shipping_rate';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {

        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

}
