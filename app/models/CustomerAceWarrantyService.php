<?php


namespace Models;


use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerAceWarrantyService extends \Models\BaseModel

{

    protected $customer_ace_warranty_service_id;
    protected $customer_ace_order_warranty_id;
    protected $ace_service_number;
    protected $status;
    protected $service_warranty_charge;
    protected $service_warranty_items;
    protected $service_date;
    protected $service_expected_finished_date;
    protected $created_at;
    protected $updated_at;

    public function initialize()

    {

        //

    }

    /**

     * @return mixed

     */

    public function getCustomerAceWarrantyServiceId()

    {

        return $this->customer_ace_warranty_service_id;

    }


    /**

     * @param mixed

     */

    public function setCustomerAceWarrantyServiceId($customer_ace_warranty_service_id)

    {

        $this->customer_ace_warranty_service_id = $customer_ace_warranty_service_id;

    }


    /**

     * @return mixed

     */

    public function getCustomerAceOrderWarrantyId()

    {

        return $this->customer_ace_order_warranty_id;

    }


    /**

     * @param mixed

     */

    public function setCustomerAceOrderWarrantyId($customer_ace_order_warranty_id)

    {

        $this->customer_ace_order_warranty_id = $customer_ace_order_warranty_id;

    }


    /**

     * @return mixed

     */

    public function getAceServiceNumber()

    {

        return $this->ace_service_number;

    }


    /**

     * @param mixed

     */

    public function setAceServiceNumber($ace_service_number)

    {

        $this->ace_service_number = $ace_service_number;

    }


    /**

     * @return mixed

     */

    public function getStatus()

    {

        return $this->status;

    }


    /**

     * @param mixed

     */

    public function setStatus($status)

    {

        $this->status = $status;

    }


    /**

     * @return mixed

     */

    public function getServiceWarrantyCharge()

    {

        return $this->service_warranty_charge;

    }


    /**

     * @param mixed

     */

    public function setServiceWarrantyCharge($service_warranty_charge)

    {

        $this->service_warranty_charge = $service_warranty_charge;

    }


    /**

     * @return mixed

     */

    public function getServiceWarrantyItems()

    {

        return $this->service_warranty_items;

    }


    /**

     * @param mixed

     */

    public function setServiceWarrantyItems($service_warranty_items)

    {

        $this->service_warranty_items = $service_warranty_items;

    }

    /**

     * @return mixed

     */

    public function getServiceDate()

    {

        return $this->service_date;

    }


    /**

     * @param mixed

     */

    public function setServiceDate($service_date)

    {

        $this->service_date = $service_date;

    }

    /**

     * @return mixed

     */

    public function getServiceExpectedFinishedDate()

    {

        return $this->service_expected_finished_date;

    }


    /**

     * @param mixed

     */

    public function setServiceExpectedFinishedDate($service_expected_finished_date)

    {

        $this->service_expected_finished_date = $service_expected_finished_date;

    }

    /**

     * @return mixed

     */

    public function getCreatedAt()

    {

        return $this->created_at;

    }


    /**

     * @param mixed

     */

    public function setCreatedAt($created_at)

    {

        $this->created_at = $created_at;

    }

    /**

     * @return mixed

     */

    public function getUpdatedAt()

    {

        return $this->updated_at;

    }


    /**

     * @param mixed

     */

    public function setUpdatedAt($updated_at)

    {

        $this->updated_at = $updated_at;

    }


    public function getDataArray($columns = array(), $showEmpty = false)

    {

        return $this->toArray($columns, $showEmpty);

    }


    public static function find($parameters = null)

    {

        return parent::find($parameters);

    }


    public static function findFirst($parameters = null)

    {

        return parent::findFirst($parameters);

    }

}