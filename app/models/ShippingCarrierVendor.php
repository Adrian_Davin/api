<?php

namespace Models;

class ShippingCarrierVendor extends \Models\BaseModel
{
  /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $id;
    protected $carrier_id;
    protected $vendor_name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'shipping_carrier_vendor';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return string
     */
    public function getVendorName() 
    {
        return $this->vendor_name;
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
          if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function getDataArray($columns = array(), $showEmpty = false) {
        $view = $this->toArray($columns, $showEmpty);
        return $view;
    }
}