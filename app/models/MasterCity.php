<?php

namespace Models;

class MasterCity extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $city_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $province_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=false)
     */
    public $city_name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('city_id', 'Models\CustomerAddress', 'city_id', array('alias' => 'CustomerAddress'));
        $this->hasMany('city_id', 'Models\MasterKecamatan', 'city_id', array('alias' => 'MasterKecamatan', 'params' => ['conditions' => 'Models\MasterKecamatan.status > 0']));
        $this->hasMany('city_id', 'Models\SalesOrderAddress', 'city_id', array('alias' => 'SalesOrderAddress'));
        $this->belongsTo('province_id', 'Models\MasterProvince', 'province_id', array('alias' => 'MasterProvince'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_city';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
    }

    public function getDataList($params = array())
    {
        $cities = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterCity::deleteCities();
        } else {
            $cities = \Library\Redis\MasterCity::getCityList();
        }

        if(empty($cities)) {
            $result = $this->find("status = 1");
            //$result = $this->find();

            if($result) {
                $citiesRedis = array();
                $cities = array();
                foreach($result as $city) {
                    $cityArray = $city->toArray();
                    $cityArray['kecamatan'] = $city->MasterKecamatan->toArray();
                    $citiesRedis[$city->city_id] = $cityArray;
                    $cities[] = $cityArray;
                }

                // save to redis
                \Library\Redis\MasterCity::setCities($citiesRedis);
            }
        }


        return $cities;
    }

    public function getData($params = array())
    {
        $city = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            \Library\Redis\MasterCity::deleteCity($params['city_id']);
        } else {
            $city = \Library\Redis\MasterCity::getCityData($params['city_id']);
        }

        if(empty($city)) {
            $cityData = $this->findFirst("city_id = '".$params['city_id']."'");

            if($cityData) {
                $city = $cityData->toArray();
                $city['kecamatan'] = $cityData->MasterKecamatan->toArray();
                \Library\Redis\MasterCity::setCity($params['city_id'],$city);
            }
        }

        return $city;
    }
}
