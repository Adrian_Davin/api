<?php

namespace Models;

use Phalcon\Db as Database;
use Helpers\LogHelper;
use Library\HTTPException;

class SalesComment extends BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_comment_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $sales_order_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $invoice_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $comment;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $refund_no;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $admin_user_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     */
    protected $flag;

    protected $errors;

    /**
     * @var $admin_user \Models\AdminUser
     */
    protected $admin_user;    

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $user_group;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    protected $is_resolved;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $last_update_user;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('admin_user_id', 'Models\AdminUser', 'admin_user_id', array('alias' => 'AdminUser',"reusable" => true));
        $this->belongsTo('invoice_id', 'Models\SalesInvoice', 'invoice_id', array('alias' => 'SalesInvoice',"reusable" => true));
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder',"reusable" => true));
    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->admin_user = new AdminUser();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_comment';
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param $errors mixed
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesComment[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesComment
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function saveData($log_file_name = "", $state = "")
    {
        $this->created_at = date("Y-m-d H:i:s");
        $this->skipAttributesOnUpdate($this->skip_attribute_on_update);

        try {
            if ($this->save() === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                LogHelper::log("sales_comment", "Sales comment save failed, error : " . json_encode($errMsg));
                $this->errors[] = "Save Sales comment failed";
            }
        } catch (\Exception $e) {
            throw new HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

    }

    public function getCommentList()
    {
        if(empty($this->invoice_id) && empty($this->sales_order_id)) {
            return array();
        }

        $query = self::query();
        if(!empty($this->invoice_id) || $this->invoice_id === 0) {
            $query->andWhere("invoice_id = " . $this->invoice_id);
        }

        if(!empty($this->sales_order_id) || $this->sales_order_id === 0) {
            $query->andWhere("sales_order_id = " . $this->sales_order_id);
        }

        if(!empty($this->refund_no) || $this->refund_no != "") {
            $query->andWhere("refund_no = '" . $this->refund_no."'");
        }

        if(!empty($this->flag) || $this->flag === 0) {
            $query->andWhere("flag = '" . $this->flag . "'");
        }

        $query->orderBy("created_at DESC");
        $result = $query->execute();

        $final_result = array();
        foreach($result as $data) {
            $commentData = $data->toArray();
            $checkAdminUser = $this->admin_user->find("admin_user_id = " . $commentData['admin_user_id'])->toArray();
            if (count($checkAdminUser) > 0) {
                unset($commentData['admin_user_id']);
                $userAdminData = $data->AdminUser->toArray(array("admin_user_id", "email", "firstname", "lastname", "flag"));
                $commentData['admin_user'] = $userAdminData;
                if($data->SalesInvoice)
                $commentData['invoice_no'] = $data->SalesInvoice->toArray()['invoice_no'];
                $final_result[] = $commentData;
            }
        }

        if($result) {
            return $final_result;
        } else {
            LogHelper::log("sales_comment", "Error when getting result list");
            return array();
        }
    }

    /**
     * Function to export comment list (oms) by specified parameter
     *
     * @param $params array Paramater to filter the data
     * @return array Response process
     */
    public function getCommentChat($params)
    {
        $response = array();
        try {
            $this->useReadOnlyConnection();
            $sql = "
                SELECT 
                    sco.sales_comment_id, 
                    sco.invoice_id,
                    (SELECT invoice_no from sales_invoice where invoice_id = sco.invoice_id) as invoice_no,
                    (SELECT store_code from sales_invoice where invoice_id = sco.invoice_id) as store_code,
                    sco.comment,
                    (SELECT email from admin_user where admin_user_id = sco.admin_user_id) as created_by,
                    created_at
                FROM
                    sales_comment as sco
                where 
                    sco.flag = 'chat'";

            if (isset($params['shipment_status'])) {
                $sql .= " AND (SELECT shipment_status from sales_shipment where invoice_id = sco.invoice_id limit 1) = '{$params['shipment_status']}'";
            }

            if (isset($params['delivery_method'])) {
                $sql .= " AND (SELECT delivery_method from sales_invoice where invoice_id = sco.invoice_id) in ({$params['delivery_method']});";
            }

            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $data = $result->fetchAll();

                $response['messages'] = 'success';
                $response['data'] = $data;
            }
        }  catch (\Exception $e) {
            $response['errors'] = $response['messages'] = 'failed';
        }

        return $response;
    }
}
