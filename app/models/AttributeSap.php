<?php
/**
 * Created by PhpStorm.
 * User: roesmien_ecomm
 * Date: 2/8/2017
 * Time: 9:49 AM
 */

namespace Models;


class AttributeSap extends \Models\BaseModel
{
    public $attribute_sap_id;

    public $sku;

    public $name_sap;

    public $department_bu;

    public $product_group;

    public $basic_data_text;

    public $characteristic;

    public $vendor_source_list;

    public $department_sisco;

    public $department_sisco_desc;

    public $tax_class;

    public $brand_id;

    public $block_proc;

    public $icg;

    public $base_uom;

    public $dchain_uom;

    public $dist_channel;

    public $netto_weight;

    public $block_sisco;

    public $created_at;


    protected $whiteList;

    public function onConstruct()
    {
        parent::onConstruct(); 
        $this->whiteList = array();
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasOne('sku', 'Models\ProductVariant', 'sku', array('alias' => 'ProductVariant'));
        $this->belongsTo('department_bu', 'Models\DepartmentBu', 'id_department_bu', array('alias' => 'DepartmentBu'));
    }

    /**
     * @return mixed
     */
    public function getProductGroup()
    {
        return $this->product_group;
    }

    /**
     * @param mixed $product_group
     */
    public function setProductGroup($product_group)
    {
        $this->product_group = $product_group;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'attribute_sap';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach($dataArray as $key => $val) {
            if(property_exists($this, $key)) {
                $this->{$key} = $val;
                // input is send but value is empty, we still need to update that fields
                if($val === 0 || $val == "") {
                    $this->whiteList[] = $key;
                }
            }

            if($key == "department_bu") {
                if(!empty($val)){
                    $departmentBu = new \Models\DepartmentBu();
                    $department_bu = $departmentBu::findFirst(' id_department_bu = '.$val);
                    if($department_bu){
                        $this->department_bu = $department_bu->toArray()['name'];
                    }else{
                        $this->department_bu = '';
                    }
                }else{
                    $this->department_bu = '';
                }

            }
            if($key == "id_department_bu") {
                $this->department_bu = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public function buildUpdateParam()
    {
        $thisArray = get_object_vars($this);

        unset($thisArray['attribute_sap_id']);

        $db_params = "";
        // filter object type out of this
        foreach($thisArray as $key => $val) {
            if(gettype($thisArray[$key]) != 'object' && (!empty($val) || $val=='0' || in_array($key,$this->whiteList) ) && $key[0] !== "_" && $key !== "master_app_id") {
                $val = (is_array($val)) ? json_encode($val) : $val;
                $db_params .= $key . " = '" . $val . "', ";
            }
        }

        $db_params = substr($db_params,0,-2);

        return $db_params;

    }

    public function updateBatch($params = array()){
        $date = date('Y-m-d H:i:s');
        try {

            $sql = "
                update attribute_sap set ".$params['stringSet'].",updated_at='".$date."' where sku = '".$params['sku']."'
            ";

            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            if($result){
                return $this->getDi()->getShared('dbMaster')->affectedRows();
            }else{
                return false;
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("Attribute Sap Batch", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'Attribute Sap Batch','message' => 'There have something error, in query batch'));
            return false;
        }
    }

}