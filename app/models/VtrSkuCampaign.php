<?php

namespace Models;

class VtrSkuCampaign extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $sku;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $campaign_name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field sku
     *
     * @param string $sku
     * @return $this
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Method to set the value of field campaign_name
     *
     * @param string $campaign_name
     * @return $this
     */
    public function setCampaignName($campaign_name)
    {
        $this->campaign_name = $campaign_name;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Returns the value of field campaign_name
     *
     * @return string
     */
    public function getCampaignName()
    {
        return $this->campaign_name;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'vtr_sku_campaign';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VtrSkuCampaign[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VtrSkuCampaign
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
