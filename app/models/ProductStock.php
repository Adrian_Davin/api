<?php

namespace Models;

//use Phalcon\Db as Database;

class ProductStock
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $stock_id;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected $store_code;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $sku;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $supplier_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $qty;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $backorder_allowed;

    protected $error;

    protected $msg;

    /**
     * @return mixed
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @param mixed $msg
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->belongsTo('store_code', 'Models\Store', 'store_code', array('alias' => 'Store', "reusable" => true));
        //$this->belongsTo('sku', 'Models\ProductVariant', 'sku', array('alias' => 'ProductVariant', "reusable" => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        //return 'product_stock';
        return '';
    }

    /**
     * @return int
     */
    public function getStockId()
    {
        return $this->stock_id;
    }

    /**
     * @param int $stock_id
     */
    public function setStockId($stock_id)
    {
        $this->stock_id = $stock_id;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->store_code;
    }

    /**
     * @param string $store_code
     */
    public function setStoreCode($store_code)
    {
        $this->store_code = $store_code;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param int $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    /**
     * @return int
     */
    public function getBackorderAllowed()
    {
        return $this->backorder_allowed;
    }

    /**
     * @param int $backorder_allowed
     */
    public function setBackorderAllowed($backorder_allowed)
    {
        $this->backorder_allowed = $backorder_allowed;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductStock[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductStock
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

//    public function setFromArray($data_array = array())
//    {
//        $this->assign($data_array);
//
//        // get not send data but have in this parameter
//        $thisArray = get_class_vars(get_class($this));
//        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray, $data_array));
//    }

//    public function updateStock()
//    {
//        // Find selected qty
//        $selectQty = $this->findFirst(
//            [
//                "conditions" => "sku ='" . $this->sku . "' AND store_code='" . $this->store_code . "'",
//            ]
//        );
//
//        // Check if the field exist or not
//        if (!empty($selectQty)) {
//            // If exists, update data to database
//            $selectQty->qty = $this->qty;
//            $selectQty->save();
//            $this->setMsg("Success update stock SKU[".$this->sku."] with store_code = ".$this->store_code);
//        } else {
//            // If not, send error response
//            $this->setError(array('code' => 'RR001','title' => 'productStock','message' => 'Parameter store_code does not exist = '.$this->store_code));
//        }
//    }

    /**
     * End Here
     */

    public function getProductStock($company_code = 'ODI', $store_code = null, $on_hand_only = null, $device = "", $business_unit = "", $is_product_scanned = null, $source = null)
    {
        if (empty($this->sku)) {
            return array();
        }

        $cartItem = new CartItem();
        $cartItem->setSku($this->sku);
        $cartItem->setQtyOrdered(1);
        $returnStores = $cartItem->getStoreWithStock($company_code, $store_code, $on_hand_only, $business_unit, $source);

        // Compare with attribute fulfillment_center from Elastic : https://ruparupa.atlassian.net/browse/RRTIC-76
        
        $productHelper = new \Helpers\ProductHelper();
        $finalReturnStores = [];
        $fulfillmentCenter = "";
        $fulfillmentStart = "";
        $fulfillmentEnd = "";
        $now = date("Y-m-d H:i:s");

        $attributesData = $productHelper->getAttributeFromElastic($this->sku);
        if(!empty($attributesData)){
            foreach($attributesData as $rowAttribute){
                if($rowAttribute['attribute_name'] == "fulfillment_center"){
                    if($rowAttribute["attribute_value"] != ""){
                        $fulfillmentCenter = explode(";",$rowAttribute["attribute_value"]);
                        foreach($fulfillmentCenter as $indexFulfill => $rowFulfillment){
                            $fulfillmentCenter[$indexFulfill] = trim($rowFulfillment);
                        }
                    }
                    
                }
                if($rowAttribute['attribute_name'] == "fulfillment_start_date"){
                    $fulfillmentStart = $rowAttribute["attribute_value"];
                }
                if($rowAttribute['attribute_name'] == "fulfillment_end_date"){
                    $fulfillmentEnd = $rowAttribute["attribute_value"];
                }
            }
        }


        $finalReturnStores = $returnStores;
        if ($on_hand_only != 1 || $is_product_scanned != 1){
            if($now > $fulfillmentStart and $now < $fulfillmentEnd){
                if(!empty($fulfillmentCenter)){
                    $finalReturnStores = [];
                    foreach($returnStores as $pickupPoint => $valStore){
                        foreach($valStore as $storeDetail){
                            if(in_array($storeDetail['store_code'],$fulfillmentCenter)){
                                $finalReturnStores[$pickupPoint] = $valStore;
                            }
                        }
                    }
                }
            }
        }

        if(!empty($finalReturnStores)){
            $productStock = array(
                'sku' => $this->sku
            );
            $totalQty = 0;
            foreach($finalReturnStores as $pickupPoint => $valStore){
                $flagStore = array();
                foreach($valStore as $storeDetail){
                    if ($storeDetail['pickup_center'] == 0 && substr($storeDetail['store_code'], 0, 1) != 'M' && substr($storeDetail['store_code'], 0, 1) != 'D') {
                        continue;
                    }

                    $pickupName = !empty($storeDetail['pickup_data']['pickup_name']) ? $storeDetail['pickup_data']['pickup_name'] : "Warehouse DC";
                    $supplierAlias = !empty($storeDetail['supplier_alias']) ? $storeDetail['supplier_alias'] : "";

                    // Skip Ace, Toys Kingdom, Selma for informa apps
                    if ($device == "informa-android" || $device == "informa-ios" || $device == "informa-huawei") {
                        if ($supplierAlias == "AHI" || $supplierAlias == "TGI") {
                            continue;
                        } else if (strpos(strtolower($pickupName), 'selma') !== false) {
                            continue;
                        }
                    } else if ($device == "selma-android" || $device == "selma-ios" || $device == "selma-huawei") {
                        if (strpos(strtolower($pickupName), 'selma') !== true) {
                            continue;
                        }
                    }

                    $storeDetail['pickup_data']['city_id'] = $storeDetail['city']['city_id'];

                    $flagStore = array(
                        'store' => array(
                            'name' => !empty($storeDetail['name']) ? $storeDetail['name'] : "Warehouse DC" ,
                            'store_code' => $storeDetail['store_code']
                        ),
                        'qty' => $storeDetail['qty'],
                        'pickup_code' => $pickupPoint,
                        'pickup_data' => $storeDetail['pickup_data'],
                        'fulfillment_center' => $storeDetail['fulfillment_center'],
                        'zone_id' => $storeDetail['zone_id']
                    );

                    if ($storeDetail['qty'] > 0) {
                        $totalQty = $totalQty + $storeDetail['qty'];
                    }

                    $productStock['location'][] = $flagStore;
                }
            }
            $productStock['global_stock_qty'] = $totalQty < 0 ? 0 : $totalQty;

            return $productStock;
        }else {
            //\Helpers\LogHelper::log("product_stock", "Error when getting result list");
            return array();
        }

//
//        if ($result->count() > 0) {
//            $productStock = array();
//            $totalQty = 0;
//            foreach ($result as $key => $val) {
//                $productStock['sku'] = $val->getSku();
//                //$productStock['name'] = $val->ProductVariant->Product->getName();
//                //$productStock['is_in_stock'] = $val->ProductVariant->getIsInStock();
//                $productStock['name'] = '';
//                $productStock['is_in_stock'] = 1;
//                $productStock['location'][] = array(
//                    "stock_id" => $val->getStockId(),
//                    "store" => !empty($val->Store) ? $val->Store->toArray(array('name', 'store_code')) : "",
//                    "qty" => $val->getQty(),
//                    "pickup_code" => empty($val->Store->PickupPoint) ? "" : $val->Store->PickupPoint->getPickupCode(),
//                    "backorder_allowed" => $val->getBackorderAllowed()
//                );
//
//                if ($val->getQty() > 0) {
//                    $totalQty = $totalQty + $val->getQty();
//                }
//            }
//
//            $productStock['global_stock_qty'] = $totalQty < 0 ? 0 : $totalQty;
//
//            return $productStock;
//        } else {
//            \Helpers\LogHelper::log("product_stock", "Error when getting result list");
//            return array();
//        }
    }

    /**
     * Function that used to update a batch of stock data
     *
     * @param array $params Parameter from api call MUST contains (store_code, sku, qty)
     * @return array Returns an array containing the response or an exception
     */
    public function updateBatchProductStock($params = array())
    {

        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            if (count($params) > 0) {
                $dcStoreList = explode(',', $_ENV['DC_STORE']);
                $wbProductList = explode(',', $_ENV['SKU_WB']);
                $medanProductList = explode(',', $_ENV['SKU_RESTORE_DC_MEDAN']);
                $makassarProductList = explode(',', $_ENV['SKU_RESTORE_DC_MAKASSAR']);

                $siscoSupplier = array(
                    'K001' => 63,
                    'L001' => 65,
                    'S001' => 64,
                    'H014' => 60,
                    'A001' => 59,
                    'T001' => 61,
                    'V001' => 161
                );

                $chunks = array_chunk($params, 3000);
                $affectedRows = $chunksIndex = 0;
                $chunkError = '';

                $now = date("Y-m-d H:i:s");
                $filterBatchPreOrder = " AND sku NOT IN (SELECT sku FROM product_preorder WHERE start_date<= '".$now."' AND end_date >='".$now."' ) ";
                $filterBatchDcZero = '';
                foreach ($chunks as $rowArray) {
                    $chunksIndex++;
                    $sqlUpdateStock = 'UPDATE product_stock SET qty = CASE';
                    $arr_sku = array();
                    foreach ($rowArray as $key => $row) {
                        $flagIgnore = 0;

                        if (in_array($row['store_code'], $dcStoreList)) {
                            // verify sku supplier id is match with store code,
                            // if not match, DONT merge the data row into query string
                            $supplierIdArray = $this->getSupplierIdBySKU(array('sku' => $row['sku']));
                            if (!in_array($siscoSupplier[$row['store_code']], $supplierIdArray)) {
                                $flagIgnore = 1;
                            }

                            $row['store_code'] = 'DC';
                            $filterBatchDcZero = ' and sku not in ( select sku from batch_dc_zero where now() between from_date and to_date ) ';
                        }

                        // Interupt the qty calculation if Store Code O001 or H001
                        if ($row['store_code'] == 'O001') {
                            $paramsGetQty['sku'] = $row['sku'];
                            $paramsGetQty['store_code'] = $row['store_code'] = 'DC';

                            // interupt for promo local
                            $paramsGetEvent['sku'] = $row['sku'];
                            $currentEvent = $this->getCurrentEvent($paramsGetEvent);
                            if ($currentEvent <> '') {
                                if ($currentEvent) {
                                    if ($row['qty'] > 0) {
                                        $row['qty'] = $row['qty'];
                                    } else {
                                        // update status event jadi 0
                                        $paramsSetEvent['sku'] = $row['sku'];
                                        $paramsSetEvent['status_event'] = 0;
                                        $this->updateEventPromoLocal($paramsSetEvent);

                                        $row['qty'] = $this->getCurrentQty($paramsGetQty) + $row['qty'];
                                    }
                                } else {
                                    $row['qty'] = $this->getCurrentQty($paramsGetQty) + $row['qty'];
                                }
                            } else {
                                $row['qty'] = $this->getCurrentQty($paramsGetQty) + $row['qty'];
                            }

                            $filterBatchDcZero = ' and sku not in ( select sku from batch_dc_zero where now() between from_date and to_date ) ';
                        } elseif ($row['store_code'] == 'H001') { // DC Cikupa

                            if(in_array($row['sku'],$wbProductList)){
                                //Set into store code Karawaci
                                $paramsGetQty['store_code'] = $row['store_code'] = 'J445';
                            }else{
                                //Set into store code Living World
                                $paramsGetQty['store_code'] = $row['store_code'] = 'J347';
                            }
                            $paramsGetQty['sku'] = $row['sku'];
                            $currentQty = $this->getCurrentQty($paramsGetQty);

                            $row['qty'] = $currentQty + $row['qty'];
                        }/*elseif ($row['store_code'] == 'H007') { // DC Medan

                            $paramsGetQty['store_code'] = $row['store_code'] = 'J338';
                            if(in_array($row['sku'],$medanProductList)){

                                //Set into store code Juanda
                                $paramsGetQty['sku'] = $row['sku'];
                                $currentQty = $this->getCurrentQty($paramsGetQty);

                                $row['qty'] = $currentQty + $row['qty'];

                            }else{
                                $flagIgnore = 1;
                            }

                        }elseif ($row['store_code'] == 'H019') { // DC Makassar

                            $paramsGetQty['store_code'] = $row['store_code'] = 'J343';
                            if(in_array($row['sku'],$makassarProductList)){
                                //Set into store code Pettarani
                                $paramsGetQty['sku'] = $row['sku'];
                                $currentQty = $this->getCurrentQty($paramsGetQty);
                                $row['qty'] = $currentQty + $row['qty'];
                            }else{
                                $flagIgnore = 1;
                            }
                        }*/

                        if ($row['sku'] <> '' && $row['qty'] <> '' && !$flagIgnore) {

                            $sqlUpdateStock .= " WHEN (store_code = '{$row['store_code']}' AND sku = '{$row['sku']}') THEN {$row['qty']}";
                            $arr_sku[] = "'{$row['sku']}'";
                        }

                        // dont' sign this variable as an array, it will make ur query result messed
                        // if you have dynamic store code please use function updateBatchProductStockMultipleStore instead
                        $strStoreCode = "'{$row['store_code']}'";
                    }

                    $strSku = implode(',',$arr_sku);

                    $sqlUpdateStock .= " END WHERE store_code in ($strStoreCode) AND sku in ($strSku) ".$filterBatchDcZero . $filterBatchPreOrder;

                    $result = $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
                    if ($result) {
                        $response['messages'] = array('success');
                        $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();

                        /**
                         * Keep Stock from SAP
                         */
                        $sqlStockSAP = "
                            update product_stock set qty_bk = qty where store_code in (".$strStoreCode.") AND sku in (".$strSku.")
                        ";
                        $this->getDi()->getShared('dbMaster')->execute($sqlStockSAP);

                    } else {
                        $chunkError .= "chunk $chunksIndex,";
                    }
                }

                if ($chunkError <> '') {
                    $chunkError = rtrim($chunkError, ',');
                    $response['errors'] = array("Update batch stock failed in chunk $chunkError, please contact ruparupa tech support");
                }

                if ($response['messages'][0] == 'success') {
                    $response['data']['affected_row'] = $affectedRows;
                }

            } else {
                $response['errors'] = array('Update batch stock failed, parameter data is empty');
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Update batch stock failed, please contact ruparupa tech support'. $e->getMessage());
        }

        return $response;
    }

    public function updateBatchProductStockPromo1111($params = array())
    {

        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            if (count($params) > 0) {
                $chunks = array_chunk($params, 3000);
                $affectedRows = $chunksIndex = 0;
                $chunkError = '';
                foreach ($chunks as $rowArray) {
                    $chunksIndex++;
                    $sqlUpdateStock = 'UPDATE product_stock SET qty = CASE';
                    $arr_sku = array();
                    foreach ($rowArray as $row) {
                        $paramsGetQty['sku'] = $row['sku'];
                        $paramsGetQty['store_code'] = $row['store_code'];
                        $row['qty'] = $this->getCurrentQty($paramsGetQty) + $row['qty'];

                        $sqlUpdateStock .= " WHEN (store_code = '{$row['store_code']}' AND sku = '{$row['sku']}') THEN {$row['qty']}";
                        $arr_sku[] = "'{$row['sku']}'";

                        $strStoreCode = "'{$row['store_code']}'";
                    }

                    $strSku = implode(',',$arr_sku);

                    $sqlUpdateStock .= " END WHERE store_code in ($strStoreCode) AND sku in ($strSku)";

                    $result = $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
                    if ($result) {
                        $response['messages'] = array('success');
                        $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();
                    } else {
                        $chunkError .= "chunk $chunksIndex,";
                    }
                }

                if ($chunkError <> '') {
                    $chunkError = rtrim($chunkError, ',');
                    $response['errors'] = array("Update batch stock failed in chunk $chunkError, please contact ruparupa tech support");
                }

                if ($response['messages'][0] == 'success') {
                    $response['data']['affected_row'] = $affectedRows;
                }
            } else {
                $response['errors'] = array('Update batch stock failed, parameter data is empty');
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Update batch stock failed, please contact ruparupa tech support'. $e->getMessage());
        }

        return $response;
    }

    /**
     * Function that used to update a batch of stock data for multiple store and sku
     *
     * @param array $params Parameter from api call MUST contains (store_code, sku, qty)
     * @return array Returns an array containing the response or an exception
     */
    public function updateBatchProductStockMultipleStore($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $affectedRows = 0;
            if (count($params) > 0) {
                foreach ($params as $row) {
                    $storeCodeArray[$row['store_code']][]['sku'] = $row['sku'];
                    // get last index
                    end($storeCodeArray[$row['store_code']]);
                    $index = key($storeCodeArray[$row['store_code']]);

                    $storeCodeArray[$row['store_code']][$index]['qty'] = $row['qty'];
                }

                // update batch per store code
                foreach ($storeCodeArray as $key => $contentArray) {
                    // reset the variable value
                    $sqlUpdateStock = 'UPDATE product_stock SET qty = CASE';
                    $arr_sku = array();
                    foreach ($contentArray as $index => $row) {
                        $sqlUpdateStock .= " WHEN (store_code = '$key' AND sku = '{$row['sku']}') THEN {$row['qty']}";
                        $strStoreCode = "'" . $key . "'";
                        $arr_sku[] = "'{$row['sku']}'";
                    }

                    $strSku = implode(',',$arr_sku);
                    $sqlUpdateStock .= " END WHERE store_code in ($strStoreCode) AND sku in ($strSku);";
                    $result = $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
                    if ($result) {
                        $response['messages'] = array('success');
                        $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();
                    }
                }
            }

            $response['messages'] = array('success');
            $response['data']['affected_row'] = $affectedRows;
        } catch (\Exception $e) {
            $response['errors'] = array('Update batch reserved stock pending shipment failed, please contact ruparupa tech support');
        }

        return $response;
    }

    /**
     * Function that used to update a batch of stock data by stock_id
     *
     * @param array $params Parameter from api call MUST contains (stock_id, qty, batch)
     * @return array Returns an array containing the response or an exception
     */
    public function updateBatchProductStockByStockId($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            if (count($params) > 0) {
                $sqlUpdateStock = 'UPDATE product_stock SET qty = CASE';
                $arrStockId = array();

                foreach ($params as $row) {
                    $sqlUpdateStock .= " WHEN stock_id = {$row['stock_id']} THEN {$row['qty']}";

                    $arrStockId[] = "{$row['stock_id']}";
                }

                $strStockId = implode(',',$arrStockId);

                $sqlUpdateStock .= " END WHERE stock_id in ($strStockId) ";
                $result = $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
                if ($result) {
                    $response['messages'] = array('Success updating stock');
                    $response['data']['affected_row'] = $this->getDi()->getShared('dbMaster')->affectedRows();
                } else {
                    $response['errors'] = array("Update batch stock failed");
                }
            } else {
                $response['errors'] = array('Update batch stock failed, parameter data is empty');
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Update batch stock failed');
        }

        return $response;
    }

    /**
     * Function to update qty to zero for certain DC sku
     *
     * @param array $params Parameter from api call contains (sku)
     * @return array Returns an array containing the response or an exception
     */
    public function updateStockDcZero($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            // remove in-active SKU
            $sql = "DELETE FROM batch_dc_zero WHERE (CURDATE() NOT BETWEEN from_date and to_date)";
            $this->getDi()->getShared('dbMaster')->execute($sql);

            $sql = "UPDATE product_stock set qty = 0 WHERE store_code = 'DC' AND sku IN ({$params['sku']})";
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            if ($result) {
                $response['messages'] = array('success');
                $response['data']['affected_row'] = $this->getDi()->getShared('dbMaster')->affectedRows();
            } else {
                $response['errors'] = array('Update stock DC failed, please contact ruparupa tech support');
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Update stock DC failed, please contact ruparupa tech support');
        }

        return $response;
    }

    /**
     * Function to update store qty to zero for certain sku in table
     *
     * @param array $params Parameter from api call contains (sku)
     * @return array Returns an array containing the response or an exception
     */
    public function allStoreStockZero($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            // remove in-active SKU
            $sql = "DELETE FROM batch_store_zero WHERE (CURDATE() NOT BETWEEN from_date and to_date)";
            $this->getDi()->getShared('dbMaster')->execute($sql);

            $sql = "UPDATE product_stock set qty = 0 WHERE store_code <> 'DC' AND sku IN ({$params['sku']})";
            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            if ($result) {
                $response['messages'] = array('success');
                $response['data']['affected_row'] = $this->getDi()->getShared('dbMaster')->affectedRows();
            } else {
                $response['errors'] = array('Update stock Store failed, please contact ruparupa tech support');
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Update stock Store failed, please contact ruparupa tech support');
        }

        return $response;
    }

    /**
     * Get sku list from table batch_dc_zero
     * No params needed
     * @return array Returns an array containing the response or an exception
     */
    public function getSkuDcBatch($params = array())
    {
        $sql = "SELECT
                    DISTINCT (sku)
                FROM
                    batch_dc_zero
                WHERE
                    (CURDATE() BETWEEN from_date and to_date)";
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        $response['data'] = array();
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $response['data'] = $result->fetchAll();
        }

        return $response;
    }

    /**
     * Get sku list from table batch_store_zero
     * No params needed
     * @return array Returns an array containing the response or an exception
     */
    public function getSkuStoreBatch($params = array())
    {
        $sql = "SELECT
                    DISTINCT (sku)
                FROM
                    batch_store_zero
                WHERE
                    (CURDATE() BETWEEN from_date and to_date)";
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        $response['data'] = array();
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $response['data'] = $result->fetchAll();
        }

        return $response;
    }

    /**
     * Function to update stock to 0 by condition of the price and qty
     * @$params MUST contains : price_operator (<, >=), price, qty_operator (<), qty
     * @return array Returns an array containing the response or an exception
     */
    public function bufferByPriceAndQty($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {

            /**
             * ToDo : Harus di refactoring, ketika stock mongo sudah naik, dengan logic supplier_id in (59,60,61,62,63,64,65,66)
             */
            $sql = "
                    SELECT 
                        sku 
                    FROM product_price 
                    WHERE
                        price {$params['price_operator']} {$params['price']} AND
                        sku NOT IN (
                          SELECT sku FROM temp_stock_no_buffer WHERE NOW() BETWEEN start_date AND end_date
                        ) AND 
                        (
                            ( sku NOT LIKE 'M%' and sku NOT LIKE 'MP%') OR ( sku LIKE 'MW%' OR sku LIKE 'MB%' OR sku LIKE 'MT%' )
                        );
                    ";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $skuList = $result->fetchAll();

                $newSkuList = array();
                foreach ($skuList as $row) {
                    $newSkuList[] = "'{$row['sku']}'";
                }

                $chunks = array_chunk($newSkuList, 3000);
                $affectedRows = 0;
                foreach ($chunks as $rowArray) {
                    $skuIn = implode(',',$rowArray);

                    $sql = "UPDATE product_stock SET qty = {$params['qty_process']}
                    WHERE sku IN ($skuIn) AND qty {$params['qty_operator']} {$params['qty']} AND store_code not in ('DC','H331')";

                    $result = $this->getDi()->getShared('dbMaster')->execute($sql);
                    if ($result) {
                        $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();
                    }
                }

                $response['messages'] = array('success');
                $response['data']['affected_row'] = $affectedRows;
            } else {
                $response['errors'] = array('No data found');
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Update batch buffer stock failed, please contact ruparupa tech support');
        }

        return $response;
    }

    /**
     * Function to get the current qty based on sku and store code
     * @param array $params Can contain store_code, sku
     * @return int Return the Qty value
     */
    private function getCurrentQty($params = array())
    {
        $currentQty = 0;
        if (count($params) > 0) {
            $sql = "SELECT COALESCE(qty, 0) as qty FROM product_stock where store_code = '{$params['store_code']}' and sku = '{$params['sku']}' limit 1";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $data = $result->fetchAll();
                $currentQty = $data[0]['qty'];
            }
        }

        return $currentQty;
    }


    private function getCurrentEvent($params = array())
    {
        $currentEvent = '';
        if (count($params) > 0) {
            $sql = "SELECT status_event FROM promo_local where sku = '{$params['sku']}' limit 1";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $data = $result->fetchAll();
                $currentEvent = $data[0]['status_event'];
            }
        }

        return $currentEvent;
    }

    private function getSupplierIdBySKU($params = array())
    {

        $dataSupplier = \Helpers\ProductHelper::getSupplierFromElastic($params);
        $returnSupplier['supplier_id'] = $dataSupplier['supplier_id'];

        return $returnSupplier;
    }

    private function updateEventPromoLocal($params = array())
    {
        try {
            $sql = "UPDATE promo_local set status_event = {$params['status_event']} WHERE sku = '{$params['sku']}'";

            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            $affectedRows = 0;
            if ($result) {
                $affectedRows = $this->getDi()->getShared('dbMaster')->affectedRows();
            }
        } catch (\Exception $e) {
            $affectedRows = 0;
        }

        return $affectedRows;
    }

    /**
     * Function to update reserved stock [multiple store and sku]
     *
     * @param array $params
     * @return array
     */
    public function updateBatchReservedStock($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $i = 0;
            if (count($params) > 0) {
                foreach ($params as $row) {
                    /*$storeCodeArray[$row['store_code']][]['sku'] = $row['sku'];
                    // get last index
                    end($storeCodeArray[$row['store_code']]);
                    $index = key($storeCodeArray[$row['store_code']]);*/

                    $paramsGetQty['sku'] = $row['sku'];
                    $paramsGetQty['store_code'] = $row['store_code'];
                    //$storeCodeArray[$row['store_code']][$index]['qty'] = $this->getCurrentQty($paramsGetQty) - $row['qty_ordered'];

                    $qtyTemp = $this->getCurrentQty($paramsGetQty) - $row['qty_ordered'];

                    $sqlUpdateStock = " UPDATE product_stock SET qty = ".$qtyTemp." where sku = '".$row['sku']."' and store_code = '".$row['store_code']."' ";
                    $result = $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
                    if ($result) {
                        $i++;
                    }
                }


                // update batch per store code
                /*foreach ($storeCodeArray as $key => $contentArray) {
                    // reset the variable value
                    $sqlUpdateStock = 'UPDATE product_stock SET qty = CASE';
                    $arr_sku = array();
                    foreach ($contentArray as $index => $row) {
                        $sqlUpdateStock .= " WHEN (store_code = '$key' AND sku = '{$row['sku']}') THEN {$row['qty']}";
                        $strStoreCode = "'" . $key . "'";
                        $arr_sku[] = "'{$row['sku']}'";
                    }

                    $strSku = implode(',',$arr_sku);
                    $sqlUpdateStock .= " END WHERE store_code in ($strStoreCode) AND sku in ($strSku);";
                    $result = $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
                    if ($result) {
                        $response['messages'] = array('success');
                        $affectedRows += $this->getDi()->getShared('dbMaster')->affectedRows();
                    }
                }*/
            }

            $response['messages'] = array('success');
            $response['data']['affected_row'] = $i;
        } catch (\Exception $e) {
            $response['errors'] = array('Update batch reserved stock pending shipment failed, please contact ruparupa tech support');
        }

        return $response;
    }

    /**
     * Function that return data about the item that it's payment is pending
     *
     * @return array Data item in array
     */
    public function getCurrentPendingPaymentQty()
    {
        $data = array();
        $sql = "
            SELECT
                sku,
                qty_ordered,
                store_code
            FROM
                sales_order_item
            WHERE
                sales_order_id IN (
                    SELECT DISTINCT
                        (sales_order_id)
                    FROM
                        sales_order_payment
                    WHERE
                        STATUS in ('pending', 'pending_payment')
                ) AND product_source NOT IN ('Marketplace') ORDER BY store_code";
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $data = $result->fetchAll();
        }

        return $data;
    }

    /**
     * Function that return data about the item that it's shipment is pending
     *
     * @return array Data item in array
     */
    public function getCurrentPendingShipmentQty()
    {
        $data = array();
        $sql = "
        SELECT
            sku,
            qty_ordered,
            store_code
        FROM
            sales_order_item
        WHERE
            sales_order_id IN (
                SELECT DISTINCT
                    (sales_order_id)
                FROM
                    sales_invoice
                WHERE
                    invoice_id IN (
                        SELECT
                            invoice_id
                        FROM
                            sales_shipment
                        WHERE
                            shipment_status in ('new','ready_to_pickup') AND
                            invoice_id in (SELECT invoice_id FROM sales_invoice where invoice_status in ('stand_by','ready_to_pickup','new'))
                    )
            ) AND product_source NOT IN ('Marketplace') ORDER BY store_code";

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if ($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $data = $result->fetchAll();
        }

        return $data;
    }

    public function importStock($param){

        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );
        try {
            $sqlImportStock = "
                    insert into product_stock(sku, store_code)
                    select
                      '".$param['sku']."' as sku,
                      store_code
                    from store where store_code in (".$param['store_code'].")  
            ";

            $result = $this->getDi()->getShared('dbMaster')->execute($sqlImportStock);
            if ($result) {
                $response['messages'] = array('success');
                $response['data']['affected_row'] = $this->getDi()->getShared('dbMaster')->affectedRows();
            } else {
                $response['errors'] = array('Import Product Stock Failed, please contact ruparupa tech support');
            }

        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("product_stock", json_encode($e->getMessage()));
            $response['errors'] = array('code' => 'RR303','title' => 'productVariant','message' => 'There have something error in update data');

        }

        return $response;
    }

    public function updateQtyBk($params)
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {

            if(!empty($params['sku'])){
                $arrSku = "'".implode("','",$params['sku'])."'";

                $sqlUpdateStock = " update product_stock set qty_bk = qty where sku in (".$arrSku.")";
                $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
                $response['messages'] = array('success');
                $response['data']['affected_row'] = $this->getDi()->getShared('dbMaster')->affectedRows();
            }else{
                $response['errors'] = array('Update stock quantity backup failed, no any sku to update');
            }

        } catch (\Exception $e) {
            $response['errors'] = array('Update batch reserved stock pending shipment failed, please contact ruparupa tech support');
        }

        return $response;
    }

    public function updateStockManual()
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {

            /**
             * update buffer stock, discount more than 70%
             */
//            $sqlUpdateBuffer = "
//                    update product_stock a
//                    inner join (
//                        select
//                            sku
//                        from product_price b where ((price - special_price) / price) >= 0.7 and ( now() between special_from_date and special_to_date ) and
//                        sku not in (
//                          select sku from temp_stock_no_buffer where now() between start_date and end_date
//                        ) AND
//                        (
//                          ( sku NOT LIKE 'M%' and sku NOT LIKE 'MP%') OR ( sku LIKE 'MW%' OR sku LIKE 'MB%' OR sku LIKE 'MT%' )
//                        )
//                        and price > 0 and price > special_price
//                    ) c on c.sku = a.sku
//                    set qty = qty - 2
//                    where store_code <> 'DC'
//
//";
//            $this->getDi()->getShared('dbMaster')->execute($sqlUpdateBuffer);

            /*
             * Clean and Care, Set always available
             */
            $sqlUpdateStock = " update product_stock set qty = 999, qty_bk = 999 where sku in ('70000788','70000789','70000790','70000791','70000792','70000826','70000827','70000828','70000829','70000830','70000831','70000832','70002374','70002375','70002611','70002612','70002613','70002615') and store_code = 'J347' ";
            $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);

            /**
             * INFORMA OLYMPUS
             */
            if(date('Y-m-d') < '2018-04-01 00:00:00'){
                $sqlUpdateStock = "                                       
                    update product_stock set qty = 5 where sku in ('HCIOLY12')
                    and store_code in (
                      select store_code from store where supplier_id = 60 and store_code not in ('H331','J357','H979')
                    );
                
                ";
                $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);

            }else{
                $sqlUpdateStock = "                                       
                    update product_stock set qty = 0 where sku in ('HCIOLY12')
                    and store_code in (
                      select store_code from store where supplier_id = 60 
                    );
                
                ";
                $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);
            }

            /**
             * set 0, minus stock
             */
            $sqlUpdateStock = " update product_stock set qty = 0 where qty < 0 ";
            $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);

            /**
             * set is_in_stock, product voucher ACE, INFORMA, TOYS
             */
//            $sqlUpdateStock = " update product_variant set is_in_stock = 1 where product_id in (
//                                    select product_id from product where supplier_id in (39,40,42)
//                                ) ";
//            $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);

            /**
             * set GWPHUROM
             */
//            $sqlUpdateStock = "
//                                update product_stock a
//                                inner join
//                                (
//                                    select
//                                        sum(qty) as qty,
//                                        store_code
//                                    from product_stock
//                                    where sku in ('10136581','10136582','10136583')
//                                    group by store_code
//                                ) b on a.store_code = b.store_code
//                                set a.qty = b.qty
//                                where a.sku = 'GWPHUROM' ";
//            $this->getDi()->getShared('dbMaster')->execute($sqlUpdateStock);

        } catch (\Exception $e) {
            $response['errors'] = array('Update batch reserved stock pending shipment failed, please contact ruparupa tech support');
        }

        return $response;
    }

    public function changStoreCode($params = array()){
        $date = date('Y-m-d H:i:s');

        if (empty($params)) {
            return false;
        }

        foreach ($params as $key => $val){
            // 1.) collect store code before updating
            $salesOrderItemModel = new SalesOrderItem();
            $salesOrderRes = $salesOrderItemModel->find([
                "columns" => "sku, qty_ordered, store_code, pickup_code",
                "conditions" => "sales_order_id = ".$val['sales_order_id']." AND sku = '".$val['sku']."'"
            ]);

            if (!empty($salesOrderRes->toArray())) {
                // after that, we need to update stock in product_stock
                $result = $salesOrderRes->toArray();

                foreach ($result as $item) {
                    $sql  = "UPDATE product_stock SET qty = (qty + ".$item['qty_ordered'].")";
                    $sql .= " WHERE sku = '".$item['sku']."' AND store_code = '".$item['store_code']."'";
                    $this->useWriteConnection();

                    try {
                        $querySuccess = $this->getDi()->getShared($this->getConnection())->query($sql);

                        // success update previous store code stock
                        // 2.) update store code and pickup code from param
                        $sql  = "UPDATE sales_order_item SET store_code = '".$val['store_code']."'";
                        $sql .= ",pickup_code = '".$val['pickup_code']."', updated_at= '".$date."'";
                        $sql .= " WHERE sku = '".$val['sku']."' AND sales_order_id = '".$val['sales_order_id']."'";
                        $this->useWriteConnection();

                        try {
                            $queryUpdateStoreCode = $this->getDi()->getShared($this->getConnection())->query($sql);

                            // 3.) update stock and last stock sales_stock_assignation with new store code
                            $productStockModel = new ProductStock();
                            $productStockResult = $productStockModel->findFirst(
                                [
                                    "columns" => "qty",
                                    "conditions" => "store_code = '".$val['store_code']."' AND sku = '".$val['sku']."'"
                                ]
                            );

                            if ($productStockResult) {
                                $productStockDest = $productStockResult->toArray();

                                $qtyDest = $productStockDest['qty'];

                                $sql  = "UPDATE sales_stock_assignation SET store_code = '".$val['store_code']."'";
                                $sql .= ", stock = ".$qtyDest.", last_stock = (".$qtyDest." - qty_ordered), updated_at= '".$date."'";
                                $sql .= " WHERE sku = '".$val['sku']."' AND sales_order_id = '".$val['sales_order_id']."'";
                                $this->useWriteConnection();

                                try {
                                    $queryUpdateAssignation = $this->getDi()->getShared($this->getConnection())->query($sql);

                                    // 4.) Last thing to do, update stock for dest store code - qtyOrdered
                                    $sql  = "UPDATE product_stock SET qty = (qty - ".$item['qty_ordered'].")";
                                    $sql .= " WHERE sku = '".$val['sku']."' AND store_code = '".$val['store_code']."'";
                                    $this->useWriteConnection();

                                    try {
                                        $queryUpdateDest = $this->getDi()->getShared($this->getConnection())->query($sql);

                                        return true;
                                    } catch (\Exception $e) {
                                        $this->errorCode = 'RR303';
                                        $this->errorMessages = 'Failed when updating destination store_code product_stock.';
                                        return false;
                                    }

                                } catch (\Exception $e) {
                                    $this->errorCode = 'RR303';
                                    $this->errorMessages = 'Failed when updating new stock and last_stock in stock_assignation.';
                                    return false;
                                }
                            }
                        } catch (\Exception $s) {
                            $this->errorCode = 'RR303';
                            $this->errorMessages = 'Failed when updating store_code or pickup_code in sales_order_item.';
                            return false;
                        }
                    } catch (\Exception $e) {
                        $this->errorCode = 'RR303';
                        $this->errorMessages = 'Failed when updating product_stock.';
                        return false;
                    }
                }

            }
        }
    }

}