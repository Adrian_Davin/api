<?php

namespace Models;

use \Library\Redis\MasterKelurahanVendor as MasterKelurahanVendorRedis;

class MasterKelurahanVendor extends \Models\BaseModel
{

    public $id;
    public $kelurahan_id;
    public $vendor_name;
    public $vendor_kelurahan_id;
    public $created_at;
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasOne('kelurahan_id', 'Models\MasterKelurahan', 'kelurahan_id', array('alias' => 'MasterKelurahan'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_kelurahan_vendor';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterKelurahanVendor[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataList($params = array())
    {
        $listKelurahanVendor = array();
        if(isset($params['reload']) && $params['reload'] == true) {
            MasterKelurahanVendorRedis::deleteListKelurahanVendor();
        } else {
            $listKelurahanVendor = MasterKelurahanVendorRedis::getListKelurahanVendor();
        }

        if(empty($listKelurahanVendor)) {
            $result = $this->find();

            if($result) {
                $listKelurahanVendorRedis = array();
                $listKelurahanVendor = array();
                foreach($result as $kelurahanVendor) {
                    $kelurahanVendorArr = $kelurahanVendor->toArray();
                    $listKelurahanVendorRedis[sprintf("%s:%s", $kelurahanVendorArr["kelurahan_id"], $kelurahanVendorArr["vendor_name"])] = $kelurahanVendorArr;
                    $listKelurahanVendor[] = $kelurahanVendorArr;
                }

                // save to redis
                MasterKelurahanVendorRedis::setListKelurahanVendor($listKelurahanVendorRedis);
            }
        }


        return $listKelurahanVendor;
    }

    public function getData($params = array())
    {
        $kelurahanVendor = array();
        $cacheID = sprintf("%s:%s", $params['kelurahan_id'], $params['vendor_name']);
        if(isset($params['reload']) && $params['reload'] == true) {
            MasterKelurahanVendorRedis::deleteKelurahanVendor($cacheID);
        } else {
            $kelurahanVendor = MasterKelurahanVendorRedis::getKelurahanVendor($cacheID);
        }

        if(empty($kelurahanVendor)) {
            $kelurahanVendorDB = $this->findFirst(sprintf("kelurahan_id = '%s' AND vendor_name = '%s'", $params['kelurahan_id'], $params['vendor_name']));

            if($kelurahanVendorDB) {
                $kelurahanVendor = $kelurahanVendorDB->toArray();
                MasterKelurahanVendorRedis::setKelurahanVendor($cacheID, $kelurahanVendor);
            }
        }

        return $kelurahanVendor;
    }
}
