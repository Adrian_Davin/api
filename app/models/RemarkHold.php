<?php
namespace Models;

class RemarkHold extends \Models\BaseModel
{
    /**
     * @var $sales_order_id
     */
    protected $sales_order_id;

    /**
     * @var $remark
     */
    protected $remark;

    public function onConstruct()
    {
        parent::onConstruct();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'remark_hold';
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * @param int $sales_order_id
     */
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        if(!empty($data_array['sales_order_id'])) {
            $this->sales_order_id = $data_array['sales_order_id'];
        }

        if(!empty($data_array['remark'])) {
            $this->remark = $data_array['remark'];
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }
}