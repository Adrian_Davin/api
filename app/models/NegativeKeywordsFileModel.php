<?php

namespace Models;


class NegativeKeywordsFileModel extends \Models\BaseModel
{
    public $file_id;

    public $file_name;

    public $email;

    protected $errors;

    protected $messages;

    /**
    * @return mixed
    */
    public function getFile_Id()
    {
        return $this->file_id;
    }

    /**
     * @param mixed $file_id
     */
    public function setFile_Id($file_id)
    {
        $this->file_id = $file_id;
    }

    public function getFile_Name()
    {
        return $this->file_name;
    }

    /**
     * @param mixed $file_name
     */
    public function setFile_Status($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
    * @return mixed
    */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->whiteList = array();
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'negative_keywords_file';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {     
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
        
}
