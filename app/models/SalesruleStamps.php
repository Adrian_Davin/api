<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Db as Database;

class SalesruleStamps extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $stamp_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $rule_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $stamp_collected;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $stamp_used;

    /**
     * @var string
     * @Column(type="varchar", length=45, nullable=true)
     */
    protected $ace_customer_id;

    /**
     * @var datetime
     * @Column(type="datetime", nullable=true)
     */
    protected $expiration_date;

    /**
     *
     * @var datetime
     * @Column(type="datetime", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var datetime
     * @Column(type="datetime", nullable=true)
     */
    protected $updated_at;

    /**
     * Create getter and setter
     */
    public function getStampId() {
        return $this->stamp_id;
    }
    
    public function setStampId($stamp_id) {
        $this->stamp_id = $stamp_id;
    }

    public function getRuleId() {
        return $this->rule_id;
    }

    public function setRuleId($rule_id) {
        $this->rule_id = $rule_id;
    }

    public function getStampCollected() {
        return $this->stamp_collected;
    }

    public function setStampCollected($stamp_collected) {
        $this->stamp_collected = $stamp_collected;
    }

    public function getStampUsed() {
        return $this->stamp_used;
    }

    public function setStampUsed($stamp_used) {
        $this->stamp_used = $stamp_used;
    }

    public function getAceCustomerId() {
        return $this->ace_customer_id;
    }

    public function setAceCustomerId($ace_customer_id) {
        $this->ace_customer_id = $ace_customer_id;
    }

    public function getExpirationDate() {
        return $this->expiration_date;
    }

    public function setExpirationDate($expiration_date) {
        $this->expiration_date = $expiration_date;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }

    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
    }

    public function getUpdatedAt() {
        return $this->updated_at;
    }

    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('rule_id', 'Models\Salesrule', 'rule_id', array('alias' => 'Salesrule', 'reusable' => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule_stamps';
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleStamps[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesruleStamps
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
