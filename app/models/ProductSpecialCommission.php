<?php

namespace Models;

use GuzzleHttp\Client;
use Helpers\ProductHelper;
use Phalcon\Db as Database;

class ProductSpecialCommission extends \Models\BaseModel
{
    
    protected $product_special_commission_id;
    protected $store_code;
    protected $sku;
    protected $special_commisison;
    protected $special_from_date;
    protected $special_to_date;
    protected $created_at;
    protected $updated_at;

    public function initialize()
    {
        
    }

    public function onConstruct()
    {
        parent::onConstruct();
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}