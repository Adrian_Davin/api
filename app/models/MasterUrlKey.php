<?php

namespace Models;
use Phalcon\Db as Database;

use Phalcon\Mvc\Model\Query;


class MasterUrlKey extends \Models\BaseModel
{

    const DEFAULT_DOCUMENT_TITLE='Ruparupa.com - ';

    protected $url_key;
    protected $url_key_old;
    protected $section;
    protected $additional_header;
    protected $additional_header_mobile;
    protected $document_title;
    protected $reference_id;
    protected $inline_script;
    protected $inline_script_mobile;
    protected $company_code;
    protected $created_at;

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getInlineScript()
    {
        return $this->inline_script;
    }

    /**
     * @param mixed $inline_script
     */
    public function setInlineScript($inline_script)
    {
        $this->inline_script = $inline_script;
    }
    /**
     * @return mixed
     */
    public function getDocumentTitle()
    {
        return $this->document_title;
    }

    /**
     * @param mixed $document_title
     */
    public function setDocumentTitle($document_title)
    {
        $this->document_title = $document_title;
    }

    /**
     * @return mixed
     */
    public function getReferenceId()
    {
        return $this->reference_id;
    }

    /**
     * @param mixed $reference_id
     */
    public function setReferenceId($reference_id)
    {
        $this->reference_id = $reference_id;
    }

    /**
     * @return mixed
     */
    public function getAdditionalHeader()
    {
        return $this->additional_header;
    }

    /**
     * @param mixed $additional_header
     */
    public function setAdditionalHeader($additional_header)
    {
        $this->additional_header = $additional_header;
    }

    /**
     * @return mixed
     */
    public function getAdditionalHeaderMobile()
    {
        return $this->additional_header_mobile;
    }

    /**
     * @param mixed $additional_header_mobile
     */
    public function setAdditionalHeaderMobile($additional_header_mobile)
    {
        $this->additional_header_mobile = $additional_header_mobile;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptMobile()
    {
        return $this->inline_script_mobile;
    }

    /**
     * @param mixed $inline_script_mobile
     */
    public function setInlineScriptMobile($inline_script_mobile)
    {
        $this->inline_script_mobile = $inline_script_mobile;
    }

    /**
     * @return mixed
     */
    public function getUrlKeyOld()
    {
        return $this->url_key_old;
    }

    /**
     * @param mixed $url_key_old
     */
    public function setUrlKeyOld($url_key_old)
    {
        $this->url_key_old = $url_key_old;
    }

    /**
     * @return mixed
     */
    public function getUrlKey()
    {
        return $this->url_key;
    }

    /**
     * @param mixed $url_key
     */
    public function setUrlKey($url_key)
    {
        $this->url_key = $url_key;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     */
    public function setSection($section)
    {
        $this->section = $section;
    }

        /**
     * Get the value of company_code
     */ 
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * Set the value of company_code
     *
     * @return  self
     */ 
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    public function onConstruct()
    {
        parent::onConstruct();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_url_key';
    }


    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 10080,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }


    public static function findFirst($parameters = null)
    {

       $redisKey = self::_createKey($parameters);
       $cache = [
           "cache" => [
               "key" => $redisKey,
               "lifetime" => 10080,
           ],
       ];
       if(!is_array($parameters)) {
           $oldParam =  $parameters;
           $parameters = array();
           $parameters['conditions'] = $oldParam;
       }
       $parameters = array_merge($parameters,$cache);

        $result = parent::findFirst($parameters);
        if(!$result) {
            $redis = new \Library\Redis();
            $redisCon = $redis::connect();
            $redisCon->delete("_PHCR".$redisKey);

            $result = parent::findFirst($parameters);
        }

        return $result;
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = str_replace('"','\"',$val);
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    /*
     * Override base model function to delete from cache
     */
    public function afterUpdate()
    {
        $redisKey = "reference_id='" . $this->reference_id ."'ANDsection='".str_replace(' ', '', $this->section)."'";
        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);

        // cleanup route
        $redisKey = "url_key,section,additional_header,inline_script,document_title,reference_id,company_code";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);

        /*
         * Also Clean up this url key from redis (if query from url key)
         */
        $redisKey = "url_key='" . $this->url_key."'";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }

    public function afterInsert()
    {
        $redisKey = "reference_id='" . $this->reference_id ."'ANDsection='".str_replace(' ', '', $this->section)."'";
        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);

        // cleanup route
        $redisKey = "url_key,section,additional_header,inline_script,document_title,reference_id,company_code";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);

        /*
         * Also Clean up this url key from redis (if query from url key)
         */
        $redisKey = "url_key='" . $this->url_key."'";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }

    public function saveUrlKey($log_file_name = "masterUrlKey")
    {
        try{
            $data = $this->getDataArray(array('additional_header','additional_header_mobile','inline_script','inline_script_mobile'));

            foreach($data as $key => $val){
                $this->{$key} = stripslashes($val);
            }
            $this->useWriteConnection();
            $saveStatus = $this->create();
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                if(!empty($messages)) {
                    $errMsg = array();
                    foreach ($messages as $message) {
                        $errMsg[] = $message->getMessage();
                    }

                    \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                    $this->errorCode = "RR301";
                    $this->errorMessages = "Failed when try to save data";

                } else {
                    \Helpers\LogHelper::log($log_file_name, "Save data failed, error : unknown error");
                }

                $transactionManager = new \Phalcon\Mvc\Model\Transaction\Manager();
                $transactionManager->setDbService($this->getConnection());
                $is_have_transaction = $transactionManager->has();

                if($is_have_transaction) {
                    throw new \Exception("Rollback transaction");
                }

                return false;
            } else {
                $this->afterInsert();
                return array('affected_rows' => '1');
            }

        }catch (\Exception $e){
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

    }

    public function hardDeleteUrlKey($log_file_name = "masterUrlKey")
    {
        try{
            // $data = $this->getDataArray(array('additional_header','additional_header_mobile','inline_script','inline_script_mobile'));

            // foreach($data as $key => $val){
            //     $this->{$key} = stripslashes($val);
            // }
            $this->useWriteConnection();
            $deleteStatus = $this->delete();
            if ($deleteStatus === false) {
                $messages = $this->getMessages();

                if(!empty($messages)) {
                    $errMsg = array();
                    foreach ($messages as $message) {
                        $errMsg[] = $message->getMessage();
                    }

                    \Helpers\LogHelper::log($log_file_name, "Hard delete data failed, error : " . json_encode($errMsg));
                    $this->errorCode = "RR301";
                    $this->errorMessages = "Failed when try to hard delete data";

                } else {
                    \Helpers\LogHelper::log($log_file_name, "hard delete data failed, error : unknown error");
                }

                $transactionManager = new \Phalcon\Mvc\Model\Transaction\Manager();
                $transactionManager->setDbService($this->getConnection());
                $is_have_transaction = $transactionManager->has();

                if($is_have_transaction) {
                    throw new \Exception("Rollback transaction");
                }

                return false;
            } else {
                $this->afterHardDelete();
                return array('affected_rows' => '1');
            }

        }catch (\Exception $e){
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

    }

    public function afterHardDelete()
    {
        $redisKey = "reference_id='" . $this->reference_id ."'ANDsection='".str_replace(' ', '', $this->section)."'";
        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);

        // cleanup route
        $redisKey = "url_key,section,additional_header,inline_script,document_title,reference_id,company_code";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);

        /*
         * Also Clean up this url key from redis (if query from url key)
         */
        $redisKey = "url_key='" . $this->url_key."'";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }

    public function updateUrlKey($section = "",$urlKey = "",$oldUrlKey=""){
        if($section == "promo_page"){
            if(isset($this->additional_header_mobile) && isset($this->inline_script_mobile)){
                $sql  = "UPDATE master_url_key ";
                $sql .= "SET url_key = \"".$urlKey."\",additional_header=\"".$this->additional_header."\",additional_header_mobile=\"".$this->additional_header_mobile."\",inline_script=\"".$this->inline_script."\"";
                $sql .= ",inline_script_mobile=\"".$this->inline_script_mobile."\",document_title=\"".$this->document_title."\"";
                $sql .= ",created_at=\"".$this->created_at."\"";
                $sql .= " WHERE url_key=\"".$oldUrlKey."\" AND company_code='".$this->company_code."'";
            } else {
                $sql  = "UPDATE master_url_key ";
                $sql .= "SET url_key = \"".$urlKey."\",additional_header=\"".$this->additional_header."\",inline_script=\"".$this->inline_script."\",document_title=\"".$this->document_title."\"";
                $sql .= ",created_at=\"".$this->created_at."\"";
                $sql .= " WHERE url_key=\"".$oldUrlKey."\" AND company_code='".$this->company_code."'";
            }
        }else{
            if(isset($this->additional_header_mobile) && isset($this->inline_script_mobile)){
                $sql  = "UPDATE master_url_key ";
                $sql .= "SET additional_header=\"".$this->additional_header."\",additional_header_mobile=\"".$this->additional_header_mobile."\",inline_script=\"".$this->inline_script."\"";
                $sql .= ",inline_script_mobile=\"".$this->inline_script_mobile."\",document_title=\"".$this->document_title."\"";
                $sql .= ",created_at=\"".$this->created_at."\"";
                $sql .= " WHERE url_key=\"".$this->url_key."\" AND company_code='".$this->company_code."'";
            } else {
                $sql  = "UPDATE master_url_key ";
                $sql .= "SET additional_header=\"".$this->additional_header."\",inline_script=\"".$this->inline_script."\",document_title=\"".$this->document_title."\"";
                $sql .= ",created_at=\"".$this->created_at."\"";
                $sql .= " WHERE url_key=\"".$this->url_key."\" AND company_code='".$this->company_code."'";
            }
        }  

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        $this->afterUpdate();

        return array('affected_rows' => $result->numRows());
    }

    public function updateCustom($dataChanged = array(),$conditions = array()){
        $setData = array();

        if(empty($dataChanged)){
            return false;
        }

        if(empty($conditions)){
            return false;
        }

        foreach ($dataChanged as $keyData => $val){
            $setData[] = $keyData." ='".$val."'";
        }

        $data = implode(',',$setData);


        //where conditions - and only
        foreach ($conditions as $keyData => $val){
            $conditionsData[] = $keyData." ='".$val."'";
        }

        $conditions = implode(' AND ',$conditionsData);

        // currently only and
        $sql  = "UPDATE master_url_key SET ";
        $sql .= $data;
        $sql .= " WHERE ";
        $sql .= $conditions;

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        $this->afterUpdate();

        return array('affected_rows' => $result->numRows());
    }
}
