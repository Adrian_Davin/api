<?php

namespace Models;

class ProductSellerBatch extends \Models\BaseModel
{
    protected $product_seller_batch_id;
    protected $csv_file;
    protected $error_csv_file;
    protected $updated_fields;
    protected $total_sku;
    protected $created_at;
    protected $updated_at;
    protected $created_by;

    /**
     * @return mixed
     */
    public function getProductSellerBatchId()
    {
        return $this->product_seller_batch_id;
    }

    /**
     * @param mixed $product_seller_batch_id
     */
    public function setProductSellerBatchId($product_seller_batch_id)
    {
        $this->product_seller_batch_id = $product_seller_batch_id;
    }

    /**
     * @return mixed
     */
    public function getCsvFile()
    {
        return $this->csv_file;
    }

    /**
     * @param mixed $csv_file
     */
    public function setCsvFile($csv_file)
    {
        $this->csv_file = $csv_file;
    }

    /**
     * @return mixed
     */
    public function getErrorCsvFile()
    {
        return $this->error_csv_file;
    }

    /**
     * @param mixed $error_csv_file
     */
    public function setErrorCsvFile($error_csv_file)
    {
        $this->error_csv_file = $error_csv_file;
    }

    /**
     * @return mixed
     */
    public function getUpdatedFields()
    {
        return $this->updated_fields;
    }

    /**
     * @param mixed $update_fields
     */
    public function setUpdatedFields($updated_fields)
    {
        $this->updated_fields = $updated_fields;
    }

    /**
     * @return mixed
     */
    public function getTotalSku()
    {
        return $this->total_sku;
    }

    /**
     * @param mixed $total_sku
     */
    public function setTotalSku($total_sku)
    {
        $this->total_sku = $total_sku;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_seller_batch';
    }


    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

}
