<?php

namespace Models;

class ProductVariantAttribute extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $product_variant_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $attribute_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $value;

    /**
     * @var \Models\attribute;
     */
    protected $attribute;

    /**
     * @var \Models\attributeOptionValue;
     */
    protected $attributeOption;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_variant_attribute';
    }

    public function initialize()
    {
        $this->setSource("product_variant_attribute");
    }

    /**
     * @return int
     */
    public function getProductVariantId()
    {
        return $this->product_variant_id;
    }

    /**
     * @param int $product_variant_id
     */
    public function setProductVariantId($product_variant_id)
    {
        $this->product_variant_id = $product_variant_id;
    }

    /**
     * @return int
     */
    public function getAttributeId()
    {
        return $this->attribute_id;
    }

    /**
     * @param int $attribute_id
     */
    public function setAttributeId($attribute_id)
    {
        $this->attribute_id = $attribute_id;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param attribute $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @return attributeOptionValue
     */
    public function getAttributeOption()
    {
        return $this->attributeOption;
    }

    /**
     * @param attributeOptionValue $attributeOption
     */
    public function setAttributeOption($attributeOption)
    {
        $this->attributeOption = $attributeOption;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductVariantAttribute
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductVariantAttribute
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        if(empty($dataArray['attribute_id'])) {
            \Helpers\LogHelper::log("product_variant", "Product variant attribute set data failed, no attribute_id");
            return false;
        }

        $this->attribute_id = $dataArray['attribute_id'];
        if(isset($dataArray['product_variant_id'])) {
            $this->product_variant_id = $dataArray['product_variant_id'];
        }

        $attribute['attribute_id'] = $dataArray['attribute_id'];
        $variant = new \Models\Attribute();
        if(isset($dataArray['code'])) {
            $attribute['code'] = $dataArray['code'];
        } else {
            $conditions = ['attribute_id'=>$dataArray['attribute_id']];
            $param = [
                'conditions' => "attribute_id=:attribute_id:",
                'bind' => $conditions
            ];
            $result = $variant::findFirst($param);
            $attribute = $result->toArray();
        }
        $variant->setFromArray($attribute);
        $this->attribute = $variant;

        $variantOption = new \Models\AttributeOptionValue();
        $masterSizeModel = new \Models\MasterSize();
        $masterAromaModel = new \Models\MasterAroma();
        $resultArray = array();

        if(!array_key_exists('value', $dataArray) && !empty($dataArray['option_value'])) {
            if($dataArray['attribute_id'] == '397'){ // size
                $details = explode('|||',$dataArray['option_value']);

                $result = $masterSizeModel->findFirst([
                    "conditions" => "attribute_unit='".$details[0]."' AND value='".$details[1]."'"
                ]);
            }
            else if($dataArray['attribute_id'] == '398'){ // aroma
                $result = $masterAromaModel->findFirst([
                    "conditions" => "value='".$dataArray['option_value']."'"
                ]);
            }
            else{
                $conditions = ['attribute_id'=>$dataArray['attribute_id'], 'value'=>$dataArray['option_value']];
                $param = [
                    'conditions' => "attribute_id=:attribute_id: AND value=:value:",
                    'bind' => $conditions
                ];
                $result = $variantOption::findFirst($param);
            }

            if($result) {
                $resultArray = $result->toArray();
                $this->value = $result->getOptionId();
            } else {
                $resultArray = array("attribute_id" => $dataArray['attribute_id']);
            }
        } else if (array_key_exists('value', $dataArray)) {
            if(!empty($dataArray['value'])) {
                $conditions = ['attribute_id'=>$dataArray['attribute_id'], 'option_id'=>$dataArray['value']];
                $param = [
                    'conditions' => "attribute_id=:attribute_id: AND option_id=:option_id:",
                    'bind' => $conditions
                ];
                $result = $variantOption::findFirst($param);
                if($result){
                    $resultArray = $result->toArray();
                }else{
                    $resultArray = array("attribute_id" => $dataArray['attribute_id'], "option_id" => $dataArray['value']);
                }

            } else {
                $resultArray = array("attribute_id" => $dataArray['attribute_id'], "option_id" => $dataArray['value']);
            }

            $this->value = $dataArray['value'];
        }

        $variantOption->setFromArray($resultArray);
        $this->attributeOption = $variantOption;

        return true;
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        if(!empty($this->attribute)) {
            $view['variant'] = $this->attribute->getDataArray();
        }

        if(!empty($this->attributeOption)) {
            $view['variantOption'] = $this->attributeOption->getDataArray();
        }

        return $view;
    }

    public function saveData($log_file_name = "", $state = "")
    {
        if(empty($this->product_variant_id)) {
            \Helpers\LogHelper::log("product", "Product variant attribute save failed, error : product_variant_id is empty");
            return "Save product variant attribute need product_variant_id";
        }

        if(empty($this->attribute_id)) {
            \Helpers\LogHelper::log("product", "Product variant attribute save failed, error : attribute_id is empty");
            return "Save product variant attribute_id is empty";
        }

        if ($this->save() === false) {
            $messages = $this->getMessages();

            $errMsg = array();
            foreach ($messages as $message) {
                $errMsg[] = $message->getMessage();
            }

            \Helpers\LogHelper::log("product", "Product variant attribute failed, error : " . json_encode($errMsg));
            return "Save product variant attribute failed";
        }

        return;
    }

    public function updateAttributeVariantBatch($params = array()){
        try {

            $sql = "
                update product_variant set ".$params['stringSet']." where  sku = '".$params['sku']."'
                
            ";

            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            if($result){
                $affectedRows = $this->getDi()->getShared('dbMaster')->affectedRows();

                /**
                 * ToDo : Checking and Update Status Global
                 */
                $productVariantLib = new \Library\ProductVariant();
                $productVariantLib->setSku($params['sku']);
                $productVariantLib->checkStatusGlobal();


                \Helpers\ProductHelper::updateProductToElastic(array(0 => $params));

                return $affectedRows;

            }else{
                return false;
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("Product Variant Batch", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'Product Variant Batch','message' => 'There have something error, in query batch'));
            return false;
        }
    }
}
