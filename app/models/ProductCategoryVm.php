<?php

namespace Models;

class ProductCategoryVm extends \Models\BaseModel
{

    protected $product_category_vm_id;
    protected $category_id;
    protected $most_sales_sku;
    protected $most_click_sku;
    protected $most_add_to_cart_sku;
    protected $most_slow_moving_sku;
    protected $most_new_arrival_sku;
    protected $most_special_price_sku;

    public function onConstruct()
    {
        parent::onConstruct();
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_category_vm';
    }

    /**
     * Get the value of product_category_vm_id
     */ 
    public function getProduct_category_vm_id()
    {
        return $this->product_category_vm_id;
    }

    /**
     * Set the value of product_category_vm_id
     *
     * @return  self
     */ 
    public function setProduct_category_vm_id($product_category_vm_id)
    {
        $this->product_category_vm_id = $product_category_vm_id;

        return $this;
    }

    /**
     * Get the value of category_id
     */ 
    public function getCategory_id()
    {
        return $this->category_id;
    }

    /**
     * Set the value of category_id
     *
     * @return  self
     */ 
    public function setCategory_id($category_id)
    {
        $this->category_id = $category_id;

        return $this;
    }

    /**
     * Get the value of most_sales_sku
     */ 
    public function getMost_sales_sku()
    {
        return $this->most_sales_sku;
    }

    /**
     * Set the value of most_sales_sku
     *
     * @return  self
     */ 
    public function setMost_sales_sku($most_sales_sku)
    {
        $this->most_sales_sku = $most_sales_sku;

        return $this;
    }

    /**
     * Get the value of most_click_sku
     */ 
    public function getMost_click_sku()
    {
        return $this->most_click_sku;
    }

    /**
     * Set the value of most_click_sku
     *
     * @return  self
     */ 
    public function setMost_click_sku($most_click_sku)
    {
        $this->most_click_sku = $most_click_sku;

        return $this;
    }

    /**
     * Get the value of most_add_to_cart_sku
     */ 
    public function getMost_add_to_cart_sku()
    {
        return $this->most_add_to_cart_sku;
    }

    /**
     * Set the value of most_add_to_cart_sku
     *
     * @return  self
     */ 
    public function setMost_add_to_cart_sku($most_add_to_cart_sku)
    {
        $this->most_add_to_cart_sku = $most_add_to_cart_sku;

        return $this;
    }

    /**
     * Get the value of most_slow_moving_sku
     */ 
    public function getMost_slow_moving_sku()
    {
        return $this->most_slow_moving_sku;
    }

    /**
     * Set the value of most_slow_moving_sku
     *
     * @return  self
     */ 
    public function setMost_slow_moving_sku($most_slow_moving_sku)
    {
        $this->most_slow_moving_sku = $most_slow_moving_sku;

        return $this;
    }

    /**
     * Get the value of most_new_arrival_sku
     */ 
    public function getMost_new_arrival_sku()
    {
        return $this->most_new_arrival_sku;
    }

    /**
     * Set the value of most_new_arrival_sku
     *
     * @return  self
     */ 
    public function setMost_new_arrival_sku($most_new_arrival_sku)
    {
        $this->most_new_arrival_sku = $most_new_arrival_sku;

        return $this;
    }

    /**
     * Get the value of most_special_price_sku
     */ 
    public function getMost_special_price_sku()
    {
        return $this->most_special_price_sku;
    }

    /**
     * Set the value of most_special_price_sku
     *
     * @return  self
     */ 
    public function setMost_special_price_sku($most_special_price_sku)
    {
        $this->most_special_price_sku = $most_special_price_sku;

        return $this;
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
