<?php

namespace Models;

class Salesrule extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $rule_id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $conditions;

    /**
     * @var string
     */
    protected $tags;

    /**
     *
     * @var string
     */
    protected $rule_type;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     *
     * @var string
     */
    protected $company_code;

    /**
     *
     * @var string
     */
    protected $from_date;

    /**
     *
     * @var string
     */
    protected $to_date;

    /**
     *
     * @var string
     */
    protected $from_time;

    /**
     *
     * @var string
     */
    protected $to_time;

    // unused code
    // /**
    //  *
    //  * @var string
    //  */
    // protected $cashback_to_date;

    /**
     *
     * @var string
     */
    protected $schedule;

    /**
     *
     * @var integer
     */
    protected $uses_per_customer;

    /**
     *
     * @var integer
     */
    protected $uses_per_customer_frequence;

    /**
     *
     * @var integer
     */
    protected $total_purchase;

    /**
     *
     * @var integer
     */
    protected $purchase_days;
    
    /**
     *
     * @var integer
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $stop_rules_processing;

    /**
     *
     * @var integer
     */
    protected $can_refund;

    /**
     * @var integer
     */
    protected $can_combine;

    /**
     *
     * @var integer
     */
    protected $priority;

    /**
     *
     * @var string
     */
    protected $landing_page_name;

    /**
     *
     * @var string
     */
    protected $landing_page_link;

    /**
     *
     * @var string
     */
    protected $bank_name;

    /**
     *
     * @var string
     */
    protected $budget_owner;


    /**
     *
     * @var string
     * @var integer
     */
    protected $device_to_publish;

    /**
     *
     * @var integer
     */
    protected $salesrule_action_id;

    /**
     *
     * @var string$
     */
    protected $attributes;

    /**
     *
     * @var double
     */
    protected $discount_amount;

    /**
     *
     * @var string
     */
    protected $pwp_data;

    /**
     *
     * @var integer
     */
    protected $pwp_maximum_item;

    /**
     *
     * @var integer
     */
    protected $pwp_multiplier;

    /**
     *
     * @var integer
     */
    protected $pwp_same_store;

        /**
     *
     * @var integer
     */
    protected $pwp_multiplier_item;

        /**
     *
     * @var integer
     */
    protected $pwp_multiplier_qty;



    /**
     *
     * @var double
     */
    protected $discount_type;

    protected $discount_step;

    protected $discount_step_max;

    protected $discount_item_sku;

    protected $discount_item_qty;

    protected $discount_item_qty_each;

    protected $discount_item_type;

    protected $associate_rule_id;

    protected $voucher_expired = 0;

    protected $voucher_generated;

    /**
     *
     * @var integer
     */
    protected $use_voucher;
    protected $qty_x_discount;
    protected $split_budget;
    protected $merchandise_budget;
    protected $marketing_budget;
    protected $split_combine;
    protected $code_length;
    
    /**
     * 
     * @var integer
     */
    protected $claimable;

    /**
     * Get the value of qty_x_discount
     */ 
    public function getQty_x_discount()
    {
        return $this->qty_x_discount;
    }

    /**
     * Set the value of qty_x_discount
     *
     * @return  self
     */ 
    public function setQty_x_discount($qty_x_discount)
    {
        $this->qty_x_discount = $qty_x_discount;

        return $this;
    }

    /**
     * Get the value of split_budget
     */ 
    public function getSplit_budget(){
		return $this->split_budget;
	}

    /**
     * Set the value of split_budget
     *
     * @return  self
     */ 
	public function setSplit_budget($split_budget){
        $this->split_budget = $split_budget;
        return $this;
    }
    
    /**
     * Get the value of split_combine
     */ 
    public function getSplit_combine(){
		return $this->split_combine;
	}

    /**
     * Set the value of split_combine
     *
     * @return  self
     */ 
	public function setSplit_combine($split_combine){
        $this->split_combine = $split_combine;
        return $this;
	}

    /**
     * Get the value of merchandise_budget
     */ 
	public function getMerchandise_budget(){
		return $this->merchandise_budget;
	}

    /**
     * Set the value of merchandise_budget
     *
     * @return  self
     */ 
	public function setMerchandise_budget($merchandise_budget){
        $this->merchandise_budget = $merchandise_budget;
        return $this;
	}

    /**
     * Get the value of marketing_budget
     */ 
	public function getMarketing_budget(){
		return $this->marketing_budget;
	}

    /**
     * Set the value of marketing_budget
     *
     * @return  self
     */ 
	public function setMarketing_budget($marketing_budget){
        $this->marketing_budget = $marketing_budget;
        return $this;
	}
    
    /**
     * Get the value of qty_x_discount
     */ 
    public function getCode_length($code_length)
    {
        return $this->code_length;
    }

    /**
     * Set the value of qty_x_discount
     *
     * @return  self
     */ 
    public function setCode_length($code_length)
    {
        $this->code_length = $code_length;

        return $this;
    }
    /**
     *
     * @var integer
     */
    protected $rule_group;

    protected $label_id;
    protected $label_action_id;

    protected $errors;
    protected $messages;
    protected $data;
    protected $customer_group;

    /**
     * @var \Models\Salesrule\CustomerGroup\Collection
     */
    protected $customerGroupObj;

    protected $max_redemption;

    protected $max_quantity;

    protected $email_template_id;
    protected $shop_reminder_id;
    protected $email_voucher_created;
    protected $template_id_email_voucher_created;
    protected $email_voucher_expired;
    protected $template_id_email_voucher_expired;
    protected $pushnotif_voucher_created;
    protected $pushnotif_voucher_expired;

    protected $limit_payment_time;
    protected $limit_used;

    protected $split_amount;

    protected $global_can_combine;

    /**
     *
     * @var string
     */
    protected $tnc;
    protected $code_format;
    protected $code_prefix;
    protected $code_suffix;
    protected $promo_type;

    protected $publish_description;
    protected $display_description;

    /**
     * Get the value of publish_description
     */ 
    public function getPublish_description()
    {
        return $this->publish_description;
    }
    /**
     * Get the value of display_description
     */ 
    public function getDisplay_description()
    {
        return $this->display_description;
    }

    /**
     * @return mixed
     */
    public function getMaxRedemption()
    {
        return $this->max_redemption;
    }

    /**
     * @param mixed $max_redemption
     */
    public function setMaxRedemption($max_redemption)
    {
        $this->max_redemption = $max_redemption;
    }

    /**
     * @return mixed
     */
    public function getCustomerGroupObj()
    {
        return $this->customerGroupObj;
    }

    /**
     * @param mixed $customerGroupObj
     */
    public function setCustomerGroupObj($customerGroupObj)
    {
        $this->customerGroupObj = $customerGroupObj;
    }

    /**
     * Method to set the value of field rule_id
     *
     * @param integer $rule_id
     * @return $this
     */
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;
        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param string $conditions
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param string $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * Method to set the value of field rule_type
     *
     * @param string $rule_type
     * @return $this
     */
    public function setRuleType($rule_type)
    {
        $this->rule_type = $rule_type;
        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Method to set the value of field company_code
     *
     * @param string $company_code
     * @return $this
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
        return $this;
    }

    /**
     * Method to set the value of field code_format
     *
     * @param string $code_format
     * @return $this
     */
    public function setCodeFormat($code_format)
    {
        $this->code_format = $code_format;
        return $this;
    }

    /**
     * Method to set the value of field code_prefix
     *
     * @param string $code_prefix
     * @return $this
     */
    public function setCodePrefix($code_prefix)
    {
        $this->code_prefix = $code_prefix;
        return $this;
    }
    /**
     * Method to set the value of field code_suffix
     *
     * @param string $code_suffix
     * @return $this
     */
    public function setCodeSuffix($code_suffix)
    {
        $this->code_suffix = $code_suffix;
        return $this;
    }
      
    /**
     * Method to set the value of field promo_type
     *
     * @param string $promo_type
     * @return $this
     */
    public function setPromoType($promo_type)
    {
        $this->promo_type = $promo_type;
        return $this;
    }

    /**
     * Method to set the value of field from_date
     *
     * @param string $from_date
     * @return $this
     */
    public function setFromDate($from_date)
    {
        $this->from_date = $from_date;
        return $this;
    }

    /**
     * Method to set the value of field to_date
     *
     * @param string $to_date
     * @return $this
     */
    public function setToDate($to_date)
    {
        $this->to_date = $to_date;

        return $this;
    }

    // unused code
    // /**
    //  * Method to set the value of field cashback_to_date
    //  *
    //  * @param string $cashback_to_date
    //  * @return $this
    //  */
	// public function setCashback_to_date($cashback_to_date){
	// 	$this->cashback_to_date = $cashback_to_date;
	// }

    /**
     * Method to set the value of field schedule
     *
     * @param string $schedule
     * @return $this
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Method to set the value of field uses_per_customer
     *
     * @param integer $uses_per_customer
     * @return $this
     */
    public function setUsesPerCustomer($uses_per_customer)
    {
        $this->uses_per_customer = $uses_per_customer;

        return $this;
    }

    /**
     * Get the value of uses_per_customer_frequence
     *
     * @return  integer
     */ 
    public function getUsesPerCustomerFrequence()
    {
        return $this->uses_per_customer_frequence;
    }

    /**
     * Set the value of uses_per_customer_frequence
     *
     * @param  integer  $uses_per_customer_frequence
     *
     * @return  self
     */ 
    public function setUsesPerCustomerFrequence($uses_per_customer_frequence)
    {
        $this->uses_per_customer_frequence = $uses_per_customer_frequence;

        return $this;
    }

    /**
     * Get the value of total_purchase
     *
     * @return  integer
     */ 
    public function getTotal_purchase(){
		return $this->total_purchase;
	}

    /**
     * Set the value of total_purchase
     *
     * @param  integer  $total_purchase
     *
     * @return  self
     */ 
	public function setTotal_purchase($total_purchase){
		$this->total_purchase = $total_purchase;
	}

    /**
     * Get the value of purchase_days
     *
     * @return  integer
     */ 
	public function getPurchase_days(){
		return $this->purchase_days;
	}

    /**
     * Set the value of purchase_days
     *
     * @param  integer  $purchase_days
     *
     * @return  self
     */ 
	public function setPurchase_days($purchase_days){
		$this->purchase_days = $purchase_days;
	}

    /**
     * Method to set the value of field status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field stop_rules_processing
     *
     * @param integer $stop_rules_processing
     * @return $this
     */
    public function setStopRulesProcessing($stop_rules_processing)
    {
        $this->stop_rules_processing = $stop_rules_processing;

        return $this;
    }

    /**
     * Method to set the value of field can_refund
     *
     * @param integer $can_refund
     * @return $this
     */
    public function setCanRefund($can_refund)
    {
        $this->can_refund = $can_refund;

        return $this;
    }

    /**
     * @return int
     */
    public function getCanCombine()
    {
        return $this->can_combine;
    }

    /**
     * @param int $can_combine
     */
    public function setCanCombine($can_combine)
    {
        $this->can_combine = $can_combine;
    }

     /**
     * @return mixed
     */
    public function getRuleGroup()
    {
        return $this->rule_group;
    }

    /**
     * @param mixed $rule_group
     */
    public function setRuleGroup($rule_group)
    {
        $this->rule_group = $rule_group;
    }

    /**
     * Method to set the value of field priority
     *
     * @param integer $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * Method to set the value of field landing_page_name
     *
     * @param string $landing_page_name
     * @return $this
     */
    public function setLandingPageName($landing_page_name)
    {
        $this->landing_page_name = $landing_page_name;
        return $this;
    }

    /**
     * Method to set the value of field landing_page_link
     *
     * @param string $landing_page_link
     * @return $this
     */
    public function setLandingPageLink($landing_page_link)
    {
        $this->landing_page_link = $landing_page_link;
        return $this;
    }

    /**
     * Method to set the value of field bank_name
     *
     * @param string $bank_name
     * @return $this
     */
    public function setBankName($bank_name)
    {
        $this->bank_name = $bank_name;
        return $this;
    }

     /**
     * Method to set the value of field budget_owner
     *
     * @param string $budget_owner
     * @return $this
     */
    public function setBudgetOwner($budget_owner)
    {
        $this->budget_owner = $budget_owner;
        return $this;
    }

     /**
     * Method to set the value of field device_to_publish
     *
     * @param string $device_to_publish
     * @return $this
     */
    public function setDeviceToPublish($device_to_publish)
    {
        $this->device_to_publish = $device_to_publish;
        return $this;
    }

    /**
     * Method to set the value of field salesrule_action_id
     *
     * @param integer $salesrule_action_id
     * @return $this
     */
    public function setSalesruleActionId($salesrule_action_id)
    {
        $this->salesrule_action_id = $salesrule_action_id;
        return $this;
    }

    /**
     * Method to set the value of field attribute
     *
     * @param string $attribute
     * @return $this
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * Method to set the value of field discount_amount
     *
     * @param double $discount_amount
     * @return $this
     */
    public function setDiscountAmount($discount_amount)
    {
        $this->discount_amount = $discount_amount;
        return $this;
    }

    /**
     * Method to set the value of field discount_step
     *
     * @param double $discount_step
     * @return $this
     */
    public function setDiscountStep($discount_step)
    {
        $this->discount_step = $discount_step;
        return $this;
    }

    /**
     * Method to set the value of field times_used
     *
     * @param integer $times_used
     * @return $this
     */
    public function setTimesUsed($times_used)
    {
        $this->times_used = $times_used;
        return $this;
    }

    /**
     * Method to set the value of field use_voucher
     *
     * @param integer $use_voucher
     * @return $this
     */
    public function setUseVoucher($use_voucher)
    {
        $this->use_voucher = $use_voucher;
        return $this;
    }

    /**
     * Returns the value of field rule_id
     *
     * @return integer
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field rule_type
     *
     * @return string
     */
    public function getRuleType()
    {
        return $this->rule_type;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Returns the value of field company_code
     *
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * Returns the value of field from_date
     *
     * @return string
     */
    public function getFromDate()
    {
        return $this->from_date;
    }

    /**
     * Returns the value of field to_date
     *
     * @return string
     */
    public function getToDate()
    {
        return $this->to_date;
    }

    /**
     * Returns the value of field from_time
     *
     * @return string
     */
    public function getFromTime()
    {
        return $this->from_time;
    }

    /**
     * Returns the value of field to_time
     *
     * @return string
     */
    public function getToTime()
    {
        return $this->to_time;
    }

     /**
     * Returns the value of field pwp_data
     *
     * @return string
     */
    public function getPwpData()
    {
        return $this->pwp_data;
    }

    /**
     * Returns the value of field pwp_maximum_item
     *
     * @return integer
     */
    public function getPwpMaximumItem()
    {
        return $this->pwp_maximum_item;
    }

     /**
     * Returns the value of field pwp_multiplier
     *
     * @return integer
     */
    public function getPwpMultiplier()
    {
        return $this->pwp_multiplier;
    }

    /**
     * Returns the value of field pwp_same_store
     *
     * @return integer
     */
    public function getPwpSameStore()
    {
        return $this->pwp_same_store;
    }

    /**
     * Returns the value of field pwp_multiplier_item
     *
     * @return integer
     */
    public function getPwpMultiplierItem()
    {
        return $this->pwp_multiplier_item;
    }

    /**
     * Returns the value of field pwp_multiplier_qty
     *
     * @return integer
     */
    public function getPwpMultiplierQty()
    {
        return $this->pwp_multiplier_qty;
    }

    /**
     * Returns the value of field code_format
     *
     * @return string
     */
    public function getCodeFormat()
    {
        return $this->code_format;
    }

    /**
     * Returns the value of field code_prefix
     *
     * @return string
     */
    public function getCodePrefix()
    {
        return $this->code_prefix;
    }

    /**
     * Returns the value of field code_suffix
     *
     * @return string
     */
    public function getCodeSuffix()
    {
        return $this->code_suffix;
    }

    /**
     * Returns the value of field promo_type
     *
     * @return string
     */
    public function getPromoType()
    {
        return $this->promo_type;
    }
    // unused code
    // /**
    //  * Returns the value of field cashback_to_date
    //  *
    //  * @return string
    //  */
    // public function getCashback_to_date(){
	// 	return $this->cashback_to_date;
	// }

    /**
     * Returns the value of field schedule
     *
     * @return string
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Returns the value of field uses_per_customer
     *
     * @return integer
     */
    public function getUsesPerCustomer()
    {
        return $this->uses_per_customer;
    }

    /**
     * Returns the value of field stop_rules_processing
     *
     * @return integer
     */
    public function getStopRulesProcessing()
    {
        return $this->stop_rules_processing;
    }

    /**
     * Returns the value of field can_refund
     *
     * @return integer
     */
    public function getCanRefund()
    {
        return $this->can_refund;
    }

    /**
     * Returns the value of field priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Returns the value of field landing_page_name
     *
     * @return string
     */
    public function getLandingPageName()
    {
        return $this->landing_page_name;
    }

    /**
     * Returns the value of field landing_page_link
     *
     * @return integer
     */
    public function getLandingPageLink()
    {
        return $this->landing_page_link;
    }

    /**
     * Returns the value of field bank_name
     *
     * @return integer
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * Returns the value of field budget_owner
     *
     * @return string
     */
    public function getBudgetOwner()
    {
        return $this->budget_owner;
    }

     /**
     * Returns the value of field device_to_publish
     *
     * @return string
     */
    public function getDeviceToPublish()
    {
        return $this->device_to_publish;
    }


    /**
     * Returns the value of field salesrule_action_id
     *
     * @return integer
     */
    public function getSalesruleActionId()
    {
        return $this->salesrule_action_id;
    }

    /**
     * Returns the value of field attribute
     *
     * @return string
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Returns the value of field discount_amount
     *
     * @return double
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * @return mixed
     */
    public function getDiscountStepMax()
    {
        return $this->discount_step_max;
    }

    /**
     * @param mixed $discount_step_max
     */
    public function setDiscountStepMax($discount_step_max)
    {
        $this->discount_step_max = $discount_step_max;
    }

    /**
     * @return mixed
     */
    public function getDiscountItemSku()
    {
        return $this->discount_item_sku;
    }

    /**
     * @param mixed $discount_item
     */
    public function setDiscountItemSku($discount_item_sku)
    {
        $this->discount_item_sku = $discount_item_sku;
    }

    /**
     * @return mixed
     */
    public function getDiscountItemQty()
    {
        return $this->discount_item_qty;
    }

    /**
     * @param mixed $discount_item_qty
     */
    public function setDiscountItemQty($discount_item_qty)
    {
        $this->discount_item_qty = $discount_item_qty;
    }

    /**
     * @return mixed
     */
    public function getDiscountItemQtyEach()
    {
        return $this->discount_item_qty_each;
    }

    /**
     * @param mixed $discount_item_qty_each
     */
    public function setDiscountItemQtyEach($discount_item_qty_each)
    {
        $this->discount_item_qty_each = $discount_item_qty_each;
    }

    /**
     * @return mixed
     */
    public function getDiscountItemType()
    {
        return $this->discount_item_type;
    }

    /**
     * @param mixed $discount_item_type
     */
    public function setDiscountItemType($discount_item_type)
    {
        $this->discount_item_type = $discount_item_type;
    }

    /**
     * @return mixed
     */
    public function getAssociateRuleId()
    {
        return $this->associate_rule_id;
    }

    /**
     * @param mixed $associate_rule_id
     */
    public function setAssociateRuleId($associate_rule_id)
    {
        $this->associate_rule_id = $associate_rule_id;
    }

    /**
     * @return mixed
     */
    public function getVoucherExpired()
    {
        return $this->voucher_expired;
    }

    /**
     * @param mixed $voucher_expired
     */
    public function setVoucherExpired($voucher_expired)
    {
        $this->voucher_expired = $voucher_expired;
    }

    /**
     * @return mixed
     */
    public function getVoucherGenerated()
    {
        return $this->voucher_generated;
    }

    /**
     * @param mixed $voucher_generated
     */
    public function setVoucherGenerated($voucher_generated)
    {
        $this->voucher_generated = $voucher_generated;
    }

    /**
     * Get the value of limit_used
     */ 
    public function getLimit_used()
    {
        return $this->limit_used;
    }

    /**
     * Set the value of limit_used
     *
     * @return  self
     */ 
    public function setLimit_used($limit_used)
    {
        $this->limit_used = $limit_used;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCustomerGroup()
    {
        return $this->customer_group;
    }

    /**
     * @param mixed $customer_group
     */
    public function setCustomerGroup($customer_group)
    {
        $this->customer_group = $customer_group;
    }

    /**
     * Get the value of label_id
     */ 
    public function getLabel_id()
    {
        return $this->label_id;
    }

    /**
     * Set the value of label_id
     *
     * @return  self
     */ 
    public function setLabel_id($label_id)
    {
        $this->label_id = $label_id;

        return $this;
    }
    
    /**
     * Get the value of label_action_id
     */ 
    public function getLabel_action_id()
    {
        return $this->label_action_id;
    }

    /**
     * Set the value of label_action_id
     *
     * @return  self
     */ 
    public function setLabel_action_id($label_action_id)
    {
        $this->label_action_id = $label_action_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTnc()
    {
        return $this->tnc;
    }

    /**
     * @param mixed $tnc
     */
    public function setTnc($tnc)
    {
        $this->tnc = $tnc;
    }
    
    /**
     * @return mixed
     */
    public function getShop_reminder_id()
    {
        return $this->shop_reminder_id;
    }

    /**
     * @param mixed $shop_reminder_id
     */
    public function setShop_reminder_id($shop_reminder_id)
    {
        $this->shop_reminder_id = $shop_reminder_id;
    }
    
    /**
     * @return mixed
     */
    public function getEmail_voucher_created()
    {
        return $this->email_voucher_created;
    }

    /**
     * @param mixed $email_voucher_created
     */
    public function setEmail_voucher_created($email_voucher_created)
    {
        $this->email_voucher_created = $email_voucher_created;
    }

    /**
     * @return mixed
     */
    public function getTemplate_id_email_voucher_created()
    {
        return $this->template_id_email_voucher_created;
    }

    /**
     * @param mixed $template_id_email_voucher_created
     */
    public function setTemplate_id_email_voucher_created($template_id_email_voucher_created)
    {
        $this->template_id_email_voucher_created = $template_id_email_voucher_created;
    }

    /**
     * @return mixed
     */
    public function getEmail_voucher_expired()
    {
        return $this->email_voucher_expired;
    }

    /**
     * @param mixed $email_voucher_expired
     */
    public function setEmail_voucher_expired($email_voucher_expired)
    {
        $this->email_voucher_expired = $email_voucher_expired;
    }

    /**
     * @return mixed
     */
    public function getTemplate_id_email_voucher_expired()
    {
        return $this->template_id_email_voucher_expired;
    }

    /**
     * @param mixed $template_id_email_voucher_expired
     */
    public function setTemplate_id_email_voucher_expired($template_id_email_voucher_expired)
    {
        $this->template_id_email_voucher_expired = $template_id_email_voucher_expired;
    }

    /**
     * @return mixed
     */
    public function getReminder_pushnotif_created()
    {
        return $this->reminder_pushnotif_created;
    }

    /**
     * @param mixed $reminder_pushnotif_created
     */
    public function setReminder_pushnotif_created($reminder_pushnotif_created)
    {
        $this->reminder_pushnotif_created = $reminder_pushnotif_created;
    }

    /**
     * @return mixed
     */
    public function getReminder_pushnotif_expired()
    {
        return $this->reminder_pushnotif_expired;
    }

    /**
     * @param mixed $reminder_pushnotif_expired
     */
    public function setReminder_pushnotif_expired($reminder_pushnotif_expired)
    {
        $this->reminder_pushnotif_expired = $reminder_pushnotif_expired;
    }

    /**
     * Get the value of claimable
     * 
     * @return integer
     */ 
    public function getClaimable()
    {
        return $this->claimable;
    }

    /**
     * Set the value of claimable
     *
     * @param integer
     * @return self
     */ 
    public function setClaimable($claimable)
    {
        $this->claimable = $claimable;

        return $this;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->hasMany('rule_id', 'SalesruleVoucher', 'rule_id', array('alias' => 'SalesruleVoucher'));
        //$this->hasMany('rule_id', 'SalesruleCustomer', 'rule_id', array('alias' => 'SalesruleCustomer'));
        //$this->hasMany('rule_id', 'SalesruleLabel', 'rule_id', array('alias' => 'SalesruleLabel'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salesrule';
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Salesrule[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Salesrule
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
            //else{
//                if($key == 'customer_group') {
//                    $customerGroups = array();
//                    foreach($dataArray['customer_group'] as $group) {
//                        $customerGroupObj = new \Models\SalesruleCustomerGroup();
//                        if(isset($dataArray['rule_id'])) {
//                            $group['rule_id'] = $dataArray['rule_id'];
//                        }
//                        $customerGroupObj->setFromArray($group);
//                        $customerGroups[] = $customerGroupObj;
//                    }
//
//                    $this->customerGroupObj = new \Models\Salesrule\CustomerGroup\Collection($customerGroups);
//                    continue;
//                }
//            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }


    public function saveSalesRule($log_file_name = "error")
    {
        $result = $this->saveData($log_file_name);
        $resultData = array(
            'return' => false,
            'rule_id' => 0
        );
        if($result){
            $sql = "DELETE FROM salesrule_customer_group";
            $sql .= " WHERE rule_id = " . $this->rule_id;
            $this->useWriteConnection();
            $this->getDi()->getShared($this->getConnection())->query($sql);

            $customerGroups = array();
            foreach($this->customer_group as $group) {
                $customerGroupObj = new \Models\SalesruleCustomerGroup();
                $group['rule_id'] = $this->rule_id;

                $customerGroupObj->setFromArray($group);
                $customerGroups[] = $customerGroupObj;
            }

            $this->customerGroupObj = new \Models\Salesrule\CustomerGroup\Collection($customerGroups);

            $this->customerGroupObj->saveData('salesrule_customer_group');
            $resultData['return'] = true;
            $resultData['rule_id'] = $this->rule_id;
        }
        return $resultData;
    }
    
    public function copySalesRule($log_file_name = "error"){
        $response = array();
        try {
            $sql = '
                    insert into salesrule (salesrule_action_id, rule_type, rule_group, name, conditions, tags, description, from_date, to_date,
                                            use_voucher, label_id, uses_per_customer, uses_per_customer_frequence, stop_rules_processing, 
                                            global_can_combine, can_combine, priority, discount_type, attributes, discount_amount, split_budget, 
                                            marketing_budget, merchandise_budget, qty_x_discount, discount_step, discount_step_max, discount_item_sku,
                                            discount_item_qty, discount_item_qty_each, discount_item_type, associate_rule_id, voucher_expired, voucher_generated,
                                            max_redemption, max_quantity, label_action_id, limit_payment_time, limit_used, email_template_id, split_amount, shop_reminder_id,
                                            email_voucher_created, template_id_email_voucher_created, email_voucher_expired, template_id_email_voucher_expired, 
                                            reminder_pushnotif_created, reminder_pushnotif_expired, company_code, status, created_at, updated_at, tnc, can_refund, budget_owner, device_to_publish) 
                                    (select salesrule_action_id, rule_type, rule_group, name, conditions, tags, description, from_date, to_date,
                                            use_voucher, label_id, uses_per_customer, uses_per_customer_frequence, stop_rules_processing, 
                                            global_can_combine, can_combine, priority, discount_type, attributes, discount_amount, split_budget, 
                                            marketing_budget, merchandise_budget, qty_x_discount, discount_step, discount_step_max, discount_item_sku,
                                            discount_item_qty, discount_item_qty_each, discount_item_type, associate_rule_id, voucher_expired, voucher_generated,
                                            max_redemption, max_quantity, label_action_id, limit_payment_time, limit_used, email_template_id, split_amount, shop_reminder_id,
                                            email_voucher_created, template_id_email_voucher_created, email_voucher_expired, template_id_email_voucher_expired, 
                                            reminder_pushnotif_created, reminder_pushnotif_expired, company_code, status, created_at, updated_at, tnc, can_refund, budget_owner, device_to_publish
                                    from salesrule
                                    where rule_id ='.$this->rule_id.');
                    ';
            $this->getDi()->getShared('dbMaster')->execute($sql);
            $salesruleGroup = \Models\SalesruleCustomerGroup::find("rule_id = " . $this->rule_id);
            $newRuleId = $this->getDi()->getShared('dbMaster')->lastInsertId();

            $groups = $salesruleGroup->toArray();
            $customerGroups = array();

            foreach($groups as $group){
                $customerGroupObj = new \Models\SalesruleCustomerGroup();
                $group['rule_id'] = $newRuleId;

                $customerGroupObj->setFromArray($group);
                $customerGroups[] = $customerGroupObj;
            }
            $this->customerGroupObj = new \Models\Salesrule\CustomerGroup\Collection($customerGroups);
            $this->customerGroupObj->saveData('salesrule_customer_group');
            $response['data']['rule_id'] = $newRuleId;

        } catch (\Exception $e) {
            $response['errors'] = 'Salesrule copy failed';
            \Helpers\LogHelper::log($log_file_name, "Logout process failed, error : " . $e->getMessage());
        }

        return $response;
        
    }

    public function checkVoucherQuantity() {
        $response = array(
            'return' => false,
            'check' => false
        );

        try {
            $sql = 'SELECT count(voucher_code) as totalVoucher from salesrule_voucher WHERE rule_id = ' .$this->rule_id;
            $result = $this->getDi()->getShared('dbMaster')->query($sql);
            $result->setFetchMode(
                \Phalcon\Db::FETCH_ASSOC
            );
            $totalVoucher = $result->fetchAll()[0]['totalVoucher'];
            if ($totalVoucher >= 2) {
                $response['check'] = true;
            }
            $response['return'] = true;

        } catch (\Exception $e) {
            \Helpers\LogHelper::log("check_voucher_salesrule", "Failed to check voucher at salesrule_voucher on update, error : " . $e->getMessage());
        }

        return $response;
    }
}