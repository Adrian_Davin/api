<?php

namespace Models;

use Helpers\LogHelper;
use Phalcon\Db;

class SalesOrderAddress extends \Models\BaseModel
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_order_address_id = 0;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $sales_order_id = 0;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $country_id = 0;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $province_id = 0;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $city_id = 0;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $kecamatan_id = 0;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $kelurahan_id = 0;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $first_name = '';

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $last_name = '';

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $phone = '';

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $full_address = '';

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $post_code = '';

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $group_shipment;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $carrier_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $address_type;

    protected $customer_address = '';
    protected $company_address_id = 0;
    protected $address_name = '';

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $geolocation;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->geolocation = getenv('DEFAULT_GEOLOCATION');
        
        $this->hasMany('sales_order_address_id', 'Models\SalesInvoice', 'billing_address_id', array('alias' => 'SalesInvoice'));
        $this->hasMany('sales_order_address_id', 'Models\SalesInvoice', 'shipping_address_id', array('alias' => 'SalesInvoice'));
        $this->belongsTo('city_id', 'Models\MasterCity', 'city_id', array('alias' => 'MasterCity', "reusable" => true));
        $this->belongsTo('country_id', 'Models\MasterCountry', 'country_id', array('alias' => 'MasterCountry', "reusable" => true));
        $this->belongsTo('kecamatan_id', 'Models\MasterKecamatan', 'kecamatan_id', array('alias' => 'MasterKecamatan', "reusable" => true));
        $this->belongsTo('kelurahan_id', 'Models\MasterKelurahan', 'kelurahan_id', array('alias' => 'MasterKelurahan', "reusable" => true));
        $this->belongsTo('province_id', 'Models\MasterProvince', 'province_id', array('alias' => 'MasterProvince', "reusable" => true));
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder', "reusable" => true));
        $this->belongsTo('company_address_id', 'Models\CompanyAddress', 'address_id', array('alias' => 'CompanyAddress'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_order_address';
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * @param int $sales_order_id
     */
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }

    /**
     * @return string
     */
    public function getGeolocation()
    {
        return $this->geolocation;
    }

    /**
     * @param string $geolocation
     */
    public function setGeolocation($geolocation)
    {
        $this->geolocation = $geolocation;
    }

    /**
     * @return string
     */
    public function getAddressName()
    {
        return $this->address_name;
    }

    /**
     * @param string $address_name
     */
    public function setAddressName($address_name)
    {
        $this->address_name = $address_name;
    }

    /**
     * @return string
     */
    public function getAddressType()
    {
        return $this->address_type;
    }

    /**
     * @param string $address_type
     */
    public function setAddressType($address_type)
    {
        $this->address_type = $address_type;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        return $this->full_address;
    }

    /**
     * @param string $full_address
     */
    public function setFullAddress($full_address)
    {
        $this->full_address = $full_address;
    }

    /**
     * @return string
     */
    public function getPostCode()
    {
        return $this->post_code;
    }

    /**
     * @param string $post_code
     */
    public function setPostCode($post_code)
    {
        $this->post_code = $post_code;
    }

    /**
     * @param mixed $company_address_id
     */
    public function setCompanyAddressId($company_address_id)
    {
        $this->company_address_id = $company_address_id;
    }

    /**
     * @return mixed
     */
    public function getCompanyAddressId()
    {
        return $this->company_address_id;
    }

    /**
     * @param int $country_id
     */
    public function setCountryId($country_id)
    {
        $this->country_id = $country_id;
    }

    /**
     * @param int $province_id
     */
    public function setProvinceId($province_id)
    {
        $this->province_id = $province_id;
    }

    /**
     * @param int $city_id
     */
    public function setCityId($city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @param int $kecamatan_id
     */
    public function setKecamatanId($kecamatan_id)
    {
        $this->kecamatan_id = $kecamatan_id;
    }

    /**
     * @param int $group_shipment
     */
    public function setGroupShipment($group_shipment)
    {
        $this->group_shipment = $group_shipment;
    }
    
     /**
     * @return mixed
     */
    public function getGroupShipment()
    {
        return $this->group_shipment;
    }    
    
    /**
     * @param int $carrier_id
     */
    public function setCarrierId($carrier_id)
    {
        $this->carrier_id = $carrier_id;
    }

     /**
     * @return mixed
     */
    public function getCarrierId()
    {
        return $this->carrier_id;
    }  

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderAddress[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderAddress
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_order_address", "TRIGGERED");
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    public function setFromArray($data_array = array())
    {
        if(empty($data_array['country'])) {
            $data_array['country']['country_id'] = 102; // indonesia
        }

        foreach($data_array as $key => $val) {
            $this->{$key} = $val;

            if($key == 'city') {
                $city = new \Models\MasterCity();
                $city->setFromArray($val);
                $this->city_id = isset($val['city_id']) ? $val['city_id'] : 0;
            }

            if($key == 'country') {
                $country = new \Models\MasterCountry();
                $country->setFromArray($val);
                $this->country_id = isset($val['country_id']) ? $val['country_id'] : 0;
            }

            if($key == 'kecamatan') {
                $kecamatan = new \Models\MasterKecamatan();
                $kecamatan->setFromArray($val);
                $this->kecamatan_id = isset($val['kecamatan_id']) ? $val['kecamatan_id'] : 0;
            }

            if($key == 'kelurahan') {
                $kelurahan = new \Models\MasterKelurahan();
                $kelurahan->setFromArray($val);
                $this->kelurahan_id = isset($val['kelurahan_id']) ? $val['kelurahan_id'] : 0;
            }

            if($key == 'province') {
                $province = new \Models\MasterProvince();
                $province->setFromArray($val);
                $this->province_id = isset($val['province_id']) ? $val['province_id'] : 0;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, true);

        unset($view['country_id']);
        unset($view['province_id']);
        unset($view['city_id']);
        unset($view['kecamatan_id']);
        unset($view['kelurahan_id']);
        unset($view['sales_order_id']);

        if(!empty($this->country_id)) {
            $view['country'] = $this->MasterCountry->toArray();
        }

        if(!empty($this->province_id) && !empty($this->MasterProvince)) {
            $view['province'] = $this->MasterProvince->toArray(['province_id','province_code','province_name']);
        }

        if(!empty($this->city_id) && !empty($this->MasterProvince)) {
            $view['city'] = $this->MasterCity->toArray(['city_id','city_name']);
        }

        if(!empty($this->kecamatan_id) && !empty($this->MasterProvince)) {
            $view['kecamatan'] = $this->MasterKecamatan->toArray(['kecamatan_id','kecamatan_name', 'coding', 'kecamatan_code', 'kode_jalur', 'sap_exp_code', 'geolocation']);
        }

        if(!empty($this->kelurahan_id) && !empty($this->MasterProvince)) {
            $view['kelurahan'] = $this->MasterKelurahan->toArray(['kelurahan_id', 'kelurahan_name', 'post_code', 'geolocation']);
        }

        $view['customer_address'] = $this->customer_address;
        $view['geolocation'] = $this->geolocation;
        $view['address_name'] = $this->address_name;

        return $view;
    }

    /**
     * @return array
     * @throws \Library\HTTPException
     */
    public function createData()
    {
        $responseContent = array();
        try {
            $this->useWriteConnection();
            $this->created_at = date('Y-m-d H:i:s');
            if ($this->create() === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                LogHelper::log("sales_order_address", "Sales Order Address save failed, error : " . json_encode($errMsg));
            }
        } catch (\Exception $e) {
            LogHelper::log("sales_order_address", "Sales Order Address update failed, error : " . $e->getMessage());
            $this->errors[] = "Update failed, please try again";
        }

        return;
    }

    public function generateAwb($params)
    {
        try {
            $sql = "
                SELECT
                    (SELECT carrier_code FROM shipping_carrier WHERE carrier_id = si.carrier_id LIMIT 1) AS carrier_code,
                    (SELECT city_name FROM master_city
                        WHERE city_id IN (
                        SELECT city_id FROM sales_order_address
                        WHERE sales_order_id = si.sales_order_id AND address_type = 'shipping') LIMIT 1) AS city_destination_name,
                    (SELECT total_qty FROM sales_invoice
                        WHERE sales_order_id = si.sales_order_id LIMIT 1) AS quantity,
                    (SELECT sum((select weight from product where product_id = (select product_id from product_variant where sku = sii.sku) limit 1) * sii.qty_ordered)
                        FROM sales_invoice_item as sii where invoice_id = si.invoice_id) as weight,
                    (SELECT sum(row_total) FROM sales_invoice_item
                        WHERE invoice_id = si.invoice_id) AS goods_value_are,
                    (SELECT concat(first_name, ' ', last_name) FROM sales_order_address
                        WHERE sales_order_id = si.sales_order_id AND address_type = 'shipping' LIMIT 1) AS recipient_name,
                    (SELECT province_name FROM master_province WHERE province_id = (
                        SELECT province_id
                        FROM sales_order_address
                        WHERE sales_order_id = si.sales_order_id AND address_type = 'shipping' LIMIT 1)) AS recipient_province_name,
                    (SELECT city_name FROM master_city WHERE city_id = (
                        SELECT city_id FROM sales_order_address WHERE sales_order_id = si.sales_order_id
                        AND address_type = 'shipping' LIMIT 1)) AS recipient_district_name,
                    (SELECT sap_exp_code FROM master_kecamatan WHERE kecamatan_id = (
                        SELECT kecamatan_id FROM sales_order_address WHERE sales_order_id = si.sales_order_id
                        AND address_type = 'shipping' LIMIT 1)) AS recipient_subdistrict_code,
                    (SELECT kecamatan_name FROM master_kecamatan WHERE kecamatan_id = (
                        SELECT kecamatan_id FROM sales_order_address WHERE sales_order_id = si.sales_order_id
                        AND address_type = 'shipping' LIMIT 1)) AS recipient_subdistrict_name,
                    (SELECT full_address FROM sales_order_address WHERE sales_order_id = si.sales_order_id
                        AND address_type = 'shipping' LIMIT 1) AS recipient_address,
                    (SELECT telephone FROM sales_order_address WHERE sales_order_id = si.sales_order_id
                        AND address_type = 'shipping' LIMIT 1) AS recipient_handphone_number
                FROM
                    sales_invoice AS si
                WHERE
                    invoice_no = '{$params['invoice_no']}'";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Db::FETCH_ASSOC);
                $dataArray = $result->fetchAll();
                $dataArray[0]['shipment_date_string'] = date("dmY");
                $dataArray[0]['shipment_time_string'] = date("H:i");

                if ($dataArray[0]['carrier_code'] == 'sap') {
                    $url = "http://182.23.64.40/sibs/sap/api/raw_data/awb";
                    $client = new \GuzzleHttp\Client();
                    /*
                     * untuk keperluan production ganti
                     * Username : RUPARUPA
                       Password  : Rupa2811
                     */
                    $res = $client->request('POST',$url, [
                        'auth' =>  ['MAJUBERSAMA', 'Majubersama987'],
                        'form_params' => [
                            'allocation_code'  => '1128',
                            'shipment_date_string' => $dataArray[0]['shipment_date_string'],
                            'shipment_time_string' => $dataArray[0]['shipment_time_string'],
                            'unique_reference_no' => $params['invoice_no'],
                            'company_id' => 'CGKN01627',
                            'company_name' => 'RUPARUPA',
                            'city_origin_name' => 'CIKARANG SELATAN', // kota
                            'city_destination_name' => 'BARANANGSIANG', // kota
                            'service_name' => 'REGULER',
                            'quantity' => intval($dataArray[0]['quantity']),
                            'weight' => $dataArray[0]['weight'],
                            'insurance_flag' => '0',
                            'cod_shipment_cost_flag' => '0',
                            'cod_goods_cost_flag' => '0',
                            'goods_is_document_flag' => '0',
                            'goods_need_packing_flag' => '0',
                            'goods_is_boxed_flag' => '0',
                            'goods_value_are' => $dataArray[0]['goods_value_are'],
                            'goods_is_high_values_flag' => '0',
                            'goods_is_electronic_flag' => '0',
                            'goods_is_dangerous_flag' => '0',
                            'goods_is_live_animal_flag' => '0',
                            'goods_is_live_plant_flag' => '0',
                            'goods_is_food_flag' => '0',
                            'goods_textile_flag' => '0',
                            'shipment_req_pickup_flag' => '0',
                            'shipper_name' => 'RUPARUPA',
                            'shipper_province_name' => 'JAWA BARAT',
                            'shipper_district_name' => 'KAB. BEKASI', // city
                            'shipper_subdistrict_code' => 'JB0612',
                            'shipper_subdistrict_name' => 'CIKARANG SELATAN', // kecamatan
                            'shipper_address' => 'Distribution Center PT.ODI, Komplek Pergudangan Kawan Lama, Jl. Industri Selatan Blok PP No. 4 Jababeka II, Desa Pasir Sari, Kecamatan Cikarang Selatan - Bekasi 17550',
                            'shipper_handphone_number' => '(021) 582-9191',
                            'recipient_name' => $dataArray[0]['recipient_name'],
                            'recipient_province_name' => $dataArray[0]['recipient_province_name'],
                            'recipient_district_name' => $dataArray[0]['recipient_district_name'],
                            'recipient_subdistrict_code' => $dataArray[0]['recipient_subdistrict_code'],
                            'recipient_subdistrict_name' => $dataArray[0]['recipient_subdistrict_name'],
                            'recipient_address' => $dataArray[0]['recipient_address'],
                            'recipient_handphone_number' => $dataArray[0]['recipient_handphone_number']
                        ]
                    ]);
                    return json_decode($res->getBody());
                }
            }
        }  catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return;
    }

    public function generateEntity(){
        $sql = "SELECT
                    first_name,
                    last_name,
                    (
                      SELECT country_code FROM master_country
                      WHERE country_id = a.country_id
                    ) AS country_code,
                    (
                      SELECT province_name FROM master_province
                      WHERE province_id = a.province_id
                    ) AS province_name,
                    (
                      SELECT city_name FROM master_city
                      WHERE city_id = a.city_id
                    ) AS city_name,
                    (
                      SELECT kecamatan_name FROM master_kecamatan
                      WHERE kecamatan_id = a.kecamatan_id
                    ) AS kecamatan_name,
                    (
                      SELECT kecamatan_code FROM master_kecamatan
                      WHERE kecamatan_id = a.kecamatan_id
                    ) AS kecamatan_code,
                    (
                      SELECT kode_jalur FROM master_kecamatan
                      WHERE kecamatan_id = a.kecamatan_id
                    ) AS kode_jalur,
                    (
                      SELECT kelurahan_name FROM master_kelurahan
                      WHERE kelurahan_id = a.kelurahan_id
                    ) AS kelurahan_name,
                    a.full_address,
                    a.post_code,
                    a.phone
                FROM sales_order_address a
                WHERE a.sales_order_id = '".$this->sales_order_id."' AND a.address_type = '".$this->address_type."'";
        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);
        if($result->numRows() > 0) {
            $result->setFetchMode(Db::FETCH_ASSOC);
            $hasil = $result->fetch();
            return $hasil;
        }else{
            return FALSE;
        }
    }    
}