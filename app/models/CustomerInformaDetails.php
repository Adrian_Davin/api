<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerInformaDetails extends \Models\BaseModel {

  protected $customer_informa_details_id;
  protected $customer_id;
  protected $informa_uid;
  protected $informa_customer_id;
  protected $informa_customer_member_no;
  protected $informa_customer_tmp_member_no;
  protected $session_id;
  protected $login_attempt;
  protected $ktp;
  protected $marital_status;
  protected $is_migrated;
  protected $updated_at;
  protected $created_at;
  protected $login_attempt_date;
  protected $forgot_pass_attempt;
  protected $forgot_pass_attempt_date;

  public function setCustomerInformaDetailsId($customer_informa_details_id) {
    $this->customer_informa_details_id = $customer_informa_details_id;
  }

  public function getCustomerInformaDetailsId()
  {
      return $this->customer_informa_details_id;
  }

  public function setInformaCustomerId($informa_customer_id)
  {
      $this->informa_customer_id = $informa_customer_id;
  }

  public function getInformaCustomerMemberNo()
  {
      return $this->informa_customer_member_no;
  }

  public function setInformaCustomerMemberNo($informa_customer_member_no)
  {
      $this->informa_customer_member_no = $informa_customer_member_no;
  }

  public function getCustomerToken()
  {
      return $this->customer_token;
  }

  public function setCustomerToken($customer_token) {
    $this->customer_token = $customer_token;
  }

  public function setLoginAttempt($login_attempt) {
    $this->login_attempt = $login_attempt;
  }

  public function getLoginAttempt()
  {
      return $this->login_attempt;
  }

  public function getUpdatedAt()
  {
      return $this->updated_at;
  }

  /**
   * @param string $updated_at
   */
  public function setUpdatedAt($updated_at)
  {
      $this->updated_at = $updated_at;
  }

  public function getKtp()
  {
      return $this->ktp;
  }

  /**
   * @param string $ktp
   */
  public function setKtp($ktp)
  {
      $this->ktp = $ktp;
  }

  public function getMaritalStatus()
  {
      return $this->marital_status;
  }
    /**
   * @param string $marital_status
   */
  public function setMaritalStatus($marital_status)
  {
      $this->marital_status = $marital_status;
  }

  public function getInformaCustomerTmpMemberNo()
  {
      return $this->informa_customer_tmp_member_no;
  }
    /**
   * @param string $informa_customer_tmp_member_no
   */
  public function setInformaCustomerTmpMemberNo($informa_customer_tmp_member_no)
  {
      $this->informa_customer_tmp_member_no = $informa_customer_tmp_member_no;
  }

  public function getIsMigrated()
  {
      return $this->is_migrated;
  }
    /**
   * @param string $is_migrated
   */
  public function setIsMigrated($is_migrated)
  {
      $this->is_migrated = $is_migrated;
  }

  public function initialize()
  {
    $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
  }

  /**
   * Returns table name mapped in the model.
   *
   * @return string
   */
  public function getSource()
  {
      return 'customer_informa_details';
  }

  /**
   * Allows to query a set of records that match the specified conditions
   *
   * @param mixed $parameters
   * @return Customer[]
   */
  public static function find($parameters = null)
  {
      return parent::find($parameters);
  }

  /**
   * Allows to query the first record that match the specified conditions
   *
   * @param mixed $parameters
   * @return Customer
   */
  public static function findFirst($parameters = null)
  {
      return parent::findFirst($parameters);
  }

  public function setFromArray($data_array = array())
  {
      $this->assign($data_array);

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
  }

  public function afterUpdate() {
    // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
    // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
    \Helpers\LogHelper::log("after_update_customer_informa", "TRIGGERED: " .json_encode($this->toArray()));
  }


  /**
   * Get the value of forgot_pass_attempt_date
   */
  public function getForgotPassAttemptDate()
  {
    return $this->forgot_pass_attempt_date;
  }

  /**
   * Set the value of forgot_pass_attempt_date
   *
   * @return  self
   */
  public function setForgotPassAttemptDate($forgot_pass_attempt_date)
  {
    $this->forgot_pass_attempt_date = $forgot_pass_attempt_date;

    return $this;
  }

  /**
   * Get the value of forgot_pass_attempt
   */
  public function getForgotPassAttempt()
  {
    return $this->forgot_pass_attempt;
  }

  /**
   * Set the value of forgot_pass_attempt
   *
   * @return  self
   */
  public function setForgotPassAttempt($forgot_pass_attempt)
  {
    $this->forgot_pass_attempt = $forgot_pass_attempt;

    return $this;
  }

  /**
   * Get the value of login_attempt_date
   */
  public function getLoginAttemptDate()
  {
    return $this->login_attempt_date;
  }

  /**
   * Set the value of login_attempt_date
   *
   * @return  self
   */
  public function setLoginAttemptDate($login_attempt_date)
  {
    $this->login_attempt_date = $login_attempt_date;

    return $this;
  }
}

?>