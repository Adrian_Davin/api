<?php

namespace Models;

use Library\Redis;

class ShippingCourierCredential extends \Models\BaseModel
{

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    protected $courier_account;

    /**
     * @return string
     */
    public function getCourierAccount()
    {
        return $this->courier_account;
    }

    /**
     * @param string $courier_account
     * @return $this
     */
    public function setCourierAccount($courier_account)
    {
        $this->courier_account = $courier_account;

        return $this;
    }

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $username;

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $password;

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $company_id;

    /**
     * @return string
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param string $company_id
     * @return $this
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;

        return $this;
    }

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $allocation_code;

    /**
     * @return string
     */
    public function getAllocationCode()
    {
        return $this->allocation_code;
    }

    /**
     * @param string $allocation_code
     * @return $this
     */
    public function setAllocationCode($allocation_code)
    {
        $this->allocation_code = $allocation_code;

        return $this;
    }

    protected $shipping_assignation;

    /**
     * @return mixed
     */
    public function getShippingAssignation()
    {
        return $this->shipping_assignation;
    }

    /**
     * @param mixed $shipping_assignation
     */
    public function setShippingAssignation($shipping_assignation)
    {
        $this->shipping_assignation = $shipping_assignation;
    }

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=false)
     */
    protected $company_code;

    /**
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param string $company_code
     * @return $this
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;

        return $this;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'shipping_courier_credential';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ShippingCourierCredential[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ShippingCourierCredential
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function saveToCache($params = array())
    {
        $courierData = self::find();

        $courierDataCache = [];
        foreach ($courierData as $courier) {

            $shippingAssignation = $courier->getShippingAssignation();
            $shippingAssignationList = explode(",",$shippingAssignation);
            $companyCode = $courier->getCompanyCode();
            foreach ($shippingAssignationList as $assignation) {
                $assignationCompanyCode = $assignation . '_' . $companyCode;
                $courierDataCache[$assignationCompanyCode] = [
                    'courier_account' => $courier->getCourierAccount(),
                    'company_code' => $courier->getCompanyCode(),
                    'username' => $courier->getUsername(),
                    'password' => $courier->getPassword(),
                    'company_id' => $courier->getCompanyId(),
                    'allocation_code' => $courier->getAllocationCode()
                ];
            }
        }

        if(!empty($courierDataCache)) {
            foreach ($courierDataCache as $key => $cache) {
                \Library\Redis\ShippingCourierCredential::setCourier($key, $cache);
            }
        }

        return true;
    }

    public function getFromCache($params = array())
    {
        return $courierData = \Library\Redis\ShippingCourierCredential::getCourier($params['shipping_assignation']);
    }

    public function deleteFromCache($params = array())
    {
        \Library\Redis\ShippingCourierCredential::deleteCourier($params['shipping_assignation']);

        return;
    }

}
