<?php

namespace Models;
use Phalcon\Db as Database;

class SalesCustomerGroup extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_customer_group_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $sales_order_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $group_id;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $member_number = '';

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $is_verified = 0;
    
    protected $expiration_date;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
    }

    /**
        * Returns table name mapped in the model.
        *
        * @return string
        */
    public function getSource()
    {
        return 'sales_customer_group';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
        
        return $view;
    }


    /**
     * Get the value of sales_customer_group_id
     *
     * @return  integer
     */ 
    public function getSalesCustomerGroupId()
    {
        return $this->sales_customer_group_id;
    }

    /**
     * Set the value of sales_customer_group_id
     *
     * @param  integer  $sales_customer_group_id
     *
     * @return  self
     */ 
    public function setSalesCustomerGroupId($sales_customer_group_id)
    {
        $this->sales_customer_group_id = $sales_customer_group_id;

        return $this;
    }

    /**
     * Get the value of sales_order_id
     *
     * @return  integer
     */ 
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * Set the value of sales_order_id
     *
     * @param  integer  $sales_order_id
     *
     * @return  self
     */ 
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;

        return $this;
    }

    /**
     * Get the value of group_id
     *
     * @return  integer
     */ 
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Set the value of group_id
     *
     * @param  integer  $group_id
     *
     * @return  self
     */ 
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;

        return $this;
    }

    /**
     * Get the value of member_number
     *
     * @return  string
     */ 
    public function getMemberNumber()
    {
        return $this->member_number;
    }

    /**
     * Set the value of member_number
     *
     * @param  string  $member_number
     *
     * @return  self
     */ 
    public function setMemberNumber(string $member_number)
    {
        $this->member_number = $member_number;

        return $this;
    }

    /**
     * Get the value of is_verified
     *
     * @return  integer
     */ 
    public function getIsVerified()
    {
        return $this->is_verified;
    }

    /**
     * Set the value of is_verified
     *
     * @param  integer  $is_verified
     *
     * @return  self
     */ 
    public function setIsVerified($is_verified)
    {
        $this->is_verified = $is_verified;

        return $this;
    }

    /**
     * Get the value of expiration_date
     */ 
    public function getExpirationDate()
    {
        return $this->expiration_date;
    }

    /**
     * Set the value of expiration_date
     *
     * @return  self
     */ 
    public function setExpirationDate($expiration_date)
    {
        $this->expiration_date = $expiration_date;

        return $this;
    }
}
