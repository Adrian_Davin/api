<?php

namespace Models;

class SalesInvoiceSap extends \Models\BaseModel
{

     /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('invoice_id', 'Models\SalesInvoice', 'invoice_id', array('alias' => 'InvoiceId'));

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_invoice_sap';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
