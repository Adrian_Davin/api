<?php

namespace Models;

use Phalcon\Mvc\Model\Validator\Email as Email;

class SupplierReport extends \Models\BaseModel
{

    protected $report_id;
    protected $supplier_id;
    protected $file_name;
    protected $report_type;
    protected $start_periode;


    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;


    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->belongsTo('supplier_id', 'Supplier', 'supplier_id', array('alias' => 'Supplier'));
        $this->getSource();
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'supplier_report';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SupplierUser[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SupplierUser
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getReport(){

        $filter = "";
        if(!empty($this->supplier_id)){
            if($this->supplier_id == "mp"){
                $filter = "";
            }else {
                $filter = "AND supplier_id = " . $this->supplier_id;
            }
        }

        if($this->report_type == "summary"){
            $filter = "";
            $sourceFile = $_ENV['MP_SUMMARY_REPORT_SRC'];
        }else{
            $sourceFile = $_ENV['MP_SETTLEMENT_REPORT_SRC'];
        }

        $supplierParam = array(
            "columns" => "CONCAT('<a href=\"$sourceFile',file_name,'\">',file_name,'</a>') as file_name",
            "conditions" => " report_type = '".$this->report_type."' ". $filter,
            "order" => " created_at DESC"
        );

        $data = $this::find($supplierParam);

        return $data;
    }


}
