<?php

namespace Models;

class MasterApp extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $master_app_id;

    /**
     *
     * @var string
     */
    protected $master_app_name;

    /**
     * Method to set the value of field master_app_id
     *
     * @param integer $master_app_id
     * @return $this
     */
    public function setMasterAppId($master_app_id)
    {
        $this->master_app_id = $master_app_id;

        return $this;
    }

    /**
     * Method to set the value of field master_app_name
     *
     * @param string $master_app_name
     * @return $this
     */
    public function setMasterAppName($master_app_name)
    {
        $this->master_app_name = $master_app_name;

        return $this;
    }

    /**
     * Returns the value of field master_app_id
     *
     * @return integer
     */
    public function getMasterAppId()
    {
        return $this->master_app_id;
    }

    /**
     * Returns the value of field master_app_name
     *
     * @return string
     */
    public function getMasterAppName()
    {
        return $this->master_app_name;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('master_app_id', 'Models\AdminUserAttributeValue', 'master_app_id', array('alias' => 'AdminUserAttributeValue'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_app';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterApp[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterApp
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
