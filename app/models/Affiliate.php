<?php

namespace Models;

class Affiliate extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $affiliate_id;

    /**
     *
     * @var integer
     */
    protected $affiliate_group_id;

    /**
     * @var integer
     */
    protected $customer_id;

    /**
     *
     * @var string
     */
    protected $first_name;

    /**
     *
     * @var string
     */
    protected $last_name;

    /**
     *
     * @var string
     */
    protected $phone_number;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var double
     */
    protected $balance;

    /**
     *
     * @var string
     */
    protected $gender;

    /**
     *
     * @var string
     */
    protected $account_type;

    /**
     *
     * @var string
     */
    protected $registerd_date;

    /**
     *
     * @var string
     */
    protected $interests;

    /**
     *
     * @var string
     */
    protected $social_media;

    /**
     *
     * @var string
     */
    protected $province;

    /**
     *
     * @var string
     */
    protected $city;
    
    /**
     *
     * @var string
     */
    protected $district;

    /**
     *
     * @var string
     */
    protected $postal_code;

    /**
     *
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $approved_date;

    /**
     *
     * @var string
     */
    protected $approved_by;

    /**
     *
     * @var string
     */
    protected $nik;

    /**
     *
     * @var string
     */
    protected $npwp;

    /**
     *
     * @var string
     */
    protected $ktp_url;


    /**
     *
     * @var string
     */
    protected $npwp_url;

    /**
     *
     * @var string
     */
    protected $created_at;

    /**
     * Get the value of customer_id
     */ 
    public function getCustomerID()
    {
        return $this->customer_id;
    }

    /**
     * Set the value of customer_id
     *
     * @return  self
     */ 
    public function setCustomerID($customer_id)
    {
        $this->customer_id = $customer_id;

        return $this;
    }

    /**
     * Get the value of affiliate_id
     */ 
    public function getAffiliateID()
    {
        return $this->affiliate_id;
    }

    /**
     * Set the value of affiliate_id
     *
     * @return  self
     */ 
    public function setAffiliateID($affiliate_id)
    {
        $this->affiliate_id = $affiliate_id;

        return $this;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'affiliate';
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Affiliate[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Affiliate
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
    
}