<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/21/2016
 * Time: 10:49 AM
 */

namespace Models;

class BaseModel extends \Phalcon\Mvc\Model
{
    private $_connection = array("master" => "dbMaster",
                                 "reader"  => "dbReader",
                                 "slave"  => "dbSlave",
                                 "revive" => "dbRevive");
    private $_activeDBConnection;

    protected $skip_attribute_on_update = [];
    protected $errorCode;
    protected $errorMessages = array();
    protected $forceUpdateFields = array();

    public function initialize()
    {
        parent::initialize();
    }

    public function onConstruct()
    {
        //Set active connection
        $this->useWriteConnection();

        // set connection service
        $this->_setMyConnectionService();

        $this->setWriteConnectionService($this->_connection['master']);
        $this->setReadConnectionService($this->_connection['slave']);

        $this->skip_attribute_on_update = array();
    }

    /**
     * @param array $attributes
     */
    public function setSkipAttributeOnUpdate($attributes = array())
    {
        $this->skip_attribute_on_update = []; // Clear the array
        if(empty($attributes)) {
            return;
        }

        // we need to always update this field
        unset($attributes['updated_at']);
        unset($attributes['last_change_password']);

        // unset ourself
        unset($attributes['skip_attribute_on_update']);

        foreach($attributes as $key => $val) {
            if($key[0] == "_") {
                unset($attributes[$key]);
            } else if( is_int($key) ){
                $this->skip_attribute_on_update[] = $val;
            } else {
                $this->skip_attribute_on_update[] = $key;
            }
        }
    }

    protected static function _createKey($parameters)
    {
        // create prefix
        $cachePrefix = str_replace("\\",":", lcfirst(get_called_class()));

        $uniqueKey[] = $cachePrefix;

        if(is_array($parameters)) {
            foreach ($parameters as $key => $value) {
                if (is_scalar($value)) {
                    $uniqueKey[] = $key . ":" . str_replace(' ', '', $value);
                } elseif (is_array($value)) {
                    $uniqueKey[] = $key . ":[" . self::_createKey($value) . "]";
                }
            }
        }else{
            $uniqueKey[] = str_replace(' ', '', $parameters);
        }

        return join(",", $uniqueKey);
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * Remove skip attrobute from array
     * @param array $attributes
     */
    public function unSetSkipAttributeOnUpdate($attributes = array())
    {
        foreach($attributes as $key => $val) {
            $thisKey = array_search($val,$this->skip_attribute_on_update);
            // SUPER BUG!
            // When array skip_attribute_on_update contains a unsetted attribute on the first element,
            // it will be ignored because of this if ($thisKey) will return false
            // if ($thisKey) {
            if (!is_bool($thisKey)) {
                unset($this->skip_attribute_on_update[$thisKey]);
            }
        }
    }

    public function toArray($columns = array(), $showEmpty = false)
    {
        $this->useReadOnlyConnection();
        if(!empty($columns)) {
            $view = parent::toArray($columns);
        } else {
            $view = parent::toArray();
        }

        if(!$showEmpty) {
            foreach($view as $key => $val) {
                if(($val == "" && $val !== 0) && !is_bool($val)) {
                    unset($view[$key]);
                }
            }
        }
        return $view;
    }

    private function _setMyConnectionService()
    {
        $this->setConnectionService($this->_connection[$this->_activeDBConnection]);
    }

    /**
     * @return mixed
     */
    public function getActiveDBConnection()
    {
        return $this->_activeDBConnection;
    }

    /**
     * @return mixed
     * get active db DI string for connecting
     */
    public function getConnection()
    {
        return $this->_connection[$this->_activeDBConnection];
    }

    public function useReadOnlyConnection()
    {
        $this->_activeDBConnection = "slave";
        $this->_setMyConnectionService();
    }

    public function useWriteConnection()
    {
        $this->_activeDBConnection = "master";
        $this->_setMyConnectionService();
    }

    public function useReviveConnection()
    {
        $this->_activeDBConnection = "revive";
        $this->_setMyConnectionService();
    }

    public function useReaderConnection()
    {
        $this->_activeDBConnection = "reader";
        $this->_setMyConnectionService();
    }

    public function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
        $this->skipAttributesOnCreate(['updated_at']);
    }

    public function beforeValidationOnUpdate() {
        $skippedEntity = [];
        $currentModel = $this->toArray(array(), true);
        // validate null value so it won't be inserted to DB
        foreach ($currentModel as $key => $val) {
            if (is_null($val) && !in_array($key, $this->forceUpdateFields)) {
                // add to skipped entity
                if ($key != 'updated_at') { // skip these
                    array_push($skippedEntity, $key);
                }
            }
        }
        // now merge skipped entities
        $mergedSkippedEntity = array_merge($skippedEntity, $this->skip_attribute_on_update);
        $this->skipAttributesOnUpdate($mergedSkippedEntity);
    }

    public function beforeUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }

    public function afterUpdate()
    {
        $primary_keys = $this->getModelsMetaData()->getPrimaryKeyAttributes($this);

        if(!empty($primary_keys)) {
            $param = [$primary_keys[0] => $this->{$primary_keys[0]}];
        }
        $redisKey = self::_createKey($param);

        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }

    public function getAction()
    {
        $primary_keys = $this->getModelsMetaData()->getPrimaryKeyAttributes($this);
        $action = "create";
        foreach($primary_keys as $primary_key) {
            if(!empty($this->{$primary_key})) {

                $action = "update";
                break; // if found one primary key, we need to update it not create
            }
        }

        return $action;
    }

    /**
     * @param string $log_file_name
     * @return bool
     * @throws \Library\HTTPException
     */
    public function saveData($log_file_name = "base_model_save_data.err", $state = "")
    {
        // Make sure we used write connection to save data to db
        $this->useWriteConnection();

        $action = $this->getAction();

        try {
            if($action == "create") {
                $saveStatus = $this->create();
            } else {
                $saveStatus = $this->update();
            }         
            
            if ($saveStatus === false) {
                $messages = $this->getMessages();
                if(!empty($messages)) {
                    $errMsg = array();
                    foreach ($messages as $message) {
                        $errMsg[] = $message->getMessage();
                    }

                    \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                    $this->errorCode = "RR301";
                    $this->errorMessages = "Failed when try to save data";

                    if($state !== ""){
                        $params_data = array(
                            "message" => "Failed at $state : Caused by: ".json_encode($errMsg),
                            "slack_channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                        );
                        $this->notificationError($params_data);
                    }
                } else {
                    \Helpers\LogHelper::log($log_file_name, "Save data failed, error : unknown error");
                    if($state !== ""){
                        $params_data = array(
                            "message" => "Failed at $state : Caused by: unknown error",
                            "slack_channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                            "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                        );
                        $this->notificationError($params_data);
                    }
                }

                $transactionManager = new \Phalcon\Mvc\Model\Transaction\Manager();
                $transactionManager->setDbService($this->getConnection());
                $is_have_transaction = $transactionManager->has();

                if($is_have_transaction) {
                    throw new \Exception("Rollback transaction");
                }

                return false;
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . $e->getMessage());
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }

    public function notificationError($params_data = array()){
        $notificationText = $params_data["message"];
        $params = [ 
            "channel" => $params_data["slack_channel"],
            "username" => $params_data["slack_username"],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }
}
