<?php

namespace Models;

class VoucherDeals extends \Models\BaseModel
{
    protected $voucher_deals_id;
    protected $identifier;
    protected $rule_id;
    protected $status;
    protected $platform;
    protected $discount_type;
    protected $voucher_amount;
    protected $percentage;
    protected $max_redemption;
    protected $final_amount;
    protected $user_type;
    protected $online_status;
    protected $offline_status;
    protected $online_quota;
    protected $offline_quota;
    protected $company_code;
    protected $title;
    protected $description;
    protected $tnc;
    protected $banner_image_small_url;
    protected $banner_image_small_title;
    protected $banner_image_large_url;
    protected $banner_image_large_title;
    protected $voucher_code_length;
    protected $voucher_code_prefix;
    protected $priority_list;
    protected $start_at;
    protected $expired_at;
    protected $created_at;
    protected $updated_at;
    protected $created_by;
    protected $updated_by;
    
    protected $errors;
    protected $messages;
    protected $data;

    public function getBannerImageSmallURL() {
        return $this->banner_image_small_url;
    }

    public function getBannerImageSmallTitle() {
        return $this->banner_image_small_title;
    }

    public function getBannerImageLargeURL() {
        return $this->banner_image_large_url;
    }
    
    public function getBannerImageLargeTitle() {
        return $this->banner_image_large_title;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getDiscountType() {
        return $this->discount_type;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'voucher_deals';
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VoucherDeals[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VoucherDeals
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
    
}
