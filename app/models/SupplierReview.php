<?php

namespace Models;

use \Library\Mongodb;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use \MongoDB\BSON\ObjectID as MongoDbObjectId;
use Phalcon\Db as Database;

class SupplierReview
{

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $review_id;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $supplier_id;

    protected $supplier_alias;

    protected $supplier_name;

    protected $customer_email;

    protected $customer_first_name;

    protected $customer_last_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $invoice_no;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $packaging_product;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $delivery_time;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $order_no;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $errors;

    protected $messages;
    protected $supplier_notification;

    protected $action;

    public function __construct()
    {
        $mongoDb = new Mongodb();
        $mongoDb->setCollection("supplier_review");
        $this->collection = $mongoDb->getCollection();
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'supplier_review';
    }

    /**
     * @return int
     */
    public function getReviewId()
    {
        return $this->review_id;
    }

    /**
     * @param int $review_id
     */
    public function setReviewId($review_id)
    {
        $this->review_id = $review_id;
    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param int $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return mixed
     */
    public function getSupplierAlias()
    {
        return $this->supplier_alias;
    }

    /**
     * @param mixed $supplier_alias
     */
    public function setSupplierAlias($supplier_alias)
    {
        $this->supplier_alias = $supplier_alias;
    }

    /**
     * @return mixed
     */
    public function getSupplierName()
    {
        return $this->supplier_name;
    }

    /**
     * @param mixed $supplier_name
     */
    public function setSupplierName($supplier_name)
    {
        $this->supplier_name = $supplier_name;
    }

    /**
     * @return mixed
     */
    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    /**
     * @param mixed $customer_email
     */
    public function setCustomerEmail($customer_email)
    {
        $this->customer_email = $customer_email;
    }

    /**
     * @return mixed
     */
    public function getCustomerFirstName()
    {
        return $this->customer_first_name;
    }

    /**
     * @param mixed $customer_first_name
     */
    public function setCustomerFirstName($customer_first_name)
    {
        $this->customer_first_name = $customer_first_name;
    }

    /**
     * @return mixed
     */
    public function getCustomerLastName()
    {
        return $this->customer_last_name;
    }

    /**
     * @param mixed $customer_last_name
     */
    public function setCustomerLastName($customer_last_name)
    {
        $this->customer_last_name = $customer_last_name;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function dataValidation($params){
        $validateAttribute = array("supplier_id","supplier_alias","supplier_name","customer_id","customer_email","customer_first_name","customer_last_name","order_no","invoice_no","packaging_product","delivery_time");

        $errors = array();
        foreach($params as $keyParam =>$param){
            if (in_array($keyParam, $validateAttribute)){
                if(empty($param)){
                    $errors[] = $keyParam." required";
                }
            }
        }
        if(count($errors)>0){
            $this->setErrors($errors);
            return false;
        }else{
            return true;
        }
    }

    public function reviewList($customeParams){

        $search_param = array();
        if(!empty($this->review_id)){
            $id = new MongoDbObjectId($this->review_id);
            $search_param['_id'] = $id;
        }

        if(!empty($this->supplier_id)){
            $supplierId = $this->supplier_id;
            $search_param['supplier_id'] = (int)$supplierId;
        }

        if(!empty($this->supplier_alias)){
            $supplierAlias = $this->supplier_alias;
            $search_param['supplier_alias'] = (string)$supplierAlias;
        }

        if(!empty($this->invoice_no)){
            $invoiceNo = $this->invoice_no;
            $search_param['invoice_no'] = (string)$invoiceNo;
        }

        if(!empty($this->order_no)){
            $orderNo = $this->order_no;
            $search_param['order_no'] = (string)$orderNo;
        }

        if(!empty($this->customer_email)){
            $customerEmail = $this->customer_email;
            $search_param['customer_email'] = (string)$customerEmail;
        }

        if(!empty($this->sku)){
            $sku = $this->sku;
            $search_param['sku'] = (string)$sku;
        }

        if(!empty($this->created_at)){
            $createdDate = $this->created_at;
            $search_param['created_at'] = $createdDate;
        }

        $pagination = array();
        if(isset($customeParams['limit'])) {
            $pagination['limit'] = (int)$customeParams['limit'];
        }

        if(isset($customeParams['offset'])) {
            $pagination['skip'] = (int)$customeParams['offset'];
        }

        $pagination['sort']  = array('created_at' => 1);
        $reviewList = $this->collection->find($search_param,$pagination);

        $reviewData = array();
        $data = array();
        foreach($reviewList as $review){
            $reviewOid = (array)$review['_id'];
            $reviewId  = $reviewOid['oid'];
            unset($review['_id']);
            $data[] = array("review_id" => $reviewId)+$review;
        }
        $reviewData['data'] = $data;
        $reviewData['recordsTotal'] = $this->collection->count($search_param);
        return $reviewData;
    }

    public function saveReview($params){
        try {
            $validate = $this->dataValidation($params);
            $params['packaging_product'] = isset($params['packaging_product'])?(int)$params['packaging_product'] : 0;
            $params['delivery_time']    = isset($params['delivery_time'])?(int)$params['delivery_time'] : 0;
            $params['supplier_id']      = isset($params['supplier_id'])?(int)$params['supplier_id'] : 0;
            $params['customer_id']      = isset($params['customer_id'])?(int)$params['customer_id'] : 0;

            $now = date("Y-m-d H:i:s");
            if($validate == true) {
                if (!empty($params['review_id'])) {

                    $this->setReviewId($params['review_id']);
                    $id = new MongoDbObjectId($params['review_id']);
                    unset($params['review_id']);

                    $review = $this->reviewList($params);
                    $params['created_at'] = $review['data'][0]['created_at'];
                    $params['updated_at'] = $now;
                    $saveReview = $this->collection->findOneAndReplace(['_id' => $id], $params);
                    if ($saveReview) {
                        $this->setMessages("Update succes");
                        $this->messages = "Update succes";
                    } else {

                    }
                } else {
                    $params['created_at'] = $now;
                    $params['updated_at'] = "";
                    $saveReview = $this->collection->insertOne($params);
                    if ($saveReview) {
                        $this->setMessages("Insert succes");
                        $this->messages = "Insert succes";
                    }
                }
            }else{
                return;
            }
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

    }

    public function supplierReviewAvg(){


        if(!empty($this->supplier_id)){
            $supplier_id = (int)$this->supplier_id;
            $filter[] = array(
                '$match' => array("supplier_id" => $supplier_id)
            );
        }

        $hasil = array();
        $filter[] =
            array(
                '$group' => array(
                    "_id" => array("supplier_id" => '$supplier_id'),
                    'average_packaging_product' => array('$avg' => '$packaging_product'),
                    'average_delivery_time' => array('$avg' => '$delivery_time')
                )
            );
        $average = $this->collection->aggregate($filter);

        foreach($average as $supplierAVG){
            $data['supplier_id'] = $supplierAVG['_id']['supplier_id'];
            $supplierAVG['average_packaging_product'] = number_format($supplierAVG['average_packaging_product'],2);
            $supplierAVG['average_delivery_time'] = number_format($supplierAVG['average_delivery_time'],2);
            $data['average_total'] = number_format(($supplierAVG['average_packaging_product'] + $supplierAVG['average_delivery_time']) / 2,2);
            unset($supplierAVG['_id']);
            $hasil[] = $supplierAVG + $data;


        }


        return $hasil;
    }

    public function supplierNotification(){

        $search_param = array();
        $search_param['supplier_notification'] = array('$exists'=>false);
        $reviewList = $this->collection->find($search_param);
        $groupReview = array();

        $products = new \Models\ProductReview();
        foreach($reviewList as $review) {

            /*
             * To do get product review base on supplier
             */
            if(!empty($review['invoice_no'])) {
                $reviewProduct = array();
                $params['invoice_no'] = $review['invoice_no'];
                $products->setFromArray($params);
                $producList = $products->reviewList();
                if(!empty($producList['list'])) {
                    foreach ($producList['list'] as $product) {
                        $reviewProduct[] = $product;
                    }
                }
            }

            /*
             * Combine supplier review with its products
             */
            if(!empty($review['supplier_id'])) {
                $review['products'] = $reviewProduct;
                $groupReview[$review['supplier_id']][] = $review;
            }

        }

        //everyday supplier review
        if(!empty($groupReview)) {
            $mailLib = new \Library\Email();
            $templateID = \Helpers\GeneralHelper::getTemplateId("mp_review_notification", "ODI");
            $mailLib->setName();
            $mailLib->setCompanyCode("ODI");
            $mailLib->setEmailSender();
            $mailLib->setEmailTag("mp_review_customer_to_seller");
            $mailLib->setTemplateId($templateID);
            $mailLib->sendSupplierReviewNotificationEmail($groupReview);

            /*
             * update  supplier_review supplier_notification status
             */

            foreach ($groupReview as $uReviewList) {
                foreach ($uReviewList as $uReview) {

                    foreach ($uReview['products'] as $uProductReview) {
                        $uProductReview['supplier_notification'] = 1;
                        $products->saveReview($uProductReview);
                    }

                    $reviewOid = (array)$uReview['_id'];
                    $reviewId = $reviewOid['oid'];
                    unset($uReview['_id']);
                    unset($uReview['products']);
                    $uReview['supplier_notification'] = 1;
                    $data = array("review_id" => $reviewId) + $uReview;
                    unset($this->supplier_id);
                    unset($this->created_at);
                    $this->saveReview($data);
                }
            }

            $this->messages = "success";
        }else{
            $this->messages = "Data Not Found";
        }

    }


    public function customerNotification($params){

        $salesOrder = new \Models\SalesInvoice();
        $now = date("Y-m-d H:i:s");

        $startDate = !empty($params['start_date'])? $params['start_date'] : date("Y-m-d",strtotime("-2 day"))." 00:00:00";
        $endDate = date("Y-m-d 23:59:59");

        $sql = "SELECT
                *,
                (SELECT GROUP_CONCAT(customer_firstname,' ',customer_lastname) FROM sales_customer WHERE sales_customer.sales_order_id = sales_invoice.sales_order_id LIMIT 1) as customer_name,
                (SELECT customer_email FROM sales_customer WHERE sales_customer.sales_order_id = sales_invoice.sales_order_id LIMIT 1) as customer_email
                FROM sales_invoice
                WHERE invoice_id IN (
                    SELECT invoice_id FROM sales_shipment WHERE updated_at >= '$startDate' AND updated_at <= '$endDate'
                  )
                AND invoice_status = 'received'
                AND invoice_no NOT IN (SELECT invoice_no FROM sales_invoice_review_notification WHERE status = 1)
        ";

        $salesOrder->useReadOnlyConnection();
        $result = $salesOrder->getDi()->getShared($salesOrder->getConnection())->query($sql);
        if($result->numRows() > 0) {
            $result->setFetchMode(Database::FETCH_ASSOC);
            $dataOrderComplete = $result->fetchAll();

            $invoiceItems = new \Models\SalesInvoiceItem();
            $productImages = new \Models\ProductImages();
            foreach ($dataOrderComplete as $dataOrder) {
                $dataItems = $invoiceItems->find(
                    array(
                        'conditions' => 'invoice_id = '.$dataOrder['invoice_id'],
                    )
                );
                $dataItems = !empty($dataItems)? $dataItems->toArray(): array();

                $dataOrder['products'] = array();
                foreach($dataItems as $item) {
                    $dataImages = $productImages->findFirst(
                        array(
                            'conditions' => 'product_variant_id = (SELECT \Models\ProductVariant.product_variant_id FROM \Models\ProductVariant WHERE \Models\ProductVariant.sku = "'.$item['sku'].'") AND angle = 1',
                        )
                    );

                    $dataImages = !empty($dataImages)? $dataImages->toArray() : array();

                    $dataOrder['products'][] = array(
                        "image" => $_ENV['CUSTOMER_REVIEW_IMAGE'].$dataImages['image_url'],
                        "name" => $item['name'],
                        "sku" => $item['sku'],
                        "qty" => $item['qty_ordered']
                    );
                }
                $dataOrder['url_review'] = "http://www.ruparupa.com/quick-review/invoiceNo=".$dataOrder['invoice_no']."&email=".$dataOrder['customer_email'];

                //everyday supplier review
                $mailLib = new \Library\Email();
                $templateID = \Helpers\GeneralHelper::getTemplateId("customer_review_notification", $dataOrder['company_code']);
                $mailLib->setName();
                $mailLib->setCompanyCode($dataOrder['company_code']);
                $mailLib->setEmailTag("cust_review_rating");
                $mailLib->setEmailSender();
                $mailLib->setTemplateId($templateID);
                $mailLib->sendCustomerReviewNotificationEmail($dataOrder);

                $updateNotification = "INSERT INTO sales_invoice_review_notification (invoice_no, status, updated_at) VALUES ('".$dataOrder['invoice_no']."',1,'".$now."') ON DUPLICATE KEY UPDATE status = 1, updated_at = '".$now."'";
                $queryUpdate = $salesOrder->getDi()->getShared($salesOrder->getConnection())->query($updateNotification);
            }

            $this->messages = "success";
        }else{
            $this->messages = "Data Not Found";
        }

    }


    /*
    public function supplierReviewAvg(){
        $search_param = array();
        if(!empty($this->supplier_id)){
            $supplierId = $this->supplier_id;

            $packagingProduct = array();
            $reviewList = $this->collection->find($search_param);
            foreach($reviewList as $review){
                $review = (array)$review;
                $packagingProduct[] = $review['packaging_product'];
                $deliveryTime[] = $review['delivery_time'];
                $totalRating[] = $review['packaging_product']+$review['delivery_time'];
            }

            $packagingProduct = array_filter($packagingProduct);
            $deliveryTime = array_filter($deliveryTime);
            $totalRating = array_filter($totalRating);

            $AvgPackagingProduct = array_sum($packagingProduct)/count($packagingProduct);
            $AvgdeliveryTime = array_sum($deliveryTime)/count($deliveryTime);
            $AvgTotalRating = (($AvgPackagingProduct+$AvgdeliveryTime)/2);

            $average = array(
                "average_packaging_product" => $AvgPackagingProduct,
                "average_delivery_time" => $AvgdeliveryTime,
                "average_total" => $AvgTotalRating
            );

            return $average;

        }
    }
    */

}
