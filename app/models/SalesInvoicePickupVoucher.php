<?php

namespace Models;

use Helpers\LogHelper;
class SalesInvoicePickupVoucher extends \Models\BaseModel
{
    protected $id;
    protected $invoice_id;
    protected $invoice_no;
    protected $invoice_suffix;
    protected $pickup_voucher;
    protected $mop_type;
    protected $amount;
    protected $status;

    public function getSource()
    {
        return 'sales_invoice_pickup_voucher';
    }
    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {        
        $this->belongsTo('invoice_id', 'Models\SalesInvoice', 'invoice_id', array('alias' => 'SalesInvoice'));
    }  

    public function setFromArray($dataArray = array())
    {
        foreach($dataArray as $key => $val) {
            $this->{$key} = $val;
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}