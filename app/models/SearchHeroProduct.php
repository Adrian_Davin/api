<?php
/**
 * Created by PhpStorm.
 * User: iwan
 * Date: 30/08/18
 * Time: 9:57
 */

namespace Models;


class SearchHeroProduct extends \Models\BaseModel
{
    /**
     * @var $search_hero_product_id
     */
    protected $search_hero_product_id;

    protected $keyword;
    protected $hero_product;
    protected $status;
    protected $created_at;
    protected $updated_at;

    public function onConstruct()
    {
        parent::onConstruct();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'search_hero_product';
    }

    /**
     * @return mixed
     */
    public function getSearchHeroProductId()
    {
        return $this->search_hero_product_id;
    }

    /**
     * @param mixed $search_hero_product_id
     */
    public function setSearchHeroProductId($search_hero_product_id)
    {
        $this->search_hero_product_id = $search_hero_product_id;
    }

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return mixed
     */
    public function getHeroProduct()
    {
        return $this->hero_product;
    }

    /**
     * @param mixed $hero_product
     */
    public function setHeroProduct($hero_product)
    {
        $this->hero_product = $hero_product;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerAddress
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
            if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

}