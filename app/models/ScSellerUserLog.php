<?php

namespace Models;

class ScSellerUserLog extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $sc_seller_user_log_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $supplier_user_id;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $action_group;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    public $action;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $ip_address;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    public $result;

    /**
     *
     * @var string
     * @Column(type="string", length=225, nullable=true)
     */
    public $full_action_name;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $short_details;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $full_details;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('supplier_user_id', 'Models\SupplierUser', 'supplier_user_id', array('alias' => 'SupplierUser'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sc_seller_user_log';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ScSellerUserLog[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ScSellerUserLog
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
