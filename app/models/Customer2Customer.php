<?php

namespace Models;

class Customer2Customer extends \Models\BaseModel
{
    protected $customer_2_customer_id;
    protected $customer_id_odi;
    protected $customer_id_ahi;
    protected $customer_id_hci;
    protected $customer_id_slm;
    protected $status;
    protected $created_at;
    protected $updated_at;
   
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('customer_id_odi', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
        $this->belongsTo('customer_id_ahi', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_2_customer';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customer2Group[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customer2Group
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        foreach ($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $group_name = $this->CustomerGroup->getGroupName();
        $view[$group_name] = $this->member_number;

        return $view;
    }

    public function getCustomer2customerId() {
        return $this->customer_2_customer_id;
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_customer2customer", "TRIGGERED: " .json_encode($this->toArray()));
    }

    public function saveData($log_file_name = "error", $state = "")
    {
        try {
            if ($this->save() === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages = "Failed when try to save data";

                if($transaction = $this->get()) {
                    $transaction->rollback(
                        "Cannot save data, rollback"
                    );

                    throw new \Exception("Rollback transaction");
                }

                return false;
            }

            return true;
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }
    }
}
