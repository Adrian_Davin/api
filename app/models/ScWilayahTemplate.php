<?php

namespace Models;

class ScWilayahTemplate extends \Models\BaseModel
{

    protected $wilayah_template_id;
    protected $supplier_id;
    protected $name;
    protected $status;

    protected $created_at;
    protected $updated_at;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getWilayahTemplateId()
    {
        return $this->wilayah_template_id;
    }

    /**
     * @param int $wilayah_template_id
     */
    public function setWilayahTemplateId($wilayah_template_id)
    {
        $this->wilayah_template_id = $wilayah_template_id;
    }

    /**
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param mixed $seller_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('wilayah_template_id', 'Models\ScWilayahMapping', 'wilayah_template_id', array('alias' => 'ScWilayahMapping'));
        $this->belongsTo('supplier_id', 'Models\Supplier', 'supplier_id', array('alias' => 'Supplier'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sc_wilayah_template';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function uploadRegionToProduct($params = array()){
        $productModel = new \Models\Product();
        $in_value = $params['all_sku'];
        $string_value = $params['template_id']; // ownfleet_template_id
        $arr_in = array();

        foreach ($in_value as $key => $val){ // IN statement in query
            $arr_in[] = "'".$val."'";
        }

        $in_string = implode(",", $arr_in);
        $in_string = '('.$in_string.')';

        $sql  = "SELECT p.product_id FROM product p, product_variant pv WHERE pv.sku IN ".$in_string." AND p.product_id = pv.product_id";

        $this->useReadOnlyConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        $result->setFetchMode(
            \Phalcon\Db::FETCH_ASSOC
        );

        $product_id = $result->fetchAll();
        $arr = array();
        $arr_final = array();

        foreach ($product_id as $key => $val){
            $arr[] = "WHEN ".$val['product_id']." THEN ";
        }

        foreach ($string_value as $key => $val){
            $arr_final[] = $arr[$key]."'$val'";
        }

        $string = implode("\n", $arr_final);

        $sql = "UPDATE product SET wilayah_template_id = ( CASE product_id ".$string." END)";
        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        $count = $result->numRows();

        if($count > 0){
            foreach ($in_value as $key => $val){
                $nsq = new \Library\Nsq();
                $message = [
                    "data" => ["sku" => $val],
                    "message" => "createCache"
                ];
                $nsq->publishCluster('product', $message);
            }

        }
        return $count ;
    }



}
