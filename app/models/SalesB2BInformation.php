<?php

namespace Models;

class SalesB2BInformation extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_b2b_information_id;

    protected $sales_order_id;
    protected $po_file;
    protected $po_name; 
    protected $is_tax_invoice;
    protected $remark;
    protected $order_type;
    protected $so_sap_no;
    protected $so_sap_status;
    protected $request_delivery_date;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_b2b_information';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
    } 

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesB2bInformation
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    public function getSalesB2BInformationID() {
        return $this->sales_b2b_information_id;
    }

    public function setSalesB2BInformationID($sales_b2b_information_id) {
        $this->sales_b2b_information_id = $sales_b2b_information_id;
    }

    public function getSalesOrderID() {
        return $this->sales_order_id;
    }

    public function setSalesOrderID($sales_order_id) {
        $this->sales_order_id = $sales_order_id;
    }

    public function getPoFile() {
        return $this->po_file;
    }

    public function setPoFile($po_file) {
        $this->po_file = $po_file;
    }

    public function getPoName() {
        return $this->po_name;
    }

    public function setPoName($po_name) {
        $this->po_name = $po_name;
    }

    public function getIsTaxInvoice() {
        return $this->is_tax_invoice;
    }

    public function setIsTaxInvoice($is_tax_invoice) {
        $this->is_tax_invoice = $is_tax_invoice;
    }

    public function getRemark() {
        return $this->remark;
    }

    public function setRemark($remark) {
        $this->remark = $remark;
    }

    public function getOrderType() {
        return $this->order_type;
    }

    public function setOrderType($order_type) {
        $this->order_type = $order_type;
    }

    public function getSoSapNumber() {
        return $this->so_sap_no;
    }

    public function setSoSapNumber($so_sap_no) {
        $this->so_sap_no = $so_sap_no;
    }

    public function getSoSapStatus() {
        return $this->so_sap_status;
    }

    public function setSoSapStatus($so_sap_status) {
        $this->so_sap_status = $so_sap_status;
    }

    public function getRequestDeliveryDate() {
        return $this->request_delivery_date;
    }

    public function setRequestDeliveryDate($request_delivery_date) {
        $this->request_delivery_date = $request_delivery_date;
    }
}