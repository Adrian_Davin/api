<?php

namespace Models;

class ShippingCarrier extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $carrier_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $carrier_name;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $carrier_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    public $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('carrier_id', 'Models\ShippingRate', 'carrier_id', array('alias' => 'ShippingRate'));
    }

    /**
     * @return string
     */
    public function getCarrierName()
    {
        return $this->carrier_name;
    }

    /**
     * @return string
     */
    public function getCarrierCode()
    {
        return $this->carrier_code;
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'shipping_carrier';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {

        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

    public static function findWithoutCache($parameters = null)
    {
        return parent::find($parameters);
    }

}
