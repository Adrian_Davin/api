<?php

namespace Models;

use Helpers\GeneralHelper;
use Helpers\ProductHelper;
use Library\Elastic;
use Phalcon\Db as Database;
use Phalcon\Mvc\Model;


/**
 * @property \Phalcon\Mvc\Model\Resultset|Model Product
 * @property \Phalcon\Mvc\Model\Resultset|Model ProductPrice
 */
class ProductVariant extends \Models\BaseModel
{

    protected $module;

    protected $role_id;

    protected $user_id;

    protected $supplier_id;

    protected $ownfleet_template_id;

    protected $priority;

    /**
     * @return mixed
     */
    public function getOwnfleetTemplateId()
    {
        return $this->ownfleet_template_id;
    }

    /**
     * @param mixed $ownfleet_template_id
     */
    public function setOwnfleetTemplateId($ownfleet_template_id)
    {
        $this->ownfleet_template_id = $ownfleet_template_id;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_variant_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $attribute_variant_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $value;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $sku;

    /**
     * @var
     */
    protected $sku_seller;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $is_in_stock;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     * @var
     */
    protected $status_content;

    protected $status_image;

    protected $status_buyer;

    protected $label;

    protected $gtin;

    protected $publish_date;

    /**
     * @return mixed
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * @param mixed $publish_date
     */
    public function setPublishDate($publish_date)
    {
        $this->publish_date = $publish_date;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @var
     */
    protected $status_mp;

    /**
     * @var
     */
    protected $status_buyer_mp;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * @var \Models\Product\Prices\Collection
     */
    protected $pricesObj;

    /**
     * @var \Models\AttributeSap
     */
    protected $attributeSapObj;

    /**
     * @var \Models\MasterRejectReason
     */
    protected $rejectReasonObj;

    /**
     * @var $imagesObj \Models\Product\Images\Collection
     */
    protected $imagesObj;

    /**
     * @var $videosObj \Models\Product\Videos\Collection
     */
    protected $videosObj;

    /**
     * @var \Models\Product\ProductVariant\Attribute\Collection
     */
    protected $variant_attribute;

    protected $master_app_id;

    protected $errors;
    protected $messages;

    protected $params;

    protected $whiteList;

    protected $category_id;

    protected $data;

    protected $list_type;

    protected $manufacture_no;

    /**
     * @return mixed
     */
    public function getManufactureNo()
    {
        return $this->manufacture_no;
    }

    /**
     * @param mixed $manufacture_no
     */
    public function setManufactureNo($manufacture_no)
    {
        $this->manufacture_no = $manufacture_no;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        /*$this->hasMany('product_variant_id', 'Models\ProductVariantAttribute', 'product_variant_id', array('alias' => 'ProductVariantAttribute'));
        $this->hasMany('product_variant_id', 'Models\ProductImages', 'product_variant_id', array('alias' => 'ProductImages'));
        $this->hasMany('product_variant_id', 'Models\ProductVideos', 'product_variant_id', array('alias' => 'ProductVideos'));
        $this->hasMany('product_variant_id', 'Models\ProductPrice', 'product_variant_id', array('alias' => 'ProductPrice'));
        $this->hasOne('sku', 'Models\AttributeSap', 'sku', array('alias' => 'AttributeSap'));*/
        $this->belongsTo('product_id', 'Models\Product', 'product_id', array('alias' => 'Product',"reusable" => true));
    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->whiteList = array();
        $this->master_app_id = 2;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param mixed $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_variant';
    }

    /**
     * @return int
     */
    public function getProductVariantId()
    {
        return $this->product_variant_id;
    }

    /**
     * @param int $product_variant_id
     */
    public function setProductVariantId($product_variant_id)
    {
        $this->product_variant_id = $product_variant_id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getAttributeVariantId()
    {
        return $this->attribute_variant_id;
    }

    /**
     * @param int $attribute_variant_id
     */
    public function setAttributeVariantId($attribute_variant_id)
    {
        $this->attribute_variant_id = $attribute_variant_id;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return int
     */
    public function getIsInStock()
    {
        return $this->is_in_stock;
    }

    /**
     * @param int $is_in_stock
     */
    public function setIsInStock($is_in_stock)
    {
        $this->is_in_stock = $is_in_stock;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param mixed $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductVariant[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductVariant
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        unset($dataArray['limit']);
        unset($dataArray['offset']);

        if (isset($dataArray['total_qty_sorting'])) {
            unset($dataArray['total_qty_sorting']);
        }

        if (isset($dataArray['name_keyword'])) {
            unset($dataArray['name_keyword']);
        }

        if (isset($dataArray['sku_keyword'])) {
            unset($dataArray['sku_keyword']);
        }

        if(isset($dataArray['master_app_id'])){
            if($dataArray['master_app_id']==$_ENV['MASTER_APP_ID_SELLER']){
                unset($dataArray['name']);
            }
        }

        $variantAttributes = array();
        foreach ($dataArray as $key => $val) {

            if(is_string($val) == true){
                $val = str_replace("'","&apos;",$val);
            }

            if (property_exists($this, $key)) {
                $this->{$key} = $val;

                // input is send but value is empty, we still need to update that fields
                if($val === 0 || $val == "") {
                    $this->whiteList[] = $key;
                }
            } else {
                if($key == 'prices') {
                    $prices = array();
                    foreach($dataArray['prices'] as $price) {
                        $pricesObj = new \Models\ProductPrice();
                        if(isset($dataArray['product_variant_id'])) {
                            $price['product_variant_id'] = $dataArray['product_variant_id'];
                        }

                        $pricesObj->setFromArray($price);
                        $prices[] = $pricesObj;
                    }

                    $this->pricesObj = new \Models\Product\Prices\Collection($prices);
                    continue;
                }

                if($key == 'images') {
                    $images = array();
                    foreach($dataArray['images'] as $angle => $image) {
                        if(isset($dataArray['product_variant_id'])) {
                            $image['product_variant_id'] = $dataArray['product_variant_id'];
                        }

                        $imageObj = new \Models\ProductImages();
                        $imageObj->setFromArray($image);
                        $images[] = $imageObj;
                    }

                    $this->imagesObj = new \Models\Product\Images\Collection($images);
                    continue;
                }

                if($key == 'videos') {
                    $videos = array();
                    foreach($dataArray['videos'] as $key => $video) {
                        if(isset($dataArray['product_variant_id'])) {
                            $video['product_variant_id'] = $dataArray['product_variant_id'];
                        }

                        $videoObj = new \Models\ProductVideos();
                        $videoObj->setFromArray($video);
                        $videos[] = $videoObj;
                    }

                    $this->videosObj = new \Models\Product\Videos\Collection($videos);
                    continue;
                }

                if($key == 'reject_reason') {
                    $rejectReason = new \Models\MasterRejectReason();
                    $rejectReason->setFromArray($val);
                    $this->rejectReasonObj = $rejectReason;
                    continue;
                }

                if($key == 'attribute_sap') {
                    $attributeSap = new \Models\AttributeSap();
                    $attributeSap->setFromArray($val);
                    $this->attributeSapObj = $attributeSap;
                    continue;
                }

                // If property not exist and not part of images and prices, assume it's a attribute
                $attrVariant = $val;
                if (isset($attrVariant['code'])) $attrVariant['code'] = $key;

                if(isset($dataArray['product_variant_id'])) {
                    $attrVariant['product_variant_id'] = $dataArray['product_variant_id'];
                }
                if(isset($val['value'])) {
                    $attrVariant['option_value'] = $val['value'];
                    unset($attrVariant['value']);
                }

                $variantAttribute = new \Models\ProductVariantAttribute();
                $setStatus = $variantAttribute->setFromArray($attrVariant);

                if($setStatus) {
                    $variantAttributes[] = $variantAttribute;
                }
            }
        }

        $this->variant_attribute = new \Models\Product\ProductVariant\Attribute\Collection($variantAttributes);
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        if(isset($view['gtin'])){
            $gtin = array();
            // if gtin exists, explode with delimiter ,
            if ($view['gtin'] != '') {
                $exGtins = explode(',', $view['gtin']);
                foreach($exGtins as $exGtin){
                    $gtin[] = array(
                        'gtin' => $exGtin
                    );
                }
            }
            unset($view['gtin']);
            $view['gtins'] = $gtin;

        }

        if(!empty($this->variant_attribute)) {
            $attribute = $this->variant_attribute->getDataArray();
            $view = array_merge($view, $attribute);
        }

        if(!empty($this->pricesObj)) {
            $view['prices'] = $this->pricesObj->getDataArray(array("price_id", "price", "special_price", "special_from_date", "special_to_date"),true);
        }

        if(!empty($this->imagesObj)) {
            $view['images'] = $this->imagesObj->getDataArray(array("image_id","image_url","image_beta_url","status","angle"),true);
        }

        if(!empty($this->videosObj)) {
            $view['videos'] = $this->videosObj->getDataArray(array("product_video_id","video_url","priority","status"),true);
        }

        if(!empty($this->attributeSapObj)) {
            $view['attribute_sap'] = $this->attributeSapObj->getDataArray(array("name_sap","department_bu","basic_data_text","characteristic","vendor_source_list","department_sisco","department_sisco_desc"),true);
        }
        if(!empty($this->rejectReasonObj)) {
            $view['reject_reason'] = $this->rejectReasonObj->getDataArray();
        }

        unset($view["created_at"]);
        unset($view["updated_at"]);

        return $view;
    }

    public function updateBatch($method = false){
        try {
            if($method == 'truncate_price'){
                $sql = "TRUNCATE TABLE temp_pricezone";
            }elseif($method == 'truncate_pricezone'){
                $sql = "TRUNCATE TABLE price_zone";
            }elseif($method == 'delete_temp_not_exist'){
                $sql = "delete from temp_pricezone where sku not in (
                            select sku from product_variant
                        )";
            }elseif($method == 'delete_temp_not_exist'){
                $sql = "delete from temp_pricezone where sku not in (
                            select sku from product_variant
                        )";
            }elseif($method == 'delete_temp_less_special'){
                $sql = "delete from temp_pricezone where price < special_price";
            }elseif($method == 'delete_temp_less_10'){
                $sql = "delete from temp_pricezone where price < 10 or special_price < 10";
            }elseif($method == 'select_temp_price'){
                $sql = "select * from temp_pricezone where zone_id = 1 order by sku;";
            }

            $result = $this->getDi()->getShared('dbMaster')->execute($sql);

            if(!$result){
                \Helpers\LogHelper::log("productVariant ".$method, 'There have something error in update data');
                $this->setErrors(array('code' => 'RR303','title' => 'productVariant Query Batch','message' => 'There have something error in update data'));
                $this->setData(array('affected_row' => 0));
            }else{
                $this->setMessages(array('Successfully Query Batch'));
                $this->setData(array('affected_row' => $this->getDi()->getShared('dbMaster')->affectedRows()));
                $this->setErrors(array());
            }
        }  catch (\Exception $e) {
            \Helpers\LogHelper::log("productVariant_queryBatch", json_encode($e->getMessage()));
            $this->setErrors(array('code' => 'RR301','title' => 'productVariant_queryBatch','message' => 'There have something error, in query batch'));
            $this->setData(array('affected_row' => 0));
        }
    }

    public function preorderStatus($sku = null){
        // check if sku is preorder
        $preorderObj = new \Models\ProductPreorder();

        if(empty($sku)){
            $sku = $this->sku;
        }

        $preOrderQuery = $preorderObj->findFirst(
            array ("conditions" => "sku ='".$sku."'")
        );

        if(!empty($preOrderQuery)){
            $preOrderData = $preOrderQuery->toArray();
            $now = time();
            $startDate = strtotime($preOrderData['start_date']);
            $endDate = strtotime($preOrderData['end_date']);
            if($now>$startDate and $now<$endDate){
                $statusPreorder = 10;
            }else {
                $statusPreorder = 0;
            }
        }else{
            $statusPreorder = 0;
        }
        return $statusPreorder;
    }

    public function setCategoryIsInStockBySku($params){
        // cek category mapping stock and update is_in_stock
        $categoryStockMappingModels = new \Models\CategoryStockMapping();
        $categoryStockMappingResult = $categoryStockMappingModels->find("sku = '{$params['sku']}'");
        $arrCategory = array();
        $arrCategoryKey = array();
        $arrIsInStock = array();
        if($categoryStockMappingResult->count() > 0){
            foreach ($categoryStockMappingResult as $categoryStockMapping){
                $categoryStock = $categoryStockMapping->toArray();
                if(isset($arrIsInStock[$categoryStock['category_id']])){
                    $arrIsInStock[$categoryStock['category_id']] += $categoryStock['qty'];
                }else{
                    $arrIsInStock[$categoryStock['category_id']] = $categoryStock['qty'];
                }

                if (!in_array($categoryStock['category_id'], $arrCategory)){
                    $key = array_search($categoryStock['category_id'],array_column($params['category_is_in_stock'], 'category_id'));
                    $arrCategory[] = $categoryStock['category_id'];
                    $arrCategoryKey[] = $key;
                }
            }
        }

        foreach($arrCategory as $key=>$category){
            $isInStockCategory = $arrIsInStock[$category] > 0 ? 1 : 0;
            $params['category_is_in_stock'][$arrCategoryKey[$key]]['is_in_stock'] = $isInStockCategory;
        }
        unset($params['sku']);

        return $params;
    }

    public function getCountIsInStockTrue()
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $sqlSku = "SELECT count(DISTINCT(sku)) as jml FROM product_stock WHERE qty > 0";
            $resultSku = $this->getDi()->getShared($this->getConnection())->query($sqlSku);
            $jml = 0;
            if ($resultSku->numRows() > 0) {
                $resultSku->setFetchMode(Database::FETCH_ASSOC);
                $resultArray = $resultSku->fetch();
                $jml = $resultArray['jml'];
            }

            $response['messages'] = array('success');
            $response['data']['count'] = $jml;
        } catch (\Exception $e) {
            $response['errors'] = array('Get count SKU for Update is in stock to TRUE failed (' . $e->getMessage() . ')');
        }

        return $response;
    }

    /**
     * Function to get sku for update is in stock to 1 if sku have qty
     *
     * @param array $params
     * @return array
     */
    public function getIsInStockTrue($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $sqlSku = "SELECT DISTINCT(sku) FROM product_stock WHERE qty > 0 limit 1000 offset " . $params['offset'];
            $resultSku = $this->getDi()->getShared($this->getConnection())->query($sqlSku);
            $data = array();
            if ($resultSku->numRows() > 0) {
                $resultSku->setFetchMode(Database::FETCH_ASSOC);
                $resultArray = $resultSku->fetchAll();
                foreach ($resultArray as $rowSku) {
                    $data[] = $rowSku['sku'];
                }
            }

            $response['messages'] = array('success');
            $response['data'] = $data;
        } catch (\Exception $e) {
            $response['errors'] = array('Get SKU for Update is in stock to TRUE failed (' . $e->getMessage() . ')');
        }

        return $response;
    }


    public function getCountIsInStockFalse()
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $sqlSku = "SELECT count(DISTINCT(sku)) as jml FROM product_stock where qty <= 0";
            $resultSku = $this->getDi()->getShared($this->getConnection())->query($sqlSku);
            $jml = 0;
            if ($resultSku->numRows() > 0) {
                $resultSku->setFetchMode(Database::FETCH_ASSOC);
                $resultArray = $resultSku->fetch();
                $jml = $resultArray['jml'];
            }

            $response['messages'] = array('success');
            $response['data']['count'] = $jml;
        } catch (\Exception $e) {
            $response['errors'] = array('Get count SKU for Update is in stock to FALSE failed (' . $e->getMessage() . ')');
        }

        return $response;
    }

    /**
     * Function to get sku for update is in stock to 0 if sku doesnt have qty
     * @param array $params
     * @return array
     */
    public function getIsInStockFalse($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $skuZeroQtyList = array();

            $sqlGetSku = 'SELECT DISTINCT(sku) as sku FROM product_stock where qty <= 0 limit 1000 offset ' . $params['offset'];
            $resultGet = $this->getDi()->getShared($this->getConnection())->query($sqlGetSku);
            if($resultGet->numRows() > 0) {
                $resultGet->setFetchMode(Database::FETCH_ASSOC);
                $getSku = $resultGet->fetchAll();

                foreach ($getSku as $row) {
                    $sqlSumQtyBySku = "SELECT SUM(qty) as totalQty FROM product_stock where sku = '{$row['sku']}'";
                    $resultSum = $this->getDi()->getShared($this->getConnection())->query($sqlSumQtyBySku);
                    $resultSum->setFetchMode(Database::FETCH_ASSOC);
                    $getQty = $resultSum->fetch();
                    foreach ($getQty as $totalQty) {
                        if ($totalQty <= 0 ) {
                            $skuZeroQtyList[] = $row['sku'];
                        }
                    }
                }
            }

            $response['messages'] = array('success');
            $response['data'] = $skuZeroQtyList;
        } catch (\Exception $e) {
            $response['errors'] = array('Get SKU for Update is in stock to FALSE failed (' . $e->getMessage() . ')');
        }

        return $response;
    }

    /**
     * Function to update sku is in stock to 0 or 1
     *
     * @param array $params
     * @return array
     */
    public function isInStock($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            $rowAffected = 0;
            foreach ($params['sku'] as $rowSku) {
                $params['sku'] = $rowSku;
                $params['is_in_stock'] = $params['is_in_stock'];
                $rowAffected += ProductHelper::updateIsInStock($params);
            }

            $response['messages'] = array('success');
            $response['data']['affected_row'] = $rowAffected;
        } catch (\Exception $e) {
            if (!$params['is_in_stock']) {
                $response['errors'] = array('Update is in stock to FALSE failed (' . $e->getMessage() . ')');
            } else {
                $response['errors'] = array('Update is in stock to TRUE failed (' . $e->getMessage() . ')');
            }
        }

        return $response;
    }

    /**
     * Function to update all DC Stock to zero
     */
    public function allDcStockZero()
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        $now = date("Y-m-d H:i:s");
        $sql = "UPDATE product_stock SET qty = 0 WHERE store_code = 'DC' AND sku NOT IN (SELECT sku FROM product_preorder WHERE start_date<= '".$now."' AND end_date >='".$now."' )";
        $result = $this->getDi()->getShared('dbMaster')->execute($sql);
        if ($result) {
            $response['messages'] = array('success');
            $response['data']['affected_row'] = $this->getDi()->getShared('dbMaster')->affectedRows();
        } else {
            $response['errors'] = array('Update is in stock to FALSE failed, please contact ruparupa tech support');
        }

        return $response;
    }

    /**
     * This can be used to set is_in_stock to zero
     * @return array
     */
    public function updateIsInStockComparePir(){

        $sql  = "UPDATE product_variant SET is_in_stock = 0 WHERE sku IN (";
        $sql .= "    SELECT sku FROM (";
        $sql .= "        SELECT sku, SUM(qty) as totalQty FROM product_stock GROUP BY sku";
        $sql .= "    ) AS subquery WHERE totalQty <= 0";
        $sql .= ")";
        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        $productVariantModel = new \Models\ProductVariant();
        $resultProductVariant = $productVariantModel->find('is_in_stock = 0');
        foreach($resultProductVariant as $productVariant){
            $sku[]['sku'] = $productVariant->getSku();
        }
        ProductHelper::updateProductToElastic($sku);

        return array('affected_rows' => $result->numRows());
    }

    /**
     * @param array $dataArray
     * @return string
     */
    public function generateSkuMp($dataArray = array()){

        $supplierId  = $dataArray['supplier_id'];
        $sql = "select sku from product_variant_reserved_sku where supplier_id = ".$supplierId." order by reserved_id desc limit 1 ";

        $this->useReadOnlyConnection();
        $lastProductQuery = $this->getDi()->getShared($this->getConnection())->query($sql);
        $lastProductData = $lastProductQuery->fetch();

        if(!empty($lastProductData['sku'])) {
            $lastProductData['increment_id'] = $lastProductData['sku'];
        }else{
            $lastProductData['increment_id'] = 0;
        }

        $newId         = \Helpers\ProductHelper::filterLastIncrement($lastProductData['increment_id']);

        $head = 'MP'.str_pad($supplierId,3,'0',STR_PAD_LEFT);
        $body = str_pad($newId,'6','0',STR_PAD_LEFT);

        $sku = $head.$body;

        return $sku;

    }

    public function totalQty($params = array())
    {
        $response = array(
            'messages' => array(),
            'data' => array(),
            'errors' => array()
        );

        try {
            if (isset($params['sku_list'])) {
                $skuList = explode(',', $params['sku_list']);
                $totalQtyList = array();
                foreach ($skuList as $row) {
                    $sqlSku = "SELECT coalesce(sum(qty),0) as total_qty FROM product_stock WHERE sku = '$row'";
                    $resultSku = $this->getDi()->getShared($this->getConnection())->query($sqlSku);
                    $totalQty = 0;
                    if ($resultSku->numRows() > 0) {
                        $resultSku->setFetchMode(Database::FETCH_ASSOC);
                        $resultArray = $resultSku->fetch();
                        if (!empty($resultArray['total_qty'])) {
                            $totalQty = $resultArray['total_qty'];
                        } else {
                            $totalQty = 0;
                        }
                    }

                    $totalQtyList[$row] = $totalQty;
                }
            }

            $response['messages'] = array('success');
            $response['data']= $totalQtyList;
        } catch (\Exception $e) {
            $response['errors'] = array('Get total Qty failed (' . $e->getMessage() . ')');
        }

        return $response;
    }

}
