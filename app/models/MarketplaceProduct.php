<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/25/2016
 * Time: 4:12 PM
 */

namespace Models;


class MarketplaceProduct extends \Models\BaseModel
{
    public function initialize()
    {
        $this->setSource("product");
    }

}