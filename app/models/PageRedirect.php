<?php
/**
 * Created by PhpStorm.
 * User: iwan
 * Date: 24/05/18
 * Time: 10:24
 */

namespace Models;


class PageRedirect extends \Models\BaseModel
{
    protected $page_redirect_id;
    protected $keyword;
    protected $url_path;
    protected $company_code;
    protected $status;
    protected $created_at;
    protected $updated_at;
    /**
     * @return mixed
     */
    public function getPageRedirectId()
    {
        return $this->page_redirect_id;
    }

    /**
     * @param mixed $page_redirect_id
     */
    public function setPageRedirectId($page_redirect_id)
    {
        $this->page_redirect_id = $page_redirect_id;
    }

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return mixed
     */
    public function getUrlPath()
    {
        return $this->url_path;
    }

    /**
     * @param mixed $url_path
     */
    public function setUrlPath($url_path)
    {
        $this->url_path = $url_path;
    }

    /**
     * @return mixed
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param mixed $company_code
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'page_redirect';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        // $redisKey = self::_createKey($parameters);
        // $cache = [
        //     "cache" => [
        //         "key" => $redisKey,
        //     ],
        // ];
        // if(!is_array($parameters)) {
        //     $oldParam =  $parameters;
        //     $parameters = array();
        //     $parameters['conditions'] = $oldParam;
        // }
        // $parameters = array_merge($parameters,$cache);
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
            if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    /*
     * Override base model function to delete from cache
     */
    public function afterUpdate()
    {

        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);

        /*
         * Also Clean up this url key from redis (if query from url key)
         */
        $redisKey = "PageRedirect";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }

}