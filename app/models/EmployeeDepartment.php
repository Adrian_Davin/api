<?php
/**
 * Created by PhpStorm.
 * User: aldi
 * Date: 25/7/2022
 * Time: 9:49 AM
 */

namespace Models;


class EmployeeDepartment extends \Models\BaseModel
{
    protected $id;

    protected $nip;

    protected $id_department_bu;

    protected $created_at;

    // /**
    //  * @return mixed
    //  */
    public function getId()
    {
        return $this->id;
    }

    // /**
    //  * @param mixed $id
    //  */
    public function setId($id)
    {
        $this->id = $id;
    }

     /**
     * @return mixed
     */
    public function getDepartementBUId()
    {
        return $this->id_department_bu;
    }

    /**
     * @param mixed $id_department_bu
     */
    public function setDepartementBUId($id_department_bu)
    {
        $this->id_department_bu = $id_department_bu;
    }

    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @param mixed $nip
     */
    public function setNip($nip)
    {
        $this->nip = $nip;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'employee_department';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }

}