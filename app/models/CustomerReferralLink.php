<?php

namespace Models;

// use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
// use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerReferralLink extends \Models\BaseModel
{
  /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $customer_referral_link_id;

    protected $customer_id;

    protected $referral_tactical_id;

    protected $channel;

    protected $type;

    protected $referral_link;

    protected $referral_code;

    protected $status;

    protected $token;
    
    protected $expired_notification;
    
    protected $expired_at;

    protected $created_at;

    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_referral_link';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

     /**
     * @param int $customer_referral_link_id
     */
    public function setCustomerReferralLinkId($customer_referral_link_id)
    {
        $this->customer_referral_link_id = $customer_referral_link_id;
    }

    /**
     * @return int 
     */
    public function getCustomerReferralLinkId()
    {
        return $this->customer_referral_link_id;
    }

    /**
     * @param int $referral_tactical_id
     */
    public function setReferralTacticalID($referral_tactical_id)
    {
        $this->referral_tactical_id = $referral_tactical_id;
    }

    /**
     * @return int 
     */
    public function getReferralTacticalID()
    {
        return $this->referral_tactical_id;
    }

    /**
     * @param string $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return string
     */
    public function getChannel() 
    {
        return $this->channel;
    } 

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType() 
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getCustomerId() 
    {
        return $this->customer_id;
    } 

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    public function getReferralLink()
    {
        return $this->referral_link;
    }

    public function setReferralLink($referral_link)
    {
        $this->referral_link = $referral_link;
    }

    public function getReferralCode()
    {
        return $this->referral_code;
    }

    public function setReferralCode($referral_code)
    {
        $this->referral_code = $referral_code;
    }

    public function  getToken(){
        return $this->token;
    }

    public function setToken($token){
        $this->token = $token;
    }

     /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus() 
    {
        return $this->status;
    } 


 /**
     * @param int $status
     */
    public function setExpiredNotification($expired_notification)
    {
        $this->expired_notification = $expired_notification;
    }

    /**
     * @return int
     */
    public function getExpiredNotification() 
    {
        return $this->expired_notification;
    } 

      /**
     * @return mixed
     */
    public function getExpiredAt()
    {
        return $this->expired_at;
    }

    /**
     * @param mixed $expired_at
     */
    public function setExpiredAt($expired_at)
    {
        $this->expired_at = $expired_at;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
          if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
  }

  public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}