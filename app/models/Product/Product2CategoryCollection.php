<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Product;


class Product2CategoryCollection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\Product2Category
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->getDataArray();
            $view[$idx] = $itemArray;

            $this->next();
        }

        return $view;
    }

    public function setProductId($product_id = 0)
    {
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setProductId($product_id);

            $this->next();
        }
    }

    public function saveData()
    {
        $error = array();
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $response = $item->saveData();

            if(!empty($response)) {
                $error[] = $response;
            }

            $this->next();
        }

        return $error;
    }

    /**
     * get category list from product 2 category mapping
     */
    public function getCategoryList()
    {
        // get product2category data
        $this->rewind();
        $product2Category = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            if($item->ProductCategory){
                $product2Category[$idx] = $item->ProductCategory->getPath();
            }
            $this->next();
        }

        $allCategory = array();
        foreach($product2Category as $category) {
            $categoryPath = explode("/",$category);
            $allCategory = array_merge($allCategory, $categoryPath);
        }

        $productCategoryModel = new \Models\ProductCategory();
        $allCategory = array_unique($allCategory);
        $allCategory = "'".implode("','", $allCategory)."'";

        $param = array(
            "columns" => array("category_id", "name"),
            "conditions" => "category_id IN(".$allCategory.") and status > 0"
        );
        $result = $productCategoryModel::find($param);

        // maping array
        $productCategoryArray = array();
        foreach($result->toArray() as $categoryData) {
            $productCategoryArray[$categoryData['category_id']] = $categoryData;
        }

        // create view
        $cat_id_exclude = array(1,2);
        $view = array();
        $master_url_model = new \Models\MasterUrlKey();
        foreach($product2Category as $category) {
            $categoryPath = explode("/", $category);
            $thisPath = array();
            foreach ($categoryPath as $category_id) {
                if(!in_array($category_id,$cat_id_exclude)){
                    if(isset($productCategoryArray[$category_id])){

                        $parameter = [
                            "conditions" => "reference_id = " . $category_id ." AND section = 'category'",
                        ];
                        $urlKey = $master_url_model::findFirst($parameter);
                        $thisUrlKey = "";
                        if($urlKey) {
                            $thisUrlKey = $urlKey->getUrlKey().".html";
                        }

                        $thisPath[] = array(
                            "category_id" => $category_id,
                            "name" => $productCategoryArray[$category_id]['name'],
                            "url_key" => $thisUrlKey
                        );
                    }else{
                        break;
                    }

                }

            }
            if(!empty($thisPath))
                $view[] = $thisPath;
        }

        return $view;
    }
}