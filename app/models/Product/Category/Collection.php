<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Product\Category;


class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\ProductCategory
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $category = $this->current();
            $categoryArray = $category->getDataArray($columns, $showEmpty);
            $view[$idx] = $categoryArray;

            $this->next();
        }
        return $view;
    }


}