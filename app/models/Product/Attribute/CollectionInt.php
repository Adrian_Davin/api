<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Product\Attribute;


class CollectionInt extends \ArrayIterator
{
    /**
     * @var
     */
    protected $productAttributeInt;
    protected $productAttributeText;
    protected $productAttributeVarchar;

    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\ProductAttributeInt
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->getDataArray();
            $view[$idx] = $itemArray;

            $this->next();
        }
        return $view;
    }

    public function setProductId($product_id = 0)
    {
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setProductId($product_id);
            $this->next();
        }
    }
    public function saveData()
    {
        $error = array();
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $response = $item->saveData();

            if(!empty($response)) {
                $error[] = $response;
            }

            $this->next();
        }

        return $error;
    }


}