<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Product\Attribute;


use Models\MasterSize;

class Collection extends \ArrayIterator
{
    protected $product_id;

    /**
     * @var \Models\Product\Attribute\CollectionInt
     */
    protected $productAttributeInt;

    /**
     * @var \Models\Product\Attribute\CollectionText
     */
    protected $productAttributeText;

    /**
     * @var \Models\Product\Attribute\CollectionVarchar
     */
    protected $productAttributeVarchar;

    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\ProductAttributeInt
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return CollectionInt
     */
    public function getProductAttributeInt()
    {
        return $this->productAttributeInt;
    }

    /**
     * @param CollectionInt $productAttributeInt
     */
    public function setProductAttributeInt($productAttributeInt)
    {
        $this->productAttributeInt = $productAttributeInt;
    }

    /**
     * @return CollectionText
     */
    public function getProductAttributeText()
    {
        return $this->productAttributeText;
    }

    /**
     * @param CollectionText $productAttributeText
     */
    public function setProductAttributeText($productAttributeText)
    {
        $this->productAttributeText = $productAttributeText;
    }

    /**
     * @return CollectionVarchar
     */
    public function getProductAttributeVarchar()
    {
        return $this->productAttributeVarchar;
    }

    /**
     * @param CollectionVarchar $productAttributeVarchar
     */
    public function setProductAttributeVarchar($productAttributeVarchar)
    {
        $this->productAttributeVarchar = $productAttributeVarchar;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $productAttributeInt = array();
        if(!empty($this->productAttributeInt)) {
            $productAttributeInt = $this->productAttributeInt->getDataArray();
        }

        $productAttributeText = array();
        if(!empty($this->productAttributeText)) {
            $productAttributeText = $this->productAttributeText->getDataArray();
        }

        $productAttributeVarchar = array();
        if(!empty($this->productAttributeVarchar)) {
            $productAttributeVarchar = $this->productAttributeVarchar->getDataArray();
        }

        $view = array_merge(
            $productAttributeInt,
            $productAttributeText,
            $productAttributeVarchar
        );

        return $view;
    }

    public function setFromArray($data_array = array())
    {
        // Each attribute have it's on type
        $attributeIds = array();
        $newAttributeArray = array();
        foreach($data_array as $attribute)
        {
            $attributeIds[] = $attribute['attribute_id'];
            $attribute["product_id"] = $this->product_id;
            $newAttributeArray[$attribute['attribute_id']] = $attribute;
        }

        $attributeList = "'".implode("','", $attributeIds)."'";
        $param = array(
            "columns" => 'attribute_id, backend_type,is_option',
            "conditions" => "attribute_id IN(".$attributeList.")"
        );

        $shippingRateDbObj = new \Models\Attribute();
        $attributeResult = $shippingRateDbObj::find($param);

        $attributeInt = array();
        $attributeText = array();
        $attributeVarchar = array();
        foreach($attributeResult->toArray() as $result)
        {

            $attributeId = $result['attribute_id'];
            if($result['backend_type'] == "int") {
                if($result['is_option'] == 1) {
                    $variantOption = new \Models\AttributeOptionValue();
                    $conditions = [
                        'attribute_id' => $newAttributeArray[$attributeId]['attribute_id'],
                        'value' => $newAttributeArray[$attributeId]['value']
                    ];
                    $param = [
                        'conditions' => "attribute_id=:attribute_id: AND value=:value:",
                        'bind' => $conditions
                    ];
                    $resultOption = $variantOption::findFirst($param);
                    if ($resultOption) {
                        $value = $resultOption->getOptionId();
                    } else {
                        $value = null;
                    }

                    $newAttributeArray[$attributeId]['value'] = $value;
                }
                $attributeIntObj = new \Models\ProductAttributeInt();
                $attributeIntObj->setFromArray($newAttributeArray[$attributeId]);
                $attributeInt[] = $attributeIntObj;
            }

            if($result['backend_type'] == "text") {
                $attributeTextObj = new \Models\ProductAttributeText();
                $attributeTextObj->setFromArray($newAttributeArray[$attributeId]);
                $attributeText[] = $attributeTextObj;
            }

            if($result['backend_type'] == "varchar") {
                if($attributeId != '250'){ // exclude manufacture no
                    $attributeVarcharObj = new \Models\ProductAttributeVarchar();
                    $attributeVarcharObj->setFromArray($newAttributeArray[$attributeId]);
                    $attributeVarchar[] = $attributeVarcharObj;
                }
            }
        }

        $this->productAttributeInt = new \Models\Product\Attribute\CollectionInt($attributeInt);
        $this->productAttributeText = new \Models\Product\Attribute\CollectionText($attributeText);
        $this->productAttributeVarchar= new \Models\Product\Attribute\CollectionVarchar($attributeVarchar);
    }

    public function setProductIdToAttribute()
    {
        $this->productAttributeInt->setProductId($this->product_id);
        $this->productAttributeText->setProductId($this->product_id);
        $this->productAttributeVarchar->setProductId($this->product_id);
    }

    public function saveData()
    {
        $this->productAttributeInt->saveData();
        $this->productAttributeText->saveData();
        $this->productAttributeVarchar->saveData();
    }




}