<?php
/*
 * This collection is collection of category of product
 */

namespace Models\Product;


class ProductCategoryCollection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\Product\Category\Collection
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $category = $this->current();
            $categoryArray = $category->getDataArray($columns, $showEmpty);
            $view[$idx] = $categoryArray;

            $this->next();
        }
        return $view;
    }


}