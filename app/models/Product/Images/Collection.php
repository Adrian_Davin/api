<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Product\Images;


class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\ProductImages
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $image = $this->current();
            $imageArray = $image->getDataArray($columns, $showEmpty);
            $view[$idx] = $imageArray;

            $this->next();
        }
        return $view;
    }

    public function setProductVariantId($product_variant_id = 0)
    {
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setProductVariantId($product_variant_id);

            $this->next();
        }
    }

    public function saveData($log_file_name = "product")
    {
        $error = array();
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $response = $item->saveData($log_file_name);

            if(!$response) {
                $error[] = "Failed to save image";
            }

            $this->next();
        }

        return $error;
    }

}