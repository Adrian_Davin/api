<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Product\ProductVariant\Attribute;


class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\ProductVariantAttribute
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $attributeVariant = $item->getDataArray();

            $attribute = array();
            $attribute['attribute_id'] = $attributeVariant['attribute_id'];
            $attribute['value'] = (empty($attributeVariant['variantOption']['value'])) ? "" : $attributeVariant['variantOption']['value'] ;

            $view[$attributeVariant['variant']['code']] = $attribute;
            $this->next();
        }

        return $view;
    }

    public function setProductVariantId($product_variant_id = 0)
    {
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setProductVariantId($product_variant_id);

            $this->next();
        }
    }

    public function saveData()
    {
        $error = array();
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $response = $item->saveData();

            if(!empty($response)) {
                $error[] = $response;
            }

            $this->next();
        }

        return $error;
    }

}