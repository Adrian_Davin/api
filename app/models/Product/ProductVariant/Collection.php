<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\Product\ProductVariant;


class Collection extends \ArrayIterator
{
    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\ProductVariant
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $variant = $this->current();
            $variantArray = $variant->getDataArray();
            $sku = $variantArray['sku'];
            unset($variantArray['sku']);

            $view[$sku] = $variantArray;

            $this->next();
        }
        return $view;
    }

    public function setProductId($product_id = 0)
    {
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setProductId($product_id);

            $this->next();
        }
    }

    public function updateCache()
    {
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();

            if($item->getStatus() == 10) {
                $nsq = new \Library\Nsq();
                $message = [
                    "data" => ["sku" => $item->getSku()],
                    "message" => "createCache"
                ];
                $nsq->publishCluster('product', $message);
            } else {
                $productModel = new \Models\Product();
                $productModel->deleteFromCache($item->getSku());
            }

            $this->next();
        }

        return true;
    }

}