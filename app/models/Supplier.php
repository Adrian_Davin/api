<?php

namespace Models;

//use Library\Store;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class Supplier extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $supplier_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $supplier_alias;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $supplier_code;

    protected $dikirim_oleh;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $logo;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $mp_flag_shipment;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $mp_type;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    protected $mp_sap_vendor_code;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    protected $mp_sap_customer_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    protected $errors;

    protected $messages;

    protected $action;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $account;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $address;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('supplier_id', '\Models\Product', 'supplier_id', array('alias' => 'Product'));
        $this->hasMany('supplier_id', '\Models\ProductStock', 'supplier_id', array('alias' => 'ProductStock'));
        $this->hasMany('supplier_id', '\Models\Store', 'supplier_id', array('alias' => 'Store'));
        $this->hasMany('supplier_id', '\Models\SupplierAccount', 'supplier_id', array('alias' => 'SupplierAccount'));
        $this->hasMany('supplier_id', '\Models\SupplierAddress', 'supplier_id', array('alias' => 'SupplierAddress'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'supplier';
    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * @param int $supplier_id
     */
    public function setSupplierId($supplier_id)
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSupplierAlias()
    {
        return $this->supplier_alias;
    }

    /**
     * @param string $supplier_alias
     */
    public function setSupplierAlias($supplier_alias)
    {
        $this->supplier_alias = $supplier_alias;
    }

    /**
     * @return string
     */
    public function getSupplierCode()
    {
        return $this->supplier_code;
    }

    /**
     * @param string $supplier_code
     */
    public function setSupplierCode($supplier_code)
    {
        $this->supplier_code = $supplier_code;
    }

    /**
     * @return mixed
     */
    public function getDikirimOleh()
    {
        return $this->dikirim_oleh;
    }

    /**
     * @param mixed $dikirim_oleh
     */
    public function setDikirimOleh($dikirim_oleh)
    {
        $this->dikirim_oleh = $dikirim_oleh;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return string
     */
    public function getMpFlagShipment()
    {
        return $this->mp_flag_shipment;
    }

    /**
     * @param string $mp_flag_shipment
     */
    public function setMpFlagShipment($mp_flag_shipment)
    {
        $this->mp_flag_shipment = $mp_flag_shipment;
    }

    /**
     * @return string
     */
    public function getMpType()
    {
        return $this->mp_type;
    }

    /**
     * @param string $mp_type
     */
    public function setMpType($mp_type)
    {
        $this->mp_type = $mp_type;
    }

    /**
     * @return string
     */
    public function getMpSapVendorCode()
    {
        return $this->mp_sap_vendor_code;
    }

    /**
     * @param string $mp_sap_vendor_code
     */
    public function setMpSapVendorCode($mp_sap_vendor_code)
    {
        $this->mp_sap_vendor_code = $mp_sap_vendor_code;
    }

    /**
     * @return string
     */
    public function getMpSapCustomerCode()
    {
        return $this->mp_sap_customer_code;
    }

    /**
     * @param string $mp_sap_customer_code
     */
    public function setMpSapCustomerCode($mp_sap_customer_code)
    {
        $this->mp_sap_customer_code = $mp_sap_customer_code;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        $redisKey = self::_createKey($parameters);
        $cache = [
            "cache" => [
                "key" => $redisKey,
                "lifetime" => 7200,
            ],
        ];
        if(!is_array($parameters)) {
            $oldParam =  $parameters;
            $parameters = array();
            $parameters['conditions'] = $oldParam;
        }
        $parameters = array_merge($parameters,$cache);

        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        unset($view["created_at"]);
        unset($view["updated_at"]);

        return $view;
    }

    public function getSupplierDetail(){
        $supplierParam = array(
            "conditions" => "supplier_id = ".$this->supplier_id
        );
        $supplier = parent::findFirst($supplierParam)->toArray();

        $account = new SupplierAccount();
        $supplier['account'] = $account::findFirst($supplierParam);
        if(!empty($supplier['account'])){
            $supplier['account']->toArray();
        }

        $address = new SupplierAddress();
        $supplier['address'] = $address::find($supplierParam)->toArray();
        return $supplier;

    }

    /*
     * Override base model function to delete from cache
     */
    public function afterUpdate()
    {

        $redis = new \Library\Redis();
        $redisCon = $redis::connect($_ENV['MODELCACHE_REDIS_HOST'],$_ENV['MODELCACHE_REDIS_PORT'],$_ENV['MODELCACHE_REDIS_DB']);

        /*
         * Also Clean up this url key from redis (if query from url key)
         */
        $redisKey = "Supplier";
        $iterator = null;
        do {
            $result = $redisCon->scan($iterator,"*".$redisKey."*");

            if(!empty($result)) {
                foreach ($result as $key) {
                    $redisCon->delete($key);
                }
            }
        } while ($iterator > 0);
    }

    public function saveSupplierProfile($log_file_name = "")
    {
        try {
            unset($this->created_at);
            // Validate if instant courier status 10 then phone must be filled
            if ((int)$this->address["instant_courier_status"] == 10) {
                if (empty($this->address["phone"])) {
                    $this->errorCode = "400";
                    $this->errorMessages = "Phone must be filled, if instant courier is enabled";
                    return false;
                }

                if (!preg_match("/(^(08))\d{8,12}$/", $this->address["phone"])) {
                    $this->errorCode = "400";
                    $this->errorMessages = "Phone number is not valid, please input a valid phone number";
                    return false;
                }
            }
            $saveStatus = $this->saveData("supplier");
            $action = $this->action;
            if($saveStatus) {
                if (empty($this->supplier_id)) {
                    $lastSupplier = parent::findFirst(array(
                        "columns" => "supplier_id",
                        "order" => "supplier_id DESC"
                    ));
                    $this->supplier_alias = "MP" . $lastSupplier['supplier_id'];
                    $this->supplier_id = $lastSupplier['supplier_id'];
                } else {
                    $this->supplier_alias = "MP" . $this->supplier_id;
                }

                $this->account['supplier_id'] = $this->address['supplier_id'] = $this->supplier_id;
                $this->account['action'] = $this->address['action'] = $this->action;

                $account = new SupplierAccount();
                $account->setFromArray($this->account);
                $account->saveAccount();

                $address = new SupplierAddress();
                $address->setFromArray($this->address);
                $lastAddress = $address->saveAddress();

                //create store warehouse
                $storeAddress['supplier_id'] = $this->supplier_id;
                $storeAddress['supplier_address_id'] = $lastAddress;
                $storeAddress['name'] = $this->name."(Warehouse MP)";
                $storeAddress['type'] = 3;
                $storeAddress['zone_id'] = 1;
                $storeAddress['fulfillment_center'] = 1;
                $storeAddress['pickup_center'] = 0;
                $storeAddress['status'] = 10;
                $storeAddress['store_code'] = 'M'.$lastAddress;
                $store = new \Models\Store();
                $store->setFromArray($storeAddress);
                $store->saveStore();


                $phql = "UPDATE supplier SET supplier_alias = '$this->supplier_alias' WHERE supplier_id = $this->supplier_id";
                $this->getDi()->getShared('dbMaster')->execute($phql);

                // Clear supplier & supplier address from redis db 1 so the data can be renew
                $redis = new \Library\Redis();
                $redisCon = $redis::connect();
                $supplierKey = "supplier:".$this->supplier_id;
                $supplierAddresskey = "supplierAddress:".$this->supplier_id;
                $supplierStorekey = "supplierStore:".$this->supplier_alias;
                // clear supplier:supplier_id
                $redisCon->delete($supplierKey);
                // clear supplieraddress:supplier_id
                $redisCon->delete($supplierAddresskey);
                // clear supplierStore:supplier_alias used on assignation
                $redisCon->delete($supplierStorekey);

                $this->afterUpdate();
                return $this->setSupplierId($this->supplier_id);
            }

            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("Supplier", "Supplier save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Save/Update Supplier failed";

                $transaction->rollback(
                    "Save/Update Supplier failed"
                );
                return false;
            }
        } catch (TxFailed $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        } catch (\Exception $e) {
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;

    }
}
