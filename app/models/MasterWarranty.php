<?php
/**
 * Created by PhpStorm.
 * User: roesmien_ecomm
 * Date: 2/8/2017
 * Time: 9:49 AM
 */

namespace Models;


class MasterWarranty extends \Models\BaseModel
{
    protected $errors;

    protected $messages;

    /**
     * @return mixed
     */


    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function onConstruct()
    {
        parent::onConstruct();
        $this->whiteList = array();
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_warranty';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AttributeSet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}