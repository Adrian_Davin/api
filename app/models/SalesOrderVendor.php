<?php

namespace Models;

class SalesOrderVendor extends \Models\BaseModel
{

    protected  $shop_id;

    protected  $order_no;

     /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('order_no', 'Models\SalesOrder', 'order_no', array('alias' => 'SalesOrder'));

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_order_vendor';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_order_vendor", "TRIGGERED");
    }

    /**
     * @return int $shop_id
     */
    public function getShopId()
    {
        return $this->shop_id;
    }

}
