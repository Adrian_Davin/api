<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class CustomerPointRedeem extends \Models\BaseModel
{
    protected $customer_point_redeem_id;
    protected $customer_id;
    protected $sales_rule_voucher_id;
    protected $voucher_point_code;
    protected $point_redeemed;
    protected $company_code;
    protected $created_at;

    public function initialize()
    {
        //
    }
    /**
     * @return mixed
     */
    public function getCustomerPointRedeemId()
    {
        return $this->customer_point_redeem_id;
    }

    /**
     * @param mixed $customer_ace_otp_temp_id
     */
    public function setCustomerPointRedeemId($customer_point_redeem_id)
    {
        $this->customer_point_redeem_id = $customer_point_redeem_id;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param mixed $email
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return mixed
     */
    public function getSalesRuleVoucherId()
    {
        return $this->sales_rule_voucher_id;
    }

    /**
     * @param mixed $phone
     */
    public function setSalesRuleVoucherId($sales_rule_voucher_id)
    {
        $this->sales_rule_voucher_id = $sales_rule_voucher_id;
    }

    /**
     * @return mixed
     */
    public function getVoucherPointCode()
    {
        return $this->voucher_point_code;
    }

    /**
     * @param mixed $access_code
     */
    public function setVoucherPointCode($voucher_point_code)
    {
        $this->voucher_point_code = $voucher_point_code;
    }

    /**
     * @return mixed
     */
    public function getPointRedeemed()
    {
        return $this->point_redeemed;
    }

    /**
     * @param mixed $validate_count
     */
    public function setPointRedeemed($point_redeemed)
    {
        $this->point_redeemed = $point_redeemed;
    }

    /**
     * @return mixed
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param mixed $request_count
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        return $this->toArray($columns, $showEmpty);
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function createRedeem()
    {
        return null;
    }

}