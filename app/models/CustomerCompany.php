<?php
/**
 * Created by PhpStorm.
 * User: wanli
 * Date: 9/4/2017
 * Time: 1:58 PM
 */

namespace Models;

class CustomerCompany extends \Models\BaseModel
{

    protected $customer_company_id;
    protected $customer_id;
    protected $company_name;
    protected $company_npwp;
    protected $kpp_pratama;
    protected $company_address;
    protected $recipient_email;
    protected $npwp_image;
    protected $is_default;
    protected $status;
    protected $created_at;
    protected $updated_at;
    protected $is_npwp_address;
    protected $nitku;
    protected $tipe_wajib_pajak;


    /**
     * @return mixed
     */
    public function getSourceFrom()
    {
        return $this->source_from;
    }

    public function initialize()
    {
        //$this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer', "reusable" => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_company';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterCountry
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
        return $view;
    }

    /**
     * Get the value of customer_company_id
     */ 
    public function getCustomer_company_id()
    {
        return $this->customer_company_id;
    }

    /**
     * Set the value of customer_company_id
     *
     * @return  self
     */ 
    public function setCustomer_company_id($customer_company_id)
    {
        $this->customer_company_id = $customer_company_id;

        return $this;
    }

    /**
     * Get the value of customer_id
     */ 
    public function getCustomer_id()
    {
        return $this->customer_id;
    }

    /**
     * Set the value of customer_id
     *
     * @return  self
     */ 
    public function setCustomer_id($customer_id)
    {
        $this->customer_id = $customer_id;

        return $this;
    }

    /**
     * Get the value of company_name
     */ 
    public function getCompany_name()
    {
        return $this->company_name;
    }

    /**
     * Set the value of company_name
     *
     * @return  self
     */ 
    public function setCompany_name($company_name)
    {
        $this->company_name = $company_name;

        return $this;
    }

    /**
     * Get the value of company_npwp
     */ 
    public function getCompany_npwp()
    {
        return $this->company_npwp;
    }

    /**
     * Set the value of company_npwp
     *
     * @return  self
     */ 
    public function setCompany_npwp($company_npwp)
    {
        $this->company_npwp = $company_npwp;

        return $this;
    }

    /**
     * Get the value of kpp_pratama
     */ 
    public function getKpp_pratama()
    {
        return $this->kpp_pratama;
    }

    /**
     * Set the value of kpp_pratama
     *
     * @return  self
     */ 
    public function setKpp_pratama($kpp_pratama)
    {
        $this->kpp_pratama = $kpp_pratama;

        return $this;
    }

    /**
     * Get the value of company_address
     */ 
    public function getCompany_address()
    {
        return $this->company_address;
    }

    /**
     * Set the value of company_address
     *
     * @return  self
     */ 
    public function setCompany_address($company_address)
    {
        $this->company_address = $company_address;

        return $this;
    }

    /**
     * Get the value of recipient_email
     */ 
    public function getRecipient_email()
    {
        return $this->recipient_email;
    }

    /**
     * Set the value of recipient_email
     *
     * @return  self
     */ 
    public function setRecipient_email($recipient_email)
    {
        $this->recipient_email = $recipient_email;

        return $this;
    }

    /**
     * Get the value of npwp_image
     */ 
    public function getNpwp_image()
    {
        return $this->npwp_image;
    }

    /**
     * Set the value of npwp_image
     *
     * @return  self
     */ 
    public function setNpwp_image($npwp_image)
    {
        $this->npwp_image = $npwp_image;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of is_default
     */ 
    public function getIs_default()
    {
        return $this->is_default;
    }

    /**
     * Set the value of is_default
     *
     * @return  self
     */ 
    public function setIs_default($is_default)
    {
        $this->is_default = $is_default;

        return $this;
    }

    /**
     * Get the value of is_npwp_address
     */ 
    public function getIsNpwpAddress()
    {
        return $this->is_npwp_address;
    }

    /**
     * Set the value of is_npwp_address
     *
     * @return  self
     */ 
    public function setIsNpwpAddress($is_npwp_address)
    {
        $this->is_npwp_address = $is_npwp_address;

        return $this;
    }

    /**
     * Get the value of nitku
     */ 
    public function getNITKU()
    {
        return $this->nitku;
    }

    /**
     * Set the value of nitk
     *
     * @return  self
     */ 
    public function setNITKU($nitku)
    {
        $this->nitku = $nitku;

        return $this;
    }

    /**
     * Get the value of tipe_wajib_pajak
     */ 
    public function getTipeWajibPajak()
    {
        return $this->tipe_wajib_pajak;
    }

    /**
     * Set the value of tipe_wajib_pajak
     *
     * @return  self
     */ 
    public function setTipeWajibPajak($tipe_wajib_pajak)
    {
        $this->tipe_wajib_pajak = $tipe_wajib_pajak;

        return $this;
    }
}