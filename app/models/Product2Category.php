<?php

namespace Models;

use GuzzleHttp\Client;
use helpers\GeneralHelper as GeneralHelper;
use Helpers\ProductHelper;
use Helpers\RouteHelper;
use MongoDB\Exception\Exception;
use Phalcon\Db as Database;

class Product2Category extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $product_id;

    /**
     *
     * @var string
     * @Column(type="int", length=11, nullable=false)
     */
    protected $category_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $is_default;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $priority;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('product_id', 'Models\Product', 'product_id', array('alias' => 'Product'));
        $this->belongsTo('category_id', 'Models\ProductCategory', 'category_id', array('alias' => 'ProductCategory'));
        $this->setSource("product_2_category");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_2_category';
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return string
     */
    public function getCategoryPath()
    {
        return $this->category_path;
    }

    /**
     * @param string $category_path
     */
    public function setCategoryPath($category_path)
    {
        $this->category_path = $category_path;
    }

    /**
     * @return int
     */
    public function getIsDefault()
    {
        return $this->is_default;
    }

    /**
     * @param int $is_default
     */
    public function setIsDefault($is_default)
    {
        $this->is_default = $is_default;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $status
     */
    public function setPriority($priority)
    {
        $this->status = $priority;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Product2Category[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Product2Category
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                if($key == 'category_id' && $val == '') {
                    $val = 0;
                }

                $this->{$key} = $val;
            }
        }
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }


    public function saveData($log_file_name = "", $state = "")
    {
        if (!empty($this->product_id)) {
            if ($this->save() === false) {

                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("product", "Product 2 categoty save failed, error : " . json_encode($errMsg));
                return "Save product 2 category failed";
            }
        } else {
            \Helpers\LogHelper::log("product", "Product 2 categoty save failed, error : no product id found");
            return "Save product 2 category need product id";
        }

        return;
    }

    public function getParentPath($categoryId = "")
    {
        if (empty($categoryId)) {
            return false;
        }

        $productCategoryModel = new ProductCategory();
        $productData = $productCategoryModel->findFirst([
            "columns" => "path",
            "conditions" => "category_id=" . $categoryId
        ]);

        if ($productData) {
            $parents = explode('/',
                substr($productData->toArray()['path'], 0, strrpos($productData->toArray()['path'], '/')));
        } else {
            return array();
        }

        return $parents;
    }

    public function saveDataBulk($params = array())
    {
        try {
            $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';
            $currentCategories = ($companyCode == 'ODI') ? 'categories' : 'categories_'.$companyCode;
            $categoryId = $params['category_id'];
            $isAnchor = isset($params['is_anchor']) ? $params['is_anchor'] : 0;
            $addSkuArr = $params['add_product']['sku'];
            $joinedAddSkuArr = str_replace(",", '","', implode(",", $addSkuArr));
            $counter = 0;

            $client = new Client();

            // Get all products from Elasticsearch at one time
            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                    '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                    '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                    '/_search';

            // get data based on current sku
            $body = json_encode(array(
                "size" => 1000,
                "_source" => [$currentCategories, "variants.sku"],
                "query" => array(
                    "bool" => array(
                        "filter" => [
                            array(
                                "nested" => array(
                                    "path" => "variants",
                                    "query" => array(
                                        "terms" => array(
                                            "variants.sku.faceted" => [$joinedAddSkuArr]
                                        )
                                    )
                                    )
                                ),
                            array(
                                "range" => array(
                                    "status" => array(
                                        "gt" => -1
                                    )
                                )
                            )
                        ]
                    )
                )
            ));

            $body = preg_replace('/\\\\\"/',"\"", $body);

            $response = $client->post($elasticUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);

            $result = \GuzzleHttp\json_decode($response->getBody(), true);
            $bodyResult = $result['hits']['hits'];

            // looking for parent path
            $path = array();
            if ($isAnchor || $isAnchor == 1 || $isAnchor == '1') {
                // we must know it's parent by query to sql
                $categoryModel = new ProductCategory();
                $resultCat = $categoryModel->findFirst(
                    [
                        'columns' => 'path',
                        'conditions' => "category_id = ". $categoryId
                    ]
                );

                if ($resultCat) {
                    $category = $resultCat->toArray();
                    $path = explode('/', $category['path']);
                }
            } else {
                $path = array($categoryId);
            }
            
            // Starts here
            foreach ($bodyResult as $key => $value) {
                $docId = $value['_id'];
                $source = $value['_source'];
                $categories = $source[$currentCategories];
                $newCategories = array();

                foreach ($path as $k => $val) {
                    // check if categories totally empty
                    if (empty($categories)) {
                        $found = false;
                    } else {
                        $found = \Helpers\GeneralHelper::in_array_r($val,$categories,false,true);
                    }

                    //proceed adding into product categories
                    if (!$found || empty($found)) {
                        // proceed adding
                        $newCategories[$k]['is_in_stock'] = 1;
                        $newCategories[$k]['category_id'] = intVal($val);
                        $newCategories[$k]['breadcrumb_position'] = 0;

                        $categoryModel = new ProductCategory();
                        $categoryRes = $categoryModel->findFirst(
                            [
                                "columns" => "name",
                                "conditions" => "category_id = " . $val
                            ]
                        );

                        $newCategories[$k]['priority'] = 100000;

                        // get name of the category
                        if ($categoryRes) {
                            $res = $categoryRes->toArray();

                            $newCategories[$k]['name'] = $res['name'];
                        } else {
                            $newCategories[$k]['name'] = ''; // todo need to handle validation error
                        }

                        // get url key
                        $routeData = RouteHelper::getRouteData($val,"category","cms", $companyCode);
                        if (!empty($routeData)) {
                            $newCategories[$k]['url_key'] = $routeData['url_key'];
                        } else {
                            $newCategories[$k]['url_key'] = ''; // todo need to handle validation error
                        }
                    }

                }

                if (!empty($newCategories)) {
                    // it means something need to be added
                    // merge it with old categories - if not empty
                    $finalCategories = $newCategories;

                    if (!empty($categories)) {
                        $finalCategories = array_merge($categories, $newCategories);
                    }

                    $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                        '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                        '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                        '/' . $docId .'/_update?refresh=true';

                    $update = array(
                        "doc" => array(
                            $currentCategories => $finalCategories
                        )
                    );

                    $updateResponse = $client->post($elasticUrl, [
                        'headers' => ['Content-Type' => 'application/json'],
                        'body' => json_encode($update)
                    ]);

                    $updateResult = \GuzzleHttp\json_decode($updateResponse->getBody(), true);

                    if ($updateResult['result'] == "updated") {
                        // it means we successfully update
                        $counter++;
                    }
                }

                $finalResponse[$key]['is_anchor'] = $isAnchor;
                $finalResponse[$key]['sku'] = $source['variants'][0]['sku']; // multivariant just get one of the product
                $finalResponse[$key]['inserted'] = $counter;
            }

            return $finalResponse;
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("productToCategory", json_encode($e->getMessage()));
            return false;
        }

    }

    public function removeDataBulk($params = array())
    {
        // DONE
        try {
            // we need to delete specific categories from this SKU
            $categoryId = $params['category_id'];
            $isAnchor = $params['is_anchor'];
            $removeSkuArr = $params['remove_product']['sku'];
            $joinedRemoveSkuArr = str_replace(",", '","', implode(",", $removeSkuArr));
            $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';
            $currentCategories = ($companyCode == 'ODI') ? 'categories' : 'categories_'.$companyCode;
            $counter = 0;

            $client = new Client();

            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                    '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                    '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                    '/_search';

            // get data based on current sku
            $body = json_encode(array(
                "size" => 1000,
                "_source" => [$currentCategories, "variants.sku"],
                "query" => array(
                    "bool" => array(
                        "filter" => [
                            array(
                                "nested" => array(
                                    "path" => "variants",
                                    "query" => array(
                                        "terms" => array(
                                            "variants.sku.faceted" => [$joinedRemoveSkuArr]
                                        )
                                    )
                                    )
                                )
                        ]
                    )
                )
            ));

            $body = preg_replace('/\\\\\"/',"\"", $body);

            $response = $client->post($elasticUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);

            $result = \GuzzleHttp\json_decode($response->getBody(), true);
            $bodyResult = $result['hits']['hits'];
            // todo if empty body result

            // Categories
            $path = array();
            if ($isAnchor || $isAnchor == 1 || $isAnchor == '1') {
                // delete all until it's parent
                // we must know it's parent by query to sql
                $categoryModel = new ProductCategory();
                $resultCat = $categoryModel->findFirst(
                    [
                        'columns' => 'path',
                        'conditions' => "category_id = ". $categoryId
                    ]
                );

                if ($resultCat) {
                    $category = $resultCat->toArray();
                    $path = explode('/', $category['path']);
                }

            } else {
                $path = array($categoryId);
            }

            foreach ($bodyResult as $key => $value) {
                $docId = $value['_id'];
                $source = $value['_source'];
                $categories = $source[$currentCategories];
                $newCategories = array();

                // loop through path to delete from elastic
                foreach ($path as $k) {
                    $found = \Helpers\GeneralHelper::in_array_r($k,$categories,false,true);

                    if ($found) {
                        if ($found['item']['breadcrumb_position'] == 0) {
                            // proceed deleting
                            unset($categories[$found['key']]);

                            // make the format for update doc
                            $newCategories = array_values($categories);

                            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                                '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                                '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                                '/' . $docId .'/_update?refresh=true';

                            
                            $update = array(
                                "doc" => array(
                                    $currentCategories => $newCategories
                                )
                            );

                            $updateResponse = $client->post($elasticUrl, [
                                'headers' => ['Content-Type' => 'application/json'],
                                'body' => json_encode($update)
                            ]);

                            $updateResult = \GuzzleHttp\json_decode($updateResponse->getBody(), true);

                            if ($updateResult['result'] == "updated") {
                                // it means we successfully update
                                $counter++;
                            }
                        }
                    }
                }

                $finalResponse[$key]['is_anchor'] = $isAnchor;
                $finalResponse[$key]['sku'] = $source['variants'][0]['sku'];
                $finalResponse[$key]['deleted'] = $counter;
            }

            return $finalResponse;

        } catch (\Exception $e) {
            \Helpers\LogHelper::log("productToCategory", json_encode($e->getMessage()));
            return false;
        }

    }

    public function saveHeroProducts ($params = array()) {
        try {
            $categoryId = $params['category_id'];
            $heroProductsArr = !empty($params['hero_products']['sku'][0]) ? $params['hero_products']['sku'] : [];
            $oldHeroProductsArr = !empty($params['old_hero_products']['sku'][0]) ? $params['old_hero_products']['sku'] : [];
            $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';
            $flagResetPriority = false;
            $arrayHeroProducts = array();

            // First Step, reset priority to 10000 if found the diff
            // compare old and new hero products, the diff means we need to update it's priority to default = 10000
            // use new hero products to update
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            $diffToResetOldProducts = array_diff($oldHeroProductsArr, $heroProductsArr); // A is not in B

            // this is need to be reset
            if (count($diffToResetOldProducts) > 0) { // ?
                // now we proceed to reset priority
                $flagResetPriority = true;
            }

            // Second Step, add new hero products and it's priority
            $actionResponse = [];
            if ($flagResetPriority) {
                $arrayHeroProducts = $diffToResetOldProducts;
                $actionResponse = $this->resetOrAddPriorityHero($arrayHeroProducts,$categoryId,"reset",$companyCode);
                $flagResetPriority = false;
            }

            $actionResponse = $this->resetOrAddPriorityHero($heroProductsArr, $categoryId, "add",$companyCode);

            return $actionResponse;
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("productToCategory", json_encode($e->getMessage()));
            return false;
        }
    }

    public function resetOrAddPriorityHero ($productArr = array(), $categoryId = 0, $actionType = "", $companyCode = 'ODI') {
        $currentCategories = ($companyCode == 'ODI') ? 'categories' : 'categories_'.$companyCode;
        $joinedProductArr = str_replace(",", '","', implode(",", $productArr));
        $finalResponse = array();
        $resultData = array();

        $client = new Client();

        $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                '/_search';

        // loop every sku to delete category
        // get data based on current sku
        $body = json_encode(array(
            "size" => 1000,
            "_source" => [$currentCategories, "variants.sku"],
            "query" => array(
                "bool" => array(
                    "filter" => [
                        array(
                            "nested" => array(
                                "path" => "variants",
                                "query" => array(
                                    "terms" => array(
                                        "variants.sku.faceted" => [$joinedProductArr]
                                    )
                                )
                                )
                            ),
                        array(
                            "range" => array(
                                "status" => array(
                                    "gt" => -1
                                )
                            )
                        )
                    ]
                )
            )
        ));

        $body = preg_replace('/\\\\\"/',"\"", $body);

        $response = $client->post($elasticUrl, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $result = \GuzzleHttp\json_decode($response->getBody(), true);
        if (!empty($result['hits']['hits'])) {
            $resultData = $result['hits']['hits'];
        } else {
            return array();
        }

        foreach ($resultData as $key => $data) {
            $counter = 0;
            $docId = $data['_id'];
            $bodyResult = $data['_source'];
            $categories = $bodyResult[$currentCategories];

            // each of sku, we need to find if request category has already exist
            $found = \Helpers\GeneralHelper::in_array_r($categoryId,$categories,false,true);

            if ($found || !empty($found)) {
                // if we found it, we able to edit, but if not found, skip
                if ($actionType == "add") {
                    // we have to find sku in $productArr by doing looping? greddy af.
                    foreach ($bodyResult['variants'] as $idx => $variant) {
                        $foundIdx = array_search($variant['sku'], $productArr);
                        if (!is_bool($foundIdx)) {
                            // which will be false
                            $found['item']['priority'] = $foundIdx;
                        }
                    }
                } else {
                    $found['item']['priority'] = 100000;
                }

                // change the priority
                $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                    '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                    '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                    '/' . $docId . '/_update?refresh=true';

                $bodyUpdate = array(
                    "script" => array(
                        "lang" => "painless",
                        "inline" => "int flag = 0; for (int index = 0; index < ctx._source.".$currentCategories.".length; index++) { if(ctx._source.".$currentCategories."[index].category_id == ".$categoryId.") { flag = index } } ctx._source.".$currentCategories."[flag] = (params.categories)",
                        "params" => array(
                            "categories" => $found['item']
                        )
                    )
                );

                $updateResponse = $client->post($elasticUrl, [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body' => json_encode($bodyUpdate)
                ]);

                $updateResult = \GuzzleHttp\json_decode($updateResponse->getBody(), true);

                if ($updateResult['result'] == "updated") {
                    // it means we successfully update
                    $counter++;
                }
            }

            $finalResponse[$key]['sku'] = $bodyResult['variants'][0]['sku'];
            $finalResponse[$key]['updated'] = $counter;
        }

        return $finalResponse;
    }

    // TODO
    public function removeProductsOnly($params = array()){
        if(empty($params)){
            return false;
        }

        $companyCode = isset($params['company_code']) && !empty($params['company_code']) ? $params['company_code'] : 'ODI';
        $currentCategories = ($companyCode == 'ODI') ? 'categories' : 'categories_'.$companyCode;
        $category_id = $params['category_id'];
        $delete_children= $params['delete_children'];
        $products = str_replace(",",'","', $params['products']);
        $productsArr = explode(',', $params['products']);
        $newId  = "";
        $finalResponse = array();

        $client = new Client();

        if(intVal($delete_children) == 10){
            // TODO
            $categoryModel = new ProductCategory();
            $categoryLevelRes = $categoryModel->find(
                [
                    "columns" => "level",
                    "conditions" => "category_id=".$category_id
                ]
            );
            $categoryLevel = $categoryLevelRes->toArray();
            if(!empty($categoryLevel)){
                // after searching for level, we need to search who have used this category
                // now we got children, we have to apply delete to all children
                if ($categoryLevel[0]['level'] <= 2) {
                    $newId = "'".$category_id."/%'";
                } else {
                    $newId = "'%/".$category_id."/%'";
                }

                //search it
                $categoryModel = new ProductCategory();
                $childrenRes = $categoryModel->find(
                    [
                        "columns" => "path",
                        "conditions" => "category_id LIKE ".$newId
                    ]
                );

                $children = $childrenRes->toArray();

                if (!empty($children)) {
                    // we have to loop through all children
                    // TODOO
                }
            }
        } else {
            // TODO: Properly handle product deletion

            // search product id
            $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
            '/' . getenv('ELASTIC_PRODUCT_INDEX') .
            '/' . getenv('ELASTIC_PRODUCT_TYPE') .
            '/_search';

            // loop every sku to delete category
            // get data based on current sku
            $body = json_encode(array(
                "size" => 1000,
                "_source" => [$currentCategories, "variants.sku"],
                "query" => array(
                    "bool" => array(
                        "filter" => [
                            array(
                                "nested" => array(
                                    "path" => "variants",
                                    "query" => array(
                                        "terms" => array(
                                            "variants.sku.faceted" => [$products]
                                        )
                                    )
                                    )
                                )
                        ]
                    )
                )
            ));
    
            $body = preg_replace('/\\\\\"/',"\"", $body);

            $response = $client->post($elasticUrl, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);

            $result = \GuzzleHttp\json_decode($response->getBody(), true);
            $counter = 0;

            if (!empty($result['hits']['hits'])) {
                $bodyResult = $result['hits']['hits'];
                foreach ($bodyResult as $key => $data) {
                    $docId = $data['_id'];
                    $source = $data['_source'];

                    $elasticUrl = getenv('ELASTIC_HOST') . ':' . getenv('ELASTIC_PORT') .
                    '/' . getenv('ELASTIC_PRODUCT_INDEX') .
                    '/' . getenv('ELASTIC_PRODUCT_TYPE') .
                    '/' . $docId . '/_update?refresh=true';

                    // get data based on current sku
                    $bodyScript = array(
                        "script" => array(
                            "lang" => "painless",
                            "inline" => "for (int index = 0; index < ctx._source.".$currentCategories.".length; index++) { if(ctx._source.".$currentCategories."[index].category_id == ".$category_id." ) { ctx._source.".$currentCategories.".remove(index) } }"
                        )
                    );

                    $response = $client->post($elasticUrl, [
                        'headers' => ['Content-Type' => 'application/json'],
                        'body' => json_encode($bodyScript)
                    ]);

                    $result = \GuzzleHttp\json_decode($response->getBody(), true);

                    if ($result['result'] == 'updated') {
                        $finalResponse[$key]['sku'] = $source['variants'][0]['sku'];
                        $finalResponse[$key]['deleted'] = true;
                        $counter++;
                    } else {
                        $finalResponse[$key]['sku'] = $source['variants'][0]['sku'];
                        $finalResponse[$key]['deleted'] = false;
                    }
                }
            }
        }

        return array('affected_rows' => $counter, 'response' => $finalResponse);
    }
}

