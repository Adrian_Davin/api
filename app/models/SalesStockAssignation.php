<?php

namespace Models;

class SalesStockAssignation extends \Models\BaseModel
{

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $sku;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_item_id;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected $store_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $stock;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $qty_ordered;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $last_stock;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected $stock_store_code;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $stock_store_code_qty;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $stock_store_code_remaining_qty;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sku', 'Models\ProductVariant', 'sku', array('alias' => 'ProductVariant'));
        $this->belongsTo('store_code', 'Models\Store', 'store_code', array('alias' => 'Store'));
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_stock_assignation';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesStockAssignation[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesStockAssignation
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
