<?php

namespace Models;

class ScOwnfleetRate extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $ownfleet_template_id;
    protected $origin_region;
    protected $dest_kecamatan_code;
    protected $rate;
    protected $is_flat;

    protected $string_value;

    protected $created_at;
    protected $updated_at;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getOwnfleetTemplateId()
    {
        return $this->ownfleet_template_id;
    }

    /**
     * @param int $ownfleet_template_id
     */
    public function setOwnfleetTemplateId($ownfleet_template_id)
    {
        $this->ownfleet_template_id = $ownfleet_template_id;
    }

    /**
     * @return mixed
     */
    public function getOriginRegion()
    {
        return $this->origin_region;
    }

    /**
     * @param mixed $origin_region
     */
    public function setOriginRegion($origin_region)
    {
        $this->origin_region = $origin_region;
    }

    /**
     * @return mixed
     */
    public function getDestKecamatanCode()
    {
        return $this->dest_kecamatan_code;
    }

    /**
     * @param mixed $dest_kecamatan_code
     */
    public function setDestKecamatanCode($dest_kecamatan_code)
    {
        $this->dest_kecamatan_code = $dest_kecamatan_code;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getIsFlat()
    {
        return $this->is_flat;
    }

    /**
     * @param mixed $is_flat
     */
    public function setIsFlat($is_flat)
    {
        $this->is_flat = $is_flat;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('ownfleet_template_id', 'Models\ScOwnfleetTemplate', 'ownfleet_template_id', array('alias' => 'ScOwnfleetTemplate'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sc_ownfleet_rate';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function insertTemplateToRate(){

        $sql  = "INSERT INTO sc_ownfleet_rate(ownfleet_template_id,origin_region,dest_kecamatan_code,rate,is_flat,created_at) ";
        $sql .= "VALUES".$this->string_value;

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        if($result->numRows() > 0){
            return $msg = "Insert template to rate success!";
        }
        else {
            return $msg = "error";
        }
    }

    public function clearShippingRate($id){
        $sql  = "DELETE FROM supplier_ownfleet_rate ";
        $sql .= "WHERE ownfleet_template_id=".$id;

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        if($result->numRows() > 0){
            return $msg = "success";
        }
        else {
            return $msg = "error";
        }
    }

}
