<?php

namespace Models;

use Phalcon\Db as Database;

class CustomerCreditCard extends \Models\BaseModel
{
    protected $id;
    protected $customer_id;
    protected $cc_name;
    protected $cc_number;
    protected $cc_brand;
    protected $saved_token_id;
    protected $saved_token_id_expired_at;
    protected $status;

 
    public function getSource()
    {
        return 'customer_credit_card';
    }

    public function initialize(){

    }

    public function getID(){
        return $this->id;
    }

    public function setID($id){
        $this->id = $id;
    }

    public function getCustomerID(){
        return $this->customer_id;
    }

    public function setCustomerID($customer_id){
        $this->customer_id = $customer_id;
    }

    public function getCCBrand(){
        return $this->cc_brand;
    }
    
    public function setCCBrand($cc_brand){
        $this->cc_brand = $cc_brand;
    }

    public function getCCName(){
        return $this->cc_name;
    }

    public function setCCName($cc_name){
        $this->cc_name = $cc_name;
    }

    public function getCCNumber(){
        return $this->cc_number;
    }

    public function setCCNumber($cc_number){
        $this->cc_number = $cc_number;
    }

    public function getSavedTokenID(){
        return $this->saved_token_id;
    }

    public function setSavedTokenID($saved_token_id){
        $this->saved_token_id = $saved_token_id;
    }

    public function getSavedTokenIDExpiredAt(){
        return $saved_token_id_expired_at;
    }

    public function setSavedTokenIDExpiredAt($saved_token_id_expired_at){
        $this->saved_token_id_expired_at = $saved_token_id_expired_at;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setStatus($status){
        $this->status = $status;
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

?>