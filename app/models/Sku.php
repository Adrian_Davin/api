<?php
namespace Models;
use Phalcon\Db as Database;

class Sku extends \Models\BaseModel
{
    public function initialize()
    {
        
    }

    public function saveSkuRequestAttempt($params = array())
    {
        $response = array(
            'data' => array(),
            'errors' => ''
        );

        if (!isset($params['sku'])) {
            $response['error'] = 'Data needed';
            return $response;
        }

        if (count($params['sku']) == 0) {
            $response['error'] = 'Data needed';
            return $response;
        }

        $skuAttempt = array();    
        $this->useReadOnlyConnection();
        foreach($params['sku'] as $value) {
            $attempt = 1;
            $sql = "select count(*) as jml from sku_request_attempt where sku = '{$value}'";                
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0){
                $result->setFetchMode(Database::FETCH_ASSOC);
                $resultArray = $result->fetch();
                if (isset($resultArray['jml'])) {
                    $attempt = $resultArray['jml'] + 1;
                }
            }

            $skuAttempt[$value] = $attempt;
        }

        $logData = '';
        $adminUserID = $params['admin_user_id'];
        $currentDatetime = date('Y-m-d H:i:s');
        $sql = "insert into sku_request_attempt (sku,request_date,attempt,admin_user_id) values ";
        foreach($skuAttempt as $key => $value) {
            $sql .= "('{$key}', '{$currentDatetime}', {$value}, {$adminUserID}),";

            $logData .= '{"method":"POST","endpoint":"/sku/request_attempt","identifier":"'.$key.'","type":"sku","tags":"product","request_body":"{"data":{"sku":"'.$key.'","request_date":"'.$currentDatetime.'","attempt":'.$value.',"admin_user_id":'.$adminUserID.'}}","response_body":"{"errors":[],"data":{"affected_rows":1},"messages":["success"]}","timestamp":"'.$currentDatetime.'"}'.PHP_EOL;
        }

        $sql = rtrim($sql, ",");

        $result = $this->getDi()->getShared('dbMaster')->execute($sql);
        if (!$result) {
            $response['error'] = 'Failed to save data';
            return $response;
        }

        $this->writeFileBeat($logData, "filebeat_api_php.log");

        $response['data']['affected_rows'] = $this->getDi()->getShared('dbMaster')->affectedRows();
        return $response;
    }

    public function writeFileBeat($msg = '', $fileName = '') {
        if (!empty($msg) && !empty($fileName)) {
            $fp = fopen(getenv("FILEBEAT_PATH").$fileName, 'a');
            fwrite($fp, $msg);
            fclose($fp);
        }
    }
}