<?php

namespace Models;

class MasterEmailTemplate extends \Models\BaseModel
{

    /**
     *
     * @var string
     */
    protected $template_code;

    /**
     *
     * @var string
     */
    protected $template_id;

    /**
     *
     * @var string
     */
    protected $company_code;

    /**
     * Method to set the value of field template_code
     *
     * @param string $template_code
     * @return $this
     */
    public function setTemplateCode($template_code)
    {
        $this->template_code = $template_code;

        return $this;
    }

    /**
     * Method to set the value of field template_id
     *
     * @param string $template_id
     * @return $this
     */
    public function setTemplateId($template_id)
    {
        $this->template_id = $template_id;

        return $this;
    }

    /**
     * Method to set the value of field company_code
     *
     * @param string $company_code
     * @return $this
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;

        return $this;
    }

    /**
     * Returns the value of field template_code
     *
     * @return string
     */
    public function getTemplateCode()
    {
        return $this->template_code;
    }

    /**
     * Returns the value of field template_id
     *
     * @return string
     */
    public function getTemplateId()
    {
        return intVal($this->template_id);
    }

    /**
     * Returns the value of field company_code
     *
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'master_email_template';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterEmailTemplate[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MasterEmailTemplate
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
