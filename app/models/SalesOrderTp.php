<?php

namespace Models;

class SalesOrderTp extends \Models\BaseModel
{
    protected $sap_no;
    protected $odi;
    protected $sku;
    protected $store_code;
    protected $qty;
    protected $status;
    protected $source;
    protected $message;
    protected $created_at;
    protected $updated_at;

    public function initialize()
    {

    }
    public function getSapNo()
    {
        return $this->sap_no;
    }

    public function setSapNo($sap_no)
    {
        $this->sap_no = $sap_no;
    }

    public function getOdi()
    {
        return $this->odi;
    }

    public function setOdi($odi)
    {
        $this->odi = $odi;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function getStoreCode()
    {
        return $this->store_code;
    }

    public function setStoreCode($store_code)
    {
        $this->store_code = $store_code;
    }

    public function getQty()
    {
        return $this->qty;
    }

    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setSource($source)
    {
        $this->source = $source;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_order_tp", "TRIGGERED");
    }
}