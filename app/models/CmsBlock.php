<?php

namespace Models;

class CmsBlock extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $cms_block_id;
    protected $title;
    protected $body_html;
    protected $status;
    protected $company_code;
    protected $identifier;

    protected $created_at;
    protected $updated_at;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getCmsBlockId()
    {
        return $this->cms_block_id;
    }

    /**
     * @param int $cms_block_id
     */
    public function setCmsBlockId($cms_block_id)
    {
        $this->cms_block_id = $cms_block_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

        /**
     * Get the value of identifier
     */ 
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set the value of identifier
     *
     * @return  self
     */ 
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getBodyHtml()
    {
        return $this->body_html;
    }

    /**
     * @param mixed $body_html
     */
    public function setBodyHtml($body_html)
    {
        $this->body_html = $body_html;
    }

      /**
     * Get the value of company_code
     */ 
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * Set the value of company_code
     *
     * @return  self
     */ 
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }  

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cms_block';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }
}
