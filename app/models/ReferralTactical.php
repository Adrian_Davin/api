<?php

namespace Models;

// use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
// use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class ReferralTactical extends \Models\BaseModel
{
  /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $referral_tactical_id;

    protected $promo_title;

    protected $campaign_code;

    protected $company_code;
    
    protected $url;

    protected $header_banner;

    protected $header_title;

    protected $start_date;

    protected $end_date;

    protected $voucher_max_amount;

    protected $total_voucher;

    protected $status;

    protected $how_to_desc;

    protected $tnc;

    protected $created_at;

    protected $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'referral_tactical';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return int 
     */
    public function getReferralTacticalID()
    {
        return $this->referral_tactical_id;
    }

    /**
     * @param int $referral_tactical_id
     */
    public function setReferralTacticalID($referral_tactical_id)
    {
        $this->referral_tactical_id = $referral_tactical_id;
    }

    public function getPromoTitle() 
    {
        return $this->promo_title;
    } 

    public function setPromoTitle($promo_title)
    {
        $this->promo_title = $promo_title;
    }

    public function getCampaignCode() 
    {
        return $this->campaign_code;
    } 

    public function setCampaignCode($campaign_code)
    {
        $this->campaign_code = $campaign_code;
    }

    public function getCompanyCode() 
    {
        return $this->company_code;
    } 

    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    public function getUrl() 
    {
        return $this->url;
    } 

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getHeaderBanner() 
    {
        return $this->header_banner;
    } 

    public function setHeaderBanner($header_banner)
    {
        $this->header_banner = $header_banner;
    }

    public function getHeaderTitle() 
    {
        return $this->header_title;
    } 

    public function setHeaderTitle($header_title)
    {
        $this->header_title = $header_title;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

     /**
     * @return int
     */
    public function getVoucherMaxAmount()
    {
        return $this->voucher_max_amount;
    }

     /**
     * @param int $voucher_max_amount
     */
    public function setVoucherMaxAmount($voucher_max_amount)
    {
        $this->voucher_max_amount = $voucher_max_amount;
    }

    /**
     * @return int
     */
    public function getTotalVoucher() 
    {
        return $this->total_voucher;
    } 

    /**
     * @param int $total_voucher
     */
    public function setTotalVoucher($total_voucher)
    {
        $this->total_voucher = $total_voucher;
    }

    public function getHowToDesc() 
    {
        return $this->how_to_desc;
    } 

    public function setHowToDesc($how_to_desc)
    {
        $this->how_to_desc = $how_to_desc;
    }

    public function getTnc() 
    {
        return $this->tnc;
    } 

    public function setTnc($tnc)
    {
        $this->tnc = $tnc;
    }

      /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

     /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function setFromArray($params = array()){

        foreach ($params as $event => $val){
          if (property_exists($this, $event)) {
                $this->{$event} = $val;
            }
        }

      // get not send data but have in this parameter
      $thisArray = get_class_vars(get_class($this));
      $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
  }

  public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }
}