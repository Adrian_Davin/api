<?php

namespace Models;

use \Library\APIWrapper;
use Phalcon\Db as Database;
class SalesOrderItemMdr extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_order_item_mdr_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_item_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $sales_order_id;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $mdr_type;

    protected $rate;

    protected $installment_term;

    protected $payment_type;

    protected $value;

    protected $value_customer;

    protected $percentage_customer;

    protected $percentage_bu;

    protected $tiering_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('sales_order_item_id', 'Models\SalesOrderItem', 'sales_order_item_id', array('alias' => 'SalesOrderItem'));
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
        $this->hasMany('sales_order_item_id', 'Models\SalesInvoiceItem', 'sales_order_item_id', array('alias' => 'SalesInvoiceItem'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_order_item_mdr';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderItemFlat[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrderItemFlat
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param array $data_array
     */
    public function setFromArray($data_array = array()) 
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function assignAllValue($val) {
        foreach($val as $keyVal => $value) {
            if (is_array($value)) {
                $this->assignAllValue($value);
                
            } else if(property_exists($this, $keyVal)) {
                $this->{$keyVal} = $value;
            }
        }
    }

    /**
     * @param array $columns
     * @param bool $showEmpty
     * @return mixed
     */
    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        return $view;
    }


    /**
     * Get the value of sales_order_item_mdr_id
     *
     * @return  integer
     */ 
    public function getSalesOrderItemMdrId()
    {
        return $this->sales_order_item_mdr_id;
    }

    /**
     * Set the value of sales_order_item_mdr_id
     *
     * @param  integer  $sales_order_item_mdr_id
     *
     * @return  self
     */ 
    public function setSalesOrderItemMdrId($sales_order_item_mdr_id)
    {
        $this->sales_order_item_mdr_id = $sales_order_item_mdr_id;

        return $this;
    }

    /**
     * Get the value of sales_order_item_id
     *
     * @return  integer
     */ 
    public function getSalesOrderItemId()
    {
        return $this->sales_order_item_id;
    }

    /**
     * Set the value of sales_order_item_id
     *
     * @param  integer  $sales_order_item_id
     *
     * @return  self
     */ 
    public function setSalesOrderItemId($sales_order_item_id)
    {
        $this->sales_order_item_id = $sales_order_item_id;

        return $this;
    }

    /**
     * Get the value of rate
     *
     * @return  string
     */ 
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set the value of rate
     *
     * @param  string  $rate
     *
     * @return  self
     */ 
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get the value of installment_term
     */ 
    public function getInstallmentTerm()
    {
        return $this->installment_term;
    }

    /**
     * Set the value of installment_term
     *
     * @return  self
     */ 
    public function setInstallmentTerm($installment_term)
    {
        $this->installment_term = $installment_term;

        return $this;
    }

    /**
     * Get the value of payment_type
     */ 
    public function getPaymentType()
    {
        return $this->payment_type;
    }

    /**
     * Set the value of payment_type
     *
     * @return  self
     */ 
    public function setPaymentType($payment_type)
    {
        $this->payment_type = $payment_type;

        return $this;
    }

    /**
     * Get the value of value
     */ 
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set the value of value
     *
     * @return  self
     */ 
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get the value of mdr_type
     *
     * @return  string
     */ 
    public function getMdrType()
    {
        return $this->mdr_type;
    }

    /**
     * Set the value of mdr_type
     *
     * @param  string  $mdr_type
     *
     * @return  self
     */ 
    public function setMdrType($mdr_type)
    {
        $this->mdr_type = $mdr_type;

        return $this;
    }


    /**
     * Get the value of value_customer
     * @return  decimal
     */ 
    public function getValueCustomer()
    {
        return $this->value_customer;
    }

    /**
     * Set the value of value_customer
     *
     * @return  decimal
     */ 
    public function setValueCustomer($value_customer)
    {
        $this->value_customer = $value_customer;

        return $this;
    }

}
