<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class Customer extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $customer_id;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $first_name;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $last_name;

    /**
     *
     * @var string
     * @Column(type="string", length=128, nullable=true)
     */
    protected $email;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $password;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $phone;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $birth_date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $gender;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $is_new;


    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $last_login;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $password_token;

    /**
     * @var \Models\Customer\Address\Collection
     */
    protected $shipping_address;

    /**
     * @var \Models\Customer\Address\Collection
     */
    protected $billing_address;

    /**
     * @var \Models\Customer\Group\Collection
     */
    protected $groups;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    protected $old_password;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $registered_by;
    protected $company_code;    
    protected $fingerprint;
    protected $login_by;
    
    protected $is_phone_verified;
    protected $is_email_verified;
    protected $member_to_update;
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('customer_id', 'Models\Company2Customer', 'customer_id', array('alias' => 'Company2Customer'));
        $this->hasMany('customer_id', 'Models\Customer2Group', 'customer_id', array('alias' => 'Customer2Group'));
        $this->hasMany('customer_id', 'Models\CustomerAddress', 'customer_id', array('alias' => 'CustomerAddress'));
        $this->hasMany('customer_id', 'Models\SalesCustomer', 'customer_id', array('alias' => 'SalesCustomer'));
        $this->hasOne('customer_id', 'Models\CustomerToken', 'customer_id', array('alias' => 'CustomerToken'));
        $this->hasOne('customer_id', 'Models\CustomerAceDetails', 'customer_id', array('alias' => 'CustomerAceDetails'));
        $this->hasOne('customer_id', 'Models\CustomerInformaDetails', 'customer_id', array('alias' => 'CustomerInformaDetails'));
        $this->hasOne('customer_id', 'Models\Customer2Customer', 'customer_id_odi', array('alias' => 'Customer2Customer'));
        $this->hasOne('customer_id', 'Models\Customer2Customer', 'customer_id_ahi', array('alias' => 'Customer2Customer'));
        $this->hasOne('customer_id', 'Models\CustomerB2bCompany', 'customer_id', array('alias' => 'CustomerB2bCompany'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        if (is_array($data_array)) {
            \Helpers\LogHelper::log("customer_payload", "Payload: ".json_encode($data_array));
        }

        if(!empty($data_array['birth_date'])) {
            $this->birth_date = date("Y-m-d", strtotime($data_array['birth_date']));
        }

        if(!empty($data_array['password'])) {
            $this->password = hash('sha256', $data_array['password']);
        }

        if(!empty($data_array['old_password'])) {
            $this->old_password = $data_array['old_password'];
        }

        if(!empty($data_array['login_by'])) {
            $this->login_by = $data_array['login_by'];
        }

        // added is_phone_verified and is_email_verified
        if (!empty($data_array['is_phone_verified'])) {
            $this->is_phone_verified = $data_array['is_phone_verified'];
        }

        // if (!empty($data_array['registered_by'])) {
        //     $this->registered_by = $data_array['registered_by'];
        // }

        if (!empty($data_array['is_email_verified'])) {
            $this->is_email_verified = $data_array['is_email_verified'];
        }

        if (!empty($data_array['member_to_update'])) {
            $this->setMemberToUpdate($data_array['member_to_update']);
        }

        if(!empty($data_array['shipping_address'])) {
            $shippingAddresses = array();
            foreach($data_array['shipping_address'] as $shipping_address) {
                $shippingAddressModel = new \Models\CustomerAddress();
                $shipping_address['address_type'] = "shipping";
                $shipping_address['customer_id'] = !empty($data_array['customer_id']) ? $data_array['customer_id'] : 0;
                $shippingAddressModel->setFromArray($shipping_address);

                $shippingAddresses[] = $shippingAddressModel;
            }

            $this->shipping_address = new \Models\Customer\Address\Collection($shippingAddresses);

        }

        if(!empty($data_array['billing_address'])) {
            $billingAddresses = array();
            foreach($data_array['billing_address'] as $billing_address) {
                $billingAddressModel = new \Models\CustomerAddress();
                $billing_address['address_type'] = "billing";
                $billing_address['customer_id'] = !empty($data_array['customer_id']) ? $data_array['customer_id'] : 0;
                $billingAddressModel->setFromArray($billing_address);

                $billingAddresses[] = $billingAddressModel;
            }

            $this->billing_address = new \Models\Customer\Address\Collection($billingAddresses);
        }

        if(!empty($data_array['group'])) {
            $customerGroupModel = new \Models\CustomerGroup();
            $customerGroups = array();

            // Mutate group name SELMA to SLM for old client apps
            $tempGroup = [];
            foreach($data_array['group'] as $groupKey => $groupVal) {
                // If the group key doesn't have prefix SELMA, then just add it to $tempGroup without change anything
                if (!substr($groupKey, 0, 5) == "SELMA") {
                    $tempGroup[$groupKey] = $groupVal;
                    continue;
                }

                // The group key have prefix SELMA, so we have to change it to SLM
                $tempGroup[str_replace("SELMA", "SLM", $groupKey)] = $groupVal;
            }
            $data_array['group'] = $tempGroup;
             
            foreach($data_array['group'] as $group_name => $member_number) {
                if(strpos($group_name, "is_verified") > 0 || strpos($group_name, "expiration_date") > 0 || strpos($group_name, "update_timestamp") > 0){
                    continue;
                }

                $customer_2_group = new \Models\Customer2Group();
                $customerGroup = $customerGroupModel::findFirst("group_name = '".$group_name."'");
                $groupWithMember = array('AHI', 'HCI', 'TGI', 'SLM', 'GSP');
                if (in_array($group_name, $groupWithMember) && empty($member_number)) {
                    continue;
                }

                // Give update_timestamp null as default value, in case there is weird condition that customer_id contains empty value
                $groupData = [
                    "customer_id" => !empty($data_array['customer_id']) ? $data_array['customer_id'] : 0,
                    "group_id" => !empty($customerGroup) ? $customerGroup->getGroupId(): 0,
                    "member_number" => $member_number,
                    "update_timestamp" => NULL,
                    "is_verified" => 0,
                    "expiration_date" => NULL
                ];

                // Set is verified = 10 for group id cohesive
                if ($groupData['group_id'] == 1) {
                    $groupData['is_verified'] = 10;
                }
                
                // Modify is_verified
                if (!empty($data_array['group'][$group_name.'_is_verified'])){
                    $groupData['is_verified'] = $data_array['group'][$group_name.'_is_verified'];
                }

                // Modify expiration_date
                if (!empty($data_array['group'][$group_name.'_expiration_date'])){
                    $groupData['expiration_date'] = date("Y-m-d H:i:s", strtotime($data_array['group'][$group_name.'_expiration_date']));
                }

                // Check if the member is updated
                $dateNow = date("Y-m-d H:i:s");
                if (!empty($this->member_to_update) && in_array($group_name, $this->member_to_update)){
                  $groupData['update_timestamp'] = $dateNow;
                } else {
                    // Otherwise get the previous update timestamp from customer 2 group table
                    $foundMember = $customer_2_group::findFirst("member_number = '". $member_number ."' AND customer_id = ". $groupData["customer_id"]);
                    if (!empty($foundMember)) {
                        // Set the timestamp to groupdata
                        $groupData['update_timestamp'] = !empty($foundMember->getUpdateTimestamp()) ? $foundMember->getUpdateTimestamp() : $dateNow;
                    }
                }
                $customer_2_group->setFromArray($groupData);
                $customerGroups[] = $customer_2_group;
            }
            $this->groups = new \Models\Customer\Group\Collection($customerGroups);

        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = preg_replace('/[^A-Za-z0-9- ]/', '', $first_name);
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = preg_replace('/[^A-Za-z0-9- ]/', '', $last_name);
        // $this->last_name = $last_name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * @param string $birth_date
     */
    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * @param string $last_login
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
    }

    /**
     * @return string
     */
    public function getPasswordToken()
    {
        return $this->password_token;
    }

    /**
     * @param string $password_token
     */
    public function setPasswordToken($password_token)
    {
        $this->password_token = $password_token;
    }

    /**
     * @return string
     */
    public function getRegisteredBy()
    {
        return $this->registered_by;
    }

    /**
     * @param string $registered_by
     */
    public function setRegisteredBy($registered_by)
    {
        $this->registered_by = $registered_by;
    }

    /**
     * @return string
     */
    public function getOldPassword()
    {
        return $this->old_password;
    }

    /**
     * @param string $old_password
     */
    public function setOldPassword($old_password)
    {
        $this->old_password = $old_password;
    }

    /**
     * @return Customer\Group\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param Customer\Group\Collection $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return mixed
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param mixed $company_code
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * @return mixed
     */
    public function getIsPhoneVerified()
    {
        return $this->is_phone_verified;
    }

    /**
     * @param mixed $is_phone_verified
     */
    public function setIsPhoneVerified($is_phone_verified)
    {
        $this->is_phone_verified = $is_phone_verified;
    }

    /**
     * @return mixed
     */
    public function getIsEmailVerified()
    {
        return $this->is_email_verified;
    }

    /**
     * @param mixed $is_email_verified
     */
    public function setIsEmailVerified($is_email_verified)
    {
        $this->is_email_verified = $is_email_verified;
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);

        if(!empty($this->groups)) {
            $view['groups'] = $this->groups->getDataArray();
        } else if(!empty($this->Customer2Group)) {
            $groups = [];
            foreach ($this->Customer2Group as $customerGroup) {
                $customerGroupObj = new \Models\Customer2Group();
                $customerGroupObj->assign($customerGroup->toArray());
                $groups[] = $customerGroupObj;
            }

            $this->groups = new \Models\Customer\Group\Collection($groups);

            $view['groups'] = $this->groups->getDataArray();
        }

        if(!empty($this->shipping_address)) {
            $view['shipping_address'] = $this->shipping_address->getDataArray();
        }

        if(!empty($this->billing_address)) {
            $view['billing_address'] = $this->billing_address->getDataArray();
        }

        return $view;
    }

    public function beforeCreate()
    {
        parent::beforeCreate();
        $this->skipAttributesOnCreate(['updated_at','last_login']);
    }

    public function beforeUpdate()
    {
        parent::beforeUpdate();
        if(empty($this->email)) {
            $this->setSkipAttributeOnUpdate(['email']);
            \Helpers\LogHelper::log("empty_email", json_encode($this->toArray()));
        }
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_customer", "TRIGGERED: " .json_encode($this->toArray()));
    }

    public function saveData($log_file_name = "", $state = "", $tx = false)
    {
        try {
            // parent::saveData();
            $this->useWriteConnection();

            if (!$tx) {
                // Create a transaction manager
                $manager = new TxManager();
                // Request a transaction
                $manager->setDbService($this->getConnection());
                $transaction = $manager->get();
            } else {
                $transaction = $tx;
            }

            $this->setTransaction($transaction);

            $action = $this->getAction();
            if($action == "create") {
                $saveStatus = $this->create();
            } else {
                $saveStatus = $this->update();
            }

            if ($saveStatus === false) {
                $messages = $this->getMessages();
                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log("customer", "Customer save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Save/Update Customer failed";

                $transaction->rollback(
                    "Save/Update Customer failed"
                );
                return false;
            }

            if(!empty($this->billing_address)) {
                try {
                    $this->billing_address->setCustomerId($this->customer_id);
                    $this->billing_address->saveData($transaction);
                } catch (\Exception $e) {
                    $transaction->rollback(
                        $e->getMessage()
                    );
                }
            }

            if(!empty($this->shipping_address)) {
                try {
                    $this->shipping_address->setCustomerId($this->customer_id);
                    $this->shipping_address->saveData($transaction);
                } catch (\Exception $e) {
                    $transaction->rollback(
                        $e->getMessage()
                    );
                }
            }

            // always delete customer group
            // note : if customer only login, don't delete group, firstname is mandatory when update
            if(!empty($this->first_name) && empty($this->login_by)) {
                $groupModel = new \Models\Customer2Group();
                $customerGroups = $groupModel::find("customer_id = " . $this->customer_id);

                if ($customerGroups->count() > 0) {
                    foreach ($customerGroups as $customerGroup) {
                        $customerGroup->setTransaction($transaction);
                        if ($customerGroup->delete() === false) {
                            $messages = $customerGroup->getMessages();

                            $errMsg = array();
                            foreach ($messages as $message) {
                                $errMsg[] = $message->getMessage();
                            }

                            \Helpers\LogHelper::log($log_file_name, "Save data failed, error : " . json_encode($errMsg));
                            $this->errorCode = "RR301";
                            $this->errorMessages = "Failed when try to save data";

                            $transaction->rollback(
                                "Error when try to delete customer groups"
                            );
                        }
                    }
                }
            }

            // add back customer group
            if(!empty($this->groups)) {
                try {
                    $this->groups->setCustomerId($this->customer_id);
                    $this->groups->saveData($transaction);
                } catch (\Exception $e) {
                    $transaction->rollback(
                        $e->getMessage()
                    );
                }
            }

            // TODO: Potential bugs, happens when ACE register and also normal registration. The issue is that 
            // group_id 1 will not be saved but after the first login, the group_id registered.
            // If front end not sending any group, create default group as guest or registered
            $customer2group = new \Models\Customer2Group();
            $customer2GroupRRRModel = new \Models\Customer2Group();
            $groupData['customer_id'] = $this->customer_id;
            if($this->status == 5) {
                $groupData['group_id'] = 0; // this is guest
            } else {
                $groupData['group_id'] = 1; // Register customer
                $customerGroupsRRR = $customer2GroupRRRModel::findFirst("customer_id = ". $this->customer_id ." AND group_id = 1" . " AND member_number LIKE 'RRR%'");
            }
            if (!empty($customerGroupsRRR)) {
                $groupData['member_number'] = $customerGroupsRRR->getMemberNumber();
            }else{
                $groupData['member_number'] = '';
            }
            $customer2group->setFromArray($groupData);
            $customer2group->setTransaction($transaction);
            $customer2group->saveData("customer");

            // In here we need to make sure, if there are any SalesCustomer data that use the same customer_id but with customer_is_guest 1, we should update it to 0
            // Note: Updating can be done after altering table sales_customer and add sales_customer_id (ALTER TABLE sales_customer ADD sales_customer_id INT AUTO_INCREMENT NOT NULL, ADD PRIMARY KEY (sales_customer_id);)
            $needToDeleteAddress = 0;
            if ($this->status > 5) {
                // it means it wants to register customer. 
                $salesCustomerObj = $this->SalesCustomer;
                foreach ($salesCustomerObj as $salesCustomer) {
                    if (empty($salesCustomer)) {
                        break;
                    }

                    if ($salesCustomer->getCustomerIsGuest() > 0) {
                        $needToDeleteAddress = 1;
                        $salesCustomer->setCustomerIsGuest(0);
                        $salesCustomer->saveData();
                    }
                }
            }

            if ($needToDeleteAddress > 0) {
                // Prepare deleting customer_address when registering from guest to customer AND don't have any orders yet.
                $salesOrderModel = new \Models\SalesOrder();
                $orderResult = $salesOrderModel->findFirst(array(
                    "conditions" => "customer_id = ".$this->customer_id
                ));

                if (empty($orderResult)) {
                    // no orders mean we are save to delete addresses
                    $customerAddressModel = new \Models\CustomerAddress();
                    $addresses  = $customerAddressModel->find(array(
                        "conditions" => "customer_id = ".$this->customer_id
                    ));
                    if ($addresses->count() > 0) {
                        foreach ($addresses as $val) {
                            $customerAddressModel = new \Models\CustomerAddress();
                            $customerAddressModel->setFromArray(array(
                                "address_id" => $val->getAddressId(),
                                "status" => 0
                            ));
                            // don't forget to set skip attribute
                            $customerAddressModel->saveData("delete_customer_address_register");
                        }
                    }
                }
            }


            if (!$tx) {
                $transaction->commit();
            } else {
                $tx = $transaction;
            }

        } catch (TxFailed $e) {
            \Helpers\LogHelper::log("customer_lock_timeout", "Error : " . json_encode($e->getMessage()));
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("customer_lock_timeout", "Error : " . json_encode($e->getMessage()));
            throw new \Library\HTTPException(
                "There seem some problem with our system, please contact ruparupa tech support",
                500,
                array(
                    'msgTitle' => 'Internal Server Error',
                    'dev' => $e->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * Check customer blacklist based on email or phone
     *
     * @param array $params Must contain email, phone and status
     * @return string return the customer is in blacklist or not
     */
    public function isBlacklistUser($params = array())
    {
        try {
            $flagBlacklist = 'no';
            if (!empty($params['email'])) {
                $sql = "SELECT COUNT(*) as jml FROM customer_alert WHERE email = '{$params['email']}' AND status = {$params['status']};";
                $this->useReadOnlyConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $hasil = $result->fetchAll();
                    if ($hasil[0]['jml'] > 0) {
                        $flagBlacklist = 'yes';
                    }
                }
            }

            if (!empty($params['phone'])) {
                $sql = "SELECT COUNT(*) as jml FROM customer_alert WHERE phone = '{$params['phone']}' AND status = {$params['status']};";
                $this->useReadOnlyConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $hasil = $result->fetchAll();
                    if ($hasil[0]['jml'] > 0) {
                        $flagBlacklist = 'yes';
                    }
                }
            }

            if (!empty($params['customer_id'])) {
                $sql = "SELECT COUNT(*) as jml FROM customer_alert WHERE customer_id = '{$params['customer_id']}' AND status = {$params['status']};";
                $this->useReadOnlyConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $hasil = $result->fetchAll();
                    if ($hasil[0]['jml'] > 0) {
                        $flagBlacklist = 'yes';
                    }
                }
            }

        } catch (\Exception $e) {
            $flagBlacklist = 'no';
        }

        return $flagBlacklist;
    }

    public function isCustomerAbuser($params = array())
    {
        try {
            $flagBlacklist = 'no';
           
            if (!empty($params['customer_id'])) {
                $sql = "SELECT COUNT(*) as jml FROM customer_abuser_group WHERE customer_id = '{$params['customer_id']}';";
                $this->useReadOnlyConnection();
                $result = $this->getDi()->getShared($this->getConnection())->query($sql);
                if($result->numRows() > 0) {
                    $result->setFetchMode(Database::FETCH_ASSOC);
                    $hasil = $result->fetchAll();
                    if ($hasil[0]['jml'] > 0) {
                        $flagBlacklist = 'yes';
                    }
                }
            }

        } catch (\Exception $e) {
            $flagBlacklist = 'no';
        }

        return $flagBlacklist;
    }

    // NOT USING COMPANY CODE
    public function isBlacklistFingerprint($params = array())
    {
        return $flagBlacklist = 'no';
        // try {
        //     $flagBlacklist = 'no';
        //     //have attempt with same fingerprint in more than 5 email or check fingerprint is block in customer_alert
        //     // if already blacklist we not need to check it again
        //     if (!empty($params['email']) && $flagBlacklist == 'no') {
                
        //         $customer = $this->findFirst("email = '".$params['email']."' and status = ".$params['status']);
        //         if(!empty($customer)) {
        //             if (!empty($customer->toArray()['fingerprint'])) {
        //                 $fingerprint = $customer->toArray()['fingerprint'];
        //                 $ex = explode(',', $fingerprint);

        //                 // is customer login in whitelist device?
        //                 $whitelist = 'no';
        //                 $exWhitelist = explode(',', getenv('FINGERPRINT_WHITELIST'));
        //                 foreach($ex as $x) {
        //                     if(in_array($x, $exWhitelist)){
        //                         $whitelist = 'yes';
        //                     }
        //                 }

        //                 if ($whitelist == 'no') {
        //                     // check fingerprint is block in customer_alert
        //                     // if already blacklist we not need to check it again
        //                     foreach($ex as $x) {
        //                         $customerAlertModel = new \Models\CustomerAlert();
        //                         $resultCustomerAlert = $customerAlertModel::query();
        //                         $resultCustomerAlert->andWhere("Models\CustomerAlert.fingerprint like '%". $x."%'");
        //                         $resultTotalCustomerAlert = $resultCustomerAlert->execute();
        //                         if(!empty($resultTotalCustomerAlert->count())) {
        //                             $flagBlacklist = 'yes';

        //                             $customerBlockModel = new \Models\CustomerAlert();
        //                             $customerBlockResult = $customerBlockModel->findFirst("email = '".$customer->email."' and status = ".$params['status']);
        //                             if(!$customerBlockResult) {
        //                                 //insert data to customer alert
        //                                 $customerAlertInsertModel = new \Models\CustomerAlert();
        //                                 $dataCustomer['cutomer_id'] =  $customer->customer_id;
        //                                 $dataCustomer['email'] =  $customer->email;
        //                                 $dataCustomer['phone'] =  $customer->phone;
        //                                 $dataCustomer['remark'] = 'Sign in using blacklist device';
        //                                 $dataCustomer['status'] =  10;
        //                                 $dataCustomer['fingerprint'] =  $x;
        //                                 $customerAlertInsertModel->setFromArray($dataCustomer);
        //                                 $customerAlertInsertModel->saveData();
        //                             }
        //                         }
        //                     }


        //                     // have attempt with same fingerprint in more than 5?
        //                     if ($flagBlacklist == 'no') {
        //                         foreach($ex as $x) {
        //                             $customerAttemptModel = new \Models\Customer();
        //                             $resultCustomer = $customerAttemptModel::query();
        //                             $resultCustomer->andWhere("Models\Customer.fingerprint like '%". $x."%'");
        //                             $resultCustomerTotal = $resultCustomer->execute();
        //                             if($resultCustomerTotal->count() > getenv('FINGERPRINT_LIMIT')) {
        //                                 $flagBlacklist = 'yes';
        //                                 // insert or update all customer that using same fingerprint to customeralert
        //                                 foreach ($resultCustomerTotal as $cust) {
        //                                     // we need to check if customer already in customer alert or not, if no insert
        //                                     $customerBlockModel = new \Models\CustomerAlert();
        //                                     $customerBlockResult = $customerBlockModel->findFirst("email = '".$cust->email."' and status = ".$params['status']);
        //                                     if(!$customerBlockResult) {
        //                                         //insert data to customer alert
        //                                         $customerAlertInsertModel = new \Models\CustomerAlert();
        //                                         $dataCustomer['cutomer_id'] =  $cust->customer_id;
        //                                         $dataCustomer['email'] =  $cust->email;
        //                                         $dataCustomer['phone'] =  $cust->phone;
        //                                         $dataCustomer['remark'] = 'Sign in to multiple accounts using same device';
        //                                         $dataCustomer['status'] =  10;
        //                                         $dataCustomer['fingerprint'] =  $x;
        //                                         $customerAlertInsertModel->setFromArray($dataCustomer);
        //                                         $customerAlertInsertModel->saveData();
        //                                     }
        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }

        // } catch (\Exception $e) {
        //     $flagBlacklist = 'no';
        // }

        // return $flagBlacklist;
    }

    public function getCustomerIdByEmail($params)
    {
        $custIdArray = array();
        if (!empty($params['email'])) {
            $sql = "SELECT customer_id FROM customer WHERE email LIKE '%{$params['email']}%';";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $custIdArray = $result->fetchAll();
            }
        }

        return $custIdArray;
    }

    public function getCustomerByCustomerId($customerId = FALSE)
    {
        $custArray = array();
        if ($customerId) {
            $sql = "SELECT * FROM customer WHERE customer_id = $customerId LIMIT 1;";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $custArray = $result->fetchAll();
            }
        }

        return $custArray;
    }

    public function getCustomerIdByExactEmail($email = '')
    {
        $custID = '';
        if (!empty($email)) {
            $sql = "SELECT customer_id FROM customer WHERE email = '$email' limit 1;";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $custArray = $result->fetch();
                $custID = $custArray['customer_id'];
            }
        }

        return $custID;
    }

    /**
     * Get the value of is_new
     *
     * @return  integer
     */ 
    public function getIsNew()
    {
        return $this->is_new;
    }

    /**
     * Set the value of is_new
     *
     * @param  integer  $is_new
     *
     * @return  self
     */ 
    public function setIsNew($is_new)
    {
        $this->is_new = $is_new;

        return $this;
    }

    /**
     * Get the value of fingerprint
     */ 
    public function getFingerprint()
    {
        return $this->fingerprint;
    }

    /**
     * Set the value of fingerprint
     *
     * @return  self
     */ 
    public function setFingerprint($fingerprint)
    {
        $this->fingerprint = $fingerprint;

        return $this;
    }
    

    /**
     * Get the value of login_by
     */ 
    public function getLogin_by()
    {
        return $this->login_by;
    }

    /**
     * Set the value of login_by
     *
     * @return  self
     */ 
    public function setLogin_by($login_by)
    {
        $this->login_by = $login_by;

        return $this;
    }

    public function getMemberToUpdate()
    {
        return $this->member_to_update;
    }

    public function setMemberToUpdate($member_to_update)
    {
        $this->member_to_update = $member_to_update;
        return $this;
    }
}
