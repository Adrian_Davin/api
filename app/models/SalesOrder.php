<?php

namespace Models;

use DateInterval;
use DateTime;
use Exception;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;
use Helpers\LogHelper;
use Helpers\CartHelper;
use Library\APIWrapper;

class SalesOrder extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $sales_order_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $customer_id;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=false)
     */
    protected $cart_id;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    protected $device;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $order_no;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $subtotal;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $discount_amount;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $handling_fee_adjust;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $shipping_amount;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $shipping_discount_amount;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $gift_cards;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $cart_rules;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $gift_cards_amount;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $grand_total;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $customer_note;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $split_cost;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $source_order_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $source_order_no;

    protected $order_type;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $company_code = 'ODI';

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $store_code = '';

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $store_code_new_retail = '';

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $parent_order_no;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $reference_order_no;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $company_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $cart_type = '';
    
    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $rrr_id;

    /**
     * @var $payment \Models\SalesOrderPayment
     */
    protected $payment;

    /**
     * @var $customer \Models\SalesCustomer
     */
    protected $customer;

    /**
     * @var $items \Models\Items\SalesOrder\Collection
     */
    protected $items;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $remote_ip;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $source_order;

    /**
     * @var $faktur_pajak \Models\SalesFakturPajak
     */
    protected $faktur_pajak;

    /**
     * @var $shipping \Models\SalesOrderAddress
     */
    protected $shipping_address;

    /**
     * @var $shipping \Models\SalesCustomerGroup
     */
    protected $sales_customer_group;

    /**
     * @var $shipping \Models\SalesOrderAddress
     */
    protected $billing_address;

    protected $payer;

    /**
     * @var $items \Models\Invoice\Collection
     */
    protected $invoices;

    /**
     * @var $voucherCollection \Models\Salesrule\Voucher\Collection
     */
    protected $voucherCollection;

    /**
     * @var $salesRulesCustomerCollection \Models\Salesrule\Customer\Collection
     */
    protected $salesRulesCustomerCollection;

    protected $has_changed_payment = -1;

    protected $can_cancel = 0;

    /**
     * @var $customerGroup \Models\CustomerGroup
     */
    protected $customerGroup;

    protected $cart_raw;
    protected $shoppingCartData;

    protected $reorder_customer_confirmation = false;

    const PROTECT_QUOTA_ONLY_VOUCHER_SERBU = false;
    const VOUCHER_PREFIX_SERBU = "SERBA8";

    protected $mdr_customer;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('sales_order_id', 'Models\SalesInvoice', 'sales_order_id', array('alias' => 'SalesInvoice'));
        $this->hasMany('sales_order_id', 'Models\SalesOrderAddress', 'sales_order_id', array('alias' => 'SalesOrderAddress', 'reusable' => true));
        $this->hasMany('sales_order_id', 'Models\SalesCustomerGroup', 'sales_order_id', array('alias' => 'SalesCustomerGroup', 'reusable' => true));
        $this->hasMany('sales_order_id', 'Models\SalesOrderItem', 'sales_order_id', array('alias' => 'SalesOrderItem', 'reusable' => true));
        $this->hasOne('sales_order_id', 'Models\SalesOrderPayment', 'sales_order_id', array('alias' => 'SalesOrderPayment'));
        $this->hasOne('sales_order_id', 'Models\SalesCustomer', 'sales_order_id', array('alias' => 'SalesCustomer', 'reusable' => true));
        $this->hasOne('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer', 'reusable' => true));
        $this->hasMany('sales_order_id', 'Models\SalesStockAssignation', 'sales_order_id', array('alias' => 'SalesStockAssignation'));
        $this->hasMany('sales_order_id', 'Models\SalesruleOrder', 'sales_order_id', array('alias' => 'SalesRuleOrder'));
        $this->hasOne('sales_order_id', 'Models\RemarkHold', 'sales_order_id', array('alias' => 'RemarkHold'));
        $this->hasOne('sales_order_id', 'Models\SalesB2BInformation', 'sales_order_id', array('alias' => 'SalesB2BInformation'));
        $this->hasOne('sales_order_id', 'Models\SalesOrderNpwp', 'sales_order_id', array('alias' => 'SalesOrderNpwp'));
        $this->hasMany('group_id', 'Models\CustomerGroup', 'group_id', array('alias' => 'customerGroup', 'reusable' => true));
        $this->hasMany('sales_order_id', 'Models\SalesOrderItemInstallation', 'sales_order_id', array('alias' => 'SalesOrderItemInstallation', 'reusable' => true));
    }

    /**
     * @return Items\SalesOrder\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * @param int $sales_order_id
     */
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

     /**
     * @return int
     */
    public function getCanCancel()
    {
        return $this->can_cancel;
    }

    /**
     * @param int $can_cancel
     */
    public function setCanCancel($can_cancel)
    {
        $this->can_cancel = $can_cancel;
    }

    /**
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param string $order_no
     */
    public function setDevice($device)
    {
        $this->device = $device;
    }

    /**
     * @return string
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * @param string $order_no
     */
    public function setOrderNo($order_no)
    {
        $this->order_no = $order_no;
    }

    /**
     * @return string
     */
    public function getCartId()
    {
        return $this->cart_id;
    }

    /**
     * @param string $cart_id
     */
    public function setCartId($cart_id)
    {
        $this->cart_id = $cart_id;
    }

    /**
     * @return string
     */
    public function getCartRules()
    {
        return $this->cart_rules;
    }

    /**
     * @param string $cart_rules
     */
    public function setCartRules($cart_rules)
    {
        $this->cart_rules = $cart_rules;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

     /**
     * @return float
     */
    public function getHandlingFeeAdjust()
    {
        return $this->handling_fee_adjust;
    }

    /**
     * @param float $handling_fee_adjust
     */
    public function setHandlingFeeAdjust($handling_fee_adjust)
    {
        $this->handling_fee_adjust = $handling_fee_adjust;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param float $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * @param float $discount_amount
     */
    public function setDiscountAmount($discount_amount)
    {
        $this->discount_amount = $discount_amount;
    }

    /**
     * @return float
     */
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

    /**
     * @param float $shipping_amount
     */
    public function setShippingAmount($shipping_amount)
    {
        $this->shipping_amount = $shipping_amount;
    }

    /**
     * @return float
     */
    public function getShippingDiscountAmount()
    {
        return $this->shipping_discount_amount;
    }

    /**
     * @param float $shipping_discount_amount
     */
    public function setShippingDiscountAmount($shipping_discount_amount)
    {
        $this->shipping_discount_amount = $shipping_discount_amount;
    }

    /**
     * @return string
     */
    public function getGiftCards()
    {
        return $this->gift_cards;
    }

    /**
     * @param string $gift_cards
     */
    public function setGiftCards($gift_cards)
    {
        $this->gift_cards = $gift_cards;
    }

    /**
     * @return mixed
     */
    public function getGiftCardsAmount()
    {
        return empty($this->gift_cards_amount) ? 0 : $this->gift_cards_amount;
    }

    /**
     * @param float $gift_cards_amount
     */
    public function setGiftCardsAmount($gift_cards_amount)
    {
        $this->gift_cards_amount = $gift_cards_amount;
    }

    /**
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->grand_total;
    }

    /**
     * @param float $grand_total
     */
    public function setGrandTotal($grand_total)
    {
        $this->grand_total = $grand_total;
    }

    /**
     * @return string
     */
    public function getCustomerNote()
    {
        return $this->customer_note;
    }

    /**
     * @param string $customer_note
     */
    public function setCustomerNote($customer_note)
    {
        $this->customer_note = $customer_note;
    }

    /**
     * @return string
     */
    public function getSplitCost()
    {
        return $this->split_cost;
    }

    /**
     * @param string $split_cost
     */
    public function setSplitCost($split_cost)
    {
        $this->split_cost = $split_cost;
    }

    /**
     * @return string
     */
    public function getSourceOrder()
    {
        return $this->source_order;
    }

    /**
     * @param string $source_order
     */
    public function setSourceOrder($source_order)
    {
        $this->source_order = $source_order;
    }

     /**
     * @return string
     */
    public function getSourceOrderNo()
    {
        return $this->source_order_no;
    }

    /**
     * @param string $source_order
     */
    public function setSourceOrderNo($source_order_no)
    {
        $this->source_order_no = $source_order_no;
    }

    /**
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }
    
    /**
     * @param string $company_code
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param int $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return SalesOrderAddress
     */
    public function getShippingAddress()
    {
        return $this->shipping_address;
    }

    /**
     * @return SalesOrderAddress
     */
    public function getBillingAddress()
    {
        return $this->billing_address;
    }

        /**
     * @return SalesCustomerGroup
     */
    public function getSalesCustomerGroup()
    {
        return $this->sales_customer_group;
    }

    /**
     * @return Salesrule\Voucher\Collection
     */
    public function getVoucherCollection()
    {
        return $this->voucherCollection;
    }

    /**
     * @return Salesrule\Customer\Collection
     */
    public function getSalesRulesCustomerCollection()
    {
        return $this->salesRulesCustomerCollection;
    }

    /**
     * @return SalesOrderPayment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @return mixed
     */
    public function getOrderType()
    {
        return $this->order_type;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->store_code;
    }

    /**
     * @param string $store_code
     */
    public function setStoreCode($store_code)
    {
        $this->store_code = $store_code;
    }

    /**
     * @return string
     */
    public function getStoreCodeNewRetail()
    {
        return $this->store_code_new_retail;
    }

    /**
     * @param string $store_code_new_retail
     */
    public function setStoreCodeNewRetail($store_code_new_retail)
    {
        $this->store_code_new_retail = $store_code_new_retail;
    }

    /**
     * @return string
     */
    public function getParentOrderNo()
    {
        return $this->parent_order_no;
    }

    /**
     * @param string $parent_order_no
     */
    public function setParentOrderNo($parent_order_no)
    {
        $this->parent_order_no = $parent_order_no;
    }

    /**
     * @return string
     */
    public function getReferenceOrderNo()
    {
        return $this->reference_order_no;
    }

    /**
     * @param string $reference_order_no
     */
    public function setReferenceOrderNo($reference_order_no)
    {
        $this->reference_order_no = $reference_order_no;
    }

    /**
     * @return string
     */
    public function getCartType()
    {
        return $this->cart_type;
    }

    /**
     * @param string $cart_type
     */
    public function setCartType($cart_type)
    {
        $this->cart_type = $cart_type;
    }

    /**
     * @return string
     */
    public function getRRRID()
    {
        return $this->rrr_id;
    }

    /**
     * @param string $rrr_id
     */
    public function setRRRID($rrr_id)
    {
        $this->rrr_id = $rrr_id;
    }

    /**
     * @return string
     */

    public function getCartRaw()
    {
        return $this->cart_raw;
    }

    /**
     * @param string $cartRaw
     */
    public function setCartRaw($cartRaw)
    {
        $this->cart_raw = $cartRaw;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_order';
    }

       /**
     * Get the value of has_changed_payment
     */ 
    public function getHasChangedPayment()
    {
        return $this->has_changed_payment;
    }

    /**
     * Set the value of has_changed_payment
     *
     * @return  self
     */ 
    public function setHasChangedPayment($has_changed_payment)
    {
        $this->has_changed_payment = $has_changed_payment;

        return $this;
    }

          /**
     * Get the value of shoppingCartData
     */ 
    public function getShoppingCartData()
    {
        return $this->shoppingCartData;
    }

    /**
     * Set the value of shoppingCartData
     *
     * @return  self
     */ 
    public function setShoppingCartData($shoppingCartData)
    {
        $this->shoppingCartData = $shoppingCartData;

        return $this;
    }

      /**
     * Get the value of mdr_customer
     */ 
    public function getMdrCustomer()
    {
        return $this->mdr_customer;
    }

    /**
     * Set the value of mdr_customer
     *
     * @return  self
     */ 
    public function setMdrCustomer($mdr_customer)
    {
        $this->mdr_customer = $mdr_customer;

        return $this;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrder[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesOrder
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_order", "TRIGGERED");
    }

    private $shippingAddressList = array();

    private $customerMemberList = array();

    private $employeeData = array();

    public function setFromShoppingCart($data_array = array())
    {
        
        if(isset($data_array['check_status_cart'])){
            if($data_array['check_status_cart']){
                if ($data_array['status'] != 'pending') {
                    $this->errorCode = "RR402";
                    $this->errorMessages[] = "Shopping cart in process";
                    return;
                }
            }
        }else{
            if ($data_array['status'] != 'pending' && $data_array['status'] != 'hold_payment') {
                $this->errorCode = "RR402";
                $this->errorMessages[] = "Shopping cart in process";
                return;
            }
        }
        
        // Split Budget
        $giftCards = json_decode($data_array['gift_cards']);

        $ruparupaSplitBudget = false;
        $ruparupaSku = "";
        $type = "";
        $hargaDiskonRuparupa = 0;
        $voucherAmountRupaRupa = 0;
        
        foreach ($giftCards as $keyGift => $gift) {
            $sql = "SELECT * FROM salesrule_split_budget where rule_id = " . $gift->rule_id;
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);

            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $ruparupaSplitBudget = true;
                $dataRule1212 = $result->fetchAll();
                $ruparupaSku = $dataRule1212[0]['sku'];
                $type = $dataRule1212[0]['type'];
                $hargaDiskonRuparupa = (int)$dataRule1212[0]['discount_amount'];
                $voucherAmountRupaRupa = (int)$gift->voucher_amount_used;
                // re-setup gift_cards
                if ($type == "merchandise") {
                    $giftCards[$keyGift]->voucher_amount_used = (int)$gift->voucher_amount_used - $hargaDiskonRuparupa;
                    $giftCards[$keyGift]->voucher_amount = (int)$gift->voucher_amount - $hargaDiskonRuparupa;
                } else {
                    $giftCards[$keyGift]->voucher_amount_used = $hargaDiskonRuparupa;
                    $giftCards[$keyGift]->voucher_amount = $hargaDiskonRuparupa;
                }
            }else{
                if($gift->voucher_affected == "shipping"){
                    $giftCards[$keyGift]->voucher_amount_used = 0;
                    $ruparupaSplitBudget = true;
                }
            }
        }

        if ($ruparupaSplitBudget) {
            $data_array['gift_cards'] = json_encode($giftCards);
        }

        // TODO: field apa aja yg bisa kosong di new cart
        $canEmpty = ['company_code', 'po', 'remote_ip', 'device', 'gift_cards_amount', 'handling_fee_adjust', 'gift_cards', 'split_cost', 'shipping_amount', 'cart_rules', 'payer', 'customer_note', 'shipping_address', 'billing_address', 'source_order', 'source_order_id', 'source_order_no', 'reference_order_no', 'grand_total_exclude_shipping', 'shipping_discount_amount', 'store_code', 'utm_parameter', 'store_code_new_retail', 'customer_member'];
        foreach ($data_array as $key => $val) {
            if ($key == 'created_at' || $key == 'updated_at') {          
                continue;
            }

            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }

            if ($key == 'reserved_order_no') {
                $this->order_no = $val;
            }

            if ($key == 'status' && $val == 'pending') {
                $this->status = 'new';
            }

            if ($key == 'order_vendor_no') {
                $this->source_order_no = $val;  
            }

            // Proccess into order level if split budget applied

            if ($key == 'gift_cards_amount') {
                if ($ruparupaSplitBudget) {
                    if ($type == "merchandise") {
                        $this->gift_cards_amount = (int)$this->gift_cards_amount - $hargaDiskonRuparupa;
                    } else {
                        $this->gift_cards_amount = ((int)$this->gift_cards_amount - $voucherAmountRupaRupa) + $hargaDiskonRuparupa;
                    }
                }
            }

            if ($key == 'discount_amount') {
                if ($ruparupaSplitBudget) {
                    if ($type == "merchandise") {
                        $this->discount_amount = (int)$this->discount_amount + $hargaDiskonRuparupa;
                    } else {
                        $this->discount_amount = (int)$this->discount_amount + $voucherAmountRupaRupa - $hargaDiskonRuparupa;
                    }
                }
            }

            if ($key == 'customer_company_id') {
                if (intval($val) > 0) {
                    $result = $this->getDi()->getShared('dbMaster')->query("select * from customer_company where customer_company_id = {$val}");
                    $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
                    $resultData = $result->fetchAll();
                    if (count($resultData) > 0) {
                        $rowData = $resultData[0];
                        $salesFakturPajakModel = new \Models\SalesFakturPajak();

                        $dataFakturPajak['company_name'] = $rowData['company_name'];
                        $dataFakturPajak['company_npwp'] = $rowData['company_npwp'];
                        $dataFakturPajak['company_address'] = $rowData['company_address'];
                        $dataFakturPajak['recipient_email'] = $rowData['recipient_email'];
                        if ($rowData['kpp_pratama'] == "yes" || $rowData['kpp_pratama'] == "no") {
                            $dataFakturPajak['kpp_pratama'] = $rowData['kpp_pratama'];
                        }
                        $dataFakturPajak['npwp_image'] = $rowData['npwp_image'];
                        if (is_null($rowData['is_npwp_address']) || $rowData['is_npwp_address'] == ''){
                            $dataFakturPajak['is_npwp_address'] = "no";
                        }else{
                            $dataFakturPajak['is_npwp_address'] = $rowData['is_npwp_address'];
                        }
                        $dataFakturPajak['nitku'] = $rowData['nitku'];
                        $dataFakturPajak['tipe_wajib_pajak'] = $rowData['tipe_wajib_pajak'];
                        $salesFakturPajakModel->setFromArray($dataFakturPajak);
                        
                        $this->faktur_pajak = $salesFakturPajakModel;
                    }
                }
            }

            if (!empty($val)) {                
                if ($key == 'items') {
                    $items = array();
                    foreach ($val as $keyItem => $valItem) {
                        $valItem['reserved_order_no'] = $this->order_no;
                        $valItem['device'] = $this->device;
                        $valItem['store_code_new_retail'] = $this->store_code_new_retail;
                        $valItem['order_type'] = $this->order_type;

                        if (isset($valItem['shipping'])) {
                            $shipping = $valItem['shipping'];

                            if (isset($shipping['shipping_address'])) {
                                if (count($shipping['shipping_address']) > 0 && $shipping['delivery_method'] != 'pickup') {
                                    $shipping['shipping_address']['carrier_id'] = $shipping['carrier_id'];
                                    $this->shippingAddressList[$shipping['group_shipment']] = $shipping['shipping_address'];
                                }
                            }
                            // There are 2 kinds of value for preorder_date on cart
		                    // If the product source isn't marketplace, then we store string that look like this "12/01/2020"
                            // If the product source is marketplace, then we store string that look like this "1" or "2" or "3"

                            // So if the product source is marketplace, 
                            // We have to calculate sales_order_item.preorder_date = now + cart preorder_date (number of days)
                            if (strtolower($valItem["product_source"]) == "marketplace") {
                                $preorder = "";
                                if (!empty($shipping['preorder_date'])) {
                                    $preorderDays = $shipping['preorder_date'];
                                    // Calculate
                                    $preorder = date('d/m/Y', strtotime('+'.$preorderDays.' days'));
                                }
                                $valItem['preorder_date'] = $preorder;
                            } else {
                                $valItem['preorder_date'] = isset($shipping['preorder_date']) ? $shipping['preorder_date'] : "";
                            }

                            //interupt shipping amount
                            $valItem['shipping']['shipping_amount'] = $shipping['shipping_amount'] / $valItem['qty_ordered'];
                            $valItem['shipping']['shipping_discount_amount'] = $shipping['shipping_discount_amount'] / $valItem['qty_ordered'];
                        }
                        //proratelevel items do in here
                        $grandTotal = $data_array['subtotal'] + $data_array['shipping_amount'] - $data_array['shipping_discount_amount'];
                        // $prorate = CartHelper::prorateLevelItems($data_array['items'], $valItem, $data_array['gift_cards'], $grandTotal);
                        $valItem['gift_cards_amount'] = $valItem['marketing']['gift_cards_amount'];

                        // Proccess into items level if split budget applied
                        if ($ruparupaSplitBudget) {
                            if ($ruparupaSku == $valItem['sku']) {
                                if ($type == "merchandise") {
                                    $valItem['marketing']['discount_amount'] = $valItem['marketing']['discount_amount'] + $hargaDiskonRuparupa;
                                    $valItem['gift_cards_amount'] = $valItem['gift_cards_amount'] - $hargaDiskonRuparupa;
                                } else {
                                    $valItem['marketing']['discount_amount'] += $voucherAmountRupaRupa - $hargaDiskonRuparupa;
                                    $valItem['gift_cards_amount'] = ($valItem['gift_cards_amount'] - $voucherAmountRupaRupa) + $hargaDiskonRuparupa;
                                }
                            }
                        }                        

                        $itemObj = new \Models\SalesOrderItem();
                        $itemObj->setFromArray($valItem);

                        $query = "SELECT count(sku) as total 
                        FROM product_stock_override_status 
                        WHERE sku = '".$valItem['sku']."' AND store_code = 'ALLSTORE' 
                        AND status = 0 AND now() >= start_date and now() <= end_date";

                        $result = $this->getDi()->getShared('dbMaster')->query($query);
                        $result->setFetchMode(
                            \Phalcon\Db::FETCH_ASSOC
                        );
                
                        $totalRow = $result->fetchAll()[0]['total'];
                        if($totalRow > 0){
                            $itemObj->setIsDcOnly(10);  
                        }

                        $itemObj->setId($valItem["id"]);

                        // set installation value item
                        $itemObj->setIsInstalled($valItem["installation"]["is_installed"]);
                        $itemObj->setIsInstallationEligible($valItem["installation"]["is_installation_eligible"]);
                        $itemObj->setIsNeedInstallationEligible($valItem["installation"]["is_need_installation_eligible"]);
                        $itemObj->setIsNeedInstallation($valItem["installation"]["is_need_installation"]);

                        // set note
                        $itemObj->setCustomerNote($valItem["customer_note"]);
                        
                        // TODO: apa itu source_order?
                        // if(empty($data_array['source_order']) && $data_array['source_order'] != 'lazada' ) {
                        //     $itemObj->checkStockItem();
                        // }

                        $items[] = $itemObj;
                    }

                    $this->items = new \Models\Items\SalesOrder\Collection($items);
                }

                if ($key == 'customer') {
                    $customer = new \Models\SalesCustomer();
                    $customer->setFromArray($val);
                    $this->customer = $customer;
                    $this->customer_id = $customer->getCustomerId();
                    if (isset($val['type']['customer_type'])) {
                        $this->order_type = $val['type']['customer_type'];
                        $this->company_id = $val['type']['company_id'];
                    }
                }
              
                if (isset($data_array['cart_type']) && $data_array['cart_type'] == "informa_b2b") {
                    $this->order_type = "b2b_informa";
                } else if (isset($data_array['cart_type']) && $data_array['cart_type'] == "ace_b2b") {
                    $this->order_type = "b2b_ace";
                } else if (isset($data_array['cart_type']) && $data_array['cart_type'] == "installation") {
                    $this->order_type = "installation";
                }

                // Set order type for additional cost b2b
                if (isset($data_array['cart_type']) && $data_array['cart_type'] == "additional_cost") {
                    $this->order_type = "additional_cost";
                }

                // Set order type for Whatsapp Shopping
                if (isset($data_array['cart_type']) && $data_array['cart_type'] == "whatsapp_shopping") {
                    $this->order_type = "wa_shopping";
                }

                if (isset($data_array['cart_type']) && $data_array['cart_type'] == "whatsapp_shopping_ctwa") {
                    $this->order_type = "ctwa_shopping";
                }
                
                if ($key == 'customer_member') {
                    foreach($val as $keyCustomerMember => $valCustomerMember){
                        $this->customerMemberList[$keyCustomerMember] = $valCustomerMember;
                    }
                }


                if ($key == 'billing_address') {
                    if (isset($data_array['customer']) && count($data_array['customer']) > 0) {
                        if (isset($data_array['customer']['customer_firstname'])) {
                            $val['first_name'] = (!empty($val['first_name'])) ? $val['first_name'] : $data_array['customer']['customer_firstname'];
                        }

                        if (isset($data_array['customer']['customer_lastname'])) {
                            $val['last_name'] = (!empty($val['last_name'])) ? $val['last_name'] : $data_array['customer']['customer_lastname'];
                        }

                        if (isset($data_array['customer']['customer_phone'])) {
                            $val['phone'] = (!empty($val['phone'])) ? $val['phone'] : $data_array['customer']['customer_phone'];
                        }
                    }

                    $billingAddress = new \Models\SalesOrderAddress();
                    $billingAddress->setFromArray($val);
                    $billingAddress->setCompanyAddressId(0);
                    if (isset($val['company_address_id'])) { // TODO: company_address_id tidak ada di new cart
                        $companyAddressId = \Helpers\SimpleEncrypt::decrypt($val['company_address_id']);
                        if (!empty($companyAddressId)) {
                            $billingAddress->setCompanyAddressId($companyAddressId);
                        }
                    }
                    $this->billing_address = $billingAddress;
                }

                if ($key == 'payment') {
                    $payment = new \Models\SalesOrderPayment();
                    $payment->setFromArray($val);
                    $this->payment = $payment;

                    if($payment->getVaBank() !== "" || $payment->getMethod() =="manual_transfer"){
                        $this->can_cancel = 10;
                    }

                    if (getenv("PAYMENT_IMPROVEMENT_SWITCH")== "OFF"){
                        $this->can_cancel = 0;
                    }
                }

                /**
                 * @todo : Uncomment if we already have payer table
                 */
                /*
                if(!empty($key == 'payer')) {
                    $payer = new \Models\SalesOrderPayer();
                    $payer->setFromArray($data_array['payer']);
                    $this->payer = $payer;
                } */
            } else if (empty($val) && !in_array($key, $canEmpty) && intval($val) !== 0) {
                if ($key == 'grand_total' && $data_array['payment']['method'] == 'free_payment') {
                    continue;
                }
               
                $this->errorCode[] = "RR401";
                $this->errorMessages[] = $key . " is required";
             
            }
        }

        if ($this->reference_order_no != "") {
            $salesOrderModel = new \Models\SalesOrder();
            $referenceOrder = $salesOrderModel->findFirst("order_no = '" . $this->reference_order_no . "'");
            if ($referenceOrder) {
                $referenceOrderArr = $referenceOrder->toArray();
                if ($referenceOrderArr['parent_order_no'] == "" && $referenceOrderArr['reference_order_no'] == "") {
                    $this->order_type = $referenceOrderArr['order_type'];
                    
                    if ($referenceOrderArr['cart_type'] == 'gift') {
                        $this->cart_type = 'gift';
                    } else {
                        $this->cart_type = $referenceOrderArr['cart_type'];
                    }
                } else if ($referenceOrderArr['parent_order_no'] != "") {
                    $parentOrder = $salesOrderModel->findFirst("order_no = '" . $referenceOrderArr['parent_order_no'] . "'");
                    if ($parentOrder) {
                        $parentOrderArr = $parentOrder->toArray();
                        $this->order_type = $referenceOrderArr['order_type'];

                        if ($parentOrderArr['cart_type'] == 'gift') {
                            $this->cart_type = 'gift';
                        } else {
                            $this->cart_type = $parentOrderArr['cart_type'];
                        }
                    }
                }
            }
        }

        $this->setHasChangedPayment($this->isValidChangePaymentMethod($data_array));
        if (getenv("PAYMENT_IMPROVEMENT_SWITCH")== "OFF"){
            $this->setHasChangedPayment(-1);
        }
        // hardcode customer id 284855 for orderb2b@ruparupa.com can't change payment
        if ($data_array['customer']['customer_id'] == 284855 || $data_array['customer']['customer_id'] == 6995318){
            $this->setHasChangedPayment(-1);
        }

        if(isset($data_array['payment']['mdr_customer']) && intval($data_array['payment']['mdr_customer']) > 0 ){
            $this->setMdrCustomer(intval($data_array['payment']['mdr_customer']));
        }
    }

    public function giftCardValidation()
    {
        if (empty($this->gift_cards_amount)) {
            $this->gift_cards_amount = 0;
        }

        if (empty($this->gift_cards)) {
            $this->gift_cards = "[]";
        } else {
            $this->salesRulesCustomerCollection = new \Models\Salesrule\Customer\Collection();
            $this->voucherCollection = new \Models\Salesrule\Voucher\Collection();

            // Validasi penggunaan giftcard lagi, termasuk times_used nya
            $gift_cards = json_decode($this->gift_cards);
            $marketingLib = new \Library\Marketing();
            foreach ($gift_cards as $voucher) {
                // if (!empty($this->store_code_new_retail)) {
                //     $prefix = strtoupper(substr($voucher->voucher_code, 0, 2));
                //     if ($prefix == "IR" || $prefix == "AR") {
                //         $voucher->voucher_code = getenv("NEW_RETAIL_VOUCHER_MEMBER");
                //     } else {
                //         $prefix = strtoupper(substr($voucher->voucher_code, 0, 3));
                //         if ($prefix == "TAM") {
                //             $voucher->voucher_code = getenv("NEW_RETAIL_VOUCHER_MEMBER");
                //         }
                //     }
                // }
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start $marketingLib->getVoucherDetail models/SalesOrder.php Cart id: '.$this->cart_id ." voucher code :".$voucher->voucher_code, 'debug');
                $voucherDetail = $marketingLib->getVoucherDetail($voucher->voucher_code, $this->company_code);
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start $marketingLib->getVoucherDetail models/SalesOrder.php Cart id: '.$this->cart_id." voucher code :".$voucher->voucher_code, 'debug');

                if (!empty($marketingLib->getErrorCode())) {
                    $this->errorCode[] = $marketingLib->getErrorCode();
                    $this->errorMessages = array_merge($this->errorMessages, $marketingLib->getErrorMessages());
                    return;
                } else {
                    if (!empty($voucher->promo_type) && $voucher->promo_type == "invoice_created") {
                        continue;
                    }

                    // Tambahkan Penggunaan giftcard
                    if ($voucherDetail["type"] == 3 && $voucherDetail["customer_id"] == 0) {
                        if (self::PROTECT_QUOTA_ONLY_VOUCHER_SERBU) {
                            if (substr($voucher->voucher_code, 0, strlen(self::VOUCHER_PREFIX_SERBU)) == self::VOUCHER_PREFIX_SERBU) {
                                continue;
                            }
                            $voucherDetail['times_used']++; 
                        } else {
                            continue;
                        }
                    } else {
                        $voucherDetail['times_used']++;
                    }

                    $giftCardModel = new \Models\SalesruleVoucher();
                    $giftCardModel->setFromArray($voucherDetail);

                    // Validasi penggunaan voucher per customer
                    // $isValidUsage = $marketingLib->maxUsageValidation($voucherDetail['rules'], $this->customer->getCustomerId(), $voucherDetail);

                    // if($isValidUsage === false) {
                    //     $this->errorCode[] = $marketingLib->getErrorCode();
                    //     $this->errorMessages = array_merge($this->errorMessages, $marketingLib->getErrorMessages());
                    //     return;
                    // }

                    $voucherUsageModel = new \Models\SalesruleCustomer();

                    // If not found we need to save it
                    if (empty($isValidUsage)) {
                        $customerVoucherUsage = [
                            "rule_id" => $voucher->rule_id,
                            "voucher_code" => $voucher->voucher_code,
                            "customer_id" => $this->customer->getCustomerId(),
                            "times_used" => 1
                        ];
                    } else {
                        $customerVoucherUsage = $isValidUsage;
                        $customerVoucherUsage['times_used']++;
                    }

                    $voucherUsageModel->setFromArray($customerVoucherUsage);

                    $this->voucherCollection->append($giftCardModel);
                    $this->salesRulesCustomerCollection->append($voucherUsageModel);
                }
            }
        }

        // Validating cart rules used
        // TODO: data_array dapetnya darimana?
        // if(!empty($data_array['cart_rules']) && is_array($data_array['cart_rules'])) {
        //     $cart_promo = json_decode($data_array['cart_rules']);

        //     if(empty($this->salesRulesCustomerCollection)) {
        //         $this->salesRulesCustomerCollection = new \Models\Salesrule\Customer\Collection();
        //     }

        //     $marketingLib = new \Library\Marketing();
        //     $marketingPromo = $marketingLib->getMarketingPromotion(['shopping_cart', 'shipping', 'payment', 'invoice_created']);
        //     foreach ($cart_promo as $appliedPromo) {
        //         foreach ($marketingPromo as $promo) {
        //             $promoList = json_decode($promo,true);

        //             if(!empty($promoList)) {
        //                 foreach ($promoList as $todayPromo) {

        //                     if($todayPromo['rule_id'] == $appliedPromo->rule_id) {
        //                         // Validasi penggunaan voucher per customer
        //                         $isValidUsage = $marketingLib->maxUsageValidation($todayPromo, $this->customer->getCustomerId());

        //                         if($isValidUsage === false) {
        //                             $this->errorCode[] = $marketingLib->getErrorCode();
        //                             $this->errorMessages = array_merge($this->errorMessages, $marketingLib->getErrorMessages());
        //                             return;
        //                         }
        //                         $voucherUsageModel = new \Models\SalesruleCustomer();

        //                         // If not found we need to save it
        //                         if(empty($isValidUsage)) {
        //                             $customerVoucherUsage = [
        //                                 "rule_id" => $appliedPromo->rule_id,
        //                                 "voucher_code" => "",
        //                                 "customer_id" => $this->customer->getCustomerId(),
        //                                 "times_used" => 1
        //                             ];
        //                         } else {
        //                             $customerVoucherUsage = $isValidUsage;
        //                             $customerVoucherUsage['times_used']++;
        //                         }

        //                         $voucherUsageModel->setFromArray($customerVoucherUsage);
        //                         $this->salesRulesCustomerCollection->append($voucherUsageModel);
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
    }

    public function addMissingDetail()
    {
        /**
         * For guest checkout, we not asking for his name, here we copy firstname and lastname from address information
         */
        if ($this->customer->getCustomerIsGuest() == 1 && empty($this->customer->getCustomerFirstname())) {
            // if shipping address not define then we use customer name from billing address
            if ($this->shipping_address) {
                $this->customer->setCustomerFirstname($this->shipping_address->getFirstName());
                $this->customer->setCustomerLastname($this->shipping_address->getLastName());
            } else if ($this->billing_address) {
                $this->customer->setCustomerFirstname($this->billing_address->getFirstName());
                $this->customer->setCustomerLastname($this->billing_address->getLastName());
            }
        }

        /**
         * For now we not asking for billing address to customer, assume billing address is same with shipping address
         */
        if ((empty($this->billing_address) || empty($this->billing_address->getFullAddress())) && !empty($this->shipping_address)) {
            //if customer type is b2b, and somehow billing address not choosen, we set billing_address here
            $resultCompanyAddress = '';
            if (!empty($this->customer->getCustomerType()) && $this->customer->getCustomerType() == 'B2B') {
                /**
                 * @var $resultCompanyAddress \Models\CompanyAddress
                 */
                $companyAddress = new \Models\CompanyAddress();
                $resultCompanyAddress = $companyAddress->findFirst('address_type = "billing" and company_id = ' . $data_array['customer']['type']['company_id'] . ' and status = 10');
            }

            $billingAddressData = $this->shipping_address->getDataArray();
            $this->billing_address = new \Models\SalesOrderAddress();
            $this->billing_address->setFromArray($billingAddressData);
            $this->billing_address->setAddressType('billing');
            if (!empty($resultCompanyAddress)) {
                $this->billing_address->setCompanyAddressId($resultCompanyAddress->getAddressId());
                $this->billing_address->setCountryId($resultCompanyAddress->getCountryId());
                $this->billing_address->setProvinceId($resultCompanyAddress->getProvinceId());
                $this->billing_address->setCityId($resultCompanyAddress->getCityId());
                $this->billing_address->setKecamatanId($resultCompanyAddress->getKecamatanId());
                $this->billing_address->setFirstName($resultCompanyAddress->getFirstName());
                $this->billing_address->setLastName($resultCompanyAddress->getLastName());
                $this->billing_address->setFullAddress($resultCompanyAddress->getFullAddress());
                $this->billing_address->setPhone($resultCompanyAddress->getPhone());
                $this->billing_address->setPostCode($resultCompanyAddress->getPostCode());
            }
        }
    }

    public function setFromArray($data_array)
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray, $data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = parent::toArray($columns, true);

        if (!empty($this->customer)) {
            $view['customer'] = $this->customer->getDataArray($columns, true);
        }

        if (!empty($this->shipping_address)) {
            $view['shipping_address'] = $this->shipping_address->getDataArray($columns, true);
        }

        if (!empty($this->billing_address)) {
            $view['billing_address'] = $this->billing_address->getDataArray($columns, true);
        }

        if (!empty($this->payment)) {
            $view['payment'] = $this->payment->getDataArray($columns, true);
        }

        if (!empty($this->items)) {
            $view['items'] = $this->items->getDataArray($columns, true);
        }


        unset($view['errorCode']);
        unset($view['errorMessages']);

        return $view;
    }
    
    /**
     * Split sales order address to shipping and billing
     */
    public function generateAddress()
    {
        if ($this->SalesOrderAddress) {
            foreach ($this->SalesOrderAddress as $address) {
                if ($address->getAddressType() == 'shipping') {
                    $this->shipping_address = $address;
                } else if ($address->getAddressType() == 'billing') {
                    $this->billing_address = $address;
                }
            }
        }
    }

    public function beforeCreate()
    {
        parent::beforeCreate();

        if (strtolower($this->source_order) == "lazada") {
            $sql = "SELECT order_no FROM sales_order WHERE order_no = '" . $this->order_no . "'";
            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {
                return false;
            }
        } else {
            $result = $this->findFirst("order_no = '" . $this->order_no . "'");
            if ($result) {
                \Helpers\LogHelper::log("createOrderError", "Order no: " . $this->order_no . " already exists in the database");
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Order number already exists in the database";
                return false;
            }
        }
    }

    public function saveTransaction($log_file_name = "transaction")
    {
       
        /**
         * Make sure we used write connection when creating order
         */
        $this->useWriteConnection();

        // Apply sales rule marketing to prevent other use it while the promo already hit it's limit
        if (!empty($this->voucherCollection)) {
            \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start voucherCollection->saveData models/SalesOrder.php Cart id: '.$this->cart_id, 'debug');
            // $this->voucherCollection->saveData("sales_rule_voucher");
            foreach ($this->voucherCollection as $voucher) {
                $sql = "UPDATE salesrule_voucher SET times_used = times_used + 1 WHERE voucher_id = " . $voucher->voucher_id;
                $this->getDi()->getShared('dbMaster')->execute($sql);
            }
            \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End voucherCollection->saveData models/SalesOrder.php Cart id: '.$this->cart_id, 'debug');
        }

        if (!empty($this->salesRulesCustomerCollection)) {
            \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start salesRulesCustomerCollection->saveData models/SalesOrder.php Cart id: '.$this->cart_id, 'debug');
            $this->salesRulesCustomerCollection->saveData("sales_rule_customer");
            \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End salesRulesCustomerCollection->saveData models/SalesOrder.php Cart id: '.$this->cart_id, 'debug');
        }

        $this->created_at = date("Y-m-d H:i:s");
        $this->updated_at = date("Y-m-d H:i:s");

        
        
        // Create a transaction manager
        $manager = new TxManager();

        // Request a transaction
        $this->useWriteConnection();
        $manager->setDbService($this->getConnection());
        $transaction = $manager->get();
        $this->setTransaction($transaction);
        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start create() models/SalesOrder.php Cart id: '.$this->cart_id, 'debug');
        if ($this->create() === false) {
            $messages = $this->getMessages();

            $errMsg = array();
            foreach ($messages as $message) {
                $errMsg[] = $message->getMessage();
            }

            \Helpers\LogHelper::log("transaction", "Order save failed, error : " . json_encode($errMsg));
            $this->errorCode = "RR301";
            $this->errorMessages[] = "Save Order failed";

            $transaction->rollback(
                "Save Order failed"
            );
            return false;
        }
        \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End create() models/SalesOrder.php Cart id: '.$this->cart_id, 'debug');

        try {
            
            /* If cart_type informa_b2b/ace_b2b and pending order id already exist, 
            then we don't need deduct stock because deduct stock already happened when create pending order */
            if ($this->shoppingCartData['cart_type'] == 'installation' || 
                (($this->shoppingCartData['cart_type'] == 'ace_b2b' || $this->shoppingCartData['cart_type'] == 'informa_b2b') &&
                isset($this->shoppingCartData['b2b']) && 
                isset($this->shoppingCartData['b2b']['pending_order_id']) &&
                $this->shoppingCartData['b2b']['pending_order_id'] != 0)) {
                $this->items->skipDeductStock = true;
            }

            $this->items->order_no = $this->order_no;
            $this->items->storeCodeNewRetail = $this->store_code_new_retail;
            $this->items->reference_order_no = $this->reference_order_no;
            $this->items->reorder_customer_confirmation = $this->reorder_customer_confirmation;
            $this->items->cart_type = $this->cart_type;            
            $this->items->setSalesOrderId($this->sales_order_id);
            $this->items->cart_id = $this->cart_id;
            $this->items->items = $this->items;
            $this->items->itemsShoppingCartData =$this->shoppingCartData['items'];
            
            //var_dump($this->SalesOrderItem);
            // var_dump($this->items->items->items->items);
            // // var_dump($this->items->items->getItems);
            
            // die;
            
            
            
            if ($this->device == "vendor") {
                     
                $this->items->saveData($transaction, substr($this->order_no, 0, 4));
            } else {
                $this->items->saveData($transaction, $this->company_code);
            }
        } catch (\Exception $e) {
        
            $transaction->rollback(
                "saveItem : " . $e->getMessage()
            );
            return false;
        }

        // remove employee from customer wishlist item
        try{

            foreach ($this->items as $valItem) {
                if (!empty($valItem->getEmployees())) {
                    $employees = $valItem->getEmployees();
                    foreach ($employees as $employee) {
                        $this->employeeData[] = array(
                            'sku' => $valItem->getSku(),
                            'nip' => $employee->getNip()
                        );
                    }
                }
            }

            $listEmployee = $this->employeeData;
            if(count($listEmployee)>0){
                $customerEmail = $this->customer->getCustomerEmail();
                $wishlistEmployeeParam = [
                    "email" => $customerEmail,
                    "items" => $listEmployee
                ];
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== Start removeWishlistEmployee models/SalesOrder.php Cart id: '.$this->cart_id, 'debug');
                $this->removeWishlistEmployee($wishlistEmployeeParam);
                \Helpers\LogHelper::trapLog('ProcessCreateOrder', '==== End removeWishlistEmployee models/SalesOrder.php Cart id: '.$this->cart_id, 'debug');
            }
        }catch (\Library\HTTPException $e) {
            LogHelper::log("storemode_wishlist", $this->order_no . " :  Remove employee on wishlist failed, Msg : " . $e->devMessage);

            $transaction->rollback(
                "saveStoremodeEmployee : " . $e->getMessage()
            );
            return false;
        }

        try {
            $this->customer->setSalesOrderId($this->sales_order_id);
            $this->customer->setTransaction($transaction);
            $this->customer->saveData("transaction");
            LogHelper::log("customer_transaction_lock_salesorder", $this->order_no . "|" . $this->customer->getCustomerId() . " : Save Customer success sales order model line 1253");
            $this->customer_id = $this->customer->getCustomerId();
        } catch (\Library\HTTPException $e) {
            LogHelper::log("createOrderError", $this->order_no . " : Save Customer failed, Msg : " . $e->devMessage);

            $transaction->rollback(
                "saveCustomer : " . $e->getMessage()
            );
            return false;
        } catch (\Exception $e) {
            $transaction->rollback(
                "saveCustomer : " . $e->getMessage()
            );
            return false;
        }

        if (!empty($this->faktur_pajak)) {
            try {
                $this->faktur_pajak->setSalesOrderId($this->sales_order_id);
                $this->faktur_pajak->setTransaction($transaction);
                $this->faktur_pajak->saveData("transaction");
            } catch (\Library\HTTPException $e) {
                LogHelper::log("createOrderError", $this->order_no . " : Save Faktur Pajak failed, Msg : " . $e->devMessage);
                $transaction->rollback(
                    "saveFakturPajak : " . $e->getMessage()
                );
                return false;
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveFakturPajak : " . $e->getMessage()
                );
                return false;
            }
        }

        if (!empty($this->shippingAddressList) && count($this->shippingAddressList) > 0) {
            try {
                // if billing address empty choose one from shipping address list
                foreach ($this->shippingAddressList as $valBillingAddress) {
                    $billingAddress = new \Models\SalesOrderAddress();
                    unset($valBillingAddress['carrier_id']);
                    $billingAddress->setFromArray($valBillingAddress);
                    $billingAddress->setCompanyAddressId(0);
                    $billingAddress->setAddressType('billing');

                    if (isset($valBillingAddress['geolocation'])) {
                        $billingAddress->setGeolocation($valBillingAddress['geolocation']);
                    }

                    if (isset($valBillingAddress['company_address_id'])) {
                        $companyAddressId = \Helpers\SimpleEncrypt::decrypt($valBillingAddress['company_address_id']);
                        if (!empty($companyAddressId)) {
                            $billingAddress->setCompanyAddressId($companyAddressId);
                        }
                    }

                    $this->shipping_address = $billingAddress;
                    $this->shipping_address->setSalesOrderId($this->sales_order_id);
                    $this->shipping_address->setTransaction($transaction);
                    break;
                }
            } catch (\Library\HTTPException $e) {
                LogHelper::log("createOrderError", $this->order_no . " : Save Billing Address failed, Msg : "  . $e->devMessage);
                $transaction->rollback(
                    "saveBillingAddress : " . $e->getMessage()
                );
                return false;
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveBillingAddress : " . $e->getMessage()
                );
                return false;
            }
        } else {
            if (!$this->billing_address) {
                $this->billing_address = new \Models\SalesOrderAddress();
                $valBillingAddress = array(
                    'sales_order_address_id' => 0,
                    'company_address_id' => 0,
                    'first_name' => 'Ruparupa',
                    'last_name' => 'ODI',
                    'phone' => '021 - 582 9191',
                    'full_address' => 'Jl. Puri Kencana No. 1',
                    'post_code' => '11610',
                    'address_type' => 'billing',
                    'address_name' => '',
                    'geolocation' => '-6.188441699999998,106.73396839999998',
                    'carrier_id' => NULL,
                    'group_shipment' => NULL,
                    'country' =>
                    array(
                        'country_id' => '102',
                        'country_code' => 'ID',
                        'country_name' => 'Indonesia',
                    ),
                    'province' =>
                    array(
                        'province_id' => '490',
                        'province_code' => 'ID-JK',
                        'province_name' => 'DKI JAKARTA',
                    ),
                    'city' =>
                    array(
                        'city_id' => '573',
                        'city_name' => 'KOTA. JAKARTA BARAT',
                    ),
                    'kecamatan' =>
                    array(
                        'kecamatan_id' => '7004',
                        'kecamatan_name' => 'KEMBANGAN',
                        'coding' => 'CGK10104',
                        'kecamatan_code' => '7004',
                        'sap_exp_code' => 'JK00',
                    ),
                    'customer_address' => '',
                );

                $this->billing_address->setFromArray($valBillingAddress);
                $this->billing_address->setAddressType('billing');
                $this->shipping_address = $this->billing_address;
                $this->shipping_address->setSalesOrderId($this->sales_order_id);
                $this->shipping_address->setTransaction($transaction);
            } else {
                $this->shipping_address = $this->billing_address;
                $this->shipping_address->setSalesOrderId($this->sales_order_id);
                $this->shipping_address->setTransaction($transaction);
            }
        }

        // override if we found billing address
        if (!empty($this->billing_address)) {
            try {
                if (!empty($this->billing_address->getFirstName())) {
                    $this->billing_address->setSalesOrderId($this->sales_order_id);
                    $this->billing_address->setAddressType('billing');
                    $this->billing_address->setTransaction($transaction);
                }
            } catch (\Library\HTTPException $e) {
                LogHelper::log("createOrderError", $this->order_no . " : Save Billing Address failed, Msg : "  . $e->devMessage);
                $transaction->rollback(
                    "saveBillingAddress : " . $e->getMessage()
                );
                return false;
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveBillingAddress : " . $e->getMessage()
                );
                return false;
            }
        }

        $this->shipping_address->saveData("transaction");

        if (!empty($this->shippingAddressList)) {
            try {
                if (count($this->shippingAddressList) > 0) {
                    foreach ($this->shippingAddressList as $keyShippingAddress => $valShippingAddress) {
                        $shippingAddress = new \Models\SalesOrderAddress();
                        $shippingAddress->setFromArray($valShippingAddress);
                        $shippingAddress->setCompanyAddressId(0);
                        $shippingAddress->setAddressType('shipping');
                        $shippingAddress->setGroupShipment($keyShippingAddress);

                        if (isset($valShippingAddress['geolocation'])) {
                            $shippingAddress->setGeolocation($valShippingAddress['geolocation']);
                        }

                        if (isset($valShippingAddress['carrier_id']) && !empty($valShippingAddress['carrier_id'])) {
                            $shippingAddress->setCarrierId($valShippingAddress['carrier_id']);
                        }

                        // TODO: company_address_id not found in new cart
                        if (isset($valShippingAddress['company_address_id'])) {
                            $companyAddressId = \Helpers\SimpleEncrypt::decrypt($valShippingAddress['company_address_id']);
                            if (!empty($companyAddressId)) {
                                $shippingAddress->setCompanyAddressId($companyAddressId);
                            }
                        }

                        $this->shipping_address = $shippingAddress;
                        $this->shipping_address->setSalesOrderId($this->sales_order_id);
                        $this->shipping_address->setTransaction($transaction);
                        $this->shipping_address->saveData("transaction");
                    }
                }
            } catch (\Library\HTTPException $e) {
                LogHelper::log("createOrderError", $this->order_no . " : Save Shipping Address failed, Msg : " . $e->devMessage);
                $transaction->rollback(
                    "saveShippingAddress : " . $e->getMessage()
                );
                return false;
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveShippingAddress : " . $e->getMessage()
                );
                return false;
            }
        }

        // Save Sales Customer Group
        if (!empty($this->customerMemberList)) {
            try {
                if (count($this->customerMemberList) > 0) {
                    foreach ($this->customerMemberList as $keyCustomerMember => $valCustomerMember) {
                        $customerMemberModel = new \Models\SalesCustomerGroup();
                        $customerMemberModel->setFromArray($valCustomerMember);

                        // Validate member that will be stored on sales_customer_group
                        if (
                            (
                                $customerMemberModel->getGroupId() == 1 && 
                                $customerMemberModel->getMemberNumber() != NULL && 
                                substr($customerMemberModel->getMemberNumber(), 0, 3) == 'RRR'
                            ) ||
                            ($customerMemberModel->getGroupId() == 4 && $customerMemberModel->getIsVerified() == 5)||
                            $customerMemberModel->getIsVerified() == 10 
                        ) {
                            $this->sales_customer_group = $customerMemberModel;
                            $this->sales_customer_group->setSalesOrderId($this->sales_order_id);
                            $this->sales_customer_group->setTransaction($transaction);
                            $this->sales_customer_group->saveData("transaction");
                        }

                        // Delete Customer 2 Group, if email like 'storemode%'
                        $customerEmail = trim(strtolower($this->customer->getCustomerEmail()));
                        if(substr($customerEmail, 0, 9) == "storemode"){
                            $groupID = $customerMemberModel->getGroupId();
                            $customerID = $this->customer->getCustomerId();
                            if(!empty($groupID)){
                                if(!empty($customerID)){
                                    $sql = "DELETE FROM customer_2_group where group_id = ". $groupID." and customer_id = ". $customerID;
                                    $this->useWriteConnection();
                                    $result = $this->getDi()->getShared($this->getConnection())->execute($sql);
                                }
                            }
                            
                        }
                    }
                }
            } catch (\Library\HTTPException $e) {
                LogHelper::log("createOrderError", $this->order_no . " : Save Sales Customer Group failed, Msg : " . $e->devMessage);
                $transaction->rollback(
                    "saveSalesCustomerGroup : " . $e->getMessage()
                );
                return false;
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveSalesCustomerGroup : " . $e->getMessage()
                );
                return false;
            }
        }

        $this->update();

        // Update is_new customer
        try {
            $customerModel = new \Models\Customer();
            if (!empty($this->customer_id)) {
                $dataCustomer = $this->customer->getDataArray();
                $statusCustomer = 5;

                if (isset($dataCustomer['customer_is_guest'])) {
                    if ($dataCustomer['customer_is_guest'] == '0') {
                        $statusCustomer = 10;
                    }
                }

                if ($this->device != "vendor") {
                    $isNew = 0;
                    foreach ($this->items as $valItem) {
                        if (!empty($valItem->getSku())) {
                            $sku = $valItem->getSku();
                            $skuMember = [getenv('MEMBERSHIP_HCI_SKU'),getenv('MEMBERSHIP_AHI_SKU'),getenv('MEMBERSHIP_TGI_SKU'),getenv('MEMBERSHIP_SLM_SKU')];
                            if(in_array($sku,$skuMember)){
                                $isNew = 10;
                            }
                        }
                    }        
                    $params = array(
                        'is_new' => $isNew,
                        'customer_id' => $this->customer_id,
                        'status' => $statusCustomer
                    );
                    $customerModel->setFromArray($params);
                    $customerModel->saveData();
                    LogHelper::log("customer_transaction_lock_salesorder", $this->order_no . "|" . $this->customer_id . " : Save Customer success sales order model line 1474");
                }
            }
        } catch (\Library\HTTPException $e) {
            LogHelper::log("createOrderError", $this->order_no . " : Save Customer failed, Msg : " . $e->devMessage);
            $transaction->rollback(
                "saveCustomer : " . $e->getMessage()
            );
            return false;
        } catch (\Exception $e) {
            $transaction->rollback(
                "saveCustomer : " . $e->getMessage()
            );
            return false;
        }

        try {
            $this->payment->setSalesOrderId($this->sales_order_id);
            $this->payment->setTotalAmountOrdered($this->grand_total);
            $this->payment->saveData("savePayment");
        } catch (\Library\HTTPException $e) {
            LogHelper::log("createOrderError", $this->order_no . " : Save Payment failed, Msg : " . $e->devMessage);
            $transaction->rollback(
                "savePayment : " . $e->getMessage()
            );
            return false;
        } catch (\Exception $e) {
            $transaction->rollback(
                "savePayment : " . $e->getMessage()
            );
            return false;
        }

        if (isset($this->cart_raw['cart_type']) && $this->cart_raw['cart_type'] === 'gift'){
            $error = $this->saveRegistryData($this->cart_raw, $this->getOrderNo());
            if ($error !== ""){
                LogHelper::log("createOrderError", sprintf("OrderNo: %s, Error: Save Registry Data, %s", $this->getOrderNo(), $error));
                $transaction->rollback(
                    "saveRegistryData : " . $error
                );
                return false;
            }

            $giftAPI = new \Library\GiftRegistryAPI();
            $getRegistryPayload = [
                "registry_id" => $this->cart_raw['gift_registry']['registry_id'],
                "customer_id" => $this->cart_raw['customer']['customer_id'],
            ];
            $ok = $giftAPI->getRegistry($getRegistryPayload);
            if (!$ok) {
                LogHelper::log("createOrderError", sprintf("OrderNo: %s, Error: Get Registry Data, %s", $this->getOrderNo(), $giftAPI->getErrorMessage()));
                $transaction->rollback(
                    "getRegistry : " . $giftAPI->getErrorMessage()
                );
                return false;
            }

            $notifError = $this->sendAlertGiftRegistryOrderCreated($this->cart_raw, $giftAPI->getData());
            if ($notifError !== ""){
                LogHelper::log("createOrderError", sprintf("OrderNo: %s, Error: sendAlertGiftRegistryOrderCreated, %s", $this->getOrderNo(), $notifError));
                $transaction->rollback(
                    "sendAlertGiftRegistryOrderCreated : " . $notifError
                );
                return false;
            }
        }


        $transaction->commit();
        return true;
    }
    private function sendAlertGiftRegistryOrderCreated($cartData, $giftData)
    {
        $product = [];
        foreach ($cartData['items'] as $item) {
            $product[] = (object)['product_name' => $item['name']];
        }
        $buyerName = sprintf("%s %s", $cartData['customer']['customer_firstname'], $cartData['customer']['customer_lastname']);
        if ($giftData["link_detail"]['is_custom'] && !empty($giftData["link_detail"]['url_key'])) {
            $customUrl = $giftData["link_detail"]['url_key'];
        } else {
            $customUrl = $giftData["link_detail"]['default_url_key'];
        }

        $cardInfo = [
            'from' => $cartData['gift_registry']['card_info']['from'],
            'message' => $cartData['gift_registry']['card_info']['message']
        ];

        $templateModel = [
            "owner_name" => $giftData["customer"]["first_name"],
            "gift_registry_url" => sprintf("%s/%s", getenv('GIFT_BASE_URL'), $giftData["link_detail"]["default_url_key"]),
            "gift_registry_url_view" => $customUrl,
            "buyer_name" => $buyerName,
            "card_info" => $cardInfo,
            "product_list" => $product
        ];

        $emailHelper = new \Library\Email();
        $templateID = \Helpers\GeneralHelper::getTemplateId("gift_registry_order_created", "ODI");
        $emailHelper->setTemplateId($templateID);
        $emailHelper->setCompanyCode("ODI");
        $emailHelper->setEmailSender();
        $emailHelper->setEmailReceiver($giftData["customer"]["email"]);
        $emailHelper->setTemplateModel($templateModel);
        $emailHelper->setEmailTag("gift_registry_order_created");
        if (!$emailHelper->send()) {
            return "Send email Failed";
        }

        $notificationHelper = new \Library\Notification();
        $iconODI = getenv('PUSH_NOTIF_ICON_ODI');
        $companyCode = "ODI";
        $notificationHelper->setTitle("Ada yang Beli Gift List Kamu!");
        $notificationHelper->setBody("Cek info lengkapnya di sini ya!");
        $notificationHelper->setKey("token");
        $notificationHelper->setIcon($iconODI);
        $notificationHelper->setPageType("gift-registry-detail");
        $notificationHelper->setUrl(sprintf("%sgift-registry/%s",getenv('RUPARUPA_URL'), $giftData["link_detail"]["default_url_key"]));
        $urlKeyNotif = $cartData["gift_registry"]["registry_id"];
        $notificationHelper->setUrlKey("$urlKeyNotif");
        $customerDeviceToken = new \Models\CustomerDeviceToken();
        $customerDeviceToken = $customerDeviceToken->getCDTByCustIdForPushNotif($giftData["customer"]['customer_id']);
        $pushNotifList = explode(',', getenv('PUSH_NOTIF_BU_CODE'));
        if (count($customerDeviceToken) > 0 && in_array($companyCode, $pushNotifList)) {
            $filteredToken = $notificationHelper->FilterTokenByHighestPriority($customerDeviceToken);

            foreach($filteredToken as $filterToken){
                $notificationHelper->setValue($filterToken["device_token"]);
                $notificationHelper->PushNotif($companyCode);
            }
        }

        return "";
    }

    private function saveRegistryData($cart, $orderNo)
    {
        $giftAPI = new \Library\GiftRegistryAPI();
        foreach ($cart['items'] as $item) {
            if ($item['parent_id'] > 0) {
                continue;
            }

            if ($cart['reference_order_no'] != "") {
                $payloadDeleteOrder = [
                    "registry_id" => $cart['gift_registry']['registry_id'],
                    "sku" => $item['sku'],
                    "order_no" => $cart['reference_order_no'],
                ];
                $okDeleteOrder = $giftAPI->deleteRegistryItemOrder($payloadDeleteOrder);
                if (!$okDeleteOrder) {
                    return $giftAPI->getErrorMessage();
                }
            }

            $payload = [
                "registry_id" => $cart['gift_registry']['registry_id'],
                "sku" => $item['sku'],
                "quantity_on_hold_payment" => $item['qty_ordered'],
                "order_no" => $orderNo,
                "cart_id" => $cart['cart_id']
            ];
            $ok = $giftAPI->updateRegistryItemOrder($payload);
            if (!$ok) {
                return $giftAPI->getErrorMessage();
            }
        }
        return "";
    }

    public function process1021()
    {
        try {
            if ($this->payment->getStatus() == 'paid') {
                $nsq = new \Library\Nsq();
                //making 1021 listener
                //making 1021 listener
                $listItems = $this->items->reservedStock;
                $listItemsSearch = $this->items->getDataArray();
                $listSku = array();
                $dataCombinedSku = array();
                //search for bogo item
                foreach ($listItemsSearch as $item) {
                    if (strpos($item['store_code'], '1000DC') !== false) {
                        if ($item['is_free_item'] == 1) {
                            array_push($listSku, $item['sku']);
                        }
                    }
                }

                //loop each item to search item that has same sku with bogo and combine their qty
                foreach ($listSku as $sku) {

                    $qtyCombined = 0;
                    foreach ($listItemsSearch as $index => $item) {
                        if (strpos($item['store_code'], '1000DC') !== false) {
                            if ($item['sku'] == $sku) {
                                $qtyCombined += $item['qty_ordered'];
                                unset($listItemsSearch[$index]);
                            }
                        }
                    }
                    $dataCombined = array(
                        "sku" => $sku,
                        "qty" => $qtyCombined,
                        "store_code" => $item['store_code']
                    );
                    array_push($dataCombinedSku, $dataCombined);
                }

                //loop item that is not bogo and 1000DC to create TP
                foreach ($listItemsSearch as $item) {
                    if (strpos($item['store_code'], '1000DC') !== false) {
                        //getting store code detail
                        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                        $endPoint = 'legacy/stock/'.$item['sku'] . $item['store_code'];
                        $apiWrapper->setEndPoint($endPoint);
                        LogHelper::log('ProcessCreateOrder', '==== Start status paid get not bogo and 1000DC models/salesOrder.php '.getenv('STOCK_V2_API').'  Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $item['store_code'] , 'debug');
                        if ($apiWrapper->send("get")) {
                            $apiWrapper->formatResponse();
                        };
                        LogHelper::log('ProcessCreateOrder', '==== End status paid get not bogo and 1000DC models/salesOrder.php'.getenv('STOCK_V2_API').'  Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $item['store_code'] , 'debug');
                        if (!empty($apiWrapper->getError())) {
                            \Helpers\LogHelper::log("createListenerTPError", "Error Getting 1000 Store Code Detail from SKU : " . $item['sku'] . ", Error : " . json_encode($apiWrapper->getError()), "error");
                        }
                        $stock1000Detail = $apiWrapper->getData();

                        //create payload TP for queue  
                        $storeCodeTp = ltrim($item['store_code'], '1000DC');
                        $storeCodeTp = str_replace('DC', '', $storeCodeTp);

                        $data = array(
                            "sku" => $item['sku'],
                            "store_code" => $storeCodeTp,
                            "order_no" => $this->order_no,
                            "qty" => $item['qty_ordered'],
                            "message" => "createOrderTp",
                            "type" => "stock",
                            "created_date" => date("Y-m-d H:i:s")
                        );
                        //insert into queue
                        $nsq->publishCluster('transaction1021', $data);
                        //create record TP in MySQL
                        $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
                        $valueString = "('" . $this->order_no . "','" . $item['sku'] . "','" . $storeCodeTp . "','" . $item['qty_ordered'] . "','process','Create TP','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                        $sql = $sql . $valueString;
                        LogHelper::log('ProcessCreateOrder', '==== Start status paid not bogo and 1000DC insert sales_order_tp models/salesOrder.php Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $stock1000Detail['store_code_detail'] , 'debug');          
                        $this->getDi()->getShared('dbMaster')->execute($sql);
                        LogHelper::log('ProcessCreateOrder', '==== End status paid not bogo and 1000DC insert sales_order_tp models/salesOrder.php Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $stock1000Detail['store_code_detail'] , 'debug'); 
                        LogHelper::log('ProcessCreateOrder', '==== End status paid not bogo and 1000DC insert sales_order_tp models/salesOrder.php Check ltrim Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. ltrim($item['store_code'], '1000DC') , 'debug');            
                    }
                }

                //loop each sku with bogo and 1000DC to create TP
                foreach ($dataCombinedSku as $item) {
                    $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                    $endPoint = 'legacy/stock/'.$item['sku'] . $item['store_code'];
                    $apiWrapper->setEndPoint($endPoint);
                    LogHelper::log('ProcessCreateOrder', '==== Start status paid get bogo and 1000DC models/salesOrder.php '.getenv('STOCK_V2_API').'  Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $item['store_code'] , 'debug');
                    if ($apiWrapper->send("get")) {
                        $apiWrapper->formatResponse();
                    };
                    LogHelper::log('ProcessCreateOrder', '==== End status paid get bogo and 1000DC models/salesOrder.php'.getenv('STOCK_V2_API').'  Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $item['store_code'] , 'debug');
                    if (!empty($apiWrapper->getError())) {
                        \Helpers\LogHelper::log("createListenerTPError", "Error Getting 1000 Store Code Detail from SKU : " . $item['sku'] . ", Error : " . json_encode($apiWrapper->getError()), "error");
                    }
                    $stock1000Detail = $apiWrapper->getData();

                    //create payload TP for queue 
                    $storeCodeTp = ltrim($item['store_code'], '1000DC');
                    $storeCodeTp = str_replace('DC', '', $storeCodeTp);

                    $data = array(
                        "sku" => $item['sku'],
                        "store_code" => $storeCodeTp,
                        "order_no" => $this->order_no,
                        "qty" => $item['qty'],
                        "message" => "createOrderTp",
                        "type" => "stock",
                        "created_date" => date("Y-m-d H:i:s")
                    );
                    //insert into queue
                    $nsq->publishCluster('transaction1021', $data);
                    //create record TP in MySQL
                    $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
                    $valueString = "('" . $this->order_no . "','" . $item['sku'] . "','" . $storeCodeTp . "','" . $item['qty_ordered'] . "','process','Create TP','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                    $sql = $sql . $valueString;
                    LogHelper::log('ProcessCreateOrder', '==== Start status paid bogo and 1000DC insert sales_order_tp bogo and 1000DC models/salesOrder.php Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $stock1000Detail['store_code_detail'] , 'debug');          
                    $this->getDi()->getShared('dbMaster')->execute($sql);
                    LogHelper::log('ProcessCreateOrder', '==== End status paid bogo and 1000DC insert sales_order_tp bogo and 1000DC models/salesOrder.php Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $stock1000Detail['store_code_detail'] , 'debug');
                    LogHelper::log('ProcessCreateOrder', '==== End status paid not bogo and 1000DC insert sales_order_tp models/salesOrder.php Check ltrim Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. ltrim($item['store_code'], '1000DC') , 'debug');        
                }
            } else if ($this->payment->getStatus() == 'verification') {
                $nsq = new \Library\Nsq();
                $data = [
                    "data" => ["order_no" => $this->order_no],
                    "message" => "captureTransaction"
                ];
                $nsq->publishCluster('transaction', $data);

                //making 1021 listener
                $listItems = $this->items->reservedStock;
                $listItemsSearch = $this->items->getDataArray();
                $listSku = array();
                $dataCombinedSku = array();
                //search for bogo item
                foreach ($listItemsSearch as $item) {
                    if (strpos($item['store_code'], '1000DC') !== false) {
                        if ($item['is_free_item'] == 1) {
                            array_push($listSku, $item['sku']);
                        }
                    }
                }

                //loop each item to search item that has same sku with bogo and combine their qty
                foreach ($listSku as $sku) {

                    $qtyCombined = 0;
                    foreach ($listItemsSearch as $index => $item) {

                        if (strpos($item['store_code'], '1000DC') !== false) {


                            if ($item['sku'] == $sku) {

                                $qtyCombined += $item['qty_ordered'];
                                unset($listItemsSearch[$index]);
                            }
                        }
                    }
                    $dataCombined = array(
                        "sku" => $sku,
                        "qty" => $qtyCombined,
                        "store_code" => $item['store_code']
                    );
                    array_push($dataCombinedSku, $dataCombined);
                }
                //loop item that is not bogo and 1000DC to create TP
                foreach ($listItemsSearch as $item) {
                    if (strpos($item['store_code'], '1000DC') !== false) {
                        //getting store code detail
                        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                        $endPoint = 'legacy/stock/'.$item['sku'] . $item['store_code'];
                        $apiWrapper->setEndPoint($endPoint);

                        if ($apiWrapper->send("get")) {
                            $apiWrapper->formatResponse();
                        };
                        if (!empty($apiWrapper->getError())) {
                            \Helpers\LogHelper::log("createListenerTPError", "Error Getting 1000 Store Code Detail from SKU : " . $item['sku'] . ", Error : " . json_encode($apiWrapper->getError()), "error");
                        }
                        $stock1000Detail = $apiWrapper->getData();

                        //create payload TP for queue  
                        $storeCodeTp = ltrim($item['store_code'], '1000DC');
                        $storeCodeTp = str_replace('DC', '', $storeCodeTp);

                        $data = array(
                            "sku" => $item['sku'],
                            "store_code" => $storeCodeTp,
                            "order_no" => $this->order_no,
                            "qty" => $item['qty_ordered'],
                            "message" => "createOrderTp",
                            "type" => "stock",
                            "created_date" => date("Y-m-d H:i:s")
                        );
                        //insert into queue
                        $nsq->publishCluster('transaction1021', $data);
                        //create record TP in MySQL
                        $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
                        $valueString = "('" . $this->order_no . "','" . $item['sku'] . "','" . $storeCodeTp . "','" . $item['qty_ordered'] . "','process','Create TP','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                        $sql = $sql . $valueString;
                        $this->getDi()->getShared('dbMaster')->execute($sql);
                    }
                }

                //loop each sku with bogo and 1000DC to create TP
                foreach ($dataCombinedSku as $item) {
                    $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                    $endPoint = 'legacy/stock/'.$item['sku'] . $item['store_code'];
                    $apiWrapper->setEndPoint($endPoint);

                    if ($apiWrapper->send("get")) {
                        $apiWrapper->formatResponse();
                    };
                    if (!empty($apiWrapper->getError())) {
                        \Helpers\LogHelper::log("createListenerTPError", "Error Getting 1000 Store Code Detail from SKU : " . $item['sku'] . ", Error : " . json_encode($apiWrapper->getError()), "error");
                    }
                    $stock1000Detail = $apiWrapper->getData();
                    
                    //create payload TP for queue 
                    $storeCodeTp = ltrim($item['store_code'], '1000DC');
                    $storeCodeTp = str_replace('DC', '', $storeCodeTp);

                    $data = array(
                        "sku" => $item['sku'],
                        "store_code" => $storeCodeTp,
                        "order_no" => $this->order_no,
                        "qty" => $item['qty'],
                        "message" => "createOrderTp",
                        "type" => "stock",
                        "created_date" => date("Y-m-d H:i:s")
                    );
                    //insert into queue
                    $nsq->publishCluster('transaction1021', $data);
                    //create record TP in MySQL
                    $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
                    $valueString = "('" . $this->order_no . "','" . $item['sku'] . "','" . $storeCodeTp . "','" . $item['qty_ordered'] . "','process','Create TP','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                    $sql = $sql . $valueString;
                    $this->getDi()->getShared('dbMaster')->execute($sql);
                }
            } else if ($this->payment->getStatus() == 'pending') {
                $nsq = new \Library\Nsq();

                //making 1021 listener
                $listItems = $this->items->reservedStock;
                $listItemsSearch = $this->items->getDataArray();
                $listSku = array();
                $dataCombinedSku = array();
                //search for bogo item
                foreach ($listItemsSearch as $item) {
                    if (strpos($item['store_code'], '1000DC') !== false) {
                        if ($item['is_free_item'] == 1) {
                            array_push($listSku, $item['sku']);
                        }
                    }
                }

                //loop each item to search item that has same sku with bogo and combine their qty
                foreach ($listSku as $sku) {
                    $qtyCombined = 0;
                    $storeCode = "";
                    foreach ($listItemsSearch as $index => $item) {

                        if (strpos($item['store_code'], '1000DC') !== false) {


                            if ($item['sku'] == $sku) {
                                $storeCode = $item['store_code'];
                                $qtyCombined += $item['qty_ordered'];
                                unset($listItemsSearch[$index]);
                            }
                        }
                    }
                    $dataCombined = array(
                        "sku" => $sku,
                        "qty" => $qtyCombined,
                        "store_code" => $storeCode
                    );
                    array_push($dataCombinedSku, $dataCombined);
                }

                //loop item that is not bogo and 1000DC to create TP
                foreach ($listItemsSearch as $item) {
                    if (strpos($item['store_code'], '1000DC') !== false) {
                        //getting store code detail
                        $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                        $endPoint = 'legacy/stock/'.$item['sku'] . $item['store_code'];
                        $apiWrapper->setEndPoint($endPoint);
                        LogHelper::log('ProcessCreateOrder', '==== Start status pending get not bogo and 1000DC models/salesOrder.php '.getenv('STOCK_V2_API').'  Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $item['store_code'] , 'debug');          
                        if ($apiWrapper->send("get")) {
                            $apiWrapper->formatResponse();
                        };
                        LogHelper::log('ProcessCreateOrder', '==== End status pending get not bogo and 1000DC models/salesOrder.php '.getenv('STOCK_V2_API').'  Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $item['store_code'] , 'debug');
                        if (!empty($apiWrapper->getError())) {
                            \Helpers\LogHelper::log("createListenerTPError", "Error Getting 1000 Store Code Detail from SKU : " . $item['sku'] . ", Error : " . json_encode($apiWrapper->getError()), "error");
                        }                        
                        $stock1000Detail = $apiWrapper->getData();

                        //create payload TP for queue  
                        $storeCodeTp = ltrim($item['store_code'], '1000DC');
                        $storeCodeTp = str_replace('DC', '', $storeCodeTp);

                        $data = array(
                            "sku" => $item['sku'],
                            "store_code" => $storeCodeTp,
                            "order_no" => $this->order_no,
                            "qty" => $item['qty_ordered'],
                            "message" => "createOrderTp",
                            "type" => "stock",
                            "created_date" => date("Y-m-d H:i:s")
                        );
                        //insert into queue
                        $nsq->publishCluster('transaction1021', $data);
                        //create record TP in MySQL
                        $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
                        $valueString = "('" . $this->order_no . "','" . $item['sku'] . "','" . $storeCodeTp . "','" . $item['qty_ordered'] . "','process','Create TP','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                        $sql = $sql . $valueString;
                        LogHelper::log('ProcessCreateOrder', '==== Start status pending not bogo and 1000DC insert sales_order_tp models/salesOrder.php Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $stock1000Detail['store_code_detail'] , 'debug');          
                        $this->getDi()->getShared('dbMaster')->execute($sql);
                        LogHelper::log('ProcessCreateOrder', '==== End status pending not bogo and 1000DC insert sales_order_tp models/salesOrder.php Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $stock1000Detail['store_code_detail'] , 'debug');     
                        LogHelper::log('ProcessCreateOrder', '==== End status paid not bogo and 1000DC insert sales_order_tp models/salesOrder.php Check ltrim Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. ltrim($item['store_code'], '1000DC') , 'debug');     
                    }
                }

                //loop each sku with bogo and 1000DC to create TP
                foreach ($dataCombinedSku as $item) {
                    $apiWrapper = new APIWrapper(getenv('STOCK_V2_API'));
                    $endPoint = 'legacy/stock/'.$item['sku'] . $item['store_code'];
                    $apiWrapper->setEndPoint($endPoint);
                    LogHelper::log('ProcessCreateOrder', '==== Start status pending get bogo and 1000DC models/salesOrder.php '.getenv('STOCK_V2_API').'  Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $item['store_code'] , 'debug');
                    if ($apiWrapper->send("get")) {
                        $apiWrapper->formatResponse();
                    };
                    LogHelper::log('ProcessCreateOrder', '==== End status pending get bogo and 1000DC models/salesOrder.php '.getenv('STOCK_V2_API').'  Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $item['store_code'] , 'debug');
                    if (!empty($apiWrapper->getError())) {
                        \Helpers\LogHelper::log("createListenerTPError", "Error Getting 1000 Store Code Detail from SKU : " . $item['sku'] . ", Error : " . json_encode($apiWrapper->getError()), "error");
                    }
                    $stock1000Detail = $apiWrapper->getData();

                    //create payload TP for queue                     
                    $storeCodeTp = ltrim($item['store_code'], '1000DC');
                    $storeCodeTp = str_replace('DC', '', $storeCodeTp);

                    $data = array(
                        "sku" => $item['sku'],
                        "store_code" => $storeCodeTp,
                        "order_no" => $this->order_no,
                        "qty" => $item['qty'],
                        "message" => "createOrderTp",
                        "type" => "stock",
                        "created_date" => date("Y-m-d H:i:s")
                    );
                    //insert into queue
                    $nsq->publishCluster('transaction1021', $data);
                    //create record TP in MySQL
                    $sql = "insert into sales_order_tp (odi, sku, store_code, qty, status, source, created_at, updated_at) values ";
                    $valueString = "('" . $this->order_no . "','" . $item['sku'] . "','" . $storeCodeTp . "','" . $item['qty_ordered'] . "','process','Create TP','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "')";
                    $sql = $sql . $valueString;
                    LogHelper::log('ProcessCreateOrder', '==== Start status pending bogo and 1000DC insert sales_order_tp bogo and 1000DC models/salesOrder.php Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $stock1000Detail['store_code_detail'] , 'debug');          
                    $this->getDi()->getShared('dbMaster')->execute($sql);
                    LogHelper::log('ProcessCreateOrder', '==== End status pending bogo and 1000DC insert sales_order_tp bogo and 1000DC models/salesOrder.php Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. $stock1000Detail['store_code_detail'] , 'debug');
                    LogHelper::log('ProcessCreateOrder', '==== End status paid not bogo and 1000DC insert sales_order_tp models/salesOrder.php Check ltrim Cart id: '.$this->cart_id.'item :'.$item['sku'] .'store code :'. ltrim($item['store_code'], '1000DC') , 'debug');       
                }
            }
        } catch (\Exception $e) {
            LogHelper::log("createOrderError", $this->order_no . " : Create Listener 1021 failed, Msg : " . $e->getMessage());
            return false;
        }

        return true;
    }

    public function saveRuleOrder()
    {
        try {
            // save gift card and cart rule into sales
            $giftcards = json_decode($this->gift_cards, true);
            // check is customer in group abuser
            $abuserLib = new \Models\CustomerAbuser();
            $abuser = $abuserLib->getGroupAbuser($this->customer_id);
            foreach ($giftcards as $giftcard) {
                $param = array();
                $param['salesrule_order_id'] = '';
                $param['sales_order_id'] = $this->sales_order_id;
                $param['rule_id'] = $giftcard['rule_id'];
                $param['voucher_code'] = $giftcard['voucher_code'];
                $param['voucher_amount_used'] = $giftcard['voucher_amount_used'];
                $param['coin_amount'] = $giftcard['coin_amount'];
                if (!empty($abuser)) {
                    $param['group_abuser'] = $abuser[0]['group'];
                }
                $salesruleOrderModel = new \Models\SalesruleOrder();
                $salesruleOrderModel->setFromArray($param);
                $salesruleOrderModel->save('salesrule_order');
            }

            $cartrules = json_decode($this->cart_rules, true);
            foreach ($cartrules as $cartrule) {
                $param = array();
                $param['salesrule_order_id'] = '';
                $param['sales_order_id'] = $this->sales_order_id;
                $param['rule_id'] = $cartrule['rule_id'];
                $param['voucher_code'] = '';
                $param['voucher_amount_used'] = '';

                $salesruleOrderModel = new \Models\SalesruleOrder();
                $salesruleOrderModel->setFromArray($param);
                $salesruleOrderModel->save('salesrule_order');
            }
        } catch (\Exception $e) {
            LogHelper::log("createOrderError", $this->order_no . " : Create salesrule order failed, Msg : " . $e->getMessage());
            return false;
        }

        return true;
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }

    /**
     * ToDo : Marketplace Settlement
     */
    public function queryMpSettlement($params = array())
    {
        try {

            $sql = "   SELECT
                            a.order_no,
                            date_format(date_add(a.delivered_date,interval 7 hour),'%Y-%m-%d') as delivered_date,
                            a.product_name,
                            a.sku,
                            a.sale_price,
                            a.qty,
                            a.row_total,
                            a.shipping_amount,
                            (a.shipping_amount - a.shipping_discount_amount) as paid_shipping_amount,
                            a.credit_memo_item,
                            a.created_at,
                            a.store_code
                        FROM (
                            SELECT
                                    (
                                        select order_no from sales_order
                                        where sales_order_id in (
                                            select 
                                                sales_order_id
                                            from sales_invoice
                                            where invoice_id = sii.invoice_id
                                        )
                                    ) as order_no,
                                    (
                                        select distinct(delivered_date) from sales_shipment
                                        where invoice_id = sii.invoice_id
                                    ) as delivered_date,
                                    sii.name as product_name,
                                    sii.sku,
                                    sii.selling_price as sale_price,
                                    sii.qty_ordered as qty,
                                    (sii.selling_price * sii.qty_ordered) as row_total,
                                    IF((SELECT shipping_amount FROM sales_invoice WHERE invoice_id = sii.invoice_id) is null, 0, (SELECT shipping_amount FROM sales_invoice WHERE invoice_id = sii.invoice_id)) as shipping_amount,
			                        IF((SELECT shipping_discount_amount FROM sales_invoice WHERE invoice_id = sii.invoice_id) is null, 0, (SELECT shipping_discount_amount FROM sales_invoice WHERE invoice_id = sii.invoice_id)) as shipping_discount_amount,
			                        IF((SELECT shipping_amount-shipping_discount_amount FROM sales_invoice WHERE invoice_id = sii.invoice_id) is null, 0, (SELECT shipping_amount-shipping_discount_amount FROM sales_invoice WHERE invoice_id = sii.invoice_id)) as paid_shipping_amount,
                                    (
										SELECT GROUP_CONCAT('{ \"qty_refunded\": \"',qty_refunded,'\", \"price\": \"',price,'\"}') FROM sales_credit_memo_item WHERE qty_refunded>0 AND sales_credit_memo_item.sales_order_item_id = sii.sales_order_item_id
                                    ) as credit_memo_item,
                                    (
                                        SELECT created_at
                                        from sales_order
                                        where sales_order_id in (
                                            select 
                                                sales_order_id
                                            from sales_invoice
                                            where invoice_id = sii.invoice_id
                                        )
                                    ) as created_at,
                                    store_code
                            FROM sales_invoice_item sii
                            where invoice_id in (
                                select 
                                    invoice_id 
                                from sales_shipment 
                                where 	shipment_status = 'received' and
                                        delivered_date between '" . $params['start_date'] . " 00:00:00' and '" . $params['end_date'] . " 23:59:59'
                            ) and supplier_alias = '" . $params['supplier_alias'] . "'
                        ) a
                  ";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            if ($result->numRows() > 0) {

                $productModel = new \Models\Product();

                $result->setFetchMode(Database::FETCH_ASSOC);
                $dbResult = $result->fetchAll();
                $paramsSource = array("shared_commision", "categories");
                $spCommissionModel = new \Models\ProductSpecialCommission();

                foreach ($dbResult as $orderIndex => $dataOrder) {
                    $getProductDetail = $productModel->getDetailFromElastic($dataOrder['sku'], $paramsSource);
                    $productDetail = $getProductDetail[0]['_source'];
                    $productDetail['shared_commision'] = !empty($productDetail['shared_commision']) ? $productDetail['shared_commision'] : 0;

                    // Check whether product have special commission or not
                    $spComTimeVal = $dbResult[$orderIndex]['created_at'];                    
                    $spCommission = $spCommissionModel->findFirst(
                        array(
                            'conditions' => 'sku = "'.$dataOrder['sku'].'" AND store_code = "'.$dataOrder['store_code'].'" AND special_from_date < "'.$spComTimeVal.'" AND special_to_date > "'.$spComTimeVal.'"',
                            'order' => 'created_at desc'
                        )
                    );

                    if($spCommission) {
                        $spCommission = $spCommission->toArray();
                        $productDetail['shared_commision'] = $spCommission['special_commission'];
                    }

                    $dbResult[$orderIndex]['commision_percent'] = $productDetail['shared_commision'];
                    $dbResult[$orderIndex]['commision_value'] = ($productDetail['shared_commision'] * $dataOrder['row_total'] / 100) / 1.1;
                    $dbResult[$orderIndex]['VAT'] = (($productDetail['shared_commision'] * $dataOrder['row_total'] / 100) / 1.1) * 0.1;
                    $dbResult[$orderIndex]['net_commision'] = ($productDetail['shared_commision'] * $dataOrder['row_total'] / 100);
                    $dbResult[$orderIndex]['category'] = "";
                    foreach ($productDetail['categories'] as $category) {
                        if ($category['breadcrumb_position'] == 1) {
                            $dbResult[$orderIndex]['category'] = $category['name'];
                            break;
                        }
                    }
                }

                return $dbResult;
            } else {
                \Helpers\LogHelper::log("salesOrder", 'There have something error, in query queryMpSettlement');
                return false;
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("salesOrder", json_encode($e->getMessage()));
            return false;
        }
    }

    /**
     * ToDo : Marketplace Settlement
     */
    public function queryTotalMpSettlement($params = array())
    {
        try {

            $sql = "   
                select
                    (
                        select count(*) as total from sales_invoice
                        where invoice_id in (
                            select 
                                invoice_id 
                            from sales_shipment 
                            where 	shipment_status = 'received' and
                                    delivered_date between date_sub('" . $params['start_date'] . " 00:00:00',interval 7 hour) and date_sub('" . $params['end_date'] . " 23:59:59',interval 7 hour)
                        ) and supplier_alias = '" . $params['supplier_alias'] . "'
                    
                    ) as total_order,
                    (
                        select IF(sum(qty_ordered) is null,0,sum(qty_ordered)) as total_qty from sales_invoice
                        where invoice_id in (
                            select 
                                invoice_id 
                            from sales_shipment 
                            where 	shipment_status = 'received' and
                                    delivered_date between date_sub('" . $params['start_date'] . " 00:00:00',interval 7 hour) and date_sub('" . $params['end_date'] . " 23:59:59',interval 7 hour)
                        ) and supplier_alias = '" . $params['supplier_alias'] . "'
                    ) as total_qty,
                    (
                        select IF(sum(subtotal) is null,0,sum(subtotal)) as total from sales_invoice
                        where invoice_id in (
                            select 
                                invoice_id 
                            from sales_shipment 
                            where 	shipment_status = 'received' and
                                    delivered_date between date_sub('" . $params['start_date'] . " 00:00:00',interval 7 hour) and date_sub('" . $params['end_date'] . " 23:59:59',interval 7 hour)
                        ) and supplier_alias = '" . $params['supplier_alias'] . "'
                    ) as total_gross
                  ";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);

            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                return $result->fetch();
            } else {
                \Helpers\LogHelper::log("salesOrder", 'There have something error, in query queryTotalMpSettlement');
                return false;
            }
        } catch (\Exception $e) {
            \Helpers\LogHelper::log("salesOrder", json_encode($e->getMessage()));
            return false;
        }
    }

    public function getKredivoId($orderNo = array())
    {
        try {
            $orderNo = implode("','", $orderNo);
            $sql = "SELECT n.transaction_id AS kredivo_id, so.order_no FROM sales_order so  
            LEFT JOIN nicepay n ON n.order_id = so.order_no 
            LEFT JOIN sales_order_payment sop ON sop.sales_order_id = so.sales_order_id
            WHERE so.order_no IN ('" . $orderNo . "') AND sop.method = 'kredivo' AND sop.type = 'credit_non_cc'";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);

            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                return $result->fetchAll();
            } else {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getOrderNoByKredivoId($kredivoId)
    {
        try {

            $sql = "SELECT so.order_no FROM sales_order so 
            LEFT JOIN nicepay n ON n.order_id = so.order_no 
            LEFT JOIN sales_order_payment sop ON sop.sales_order_id = so.sales_order_id
            WHERE n.transaction_id = '" . $kredivoId . "' AND sop.method = 'kredivo' AND sop.type = 'credit_non_cc'
            LIMIT 1";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);

            $orderNo = array();
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $kredivo = $result->fetch();
                array_push($orderNo, $kredivo['order_no']);
            }
            return $orderNo;
        } catch (\Exception $e) {
            return array();
        }
    }
    /* Start RR2020-2821 enhance Payment */
         
    public function isValidChangePaymentMethod($data_array ){        
        $result = $this->getHasChangedPayment(); 
        if (empty($data_array['payment']['va_bank']) && $data_array['payment']['method'] != 'qris'){
            return $result;
        }
        $result = 0;  
        if (!empty($data_array['cart_rules']) &&  $data_array['cart_rules'] !="[]"){            
            $promoData = json_decode($data_array['cart_rules']);            
            foreach($promoData as $data){
                if ($data->action->applied_action === "discount_payment"){
                    $result = $this->getHasChangedPayment();   
                    break;
                } 
            }              
        }
          
        $startTime = !is_null($data_array['updated_at']) ? $data_array['updated_at'] : $data_array['created_at'];       
        $limitTime = new DateTime($startTime);
        $limitTime->add(new DateInterval('PT4H'));
     
        $result = new DateTime()  > $limitTime ? -1 : $result; // cek condition still in 4 hour after create order
        return $result;
    } 
     /* End RR2020-2821 enhance Payment */ 

    public function removeWishlistEmployee($wishlistEmployeeParam){
        $apiWrapper = new APIWrapper(getenv('CUSTOMER_API'));
        $apiWrapper->setEndPoint("cart-wishlist/employee");
        $apiWrapper->setParam($wishlistEmployeeParam);
        if ($apiWrapper->sendRequest("put")) {
            $apiWrapper->formatResponse();
            LogHelper::log('storemode_wishlist','[SUCCESS] ' . json_encode($apiWrapper->getData()));
        } else {
            LogHelper::log('storemode_wishlist','[ERROR] when calling customer API ' . json_encode($apiWrapper->getError()));
        }
    }
 
    public function getOrderSummary($params = array()) {
        $offset    = 0;
        $limit     = 20;
        $giftCards = '';

        if(isset($params["limit"])) {
            $limit = $params["limit"];
        }
        if(isset($params["offset"])) {
            $offset = $params["offset"];
        }

        if(!isset($params["gift_cards"])) {
            $this->errorCode = "404";
            $this->errorMessages = "please put parameter gift_cards, etc.";
            return array();
        } else {
            $giftCards = $params["gift_cards"];
        }

        $this->useReadOnlyConnection();

        $sql = "select".
        " distinct".
        "	 so.order_no as order_no,".
        "	 so.created_at as created_at,".
        "	 sc.customer_email as customer_email".
        " from".
        "	 salesrule_order sro ".
        " left join".
        "	 sales_order so ".
        " on".
        "	 sro.sales_order_id = so.sales_order_id".
        " left join".
        "	 sales_customer sc  ".
        " on".
        "	 sc.sales_order_id  = so.sales_order_id ".
        " where".
        "	 sro.voucher_code = \"".$giftCards."\"".
        " limit ".$limit."".
        " offset ".$offset.";";

        $queryResult = $this->getDi()->getShared($this->getConnection())->query($sql);

        $queryResult->setFetchMode(Database::FETCH_OBJ);
        $orders = $queryResult->fetchAll();
        return $orders;
    }

    public function saveSalesRuleCreditCard($shoppingCartData) {
        try {
            $cartrules = json_decode($shoppingCartData['cart_rules'], true);

            foreach ($cartrules as $cartrule) {
                //check condition if cart rule attribute has save cc
                $isSaveCC = false;
                foreach ($cartrule['conditions'] as $condition) {   
                    if ($condition['attribute'] == 'payment.SaveCC') {
                        $isSaveCC = true;
                    }
                }

                if (!$isSaveCC) {
                    continue;
                }

                $salesRuleCreditCardModel = new \Models\SalesruleCreditCard();
 
                $param = array();
                $param['customer_id'] = $shoppingCartData['customer']['customer_id']; 
                $param['rule_id'] = $cartrule['rule_id']; 
                $param['cc_type'] =  $shoppingCartData['payment']['cc_type']; 
                $param['cc_number'] = $shoppingCartData['payment']['cc_number']; 

            
                $salesRuleCreditCardModel->setFromArray($param);
                $salesRuleCreditCardModel->saveData();
            }
        } catch (\Exception $e) {
            LogHelper::log("createOrderError", $this->order_no . " : Create salesrule credit card failed, Msg : " . $e->getMessage());
            return false;
        }

        return true;
    }

    public function saveSalesruleOrderPackage($shoppingCartData) {
        try {
            $mapRuleIDPackageID = array();

            // loop through cart rules, get rule id with condition package id is
            $cartrules = json_decode($shoppingCartData['cart_rules'], true);

            foreach ($cartrules as $cartrule) {
                $packageID = "";
                //check condition if cart rule attribute have package id
                foreach ($cartrule['conditions'] as $condition) {   
                    if ($condition['attribute'] == 'items.PackageID') {
                       $packageID = $condition['value'];
                       break;
                    }
                }

                if ($packageID != "") {
                    $mapRuleIDPackageID[$cartrule['rule_id']] = $packageID;
                }
             
            }


            // loop through cart items
            // if item have attribute package_id
            // create map [package_id][package_qty]

            $mapPackageQty = array();
            foreach ($shoppingCartData['items'] as $item) {
                if (isset($item['package']['package_id'])) {
                    if ($item['package']['package_id'] != 0) {
                        $mapPackageQty[$item['package']['package_id']] = $item['package']['qty_ordered'];
                    }
                }
            }

            foreach ($mapRuleIDPackageID as $ruleID => $packageID) {
                foreach ($mapPackageQty as $itemPackageID => $packageQty) {
                    if ($packageID == $itemPackageID) {
                        // insert to \Models\SalesruleOrderPackage() using created map

                        $salesruleOrderPackageModel = new \Models\SalesruleOrderPackage();
                        
                        $param = array();
                        $param['rule_id'] = $ruleID; 
                        $param['sales_order_id'] = $this->sales_order_id;
                        $param['salesrule_package_id'] = $itemPackageID;
                        $param['package_qty'] = $packageQty; 

                        $salesruleOrderPackageModel->setFromArray($param);
                        $salesruleOrderPackageModel->saveData();

                        break;
                    }
                }
            }

        } catch (\Exception $e) {
            LogHelper::log("createOrderError", $this->order_no . " : Create salesrule order package failed, Msg : " . $e->getMessage());
            return false;
        }

        return true;
    }
}
