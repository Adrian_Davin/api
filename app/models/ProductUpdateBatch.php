<?php

namespace Models;

use Phalcon\Db;

class ProductUpdateBatch extends \Models\BaseModel
{

    protected $product_update_batch_id;
    protected $file_csv;
    protected $updated_fields;
    protected $sku;
    protected $total_updated;
    protected $total_data;
    protected $created_at;
    protected $updated_at;
    protected $created_user;
    protected $errorCode;
    protected $errorMessages;
    protected $updated_data;

    /**
     * @return mixed
     */
    public function getProductUpdateBatchId()
    {
        return $this->product_update_batch_id;
    }

    /**
     * @param mixed $product_update_batch_id
     */
    public function setProductUpdateBatchId($product_update_batch_id)
    {
        $this->product_update_batch_id = $product_update_batch_id;
    }

    /**
     * @return mixed
     */
    public function getFileCsv()
    {
        return $this->file_csv;
    }

    /**
     * @param mixed $file_csv
     */
    public function setFileCsv($file_csv)
    {
        $this->file_csv = $file_csv;
    }

    /**
     * @return mixed
     */
    public function getUpdatedFields()
    {
        return $this->updated_fields;
    }

    /**
     * @param mixed $update_fields
     */
    public function setUpdatedFields($updated_fields)
    {
        $this->updated_fields = $updated_fields;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getTotalUpdated()
    {
        return $this->total_updated;
    }

    /**
     * @param mixed $total_updated
     */
    public function setTotalUpdated($total_updated)
    {
        $this->total_updated = $total_updated;
    }

    /**
     * @return mixed
     */
    public function getTotalData()
    {
        return $this->total_data;
    }

    /**
     * @param mixed $total_data
     */
    public function setTotalData($total_data)
    {
        $this->total_data = $total_data;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedUser()
    {
        return $this->created_user;
    }

    /**
     * @param mixed $created_user
     */
    public function setCreatedUser($created_user)
    {
        $this->created_user = $created_user;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * @return mixed
     */
    public function getUpdatedData()
    {
        return $this->updated_data;
    }

    /**
     * @param mixed $updated_data
     */
    public function setUpdatedData($updated_data)
    {
        $this->updated_data = $updated_data;
    }



    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'product_update_batch';
    }


    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {

                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

}
