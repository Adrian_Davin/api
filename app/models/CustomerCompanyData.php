<?php

namespace Models;

class CustomerCompanyData extends \Models\BaseModel
{

    protected $customer_company_data_id;
    protected $customer_b2b_account_id;
    protected $company_name;

    /**
     * @return mixed
     */
    public function getSourceFrom()
    {
        return $this->source_from;
    }

    public function initialize()
    {
        $this->belongsTo('customer_company_data_id', 'Models\CustomerB2bCompany', 'customer_company_data_id', array('alias' => 'CustomerB2bCompany'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customer_company_data';
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CustomerCompanyData
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($dataArray = array())
    {
        foreach ($dataArray as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$dataArray));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
        return $view;
    }

    public function getCustomerCompanyDataId() {
        return $this->customer_company_data_id;
    }

    public function setCustomerCompanyDataId($customer_company_data_id) {
        $this->customer_company_data_id = $customer_company_data_id;
    }

    public function getCompanyName() {
        return $this->company_name;
    }

    public function setCompanyName($company_name) {
        $this->company_name = $company_name;
    }

    public function getCustomerB2bAccountId() {
        return $this->customer_b2b_account_id;
    }

    public function setCustomerB2bAccountId($customer_b2b_account_id) {
        $this->customer_b2b_account_id = $customer_b2b_account_id;
    }
}