<?php

namespace Models;

use Helpers\LogHelper;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;
use Library\Nsq;

class SalesInvoice extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $invoice_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $invoice_status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $sales_order_id;

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $subtotal;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $discount_amount = 0;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $shipping_amount =  0;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $shipping_discount_amount = 0;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $handling_fee_adjust = 0;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $grand_total = 0;

    /**
     *
     * @var double
     * @Column(type="double", length=12, nullable=true)
     */
    protected $qty_ordered = 0;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $invoice_no;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $gift_cards_amount;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $sap_so_number;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $pickup_voucher;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $delivery_method;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $store_code;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $supplier_alias;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    protected $pickup_code;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $receipt_id;

    /**
     *
     * @var double
     * @Column(type="double", length=10, nullable=true)
     */
    protected $real_price;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    protected $pin_pos;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $total_koli;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $status_packed;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=true)
     */
    protected $sla_age;

    /**
     *
     * @var string
     * @Column(type="string", length=30, nullable=true)
     */
    protected $ttpu_no;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $status_fulfillment;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $customer_pin_code;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $company_code = 'ODI';

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $updated_at;

    /**
     * @var \Models\Items\Invoice\Collection
     */
    protected $invoiceItems;

    /**
     * @var \Models\Shipment\Collection
     */
    protected $shipmentList;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasOne('invoice_id', 'Models\SalesShipment', 'invoice_id', array('alias' => 'SalesShipment')); 
        $this->hasMany('invoice_id', 'Models\SalesInvoiceItem', 'invoice_id', array('alias' => 'SalesInvoiceItem'));
        $this->hasMany('invoice_id', 'Models\SalesInvoiceRemark', 'invoice_id', array('alias' => 'SalesInvoiceRemark'));
        $this->hasMany('invoice_id', 'Models\SalesComment', 'invoice_id', array('alias' => 'SalesComment'));
        $this->hasOne('invoice_id', 'Models\SalesReorder', 'invoice_id', array('alias' => 'SalesReorder'));
        $this->hasMany('invoice_no', 'Models\SalesInvoiceOd', 'invoice_no', array('alias' => 'InvoiceNo'));
        $this->hasMany('invoice_id', 'Models\SalesInvoicePickupVoucher', 'invoice_id', array('alias' => 'SalesInvoicePickupVoucher'));
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
        $this->belongsTo('store_code', 'Models\Store', 'store_code', array('alias' => 'Store', 'reusable' => true));
        $this->belongsTo('supplier_alias', 'Models\Supplier', 'supplier_alias', array('alias' => 'Supplier', 'reusable' => true));
        $this->belongsTo('pickup_code', 'Models\PickupPoint', 'pickup_code', array('alias' => 'PickupPoint', 'reusable' => true));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_invoice';
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoice_id;
    }

    /**
     * @return string
     */
    public function getInvoiceNo()
    {
        return $this->invoice_no;
    }

    /**
     * @param string $invoice_no
     */
    public function setInvoiceNo($invoice_no)
    {
        $this->invoice_no = $invoice_no;
    }

    /**
     * @return string
     */
    public function getInvoiceStatus()
    {
        return $this->invoice_status;
    }

    /**
     * @param string $invoice_status
     */
    public function setInvoiceStatus($invoice_status)
    {
        $this->invoice_status = $invoice_status;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->store_code;
    }

    /**
     * @param string $store_code
     */
    public function setStoreCode($store_code)
    {
        $this->store_code = $store_code;
    }

    /**
     * @return string
     */
    public function getDeliveryMethod()
    {
        return $this->delivery_method;
    }

    /**
     * @param string $delivery_method
     */
    public function setDeliveryMethod($delivery_method)
    {
        $this->delivery_method = $delivery_method;
    }

    /**
     * @return string
     */
    public function getPickupVoucher()
    {
        return $this->pickup_voucher;
    }

    /**
     * @param string $pickup_voucher
     */
    public function setPickupVoucher($pickup_voucher)
    {
        $this->pickup_voucher = $pickup_voucher;
    }

    /**
     * @return string
     */
    public function getSupplierAlias()
    {
        return $this->supplier_alias;
    }

    /**
     * @param string $supplier_alias
     */
    public function setSupplierAlias($supplier_alias)
    {
        $this->supplier_alias = $supplier_alias;
    }

    /**
     * @return string
     */
    public function getPickupCode()
    {
        return $this->pickup_code;
    }

    /**
     * @param string $pickup_code
     */
    public function setPickupCode($pickup_code)
    {
        $this->pickup_code = $pickup_code;
    }

    /**
     * @return string
     */
    public function getSapSoNumber()
    {
        return $this->sap_so_number;
    }

    /**
     * @param string $sap_so_number
     */
    public function setSapSoNumber($sap_so_number)
    {
        $this->sap_so_number = $sap_so_number;
    }

    /**
     * @return string
     */
    public function getReceiptId()
    {
        return $this->receipt_id;
    }

    /**
     * @param string $receipt_id
     */
    public function setReceiptId($receipt_id)
    {
        $this->receipt_id = $receipt_id;
    }

    /**
     * @return float
     */
    public function getRealPrice()
    {
        return $this->real_price;
    }

    /**
     * @param float $real_price
     */
    public function setRealPrice($real_price)
    {
        $this->real_price = $real_price;
    }
    
    /**
     * @return string
     */
    public function getPinPos()
    {
        return $this->pin_pos;
    }

    /**
     * @param string $pin_pos
     */
    public function setPinPos($pin_pos)
    {
        $this->pin_pos = $pin_pos;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param float $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * @param float $discount_amount
     */
    public function setDiscountAmount($discount_amount)
    {
        $this->discount_amount = $discount_amount;
    }

    /**
     * @return float
     */
    public function getShippingAmount()
    {
        return $this->shipping_amount;
    }

    /**
     * @param float $shipping_amount
     */
    public function setShippingAmount($shipping_amount)
    {
        $this->shipping_amount = $shipping_amount;
    }

    /**
     * @return float
     */
    public function getShippingDiscountAmount()
    {
        return $this->shipping_discount_amount;
    }

    /**
     * @param float $shipping_discount_amount
     */
    public function setShippingDiscountAmount($shipping_discount_amount)
    {
        $this->shipping_discount_amount = $shipping_discount_amount;
    }

    /**
     * @return float
     */
    public function getHandlingFeeAdjust()
    {
        return $this->handling_fee_adjust;
    }

    /**
     * @param float $handling_fee_adjust
     */
    public function setHandlingFeeAdjust($handling_fee_adjust)
    {
        $this->handling_fee_adjust = $handling_fee_adjust;
    }

    /**
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->grand_total;
    }

    /**
     * @param float $grand_total
     */
    public function setGrandTotal($grand_total)
    {
        $this->grand_total = $grand_total;
    }

    /**
     * @return float
     */
    public function getQtyOrdered()
    {
        return $this->qty_ordered;
    }

    /**
     * @param float $qty_ordered
     */
    public function setQtyOrdered($qty_ordered)
    {
        $this->qty_ordered = $qty_ordered;
    }

    /**
     * @return float
     */
    public function getGiftCardsAmount()
    {
        return $this->gift_cards_amount;
    }

    /**
     * @param float $gift_cards_amount
     */
    public function setGiftCardsAmount($gift_cards_amount)
    {
        $this->gift_cards_amount = $gift_cards_amount;
    }

    /**
     * @return integer
     */
    public function getTotalKoli()
    {
        return $this->total_koli;
    }

    /**
     * @param integer $total_koli
     */
    public function setTotalKoli($total_koli)
    {
        $this->total_koli = $total_koli;
    }

    /**
     * @return integer
     */
    public function getStatusPacked()
    {
        return $this->status_packed;
    }

    /**
     * @param integer $status_packed
     */
    public function setStatusPacked($status_packed)
    {
        $this->status_packed = $status_packed;
    }

    /**
     * @return integer
     */
    public function getSlaAge()
    {
        return $this->sla_age;
    }

    /**
     * @param integer $sla_age
     */
    public function setSlaAge($sla_age)
    {
        $this->sla_age = $sla_age;
    }

    /**
     * @return string
     */
    public function getTtpuNo()
    {
        return $this->ttpu_no;
    }

    /**
     * @param string $ttpu_no
     */
    public function setTtpuNo($ttpu_no)
    {
        $this->ttpu_no = $ttpu_no;
    }

    /**
     * @return string
     */
    public function getStatusFulfillment()
    {
        return $this->status_fulfillment;
    }

    /**
     * @param string $status_fulfillment
     */
    public function setStatusFulfillment($status_fulfillment)
    {
        $this->status_fulfillment = $status_fulfillment;
    }

    /**
     * @return string
     */
    public function getCustomerPinCode()
    {
        return $this->customer_pin_code;
    }

    /**
     * @param string $customer_pin_code
     */
    public function setCustomerPinCode($customer_pin_code)
    {
        $this->customer_pin_code = $customer_pin_code;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param string $company_code
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;
    }

    public function getInvoiceItems()
    {
        return $this->invoiceItems;
    }

    public function setInvoiceItems($invoiceItems)
    {
        $this->invoiceItems = $invoiceItems;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesInvoice[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesInvoice
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_invoice", "TRIGGERED");
    }

    public function setFromArray($data_array = array())
    {
        $mdrCustomer = 0.0;
        foreach($data_array as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }

            if($key == 'items') {
                $invoiceItems = array();
                foreach($val as $item) {
                    $this->qty_ordered += $item['qty_ordered'];
                    $itemSubTotal = \Helpers\Transaction::countSubTotalItem($item['selling_price'],$item['handling_fee_adjust']);
                    $itemRowTotal = \Helpers\Transaction::countTotalItem($itemSubTotal,$item['qty_ordered']);
                    $this->subtotal += $itemRowTotal;
                    $invoiceItemModel = new \Models\SalesInvoiceItem();
                    $invoiceItemModel->setFromArray($item);
                    $invoiceItems[] = $invoiceItemModel;                      
                    $mdrCustomer +=  isset($item['mdr_customer']) ? $item['mdr_customer'] : 0;                  
                    
                }
                $this->invoiceItems = new \Models\Items\Invoice\Collection($invoiceItems);
            }

            if($key == 'shipment') {
                $shipmentList = array();
                foreach($val as $shipment) {
                    $salesShipmentModel = new \Models\SalesShipment();
                    $salesShipmentModel->setFromArray($shipment);
                    $shipmentList[] = $salesShipmentModel;
                }
                
                $this->shipmentList = new \Models\Shipment\Collection($shipmentList);
            }
        }

        /*
         * Subtotal here is total of row total items
         * Discount item that applied in item not include in global discount amount
         */
        $this->subtotal = round($this->subtotal);  
        $this->shipping_amount = round($this->shipping_amount);
        $this->shipping_discount_amount = round($this->shipping_discount_amount);
        $this->gift_cards_amount = round($this->gift_cards_amount);        
        $this->grand_total = \Helpers\Transaction::countTotal($this->subtotal, $this->discount_amount, $this->shipping_amount,$this->shipping_discount_amount, $this->gift_cards_amount,$mdrCustomer);        
   }

    public function setFromArrayUpdate($data_array = array())
    {
        $this->assign($data_array);

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = parent::toArray($columns,true);
        $invoiceItems = [];
        if($this->SalesInvoiceItem && empty($this->invoiceItems)) {
            foreach($this->SalesInvoiceItem as $item) {
                $invoiceItemModel = new \Models\SalesInvoiceItem();
                $invoiceItemModel->setFromArray($item->toArray());
                $invoiceItems[] = $invoiceItemModel;
            }
            $this->invoiceItems = new \Models\Items\Invoice\Collection($invoiceItems);
        }

        if($this->SalesShipment && empty($this->shipment)) {
            $salesShipmentModel = new \Models\SalesShipment(); 
            $salesShipmentModel->setFromArray($this->SalesShipment->toArray()); 
            $this->shipment = $salesShipmentModel;
        }

        if ($this->SalesReorder) {
            $salesReorderModel = new \Models\SalesReorder(); 
            $view['reorder'] = $this->SalesReorder->toArray(); 
        }

        if(!empty($this->Store)) {
            unset($view['store_code']);
            $view['store']  = $this->Store->toArray(array('store_id','store_code','name','email_manager'));

            if($this->Store->SupplierAddress) {
                $supplierAddress = new \Models\SupplierAddress();
                $supplierAddress->assign($this->Store->SupplierAddress->toArray());
                $storeAddress = $supplierAddress->getDataArray([],true);
                unset($storeAddress['supplier_address_id']);
                unset($storeAddress['supplier_address_type']);
                unset($storeAddress['supplier_addresscol']);
                $view['store'] = array_merge($view['store'],$storeAddress);
            }

            if($this->Store->PickupPoint) {
                $pickupPointModel = new \Models\PickupPoint();
                $pickupPointModel->assign($this->Store->PickupPoint->toArray());
                $storeAddress = $pickupPointModel->getDataArray([],true);
                $view['store'] = array_merge($view['store'],$storeAddress);
            }
        }

        if(!empty($this->Supplier)) {
            unset($view['supplier_alias']);
            $view['supplier'] = $this->Supplier->toArray(array('supplier_id', 'name', 'supplier_code', 'supplier_alias'));
        }

        if(!empty($this->PickupPoint)) {
            unset($view['pickup_code']);
            $view['pickup_code'] = $this->PickupPoint->toArray(array('pickup_code','pickup_name','email_pic','address_line_1','address_line_2'));
        }

        if(!empty($this->invoiceItems)) {
            $view['items'] = $this->invoiceItems->getDataArray([], true);

            // count subtotal
            /*foreach($view['items'] as $item) {
                $view['qty_ordered'] += $item['qty_ordered'];
                $view['handling_fee_adjust'] += \Helpers\Transaction::countHandlingFee($item['handling_fee_adjust'],$item['qty_ordered']);
                $view['subtotal'] += $item['subtotal'];
            }*/

            $view['grand_total'] = \Helpers\Transaction::countTotal($view['subtotal'], $view['discount_amount'], $view['shipping_amount'],$view['shipping_discount_amount'], $view['gift_cards_amount']);
        }

        if(!empty($this->shipment)) { 
            $view['shipment'] = $this->shipment->getDataArray([], true);
        }

        $view['pickup_vouchers'] = array();
       
        if(count($this->SalesInvoicePickupVoucher) > 0) {
            $pickupVouchersRaw = $this->SalesInvoicePickupVoucher->toArray();
            $pickupVouchersRowCount = count($pickupVouchersRaw);

            for ($i = 0; $i < $pickupVouchersRowCount; $i++) {
                if ($pickupVouchersRaw[$i]['status'] == '-1') {
                    continue;
                }

                $pickupVoucher['pickup_voucher'] = $pickupVouchersRaw[$i]['pickup_voucher'];
                $pickupVoucher['mop_type'] = $pickupVouchersRaw[$i]['mop_type'];
                array_push($view['pickup_vouchers'], $pickupVoucher);
            }

        // if pickup voucher exists in sales_invoice.pickup_voucher but not in sales_invoice_pickup_voucher
        // we will assume the type of this voucher is product 
        } else if (!empty($this->pickup_voucher)){
            $pickupVoucher['pickup_voucher'] = $this->pickup_voucher;
            $pickupVoucher['mop_type'] = $_ENV['MOP_TYPE_PRODUCT'];
            array_push($view['pickup_vouchers'], $pickupVoucher);
        }

        // invoice shipment tracking
        $salesShipmentTrackingModel = new \Models\SalesShipmentTracking();
        $params = array(
            'invoice_no' => $view['invoice_no']
        );
        $response = $salesShipmentTrackingModel->getAwbTracking($params);
        $formattedData = array();

        if (empty($response['errors'])) {
            // reformat structure of status
            if (count($response['data']) > 0) {
                if (count($response['data']) == 1) {
                    if (isset($response['data'][0]['status'])) {
                        $cleanData = str_replace("\\","",$response['data'][0]['status']);
                        $data = json_decode($cleanData, True);
                        foreach ($data as $key => $value) {
                            if ($key == 'info') {
                                $flagNew = 'yes';
                            }
                        }

                        if (isset($flagNew)) {
                            $formattedData = $this->processNewFormat($cleanData);
                        } else {
                            $formattedData = $this->processOldFormat($response['data']);
                        }
                    }
                } else {
                    // check new data if exist in array list
                    for ($i=0;$i<count($response['data']);$i++) {
                        $cleanData = str_replace("\\","",$response['data'][$i]['status']);
                        $data = json_decode($cleanData, True);
                        foreach ($data as $key => $value) {
                            if ($key == 'info') {
                                $index = $i;
                            }
                        }
                    }

                    if (isset($index)) { // found new format
                        $cleanData = str_replace("\\","",$response['data'][$index]['status']);
                        $formattedData = $this->processNewFormat($cleanData);
                    } else { // all data is old format
                        $formattedData = $this->processOldFormat($response['data']);
                    }
                }
            }
        }

        $view['shipment_tracking'] = array_reverse($formattedData);

        if (!empty ($this->receipt_id)) {
            $view['receipt_id'] = $this->receipt_id;
        } else {    
            $view['receipt_id'] = '';
        }

        return $view;
    }

    public function beforeCreate()
    {
        parent::beforeCreate();
        $result = $this->findFirst("invoice_no = '" . $this->invoice_no ."'");
        if($result) {
            do {
                $result = $this->findFirst("invoice_no = '" . $this->invoice_no ."'");
                $this->invoice_no = \Helpers\Transaction::generateInvoiceNo();
            } while(!empty($result));
        }
    }

    public function createInvoice($log_file_name = "", $state = "")
    {
        try {
            // Create a transaction manager
            $manager = new TxManager();

            // Request a transaction
            $this->useWriteConnection();
            $manager->setDbService($this->getConnection());
            $transaction = $manager->get();
            $this->setTransaction($transaction);
            
            // create sales invoice
            $saveStatus = $this->create();
            if ($saveStatus === false) {
                $messages = $this->getMessages();

                $errMsg = array();
                foreach ($messages as $message) {
                    $errMsg[] = $message->getMessage();
                }

                \Helpers\LogHelper::log($log_file_name, "Invoice save failed, error : " . json_encode($errMsg));
                $this->errorCode = "RR301";
                $this->errorMessages[] = "Create Invoice failed";

                if($state !== "") {
                    $params_data = array(
                        "message" => "Failed at $state : Caused by: ".json_encode($errMsg),
                        "slack_channel" => $_ENV['MARKETING_SLACK_CHANNEL'],
                        "slack_username" => $_ENV['OPS_SLACK_USERNAME']
                    );
                    $this->notificationError($params_data);
                }

                $transaction->rollback(
                    "Create invoice failed"
                );
                return false;
            }

            try {
                $this->invoiceItems->setInvoiceId($this->invoice_id);
                $this->invoiceItems->saveData($transaction);
            } catch (\Exception $e) {
                $transaction->rollback(
                    "saveItemInvoice : " . $e->getMessage()
                );
            }

            if(!empty($this->shipmentList)) {
                try {
                    if ($this->store_code[0] != "M") {
                        $shipmentListData = $this->shipmentList->getDataArray();
                        if (count($shipmentListData) > 0 && isset($shipmentListData[0]['shipping_address'])) {                       
                            $carrierID = $shipmentListData[0]['shipping_address']['carrier_id'];

                            // currently only for AHI
                            $ownfleetKL = array(getenv('OWNFLEET_ACE'));

                            if ($shipmentListData[0]['shipment_type'] == 'ownfleet' && $shipmentListData[0]['total_weight'] > 0 && in_array($carrierID, $ownfleetKL)) {
                                    $shippingRateACE = 95000;
                                    if (!empty(getenv('OWNFLEET_ACE_SHIPPING_RATE'))) {
                                        $shippingRateACE = getenv('OWNFLEET_ACE_SHIPPING_RATE');
                                    }

                                    $this->shipmentList->setRealShippingAmount($shippingRateACE);
                            }                        
                        }                        
                    }
                    
                    $this->shipmentList->setInvoiceId($this->invoice_id);
                    $this->shipmentList->saveData($transaction);
                } catch (\Exception $e) {
                    $transaction->rollback(
                        "saveShipment : " . $e->getMessage()
                    );
                }
            }

            // sales comment for preorder
            $invoiceItems = $this->invoiceItems;
            foreach($invoiceItems as $item) {
                if($item->getPreorderDate() != null && $item->getPreorderDate() != ''){
                    $params['sales_order_id'] = $this->sales_order_id;
                    $params['invoice_id'] = $this->invoice_id;
                    $params['comment'] = "Produk Preorder, sku no ".$item->getSku()." (".$item->getQtyOrdered()." pcs)"." akan tersedia pada tanggal ".$item->getPreorderDate();
                    $params['supplier_user_id'] = substr($item->getSupplierAlias(), 0, 2) == "MP" ? 103 : 0;
                    $params['admin_user_id'] = substr($item->getSupplierAlias(), 0, 2) == "MP" ? 0 : 1;
                    $params['user_group'] = substr($item->getSupplierAlias(), 0, 2) == "MP" ? 'system' : null;
                    $params['flag']= 'chat';
                    // create 2 info for menu oms sales order and dc / store dashboard
                    $commentModel = new \Models\SalesComment();        
                    $commentModel->setFromArray($params);
                    $commentModel->saveData();

                    if (substr($item->getSupplierAlias(), 0, 2) != "MP") {
                        $params['flag'] = 'info';
                        $commentModel = new \Models\SalesComment();        
                        $commentModel->setFromArray($params);
                        $commentModel->saveData();
                    }
                }

                try {
                    // This is for support Gift Registry. The code is used to inserting invoice_no to gift registry
                    // Only run this if order is not from shoped
                    $orderData = $this->SalesOrder;
                    $listOrderVendorPrefix = ['ODIT', 'ODIS', 'ODIK'];
                    if (!in_array(substr($orderData->order_no, 0, 4), $listOrderVendorPrefix)) {
                        $cartModel = new \Models\Cart();
                        $cartModel->setCartId($orderData->cart_id);

                        $headers = [
                            'No-Refresh' => "1",
                            'Cart-Type' => "gift"
                        ];
                        $cartData = $cartModel->getCartDetailV2($headers);
                        if (!empty($cartModel->getErrorCode()) && !empty($cartModel->getErrorMessages()['message'])) {
                            throw new \Exception($cartModel->getErrorMessages()['message']);
                        }

                        $parentID = (int) $item->SalesOrderItem->getParentId();
                        if ($cartData['cart_type'] === 'gift' && $parentID == 0){
                            $payload = [
                                "registry_id" => $cartData['gift_registry']['registry_id'],
                                "sku" => $item->getSku(),
                                "quantity_on_hold_payment" => -$item->getQtyOrdered(),
                                "quantity_paid" => $item->getQtyOrdered(),
                                "order_no" => $orderData->order_no,
                                "invoice_no" => $this->getInvoiceNo()
                            ];

                            $giftAPI = new \Library\GiftRegistryAPI();
                            $ok = $giftAPI->updateRegistryItemOrder($payload);
                            if (!$ok) {
                                $errMessage = "Failed save invoice to gift registry";
                                if (!empty($giftAPI->getErrorMessage())) {
                                    $errMessage = sprintf("%s with message %s", $errMessage, $giftAPI->getErrorMessage());
                                }
                                throw new \Exception($errMessage);
                            }
                        }   
                    }
                } catch (\Exception $e) {
                    LogHelper::log($log_file_name, sprintf("Failed process gift data, order_no: %s, invoice_no: %s, error: %s", $orderData->order_no, $this->getInvoiceNo(), $e->getMessage()));
                    $transaction->rollback(
                        "saveRegistry : " . $e->getMessage()
                    );
                }
            }

            $transaction->commit();
        } catch (TxFailed $e) {
            throw new \Exception($e->getMessage());
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getShippingRate($originKecamatanID = 0,$destinationKecamatanID = 0, $carrierID = 0)
    {
        $shippingRate = 0;
        if ($originKecamatanID > 0 && $destinationKecamatanID > 0 && $carrierID > 0) {
            $cartModel = new \Models\Cart();
            $shippingRateData = $cartModel->getShippingRate($originKecamatanID,$destinationKecamatanID, $carrierID);
            if (isset($shippingRateData['price'])) {
                return $shippingRateData['price'];
            }
        }
    }

    // public function beforeValidationOnUpdate()
    // {
    //     $this->setup(array(
    //         "notNullValidations" => false
    //     ));
    // }

    public function updateBatchSoNumber($params = array())
    {
        try {
            $sql = "UPDATE sales_invoice SET sap_so_number = CASE ";

            foreach ($params as $row) {
                $sql .= " WHEN (invoice_no = '{$row['INVOICEID']}') THEN '{$row['SAP_SO']}'";

                $arr_invoice[] = "'{$row['INVOICEID']}'";
            }

            $strInvoice = implode(',',$arr_invoice);
            $sql .= " END WHERE invoice_no in ($strInvoice) ";

            $result = $this->getDi()->getShared('dbMaster')->execute($sql);
            if ($result) {
                $response['messages'] = array('success');
                $response['data']['affected_row'] = $this->getDi()->getShared('dbMaster')->affectedRows();
                $response['errors'] = [];
            } else {
                $response['errors'] = array('Update batch so sap number failed, please contact ruparupa tech support');
            }
        } catch (\Exception $e) {
            $response['errors'] = array('Update batch so sap number failed, please contact ruparupa tech support');
        }

        return $response;
    }

    public function checkEmptySoSap()
    {
        $response['data'] = $response['errors'] = array();

        try {
            $sql = "
            SELECT 
                so.order_no,
                si.invoice_no,
                si.created_at
            FROM
                sales_invoice si LEFT JOIN
                sales_order so ON so.sales_order_id = si.sales_order_id
            WHERE 
                ISNULL(si.sap_so_number) AND
                si.delivery_method = 'delivery' AND
                si.invoice_status IN ('new','processing') AND
                substr(si.store_code,1,2) = 'DC' AND
                time_to_sec((TIMEDIFF(NOW(),si.created_at))) / 3600 >= {$_ENV['HOUR_TOLERANCE']};";

            $this->useReadOnlyConnection();
            $result = $this->getDi()->getShared($this->getConnection())->query($sql);
            $data = [];
            if ($result->numRows() > 0) {
                $result->setFetchMode(Database::FETCH_ASSOC);
                $data = $result->fetchAll();

                $concatText = '';
                foreach ($data as $row) {
                    if (strlen($row['order_no']) == 14) {
                        $row['order_no'] = $row['order_no'] . '   ';
                    }

                    $concatText .= "Order No. : {$row['order_no']}, Invoice No. : {$row['invoice_no']}, Created at: {$row['created_at']} \n";
                }

                $params = [ "channel" => $_ENV['ESS_SLACK_CHANNEL'],
                    "username" => $_ENV['ESS_SLACK_USERNAME'],
                    "text" => $concatText,
                    "icon_emoji" => ':robot_face:'
                ];

                // call queue for webhook slack
                $nsq = new Nsq();
                $message = [
                    "data" => $params,
                    "message" => "customerAlert"
                ];
                $nsq->publishCluster('transaction', $message);
            }

            $response['messages'] = array('success');
            $response['data'] = $data;
        } catch (\Exception $e) {
            $response['errors'] = array('Check Shipping Null failed, please contact ruparupa tech support ('.$e->getMessage() . ")");
        }

        return $response;
    }

    // shipment tracking purposes
    private function processNewFormat($cleanData = array())
    {
        $data = json_decode($cleanData, True);

        foreach ($data as $key => $value) {
            if ($key == 'info') {
                $paramInfo = $value;
            }

            if ($key == 'manifest') {
                $paramManifest = $value;
            }
        }

        $reformattedData = array();

        if (isset($paramInfo) && isset($paramManifest)) {
            if ($paramInfo['courier_name'] == 'JNE') {
                for ($i=0;$i<count($paramManifest);$i++) {
                    // reformat
                    $reformattedData[$i]['awb_no'] = isset($paramManifest[$i]['mfcnote_no']) ? $paramManifest[$i]['mfcnote_no'] : '-';
                    $reformattedData[$i]['courier_name'] = isset($paramInfo['courier_name']) ? $paramInfo['courier_name'] : '-';
                    $reformattedData[$i]['manifest_date'] = (isset($paramManifest[$i]['manifest_date']) && (($timestamp = strtotime($paramManifest[$i]['manifest_date'])) !== false)) ?
                        date("d-M-Y H:i:s", strtotime($paramManifest[$i]['manifest_date'])) : '-';
                    $reformattedData[$i]['description'] = isset($paramManifest[$i]['keterangan']) ? $paramManifest[$i]['keterangan'] : '-';
                    $reformattedData[$i]['location'] = isset($paramManifest[$i]['city_name']) ? $paramManifest[$i]['city_name'] : '-';
                }
            } elseif ($paramInfo['courier_name'] == 'SAP Express') {
                for ($i=0;$i<count($paramManifest);$i++) {
                    // reformat
                    $reformattedData[$i]['awb_no'] = isset($paramManifest[$i]['cn_no']) ? $paramManifest[$i]['cn_no'] : '-';
                    $reformattedData[$i]['courier_name'] = isset($paramInfo['courier_name']) ? $paramInfo['courier_name'] : '-';
                    $reformattedData[$i]['manifest_date'] = (isset($paramManifest[$i]['cn_date']) && (($timestamp = strtotime($paramManifest[$i]['cn_date'])) !== false)) ?
                        date("d-M-Y H:i:s", strtotime($paramManifest[$i]['cn_date'])) : '-';
                    $reformattedData[$i]['description'] = isset($paramManifest[$i]['cn_status']) && isset($paramManifest[$i]['cn_desc']) ? $paramManifest[$i]['cn_status'].' | '.$paramManifest[$i]['cn_desc'] : '-';
                    $reformattedData[$i]['location'] = isset($paramManifest[$i]['cn_locationby']) ? $paramManifest[$i]['cn_locationby'] : '-';
                }
            } elseif ($paramInfo['courier_name'] == 'Ninja Van') {
                for ($i=0;$i<count($paramManifest);$i++) {
                    // reformat
                    $reformattedData[$i]['awb_no'] = isset($paramManifest[$i]['cn_no']) ? $paramManifest[$i]['cn_no'] : '-';
                    $reformattedData[$i]['courier_name'] = isset($paramInfo['courier_name']) ? $paramInfo['courier_name'] : '-';
                    $reformattedData[$i]['manifest_date'] = (isset($paramManifest[$i]['cn_date']) && (($timestamp = strtotime($paramManifest[$i]['cn_date'])) !== false)) ?
                        date("d-M-Y H:i:s", strtotime($paramManifest[$i]['cn_date'])) : '-';
                    $reformattedData[$i]['description'] = isset($paramManifest[$i]['cn_status']) && isset($paramManifest[$i]['cn_desc']) ? $paramManifest[$i]['cn_status'].' | '.$paramManifest[$i]['cn_desc'] : '-';
                    $reformattedData[$i]['location'] = isset($paramManifest[$i]['cn_locationby']) ? $paramManifest[$i]['cn_locationby'] : '-';
                }
            } elseif ($paramInfo['courier_name'] == 'NCS') {
                for ($i=0;$i<count($paramManifest);$i++) {
                    // reformat
                    $reformattedData[$i]['awb_no'] = isset($paramManifest[$i]['mfcnote_no']) ? $paramManifest[$i]['mfcnote_no'] : '-';
                    $reformattedData[$i]['courier_name'] = isset($paramInfo['courier_name']) ? $paramInfo['courier_name'] : '-';
                    $reformattedData[$i]['manifest_date'] = (isset($paramManifest[$i]['manifest_date']) && (($timestamp = strtotime($paramManifest[$i]['manifest_date'])) !== false)) ?
                        date("d-M-Y H:i:s", strtotime($paramManifest[$i]['manifest_date'])) : '-';
                    $reformattedData[$i]['description'] = isset($paramManifest[$i]['keterangan']) ? $paramManifest[$i]['keterangan'] : '-';
                    $reformattedData[$i]['location'] = isset($paramManifest[$i]['city_name']) ? $paramManifest[$i]['city_name'] : '-';
                }
            } elseif ($paramInfo['courier_name'] == 'Informa Delivery' || $paramInfo['courier_name'] == 'Ace Delivery') {
                for ($i=0;$i<count($paramManifest);$i++) {
                    // reformat
                    $reformattedData[$i]['awb_no'] = isset($paramManifest[$i]['mfcnote_no']) ? $paramManifest[$i]['mfcnote_no'] : '-';
                    $reformattedData[$i]['courier_name'] = isset($paramInfo['courier_name']) ? $paramInfo['courier_name'] : '-';
                    $reformattedData[$i]['manifest_date'] = (isset($paramManifest[$i]['manifest_date']) && (($timestamp = strtotime($paramManifest[$i]['manifest_date'])) !== false)) ?
                        date("d-M-Y H:i:s", strtotime($paramManifest[$i]['manifest_date'])) : '-';
                    $reformattedData[$i]['description'] = isset($paramManifest[$i]['keterangan']) ? $paramManifest[$i]['keterangan'] : '-';
                    $reformattedData[$i]['location'] = isset($paramManifest[$i]['city_name']) ? $paramManifest[$i]['city_name'] : '-';
                    $reformattedData[$i]['driver_name'] = isset($paramManifest[$i]['driver_name']) ? $paramManifest[$i]['driver_name'] : '-';
                    $reformattedData[$i]['vehicle_number'] = isset($paramManifest[$i]['vehicle_number']) ? $paramManifest[$i]['vehicle_number'] : '-';
                }
            } else if (strtolower(substr($paramInfo['courier_name'], 0, 7)) == "go-send") {
                for ($i=0; $i < count($paramManifest); $i++) {
                    // reformat
                    $reformattedData[$i]['awb_no'] = isset($paramManifest[$i]['mfcnote_no']) ? $paramManifest[$i]['mfcnote_no'] : '-';
                    $reformattedData[$i]['courier_name'] = isset($paramInfo['courier_name']) ? $paramInfo['courier_name'] : '-';
                    $reformattedData[$i]['manifest_date'] = (isset($paramManifest[$i]['manifest_date']) && (($timestamp = strtotime($paramManifest[$i]['manifest_date'])) !== false)) ?
                        date("d-M-Y H:i:s", strtotime($paramManifest[$i]['manifest_date'])) : '-';
                    $reformattedData[$i]['description'] = isset($paramManifest[$i]['keterangan']) ? $paramManifest[$i]['keterangan'] : '-';
                    $reformattedData[$i]['location'] = isset($paramManifest[$i]['city_name']) ? $paramManifest[$i]['city_name'] : '-';
                    $reformattedData[$i]['driver_info'] = isset($paramManifest[$i]['driver_info']) ? $paramManifest[$i]['driver_info'] : '-';
                    $reformattedData[$i]['live_tracking'] = isset($paramManifest[$i]['live_tracking']) ? $paramManifest[$i]['live_tracking'] : '-';
                }
            }
        } else {
            $reformattedData = array();
        }
        return array_reverse($reformattedData);
    }

    private function processOldFormat($result = array())
    {
        $reformattedData = array();

        for ($i=0;$i<count($result);$i++) {
            $statusArray = json_decode($result[$i]['status'], True);

            // reformat
            $reformattedData[$i]['awb_no'] = (isset($result[$i]['no_awb'])) ? $result[$i]['no_awb'] : '-';
            $reformattedData[$i]['courier_name'] = (isset($result[$i]['carrier_name'])) ? $result[$i]['carrier_name'] : '-';
            $reformattedData[$i]['manifest_date'] = (isset($result[$i]['created_at']) && (($timestamp = strtotime($result[$i]['created_at'])) !== false)) ?
                date("d-M-Y H:i:s", strtotime($result[$i]['created_at'])) : '-';
            $reformattedData[$i]['description'] = isset($statusArray['status']) && isset($statusArray['description']) ? $statusArray['status'].' | '.$statusArray['description'] : '-';
            $reformattedData[$i]['location'] = '-'; // to be received soon
        }

        return array_reverse($reformattedData);
    }

    /**
     *
     * check for previously exist similar invoice by sales_order_id and store_code and delivery_method and subtotal
     * prevent double invoice to be created
     *
     * @return bool
     */
    public function checkExists () {
        $result = $this->findFirst(
            [
                'columns' => 'invoice_id',
                'conditions' => 'sales_order_id=?1 AND store_code=?2 AND delivery_method=?3 AND subtotal=?4',
                'bind' => [
                    1 => $this->sales_order_id,
                    2 => $this->store_code,
                    3 => $this->delivery_method,
                    4 => $this->subtotal
                ]
            ]);
        if($result) {
            return true;
        } else {
            return false;
        }

    }

    public function notificationError($params_data = array()){
        $notificationText = $params_data["message"];
        $params = [ 
            "channel" => $params_data["slack_channel"],
            "username" => $params_data["slack_username"],
            "text" => $notificationText,
            "icon_emoji" => ':robot_face:'
        ];

        // call queue for webhook slack
        $nsq = new \Library\Nsq();
        $message = [
            "data" => $params,
            "message" => "customerAlert"
        ];
        $nsq->publishCluster('transaction', $message);
    }

    public function checkIfConsignment(){
        $isConsignment = false;
        $invoiceItems = $this->invoiceItems;
        if (empty($invoiceItems)) {
            $invoiceItems = $this->SalesInvoiceItem;
            $this->invoiceItems = $this->SalesInvoiceItem;
        }
        foreach($invoiceItems as $item) {
            $isConsignment = $item->SalesOrderItemFlat->getConsignmentControl() == "1";
            if ($isConsignment) {
                break;
            }
        }

        return $isConsignment;
    }
}
