<?php
/**
 * Created by PhpStorm.
 * User: andy.antonius
 * Date: 10/19/2016
 * Time: 11:50 AM
 */

namespace Models\CartItem;

use Library\Marketing;
use \Models\CartItem;
use \Helpers\CartHelper;
use \Helpers\ProductHelper;
use \Helpers\Transaction as TransactionHelper;
use \Library\Marketing as MarketingLib;

class Collection extends \ArrayIterator
{
    public $itemList = array();
    public $kecamatan_code;
    protected $error_code;
    protected $error_messages = [];
    protected $sku;

    public function __construct($array = array(), $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    /**
     * @return \Models\CartItem
     */
    public function current()
    {
        return parent::current();
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->error_messages;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @return array
     */
    public function getDataArray()
    {
        $this->rewind();
        $view = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->getDataArray();
            $this->itemList[] = $itemArray['sku'];
            $view[$idx] = $itemArray;

            $this->next();
        }
        return $view;
    }

    public function defineCourierID()
    {
        $this->rewind();
        $view = array();
        $cartModel = new \Models\Cart();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->getDataArray();
            if (isset($itemArray['shipping_address']['kecamatan']['kecamatan_code']) && !CartHelper::isGosend($itemArray['carrier_id'])) {
                $courierID = $itemArray['carrier_id'];
                $item->setCarrierId($courierID);
                
                $itemArray['shipping_address']['carrier_id'] = $courierID;
                $item->setShippingAddress($itemArray['shipping_address']);
            }
            
            $this->next();
        }
    }

    public function completeShippingAddress()
    {
        $this->rewind();
        $view = array();
        $cartModel = new \Models\Cart();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $itemArray = $item->getDataArray();
            $shippingAddress = $itemArray['shipping_address'];
            if (count($itemArray['shipping_address']) > 0 && !empty($shippingAddress['sales_order_address_id'])) {
                if (!isset($itemArray['shipping_address']['province']['province_name'])) {
                    $resultProvince = $cartModel->getDataProvince($itemArray['shipping_address']['province']['province_id']);
                    if (count($resultProvince) > 0) {
                        $shippingAddress['province'] = $resultProvince;
                    }
                }

                if (!isset($itemArray['shipping_address']['city']['city_name'])) {
                    $resultCity = $cartModel->getDataCity($itemArray['shipping_address']['city']['city_id']);
                    if (count($resultCity) > 0) {
                        $shippingAddress['city'] = $resultCity;
                    }
                }

                if (!isset($itemArray['shipping_address']['kecamatan']['kecamatan_name'])) {
                    $resultKecamatan = $cartModel->getDataKecamatan($itemArray['shipping_address']['kecamatan']['kecamatan_id']);
                    if (count($resultKecamatan) > 0) {
                        $shippingAddress['kecamatan'] = $resultKecamatan;
                    }
                }

                $item->setShippingAddress($shippingAddress);
            }
            
            $this->next();
        }
    }

    public function setShippingReorder()
    {
        $this->rewind();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();
            $item->setShippingAmount(0);
            $item->setShippingDiscountAmount(0);
            $this->next();
        }
    }

    public function generateVirtualInvoice()
    {
        $this->rewind();
        $view = array();
        $originShippingRegion = array();
        $originShippingDistrict = array();
        $invoice = array();
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();

            $currentOriginShippingRegion = $item->getOriginShippingRegion();
            $currentOriginShippingDistrict = $item->getOriginShippingDistrict();
           
            $delivery_method = $item->getDeliveryMethod();

            if(!empty($currentOriginShippingRegion) && $delivery_method != "pickup" && $delivery_method != "ownfleet") {
                $originShippingRegion[] = $currentOriginShippingRegion;
            }

            if(!empty($currentOriginShippingDistrict) && $delivery_method != "pickup" && $delivery_method != "ownfleet") {
                $originShippingDistrict[] = $currentOriginShippingDistrict;
            }

            $itemArray = $item->getDataArray();

            $courierPickup = ($itemArray['delivery_method'] == 'pickup') ? $itemArray['store_code'] : $itemArray['carrier_id'];
            $geolocation = (isset($itemArray['shipping_address']['geolocation'])) ? $itemArray['shipping_address']['geolocation'] : 'geolocation_empty';

            $itemArray['store_code'] = isset($itemArray['store_code'])? $itemArray['store_code'] : "";
            if($delivery_method != 'pickup') {
                $invoice[$itemArray['supplier_alias']][$itemArray['store_code']][$delivery_method][$courierPickup][$geolocation]['origin_shipping_region'] = $currentOriginShippingRegion;
                $invoice[$itemArray['supplier_alias']][$itemArray['store_code']][$delivery_method][$courierPickup][$geolocation]['origin_shipping_district'] = $currentOriginShippingDistrict;
            }

            $invoice[$itemArray['supplier_alias']][$itemArray['store_code']][$delivery_method][$courierPickup][$geolocation][] = $itemArray;
            $this->next();
        }

        $view['invoice'] = $invoice;
        $view['origin_shipping_region'] = array_unique($originShippingRegion);
        $view['origin_shipping_district'] = array_unique($originShippingDistrict);

        return $view;
    }

    public function applyMarketingPromotion($rules = array(), $voucher_detail = array(), $company_code = 'ODI')
    {
        if(empty($rules['action']['attributes'])) {
            $discountCartAttribute = ['items','selling_price'];
        } else {
            $discountCartAttribute = explode(".",$rules['action']['attributes']);
        }
        $discountAmount = 0;
        
        $discountItemSkuList = array();
        if($rules['action']['applied_action'] == "the_cheapest" || $rules['action']['applied_action'] == "the_expensive") {
            $order = ($rules['action']['applied_action'] == "the_cheapest") ? "asc" : "desc";
            $sortedItem = $this->sortSkuByField($order);
            
            /**
             * discount_step_max in the_cheapest and the_expensive show how many items of the cheapest/expensive can get the discount
             */
            for($i=0;$i<$rules['action']['discount_step_max'];$i++) {
                if(isset($sortedItem[$i])) {
                    $discountItemSkuList[] = $sortedItem[$i];
                }
                
            }
        }
        
        $giftCardInfo = array();
        $currentDiscount = array();
        $giftCardAmount = $discountAmountGet = 0;
        
        // Handle specific conditions attribute
        $aggregate_rule_id = 0;
        $promo1212 = false;
        foreach($rules['conditions'] as $key => $value) {
            if ($value['attribute'] == 'items.grand_total_exclude_shipping') {
                $grandTotalExcludeShipping = 0;
                $operator = $value['operator'];
                $amountValue = $value['value'];
                
                // remove the items.grand_total_exclude_shipping condition from array
                unset($rules['conditions'][$key]);
            }else if ($value['attribute'] == 'cart_rules.rule_id') {
                if(isset($rules['type'])){
                    if($rules['type'] == 'payment'){
                        $aggregate_rule_id = $value['value'];
                    }
                }
            }elseif ($value['attribute'] == 'items.subtotal') {
                $itemsSubtotal = 0;
                $operator = $value['operator'];
                $amountValue = $value['value'];
                
                // remove the items.subtotal condition from array
                unset($rules['conditions'][$key]);
            }else if ($value['attribute'] == 'items.product_source' && $value['value'] == "99999"){
                $promo1212 = true;
            }
        }
        
        $rules['conditions'] = array_values($rules['conditions']);
        
        /**
         * Remove item level for condition because we already on item level
         */
        $rules['conditions'] = CartHelper::keepAttributeConditions($rules['conditions']);
        if(empty($rules['conditions'])) {
            return [$giftCardAmount,$giftCardInfo, $currentDiscount,$discountAmountGet];
        }
        
        $rules['conditions'] = CartHelper::removeAtributeCondition($rules['conditions']);
        
        $is_shipping_promo = ($discountCartAttribute[count($discountCartAttribute) - 1] == "shipping_amount") ? true : false;

        $marketingLib = new MarketingLib();
        
        /**
         * to check grandtotal in item according to it's condition
         * we loop the item separately only to count the grandtotal
         */
        $flagGrandTotalExcludeShipping = false;
        $countItemGosendApplied = 0;
        $shippingDiscountAmountGosend = 0;
        $gosendRuleId = explode(',',getenv('GOSEND_RULE_ID'));
        
        if (isset($grandTotalExcludeShipping)) {
            $this->rewind();
            while ($this->valid()) {
                $item = $this->current();
                $itemArray = $item->getDataArray();
                \Helpers\LogHelper::log('debug_625', '/app/Models/CartItem/Collection.php line 288', 'debug');
                $conditonCheckingStatus = $marketingLib->marketingConditionCheck($rules['conditions'], $itemArray);
                $matchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;
                
                if($matchCondition && !$item->getIsFreeItem()){
                    $valueRedeemRefund = $marketingLib->redeemRefundCheck($itemArray['gift_cards']);
                    if($promo1212){
                        $grandTotalExcludeShipping += (($itemArray['selling_price'] + $itemArray['handling_fee_adjust']) * $itemArray['qty_ordered']) - $itemArray['gift_cards_amount'];
                    } else if ($valueRedeemRefund > 0){
                        $grandTotalExcludeShipping += $itemArray['row_total'] - $itemArray['gift_cards_amount'] + $valueRedeemRefund;
                    } else{
                        $grandTotalExcludeShipping += $itemArray['row_total'] - $itemArray['gift_cards_amount'];
                    }
                    if(in_array($rules['rule_id'],$gosendRuleId)){
                        $countItemGosendApplied ++;
                    }
                
                }
                    
                $this->next();
            }
            
            if ($operator == '>=') {
                if ($grandTotalExcludeShipping >= $amountValue) {
                    $flagGrandTotalExcludeShipping = true;
                    if(in_array($rules['rule_id'],$gosendRuleId)){
                        $shippingDiscountAmountGosend = ceil(20000 / (($countItemGosendApplied == 0)? 1 : $countItemGosendApplied));
                    }
                }
            }
        }

        if (isset($itemsSubtotal)) {
            $this->rewind();
            while ($this->valid()) {
                $item = $this->current();
                $itemArray = $item->getDataArray();
                 \Helpers\LogHelper::log('debug_625', '/app/Models/CartItem/Collection.php line 325', 'debug');
                $conditonCheckingStatus = $marketingLib->marketingConditionCheck($rules['conditions'], $itemArray);
                $matchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;
                
                if($matchCondition && !$item->getIsFreeItem()){
                    $itemsSubtotal += $itemArray['row_total'];
                    if(in_array($rules['rule_id'],$gosendRuleId)){
                        $countItemGosendApplied ++;
                    }
                
                }
                    
                $this->next();
            }
            
            if ($operator == '>=') {
                if ($itemsSubtotal >= $amountValue) {
                    $flagGrandTotalExcludeShipping = true;
                    if(in_array($rules['rule_id'],$gosendRuleId)){
                        $shippingDiscountAmountGosend = ceil(20000 / (($countItemGosendApplied == 0)? 1 : $countItemGosendApplied));
                    }
                }
            }
        }

        $this->rewind();
        while ($this->valid()) {
            $isGetItem = false;
            $item = $this->current();
            $itemArray = $item->getDataArray();
            
            // do check free item
            if($item->getIsFreeItem() && !$is_shipping_promo) {
                $this->next();
                continue;
            }

             \Helpers\LogHelper::log('debug_625', '/app/Models/CartItem/Collection.php line 362', 'debug');
            $conditonCheckingStatus = $marketingLib->marketingConditionCheck($rules['conditions'],$item->getDataArray());
            $matchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;
            
            // Double check if grandtotal not pass the condition
            if (isset($grandTotalExcludeShipping) || isset($itemsSubtotal)) {
                //$matchCondition = ($matchCondition && $flagGrandTotalExcludeShipping && !CartHelper::isGosend($itemArray['carrier_id'])) ? true : false;
                $matchCondition = ($matchCondition && $flagGrandTotalExcludeShipping) ? true : false;
            }

            if($matchCondition) {
                $save_rule = false;

                /*
                * $originDiscountAmount : origin discount amount
                * $discountAmount : Total discount amount applied (used)
                */

                /*
                 * If not shipping discount amount apply the promo
                 * discount shipping amount per item will be counting when counting shipping fee for all items
                 *
                 * discount_step_max == 0 mean unlimited, we don't need to check it
                 */
                $finalShippingDiscount = 0;
                if(!$is_shipping_promo) {
                    $discountAmount = 0;
                    if(count($discountCartAttribute) == 1 && $discountCartAttribute[0] == 'items') {
                        $discountAttribute = "subtotal";
                    } else {
                        $discountAttribute = $discountCartAttribute[count($discountCartAttribute) - 1];
                    }

                    $amountToDiscount = $item->getValue($discountAttribute);
                    if(empty($amountToDiscount)) {
                        $amountToDiscount = $item->getSubtotal();
                    }

                    if($discountAttribute == 'subtotal') {
                        $amountToDiscount -= $item->getGiftCardsAmount();
                    }

                    // If buy something get something
                    if(substr($rules['action']['applied_action'],0,8) == 'get_item') {
                        $isGetItem = true;

                        // Do validation
                        if($item->getQtyOrdered() < $rules['action']['discount_step'] || empty($rules['action']['discount_item_qty'])) {
                            $this->next();
                            continue;
                        }

                        $itemRules = $item->getCartRules();
                        $appliedStep = 0;
                        if(!empty($itemRules) && $rules['action']['discount_step_max'] != 0) {
                            foreach ($itemRules as $rule) {
                                if($rule['rule_id'] == $rules['rule_id']) {
                                    $appliedStep = empty($rule['applied_step']) ? 1 : $rule['applied_step'];
                                    break;
                                }
                            }

                            // If total free item same with maximum allowed quit
                            if($appliedStep == $rules['action']['discount_step_max']) {
                                $this->next();
                                continue;
                            }
                        }

                        // Counting how many step this item can get
                        $totalFreeStep = 0;
                        if($rules['action']['discount_step_max'] != 0) {
                            $totalFreeStep = floor( $item->getQtyOrdered() / $rules['action']['discount_step'] );

                            /**
                             * If total free step he have is more than allowed, reset to max free allowed
                             */
                            if($totalFreeStep > $rules['action']['discount_step_max']) {
                                $totalFreeStep = $rules['action']['discount_step_max'];
                            }
                        } else {
                            $totalFreeStep = floor( $item->getQtyOrdered() / $rules['action']['discount_step'] );
                        }

                        /**
                         * Check if total applied is not more than allowed
                         */
                        if((!empty($appliedStep) && $totalFreeStep == $appliedStep) || empty($totalFreeStep)) {
                            $this->next();
                            continue;
                        }

                        // get final free step can get
                        $totalFreeQty = $totalFreeStep * $rules['action']['discount_item_qty'];
                        $rules['applied_step'] = $totalFreeStep;

                        $appliedRule[] = $rules;

                        $rules['action']['max_redemption'] = isset($rules['action']['max_redemption']) ? $rules['action']['max_redemption'] : 0;

                        $itemObj= new CartItem();

                        // Buy 1 get same item
                        if(empty($rules['action']['discount_item_sku'])) {
                            $itemArray = $item->getDataArray();
                            $do_calculation = false;
                        } else { // buy X get Y
                            // get item detail
                            $productData = ProductHelper::getFlatProductData($rules['action']['discount_item_sku']);

                            $itemArray['sku'] = $rules['action']['discount_item_sku'];
                            $itemArray['name'] = $productData['name'];
                            $itemArray['weight'] = $productData['weight'];
                            $itemArray['packaging_height'] = $productData['packaging_height'];
                            $itemArray['packaging_length'] = $productData['packaging_length'];
                            $itemArray['packaging_width'] = $productData['packaging_width'];
                            $itemArray['packaging_uom'] = $productData['packaging_uom'];
                            $itemArray['supplier_alias'] = $productData['supplier_alias'];
                            $itemArray['product_source'] = isset($productData['product_source']) ? $productData['product_source'] : "";
                            $itemArray['primary_image_url'] = $productData['primary_image_url'];

                            $amountToDiscount = \Helpers\ProductHelper::getActiveProductPrice($productData);

                            $do_calculation = true;
                        }

                        $itemArray['qty_ordered'] = $totalFreeQty;
                        $itemArray['is_free_item']= 1;
                        $itemArray['parent_id'] = $item->getId();
                        $itemArray['id'] = '';
                        $itemArray['cart_rules'] = empty($itemArray['cart_rules']) ? '' : $itemArray['cart_rules'];
                        $itemArray['discount_amount'] = 0; // free item must not get discount promotion in parent

                        /**
                         * Set data to item object
                         */
                        if($do_calculation) {
                            $itemObj->setFromArray($itemArray);
                        } else {
                            $itemObj->initialize($itemArray);
                        }

                        if ($item->getSku() == $itemObj->getSku()) {
                            $originQty = $item->getQtyOrdered();

                            $totalQty = $originQty + $itemObj->getQtyOrdered();
                            //
                            /**
                             * We need to check if the we can supply total qty (item + free item)
                             */
                            $item->setQtyOrdered($totalQty);
                            $item->checkStockAndAssignation($this->kecamatan_code, $company_code, null, $item->getCarrierId());

                            /**
                             * Finally we need to put correct qty for free amount
                             */
                            $item->setQtyOrdered($originQty);
                        } else {
                            // Is this item parent is free item, folow the eldest
                            if ($item->getIsFreeItem()) {
                                $itemObj->setParentId($item->getParentId());
                            }

                            //$itemObj->checkStockAndAssignation($this->kecamatan_code);

                            /**
                             * If free item is assign to different store, user can not buy it
                             */
                            /*if ($item->getStoreCode() != $itemObj->getStoreCode()) {
                                $item->setCanDelivery(false);
                                $item->setPickupLocationList([]);

                                $this->error_code = "RR203";
                                $this->error_messages[$item->getSku()][] = "Stok tidak cukup atau sedang kosong";
                            }*/
                        }

                        /**
                         * Now we need copy result for shipping assignation and stock result to parent
                         */
                        $itemObj->setDeliveryMethod($item->getDeliveryMethod());
                        $itemObj->setStoreCode($item->getStoreCode());
                        $itemObj->setPickupCode($item->getPickupCode());
                        $itemObj->setSupplierAlias($item->getSupplierAlias());
                        $itemObj->setSupplierAliasOriginal($item->getSupplierAliasOriginal());
                        $itemObj->setPickupLocationList($item->getPickupLocationList());
                        $itemObj->setCanDelivery($item->isCanDelivery());

                        // Finaly we neet to reset voucher amount ussage for free item
                        $itemObj->setGiftCards('[]');
                        $itemObj->setGiftCardsAmount(0);

                        $item->setIsHaveChild(true);

                        $originDiscountAmount = isset($rules['action']['discount_amount']) ? $rules['action']['discount_amount'] : 0;
                        $action = substr($rules['action']['applied_action'],9,strlen($rules['action']['applied_action']));
                        if($action == 'discount') {
                            // count discount
                            $discountAmount = 0;
                            if($rules['action']['discount_type'] == 'fix') {
                                // We need to know how much voucher used to deduct this item
                                $discountAmount =  CartHelper::countDiscountAmountUsed($amountToDiscount, $originDiscountAmount);
                            } else if ($rules['action']['discount_type'] == 'percent') {
                                $discountAmountGet = ($rules['action']['discount_amount'] / 100) * ($amountToDiscount);
                                if(!empty((int)$rules['action']['max_redemption']) && $discountAmountGet > $rules['action']['max_redemption']) {
                                    $discountAmountGet = $rules['action']['max_redemption'];
                                }
                                $originDiscountAmount = $discountAmount =  CartHelper::countDiscountAmountUsed($amountToDiscount,$discountAmountGet);
                            } else if ($rules['action']['discount_type'] == 'final_amount') {
                                $originDiscountAmount = $amountToDiscount - $rules['action']['final_amount'];
                                $discountAmount = CartHelper::countDiscountAmountUsed($amountToDiscount,$originDiscountAmount);
                            }
                            $discountAmount = round($discountAmount);
                            if($discountAttribute == 'row_total') {
                                $discountAmount = round($discountAmount/$itemObj->getQtyOrdered());
                            }
                            $itemDiscount = $item->getDiscountAmount() + $discountAmount;

                            // Make sure we don't give discount more than product price
                            $itemDiscount =  CartHelper::countDiscountAmountUsed($amountToDiscount,$itemDiscount);
                            $itemObj->setDiscountAmount($itemDiscount);

                            if($discountAttribute == 'normal_price') {
                                $itemObj->setSellingPrice($item->getNormalPrice());
                            }
                            $itemObj->countSubTotal();
                            $itemObj->countRowTotal();
                        } else {
                            $itemObj->setSellingPrice((int)$rules['action']['discount_amount']);
                        }

                        $itemObj->countSubTotal();
                        $itemObj->countRowTotal();
                        $itemObj->setCartRules($appliedRule);
                        $this->append($itemObj);

                        $currentDiscount[] = [
                            'name' => $rules['name'],
                            'discount_amount' => 0
                        ];

                        $save_rule = true;

                    } else if($rules['action']['applied_action'] == 'discount' || $rules['action']['applied_action'] == 'use_voucher' || in_array($item->getSku(),$discountItemSkuList)) { // Buy something get discount
                        $is_rule_exist = CartHelper::checkRuleIsExist($item->getCartRules(),$rules);
                        if($is_rule_exist) {
                            $this->next();
                            continue;
                        }

                        if(!property_exists($item,$discountAttribute)) {
                            return;
                        } else {
                            $discountAttributeAmount = $item->getValue($discountAttribute);
                        }

                        /**
                         * Implement max percentage for voucher ussage
                         */
                        $voucherAmountPercentage = 0;
                        $discountAmount = $rules['action']['discount_amount'];
                        if(!empty($voucher_detail['percentage']) && $voucher_detail['percentage'] > 0) {
                            $voucherAmountPercentage = round(($voucher_detail['percentage'] / 100) * $discountAttributeAmount);

                            if(empty((int)$rules['action']['discount_amount'])) {
                                $discountAmount = $voucherAmountPercentage;
                            } else if(!empty((int)$rules['action']['discount_amount']) && $voucherAmountPercentage > $rules['action']['discount_amount']) {
                                $discountAmount = $rules['action']['discount_amount'];
                            }
                        }

                        $originDiscountAmount = isset($rules['action']['discount_amount']) ? $discountAmount : 0;
                        if(!isset($rules['action']['discount_type']) || $rules['action']['discount_type'] == 'fix') {
                            // We need to know how much voucher used to deduct this item
                            $discountAmount =  CartHelper::countDiscountAmountUsed($amountToDiscount, $originDiscountAmount);
                        } else if ($rules['action']['discount_type'] == 'percent') {

                            if(!empty($voucherAmountPercentage)) {

                                $discountAmountGet = $originDiscountAmount;
                            } else {
                                $discountAmountGet = round(($originDiscountAmount / 100) * $discountAttributeAmount);
                            }
                            if(!empty((int)$rules['action']['max_redemption']) && $discountAmountGet > $rules['action']['max_redemption']) {
                                $discountAmountGet = $rules['action']['max_redemption'];
                            }
                            $originDiscountAmount = $discountAmount = CartHelper::countDiscountAmountUsed($amountToDiscount,$discountAmountGet);
                        } else if ($rules['action']['discount_type'] == 'final_amount') {
                            $originDiscountAmount = $discountAttributeAmount - $rules['action']['final_amount'];
                            $discountAmount = CartHelper::countDiscountAmountUsed($amountToDiscount,$originDiscountAmount);
                        }

                        $discountAmount = round($discountAmount);

                        if($discountAttribute == 'row_total') {
                            $discountAmount = ceil($discountAmount/$item->getQtyOrdered());
                        }

                        /**
                         * Promo Marchandise, set discount amount
                         */
                        if($discountAttribute == 'normal_price' && $rules['use_voucher'] == 0) {
                            $discountItem = 0;
                            if($rules['action']['discount_type'] == 'fix'){
                                $discountItem = (int)$rules['action']['discount_amount'];
                            }elseif($rules['action']['discount_type'] == 'percent'){
                                $discountCurrent = (($item->getNormalPrice() - $item->getSellingPrice()) / $item->getNormalPrice()) * 100;
                                $discountItem = round((($rules['action']['discount_amount'] - $discountCurrent) / 100) * $item->getNormalPrice());
                            }
                            $item->setDiscountAmount($discountItem);
                        }

                        if($discountAttribute == 'row_total' && $rules['use_voucher'] == 0){
                            $item->setDiscountAmount($discountAmount);
                        }    

                        if($discountAttribute == 'subtotal' && $rules['use_voucher'] == 0){
                            $discountAmount = round($discountAmount/$item->getQtyOrdered());
                            $item->setDiscountAmount($discountAmount);
                        }

                        if (!empty($voucher_detail)) {
                            if(!empty((int)$voucher_detail['voucher_amount']) || !empty((int)$voucher_detail['final_amount']) || !empty((int)$voucher_detail['percentage'])){
                                $itemDiscount = $item->getDiscountAmount() + $discountAmount;
                                $itemDiscount = CartHelper::countDiscountAmountUsed($amountToDiscount,$itemDiscount);
    
                                if($discountAttribute == 'selling_price') {
                                    $sellingPrice = $item->getSellingPrice() - $discountAmount;
                                    $item->setSellingPrice($sellingPrice);
                                } else {
                                    $item->setDiscountAmount($itemDiscount);
                                }
    
                                if($discountAttribute == 'normal_price') {
                                    $item->setSellingPrice($item->getNormalPrice());
                                }
                            }
                        }

                        $item->countSubTotal();
                        $item->countRowTotal();

                        $currentDiscount[] = [
                            'name' => $rules['name'],
                            'discount_amount' => $originDiscountAmount
                        ];

                        $save_rule = true;
                    } else if($rules['action']['applied_action'] == 'item_fixed') {
                        $is_rule_exist = CartHelper::checkRuleIsExist($item->getCartRules(),$rules);
                        if($is_rule_exist) {
                            $this->next();
                            continue;
                        }

                        $originDiscountAmount = intval(isset($rules['action']['discount_amount']) ? $rules['action']['discount_amount'] : 0);

                        $item->setValue($discountAttribute, $originDiscountAmount);
                        if($discountAttribute != "subtotal") {
                            $item->countSubTotal();
                        }

                        $item->countRowTotal();

                        $currentDiscount[] = [
                            'name' => $rules['name'],
                            'discount_amount' => $originDiscountAmount
                        ];

                        $save_rule = true;
                    }
                   
                    // if using voucher save voucher usage information
                    if(!empty($aggregate_rule_id)){
                        $giftCards = json_decode($item->getGiftCards(),true);
                        $aggregateVoucher = array();
                        foreach($giftCards as $keyGiftCards => $rowGiftCards){
                            if($rowGiftCards['rule_id'] == $aggregate_rule_id){
                                $aggregateVoucher = $rowGiftCards;                                
                                break;
                            }
                        }
                    }

                    if(!empty($voucher_detail) || !empty($aggregateVoucher)) {
                        $save_rule = false;
                        $giftCardAmount = $giftCardAmount + ($discountAmount * $item->getQtyOrdered());

                        $itemGiftCardAmount = $item->getGiftCardsAmount() + $discountAmount;
                        //$item->setGiftCardsAmount($itemGiftCardAmount); we do this in prorate fixer

                        if(!empty($aggregateVoucher)){
                            $itemGiftCardInfo = $giftCardInfo[] = [
                                "rule_id" => $aggregateVoucher['rule_id'],
                                "voucher_code" => $aggregateVoucher['voucher_code'],
                                "voucher_amount" => $originDiscountAmount,
                                "voucher_expired" => $aggregateVoucher['expiration_date'],
                                "voucher_amount_used" => $discountAmount * $item->getQtyOrdered(),
                                "can_combine" => $aggregateVoucher['can_combine'],
                                "global_can_combine" => $aggregateVoucher['global_can_combine'],
                                "voucher_type" => $aggregateVoucher['type'],
                                "voucher_affected" => "item",
                                "sku" => $aggregateVoucher['sku']
                            ];
                            $itemGiftCardInfo['voucher_amount_used'] = $discountAmount;

                            $currentGiftCards = json_decode($item->getGiftCards(),true);
                            
                            // Update existing giftcards
                            unset($currentGiftCards[$keyGiftCards]);
                            $item->setGiftCards(json_encode(array_merge($currentGiftCards, [$itemGiftCardInfo])));

                        }else{
                            // Voucher discount amount already counted before
                            $itemGiftCardInfo = $giftCardInfo[] = [
                                "rule_id" => $rules['rule_id'],
                                "voucher_code" => $voucher_detail['voucher_code'],
                                "voucher_amount" => $originDiscountAmount,
                                "voucher_expired" => $voucher_detail['expiration_date'],
                                "voucher_amount_used" => $discountAmount * $item->getQtyOrdered(),
                                "can_combine" => $rules['can_combine'],
                                "global_can_combine" => $rules['global_can_combine'],
                                "voucher_type" => $voucher_detail['type'],
                                "voucher_affected" => "item",
                                "sku" =>  $item->getSku()
                            ];

                            $itemGiftCardInfo['voucher_amount_used'] = $discountAmount;

                            $currentGiftCards = json_decode($item->getGiftCards(),true);
                            $item->setGiftCards(json_encode(array_merge($currentGiftCards, [$itemGiftCardInfo])));
                        }                       

                        /*
                         * If using voucher, no need to put as item discount amount
                         * So we need to deduct it what have been added before
                         * For get item no need to save the discount to item
                         */
                        if($isGetItem === false) {
                            //unset($currentDiscount[count($currentDiscount)-1]);
                            $currentDiscount = [];
                            $itemDiscount = $item->getDiscountAmount() - $discountAmount;
                            $item->setDiscountAmount($itemDiscount);
                            $item->countSubTotal();
                            $item->countRowTotal();
                        }
                    } else {
                        if(isset($rules['type'])){
                            if($rules['type'] == 'payment'){
                                $item->setDiscountAmount(0);
                                $item->countSubTotal();
                                $item->countRowTotal();
                            }
                        }
                    }
                } else if($is_shipping_promo) {
                    // If we don't have shipping amount then we do counting later
                    if(empty($item->getShippingAmount())) {
                        $rules['rule_apply'] = false;
                    } else {
                        $is_rule_exist = CartHelper::checkRuleIsExist($item->getCartRules(),$rules);
                        if($is_rule_exist) {
                            $this->next();
                            continue;
                        }

                        $rules['rule_apply'] = true;

                        // let's apply shipping rule here
                        $shippingAmount = $item->getShippingAmount() - $item->getShippingDiscountAmount();

                        /**
                         * Implement max percentage for voucher ussage
                         */
                        if(!empty($voucher_detail['percentage']) && $voucher_detail['percentage'] > 0) {
                            $voucherAmountPercentage = round(($voucher_detail['percentage'] / 100) * $shippingAmount);
                            if($voucherAmountPercentage < $rules['action']['discount_amount']) {
                                $rules['action']['discount_amount'] = $voucherAmountPercentage;
                            }
                        }

                        if($rules['action']['discount_type'] == 'percent') {
                            $discountAmountGet = $shippingAmount * ($rules['action']['discount_amount'] / 100);
                            if(!empty($rules['action']['max_redemption']) && $discountAmountGet > $rules['action']['max_redemption']) {
                                $discountAmountGet = $rules['action']['max_redemption'];
                            }

                            $originDiscountAmount = $discountShippingAmount =  CartHelper::countDiscountAmountUsed($shippingAmount,$discountAmountGet);
                        } else if ($rules['action']['discount_type'] == 'final_amount') {
                            $originDiscountAmount = $shippingAmount - $rules['action']['final_amount'];
                            $discountShippingAmount = CartHelper::countDiscountAmountUsed($shippingAmount,$originDiscountAmount);
                        } else {
                            $originDiscountAmount = $rules['action']['discount_amount'];
                            $discountShippingAmount =  CartHelper::countDiscountAmountUsed($shippingAmount, $originDiscountAmount);
                        }

                        $finalShippingDiscount =  $item->getShippingDiscountAmount() + $discountShippingAmount;
                        
                        // count final shipping discount amount gosend per item
                        if($shippingDiscountAmountGosend > 0 && !$item->getIsFreeItem()){
                            $finalShippingDiscount = $shippingDiscountAmountGosend;
                        }
                        
                        if($finalShippingDiscount > $item->getShippingAmount()){
                            $finalShippingDiscount = $item->getShippingAmount();
                        }

                        $item->setShippingDiscountAmount($finalShippingDiscount);
                    }

                    if(!empty($voucher_detail)) {
                        $save_rule = false;
                        $rules['voucher'] = $voucher_detail; // save voucher info in rules
                        $giftCardInfo[] = [
                            "rule_id" => $rules['rule_id'],
                            "voucher_code" => $voucher_detail['voucher_code'],
                            "voucher_amount" => $originDiscountAmount,
                            "voucher_expired" => $voucher_detail['expiration_date'],
                            "voucher_amount_used" => $finalShippingDiscount,
                            "can_combine" => $rules['can_combine'],
                            "global_can_combine" => $rules['global_can_combine'],
                            "voucher_type" => $voucher_detail['type'],
                            "voucher_affected" => "shipping",
                            "sku" =>  $item->getSku()
                        ];
                    }
                }

                // Validate if current rule already add in this item
                if($save_rule) {
                    $is_rule_exist = CartHelper::checkRuleIsExist($item->getCartRules(),$rules);

                    if(!$is_rule_exist) {
                        $cartRules = $item->getCartRules();
                        $currentRule[0] = $rules;
                        $cartRules = array_merge($cartRules,$currentRule);
                        $item->setCartRules($cartRules);
                        
                        // Set price modified
                        $item->setPriceModifed('marketing_promo');
                        $modifiedDescription = [];
                        foreach($cartRules as $row){
                            $modifiedDescription[] = $row['rule_id']." - ".substr($row['name'],0,50);
                        }
                        $modifiedDescription = implode('<br>',$modifiedDescription);
                        $item->setModifiedDescription(substr($modifiedDescription,0,200));
                    }
                }
            }

            $this->next();
        }

        return [$giftCardAmount,$giftCardInfo, $currentDiscount, $discountAmountGet];
    }

    public function countShippingDiscountAmount()
    {
        $this->rewind();
        $shippingDiscountAmount = 0;
        while ($this->valid()) {
            $item = $this->current();
            $shippingDiscountAmount += $item->getShippingDiscountAmount();
            $this->next();
        }
        return $shippingDiscountAmount;
    }

    public function countShippingAmount()
    {
        $this->rewind();
        $shippingAmount = 0;
        while ($this->valid()) {
            $item = $this->current();
            $itemArray = $item->getDataArray();
            if (!CartHelper::isGosend($itemArray['carrier_id'])) {
                $shippingAmount += $item->getShippingAmount() * $item->getQtyOrdered();
            } else {
                $shippingAmount += $item->getShippingAmount();
            }

            $this->next();
        }
        return $shippingAmount;
    }

    /*
     * count for row total, total qty ordered and discount amount
     */
    public function countTotal()
    {
        $this->rewind();
        $totalQty = 0;
        $rowTotal = 0;
        $rowTotalWithoutHandling = 0;
        while ($this->valid()) {
            $item = $this->current();
            $totalQty += $item->getQtyOrdered();
            $rowTotal += $item->getRowTotal();
            $rowTotalWithoutHandling += $item->countRowTotalWithoutHandling();
            $this->next();
        }
        return [$totalQty, $rowTotal, $rowTotalWithoutHandling];
    }

    public function distributeShippingAmount($shipping_amount = 0, $items = array())
    {
        // Count total weight
        $totalWeight = 0;
        foreach ($items as $item) {
            $totalPerItem = CartHelper::countWeight([$item]);
            $totalWeight += $totalPerItem['shipping'];
        }

        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $itemArray = $item->getDataArray();

            foreach ($items as $thisItem) {
                if($thisItem['sku'] === $itemArray['sku']) {
                    $itemWeight = CartHelper::countWeight([$itemArray]);
                    $item_shipping_amount = ($itemWeight['shipping'] / $totalWeight) * $shipping_amount;

                    // Set to item in qty atomic
                    $item->setShippingAmount($item_shipping_amount / $thisItem['qty_ordered']);

                    if(!empty($item->getShippingDiscountAmount()) && $item->getShippingDiscountAmount() > $item->getShippingAmount()) {
                        $item->setShippingDiscountAmount($item->getShippingAmount());
                    }
                }
            }

            $this->next();
        }
    }

    public function assignShippingAmountToItem($itemParams = array(), $isFreeItem = 0)
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $itemArray = $item->getDataArray();
            if($itemParams['sku'] === $itemArray['sku'] && ($itemArray['is_free_item'] == $isFreeItem)) {
                if (isset($itemParams['shipping_amount'])) {
                    $item->setShippingAmount($itemParams['shipping_amount']);
                }
                
                $item->setCarrierId($itemParams['carrier_id']);

                $itemArray['shipping_address']['carrier_id'] = $itemParams['carrier_id'];
                $item->setShippingAddress($itemArray['shipping_address']);

                break;
            }

            $this->next();
        }
    }

    public function getGroupShipmentBySKU($sku = "")
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $itemArray = $item->getDataArray();
            if($itemArray['sku'] === $sku) {
                return $item->getGroupShipment();
            }

            $this->next();
        }
    }

    /**
     * This function still have bug in it, we need to fix it for promotion with no free shipping
     * @param int $shipping_discount_amount
     * @param array $items
     */
    public function distributeShippingDiscountAmount($shipping_discount_amount = 0, $items = array())
    {
        // Count total weight
        $totalWeight = 0;
        $shippingAmount = 0;
        foreach ($items as $item) {
            if($item['delivery_method'] == 'pickup') {
                continue;
            }

            $totalPerItem = CartHelper::countWeight([$item]);
            $totalWeight += $totalPerItem['original'];
            $shippingAmount += $item['shipping_amount'] * $item['qty_ordered'];
        }

        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $itemArray = $item->getDataArray();
            foreach ($items as $thisItem) {
                if($thisItem['delivery_method'] == 'pickup') {
                    continue;
                }

                if($thisItem['sku'] === $itemArray['sku']) {

                    if($shippingAmount == $shipping_discount_amount) {
                        $finalShipping = $thisItem['shipping_amount'];
                    } else {
                        $itemWeight = CartHelper::countWeight([$itemArray]);
                        $shippingDiscountAmountItem = ($itemWeight['original'] / $totalWeight) * $shipping_discount_amount;
                        $shippingDiscountAmountUnit = $shippingDiscountAmountItem / $thisItem['qty_ordered'];

                        $finalShipping = \Helpers\CartHelper::countDiscountAmountUsed($itemArray['shipping_amount'],$shippingDiscountAmountUnit);

                        if($finalShipping  < 0 ) {
                            $finalShipping  = $itemArray['shipping_amount'];
                        }
                    }

                    // Set to item in qty atomic
                    $item->setShippingDiscountAmount($finalShipping);

                }
            }

            $this->next();
        }
    }

    public function distributeFullShippingDiscountAmount()
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $item->setShippingDiscountAmount($item->getShippingAmount());
            $this->next();
        }
    }

    public function sortSkuByField($order = "asc",$field = "row_total")
    {
        $this->rewind();
        $sortedItems = array();
        while ($this->valid()) {
            $item = $this->current();
            if($item->getIsFreeItem() == 0) {
                $value = $item->getValue($field);
                $sortedItems[$value] = $item->getSku();
            }

            $this->next();
        }

        if($order == "asc") {
            ksort($sortedItems);
        } else if($order == "desc") {
            krsort($sortedItems);
        }

        // change to int key
        $finalSortedItems = array();
        foreach ($sortedItems as $key => $val) {
            $finalSortedItems[] = $val;
        }


        return $finalSortedItems;
    }

    public function checkRestrictedEvent($customer_id = 0)
    {
        if (empty($customer_id)) {
            return;
        }

        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $item->checkRestrictedEvent($customer_id);

            $this->next();
        }
    }

    public function checkStockAndAssignation($company_code = 'ODI', $customer = null)
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            if ($item->getDeliveryMethod() == 'pickup') {
                $item->setCarrierId('');
            }
            
            $itemShippingAddress = $item->getShippingAddress();
            if (is_object($itemShippingAddress)) {
                $itemShippingAddress = $itemShippingAddress->getDataArray();
            }
            
            if (!$item->isRestrictedEvent()) {
                $this->kecamatan_code = (isset($itemShippingAddress['kecamatan']['kecamatan_code'])) ? $itemShippingAddress['kecamatan']['kecamatan_code'] : $this->kecamatan_code;
                $item->checkStockAndAssignation($this->kecamatan_code, $company_code, $customer, $item->getCarrierId());
                if(!empty($item->getErrorMessages())) {
                    $this->error_code = $item->getErrorCode();
                    $this->sku = $item->getSku();
                    $this->error_messages = $item->getErrorMessages();
                }
            }

            $this->next();
        }
    }

    public function assignLazadaItem()
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $item->assignLazadaItem();

            $this->next();
        }
    }

    public function removePromotions($rule_id = 0, $voucher_code = "")
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();

            $cartRuleApplied = [];
            $used_gift_card = [];
            if(!empty($item->getCartRules())) {
                $cartRuleApplied = $item->getCartRules();
                foreach ($cartRuleApplied as $key => $card_info) {
                    if(strtolower($card_info['rule_id']) == strtolower($rule_id)) {
                        unset($cartRuleApplied[$key]);
                        $used_gift_card = $card_info;
                        break;
                    }
                }
            }

            /**
             * Remove gift card ussage and amount
             */
            if(!empty($item->getGiftCardsAmount())) {
                $giftCardInfo = json_decode($item->getGiftCards(),true);
                foreach ($giftCardInfo as $key => $card_info) {
                    if(strtolower($card_info['voucher_code']) == strtolower($voucher_code)) {
                        $finalAmount = $item->getGiftCardsAmount() - $card_info['voucher_amount_used'];
                        $item->setGiftCardsAmount($finalAmount);
                        unset($giftCardInfo[$key]);
                    }
                }

                $item->setGiftCards(json_encode($giftCardInfo));
            }

            if(empty($used_gift_card)) {
                $this->next();
                continue;
            }

            if(empty($rules['action']['attributes'])) {
                $discountCartAttribute = ['items','selling_price'];
            } else {
                $discountCartAttribute = explode(".",$rules['action']['attributes']);
            }
            $is_shipping_promo = ($discountCartAttribute[count($discountCartAttribute) - 1] == "shipping_amount") ? true : false;

            if(substr($used_gift_card['action']['applied_action'],0,8) == 'get_item' && $item->getIsFreeItem() == 1) {
                $this->offsetUnset( $this->key());
                $this->next();
            } else if($is_shipping_promo) {
                $shippingDiscount = $item->getShippingDiscountAmount() - $used_gift_card['voucher_amount_used'];
                $item->setShippingDiscountAmount($shippingDiscount);
            }

            $item->setCartRules($cartRuleApplied);

            $this->next();
        }

        return;
    }

    public function resetShippingRate()
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $item->setShippingAmount(0);
            $item->setShippingDiscountAmount(0);
            $this->next();
        }
    }

    public function countGiftCardAmount()
    {
        $gift_card_amount = 0;
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $gift_card_amount += ($item->getGiftCardsAmount() * $item->getQtyOrdered());
            $this->next();
        }

        return $gift_card_amount;
    }

    public function countItemGiftCard($voucher_code = "")
    {
        if(empty($voucher_code)) {
            return 0;
        }

        $gift_card_amount = 0;
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();

            $currentGiftCards = json_decode($item->getGiftCards(),true);
            foreach ($currentGiftCards as $gc) {
                if($gc['voucher_code'] == $voucher_code) {
                    $gift_card_amount += $gc['voucher_amount_used'] * $item->getQtyOrdered();
                    break;
                }
            }

            $this->next();
        }

        return $gift_card_amount;

    }

    public function clearGiftCard()
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();
            $item->setGiftCards('[]');
            $item->setGiftCardsAmount(0);
            $this->next();
        }
    }

    /**
     * Prorate gift card to item, item that will be proreated only item is eligible to get gift card
     * @param $gift_card_info
     * @param array $shopping_cart
     * @param array $rules
     */
    public function prorateGiftCard($gift_card_info, $shopping_cart = [], $rules = [], $discount_attribute = 'subtotal')
    {
        $rules['conditions'] = CartHelper::keepAttributeConditions($rules['conditions']);
        $rules['conditions'] = CartHelper::removeAtributeCondition($rules['conditions']);

        $subtotalWithHandling = $shopping_cart['subtotal'] + $shopping_cart['handling_fee_adjust'];
        $grand_total = TransactionHelper::countTotal($subtotalWithHandling, $shopping_cart['discount_amount'], $shopping_cart['shipping_amount'],$shopping_cart['shipping_discount_amount'], $shopping_cart['gift_cards_amount']);
        $originGiftCardAmount = intval($gift_card_info['voucher_amount_used']);

        // If voucher amount make grand total = 0, prorate to all item
        if($grand_total <= 0) {
            $this->rewind();
            while ($this->valid()) {
                $item = $this->current();

                $itemRowTotalWithShipping = $item->getRowTotal() + (($item->getShippingAmount() - $item->getShippingDiscountAmount()) * $item->getQtyOrdered());
                $giftCardAmount = $itemRowTotalWithShipping;
                $finalGiftCardAmount = $giftCardAmount / $item->getQtyOrdered();
                $giftCardAmountGet = $finalGiftCardAmount - $item->getGiftCardsAmount();

                $itemGiftCardInfo = $gift_card_info;
                $itemGiftCardInfo['voucher_amount_used'] = $giftCardAmountGet;

                $currentGiftCards = json_decode($item->getGiftCards(),true);
                $item->setGiftCards(json_encode(array_merge($currentGiftCards, [$itemGiftCardInfo])));
                $item->setGiftCardsAmount($finalGiftCardAmount);

                $this->next();
            }
        } else {
            $marketingLib = new Marketing();
            $eligibleItems = [];
            $totalEligibleAmount = 0;

            /**
             * Before we prorate, we need to know which item is eligible to get this promotion
             */
            $this->rewind();
            while ($this->valid()) {
                $item = $this->current();

                // do check free item
                $refundVoucher = explode(',', getenv('REFUND_RULE_ID'));
                if($item->getIsFreeItem() && !in_array($rules['rule_id'],$refundVoucher)) {
                    $this->next();
                    continue;
                }

                 \Helpers\LogHelper::log('debug_625', '/app/Models/CartItem/Collection.php line 1363', 'debug');
                $conditonCheckingStatus = $marketingLib->marketingConditionCheck($rules['conditions'],$item->getDataArray());;
                $matchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;

                if($matchCondition) {
                    $eligibleItems[] = $item->getId();
                    $subtotalItemWithHandling = intval($item->getRowTotal() - ($item->getGiftCardsAmount() * $item->getQtyOrdered()));

                    if($discount_attribute == 'grand_total') {
                        $shippingAmount = ($item->getShippingAmount() - $item->getShippingDiscountAmount()) * $item->getQtyOrdered();
                        $totalEligibleAmount += ceil($subtotalItemWithHandling + $shippingAmount);
                    } else if ($discount_attribute == 'subtotal' || $discount_attribute == 'subtotal_bundling') {
                        $totalEligibleAmount += $subtotalItemWithHandling;
                    }

                }

                $this->next();
            }

            if(!empty($eligibleItems)) {
                /**
                 * Before we prorate gift card, there are a posibility that total amount of eligible item is less then current gift card amount
                 * if that happened we need to change gift card amount to maksimum amount that customer can get
                 */
                if($totalEligibleAmount < $originGiftCardAmount && count($eligibleItems) < $this->count()) {
                    $originGiftCardAmount = $totalEligibleAmount;
                }

                if (empty($totalEligibleAmount)) {
                    return;
                }

                $this->rewind();
                $totalAppliedGcAmount = 0;
                $eligbleCount = 0;
                while ($this->valid()) {
                    $item = $this->current();

                    if(in_array($item->getId(),$eligibleItems)) {
                        $eligbleCount++;

                        $subtotalItemWithHandling = intval($item->getRowTotal() - ($item->getGiftCardsAmount() * $item->getQtyOrdered()));
                        if($discount_attribute == 'grand_total') {
                            $shippingAmount = ($item->getShippingAmount() - $item->getShippingDiscountAmount()) * $item->getQtyOrdered();
                            $itemAmount = ceil($subtotalItemWithHandling + $shippingAmount);
                        } else if ($discount_attribute == 'subtotal' || $discount_attribute == 'subtotal_bundling') {
                            $itemAmount = $subtotalItemWithHandling;
                        }

                        $giftCardAmount = round(($itemAmount / $totalEligibleAmount) * $originGiftCardAmount);

                        $totalAppliedGcAmount += $giftCardAmount;
                        if(count($eligibleItems) == $eligbleCount && $totalAppliedGcAmount != $originGiftCardAmount) {
                            $giftCardAmount = $originGiftCardAmount - ($totalAppliedGcAmount - $giftCardAmount);
                        }

                        $giftCardAmountGet = $giftCardAmount / $item->getQtyOrdered();
                        $finalGiftCardAmount = $giftCardAmountGet + $item->getGiftCardsAmount();

                        if($finalGiftCardAmount > ($item->getRowTotal() + ($item->getShippingAmount() + $item->getQtyOrdered()))) {
                            $giftCardAmountGet = $finalGiftCardAmount - $item->getGiftCardsAmount();
                            $finalGiftCardAmount = $itemAmount;
                        }

                        $itemGiftCardInfo = $gift_card_info;
                        $itemGiftCardInfo['voucher_amount_used'] = $giftCardAmountGet;

                        $currentGiftCards = json_decode($item->getGiftCards());
                        $item->setGiftCards(json_encode(array_merge($currentGiftCards, [$itemGiftCardInfo])));
                        $item->setGiftCardsAmount($finalGiftCardAmount);
                    }

                    $this->next();
                }
            }
        }
    }
    
    public function splitInvoiceShippingPromotion($conditions = [])
    {
        $marketingLib = new Marketing();
        $eligibleItems = [];

        // Remove item from this condition
        $conditions = CartHelper::removeAtributeCondition($conditions);

        /**
         * Grouping item into 2 group
         * One group is eligible for promotion, other is non eligible items
         */
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();

            // do check free item
            if($item->getIsFreeItem()) {
                $this->next();
                continue;
            }

             \Helpers\LogHelper::log('debug_625', '/app/Models/CartItem/Collection.php line 1464', 'debug');
            $conditonCheckingStatus = $marketingLib->marketingConditionCheck($conditions,$item->getDataArray());;
            $matchCondition = isset($conditonCheckingStatus[0]) ? $conditonCheckingStatus[0] : false;

            if($matchCondition) {
                $eligibleItems[] = $item->getId();
            }

            $this->next();
        }

        /**
         * Now from this thow group we need to split into invoice
         */
        $this->rewind();
        $view = array();
        $originShippingRegion = array();
        $freeShippingInvoice = [];
        $nonFreeShippingInvoice = [];
        while ($this->valid()) {
            $idx = $this->key();
            $item = $this->current();

            $delivery_method = $item->getDeliveryMethod();
            $currentOriginShippingRegion = $item->getOriginShippingRegion();
            if(!empty($currentOriginShippingRegion) && $delivery_method != "pickup") {
                $originShippingRegion[] = $currentOriginShippingRegion;
            }

            $delivery_method = $item->getDeliveryMethod();
            $itemArray = $item->getDataArray();
            $itemArray['store_code'] = isset($itemArray['store_code'])? $itemArray['store_code'] : "";

            if(in_array($item->getId(),$eligibleItems)) {
                if($delivery_method != 'pickup') {
                    $freeShippingInvoice[$itemArray['supplier_alias']][$itemArray['store_code']][$delivery_method]['origin_shipping_region'] = $currentOriginShippingRegion;
                }

                $freeShippingInvoice[$itemArray['supplier_alias']][$itemArray['store_code']][$delivery_method][$idx] = $itemArray;
            } else {
                if($delivery_method != 'pickup') {
                    $nonFreeShippingInvoice[$itemArray['supplier_alias']][$itemArray['store_code']][$delivery_method]['origin_shipping_region'] = $currentOriginShippingRegion;
                }

                $nonFreeShippingInvoice[$itemArray['supplier_alias']][$itemArray['store_code']][$delivery_method][$idx] = $itemArray;
            }


            $this->next();
        }

        $view['freeShippingInvoice'] = $freeShippingInvoice;
        $view['nonFreeShippingInvoice'] = $nonFreeShippingInvoice;
        $view['origin_shipping_region'] = array_unique($originShippingRegion);

        return $view;
    }

    public function markCanNotBuy($skus = [], $flag = '')
    {
        $this->rewind();
        while ($this->valid()) {
            $item = $this->current();

            if (!empty($skus)) {
                if (!in_array($item->getSku(), $skus)) {
                    $this->next();
                    continue;
                }
            }

            $item->setCanDelivery(false);
            $item->setPickupLocationList([]);

            $this->sku = $item->getSku();
            if($flag == 'bogo'){
                $this->error_code = "RR207";
                $this->error_messages = "Stok free item tidak cukup atau sedang kosong";
            }else{
                $this->error_code = "RR203";
                $this->error_messages = "Stok tidak cukup atau sedang kosong";
            }
            $this->next();
        }
    }

    public function itemsValidation($customer_id = 0, $company_code = 'ODI')
    {
        $this->rewind();
        $items = [];
        while ($this->valid()) {
            $item = $this->current();

            if($item->getIsFreeItem() == 0) {
                $items[$item->getId()] = $item; // Store non free item
                $item->itemValidation($company_code);
            } else {
                /**
                 * @var $parentItem \Models\CartItem
                 */
                $parentItem = $items[$item->getParentId()];
                if ($item->getSku() == $parentItem->getSku()) {
                    $originQty = $parentItem->getQtyOrdered();

                    $totalQty = $originQty + $item->getQtyOrdered();

                    /**
                     * We need to check if the we can supply total qty (item + free item)
                     */
                    $parentItem->setQtyOrdered($totalQty);
                    $parentItem->itemValidation($company_code);

                    /**
                     * Finally we need to put correct qty for free amount
                     */
                    $parentItem->setQtyOrdered($originQty);
                }

                /**
                 * Now we need copy result for shipping assignation and stock result to parent
                 */
                $item->setDeliveryMethod($parentItem->getDeliveryMethod());
                $item->setStoreCode($parentItem->getStoreCode());
                $item->setPickupCode($parentItem->getPickupCode());
                $item->setSupplierAlias($parentItem->getSupplierAlias());
                $item->setSupplierAliasOriginal($parentItem->getSupplierAliasOriginal());
                $item->setPickupLocationList($parentItem->getPickupLocationList());
                $item->setCanDelivery($parentItem->isCanDelivery());

                // If normal price is less or zero, don't let people buy it
                if ($item->getNormalPrice() <= 0) {
                    $item->setCanDelivery(false);
                    $item->setPickupLocationList([]);
                }
            }

            $item->checkRestrictedEvent($customer_id, $company_code);

            if ($item->isRestrictedEvent()) {
                $this->error_code = "RR203";
                $this->error_messages['type'] = "sku";  
                $this->error_messages['value'] = $item->getSku();  
                $this->error_messages['message'] = "Pembelian terbatas. Setiap akun terdaftar hanya diperbolehkan melakukan satu (1) kali transaksi untuk satu (1) produk Flash Sale";
            }

            if (!empty($item->getErrorMessages()) && !isset($this->error_messages[$item->getSku()])) {
                $this->error_messages = $item->getErrorMessages();
            }

            $this->next();
        }
    }

    public function getBOGOMixSku()
    {
        $this->rewind();
        $skuList = [];
        while ($this->valid()) {
            $item = $this->current();

            if ($item->isIsHaveChild() == true && empty($item->getParentId())) {
                $skuList[$item->getId()][] = ['sku' => $item->getSku(), 'qty' => $item->getQtyOrdered()];
            } else if (!empty($item->getParentId())) {
                $skuList[$item->getParentId()][] = ['sku' => $item->getSku(), 'qty' => $item->getQtyOrdered()];
            }

            $this->next();
        }

        return $skuList;
    }

    public function assignBOGOMixSku($skus = [], $store_list = [], $company_code = 'ODI', $assignation = [])
    {
        $this->rewind();
        $items = [];
        while ($this->valid()) {
            $item = $this->current();

            $shippingAddressArray = $item->getShippingAddress(); 
            if(gettype($shippingAddressArray) == 'object') {
                $shippingAddressArray = $shippingAddressArray->getDataArray();
            }

            $kecamatan_code = (isset($shippingAddressArray['kecamatan']['kecamatan_code'])) ? $shippingAddressArray['kecamatan']['kecamatan_code'] : ''; 
            if (in_array($item->getSku(), $skus)) {
                if (empty($store_list)) {
                    $item->setCanDelivery(false);
                    $item->setPickupLocationList([]);
                } else {
                    if ($item->getParentId() == 0) {
                        if ($item->getDeliveryMethod() == 'delivery' || $item->getDeliveryMethod() == 'store_fulfillment') {
                            $item->shippingAssignation($kecamatan_code, $store_list, $company_code, $item->getCarrierId());
                            $item->preparePickupLocationList($store_list);
                        } else if ($item->getDeliveryMethod() == 'pickup') {
                            $item->pickupAssignation($store_list,$kecamatan_code);

                            if (count($assignation) > 0) {
                                foreach($assignation as $rowAssignation) {
                                    if (isset($store_list[$rowAssignation])) {
                                        $item->setCanDelivery(true);
                                        break;
                                    }
                                }
                            }
                        }

                        $items[$item->getId()] = $item;
                    } else {
                        /**
                         * @var $parentItem \Models\CartItem
                         */
                        $parentItem = $items[$item->getParentId()];
                        $item->setDeliveryMethod($parentItem->getDeliveryMethod());
                        $item->setStoreCode($parentItem->getStoreCode());
                        $item->setPickupCode($parentItem->getPickupCode());
                        $item->setSupplierAlias($parentItem->getSupplierAlias());
                        $item->setSupplierAliasOriginal($parentItem->getSupplierAliasOriginal());
                        $item->setPickupLocationList($parentItem->getPickupLocationList());
                        $item->setCanDelivery($parentItem->isCanDelivery());
                        $item->setCarrierId($parentItem->getCarrierId());  
                    }
                }
            }

            $this->next();
        }
    }

    public function defineGroupShipment($groupShipmentPerSKU = array())
    {
        $this->rewind();
        $items = [];
        while ($this->valid()) {
            $item = $this->current();
            $item->setGroupShipment($groupShipmentPerSKU[$item->getSku()]);
            
            // remove shipping addres on pickup items
            if ($item->getDeliveryMethod() == 'pickup') {
                $item->setShippingAddress(array());
            }

            $this->next();
        }
    }

    // used if voucher redeem > voucher value
    public function prorateFixer($voucher_detail)
    {
        $rule_id["rule_id"] = $voucher_detail["rule_id"];
        $strRule = str_replace(array("{","}"),"",json_encode($rule_id));//json_encode($rule_id);

        $this->rewind();
        $grandRowTotal = 0;
        while ($this->valid()) {
            $item = $this->current();
            $itemArray = $item->getDataArray();
            $rules = $itemArray['gift_cards'];

            if(strpos($rules, $strRule)){
                $grandRowTotal += $itemArray['row_total'];
            }

            $this->next();
        }

        $actualGiftCardAmount = round($voucher_detail['voucher_amount']);
        $finalAmount = round($voucher_detail['final_amount']);
        
        $totalGiftCardAmount = 0;
        if($actualGiftCardAmount > $grandRowTotal){
            $actualGiftCardAmount = $grandRowTotal;
        }        
        $lastSku="";
        $this->rewind();        
        while ($this->valid()) {
            $item = $this->current();
            $itemArray = $item->getDataArray();
            $rules = $itemArray['gift_cards'];

            if(strpos($rules, $strRule)){
                $prorate = ($itemArray['row_total']/$grandRowTotal)*$actualGiftCardAmount/$itemArray['qty_ordered'];
                $currentGiftCardAmount = $item->getGiftCardsAmount();
                $item->setGiftCardsAmount($currentGiftCardAmount + round($prorate));
                $totalGiftCardAmount += (round($prorate) * $itemArray['qty_ordered']);
                $lastSku = $itemArray['sku'];

                // hf for final amount
                if($finalAmount > 0 ){
                    $actualGiftCardAmount = $item->getSellingPrice() - $finalAmount;
                    $item->setGiftCardsAmount($actualGiftCardAmount);
                }
            }

            $this->next();
        }

        // if the voucher is not final amount, we must do this 
        if($finalAmount == 0){                    
            $this->rewind();
            while ($this->valid()) {
                $item = $this->current();
                $itemArray = $item->getDataArray();
                $rules = $itemArray['gift_cards'];
                $sku = $itemArray['sku'];
    
                if($sku == $lastSku){
                    $selisih = 0;
                    $prorate = $itemArray['gift_cards_amount'];
                    if($totalGiftCardAmount>$actualGiftCardAmount){                   
                        $selisih = $totalGiftCardAmount - $actualGiftCardAmount;
                        $prorate = $prorate - $selisih;
                    }elseif($totalGiftCardAmount<$actualGiftCardAmount){
                        $selisih = $actualGiftCardAmount - $totalGiftCardAmount;
                        $prorate = $prorate + $selisih;
                    }
    
                    $item->setGiftCardsAmount($prorate);
                                                    
                }
    
                $this->next();
            }       
        }        
    }
}