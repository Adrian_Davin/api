<?php
namespace Models;

class MarketingAdminRule extends \Models\BaseModel
{

    /**
     *
     * @var integer
     */
    protected $rule_id;

    /**
     *
     * @var integer
     */
    protected $role_id;

    /**
     *
     * @var string
     */
    protected $resource_id;

    /**
     * Method to set the value of field rule_id
     *
     * @param integer $rule_id
     * @return $this
     */
    public function setRuleId($rule_id)
    {
        $this->rule_id = $rule_id;

        return $this;
    }

    /**
     * Method to set the value of field role_id
     *
     * @param integer $role_id
     * @return $this
     */
    public function setRoleId($role_id)
    {
        $this->role_id = $role_id;

        return $this;
    }

    /**
     * Method to set the value of field resource_id
     *
     * @param string $resource_id
     * @return $this
     */
    public function setResourceId($resource_id)
    {
        $this->resource_id = $resource_id;

        return $this;
    }

    /**
     * Returns the value of field rule_id
     *
     * @return integer
     */
    public function getRuleId()
    {
        return $this->rule_id;
    }

    /**
     * Returns the value of field role_id
     *
     * @return integer
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * Returns the value of field resource_id
     *
     * @return string
     */
    public function getResourceId()
    {
        return $this->resource_id;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'marketing_admin_rule';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MarketingAdminRule[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MarketingAdminRule
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
