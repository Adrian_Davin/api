<?php

namespace Models;

class SalesCustomer extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $sales_customer_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    protected $sales_order_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $customer_id;

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $customer_firstname = '';

    /**
     *
     * @var string
     * @Column(type="string", length=35, nullable=true)
     */
    protected $customer_lastname = '';

    /**
     *
     * @var string
     * @Column(type="string", length=128, nullable=true)
     */
    protected $customer_email = '';

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $customer_is_guest = 1;
    protected $customer_is_new = 0;
    protected $customer_type;
    protected $order_type;
    protected $company_id;
    protected $customer_phone;
    protected $customer_group = [0];
    protected $registered_by;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('sales_customer_id', 'Models\SalesOrder', 'sales_customer_id', array('alias' => 'SalesOrder'));
        $this->belongsTo('customer_id', 'Models\Customer', 'customer_id', array('alias' => 'Customer'));
        $this->belongsTo('sales_order_id', 'Models\SalesOrder', 'sales_order_id', array('alias' => 'SalesOrder'));
        $this->hasMany('customer_id', 'Models\Customer2Group', 'customer_id', array('alias' => 'Customer2Group'));
        $this->hasOne('customer_id', 'Models\CustomerB2bCompany', 'customer_id', array('alias' => 'CustomerB2bCompany'));
    }

    /**
     * @return mixed
     */
    public function getCustomerPhone()
    {
        return $this->customer_phone;
    }

    /**
     * @param mixed $customer_phone
     */
    public function setCustomerPhone($customer_phone)
    {
        $this->customer_phone = $customer_phone;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sales_customer';
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->sales_order_id;
    }

    /**
     * @param int $sales_order_id
     */
    public function setSalesOrderId($sales_order_id)
    {
        $this->sales_order_id = $sales_order_id;
    }

    /**
     * @return string
     */
    public function getCustomerFirstname()
    {
        return $this->customer_firstname;
    }

    /**
     * @param string $customer_firstname
     */
    public function setCustomerFirstname($customer_firstname)
    {
        $this->customer_firstname = $customer_firstname;
    }

    /**
     * @return string
     */
    public function getCustomerLastname()
    {
        return $this->customer_lastname;
    }

    /**
     * @param string $customer_lastname
     */
    public function setCustomerLastname($customer_lastname)
    {
        $this->customer_lastname = $customer_lastname;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    /**
     * @param string $customer_email
     */
    public function setCustomerEmail($customer_email)
    {
        $this->customer_email = $customer_email;
    }

    /**
     * @return int
     */
    public function getCustomerIsGuest()
    {
        return $this->customer_is_guest;
    }

    /**
     * @param int $customer_is_guest
     */
    public function setCustomerIsGuest($customer_is_guest)
    {
        $this->customer_is_guest = (int)$customer_is_guest;
    }

    /**
     * @return array
     */
    public function getCustomerGroup()
    {
        return $this->customer_group;
    }

    /**
     * @param array $customer_group
     */
    public function setCustomerGroup($customer_group)
    {
        $this->customer_group = $customer_group;
    }

    /**
     * @return string
     */
    public function getCustomerType()
    {
        return $this->customer_type;
    }

    /**
     * @param string $customer_type
     */
    public function setCustomerType($customer_type)
    {
        $this->customer_type = $customer_type;
    }

    /**
     * @return mixed
     */
    public function getOrderType()
    {
        return $this->order_type;
    }

    /**
     * @param mixed $customer_order_type
     */
    public function setOrderType($order_type)
    {
        $this->order_type = $order_type;
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SalesCustomer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function afterUpdate() {
        // this is just overriding the afterUpdate lifecycle because this could be the slowest process after all.
        // Why? Every update sequence, BaseModel will check redis using very huge amount of iterators
        \Helpers\LogHelper::log("after_update_sales_customer", "TRIGGERED: " .json_encode($this->toArray()));
    }

    public function setFromArray($data_array = array())
    {
        $this->assign($data_array);

        if(isset($data_array['customer_group'])) {
            $this->customer_group = $data_array['customer_group'];
        }
        if(isset($data_array['customer_phone'])) {
            $this->customer_phone = $data_array['customer_phone'];
        }
        if(isset($data_array['customer_is_new'])) {
            $this->customer_is_new = $data_array['customer_is_new'];
        }
        
        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$data_array));
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {
        $view = $this->toArray($columns, $showEmpty);
        unset($view['sales_order_id']);
        $view['customer_firstname'] = (empty($this->customer_firstname)) ? '' : $this->customer_firstname;
        $view['customer_lastname'] = (empty($this->customer_lastname)) ? '' : $this->customer_lastname;
        $view['customer_group'] = $this->customer_group;
        $view['customer_type'] = $this->customer_type;
        $view['order_type'] = $this->order_type;
        $view['company_id'] = $this->company_id;
        $view['customer_is_new'] = $this->customer_is_new;
        $view['registered_by'] = $this->registered_by;
        $view['customer_phone'] = $this->customer_phone;
        return $view;
    }

    public function checkCustomerType($customerId){
        if($customerId == '') {
            $this->customer_type = 'B2C';
            $this->company_id = 0;
        } else {
            $company2CustomerModel =  new \Models\Company2Customer();
            $result = $company2CustomerModel->findFirst("customer_id = $customerId and status = 10");
            if($result){
                $this->customer_type = 'B2B';
                $this->order_type = $result->Company->getOrderType();
                $this->company_id = $result->toArray()['company_id'];
            }else{
                $this->customer_type = 'B2C';
                $this->company_id = 0;
            }
        }
    }

    /**
     * Get the value of customer_is_new
     */ 
    public function getCustomerIsNew()
    {
        return $this->customer_is_new;
    }

    /**
     * Set the value of customer_is_new
     *
     * @return  self
     */ 
    public function setCustomerIsNew($customer_is_new)
    {
        $this->customer_is_new = $customer_is_new;

        return $this;
    }

    /**
     * Get the value of registered_by
     */ 
    public function getRegisteredBy()
    {
        return $this->registered_by;
    }

    /**
     * Set the value of registered_by
     *
     * @return  self
     */ 
    public function setRegisteredBy($registered_by)
    {
        $this->registered_by = $registered_by;

        return $this;
    }
}
