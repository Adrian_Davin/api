<?php


namespace Models;

class CustomerAceStoreReferral extends \Models\BaseModel
{

    protected $customer_ace_store_referral_id;
    protected $customer_id;
    protected $employee_referral_nip;
    protected $employee_referral_name;
    protected $employee_referral_site_code_sap;
    protected $type;
    protected $sales_assistant_ticket_no;
    protected $status;
    protected $created_at;

    public function initialize()
    {
        //
    }

    /**

     * @return mixed

     */

    public function getCustomerAceStoreReferralId()
    {

        return $this->customer_ace_store_referral_id;
    }


    /**

     * @param mixed $customer_ace_otp_temp_id

     */
    public function setCustomerAceStoreReferralId($customer_ace_store_referral_id)
    {

        $this->customer_ace_store_referral_id = $customer_ace_store_referral_id;
    }


    /**

     * @return mixed

     */
    public function getCustomerId()
    {

        return $this->customer_id;
    }


    /**

     * @param mixed

     */
    public function setCustomerId($customer_id)
    {

        $this->customer_id = $customer_id;
    }

    /**

     * @return mixed

     */
    public function getEmployeeReferralNip()
    {

        return $this->employee_referral_nip;
    }


    /**

     * @param mixed

     */
    public function setEmployeeReferralNip($employee_referral_nip)
    {

        $this->employee_referral_nip = $employee_referral_nip;
    }


    /**

     * @return mixed

     */
    public function getEmployeeReferralName()
    {

        return $this->employee_referral_name;
    }


    /**

     * @param mixed

     */
    public function setEmployeeReferralName($employee_referral_name)
    {

        $this->employee_referral_name = $employee_referral_name;
    }

    /**

     * @return mixed

     */
    public function getEmployeeReferralSiteCodeSap()
    {

        return $this->employee_referral_site_code_sap;
    }


    /**

     * @param mixed

     */
    public function setEmployeeReferralSiteCodeSap($employee_referral_site_code_sap)
    {

        $this->employee_referral_site_code_sap = $employee_referral_site_code_sap;
    }


    /**

     * @return mixed

     */
    public function getType()
    {

        return $this->type;
    }


    /**

     * @param mixed

     */
    public function setType($type)
    {

        $this->type = $type;
    }


    public function getSalesAssistantTicketNo()
    {

        return $this->sales_assistant_ticket_no;
    }


    /**

     * @param mixed

     */
    public function setSalesAssistantTicketNo($sales_assistant_ticket_no)
    {

        $this->sales_assistant_ticket_no = $sales_assistant_ticket_no;
    }

    /**

     * @return mixed

     */
    public function getStatus()
    {

        return $this->status;
    }


    /**

     * @param mixed

     */
    public function setStatus($status)
    {

        $this->status = $status;
    }

    /**

     * @param mixed

     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**

     * @param mixed

     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getDataArray($columns = array(), $showEmpty = false)
    {

        return $this->toArray($columns, $showEmpty);
    }

    public static function find($parameters = null)
    {

        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {

        return parent::findFirst($parameters);
    }
}
