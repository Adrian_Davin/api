<?php

namespace Models;

use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Db as Database;

class DisposableEmailDomain extends \Models\BaseModel
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(type="string", length=128, nullable=false unique=true)
     */
    protected $domain_name;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", length=128, nullable=false)
     */
    protected $created_by;

    /**
     *
     * @var create_date
     * @Column(type="datetime", nullable=true)
     */
    protected $create_date;

    /**
     *
     * @var string
     * @Column(type="string", length=128, nullable=true)
     */
    protected $modified_by;

    /**
     *
     * @var create_date
     * @Column(type="datetime", nullable=true)
     */
    protected $modify_time;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'disposable_email_domains';
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DisposableEmailDomain
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}