<?php

namespace Models;

class SplashPage extends BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $splash_page_id;
    protected $title;
    protected $attribute;
    protected $attribute_value;
    protected $big_banner;
    protected $alt;
    protected $description;
    protected $header_description;
    protected $footer_description;
    protected $status;

    protected $meta_title;
    protected $meta_keywords;
    protected $meta_description;

    protected $old_url_key;
    protected $url_key;
    protected $additional_header;
    protected $inline_script;
    protected $inline_script_seo;
    protected $inline_script_seo_mobile;
    protected $document_title;

    protected $created_at;
    protected $updated_at;
    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getSplashPageId()
    {
        return $this->splash_page_id;
    }

    /**
     * @param int $splash_page_id
     */
    public function setSplashPageId($splash_page_id)
    {
        $this->splash_page_id = $splash_page_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param mixed $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @return mixed
     */
    public function getAttributeValue()
    {
        return $this->attribute_value;
    }

    /**
     * @param mixed $attribute_value
     */
    public function setAttributeValue($attribute_value)
    {
        $this->attribute_value = $attribute_value;
    }

    /**
     * @return mixed
     */
    public function getBigBanner()
    {
        return $this->big_banner;
    }

    /**
     * @param mixed $big_banner
     */
    public function setBigBanner($big_banner)
    {
        $this->big_banner = $big_banner;
    }

    /**
     * @return mixed
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @param mixed $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * @return mixed
     */
    public function getHeaderDescription()
    {
        return $this->header_description;
    }

    /**
     * @param mixed $header_description
     */
    public function setHeaderDescription($header_description)
    {
        $this->header_description = $header_description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getFooterDescription()
    {
        return $this->footer_description;
    }

    /**
     * @param mixed $footer_description
     */
    public function setFooterDescription($footer_description)
    {
        $this->footer_description = $footer_description;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * @param mixed $meta_title
     */
    public function setMetaTitle($meta_title)
    {
        $this->meta_title = $meta_title;
    }

    /**
     * @return mixed
     */
    public function getMetaKeywords()
    {
        return $this->meta_keywords;
    }

    /**
     * @param mixed $meta_keywords
     */
    public function setMetaKeywords($meta_keywords)
    {
        $this->meta_keywords = $meta_keywords;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * @param mixed $meta_description
     */
    public function setMetaDescription($meta_description)
    {
        $this->meta_description = $meta_description;
    }

    /**
     * @return mixed
     */
    public function getDocumentTitle()
    {
        return $this->document_title;
    }

    /**
     * @param mixed $document_title
     */
    public function setDocumentTitle($document_title)
    {
        $this->document_title = $document_title;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUrlKey()
    {
        return $this->url_key;
    }

    /**
     * @param mixed $url_key
     */
    public function setUrlKey($url_key)
    {
        $this->url_key = $url_key;
    }

    /**
     * @return mixed
     */
    public function getOldUrlKey()
    {
        return $this->old_url_key;
    }

    /**
     * @param mixed $old_url_key
     */
    public function setOldUrlKey($old_url_key)
    {
        $this->old_url_key = $old_url_key;
    }

    /**
     * @return mixed
     */
    public function getAdditionalHeader()
    {
        return $this->additional_header;
    }

    /**
     * @param mixed $additional_header
     */
    public function setAdditionalHeader($additional_header)
    {
        $this->additional_header = $additional_header;
    }

    /**
     * @return mixed
     */
    public function getInlineScript()
    {
        return $this->inline_script;
    }

    /**
     * @param mixed $inline_script
     */
    public function setInlineScript($inline_script)
    {
        $this->inline_script = $inline_script;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptSeo()
    {
        return $this->inline_script_seo;
    }

    /**
     * @param mixed $inline_script_seo
     */
    public function setInlineScriptSeo($inline_script_seo)
    {
        $this->inline_script_seo = $inline_script_seo;
    }

    /**
     * @return mixed
     */
    public function getInlineScriptSeoMobile()
    {
        return $this->inline_script_seo_mobile;
    }

    /**
     * @param mixed $inline_script_seo_mobile
     */
    public function setInlineScriptSeoMobile($inline_script_seo_mobile)
    {
        $this->inline_script_seo_mobile = $inline_script_seo_mobile;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'splash_page';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

}
