<?php

namespace Models;

class ScWilayahMapping extends \Models\BaseModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $wilayah_template_id;
    protected $origin_region;
    protected $dest_kecamatan_code;

    protected $string_value;

    protected $created_at;

    protected $errorCode;
    protected $errorMessages;

    /**
     * @return int
     */
    public function getWilayahTemplateId()
    {
        return $this->wilayah_template_id;
    }

    /**
     * @param int $wilayah_template_id
     */
    public function setWilayahTemplateId($wilayah_template_id)
    {
        $this->wilayah_template_id = $wilayah_template_id;
    }

    /**
     * @return mixed
     */
    public function getOriginRegion()
    {
        return $this->origin_region;
    }

    /**
     * @param mixed $origin_region
     */
    public function setOriginRegion($origin_region)
    {
        $this->origin_region = $origin_region;
    }

    /**
     * @return mixed
     */
    public function getDestKecamatanCode()
    {
        return $this->dest_kecamatan_code;
    }

    /**
     * @param mixed $dest_kecamatan_code
     */
    public function setDestKecamatanCode($dest_kecamatan_code)
    {
        $this->dest_kecamatan_code = $dest_kecamatan_code;
    }

    /**
     * @return mixed
     */
    public function getStringValue()
    {
        return $this->string_value;
    }

    /**
     * @param mixed $string_value
     */
    public function setStringValue($string_value)
    {
        $this->string_value = $string_value;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param mixed $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('wilayah_template_id', 'Models\ScWilayahTemplate', 'wilayah_template_id', array('alias' => 'ScWilayahTemplate'));

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sc_wilayah_mapping';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function setFromArray($params = array()){

            foreach ($params as $event => $val){
              if (property_exists($this, $event)) {
                    $this->{$event} = $val;
                }
            }

        // get not send data but have in this parameter
        $thisArray = get_class_vars(get_class($this));
        $this->setSkipAttributeOnUpdate(array_diff_key($thisArray,$params));
    }

    public function insertTemplateToRegion(){

        $sql  = "INSERT INTO sc_wilayah_mapping(wilayah_template_id,origin_region,dest_kecamatan_code,created_at) ";
        $sql .= "VALUES".$this->string_value;

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        if($result->numRows() > 0){
            return $msg = "Insert template to region success!";
        }
        else {
            return $msg = "error";
        }
    }

    public function clearRegionMapping($id){
        $sql  = "DELETE FROM sc_wilayah_mapping ";
        $sql .= "WHERE wilayah_template_id=".$id;

        $this->useWriteConnection();
        $result = $this->getDi()->getShared($this->getConnection())->query($sql);

        if($result->numRows() > 0){
            return $msg = "success";
        }
        else {
            return $msg = "error";
        }
    }

}
