<?php

use Phalcon\Di;
use Phalcon\Test\UnitTestCase as PhalconTestCase;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\MetaData\Memory as modelsMetadata;

abstract class UnitTestCase extends PhalconTestCase
{
    /**
     * @var bool
     */
    private $_loaded = false;

    public function setUp()
    {
        parent::setUp();

        // Load any additional services that might be required during testing
        $di = Di::getDefault();

        // Get any DI components here. If you have a config, be sure to pass it to the parent
        $di->set(
            "modelsManager",
            function() {
                return new ModelsManager();
            }
        );

        $di->set(
            "modelsMetadata",
            function() {
                return new modelsMetadata();
            }
        );

        /**
         * Shared configuration service
         */
        $di->setShared('config', function () {
            return include APP_PATH . "/config/config.php";
        });

        /**
         * Database connection is created based in the parameters defined in the configuration file
         */
        $di->setShared('dbMaster', function () {
            $config = $this->getConfig();

            $dbConfig = $config->dbMaster->toArray();
            $adapter = $dbConfig['adapter'];
            unset($dbConfig['adapter']);

            $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

            return new $class($dbConfig);
        });

        $di->setShared('dbSlave', function () {
            $config = $this->getConfig();

            $dbConfig = $config->dbSlave->toArray();
            $adapter = $dbConfig['adapter'];
            unset($dbConfig['adapter']);

            $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

            return new $class($dbConfig);
        });

        $di->setShared('dbRevive', function () {
            $config = $this->getConfig();

            $dbConfig = $config->dbRevive->toArray();
            $adapter = $dbConfig['adapter'];
            unset($dbConfig['adapter']);

            $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

            return new $class($dbConfig);
        });

        // Set the models cache service
        $di->set(
            "modelsCache",
            function () {
                // Cache data for one day by default
                $frontCache = new Phalcon\Cache\Frontend\Data(
                    [
                        "lifetime" => 86400,
                    ]
                );

                // Memcached connection settings
                $cache = new Phalcon\Cache\Backend\Redis(
                    $frontCache,
                    [
                        'host' => $_ENV['MODELCACHE_REDIS_HOST'],
                        'port' => $_ENV['MODELCACHE_REDIS_PORT'],
                        'persistent' => false,
                        'index' => $_ENV['MODELCACHE_REDIS_DB'],
                    ]
                );

                return $cache;
            }
        );

        $this->setDi($di);

        $this->_loaded = true;
    }

    /**
     * Check if the test case is setup properly
     *
     * @throws \PHPUnit_Framework_IncompleteTestError;
     */
    public function __destruct()
    {

    }
}