<?php

use Phalcon\Di;
use Phalcon\Di\FactoryDefault;
use Phalcon\Loader;

ini_set("display_errors", 1);
error_reporting(E_ALL);

define("ROOT_PATH", __DIR__);
define('APP_PATH', ROOT_PATH.'/../app');
define('VAR_PATH', ROOT_PATH.'/../var');
define('FILE_PATH', ROOT_PATH.'/../Test');

set_include_path(
    ROOT_PATH . PATH_SEPARATOR . get_include_path()
);

// Required for phalcon/incubator
include __DIR__ . "/../vendor/autoload.php";

// Use the application autoloader to autoload the classes
// Autoload the dependencies found in composer
$loader = new Loader();

$loader->registerDirs(
    [
        ROOT_PATH,
    ]
);

// Register namespaces
$loader->registerNamespaces(
    array(
        'Controllers' => APP_PATH . "/controllers",
        'Models' => APP_PATH . "/models",
        'Library' => APP_PATH . "/library",
        'Helpers' => APP_PATH . "/helpers",
    )
);

$loader->register();

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

$di = new FactoryDefault();

Di::reset();

// Add any needed services to the DI here

Di::setDefault($di);