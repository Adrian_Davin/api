<?php

namespace Test\Marketing;

use Models\Cart as CartModel;

/**
 * Class UnitTest
 */
class CashbackConditionTest extends \UnitTestCase
{

    /**
     * This test only testing single sisco item
     */
    public function testCashbackSingleItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/cashback_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_single.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $this->paymentCreditCart($cartData,$conditions);
        $this->paymentCreditCartInstallment($cartData,$conditions);
        $this->paymentNegativeSubtotal($cartData,$conditions);
        $this->paymentNegativePaymentMethod($cartData,$conditions);
    }

    public function testCashbackMixedItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/cashback_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $this->paymentCreditCart($cartData,$conditions);
        $this->paymentCreditCartInstallment($cartData,$conditions);
        $this->paymentNegativeSubtotal($cartData,$conditions);
        $this->paymentNegativePaymentMethod($cartData,$conditions);
    }

    public function testCashbackPromoItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/cashback_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_promo_item.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }


    /*
     * Below is the testing scenario that need to be test
     */

    public function cashbackHaveCertainRuleId($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "cart_rules.rules_id",
            "operator"=> ">=",
            "value"=> "290"
        ];

        $cartData['gift_cards'] = "[{\"rule_id\":\"290\",\"voucher_code\":\"SUPERTEN\",\"voucher_amount\":68600,\"voucher_expired\":\"2017-10-12 00:00:00\",\"voucher_amount_used\":68600,\"voucher_type\":\"3\",\"voucher_affected\":\"item\",\"sku\":\"10074543\"}]";

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function paymentCreditCart($cartData,$conditions)
    {

        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "payment.method",
            "operator"=> "()",
            "value"=> "vpayment,vpaymentins"
        ];

        $cartData['payment'] = [
            "method" => "vpayment",
            "cc_bin" => "541322"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function paymentCreditCartInstallment($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "payment.method",
            "operator"=> "()",
            "value"=> "vpayment,vpaymentins"
        ];

        $cartData['payment'] = [
            "method" => "vpaymentins",
            "cc_bin" => "541322"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(true, $isValidCondition);
    }

    public function negativeCashbackHaveCertainRuleId($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "cart_rules.rules_id",
            "operator"=> ">=",
            "value"=> "290"
        ];

        $cartData['gift_cards_amount'] = "";

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function paymentNegativeSubtotal($cartData,$conditions)
    {
        $isValidCondition = false;
        $cartData['subtotal'] = 1000001;

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(false, $isValidCondition);
    }

    public function paymentNegativePaymentMethod($cartData,$conditions)
    {
        $isValidCondition = false;
        $cartData['subtotal'] = 1000000;

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(false, $isValidCondition);
    }


}