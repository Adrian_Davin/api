<?php

namespace Test\Marketing;

use Models\Cart as CartModel;

/**
 * Class UnitTest
 */
class DiscountApplyTest extends \UnitTestCase
{

    public function testDiscountFix()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/shopping_cart_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $promo['action']['discount_type'] = "fix";
        $promo['action']["applied_action"] = "discount";

        $this->shippingAmountAttribute($cartData,$promo,100000,6000);
        $this->subTotalAttribute($cartData,$promo,100000,100000);
        $this->grandTotalAttribute($cartData,$promo,100000,100000);
        $this->subTotalItemXAttribute($cartData,$promo,10000,10000);
        $this->rowTotalItemXAttribute($cartData,$promo,100000,20000);
        $this->sellingPriceItemXAttribute($cartData,$promo,10000,10000);
        $this->normalPriceItemXAttribute($cartData,$promo,10000,10000);

        $this->subTotalItemXAttributeNegative($cartData,$promo,10000);
        $this->rowTotalItemXAttributeNegative($cartData,$promo,100000);
        $this->sellingPriceItemXAttributeNegative($cartData,$promo,10000);
        $this->normalPriceItemXAttributeNegative($cartData,$promo,10000);
    }

    public function testDiscountPercent()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/shopping_cart_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $promo['action']['discount_type'] = "percent";
        $promo['action']["applied_action"] = "discount";
        $promo['action']["max_redemption"] = 0;

        $this->shippingAmountAttribute($cartData,$promo,10,600);
        $this->subTotalAttribute($cartData,$promo,10,37490);
        $this->grandTotalAttribute($cartData,$promo,10,37490);
        $this->subTotalItemXAttribute($cartData,$promo,10,2000);
        $this->rowTotalItemXAttribute($cartData,$promo,10,2000);
        $this->sellingPriceItemXAttribute($cartData,$promo,10,2000);
        $this->normalPriceItemXAttribute($cartData,$promo,10,2000);

        $this->subTotalItemXAttributeNegative($cartData,$promo,10000);
        $this->rowTotalItemXAttributeNegative($cartData,$promo,100000);
        $this->sellingPriceItemXAttributeNegative($cartData,$promo,10000);
        $this->normalPriceItemXAttributeNegative($cartData,$promo,10000);
    }

    /*
     * Below is the testing scenario that need to be test
     */
    public function shippingAmountAttribute($cartData,$rules,$discount_amount = 0,$expected_result)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "shipping_amount";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->setShippingAmount(6000);
        $cartModel->applyMarketingAction($rules);

        $this->assertEquals($expected_result, $cartModel->getShippingDiscountAmount());
    }


    public function grandTotalAttribute($cartData,$rules,$discount_amount=0,$expected_result)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "grand_total";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $this->assertEquals($expected_result, $cartModel->getDiscountAmount());
    }

    public function subTotalAttribute($cartData,$rules,$discount_amount=0,$expected_result)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "subtotal";


        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $this->assertEquals($expected_result, $cartModel->getDiscountAmount());
    }

    public function subTotalItemXAttribute($cartData,$rules,$discount_amount=0,$expected_result)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "items.subtotal";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();
        $this->assertEquals($expected_result, $cartDataAfterApply['items'][1]['discount_amount']);
    }

    public function rowTotalItemXAttribute($cartData,$rules,$discount_amount=0,$expected_result)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "items.row_total";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();
        $this->assertEquals($expected_result, $cartDataAfterApply['items'][1]['discount_amount']);
    }

    public function sellingPriceItemXAttribute($cartData,$rules,$discount_amount=0,$expected_result)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "items.selling_price";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();
        $this->assertEquals($expected_result, $cartDataAfterApply['items'][1]['discount_amount']);
    }

    public function normalPriceItemXAttribute($cartData,$rules,$discount_amount=0,$expected_result)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "items.normal_price";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();
        $this->assertEquals($cartData['items'][1]['normal_price'], $cartDataAfterApply['items'][1]['selling_price']);
        $this->assertEquals($expected_result, $cartDataAfterApply['items'][1]['discount_amount']);
    }

    public function subTotalItemXAttributeNegative($cartData,$rules,$discount_amount=0)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "items.subtotal";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();
        $this->assertEquals(0, $cartDataAfterApply['items'][0]['discount_amount']);
    }

    public function rowTotalItemXAttributeNegative($cartData,$rules,$discount_amount=0)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "items.row_total";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();
        $this->assertEquals(0, $cartDataAfterApply['items'][0]['discount_amount']);
    }

    public function sellingPriceItemXAttributeNegative($cartData,$rules,$discount_amount=0)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "items.selling_price";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();
        $this->assertEquals(0, $cartDataAfterApply['items'][0]['discount_amount']);
    }

    public function normalPriceItemXAttributeNegative($cartData,$rules,$discount_amount=0)
    {
        $rules['action']["discount_amount"] = $discount_amount;
        $rules['action']["attributes"] = "items.normal_price";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();
        $this->assertEquals($cartData['items'][1]['normal_price'], $cartDataAfterApply['items'][1]['selling_price']);
        $this->assertEquals(0, $cartDataAfterApply['items'][0]['discount_amount']);
    }

}