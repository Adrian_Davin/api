<?php

namespace Test\Marketing;

use Models\Cart as CartModel;

/**
 * Class UnitTest
 */
class VoucherConditionTest extends \UnitTestCase
{

    /**
     * This test only testing single sisco item
     */
    public function testSingleItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/voucher_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_single.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $this->voucherWithMinimumPurchase($cartData,$conditions);
        $this->voucherWithMinimumPurchaseNegatif($cartData,$conditions);
        $this->voucherWithMaxItemQty($cartData,$conditions);

    }

    public function testMixedItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/voucher_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $this->voucherWithMinimumPurchase($cartData,$conditions);
        $this->voucherWithMinimumPurchaseNegatif($cartData,$conditions);

    }

    public function testPromoItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/voucher_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_promo_item.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }


    /*
     * Below is the testing scenario that need to be test
     */

    public function voucherWithMinimumPurchase($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "subtotal",
            "operator"=> ">=",
            "value"=> "100000"
        ];

        $cartData['subtotal'] = 100000;

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function voucherWithMaxItemQty($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "items.qty_ordered",
            "operator"=> "==",
            "value"=> "1"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function voucherWithMinimumPurchaseNegatif($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "subtotal",
            "operator"=> ">=",
            "value"=> "100000"
        ];

        $cartData['subtotal'] = 90000;

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(false, $isValidCondition);
    }

    public function voucherWithMaxItemQtyNegative($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "items.qty_ordered",
            "operator"=> "==",
            "value"=> "1"
        ];

        $cartData['total_qty_item'] = 3;;
        $cartData['items'][0]['qty_ordered'] = 3;

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function voucherWithCertainSku($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "items.sku",
            "operator"=> "()",
            "value"=> "X106879,X106882,X106877,10024965,X064224"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function voucherWithCertainSkuNegative($cartData,$conditions)
    {
        $conditions[] = [
            "aggregator"=> "AND",
            "attribute"=> "items.sku",
            "operator"=> "()",
            "value"=> "X106879,X106882,X106877"
        ];


        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }
}