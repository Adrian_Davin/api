<?php

namespace Test\Marketing;

use Models\Cart as CartModel;

/**
 * Class UnitTest
 */
class OperatorConditionTest extends \UnitTestCase
{
    public function testSingleItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/voucher_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_single.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = [];
        $this->operatorEqual($cartData,$conditions);
        $this->operatorNotEqual($cartData,$conditions);
        $this->operatorGreaterThen($cartData,$conditions);
        $this->operatorGreeterEqualThen($cartData,$conditions);
        $this->operatorLessEqualThen($cartData,$conditions);
        $this->operatorLessThen($cartData,$conditions);
        $this->operatorIn($cartData,$conditions);
        $this->operatorNotIn($cartData,$conditions);
        $this->operatorContain($cartData,$conditions);
        $this->operatorEqualNegative($cartData,$conditions);
        $this->operatorNotEqualNegative($cartData,$conditions);
        $this->operatorGreaterThenNegative($cartData,$conditions);
        $this->operatorGreeterEqualThenNegative($cartData,$conditions);
        $this->operatorLessEqualThenNegative($cartData,$conditions);
        $this->operatorLessThenNegative($cartData,$conditions);
        $this->operatorInNegative($cartData,$conditions);
        $this->operatorNotInNegative($cartData,$conditions);
        $this->operatorContainNegative($cartData,$conditions);
    }

    public function testMixedItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/voucher_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = [];
        $this->operatorEqualMixed($cartData,$conditions);
        $this->operatorNotEqual($cartData,$conditions);
        $this->operatorGreaterThen($cartData,$conditions);
        $this->operatorGreeterEqualThen($cartData,$conditions);
        $this->operatorLessEqualThen($cartData,$conditions);
        $this->operatorLessThen($cartData,$conditions);
        $this->operatorIn($cartData,$conditions);
        $this->operatorNotIn($cartData,$conditions);
        $this->operatorContain($cartData,$conditions);
        $this->operatorEqualNegativeMixed($cartData,$conditions);
        $this->operatorGreaterThenNegative($cartData,$conditions);
        $this->operatorGreeterEqualThenNegative($cartData,$conditions);
        $this->operatorLessEqualThenNegativeMixed($cartData,$conditions);
        $this->operatorLessThenNegative($cartData,$conditions);
        $this->operatorInNegative($cartData,$conditions);
        $this->operatorNotInNegativeMixed($cartData,$conditions);
        $this->operatorContainNegative($cartData,$conditions);
    }

    /*
     * Below is the testing scenario that need to be test
     */

    public function operatorEqual($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.sku",
            "operator"=> "==",
            "value"=> "10024965"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorEqualMixed($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.sku",
            "operator"=> "==",
            "value"=> "X064224"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorNotEqual($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.qty_ordered",
            "operator"=> "!=",
            "value"=> "2"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorLessThen($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "subtotal",
            "operator"=> "<",
            "value"=> "500000"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorLessEqualThen($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "subtotal",
            "operator"=> "<=",
            "value"=> "448500"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorGreaterThen($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "grand_total_exclude_shipping",
            "operator"=> ">",
            "value"=> "100000"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorGreeterEqualThen($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.subtotal",
            "operator"=> ">=",
            "value"=> "149500"
        ];


        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorIn($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.supplier_alias",
            "operator"=> "()",
            "value"=> "AHI,HCI,TGI"
        ];


        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorContain($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "shipping_address.full_address",
            "operator"=> "%",
            "value"=> "kawan lama"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);

        $conditions = []; // Reset Condition
        $conditions[] = [
            "attribute"=> "items.name",
            "operator"=> "%",
            "value"=> "krisbow"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorNotIn($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.category.category_id",
            "operator"=> "!()",
            "value"=> "5663,2983"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function operatorEqualNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "discount_amount",
            "operator"=> "==",
            "value"=> "0"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorEqualNegativeMixed($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.store_code",
            "operator"=> "==",
            "value"=> "A123"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorNotEqualNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "shipping_address.kecamatan.kecamatan_code",
            "operator"=> "!=",
            "value"=> "CGK10400JAKARTA UTARA"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorLessThenNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "grand_total",
            "operator"=> "<",
            "value"=> "100000"
        ];

        $cartData['subtotal'] = 90000;

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorLessEqualThenNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.row_total",
            "operator"=> "<=",
            "value"=> "100000"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorLessEqualThenNegativeMixed($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.row_total",
            "operator"=> "<=",
            "value"=> "1000"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorGreaterThenNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.selling_price",
            "operator"=> ">",
            "value"=> "1000000"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorGreeterEqualThenNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "shipping_discount_amount",
            "operator"=> ">=",
            "value"=> "300000"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorInNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "cart_rules.rule_id",
            "operator"=> "()",
            "value"=> "290,291,292"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorNotInNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.store_code",
            "operator"=> "!()",
            "value"=> "A392,A322"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorContainNegative($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "shipping_address.full_address",
            "operator"=> "%",
            "value"=> "joglo"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);

        $conditions = []; // Reset Condition
        $conditions[] = [
            "attribute"=> "items.name",
            "operator"=> "%",
            "value"=> "rumah"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(false, $isValidCondition);
    }

    public function operatorNotInNegativeMixed($cartData,$conditions)
    {
        $conditions[] = [
            "attribute"=> "items.sku",
            "operator"=> "!()",
            "value"=> "X901568,X064224,M073000012,035773-1"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }


}