<?php

namespace Test\Marketing;

use Library\Marketing as Marketing;

/**
 * Class UnitTest
 */
class CanCombineTest extends \UnitTestCase
{

    /**
     * This test only testing single sisco item
     */
    public function testCashback()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/voucher_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $this->canCombine($cartData,$promo);
        $this->canNotCombineHaveGiftCard($cartData,$promo);
        $this->canNotCombineGetPromotion($cartData,$promo);

    }


    /*
     * Below is the testing scenario that need to be test
     */

    /**
     * In this case, customer already get promotion (free shipping) but this promotion can be combne with other promotion
     * @param $cartData
     * @param $rules
     */
    public function canCombine($cartData,$rules)
    {
        $marketingLib = new Marketing();
        $canCombine  = $marketingLib->canCombineCheck($cartData,$rules);

        $this->assertEquals(true, $canCombine);
    }

    /**
     * This case, customer already have gift card but they still want to apply again
     * @param $cartData
     * @param $rules
     */
    public function canNotCombineHaveGiftCard($cartData,$rules)
    {
        $cartData['gift_cards'] = "[{\"rule_id\":\"299\",\"voucher_code\":\"SUPERTEN\",\"voucher_amount\":68600,\"voucher_expired\":\"2017-10-12 00:00:00\",\"voucher_amount_used\":68600,\"voucher_type\":\"3\",\"voucher_affected\":\"item\",\"sku\":\"10074543\"}]";

        $marketingLib = new Marketing();
        $canCombine  = $marketingLib->canCombineCheck($cartData,$rules);

        $this->assertEquals(false, $canCombine);
    }

    /**
     * In this case, customer already get promotion that can not combine with other promotion
     * @param $cartData
     * @param $rules
     */
    public function canNotCombineGetPromotion($cartData,$rules)
    {
        $cartData['cart_rules'] = "[{\"rule_id\":\"2\",\"name\":\"Free Shipping Jabodetabek\",\"conditions\":[{\"attribute\":\"grand_total_exclude_shipping\",\"operator\":\">=\",\"value\":\"300000\"},{\"aggregator\":\"AND\",\"attribute\":\"shipping_address.city.city_id\",\"operator\":\"()\",\"value\":\"1,394,62,45,388,402,339,455,372,373,374,375,376,377\"}],\"description\":\"Free Shipping Jabodetabek Min. Belanja Rp. 300.000, max 3 kg\",\"use_voucher\":\"0\",\"uses_per_customer\":\"0\",\"stop_rules_processing\":null,\"can_combine\":\"0\",\"priority\":\"1\",\"action\":{\"applied_action\":\"discount\",\"action_process\":\"-\",\"discount_amount\":\"100.00\",\"attributes\":\"shipping_amount\",\"discount_type\":\"percent\",\"max_redemption\":0},\"customer_group_id\":\"0,1,2,3,4\"}]";

        $marketingLib = new Marketing();
        $canCombine  = $marketingLib->canCombineCheck($cartData,$rules);

        $this->assertEquals(false, $canCombine);
    }


}