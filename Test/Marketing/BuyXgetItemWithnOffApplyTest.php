<?php

namespace Test\Marketing;

use Models\Cart as CartModel;

/**
 * Class UnitTest
 */
class BuyXgetItemWithnOffApplyTest extends \UnitTestCase
{

    public function testDiscountFix()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/shopping_cart_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $promo['action']['discount_type'] = "fix";
        $promo['action']["applied_action"] = "get_item_discount";
        $promo['action']['discount_item_sku'] = "10054201";
        $promo['action']['discount_item_qty'] = "1"; // Number of free item that customer will get
        $promo['action']['discount_step_max'] = "0"; // Number of reoucrring promo get for getting free item
        $promo['action']['discount_step'] = "1"; // Qty step from an item

        $this->discountAmountGetCheck($cartData,$promo,10000,10000);
        $this->freeItemQtyGet($cartData,$promo);
        $this->maxQtyStep($cartData,$promo);
        $this->canNotGetFreeItem($cartData,$promo);
    }

    public function testDiscountPercent()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/shopping_cart_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $promo['action']['discount_type'] = "percent";
        $promo['action']["applied_action"] = "get_item_discount";
        $promo['action']['discount_item_sku'] = "10054201";
        $promo['action']['discount_item_qty'] = "1"; // Number of free item that customer will get
        $promo['action']['discount_step_max'] = "0"; // Number of reoucrring promo get for getting free item
        $promo['action']['discount_step'] = "1"; // Qty step from an item

        $this->discountAmountGetCheck($cartData,$promo,10,5000);
    }

    /*
     * Below is the testing scenario that need to be test
     */
    public function freeItemQtyGet($cartData,$rules)
    {
        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();

        $freeItemQty = 0;
        foreach ($cartDataAfterApply['items'] as $item) {
            if($item['is_free_item'] == 1) {
                $freeItemQty += $item['qty_ordered'];
            }
        }

        $eligibleFreeItem =  $rules['action']['discount_item_qty'] * (!empty($rules['action']['discount_step_max']) ? $rules['action']['discount_step_max'] : 1);


        $this->assertEquals($eligibleFreeItem, $freeItemQty);
    }

    public function maxQtyStep($cartData,$rules)
    {
        $cartData['items'][1]['qty_ordered'] = 5;

        // Modified rule a little
        $rules['action']['discount_step_max'] = 3;
        unset($rules['conditions'][1]); // Remove item qty condition check

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();

        $freeItemQty = 0;
        foreach ($cartDataAfterApply['items'] as $item) {
            if($item['is_free_item'] == 1) {
                $freeItemQty += $item['qty_ordered'];
            }
        }

        $eligibleFreeItem =  $rules['action']['discount_item_qty'] * (!empty($rules['action']['discount_step_max']) ? $rules['action']['discount_step_max'] : 1);

        $this->assertEquals($eligibleFreeItem, $freeItemQty);
    }

    /**
     * This test prove that customer don't get item because the qty not fit with discount_item_qty
     * @param $cartData
     * @param $rules
     */
    public function canNotGetFreeItem($cartData,$rules)
    {
        $cartData['items'][1]['qty_ordered'] = 5;

        // Modified rule a little
        $rules['action']['discount_step_max'] = 3;
        $promo['action']['discount_item_qty'] = 6;
        unset($rules['conditions'][1]); // Remove item qty condition check

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();

        $freeItemQty = 0;
        foreach ($cartDataAfterApply['items'] as $item) {
            if($item['is_free_item'] == 1) {
                $freeItemQty += $item['qty_ordered'];
            }
        }

        $eligibleFreeItem = $rules['action']['discount_item_qty'] * (!empty($rules['action']['discount_step_max']) ? $rules['action']['discount_step_max'] : 1);

        $this->assertEquals($eligibleFreeItem, $freeItemQty);
    }

    public function getSameItem($cartData,$rules)
    {
        $promo['action']['discount_item_sku'] = "";

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();

        $freeItemQty = 0;
        foreach ($cartDataAfterApply['items'] as $item) {
            if($item['is_free_item'] == 1) {
                $freeItemQty += $item['qty_ordered'];
            }
        }

        $eligibleFreeItem =  $rules['action']['discount_item_qty'] * (!empty($rules['action']['discount_step_max']) ? $rules['action']['discount_step_max'] : 1);


        $this->assertEquals($eligibleFreeItem, $freeItemQty);
    }

    public function discountAmountGetCheck($cartData, $rules, $discount_amount=0, $expected_result)
    {
        $rules['action']["discount_amount"] = $discount_amount;

        $cartModel = new CartModel();
        $cartModel->setFromArray($cartData);
        $cartModel->applyMarketingAction($rules);

        $cartDataAfterApply = $cartModel->getDataArray();

        $discountAmountGet = 0;
        foreach ($cartDataAfterApply['items'] as $item) {
            if($item['is_free_item'] == 1) {
                $discountAmountGet += $item['discount_amount'];
            }
        }


        $this->assertEquals($expected_result, $discountAmountGet);
    }
}