<?php

namespace Test\Marketing;

use Library\Marketing as Marketing;
use Models\Cart as CartModel;

/**
 * Class UnitTest
 */
class PaymentConditionTest extends \UnitTestCase
{

    /**
     * This test only testing single sisco item
     */
    public function testPayementSingleItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/payment_promo.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_single.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $this->paymentCreditCart($cartData,$conditions);
        $this->paymentCreditCartInstallment($cartData,$conditions);
        $this->paymentNegativeSubtotal($cartData,$conditions);
        $this->paymentNegativePaymentMethod($cartData,$conditions);
    }

    public function testPaymentMixedItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/payment_promo.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $this->paymentCreditCart($cartData,$conditions);
        $this->paymentCreditCartInstallment($cartData,$conditions);
        $this->paymentNegativeSubtotal($cartData,$conditions);
        $this->paymentNegativePaymentMethod($cartData,$conditions);
    }

    public function testPromoItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/payment_promo.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_promo_item.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }


    /*
     * Below is the testing scenario that need to be test
     */

    public function paymentCreditCart($cartData,$conditions)
    {
        /**
         * Testing for regular cc payment
         */
        $cartData['payment'] = [
            "method" => "vpayment",
            "cc_bin" => "541322"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function paymentCreditCartInstallment($cartData,$conditions)
    {
        /**
         * Testing for cc installment
         */
        $isValidCondition = false;
        $cartData['payment'] = [
            "method" => "vpaymentins",
            "cc_bin" => "541322"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(true, $isValidCondition);
    }

    public function paymentNegativeSubtotal($cartData,$conditions)
    {
        $isValidCondition = false;
        $cartData['subtotal'] = 1000001;
        $cartData['payment'] = [
            "method" => "vpayment",
            "cc_bin" => "541322"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(false, $isValidCondition);
    }

    public function paymentNegativePaymentMethod($cartData,$conditions)
    {
        $isValidCondition = false;
        $cartData['subtotal'] = 1000000;
        $cartData['payment'] = [
            "method" => "vpaymentva",
            "cc_bin" => "541322"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);
        $this->assertEquals(false, $isValidCondition);
    }


}