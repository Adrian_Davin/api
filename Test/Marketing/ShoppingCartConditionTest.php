<?php

namespace Test\Marketing;

use Models\Cart as CartModel;

/**
 * Class UnitTest
 */
class ShoppingCartConditionTest extends \UnitTestCase
{

    /**
     * This test only testing single sisco item
     */
    public function testSingleItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/shopping_cart_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_single.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $this->normalTest($cartData,$conditions);
        $this->negativeTest($cartData,$conditions);

    }

    public function testMixedItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/shopping_cart_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_mixed.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $this->normalTest($cartData,$conditions);
        $this->negativeTest($cartData,$conditions);
    }

    public function testPromoItem()
    {
        $file_location = FILE_PATH.'/Marketing/PromotionJson/shopping_cart_promotion.json';
        $promo = [];
        if(file_exists($file_location)) {
            $promoJson = file_get_contents($file_location);
            $promo = json_decode($promoJson,true);
        }
        $this->assertNotEmpty($promo);

        $file_location = FILE_PATH.'/Marketing/CartDataJson/cart_login_promo_item.json';
        $cartData = [];
        if(file_exists($file_location)) {
            $jsonContent = file_get_contents($file_location);
            $cartData = json_decode($jsonContent,true);
        }

        $this->assertNotEmpty($cartData);

        $conditions = $promo['conditions'];
        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }


    /*
     * Below is the testing scenario that need to be test
     */

    public function normalTest($cartData,$conditions)
    {
        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(true, $isValidCondition);
    }

    public function negativeTest($cartData,$conditions)
    {
        $conditions[0] = [
            "attribute"=> "items.sku",
            "operator"=> "()",
            "value"=> "X106879,X106882,X106877"
        ];

        $cartModel = new CartModel();
        $isValidCondition = $cartModel->marketingConditionCheck($conditions,$cartData);

        $this->assertEquals(false, $isValidCondition);
    }


}