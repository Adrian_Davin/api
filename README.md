# RUPARUPA API #

# PHP
# Notes on `.env.development`

Every time developer add new env, please add the additional env into `.env.development`.
Keep in mind that `.env.development` will be used for building Multistaging.
  
**Keep in mind these rules:**
```
- Add new env to ".env.development" with placeholdering as follows:
- For API Endpoints:
    Example:
      API_URL = http://tugu.api.ruparupastg.my.id >> API_URL = http://{project_name}.api.ruparupastg.my.id

- For Database Name/Index/etc:
    Example:
      DB_NAME = ruparupadb2_tugu >> DB_NAME = ruparupadb2_{project_name}
      ELASTIC_INDEX = productsvariant_tugu >> productsvariant_{project_name}
```

Current Version 0.1 (initial)

Maintainer: 

+ @andy_antonius

+ @hendyw

Primary Resources:

+ /products

+ /carts

+ /orders

+ /customers

ENV File example : `.env.example`

/var directory skeleton :

	├── var
	│   ├── logs
	│   └── journal
	│   └── pickup_voucher
	│   └── pickup_voucher
	│   └── sob2c
