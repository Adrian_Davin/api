CREATE TABLE `customer_device` (
  `customer_device_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customer_device_id`),
  KEY `customer_id_device_id_idx` (`customer_id`, `device_id`) USING BTREE
)