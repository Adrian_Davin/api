-- Store To Carrier
CREATE TABLE `store_2_carrier` (
  `store_code` varchar(10) NOT NULL,
  `carrier_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- update length cart_id
ALTER TABLE `sales_order` 
CHANGE COLUMN `cart_id` `cart_id` VARCHAR(60) NULL DEFAULT NULL ;

-- update length cart_id
ALTER TABLE `ovo` 
CHANGE COLUMN `cart_id` `cart_id` VARCHAR(60) NULL DEFAULT NULL ;

-- update length cart_id
ALTER TABLE `sprintasia` 
CHANGE COLUMN `cart_id` `cart_id` VARCHAR(60) NULL DEFAULT NULL ;