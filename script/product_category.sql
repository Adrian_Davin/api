ALTER TABLE `product_category` 
ADD COLUMN `terminology` VARCHAR(255) NULL AFTER `article_hierarchy`;

ALTER TABLE `product_category` 
ADD COLUMN `exclude_category` TEXT NULL AFTER `rule_based_hero_products`;

ALTER TABLE `product_category` 
ADD COLUMN `exclude_sku` TEXT NULL AFTER `exclude_category`;

ALTER TABLE `product_category` 
ADD COLUMN `tags` TEXT NULL AFTER `exclude_sku`;
