CREATE TABLE `ruparupadb_2_aceapps`.`customer_ace_order_warranty_non_membership` (
  `customer_ace_order_warranty_non_membership_id` INT NOT NULL AUTO_INCREMENT,
  `ace_customer_id` VARCHAR(30) NOT NULL,
  `receipt_id_and_sku` VARCHAR(50) NOT NULL,
  `warranty_code` VARCHAR(15) NULL,
  `ace_warranty_id` VARCHAR(50) NULL,
  `serial_number` VARCHAR(255) NULL,
  `warranty_number` VARCHAR(255) NULL,
  `new_item_replacement_warranty_date` DATE NULL,
  `spare_part_warranty_date` DATE NULL,
  `service_warranty_date` DATE NULL,
  `spare_parts_detail_warranty` JSON NULL,
  `status` NOT NULL DEFAULT '0'
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customer_ace_order_warranty_non_membership_id`));
