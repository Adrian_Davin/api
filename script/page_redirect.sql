CREATE TABLE `ruparupadb_multicompany`.`page_redirect` (
  `page_redirect_id` INT NOT NULL AUTO_INCREMENT,
  `keyword` VARCHAR(100) NULL,
  `url_path` TEXT NULL,
  `company_code` VARCHAR(45) NULL,
  `status` TINYINT(4) NULL DEFAULT 10,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`page_redirect_id`));