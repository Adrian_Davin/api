ALTER TABLE `ruparupadb_2`.`sales_invoice_item`
ADD COLUMN `available_stock` INT(10) NULL DEFAULT NULL AFTER `is_free_item`,
ADD COLUMN `status_fulfillment` VARCHAR(50) NULL DEFAULT NULL AFTER `available_stock`,
ADD COLUMN `reason` VARCHAR(255) NULL DEFAULT NULL AFTER `status_fulfillment`,
ADD COLUMN `note` VARCHAR(255) NULL DEFAULT NULL AFTER `reason`,
ADD COLUMN `created_at` DATETIME(0) NULL DEFAULT NULL AFTER `note`,
ADD COLUMN `updated_at` DATETIME(0) NULL DEFAULT NULL AFTER `created_at`;


ALTER TABLE `sales_invoice_item`
ADD COLUMN `price_modified` VARCHAR(255) NULL DEFAULT NULL AFTER `selling_price`;