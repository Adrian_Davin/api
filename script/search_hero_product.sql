CREATE TABLE `search_hero_product` (
  `search_hero_product_id` INT NOT NULL AUTO_INCREMENT,
  `keyword` VARCHAR(255) NULL,
  `hero_product` LONGTEXT NULL,
  `status` TINYINT(4) NULL DEFAULT '10',
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`search_hero_product_id`));
