CREATE TABLE `ruparupadb_2`.`price_override_sap` (
  `price_override_sap_id` INT NOT NULL,
  `sku` VARCHAR(30) NULL,
  `from_date` DATE NULL,
  `to_date` DATE NULL,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`price_override_sap_id`));

ALTER TABLE `ruparupadb_2`.`price_override_sap`
CHANGE COLUMN `price_override_sap_id` `price_override_sap_id` INT(11) NOT NULL AUTO_INCREMENT ;
