ALTER TABLE `ruparupadb_multicompany`.`customer_address`
CHANGE COLUMN `is_default` `is_default` TINYINT(3) NULL DEFAULT '0' ,
ADD COLUMN `address_name` VARCHAR(45) CHARACTER SET 'utf8' NULL AFTER `phone`;

UPDATE ruparupadb_multicompany.customer_address SET address_name = LEFT(full_address , 30)

