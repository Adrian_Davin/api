CREATE TABLE `customer_ace_store_referral` (
  `customer_ace_store_referral_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `employee_referral_nip` varchar(12) NOT NULL,
  `employee_referral_name` varchar(45) NOT NULL,
  `employee_referral_site_code_sap` varchar(45) DEFAULT NULL,
  `type` enum('register') NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `PRIMARY_KEY` (`customer_ace_store_referral_id`),
  KEY `fk_customer_ace_store_referral_1_idx` (`customer_id`),
  CONSTRAINT `fk_customer_ace_store_referral_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
