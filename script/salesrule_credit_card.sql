CREATE TABLE `salesrule_credit_card` (
    `salesrule_credit_card_id` int(11) NOT NULL AUTO_INCREMENT,
    `customer_id` int(11) NOT NULL,
    `cc_type` varchar(135) NOT NULL,
    `cc_number` varchar(45) NOT NULL,
    `rule_id` int(10) not null,
    `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`salesrule_credit_card_id`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1;