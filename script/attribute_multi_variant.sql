ALTER TABLE `ruparupadb_2`.`sales_order_item`
ADD COLUMN `attributes` TEXT NULL AFTER `cart_rules`;

ALTER TABLE `ruparupadb_2`.`sales_invoice_item`
ADD COLUMN `attributes` TEXT NULL DEFAULT '' AFTER `is_free_item`;

