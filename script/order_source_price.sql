ALTER TABLE `sales_order_item`
ADD COLUMN `price_modified` VARCHAR(255) NULL DEFAULT NULL AFTER `selling_price`;

ALTER TABLE `sales_invoice_item`
ADD COLUMN `price_modified` VARCHAR(255) NULL DEFAULT NULL AFTER `selling_price`;

ALTER table product_price add column last_modified varchar(255) after special_to_date;


ALTER TABLE `sales_order_item`
ADD COLUMN `modified_description` VARCHAR(255) NULL DEFAULT NULL AFTER `price_modified`;

ALTER TABLE `sales_invoice_item`
ADD COLUMN `modified_description` VARCHAR(255) NULL DEFAULT NULL AFTER `price_modified`;

ALTER table `product_price` add column modified_description varchar(255) after special_to_date;