CREATE TABLE `marketing_tukarvoucher` (
  `tukarvoucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `customer_first_name` varchar(100) DEFAULT NULL,
  `customer_last_name` varchar(100) DEFAULT NULL,
  `customer_email` varchar(100) DEFAULT NULL,
  `customer_password` varchar(100) DEFAULT NULL,
  `customer_phone` varchar(45) DEFAULT NULL,
  `customer_denom` int(11) DEFAULT NULL,
  `customer_merchant` varchar(20) DEFAULT 'BCA',
  `voucher_code_generated` varchar(225) DEFAULT NULL,
  `is_email_sent` tinyint(4) DEFAULT '0',
  `user_created` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`tukarvoucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
