ALTER TABLE `ruparupadb_2`.`shipping_rate`
ADD COLUMN `origin_kecamatan_id` VARCHAR(45) NULL DEFAULT NULL AFTER `origin_province_code`;

ALTER TABLE `ruparupadb_2`.`shipping_rate`
ADD COLUMN `origin_kecamatan_code` VARCHAR(45) NULL DEFAULT NULL AFTER `origin_kecamatan_id`;

UPDATE shipping_rate SET origin_kecamatan_id = '1170', origin_kecamatan_code = 'DPS10000DENPASAR' WHERE origin_province_code = 'ID-BA' AND carrier_id = '1';
UPDATE shipping_rate SET origin_kecamatan_id = '4927', origin_kecamatan_code = 'TGR10000TANGERANG' WHERE origin_province_code = 'ID-BT' AND carrier_id = '1';
UPDATE shipping_rate SET origin_kecamatan_id = '1413', origin_kecamatan_code = 'SUB10100GRESIK' WHERE origin_province_code = 'ID-JI' AND carrier_id = '1';
UPDATE shipping_rate SET origin_kecamatan_id = '980', origin_kecamatan_code = 'BKI10100CIKARANG' WHERE origin_province_code = 'ID-JK' AND carrier_id = '1';
UPDATE shipping_rate SET origin_kecamatan_id = '5336', origin_kecamatan_code = 'UPG10000UJUNG PANDANG' WHERE origin_province_code = 'ID-SN' AND carrier_id = '1';
UPDATE shipping_rate SET origin_kecamatan_id = '2824', origin_kecamatan_code = 'MES10027MEDAN MAIMUN' WHERE origin_province_code = 'ID-SU' AND carrier_id = '1';

UPDATE shipping_rate SET origin_kecamatan_id = '4927', origin_kecamatan_code = 'TGR10000TANGERANG' WHERE origin_province_code = 'ID-BT' AND carrier_id = '2';
UPDATE shipping_rate SET origin_kecamatan_id = '980', origin_kecamatan_code = 'BKI10100CIKARANG' WHERE origin_province_code = 'ID-JK' AND carrier_id = '2';
UPDATE shipping_rate SET origin_kecamatan_id = '1367', origin_kecamatan_code = 'SRG10005GENUK' WHERE origin_province_code = 'ID-JT' AND carrier_id = '2';