-- add TGI field so we can track which customer has token from tgi online for push notifications
ALTER TABLE customer_device_token ADD TGI int(11) DEFAULT 0 NULL after HCI;