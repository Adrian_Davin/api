ALTER TABLE `customer_alert`
ADD COLUMN `customer_id` INT(10) NULL AFTER `customer_alert_id`;

UPDATE ruparupadb_2_migration.customer_alert a
left join ruparupadb_2_migration.customer b on a.email = b.email
SET a.customer_id = b.customer_id