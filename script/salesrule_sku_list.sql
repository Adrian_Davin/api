CREATE TABLE IF NOT EXISTS `salesrule_sku_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `sku` varchar(30) DEFAULT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `salesrule_sku_list_rule_id_IDX` (`rule_id`,`sku`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;