ALTER TABLE `ruparupadb_2`.`sales_order` 
CHANGE COLUMN `cart_id` `cart_id` VARCHAR(60) NULL DEFAULT NULL ;

ALTER TABLE `ruparupadb_2_voucher`.`sales_order_payment` 
ADD COLUMN `can_cancel` INT(3) NULL AFTER `updated_at`;

ALTER TABLE `ruparupadb_2_voucher`.`sales_order_payment` 
ADD COLUMN  `has_changed_payment` int(10) DEFAULT NULL COMMENT 'Counter how many customer change payment';