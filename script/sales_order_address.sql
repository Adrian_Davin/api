ALTER TABLE `ruparupadb_multicompany`.`sales_order_address`
ADD COLUMN `address_name` VARCHAR(45) CHARACTER SET 'utf8' NULL AFTER `address_type`;

UPDATE ruparupadb_multicompany.sales_order_address SET address_name = LEFT(full_address , 30)