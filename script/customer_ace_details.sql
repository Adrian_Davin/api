ALTER TABLE `ruparupadb_2_aceapps`.`customer_ace_details` 
ADD COLUMN `status` TINYINT(1) NOT NULL DEFAULT 1 AFTER `login_attempt`,
ADD COLUMN `expired_date` DATETIME NULL DEFAULT NULL AFTER `status`;
