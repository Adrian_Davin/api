# MARKETING_EVENT
ALTER TABLE `ruparupadb_multicompany`.`event` 
ADD COLUMN `identifier` VARCHAR(45) NULL DEFAULT '' AFTER `title`,
ADD COLUMN `company_code` VARCHAR(45) NULL DEFAULT 'ODI' AFTER `products`;
