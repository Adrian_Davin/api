ALTER TABLE `ruparupadb_2_aceapps`.`customer_ace_otp_temp` 
ADD COLUMN `customer_id` INT NOT NULL DEFAULT 0 AFTER `customer_ace_otp_temp_id`,
ADD COLUMN `type` VARCHAR(45) NULL AFTER `request_count`;

ALTER TABLE `ruparupadb_2_aceapps`.`customer_ace_otp_temp` 
CHANGE COLUMN `type` `type` ENUM('register', 'verify_phone', 'verify_email', 'valid_change_phone', 'valid_change_email', 'change_phone', 'change_email', 'redeem_point') NULL DEFAULT NULL;

ALTER TABLE `ruparupadb_2_aceapps`.`customer_ace_otp_temp` 
CHANGE COLUMN `type` `type` ENUM('register', 'verify_phone', 'verify_email', 'valid_change_phone', 'valid_change_email', 'change_phone', 'change_email', 'redeem_point', 'digital_stamp') NULL DEFAULT NULL;

ALTER TABLE `ruparupadb_2_aceapps`.`customer_ace_otp_temp` 
CHANGE COLUMN `type` `type` ENUM('register', 'verify_phone', 'verify_email', 'valid_change_phone', 'valid_change_email', 'change_phone', 'change_email', 'redeem_point', 'digital_stamp', 'send_voucher') NULL DEFAULT NULL;