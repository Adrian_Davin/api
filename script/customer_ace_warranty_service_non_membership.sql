CREATE TABLE `ruparupadb_2_aceapps`.`customer_ace_warranty_service_non_membership` (
  `customer_ace_warranty_service_non_membership_id` INT NOT NULL AUTO_INCREMENT,
  `customer_ace_order_warranty_non_membership_id` INT NOT NULL,
  `ace_service_number` VARCHAR(255) NOT NULL,
  `status` INT NULL,
  `service_warranty_charge` DECIMAL(12,2) NULL,
  `service_warranty_items` JSON NULL,
  `service_date` DATE NULL,
  `service_expected_finished_date` DATE NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customer_ace_warranty_service_non_membership_id`));
