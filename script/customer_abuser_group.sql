CREATE TABLE `customer_abuser_group` (
  `customer_id` INT NOT NULL,
  `group` INT NULL,
  PRIMARY KEY (`customer_id`));

ALTER TABLE `salesrule_order` 
ADD COLUMN `group_abuser` INT NULL AFTER `voucher_amount_used`;
