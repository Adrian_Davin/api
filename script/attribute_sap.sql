ALTER TABLE `ruparupadb_2`.`attribute_sap`
ADD COLUMN `tax_class` VARCHAR(10) NULL DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `brand_id` VARCHAR(10) NULL DEFAULT NULL AFTER `tax_class`,
ADD COLUMN `block_proc` VARCHAR(45) NULL DEFAULT NULL AFTER `brand_id`,
ADD COLUMN `icg` VARCHAR(10) NULL DEFAULT NULL AFTER `block_proc`,
ADD COLUMN `base_uom` VARCHAR(10) NULL DEFAULT NULL AFTER `icg`,
ADD COLUMN `dchain_uom` VARCHAR(10) NULL DEFAULT NULL AFTER `base_uom`,
ADD COLUMN `dist_channel` VARCHAR(10) NULL DEFAULT NULL AFTER `dchain_uom`;

ALTER TABLE `ruparupadb_2`.`attribute_sap`
ADD COLUMN `netto_weight` VARCHAR(30) NULL AFTER `characteristic`;
