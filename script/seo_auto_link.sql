CREATE TABLE `seo_auto_link` (
  `seo_auto_link_id` INT NOT NULL AUTO_INCREMENT,
  `seo_header` LONGTEXT CHARACTER SET 'utf8' NULL,
  `seo_footer` LONGTEXT CHARACTER SET 'utf8' NULL,
  `seo_link` VARCHAR(50) NULL,
  `company_code` CHAR(3) NULL,
  `status` INT NULL DEFAULT 10,
  `is_default` INT NULL DEFAULT 0,
  `hero_product` LONGTEXT CHARACTER SET 'utf8' NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`seo_auto_link_id`));

ALTER TABLE `seo_auto_link` 
ADD COLUMN `flag` INT(1) NULL DEFAULT 0 AFTER `is_default`;

ALTER TABLE `seo_auto_link` 
ADD COLUMN `typo` INT(1) NULL DEFAULT 0 AFTER `is_default`;


