ALTER TABLE master_kecamatan
ADD COLUMN `same_day` TINYINT(4) NULL DEFAULT '0' AFTER `sap_exp_code`;

ALTER TABLE master_kecamatan
ADD COLUMN `instant_delivery` TINYINT(4) NULL DEFAULT '0' AFTER `same_day`;

UPDATE master_kecamatan mk
left join master_city mc on mc.city_id= mk.city_id
left join master_province mp on mp.province_id = mc.province_id
SET instant_delivery = '1'
where city_name like '%jakarta%' or city_name like '%bekasi%' or city_name like '%bogor%'or city_name like '%tangerang%';

UPDATE master_kecamatan mk
left join master_city mc on mc.city_id= mk.city_id
left join master_province mp on mp.province_id = mc.province_id
SET same_day = '1'
where city_name like '%jakarta%';

CREATE TABLE gosend_mapping (
  `gosend_mapping_id` INT(11) NOT NULL AUTO_INCREMENT,
  `kecamatan_code` VARCHAR(200) NULL,
  `pickup_code` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`gosend_mapping_id`));

ALTER TABLE customer_address
ADD COLUMN `geolocation` VARCHAR(100) NULL AFTER `is_default`;

ALTER TABLE sales_customer
ADD COLUMN `customer_phone` VARCHAR(50) NULL AFTER `customer_is_guest`;

ALTER TABLE sales_order_item
ADD COLUMN `group_shipment` TINYINT(4) NULL AFTER `status_preorder`;

ALTER TABLE sales_order_address
ADD COLUMN `geolocation` VARCHAR(100) NULL AFTER `address_name`,
ADD COLUMN `carrier_id` TINYINT(4) NULL AFTER `geolocation`,
ADD COLUMN `group_shipment` TINYINT(4) NULL AFTER `carrier_id`;

ALTER TABLE sales_shipment
ADD COLUMN `group_shipment` TINYINT(4) NULL AFTER `updated_at`;

ALTER TABLE pickup_point
ADD COLUMN `geolocation` VARCHAR(100) NULL AFTER `updated_at`,
ADD COLUMN `is_express_courier` TINYINT(4) NULL DEFAULT 0 AFTER `geolocation`;