CREATE TABLE `customer_fbleads` (
  `customer_fbleads` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `landing_page_url` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_fbleads`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
