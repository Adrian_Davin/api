CREATE TABLE `ruparupadb_2_aceapps`.`customer_ace_warranty_service` (
  `customer_ace_warranty_service_id` INT NOT NULL,
  `customer_ace_order_warranty_id` INT NOT NULL,
  `ace_service_number` VARCHAR(255) NULL,
  `status` INT NULL,
  `service_warranty_charge` DECIMAL(12,2) NULL,
  `service_warranty_items` JSON NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` VARCHAR(50) NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` VARCHAR(50) NULL,
  PRIMARY KEY (`customer_ace_warranty_service_id`),
	INDEX `fk_customer_ace_warranty_service_1_idx` (`customer_ace_order_warranty_id` ASC),
	CONSTRAINT `fk_customer_ace_warranty_service_1`
    FOREIGN KEY (`customer_ace_order_warranty_id`)
    REFERENCES `ruparupadb_2_aceapps`.`customer_ace_order_warranty` (`customer_ace_order_warranty_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);