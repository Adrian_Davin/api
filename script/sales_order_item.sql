ALTER TABLE `ruparupadb_2`.`sales_order_item`
ADD COLUMN `gift_cards_amount` DECIMAL(12,2) NULL DEFAULT 0 AFTER `shipping_discount_amount`;

ALTER TABLE `sales_order_item`
ADD COLUMN `price_modified` VARCHAR(255) NULL DEFAULT NULL AFTER `selling_price`;

ALTER TABLE `sales_order_item` 
ADD COLUMN `first_utm_parameter` TEXT NULL AFTER `group_shipment`,
ADD COLUMN `utm_parameter` TEXT NULL AFTER `first_utm_parameter`;

ALTER TABLE `sales_order_item` 
ADD COLUMN `site_source` VARCHAR(20) NULL AFTER `utm_parameter`;

ALTER TABLE `ruparupadb_2_marketplace`.`sales_order_item` 
ADD COLUMN `preorder_date` VARCHAR(45) NULL DEFAULT '' AFTER `updated_at`;
