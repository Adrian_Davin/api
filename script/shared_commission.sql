ALTER TABLE `ruparupadb_2`.`sales_invoice_item`
ADD COLUMN `commission` VARCHAR(10) NULL DEFAULT 0 AFTER `company_code`;

ALTER TABLE `ruparupadb_2`.`sales_order_item`
ADD COLUMN `commission` VARCHAR(10) NULL DEFAULT 0 AFTER `selling_price`;

ALTER TABLE `ruparupadb_2`.`sales_invoice_item`
ADD COLUMN `net_sales` DECIMAL(12,2) NULL DEFAULT 0 AFTER `commission`;

ALTER TABLE `ruparupadb_2`.`sales_order_item`
ADD COLUMN `net_sales` DECIMAL(12,2) NULL DEFAULT 0 AFTER `commission`;

CREATE TABLE `product_commission_net_sales_batch` (
  `batch_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'created_at',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  PRIMARY KEY (`batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `ruparupadb_2`.`product_net_sales` (
  `net_sales_id` INT NOT NULL AUTO_INCREMENT,
  `sku` VARCHAR(20) NULL,
  `product_variant_id` INT(11) NULL DEFAULT '0',
  `net_sales` DECIMAL(12,2) NULL DEFAULT NULL,
  `special_net_sales` DECIMAL(12,2) NULL DEFAULT '0.00',
  `special_from_date` DATETIME NULL DEFAULT NULL,
  `special_to_date` DATETIME NULL DEFAULT NULL,
  `status` TINYINT(4) NULL DEFAULT '10' COMMENT '-1 => delete\n0 => disabled\n10 => enabled',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`net_sales_id`));

CREATE TABLE `product_commission` (
  `commission_id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(20) DEFAULT NULL,
  `product_variant_id` int(11) DEFAULT '0',
  `commission` decimal(12,2) DEFAULT NULL,
  `special_commission` decimal(12,2) DEFAULT '0.00',
  `special_from_date` datetime DEFAULT NULL,
  `special_to_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '10' COMMENT '-1 => delete\n0 => disabled\n10 => enabled',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`commission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
