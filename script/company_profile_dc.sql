CREATE TABLE `ruparupadb_2_margin`.`company_profile_dc` (
  `company_id` INT NOT NULL AUTO_INCREMENT,
  `sales_office` VARCHAR(10) NULL,
  `sales_office_description` VARCHAR(60) NULL,
  `sales_group` VARCHAR(10) NULL,
  `sales_group_description` VARCHAR(60) NULL,
  `salesman` VARCHAR(45) NULL,
  `salesman_code` VARCHAR(60) NULL,
  `company_code` VARCHAR(10) NULL,
  `store_code` VARCHAR(10) NULL,
  PRIMARY KEY (`company_id`));

