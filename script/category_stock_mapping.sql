CREATE TABLE `category_stock_mapping` (
  `category_id` varchar(255) NOT NULL,
  `pickup_id` int(11) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`pickup_id`,`sku`,`category_id`)
);

#TAMBAH CATEGORY PRODUCT DAN PILIH PRODUCT DI GIDEON
#IMPORT CATEGORY DAN PRODUCT DI TABLE CAETEGORY_STOCK_MAPPING
#TAMBAH MAPPING
"category_is_in_stock": {
	            "type": "nested",
	            "properties": {
	              "category_id": {
	                "type": "keyword",
	                "store": true,
	                "include_in_all": false
	              },
	              "url_key": {
	                "type": "keyword",
	                "store": true,
	                "include_in_all": false
	              },
	              "is_in_stock": {
	                "type": "keyword",
	                "store": true,
	                "include_in_all": false
	              }
	            }
	          },
#Reindex