CREATE TABLE `ttpu_detail` (
  `ttpu_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `ttpu_id` int(11) DEFAULT NULL,
  `koli` varchar(45) DEFAULT NULL,
  `order_no` varchar(45) DEFAULT NULL,
  `invoice_no` varchar(45) DEFAULT NULL,
  `detail_status` varchar(45) DEFAULT NULL,
  `delivery_method` varchar(45) DEFAULT NULL,
  `customer_name` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`ttpu_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

CREATE TABLE `ttpu_header` (
  `ttpu_id` int(11) NOT NULL AUTO_INCREMENT,
  `ttpu_no` varchar(45) DEFAULT NULL,
  `store_code` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT 'pending',
  `page` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ttpu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

CREATE TABLE `ttpu_items` (
  `ttpu_items_id` int(11) NOT NULL AUTO_INCREMENT,
  `ttpu_detail_id` int(11) DEFAULT NULL,
  `sku` varchar(45) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`ttpu_items_id`),
  UNIQUE KEY `ttpu_detail_id_UNIQUE` (`ttpu_items_id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=latin1;
