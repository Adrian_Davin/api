CREATE TABLE salesrule_registration_pool (
  salesrule_registration_pool_id int(11) NOT NULL AUTO_INCREMENT,
  title varchar(100) DEFAULT NULL,
  company_code varchar(45) DEFAULT 'ODI',
  voucher_rules text,
  first_reminder tinyint(4) DEFAULT NULL,
  second_reminder tinyint(4) DEFAULT NULL,
  third_reminder tinyint(4) DEFAULT NULL,
  fourth_reminder tinyint(4) DEFAULT NULL,
  second_reminder_template_id int(11) DEFAULT '0',
  fourth_reminder_template_id int(11) DEFAULT '0',
  status tinyint(4) DEFAULT '10' COMMENT '-1 => deleted\n0 => disabled\n10 => enabled',
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  PRIMARY KEY (salesrule_registration_pool_id)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

INSERT INTO salesrule_registration_pool
(title,company_code,voucher_rules,first_reminder,second_reminder,third_reminder,fourth_reminder,second_reminder_template_id,fourth_reminder_template_id,status,created_at) 
VALUES
('Voucher Registrasi ODI', 'ODI', '[{\"rule_id\":13655,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":30000},{\"rule_id\":13651,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":20000},{\"rule_id\":13653,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":30000},{\"rule_id\":14556,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":50000},{\"rule_id\":13652,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":40000},{\"rule_id\":13654,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":30000}]', '7', '30', '-14', '-30', '25563194', '25563194', '10', now()),
('Voucher Registrasi AHI', 'AHI', '[{\"rule_id\":14560,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":10,\"max_redemption\":50000,\"voucher_amount\":0},{\"rule_id\":14562,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":10,\"max_redemption\":20000,\"voucher_amount\":0},{\"rule_id\":14558,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":10000},{\"rule_id\":14559,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":10,\"max_redemption\":50000,\"voucher_amount\":0},{\"rule_id\":14561,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":10,\"max_redemption\":20000,\"voucher_amount\":0},{\"rule_id\":14557,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":10000}]', '7', '30', '-14', '-30', '25645928', '25645928', '10', now()),
('Voucher Registrasi TGI', 'TGI', '[{\"rule_id\":14563,\"prefix\":\"REG\",\"code_length\":10,\"expiration_days\":90,\"percentage\":0,\"max_redemption\":0,\"voucher_amount\":50000}]', '7', '30', '-14', '-30', '25598744', '25598744', '10', now());

-- add menu to marketing_admin_role
UPDATE marketing_admin_role SET module_access = '{\"customer_management\":{\"customer_alert\":[\"view\",\"edit\",\"add\"]},\"promotions\":{\"sales_rules\":[\"view\",\"edit\",\"add\"],\"voucher_registration_pool\":[\"view\",\"edit\",\"add\"],\"event\":[\"view\",\"edit\",\"add\"]},\"user_account\":{\"my_account\":[\"view\",\"edit\"]},\"payment_method\":{\"virtual_account\":[\"view\",\"edit\"],\"credit_card_installment\":[\"view\",\"edit\"],\"emoney\":[\"view\",\"edit\"]},\"store\":{\"filter\":\"\"}}' WHERE (role_id = 1);
UPDATE marketing_admin_role SET module_access = '{\"customer_management\":{\"customer_alert\":[\"view\",\"edit\",\"add\"]},\"promotions\":{\"sales_rules\":[\"view\",\"edit\",\"add\"],\"voucher_registration_pool\":[\"view\",\"edit\",\"add\"],\"event\":[\"view\"]},\"user_account\":{\"my_account\":[\"view\",\"edit\"]},\"payment_method\":{\"virtual_account\":[\"view\",\"edit\"],\"credit_card_installment\":[\"view\",\"edit\"],\"emoney\":[\"view\",\"edit\"]},\"store\":{\"filter\":\"\"}}' WHERE (role_id = 8);