# CMS_BLOCK
UPDATE `ruparupadb_multicompany`.`cms_block` SET identifier = title
WHERE identifier IS NULL;
ALTER TABLE `ruparupadb_multicompany`.`cms_block` 
ADD COLUMN `company_code` VARCHAR(45) NULL DEFAULT 'ODI' AFTER `body_html`;

# STATIC_PAGE
ALTER TABLE `ruparupadb_multicompany`.`static_page` 
ADD COLUMN `company_code` VARCHAR(45) NULL DEFAULT 'ODI' AFTER `body_html_mobile`;
ALTER TABLE `ruparupadb_multicompany`.`static_page` 
ADD COLUMN `identifier` VARCHAR(255) NULL DEFAULT '' AFTER `title`;

# URL_KEY
ALTER TABLE `ruparupadb_multicompany`.`master_url_key` 
DROP COLUMN `inspiration_id`,
DROP COLUMN `category_id`,
ADD COLUMN `company_code` VARCHAR(45) NOT NULL DEFAULT 'ODI' AFTER `reference_id`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`url_key`, `company_code`);

# CATEGORY
ALTER TABLE `ruparupadb_multicompany`.`product_category` 
ADD COLUMN `company_code` VARCHAR(45) NULL DEFAULT 'ODI' AFTER `rule_based_hero_products`;