SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for shipping_courier_credential
-- ----------------------------
DROP TABLE IF EXISTS `shipping_courier_credential`;
CREATE TABLE `shipping_courier_credential` (
  `courier_account` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `company_id` varchar(50) DEFAULT NULL,
  `allocation_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`courier_account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `ruparupadb_2`.`shipping_courier_credential`
ADD COLUMN `shipping_assignation` TEXT NULL AFTER `allocation_code`;

-- ----------------------------
-- Records of shipping_courier_credential
-- ----------------------------
/** BELOW ARE PRODUCTION CREDENTIALL **/
INSERT INTO `shipping_courier_credential` VALUES ('sap_dc', 'RUPARUPA', 'Rupa2811', 'CGKN01627', '1128', 'DC');
INSERT INTO `shipping_courier_credential` VALUES ('sap_tgr', 'RUPARUPA106', 'Rupa2106', 'CGKN02325', '1506', 'pickup_livingworld,pickup_qbig_bsd,pickup_supermall_karawaci');
INSERT INTO `shipping_courier_credential` VALUES ('sap_mdn', 'RUPARUAPAKNO', 'Rupa0458', 'KNON00458', '1058', 'pickup_juanda_medan,pickup_center_point_medan,pickup_s_parman_medan');
INSERT INTO `shipping_courier_credential` VALUES ('sap_sby', 'RUPARUPASUB', 'Rupa0813', 'SUBN00813', '1013', 'pickup_lenmarchsurabaya,pickup_tunjungan_plaza_surabaya,pickup_royal_plaza_surabaya_ace,pickup_royal_plaza_surabaya_informa,pickup_eastcoast,pickup_sidoarjo');
INSERT INTO `shipping_courier_credential` VALUES ('sap_dps', 'RUPARUPADPS', 'Rupa0321', 'DPSN00321', '1021', 'pickup_bali_galeria,pickup_living_plaza_gatsu_denpasar,pickup_gatsu_denpasar');
INSERT INTO `shipping_courier_credential` VALUES ('sap_mkr', 'RUPARUAPAUPG', 'Rupa0188', 'UPGN00188', '1088', 'pickup_latanete_makassar,pickup_living_plaza_pettarani,pickup_panakkukang_makasar');

/** BELOW ARE TESTING CREDENTIALL **/
INSERT INTO `shipping_courier_credential` VALUES ('sap_dc', 'MAJUBERSAMA', 'Majubersama987', 'CGKN02325', '1109', 'DC');
INSERT INTO `shipping_courier_credential` VALUES ('sap_tgr', 'MAJUBERSAMA', 'Majubersama987', 'CGKN02325', '1109', 'pickup_livingworld,pickup_qbig_bsd,pickup_supermall_karawaci');
INSERT INTO `shipping_courier_credential` VALUES ('sap_mdn', 'MAJUBERSAMA', 'Majubersama987', 'CGKN02325', '1109', 'DC');
INSERT INTO `shipping_courier_credential` VALUES ('sap_sby', 'MAJUBERSAMA', 'Majubersama987', 'CGKN02325', '1109', 'pickup_livingworld,pickup_qbig_bsd,pickup_supermall_karawaci');
INSERT INTO `shipping_courier_credential` VALUES ('sap_bali', 'MAJUBERSAMA', 'Majubersama987', 'CGKN02325', '1109', 'DC');
INSERT INTO `shipping_courier_credential` VALUES ('sap_mkr', 'MAJUBERSAMA', 'Majubersama987', 'CGKN02325', '1109', 'pickup_livingworld,pickup_qbig_bsd,pickup_supermall_karawaci');
