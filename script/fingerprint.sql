ALTER TABLE `customer` 
ADD COLUMN `fingerprint` VARCHAR(128) NULL AFTER `company_code`;

ALTER TABLE `customer_alert` 
ADD COLUMN `fingerprint` VARCHAR(128) NULL AFTER `status`;
