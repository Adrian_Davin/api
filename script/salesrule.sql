ALTER TABLE `ruparupadb_2`.`salesrule`
ADD COLUMN `max_quantity` TINYINY NULL AFTER `max_redemption`;

ALTER TABLE `ruparupadb_develop`.`salesrule`
CHANGE COLUMN `discount_type` `discount_type` VARCHAR(20) NULL DEFAULT 'percent' COMMENT '- percent\n- fix' ;

ALTER TABLE `ruparupadb_2_20180810`.`salesrule_split_budget` 
ADD COLUMN `type` VARCHAR(45) NULL AFTER `rule_id`;

ALTER TABLE `ruparupadb_2_20180810`.`salesrule`
ADD COLUMN `tnc` LONGTEXT NOT NULL AFTER `updated_at`;

ALTER TABLE `ruparupadb_2_20180810`.`salesrule`
ADD COLUMN `total_purchase` DECIMAL(13,0) NULL AFTER `uses_per_customer_frequence`,
ADD COLUMN `split_combine` TINYINT(4) NULL DEFAULT 0 AFTER `split_amount`,
ADD COLUMN `purchase_days` INT(11) NULL AFTER `total_purchase`,
ADD COLUMN `schedule` VARCHAR(255) NOT NULL DEFAULT '' AFTER `to_date`,
ADD COLUMN `can_refund` TINYINT(4) NOT NULL DEFAULT '0' AFTER `stop_rules_processing`;
-- ADD COLUMN `cashback_to_date` DATETIME NULL AFTER `to_date`,


ALTER TABLE salesrule ADD from_time varchar(5) NULL COMMENT 'Rule start of hour usability';
ALTER TABLE salesrule ADD to_time varchar(5) NULL COMMENT 'Rule end of hour usability';