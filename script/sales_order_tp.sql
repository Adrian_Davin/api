CREATE TABLE `sales_order_tp` (
  `tp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sap_no` varchar(255) DEFAULT NULL,
  `odi` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `store_code` varchar(255) DEFAULT NULL,
  `qty` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`tp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=361 DEFAULT CHARSET=latin1;