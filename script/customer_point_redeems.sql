CREATE TABLE `ruparupadb_2_aceapps`.`customer_point_redeem` (
    `customer_point_redeem_id` INT NOT NULL AUTO_INCREMENT,
    `customer_id` INT(11) NOT NULL,
    `sales_rule_voucher_id` INT(11) UNSIGNED NULL DEFAULT NULL,
    `point_redeemed` INT NULL,
    `company_code` ENUM('ODI', 'AHI', 'HCI', 'TGI') NOT NULL,
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    INDEX `fk_customer_point_redeems_1_idx` (`customer_id` ASC),
    INDEX `fk_customer_point_redeems_2_idx` (`sales_rule_voucher_id` ASC),
    CONSTRAINT `fk_customer_point_redeems_1` FOREIGN KEY (`customer_id`)
        REFERENCES `ruparupadb_2_aceapps`.`customer` (`customer_id`)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT `fk_customer_point_redeems_2` FOREIGN KEY (`sales_rule_voucher_id`)
        REFERENCES `ruparupadb_2_aceapps`.`salesrule_voucher` (`voucher_id`)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (`customer_point_redeem_id`)
);