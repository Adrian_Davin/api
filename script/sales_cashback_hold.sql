CREATE TABLE `sales_cashback_hold` (
  `sales_cashback_hold_id` INT NOT NULL AUTO_INCREMENT,
  `order_no` VARCHAR(45) NULL,
  `status` INT(1) NULL,
  `type` VARCHAR(45) NULL,
  PRIMARY KEY (`sales_cashback_hold_id`));


INSERT INTO `ruparupadb_2_jefri`.`master_email_template` (`template_code`, `template_id`, `company_code`) VALUES ('unrelease_customer_cashback', '21427847', 'ODI');
INSERT INTO `ruparupadb_2_jefri`.`master_email_template` (`template_code`, `template_id`, `company_code`) VALUES ('unrelease_customer_cashback', '21431237', 'AHI');
