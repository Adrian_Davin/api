ALTER TABLE `ruparupadb_2_margin`.`sales_order_item_giftcards` 
ADD COLUMN `split_voucher_amount_difference` DECIMAL(12,2) NULL AFTER `voucher_amount_difference`,
ADD COLUMN `split_discount_amount_difference` DECIMAL(12,2) NULL AFTER `split_voucher_amount_difference`;
