ALTER TABLE `customer`
ADD COLUMN `company_code` VARCHAR(45) NULL DEFAULT 'ODI' AFTER `registered_by`;

ALTER TABLE `customer` 
ADD COLUMN `is_new` TINYINT NULL DEFAULT 10 AFTER `status`;
