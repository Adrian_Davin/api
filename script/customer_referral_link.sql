ALTER TABLE customer_referral_link ADD referral_code varchar(100) NULL AFTER referral_link;

ALTER TABLE customer_referral_link
ADD CONSTRAINT referral_code UNIQUE (referral_code);