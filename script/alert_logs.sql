CREATE TABLE `ruparupadb_2`.`alert_logs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `invoice_no` VARCHAR(45) NULL,
  `msg` LONGTEXT NULL,
  `alert_type` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
