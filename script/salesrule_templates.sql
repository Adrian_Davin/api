CREATE TABLE IF NOT EXISTS `salesrule_templates` (
  `template_rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template Rule Id',
  `salesrule_action_id` int(11) DEFAULT NULL,
  `rule_type` varchar(100) DEFAULT NULL COMMENT 'shopping_cart, shipping, payment, items',
  `rule_group` int(11) DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL COMMENT 'Template name',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `rule_name` varchar(255) DEFAULT NULL,
  `conditions` text,
  `description` text COMMENT 'Description',
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `schedule` varchar(255) NOT NULL DEFAULT '',
  `use_voucher` smallint(5) unsigned DEFAULT '0' COMMENT '0 => No\n1 => Yes',
  `label_id` int(11) unsigned DEFAULT '0' COMMENT 'mapping with product_labels',
  `uses_per_customer` int(11) DEFAULT '0' COMMENT '1 CUSTOMER boleh memakai voucher X kali',
  `uses_per_customer_frequence` int(11) DEFAULT '0',
  `total_purchase` decimal(13,0) DEFAULT '0',
  `purchase_days` int(11) DEFAULT '0',
  `stop_rules_processing` smallint(6) DEFAULT '0' COMMENT 'Stop Rules Processing',
  `can_refund` tinyint(4) NOT NULL DEFAULT '0',
  `global_can_combine` tinyint(4) DEFAULT '0',
  `can_combine` tinyint(4) DEFAULT '0',
  `priority` int(10) DEFAULT '0' COMMENT 'Sort Order',
  `landing_page_name` varchar(255) DEFAULT NULL,
  `landing_page_link` text,
  `bank_name` varchar(50) DEFAULT NULL,
  `discount_type` varchar(20) DEFAULT 'percent' COMMENT '- percent\n- fix',
  `attributes` varchar(100) DEFAULT NULL COMMENT 'shipping_fee, product, grand_total, subtotal, items X',
  `discount_amount` decimal(12,2) DEFAULT '0.00' COMMENT 'Percent / Fix Off',
  `split_budget` tinyint(4) DEFAULT '0',
  `marketing_budget` decimal(12,0) DEFAULT '0',
  `merchandise_budget` decimal(12,0) DEFAULT '0',
  `qty_x_discount` int(10) DEFAULT NULL COMMENT 'Maximum Quantity that eligible for discount',
  `discount_step` tinyint(3) unsigned DEFAULT NULL COMMENT 'Discount Qty Step Items X',
  `discount_step_max` int(11) DEFAULT NULL COMMENT 'Maximum Qty Step items X',
  `discount_item_sku` text COMMENT 'items Y ( Applied for action buy X get Y )',
  `discount_item_qty` tinyint(4) DEFAULT NULL COMMENT 'Quantity items Y ( Applied for action buy X get Y )\nHow many items Y getting discount in single condition',
  `discount_item_qty_each` tinyint(4) DEFAULT NULL COMMENT 'Quantity each items Y ( Applied for action buy X get Y and Type Y is selected )\n',
  `discount_item_type` varchar(30) DEFAULT NULL COMMENT 'Type Items Y \n- selected\n- auto',
  `associate_rule_id` int(11) DEFAULT NULL,
  `voucher_expired` tinyint(4) NOT NULL DEFAULT '0',
  `voucher_generated` tinyint(4) DEFAULT '0' COMMENT '1 => automatic\n0 => manual',
  `max_redemption` decimal(11,2) DEFAULT NULL,
  `max_quantity` tinyint(4) NOT NULL DEFAULT '0',
  `label_action_id` int(11) unsigned DEFAULT '0' COMMENT 'mapping with product_label',
  `limit_payment_time` tinyint(4) NOT NULL DEFAULT '0',
  `limit_used` int(11) NOT NULL DEFAULT '0',
  `email_template_id` int(11) NOT NULL DEFAULT '0',
  `split_amount` decimal(11,0) DEFAULT '0',
  `split_combine` tinyint(4) DEFAULT '0',
  `shop_reminder_id` varchar(100) DEFAULT '',
  `email_voucher_created` int(11) DEFAULT '0' COMMENT 'days',
  `template_id_email_voucher_created` int(11) DEFAULT '0',
  `email_voucher_expired` int(11) DEFAULT '0' COMMENT 'days',
  `template_id_email_voucher_expired` int(11) DEFAULT '0',
  `pushnotif_voucher_created` int(11) DEFAULT '0',
  `pushnotif_voucher_expired` int(11) DEFAULT '0',
  `code_length` int(11) DEFAULT NULL,
  `code_format` varchar(45) DEFAULT NULL,
  `code_prefix` varchar(45) DEFAULT NULL,
  `code_suffix` varchar(45) DEFAULT NULL,
  `promo_type` varchar(45) DEFAULT NULL,
  `company_code` varchar(45) DEFAULT 'ODI',
  `status` smallint(6) NOT NULL DEFAULT '10' COMMENT 'Is Active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tnc` longtext NOT NULL,
  `publish_description` varchar(255) NOT NULL DEFAULT '',
  `display_description` varchar(125) NOT NULL DEFAULT '',
  `customer_groups` varchar(255) DEFAULT NULL,
  `budget_owner` varchar(100) DEFAULT NULL,
  `device_to_publish` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`template_rule_id`),
  KEY `IDX_SALESRULE_TEMPLATE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`status`,`priority`,`to_date`,`from_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Templates';



ALTER TABLE salesrule_templates ADD from_time varchar(5) NULL COMMENT 'Rule start of hour usability';
ALTER TABLE salesrule_templates ADD to_time varchar(5) NULL COMMENT 'Rule end of hour usability';