CREATE TABLE `ruparupadb_2`.`product_preorder` (
  `sku` VARCHAR(30) NOT NULL,
  `start_date` DATETIME NULL,
  `end_date` DATETIME NULL,
  PRIMARY KEY (`sku`),
  UNIQUE INDEX `sku_UNIQUE` (`sku` ASC));

ALTER TABLE `ruparupadb_2`.`sales_order_item`
ADD COLUMN `status_preorder` INT(5) NULL DEFAULT 0 AFTER `is_free_item`;

CREATE TABLE `ruparupadb_2`.`sales_invoice_review_notification` (
  `invoice_no` VARCHAR(50) NOT NULL,
  `status` TINYINT(3) NULL,
  PRIMARY KEY (`invoice_no`),
  UNIQUE INDEX `invoice_no_UNIQUE` (`invoice_no` ASC));

ALTER TABLE `ruparupadb_2`.`sales_invoice_review_notification`
ADD COLUMN `updated_at` DATETIME NULL DEFAULT NULL AFTER `status`;

CREATE TABLE `product_change_log` (
  `product_id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `description` text,
  `how_to_use` text,
  `tips_trick` text,
  `specification` text,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_id_UNIQUE` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ruparupadb_multivariant`.`product_category`
ADD COLUMN `is_rule_based` TINYINT(4) NULL AFTER `status`,
ADD COLUMN `filter_condition` TEXT NULL AFTER `is_rule_based`,
ADD COLUMN `rule_based_hero_products` TEXT NULL AFTER `filter_condition`;


ALTER TABLE `ruparupadb_multicompany`.`cms_block`
ADD COLUMN `company_code` VARCHAR(45) NULL DEFAULT 'ODI' AFTER `body_html`;

# Customer OTP
CREATE TABLE `customer_otp` (
  `customer_otp_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `access_code` varchar(55) DEFAULT NULL,
  `expired_on` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`customer_otp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

ALTER TABLE `customer_otp`
ADD UNIQUE INDEX `customer_id_UNIQUE` (`customer_id` ASC);
;

ALTER TABLE ruparupadb_2_20180810.customer ADD is_phone_verified TINYINT(4) DEFAULT 0 NOT NULL AFTER phone;

#