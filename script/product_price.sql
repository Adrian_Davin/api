ALTER TABLE `product_price` 
ADD COLUMN `last_modified` VARCHAR(255) NULL AFTER `special_to_date`,
ADD COLUMN `modified_description` VARCHAR(255) NULL AFTER `last_modified`;