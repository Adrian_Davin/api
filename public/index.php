<?php

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;

error_reporting(E_ALL);

define('APP_PATH', realpath('../app'));
define('VAR_PATH', realpath('../var'));

try {

    $di = new FactoryDefault();

    /**
     * Include Services
     */
    /** @noinspection PhpIncludeInspection */
    include APP_PATH . '/config/services.php';


    /**
     * Call the autoloader service.  We don't need to keep the results.
     */
    $di->getLoader();

    /**
     * Starting the application
     * Assign service locator to the application
     */
    $app = new Micro($di);

	$app->before(function() use ($app) {
		// Validate header content type, we only accept (for now) json type only
		$headers = $app->request->getHeaders();
        if(substr(@$headers['Content-Type'],0,16) != 'application/json') {
			// This handler can be stop but can not return the massage, so we throw exception and catch it
			$contentType = empty($headers['Content-Type']) ? "empty" : $headers['Content-Type'];
			$message = 'Content type only support application/json, receive content type ' . $contentType;

			throw new Library\HTTPException(
				"One of the HTTP headers specified in the request is not supported.",
				400,
				array(
					'msgTitle' => 'HTTP headers not supported',
					'dev' => $message
				)
			);
		}
	});

    /**
     * Include Application
     */
    /** @noinspection PhpIncludeInspection */
    include APP_PATH . '/app.php';

	// Mount collections
	$files = array_diff(scandir(APP_PATH."/collections/"), array('..', '.'));
	foreach ($files as $file) {
		$pathinfo = pathinfo($file);

		// For subdirectories
		if (!array_key_exists('extension', $pathinfo)) {
			$dirName = $pathinfo['basename'];
			$subFiles = array_diff(scandir(APP_PATH."/collections/$dirName"), array('..', '.'));
			foreach ($subFiles as $subFile) {
				$subPathInfo = pathinfo($subFile);
				if ($subPathInfo['extension'] === 'php') {
					/** @noinspection PhpIncludeInspection */
            		$microCollection = include_once(APP_PATH."/collections/$dirName/".$subPathInfo['basename']);
					$app->mount($microCollection);		
				}	
			}
		}

		else if ($pathinfo['extension'] === 'php') {
            /** @noinspection PhpIncludeInspection */
            $microCollection = include_once(APP_PATH."/collections/".$pathinfo['basename']);
			$app->mount($microCollection);
		}
	}

	/**
     * Handle the request
     */
    $app->handle();

} catch (\Library\HTTPException $e) {

	$e->send();
	return;
}
